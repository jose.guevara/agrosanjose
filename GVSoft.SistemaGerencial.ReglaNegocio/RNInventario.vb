﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNInventario
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "GVSoft.SistemaGerencial.ReglaNegocio.RNInventario"


    Public Shared Function ObtieneEncabezadoTrasladosXFecha(ByVal FechaIncial As Integer, ByVal FechaFinal As Integer, ByVal IdAgencia As Integer) As DataTable
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroArqueo As Integer = 0
        Dim sNumeroArqueo As String = String.Empty
        Try
            Return ADInventario.ObtieneEncabezadoTrasladosXFecha(FechaIncial, FechaFinal, IdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Sub IngresaTransaccionPedidos(ByVal objPedidos As SEMaestroPedidos, ByVal lstPedidosDet As List(Of SEDetallePedidos), ByVal Serie As String)
        Try
            ADInventario.IngresaTransaccionPedidos(objPedidos, lstPedidosDet, Serie)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ObtieneNumeroPedido(ByVal RegistroAgencia As Int16, ByVal Serie As String, ByVal TipoFact As Integer) As String
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroPedido As Integer = 0
        Dim sNumeroPedido As String = String.Empty
        Try
            NumeroPedido = ADInventario.ObtieneNumeroPedido(RegistroAgencia, Serie, TipoFact)
            'sNumeroProforma = Format(NumeroProforma + 1, "000000000000")
            sNumeroPedido = Format(NumeroPedido + 1, "0000000")

            Return sNumeroPedido
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function


    Public Shared Function ExtraerPedidoXNumeroPedido(ByVal strNumeroFact As String) As IDataReader
        Try
            Return ADInventario.ExtraerPedidoXNumeroPedido(strNumeroFact)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneDetalleProductosPedidos(ByVal strNumeroPedido As String) As IDataReader
        Try
            Return ADInventario.ObtieneDetalleProductosPedidos(strNumeroPedido)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneDetalleProformasAFacturar(ByVal strNumeroPedido As String) As IDataReader
        Try
            Return ADInventario.ObtieneDetalleProformasAFacturar(strNumeroPedido)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneSerieParametrosTrasladoInventario(ByVal pnIdAgencia As Integer) As IDataReader
        Try
            Return ADInventario.ObtieneSerieParametrosTrasladoInventario(pnIdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

End Class
