﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Windows.Forms

Public Class RNAgencias
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "RNAgencias"


    Public Shared Function ObtieneAgenciaDefault() As Integer
        Try
            Return ADAgencia.ObtieneAgenciaDefault()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneAgenciaDefaultDr() As IDataReader
        Try
            Return ADAgencia.ObtieneAgenciaDefaultDr()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneAgenciasxUsuarioTodos(ByVal IdRegistroUsuario As Integer) As DataTable
        Try
            Return ADAgencia.ObtieneAgenciasxUsuarioTodos(IdRegistroUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneAgenciaxCodigo(ByVal IdSucursal As Integer) As DataTable
        Try
            Return ADAgencia.ObtieneAgenciaxCodigo(IdSucursal)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Sub EliminaAgenciasxUsuario(ByVal IdRegistroUsuario As Integer)
        Try
            ADUsuarios.EliminaAgenciasxUsuarios(IdRegistroUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub CargarComboAgencias(ByVal ComboAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx, ByVal nIdUser As Integer)
        Dim drAgencias As DataRow
        Dim dtAgencias As DataTable
        Try
            dtAgencias = RNAgencias.ObtieneAgenciasxUsuarioTodos(nIdUser)
            drAgencias = dtAgencias.NewRow()
            drAgencias("Registro") = 0
            drAgencias("Descripcion") = "<Seleccione Agencia>"
            dtAgencias.Rows.Add(drAgencias)

            ComboAgencias.DataSource = dtAgencias
            ComboAgencias.DisplayMember = "Descripcion"
            ComboAgencias.ValueMember = "Registro"
            ComboAgencias.SelectedValue = ObtieneAgenciaDefault()

        Catch ex As Exception
            'MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return
        End Try
    End Sub


    Public Shared Sub CargarComboAgenciasConTodos(ByVal ComboAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx, ByVal nIdUser As Integer)
        Dim drAgencias As DataRow
        Dim dtAgencias As DataTable
        Try
            dtAgencias = RNAgencias.ObtieneAgenciasxUsuarioTodos(nIdUser)
            drAgencias = dtAgencias.NewRow()
            drAgencias("Registro") = 0
            drAgencias("Descripcion") = "<Todas las Agencia>"
            dtAgencias.Rows.Add(drAgencias)

            ComboAgencias.DataSource = dtAgencias
            ComboAgencias.DisplayMember = "Descripcion"
            ComboAgencias.ValueMember = "Registro"
            ComboAgencias.SelectedValue = 0
        Catch ex As Exception
            'MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return
        End Try
    End Sub

    Public Shared Sub CargarComboAgenciasSinDefault(ByVal ComboAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx, ByVal nIdUser As Integer)
        '  Dim drAgencias As DataRow
        Dim dtAgencias As DataTable
        Try
            dtAgencias = RNAgencias.ObtieneAgenciasxUsuarioTodos(nIdUser)
            'drAgencias = dtAgencias.NewRow()
            'drAgencias("Registro") = 0
            'drAgencias("Descripcion") = "<Seleccione Agencia>"
            'dtAgencias.Rows.Add(drAgencias)

            ComboAgencias.DataSource = dtAgencias
            ComboAgencias.DisplayMember = "Descripcion"
            ComboAgencias.ValueMember = "Registro"
            ' ComboAgencias.SelectedValue = 0
        Catch ex As Exception
            'MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return
        End Try
    End Sub

    Public Shared Function ObtieneAgenciaCasaMatriz() As IDataReader
        Try
            Return ADAgencia.ObtieneAgenciaCasaMatriz()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneAgenciasxUsuarioExcluyendoParametro(ByVal pnIdAgencia As Integer) As IDataReader
        Try
            Return ADAgencia.ObtieneAgenciasxUsuarioExcluyendoParametro(pnIdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
