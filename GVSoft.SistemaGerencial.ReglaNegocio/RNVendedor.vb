﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNVendedor
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNVendedor"

    Public Shared Function ObtieneVendedorCodigoVendedor(ByVal psCodigoVendedor As String) As DataTable
        Dim dtRecibos As DataTable = Nothing
        Try
            dtRecibos = ADVendedores.ObtieneVendedorCodigoVendedor(psCodigoVendedor)
            Return dtRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return dtRecibos
        End Try
    End Function

End Class
