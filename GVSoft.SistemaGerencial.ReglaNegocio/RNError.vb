﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports System.Linq

Public Class RNError
    Public Shared Sub IntgresardetError(ByVal Clase As String, ByVal Metodo As String, ByVal Sentencia As String, _
    ByVal Descripcion As String)
        Try
            RNError.IntgresardetError(Clase, Metodo, Sentencia, Descripcion)
        Catch ex As Exception
            ADError.IntgresardetError("RNError", "IntgresardetError", "", ex.Message.ToString())
            Throw ex
        End Try
    End Sub

End Class
