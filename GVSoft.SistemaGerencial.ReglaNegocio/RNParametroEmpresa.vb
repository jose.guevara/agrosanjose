﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNParametroEmpresa

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "RNParametroEmpresa"

    Public Shared Function ObtieneParametroEmprea() As DataTable
        Try
            Return ADParametroEmpresa.ObtieneParametroEmpresa()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneParametroEmpreaEN() As SEParametroEmpresa
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dtDatos = ADParametroEmpresa.ObtieneParametroEmpresa()
            If SUFunciones.ValidaDataTable(dtDatos) Then
                objParametroEmpresa = New SEParametroEmpresa()
                objParametroEmpresa.IdEmpresa = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("IdEmpresa"))
                objParametroEmpresa.IndicaAnulaFacturaMesAnterior = 0
                If dtDatos.Rows(0)("IndicaAnulaFacturaMesAnterior").ToString.ToLower() = "true" Then
                    objParametroEmpresa.IndicaAnulaFacturaMesAnterior = 1
                End If
                objParametroEmpresa.IndicaAnulaReciboMesAnterior = 0
                If dtDatos.Rows(0)("IndicaAnulaReciboMesAnterior").ToString.ToLower() = "true" Then
                    objParametroEmpresa.IndicaAnulaReciboMesAnterior = 1
                End If
                objParametroEmpresa.IndicaAnulaCompraMesAnterior = 0
                If dtDatos.Rows(0)("IndicaAnulaCompraMesAnterior").ToString.ToLower() = "true" Then
                    objParametroEmpresa.IndicaAnulaCompraMesAnterior = 1
                End If


                objParametroEmpresa.MonedaFacturaContado = 0
                objParametroEmpresa.MonedaFacturaContado = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("MonedaFacturaContado"))
                objParametroEmpresa.MonedaFacturaCredito = 0
                objParametroEmpresa.MonedaFacturaCredito = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("MonedaFacturaCredito"))

                objParametroEmpresa.CantidadItemFactura = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("CantidadItemFactura"))
                objParametroEmpresa.CantidadItemFacturaRapida = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("CantidadItemFacturaRapida"))
                objParametroEmpresa.CantidadItemRecibos = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("CantidadItemRecibos"))


                objParametroEmpresa.PermiteFacturarBajoCosto = dtDatos.Rows(0)("PermiteFacturarBajoCosto").ToString.ToLower()
                objParametroEmpresa.PermiteFacturarClienteSinDisponible = dtDatos.Rows(0)("PermiteFacturarClienteSinDisponible").ToString.ToLower()
                objParametroEmpresa.PermiteFacturarClienteConFacturasPendiente = dtDatos.Rows(0)("PermiteFacturarClienteFacturasPendiente").ToString.ToLower()
                objParametroEmpresa.IndicaTipoRedondeo = dtDatos.Rows(0)("IndicaTipoRedondeo").ToString.ToUpper()

                objParametroEmpresa.PermitePagarFacturasRecientesConAntiguaPendiente = dtDatos.Rows(0)("PermitePagarFacturasRecientesConAntiguaPendiente").ToString.ToLower()

                objParametroEmpresa.OpcionFacturarClienteSinDisponible = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("OpcionFacturarClienteSinDisponible").ToString.Trim())
                objParametroEmpresa.OpcionFacturarClienteConFacturasPendiente = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("OpcionFacturarClienteFacturasPendiente").ToString.Trim())
                objParametroEmpresa.OpcionFacturarBajoCosto = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("OpcionFacturarBajoCosto").ToString.Trim())
                objParametroEmpresa.DesactivaObjetoFechaEnOperaciones = dtDatos.Rows(0)("DesactivaObjetoFechaEnOperaciones").ToString.ToLower()
                objParametroEmpresa.CantidadDecimalRedondeo = SUConversiones.ConvierteAInt(dtDatos.Rows(0)("CantidadDecimalRedondeo"))
                objParametroEmpresa.PermiteFacturarSinExistenciaProducto = 0
                If dtDatos.Rows(0)("PermiteFacturarSinExistenciaProducto").ToString.ToLower() = "true" Then
                    objParametroEmpresa.PermiteFacturarSinExistenciaProducto = 1
                End If
            End If
            Return objParametroEmpresa
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

End Class
