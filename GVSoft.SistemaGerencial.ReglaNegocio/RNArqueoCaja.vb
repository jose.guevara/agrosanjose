﻿Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNArqueoCaja

    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    'Public Function ObtenerNumeroArqueo(ByVal RegistroAgencia As Int16) As IDataReader
    '    Dim drRecibos As IDataReader
    '    Dim clsADRecibos As New ADRecibos()

    '    Try
    '        drRecibos = ADArqueoCaja.ObtieneNumeroArqueoParametro(RegistroAgencia)
    '        Return drRecibos
    '    Catch ex As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = "RNArqueoCaja"
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = ex.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
    '        SDError.IngresarError(objError)
    '        Return Nothing
    '    End Try
    'End Function

    Public Shared Function ObtenerNumeroArqueo(ByVal RegistroAgencia As Int16) As String
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroArqueo As Integer = 0
        Dim sNumeroArqueo As String = String.Empty
        Try
            NumeroArqueo = ADArqueoCaja.ObtieneNumeroArqueoParametro(RegistroAgencia)
            sNumeroArqueo = Format(NumeroArqueo + 1, "000000000000")

            Return sNumeroArqueo
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ValidaExisteArqueos(ByVal RegistroAgencia As Int16) As Integer
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroArqueo As Integer = 0
        Dim sNumeroArqueo As String = String.Empty
        Try
            NumeroArqueo = ADArqueoCaja.ValidaExisteArqueos(RegistroAgencia)
            Return NumeroArqueo
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ValidaArqueoACerrar(ByVal RegistroAgencia As Integer, ByVal NumeroArqueo As String) As Integer
        Dim clsADRecibos As New ADRecibos()
        Dim Existe As Integer = 0
        Dim sNumeroArqueo As String = String.Empty
        Try
            Existe = ADArqueoCaja.ValidaArqueoACerrar(RegistroAgencia, NumeroArqueo)
            Return Existe
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ValidaArqueoAbierto() As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim Resultado As Object() = New Object(2) {}
        Dim sNumeroArqueo As String = String.Empty
        Dim dtArqAbierto As DataTable = Nothing

        Try
            dtArqAbierto = ADArqueoCaja.ValidaArqueoAbierto()
            Resultado(0) = dtArqAbierto.Rows(0).Item("ArqueoAbierto")
            Resultado(1) = dtArqAbierto.Rows(0).Item("NumeroCuadre")
            Return Resultado
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneSaldoInicial(ByVal RegistroAgencia As Int16) As Double
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroArqueo As Double
        Dim sNumeroArqueo As String = String.Empty
        Try
            NumeroArqueo = ADArqueoCaja.ObtieneSaldoInicial(RegistroAgencia)
            Return NumeroArqueo
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtenerUltimoArqueoxUsuario(ByVal IdUsuario As Integer) As DataTable
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroArqueo As Integer = 0
        Dim sNumeroArqueo As String = String.Empty
        Try
            Return ADArqueoCaja.ObtieneUltimoArqueoxUsuario(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneInformacionCuadreGuardadoACerrar(ByVal NumeroArqueo As Integer, ByVal IdAgencia As Integer) As DataTable
        Dim clsADRecibos As New ADRecibos()
        Dim sNumeroArqueo As String = String.Empty
        Try
            Return ADArqueoCaja.ObtieneInformacionCuadreGuardadoACerrar(NumeroArqueo, IdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtenerPrimeraFacturaxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As String) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtienePrimeraFacturaxFecha(pnIdUsuario, psFecha)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerPrimerReciboxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As String) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtienePrimerReciboxFecha(pnIdUsuario, psFecha)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerPrimeraFactura(ByVal pnIdUsuario As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtienePrimeraFactura(pnIdUsuario)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerPrimeraFacturaRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtienePrimeraFacturaRecalcular(pnIdUsuario, pnIdArqueo)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerPrimerRecibo(ByVal pnIdUsuario As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtienePrimerRecibo(pnIdUsuario)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerPrimerReciboRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtienePrimerReciboRecalcular(pnIdUsuario, pnIdArqueo)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerUltimaFacturaxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As String) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtieneUltimaFacturaxFecha(pnIdUsuario, psFecha)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerUltimoReciboxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As String) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtieneUltimoReciboxFecha(pnIdUsuario, psFecha)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerUltimaFactura(ByVal pnIdUsuario As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtieneUltimaFactura(pnIdUsuario)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerUltimaFacturaRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtieneUltimaFacturaRecalcular(pnIdUsuario, pnIdArqueo)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerUltimoRecibo(ByVal pnIdUsuario As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtieneUltimoRecibo(pnIdUsuario)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerUltimoReciboRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As Object()
        Dim clsADRecibos As New ADRecibos()
        Dim ldtPrimerNUmeroFactura As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            ldtPrimerNUmeroFactura = ADArqueoCaja.ObtieneUltimoReciboRecalcular(pnIdUsuario, pnIdArqueo)
            If ldtPrimerNUmeroFactura IsNot Nothing Then
                If ldtPrimerNUmeroFactura.Rows.Count > 0 Then
                    Resultado(0) = ldtPrimerNUmeroFactura.Rows(0).Item("numero")
                    Resultado(1) = ldtPrimerNUmeroFactura.Rows(0).Item("numfechaing")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerSaldoTotalFacturaRecibosCuadre(ByVal pnIdUsuario As Integer, ByVal sPrimerRecibo As String, ByVal sUltimoRecibo As String, ByVal sPrimerFactura As String, ByVal sUltimaFactura As String) As Object
        Dim clsADRecibos As New ADRecibos()
        Dim dtSaldos As DataTable = Nothing
        Dim Resultado As Object() = New Object(2) {}
        Try
            dtSaldos = ADArqueoCaja.ObtenerSaldoTotalFacturaRecibosCuadre(pnIdUsuario, sPrimerRecibo, sUltimoRecibo, sPrimerFactura, sUltimaFactura)
            If dtSaldos IsNot Nothing Then
                If dtSaldos.Rows.Count > 0 Then
                    Resultado(0) = dtSaldos.Rows(0).Item("TotalRecibos")
                    Resultado(1) = dtSaldos.Rows(0).Item("TotalFacturas")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function ObtenerFechaGlobal(ByVal psFechaFactura As String, ByVal psFechaRecibo As String, _
                                               ByVal lsInficaTipoFecha As String) As Date
        Dim ldFechaFactura As Date = Nothing
        Dim ldFechaRecibo As Date = Nothing
        Dim ldFecha As Date = Nothing

        Dim lnAnioFactura As Integer = 0
        Dim lnMesFactura As Integer = 0
        Dim lnDiaFactura As Integer = 0

        Dim lnAnioRecibo As Integer = 0
        Dim lnMesRecibo As Integer = 0
        Dim lnDiaRecibo As Integer = 0

        Dim Resultado As Object() = New Object(2) {}
        Try
            SUConversiones.ObtieneDiaMesAnio(psFechaFactura, lnDiaFactura, lnMesFactura, lnAnioFactura)
            ldFechaFactura = New Date(lnAnioFactura, lnMesFactura, lnDiaFactura)

            lnAnioRecibo = 0
            lnMesRecibo = 0
            lnDiaRecibo = 0

            SUConversiones.ObtieneDiaMesAnio(psFechaRecibo, lnDiaRecibo, lnMesRecibo, lnAnioRecibo)
            ldFechaRecibo = New Date(lnAnioRecibo, lnMesRecibo, lnDiaRecibo)

            If lsInficaTipoFecha = "I" Then
                If (ldFechaRecibo >= ldFechaFactura) Then
                    ldFecha = ldFechaFactura
                Else
                    ldFecha = ldFechaRecibo
                End If

            Else
                If (ldFechaRecibo >= ldFechaFactura) Then
                    ldFecha = ldFechaRecibo
                Else
                    ldFecha = ldFechaFactura
                End If

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return ldFecha
    End Function

    Public Shared Function ObtenerFechaInicioFin(ByVal psFechaInicioFactura As String, ByVal psFechaFinFactura As String, _
                                                 ByVal psFechaInicioRecibo As String, ByVal psFechaFinRecibo As String) As Object()
        Dim ldFechaInicioFactura As Date = Nothing
        Dim ldFechaFinFactura As Date = Nothing
        Dim ldFechaInicioRecibo As Date = Nothing
        Dim ldFechaFinRecibo As Date = Nothing
        Dim lsFechaInicio As String = Nothing
        Dim lsFechaFin As String = Nothing

        Dim lnAnioFactura As Integer = 0
        Dim lnMesFactura As Integer = 0
        Dim lnDiaFactura As Integer = 0

        Dim lnAnioRecibo As Integer = 0
        Dim lnMesRecibo As Integer = 0
        Dim lnDiaRecibo As Integer = 0

        Dim Resultado As Object() = New Object(2) {}
        Try
            SUConversiones.ObtieneDiaMesAnio(psFechaInicioFactura, lnDiaFactura, lnMesFactura, lnAnioFactura)
            ldFechaInicioFactura = New Date(lnAnioFactura, lnMesFactura, lnDiaFactura)

            lnAnioRecibo = 0
            lnMesRecibo = 0
            lnDiaRecibo = 0

            SUConversiones.ObtieneDiaMesAnio(psFechaInicioRecibo, lnDiaRecibo, lnMesRecibo, lnAnioRecibo)
            ldFechaInicioRecibo = New Date(lnAnioRecibo, lnMesRecibo, lnDiaRecibo)

            If (ldFechaInicioRecibo >= ldFechaInicioFactura) Then
                lsFechaInicio = lnAnioRecibo.ToString() & "-" & lnMesRecibo.ToString() & "-" & lnDiaRecibo.ToString()
            Else
                lsFechaInicio = lnAnioFactura.ToString() & "-" & lnMesFactura.ToString() & "-" & lnDiaFactura.ToString()
            End If

            lnAnioFactura = 0
            lnMesFactura = 0
            lnDiaFactura = 0

            lnAnioRecibo = 0
            lnMesRecibo = 0
            lnDiaRecibo = 0


            SUConversiones.ObtieneDiaMesAnio(psFechaFinFactura, lnDiaFactura, lnMesFactura, lnAnioFactura)
            ldFechaFinFactura = New Date(lnAnioFactura, lnMesFactura, lnDiaFactura)


            SUConversiones.ObtieneDiaMesAnio(psFechaFinRecibo, lnDiaRecibo, lnMesRecibo, lnAnioRecibo)
            ldFechaFinRecibo = New Date(lnAnioRecibo, lnMesRecibo, lnDiaRecibo)

            If (ldFechaFinRecibo >= ldFechaFinFactura) Then
                lsFechaFin = lnAnioRecibo.ToString() & "-" & lnMesRecibo.ToString() & "-" & lnDiaRecibo.ToString()
            Else
                lsFechaFin = lnAnioFactura.ToString() & "-" & lnMesFactura.ToString() & "-" & lnDiaFactura.ToString()
            End If

            Resultado(0) = lsFechaInicio
            Resultado(1) = lsFechaFin

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
        Return Resultado
    End Function

    Public Shared Function IngresaCuadreCajaCompleto(ByVal RegistroAgencia As Int16, ByVal objMaestroArqueo As SECuadreCajaEncabezado, _
                                                ByVal lstDetalleArqueo As List(Of SECuadreCajaDetalle), ByVal lstDetalleInformacionMonetariaArqueo As List(Of SEInformacionMonetaria)) As Integer
        Try
            Return ADArqueoCaja.IngresaCuadreCajaCompleta(RegistroAgencia, objMaestroArqueo, _
                                                         lstDetalleArqueo, lstDetalleInformacionMonetariaArqueo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function RecalcularCuadreCajaCompleto(ByVal RegistroAgencia As Int16, ByVal objMaestroArqueo As SECuadreCajaEncabezado, _
                                                ByVal lstDetalleArqueo As List(Of SECuadreCajaDetalle), ByVal lstDetalleInformacionMonetariaArqueo As List(Of SEInformacionMonetaria)) As Integer
        Try
            Return ADArqueoCaja.RecalcularCuadreCajaCompleto(RegistroAgencia, objMaestroArqueo, _
                                                         lstDetalleArqueo, lstDetalleInformacionMonetariaArqueo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneFacturasYRecibosARealizarCierre(ByVal pnIdUsuario As Integer, ByVal psFacturaInicial As String, _
                                                                  ByVal psFacturaFinal As String, ByVal psReciboInicial As String, _
                                                                  ByVal psReciboFinal As String, ByVal psFechaInicial As String, _
                                                                  ByVal psFechaFinal As String) As List(Of SECuadreCajaDetalle)
        Dim dtFacturasParaCierre As DataTable = Nothing
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim objDetalleCuadreCaja As SECuadreCajaDetalle
        Dim dtRecibosParaCierre As DataTable = Nothing
        Try
            objDetalleCuadreCaja = New SECuadreCajaDetalle()
            lstDetalleCuadreCaja = New List(Of SECuadreCajaDetalle)
            dtFacturasParaCierre = ADArqueoCaja.ObtieneFacturasParaCuadreCaja(pnIdUsuario, psFacturaInicial, psFacturaFinal, psFechaInicial, psFechaFinal)
            If Not dtFacturasParaCierre Is Nothing Then
                If dtFacturasParaCierre.Rows.Count > 0 Then
                    For i = 0 To dtFacturasParaCierre.Rows.Count - 1
                        objDetalleCuadreCaja.IdCuadre = 0
                        objDetalleCuadreCaja.IdMoneda = dtFacturasParaCierre.Rows(i).Item("IdMoneda")
                        objDetalleCuadreCaja.NumeroDocumento = dtFacturasParaCierre.Rows(i).Item("NumeroDocumento")
                        objDetalleCuadreCaja.MontoTotalDocumento = SUConversiones.ConvierteADouble(dtFacturasParaCierre.Rows(i).Item("MontoTotalDocumento"))
                        objDetalleCuadreCaja.IdEstadoDocumento = dtFacturasParaCierre.Rows(i).Item("IdEstadoDocumento")
                        objDetalleCuadreCaja.IdUsuarioDocumento = SUConversiones.ConvierteAInt(dtFacturasParaCierre.Rows(i).Item("IdUsuarioDocumento"))
                        objDetalleCuadreCaja.FechaDocumento = dtFacturasParaCierre.Rows(i).Item("FechaDocumento")
                        objDetalleCuadreCaja.FechaDocumentoNum = dtFacturasParaCierre.Rows(i).Item("FechaDocumentoNum")
                        objDetalleCuadreCaja.TipoDocumento = dtFacturasParaCierre.Rows(i).Item("IdTipoDocumento")
                        objDetalleCuadreCaja.DescripcionTipoDocumento = dtFacturasParaCierre.Rows(i).Item("DesTipoDocumento")
                        lstDetalleCuadreCaja.Add(New SECuadreCajaDetalle(objDetalleCuadreCaja))
                    Next
                End If
            End If
            dtRecibosParaCierre = ADArqueoCaja.ObtieneRecibosParaCuadreCaja(pnIdUsuario, psReciboInicial, psReciboFinal, psFechaInicial, psFechaFinal)
            If Not dtRecibosParaCierre Is Nothing Then
                If dtRecibosParaCierre.Rows.Count > 0 Then
                    For i = 0 To dtRecibosParaCierre.Rows.Count - 1
                        objDetalleCuadreCaja.IdCuadre = 0
                        objDetalleCuadreCaja.IdMoneda = dtRecibosParaCierre.Rows(i).Item("IdMoneda")
                        objDetalleCuadreCaja.NumeroDocumento = dtRecibosParaCierre.Rows(i).Item("NumeroDocumento")
                        objDetalleCuadreCaja.MontoTotalDocumento = SUConversiones.ConvierteADouble(dtRecibosParaCierre.Rows(i).Item("MontoTotalDocumento"))
                        objDetalleCuadreCaja.IdEstadoDocumento = dtRecibosParaCierre.Rows(i).Item("IdEstadoDocumento")
                        objDetalleCuadreCaja.IdUsuarioDocumento = SUConversiones.ConvierteAInt(dtRecibosParaCierre.Rows(i).Item("IdUsuarioDocumento"))
                        objDetalleCuadreCaja.FechaDocumento = dtRecibosParaCierre.Rows(i).Item("FechaDocumento")
                        objDetalleCuadreCaja.FechaDocumentoNum = dtRecibosParaCierre.Rows(i).Item("FechaDocumentoNum")
                        objDetalleCuadreCaja.TipoDocumento = dtRecibosParaCierre.Rows(i).Item("IdTipoDocumento")
                        objDetalleCuadreCaja.DescripcionTipoDocumento = dtRecibosParaCierre.Rows(i).Item("DesTipoDocumento")
                        lstDetalleCuadreCaja.Add(New SECuadreCajaDetalle(objDetalleCuadreCaja))
                    Next
                End If
            End If
            Return lstDetalleCuadreCaja
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneFacturasYRecibosARecalcular(ByVal pnIdUsuario As Integer, ByVal psFacturaInicial As String, _
                                                                  ByVal psFacturaFinal As String, ByVal psReciboInicial As String, _
                                                                  ByVal psReciboFinal As String, ByVal pnIdArqueo As Integer) As List(Of SECuadreCajaDetalle)
        Dim dtFacturasParaCierre As DataTable = Nothing
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim objDetalleCuadreCaja As SECuadreCajaDetalle
        Dim dtRecibosParaCierre As DataTable = Nothing
        Try
            objDetalleCuadreCaja = New SECuadreCajaDetalle()
            lstDetalleCuadreCaja = New List(Of SECuadreCajaDetalle)
            dtFacturasParaCierre = ADArqueoCaja.ObtieneFacturasParaRecalcularCuadreCaja(pnIdUsuario, psFacturaInicial, psFacturaFinal, pnIdArqueo)
            If Not dtFacturasParaCierre Is Nothing Then
                If dtFacturasParaCierre.Rows.Count > 0 Then
                    For i = 0 To dtFacturasParaCierre.Rows.Count - 1
                        objDetalleCuadreCaja.IdCuadre = 0
                        objDetalleCuadreCaja.IdMoneda = dtFacturasParaCierre.Rows(i).Item("IdMoneda")
                        objDetalleCuadreCaja.NumeroDocumento = dtFacturasParaCierre.Rows(i).Item("NumeroDocumento")
                        objDetalleCuadreCaja.MontoTotalDocumento = SUConversiones.ConvierteADouble(dtFacturasParaCierre.Rows(i).Item("MontoTotalDocumento"))
                        objDetalleCuadreCaja.IdEstadoDocumento = dtFacturasParaCierre.Rows(i).Item("IdEstadoDocumento")
                        objDetalleCuadreCaja.IdUsuarioDocumento = SUConversiones.ConvierteAInt(dtFacturasParaCierre.Rows(i).Item("IdUsuarioDocumento"))
                        objDetalleCuadreCaja.FechaDocumento = dtFacturasParaCierre.Rows(i).Item("FechaDocumento")
                        objDetalleCuadreCaja.FechaDocumentoNum = dtFacturasParaCierre.Rows(i).Item("FechaDocumentoNum")
                        objDetalleCuadreCaja.TipoDocumento = dtFacturasParaCierre.Rows(i).Item("IdTipoDocumento")
                        objDetalleCuadreCaja.DescripcionTipoDocumento = dtFacturasParaCierre.Rows(i).Item("DesTipoDocumento")
                        lstDetalleCuadreCaja.Add(New SECuadreCajaDetalle(objDetalleCuadreCaja))
                    Next
                End If
            End If
            dtRecibosParaCierre = ADArqueoCaja.ObtieneRecibosParaRecalcularCuadreCaja(pnIdUsuario, psReciboInicial, psReciboFinal, pnIdArqueo)
            If Not dtRecibosParaCierre Is Nothing Then
                If dtRecibosParaCierre.Rows.Count > 0 Then
                    For i = 0 To dtRecibosParaCierre.Rows.Count - 1
                        objDetalleCuadreCaja.IdCuadre = 0
                        objDetalleCuadreCaja.IdMoneda = dtRecibosParaCierre.Rows(i).Item("IdMoneda")
                        objDetalleCuadreCaja.NumeroDocumento = dtRecibosParaCierre.Rows(i).Item("NumeroDocumento")
                        objDetalleCuadreCaja.MontoTotalDocumento = SUConversiones.ConvierteADouble(dtRecibosParaCierre.Rows(i).Item("MontoTotalDocumento"))
                        objDetalleCuadreCaja.IdEstadoDocumento = dtRecibosParaCierre.Rows(i).Item("IdEstadoDocumento")
                        objDetalleCuadreCaja.IdUsuarioDocumento = SUConversiones.ConvierteAInt(dtRecibosParaCierre.Rows(i).Item("IdUsuarioDocumento"))
                        objDetalleCuadreCaja.FechaDocumento = dtRecibosParaCierre.Rows(i).Item("FechaDocumento")
                        objDetalleCuadreCaja.FechaDocumentoNum = dtRecibosParaCierre.Rows(i).Item("FechaDocumentoNum")
                        objDetalleCuadreCaja.TipoDocumento = dtRecibosParaCierre.Rows(i).Item("IdTipoDocumento")
                        objDetalleCuadreCaja.DescripcionTipoDocumento = dtRecibosParaCierre.Rows(i).Item("DesTipoDocumento")
                        lstDetalleCuadreCaja.Add(New SECuadreCajaDetalle(objDetalleCuadreCaja))
                    Next
                End If
            End If
            Return lstDetalleCuadreCaja
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneFacturasParaRealizarCierre(ByVal pnIdUsuario As Integer, ByVal psFacturaInicial As String, _
                                                         ByVal psFacturaFinal As String, ByVal psFechaInicial As String, _
                                                         ByVal psFechaFinal As String) As DataTable
        Try
            Return ADArqueoCaja.ObtieneFacturasParaCuadreCaja(pnIdUsuario, psFacturaInicial, psFacturaFinal, _
                                                              psFechaInicial, psFechaFinal)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneDatosEncabezadoArqueoDiario(ByVal pnIdUsuario As Integer) As DataTable
        Try
            Return ADArqueoCaja.ObtieneDatosEncabezadoArqueoDiarios(pnIdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneRecibosParaRealizarCierre(ByVal pnIdUsuario As Integer, ByVal psFacturaInicial As String, _
                                                         ByVal psFacturaFinal As String, ByVal psFechaInicial As String, _
                                                         ByVal psFechaFinal As String) As DataTable
        Try
            'Return ADArqueoCaja.IngresaCuadreCajaCompleta(RegistroAgencia, objMaestroArqueo, _
            '                                             lstDetalleArqueo, lstDetalleInformacionMonetariaArqueo)
            Return Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub CerrarCuadreCaja(ByVal RegistroAgencia As Int16, ByVal IdCuadre As Integer, ByVal IdUsuarioModifica As Integer)

        Try
            ADArqueoCaja.CerrarCuadreCaja(RegistroAgencia, IdCuadre, IdUsuarioModifica)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try

    End Sub

    Public Shared Sub EliminarArqueoDeImportacion(ByVal IdCuadre As Integer)

        Try
            ADArqueoCaja.EliminarArqueoDeImportacion(IdCuadre)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNArqueoCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try

    End Sub

End Class
