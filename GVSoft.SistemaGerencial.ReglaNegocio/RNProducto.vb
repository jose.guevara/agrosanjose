﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports System.Linq

Public Class RNProducto

    ''' <summary>
    ''' Graba producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub GrabaProducto(ByVal nRegistro As Integer, ByVal sCodigo As String, ByVal sDescripcion As String, _
                             ByVal nClase As Integer, ByVal nSubClase As Integer, ByVal nUnidad As Integer, _
                             ByVal nEmpaque As Integer, ByVal nPVPC As Double, ByVal nPVDC As Double, _
                             ByVal nPVPU As Double, ByVal nPVDU As Double, ByVal nImpuesto As Integer, _
                             ByVal nEstado As Integer, ByVal nVerificar As Integer, ByVal nProveedor As Integer, _
                             ByVal nMaximaDescuento As Double)
        Try
            ADProductos.GrabaProducto(nRegistro, sCodigo, sDescripcion, nClase, nSubClase, _
                                                                      nUnidad, nEmpaque, nPVPC, nPVDC, nPVPU, nPVDU, nImpuesto, _
                                                                      nEstado, nVerificar, nProveedor, nMaximaDescuento)
        Catch ex As Exception
            ADError.IntgresardetError("RNProductos", "GrabaProducto", "", ex.Message.ToString())
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Busca datos de producto por codigo de producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function BuscarProductoxCodigoProducto(ByVal sCodigoProducto As String) As DataTable
        Try
            Return ADProductos.ObtieneProductosxCodigo(sCodigoProducto)
        Catch ex As Exception
            ADError.IntgresardetError("RNProductos", "BuscarProductoxCodigoProducto", "", ex.Message.ToString())
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene el primer registro de producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function BuscarProductoPrimerRegistro() As DataTable
        Try
            Dim dtProductos As DataTable = New DataTable()
            dtProductos = ADProductos.ObtienePrimerProducto()
            Return dtProductos
        Catch ex As Exception
            ADError.IntgresardetError("RNProductos", "BuscarProductoPrimerRegistro", "", ex.Message.ToString())
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el primer registro de producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function BuscarProductoSiguiente(ByVal nRegistro As Integer) As DataTable
        Try
            Dim dtProductos As DataTable = New DataTable()
            dtProductos = ADProductos.ObtieneSiguienteProducto(nRegistro)
            Return dtProductos
        Catch ex As Exception
            ADError.IntgresardetError("RNProductos", "BuscarProductoSiguiente", "", ex.Message.ToString())
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el primer registro de producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function BuscarProductoAnterior(ByVal nRegistro As Integer) As DataTable
        Try
            Dim dtProductos As DataTable = New DataTable()
            dtProductos = ADProductos.ObtieneAnteriorProducto(nRegistro)
            Return dtProductos
        Catch ex As Exception
            ADError.IntgresardetError("RNProductos", "BuscarProductoAnterior", "", ex.Message.ToString())
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el primer registro de producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function BuscarUltimoProducto() As DataTable
        Try
            Dim dtProductos As DataTable = New DataTable()
            dtProductos = ADProductos.ObtieneUltimoProducto()
            Return dtProductos
        Catch ex As Exception
            ADError.IntgresardetError("RNProductos", "BuscarUltimoProducto", "", ex.Message.ToString())
            Throw ex
        End Try
    End Function
End Class
