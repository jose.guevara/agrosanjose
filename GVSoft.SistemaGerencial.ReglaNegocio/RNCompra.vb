﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNCompra
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNCompra"

    Public Shared Function ValidaComprasImportar(ByVal psFactura As String, ByVal pnIdProveedor As Integer) As Integer
        Try
            Return ADCompra.ValidaComprasImportar(psFactura, pnIdProveedor)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ValidaDatosImportado(ByVal pnIdAccion As Integer) As DataTable
        Try
            Return ADCompra.ValidaDatosImportados(pnIdAccion)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneComprasAimpotar() As DataTable
        Try
            Return ADCompra.ObtieneComprasAImportar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneComprasAimpotar(ByVal IdCompra As Long) As DataTable
        Try
            Return ADCompra.ObtieneComprasAImportar(IdCompra)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaRegistroEnArchivosImportarCompra(ByVal psNombreArchivo As String, ByVal psFechaIngreso As String, ByVal pnFechaIngresoNum As Integer, pnIdUsuario As Integer)
        Try
            ADCompra.IngresaRegistroEnArchivosImportarCompra(psNombreArchivo, psFechaIngreso, pnFechaIngresoNum, pnIdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Sub LimpiarArchivosImportarCompra()
        Try
            ADCompra.LimpiarArchivosImportarCompra()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleImportarCompra(ByVal registro As Long, ByVal numero As String, ByVal fechaingresoNum As Integer, ByVal fechaingreso As String,
                                                    ByVal vendcodigo As String, ByVal clicodigo As String, ByVal clinombre As String, ByVal subtotal As Double,
                                                    ByVal impuesto As Double, ByVal retencion As Double, ByVal total As Double, ByVal codusuario As String,
                                                    ByVal agencodigo As String, ByVal status As Integer, ByVal impreso As Integer, ByVal tipoCompra As Integer,
                                                    ByVal diascredito As Integer, ByVal numfechaanul As Integer, ByVal prodcodigo As String, ByVal proddescrip As String,
                                                    ByVal cantidad As Integer, ByVal precio As Double, ByVal valor As Double, ByVal iva As Double, ByVal EsBonificacion As Integer,
                                                    ByVal IdProveedor As String, ByVal registrodet As Long, ByVal registroerror As String)
        Try
            ADCompra.IngresaDetalleImportarCompra(registro, numero, fechaingresoNum, fechaingreso, vendcodigo, clicodigo, clinombre, subtotal,
                                                                       impuesto, retencion, total, codusuario, agencodigo, status, impreso, tipoCompra, diascredito,
                                                                       numfechaanul, prodcodigo, proddescrip, cantidad, precio, valor, iva, EsBonificacion,
                                                                       IdProveedor, registrodet, registroerror)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaTransaccionCompra(ByVal objMaestroCompra As SEEncabezadoCompra, ByVal lstDetalleCompra As List(Of SEDetalleCompra), ByVal intTipoCompra As Integer)
        Try
            ADCompra.IngresaTransaccionCompra(objMaestroCompra, lstDetalleCompra, intTipoCompra)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Function ObtieneArchivoComprasAimpotar(ByVal psNombreArchivo As String) As DataTable
        Try
            Return ADCompra.ObtieneArchivoComprasAImportar(psNombreArchivo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class

