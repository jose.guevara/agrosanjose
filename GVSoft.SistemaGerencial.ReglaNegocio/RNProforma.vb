﻿Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNProforma
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    Public Shared Function ObtieneNumeroProformaParametro(ByVal RegistroAgencia As Int16, ByVal Serie As String, ByVal TipoFact As Integer) As String
        Dim clsADRecibos As New ADRecibos()
        Dim NumeroProforma As Integer = 0
        Dim sNumeroProforma As String = String.Empty
        Try
            NumeroProforma = ADProforma.ObtieneNumeroProformaParametro(RegistroAgencia, Serie, TipoFact)
            'sNumeroProforma = Format(NumeroProforma + 1, "000000000000")
            sNumeroProforma = Format(NumeroProforma + 1, "0000000")

            Return sNumeroProforma
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNProforma"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function IngresaTransaccionProforma(ByVal RegistroAgencia As Int16, ByVal objMaestroProforma As SEMaestroProforma, _
                                                ByVal lstDetalleProforma As List(Of SEDetalleProforma), ByVal Serie As String) As Integer
        Try
            Return ADProforma.IngresaTransaccionProforma(RegistroAgencia, objMaestroProforma, lstDetalleProforma, Serie)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNProforma"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

End Class
