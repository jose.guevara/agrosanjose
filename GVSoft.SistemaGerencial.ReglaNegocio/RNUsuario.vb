﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.SqlClient

Public Class RNUsuario
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "SNUsuario"

    Public Shared Sub IngresaPermisoUsuario(ByVal IdUsuario As Integer, ByVal IdPermiso As Integer)
        Try
            ADUsuarios.IngresaPermisosUsuario(IdPermiso, IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Sub IngresaAgenciaxUsuario(ByVal IdUsuario As Integer, ByVal IdAgencia As Integer)
        Try
            ADUsuarios.IngresaAngenciaxUsuario(IdUsuario, IdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Sub IngresaReportesxUsuario(ByVal IdUsuario As Integer, ByVal IdReporte As Integer)
        Try
            ADUsuarios.IngresaReportesxUsuario(IdUsuario, IdReporte)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaUsuario(ByVal IdRegistroUsuario As Integer, ByVal CodigoUsuario As String, ByVal NombreUsuario As String, _
                               ByVal Apellidos As String, ByVal Direccion As String, ByVal Telefono As String, ByVal Cargo As String, _
                               ByVal Clave As String, ByVal Estado As Integer, ByVal Impresora As Integer, ByVal Verificar As Integer)
        Try
            ADUsuarios.IngresaUsuario(SUConversiones.ConvierteAInt(IdRegistroUsuario), CodigoUsuario, NombreUsuario, Apellidos, Direccion, Telefono, Cargo, _
                                               Clave, SUConversiones.ConvierteAInt(Estado), SUConversiones.ConvierteAInt(Impresora), SUConversiones.ConvierteAInt(Verificar))

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Sub EliminaPermisosxUsuario(ByVal IdRegistroUsuario As Integer)
        Try
            ADUsuarios.EliminaOpcionesxUsuarios(IdRegistroUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ObtieneInformacionUsuario(ByVal CodigoUsuario As String, ByVal ModoBusqueda As Integer) As IDataReader
        Try
            Return ADUsuarios.ObtieneInformacionxUsuario(CodigoUsuario, ModoBusqueda)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ValidaUsuario(ByVal CodigoUsuario As String, ByVal Clave As String) As IDataReader
        Try
            Return ADUsuarios.ValidaUsuario(CodigoUsuario, Clave)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneDescuentosxUsuarioParaSeguridad(ByVal pnIdUsuario As Integer) As DataTable
        Try
            Return ADUsuarios.ObtieneDescuentosxUsuarioParaSeguridad(pnIdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneDescuentosxUsuario(ByVal pnIdUsuario As Integer) As DataTable
        Try
            Return ADUsuarios.ObtieneDescuentosxUsuario(pnIdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Sub IngresaDescuentosxUsuarioParaSeguridad(ByVal pnIdUsuario As Integer, ByVal IdDescuento As Integer)
        Try
            ADUsuarios.IngresaDescuentosxUsuarioParaSeguridad(pnIdUsuario, IdDescuento)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Sub EliminaDescuentosxUsuarioParaSeguridad(ByVal pnIdUsuario As Integer)
        Try
            ADUsuarios.EliminaDescuentosxUsuarioParaSeguridad(pnIdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
