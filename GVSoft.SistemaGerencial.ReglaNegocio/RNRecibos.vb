﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNRecibos
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNRecibos"

    Public Shared Function ObtieneMontoSegunMonedaRecibo(ByVal pnMontoAConvertir As Decimal, ByVal pnMonedaFactura As Double,
                                                    ByVal pnTipoCambio As Double,
                                              ByVal pnMonedaRecibo As Integer, ByVal pnCantidadDecimal As Integer) As Decimal
        Dim lnMontoConvertido As Decimal = 0
        Dim lnMonedaRecibo As Integer = 0
        Dim lnTasaCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambio = pnTipoCambio
            lnMonedaRecibo = pnMonedaRecibo
            If lnMonedaRecibo <> pnMonedaFactura Then
                If lnMonedaRecibo = 1 Then
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir * lnTasaCambio), objParametro.IndicaTipoRedondeo, pnCantidadDecimal)
                Else
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, pnCantidadDecimal)
                End If
            Else
                lnMontoConvertido = pnMontoAConvertir
            End If
            Return lnMontoConvertido
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function
    Public Shared Function ObtieneMontoSegunMoneda(ByVal pnMontoAConvertir As Decimal, ByVal pnMonedaFactura As Double, ByVal pnTipoCambio As Double,
                                              ByVal pnMonedaRecibo As Integer, ByVal pnCantidadDecimal As Integer) As Decimal
        Dim lnMontoConvertido As Decimal = 0
        Dim lnMonedaRecibo As Integer = 0
        Dim lnTasaCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambio = pnTipoCambio
            lnMonedaRecibo = pnMonedaRecibo
            If lnMonedaRecibo <> pnMonedaFactura Then
                If pnMonedaFactura = 1 Then
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir * lnTasaCambio), objParametro.IndicaTipoRedondeo, pnCantidadDecimal)
                Else
                    'lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, pnCantidadDecimal)
                End If
            Else
                lnMontoConvertido = pnMontoAConvertir
            End If
            Return lnMontoConvertido
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function
    Public Shared Function ObtenerInformacionRecibo(ByVal NumeroRecibo As String) As IDataReader
        Dim drRecibos As IDataReader = Nothing
        Dim clsADRecibos As New ADRecibos()

        Try
            drRecibos = ADRecibos.ObtieneInformacionRecibo(NumeroRecibo)
            Return drRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return drRecibos
            'Return drRecibos
        End Try
    End Function
    Public Shared Function ObtenerDTInformacionRecibo(ByVal NumeroRecibo As String) As DataTable
        Dim dtRecibos As DataTable = Nothing
        Try
            dtRecibos = ADRecibos.ObtieneDTInformacionRecibo(NumeroRecibo)
            Return dtRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return dtRecibos
        End Try
    End Function
    Public Shared Function VerificaEstadoRecibo(ByVal NumeroRecibo As String) As DataTable
        Dim dtRecibos As DataTable = Nothing

        Try
            dtRecibos = ADRecibos.VerificaEstadoRecibo(NumeroRecibo)
            Return dtRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return dtRecibos
        End Try
    End Function
    Public Shared Function VerificaNumeroRecibo(ByVal psNumeroRecibo As String, ByVal pnIdUsuario As Integer, _
                                                ByVal psFechaInicio As String, ByVal psFechaFin As String) As Object()
        Dim Resultado As Object() = New Object(4) {}
        Dim ldFechaFactura As Date = Nothing
        Dim ldFechaInicio As Date = Nothing
        Dim ldFechaFin As Date = Nothing
        Dim dtVerificaRecibo As DataTable = Nothing
        Try
            Resultado(0) = 0
            Resultado(1) = String.Empty
            Resultado(2) = 0
            Resultado(3) = String.Empty
            ldFechaInicio = SUConversiones.ConvierteADate(psFechaInicio)
            ldFechaFin = SUConversiones.ConvierteADate(psFechaFin)
            dtVerificaRecibo = ADRecibos.VerificaNumeroRecibo(psNumeroRecibo)
            If Not (dtVerificaRecibo Is Nothing) Then
                If dtVerificaRecibo.Rows.Count > 0 Then
                    For i = 0 To dtVerificaRecibo.Rows.Count - 1
                        If SUConversiones.ConvierteAInt(dtVerificaRecibo.Rows(i).Item("IdUsuario")) <> pnIdUsuario Then
                            Resultado(0) = 1
                            Resultado(1) = "El usuario asignado al recibo no corresponde al usuario actual"
                        End If
                        If SUConversiones.ConvierteAInt(dtVerificaRecibo.Rows(i).Item("NumeroArqueo")) <> 0 Then
                            Resultado(0) = 1
                            Resultado(1) = "Este recibo ya fué tomada en cuenta en otro Arqueo, favor revisar"
                        End If
                        If SUConversiones.ConvierteAInt(dtVerificaRecibo.Rows(i).Item("estado")) <> 0 Then
                            Resultado(0) = 1
                            Resultado(1) = "El recibo no está activo y no puede ser tomada en cuenta para el Arqueo, favor revisar"
                        End If
                        ldFechaFactura = SUConversiones.ConvierteADate(dtVerificaRecibo.Rows(i).Item("FechaRecibo"))
                        If Not (ldFechaFactura >= ldFechaInicio) And (ldFechaFactura <= ldFechaFin) Then
                            Resultado(0) = 1
                            Resultado(1) = "Recibo no fué realizado en la fecha de Arqueo"
                        End If
                    Next
                Else
                    Resultado(0) = 1
                    Resultado(1) = "No existe recibo"
                End If
            Else
                Resultado(0) = 1
                Resultado(1) = "No existe recibo"
            End If
            Return Resultado
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Resultado
        End Try
    End Function

    Public Shared Function ObtieneInformacionReciboXClienteNumeroRecibos(ByVal IdCliente As Integer, ByVal NumeroRecibo As String) As IDataReader
        Dim drRecibos As IDataReader = Nothing
        Dim clsADRecibos As New ADRecibos()

        Try
            drRecibos = ADRecibos.ObtieneInformacionReciboXClienteNumeroRecibos(IdCliente, NumeroRecibo)
            Return drRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return drRecibos
        End Try
    End Function

    Public Shared Function ObtieneInformacionFacturasXClientes(ByVal IdCliente As Integer) As IDataReader
        Dim drRecibos As IDataReader = Nothing
        Dim clsADRecibos As New ADRecibos()

        Try
            drRecibos = ADRecibos.ObtieneInformacionFacturasXClientes(IdCliente)
            Return drRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return drRecibos
        End Try
    End Function

    Public Shared Function ValidaPermisoPagaFacturaRecientesConFacturasAntiguaConSaldo(ByVal psUsuario As Integer) As Integer
        Try
            Return ADRecibos.ValidaPermisoPagaFacturaRecientesConFacturasAntiguaConSaldo(psUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function



    Public Shared Function ConsultaDetalleReciboxNumeroRecibo(ByVal NumeroRecibo As String) As IDataReader
        Dim drRecibos As IDataReader = Nothing
        Dim clsADRecibos As New ADRecibos()

        Try
            drRecibos = ADRecibos.ConsultaDetalleReciboxNumeroRecibo(NumeroRecibo)
            Return drRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return drRecibos
        End Try
    End Function

    Public Shared Sub EliminaReciboAImportarxNumeroRecibo(ByVal psNumeroRecibo As String)
        Try
            ADRecibos.EliminaReciboAImportarxNumeroRecibo(psNumeroRecibo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene Saldo Pendiente del Cliente
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtenerSaldoPendiente(ByVal IdCliente As Integer) As Double
        Try
            Return ADRecibos.ObtenerSaldoPendiente(IdCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaTransaccionRecibos(ByVal objMaestroRecibos As SERecibosEncabezado, ByVal lstDetalleRecibo As List(Of SERecibosDetalle), ByVal Retencion As Double, ByVal Pendiente As Double)
        Try
            ADRecibos.IngresaTransaccionRecibos(objMaestroRecibos, lstDetalleRecibo, Retencion, Pendiente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaTransaccionNotaCredito(ByVal objMaestroRecibos As SERecibosEncabezado, ByVal lstDetalleRecibo As List(Of SERecibosDetalle))
        Try
            ADRecibos.IngresaTransaccionNotaCredito(objMaestroRecibos, lstDetalleRecibo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Function ObtieneFacturasPendientesxCliente(ByVal IdCliente As Integer) As IDataReader
        Dim drRecibos As IDataReader
        Dim clsADRecibos As New ADRecibos()

        Try
            drRecibos = ADRecibos.ObtieneFacturasPendientesxCliente(IdCliente)
            Return drRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Sub AnulaNotaCredito(ByVal IdCliente As Integer, ByVal Numero As String, ByVal FechaNum As Integer,
                                       ByVal MontoRec As Double, ByVal SaldoFavor As Double, ByVal TipoRecibo As Integer)
        Try
            ADRecibos.AnulaNotaCredito(IdCliente, Numero, FechaNum, MontoRec, SaldoFavor, TipoRecibo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
