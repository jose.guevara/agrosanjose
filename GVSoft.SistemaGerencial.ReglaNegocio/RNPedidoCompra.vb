﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports DevComponents.DotNetBar.Controls
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNPedidoCompra
    Private objAuditoria As SEAuditoria = New SEAuditoria()
    Private objError As SEError = New SEError()


    Public Sub New()
        If objAuditoria Is Nothing Then
            objAuditoria = New SEAuditoria
        End If
        objAuditoria.NombreClase = "RNPedidoCompra"
    End Sub

    Public Function ObtieneNumeroPedido(ByVal RegistroAgencia As Int16, ByVal Serie As String, ByVal TipoFact As Integer) As String
        Dim clPedidoCompra As New ADPedidoCompra()
        Dim NumeroPedido As Integer = 0
        Dim sNumeroPedido As String = String.Empty
        Try
            NumeroPedido = clPedidoCompra.ObtieneNumeroPedido(RegistroAgencia, Serie, TipoFact)
            'sNumeroProforma = Format(NumeroProforma + 1, "000000000000")
            sNumeroPedido = Format(NumeroPedido + 1, "0000000")

            Return sNumeroPedido
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Sub IngresaTransaccionFacturas(ByVal objMaestroPedidoCompra As SEMaestroPedidoCompra, ByVal lstDetallePeidoCompra As List(Of SEDetPedidoCompra), ByVal psSerie As String, ByVal pnTipoFactura As Integer)
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()

            clPedidoCompra.IngresaTransaccionPedidoCompra(objMaestroPedidoCompra, lstDetallePeidoCompra, psSerie, pnTipoFactura)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Function UbicarPendientes(ByVal pnIdAgencia As Integer, ByVal pnIdTipoFactura As Integer) As IDataReader
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()

            Return clPedidoCompra.UbicarPendientes(pnIdAgencia, pnIdTipoFactura)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function ObtieneSeriexAgencia(ByVal pnIdAgencia As Integer) As IDataReader
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()

            Return clPedidoCompra.ObtieneSeriexAgencia(pnIdAgencia)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Function ObtienePedidoCompraxNumero(ByVal psNumeroPedido As String) As IDataReader
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()

            Return clPedidoCompra.ObtienePedidoCompraxNumero(psNumeroPedido)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Function ObtienePedidoCompraxId(ByVal pnIdPedido As Integer) As IDataReader
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()

            Return clPedidoCompra.ObtienePedidoCompraxId(pnIdPedido)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Function ExtraerPedidoXNumeroPedidoCompraxNumeroYAgencia(ByVal pnIdAgencia As Integer, ByVal psNumero As String) As IDataReader
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()

            Return clPedidoCompra.ExtraerPedidoXNumeroPedidoCompraxNumeroYAgencia(pnIdAgencia, psNumero)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Sub CargarComboPedidosCompraPendiente(ByVal Combo As DevComponents.DotNetBar.Controls.ComboBoxEx, ByVal nIdUser As Integer)
        Dim drDatos As DataRow = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()
            dtDatos = clPedidoCompra.ObtienePedidosPendientesTodos()
            drDatos = dtDatos.NewRow()
            drDatos("IdPedido") = 0
            drDatos("DesPedido") = "<Seleccione Pedido>"
            dtDatos.Rows.Add(drDatos)

            Combo.DataSource = dtDatos
            Combo.DisplayMember = "DesPedido"
            Combo.ValueMember = "IdPedido"
            Combo.SelectedValue = 0
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub CargarComboPedidosCompraPendientexAgenciaYProveedor(ByVal Combo As ComboBoxEx, ByVal nIdUser As Integer, ByVal IdSucursal As Integer, ByVal IdProveedor As Integer)
        Dim drDatos As DataRow = Nothing
        Dim dtDatos As DataTable = Nothing
        Dim clPedidoCompra As ADPedidoCompra = Nothing
        Dim objError As SEError = Nothing
        Dim objAuditoria As SEAuditoria = Nothing
        Try
            clPedidoCompra = New ADPedidoCompra()
            dtDatos = clPedidoCompra.ObtienePedidosPendientesxAgenciaYProveedor(IdSucursal, IdProveedor)
            drDatos = dtDatos.NewRow()
            drDatos("IdPedido") = 0
            drDatos("DesPedido") = "<Seleccione Pedido>"
            dtDatos.Rows.Add(drDatos)

            Combo.DataSource = dtDatos
            Combo.DisplayMember = "DesPedido"
            Combo.ValueMember = "IdPedido"
            Combo.SelectedValue = 0
        Catch ex As Exception
            objError = New SEError()
            objAuditoria = New SEAuditoria()
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

End Class
