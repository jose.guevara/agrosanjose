﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
'Imports System.Windows.Forms
Public Class RNProveedor
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNProveedor"


    Public Shared Sub CargarComboProveedor(ByVal Combo As DevComponents.DotNetBar.Controls.ComboBoxEx, ByVal nIdUser As Integer)
        Dim drDatos As DataRow = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dtDatos = ADProveedor.ObtieneProveedoresTodos()
            drDatos = dtDatos.NewRow()
            drDatos("Registro") = 0
            drDatos("Nombre") = "<Seleccione Proveedor>"
            dtDatos.Rows.Add(drDatos)

            Combo.DataSource = dtDatos
            Combo.DisplayMember = "Nombre"
            Combo.ValueMember = "Registro"
            Combo.SelectedValue = 0
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ObtieneProveedorxCodigo(ByVal psCodigo As String) As IDataReader

        Try
            Return ADProveedor.ObtieneProveedoresxCodigo(psCodigo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneProveedoresProducto() As IDataReader
        Try
            Return ADProveedor.ObtieneProveedoresProducto()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
