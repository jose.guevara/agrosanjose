﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades

Public Class RNBusqueda
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "RNBusqueda"

    Public Shared Function ObtieneDatosListadoAyuda(ByVal pnTipoListadoAyuda As Integer, ByVal pnTipoBusqueda As Integer, ByVal psCodigo As String, ByVal psDescripcion As String, ByVal pnAgencia As Integer, ByVal pnIdUsuario As Integer) As DataTable
        Dim dtDatosListadoAyuda As DataTable = Nothing
        Dim dtTmpBusqueda As DataTable = Nothing
        Dim drRegistro As DataRow() = Nothing
        Try
            Select Case pnTipoListadoAyuda
                Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
                    dtDatosListadoAyuda = ADVendedores.ObtieneVendedoresTodos()
                Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
                    dtDatosListadoAyuda = ADCliente.ObtieneClientesTodos()
                Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
                    If pnTipoBusqueda = 2 Then
                        dtDatosListadoAyuda = ADProducto.ObtieneProductosxDescripcion(psDescripcion, pnAgencia, pnIdUsuario)
                    Else
                        dtDatosListadoAyuda = ADProducto.ObtieneProductosxCodigo(psCodigo, pnAgencia, pnIdUsuario)
                    End If
                Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
                    dtDatosListadoAyuda = ADCuentaContable.ObtieneCuentaContableTodos()
                Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
                    dtDatosListadoAyuda = ADCuentaBancaria.ObtieneCuentaBancaria()
                Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
                    dtDatosListadoAyuda = ADBeneficiario.ObtieneBeneficiarioTodos()
                Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
                    dtDatosListadoAyuda = ADProveedor.ObtieneProveedoresTodos()
            End Select

            If dtDatosListadoAyuda IsNot Nothing Then
                If dtDatosListadoAyuda.Rows.Count > 0 Then
                    dtTmpBusqueda = dtDatosListadoAyuda.Clone()
                End If
            End If
            Select Case pnTipoBusqueda
                Case 0
                    dtTmpBusqueda = dtDatosListadoAyuda
                Case 1

                    Select Case pnTipoListadoAyuda
                        Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                        Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                        Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
                            drRegistro = dtDatosListadoAyuda.Select("Codigo like '%" & psCodigo & "%' ")
                    End Select
                    'For Each reg As DataRow In drRegistro
                    '    dtTmpBusqueda.ImportRow(reg)
                    'Next
                Case 2
                    Select Case pnTipoListadoAyuda
                        Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
                            drRegistro = dtDatosListadoAyuda.Select("Nombre like '%" & psDescripcion & "%' ")
                        Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
                            drRegistro = dtDatosListadoAyuda.Select("Nombre like '%" & psDescripcion & "%' ")
                        Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
                            drRegistro = dtDatosListadoAyuda.Select("Descrip like '%" & psDescripcion & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
                            drRegistro = dtDatosListadoAyuda.Select("Descripcion like '%" & psDescripcion & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
                            drRegistro = dtDatosListadoAyuda.Select("Descripcion like '%" & psDescripcion & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
                            drRegistro = dtDatosListadoAyuda.Select("Nombre like '%" & psDescripcion & "%' ")
                        Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
                            drRegistro = dtDatosListadoAyuda.Select("Nombre like '%" & psDescripcion & "%' ")
                    End Select
            End Select
            If drRegistro IsNot Nothing Then
                For Each reg As DataRow In drRegistro
                    dtTmpBusqueda.ImportRow(reg)
                Next
            End If
            Return dtTmpBusqueda
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function FiltraDatosListadoAyuda(ByVal pnTipoListadoAyuda As Integer, ByVal pnTipoBusqueda As Integer, ByVal pdtDatosListadoAyuda As DataTable, ByVal psValor As String) As DataTable
        Dim dtTmpBusqueda As DataTable = Nothing
        Dim drRegistro As DataRow() = Nothing
        Try

            If pdtDatosListadoAyuda IsNot Nothing Then
                If pdtDatosListadoAyuda.Rows.Count > 0 Then
                    dtTmpBusqueda = pdtDatosListadoAyuda.Clone()
                End If
            End If
            If pdtDatosListadoAyuda IsNot Nothing Then


                Select Case pnTipoBusqueda
                    Case 0
                        dtTmpBusqueda = pdtDatosListadoAyuda
                    Case 1

                        Select Case pnTipoListadoAyuda
                            Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
                                drRegistro = pdtDatosListadoAyuda.Select("Codigo like '%" & psValor & "%' ")
                        End Select
                        'For Each reg As DataRow In drRegistro
                        '    dtTmpBusqueda.ImportRow(reg)
                        'Next
                    Case 2
                        Select Case pnTipoListadoAyuda
                            Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
                                drRegistro = pdtDatosListadoAyuda.Select("Nombre like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
                                drRegistro = pdtDatosListadoAyuda.Select("Nombre like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
                                drRegistro = pdtDatosListadoAyuda.Select("Descrip like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
                                drRegistro = pdtDatosListadoAyuda.Select("Descripcion like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
                                drRegistro = pdtDatosListadoAyuda.Select("Descripcion like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
                                drRegistro = pdtDatosListadoAyuda.Select("Nombre like '%" & psValor & "%' ")
                            Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
                                drRegistro = pdtDatosListadoAyuda.Select("Nombre like '%" & psValor & "%' ")
                        End Select
                End Select
                If drRegistro IsNot Nothing Then
                    For Each reg As DataRow In drRegistro
                        dtTmpBusqueda.ImportRow(reg)
                    Next
                End If
            End If
            Return dtTmpBusqueda
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
End Class
