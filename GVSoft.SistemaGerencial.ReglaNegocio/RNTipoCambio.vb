﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.Common
Public Class RNTipoCambio
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "RNTipoCambio"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneTipoCambioDelDia() As Decimal
        Try
            Return SUConversiones.ConvierteADecimal(ADTipoCambio.ObtieneTipoCambioDelDia())
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneTipoCambio(ByVal psFecha As String) As Double
        Try
            Return SUConversiones.ConvierteADouble(ADTipoCambio.ObtieneTipoCambio(psFecha))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneTipoCambioTodos() As DataTable
        Try
            Return ADTipoCambio.ObtieneTipoCambioTodas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub IngresaTipoCambio(ByVal psFecha As String, ByVal pnFecha As Integer, ByVal pnTasaCambio As Double)
        Try
            ADTipoCambio.IngresaTipoCambio(psFecha, pnFecha, pnTasaCambio)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

End Class
