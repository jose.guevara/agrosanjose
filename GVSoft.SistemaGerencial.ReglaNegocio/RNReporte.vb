﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNReporte
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "SNReporte"

    Public Shared Function ObtieneReportexCuenta(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psCuentaInicio As String, ByVal psCuentaFin As String) As DataTable
        Try
            'Return SDEmpresa.ObtieneNombreEmpresa()
            Return ADReportes.ObtieneReportexCuenta(psFechaInicio, psFechaFin, psCuentaInicio, psCuentaFin)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneReportexCuentaListado(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psCuentaInicio As String, ByVal psCuentaFin As String) As DataTable
        Try
            'Return SDEmpresa.ObtieneNombreEmpresa()
            Return ADReportes.ObtieneReportexCuentaListado(psFechaInicio, psFechaFin, psCuentaInicio, psCuentaFin)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
