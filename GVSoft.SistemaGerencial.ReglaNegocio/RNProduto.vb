﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNProduto
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNProduto"

    Public Shared Function ObtieneDetalleProducto(ByVal IdProducto As Integer, ByVal IdAgencia As Integer) As IDataReader

        Try
            Return ADProducto.ObtieneDetalleProducto(IdProducto, IdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneListadoPrecioVenta(ByVal pnIdTipoLita As Integer) As DataTable
        Dim dtDatos As DataTable = Nothing
        Try
            Select Case pnIdTipoLita
                Case 1
                    dtDatos = ADProducto.ObtieneListadoProductos()
                Case 2
                    dtDatos = ADProducto.ObtieneListadoPrecioCostoProducto()
                Case 3
                    dtDatos = ADProducto.ObtieneListadoPrecioVentaProducto()
                Case 4
                    dtDatos = ADProducto.ObtieneListadoPrecioVentaMayoristaProducto()
                Case 5
                    dtDatos = ADProducto.ObtieneListadoPrecioVentaMinoristaProducto()
                Case 6
                    dtDatos = ADProducto.ObtieneListadoPrecioYExistenciaActual()
            End Select
            Return dtDatos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function BuscarProductoxCodigo(ByVal psCodigo As String) As IDataReader

        Try
            Return ADProducto.BuscaProductoxCodigo(psCodigo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function UbicarProducto(ByVal psCodigoProducto As String) As IDataReader

        Try
            Return ADProducto.UbicarProducto(psCodigoProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function UbicarPrecio(ByVal pnIdProducto As Integer, ByVal pnIdIndex As Integer, ByVal pnIdTipoFactura As Integer, ByVal pnIdAgencia As Integer) As IDataReader

        Try
            Return ADProducto.UbicarPrecio(pnIdProducto, pnIdIndex, pnIdTipoFactura, pnIdAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ExtraerInventarioTraslado(ByVal IdAgencia As Integer, ByVal strNumeroFact As String, ByVal TipoFactura As Integer) As IDataReader

        Try
            Return ADProducto.ExtraerInventarioTraslado(IdAgencia, strNumeroFact, TipoFactura)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtienePrecionMinimo(ByVal CodigoProducto As String) As Double
        'Dim dtProducto As DataTable = Nothing
        Dim lnPrecioMinimoAFacturar As Decimal = 0
        Try
            lnPrecioMinimoAFacturar = ADProducto.ObtienePrecioMinimoxCodigo(CodigoProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return lnPrecioMinimoAFacturar
    End Function
    Public Shared Function ObtienePrecionMinimoxMoneda(ByVal CodigoProducto As String, ByVal pnIdMoneda As Integer) As Double
        'Dim dtProducto As DataTable = Nothing
        Dim lnPrecioMinimoAFacturar As Decimal = 0
        Try
            lnPrecioMinimoAFacturar = ADProducto.ObtienePrecioMinimoxCodigo(CodigoProducto, pnIdMoneda)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return lnPrecioMinimoAFacturar
    End Function

    Public Shared Function ObtienePrecioCostoxMoneda(ByVal CodigoProducto As String, ByVal pnIdMoneda As Integer) As Double
        'Dim dtProducto As DataTable = Nothing
        Dim lnPrecioCostoAFacturar As Decimal = 0
        Try
            lnPrecioCostoAFacturar = ADProducto.ObtienePrecioCostoxCodigoYMoneda(CodigoProducto, pnIdMoneda)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return lnPrecioCostoAFacturar
    End Function

    Public Shared Function ObtieneExistenciaActualProducto(ByVal pnIdAgencia As Integer, ByVal pnIdProducto As Integer) As Decimal
        'Dim dtProducto As DataTable = Nothing
        Dim lnPrecioMinimoAFacturar As Decimal = 0
        Try
            lnPrecioMinimoAFacturar = ADProducto.ObtieneExistenciaActualProducto(pnIdAgencia, pnIdProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return lnPrecioMinimoAFacturar
    End Function

    Public Shared Function UbicarMovimientoInventarioTraslado(ByVal IdAgencia As Integer, ByVal strNumeroFact As String) As IDataReader

        Try
            Return ADProducto.UbicarMovimientoInventarioTraslado(IdAgencia, strNumeroFact)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaTransaccionTraslado(ByVal objMovInventario As SEMovimientoInventarioEnc, ByVal lstMovInventarioDet As List(Of SEMovimientoInventarioDet), ByVal intTipoFactura As Integer)
        Try
            ADProducto.IngresaTransaccionTraslado(objMovInventario, lstMovInventarioDet, intTipoFactura)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ObtenerCamposImportarCorreccionIF(ByVal CodProducto As String, ByVal CodAgencia As String, ByVal Cantidad As Double) As IDataReader

        Try
            Return ADProducto.ObtenerCamposImportarCorreccionIF(CodProducto, CodAgencia, Cantidad)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaMovimientoCorreccionIF(ByVal IdMov As Integer, ByVal NumFecha As Integer, ByVal NumTiempo As Integer,
                                                    ByVal IdAgencia As Integer, ByVal IdProducto As Integer, ByVal strFech As String,
                                                    ByVal strDoc As String, ByVal strTipoMov As String, ByVal strMoneda As String,
                                                    ByVal Total As Double, ByVal Cantidad As Double, ByVal Costo As Double, ByVal Saldo As Double,
                                                    ByVal strDebe As String, ByVal strHaber As String, ByVal IdUser As Integer, ByVal CantConv As Double,
                                                    ByVal IdProdConv As Integer, ByVal IdAgenConv As Integer, ByVal Origen As Integer)

        Try
            ADProducto.IngresaMovimientoCorreccionIF(IdMov, NumFecha, NumTiempo, IdAgencia, IdProducto, strFech,
                                                         strDoc, strTipoMov, strMoneda, Total, Cantidad, Costo, Saldo, strDebe,
                                                         strHaber, IdUser, CantConv, IdProdConv, IdAgenConv, Origen)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub AcutalizaPrecioProductosCORConTasaDelDia()

        Try
            ADProducto.AcutalizaPrecioProductosCORConTasaDelDia()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ObtieneCantidadMinimaxIdSucursalYCodigoProducto(ByVal pnIdSucursal As Integer, ByVal psCodigoProducto As String) As DataTable
        Try
            Return ADProducto.ObtieneCantidadMinimaxIdSucursalYCodigoProducto(pnIdSucursal, psCodigoProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneCantidadMinimaxIdSucursal(ByVal pnIdSucursal As Integer) As DataTable
        Try
            Return ADProducto.ObtieneCantidadMinimaxIdSucursal(pnIdSucursal)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneProductosTodos() As DataTable
        Try
            Return ADProducto.ObtieneProductosTodos()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Sub CargarComboProducto(ByVal ComboProducto As DevComponents.DotNetBar.Controls.ComboBoxEx)
        Dim drProducto As DataRow = Nothing
        Dim dtProducto As DataTable = Nothing
        Try
            dtProducto = ADProducto.ObtieneProductosTodos()
            drProducto = dtProducto.NewRow()
            drProducto("Registro") = 0
            drProducto("Descripcion") = "<Seleccione Producto>"
            dtProducto.Rows.Add(drProducto)

            ComboProducto.DataSource = dtProducto
            ComboProducto.DisplayMember = "Descripcion"
            ComboProducto.ValueMember = "Registro"
            ComboProducto.SelectedValue = 0
        Catch ex As Exception
            'MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return
        End Try
    End Sub

    Public Shared Function ObtieneMinimoProductosxSucursalYProducto(ByVal pnIdSucursal As Integer, ByVal pnIdProducto As Integer) As DataTable
        Try
            Return ADProducto.ObtieneMinimoProductosxSucursalYProducto(pnIdSucursal, pnIdProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneMinimoProductosxSucursal(ByVal pnIdSucursal As Integer) As DataTable
        Try
            Return ADProducto.ObtieneMinimoProductosxSucursal(pnIdSucursal)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub GuardarMinimosProductos(ByVal pnIdSucursal As Integer, ByVal pnIdProducto As Integer,
                                                   ByVal pnCantidad As Integer, ByVal pnActivo As Integer)
        Try
            ADProducto.GuardarMinimosProductos(pnIdSucursal, pnIdProducto, pnCantidad, pnActivo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub CargaComboProductos(ByVal cmb As ComboBox)
        Dim ldrDatos As IDataReader = Nothing
        Dim col As AutoCompleteStringCollection = Nothing
        Try
            ldrDatos = Nothing
            'dtrAgro2K.Close()
            cmb.DropDownStyle = ComboBoxStyle.DropDown
            'ddlProveedor.AutoCompleteSource = AutoCompleteSource.ListItems
            cmb.AutoCompleteSource = AutoCompleteSource.CustomSource
            cmb.AutoCompleteCustomSource = col
            cmb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            ldrDatos = Nothing
            ldrDatos = ADProducto.ObtieneProductosTodosDR()
            col = Nothing
            col = New AutoCompleteStringCollection
            If ldrDatos IsNot Nothing Then
                While ldrDatos.Read
                    ' ddlSubClase.Items.Add(dtrAgro2K.GetValue(1))
                    cmb.Items.Add(ldrDatos.Item("Descripcion"))
                    'ComboBox8.Items.Add(dtrAgro2K.GetValue(0))
                    'col.Add(dtrAgro2K.GetValue(1))
                    col.Add(ldrDatos.Item("Descripcion"))
                End While
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
