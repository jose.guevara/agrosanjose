﻿
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports System.Data.SqlClient
Imports DevComponents.DotNetBar.Controls

Public Class RNCuentaContable
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "SNCuentaContable"

    Public Shared Function EliminarCuentaContable(ByVal psCuentaContable As String) As Integer
        Try
            Return ADCuentaContable.EliminaCuentaContable(psCuentaContable)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtienerCatalogoNivel1() As DataTable

        Try
            Return ADContabilidad.ObtienerCatalogoNivel1()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtienerCatalogoNivel2() As DataTable

        Try
            Return ADContabilidad.ObtienerCatalogoNivel2()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtienerCatalogoNivel3() As DataTable

        Try
            Return ADContabilidad.ObtienerCatalogoNivel3()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtienerCatalogoNivel4() As DataTable

        Try
            Return ADContabilidad.ObtienerCatalogoNivel4()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtienerViewCatalogoConta() As DataTable

        Try
            Return ADContabilidad.ObtienerViewCatalogoConta()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtienerAnexosSaldo() As DataTable
        Try
            Return ADContabilidad.ObtienerAnexosSaldo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtenerFormulasReportes(ByVal psCodigoReporte As String) As SqlDataReader
        Try
            Return ADContabilidad.ObtenerFormulasReportes(psCodigoReporte)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtenerSaldoInicialCuenta(ByVal psCodigoCuenta As String, ByVal psFechaInicial As String) As SqlDataReader
        Try
            Return ADContabilidad.ObtenerSaldoInicialCuenta(psCodigoCuenta, psFechaInicial)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtenerMovimientoCuenta(ByVal psCodigoCuenta As String, ByVal psFechaInicial As String, ByVal psFechaFinal As String) As SqlDataReader
        Try
            Return ADContabilidad.ObtenerMovimientoCuenta(psCodigoCuenta, psFechaInicial, psFechaFinal)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtenerDescripcionCuenta(ByVal psCodigoCuenta As String) As String
        Try
            Return ADContabilidad.ObtenerDescripcionCuenta(psCodigoCuenta)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function rptObtenerAsientoDiarioxFecha(ByVal pdFechaInicio As String, ByVal pdFechaFin As String) As DataTable
        Try
            Return ADContabilidad.rptObtenerAsientoDiarioxFecha(pdFechaInicio, pdFechaFin)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Sub GeneraAnexoEstadoResultado()
        Try
            ADContabilidad.GeneraAnexoEstadoResultado()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Sub GenerarBalanceGeneral()
        Try
            ADContabilidad.GenerarBalanceGeneral()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Function ObtenerCatalogoContable() As DataTable
        Try
            Return ADContabilidad.ObtenerCatalogoContable()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Sub CargarComboCatalogoContable(ByVal ComboCatalogo As ComboBoxEx)
        Dim drCatalogoContable As DataRow
        Dim dtCatalogoContable As DataTable
        Try
            dtCatalogoContable = ADContabilidad.ObtenerCatalogoContable()
            drCatalogoContable = dtCatalogoContable.NewRow()
            drCatalogoContable("cdgocuenta") = "-1"
            drCatalogoContable("desccuenta") = "<Seleccione Cuenta>"
            dtCatalogoContable.Rows.Add(drCatalogoContable)

            ComboCatalogo.DataSource = dtCatalogoContable
            ComboCatalogo.DisplayMember = "desccuenta"
            ComboCatalogo.ValueMember = "cdgocuenta"
            ComboCatalogo.SelectedValue = "-1"

        Catch ex As Exception
            'MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return
        End Try
    End Sub
    Public Shared Function ObtenerUltimoConsecutivoAsiento() As Integer
        Try
            Return ADContabilidad.ObtenerUltimoConsecutivoAsiento()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class



