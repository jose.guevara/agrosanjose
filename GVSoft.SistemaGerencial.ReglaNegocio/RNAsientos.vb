﻿Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.AccesoDatos

Public Class RNAsientos
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNAsientos"

    Public Shared Function ObtieneMasientosxNumSolic(ByVal psNumeroSolicitud As Integer) As DataTable
        Try
            Return ADAsientos.ObtieneMasientosxNumSolic(psNumeroSolicitud)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function RealizaBusquedaAsiento(ByVal psCuentaContable As String, ByVal psNumeroAsiento As String, ByVal psNumeroDocumento As String, ByVal pnAnio As Integer, ByVal pnMes As Integer) As DataTable
        Try
            Return ADAsientos.RealizaBusquedaAsiento(psCuentaContable, psNumeroAsiento, psNumeroDocumento, pnAnio, pnMes)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneDasientosxNumSolic(ByVal psNumeroSolicitud As Integer) As DataTable
        Try
            Return ADAsientos.ObtieneDasientosxNumSolic(psNumeroSolicitud)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneReporteAsientosContables(ByVal psNumeroAsiento As String, ByVal psAnioMes As String, ByVal pnNumeroMovimiento As Integer) As DataTable
        Try
            Return ADAsientos.ObtieneReporteAsientosContables(psNumeroAsiento, psAnioMes, pnNumeroMovimiento)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneReporteAsientosContablesEncabezado(ByVal psNumeroAsiento As String, ByVal psAnioMes As String, ByVal pnNumeroMovimiento As Integer) As DataTable
        Try
            Return ADAsientos.ObtieneReporteAsientosContablesEncabezado(psNumeroAsiento, psAnioMes, pnNumeroMovimiento)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
