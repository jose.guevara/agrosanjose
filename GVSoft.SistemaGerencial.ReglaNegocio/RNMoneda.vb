﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Windows.Forms
Public Class RNMoneda
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "RNMoneda"

    Public Shared Function ObtieneDescripcionMoneda(ByVal pnIdMoneda As Integer) As String
        Try
            Return ADMoneda.ObtieneDescripcionMoneda(pnIdMoneda)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneSignoMoneda(ByVal pnIdMoneda As Integer) As String
        Try
            Return ADMoneda.ObtieneSignoMoneda(pnIdMoneda)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    Public Shared Sub CargaComboMoneda(ByVal cmbMoneda As ComboBox)
        Try
            cmbMoneda.DataSource = RNMoneda.ObtieneMonedaTodas()
            cmbMoneda.DisplayMember = "Descripcion"
            cmbMoneda.ValueMember = "Registro"
            'cmbMoneda.DataBindings()

            'Return ADMoneda.ObtieneDescripcionMoneda(pnIdMoneda)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ObtieneMonedaTodas() As DataTable
        Try
            Return ADMoneda.ObtieneMonedaTodas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
