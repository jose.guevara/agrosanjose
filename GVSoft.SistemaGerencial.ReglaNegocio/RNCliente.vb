﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNCliente
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "RNCliente"

    Public Shared Function ObtenerDisponibleCliente(ByVal IdCliente As Integer) As DataTable

        Try
            Return ADCliente.ObtieneDisponiblexClientes(IdCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneDetalleClientexRegistro(ByVal IdCliente As Integer) As DataTable

        Try
            Return ADCliente.ObtieneDetalleClientexRegistro(IdCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneClientexCodigoCliente(ByVal psCodigoCliente As String) As DataTable

        Try
            Return ADCliente.ObtieneClientexCodigoCliente(psCodigoCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ValidaDisponibleCliente(ByVal IdCliente As Integer, ByVal pnMontoFactura As Double) As Object()
        Dim lbRespuesta As Boolean = True
        Dim ldtDatos As DataTable = Nothing
        Dim lnDisponible As Double = 0
        Dim objResultado As Object() = New Object(2) {}
        Try
            lbRespuesta = True
            ldtDatos = ObtenerDisponibleCliente(IdCliente)
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                lnDisponible = SUConversiones.ConvierteADouble(ldtDatos.Rows(0)("Dispoible"))
                If lnDisponible < 0 Then
                    lbRespuesta = False
                End If
                If lnDisponible - pnMontoFactura < 0 Then
                    lbRespuesta = False
                Else
                    lbRespuesta = True
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        objResultado(0) = lbRespuesta
        objResultado(1) = lnDisponible
        Return objResultado
    End Function
    Public Shared Function ObtenerCantidadFacturasPendientexCliente(ByVal IdCliente As Integer) As Integer

        Try
            Return ADCliente.ObtieneCantidadFacturasPendientes(IdCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub AcutalizaSaldoClienteCORConTasaDelDia()

        Try
            ADCliente.AcutalizaSaldoClienteCORConTasaDelDia()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Shared Function ObtieneClientexId(ByVal pnIdCliente As Integer) As IDataReader
        Try
            Return ADCliente.ObtieneClientexId(pnIdCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtieneClientexCodigo(ByVal CodigoCliente As String) As IDataReader
        Dim drCliente As IDataReader
        Try
            drCliente = ADCliente.ObtieneClientexCodigo(CodigoCliente)
            Return drCliente
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneDetalleClienteParaRecibo(ByVal pnIdCliente As Integer) As IDataReader
        Try
            Return ADCliente.ObtieneDetalleClienteParaRecibo(pnIdCliente)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function UbicarClientexIdCliente(ByVal IdCliente As Integer) As IDataReader
        Dim drCliente As IDataReader
        Dim clsADRecibos As New ADRecibos()

        Try
            drCliente = ADCliente.UbicarClientexIdCliente(IdCliente)
            Return drCliente
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
