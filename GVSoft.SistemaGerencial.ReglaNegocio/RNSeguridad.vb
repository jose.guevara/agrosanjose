﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNSeguridad
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "SNSeguridad"

    Public Shared Function ObtieneModulosxUsuario(ByVal IdUsuario As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneModulosxUsuario(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    Public Shared Function ObtieneModulosxUsuarioTodos(ByVal IdUsuario As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneModulosxUsuarioTodos(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneOpcionesxUsuarioYModuloYGrupo(ByVal IdUsuario As String, ByVal IdModulo As Integer, ByVal IdGrupo As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneOpcionesxUsuarioYModuloYGrupo(IdUsuario, IdModulo, IdGrupo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneOpcionesxUsuarioYModuloYGrupoTodos(ByVal IdUsuario As String, ByVal IdModulo As Integer, ByVal IdGrupo As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneOpcionesxUsuarioYModuloYGrupoTodos(IdUsuario, IdModulo, IdGrupo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneOpcionesxUsuarioYModulo(ByVal IdUsuario As String, ByVal IdModulo As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneOpcionesxUsuarioYModulo(IdUsuario, IdModulo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneOpcionesxUsuarioYModuloTodos(ByVal IdUsuario As String, ByVal IdModulo As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneOpcionesxUsuarioYModuloTodos(IdUsuario, IdModulo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneOpcionesxUsuario(ByVal IdUsuario As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneOpcionesxUsuario(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    Public Shared Function ObtieneGruposxUsuario(ByVal IdUsuario As String) As DataTable
        Try
            Return ADSeguridad.ObtieneGruposxUsuario(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneGruposxUsuarioYModulo(ByVal IdUsuario As String, ByVal IdModulo As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneGruposxUsuarioYModulo(IdUsuario, IdModulo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneGruposxUsuarioYModuloTodos(ByVal IdUsuario As String, ByVal IdModulo As Integer) As DataTable
        Try
            Return ADSeguridad.ObtieneGruposxUsuarioYModuloTodos(IdUsuario, IdModulo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaUsuario(ByVal IdUsuario As String, ByVal sPassword As String) As Object()
        Dim Resultado As Object() = New Object(4) {}
        Dim dtDatosSeguridad As DataTable
        Dim nValidaUsuario As Integer = 0

        Try
            Resultado(0) = 0
            Resultado(1) = "Usuario autenticado corretamente"
            dtDatosSeguridad = ADSeguridad.ObtieneDatosUsuarioxIUsuarioYPassword(IdUsuario, sPassword)
            If Not (dtDatosSeguridad Is Nothing) Then
                If dtDatosSeguridad.Rows.Count > 0 Then
                    If dtDatosSeguridad.Rows(0).Item("Activo") = 0 Then
                        Resultado(0) = 2
                        Resultado(1) = "Usuario inactivo"
                        Resultado(2) = ""
                        Resultado(3) = ""
                        Resultado(4) = ""
                    Else
                        Resultado(2) = SUConversiones.ConvierteAInt(dtDatosSeguridad.Rows(0).Item("IdUsuario").ToString().Trim())
                        Resultado(3) = dtDatosSeguridad.Rows(0).Item("Usuario").ToString().Trim()
                        Resultado(4) = dtDatosSeguridad.Rows(0).Item("PriNombre").ToString().Trim() & " " & dtDatosSeguridad.Rows(0).Item("SegNombre").ToString().Trim() & " " & dtDatosSeguridad.Rows(0).Item("PriApellido").ToString().Trim() & " " & dtDatosSeguridad.Rows(0).Item("SegApellido").ToString().Trim()
                    End If
                Else
                    Resultado(0) = 3
                    Resultado(1) = "Usuario o clave invalida"
                End If
            Else
                Resultado(0) = 3
                Resultado(1) = "Usuario o clave invalida"
            End If
            Return Resultado
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Resultado(0) = 3
            Resultado(1) = "Error - " & ex.Message.ToString()
            Return Resultado
        End Try
    End Function
End Class
