﻿Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNCuentaContableProducto
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared sNombreClase As String = "RNCuentaContableProducto"

    Public Shared Function ObtieneCuentaContableProductoxLlave(ByVal pnIdAgencia As Integer, ByVal pnIdProducto As Integer) As DataTable
        Try
            Return ADCuentaContableProducto.ObtieneCuentaContableProductoxLlave(pnIdAgencia, pnIdProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneCuentaContableProductoTodos() As DataTable
        Try
            Return ADCuentaContableProducto.ObtieneCuentaContableProductoTodos()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Sub IngresaCuentaContableProducto(ByVal pnIdAgencia As Integer, ByVal pnIdProducto As Integer, ByVal psCuentaContable As String)
        Try
            ADCuentaContableProducto.IngresaCuentaContableProducto(pnIdAgencia, pnIdProducto, psCuentaContable)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub ActualizaCuentaContableProducto(ByVal pnIdAgencia As Integer, ByVal pnIdProducto As Integer, ByVal psCuentaContable As String, ByVal pnActivo As Integer)
        Try
            ADCuentaContableProducto.ActualizaCuentaContableProducto(pnIdAgencia, pnIdProducto, psCuentaContable, pnActivo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub GuardaCuentaContableProducto(ByVal pnIdAgencia As Integer, ByVal pnIdProducto As Integer, ByVal psCuentaContable As String, ByVal pnActivo As Integer)
        Try
            ADCuentaContableProducto.GuardaCuentaContableProducto(pnIdAgencia, pnIdProducto, psCuentaContable, pnActivo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
