﻿Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class RNRecibosProveedor

    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    Public Shared Function IngresaReciboProveedor(ByVal objReciboProveedor As SEReciboProveedor, ByVal lstDetalleReciboProveedor As List(Of SEDetalleReciboProveedor)) As Integer
        Try
            Return ADReciboProveedor.IngresaReciboProveedor(objReciboProveedor, lstDetalleReciboProveedor)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNRecibosProveedor"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ObtenerInformacionRecibo(ByVal IdProveedor As Integer, ByVal NumeroRecibo As String) As IDataReader
        Dim drRecibos As IDataReader = Nothing
        Dim clsADRecibos As New ADRecibos()

        Try
            drRecibos = ADReciboProveedor.ObtieneInformacionRecibo(IdProveedor, NumeroRecibo)
            Return drRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNRecibosProveedor"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return drRecibos
            'Return drRecibos
        End Try
    End Function

    Public Shared Function ObtenerInformacionReciboDT(ByVal IdProveedor As Integer, ByVal NumeroRecibo As String) As DataTable
        Dim dtRecibos As DataTable = Nothing
        Try
            dtRecibos = ADReciboProveedor.ObtieneInformacionReciboDT(IdProveedor, NumeroRecibo)
            Return dtRecibos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNRecibosProveedor"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return dtRecibos
        End Try
    End Function
End Class
