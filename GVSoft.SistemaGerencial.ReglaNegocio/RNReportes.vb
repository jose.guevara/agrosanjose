﻿Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Windows.Forms

Public Class RNReportes
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "RNReportes"


    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneCatReportesxModuloyUsuario(ByVal IdUsuario As Integer, ByVal IdModulo As Integer) As DataTable
        Try
            Return ADReporetes.ObtieneCatReportesxModuloyUsuario(IdUsuario, IdModulo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNReportes"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneModulosReportesxUsuario(ByVal IdUsuario As Integer) As DataTable
        Try
            Return ADReportes.ObtieneModulosReportexUsuario(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNReportes"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneReportesxModulosyUsuario(ByVal IdModulo As Integer, ByVal IdUsuario As Integer) As DataTable
        Try
            Return ADReportes.ObtieneReportexModuloyUsuario(IdModulo, IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNReportes"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Sub EliminaReportesxUsuario(ByVal IdUsuario As Integer)
        Try
            ADReportes.EliminaReportesxUsuarios(IdUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNReportes"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return
        End Try
    End Sub
    Public Shared Sub CargarComboReportes(ByVal ComboReporte As DevComponents.DotNetBar.Controls.ComboBoxEx, ByVal nIdUser As Integer, ByVal nIdMod As Integer)
        Dim drReportes As DataRow
        Dim dtReportes As DataTable
        Try
            dtReportes = RNReportes.ObtieneCatReportesxModuloyUsuario(nIdUser, nIdMod)
            drReportes = dtReportes.NewRow()
            drReportes("OpcionReporte") = -1
            drReportes("Descripcion") = "<Seleccione Reporte>"
            dtReportes.Rows.Add(drReportes)

            ComboReporte.DataSource = dtReportes
            ComboReporte.DisplayMember = "Descripcion"
            ComboReporte.ValueMember = "OpcionReporte"
            ComboReporte.SelectedValue = -1
        Catch ex As Exception
            'MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ReporteFacturasConsecutivas(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psSucursal As String,
                                                       ByVal psTipoFactura As String, ByVal psVendedor As String) As DataTable
        Try
            Return ADReportes.ReporteFacturasConsecutivas(psFechaInicio, psFechaFin, psSucursal, psTipoFactura, psVendedor)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNReportes"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
End Class
