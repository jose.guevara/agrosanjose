﻿Imports GVSoft.SistemaGerencial.AccesoDatos

    ''' <summary>
    ''' Obtiene los cheques grababados y activos en la tabla
    ''' </summary>
    ''' <remarks></remarks>
Public Class RNCheques

    Public Function ObtenerCheques() As DataTable
        Dim dtCheques As DataTable
        Dim ClaseADCheques As New ADCheques

        dtCheques = ClaseADCheques.ObtieneCheques()
        Return dtCheques

    End Function

    ''' <summary>
    ''' Obtiene el detalle de un cheque por numero de cheque 
    ''' </summary>
    ''' <param name="psNumeroCheque">Numero de cheque</param>
    ''' <retorna>un datatable</retorna>

    Public Function ObtenerCheques(ByVal psNumeroCheque As String) As DataTable
        Dim dtCheques As DataTable
        Dim ClaseADCheques As New ADCheques

        dtCheques = ClaseADCheques.ObtieneCheques(psNumeroCheque)
        Return dtCheques

    End Function

    ''' <summary>
    ''' inserta un nuevo cheque pasandole todos los parametros de que corresponeden a la tabla
    ''' </summary>
    ''' <param name="pnCtabanregistro">Numero de registro en banco</param>
    ''' <retorna>un datatable</retorna>

    Public Function InsertaCheque(ByVal pnCtabanregistro As Integer, ByVal pnBenefregistro As Integer, ByVal psBeneficiario As String, _
                                  ByVal psDescripcion As String, ByVal pdFechacheque As DateTime, ByVal psDocumento As String, _
                                  ByVal pdMonto As Double, ByVal pnTipomovim As Integer) As Object()

        Dim ClaseADCheques As New ADCheques

        Return ClaseADCheques.InsertaCheque(pnCtabanregistro, pnBenefregistro, psBeneficiario, psDescripcion, pdFechacheque, _
                                            psDocumento, pdMonto, pnTipomovim)
    End Function

    Public Function ModifCtasBancarias(ByVal registrocta As Integer, ByRef blnactual As Double) As Object()
        Dim ClaseADCheques As New ADCheques
        Return ClaseADCheques.ModifCtasBancarias(registrocta, blnactual)
    End Function


End Class
