﻿Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.AccesoDatos
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class RNFacturas
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "GVSoft.SistemaGerencial.AccesoDatos.RNFacturas"

    Public Shared Function VerificaNumeroFactura(ByVal psNumeroFactura As String, ByVal pnIdUsuario As Integer, _
                                                ByVal psFechaInicio As String, ByVal psFechaFin As String) As Object()
        Dim Resultado As Object() = New Object(4) {}
        Dim dtVerificaFactura As DataTable = Nothing
        Dim ldFechaFactura As Date = Nothing
        Dim ldFechaInicio As Date = Nothing
        Dim ldFechaFin As Date = Nothing
        Try
            Resultado(0) = 0
            Resultado(1) = String.Empty
            Resultado(2) = 0
            Resultado(3) = String.Empty
            ldFechaInicio = SUConversiones.ConvierteADate(psFechaInicio)
            ldFechaFin = SUConversiones.ConvierteADate(psFechaFin)
            dtVerificaFactura = ADFacturas.VerificaNumeroFactura(psNumeroFactura)
            If Not (dtVerificaFactura Is Nothing) Then
                If dtVerificaFactura.Rows.Count > 0 Then
                    For i = 0 To dtVerificaFactura.Rows.Count - 1
                        If SUConversiones.ConvierteAInt(dtVerificaFactura.Rows(i).Item("IdUsuario")) <> pnIdUsuario Then
                            Resultado(0) = 1
                            Resultado(1) = "El usuario asignado a la factura no corresponde al usuario actual"
                        End If
                        If SUConversiones.ConvierteAInt(dtVerificaFactura.Rows(i).Item("NumeroArqueo")) <> 0 Then
                            Resultado(0) = 1
                            Resultado(1) = "Esta factura ya fué tomada en cuenta en otro Arqueo, favor revisar"
                        End If
                        If SUConversiones.ConvierteAInt(dtVerificaFactura.Rows(i).Item("EstadoDocumento")) <> 0 Then
                            Resultado(0) = 1
                            Resultado(1) = "La factura no está activa y no puede ser tomada en cuenta para el Arqueo, favor revisar"
                        End If
                        ldFechaFactura = SUConversiones.ConvierteADate(dtVerificaFactura.Rows(i).Item("FechaDocumento"))
                        If Not (ldFechaFactura >= ldFechaInicio) And (ldFechaFactura <= ldFechaFin) Then
                            Resultado(0) = 1
                            Resultado(1) = "Factura no fué realizada en la fecha de cuadre"
                        End If
                    Next
                Else
                    Resultado(0) = 1
                    Resultado(1) = "No existe factura"
                End If
            Else
                Resultado(0) = 1
                Resultado(1) = "No existe factura"
            End If

            Return Resultado
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Resultado(2) = 1
            Resultado(3) = ex.Message()
            Throw ex
        End Try
    End Function
    Public Shared Function ValidaPrecioVenta(ByVal pnCantidadFacturada As Decimal, ByVal pnCantidadBonificada As Decimal, ByVal pnPrecioVenta As Decimal, ByVal pnPrecioVentaMinimo As Decimal) As Object()
        Dim Resultado As Object() = New Object(3) {}
        Dim lnTotalCantidad As Decimal = 0
        Dim lnTotalValorFacturado As Decimal = 0
        Dim lnPrecioPromedio As Decimal = 0
        Try
            If pnCantidadBonificada > 0 Then
                lnTotalCantidad = pnCantidadFacturada
                lnTotalValorFacturado = Math.Round((lnTotalCantidad * pnPrecioVenta), 2)

                lnPrecioPromedio = Math.Round((lnTotalValorFacturado / (pnCantidadFacturada + pnCantidadBonificada)), 2)
                Resultado(0) = 0
                Resultado(1) = "Precio de venta valido"
                Resultado(2) = lnPrecioPromedio
                If lnPrecioPromedio < pnPrecioVentaMinimo Then
                    Resultado(0) = 1
                    Resultado(1) = "Precio de venta por abajo del margen"
                    Resultado(2) = lnPrecioPromedio
                End If
            Else
                Resultado(0) = 0
                Resultado(1) = "Precio de venta valido"
                Resultado(2) = pnPrecioVenta
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Resultado(0) = 1
            Resultado(1) = ex.Message()
            Resultado(2) = 0
        End Try
        Return Resultado
    End Function
    Public Shared Function ValidaPrecioVentaI(ByVal pnCantidadFacturada As Decimal, ByVal pnCantidadBonificada As Decimal, ByVal pnPrecioVenta As Decimal, ByVal pnPrecioVentaMinimo As Decimal) As Object()
        Dim Resultado As Object() = New Object(3) {}
        Dim lnTotalCantidad As Decimal = 0
        Dim lnTotalValorFacturado As Decimal = 0
        Dim lnPrecioPromedio As Decimal = 0
        Try
            If pnCantidadBonificada > 0 Then
                lnTotalCantidad = pnCantidadFacturada + pnCantidadBonificada
                lnTotalValorFacturado = Math.Round((lnTotalCantidad * pnPrecioVenta), 2)

                lnPrecioPromedio = Math.Round((lnTotalValorFacturado / pnCantidadFacturada), 2)
                Resultado(0) = 0
                Resultado(1) = "Precio de venta valido"
                Resultado(2) = lnPrecioPromedio
                If lnPrecioPromedio < pnPrecioVentaMinimo Then
                    Resultado(0) = 1
                    Resultado(1) = "Precio de venta por abajo del margen"
                    Resultado(2) = lnPrecioPromedio
                End If
            Else
                Resultado(0) = 0
                Resultado(1) = "Precio de venta valido"
                Resultado(2) = pnPrecioVenta
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Resultado(0) = 1
            Resultado(1) = ex.Message()
            Resultado(2) = 0
        End Try
        Return Resultado
    End Function

    Public Shared Function ValidaPermisoVerCosto(ByVal psUsuario As Integer) As Integer
        Try
            Return ADFacturas.ValidaPermisoVercosto(psUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ValidaPermisoPermiteCambiarFechaEnOperaciones(ByVal psUsuario As Integer) As Integer
        Try
            Return ADFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(psUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub EliminarFacturaAImportar(ByVal psNumeroFactura As String)
        Try
            ADFacturas.EliminarFacturaAImportar(psNumeroFactura)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ValidaPermisoFacturarBajoCosto(ByVal psUsuario As Integer) As Integer
        Try
            Return ADFacturas.ValidaPermisoFacturarBajoCosto(psUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ValidaPermisoFacturarClienteSinDisponible(ByVal psUsuario As Integer) As Integer
        Try
            Return ADFacturas.ValidaPermisoFacturarClienteSinDisponible(psUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ValidaPermisoFacturarClienteConFacturasPendiente(ByVal psUsuario As Integer) As Integer
        Try
            Return ADFacturas.ValidaPermisoFacturarClienteConFacturasPendiente(psUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function UbicarPendientes(ByVal RegistroAgencia As Integer, ByVal TipoFact As Integer) As IDataReader

        Try
            Return ADFacturas.UbicarPendientes(RegistroAgencia, TipoFact)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ActualizarConsecutivoFacturaRapida(ByVal pnRegistroAgencia As Integer, ByVal psSerie As String) As Integer
        Try
            Return ADFacturas.ActualizaConsecutivoFacturaRapida(pnRegistroAgencia, psSerie)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneConsecutivoFacturaRapida(ByVal pnRegistroAgencia As Integer, ByVal psSerie As String) As Integer
        Try
            Return ADFacturas.ObtieneConsecutivoFacturaRapida(pnRegistroAgencia, psSerie)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ExtraerFactura(ByVal RegistroAgencia As Integer, ByVal strNumeroFact As String, ByVal TipoFact As Integer) As IDataReader

        Try
            Return ADFacturas.ExtraerFactura(RegistroAgencia, strNumeroFact, TipoFact)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ExtraerFacturaDT(ByVal RegistroAgencia As Integer, ByVal strNumeroFact As String, ByVal TipoFact As Integer) As DataTable

        Try
            Return ADFacturas.ExtraerFacturaDT(RegistroAgencia, strNumeroFact, TipoFact)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function UbicarFacturas(ByVal RegistroAgencia As Integer, ByVal strNumeroFact As String) As IDataReader

        Try
            Return ADFacturas.UbicarFacturas(RegistroAgencia, strNumeroFact)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaTransaccionFacturas(ByVal objMaestroFacturas As SEEncabezadoFacturas, ByVal lstDetalleFactura As List(Of SEDetalleFacturas), ByVal intTipoFactura As Integer)
        Try
            ADFacturas.IngresaTransaccionFacturas(objMaestroFacturas, lstDetalleFactura, intTipoFactura)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

End Class

