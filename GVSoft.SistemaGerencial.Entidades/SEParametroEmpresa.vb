﻿Public Class SEParametroEmpresa
    Private _IdEmpresa As Integer
    Public Property IdEmpresa() As Integer
        Get
            Return (_IdEmpresa)
        End Get
        Set(ByVal value As Integer)
            _IdEmpresa = value
        End Set
    End Property
    Private _PermisoVerPrecionCosto As Integer
    Public Property PermisoVerPrecionCosto() As Integer
        Get
            Return (_PermisoVerPrecionCosto)
        End Get
        Set(ByVal value As Integer)
            _PermisoVerPrecionCosto = value
        End Set
    End Property
    Private _IndicaAnulaFacturaMesAnterior As Integer
    Public Property IndicaAnulaFacturaMesAnterior() As Integer
        Get
            Return (_IndicaAnulaFacturaMesAnterior)
        End Get
        Set(ByVal value As Integer)
            _IndicaAnulaFacturaMesAnterior = value
        End Set
    End Property
    Private _IndicaAnulaReciboMesAnterior As Integer
    Public Property IndicaAnulaReciboMesAnterior() As Integer
        Get
            Return (_IndicaAnulaReciboMesAnterior)
        End Get
        Set(ByVal value As Integer)
            _IndicaAnulaReciboMesAnterior = value
        End Set
    End Property
    Private _CantidadItemFactura As Integer
    Public Property CantidadItemFactura() As Integer
        Get
            Return (_CantidadItemFactura)
        End Get
        Set(ByVal value As Integer)
            _CantidadItemFactura = value
        End Set
    End Property
    Private _CantidadItemFacturaRapida As Integer
    Public Property CantidadItemFacturaRapida() As Integer
        Get
            Return (_CantidadItemFacturaRapida)
        End Get
        Set(ByVal value As Integer)
            _CantidadItemFacturaRapida = value
        End Set
    End Property
    Private _CantidadItemRecibos As Integer
    Public Property CantidadItemRecibos() As Integer
        Get
            Return (_CantidadItemRecibos)
        End Get
        Set(ByVal value As Integer)
            _CantidadItemRecibos = value
        End Set
    End Property
    Private _PermiteFacturarBajoCosto As String
    Public Property PermiteFacturarBajoCosto() As String
        Get
            Return (_PermiteFacturarBajoCosto)
        End Get
        Set(ByVal value As String)
            _PermiteFacturarBajoCosto = value
        End Set
    End Property
    Private _PermitePagarFacturasRecientesConAntiguaPendiente As String
    Public Property PermitePagarFacturasRecientesConAntiguaPendiente() As String
        Get
            Return (_PermitePagarFacturasRecientesConAntiguaPendiente)
        End Get
        Set(ByVal value As String)
            _PermitePagarFacturasRecientesConAntiguaPendiente = value
        End Set
    End Property

    Private _PermiteFacturarClienteSinDisponible As String
    Public Property PermiteFacturarClienteSinDisponible() As String
        Get
            Return (_PermiteFacturarClienteSinDisponible)
        End Get
        Set(ByVal value As String)
            _PermiteFacturarClienteSinDisponible = value
        End Set
    End Property
    Private _PermiteFacturarClienteConFacturasPendiente As String
    Public Property PermiteFacturarClienteConFacturasPendiente() As String
        Get
            Return (_PermiteFacturarClienteConFacturasPendiente)
        End Get
        Set(ByVal value As String)
            _PermiteFacturarClienteConFacturasPendiente = value
        End Set
    End Property
    Private _OpcionFacturarBajoCosto As Integer
    Public Property OpcionFacturarBajoCosto() As Integer
        Get
            Return (_OpcionFacturarBajoCosto)
        End Get
        Set(ByVal value As Integer)
            _OpcionFacturarBajoCosto = value
        End Set
    End Property

    Private _OpcionFacturarClienteSinDisponible As Integer
    Public Property OpcionFacturarClienteSinDisponible() As Integer
        Get
            Return (_OpcionFacturarClienteSinDisponible)
        End Get
        Set(ByVal value As Integer)
            _OpcionFacturarClienteSinDisponible = value
        End Set
    End Property

    Private _OpcionFacturarClienteConFacturasPendiente As Integer
    Public Property OpcionFacturarClienteConFacturasPendiente() As Integer
        Get
            Return (_OpcionFacturarClienteConFacturasPendiente)
        End Get
        Set(ByVal value As Integer)
            _OpcionFacturarClienteConFacturasPendiente = value
        End Set
    End Property
    Private _IndicaAnulaCompraMesAnterior As Integer
    Public Property IndicaAnulaCompraMesAnterior() As Integer
        Get
            Return (_IndicaAnulaCompraMesAnterior)
        End Get
        Set(ByVal value As Integer)
            _IndicaAnulaCompraMesAnterior = value
        End Set
    End Property
    Private _MonedaFacturaCredito As Integer
    Public Property MonedaFacturaCredito() As Integer
        Get
            Return (_MonedaFacturaCredito)
        End Get
        Set(ByVal value As Integer)
            _MonedaFacturaCredito = value
        End Set
    End Property
    Private _MonedaFacturaContado As Integer
    Public Property MonedaFacturaContado() As Integer
        Get
            Return (_MonedaFacturaContado)
        End Get
        Set(ByVal value As Integer)
            _MonedaFacturaContado = value
        End Set
    End Property
    Private _IndicaTipoRedondeo As String
    Public Property IndicaTipoRedondeo() As String
        Get
            Return (_IndicaTipoRedondeo)
        End Get
        Set(ByVal value As String)
            _IndicaTipoRedondeo = value
        End Set
    End Property

    Private _DesactivaObjetoFechaEnOperaciones As String
    Public Property DesactivaObjetoFechaEnOperaciones() As String
        Get
            Return (_DesactivaObjetoFechaEnOperaciones)
        End Get
        Set(ByVal value As String)
            _DesactivaObjetoFechaEnOperaciones = value
        End Set
    End Property
    Private _CantidadDecimalRedondeo As Integer
    Public Property CantidadDecimalRedondeo() As Integer
        Get
            Return (_CantidadDecimalRedondeo)
        End Get
        Set(ByVal value As Integer)
            _CantidadDecimalRedondeo = value
        End Set
    End Property
    Private _PermiteFacturarSinExistenciaProducto As Integer
    Public Property PermiteFacturarSinExistenciaProducto() As Integer
        Get
            Return (_PermiteFacturarSinExistenciaProducto)
        End Get
        Set(ByVal value As Integer)
            _PermiteFacturarSinExistenciaProducto = value
        End Set
    End Property

End Class
