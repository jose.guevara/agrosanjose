﻿Public Class SEOpcionxPerfil
    Private _IdPerfil As Int64
    Public Property IdPerfil() As Int64
        Get
            Return (_IdPerfil)
        End Get
        Set(ByVal value As Int64)
            _IdPerfil = value
        End Set
    End Property
    Private _IdOpcionMenu As Int64
    Public Property IdOpcionMenu() As Int64
        Get
            Return (_IdOpcionMenu)
        End Get
        Set(ByVal value As Int64)
            _IdOpcionMenu = value
        End Set
    End Property

    Public Sub New(ByVal pvnIdPerfil As Int64, _
ByVal pvnIdOpcionMenu As Int64)
        _IdPerfil = pvnIdPerfil
        _IdOpcionMenu = pvnIdOpcionMenu
    End Sub
End Class
