﻿Public Class SEOpcionModulo
    Private _IdModulo As Int64
    Public Property IdModulo() As Int64
        Get
            Return (_IdModulo)
        End Get
        Set(ByVal value As Int64)
            _IdModulo = value
        End Set
    End Property
    Private _IdOpcionMenu As Int64
    Public Property IdOpcionMenu() As Int64
        Get
            Return (_IdOpcionMenu)
        End Get
        Set(ByVal value As Int64)
            _IdOpcionMenu = value
        End Set
    End Property
    Private _Activo As Int16
    Public Property Activo() As Int16
        Get
            Return (_Activo)
        End Get
        Set(ByVal value As Int16)
            _Activo = value
        End Set
    End Property

    Public Sub New(ByVal pvnIdModulo As Int64, _
ByVal pvnIdOpcionMenu As Int64, _
ByVal pvnActivo As Int16)
        _IdModulo = pvnIdModulo
        _IdOpcionMenu = pvnIdOpcionMenu
        _Activo = pvnActivo
    End Sub
End Class
