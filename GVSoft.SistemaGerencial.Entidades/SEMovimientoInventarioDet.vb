﻿Public Class SEMovimientoInventarioDet
    Private _registro As Int64
    Public Property registro() As Int64
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Int64)
            _registro = value
        End Set
    End Property
    Private _prodregistro As Int16
    Public Property prodregistro() As Int16
        Get
            Return (_prodregistro)
        End Get
        Set(ByVal value As Int16)
            _prodregistro = value
        End Set
    End Property
    Private _cantidad As Double
    Public Property cantidad() As Double
        Get
            Return (_cantidad)
        End Get
        Set(ByVal value As Double)
            _cantidad = value
        End Set
    End Property
    Private _PrecioCosto As Double
    Public Property PrecioCosto() As Double
        Get
            Return (_PrecioCosto)
        End Get
        Set(ByVal value As Double)
            _PrecioCosto = value
        End Set
    End Property
    Private _precio As Double
    Public Property precio() As Double
        Get
            Return (_precio)
        End Get
        Set(ByVal value As Double)
            _precio = value
        End Set
    End Property
    Private _valor As Double
    Public Property valor() As Double
        Get
            Return (_valor)
        End Get
        Set(ByVal value As Double)
            _valor = value
        End Set
    End Property
    Private _igv As Double
    Public Property igv() As Double
        Get
            Return (_igv)
        End Get
        Set(ByVal value As Double)
            _igv = value
        End Set
    End Property

    Public Sub New(ByVal objMovInventarioDet As SEMovimientoInventarioDet)
        _registro = objMovInventarioDet.registro
        _prodregistro = objMovInventarioDet.prodregistro
        _cantidad = objMovInventarioDet.cantidad
        _PrecioCosto = objMovInventarioDet.PrecioCosto
        _precio = objMovInventarioDet.precio
        _valor = objMovInventarioDet.valor
        _igv = objMovInventarioDet.igv
    End Sub

    Public Sub New()

    End Sub

End Class
