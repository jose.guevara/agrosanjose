﻿Public Class SEDetallePedidos
    Private _IdPedido As Int64
    Public Property IdPedido() As Int64
        Get
            Return (_IdPedido)
        End Get
        Set(ByVal value As Int64)
            _IdPedido = value
        End Set
    End Property
    Private _ProdRegistro As Int16
    Public Property ProdRegistro() As Int16
        Get
            Return (_ProdRegistro)
        End Get
        Set(ByVal value As Int16)
            _ProdRegistro = value
        End Set
    End Property
    Private _Cantidad As Double
    Public Property Cantidad() As Double
        Get
            Return (_Cantidad)
        End Get
        Set(ByVal value As Double)
            _Cantidad = value
        End Set
    End Property
    Private _PrecioCosto As Double
    Public Property PrecioCosto() As Double
        Get
            Return (_PrecioCosto)
        End Get
        Set(ByVal value As Double)
            _PrecioCosto = value
        End Set
    End Property
    Private _CostoUnitario As Double
    Public Property CostoUnitario() As Double
        Get
            Return (_CostoUnitario)
        End Get
        Set(ByVal value As Double)
            _CostoUnitario = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total() As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _IGV As Double
    Public Property IGV() As Double
        Get
            Return (_IGV)
        End Get
        Set(ByVal value As Double)
            _IGV = value
        End Set
    End Property

    Public Sub New(ByVal objPedidoInventario As SEDetallePedidos)
        _IdPedido = objPedidoInventario.IdPedido
        _ProdRegistro = objPedidoInventario.ProdRegistro
        _Cantidad = objPedidoInventario.Cantidad
        _PrecioCosto = objPedidoInventario.PrecioCosto
        _CostoUnitario = objPedidoInventario.CostoUnitario
        _Total = objPedidoInventario.Total
        _IGV = objPedidoInventario.IGV
    End Sub

    Public Sub New()

    End Sub

End Class
