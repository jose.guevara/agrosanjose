﻿Public Class SETotalesFactura
    Private _SubTotal As Double
    Public Property SubTotal() As Double
        Get
            Return (_SubTotal)
        End Get
        Set(ByVal value As Double)
            _SubTotal = value
        End Set
    End Property
    Private _TotalImpuesto As Double
    Public Property TotalImpuesto() As Double
        Get
            Return (_TotalImpuesto)
        End Get
        Set(ByVal value As Double)
            _TotalImpuesto = value
        End Set
    End Property
    Private _TotalRetencion As Double
    Public Property TotalRetencion() As Double
        Get
            Return (_TotalRetencion)
        End Get
        Set(ByVal value As Double)
            _TotalRetencion = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total() As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property

    Public Sub New(ByVal pvnSubTotal As Double, _
                        ByVal pvnTotalImpuesto As Double, _
                        ByVal pvnTotalRetencion As Double, _
                        ByVal pvnTotal As Double)
        _SubTotal = pvnSubTotal
        _TotalImpuesto = pvnTotalImpuesto
        _TotalRetencion = pvnTotalRetencion
        _Total = pvnTotal
    End Sub

    Public Sub New()
    End Sub
End Class
