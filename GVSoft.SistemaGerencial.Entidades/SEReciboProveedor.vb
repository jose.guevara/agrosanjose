﻿Public Class SEReciboProveedor

    Private _IdRegistro As Int64
    Public Property IdRegistro() As Int64
        Get
            Return (_IdRegistro)
        End Get
        Set(ByVal value As Int64)
            _IdRegistro = value
        End Set
    End Property
    Private _Fecha As String
    Public Property Fecha() As String
        Get
            Return (_Fecha)
        End Get
        Set(ByVal value As String)
            _Fecha = value
        End Set
    End Property
    Private _NumFecha As Int64
    Public Property NumFecha() As Int64
        Get
            Return (_NumFecha)
        End Get
        Set(ByVal value As Int64)
            _NumFecha = value
        End Set
    End Property
    Private _IdProveedor As Int16
    Public Property IdProveedor() As Int16
        Get
            Return (_IdProveedor)
        End Get
        Set(ByVal value As Int16)
            _IdProveedor = value
        End Set
    End Property
    Private _IdVendedor As Int16
    Public Property IdVendedor() As Int16
        Get
            Return (_IdVendedor)
        End Get
        Set(ByVal value As Int16)
            _IdVendedor = value
        End Set
    End Property
    Private _Numero As String
    Public Property Numero() As String
        Get
            Return (_Numero)
        End Get
        Set(ByVal value As String)
            _Numero = value
        End Set
    End Property
    Private _Monto As Double
    Public Property Monto() As Double
        Get
            Return (_Monto)
        End Get
        Set(ByVal value As Double)
            _Monto = value
        End Set
    End Property
    Private _Interes As Double
    Public Property Interes() As Double
        Get
            Return (_Interes)
        End Get
        Set(ByVal value As Double)
            _Interes = value
        End Set
    End Property
    Private _Retencion As Double
    Public Property Retencion() As Double
        Get
            Return (_Retencion)
        End Get
        Set(ByVal value As Double)
            _Retencion = value
        End Set
    End Property
    Private _FormaPago As Int16
    Public Property FormaPago() As Int16
        Get
            Return (_FormaPago)
        End Get
        Set(ByVal value As Int16)
            _FormaPago = value
        End Set
    End Property
    Private _NumChqTarj As String
    Public Property NumChqTarj() As String
        Get
            Return (_NumChqTarj)
        End Get
        Set(ByVal value As String)
            _NumChqTarj = value
        End Set
    End Property
    Private _NombreBanco As String
    Public Property NombreBanco() As String
        Get
            Return (_NombreBanco)
        End Get
        Set(ByVal value As String)
            _NombreBanco = value
        End Set
    End Property
    Private _ReciboProv As String
    Public Property ReciboProv() As String
        Get
            Return (_ReciboProv)
        End Get
        Set(ByVal value As String)
            _ReciboProv = value
        End Set
    End Property
    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return (_Descripcion)
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Private _IdUsuario As Int16
    Public Property IdUsuario() As Int16
        Get
            Return (_IdUsuario)
        End Get
        Set(ByVal value As Int16)
            _IdUsuario = value
        End Set
    End Property
    Private _TipoRecibo As Int16
    Public Property TipoRecibo() As Int16
        Get
            Return (_TipoRecibo)
        End Get
        Set(ByVal value As Int16)
            _TipoRecibo = value
        End Set
    End Property
    Private _Fecha_ing As DateTime
    Public Property Fecha_ing() As DateTime
        Get
            Return (_Fecha_ing)
        End Get
        Set(ByVal value As DateTime)
            _Fecha_ing = value
        End Set
    End Property
    Private _SaldoFavor As Double
    Public Property SaldoFavor() As Double
        Get
            Return (_SaldoFavor)
        End Get
        Set(ByVal value As Double)
            _SaldoFavor = value
        End Set
    End Property
    Private _Estado As Int16
    Public Property Estado() As Int16
        Get
            Return (_Estado)
        End Get
        Set(ByVal value As Int16)
            _Estado = value
        End Set
    End Property
    Private _Descr01 As String
    Public Property Descr01() As String
        Get
            Return (_Descr01)
        End Get
        Set(ByVal value As String)
            _Descr01 = value
        End Set
    End Property
    Private _Descr02 As String
    Public Property Descr02() As String
        Get
            Return (_Descr02)
        End Get
        Set(ByVal value As String)
            _Descr02 = value
        End Set
    End Property
    Private _Descr03 As String
    Public Property Descr03() As String
        Get
            Return (_Descr03)
        End Get
        Set(ByVal value As String)
            _Descr03 = value
        End Set
    End Property
    Private _IdAgencia As Int64
    Public Property IdAgencia() As Int64
        Get
            Return (_IdAgencia)
        End Get
        Set(ByVal value As Int64)
            _IdAgencia = value
        End Set
    End Property
    Private _FechaAnulado As DateTime
    Public Property FechaAnulado() As DateTime
        Get
            Return (_FechaAnulado)
        End Get
        Set(ByVal value As DateTime)
            _FechaAnulado = value
        End Set
    End Property
    Private _NumeroArqueo As Int64
    Public Property NumeroArqueo() As Int64
        Get
            Return (_NumeroArqueo)
        End Get
        Set(ByVal value As Int64)
            _NumeroArqueo = value
        End Set
    End Property

    Public Sub New()

    End Sub

    'Public Sub New(ByVal objReciboProveedor As SEReciboProveedor)

    ' _IdRegistro = objReciboProveedor.IdRegistro
    ' _Fecha = objReciboProveedor.Fecha
    ' _NumFecha = objReciboProveedor.NumFecha
    ' _IdProveedor = objReciboProveedor.IdProveedor
    ' _IdVendedor = objReciboProveedor.IdVendedor
    ' _Numero = objReciboProveedor.Numero
    ' _Monto = objReciboProveedor.Monto
    ' _Interes = objReciboProveedor.Interes
    ' _Retencion = objReciboProveedor.Retencion
    ' _FormaPago = objReciboProveedor.FormaPago
    ' _NumChqTarj = objReciboProveedor.NumChqTarj
    ' _NombreBanco = objReciboProveedor.NombreBanco
    ' _ReciboProv = objReciboProveedor.ReciboProv
    ' _Descripcion = objReciboProveedor.Descripcion
    ' _IdUsuario = objReciboProveedor.IdUsuario
    ' _TipoRecibo = objReciboProveedor.TipoRecibo
    ' _Fecha_ing = objReciboProveedor.Fecha_ing
    ' _SaldoFavor = objReciboProveedor.SaldoFavor
    ' _Estado = objReciboProveedor.Estado
    ' _Descr01 = objReciboProveedor.Descr01
    ' _Descr02 = objReciboProveedor.Descr02
    ' _Descr03 = objReciboProveedor.Descr03
    ' _IdAgencia = objReciboProveedor.IdAgencia
    ' _FechaAnulado = objReciboProveedor.FechaAnulado
    ' _NumeroArqueo = objReciboProveedor.NumeroArqueo

    'End Sub



End Class
