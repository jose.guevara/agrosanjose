﻿Public Class SEMaestroPedidos

    Private _IdPedido As Int64
    Public Property IdPedido() As Int64
        Get
            Return (_IdPedido)
        End Get
        Set(ByVal value As Int64)
            _IdPedido = value
        End Set
    End Property
    Private _NumeroDocumento As String
    Public Property NumeroDocumento() As String
        Get
            Return (_NumeroDocumento)
        End Get
        Set(ByVal value As String)
            _NumeroDocumento = value
        End Set
    End Property
    Private _IdMovimiento As String
    Public Property IdMovimiento() As String
        Get
            Return (_IdMovimiento)
        End Get
        Set(ByVal value As String)
            _IdMovimiento = value
        End Set
    End Property
    Private _NumFechaIng As Int64
    Public Property NumFechaIng() As Int64
        Get
            Return (_NumFechaIng)
        End Get
        Set(ByVal value As Int64)
            _NumFechaIng = value
        End Set
    End Property
    Private _FechaIngreso As String
    Public Property FechaIngreso() As String
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As String)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoReg As DateTime
    Public Property FechaIngresoReg() As DateTime
        Get
            Return (_FechaIngresoReg)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngresoReg = value
        End Set
    End Property
    Private _SubTotal As Double
    Public Property SubTotal() As Double
        Get
            Return (_SubTotal)
        End Get
        Set(ByVal value As Double)
            _SubTotal = value
        End Set
    End Property
    Private _SubTotalCosto As Double
    Public Property SubTotalCosto() As Double
        Get
            Return (_SubTotalCosto)
        End Get
        Set(ByVal value As Double)
            _SubTotalCosto = value
        End Set
    End Property
    Private _Impuesto As Double
    Public Property Impuesto() As Double
        Get
            Return (_Impuesto)
        End Get
        Set(ByVal value As Double)
            _Impuesto = value
        End Set
    End Property
    Private _Retencion As Double
    Public Property Retencion() As Double
        Get
            Return (_Retencion)
        End Get
        Set(ByVal value As Double)
            _Retencion = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total() As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _UserRegistro As Int16
    Public Property UserRegistro() As Int16
        Get
            Return (_UserRegistro)
        End Get
        Set(ByVal value As Int16)
            _UserRegistro = value
        End Set
    End Property
    Private _AgenregistroOrigen As Int16
    Public Property AgenregistroOrigen() As Int16
        Get
            Return (_AgenregistroOrigen)
        End Get
        Set(ByVal value As Int16)
            _AgenregistroOrigen = value
        End Set
    End Property
    Private _IdEstado As Int16
    Public Property IdEstado() As Int16
        Get
            Return (_IdEstado)
        End Get
        Set(ByVal value As Int16)
            _IdEstado = value
        End Set
    End Property
    Private _TipoCambio As Double
    Public Property TipoCambio() As Double
        Get
            Return (_TipoCambio)
        End Get
        Set(ByVal value As Double)
            _TipoCambio = value
        End Set
    End Property
    Private _NumFechaAnul As Int64
    Public Property NumFechaAnul() As Int64
        Get
            Return (_NumFechaAnul)
        End Get
        Set(ByVal value As Int64)
            _NumFechaAnul = value
        End Set
    End Property

    Public Sub New(ByVal pvbigintIdPedido As Integer, _
                    ByVal pvsNumeroDocumento As String, _
                    ByVal pvsIdMovimiento As String, _
                    ByVal pvnNumFechaIng As Int64, _
                    ByVal pvsFechaIngreso As String, _
                    ByVal pvdFechaIngresoReg As DateTime, _
                    ByVal pvnSubTotal As Double, _
                    ByVal pvnSubTotalCosto As Double, _
                    ByVal pvnImpuesto As Double, _
                    ByVal pvnRetencion As Double, _
                    ByVal pvnTotal As Double, _
                    ByVal pvnUserRegistro As Int16, _
                    ByVal pvnAgenregistroOrigen As Int16, _
                    ByVal pvnIdEstado As Int16, _
                    ByVal pvnTipoCambio As Double, _
                    ByVal pvnNumFechaAnul As Int64)

        _IdPedido = pvbigintIdPedido
        _NumeroDocumento = pvsNumeroDocumento
        _IdMovimiento = pvsIdMovimiento
        _NumFechaIng = pvnNumFechaIng
        _FechaIngreso = pvsFechaIngreso
        _FechaIngresoReg = pvdFechaIngresoReg
        _SubTotal = pvnSubTotal
        _SubTotalCosto = pvnSubTotalCosto
        _Impuesto = pvnImpuesto
        _Retencion = pvnRetencion
        _Total = pvnTotal
        _UserRegistro = pvnUserRegistro
        _AgenregistroOrigen = pvnAgenregistroOrigen
        _IdEstado = pvnIdEstado
        _TipoCambio = pvnTipoCambio
        _NumFechaAnul = pvnNumFechaAnul

    End Sub

    Public Sub New()

    End Sub

End Class
