﻿Public Class SECuadreCajaEncabezado
    Private _IdCuadre As Int64
    Public Property IdCuadre() As Int64
        Get
            Return (_IdCuadre)
        End Get
        Set(ByVal value As Int64)
            _IdCuadre = value
        End Set
    End Property
    Private _IdMoneda As String
    Public Property IdMoneda() As String
        Get
            Return (_IdMoneda)
        End Get
        Set(ByVal value As String)
            _IdMoneda = value
        End Set
    End Property
    Private _IdUsuario As Int64
    Public Property IdUsuario() As Int64
        Get
            Return (_IdUsuario)
        End Get
        Set(ByVal value As Int64)
            _IdUsuario = value
        End Set
    End Property
    Private _IdSucursal As Int64
    Public Property IdSucursal() As Int64
        Get
            Return (_IdSucursal)
        End Get
        Set(ByVal value As Int64)
            _IdSucursal = value
        End Set
    End Property
    Private _NumeroCuadre As String
    Public Property NumeroCuadre() As String
        Get
            Return (_NumeroCuadre)
        End Get
        Set(ByVal value As String)
            _NumeroCuadre = value
        End Set
    End Property
    Private _FechaInicioCuadre As String
    Public Property FechaInicioCuadre() As String
        Get
            Return (_FechaInicioCuadre)
        End Get
        Set(ByVal value As String)
            _FechaInicioCuadre = value
        End Set
    End Property
    Private _FechaInicioCuadreNum As String
    Public Property FechaInicioCuadreNum() As String
        Get
            Return (_FechaInicioCuadreNum)
        End Get
        Set(ByVal value As String)
            _FechaInicioCuadreNum = value
        End Set
    End Property
    Private _FechaCuadreFin As String
    Public Property FechaCuadreFin() As String
        Get
            Return (_FechaCuadreFin)
        End Get
        Set(ByVal value As String)
            _FechaCuadreFin = value
        End Set
    End Property
    Private _FechaCuadreFinNum As String
    Public Property FechaCuadreFinNum() As String
        Get
            Return (_FechaCuadreFinNum)
        End Get
        Set(ByVal value As String)
            _FechaCuadreFinNum = value
        End Set
    End Property
    Private _FacturaCreditoInicial As String
    Public Property FacturaCreditoInicial() As String
        Get
            Return (_FacturaCreditoInicial)
        End Get
        Set(ByVal value As String)
            _FacturaCreditoInicial = value
        End Set
    End Property
    Private _FacturaCreditoFinal As String
    Public Property FacturaCreditoFinal() As String
        Get
            Return (_FacturaCreditoFinal)
        End Get
        Set(ByVal value As String)
            _FacturaCreditoFinal = value
        End Set
    End Property
    Private _FacturaContadoInicial As String
    Public Property FacturaContadoInicial() As String
        Get
            Return (_FacturaContadoInicial)
        End Get
        Set(ByVal value As String)
            _FacturaContadoInicial = value
        End Set
    End Property
    Private _FacturaContadoFinal As String
    Public Property FacturaContadoFinal() As String
        Get
            Return (_FacturaContadoFinal)
        End Get
        Set(ByVal value As String)
            _FacturaContadoFinal = value
        End Set
    End Property
    Private _ReciboInicialCredito As String
    Public Property ReciboInicialCredito() As String
        Get
            Return (_ReciboInicialCredito)
        End Get
        Set(ByVal value As String)
            _ReciboInicialCredito = value
        End Set
    End Property
    Private _ReciboFinalCredito As String
    Public Property ReciboFinalCredito() As String
        Get
            Return (_ReciboFinalCredito)
        End Get
        Set(ByVal value As String)
            _ReciboFinalCredito = value
        End Set
    End Property
    Private _ReciboInicialDebito As String
    Public Property ReciboInicialDebito() As String
        Get
            Return (_ReciboInicialDebito)
        End Get
        Set(ByVal value As String)
            _ReciboInicialDebito = value
        End Set
    End Property
    Private _ReciboFinalDebito As String
    Public Property ReciboFinalDebito() As String
        Get
            Return (_ReciboFinalDebito)
        End Get
        Set(ByVal value As String)
            _ReciboFinalDebito = value
        End Set
    End Property
    Private _SaldoInicial As Double
    Public Property SaldoInicial() As Double
        Get
            Return (_SaldoInicial)
        End Get
        Set(ByVal value As Double)
            _SaldoInicial = value
        End Set
    End Property
    Private _OtrosIngresos As Double
    Public Property OtrosIngresos() As Double
        Get
            Return (_OtrosIngresos)
        End Get
        Set(ByVal value As Double)
            _OtrosIngresos = value
        End Set
    End Property
    Private _SobranteCaja As Double
    Public Property SobranteCaja() As Double
        Get
            Return (_SobranteCaja)
        End Get
        Set(ByVal value As Double)
            _SobranteCaja = value
        End Set
    End Property
    Private _MontoTotalVentas As Double
    Public Property MontoTotalVentas() As Double
        Get
            Return (_MontoTotalVentas)
        End Get
        Set(ByVal value As Double)
            _MontoTotalVentas = value
        End Set
    End Property
    Private _MontoTotalRecibos As Double
    Public Property MontoTotalRecibos() As Double
        Get
            Return (_MontoTotalRecibos)
        End Get
        Set(ByVal value As Double)
            _MontoTotalRecibos = value
        End Set
    End Property
    Private _MontoGastos As Double
    Public Property MontoGastos() As Double
        Get
            Return (_MontoGastos)
        End Get
        Set(ByVal value As Double)
            _MontoGastos = value
        End Set
    End Property
    Private _MontoDepositos As Double
    Public Property MontoDepositos() As Double
        Get
            Return (_MontoDepositos)
        End Get
        Set(ByVal value As Double)
            _MontoDepositos = value
        End Set
    End Property
    Private _MontoTotalDineroCaja As Double
    Public Property MontoTotalDineroCaja() As Double
        Get
            Return (_MontoTotalDineroCaja)
        End Get
        Set(ByVal value As Double)
            _MontoTotalDineroCaja = value
        End Set
    End Property

    Private _TotalIngresos As Double
    Public Property TotalIngresos() As Double
        Get
            Return (_TotalIngresos)
        End Get
        Set(ByVal value As Double)
            _TotalIngresos = value
        End Set
    End Property
    Private _TotalEgresos As Double
    Public Property TotalEgresos() As Double
        Get
            Return (_TotalEgresos)
        End Get
        Set(ByVal value As Double)
            _TotalEgresos = value
        End Set
    End Property
    Private _SaldoFinal As Double
    Public Property SaldoFinal() As Double
        Get
            Return (_SaldoFinal)
        End Get
        Set(ByVal value As Double)
            _SaldoFinal = value
        End Set
    End Property
    Private _Tasa As Double
    Public Property Tasa() As Double
        Get
            Return (_Tasa)
        End Get
        Set(ByVal value As Double)
            _Tasa = value
        End Set
    End Property
    Private _Observacion As String
    Public Property Observacion() As String
        Get
            Return (_Observacion)
        End Get
        Set(ByVal value As String)
            _Observacion = value
        End Set
    End Property
    Private _IdEstado As String
    Public Property IdEstado() As String
        Get
            Return (_IdEstado)
        End Get
        Set(ByVal value As String)
            _IdEstado = value
        End Set
    End Property
    Private _FechaModifica As DateTime
    Public Property FechaModifica() As DateTime
        Get
            Return (_FechaModifica)
        End Get
        Set(ByVal value As DateTime)
            _FechaModifica = value
        End Set
    End Property
    Private _FechaModificaNum As String
    Public Property FechaModificaNum() As String
        Get
            Return (_FechaModificaNum)
        End Get
        Set(ByVal value As String)
            _FechaModificaNum = value
        End Set
    End Property
    Private _FechaAnula As DateTime
    Public Property FechaAnula() As DateTime
        Get
            Return (_FechaAnula)
        End Get
        Set(ByVal value As DateTime)
            _FechaAnula = value
        End Set
    End Property
    Private _FechaAnulaNum As String
    Public Property FechaAnulaNum() As String
        Get
            Return (_FechaAnulaNum)
        End Get
        Set(ByVal value As String)
            _FechaAnulaNum = value
        End Set
    End Property
    Private _FechaIngreso As DateTime
    Public Property FechaIngreso() As DateTime
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoNum As String
    Public Property FechaIngresoNum() As String
        Get
            Return (_FechaIngresoNum)
        End Get
        Set(ByVal value As String)
            _FechaIngresoNum = value
        End Set
    End Property
    Private _IdUsuarioIngresa As Int64
    Public Property IdUsuarioIngresa() As Int64
        Get
            Return (_IdUsuarioIngresa)
        End Get
        Set(ByVal value As Int64)
            _IdUsuarioIngresa = value
        End Set
    End Property
    Private _IdUsuarioModifica As Int64
    Public Property IdUsuarioModifica() As Int64
        Get
            Return (_IdUsuarioModifica)
        End Get
        Set(ByVal value As Int64)
            _IdUsuarioModifica = value
        End Set
    End Property
    Private _IdUsuarioAnula As Int64
    Public Property IdUsuarioAnula() As Int64
        Get
            Return (_IdUsuarioAnula)
        End Get
        Set(ByVal value As Int64)
            _IdUsuarioAnula = value
        End Set
    End Property
    Private _IdUltimoCuadre As Int64
    Public Property IdUltimoCuadre() As Int64
        Get
            Return (_IdUltimoCuadre)
        End Get
        Set(ByVal value As Int64)
            _IdUltimoCuadre = value
        End Set
    End Property
    Public Sub New()

    End Sub
End Class
