﻿Public Class SEUsuario
    Private _IdUsuario As Int64
    Public Property IdUsuario() As Int64
        Get
            Return (_IdUsuario)
        End Get
        Set(ByVal value As Int64)
            _IdUsuario = value
        End Set
    End Property
    Private _Usuario As String
    Public Property Usuario() As String
        Get
            Return (_Usuario)
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property
    Private _PriNombre As String
    Public Property PriNombre() As String
        Get
            Return (_PriNombre)
        End Get
        Set(ByVal value As String)
            _PriNombre = value
        End Set
    End Property
    Private _SegNombre As String
    Public Property SegNombre() As String
        Get
            Return (_SegNombre)
        End Get
        Set(ByVal value As String)
            _SegNombre = value
        End Set
    End Property
    Private _PriApellido As String
    Public Property PriApellido() As String
        Get
            Return (_PriApellido)
        End Get
        Set(ByVal value As String)
            _PriApellido = value
        End Set
    End Property
    Private _SegApellido As String
    Public Property SegApellido() As String
        Get
            Return (_SegApellido)
        End Get
        Set(ByVal value As String)
            _SegApellido = value
        End Set
    End Property
    Private _Password As String
    Public Property Password() As String
        Get
            Return (_Password)
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property
    Private _FecBaja As DateTime
    Public Property FecBaja() As DateTime
        Get
            Return (_FecBaja)
        End Get
        Set(ByVal value As DateTime)
            _FecBaja = value
        End Set
    End Property
    Private _Activo As Int16
    Public Property Activo() As Int16
        Get
            Return (_Activo)
        End Get
        Set(ByVal value As Int16)
            _Activo = value
        End Set
    End Property

    Public Sub New(ByVal pvnIdUsuario As Int64, _
ByVal pvsUsuario As String, _
ByVal pvsPriNombre As String, _
ByVal pvsSegNombre As String, _
ByVal pvsPriApellido As String, _
ByVal pvsSegApellido As String, _
ByVal pvsPassword As String, _
ByVal pvdFecBaja As DateTime, _
ByVal pvnActivo As Int16)
        _IdUsuario = pvnIdUsuario
        _Usuario = pvsUsuario
        _PriNombre = pvsPriNombre
        _SegNombre = pvsSegNombre
        _PriApellido = pvsPriApellido
        _SegApellido = pvsSegApellido
        _Password = pvsPassword
        _FecBaja = pvdFecBaja
        _Activo = pvnActivo
    End Sub
End Class
