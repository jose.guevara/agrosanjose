﻿Public Class SEProductoMovimiento
    Private _registro As Long
    Public Property registro As Long
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Long)
            _registro = value
        End Set
    End Property
    Private _numfechaing As Double
    Public Property numfechaing As Double
        Get
            Return (_numfechaing)
        End Get
        Set(ByVal value As Double)
            _numfechaing = value
        End Set
    End Property
    Private _numtiempo As Long
    Public Property numtiempo As Long
        Get
            Return (_numtiempo)
        End Get
        Set(ByVal value As Long)
            _numtiempo = value
        End Set
    End Property
    Private _agenregistro As Integer
    Public Property agenregistro As Integer
        Get
            Return (_agenregistro)
        End Get
        Set(ByVal value As Integer)
            _agenregistro = value
        End Set
    End Property
    Private _prodregistro As Integer
    Public Property prodregistro As Integer
        Get
            Return (_prodregistro)
        End Get
        Set(ByVal value As Integer)
            _prodregistro = value
        End Set
    End Property
    Private _fechaingreso As String
    Public Property fechaingreso As String
        Get
            Return (_fechaingreso)
        End Get
        Set(ByVal value As String)
            _fechaingreso = value
        End Set
    End Property
    Private _FechaIngresoReg As DateTime
    Public Property FechaIngresoReg As DateTime
        Get
            Return (_FechaIngresoReg)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngresoReg = value
        End Set
    End Property
    Private _numdocumento As String
    Public Property numdocumento As String
        Get
            Return (_numdocumento)
        End Get
        Set(ByVal value As String)
            _numdocumento = value
        End Set
    End Property
    Private _codigomovim As String
    Public Property codigomovim As String
        Get
            Return (_codigomovim)
        End Get
        Set(ByVal value As String)
            _codigomovim = value
        End Set
    End Property
    Private _codigomoneda As String
    Public Property codigomoneda As String
        Get
            Return (_codigomoneda)
        End Get
        Set(ByVal value As String)
            _codigomoneda = value
        End Set
    End Property
    Private _totalfactura As Double
    Public Property totalfactura As Double
        Get
            Return (_totalfactura)
        End Get
        Set(ByVal value As Double)
            _totalfactura = value
        End Set
    End Property
    Private _Cantidad As Double
    Public Property Cantidad As Double
        Get
            Return (_Cantidad)
        End Get
        Set(ByVal value As Double)
            _Cantidad = value
        End Set
    End Property
    Private _TotalCosto As Double
    Public Property TotalCosto As Double
        Get
            Return (_TotalCosto)
        End Get
        Set(ByVal value As Double)
            _TotalCosto = value
        End Set
    End Property

    Private _Saldo As Double
    Public Property Saldo As Double
        Get
            Return (_Saldo)
        End Get
        Set(ByVal value As Double)
            _Saldo = value
        End Set
    End Property

    Private _creditos_cnt As Double
    Public Property creditos_cnt As Double
        Get
            Return (_creditos_cnt)
        End Get
        Set(ByVal value As Double)
            _creditos_cnt = value
        End Set
    End Property
    Private _creditos_cto As Double
    Public Property creditos_cto As Double
        Get
            Return (_creditos_cto)
        End Get
        Set(ByVal value As Double)
            _creditos_cto = value
        End Set
    End Property
    Private _creditos_mto As Double
    Public Property creditos_mto As Double
        Get
            Return (_creditos_mto)
        End Get
        Set(ByVal value As Double)
            _creditos_mto = value
        End Set
    End Property
    Private _debitos_cnt As Double
    Public Property debitos_cnt As Double
        Get
            Return (_debitos_cnt)
        End Get
        Set(ByVal value As Double)
            _debitos_cnt = value
        End Set
    End Property
    Private _debitos_cto As Double
    Public Property debitos_cto As Double
        Get
            Return (_debitos_cto)
        End Get
        Set(ByVal value As Double)
            _debitos_cto = value
        End Set
    End Property
    Private _debitos_mto As Double
    Public Property debitos_mto As Double
        Get
            Return (_debitos_mto)
        End Get
        Set(ByVal value As Double)
            _debitos_mto = value
        End Set
    End Property
    Private _saldo_cnt As Double
    Public Property saldo_cnt As Double
        Get
            Return (_saldo_cnt)
        End Get
        Set(ByVal value As Double)
            _saldo_cnt = value
        End Set
    End Property
    Private _saldo_cto As Double
    Public Property saldo_cto As Double
        Get
            Return (_saldo_cto)
        End Get
        Set(ByVal value As Double)
            _saldo_cto = value
        End Set
    End Property
    Private _saldo_mto As Double
    Public Property saldo_mto As Double
        Get
            Return (_saldo_mto)
        End Get
        Set(ByVal value As Double)
            _saldo_mto = value
        End Set
    End Property
    Private _cuenta_debe As String
    Public Property cuenta_debe As String
        Get
            Return (_cuenta_debe)
        End Get
        Set(ByVal value As String)
            _cuenta_debe = value
        End Set
    End Property
    Private _cuenta_haber As String
    Public Property cuenta_haber As String
        Get
            Return (_cuenta_haber)
        End Get
        Set(ByVal value As String)
            _cuenta_haber = value
        End Set
    End Property
    Private _userregistro As Integer
    Public Property userregistro As Integer
        Get
            Return (_userregistro)
        End Get
        Set(ByVal value As Integer)
            _userregistro = value
        End Set
    End Property
    Private _origen As Integer
    Public Property origen As Integer
        Get
            Return (_origen)
        End Get
        Set(ByVal value As Integer)
            _origen = value
        End Set
    End Property

    Public Sub New()

    End Sub
    Public Sub New(ByVal pvnRegistro As Long,
    ByVal pvnNumfechaing As Double,
    ByVal pvnNumtiempo As Long,
    ByVal pvnAgenregistro As Integer,
    ByVal pvnProdregistro As Integer,
    ByVal pvsFechaingreso As String,
    ByVal pvdFechaIngresoReg As DateTime,
    ByVal pvsNumdocumento As String,
    ByVal pvsCodigomovim As String,
    ByVal pvsCodigomoneda As String,
    ByVal pvnTotalfactura As Double,
    ByVal pvnCantidad As Double,
    ByVal pvnTotalCosto As Double,
    ByVal pvnCreditos_cnt As Double,
    ByVal pvnCreditos_cto As Double,
    ByVal pvnCreditos_mto As Double,
    ByVal pvnDebitos_cnt As Double,
    ByVal pvnDebitos_cto As Double,
    ByVal pvnDebitos_mto As Double,
    ByVal pvnSaldo_cnt As Double,
    ByVal pvnSaldo_cto As Double,
    ByVal pvnSaldo_mto As Double,
    ByVal pvsCuenta_debe As String,
    ByVal pvsCuenta_haber As String,
    ByVal pvnUserregistro As Integer,
    ByVal pvnOrigen As Integer)

        _registro = pvnRegistro
        _numfechaing = pvnNumfechaing
        _numtiempo = pvnNumtiempo
        _agenregistro = pvnAgenregistro
        _prodregistro = pvnProdregistro
        _fechaingreso = pvsFechaingreso
        _FechaIngresoReg = pvdFechaIngresoReg
        _numdocumento = pvsNumdocumento
        _codigomovim = pvsCodigomovim
        _codigomoneda = pvsCodigomoneda
        _totalfactura = pvnTotalfactura
        _Cantidad = pvnCantidad
        _TotalCosto = pvnTotalCosto
        _creditos_cnt = pvnCreditos_cnt
        _creditos_cto = pvnCreditos_cto
        _creditos_mto = pvnCreditos_mto
        _debitos_cnt = pvnDebitos_cnt
        _debitos_cto = pvnDebitos_cto
        _debitos_mto = pvnDebitos_mto
        _saldo_cnt = pvnSaldo_cnt
        _saldo_cto = pvnSaldo_cto
        _saldo_mto = pvnSaldo_mto
        _cuenta_debe = pvsCuenta_debe
        _cuenta_haber = pvsCuenta_haber
        _userregistro = pvnUserregistro
        _origen = pvnOrigen
    End Sub
End Class
