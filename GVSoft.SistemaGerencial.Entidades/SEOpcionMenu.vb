﻿Public Class SEOpcionMenu
    Private _IdOpcionMenu As Int64
    Public Property IdOpcionMenu() As Int64
        Get
            Return (_IdOpcionMenu)
        End Get
        Set(ByVal value As Int64)
            _IdOpcionMenu = value
        End Set
    End Property
    Private _NombreOpcion As String
    Public Property NombreOpcion() As String
        Get
            Return (_NombreOpcion)
        End Get
        Set(ByVal value As String)
            _NombreOpcion = value
        End Set
    End Property
    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return (_Descripcion)
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Private _IdModulo As Int64
    Public Property IdModulo() As Int64
        Get
            Return (_IdModulo)
        End Get
        Set(ByVal value As Int64)
            _IdModulo = value
        End Set
    End Property
    Private _IdGrupo As Int64
    Public Property IdGrupo() As Int64
        Get
            Return (_IdGrupo)
        End Get
        Set(ByVal value As Int64)
            _IdGrupo = value
        End Set
    End Property
    Private _CodOpcionPadre As Int64
    Public Property CodOpcionPadre() As Int64
        Get
            Return (_CodOpcionPadre)
        End Get
        Set(ByVal value As Int64)
            _CodOpcionPadre = value
        End Set
    End Property
    Private _Posicion As Int64
    Public Property Posicion() As Int64
        Get
            Return (_Posicion)
        End Get
        Set(ByVal value As Int64)
            _Posicion = value
        End Set
    End Property
    Private _Icono As String
    Public Property Icono() As String
        Get
            Return (_Icono)
        End Get
        Set(ByVal value As String)
            _Icono = value
        End Set
    End Property
    Private _Forma As String
    Public Property Forma() As String
        Get
            Return (_Forma)
        End Get
        Set(ByVal value As String)
            _Forma = value
        End Set
    End Property
    Private _Activo As Int16
    Public Property Activo() As Int16
        Get
            Return (_Activo)
        End Get
        Set(ByVal value As Int16)
            _Activo = value
        End Set
    End Property

    Public Sub New(ByVal pvnIdOpcionMenu As Int64, _
ByVal pvsNombreOpcion As String, _
ByVal pvsDescripcion As String, _
ByVal pvnIdModulo As Int64, _
ByVal pvnIdGrupo As Int64, _
ByVal pvnCodOpcionPadre As Int64, _
ByVal pvnPosicion As Int64, _
ByVal pvsIcono As String, _
ByVal pvsForma As String, _
ByVal pvnActivo As Int16)
        _IdOpcionMenu = pvnIdOpcionMenu
        _NombreOpcion = pvsNombreOpcion
        _Descripcion = pvsDescripcion
        _IdModulo = pvnIdModulo
        _IdGrupo = pvnIdGrupo
        _CodOpcionPadre = pvnCodOpcionPadre
        _Posicion = pvnPosicion
        _Icono = pvsIcono
        _Forma = pvsForma
        _Activo = pvnActivo

    End Sub

End Class
