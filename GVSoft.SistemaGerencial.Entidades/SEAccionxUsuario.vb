﻿Public Class SEAccionxUsuario
    Private _IdUsuario As Int64
    Public Property IdUsuario() As Int64
        Get
            Return (_IdUsuario)
        End Get
        Set(ByVal value As Int64)
            _IdUsuario = value
        End Set
    End Property
    Private _IdOpcionMenu As Int64
    Public Property IdOpcionMenu() As Int64
        Get
            Return (_IdOpcionMenu)
        End Get
        Set(ByVal value As Int64)
            _IdOpcionMenu = value
        End Set
    End Property
    Private _IdAccion As Int64
    Public Property IdAccion() As Int64
        Get
            Return (_IdAccion)
        End Get
        Set(ByVal value As Int64)
            _IdAccion = value
        End Set
    End Property
    Private _Activo As Int16
    Public Property Activo() As Int16
        Get
            Return (_Activo)
        End Get
        Set(ByVal value As Int16)
            _Activo = value
        End Set
    End Property

    Public Sub New(ByVal pvnIdUsuario As Int64, _
ByVal pvnIdOpcionMenu As Int64, _
ByVal pvnIdAccion As Int64, _
ByVal pvnActivo As Int16)
        _IdUsuario = pvnIdUsuario
        _IdOpcionMenu = pvnIdOpcionMenu
        _IdAccion = pvnIdAccion
        _Activo = pvnActivo

    End Sub
End Class
