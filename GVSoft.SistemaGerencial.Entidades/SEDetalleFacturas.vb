﻿Public Class SEDetalleFacturas

    Private _registro As Int64
    Public Property registro() As Int64
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Int64)
            _registro = value
        End Set
    End Property
    Private _prodregistro As Int16
    Public Property prodregistro() As Int16
        Get
            Return (_prodregistro)
        End Get
        Set(ByVal value As Int16)
            _prodregistro = value
        End Set
    End Property
    Private _cantidad As Double
    Public Property cantidad() As Double
        Get
            Return (_cantidad)
        End Get
        Set(ByVal value As Double)
            _cantidad = value
        End Set
    End Property
    Private _PrecioCosto As Double
    Public Property PrecioCosto() As Double
        Get
            Return (_PrecioCosto)
        End Get
        Set(ByVal value As Double)
            _PrecioCosto = value
        End Set
    End Property
    Private _precio As Double
    Public Property precio() As Double
        Get
            Return (_precio)
        End Get
        Set(ByVal value As Double)
            _precio = value
        End Set
    End Property
    Private _valor As Double
    Public Property valor() As Double
        Get
            Return (_valor)
        End Get
        Set(ByVal value As Double)
            _valor = value
        End Set
    End Property
    Private _igv As Double
    Public Property igv() As Double
        Get
            Return (_igv)
        End Get
        Set(ByVal value As Double)
            _igv = value
        End Set
    End Property
    Private _PrcDescuento As Double = 0
    Public Property PrcDescuento() As Double
        Get
            Return (_PrcDescuento)
        End Get
        Set(ByVal value As Double)
            _PrcDescuento = value
        End Set
    End Property
    Private _MontoDescuento As Double = 0
    Public Property MontoDescuento() As Double
        Get
            Return (_MontoDescuento)
        End Get
        Set(ByVal value As Double)
            _MontoDescuento = value
        End Set
    End Property
    Private _EsBonificacion As Integer
    Public Property EsBonificacion() As Integer
        Get
            Return (_EsBonificacion)
        End Get
        Set(ByVal value As Integer)
            _EsBonificacion = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal objDetalleFactura As SEDetalleFacturas)

        _registro = objDetalleFactura.registro
        _prodregistro = objDetalleFactura.prodregistro
        _cantidad = objDetalleFactura.cantidad
        _PrecioCosto = objDetalleFactura.PrecioCosto
        _precio = objDetalleFactura.precio
        _valor = objDetalleFactura.valor
        _igv = objDetalleFactura.igv
        _EsBonificacion = objDetalleFactura.EsBonificacion
    End Sub

End Class
