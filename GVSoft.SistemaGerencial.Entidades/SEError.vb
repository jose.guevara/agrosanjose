﻿Public Class SEError
    Private _IdError As Int64
    Public Property IdError() As Int64
        Get
            Return (_IdError)
        End Get
        Set(ByVal value As Int64)
            _IdError = value
        End Set
    End Property
    Private _Fecha As DateTime
    Public Property Fecha() As DateTime
        Get
            Return (_Fecha)
        End Get
        Set(ByVal value As DateTime)
            _Fecha = value
        End Set
    End Property
    Private _FechaNum As String
    Public Property FechaNum() As String
        Get
            Return (_FechaNum)
        End Get
        Set(ByVal value As String)
            _FechaNum = value
        End Set
    End Property
    Private _Clase As String
    Public Property Clase() As String
        Get
            Return (_Clase)
        End Get
        Set(ByVal value As String)
            _Clase = value
        End Set
    End Property
    Private _Proceso As String
    Public Property Proceso() As String
        Get
            Return (_Proceso)
        End Get
        Set(ByVal value As String)
            _Proceso = value
        End Set
    End Property
    Private _Metodo As String
    Public Property Metodo() As String
        Get
            Return (_Metodo)
        End Get
        Set(ByVal value As String)
            _Metodo = value
        End Set
    End Property
    Private _Sentencia As String
    Public Property Sentencia() As String
        Get
            Return (_Sentencia)
        End Get
        Set(ByVal value As String)
            _Sentencia = value
        End Set
    End Property
    Private _descripcion As String
    Public Property descripcion() As String
        Get
            Return (_descripcion)
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Public Sub New(ByVal pvnIdError As Int64, _
    ByVal pvdFecha As DateTime, _
    ByVal pvsFechaNum As String, _
    ByVal pvsClase As String, _
    ByVal pvsProceso As String, _
    ByVal pvsMetodo As String, _
    ByVal pvsSentencia As String, _
    ByVal pvsDescripcion As String)

        _IdError = pvnIdError
        _Fecha = pvdFecha
        _FechaNum = pvsFechaNum
        _Clase = pvsClase
        _Proceso = pvsProceso
        _Metodo = pvsMetodo
        _Sentencia = pvsSentencia
        _descripcion = pvsDescripcion
    End Sub
    Public Sub New()
    End Sub
End Class
