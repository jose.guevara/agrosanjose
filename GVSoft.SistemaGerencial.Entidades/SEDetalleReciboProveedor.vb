﻿Public Class SEDetalleReciboProveedor

    Private _IdRegistro As Int64
    Public Property IdRegistro() As Int64
        Get
            Return (_IdRegistro)
        End Get
        Set(ByVal value As Int64)
            _IdRegistro = value
        End Set
    End Property
    Private _Numero As String
    Public Property Numero() As String
        Get
            Return (_Numero)
        End Get
        Set(ByVal value As String)
            _Numero = value
        End Set
    End Property
    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return (_Descripcion)
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property
    Private _Monto As Double
    Public Property Monto() As Double
        Get
            Return (_Monto)
        End Get
        Set(ByVal value As Double)
            _Monto = value
        End Set
    End Property
    Private _Interes As Double
    Public Property Interes() As Double
        Get
            Return (_Interes)
        End Get
        Set(ByVal value As Double)
            _Interes = value
        End Set
    End Property
    Private _Mantvlr As Double
    Public Property Mantvlr() As Double
        Get
            Return (_Mantvlr)
        End Get
        Set(ByVal value As Double)
            _Mantvlr = value
        End Set
    End Property
    Private _Pendiente As Double
    Public Property Pendiente() As Double
        Get
            Return (_Pendiente)
        End Get
        Set(ByVal value As Double)
            _Pendiente = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal objDetalleReciboProveedor As SEDetalleReciboProveedor)
        _IdRegistro = objDetalleReciboProveedor.IdRegistro
        _Numero = objDetalleReciboProveedor.Numero
        _Descripcion = objDetalleReciboProveedor.Descripcion
        _Monto = objDetalleReciboProveedor.Monto
        _Interes = objDetalleReciboProveedor.Interes
        _Mantvlr = objDetalleReciboProveedor.Mantvlr
        _Pendiente = objDetalleReciboProveedor.Pendiente
    End Sub
End Class
