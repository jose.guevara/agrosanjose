﻿Public Class SEDetalleProforma

    Private _IdProforma As Int64
    Public Property IdProforma() As Int64
        Get
            Return (_IdProforma)
        End Get
        Set(ByVal value As Int64)
            _IdProforma = value
        End Set
    End Property
    Private _IdProducto As Int16
    Public Property IdProducto() As Int16
        Get
            Return (_IdProducto)
        End Get
        Set(ByVal value As Int16)
            _IdProducto = value
        End Set
    End Property
    Private _Cantidad As Double
    Public Property Cantidad() As Double
        Get
            Return (_Cantidad)
        End Get
        Set(ByVal value As Double)
            _Cantidad = value
        End Set
    End Property
    Private _PrecioCosto As Double
    Public Property PrecioCosto() As Double
        Get
            Return (_PrecioCosto)
        End Get
        Set(ByVal value As Double)
            _PrecioCosto = value
        End Set
    End Property
    Private _Precio As Double
    Public Property Precio() As Double
        Get
            Return (_Precio)
        End Get
        Set(ByVal value As Double)
            _Precio = value
        End Set
    End Property
    Private _Valor As Double
    Public Property Valor() As Double
        Get
            Return (_Valor)
        End Get
        Set(ByVal value As Double)
            _Valor = value
        End Set
    End Property
    Private _Igv As Double
    Public Property Igv() As Double
        Get
            Return (_Igv)
        End Get
        Set(ByVal value As Double)
            _Igv = value
        End Set
    End Property
    Private _EsBonificado As Double
    Public Property EsBonificado() As Double
        Get
            Return (_EsBonificado)
        End Get
        Set(ByVal value As Double)
            _EsBonificado = value
        End Set
    End Property
    Public Sub New()

    End Sub

    Public Sub New(ByVal objDetalleProforma As SEDetalleProforma)

        _IdProforma = objDetalleProforma.IdProforma
        _IdProducto = objDetalleProforma.IdProducto
        _Cantidad = objDetalleProforma.Cantidad
        _PrecioCosto = objDetalleProforma.PrecioCosto
        _Precio = objDetalleProforma.Precio
        _Valor = objDetalleProforma.Valor
        _Igv = objDetalleProforma.Igv
        _EsBonificado = objDetalleProforma.EsBonificado
    End Sub

End Class
