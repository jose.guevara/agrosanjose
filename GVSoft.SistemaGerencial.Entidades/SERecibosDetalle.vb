﻿Public Class SERecibosDetalle

    '    Public Sub New(ByVal pvsNumeroRecibo As String, _
    'ByVal pvsNumeroFactura As String, _
    'ByVal pvnSaldoFactura As Double, _
    'ByVal pvnNuevoSaldoFactura As Double, _
    'ByVal pvnMontoAPagarFactura As Double, _
    'ByVal pvnInteres As Double, _
    'ByVal pvnMantenimientoAlValor As Double, _
    'ByVal pvnPorAplicar As Double, _
    'ByVal pvsDescripcionPago As String, _
    'ByVal pvsTipoCancelacion As String, _
    'ByVal pvsDescripcionRecibo As String)
    '        _NumeroRecibo = pvsNumeroRecibo
    '        _NumeroFactura = pvsNumeroFactura
    '        _SaldoFactura = pvnSaldoFactura
    '        _NuevoSaldoFactura = pvnNuevoSaldoFactura
    '        _MontoAPagarFactura = pvnMontoAPagarFactura
    '        _Interes = pvnInteres
    '        _MantenimientoAlValor = pvnMantenimientoAlValor
    '        _PorAplicar = pvnPorAplicar
    '        _DescripcionPago = pvsDescripcionPago
    '        _TipoCancelacion = pvsTipoCancelacion
    '        _DescripcionRecibo = pvsDescripcionRecibo
    '    End Sub

    Public Sub New()

    End Sub
    Public Sub New(ByVal objDetalleRecibos As SERecibosDetalle)
        _registro = objDetalleRecibos.registro
        _numero = objDetalleRecibos.numero
        _descripcion = objDetalleRecibos.descripcion
        _monto = objDetalleRecibos.monto
        _Interes = objDetalleRecibos.Interes
        _mantvlr = objDetalleRecibos.mantvlr
        _pendiente = objDetalleRecibos.pendiente

    End Sub
    Private _registro As Int64
    Public Property registro() As Int64
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Int64)
            _registro = value
        End Set
    End Property
    Private _numero As String
    Public Property numero() As String
        Get
            Return (_numero)
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property
    Private _descripcion As String
    Public Property descripcion() As String
        Get
            Return (_descripcion)
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
    Private _monto As Double
    Public Property monto() As Double
        Get
            Return (_monto)
        End Get
        Set(ByVal value As Double)
            _monto = value
        End Set
    End Property
    Private _interes As Double
    Public Property interes() As Double
        Get
            Return (_interes)
        End Get
        Set(ByVal value As Double)
            _interes = value
        End Set
    End Property
    Private _mantvlr As Double
    Public Property mantvlr() As Double
        Get
            Return (_mantvlr)
        End Get
        Set(ByVal value As Double)
            _mantvlr = value
        End Set
    End Property
    Private _pendiente As Double
    Public Property pendiente() As Double
        Get
            Return (_pendiente)
        End Get
        Set(ByVal value As Double)
            _pendiente = value
        End Set
    End Property
    Private _MontoDescuento As Double
    Public Property MontoDescuento() As Double
        Get
            Return (_MontoDescuento)
        End Get
        Set(ByVal value As Double)
            _MontoDescuento = value
        End Set
    End Property
    Private _PorcentajeDescuento As Double
    Public Property PorcentajeDescuento() As Double
        Get
            Return (_PorcentajeDescuento)
        End Get
        Set(ByVal value As Double)
            _PorcentajeDescuento = value
        End Set
    End Property
#Region "Codigo de Propiedades antes del cambio"
    'Private _NumeroRecibo As String
    'Public Property NumeroRecibo() As String
    '    Get
    '        Return (_NumeroRecibo)
    '    End Get
    '    Set(ByVal value As String)
    '        _NumeroRecibo = value
    '    End Set
    'End Property
    'Private _NumeroFactura As String
    'Public Property NumeroFactura() As String
    '    Get
    '        Return (_NumeroFactura)
    '    End Get
    '    Set(ByVal value As String)
    '        _NumeroFactura = value
    '    End Set
    'End Property
    'Private _SaldoFactura As Double
    'Public Property SaldoFactura() As Double
    '    Get
    '        Return (_SaldoFactura)
    '    End Get
    '    Set(ByVal value As Double)
    '        _SaldoFactura = value
    '    End Set
    'End Property
    'Private _NuevoSaldoFactura As Double
    'Public Property NuevoSaldoFactura() As Double
    '    Get
    '        Return (_NuevoSaldoFactura)
    '    End Get
    '    Set(ByVal value As Double)
    '        _NuevoSaldoFactura = value
    '    End Set
    'End Property
    'Private _MontoAPagarFactura As Double
    'Public Property MontoAPagarFactura() As Double
    '    Get
    '        Return (_MontoAPagarFactura)
    '    End Get
    '    Set(ByVal value As Double)
    '        _MontoAPagarFactura = value
    '    End Set
    'End Property
    'Private _MontoRecibo As Double
    'Public Property MontoRecibo() As Double
    '    Get
    '        Return (_MontoRecibo)
    '    End Get
    '    Set(ByVal value As Double)
    '        _MontoRecibo = value
    '    End Set
    'End Property

    'Private _Interes As Double
    'Public Property Interes() As Double
    '    Get
    '        Return (_Interes)
    '    End Get
    '    Set(ByVal value As Double)
    '        _Interes = value
    '    End Set
    'End Property
    'Private _MantenimientoAlValor As Double
    'Public Property MantenimientoAlValor() As Double
    '    Get
    '        Return (_MantenimientoAlValor)
    '    End Get
    '    Set(ByVal value As Double)
    '        _MantenimientoAlValor = value
    '    End Set
    'End Property
    'Private _PorAplicar As Double
    'Public Property PorAplicar() As Double
    '    Get
    '        Return (_PorAplicar)
    '    End Get
    '    Set(ByVal value As Double)
    '        _PorAplicar = value
    '    End Set
    'End Property
    'Private _DescripcionPago As String
    'Public Property DescripcionPago() As String
    '    Get
    '        Return (_DescripcionPago)
    '    End Get
    '    Set(ByVal value As String)
    '        _DescripcionPago = value
    '    End Set
    'End Property
    'Private _TipoCancelacion As String
    'Public Property TipoCancelacion() As String
    '    Get
    '        Return (_TipoCancelacion)
    '    End Get
    '    Set(ByVal value As String)
    '        _TipoCancelacion = value
    '    End Set
    'End Property
    'Private _DescripcionRecibo As String
    'Public Property DescripcionRecibo() As String
    '    Get
    '        Return (_DescripcionRecibo)
    '    End Get
    '    Set(ByVal value As String)
    '        _DescripcionRecibo = value
    '    End Set
    'End Property

#End Region

End Class
