﻿Public Class SEMaestroPedidoCompra

    Private _IdPedido As Long
    Public Property IdPedido As Long
        Get
            Return (_IdPedido)
        End Get
        Set(ByVal value As Long)
            _IdPedido = value
        End Set
    End Property
    Private _NumeroDocumento As String
    Public Property NumeroDocumento As String
        Get
            Return (_NumeroDocumento)
        End Get
        Set(ByVal value As String)
            _NumeroDocumento = value
        End Set
    End Property
    Private _FechaIngresoNum As Long
    Public Property FechaIngresoNum As Long
        Get
            Return (_FechaIngresoNum)
        End Get
        Set(ByVal value As Long)
            _FechaIngresoNum = value
        End Set
    End Property
    Private _FechaIngreso As String
    Public Property FechaIngreso As String
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As String)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoReg As DateTime
    Public Property FechaIngresoReg As DateTime
        Get
            Return (_FechaIngresoReg)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngresoReg = value
        End Set
    End Property
    Private _IdProveedor As Long
    Public Property IdProveedor As Long
        Get
            Return (_IdProveedor)
        End Get
        Set(ByVal value As Long)
            _IdProveedor = value
        End Set
    End Property
    Private _SubTotal As Double
    Public Property SubTotal As Double
        Get
            Return (_SubTotal)
        End Get
        Set(ByVal value As Double)
            _SubTotal = value
        End Set
    End Property
    Private _SubTotalCosto As Double
    Public Property SubTotalCosto As Double
        Get
            Return (_SubTotalCosto)
        End Get
        Set(ByVal value As Double)
            _SubTotalCosto = value
        End Set
    End Property
    Private _Impuesto As Double
    Public Property Impuesto As Double
        Get
            Return (_Impuesto)
        End Get
        Set(ByVal value As Double)
            _Impuesto = value
        End Set
    End Property
    Private _Retencion As Double
    Public Property Retencion As Double
        Get
            Return (_Retencion)
        End Get
        Set(ByVal value As Double)
            _Retencion = value
        End Set
    End Property
    Private _Descuento As Double
    Public Property Descuento As Double
        Get
            Return (_Descuento)
        End Get
        Set(ByVal value As Double)
            _Descuento = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _UserRegistro As Long
    Public Property UserRegistro As Long
        Get
            Return (_UserRegistro)
        End Get
        Set(ByVal value As Long)
            _UserRegistro = value
        End Set
    End Property
    Private _AgenregistroOrigen As Long
    Public Property AgenregistroOrigen As Long
        Get
            Return (_AgenregistroOrigen)
        End Get
        Set(ByVal value As Long)
            _AgenregistroOrigen = value
        End Set
    End Property
    Private _IdEstado As Long
    Public Property IdEstado As Long
        Get
            Return (_IdEstado)
        End Get
        Set(ByVal value As Long)
            _IdEstado = value
        End Set
    End Property
    Private _TipoCambio As Double
    Public Property TipoCambio As Double
        Get
            Return (_TipoCambio)
        End Get
        Set(ByVal value As Double)
            _TipoCambio = value
        End Set
    End Property
    Private _FechaAnulacion As DateTime
    Public Property FechaAnulacion As DateTime
        Get
            Return (_FechaAnulacion)
        End Get
        Set(ByVal value As DateTime)
            _FechaAnulacion = value
        End Set
    End Property
    Private _FechaAnulacionNum As Long
    Public Property FechaAnulacionNum As Long
        Get
            Return (_FechaAnulacionNum)
        End Get
        Set(ByVal value As Long)
            _FechaAnulacionNum = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal pvnIdPedido As Long, ByVal pvsNumeroDocumento As String, ByVal pvnFechaIngresoNum As Long,
                    ByVal pvsFechaIngreso As String, ByVal pvdFechaIngresoReg As DateTime, ByVal pvnIdProveedor As Long, ByVal pvnSubTotal As Double,
                    ByVal pvnSubTotalCosto As Double, ByVal pvnImpuesto As Double, ByVal pvnRetencion As Double, ByVal pvnDescuento As Double,
                    ByVal pvnTotal As Double, ByVal pvnUserRegistro As Long, ByVal pvnAgenregistroOrigen As Long, ByVal pvnIdEstado As Long,
                    ByVal pvnTipoCambio As Double, ByVal pvdFechaAnulacion As DateTime, ByVal pvnFechaAnulacionNum As Long)

        _IdPedido = pvnIdPedido
        _NumeroDocumento = pvsNumeroDocumento
        _FechaIngresoNum = pvnFechaIngresoNum
        _FechaIngreso = pvsFechaIngreso
        _FechaIngresoReg = pvdFechaIngresoReg
        _IdProveedor = pvnIdProveedor
        _SubTotal = pvnSubTotal
        _SubTotalCosto = pvnSubTotalCosto
        _Impuesto = pvnImpuesto
        _Retencion = pvnRetencion
        _Descuento = pvnDescuento
        _Total = pvnTotal
        _UserRegistro = pvnUserRegistro
        _AgenregistroOrigen = pvnAgenregistroOrigen
        _IdEstado = pvnIdEstado
        _TipoCambio = pvnTipoCambio
        _FechaAnulacion = pvdFechaAnulacion
        _FechaAnulacionNum = pvnFechaAnulacionNum
    End Sub


End Class
