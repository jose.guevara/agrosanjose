﻿Public Class SEDetPedidoCompra
    Private _IdPedido As Long
    Public Property IdPedido As Long
        Get
            Return (_IdPedido)
        End Get
        Set(ByVal value As Long)
            _IdPedido = value
        End Set
    End Property
    Private _IdDetalle As Long
    Public Property IdDetalle As Long
        Get
            Return (_IdDetalle)
        End Get
        Set(ByVal value As Long)
            _IdDetalle = value
        End Set
    End Property
    Private _EsBonificable As Integer
    Public Property EsBonificable As Integer
        Get
            Return (_EsBonificable)
        End Get
        Set(ByVal value As Integer)
            _EsBonificable = value
        End Set
    End Property
    Private _IdProducto As Long
    Public Property IdProducto As Long
        Get
            Return (_IdProducto)
        End Get
        Set(ByVal value As Long)
            _IdProducto = value
        End Set
    End Property
    Private _Cantidad As Double
    Public Property Cantidad As Double
        Get
            Return (_Cantidad)
        End Get
        Set(ByVal value As Double)
            _Cantidad = value
        End Set
    End Property
    Private _PrecioCosto As Double
    Public Property PrecioCosto As Double
        Get
            Return (_PrecioCosto)
        End Get
        Set(ByVal value As Double)
            _PrecioCosto = value
        End Set
    End Property
    Private _SubTotal As Double
    Public Property SubTotal As Double
        Get
            Return (_SubTotal)
        End Get
        Set(ByVal value As Double)
            _SubTotal = value
        End Set
    End Property
    Private _Impuesto As Double
    Public Property Impuesto As Double
        Get
            Return (_Impuesto)
        End Get
        Set(ByVal value As Double)
            _Impuesto = value
        End Set
    End Property
    Private _Descuento As Double
    Public Property Descuento As Double
        Get
            Return (_Descuento)
        End Get
        Set(ByVal value As Double)
            _Descuento = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _FechaIngreso As DateTime
    Public Property FechaIngreso As DateTime
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoNum As Long
    Public Property FechaIngresoNum As Long
        Get
            Return (_FechaIngresoNum)
        End Get
        Set(ByVal value As Long)
            _FechaIngresoNum = value
        End Set
    End Property

    Public Sub New()

    End Sub
    Public Sub New(ByVal pvnIdPedido As Long, ByVal pvnIdDetalle As Long, ByVal pvnEsBonificable As Integer, ByVal pvnIdProducto As Long, ByVal pvnCantidad As Double,
                    ByVal pvnPrecioCosto As Double, ByVal pvnSubTotal As Double, ByVal pvnImpuesto As Double, ByVal pvnDescuento As Double, ByVal pvnTotal As Double,
                    ByVal pvdFechaIngreso As DateTime, ByVal pvnFechaIngresoNum As Long)
        _IdPedido = pvnIdPedido
        _IdDetalle = pvnIdDetalle
        _EsBonificable = pvnEsBonificable
        _IdProducto = pvnIdProducto
        _Cantidad = pvnCantidad
        _PrecioCosto = pvnPrecioCosto
        _SubTotal = pvnSubTotal
        _Impuesto = pvnImpuesto
        _Descuento = pvnDescuento
        _Total = pvnTotal
        _FechaIngreso = pvdFechaIngreso
        _FechaIngresoNum = pvnFechaIngresoNum
    End Sub



End Class
