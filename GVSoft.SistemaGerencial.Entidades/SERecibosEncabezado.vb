﻿Public Class SERecibosEncabezado


    Public Sub New()

    End Sub

    Public Sub New(ByVal pvbigintRegistro As Integer,
                    ByVal pvsFecha As String,
                    ByVal pvnNumfecha As Int64,
                    ByVal pvnCliregistro As Int16,
                    ByVal pvnVendregistro As Int16,
                    ByVal pvsNumero As String,
                    ByVal pvnMonto As Double,
                    ByVal pvnInteres As Double,
                    ByVal pvnRetencion As Double,
                    ByVal pvnFormapago As Int16,
                    ByVal pvsNumchqtarj As String,
                    ByVal pvsNombrebanco As String,
                    ByVal pvsReciboprov As String,
                    ByVal pvtextDescripcion As String,
                    ByVal pvnUsrregistro As Int16,
                    ByVal pvnTiporecibo As Int16,
                    ByVal pvFecha_ing As DateTime,
                    ByVal pvnSaldofavor As Double,
                    ByVal pvnEstado As Int16,
                    ByVal pvtextDescr01 As String,
                    ByVal pvtextDescr02 As String,
                    ByVal pvtextDescr03 As String,
                    ByVal pvnAgenRegistro As Int64,
                    ByVal pvdFechaAnulado As DateTime,
                    ByVal pvnNumeroArqueo As Int64,
                    ByVal pvnIdMoneda As Integer,
                    ByVal pvnTipoCambio As Double,
                   ByVal pvnDescuento As Double
        )

        _registro = pvbigintRegistro
        _fecha = pvsFecha
        _numfecha = pvnNumfecha
        _cliregistro = pvnCliregistro
        _vendregistro = pvnVendregistro
        _numero = pvsNumero
        _monto = pvnMonto
        _interes = pvnInteres
        _retencion = pvnRetencion
        _formapago = pvnFormapago
        _numchqtarj = pvsNumchqtarj
        _nombrebanco = pvsNombrebanco
        _reciboprov = pvsReciboprov
        _descripcion = pvtextDescripcion
        _usrregistro = pvnUsrregistro
        _tiporecibo = pvnTiporecibo
        _fecha_ing = fecha_ing
        _saldofavor = pvnSaldofavor
        _estado = pvnEstado
        _descr01 = pvtextDescr01
        _descr02 = pvtextDescr02
        _descr03 = pvtextDescr03
        _agenRegistro = pvnAgenRegistro
        _FechaAnulado = pvdFechaAnulado
        _NumeroArqueo = pvnNumeroArqueo
        _IdMoneda = pvnIdMoneda
        _TipoCambio = pvnTipoCambio
        _Descuento = pvnDescuento
    End Sub

    Private _registro As Int64
    Public Property registro() As Int64
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Int64)
            _registro = value
        End Set
    End Property
    Private _fecha As String
    Public Property fecha() As String
        Get
            Return (_fecha)
        End Get
        Set(ByVal value As String)
            _fecha = value
        End Set
    End Property
    Private _numfecha As Int64
    Public Property numfecha() As Int64
        Get
            Return (_numfecha)
        End Get
        Set(ByVal value As Int64)
            _numfecha = value
        End Set
    End Property
    Private _cliregistro As Int16
    Public Property cliregistro() As Int16
        Get
            Return (_cliregistro)
        End Get
        Set(ByVal value As Int16)
            _cliregistro = value
        End Set
    End Property
    Private _vendregistro As Int16
    Public Property vendregistro() As Int16
        Get
            Return (_vendregistro)
        End Get
        Set(ByVal value As Int16)
            _vendregistro = value
        End Set
    End Property
    Private _numero As String
    Public Property numero() As String
        Get
            Return (_numero)
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property
    Private _monto As Double
    Public Property monto() As Double
        Get
            Return (_monto)
        End Get
        Set(ByVal value As Double)
            _monto = value
        End Set
    End Property
    Private _interes As Double
    Public Property interes() As Double
        Get
            Return (_interes)
        End Get
        Set(ByVal value As Double)
            _interes = value
        End Set
    End Property
    Private _retencion As Double
    Public Property retencion() As Double
        Get
            Return (_retencion)
        End Get
        Set(ByVal value As Double)
            _retencion = value
        End Set
    End Property
    Private _formapago As Int16
    Public Property formapago() As Int16
        Get
            Return (_formapago)
        End Get
        Set(ByVal value As Int16)
            _formapago = value
        End Set
    End Property
    Private _numchqtarj As String
    Public Property numchqtarj() As String
        Get
            Return (_numchqtarj)
        End Get
        Set(ByVal value As String)
            _numchqtarj = value
        End Set
    End Property
    Private _nombrebanco As String
    Public Property nombrebanco() As String
        Get
            Return (_nombrebanco)
        End Get
        Set(ByVal value As String)
            _nombrebanco = value
        End Set
    End Property
    Private _NoDeposito As String
    Public Property NoDeposito() As String
        Get
            Return (_NoDeposito)
        End Get
        Set(ByVal value As String)
            _NoDeposito = value
        End Set
    End Property
    Private _reciboprov As String
    Public Property reciboprov() As String
        Get
            Return (_reciboprov)
        End Get
        Set(ByVal value As String)
            _reciboprov = value
        End Set
    End Property
    Private _descripcion As String
    Public Property descripcion() As String
        Get
            Return (_descripcion)
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
    Private _usrregistro As Int16
    Public Property usrregistro() As Int16
        Get
            Return (_usrregistro)
        End Get
        Set(ByVal value As Int16)
            _usrregistro = value
        End Set
    End Property
    Private _tiporecibo As Int16
    Public Property tiporecibo() As Int16
        Get
            Return (_tiporecibo)
        End Get
        Set(ByVal value As Int16)
            _tiporecibo = value
        End Set
    End Property
    Private _fecha_ing As DateTime
    Public Property fecha_ing() As DateTime
        Get
            Return (_fecha_ing)
        End Get
        Set(ByVal value As DateTime)
            _fecha_ing = value
        End Set
    End Property
    Private _saldofavor As Double
    Public Property saldofavor() As Double
        Get
            Return (_saldofavor)
        End Get
        Set(ByVal value As Double)
            _saldofavor = value
        End Set
    End Property
    Private _estado As Int16
    Public Property estado() As Int16
        Get
            Return (_estado)
        End Get
        Set(ByVal value As Int16)
            _estado = value
        End Set
    End Property
    Private _descr01 As String
    Public Property descr01() As String
        Get
            Return (_descr01)
        End Get
        Set(ByVal value As String)
            _descr01 = value
        End Set
    End Property
    Private _descr02 As String
    Public Property descr02() As String
        Get
            Return (_descr02)
        End Get
        Set(ByVal value As String)
            _descr02 = value
        End Set
    End Property
    Private _descr03 As String
    Public Property descr03() As String
        Get
            Return (_descr03)
        End Get
        Set(ByVal value As String)
            _descr03 = value
        End Set
    End Property
    Private _agenRegistro As Int64
    Public Property agenRegistro() As Int64
        Get
            Return (_agenRegistro)
        End Get
        Set(ByVal value As Int64)
            _agenRegistro = value
        End Set
    End Property
    Private _FechaAnulado As DateTime
    Public Property FechaAnulado() As DateTime
        Get
            Return (_FechaAnulado)
        End Get
        Set(ByVal value As DateTime)
            _FechaAnulado = value
        End Set
    End Property
    Private _NumeroArqueo As Int64
    Public Property NumeroArqueo() As Int64
        Get
            Return (_NumeroArqueo)
        End Get
        Set(ByVal value As Int64)
            _NumeroArqueo = value
        End Set
    End Property

    Private _IdMoneda As Integer
    Public Property IdMoneda() As Integer
        Get
            Return (_IdMoneda)
        End Get
        Set(ByVal value As Integer)
            _IdMoneda = value
        End Set
    End Property

    Private _TipoCambio As Double
    Public Property TipoCambio() As Double
        Get
            Return (_TipoCambio)
        End Get
        Set(ByVal value As Double)
            _TipoCambio = value
        End Set
    End Property

    Private _Descuento As Double
    Public Property Descuento() As Double
        Get
            Return (_Descuento)
        End Get
        Set(ByVal value As Double)
            _Descuento = value
        End Set
    End Property

#Region "Propiedades de la entidad antes del cambio"
    'Private _FechaRecibo As String
    'Public Property FechaRecibo() As String
    '    Get
    '        Return (_FechaRecibo)
    '    End Get
    '    Set(ByVal value As String)
    '        _FechaRecibo = value
    '    End Set
    'End Property
    'Private _NumeroRecibo As String
    'Public Property NumeroRecibo() As String
    '    Get
    '        Return (_NumeroRecibo)
    '    End Get
    '    Set(ByVal value As String)
    '        _NumeroRecibo = value
    '    End Set
    'End Property
    'Private _Monto As Double
    'Public Property Monto() As Double
    '    Get
    '        Return (_Monto)
    '    End Get
    '    Set(ByVal value As Double)
    '        _Monto = value
    '    End Set
    'End Property
    'Private _Nombre As String
    'Public Property Nombre() As String
    '    Get
    '        Return (_Nombre)
    '    End Get
    '    Set(ByVal value As String)
    '        _Nombre = value
    '    End Set
    'End Property
    'Private _Codigo As String
    'Public Property Codigo() As String
    '    Get
    '        Return (_Codigo)
    '    End Get
    '    Set(ByVal value As String)
    '        _Codigo = value
    '    End Set
    'End Property
    'Private _CuentaContable As String
    'Public Property CuentaContable() As String
    '    Get
    '        Return (_CuentaContable)
    '    End Get
    '    Set(ByVal value As String)
    '        _CuentaContable = value
    '    End Set
    'End Property
    'Private _Descripcion As String
    'Public Property Descripcion() As String
    '    Get
    '        Return (_Descripcion)
    '    End Get
    '    Set(ByVal value As String)
    '        _Descripcion = value
    '    End Set
    'End Property
    'Private _Recibido As String
    'Public Property Recibido() As String
    '    Get
    '        Return (_Recibido)
    '    End Get
    '    Set(ByVal value As String)
    '        _Recibido = value
    '    End Set
    'End Property
    'Private _NumeroTarjeta As String
    'Public Property NumeroTarjeta() As String
    '    Get
    '        Return (_NumeroTarjeta)
    '    End Get
    '    Set(ByVal value As String)
    '        _NumeroTarjeta = value
    '    End Set
    'End Property
    'Private _Banco As String
    'Public Property Banco() As String
    '    Get
    '        Return (_Banco)
    '    End Get
    '    Set(ByVal value As String)
    '        _Banco = value
    '    End Set
    'End Property
    'Private _Anio As String
    'Public Property Anio() As Integer
    '    Get
    '        Return (_Anio)
    '    End Get
    '    Set(ByVal value As Integer)
    '        _Anio = value
    '    End Set
    'End Property
    'Private _Anulado As String
    'Public Property Anulado() As String
    '    Get
    '        Return (_Anulado)
    '    End Get
    '    Set(ByVal value As String)
    '        _Anulado = value
    '    End Set
    'End Property

#End Region

End Class
