﻿Public Class SEInformacionMonetaria
    Private _IdCuadre As Int64
    Public Property IdCuadre() As Int64
        Get
            Return (_IdCuadre)
        End Get
        Set(ByVal value As Int64)
            _IdCuadre = value
        End Set
    End Property
    Private _IdMoneda As String
    Public Property IdMoneda() As String
        Get
            Return (_IdMoneda)
        End Get
        Set(ByVal value As String)
            _IdMoneda = value
        End Set
    End Property
    Private _IdSucursal As String
    Public Property IdSucursal() As String
        Get
            Return (_IdSucursal)
        End Get
        Set(ByVal value As String)
            _IdSucursal = value
        End Set
    End Property
    Private _DenominacionBillete As Double
    Public Property DenominacionBillete() As Double
        Get
            Return (_DenominacionBillete)
        End Get
        Set(ByVal value As Double)
            _DenominacionBillete = value
        End Set
    End Property
    Private _Cantidad As Int64
    Public Property Cantidad() As Int64
        Get
            Return (_Cantidad)
        End Get
        Set(ByVal value As Int64)
            _Cantidad = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total() As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _FechaIngreso As DateTime
    Public Property FechaIngreso() As DateTime
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoNum As String
    Public Property FechaIngresoNum() As String
        Get
            Return (_FechaIngresoNum)
        End Get
        Set(ByVal value As String)
            _FechaIngresoNum = value
        End Set
    End Property
    Public Sub New()

    End Sub

    Public Sub New(ByVal objInformacionMonetaria As SEInformacionMonetaria)
        _Cantidad = objInformacionMonetaria.Cantidad
        _DenominacionBillete = objInformacionMonetaria.DenominacionBillete
        _IdMoneda = objInformacionMonetaria.IdMoneda
        _Total = objInformacionMonetaria.Total
    End Sub
End Class
