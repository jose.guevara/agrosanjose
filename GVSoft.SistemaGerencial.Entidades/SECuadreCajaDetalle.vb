﻿Public Class SECuadreCajaDetalle
    Private _IdCuadre As Int64
    Public Property IdCuadre() As Int64
        Get
            Return (_IdCuadre)
        End Get
        Set(ByVal value As Int64)
            _IdCuadre = value
        End Set
    End Property
    Private _IdMoneda As String
    Public Property IdMoneda() As String
        Get
            Return (_IdMoneda)
        End Get
        Set(ByVal value As String)
            _IdMoneda = value
        End Set
    End Property
    Private _NumeroDocumento As String
    Public Property NumeroDocumento() As String
        Get
            Return (_NumeroDocumento)
        End Get
        Set(ByVal value As String)
            _NumeroDocumento = value
        End Set
    End Property
    Private _MontoTotalDocumento As Double
    Public Property MontoTotalDocumento() As Double
        Get
            Return (_MontoTotalDocumento)
        End Get
        Set(ByVal value As Double)
            _MontoTotalDocumento = value
        End Set
    End Property
    Private _TipoDocumento As String
    Public Property TipoDocumento() As String
        Get
            Return (_TipoDocumento)
        End Get
        Set(ByVal value As String)
            _TipoDocumento = value
        End Set
    End Property
    Private _IdSucursal As String
    Public Property IdSucursal() As String
        Get
            Return (_IdSucursal)
        End Get
        Set(ByVal value As String)
            _IdSucursal = value
        End Set
    End Property
    Private _DescripcionTipoDocumento As String
    Public Property DescripcionTipoDocumento() As String
        Get
            Return (_DescripcionTipoDocumento)
        End Get
        Set(ByVal value As String)
            _DescripcionTipoDocumento = value
        End Set
    End Property
    Private _FechaDocumento As String
    Public Property FechaDocumento() As String
        Get
            Return (_FechaDocumento)
        End Get
        Set(ByVal value As String)
            _FechaDocumento = value
        End Set
    End Property
    Private _FechaDocumentoNum As String
    Public Property FechaDocumentoNum() As String
        Get
            Return (_FechaDocumentoNum)
        End Get
        Set(ByVal value As String)
            _FechaDocumentoNum = value
        End Set
    End Property
    Private _FechaIngreso As String
    Public Property FechaIngreso() As String
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As String)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoNum As String
    Public Property FechaIngresoNum() As String
        Get
            Return (_FechaIngresoNum)
        End Get
        Set(ByVal value As String)
            _FechaIngresoNum = value
        End Set
    End Property
    Private _IdEstadoDocumento As Int64
    Public Property IdEstadoDocumento() As Int64
        Get
            Return (_IdEstadoDocumento)
        End Get
        Set(ByVal value As Int64)
            _IdEstadoDocumento = value
        End Set
    End Property
    Private _IdUsuarioDocumento As Int64
    Public Property IdUsuarioDocumento() As Int64
        Get
            Return (_IdUsuarioDocumento)
        End Get
        Set(ByVal value As Int64)
            _IdUsuarioDocumento = value
        End Set
    End Property
    Public Sub New()

    End Sub

    Public Sub New(ByVal objDetalleCuadreCaja As SECuadreCajaDetalle)
        _IdCuadre = objDetalleCuadreCaja.IdCuadre
        _IdMoneda = objDetalleCuadreCaja.IdMoneda
        _NumeroDocumento = objDetalleCuadreCaja.NumeroDocumento
        _MontoTotalDocumento = objDetalleCuadreCaja.MontoTotalDocumento
        _FechaDocumento = objDetalleCuadreCaja.FechaDocumento
        _FechaDocumentoNum = objDetalleCuadreCaja.FechaDocumentoNum
        _FechaIngreso = objDetalleCuadreCaja.FechaIngreso
        _FechaIngresoNum = objDetalleCuadreCaja.FechaIngresoNum
        _IdUsuarioDocumento = objDetalleCuadreCaja.IdUsuarioDocumento
        _IdEstadoDocumento = objDetalleCuadreCaja.IdEstadoDocumento
        _TipoDocumento = objDetalleCuadreCaja.TipoDocumento
        _DescripcionTipoDocumento = objDetalleCuadreCaja.DescripcionTipoDocumento
    End Sub
End Class
