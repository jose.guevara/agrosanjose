﻿Public Class SEOpcionxUsuario
    Private _IdUsuario As Int64
    Public Property IdUsuario() As Int64
        Get
            Return (_IdUsuario)
        End Get
        Set(ByVal value As Int64)
            _IdUsuario = value
        End Set
    End Property
    Private _IdOpcionMenu As Int64
    Public Property IdOpcionMenu() As Int64
        Get
            Return (_IdOpcionMenu)
        End Get
        Set(ByVal value As Int64)
            _IdOpcionMenu = value
        End Set
    End Property
    Public Sub New(ByVal pvnIdUsuario As Int64, _
ByVal pvnIdOpcionMenu As Int64)
        _IdUsuario = pvnIdUsuario
        _IdOpcionMenu = pvnIdOpcionMenu
    End Sub
End Class
