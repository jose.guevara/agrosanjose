﻿Public Class SEMovimientoInventarioEnc
    Private _registro As Int64
    Public Property registro() As Int64
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Int64)
            _registro = value
        End Set
    End Property
    Private _numero As String
    Public Property numero() As String
        Get
            Return (_numero)
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property
    Private _IdMovimiento As String
    Public Property IdMovimiento() As String
        Get
            Return (_IdMovimiento)
        End Get
        Set(ByVal value As String)
            _IdMovimiento = value
        End Set
    End Property
    Private _numfechaing As Int64
    Public Property numfechaing() As Int64
        Get
            Return (_numfechaing)
        End Get
        Set(ByVal value As Int64)
            _numfechaing = value
        End Set
    End Property
    Private _fechaingreso As String
    Public Property fechaingreso() As String
        Get
            Return (_fechaingreso)
        End Get
        Set(ByVal value As String)
            _fechaingreso = value
        End Set
    End Property
    Private _FechaIngresoReg As DateTime
    Public Property FechaIngresoReg() As DateTime
        Get
            Return (_FechaIngresoReg)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngresoReg = value
        End Set
    End Property
    Private _vendregistro As Int16
    Public Property vendregistro() As Int16
        Get
            Return (_vendregistro)
        End Get
        Set(ByVal value As Int16)
            _vendregistro = value
        End Set
    End Property
    Private _cliregistro As Int16
    Public Property cliregistro() As Int16
        Get
            Return (_cliregistro)
        End Get
        Set(ByVal value As Int16)
            _cliregistro = value
        End Set
    End Property
    Private _clinombre As String
    Public Property clinombre() As String
        Get
            Return (_clinombre)
        End Get
        Set(ByVal value As String)
            _clinombre = value
        End Set
    End Property
    Private _subtotal As Double
    Public Property subtotal() As Double
        Get
            Return (_subtotal)
        End Get
        Set(ByVal value As Double)
            _subtotal = value
        End Set
    End Property
    Private _SubTotalCosto As Double
    Public Property SubTotalCosto() As Double
        Get
            Return (_SubTotalCosto)
        End Get
        Set(ByVal value As Double)
            _SubTotalCosto = value
        End Set
    End Property
    Private _impuesto As Double
    Public Property impuesto() As Double
        Get
            Return (_impuesto)
        End Get
        Set(ByVal value As Double)
            _impuesto = value
        End Set
    End Property
    Private _retencion As Double
    Public Property retencion() As Double
        Get
            Return (_retencion)
        End Get
        Set(ByVal value As Double)
            _retencion = value
        End Set
    End Property
    Private _total As Double
    Public Property total() As Double
        Get
            Return (_total)
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property
    Private _userregistro As Int16
    Public Property userregistro() As Int16
        Get
            Return (_userregistro)
        End Get
        Set(ByVal value As Int16)
            _userregistro = value
        End Set
    End Property
    Private _agenregistroOrigen As Int16
    Public Property agenregistroOrigen() As Int16
        Get
            Return (_agenregistroOrigen)
        End Get
        Set(ByVal value As Int16)
            _agenregistroOrigen = value
        End Set
    End Property
    Private _agenregistroDestino As Int16
    Public Property agenregistroDestino() As Int16
        Get
            Return (_agenregistroDestino)
        End Get
        Set(ByVal value As Int16)
            _agenregistroDestino = value
        End Set
    End Property
    Private _status As Int16
    Public Property status() As Int16
        Get
            Return (_status)
        End Get
        Set(ByVal value As Int16)
            _status = value
        End Set
    End Property
    Private _impreso As Int16
    Public Property impreso() As Int16
        Get
            Return (_impreso)
        End Get
        Set(ByVal value As Int16)
            _impreso = value
        End Set
    End Property
    Private _deptregistro As Int16
    Public Property deptregistro() As Int16
        Get
            Return (_deptregistro)
        End Get
        Set(ByVal value As Int16)
            _deptregistro = value
        End Set
    End Property
    Private _negregistro As Int16
    Public Property negregistro() As Int16
        Get
            Return (_negregistro)
        End Get
        Set(ByVal value As Int16)
            _negregistro = value
        End Set
    End Property
    Private _tipofactura As Int16
    Public Property tipofactura() As Int16
        Get
            Return (_tipofactura)
        End Get
        Set(ByVal value As Int16)
            _tipofactura = value
        End Set
    End Property
    Private _diascredito As Int16
    Public Property diascredito() As Int16
        Get
            Return (_diascredito)
        End Get
        Set(ByVal value As Int16)
            _diascredito = value
        End Set
    End Property
    Private _tipocambio As Double
    Public Property tipocambio() As Double
        Get
            Return (_tipocambio)
        End Get
        Set(ByVal value As Double)
            _tipocambio = value
        End Set
    End Property
    Private _numfechaanul As Int64
    Public Property numfechaanul() As Int64
        Get
            Return (_numfechaanul)
        End Get
        Set(ByVal value As Int64)
            _numfechaanul = value
        End Set
    End Property

    Public Sub New(ByVal pvbigintRegistro As Integer, _
                    ByVal pvsNumero As String, _
                    ByVal pvsIdMovimiento As String, _
                    ByVal pvnNumfechaing As Int64, _
                    ByVal pvsFechaingreso As String, _
                    ByVal pvdFechaIngresoReg As DateTime, _
                    ByVal pvnVendregistro As Int16, _
                    ByVal pvnCliregistro As Int16, _
                    ByVal pvsClinombre As String, _
                    ByVal pvnSubtotal As Double, _
                    ByVal pvnSubTotalCosto As Double, _
                    ByVal pvnImpuesto As Double, _
                    ByVal pvnRetencion As Double, _
                    ByVal pvnTotal As Double, _
                    ByVal pvnUserregistro As Int16, _
                    ByVal pvnAgenregistroOrigen As Int16, _
                    ByVal pvnAgenregistroDestino As Int16, _
                    ByVal pvnStatus As Int16, _
                    ByVal pvnImpreso As Int16, _
                    ByVal pvnDeptregistro As Int16, _
                    ByVal pvnNegregistro As Int16, _
                    ByVal pvnTipofactura As Int16, _
                    ByVal pvnDiascredito As Int16, _
                    ByVal pvnTipocambio As Double, _
                    ByVal pvnNumfechaanul As Int64)

        _registro = pvbigintRegistro
        _numero = pvsNumero
        _IdMovimiento = pvsIdMovimiento
        _numfechaing = pvnNumfechaing
        _fechaingreso = pvsFechaingreso
        _FechaIngresoReg = pvdFechaIngresoReg
        _vendregistro = pvnVendregistro
        _cliregistro = pvnCliregistro
        _clinombre = pvsClinombre
        _subtotal = pvnSubtotal
        _SubTotalCosto = pvnSubTotalCosto
        _impuesto = pvnImpuesto
        _retencion = pvnRetencion
        _total = pvnTotal
        _userregistro = pvnUserregistro
        _agenregistroOrigen = pvnAgenregistroOrigen
        _agenregistroDestino = pvnAgenregistroDestino
        _status = pvnStatus
        _impreso = pvnImpreso
        _deptregistro = pvnDeptregistro
        _negregistro = pvnNegregistro
        _tipofactura = pvnTipofactura
        _diascredito = pvnDiascredito
        _tipocambio = pvnTipocambio
        _numfechaanul = pvnNumfechaanul

    End Sub

    Public Sub New()

    End Sub

End Class
