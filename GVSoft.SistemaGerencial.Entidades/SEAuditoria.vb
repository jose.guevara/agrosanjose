﻿Public Class SEAuditoria
    Private _SentenciaSql As String = String.Empty
    Public Property SentenciaSql As String
        Get
            Return (_SentenciaSql)
        End Get
        Set(ByVal value As String)
            _SentenciaSql = value
        End Set
    End Property
    Private _NombreClase As String = String.Empty
    Public Property NombreClase As String
        Get
            Return (_NombreClase)
        End Get
        Set(ByVal value As String)
            _NombreClase = value
        End Set
    End Property

    Private _NombreMetodo As String = String.Empty
    Public Property NombreMetodo As String
        Get
            Return (_NombreMetodo)
        End Get
        Set(ByVal value As String)
            _NombreMetodo = value
        End Set
    End Property


    Public Sub New()

    End Sub
    Public Sub New(ByVal pvsSentenciaSql As String, ByVal pvsNombreClase As String, ByVal pvsNombreMetodo As String)
        _SentenciaSql = pvsSentenciaSql
        _NombreClase = pvsNombreClase
        _NombreMetodo = pvsNombreMetodo
    End Sub

End Class
