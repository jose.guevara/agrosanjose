﻿Public Class SEMaestroProforma

    Private _IdProforma As Int64
    Public Property IdProforma() As Int64
        Get
            Return (_IdProforma)
        End Get
        Set(ByVal value As Int64)
            _IdProforma = value
        End Set
    End Property
    Private _NumeroProforma As String
    Public Property NumeroProforma() As String
        Get
            Return (_NumeroProforma)
        End Get
        Set(ByVal value As String)
            _NumeroProforma = value
        End Set
    End Property
    Private _NumFechaIngreso As Int64
    Public Property NumFechaIngreso() As Int64
        Get
            Return (_NumFechaIngreso)
        End Get
        Set(ByVal value As Int64)
            _NumFechaIngreso = value
        End Set
    End Property
    Private _FechaIngreso As String
    Public Property FechaIngreso() As String
        Get
            Return (_FechaIngreso)
        End Get
        Set(ByVal value As String)
            _FechaIngreso = value
        End Set
    End Property
    Private _FechaIngresoReg As DateTime
    Public Property FechaIngresoReg() As DateTime
        Get
            Return (_FechaIngresoReg)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngresoReg = value
        End Set
    End Property
    Private _IdVendedor As Int16
    Public Property IdVendedor() As Int16
        Get
            Return (_IdVendedor)
        End Get
        Set(ByVal value As Int16)
            _IdVendedor = value
        End Set
    End Property
    Private _IdCliente As Int16
    Public Property IdCliente() As Int16
        Get
            Return (_IdCliente)
        End Get
        Set(ByVal value As Int16)
            _IdCliente = value
        End Set
    End Property
    Private _Cliente As String
    Public Property Cliente() As String
        Get
            Return (_Cliente)
        End Get
        Set(ByVal value As String)
            _Cliente = value
        End Set
    End Property
    Private _Direccion As String
    Public Property Direccion() As String
        Get
            Return (_Direccion)
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Private _SubTotal As Double
    Public Property SubTotal() As Double
        Get
            Return (_SubTotal)
        End Get
        Set(ByVal value As Double)
            _SubTotal = value
        End Set
    End Property
    Private _SubTotalCosto As Double
    Public Property SubTotalCosto() As Double
        Get
            Return (_SubTotalCosto)
        End Get
        Set(ByVal value As Double)
            _SubTotalCosto = value
        End Set
    End Property
    Private _Impuesto As Double
    Public Property Impuesto() As Double
        Get
            Return (_Impuesto)
        End Get
        Set(ByVal value As Double)
            _Impuesto = value
        End Set
    End Property
    Private _Retencion As Double
    Public Property Retencion() As Double
        Get
            Return (_Retencion)
        End Get
        Set(ByVal value As Double)
            _Retencion = value
        End Set
    End Property
    Private _Total As Double
    Public Property Total() As Double
        Get
            Return (_Total)
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _IdUsuario As Int16
    Public Property IdUsuario() As Int16
        Get
            Return (_IdUsuario)
        End Get
        Set(ByVal value As Int16)
            _IdUsuario = value
        End Set
    End Property
    Private _IdSucursal As Int16
    Public Property IdSucursal() As Int16
        Get
            Return (_IdSucursal)
        End Get
        Set(ByVal value As Int16)
            _IdSucursal = value
        End Set
    End Property
    Private _IdEstado As Int16
    Public Property IdEstado() As Int16
        Get
            Return (_IdEstado)
        End Get
        Set(ByVal value As Int16)
            _IdEstado = value
        End Set
    End Property
    Private _Impreso As Int16
    Public Property Impreso() As Int16
        Get
            Return (_Impreso)
        End Get
        Set(ByVal value As Int16)
            _Impreso = value
        End Set
    End Property
    Private _IdDepartamento As Int16
    Public Property IdDepartamento() As Int16
        Get
            Return (_IdDepartamento)
        End Get
        Set(ByVal value As Int16)
            _IdDepartamento = value
        End Set
    End Property
    Private _IdNegocio As Int16
    Public Property IdNegocio() As Int16
        Get
            Return (_IdNegocio)
        End Get
        Set(ByVal value As Int16)
            _IdNegocio = value
        End Set
    End Property
    Private _TipoFactura As Int16
    Public Property TipoFactura() As Int16
        Get
            Return (_TipoFactura)
        End Get
        Set(ByVal value As Int16)
            _TipoFactura = value
        End Set
    End Property
    Private _DiasCredito As Int16
    Public Property DiasCredito() As Int16
        Get
            Return (_DiasCredito)
        End Get
        Set(ByVal value As Int16)
            _DiasCredito = value
        End Set
    End Property
    Private _TipoCambio As Double
    Public Property TipoCambio() As Double
        Get
            Return (_TipoCambio)
        End Get
        Set(ByVal value As Double)
            _TipoCambio = value
        End Set
    End Property
    Private _NumFechaAnulado As Int64
    Public Property NumFechaAnulado() As Int64
        Get
            Return (_NumFechaAnulado)
        End Get
        Set(ByVal value As Int64)
            _NumFechaAnulado = value
        End Set
    End Property
    Private _NumeroArqueo As Int64
    Public Property NumeroArqueo() As Int64
        Get
            Return (_NumeroArqueo)
        End Get
        Set(ByVal value As Int64)
            _NumeroArqueo = value
        End Set
    End Property

    Public Sub New(ByVal pvbigintIdProforma As Integer, _
                    ByVal pvsNumeroProforma As String, _
                    ByVal pvnNumFechaIngreso As Int64, _
                    ByVal pvsFechaIngreso As String, _
                    ByVal pvdFechaIngresoReg As DateTime, _
                    ByVal pvnIdVendedor As Int16, _
                    ByVal pvnIdCliente As Int16, _
                    ByVal pvsCliente As String, _
                    ByVal pvnSubTotal As Double, _
                    ByVal pvnSubTotalCosto As Double, _
                    ByVal pvnImpuesto As Double, _
                    ByVal pvnRetencion As Double, _
                    ByVal pvnTotal As Double, _
                    ByVal pvnIdUsuario As Int16, _
                    ByVal pvnIdSucursal As Int16, _
                    ByVal pvnIdEstado As Int16, _
                    ByVal pvnImpreso As Int16, _
                    ByVal pvnIdDepartamento As Int16, _
                    ByVal pvnIdNegocio As Int16, _
                    ByVal pvnTipoFactura As Int16, _
                    ByVal pvnDiasCredito As Int16, _
                    ByVal pvnTipoCambio As Double, _
                    ByVal pvnNumFechaAnulado As Int64, _
                    ByVal pvnNumeroArqueo As Int64)

        _IdProforma = pvbigintIdProforma
        _NumeroProforma = pvsNumeroProforma
        _NumFechaIngreso = pvnNumFechaIngreso
        _FechaIngreso = pvsFechaIngreso
        _FechaIngresoReg = pvdFechaIngresoReg
        _IdVendedor = pvnIdVendedor
        _IdCliente = pvnIdCliente
        _Cliente = pvsCliente
        _SubTotal = pvnSubTotal
        _SubTotalCosto = pvnSubTotalCosto
        _Impuesto = pvnImpuesto
        _Retencion = pvnRetencion
        _Total = pvnTotal
        _IdUsuario = pvnIdUsuario
        _IdSucursal = pvnIdSucursal
        _IdEstado = pvnIdEstado
        _Impreso = pvnImpreso
        _IdDepartamento = pvnIdDepartamento
        _IdNegocio = pvnIdNegocio
        _TipoFactura = pvnTipoFactura
        _DiasCredito = pvnDiasCredito
        _TipoCambio = pvnTipoCambio
        _NumFechaAnulado = pvnNumFechaAnulado
        _NumeroArqueo = pvnNumeroArqueo

    End Sub

    Public Sub New()

    End Sub


End Class
