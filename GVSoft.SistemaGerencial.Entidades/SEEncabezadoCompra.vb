﻿Public Class SEEncabezadoCompra
    Private _registro As Int64
    Public Property registro() As Int64
        Get
            Return (_registro)
        End Get
        Set(ByVal value As Int64)
            _registro = value
        End Set
    End Property
    Private _numero As String
    Public Property numero() As String
        Get
            Return (_numero)
        End Get
        Set(ByVal value As String)
            _numero = value
        End Set
    End Property
    Private _numfechaing As Int64
    Public Property numfechaing() As Int64
        Get
            Return (_numfechaing)
        End Get
        Set(ByVal value As Int64)
            _numfechaing = value
        End Set
    End Property
    Private _fechaingreso As String
    Public Property fechaingreso() As String
        Get
            Return (_fechaingreso)
        End Get
        Set(ByVal value As String)
            _fechaingreso = value
        End Set
    End Property
    Private _FechaIngresoReg As DateTime
    Public Property FechaIngresoReg() As DateTime
        Get
            Return (_FechaIngresoReg)
        End Get
        Set(ByVal value As DateTime)
            _FechaIngresoReg = value
        End Set
    End Property
    Private _vendregistro As Int16
    Public Property vendregistro() As Int16
        Get
            Return (_vendregistro)
        End Get
        Set(ByVal value As Int16)
            _vendregistro = value
        End Set
    End Property
    Private _cliregistro As Int16
    Public Property cliregistro() As Int16
        Get
            Return (_cliregistro)
        End Get
        Set(ByVal value As Int16)
            _cliregistro = value
        End Set
    End Property
    Private _clinombre As String
    Public Property clinombre() As String
        Get
            Return (_clinombre)
        End Get
        Set(ByVal value As String)
            _clinombre = value
        End Set
    End Property
    Private _subtotal As Double
    Public Property subtotal() As Double
        Get
            Return (_subtotal)
        End Get
        Set(ByVal value As Double)
            _subtotal = value
        End Set
    End Property
    Private _SubTotalCosto As Double
    Public Property SubTotalCosto() As Double
        Get
            Return (_SubTotalCosto)
        End Get
        Set(ByVal value As Double)
            _SubTotalCosto = value
        End Set
    End Property
    Private _impuesto As Double
    Public Property impuesto() As Double
        Get
            Return (_impuesto)
        End Get
        Set(ByVal value As Double)
            _impuesto = value
        End Set
    End Property
    Private _retencion As Double
    Public Property retencion() As Double
        Get
            Return (_retencion)
        End Get
        Set(ByVal value As Double)
            _retencion = value
        End Set
    End Property
    Private _total As Double
    Public Property total() As Double
        Get
            Return (_total)
        End Get
        Set(ByVal value As Double)
            _total = value
        End Set
    End Property
    Private _userregistro As Int16
    Public Property userregistro() As Int16
        Get
            Return (_userregistro)
        End Get
        Set(ByVal value As Int16)
            _userregistro = value
        End Set
    End Property
    Private _agenregistro As Int16
    Public Property agenregistro() As Int16
        Get
            Return (_agenregistro)
        End Get
        Set(ByVal value As Int16)
            _agenregistro = value
        End Set
    End Property
    Private _status As Int16
    Public Property status() As Int16
        Get
            Return (_status)
        End Get
        Set(ByVal value As Int16)
            _status = value
        End Set
    End Property
    Private _impreso As Int16
    Public Property impreso() As Int16
        Get
            Return (_impreso)
        End Get
        Set(ByVal value As Int16)
            _impreso = value
        End Set
    End Property
    Private _deptregistro As Int16
    Public Property deptregistro() As Int16
        Get
            Return (_deptregistro)
        End Get
        Set(ByVal value As Int16)
            _deptregistro = value
        End Set
    End Property
    Private _negregistro As Int16
    Public Property negregistro() As Int16
        Get
            Return (_negregistro)
        End Get
        Set(ByVal value As Int16)
            _negregistro = value
        End Set
    End Property
    Private _tipoCompra As Int16
    Public Property tipoCompra() As Int16
        Get
            Return (_tipoCompra)
        End Get
        Set(ByVal value As Int16)
            _tipoCompra = value
        End Set
    End Property
    Private _diascredito As Int16
    Public Property diascredito() As Int16
        Get
            Return (_diascredito)
        End Get
        Set(ByVal value As Int16)
            _diascredito = value
        End Set
    End Property
    Private _tipocambio As Double
    Public Property tipocambio() As Double
        Get
            Return (_tipocambio)
        End Get
        Set(ByVal value As Double)
            _tipocambio = value
        End Set
    End Property
    Private _numfechaanul As Int64
    Public Property numfechaanul() As Int64
        Get
            Return (_numfechaanul)
        End Get
        Set(ByVal value As Int64)
            _numfechaanul = value
        End Set
    End Property
    Private _NumeroArqueo As Int64
    Public Property NumeroArqueo() As Int64
        Get
            Return (_NumeroArqueo)
        End Get
        Set(ByVal value As Int64)
            _NumeroArqueo = value
        End Set
    End Property

    Private _IdPedido As Int64
    Public Property IdPedido() As Int64
        Get
            Return (_IdPedido)
        End Get
        Set(ByVal value As Int64)
            _IdPedido = value
        End Set
    End Property

    Private _Direccion As String
    Public Property Direccion() As String
        Get
            Return (_Direccion)
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property
    Private _Ruc As String
    Public Property Ruc() As String
        Get
            Return (_Ruc)
        End Get
        Set(ByVal value As String)
            _Ruc = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal pvbigintRegistro As Int64, ByVal pvsNumero As String,
                    ByVal pvnNumfechaing As Int64, ByVal pvsFechaingreso As String,
                    ByVal pvdFechaIngresoReg As DateTime, ByVal pvnVendregistro As Int16,
                    ByVal pvnCliregistro As Int16, ByVal pvsClinombre As String,
                    ByVal pvnSubtotal As Double, ByVal pvnSubTotalCosto As Double,
                    ByVal pvnImpuesto As Double, ByVal pvnRetencion As Double,
                    ByVal pvnTotal As Double, ByVal pvnUserregistro As Int16,
                    ByVal pvnAgenregistro As Int16, ByVal pvnStatus As Int16,
                    ByVal pvnImpreso As Int16, ByVal pvnDeptregistro As Int16,
                    ByVal pvnNegregistro As Int16, ByVal pvnTipoCompra As Int16,
                    ByVal pvnDiascredito As Int16, ByVal pvnTipocambio As Double,
                    ByVal pvnNumfechaanul As Int64, ByVal pvnNumeroArqueo As Int64,
                    ByVal pvsDireccion As String, ByVal pvsRuc As String, ByVal pvnIdPedido As Integer)

        _registro = pvbigintRegistro
        _numero = pvsNumero
        _numfechaing = pvnNumfechaing
        _fechaingreso = pvsFechaingreso
        _FechaIngresoReg = pvdFechaIngresoReg
        _vendregistro = pvnVendregistro
        _cliregistro = pvnCliregistro
        _clinombre = pvsClinombre
        _subtotal = pvnSubtotal
        _SubTotalCosto = pvnSubTotalCosto
        _impuesto = pvnImpuesto
        _retencion = pvnRetencion
        _total = pvnTotal
        _userregistro = pvnUserregistro
        _agenregistro = pvnAgenregistro
        _status = pvnStatus
        _impreso = pvnImpreso
        _deptregistro = pvnDeptregistro
        _negregistro = pvnNegregistro
        _tipoCompra = pvnTipoCompra
        _diascredito = pvnDiascredito
        _tipocambio = pvnTipocambio
        _numfechaanul = pvnNumfechaanul
        _NumeroArqueo = pvnNumeroArqueo
        _Direccion = pvsDireccion
        _Ruc = pvsRuc
        _IdPedido = pvnIdPedido
    End Sub
End Class
