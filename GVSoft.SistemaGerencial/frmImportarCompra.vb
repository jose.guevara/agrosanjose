Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmImportarCompra
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImportar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Valor As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents IVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label18 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportarCompra))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImportar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(712, 72)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas en Carga de la Agencia"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(56, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(0, 13)
        Me.Label18.TabIndex = 5
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(136, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox2.TabIndex = 4
        Me.ComboBox2.Text = "ComboBox2"
        Me.ComboBox2.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(160, 24)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(544, 40)
        Me.TextBox1.TabIndex = 3
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(56, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(88, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Compra"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.TextBox18)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.TextBox17)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.TextBox16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.TextBox15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.TextBox4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.TextBox3)
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 153)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(712, 88)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Encabezado de la Factura"
        '
        'CheckBox1
        '
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.Location = New System.Drawing.Point(608, 56)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox1.TabIndex = 25
        Me.CheckBox1.Text = "Eliminar"
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(504, 56)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(88, 20)
        Me.TextBox18.TabIndex = 23
        Me.TextBox18.Text = "TextBox18"
        Me.TextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(464, 56)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(36, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Total"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(376, 56)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(72, 20)
        Me.TextBox17.TabIndex = 20
        Me.TextBox17.Text = "TextBox17"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(312, 56)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(65, 13)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "Retenci�n"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(224, 56)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(72, 20)
        Me.TextBox16.TabIndex = 18
        Me.TextBox16.Text = "TextBox16"
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(168, 56)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 13)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "Impuesto"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(64, 56)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(88, 20)
        Me.TextBox15.TabIndex = 16
        Me.TextBox15.Text = "TextBox15"
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "SubTotal"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(616, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 32)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Guardar Cambios"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(560, 24)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(40, 20)
        Me.TextBox4.TabIndex = 5
        Me.TextBox4.Text = "TextBox4"
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(488, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "D�as Cr�dito"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(145, 24)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(344, 20)
        Me.TextBox3.TabIndex = 3
        Me.TextBox3.Text = "TextBox3"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(72, 24)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(72, 20)
        Me.TextBox2.TabIndex = 2
        Me.TextBox2.Text = "TextBox2"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(5, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Proveedor"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvDetalle)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 246)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(712, 144)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle de la Compra"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Cantidad, Me.Precio, Me.Valor, Me.IVA})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(9, 16)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(694, 119)
        Me.dgvDetalle.TabIndex = 95
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Descripcion
        '
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 280
        '
        'Cantidad
        '
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        Me.Cantidad.Width = 65
        '
        'Precio
        '
        '
        '
        '
        Me.Precio.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Precio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Increment = 1.0R
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Increment = 1.0R
        Me.Valor.Name = "Valor"
        Me.Valor.ReadOnly = True
        Me.Valor.Width = 90
        '
        'IVA
        '
        Me.IVA.HeaderText = "IVA"
        Me.IVA.Name = "IVA"
        Me.IVA.ReadOnly = True
        Me.IVA.Visible = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Importar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Procesar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 394)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(712, 134)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Resumen de la Importaci�n"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.TextBox14)
        Me.GroupBox6.Controls.Add(Me.TextBox13)
        Me.GroupBox6.Controls.Add(Me.TextBox12)
        Me.GroupBox6.Controls.Add(Me.TextBox11)
        Me.GroupBox6.Controls.Add(Me.TextBox10)
        Me.GroupBox6.Controls.Add(Me.Label9)
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Controls.Add(Me.Label11)
        Me.GroupBox6.Controls.Add(Me.Label12)
        Me.GroupBox6.Controls.Add(Me.Label13)
        Me.GroupBox6.Location = New System.Drawing.Point(16, 73)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(680, 56)
        Me.GroupBox6.TabIndex = 13
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Compras al Cr�dito"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(632, 24)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(40, 20)
        Me.TextBox14.TabIndex = 22
        Me.TextBox14.Text = "TextBox14"
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(480, 24)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(88, 20)
        Me.TextBox13.TabIndex = 21
        Me.TextBox13.Text = "TextBox13"
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(360, 24)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(72, 20)
        Me.TextBox12.TabIndex = 20
        Me.TextBox12.Text = "TextBox12"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(216, 24)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(72, 20)
        Me.TextBox11.TabIndex = 19
        Me.TextBox11.Text = "TextBox11"
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(64, 24)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(88, 20)
        Me.TextBox10.TabIndex = 18
        Me.TextBox10.Text = "TextBox10"
        Me.TextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(576, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Cantidad"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(440, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Total"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(296, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Retenci�n"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(160, 24)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 13)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Impuesto"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "SubTotal"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox5)
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.TextBox9)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.TextBox8)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.TextBox7)
        Me.GroupBox5.Controls.Add(Me.TextBox6)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(680, 56)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Compras al Contado"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(64, 24)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(88, 20)
        Me.TextBox5.TabIndex = 20
        Me.TextBox5.Text = "TextBox5"
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "SubTotal"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(632, 24)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(40, 20)
        Me.TextBox9.TabIndex = 18
        Me.TextBox9.Text = "TextBox9"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(576, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Cantidad"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(480, 24)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(88, 20)
        Me.TextBox8.TabIndex = 16
        Me.TextBox8.Text = "TextBox8"
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(440, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Total"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(360, 24)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(72, 20)
        Me.TextBox7.TabIndex = 14
        Me.TextBox7.Text = "TextBox7"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(216, 24)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(72, 20)
        Me.TextBox6.TabIndex = 13
        Me.TextBox6.Text = "TextBox6"
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(296, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Retenci�n"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(160, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Impuesto"
        '
        'Timer1
        '
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdImportar, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(712, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 35
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdImportar
        '
        Me.cmdImportar.Image = CType(resources.GetObject("cmdImportar.Image"), System.Drawing.Image)
        Me.cmdImportar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdImportar.Name = "cmdImportar"
        Me.cmdImportar.Text = "Importar<F3>"
        Me.cmdImportar.Tooltip = "Importar"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Procesar<F4>"
        Me.cmdiLimpiar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmImportarCompra
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(712, 530)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmImportarCompra"
        Me.Text = "Importar Compra"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String = String.Empty
    Dim strNombAlmacenar As String = String.Empty

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmImportarCompra"

    Private Sub frmImportarCompra_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            TextBox3.ReadOnly = True
            TextBox4.ReadOnly = True
            TextBox5.ReadOnly = True
            TextBox6.ReadOnly = True
            TextBox7.ReadOnly = True
            TextBox8.ReadOnly = True
            TextBox9.ReadOnly = True
            TextBox10.ReadOnly = True
            TextBox11.ReadOnly = True
            TextBox12.ReadOnly = True
            TextBox13.ReadOnly = True
            TextBox14.ReadOnly = True
            TextBox15.ReadOnly = True
            TextBox16.ReadOnly = True
            TextBox17.ReadOnly = True
            TextBox18.ReadOnly = True
            TextBox2.Text = ""
            TextBox3.Text = ""
            TextBox4.Text = ""
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox8.Text = ""
            TextBox9.Text = ""
            TextBox10.Text = ""
            TextBox11.Text = ""
            TextBox12.Text = ""
            TextBox13.Text = ""
            TextBox14.Text = ""
            TextBox15.Text = ""
            TextBox16.Text = ""
            TextBox17.Text = ""
            TextBox18.Text = ""
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            ValidarDatosImportados()
            CargarResumen()
            CargarFacturas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmImportarCompra_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                ObtenerArchivo()
            ElseIf e.KeyCode = Keys.F4 Then
                ProcesarArchivo()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 2
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click

        Select Case sender.name.ToString
            Case "Importar" : ObtenerArchivo()
            Case "Procesar"
            Case "Salir" : Me.Close()
        End Select

    End Sub

    Sub Limpiar()
        Try
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox3.Text = ""
            TextBox4.Text = ""
            TextBox15.Text = ""
            TextBox16.Text = ""
            TextBox17.Text = ""
            TextBox18.Text = ""
            dgvDetalle.Rows.Clear()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Sub

    Sub ObtenerArchivo()
        Dim dlgNew As New OpenFileDialog
        Dim strLine, strDrive As String

        Dim lngRegistro As Long
        Dim strFacturas As String
        Dim intFechaIng As Integer
        Dim strFechaIng As String
        Dim strVendCodigo As String
        Dim strCliCodigo As String
        Dim strCliNombre As String
        Dim dblSubTotal As Double
        Dim dblImpuesto As Double
        Dim dblRetencion As Double
        Dim dblTotal As Double
        Dim strUsrCodigo As String
        Dim bytAgenCodigo As Byte
        Dim bytStatus As Byte
        Dim bytImpreso As Byte
        Dim byttipoCompra As Byte
        Dim intDiasCredito As Integer
        Dim intNumFecAnul As Integer
        Dim strProdCodigo As String
        Dim strProdDescrip As String
        Dim dblCantidad As Double
        Dim dblPrecio As Double
        Dim dblValor As Double
        Dim dblIva As Double
        Dim EsBonificacion As Integer
        Dim IdProv As Integer
        Dim intRegDetalle As Integer
        Dim strArchivo, strArchivo2, strClave, strComando As String
        Dim Columnas As String()

        Dim existe, er As Integer
        Dim strQueryVal As String = ""
        Dim ldtDatos As DataTable = Nothing

        Try

            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importaci�n")
                Exit Sub
            End If
            strDrive = ""
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "cpras_")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MsgBox("No es un archivo que contiene las Compras de una agencia. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            strNombAlmacenar = ""
            strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
                Exit Sub
            End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'strQuery = ""
            'strQuery = "truncate table tmp_ComprasImp"
            'cmdAgro2K.CommandText = strQuery
            'cmdAgro2K.ExecuteNonQuery()
            RNCompra.LimpiarArchivosImportarCompra()
            strNombArchivo = ""
            strNombArchivo = strArchivo2
            ''If cnnAgro2K.State = ConnectionState.Open Then
            ''    cnnAgro2K.Close()
            ''End If
            ''cnnAgro2K.Open()
            ''cmdAgro2K.Connection = cnnAgro2K
            intIncr = 0
            intIncr = InStrRev(strNombArchivo, "\")
            strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
            'strQuery = ""
            'strQuery = "select * from tbl_ArchivosImpCompras where archivo = '" & strNombAlmacenar & "'"
            'cmdAgro2K.CommandText = strQuery
            Try
                intError = 0
                ldtDatos = Nothing
                ldtDatos = RNCompra.ObtieneArchivoComprasAimpotar(strNombArchivo)
                If SUFunciones.ValidaDataTable(ldtDatos) Then
                    intError = 0  'intError = 1 Se asigna cero para no validar el nombre del archivo de requisas que ya existe
                End If
                'dtrAgro2K = cmdAgro2K.ExecuteReader
                'While dtrAgro2K.Read
                '    intError = 0  'intError = 1 Se asigna cero para no validar el nombre del archivo de requisas que ya existe
                'End While
                'dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            If intError = 1 Then
                MsgBox("Archivo ya fue procesado.  Verifique.", MsgBoxStyle.Critical, "Error de Archivo")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = Nothing
            Try
                sr = File.OpenText(strArchivo2)
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            intIncr = 0
            intError = 0
            intRegDetalle = 0

            er = 1  ' con uno indica que las Compras del archivo que se importa ya existen, su valor cambia si al menos una Compra no existe
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                Columnas = strLine.Trim().Split("|")
                intIncr = InStr(1, strLine, "|")
                lngRegistro = 0
                lngRegistro = ConvierteAInt(Columnas(0).Trim())
                strFacturas = ""
                strFacturas = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strFacturas = Columnas(1).Trim()
                intFechaIng = 0
                intFechaIng = ConvierteAInt(Columnas(2).Trim())
                strFechaIng = ""
                strFechaIng = Columnas(3).Trim()
                strVendCodigo = ""
                strVendCodigo = Columnas(4).Trim()
                strCliCodigo = ""
                strCliCodigo = Columnas(5).Trim()
                strCliNombre = ""
                strCliNombre = Columnas(6).Trim()
                dblSubTotal = 0
                dblSubTotal = ConvierteADouble(Columnas(7).Trim())
                dblImpuesto = 0
                dblImpuesto = ConvierteADouble(Columnas(8).Trim())
                dblRetencion = 0
                dblRetencion = ConvierteADouble(Columnas(9).Trim())
                dblTotal = 0
                dblTotal = ConvierteADouble(Columnas(10).Trim())
                strUsrCodigo = ""
                strUsrCodigo = Columnas(11).Trim()
                bytAgenCodigo = 0
                bytAgenCodigo = ConvierteAInt(Columnas(12).Trim())
                bytStatus = 0
                bytStatus = ConvierteAInt(Columnas(13).Trim())
                bytImpreso = 0
                bytImpreso = ConvierteAInt(Columnas(14).Trim())
                byttipoCompra = 0
                byttipoCompra = ConvierteAInt(Columnas(15).Trim())
                intDiasCredito = 0
                intDiasCredito = ConvierteAInt(Columnas(16).Trim())
                intNumFecAnul = 0
                intNumFecAnul = ConvierteAInt(Columnas(17).Trim())
                strProdCodigo = ""
                strProdCodigo = Columnas(18).Trim()
                strProdDescrip = ""
                strProdDescrip = Columnas(19).Trim()
                dblCantidad = 0
                dblCantidad = ConvierteADouble(Columnas(20).Trim())
                dblPrecio = 0
                dblPrecio = ConvierteADouble(Columnas(21).Trim())
                dblValor = 0
                dblValor = ConvierteADouble(Columnas(22).Trim())
                dblIva = 0
                dblIva = ConvierteADouble(Columnas(23).Trim())
                EsBonificacion = 0
                EsBonificacion = ConvierteAInt(Columnas(24).Trim())
                IdProv = 0
                IdProv = ConvierteAInt(Columnas(25).Trim())

                intRegDetalle = intRegDetalle + 1
                If bytStatus = 1 Then
                    bytImpreso = 0
                End If
                'strQueryVal = "exec spValidaCompraImportar '" & strFacturas & "', " & IdProv

                'If cnnAgro2K.State = ConnectionState.Open Then
                '    cnnAgro2K.Close()
                'End If

                'cnnAgro2K.Open()
                Try
                    'cmdAgro2K.CommandText = strQueryVal
                    'dtrAgro2K = cmdAgro2K.ExecuteReader()
                    'While dtrAgro2K.Read
                    '    existe = dtrAgro2K.GetValue(0)
                    'End While
                    'dtrAgro2K.Close()
                    existe = 0
                    existe = RNCompra.ValidaComprasImportar(strFacturas, IdProv)

                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try

                If existe = 0 Then  'Cero indica que la requisa no existe en el sitema y por tanto se ingresa, 1 indica que existe y no se ingresa
                    er = existe
                    'strQuery = ""
                    'strQuery = "insert into tmp_ComprasImp values (" & lngRegistro & ", '" & strFacturas & "', "
                    'strQuery = strQuery + "" & intFechaIng & ", '" & strFechaIng & "', '" & strVendCodigo & "', "
                    'strQuery = strQuery + "'" & strCliCodigo & "', '" & strCliNombre & "', " & dblSubTotal & ", "
                    'strQuery = strQuery + "" & dblImpuesto & ", " & dblRetencion & ", " & dblTotal & ", "
                    'strQuery = strQuery + "'" & strUsrCodigo & "', " & bytAgenCodigo & ", " & bytStatus & ", "
                    'strQuery = strQuery + "" & bytImpreso & ", " & byttipoCompra & ", " & intDiasCredito & ", "
                    'strQuery = strQuery + "" & intNumFecAnul & ", '" & strProdCodigo & "', "
                    'strQuery = strQuery + "'" & strProdDescrip & "', " & dblCantidad & ", " & dblPrecio & ", "
                    'strQuery = strQuery + "" & dblValor & "," & dblIva & "," & EsBonificacion & ", " & IdProv & ", " & intRegDetalle & ",'Sin Error')"
                    Try
                        'cmdAgro2K.CommandText = strQuery
                        'cmdAgro2K.ExecuteNonQuery()
                        RNCompra.IngresaDetalleImportarCompra(lngRegistro, strFacturas, intFechaIng, strFechaIng, strVendCodigo, strCliCodigo, strCliNombre, dblSubTotal,
                                                               dblImpuesto, dblRetencion, dblTotal, strUsrCodigo, bytAgenCodigo, bytStatus, bytImpreso, byttipoCompra,
                                                              intDiasCredito, intNumFecAnul, strProdCodigo, strProdDescrip, dblCantidad, dblPrecio, dblValor, dblIva, EsBonificacion,
                                                              IdProv, intRegDetalle, "Sin Error")
                    Catch exc As Exception
                        intError = 1
                        MsgBox(exc.Message.ToString)
                        Me.Cursor = Cursors.Default
                        Exit Do
                    End Try
                End If
            Loop
            sr.Close()
            If er = 0 Then 'Condicionamos para indicarle al husuario si ya ha ingresado las Facturas de compras
                If intError = 0 Then
                    ValidarDatosImportados()
                    CargarResumen()
                    CargarFacturas()
                    MsgBox("Finalizado satisfactoriamente la importaci�n de las Compras.", MsgBoxStyle.Information, "Proceso Finalizado")
                Else
                    MsgBox("Hubieron errores al importar el archivo de Compras.", MsgBoxStyle.Critical, "Error en la Importaci�n")
                End If
            Else
                MsgBox("El archivo de Facturas de Compras ya fue Importado.", MsgBoxStyle.Critical, "Error en la Importaci�n")
            End If

            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub ValidarDatosImportados()

        ''Dim cmdTmp As New SqlCommand("ImportarCompra", cnnAgro2K)
        ''Dim prmTmp01 As New SqlParameter
        Dim ldtDatos As DataTable = Nothing
        Dim lnResultado As Integer = 0
        Try
            ldtDatos = RNCompra.ValidaDatosImportado(0)
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                lnResultado = SUConversiones.ConvierteAInt(ldtDatos.Rows(0).Item(0))
            End If
            If lnResultado = 0 Then
                MsgBox("No hubieron errores en la validaci�n de las Compras", MsgBoxStyle.Information, "Validaci�n de Datos")
            ElseIf lnResultado <> 254 Then
                MsgBox("Hubieron errores en la validaci�n de las Compras", MsgBoxStyle.Critical, "Validaci�n de Datos")
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        'With prmTmp01
        '    .ParameterName = "@intAccion"
        '    .SqlDbType = SqlDbType.TinyInt
        '    .Value = 0
        'End With
        'With cmdTmp
        '    .Parameters.Add(prmTmp01)
        '    .CommandType = CommandType.StoredProcedure
        'End With
        'If cmdTmp.Connection.State = ConnectionState.Open Then
        '    cmdTmp.Connection.Close()
        'End If
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdTmp.Connection = cnnAgro2K
        'Try
        '    dtrAgro2K = cmdTmp.ExecuteReader
        '    While dtrAgro2K.Read
        '        If dtrAgro2K.GetValue(0) = 0 Then
        '            MsgBox("No hubieron errores en la validaci�n de las Compras", MsgBoxStyle.Information, "Validaci�n de Datos")
        '        ElseIf dtrAgro2K.GetValue(0) <> 254 Then
        '            MsgBox("Hubieron errores en la validaci�n de las Compras", MsgBoxStyle.Critical, "Validaci�n de Datos")
        '        End If
        '    End While
        '    dtrAgro2K.Close()
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        'End Try

    End Sub

    Sub CargarResumen()
        Dim lnResultado As Integer = 0
        Dim ldtDatos As DataTable = Nothing
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            ldtDatos = RNCompra.ValidaDatosImportado(1)
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                TextBox5.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("subtotal")), "##,##0.#0")
                TextBox6.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("impuesto")), "##,##0.#0")
                TextBox7.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("retencion")), "##,##0.#0")
                TextBox8.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("total")), "##,##0.#0")
                TextBox9.Text = SUConversiones.ConvierteAInt(ldtDatos.Rows(0).Item("cantidad"))
            End If
            ldtDatos = Nothing
            ldtDatos = RNCompra.ValidaDatosImportado(2)
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                TextBox10.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("subtotal")), "##,##0.#0")
                TextBox11.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("impuesto")), "##,##0.#0")
                TextBox12.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("retencion")), "##,##0.#0")
                TextBox13.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(0).Item("total")), "##,##0.#0")
                TextBox14.Text = SUConversiones.ConvierteAInt(ldtDatos.Rows(0).Item("cantidad"))
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
        'Dim cmdTmp As New SqlCommand("ImportarCompra", cnnAgro2K)
        'Dim prmTmp01 As New SqlParameter


        'With prmTmp01
        '    .ParameterName = "@intAccion"
        '    .SqlDbType = SqlDbType.TinyInt
        '    .Value = 1
        'End With
        'With cmdTmp
        '    .Parameters.Add(prmTmp01)
        '    .CommandType = CommandType.StoredProcedure
        'End With
        'If cmdTmp.Connection.State = ConnectionState.Open Then
        '    cmdTmp.Connection.Close()
        'End If
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdTmp.Connection = cnnAgro2K
        'Try
        '    dtrAgro2K = cmdTmp.ExecuteReader
        '    While dtrAgro2K.Read
        '        If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
        '            TextBox5.Text = Format(CDbl(dtrAgro2K.GetValue(0)), "##,##0.#0")
        '            TextBox6.Text = Format(CDbl(dtrAgro2K.GetValue(1)), "##,##0.#0")
        '            TextBox7.Text = Format(CDbl(dtrAgro2K.GetValue(2)), "##,##0.#0")
        '            TextBox8.Text = Format(CDbl(dtrAgro2K.GetValue(3)), "##,##0.#0")
        '            TextBox9.Text = dtrAgro2K.GetValue(4)
        '        End If
        '    End While
        '    dtrAgro2K.Close()
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        'End Try

        'If cmdTmp.Connection.State = ConnectionState.Open Then
        '    cmdTmp.Connection.Close()
        'End If
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdTmp.Connection = cnnAgro2K
        'cmdTmp.Parameters(0).Value = 2
        'Try
        '    dtrAgro2K = cmdTmp.ExecuteReader
        '    While dtrAgro2K.Read
        '        If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
        '            TextBox10.Text = Format(CDbl(dtrAgro2K.GetValue(0)), "##,##0.#0")
        '            TextBox11.Text = Format(CDbl(dtrAgro2K.GetValue(1)), "##,##0.#0")
        '            TextBox12.Text = Format(CDbl(dtrAgro2K.GetValue(2)), "##,##0.#0")
        '            TextBox13.Text = Format(CDbl(dtrAgro2K.GetValue(3)), "##,##0.#0")
        '            TextBox14.Text = dtrAgro2K.GetValue(4)
        '        End If
        '    End While
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        'Finally
        '    dtrAgro2K.Close()
        '    cmdTmp.Connection.Close()
        '    cnnAgro2K.Close()
        '    Me.Cursor = System.Windows.Forms.Cursors.Default
        'End Try

    End Sub

    Sub CargarFacturas()
        Dim ldtDatos As DataTable = Nothing
        Try
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()

            ldtDatos = RNCompra.ObtieneComprasAimpotar()
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                For i As Int32 = 0 To ldtDatos.Rows.Count - 1
                    ComboBox1.Items.Add(ldtDatos.Rows(i).Item(1))
                    ComboBox2.Items.Add(ldtDatos.Rows(i).Item(0))
                Next
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
        End Try
        'strQuery = ""
        'strQuery = "select distinct registro, numero, tipoCompra from tmp_Comprasimp where status = 0 order by tipoCompra, numero"
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdAgro2K.Connection = cnnAgro2K
        'Try
        '    cmdAgro2K.CommandText = strQuery
        '    dtrAgro2K = cmdAgro2K.ExecuteReader
        '    While dtrAgro2K.Read
        '        ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
        '        ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
        '    End While
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        'End Try
        'dtrAgro2K.Close()
        'cmdAgro2K.Connection.Close()
        'cnnAgro2K.Close()

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdAgro2K.Connection = cnnAgro2K
        Dim ldtDatos As DataTable = Nothing
        Try
            Limpiar()
            ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
            TextBox1.Text = "Sin Error"

            ldtDatos = RNCompra.ObtieneComprasAimpotar(SUConversiones.ConvierteALong(ComboBox2.SelectedItem))
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                For i As Int32 = 0 To ldtDatos.Rows.Count - 1
                    Label18.Text = ldtDatos.Rows(i).Item(3)
                    TextBox2.Text = ldtDatos.Rows(i).Item(5)
                    TextBox3.Text = ldtDatos.Rows(i).Item(6)
                    TextBox4.Text = ldtDatos.Rows(i).Item(16)
                    TextBox15.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(i).Item(7)), "##,##0.#0")
                    TextBox16.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(i).Item(8)), "##,##0.#0")
                    TextBox17.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(i).Item(9)), "##,##0.#0")
                    TextBox18.Text = Format(SUConversiones.ConvierteADouble(ldtDatos.Rows(i).Item(10)), "##,##0.#0")

                    dgvDetalle.Rows.Add(ldtDatos.Rows(i).Item(18), ldtDatos.Rows(i).Item(19), Format(ldtDatos.Rows(i).Item(20), "##,##0.#0"),
                                    Format(ldtDatos.Rows(i).Item(21), "##,##0.#0"), Format(ldtDatos.Rows(i).Item(22), "##,##0.#0"),
                                    Format(ldtDatos.Rows(i).Item(23), "##,##0.#0"), SUConversiones.ConvierteAInt(ldtDatos.Rows(i).Item(26)))

                    If ldtDatos.Rows(i).Item(27) <> "Sin Error" Then
                        If Len(TextBox1.Text) < Len(ldtDatos.Rows(i).Item(25)) Then
                            TextBox1.Text = ldtDatos.Rows(i).Item(25)
                        End If
                    End If

                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
        'strQuery = ""
        'strQuery = "select * from tmp_Comprasimp where registro = " & CLng(ComboBox2.SelectedItem) & ""
        'Try
        '    cmdAgro2K.CommandText = strQuery
        '    dtrAgro2K = cmdAgro2K.ExecuteReader
        '    While dtrAgro2K.Read
        '        Label18.Text = dtrAgro2K.GetValue(3)
        '        TextBox2.Text = dtrAgro2K.GetValue(5)
        '        TextBox3.Text = dtrAgro2K.GetValue(6)
        '        TextBox4.Text = dtrAgro2K.GetValue(16)
        '        TextBox15.Text = Format(dtrAgro2K.GetValue(7), "##,##0.#0")
        '        TextBox16.Text = Format(dtrAgro2K.GetValue(8), "##,##0.#0")
        '        TextBox17.Text = Format(dtrAgro2K.GetValue(9), "##,##0.#0")
        '        TextBox18.Text = Format(dtrAgro2K.GetValue(10), "##,##0.#0")

        '        dgvDetalle.Rows.Add(dtrAgro2K.GetValue(18), dtrAgro2K.GetValue(19), Format(dtrAgro2K.GetValue(20), "##,##0.#0"), _
        '                            Format(dtrAgro2K.GetValue(21), "##,##0.#0"), Format(dtrAgro2K.GetValue(22), "##,##0.#0"), _
        '                            Format(dtrAgro2K.GetValue(23), "##,##0.#0"), CInt(dtrAgro2K.GetValue(26)))

        '        If dtrAgro2K.GetValue(27) <> "Sin Error" Then
        '            If Len(TextBox1.Text) < Len(dtrAgro2K.GetValue(25)) Then
        '                TextBox1.Text = dtrAgro2K.GetValue(25)
        '            End If
        '        End If
        '    End While
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        'End Try
        'dtrAgro2K.Close()
        'cmdAgro2K.Connection.Close()
        'cnnAgro2K.Close()

    End Sub


    'Function UbicarProducto(ByVal prm01 As String) As Boolean

    '    Dim intEnc As Integer

    '    intEnc = 0
    '    strQuery = ""
    '    strQuery = "Select Codigo, Descripcion, Pvpc, Impuesto from prm_Productos Where Codigo = '" & prm01 & "'"
    '    If cnnAgro2K.State = ConnectionState.Open Then
    '        cnnAgro2K.Close()
    '    End If
    '    If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '        cmdAgro2K.Connection.Close()
    '    End If
    '    cnnAgro2K.Open()
    '    cmdAgro2K.Connection = cnnAgro2K
    '    cmdAgro2K.CommandText = strQuery
    '    dtrAgro2K = cmdAgro2K.ExecuteReader
    '    While dtrAgro2K.Read
    '    End While
    '    dtrAgro2K.Close()
    '    cmdAgro2K.Connection.Close()
    '    cnnAgro2K.Close()
    '    If intEnc = 0 Then
    '        UbicarProducto = False
    '        Exit Function
    '    End If
    '    UbicarProducto = True

    'End Function


    Sub ProcesarArchivo()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim ldtDatos As DataTable = Nothing
        Dim lnResultado As Integer = 0
        Dim bytContinuar As Byte
        Try
            bytContinuar = 0
            ldtDatos = RNCompra.ValidaDatosImportado(0)
            If SUFunciones.ValidaDataTable(ldtDatos) Then
                lnResultado = SUConversiones.ConvierteAInt(ldtDatos.Rows(0).Item(0))
            End If
            If lnResultado = 0 Then
                MsgBox("No hubieron errores en la validaci�n de las Compras", MsgBoxStyle.Information, "Validaci�n de Datos")
            ElseIf lnResultado <> 254 Then
                bytContinuar = 1
                MsgBox("Hubieron errores en la validaci�n de las Compras", MsgBoxStyle.Critical, "Validaci�n de Datos")
            End If

            If bytContinuar = 0 Then
                ldtDatos = Nothing
                Try
                    ldtDatos = RNCompra.ValidaDatosImportado(3)
                Catch ex As Exception
                    MsgBox(ex.Message.ToString)
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Sub
                End Try
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
        'Dim cmdTmp As New SqlCommand("ImportarCompra", cnnAgro2K)
        'Dim prmTmp01 As New SqlParameter

        'With prmTmp01
        '    .ParameterName = "@intAccion"
        '    .SqlDbType = SqlDbType.TinyInt
        '    .Value = 0
        'End With
        'With cmdTmp
        '    .Parameters.Add(prmTmp01)
        '    .CommandType = CommandType.StoredProcedure
        'End With
        'If cmdTmp.Connection.State = ConnectionState.Open Then
        '    cmdTmp.Connection.Close()
        'End If
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdTmp.Connection = cnnAgro2K
        'Try
        '    dtrAgro2K = cmdTmp.ExecuteReader
        '    While dtrAgro2K.Read
        '        If dtrAgro2K.GetValue(0) = 0 Then
        '            MsgBox("No hay errores en la validaci�n de las Compras. Se procedera a ingresar las Compras.", MsgBoxStyle.Information, "Validaci�n de Datos")
        '        ElseIf dtrAgro2K.GetValue(0) <> 254 Then
        '            bytContinuar = 1
        '            MsgBox("Continuan los errores en la validaci�n de las Compras", MsgBoxStyle.Critical, "Validaci�n de Datos")
        '            Exit Sub
        '        End If
        '    End While
        '    dtrAgro2K.Close()
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        '    dtrAgro2K.Close()
        '    Me.Cursor = System.Windows.Forms.Cursors.Default
        '    Exit Sub
        'End Try
        'If bytContinuar = 0 Then
        '    cmdTmp.Parameters(0).Value = 3
        '    Try
        '        cmdTmp.ExecuteNonQuery()
        '    Catch exc As Exception
        '        MsgBox(exc.Message.ToString)
        '        dtrAgro2K.Close()
        '        Me.Cursor = System.Windows.Forms.Cursors.Default
        '        Exit Sub
        '    End Try
        'End If
        'strQuery = ""
        'strQuery = "insert into tbl_ArchivosImpCompras values ('" & strNombAlmacenar & "', "
        'strQuery = strQuery + "'" & Format(Now, "dd-MMM-yyyy") & "', " & Format(Now, "yyyyMMdd") & ", "
        'strQuery = strQuery + "" & lngRegUsuario & ")"
        'cmdAgro2K.CommandText = strQuery
        Try
            '            cmdAgro2K.ExecuteNonQuery()
            RNCompra.IngresaRegistroEnArchivosImportarCompra(strNombAlmacenar, Format(Now, "dd-MMM-yyyy"), Format(Now, "yyyyMMdd"), lngRegUsuario)
            RNCompra.LimpiarArchivosImportarCompra()
            MsgBox("Finalizo el proceso de importar Comprar de una agencia a la Casa Matriz.", MsgBoxStyle.Exclamation, "Proceso Finalizado")
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'MsgBox(exc.Message.ToString)
            Throw exc

        End Try
        'strQuery = ""
        'strQuery = "truncate table tmp_Comprasimp"
        'cmdAgro2K.CommandText = strQuery
        'cmdAgro2K.ExecuteNonQuery()
        'cmdAgro2K.Connection.Close()
        'cnnAgro2K.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Close()

    End Sub

    Function UbicarCliente(ByVal strDato As String) As Boolean
        Dim ldtDatos As DataTable = Nothing
        Try
            strDirecFact = ""
            TextBox14.Text = "0"
            TextBox3.Text = ""
            intError = 1

            ldtDatos = RNCliente.ObtieneClientexCodigoCliente(TextBox2.Text.Trim)

            If SUFunciones.ValidaDataTable(ldtDatos) Then
                intError = 0
                TextBox3.Text = ldtDatos.Rows(0).Item("Nombre")

            End If
            If intError = 1 Then
                TextBox2.Text = ""
                TextBox3.Text = ""
                MsgBox("C�digo de cliente no est� registrado.  Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Function
            End If
            If TextBox14.Text = "0" Then
                UbicarCliente = False
                Exit Function
            End If
            UbicarCliente = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        'strQuery = "Select C.nombre from prm_Clientes C Where C.Codigo = '" & TextBox2.Text & "' AND C.Estado = 0"
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdAgro2K.Connection = cnnAgro2K
        'cmdAgro2K.CommandText = strQuery
        'dtrAgro2K = cmdAgro2K.ExecuteReader
        'While dtrAgro2K.Read
        '    intError = 0
        '    TextBox3.Text = dtrAgro2K.GetValue(0)
        'End While
        'dtrAgro2K.Close()
        'cmdAgro2K.Connection.Close()
        'cnnAgro2K.Close()
        'If TextBox14.Text = "0" Then
        '    UbicarCliente = False
        '    Exit Function
        'End If
        'UbicarCliente = True

    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                TextBox2.Text = strUbicar : UbicarCliente(TextBox2.Text) : TextBox3.Focus()
            End If
        End If

    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox2.KeyDown

        If e.KeyCode = Keys.Enter Then
            TextBox3.Focus()
        End If

    End Sub

    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.LostFocus

        UbicarCliente(TextBox2.Text)

    End Sub

    Private Sub cmdImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportar.Click
        Try
            ObtenerArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            ProcesarArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
