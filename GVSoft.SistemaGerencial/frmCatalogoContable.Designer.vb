<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCatalogoContable
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCatalogoContable))
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ExpandablePanel1 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel1 = New DevComponents.DotNetBar.ItemPanel()
        Me.ComboB_categoria = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.Label_reaumensaldos = New DevComponents.DotNetBar.LabelX()
        Me.ItemPanel5 = New DevComponents.DotNetBar.ItemPanel()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_saldoiniNac = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_saldofinNac = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_haberNac = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_debeNac = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Bar2 = New DevComponents.DotNetBar.Bar()
        Me.LabelItem_msg = New DevComponents.DotNetBar.LabelItem()
        Me.ComboB_Tipo_Tabdetalle = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_codmoneda_Tabdetalle = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_Desccuenta_Tabdetalle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextB_cdgocuenta_Tabdetalle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.SuperTabItem_detalle = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ExpandablePanel3 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel4 = New DevComponents.DotNetBar.ItemPanel()
        Me.GridP_Catalogo = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ExpandablePanel2 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel3 = New DevComponents.DotNetBar.ItemPanel()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.ComboB_Tipo_TabBusquedas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_codmoneda_TabBusquedas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_Desccuenta = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextB_CuentaContable = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.SuperTabItem_Buscar = New DevComponents.DotNetBar.SuperTabItem()
        Me.LabelItem_pantalla = New DevComponents.DotNetBar.LabelItem()
        Me.ToolbarB_GuardarCuenta = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_limpiardetalle = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_BuscarCuenta = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_limpiarbusqueda = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_ImprimirCatalogo = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_NuevaCuenta = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_menuprincipal = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_cerrarpantalla = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSalir = New DevComponents.DotNetBar.ButtonItem()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.ExpandablePanel1.SuspendLayout()
        Me.ItemPanel1.SuspendLayout()
        Me.ItemPanel5.SuspendLayout()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.ExpandablePanel3.SuspendLayout()
        Me.ItemPanel4.SuspendLayout()
        CType(Me.GridP_Catalogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExpandablePanel2.SuspendLayout()
        Me.ItemPanel3.SuspendLayout()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 5)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(1088, 474)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 17
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem_Buscar, Me.SuperTabItem_detalle, Me.ButtonItem_NuevaCuenta, Me.ButtonItem_menuprincipal, Me.ButtonItem_cerrarpantalla, Me.LabelItem_pantalla, Me.btnSalir})
        Me.SuperTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.ExpandablePanel1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(1088, 448)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem_detalle
        '
        'ExpandablePanel1
        '
        Me.ExpandablePanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel1.Controls.Add(Me.ItemPanel1)
        Me.ExpandablePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel1.ExpandOnTitleClick = True
        Me.ExpandablePanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel1.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel1.Name = "ExpandablePanel1"
        Me.ExpandablePanel1.Size = New System.Drawing.Size(1088, 248)
        Me.ExpandablePanel1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel1.Style.BackColor1.Color = System.Drawing.Color.SteelBlue
        Me.ExpandablePanel1.Style.BackColor2.Color = System.Drawing.Color.SteelBlue
        Me.ExpandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel1.Style.GradientAngle = 90
        Me.ExpandablePanel1.TabIndex = 2
        Me.ExpandablePanel1.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel1.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel1.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.ExpandablePanel1.TitleStyle.BorderWidth = 0
        Me.ExpandablePanel1.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel1.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel1.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel1.TitleText = "Datos Generales de la Cuenta"
        '
        'ItemPanel1
        '
        '
        '
        '
        Me.ItemPanel1.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel1.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel1.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel1.BackgroundStyle.Class = ""
        Me.ItemPanel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel1.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel1.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel1.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel1.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel1.ContainerControlProcessDialogKey = True
        Me.ItemPanel1.Controls.Add(Me.ComboB_categoria)
        Me.ItemPanel1.Controls.Add(Me.LabelX6)
        Me.ItemPanel1.Controls.Add(Me.Label_reaumensaldos)
        Me.ItemPanel1.Controls.Add(Me.ItemPanel5)
        Me.ItemPanel1.Controls.Add(Me.Bar2)
        Me.ItemPanel1.Controls.Add(Me.ComboB_Tipo_Tabdetalle)
        Me.ItemPanel1.Controls.Add(Me.ComboB_codmoneda_Tabdetalle)
        Me.ItemPanel1.Controls.Add(Me.LabelX1)
        Me.ItemPanel1.Controls.Add(Me.LabelX2)
        Me.ItemPanel1.Controls.Add(Me.LabelX3)
        Me.ItemPanel1.Controls.Add(Me.LabelX4)
        Me.ItemPanel1.Controls.Add(Me.TextB_Desccuenta_Tabdetalle)
        Me.ItemPanel1.Controls.Add(Me.TextB_cdgocuenta_Tabdetalle)
        Me.ItemPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1})
        Me.ItemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel1.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel1.Name = "ItemPanel1"
        Me.ItemPanel1.Size = New System.Drawing.Size(1088, 222)
        Me.ItemPanel1.TabIndex = 0
        Me.ItemPanel1.Text = "ItemPanel1"
        '
        'ComboB_categoria
        '
        Me.ComboB_categoria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ComboB_categoria.DisplayMember = "Text"
        Me.ComboB_categoria.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_categoria.FormattingEnabled = True
        Me.ComboB_categoria.ItemHeight = 15
        Me.ComboB_categoria.Location = New System.Drawing.Point(177, 118)
        Me.ComboB_categoria.Name = "ComboB_categoria"
        Me.ComboB_categoria.Size = New System.Drawing.Size(426, 21)
        Me.ComboB_categoria.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_categoria.TabIndex = 48
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.Class = ""
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(68, 114)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(82, 23)
        Me.LabelX6.TabIndex = 49
        Me.LabelX6.Text = "Categoria"
        '
        'Label_reaumensaldos
        '
        Me.Label_reaumensaldos.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_reaumensaldos.BackgroundStyle.Class = ""
        Me.Label_reaumensaldos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_reaumensaldos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_reaumensaldos.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label_reaumensaldos.Location = New System.Drawing.Point(45, 140)
        Me.Label_reaumensaldos.Name = "Label_reaumensaldos"
        Me.Label_reaumensaldos.Size = New System.Drawing.Size(493, 23)
        Me.Label_reaumensaldos.TabIndex = 47
        Me.Label_reaumensaldos.Text = "Resumen de Saldos Al Cierre ?"
        '
        'ItemPanel5
        '
        Me.ItemPanel5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.ItemPanel5.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel5.ContainerControlProcessDialogKey = True
        Me.ItemPanel5.Controls.Add(Me.LabelX5)
        Me.ItemPanel5.Controls.Add(Me.TextB_saldoiniNac)
        Me.ItemPanel5.Controls.Add(Me.LabelX20)
        Me.ItemPanel5.Controls.Add(Me.TextB_saldofinNac)
        Me.ItemPanel5.Controls.Add(Me.LabelX19)
        Me.ItemPanel5.Controls.Add(Me.TextB_haberNac)
        Me.ItemPanel5.Controls.Add(Me.LabelX18)
        Me.ItemPanel5.Controls.Add(Me.TextB_debeNac)
        Me.ItemPanel5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel5.Location = New System.Drawing.Point(43, 166)
        Me.ItemPanel5.Name = "ItemPanel5"
        Me.ItemPanel5.Size = New System.Drawing.Size(918, 51)
        Me.ItemPanel5.TabIndex = 46
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.Class = ""
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(57, 2)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(132, 23)
        Me.LabelX5.TabIndex = 32
        Me.LabelX5.Text = "Saldo Inicial"
        '
        'TextB_saldoiniNac
        '
        Me.TextB_saldoiniNac.BackColor = System.Drawing.SystemColors.ButtonHighlight
        '
        '
        '
        Me.TextB_saldoiniNac.Border.Class = "TextBoxBorder"
        Me.TextB_saldoiniNac.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_saldoiniNac.Enabled = False
        Me.TextB_saldoiniNac.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_saldoiniNac.Location = New System.Drawing.Point(57, 27)
        Me.TextB_saldoiniNac.Name = "TextB_saldoiniNac"
        Me.TextB_saldoiniNac.Size = New System.Drawing.Size(154, 21)
        Me.TextB_saldoiniNac.TabIndex = 33
        Me.TextB_saldoiniNac.Text = "0.00"
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.Class = ""
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(707, 1)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(132, 23)
        Me.LabelX20.TabIndex = 23
        Me.LabelX20.Text = "Saldo Final"
        '
        'TextB_saldofinNac
        '
        Me.TextB_saldofinNac.BackColor = System.Drawing.SystemColors.ButtonHighlight
        '
        '
        '
        Me.TextB_saldofinNac.Border.Class = "TextBoxBorder"
        Me.TextB_saldofinNac.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_saldofinNac.Enabled = False
        Me.TextB_saldofinNac.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_saldofinNac.Location = New System.Drawing.Point(707, 26)
        Me.TextB_saldofinNac.Name = "TextB_saldofinNac"
        Me.TextB_saldofinNac.Size = New System.Drawing.Size(154, 21)
        Me.TextB_saldofinNac.TabIndex = 31
        Me.TextB_saldofinNac.Text = "0.00"
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.Class = ""
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(490, 2)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(78, 23)
        Me.LabelX19.TabIndex = 22
        Me.LabelX19.Text = "Cr�dito"
        '
        'TextB_haberNac
        '
        Me.TextB_haberNac.BackColor = System.Drawing.SystemColors.ButtonHighlight
        '
        '
        '
        Me.TextB_haberNac.Border.Class = "TextBoxBorder"
        Me.TextB_haberNac.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_haberNac.Enabled = False
        Me.TextB_haberNac.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_haberNac.Location = New System.Drawing.Point(490, 27)
        Me.TextB_haberNac.Name = "TextB_haberNac"
        Me.TextB_haberNac.Size = New System.Drawing.Size(154, 21)
        Me.TextB_haberNac.TabIndex = 30
        Me.TextB_haberNac.Text = "0.00"
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.Class = ""
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(269, 2)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(78, 23)
        Me.LabelX18.TabIndex = 21
        Me.LabelX18.Text = "D�bito"
        '
        'TextB_debeNac
        '
        Me.TextB_debeNac.BackColor = System.Drawing.SystemColors.ButtonHighlight
        '
        '
        '
        Me.TextB_debeNac.Border.Class = "TextBoxBorder"
        Me.TextB_debeNac.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_debeNac.Enabled = False
        Me.TextB_debeNac.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_debeNac.Location = New System.Drawing.Point(269, 27)
        Me.TextB_debeNac.Name = "TextB_debeNac"
        Me.TextB_debeNac.Size = New System.Drawing.Size(154, 21)
        Me.TextB_debeNac.TabIndex = 29
        Me.TextB_debeNac.Text = "0.00"
        '
        'Bar2
        '
        Me.Bar2.AccessibleDescription = "Bar2 (Bar2)"
        Me.Bar2.AccessibleName = "Bar2"
        Me.Bar2.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar2.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar2.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_GuardarCuenta, Me.ToolbarB_limpiardetalle, Me.LabelItem_msg})
        Me.Bar2.Location = New System.Drawing.Point(2, 2)
        Me.Bar2.MenuBar = True
        Me.Bar2.Name = "Bar2"
        Me.Bar2.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar2.Size = New System.Drawing.Size(1084, 26)
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar2.TabIndex = 45
        Me.Bar2.TabStop = False
        Me.Bar2.Text = "Bar2"
        '
        'LabelItem_msg
        '
        Me.LabelItem_msg.DividerStyle = True
        Me.LabelItem_msg.ForeColor = System.Drawing.Color.Yellow
        Me.LabelItem_msg.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.LabelItem_msg.Name = "LabelItem_msg"
        Me.LabelItem_msg.Stretch = True
        Me.LabelItem_msg.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ComboB_Tipo_Tabdetalle
        '
        Me.ComboB_Tipo_Tabdetalle.DisplayMember = "Text"
        Me.ComboB_Tipo_Tabdetalle.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_Tipo_Tabdetalle.FormattingEnabled = True
        Me.ComboB_Tipo_Tabdetalle.ItemHeight = 15
        Me.ComboB_Tipo_Tabdetalle.Location = New System.Drawing.Point(765, 85)
        Me.ComboB_Tipo_Tabdetalle.Name = "ComboB_Tipo_Tabdetalle"
        Me.ComboB_Tipo_Tabdetalle.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_Tipo_Tabdetalle.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_Tipo_Tabdetalle.TabIndex = 4
        '
        'ComboB_codmoneda_Tabdetalle
        '
        Me.ComboB_codmoneda_Tabdetalle.DisplayMember = "Text"
        Me.ComboB_codmoneda_Tabdetalle.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_codmoneda_Tabdetalle.FormattingEnabled = True
        Me.ComboB_codmoneda_Tabdetalle.ItemHeight = 15
        Me.ComboB_codmoneda_Tabdetalle.Location = New System.Drawing.Point(765, 50)
        Me.ComboB_codmoneda_Tabdetalle.Name = "ComboB_codmoneda_Tabdetalle"
        Me.ComboB_codmoneda_Tabdetalle.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_codmoneda_Tabdetalle.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_codmoneda_Tabdetalle.TabIndex = 3
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(643, 81)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(94, 23)
        Me.LabelX1.TabIndex = 40
        Me.LabelX1.Text = "Tipo Cuenta"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(646, 50)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(94, 23)
        Me.LabelX2.TabIndex = 39
        Me.LabelX2.Text = "Moneda Cuenta"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.Class = ""
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(68, 84)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(107, 23)
        Me.LabelX3.TabIndex = 38
        Me.LabelX3.Text = "Nombre Cuenta"
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.Class = ""
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(68, 50)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(101, 23)
        Me.LabelX4.TabIndex = 37
        Me.LabelX4.Text = "Cuenta Contable"
        '
        'TextB_Desccuenta_Tabdetalle
        '
        Me.TextB_Desccuenta_Tabdetalle.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_Desccuenta_Tabdetalle.Border.Class = "TextBoxBorder"
        Me.TextB_Desccuenta_Tabdetalle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_Desccuenta_Tabdetalle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_Desccuenta_Tabdetalle.Location = New System.Drawing.Point(178, 86)
        Me.TextB_Desccuenta_Tabdetalle.Name = "TextB_Desccuenta_Tabdetalle"
        Me.TextB_Desccuenta_Tabdetalle.Size = New System.Drawing.Size(425, 21)
        Me.TextB_Desccuenta_Tabdetalle.TabIndex = 2
        '
        'TextB_cdgocuenta_Tabdetalle
        '
        Me.TextB_cdgocuenta_Tabdetalle.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_cdgocuenta_Tabdetalle.Border.Class = "TextBoxBorder"
        Me.TextB_cdgocuenta_Tabdetalle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_cdgocuenta_Tabdetalle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_cdgocuenta_Tabdetalle.Location = New System.Drawing.Point(178, 47)
        Me.TextB_cdgocuenta_Tabdetalle.Name = "TextB_cdgocuenta_Tabdetalle"
        Me.TextB_cdgocuenta_Tabdetalle.Size = New System.Drawing.Size(425, 21)
        Me.TextB_cdgocuenta_Tabdetalle.TabIndex = 1
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'SuperTabItem_detalle
        '
        Me.SuperTabItem_detalle.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem_detalle.GlobalItem = False
        Me.SuperTabItem_detalle.Name = "SuperTabItem_detalle"
        Me.SuperTabItem_detalle.Text = "Detalle/Nuevo"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.ExpandablePanel3)
        Me.SuperTabControlPanel2.Controls.Add(Me.ExpandablePanel2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(1088, 448)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem_Buscar
        '
        'ExpandablePanel3
        '
        Me.ExpandablePanel3.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel3.Controls.Add(Me.ItemPanel4)
        Me.ExpandablePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel3.ExpandOnTitleClick = True
        Me.ExpandablePanel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel3.Location = New System.Drawing.Point(0, 149)
        Me.ExpandablePanel3.Name = "ExpandablePanel3"
        Me.ExpandablePanel3.Size = New System.Drawing.Size(1088, 295)
        Me.ExpandablePanel3.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel3.Style.BackColor1.Color = System.Drawing.SystemColors.MenuHighlight
        Me.ExpandablePanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel3.Style.GradientAngle = 90
        Me.ExpandablePanel3.TabIndex = 2
        Me.ExpandablePanel3.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel3.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel3.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel3.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel3.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel3.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel3.TitleText = "Registros Encontrados"
        '
        'ItemPanel4
        '
        '
        '
        '
        Me.ItemPanel4.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel4.BackgroundStyle.Class = ""
        Me.ItemPanel4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel4.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel4.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel4.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel4.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel4.ContainerControlProcessDialogKey = True
        Me.ItemPanel4.Controls.Add(Me.GridP_Catalogo)
        Me.ItemPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel4.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel4.Name = "ItemPanel4"
        Me.ItemPanel4.Size = New System.Drawing.Size(1088, 269)
        Me.ItemPanel4.TabIndex = 3
        Me.ItemPanel4.Text = "ItemPanel4"
        '
        'GridP_Catalogo
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_Catalogo.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.GridP_Catalogo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_Catalogo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.GridP_Catalogo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_Catalogo.DefaultCellStyle = DataGridViewCellStyle3
        Me.GridP_Catalogo.EnableHeadersVisualStyles = False
        Me.GridP_Catalogo.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_Catalogo.Location = New System.Drawing.Point(4, 4)
        Me.GridP_Catalogo.Name = "GridP_Catalogo"
        Me.GridP_Catalogo.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_Catalogo.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.GridP_Catalogo.RowHeadersWidth = 20
        Me.GridP_Catalogo.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_Catalogo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_Catalogo.Size = New System.Drawing.Size(1080, 259)
        Me.GridP_Catalogo.TabIndex = 0
        '
        'ExpandablePanel2
        '
        Me.ExpandablePanel2.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel2.Controls.Add(Me.ItemPanel3)
        Me.ExpandablePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel2.ExpandOnTitleClick = True
        Me.ExpandablePanel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel2.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel2.Name = "ExpandablePanel2"
        Me.ExpandablePanel2.Size = New System.Drawing.Size(1088, 149)
        Me.ExpandablePanel2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel2.Style.BackColor1.Color = System.Drawing.Color.SteelBlue
        Me.ExpandablePanel2.Style.BackColor2.Color = System.Drawing.Color.SteelBlue
        Me.ExpandablePanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel2.Style.GradientAngle = 90
        Me.ExpandablePanel2.TabIndex = 1
        Me.ExpandablePanel2.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel2.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel2.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel2.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.ExpandablePanel2.TitleStyle.BorderWidth = 0
        Me.ExpandablePanel2.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel2.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel2.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel2.TitleText = "Opciones de B�squedas"
        '
        'ItemPanel3
        '
        '
        '
        '
        Me.ItemPanel3.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel3.BackgroundStyle.Class = ""
        Me.ItemPanel3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel3.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel3.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel3.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel3.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel3.ContainerControlProcessDialogKey = True
        Me.ItemPanel3.Controls.Add(Me.Bar1)
        Me.ItemPanel3.Controls.Add(Me.ComboB_Tipo_TabBusquedas)
        Me.ItemPanel3.Controls.Add(Me.ComboB_codmoneda_TabBusquedas)
        Me.ItemPanel3.Controls.Add(Me.LabelX25)
        Me.ItemPanel3.Controls.Add(Me.LabelX24)
        Me.ItemPanel3.Controls.Add(Me.LabelX23)
        Me.ItemPanel3.Controls.Add(Me.LabelX22)
        Me.ItemPanel3.Controls.Add(Me.TextB_Desccuenta)
        Me.ItemPanel3.Controls.Add(Me.TextB_CuentaContable)
        Me.ItemPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem2})
        Me.ItemPanel3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel3.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel3.Name = "ItemPanel3"
        Me.ItemPanel3.Size = New System.Drawing.Size(1088, 123)
        Me.ItemPanel3.TabIndex = 0
        Me.ItemPanel3.Text = "ItemPanel3"
        '
        'Bar1
        '
        Me.Bar1.AccessibleDescription = "Bar1 (Bar1)"
        Me.Bar1.AccessibleName = "Bar1"
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar1.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_BuscarCuenta, Me.ToolbarB_limpiarbusqueda, Me.ButtonI_ImprimirCatalogo})
        Me.Bar1.Location = New System.Drawing.Point(2, 2)
        Me.Bar1.MenuBar = True
        Me.Bar1.Name = "Bar1"
        Me.Bar1.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar1.Size = New System.Drawing.Size(1084, 26)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar1.TabIndex = 45
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'ComboB_Tipo_TabBusquedas
        '
        Me.ComboB_Tipo_TabBusquedas.DisplayMember = "Text"
        Me.ComboB_Tipo_TabBusquedas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_Tipo_TabBusquedas.FormattingEnabled = True
        Me.ComboB_Tipo_TabBusquedas.ItemHeight = 15
        Me.ComboB_Tipo_TabBusquedas.Location = New System.Drawing.Point(765, 85)
        Me.ComboB_Tipo_TabBusquedas.Name = "ComboB_Tipo_TabBusquedas"
        Me.ComboB_Tipo_TabBusquedas.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_Tipo_TabBusquedas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_Tipo_TabBusquedas.TabIndex = 4
        '
        'ComboB_codmoneda_TabBusquedas
        '
        Me.ComboB_codmoneda_TabBusquedas.DisplayMember = "Text"
        Me.ComboB_codmoneda_TabBusquedas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_codmoneda_TabBusquedas.FormattingEnabled = True
        Me.ComboB_codmoneda_TabBusquedas.ItemHeight = 15
        Me.ComboB_codmoneda_TabBusquedas.Location = New System.Drawing.Point(765, 50)
        Me.ComboB_codmoneda_TabBusquedas.Name = "ComboB_codmoneda_TabBusquedas"
        Me.ComboB_codmoneda_TabBusquedas.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_codmoneda_TabBusquedas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_codmoneda_TabBusquedas.TabIndex = 3
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.Class = ""
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(643, 81)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(94, 23)
        Me.LabelX25.TabIndex = 40
        Me.LabelX25.Text = "Tipo Cuenta"
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.Class = ""
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(646, 50)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(94, 23)
        Me.LabelX24.TabIndex = 39
        Me.LabelX24.Text = "Moneda Cuenta"
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.Class = ""
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(41, 84)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(119, 23)
        Me.LabelX23.TabIndex = 38
        Me.LabelX23.Text = "Nombre Cuenta"
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.Class = ""
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(44, 50)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(113, 23)
        Me.LabelX22.TabIndex = 37
        Me.LabelX22.Text = "Cuenta Contable"
        '
        'TextB_Desccuenta
        '
        Me.TextB_Desccuenta.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_Desccuenta.Border.Class = "TextBoxBorder"
        Me.TextB_Desccuenta.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_Desccuenta.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_Desccuenta.Location = New System.Drawing.Point(178, 86)
        Me.TextB_Desccuenta.Name = "TextB_Desccuenta"
        Me.TextB_Desccuenta.Size = New System.Drawing.Size(425, 21)
        Me.TextB_Desccuenta.TabIndex = 2
        '
        'TextB_CuentaContable
        '
        Me.TextB_CuentaContable.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_CuentaContable.Border.Class = "TextBoxBorder"
        Me.TextB_CuentaContable.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_CuentaContable.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_CuentaContable.Location = New System.Drawing.Point(178, 47)
        Me.TextB_CuentaContable.Name = "TextB_CuentaContable"
        Me.TextB_CuentaContable.Size = New System.Drawing.Size(425, 21)
        Me.TextB_CuentaContable.TabIndex = 1
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'SuperTabItem_Buscar
        '
        Me.SuperTabItem_Buscar.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem_Buscar.GlobalItem = False
        Me.SuperTabItem_Buscar.Name = "SuperTabItem_Buscar"
        Me.SuperTabItem_Buscar.Text = "B�squedas"
        '
        'LabelItem_pantalla
        '
        Me.LabelItem_pantalla.Enabled = False
        Me.LabelItem_pantalla.Name = "LabelItem_pantalla"
        Me.LabelItem_pantalla.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ToolbarB_GuardarCuenta
        '
        Me.ToolbarB_GuardarCuenta.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_GuardarCuenta.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_GuardarCuenta.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.DDORes_dll_21_101
        Me.ToolbarB_GuardarCuenta.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_GuardarCuenta.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_GuardarCuenta.Name = "ToolbarB_GuardarCuenta"
        Me.ToolbarB_GuardarCuenta.Text = "Guardar/Actualizar Cuenta"
        '
        'ToolbarB_limpiardetalle
        '
        Me.ToolbarB_limpiardetalle.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_limpiardetalle.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_limpiardetalle.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ResourceForkEraser
        Me.ToolbarB_limpiardetalle.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_limpiardetalle.Name = "ToolbarB_limpiardetalle"
        Me.ToolbarB_limpiardetalle.Text = "Limpiar Detalle"
        '
        'ToolbarB_BuscarCuenta
        '
        Me.ToolbarB_BuscarCuenta.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_BuscarCuenta.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_BuscarCuenta.Image = CType(resources.GetObject("ToolbarB_BuscarCuenta.Image"), System.Drawing.Image)
        Me.ToolbarB_BuscarCuenta.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_BuscarCuenta.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_BuscarCuenta.Name = "ToolbarB_BuscarCuenta"
        Me.ToolbarB_BuscarCuenta.Text = "Buscar Cuenta"
        '
        'ToolbarB_limpiarbusqueda
        '
        Me.ToolbarB_limpiarbusqueda.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_limpiarbusqueda.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_limpiarbusqueda.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ResourceForkEraser
        Me.ToolbarB_limpiarbusqueda.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_limpiarbusqueda.Name = "ToolbarB_limpiarbusqueda"
        Me.ToolbarB_limpiarbusqueda.Text = "Limpiar Controles"
        '
        'ButtonI_ImprimirCatalogo
        '
        Me.ButtonI_ImprimirCatalogo.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_ImprimirCatalogo.ForeColor = System.Drawing.Color.White
        Me.ButtonI_ImprimirCatalogo.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ButtonI_ImprimirCatalogo.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_ImprimirCatalogo.Name = "ButtonI_ImprimirCatalogo"
        Me.ButtonI_ImprimirCatalogo.Text = "Imprimir Cat�logo"
        '
        'ButtonItem_NuevaCuenta
        '
        Me.ButtonItem_NuevaCuenta.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_NuevaCuenta.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Add
        Me.ButtonItem_NuevaCuenta.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_NuevaCuenta.Name = "ButtonItem_NuevaCuenta"
        Me.ButtonItem_NuevaCuenta.Text = "Agregar Cuenta"
        '
        'ButtonItem_menuprincipal
        '
        Me.ButtonItem_menuprincipal.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_menuprincipal.Image = CType(resources.GetObject("ButtonItem_menuprincipal.Image"), System.Drawing.Image)
        Me.ButtonItem_menuprincipal.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_menuprincipal.Name = "ButtonItem_menuprincipal"
        Me.ButtonItem_menuprincipal.Text = "Men� Principal"
        Me.ButtonItem_menuprincipal.Tooltip = "Retorna al Men� Principal de la Contabilidad"
        '
        'ButtonItem_cerrarpantalla
        '
        Me.ButtonItem_cerrarpantalla.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_cerrarpantalla.Image = CType(resources.GetObject("ButtonItem_cerrarpantalla.Image"), System.Drawing.Image)
        Me.ButtonItem_cerrarpantalla.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_cerrarpantalla.Name = "ButtonItem_cerrarpantalla"
        Me.ButtonItem_cerrarpantalla.Text = "Eliminar"
        Me.ButtonItem_cerrarpantalla.Tooltip = "Elimina cuenta contable"
        '
        'btnSalir
        '
        Me.btnSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSalir.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.salir
        Me.btnSalir.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.Tooltip = "Cierra la Ventana y Sale de la Aplicaci�n"
        '
        'frmCatalogoContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1100, 490)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.DoubleBuffered = True
        Me.Name = "frmCatalogoContable"
        Me.Text = "frmCatalogoContable"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.ExpandablePanel1.ResumeLayout(False)
        Me.ItemPanel1.ResumeLayout(False)
        Me.ItemPanel5.ResumeLayout(False)
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ExpandablePanel3.ResumeLayout(False)
        Me.ItemPanel4.ResumeLayout(False)
        CType(Me.GridP_Catalogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExpandablePanel2.ResumeLayout(False)
        Me.ItemPanel3.ResumeLayout(False)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents ExpandablePanel3 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents ItemPanel4 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents GridP_Catalogo As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ExpandablePanel2 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents ItemPanel3 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_BuscarCuenta As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_limpiarbusqueda As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonI_ImprimirCatalogo As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ComboB_Tipo_TabBusquedas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboB_codmoneda_TabBusquedas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_Desccuenta As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TextB_CuentaContable As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents SuperTabItem_Buscar As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents ExpandablePanel1 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents ItemPanel1 As DevComponents.DotNetBar.ItemPanel
    Private WithEvents ComboB_categoria As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label_reaumensaldos As DevComponents.DotNetBar.LabelX
    Friend WithEvents ItemPanel5 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_saldoiniNac As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_saldofinNac As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_haberNac As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_debeNac As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_GuardarCuenta As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_limpiardetalle As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem_msg As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ComboB_Tipo_Tabdetalle As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboB_codmoneda_Tabdetalle As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_Desccuenta_Tabdetalle As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TextB_cdgocuenta_Tabdetalle As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents SuperTabItem_detalle As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ButtonItem_NuevaCuenta As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_menuprincipal As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_cerrarpantalla As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem_pantalla As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnSalir As DevComponents.DotNetBar.ButtonItem
End Class
