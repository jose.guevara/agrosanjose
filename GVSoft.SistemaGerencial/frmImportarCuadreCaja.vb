Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio


Public Class frmImportarCuadreCaja

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String
    Dim strNombAlmacenar As String

    Private Sub frmImportarCuadreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtFacturaInicial.ReadOnly = True
        txtFacturaFinal.ReadOnly = True
        txtReciboInicial.ReadOnly = True
        txtReciboFinal.ReadOnly = True
        txtDeposito.ReadOnly = True
        txtSaldoInicial.ReadOnly = True
        txtGastos.ReadOnly = True

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If

        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K

    End Sub

    Private Sub frmImportarCuadreCaja_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            ObtenerArchivo()
        ElseIf e.KeyCode = Keys.F4 Then
            ProcesarArchivo()
        ElseIf e.KeyCode = Keys.F5 Then
            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If
    End Sub

    Private Sub cmdSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportar.Click
        ObtenerArchivo()
    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        ProcesarArchivo()
    End Sub

    Sub Limpiar()

        txtMensajeError.Text = ""
        txtFacturaInicial.Text = ""
        txtFacturaFinal.Text = ""
        txtReciboInicial.Text = ""
        txtReciboFinal.Text = ""
        txtDeposito.Text = ""
        txtSaldoInicial.Text = ""
        txtGastos.Text = ""
        dgvxDetalleCuadreCaja.Rows.Clear()

    End Sub

    Sub ObtenerArchivo()
        Dim dlgNew As New OpenFileDialog
        Dim strLine, strDrive As String

        Dim strArchivo, strArchivo2, strClave, strComando As String

        Dim IdCuadre, IdUsuario, IdSucursal, IdUsuarioIngresa, IdUsuarioModifica, IdUsuarioAnula As Integer
        Dim IdUltimoCuadre, FechaDocumentoNum, IdUsuarioDocumento, IdEstadoDocumento, CantC_500 As Integer
        Dim CantC_200, CantC_100, CantC_50, CantC_20, CantC_10, CantC_5, CantC_1, CantC_050 As Integer
        Dim CantC_025, CantC_010, CantC_005, CantC_001, CantD_100, CantD_50, CantD_20, CantD_10 As Integer
        Dim CantD_5, CantD_1, CantD_050, CantD_010, IndicaError As Integer
        Dim IdMoneda, FechaInicioCuadreNum, FechaCuadreFinNum, FacturaCreditoInicial, FacturaCreditoFinal As String
        Dim FacturaContadoInicial, FacturaContadoFinal, ReciboInicialCredito, ReciboFinalCredito As String
        Dim ReciboInicialDebito, ReciboFinalDebito, Observacion, FechaModificaNum, FechaAnulaNum As String
        Dim FechaIngresoNum, NumeroDocumento, IdMonedaC, IdMonedaD, RegistroError, NumeroCuadre As String
        Dim IdEstado, IdTipoDocumento As Char

        Dim FechaInicioCuadre, HoraInicioCuadre, FechaCuadreFin, HoraCuadreFin, FechaModifica, HoraModifica, FechaAnula, HoraAnula, FechaIngreso, HoraIngreso, FechaDocumento, HoraDocumento As String

        Dim SaldoInicial, MontoTotalVentas, MontoTotalRecibos, MontoGastos, MontoDepositos, OtrosIngresos, SobranteCaja As Double
        Dim MontoTotalDineroCaja, MontoTotalIngreso, MontoTotalEgreso, SaldoFinal, Tasa As Double
        Dim MontoTotalDocumento, DenC_500, DenC_200, DenC_100, DenC_50, DenC_20, DenC_10, DenC_5, DenC_1 As Double
        Dim DenC_050, DenC_025, DenC_010, DenC_005, DenC_001, DenD_100, DenD_50, DenD_20, DenD_10 As Double
        Dim DenD_5, DenD_1, DenD_050, DenD_010 As Double

        Dim Columnas As String()

        dlgNew.InitialDirectory = "C:\"
        dlgNew.Filter = "ZIP files (*.zip)|*.zip"
        dlgNew.RestoreDirectory = True
        If dlgNew.ShowDialog() <> DialogResult.OK Then
            MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
            Exit Sub
        End If
        strDrive = String.Empty
        strComando = String.Empty
        strArchivo = String.Empty
        strArchivo2 = String.Empty
        strClave = String.Empty
        intIncr = 0
        intIncr = InStr(dlgNew.FileName, "CuadreCaja_")
        strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
        If intIncr = 0 Then
            MsgBox("No es un archivo que contiene los Arqueos de Caja. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        cbNumerosCuadre.Items.Clear()
        ComboBox2.Items.Clear()
        strArchivo = dlgNew.FileName
        strClave = "Agro2K_2008"
        intIncr = 0
        intIncr = InStrRev(dlgNew.FileName, "\")
        strDrive = ""
        strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
        strNombAlmacenar = ""
        strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
        intIncr = 0
        'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
        ' intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
        If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
            MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
            Exit Sub
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "truncate table tmpCuadreCaja"
        cmdAgro2K.CommandText = strQuery
        cmdAgro2K.ExecuteNonQuery()
        strNombArchivo = ""
        strNombArchivo = strArchivo2
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        intIncr = 0
        intIncr = InStrRev(strNombArchivo, "\")
        strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
        strQuery = ""
        strQuery = "select * from tbl_ArchivosImp where archivo = '" & strNombAlmacenar & "'"
        cmdAgro2K.CommandText = strQuery
        Try
            intError = 0
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                intError = 1
            End While
            dtrAgro2K.Close()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        If intError = 1 Then
            MsgBox("Archivo ya fue procesado.  Verifique.", MsgBoxStyle.Critical, "Error de Archivo")
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim sr As StreamReader = File.OpenText(strArchivo2)
        intIncr = 0
        intError = 0
        Try

            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                Columnas = strLine.Trim().Split("|")
                IdCuadre = 0
                IdCuadre = ConvierteAInt(Columnas(0).ToString())
                IdMoneda = String.Empty
                IdMoneda = Columnas(1).ToString()
                IdUsuario = 0
                IdUsuario = ConvierteAInt(Columnas(2).ToString())
                IdSucursal = 0
                IdSucursal = Columnas(3).ToString()
                NumeroCuadre = Columnas(4).ToString()
                FechaInicioCuadre = Columnas(5).ToString()
                FechaInicioCuadreNum = String.Empty
                FechaInicioCuadreNum = Columnas(6).ToString()
                FechaCuadreFin = Columnas(7).ToString()
                FechaCuadreFinNum = String.Empty
                FechaCuadreFinNum = Columnas(8).ToString()
                FacturaCreditoInicial = String.Empty
                FacturaCreditoInicial = Columnas(9).ToString()
                FacturaCreditoFinal = String.Empty
                FacturaCreditoFinal = Columnas(10).ToString()
                FacturaContadoInicial = String.Empty
                FacturaContadoInicial = Columnas(11).ToString()
                FacturaContadoFinal = String.Empty
                FacturaContadoFinal = Columnas(12).ToString()
                ReciboInicialCredito = String.Empty
                ReciboInicialCredito = Columnas(13).ToString()
                ReciboFinalCredito = Columnas(14).ToString()
                ReciboInicialDebito = Columnas(15).ToString()
                ReciboFinalDebito = Columnas(16).ToString()
                SaldoInicial = ConvierteADouble(Columnas(17).ToString())
                MontoTotalVentas = ConvierteADouble(Columnas(18).ToString())
                MontoTotalRecibos = ConvierteADouble(Columnas(19).ToString())
                MontoGastos = ConvierteADouble(Columnas(20).ToString())
                MontoDepositos = ConvierteADouble(Columnas(21).ToString())
                MontoTotalDineroCaja = ConvierteADouble(Columnas(22).ToString())
                MontoTotalIngreso = ConvierteADouble(Columnas(23).ToString())
                MontoTotalEgreso = ConvierteADouble(Columnas(24).ToString())
                SaldoFinal = ConvierteADouble(Columnas(25).ToString())
                Tasa = ConvierteADouble(Columnas(26).ToString())
                Observacion = quitarSaltosLinea(Columnas(27).ToString())
                IdEstado = Columnas(28).ToString()
                FechaModifica = Columnas(29).ToString()
                FechaModificaNum = Columnas(30).ToString()
                FechaAnula = Columnas(31).ToString()
                FechaAnulaNum = Columnas(32).ToString()
                FechaIngreso = Columnas(33).ToString()
                FechaIngresoNum = Columnas(34).ToString()
                IdUsuarioIngresa = ConvierteAInt(Columnas(35).ToString())
                IdUsuarioModifica = ConvierteAInt(Columnas(36).ToString())
                IdUsuarioAnula = ConvierteAInt(Columnas(37).ToString())
                IdUltimoCuadre = ConvierteAInt(Columnas(38).ToString())
                NumeroDocumento = Columnas(39).ToString()
                MontoTotalDocumento = ConvierteADouble(Columnas(40).ToString())
                IdTipoDocumento = Columnas(41).ToString()
                FechaDocumento = Columnas(42).ToString()
                FechaDocumentoNum = ConvierteAInt(Columnas(43).ToString())
                IdUsuarioDocumento = ConvierteAInt(Columnas(44).ToString())
                IdEstadoDocumento = ConvierteAInt(Columnas(45).ToString())
                IdMonedaC = Columnas(46).ToString()
                DenC_500 = ConvierteADouble(Columnas(47).ToString())
                CantC_500 = ConvierteAInt(Columnas(48).ToString())
                DenC_200 = ConvierteADouble(Columnas(49).ToString())
                CantC_200 = ConvierteAInt(Columnas(50).ToString())
                DenC_100 = ConvierteADouble(Columnas(51).ToString())
                CantC_100 = ConvierteAInt(Columnas(52).ToString())
                DenC_50 = ConvierteADouble(Columnas(53).ToString())
                CantC_50 = ConvierteAInt(Columnas(54).ToString())
                DenC_20 = ConvierteADouble(Columnas(55).ToString())
                CantC_20 = ConvierteAInt(Columnas(56).ToString())
                DenC_10 = ConvierteADouble(Columnas(57).ToString())
                CantC_10 = ConvierteAInt(Columnas(58).ToString())
                DenC_5 = ConvierteADouble(Columnas(59).ToString())
                CantC_5 = ConvierteAInt(Columnas(60).ToString())
                DenC_1 = ConvierteADouble(Columnas(61).ToString())
                CantC_1 = ConvierteAInt(Columnas(62).ToString())
                DenC_050 = ConvierteADouble(Columnas(63).ToString())
                CantC_050 = ConvierteAInt(Columnas(64).ToString())
                DenC_025 = ConvierteADouble(Columnas(65).ToString())
                CantC_025 = ConvierteAInt(Columnas(66).ToString())
                DenC_010 = ConvierteADouble(Columnas(67).ToString())
                CantC_010 = ConvierteAInt(Columnas(68).ToString())
                DenC_005 = ConvierteADouble(Columnas(69).ToString())
                CantC_005 = ConvierteAInt(Columnas(70).ToString())
                DenC_001 = ConvierteADouble(Columnas(71))
                CantC_001 = ConvierteAInt(Columnas(72).ToString())
                IdMonedaD = Columnas(73).ToString()
                DenD_100 = ConvierteADouble(Columnas(74).ToString())
                CantD_100 = ConvierteAInt(Columnas(75).ToString())
                DenD_50 = ConvierteADouble(Columnas(76).ToString())
                CantD_50 = ConvierteAInt(Columnas(77).ToString())
                DenD_20 = ConvierteADouble(Columnas(78).ToString())
                CantD_20 = ConvierteAInt(Columnas(79).ToString())
                DenD_10 = ConvierteADouble(Columnas(80).ToString())
                CantD_10 = ConvierteAInt(Columnas(81).ToString())
                DenD_5 = ConvierteADouble(Columnas(82).ToString())
                CantD_5 = ConvierteAInt(Columnas(83).ToString())
                DenD_1 = ConvierteADouble(Columnas(84).ToString())
                CantD_1 = ConvierteAInt(Columnas(85).ToString())
                DenD_050 = ConvierteADouble(Columnas(86).ToString())
                CantD_050 = ConvierteAInt(Columnas(87).ToString())
                DenD_010 = ConvierteADouble(Columnas(88).ToString())
                CantD_010 = ConvierteAInt(Columnas(89).ToString())
                HoraInicioCuadre = Columnas(90).ToString()
                HoraCuadreFin = Columnas(91).ToString()
                HoraModifica = Columnas(92).ToString()
                HoraAnula = Columnas(93).ToString()
                HoraIngreso = Columnas(94).ToString()
                HoraDocumento = Columnas(95).ToString()
                OtrosIngresos = ConvierteADouble(Columnas(96).ToString())
                SobranteCaja = ConvierteADouble(Columnas(97).ToString())
                RegistroError = "Sin Error"
                IndicaError = 0

                strQuery = ""
                strQuery = "exec spIngresaImportarCuadreCaja " & IdCuadre & ", '" & IdMoneda & "', "
                strQuery = strQuery + "" & IdUsuario & ", " & IdSucursal & ", '" & NumeroCuadre & "', '" & FechaInicioCuadre & "', '" & FechaInicioCuadreNum & "', "
                strQuery = strQuery + "'" & FechaCuadreFin & "', '" & FechaCuadreFinNum & "', '" & FacturaCreditoInicial & "', "
                strQuery = strQuery + "'" & FacturaCreditoFinal & "', '" & FacturaContadoInicial & "', '" & FacturaContadoFinal & "', '" & ReciboInicialCredito & " ', "
                strQuery = strQuery + "'" & ReciboFinalCredito & "', ' " & ReciboInicialDebito & " ', '" & ReciboFinalDebito & " ', "
                strQuery = strQuery + "" & SaldoInicial & ", " & MontoTotalVentas & ", " & MontoTotalRecibos & ", " & MontoGastos & ", " & MontoDepositos & ", "
                strQuery = strQuery + "" & MontoTotalDineroCaja & ", " & MontoTotalIngreso & ", " & MontoTotalEgreso & ", " & SaldoFinal & ", " & Tasa & ", '" & Observacion & " ', "
                strQuery = strQuery + "'" & IdEstado & "', '" & FechaModifica & "', '" & FechaModificaNum & " ', '" & FechaAnula & "', "
                strQuery = strQuery + "'" & FechaAnulaNum & "','" & FechaIngreso & "', '" & FechaIngresoNum & "', " & IdUsuarioIngresa & ", "
                strQuery = strQuery + "" & IdUsuarioModifica & "," & IdUsuarioAnula & "," & IdUltimoCuadre & ", '" & NumeroDocumento & "', " & MontoTotalDocumento & ", '" & IdTipoDocumento & "', '" & FechaDocumento & "', "
                strQuery = strQuery + "" & FechaDocumentoNum & "," & IdUsuarioDocumento & "," & IdEstadoDocumento & ", '" & IdMonedaC & "', " & DenC_500 & ", " & CantC_500 & ", " & DenC_200 & ", "
                strQuery = strQuery + "" & CantC_200 & "," & DenC_100 & "," & CantC_100 & ", " & DenC_50 & ", " & CantC_50 & ", " & DenC_20 & ", " & CantC_20 & ", " & DenC_10 & ", " & CantC_10 & ", "
                strQuery = strQuery + "" & DenC_5 & "," & CantC_5 & "," & DenC_1 & ", " & CantC_1 & ", " & DenC_050 & ", " & CantC_050 & ", " & DenC_025 & ", " & CantC_025 & ", " & DenC_010 & ", "
                strQuery = strQuery + "" & CantC_010 & "," & DenC_005 & "," & CantC_005 & ", " & DenC_001 & ", " & CantC_001 & ", '" & IdMonedaD & "', " & DenD_100 & ", " & CantD_100 & ", " & DenD_50 & ", "
                strQuery = strQuery + "" & CantD_50 & "," & DenD_20 & "," & CantD_20 & ", " & DenD_10 & ", " & CantD_10 & ", " & DenD_5 & ", " & CantD_5 & ", " & DenD_1 & ", " & CantD_1 & ", "
                strQuery = strQuery + "" & DenD_050 & "," & CantD_050 & "," & DenD_010 & ", " & CantD_010 & ", '" & HoraInicioCuadre & "', '" & HoraCuadreFin & "', "
                strQuery = strQuery + "'" & HoraModifica & "', '" & HoraAnula & "','" & HoraIngreso & "', '" & HoraDocumento & "', '" & RegistroError & "', " & IndicaError & ", " & OtrosIngresos & ", " & SobranteCaja & " "

                Try
                    cmdAgro2K.CommandText = strQuery
                    cmdAgro2K.ExecuteNonQuery()
                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try
            Loop
            sr.Close()
            ValidarDatosImportados()
            CargarRecibos()
            CargarResumen()
            If intError = 0 Then
                'ValidarDatosImportados()
                ''CargarResumen()
                'CargarRecibos()
                MsgBox("Finalizado satisfactoriamente la importación.", MsgBoxStyle.Information, "Proceso Finalizado")
            Else
                MsgBox("Hubieron errores al importar el archivo.", MsgBoxStyle.Critical, "Error en la Importación")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If
            Me.Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error de Archivo")
            Me.Cursor = Cursors.Default
            Exit Sub

        End Try

    End Sub

    Sub ValidarDatosImportados()

        Dim cmdTmp As New SqlCommand("spValidaImportarCuadreCaja", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter

        intError = 0

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                If ConvierteAInt(dtrAgro2K.GetValue(0)) = 0 Then
                    intError = 0
                ElseIf ConvierteAInt(dtrAgro2K.GetValue(0)) <> 254 Then
                    intError = 1
                End If
            End While

            dtrAgro2K.Close()

        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try

    End Sub

    Sub CargarResumen()
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "exec spObtieneResumenCuadreAImportar "
        
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtTotalCantidadCuadre.Text = Format(ConvierteADouble(dtrAgro2K.Item("TotalArqueos")), "##,##0")
                txtMontoTotalCuadres.Text = Format(ConvierteADouble(dtrAgro2K.Item("MontoTotalArqueos")), "##,##0.#0")
            End While
            dtrAgro2K.Close()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try

    End Sub

    Sub CargarRecibos()

        cbNumerosCuadre.Items.Clear()
        ComboBox2.Items.Clear()
        strQuery = ""
        strQuery = "exec spObtienCuadresACargar"
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                cbNumerosCuadre.Items.Add(dtrAgro2K.GetValue(0))
                'ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
            End While
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub CargarDetalleCuadreImportar()
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "exec spObtieneDetalleCuadreAImportar " & cbNumerosCuadre.SelectedItem & ""
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                dgvxDetalleCuadreCaja.Rows.Add(dtrAgro2K.Item("NumeroFactura"), Format(dtrAgro2K.Item("MontoFactura"), "##,##0.#0"), dtrAgro2K.Item("NumeroRecibo"), Format(dtrAgro2K.Item("MontoRecibo"), "##,##0.#0"))
            End While
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub cbNumerosCuadre_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNumerosCuadre.SelectedIndexChanged

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        Limpiar()
        'ComboBox2.SelectedIndex = cbNumerosCuadre.SelectedIndex
        txtMensajeError.Text = "Sin Error"
        strQuery = ""
        strQuery = "exec spObtieneEncabezadoCuadreAImportar " & cbNumerosCuadre.SelectedItem & ""
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtFacturaInicial.Text = dtrAgro2K.Item("FacturaContadoInicial")
                txtFacturaFinal.Text = dtrAgro2K.Item("FacturaContadoFinal")
                txtReciboInicial.Text = dtrAgro2K.Item("ReciboInicialCredito")
                txtReciboFinal.Text = dtrAgro2K.Item("ReciboFinalCredito")
                txtSaldoInicial.Text = Format(dtrAgro2K.Item("SaldoInicial"), "##,##0.#0")
                txtGastos.Text = Format(dtrAgro2K.Item("MontoGastos"), "##,##0.#0")
                txtDeposito.Text = Format(dtrAgro2K.Item("MontoDepositos"), "##,##0.#0")
                lblFechaArqueo.Text = dtrAgro2K.Item("FechaInicioCuadre").ToString()
                'dgvxDetalleCuadreCaja.Rows.Add(dtrAgro2K.Item("NumeroFactura"), Format(dtrAgro2K.Item("monto"), "##,##0.#0"), dtrAgro2K.Item("FechaIngFormateada"))

                If dtrAgro2K.Item("IndicaError") Then
                    txtMensajeError.Text = dtrAgro2K.Item("RegistroError")
                End If
            End While
            CargarDetalleCuadreImportar()

        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub ProcesarArchivo()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("spValidaImportarCuadreCaja", cnnAgro2K)
        Dim cmdTmp2 As New SqlCommand("spImportarCuadresCajas", cnnAgro2K)


        Dim prmTmp01 As New SqlParameter
        Dim bytContinuar As Byte

        bytContinuar = 0

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                If dtrAgro2K.GetValue(0) = 0 Then
                    MsgBox("No hay errores en la validación de los Cuadres a Importar. Se procedera a ingresar los Cuadres.", MsgBoxStyle.Information, "Validación de Datos")
                ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                    bytContinuar = 1
                    MsgBox("Continuan los errores en la validación de los Cuadres a Importar", MsgBoxStyle.Critical, "Validación de Datos")
                    Exit Sub
                End If
            End While
            dtrAgro2K.Close()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
            dtrAgro2K.Close()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Exit Sub
        End Try
        If bytContinuar = 0 Then
            If cmdTmp2.Connection.State = ConnectionState.Open Then
                cmdTmp2.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp2.Connection = cnnAgro2K
            Try
                cmdTmp2.ExecuteNonQuery()
                'dtrAgro2K = cmdTmp2.ExecuteReader
                'While dtrAgro2K.Read
                '    import = dtrAgro2K.GetValue(0)
                'End While
                ''dtrAgro2K.Close()
                MsgBox("Finalizo el proceso de importar Cuadres de una agencia a la Casa Matriz.", MsgBoxStyle.Exclamation, "Proceso Finalizado")
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                dtrAgro2K.Close()
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            End Try
        End If

        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Close()

    End Sub

    Sub QutarArqueImport()
        Dim IdArqueo As Integer = 0

        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        'cnnAgro2K.Open()
        'cmdAgro2K.Connection = cnnAgro2K
        'strQuery = ""
        'strQuery = "exec spQuitarNumeroArqueoImportarExistente " & cbNumerosCuadre.SelectedItem & ""

        Try
            'cmdAgro2K.CommandText = strQuery

            'cmdAgro2K.ExecuteNonQuery()

            'dtrAgro2K.Close()
            IdArqueo = SUConversiones.ConvierteAInt(cbNumerosCuadre.SelectedItem)
            RNArqueoCaja.EliminarArqueoDeImportacion(IdArqueo)
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
    End Sub

    Private Sub cmdEliminarArqueo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEliminarArqueo.Click
        If cbNumerosCuadre.Items.Count > 0 Then
            QutarArqueImport()

            CargarRecibos()
            CargarResumen()

            dgvxDetalleCuadreCaja.Rows.Clear()
            txtDeposito.Text = ""
            txtGastos.Text = ""
            txtSaldoInicial.Text = ""
            txtFacturaFinal.Text = ""
            txtFacturaInicial.Text = ""
            txtReciboFinal.Text = ""
            txtReciboInicial.Text = ""
            txtMensajeError.Text = ""
        Else
            MsgBox("No ha seleccionado un Numero de Arqueo para Retirar del Import..", MsgBoxStyle.Exclamation, "Quitar Arqueo del Import")
        End If


    End Sub


End Class