﻿Imports System.Windows.Forms
' Imports Microsoft.Reporting.WinForms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class Frm_RPT_Balanza_saldos
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "Frm_RPT_Balanza_saldos"

    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load

        Dim i As Integer = 0
        Dim val_max As Integer = 0
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim dset_anexo_saldos As New DS_Anexos_saldos
        Dim dtCatalogoNivel1 As DataTable = Nothing
        Dim dtCatalogoNivel2 As DataTable = Nothing
        Dim dtCatalogoNivel3 As DataTable = Nothing
        Dim dtCatalogoNivel4 As DataTable = Nothing
        Dim dtViewCatalogoConta As DataTable = Nothing

        Dim dset_View_catalogoconta As New DS_Anexos_saldos

        Try
            val_max = SUConversiones.ConvierteAInt(SIMF_CONTA.numeformulas)

            dtCatalogoNivel1 = RNCuentaContable.ObtienerCatalogoNivel1()
            dtCatalogoNivel1.TableName = "CataNivel1"
            dset_View_catalogoconta.Tables.Add(dtCatalogoNivel1.Clone())

            dtCatalogoNivel2 = RNCuentaContable.ObtienerCatalogoNivel2()
            dtCatalogoNivel2.TableName = "CataNivel2"
            dset_View_catalogoconta.Tables.Add(dtCatalogoNivel2.Clone())

            dtCatalogoNivel3 = RNCuentaContable.ObtienerCatalogoNivel3()
            dtCatalogoNivel3.TableName = "CataNivel3"
            dset_View_catalogoconta.Tables.Add(dtCatalogoNivel3.Clone())

            dtCatalogoNivel4 = RNCuentaContable.ObtienerCatalogoNivel4()
            dtCatalogoNivel4.TableName = "CataNivel4"
            dset_View_catalogoconta.Tables.Add(dtCatalogoNivel4.Clone())

            dtViewCatalogoConta = RNCuentaContable.ObtienerViewCatalogoConta()
            dtViewCatalogoConta.TableName = "View_catalogoconta"
            dset_View_catalogoconta.Tables.Add(dtViewCatalogoConta.Clone())

            Dim report_balanza_saldos As New RPT_Balanza_saldos
            report_balanza_saldos.SetDataSource(dset_View_catalogoconta)
            '' '' ''report_balanza_saldos.SetDatabaseLogon(SIMF_CONTA.usuario, SIMF_CONTA.pws, SIMF_CONTA.server, SIMF_CONTA.base)
            ' establecer la fórmula de selección de registros
            ' CrystalReportViewer1.ParameterFieldInfo = jackcrystal.lista_parametros


            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_balanza_saldos.SetParameterValue(nombreformula, valor)
                End If
            Next
            report_balanza_saldos.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_balanza_saldos
            Me.CrystalReportViewer1.Refresh()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
End Class