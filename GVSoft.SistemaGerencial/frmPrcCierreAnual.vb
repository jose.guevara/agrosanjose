Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.Office.Interop

Public Class frmPrcCierreAnual
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImportar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Unidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Costo As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Monto As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrcCierreAnual))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Unidad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Costo = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
        Me.Monto = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdImportar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 71)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(712, 83)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cierre"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Items.AddRange(New Object() {"Ultima compra del producto", "Costo en la hoja de Excel"})
        Me.ComboBox3.Location = New System.Drawing.Point(520, 24)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(152, 21)
        Me.ComboBox3.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(432, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Obtener Costo"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(320, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 1
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(248, 56)
        Me.TextBox3.MaxLength = 12
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(96, 20)
        Me.TextBox3.TabIndex = 4
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(72, 56)
        Me.TextBox2.MaxLength = 12
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(96, 20)
        Me.TextBox2.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "DocCierre"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(184, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "DocInicial"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(280, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Fecha"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Location = New System.Drawing.Point(264, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(8, 21)
        Me.ComboBox2.TabIndex = 2
        Me.ComboBox2.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(64, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(200, 21)
        Me.ComboBox1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Agencia"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvDetalle)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 160)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(712, 360)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de los Productos de la Agencia"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Unidad, Me.Cantidad, Me.Costo, Me.Monto})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(8, 22)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(694, 327)
        Me.dgvDetalle.TabIndex = 96
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Descripcion
        '
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 280
        '
        'Unidad
        '
        Me.Unidad.HeaderText = "Unidad"
        Me.Unidad.Name = "Unidad"
        Me.Unidad.ReadOnly = True
        Me.Unidad.Width = 70
        '
        'Cantidad
        '
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        Me.Cantidad.Width = 65
        '
        'Costo
        '
        '
        '
        '
        Me.Costo.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Costo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Costo.HeaderText = "Costo"
        Me.Costo.Increment = 1
        Me.Costo.Name = "Costo"
        Me.Costo.ReadOnly = True
        Me.Costo.Width = 80
        '
        'Monto
        '
        '
        '
        '
        Me.Monto.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Monto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Increment = 1
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdImportar, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.ButtonItem1, Me.LabelItem5, Me.cmdiSalir, Me.LabelItem4})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(712, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 35
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdImportar
        '
        Me.cmdImportar.Image = CType(resources.GetObject("cmdImportar.Image"), System.Drawing.Image)
        Me.cmdImportar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdImportar.Name = "cmdImportar"
        Me.cmdImportar.Text = "Importar Archivo<F3>"
        Me.cmdImportar.Tooltip = "Importar"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Procesar<F4>"
        Me.cmdiLimpiar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'ButtonItem1
        '
        Me.ButtonItem1.Image = CType(resources.GetObject("ButtonItem1.Image"), System.Drawing.Image)
        Me.ButtonItem1.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.ButtonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Text = "Excel<F5>"
        Me.ButtonItem1.Tooltip = "Procesar"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'frmPrcCierreAnual
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(712, 525)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPrcCierreAnual"
        Me.Text = "frmPrcCierreAnual"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmPrcCierreAnual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        'FormatearGrid()
        Cargar()

    End Sub

    Private Sub frmPrcCierreAnual_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            ObtenerArchivo()
        ElseIf e.KeyCode = Keys.F4 Then
            ProcesarCierre()
        ElseIf e.KeyCode = Keys.F5 Then
            ExportarExcel()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0 : ObtenerArchivo()
        '    Case 1 : ProcesarCierre()
        '    Case 2 : ExportarExcel()
        '    Case 3 : Me.Close()
        'End Select

    End Sub

    'Sub FormatearGrid()

    '    MSFlexGrid2.FormatString = "C�digo      |<Descripci�n                                                               |<Unidad   |>Cantidad  |>Costo    |>Monto              "

    'End Sub

    Sub Cargar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Agencias"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        DateTimePicker1.Value = Now
        ComboBox1.SelectedIndex = 0
        ComboBox3.SelectedIndex = 0
        TextBox2.Text = "cierre"
        TextBox3.Text = "inicial"

    End Sub

    Sub ReleaseObject(ByVal obj As Object)

        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch exc As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try

    End Sub

    Sub ObtenerArchivo()

        If ComboBox1.SelectedItem = "" Then
            MsgBox("Tiene que escoger una de las agencias para procesar el cierre anual.", MsgBoxStyle.Critical, "Faltan Datos")
            ComboBox1.Focus()
            Exit Sub
        End If
        If ComboBox3.SelectedItem = "" Then
            MsgBox("Tiene que escoger una de las opciones de costo para procesar el cierre anual.", MsgBoxStyle.Critical, "Faltan Datos")
            ComboBox3.Focus()
            Exit Sub
        End If
        If TextBox2.Text = "" Then
            MsgBox("Tiene que digitar el documento de cierre para procesar el cierre anual.", MsgBoxStyle.Critical, "Faltan Datos")
            TextBox2.Focus()
            Exit Sub
        End If
        If TextBox3.Text = "" Then
            MsgBox("Tiene que digitar el documento inicial para procesar el cierre anual.", MsgBoxStyle.Critical, "Faltan Datos")
            TextBox3.Focus()
            Exit Sub
        End If
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim range As Excel.Range
        Dim rCnt As Integer = 0
        Dim obj As Object = Nothing
        Dim strArchivo As String = String.Empty
        Dim dlgNew As New OpenFileDialog
        Dim strCodigo As String, strDescrip As String, strUnidad As String
        Dim dblCant As Double, dblCosto As Double, dblMonto As Double
        Dim cmdTmp As New SqlCommand("sp_PrcCierreAnual", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter

        ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
        With prmTmp01
            .ParameterName = "@intTipo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 1
        End With
        With prmTmp02
            .ParameterName = "@agenregistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox2.SelectedItem
        End With
        With prmTmp03
            .ParameterName = "@prodcodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp04
            .ParameterName = "@proddescrip"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp05
            .ParameterName = "@produnidad"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp06
            .ParameterName = "@cantidad"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp07
            .ParameterName = "@costo"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp08
            .ParameterName = "@monto"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp09
            .ParameterName = "@doccierre"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox2.Text
        End With
        With prmTmp10
            .ParameterName = "@docinicial"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox3.Text
        End With
        With prmTmp11
            .ParameterName = "@strnumfechaing"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp12
            .ParameterName = "@lngnumfechaing"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp13
            .ParameterName = "@lngnumtiempo"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp14
            .ParameterName = "@intExcelCosto"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox3.SelectedIndex
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .CommandType = CommandType.StoredProcedure
        End With

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        strArchivo = ""
        dlgNew.InitialDirectory = "C:\"
        dlgNew.Filter = "Excel files (*.xls)|*.xls"
        dlgNew.RestoreDirectory = True
        If dlgNew.ShowDialog() <> DialogResult.OK Then
            MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importaci�n")
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Exit Sub
        End If
        ComboBox1.Enabled = False
        DateTimePicker1.Enabled = False
        ComboBox3.Enabled = False
        TextBox2.ReadOnly = True
        TextBox3.ReadOnly = True
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Truncate Table tmp_CierreAnual"
        cmdAgro2K.CommandText = strQuery
        cmdAgro2K.ExecuteNonQuery()
        cmdAgro2K.Connection.Close()
        xlApp = New Excel.Application
        xlWorkBook = xlApp.Workbooks.Open(dlgNew.FileName)
        xlWorkSheet = xlWorkBook.Worksheets(1)
        range = xlWorkSheet.UsedRange
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        For rCnt = 2 To range.Rows.Count
            obj = CType(range.Cells(rCnt, 1), Excel.Range)
            strCodigo = CStr(obj.value)
            If strCodigo = "" Then
                Exit For
            End If
            cmdTmp.Parameters(2).Value = strCodigo
            obj = CType(range.Cells(rCnt, 2), Excel.Range)
            strDescrip = CStr(obj.value)
            cmdTmp.Parameters(3).Value = strDescrip
            obj = CType(range.Cells(rCnt, 3), Excel.Range)
            strUnidad = CStr(obj.value)
            cmdTmp.Parameters(4).Value = strUnidad
            obj = CType(range.Cells(rCnt, 4), Excel.Range)
            dblCant = CDbl(obj.value)
            cmdTmp.Parameters(5).Value = dblCant
            obj = CType(range.Cells(rCnt, 5), Excel.Range)
            dblCosto = CDbl(obj.value)
            cmdTmp.Parameters(6).Value = dblCosto
            obj = CType(range.Cells(rCnt, 6), Excel.Range)
            dblMonto = CDbl(obj.value)
            cmdTmp.Parameters(7).Value = dblMonto
            Try
                If ComboBox3.SelectedIndex = 0 Then
                    dblCosto = 0
                    dtrAgro2K = cmdTmp.ExecuteReader
                    While dtrAgro2K.Read
                        dblCosto = dtrAgro2K.GetValue(0)
                    End While
                    dtrAgro2K.Close()
                Else
                    cmdTmp.ExecuteNonQuery()
                End If
                dgvDetalle.Rows.Add(strCodigo, strDescrip, strUnidad, dblCant, dblCosto, _
                                    Format(dblCant * dblCosto, "#,##0.#0"))
                'MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
                'MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
                'MSFlexGrid2.Col = 0
                'MSFlexGrid2.Text = strCodigo
                'MSFlexGrid2.Col = 1
                'MSFlexGrid2.Text = strDescrip
                'MSFlexGrid2.Col = 2
                'MSFlexGrid2.Text = strUnidad
                'MSFlexGrid2.Col = 3
                'MSFlexGrid2.Text = dblCant
                'MSFlexGrid2.Col = 4
                'MSFlexGrid2.Text = dblCosto
                'MSFlexGrid2.Col = 5
                'MSFlexGrid2.Text = Format(dblCant * dblCosto, "#,##0.#0")
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
        Next rCnt
        cmdTmp.Connection.Close()
        cnnAgro2K.Close()
        xlWorkBook.Close()
        xlApp.Quit()
        ReleaseObject(xlApp)
        ReleaseObject(xlWorkBook)
        ReleaseObject(xlWorkSheet)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        TextBox2.Focus()

    End Sub

    Sub ExportarExcel()

        Dim objApp As Excel.Application
        Dim objBooks As Excel.Workbooks
        Dim objBook As Excel._Workbook
        Dim objSheets As Excel.Sheets
        Dim objSheet As Excel._Worksheet
        Dim strRange As Excel.Range
        'Dim saRet(MSFlexGrid2.Rows, 6) As String
        Dim saRet(dgvDetalle.Rows.Count + 1, 6) As String
        Dim intFila As Integer, intColumna As Integer

        objApp = New Excel.Application
        objBooks = objApp.Workbooks
        objBook = objBooks.Add
        objSheets = objBook.Worksheets
        objSheet = objSheets(1)

        strRange = objSheet.Range("A1:F1", Reflection.Missing.Value)
        strRange.Cells.Font.Bold = True
        strRange = objSheet.Range("A1", Reflection.Missing.Value)
        strRange = strRange.Resize(dgvDetalle.Rows.Count + 1, 6)
        'For intFila = 0 To MSFlexGrid2.Rows - 1
        '    MSFlexGrid2.Row = intFila
        '    For intColumna = 0 To 5
        '        MSFlexGrid2.Col = intColumna
        '        saRet(intFila, intColumna) = MSFlexGrid2.Text
        '    Next intColumna
        'Next intFila

        'Buscar un metodo para agregar los encabezados del grid
        'Agregar Encabezados de columnas
        For intColumna = 0 To 5
            saRet(0, intColumna) = dgvDetalle.Columns(intColumna).HeaderText
        Next intColumna

        'Registros del grid
        For intFila = 0 To dgvDetalle.Rows.Count - 1
            For intColumna = 0 To 5
                saRet(intFila + 1, intColumna) = dgvDetalle.Rows(intFila).Cells(intColumna).Value
            Next intColumna
        Next intFila
        strRange.Value = saRet
        strRange.EntireColumn.AutoFit()
        objApp.Visible = True
        objApp.UserControl = True
        strRange = Nothing
        objSheet = Nothing
        objSheets = Nothing
        objBooks = Nothing

    End Sub

    Sub ProcesarCierre()

        intResp = 0
        intResp = MsgBox("Se iniciar� el proceso de cierre anual de la agencia, �Desea continuar con el proceso de cierre anual de la agencia?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Proceso")
        If intResp = 7 Then
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_PrcCierreAnual", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim lngNumTiempo, lngNumFecha As Long
        Dim lngAgencia01 As Byte
        '  Dim dteFecha As Date

        lngNumFecha = 0
        lngRegistro = 0
        lngAgencia01 = 0
        lngNumTiempo = 0
        lngNumTiempo = Format(Now, "Hmmss")
        lngNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
        ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
        lngAgencia01 = ComboBox2.SelectedItem

        ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
        With prmTmp01
            .ParameterName = "@intTipo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 2
        End With
        With prmTmp02
            .ParameterName = "@agenregistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp03
            .ParameterName = "@prodcodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp04
            .ParameterName = "@proddescrip"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp05
            .ParameterName = "@produnidad"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp06
            .ParameterName = "@cantidad"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp07
            .ParameterName = "@costo"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp08
            .ParameterName = "@monto"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp09
            .ParameterName = "@doccierre"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp10
            .ParameterName = "@docinicial"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp11
            .ParameterName = "@strnumfechaing"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp12
            .ParameterName = "@lngnumfechaing"
            .SqlDbType = SqlDbType.Decimal
            .Value = lngNumFecha
        End With
        With prmTmp13
            .ParameterName = "@lngnumtiempo"
            .SqlDbType = SqlDbType.Decimal
            .Value = lngNumTiempo
        End With
        With prmTmp14
            .ParameterName = "@intExcelCosto"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox3.SelectedIndex
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .CommandType = CommandType.StoredProcedure
        End With

        Try
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            cmdTmp.CommandTimeout = 0
            cmdTmp.ExecuteNonQuery()
            cmdTmp.Parameters(0).Value = 3
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        Finally
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
        End Try
        Me.Cursor = Cursors.Default
        MsgBox("Se finaliz� el proceso del cierre anual de inventario.  A continuaci�n se generar el reporte de diferencias entre productos.", MsgBoxStyle.Information, "Reporte Cierre Anual")
        intRptExportar = 0
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptExportar = 0
        intRptOtros = 4
        strQuery = ""
        strQuery = "select a.descripcion, c.registro, c.codigo clascodigo, c.descripcion clasdescrip, "
        strQuery = strQuery + "p.codigo prodcodigo, p.descripcion proddescrip, u.codigo unidcodigo, "
        strQuery = strQuery + "d.cntfisico, d.cntsistema, abs(d.cntfisico - d.cntsistema) cntdiferencia "
        strQuery = strQuery + "from tmp_CierreAnualDif d, prm_agencias a, prm_productos p, prm_clases c, "
        strQuery = strQuery + "prm_unidades u where d.agencia = a.registro And d.prodcodigo = p.codigo "
        strQuery = strQuery + "and p.regclase = c.registro and p.regunidad = u.registro "
        strQuery = strQuery + "order by c.registro, p.descripcion"
        strNombAgenEstInv = ComboBox1.SelectedItem
        Dim frmNew As New actrptViewer
        intRptExportar = 1
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()

    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown, DateTimePicker1.KeyDown, ComboBox3.KeyDown, TextBox2.KeyDown, TextBox3.KeyDown

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            Select Case sender.name.ToString
                Case "ComboBox1" : DateTimePicker1.Focus()
                Case "DateTimePicker1" : ComboBox3.Focus()
                Case "ComboBox3" : TextBox2.Focus()
                Case "TextBox2" : TextBox3.Focus()
                Case "TextBox3" : ComboBox1.Focus()
            End Select
        End If

    End Sub

    Private Sub TextBox2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox2.GotFocus, TextBox3.GotFocus

        sender.selectall()

    End Sub

    'Private Sub MSFlexGrid2_DblClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles MSFlexGrid2.DblClick

    '    Dim strValor As String
    '    Dim intCol As Integer, dblCant As Double

    '    If ComboBox3.SelectedIndex = 0 Then
    '        MSFlexGrid2.Col = 4
    '        intCol = MSFlexGrid2.Col
    '        strValor = 0
    '        strValor = InputBox("Ingrese el Nuevo Valor", "Costo")
    '        If Trim(strValor) = "" Then
    '            Exit Sub
    '        End If
    '        If IsNumeric(strValor) = False Then
    '            Exit Sub
    '        End If
    '        If IsNumeric(strValor) = True Then
    '            dblCant = 0
    '            MSFlexGrid2.Text = Format(CDbl(strValor), "###,##0.#0")
    '            MSFlexGrid2.Col = intCol - 1
    '            dblCant = CDbl(MSFlexGrid2.Text)
    '            MSFlexGrid2.Col = intCol + 1
    '            MSFlexGrid2.Text = Format(CDbl(dblCant) * CDbl(strValor), "###,##0.#0")
    '            If cnnAgro2K.State = ConnectionState.Open Then
    '                cnnAgro2K.Close()
    '            End If
    '            If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '                cmdAgro2K.Connection.Close()
    '            End If
    '            cnnAgro2K.Open()
    '            cmdAgro2K.Connection = cnnAgro2K
    '            MSFlexGrid2.Col = 0
    '            strQuery = ""
    '            strQuery = "update tmp_cierreanual set costo = " & CDbl(strValor) & ", "
    '            strQuery = strQuery + "monto = " & (CDbl(dblCant) * CDbl(strValor)) & " where "
    '            strQuery = strQuery + "prodcodigo = '" & Trim(MSFlexGrid2.Text) & "'"
    '            cmdAgro2K.CommandText = strQuery
    '            cmdAgro2K.ExecuteNonQuery()
    '            cmdAgro2K.Connection.Close()
    '            cnnAgro2K.Open()
    '        End If
    '    End If

    'End Sub

    Private Sub cmdImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportar.Click
        ObtenerArchivo()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        ProcesarCierre()
    End Sub

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem1.Click
        ExportarExcel()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
