Imports System.Data.SqlClient
Imports System.Text

Public Class frmUsuarios
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCodigoUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents cntmnuUsuariosRg As System.Windows.Forms.ContextMenu
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombres As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ddlEstado As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents txtApellidos As System.Windows.Forms.TextBox
    Friend WithEvents txtRegistro As System.Windows.Forms.TextBox
    Friend WithEvents txtConfirmar As System.Windows.Forms.TextBox
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCargo As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents clbPermisos As System.Windows.Forms.CheckedListBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents clbSucursales As System.Windows.Forms.CheckedListBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ddlImpresora As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsuarios))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ddlImpresora = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtCargo = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtApellidos = New System.Windows.Forms.TextBox
        Me.txtTelefono = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtDireccion = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtRegistro = New System.Windows.Forms.TextBox
        Me.ddlEstado = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtConfirmar = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNombres = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtCodigoUsuario = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cntmnuUsuariosRg = New System.Windows.Forms.ContextMenu
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.clbSucursales = New System.Windows.Forms.CheckedListBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.clbPermisos = New System.Windows.Forms.CheckedListBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8, Me.MenuItem10})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 4
        Me.MenuItem10.Text = "Igual"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem9})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ddlImpresora)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtCargo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtApellidos)
        Me.GroupBox1.Controls.Add(Me.txtTelefono)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtRegistro)
        Me.GroupBox1.Controls.Add(Me.ddlEstado)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtConfirmar)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtClave)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNombres)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCodigoUsuario)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(616, 168)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de los Usuarios"
        '
        'ddlImpresora
        '
        Me.ddlImpresora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlImpresora.Items.AddRange(New Object() {"local", "red"})
        Me.ddlImpresora.Location = New System.Drawing.Point(240, 128)
        Me.ddlImpresora.Name = "ddlImpresora"
        Me.ddlImpresora.Size = New System.Drawing.Size(104, 21)
        Me.ddlImpresora.TabIndex = 18
        Me.ddlImpresora.Tag = "Si el usuario tiene una impresora local o de red asignada"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(176, 128)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Impresora"
        '
        'txtCargo
        '
        Me.txtCargo.Location = New System.Drawing.Point(48, 96)
        Me.txtCargo.MaxLength = 30
        Me.txtCargo.Name = "txtCargo"
        Me.txtCargo.Size = New System.Drawing.Size(280, 20)
        Me.txtCargo.TabIndex = 5
        Me.txtCargo.Tag = "Cargo del usuario"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 96)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Cargo"
        '
        'txtApellidos
        '
        Me.txtApellidos.Location = New System.Drawing.Point(408, 32)
        Me.txtApellidos.MaxLength = 35
        Me.txtApellidos.Name = "txtApellidos"
        Me.txtApellidos.Size = New System.Drawing.Size(200, 20)
        Me.txtApellidos.TabIndex = 2
        Me.txtApellidos.Tag = "Apellido del usuario"
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(512, 64)
        Me.txtTelefono.MaxLength = 16
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(96, 20)
        Me.txtTelefono.TabIndex = 4
        Me.txtTelefono.Tag = "Tel�fono del usuario"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(456, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Tel�fono"
        '
        'txtDireccion
        '
        Me.txtDireccion.Location = New System.Drawing.Point(72, 64)
        Me.txtDireccion.MaxLength = 100
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(368, 20)
        Me.txtDireccion.TabIndex = 3
        Me.txtDireccion.Tag = "Direcci�n del usuario"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Direcci�n"
        '
        'txtRegistro
        '
        Me.txtRegistro.Location = New System.Drawing.Point(128, 32)
        Me.txtRegistro.MaxLength = 12
        Me.txtRegistro.Name = "txtRegistro"
        Me.txtRegistro.Size = New System.Drawing.Size(8, 20)
        Me.txtRegistro.TabIndex = 10
        Me.txtRegistro.TabStop = False
        Me.txtRegistro.Tag = "C�digo asignado a la agencia"
        Me.txtRegistro.Visible = False
        '
        'ddlEstado
        '
        Me.ddlEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlEstado.Items.AddRange(New Object() {"activo", "inactivo", "eliminado"})
        Me.ddlEstado.Location = New System.Drawing.Point(56, 128)
        Me.ddlEstado.Name = "ddlEstado"
        Me.ddlEstado.Size = New System.Drawing.Size(104, 21)
        Me.ddlEstado.TabIndex = 8
        Me.ddlEstado.Tag = "Estado del registro"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 128)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Estado"
        '
        'txtConfirmar
        '
        Me.txtConfirmar.Location = New System.Drawing.Point(536, 96)
        Me.txtConfirmar.MaxLength = 12
        Me.txtConfirmar.Name = "txtConfirmar"
        Me.txtConfirmar.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtConfirmar.Size = New System.Drawing.Size(72, 20)
        Me.txtConfirmar.TabIndex = 7
        Me.txtConfirmar.Tag = "Clave del usuario"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(472, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Confirmar"
        '
        'txtClave
        '
        Me.txtClave.Location = New System.Drawing.Point(384, 96)
        Me.txtClave.MaxLength = 12
        Me.txtClave.Name = "txtClave"
        Me.txtClave.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtClave.Size = New System.Drawing.Size(72, 20)
        Me.txtClave.TabIndex = 6
        Me.txtClave.Tag = "Clave del usuario"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(344, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Clave"
        '
        'txtNombres
        '
        Me.txtNombres.Location = New System.Drawing.Point(200, 32)
        Me.txtNombres.MaxLength = 35
        Me.txtNombres.Name = "txtNombres"
        Me.txtNombres.Size = New System.Drawing.Size(200, 20)
        Me.txtNombres.TabIndex = 1
        Me.txtNombres.Tag = "Nombre del usuario"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(144, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nombres"
        '
        'txtCodigoUsuario
        '
        Me.txtCodigoUsuario.Location = New System.Drawing.Point(56, 32)
        Me.txtCodigoUsuario.MaxLength = 12
        Me.txtCodigoUsuario.Name = "txtCodigoUsuario"
        Me.txtCodigoUsuario.Size = New System.Drawing.Size(72, 20)
        Me.txtCodigoUsuario.TabIndex = 0
        Me.txtCodigoUsuario.Tag = "C�digo asignado al usuario"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "C�digo"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(616, 65)
        Me.ToolBar1.TabIndex = 18
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuUsuariosRg
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Name = "ToolBarButton4"
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.ToolBarButton4.ToolTipText = "Registros"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Name = "ToolBarButton7"
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 497)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(616, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 19
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Name = "StatusBarPanel1"
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Name = "StatusBarPanel2"
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 530
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ListBox1)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.clbSucursales)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.clbPermisos)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 232)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(616, 264)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Permisos de los Usuarios"
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(544, 120)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(56, 17)
        Me.ListBox1.TabIndex = 8
        Me.ListBox1.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(544, 80)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(64, 32)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Rechazar Todos"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(544, 32)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(64, 32)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Conceder Todos"
        '
        'clbSucursales
        '
        Me.clbSucursales.CheckOnClick = True
        Me.clbSucursales.Location = New System.Drawing.Point(320, 24)
        Me.clbSucursales.Name = "clbSucursales"
        Me.clbSucursales.Size = New System.Drawing.Size(208, 229)
        Me.clbSucursales.TabIndex = 5
        Me.clbSucursales.ThreeDCheckBoxes = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(232, 80)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(64, 32)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Rechazar Todos"
        '
        'clbPermisos
        '
        Me.clbPermisos.CheckOnClick = True
        Me.clbPermisos.Items.AddRange(New Object() {"+ Usuarios", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Agencias", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Departamentos", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Municipios", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Vendedores", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Clientes", "    - agregar", "    - visualizar", "    - modificar", "    - vendedor", "    - dias cr�dito", "    - reportes", "+ Clases", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ SubClases", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Empaques", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Unidades", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Proveedores", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Productos", "    - agregar/modificar", "    - visualizar", "    - detalle", "    - reportes", "+ Conversiones", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Negocios", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Movimiento Inventario", "    - agregar", "+ Movimiento Conversiones", "    - agregar", "+ Presupuestos", "    - agregar/modificar", "    - visualizar", "+ Razones Anular", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Facturas", "    + Contado", "        - elaborar", "        - imprimir pendientes", "        - ingreso manual", "        - anular", "        - consultar", "        - descuentos", "    + Cr�dito", "        - elaborar", "        - imprimir pendientes", "        - ingreso manual", "        - anular", "        - consultar", "    + Correcciones", "        - procesar", "    + Exportar Ventas", "        - procesar", "    + Importar Ventas", "        - agregar lote", "        - modificar lote", "        - procesar lote", "+ Tipo de Cambio", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Reportes", "    + Inventario", "        - generar", "        - exportar", "    + Facturaci�n", "        - generar", "        - exportar", "    + Cuentas por Cobrar", "        - generar", "        - exportar", "        - estados de cuentas", "    + Presupuestos", "        - generar", "        - exportar", "    + Generales", "        - generar", "        - exportar", "+ Cuentas por Cobrar", "    + Recibos de Caja", "        - elaborar", "        - consultar", "        - anular", "    + Notas de Cr�dito", "        - elaborar", "        - consultar", "        - anular", "    + Notas de D�bito", "        - elaborar", "        - consultar", "        - anular", "    + Recibos Varios", "        - elaborar", "        - consultar", "        - anular", "+ Bancos", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Cat�logo Contable", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Exportar", "    - Estado del Inventario", "    - Tasas de Cambio", "    - Clientes", "    - Clases", "    - SubClases", "    - Unidades", "    - Empaques", "    - Proveedores", "    - Productos", "    - Vendedores", "+ Importar", "    - Estado del Inventario", "    - Tasas de Cambio", "    - Clientes", "    - Clases", "    - SubClases", "    - Unidades", "    - Empaques", "    - Proveedores", "    - Productos", "    - Vendedores", "+ Areas", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Puestos", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Numero de Facturas", "    - cambiar numeros", "+ Porcentajes", "    - cambiar porcentajes", "+ Empleados", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Cuentas Bancarias", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Chequeras", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Retenci�n Chequeras", "    - cambiar porcentajes", "+ Parametros Recibos Caja", "    - agregar/modificar", "    - visualizar", "+ Parametros Notas Cr�dito", "    - agregar/modificar", "    - visualizar", "+ Parametros Notas D�bito", "    - agregar/modificar", "    - visualizar", "+ Definici�n de Comisiones", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Letras de cambio", "    - cambiar letras de cambio", "+ Procesamiento de Archivos", "    - cierre anual del inventario", "+ Parametros Empresa", "    - cambiar datos de la empresa", "+ Partidas Contables", "    - agregar/modificar", "    - visualizar", "    - reportes", "+ Cheques", "    - elaborar", "    - consultar", "    - reversar", "    - anular", "+ Dep�sitos", "    - elaborar", "    - consultar", "    - reversar", "    - anular", "+ Notas de Cr�dito", "    - elaborar", "    - consultar", "    - reversar", "    - anular", "+ Notas de D�bito", "    - elaborar", "    - consultar", "    - reversar", "    - anular", "+ Reportes", "    + Cheques", "        - generar", "        - exportar", "+ Beneficiarios", "    - agregar/modificar", "    - visualizar", "    - reportes"})
        Me.clbPermisos.Location = New System.Drawing.Point(8, 24)
        Me.clbPermisos.Name = "clbPermisos"
        Me.clbPermisos.Size = New System.Drawing.Size(208, 229)
        Me.clbPermisos.TabIndex = 3
        Me.clbPermisos.ThreeDCheckBoxes = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(232, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 32)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Conceder Todos"
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15, Me.MenuItem16, Me.MenuItem17, Me.MenuItem18})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.Text = "expandir m�dulo"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 1
        Me.MenuItem16.Text = "colapsar m�dulo"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 2
        Me.MenuItem17.Text = "expandir todos"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 3
        Me.MenuItem18.Text = "colapsar todos"
        '
        'frmUsuarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(616, 521)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.StatusBar1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmUsuarios"
        Me.Text = "frmUsuarios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Dim strCambios(8) As String
    Public txtCollection As New Collection

    Private Sub frmUsuarios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        CreateMyContextMenu()
        txtCollection.Add(txtCodigoUsuario)
        txtCollection.Add(txtNombres)
        txtCollection.Add(txtApellidos)
        txtCollection.Add(txtDireccion)
        txtCollection.Add(txtTelefono)
        txtCollection.Add(txtCargo)
        txtCollection.Add(txtClave)
        txtCollection.Add(txtConfirmar)
        txtCollection.Add(txtRegistro)
        Iniciar()
        Limpiar()
        MenuItem9.Visible = False
        intModulo = 0

    End Sub

    Private Sub frmUsuarios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = txtRegistro.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = txtRegistro.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
            Case 3
                MoverRegistro(sender.text)
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngUsuarios", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter

        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtCodigoUsuario.Text
        End With
        With prmTmp03
            .ParameterName = "@strNombre"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNombres.Text
        End With
        With prmTmp04
            .ParameterName = "@strApellido"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtApellidos.Text
        End With
        With prmTmp05
            .ParameterName = "@strDireccion"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtDireccion.Text
        End With
        With prmTmp06
            .ParameterName = "@strTelefono"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtTelefono.Text
        End With
        With prmTmp07
            .ParameterName = "@strCargo"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtCargo.Text
        End With
        With prmTmp08
            .ParameterName = "@strClave"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtClave.Text
        End With
        With prmTmp09
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ddlEstado.SelectedIndex
        End With
        With prmTmp10
            .ParameterName = "@intImpresora"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ddlImpresora.SelectedIndex
        End With
        With prmTmp11
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            lngRegistro = txtRegistro.Text
            cmdTmp.Parameters(0).Value = lngRegistro
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        cmdTmp.Parameters(10).Value = 1
        Try
            cmdTmp.ExecuteNonQuery()
            If intResp = 6 Then
                If txtNombres.Text <> strCambios(0) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Nombre", strCambios(0), txtNombres.Text)
                End If
                If txtApellidos.Text <> strCambios(1) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Apellido", strCambios(1), txtApellidos.Text)
                End If
                If txtDireccion.Text <> strCambios(2) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Direcci�n", strCambios(2), txtDireccion.Text)
                End If
                If txtTelefono.Text <> strCambios(3) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Tel�fono", strCambios(3), txtTelefono.Text)
                End If
                If txtCargo.Text <> strCambios(4) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Cargo", strCambios(4), txtCargo.Text)
                End If
                If txtClave.Text <> strCambios(5) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Clave", "********", "********")
                End If
                If ddlEstado.SelectedItem <> strCambios(6) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Estado", strCambios(6), ddlEstado.SelectedItem)
                End If
                If ddlImpresora.SelectedItem <> strCambios(7) Then
                    Ing_Bitacora(0, txtRegistro.Text, "Impresora", strCambios(7), ddlImpresora.SelectedItem)
                End If
            ElseIf intResp = 0 Then
                Ing_Bitacora(0, lngRegistro, "C�digo", "", txtCodigoUsuario.Text)
            End If
            GuardarDetalle1()
            GuardarDetalle2()
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            Limpiar()
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub GuardarDetalle1()

        Dim cmdTmp As New SqlCommand("sp_IngUsuariosPermisos", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter

        With prmTmp01
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtCodigoUsuario.Text
        End With
        With prmTmp02
            .ParameterName = "@intPermiso"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 254
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        cmdTmp.ExecuteNonQuery()
        For intIncr = 0 To clbPermisos.Items.Count - 1
            If clbPermisos.GetItemChecked(intIncr) = True Then
                cmdTmp.Parameters(1).Value = intIncr
            Else
                cmdTmp.Parameters(1).Value = 0
            End If
            Try
                cmdTmp.ExecuteNonQuery()
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                Exit Sub
            End Try
        Next intIncr

    End Sub

    Sub GuardarDetalle2()

        Dim cmdTmp As New SqlCommand("sp_IngUsuariosAgencias", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter

        With prmTmp01
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtCodigoUsuario.Text
        End With
        With prmTmp02
            .ParameterName = "@intPermiso"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 254
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        cmdTmp.ExecuteNonQuery()
        For intIncr = 0 To clbSucursales.Items.Count - 1
            If clbSucursales.GetItemChecked(intIncr) = True Then
                ListBox1.SelectedIndex = intIncr
                cmdTmp.Parameters(1).Value = ListBox1.SelectedItem
                Try
                    cmdTmp.ExecuteNonQuery()
                Catch exc As Exception
                    MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                    Exit Sub
                End Try
            End If
        Next intIncr

    End Sub

    Sub Limpiar()

        'For intIncr = 1 To txtCollection.Count - 1
        For intIncr = 1 To txtCollection.Count
            txtCollection.Item(intIncr).text = ""
        Next
        txtRegistro.Text = "0"
        For intIncr = 0 To 7
            strCambios(intIncr) = 0
        Next intIncr
        For intIncr = 0 To clbPermisos.Items.Count - 1
            clbPermisos.SetItemChecked(intIncr, False)
        Next intIncr
        For intIncr = 0 To clbSucursales.Items.Count - 1
            clbSucursales.SetItemChecked(intIncr, False)
        Next intIncr
        ddlEstado.SelectedIndex = 0
        ddlImpresora.SelectedIndex = 0
        txtCodigoUsuario.Focus()

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Agencias Order By Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox1.Items.Add(dtrAgro2K.GetValue(0))
            clbSucursales.Items.Add(dtrAgro2K.GetValue(1))

        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub MoverRegistro(ByVal StrDato As String)
        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(CodUsuario) from prm_Usuarios"
            Case "Anterior"
                strQuery = "Select Max(CodUsuario) from prm_Usuarios Where CodUsuario < '" & txtCodigoUsuario.Text & "'"
            Case "Siguiente"
                strQuery = "Select Min(CodUsuario) from prm_Usuarios Where CodUsuario > '" & txtCodigoUsuario.Text & "'"
            Case "Ultimo"
                strQuery = "Select Max(CodUsuario) from prm_Usuarios"
            Case "Igual"
                strQuery = "Select CodUsuario from prm_Usuarios Where CodUsuario = '" & txtCodigoUsuario.Text & "'"
                strCodigoTmp = ""
                strCodigoTmp = txtCodigoUsuario.Text
        End Select
        Limpiar()
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim strCodigo As String

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        strCodigo = ""
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                strCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If strCodigo = "" Then
            If StrDato = "Igual" Then
                txtCodigoUsuario.Text = strCodigoTmp
                MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
            Else
                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From prm_Usuarios Where CodUsuario = '" & strCodigo & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            txtRegistro.Text = dtrAgro2K.GetValue(0)
            txtCodigoUsuario.Text = dtrAgro2K.GetValue(1)
            txtNombres.Text = dtrAgro2K.GetValue(2)
            txtApellidos.Text = dtrAgro2K.GetValue(3)
            txtDireccion.Text = dtrAgro2K.GetValue(4)
            txtTelefono.Text = dtrAgro2K.GetValue(5)
            txtCargo.Text = dtrAgro2K.GetValue(6)
            txtClave.Text = dtrAgro2K.GetValue(7)
            txtConfirmar.Text = dtrAgro2K.GetValue(7)
            ddlEstado.SelectedIndex = dtrAgro2K.GetValue(8)
            ddlImpresora.SelectedIndex = dtrAgro2K.GetValue(9)
            For intIncr = 0 To 5
                strCambios(intIncr) = dtrAgro2K.GetValue(intIncr + 2)
            Next intIncr
            strCambios(6) = ddlEstado.SelectedItem
            strCambios(7) = ddlImpresora.SelectedItem
        End While
        dtrAgro2K.Close()
        If txtCodigoUsuario.Text <> "" Then
            strQuery = ""
            strQuery = "Select * From prm_UsuariosPermisos Where CliRegistro = " & CInt(txtRegistro.Text) & ""
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                clbPermisos.SetItemChecked(CInt(dtrAgro2K.GetValue(1)), True)
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select a.usrregistro,a.agenregistro, Descripcion  DesAgencia From prm_UsuariosAgencias a,  prm_Agencias b Where a.UsrRegistro = " & CInt(txtRegistro.Text) & " and a.agenregistro = b.registro"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                clbSucursales.SetItemChecked(clbSucursales.Items.IndexOf(dtrAgro2K.GetValue(2)), True)
            End While
            dtrAgro2K.Close()
        End If
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        txtRegistro.Focus()

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem
        Dim cntmenuItem5 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"
        cntmenuItem5.Text = "Igual"

        cntmnuUsuariosRg.MenuItems.Add(cntmenuItem1)
        cntmnuUsuariosRg.MenuItems.Add(cntmenuItem2)
        cntmnuUsuariosRg.MenuItems.Add(cntmenuItem3)
        cntmnuUsuariosRg.MenuItems.Add(cntmenuItem4)
        cntmnuUsuariosRg.MenuItems.Add(cntmenuItem5)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoUsuario.GotFocus, txtNombres.GotFocus, txtApellidos.GotFocus, txtDireccion.GotFocus, txtTelefono.GotFocus, txtCargo.GotFocus, txtClave.GotFocus, ddlEstado.GotFocus, ddlImpresora.GotFocus, txtConfirmar.GotFocus

        Dim strCampo As String = String.Empty

        Select Case sender.name.ToString
            Case "txtCodigoUsuario" : strCampo = "C�digo"
            Case "txtNombres" : strCampo = "Nombre"
            Case "txtApellidos" : strCampo = "Apellido"
            Case "txtDireccion" : strCampo = "Direcci�n"
            Case "txtTelefono" : strCampo = "Tel�fono"
            Case "txtCargo" : strCampo = "Cargo"
            Case "txtClave" : strCampo = "Clave"
            Case "txtConfirmar" : strCampo = "Confirmar"
            Case "ddlEstado" : strCampo = "Estado"
            Case "ddlImpresora" : strCampo = "Impresora"
        End Select
        sender.selectall()
        StatusBar1.Panels.Item(0).Text = strCampo
        StatusBar1.Panels.Item(1).Text = sender.tag

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                txtCodigoUsuario.Text = strUbicar
                MoverRegistro("Igual")
            End If
        End If

    End Sub

    'Private Sub ComboBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoUsuario.KeyDown, txtNombres.KeyDown, txtApellidos.KeyDown, txtDireccion.KeyDown, txtTelefono.KeyDown, txtCargo.KeyDown, txtClave.KeyDown, txtConfirmar.KeyDown, ddlEstado.KeyDown, ddlImpresora.KeyDown, txtConfirmar.KeyDown

    '    If e.KeyCode = Keys.Enter Then
    '        Select Case sender.name
    '            Case "TextBox1" : MoverRegistro("Igual") : txtNombres.Focus()
    '            Case "TextBox2" : txtApellidos.Focus()
    '            Case "TextBox3" : txtDireccion.Focus()
    '            Case "TextBox4" : txtTelefono.Focus()
    '            Case "TextBox5" : txtCargo.Focus()
    '            Case "TextBox6" : txtClave.Focus()
    '            Case "TextBox7" : txtConfirmar.Focus()
    '            Case "TextBox8" : ddlEstado.Focus()
    '            Case "ComboBox1" : ddlImpresora.Focus()
    '            Case "ComboBox2" : txtCodigoUsuario.Focus()
    '        End Select
    '    End If

    'End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoUsuario.KeyDown, txtNombres.KeyDown, txtApellidos.KeyDown, txtDireccion.KeyDown, txtTelefono.KeyDown, txtCargo.KeyDown, txtClave.KeyDown, txtConfirmar.KeyDown, ddlEstado.KeyDown, ddlImpresora.KeyDown, txtConfirmar.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "txtCodigoUsuario" : MoverRegistro("Igual") : txtNombres.Focus()
                Case "txtNombres" : txtApellidos.Focus()
                Case "txtApellidos" : txtDireccion.Focus()
                Case "txtDireccion" : txtTelefono.Focus()
                Case "txtTelefono" : txtCargo.Focus()
                Case "txtCargo" : txtClave.Focus()
                Case "txtClave" : txtConfirmar.Focus()
                Case "txtConfirmar" : ddlEstado.Focus()
                Case "ddlEstado" : ddlImpresora.Focus()
                Case "ddlImpresora" : txtCodigoUsuario.Focus()
            End Select
        End If

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click

        If sender.name = "Button1" Then
            For intIncr = 0 To clbPermisos.Items.Count - 1
                clbPermisos.SetItemChecked(intIncr, True)
            Next intIncr
        ElseIf sender.name = "Button2" Then
            For intIncr = 0 To clbPermisos.Items.Count - 1
                clbPermisos.SetItemChecked(intIncr, False)
            Next intIncr
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click, Button4.Click

        If sender.name = "Button3" Then
            For intIncr = 0 To clbSucursales.Items.Count - 1
                clbSucursales.SetItemChecked(intIncr, True)
            Next intIncr
        ElseIf sender.name = "Button4" Then
            For intIncr = 0 To clbSucursales.Items.Count - 1
                clbSucursales.SetItemChecked(intIncr, False)
            Next intIncr
        End If

    End Sub



End Class
