Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmReportesProformas


    Private Sub frmReportesProformas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UbicarAgencia(lngRegUsuario)
        ObtieneSeriexAgencia()
        RNReportes.CargarComboReportes(cbReportes, nIdUsuario, nIdModulo)
    End Sub

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        ddlSerie.SelectedItem = -1
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        'If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 3 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
        '    'strQuery = "Select a.IdSerie, Descripcion From catSerieFactura  a,tbl_ultimonumero b where Activo=1 and b.agenregistro =" & lngRegAgencia & " and a.IdSerie = b.IdSerie and Contado >0"
        strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",1"
        'End If
        'If intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 6 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
        '    'strQuery = "Select a.IdSerie, Descripcion From catSerieFactura  a,tbl_ultimonumero b where Activo=1 and b.agenregistro =" & lngRegAgencia & " and a.IdSerie = b.IdSerie and Credito >0"
        '    strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",4"
        'End If
        If strQuery.Trim.Length > 0 Then
            ddlSerie.Items.Clear()
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                IndicaObtieneRegistro = 1

            End While
        End If
        If IndicaObtieneRegistro = 1 Then
            ddlSerie.SelectedIndex = 0
        End If

        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
    End Sub

    Public Sub MostrarReporte()

        'Dim frmNew As New frmEscogerClientes
        Dim frmNew As Form
        Dim strAgencias As String = String.Empty
        Dim strAgencias2 As String = String.Empty
        Dim strNumeroProforma As String = String.Empty

        intRptProformas = cbReportes.SelectedValue

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        If intRptProformas = 1 Then
            strNumeroProforma = ddlSerie.Text & Format(ConvierteAInt(txtNumeroFactura.Text), "0000000")
            strQuery = ""
            strQuery = "spConsultarProformaXNumeroProforma " & strNumeroProforma

            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
            strNumeroProforma = String.Empty

        ElseIf intRptProformas = 2 Then
            strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""
            strQuery = "spConsultarProformaXFecha " & ConvierteAInt(strFechaInicial) & ", " & ConvierteAInt(strFechaFinal)

            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()

        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        If cbReportes.SelectedValue = -1 Then
            MsgBox("No ha Seleccionado una opcion de Reporte a Mostrar..!", MsgBoxStyle.Exclamation, "Reportes de Arqueos de Cajas")
            Exit Sub
        Else
            MostrarReporte()
        End If
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        txtNumeroFactura.Text = String.Empty
    End Sub
End Class