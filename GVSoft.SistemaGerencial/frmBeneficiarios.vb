﻿Imports System.Data.SqlClient
Imports System.Text

Public Class frmBeneficiarios

    Dim strCambios(2) As String

    Private Sub frmBeneficiarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        CreateMyContextMenu()
        Limpiar()
        intModulo = 42

    End Sub

    Private Sub frmBeneficiarios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = TextBox3.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = TextBox3.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)


    End Sub

    Sub Guardar()

        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngBeneficiarios", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter

        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp03
            .ParameterName = "@strNombre"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox2.Text
        End With
        With prmTmp04
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox1.SelectedIndex
        End With
        With prmTmp05
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, ¿Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            lngRegistro = TextBox3.Text
            cmdTmp.Parameters(0).Value = lngRegistro
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        cmdTmp.Parameters(4).Value = 1

        Try
            cmdTmp.ExecuteNonQuery()
            If intResp = 6 Then
                If TextBox2.Text <> strCambios(0) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Nombre", strCambios(0), TextBox2.Text)
                End If
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Estado", strCambios(1), ComboBox1.SelectedItem)
                End If
            ElseIf intResp = 0 Then
                Ing_Bitacora(intModulo, lngRegistro, "Código", "", TextBox1.Text)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicación Satisfactoria")
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicación")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar()

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = "0"
        ComboBox1.SelectedIndex = 0
        strCambios(0) = ""
        strCambios(1) = ""
        TextBox1.Focus()

    End Sub

    Sub MoverRegistro(ByVal StrDato As String)

        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(Codigo) from prm_Beneficiarios"
            Case "Anterior"
                strQuery = "Select Max(Codigo) from prm_Beneficiarios Where codigo < '" & TextBox1.Text & "'"
            Case "Siguiente"
                strQuery = "Select Min(Codigo) from prm_Beneficiarios Where codigo > '" & TextBox1.Text & "'"
            Case "Ultimo"
                strQuery = "Select Max(Codigo) from prm_Beneficiarios"
            Case "Igual"
                strQuery = "Select Codigo from prm_Beneficiarios Where codigo = '" & TextBox1.Text & "'"
                strCodigoTmp = ""
                strCodigoTmp = TextBox1.Text
        End Select
        Limpiar()
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim strCodigo As String

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        strCodigo = ""
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                strCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If strCodigo = "" Then
            If StrDato = "Igual" Then
                TextBox1.Text = strCodigoTmp
                MsgBox("No existe un registro con ese código en la tabla", MsgBoxStyle.Information, "Extracción de Registro")
            Else
                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracción de Registro")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From prm_Beneficiarios Where Codigo = '" & strCodigo & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox3.Text = dtrAgro2K.GetValue(0)
            TextBox1.Text = dtrAgro2K.GetValue(1)
            TextBox2.Text = dtrAgro2K.GetValue(2)
            ComboBox1.SelectedIndex = dtrAgro2K.GetValue(3)
            strCambios(0) = dtrAgro2K.GetValue(2)
            strCambios(1) = ComboBox1.SelectedItem
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem
        Dim cntmenuItem5 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"
        cntmenuItem5.Text = "Igual"

        cntmnuBeneficiariosRg.MenuItems.Add(cntmenuItem1)
        cntmnuBeneficiariosRg.MenuItems.Add(cntmenuItem2)
        cntmnuBeneficiariosRg.MenuItems.Add(cntmenuItem3)
        cntmnuBeneficiariosRg.MenuItems.Add(cntmenuItem4)
        cntmnuBeneficiariosRg.MenuItems.Add(cntmenuItem5)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, ComboBox1.GotFocus, TextBox2.GotFocus

        UltraStatusBar1.Panels.Item(0).Text = IIf(sender.name = "TextBox1", "Código", IIf(sender.name = "TextBox2", "Nombre", "Estado"))
        UltraStatusBar1.Panels.Item(1).Text = sender.tag
        sender.selectall()

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, ComboBox1.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "TextBox1" : MoverRegistro("Igual") : TextBox2.Focus()
                Case "TextBox2" : ComboBox1.Focus()
                Case "ComboBox1" : TextBox1.Focus()
            End Select
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                TextBox1.Text = strUbicar
                MoverRegistro("Igual")
            End If
        End If

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        'Dim frmNew As New frmGeneral
        'lngRegistro = TextBox3.Text
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        'frmNew.ShowDialog()
    End Sub

    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Dim frmNew As New frmBitacora
        lngRegistro = TextBox3.Text
        frmNew.ShowDialog()
    End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Dim frmNew As New actrptViewer
        intRptFactura = 30
        strQuery = "exec sp_Reportes " & intModulo & " "
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        MoverRegistro("Primero")
    End Sub

    Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
        MoverRegistro("Anterior")
    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        MoverRegistro("Siguiente")
    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        MoverRegistro("Ultimo")
    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        MoverRegistro("Igual")
    End Sub

End Class
