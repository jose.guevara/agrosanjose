Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptFactContado2
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private txtCantidad As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtCodigoUnidad As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtValor As DataDynamics.ActiveReports.TextBox = Nothing
    Friend WithEvents txtPrecio As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtDescripcion As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtCodigoProducto As DataDynamics.ActiveReports.TextBox
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptFactContado2))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtCantidad = New DataDynamics.ActiveReports.TextBox
        Me.txtCodigoUnidad = New DataDynamics.ActiveReports.TextBox
        Me.txtValor = New DataDynamics.ActiveReports.TextBox
        Me.txtPrecio = New DataDynamics.ActiveReports.TextBox
        Me.txtDescripcion = New DataDynamics.ActiveReports.TextBox
        Me.txtCodigoProducto = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtValor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtCantidad, Me.txtCodigoUnidad, Me.txtValor, Me.txtPrecio, Me.txtDescripcion, Me.txtCodigoProducto})
        Me.Detail.Height = 0.1875!
        Me.Detail.Name = "Detail"
        '
        'txtCantidad
        '
        Me.txtCantidad.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCantidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidad.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCantidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidad.Border.RightColor = System.Drawing.Color.Black
        Me.txtCantidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidad.Border.TopColor = System.Drawing.Color.Black
        Me.txtCantidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidad.DataField = "Cantidad"
        Me.txtCantidad.Height = 0.1875!
        Me.txtCantidad.Left = 0.0625!
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Style = ""
        Me.txtCantidad.Top = 0.0!
        Me.txtCantidad.Width = 0.875!
        '
        'txtCodigoUnidad
        '
        Me.txtCodigoUnidad.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCodigoUnidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoUnidad.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCodigoUnidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoUnidad.Border.RightColor = System.Drawing.Color.Black
        Me.txtCodigoUnidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoUnidad.Border.TopColor = System.Drawing.Color.Black
        Me.txtCodigoUnidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoUnidad.DataField = "CadigoUnidad"
        Me.txtCodigoUnidad.Height = 0.1875!
        Me.txtCodigoUnidad.Left = 1.0!
        Me.txtCodigoUnidad.Name = "txtCodigoUnidad"
        Me.txtCodigoUnidad.Style = ""
        Me.txtCodigoUnidad.Top = 0.0!
        Me.txtCodigoUnidad.Width = 0.9375!
        '
        'txtValor
        '
        Me.txtValor.Border.BottomColor = System.Drawing.Color.Black
        Me.txtValor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtValor.Border.LeftColor = System.Drawing.Color.Black
        Me.txtValor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtValor.Border.RightColor = System.Drawing.Color.Black
        Me.txtValor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtValor.Border.TopColor = System.Drawing.Color.Black
        Me.txtValor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtValor.DataField = "Valor"
        Me.txtValor.Height = 0.1875!
        Me.txtValor.Left = 6.625!
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Style = ""
        Me.txtValor.Top = 0.0!
        Me.txtValor.Width = 1.5!
        '
        'txtPrecio
        '
        Me.txtPrecio.Border.BottomColor = System.Drawing.Color.Black
        Me.txtPrecio.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecio.Border.LeftColor = System.Drawing.Color.Black
        Me.txtPrecio.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecio.Border.RightColor = System.Drawing.Color.Black
        Me.txtPrecio.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecio.Border.TopColor = System.Drawing.Color.Black
        Me.txtPrecio.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecio.DataField = "Precio"
        Me.txtPrecio.Height = 0.1875!
        Me.txtPrecio.Left = 5.6875!
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.OutputFormat = resources.GetString("txtPrecio.OutputFormat")
        Me.txtPrecio.Style = "text-align: right; "
        Me.txtPrecio.Text = Nothing
        Me.txtPrecio.Top = 0.0!
        Me.txtPrecio.Visible = False
        Me.txtPrecio.Width = 0.8125!
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDescripcion.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDescripcion.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDescripcion.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDescripcion.Border.RightColor = System.Drawing.Color.Black
        Me.txtDescripcion.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDescripcion.Border.TopColor = System.Drawing.Color.Black
        Me.txtDescripcion.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDescripcion.DataField = "Descripcion"
        Me.txtDescripcion.Height = 0.1875!
        Me.txtDescripcion.Left = 3.1875!
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.OutputFormat = resources.GetString("txtDescripcion.OutputFormat")
        Me.txtDescripcion.Style = "text-align: right; "
        Me.txtDescripcion.Text = Nothing
        Me.txtDescripcion.Top = 0.0!
        Me.txtDescripcion.Width = 2.375!
        '
        'txtCodigoProducto
        '
        Me.txtCodigoProducto.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCodigoProducto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProducto.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCodigoProducto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProducto.Border.RightColor = System.Drawing.Color.Black
        Me.txtCodigoProducto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProducto.Border.TopColor = System.Drawing.Color.Black
        Me.txtCodigoProducto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProducto.DataField = "CodigoProducto"
        Me.txtCodigoProducto.Height = 0.1875!
        Me.txtCodigoProducto.Left = 2.125!
        Me.txtCodigoProducto.Name = "txtCodigoProducto"
        Me.txtCodigoProducto.OutputFormat = resources.GetString("txtCodigoProducto.OutputFormat")
        Me.txtCodigoProducto.Style = "text-align: right; "
        Me.txtCodigoProducto.Text = Nothing
        Me.txtCodigoProducto.Top = 0.0!
        Me.txtCodigoProducto.Width = 0.9375!
        '
        'PageHeader
        '
        Me.PageHeader.Height = 1.270833!
        Me.PageHeader.Name = "PageHeader"
        '
        'PageFooter
        '
        Me.PageFooter.Height = 0.40625!
        Me.PageFooter.Name = "PageFooter"
        '
        'actrptFactContado2
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.5!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 9.604167!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" & _
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtValor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptFactContado2_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.Document.Printer.PrinterName = ""
    End Sub

End Class
