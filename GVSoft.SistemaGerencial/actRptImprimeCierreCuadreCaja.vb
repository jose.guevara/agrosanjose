Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document
Imports GVSoft.SistemaGerencial.Utilidades

Public Class actRptImprimeCierreCuadreCaja
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actRptImprimeCierreCuadreCaja_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

    End Sub

    Private Sub actRptImprimeCierreCuadreCaja_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.5F
        TextBox25.Text = Format(Now, "dd-MMM-yyyy")
        TextBox26.Text = Format(Now, "hh:mm:ss tt")
        TextBox7.Text = Format(Now, "Long Date")
        TextBox3.Text = Format(Now, "Long Time")
        Label.Text = strNombEmpresa

        'UbicarAgencia(lngRegUsuario)
        'lblAgencia.Text = lblAgencia.Text & sNombreAgencia
        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub GroupHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format
        Dim Dif, TotalIng, TotalEgr, TotalCaja As Double

        Try
            TotalIng = SUConversiones.ConvierteADouble(lblTotalIngresos.Text)
            TotalEgr = SUConversiones.ConvierteADouble(lblTotalEgresos.Text)

            Dif = TotalIng - TotalEgr
            lblDifIngEgr.Text = Dif

            lblDiferenciaEfectivo.Text = SUConversiones.ConvierteADouble(lblDineroCaja.Text) - SUConversiones.ConvierteADouble(lblDifIngEgr.Text)

            'TotalCaja = (ConvierteADouble(lblSaldoAnterior.Text) + ConvierteADouble(lblTotalIngresos.Text)) - ConvierteADouble(lblGastos.Text)
            TotalCaja = (SUConversiones.ConvierteADouble(lblSaldoAnterior.Text) + SUConversiones.ConvierteADouble(lblTotalIngresos.Text) + SUConversiones.ConvierteADouble(lblOtrosIngresos.Text) + SUConversiones.ConvierteADouble(lblSobranteCaja.Text)) - SUConversiones.ConvierteADouble(lblGastos.Text)
            lblTotalCaja.Text = TotalCaja

        Catch ex As Exception
            Return
        End Try

    End Sub

End Class
