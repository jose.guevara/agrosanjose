Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class actrptImprimirTicketVenta

    Private Sub actrptImprimirTicketVenta_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim intLblIncr As Integer
        Dim dtAgencia As DataTable = Nothing
        Try
            dtAgencia = RNAgencias.ObtieneAgenciaxCodigo(lngRegAgencia)
            If dtAgencia IsNot Nothing Then
                If dtAgencia.Rows.Count > 0 Then
                    Label127.Text = dtAgencia.Rows(0)("descripcion")
                    Label136.Text = dtAgencia.Rows(0)("direccion")
                    Label137.Text = dtAgencia.Rows(0)("telefono")
                    Label138.Text = dtAgencia.Rows(0)("Ruc")
                End If
            End If
            Label2.Text = intDiaFact
            Label3.Text = strMesFact
            Label4.Text = lngYearFact
            Label.Text = UCase(strClienteFact)
            Label1.Text = UCase(strVendedor)
            'Label103.Text = "U$ " & CStr(Format(dblSubtotal, "#,##0.#0"))  // TipoCambio
            'Label104.Text = "U$ " & CStr(Format(dblImpuesto, "#,##0.#0"))  // TipoCambio
            'Label105.Text = "U$ " & CStr(Format(dblRetencion, "#,##0.#0")) // TipoCambio
            'Label106.Text = "U$ " & CStr(Format(dblTotal, "#,##0.#0"))     // TipoCambio
            Label103.Text = Format(dblSubtotal, "#,##0.#0")
            Label104.Text = Format(dblImpuesto, "#,##0.#0")
            Label105.Text = Format(dblRetencion, "#,##0.#0")
            Label106.Text = Format(dblTotal, "#,##0.#0")
            lblPago.Text = Format(dblTotalPagar, "#,##0.#0")
            lblCambio.Text = Format(dblCambio, "#,##0.#0")
            Label107.Text = strNumeroFact
            Label108.Text = strValorFactCOR
            Label109.Text = "T/C  " & CStr(dblTipoCambio)
            intLblIncr = 0
            'Dim total As Integer = 0
            'For intIncr = 0 To 13
            'If arrContado(intIncr, 0) = "" Then
            'Exit For
            'End If
            'For intIncr2 = 0 To 6
            'intLblIncr = intLblIncr + 1
            'dtR("Codigo") = arrContado(intIncr, intIncr2)
            'If intIncr2 = 5 Then
            'If lblCollection(intLblIncr).text = "Si" Then
            'lblCollection(intLblIncr).text = "*"
            'End If
            'End If
            'Next intIncr2
            'total = total + 1
            'Next intIncr
            'Label129.Text = total

            Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
            Me.PageSettings.PaperHeight = 11.0F
            Me.PageSettings.PaperWidth = 8.5F
            Me.Document.Printer.PrinterName = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub actrptFactContado_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Me.Document.Print(False, False)
        'POPOPEN$ = Chr$(27) + Chr$(112) + Chr$(0) + Chr$(50) + Chr$(250)
        'cash drawer tm-u210----> 27,112,0,25,250 	
        'cash drawer tm-u220----> 27,112,0,25,250 OR 27,112 	

        ' Print(1, Chr(27), Chr(112), Chr(48), Chr(20), Chr(200))
        Print(Chr(27) & Chr(112) & Chr(0))
        'PrintLine(1, Chr(27) + Chr(112) + Chr(0) + Chr(25) + Chr(250))
    End Sub

End Class
