Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class actrptEstadoCuentasProveedores 
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actrptEstadoCuentasProveedores_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.0F
        Label.Text = strNombEmpresa

        TextBox7.Text = Format(Now, "Long Date")
        TextBox3.Text = Format(Now, "Long Time")

        TextBox25.Text = Format(Now, "Short Date")
        TextBox26.Text = Format(Now, "Long Time")
        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actrptEstadoCuentasProveedores_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select
    End Sub
End Class
