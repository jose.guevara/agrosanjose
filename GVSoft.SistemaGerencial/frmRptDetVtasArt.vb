Imports System.Data.SqlClient
Imports System.Text
Imports System.Configuration
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Entidades
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Utilidades
Imports Microsoft.Reporting.WinForms

Public Class frmRptDetVtasArt
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox9 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox10 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox11 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox12 As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ListBox13 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox14 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox15 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox16 As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox17 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox18 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox19 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox20 As System.Windows.Forms.ListBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ListBox21 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox22 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox23 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox24 As System.Windows.Forms.ListBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ListBox25 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox26 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox27 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox28 As System.Windows.Forms.ListBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents ListBox29 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox30 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox31 As System.Windows.Forms.ListBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdReportes As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cbReportes As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ListBox32 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRptDetVtasArt))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ListBox32 = New System.Windows.Forms.ListBox
        Me.ListBox31 = New System.Windows.Forms.ListBox
        Me.ListBox30 = New System.Windows.Forms.ListBox
        Me.ListBox29 = New System.Windows.Forms.ListBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.ListBox28 = New System.Windows.Forms.ListBox
        Me.ListBox27 = New System.Windows.Forms.ListBox
        Me.ListBox26 = New System.Windows.Forms.ListBox
        Me.ListBox25 = New System.Windows.Forms.ListBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.ListBox24 = New System.Windows.Forms.ListBox
        Me.ListBox23 = New System.Windows.Forms.ListBox
        Me.ListBox22 = New System.Windows.Forms.ListBox
        Me.ListBox21 = New System.Windows.Forms.ListBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.ListBox20 = New System.Windows.Forms.ListBox
        Me.ListBox19 = New System.Windows.Forms.ListBox
        Me.ListBox18 = New System.Windows.Forms.ListBox
        Me.ListBox17 = New System.Windows.Forms.ListBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.ListBox16 = New System.Windows.Forms.ListBox
        Me.ListBox15 = New System.Windows.Forms.ListBox
        Me.ListBox14 = New System.Windows.Forms.ListBox
        Me.ListBox13 = New System.Windows.Forms.ListBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ListBox12 = New System.Windows.Forms.ListBox
        Me.ListBox11 = New System.Windows.Forms.ListBox
        Me.ListBox10 = New System.Windows.Forms.ListBox
        Me.ListBox9 = New System.Windows.Forms.ListBox
        Me.ListBox8 = New System.Windows.Forms.ListBox
        Me.ListBox7 = New System.Windows.Forms.ListBox
        Me.ListBox6 = New System.Windows.Forms.ListBox
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.cmdReportes = New DevComponents.DotNetBar.ButtonX
        Me.Label18 = New System.Windows.Forms.Label
        Me.cbReportes = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ListBox32)
        Me.GroupBox1.Controls.Add(Me.ListBox31)
        Me.GroupBox1.Controls.Add(Me.ListBox30)
        Me.GroupBox1.Controls.Add(Me.ListBox29)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.ListBox28)
        Me.GroupBox1.Controls.Add(Me.ListBox27)
        Me.GroupBox1.Controls.Add(Me.ListBox26)
        Me.GroupBox1.Controls.Add(Me.ListBox25)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.ListBox24)
        Me.GroupBox1.Controls.Add(Me.ListBox23)
        Me.GroupBox1.Controls.Add(Me.ListBox22)
        Me.GroupBox1.Controls.Add(Me.ListBox21)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.ListBox20)
        Me.GroupBox1.Controls.Add(Me.ListBox19)
        Me.GroupBox1.Controls.Add(Me.ListBox18)
        Me.GroupBox1.Controls.Add(Me.ListBox17)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ListBox16)
        Me.GroupBox1.Controls.Add(Me.ListBox15)
        Me.GroupBox1.Controls.Add(Me.ListBox14)
        Me.GroupBox1.Controls.Add(Me.ListBox13)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ListBox12)
        Me.GroupBox1.Controls.Add(Me.ListBox11)
        Me.GroupBox1.Controls.Add(Me.ListBox10)
        Me.GroupBox1.Controls.Add(Me.ListBox9)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 97)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(752, 348)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'ListBox32
        '
        Me.ListBox32.HorizontalScrollbar = True
        Me.ListBox32.Location = New System.Drawing.Point(656, 328)
        Me.ListBox32.Name = "ListBox32"
        Me.ListBox32.Size = New System.Drawing.Size(64, 17)
        Me.ListBox32.TabIndex = 48
        Me.ListBox32.Visible = False
        '
        'ListBox31
        '
        Me.ListBox31.HorizontalScrollbar = True
        Me.ListBox31.Location = New System.Drawing.Point(504, 328)
        Me.ListBox31.Name = "ListBox31"
        Me.ListBox31.Size = New System.Drawing.Size(120, 17)
        Me.ListBox31.TabIndex = 47
        Me.ListBox31.Visible = False
        '
        'ListBox30
        '
        Me.ListBox30.HorizontalScrollbar = True
        Me.ListBox30.Location = New System.Drawing.Point(656, 256)
        Me.ListBox30.Name = "ListBox30"
        Me.ListBox30.Size = New System.Drawing.Size(88, 69)
        Me.ListBox30.TabIndex = 46
        '
        'ListBox29
        '
        Me.ListBox29.HorizontalScrollbar = True
        Me.ListBox29.Location = New System.Drawing.Point(504, 256)
        Me.ListBox29.Name = "ListBox29"
        Me.ListBox29.Size = New System.Drawing.Size(144, 69)
        Me.ListBox29.TabIndex = 45
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(512, 240)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(232, 16)
        Me.Label17.TabIndex = 44
        Me.Label17.Text = "Registro de los Municipios"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox28
        '
        Me.ListBox28.HorizontalScrollbar = True
        Me.ListBox28.Location = New System.Drawing.Point(400, 328)
        Me.ListBox28.Name = "ListBox28"
        Me.ListBox28.Size = New System.Drawing.Size(64, 17)
        Me.ListBox28.TabIndex = 43
        Me.ListBox28.Visible = False
        '
        'ListBox27
        '
        Me.ListBox27.HorizontalScrollbar = True
        Me.ListBox27.Location = New System.Drawing.Point(256, 328)
        Me.ListBox27.Name = "ListBox27"
        Me.ListBox27.Size = New System.Drawing.Size(120, 17)
        Me.ListBox27.TabIndex = 42
        Me.ListBox27.Visible = False
        '
        'ListBox26
        '
        Me.ListBox26.HorizontalScrollbar = True
        Me.ListBox26.Location = New System.Drawing.Point(400, 256)
        Me.ListBox26.Name = "ListBox26"
        Me.ListBox26.Size = New System.Drawing.Size(88, 69)
        Me.ListBox26.TabIndex = 41
        '
        'ListBox25
        '
        Me.ListBox25.HorizontalScrollbar = True
        Me.ListBox25.Location = New System.Drawing.Point(256, 256)
        Me.ListBox25.Name = "ListBox25"
        Me.ListBox25.Size = New System.Drawing.Size(144, 69)
        Me.ListBox25.TabIndex = 40
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(256, 240)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(232, 16)
        Me.Label16.TabIndex = 39
        Me.Label16.Text = "Registro de los Departamentos"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox24
        '
        Me.ListBox24.HorizontalScrollbar = True
        Me.ListBox24.Location = New System.Drawing.Point(160, 328)
        Me.ListBox24.Name = "ListBox24"
        Me.ListBox24.Size = New System.Drawing.Size(64, 17)
        Me.ListBox24.TabIndex = 38
        Me.ListBox24.Visible = False
        '
        'ListBox23
        '
        Me.ListBox23.HorizontalScrollbar = True
        Me.ListBox23.Location = New System.Drawing.Point(16, 328)
        Me.ListBox23.Name = "ListBox23"
        Me.ListBox23.Size = New System.Drawing.Size(120, 17)
        Me.ListBox23.TabIndex = 37
        Me.ListBox23.Visible = False
        '
        'ListBox22
        '
        Me.ListBox22.HorizontalScrollbar = True
        Me.ListBox22.Location = New System.Drawing.Point(160, 256)
        Me.ListBox22.Name = "ListBox22"
        Me.ListBox22.Size = New System.Drawing.Size(88, 69)
        Me.ListBox22.TabIndex = 36
        '
        'ListBox21
        '
        Me.ListBox21.HorizontalScrollbar = True
        Me.ListBox21.Location = New System.Drawing.Point(8, 256)
        Me.ListBox21.Name = "ListBox21"
        Me.ListBox21.Size = New System.Drawing.Size(144, 69)
        Me.ListBox21.TabIndex = 35
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(8, 240)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(232, 16)
        Me.Label15.TabIndex = 34
        Me.Label15.Text = "Registro de las SubClases"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(512, 168)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(128, 16)
        Me.Label14.TabIndex = 33
        Me.Label14.Text = "<F5> AYUDA"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(264, 168)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 16)
        Me.Label13.TabIndex = 32
        Me.Label13.Text = "<F5> AYUDA"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(512, 128)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(232, 16)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Registro de los Clientes"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(264, 128)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(224, 16)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Registro de los Art�culos"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox20
        '
        Me.ListBox20.Location = New System.Drawing.Point(656, 216)
        Me.ListBox20.Name = "ListBox20"
        Me.ListBox20.Size = New System.Drawing.Size(88, 17)
        Me.ListBox20.TabIndex = 29
        Me.ListBox20.Visible = False
        '
        'ListBox19
        '
        Me.ListBox19.HorizontalScrollbar = True
        Me.ListBox19.Location = New System.Drawing.Point(656, 144)
        Me.ListBox19.Name = "ListBox19"
        Me.ListBox19.Size = New System.Drawing.Size(88, 69)
        Me.ListBox19.TabIndex = 28
        '
        'ListBox18
        '
        Me.ListBox18.Location = New System.Drawing.Point(408, 216)
        Me.ListBox18.Name = "ListBox18"
        Me.ListBox18.Size = New System.Drawing.Size(88, 17)
        Me.ListBox18.TabIndex = 27
        Me.ListBox18.Visible = False
        '
        'ListBox17
        '
        Me.ListBox17.HorizontalScrollbar = True
        Me.ListBox17.Location = New System.Drawing.Point(408, 144)
        Me.ListBox17.Name = "ListBox17"
        Me.ListBox17.Size = New System.Drawing.Size(88, 69)
        Me.ListBox17.TabIndex = 26
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(504, 144)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(144, 20)
        Me.TextBox2.TabIndex = 25
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(256, 144)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(144, 20)
        Me.TextBox1.TabIndex = 24
        '
        'ListBox16
        '
        Me.ListBox16.Location = New System.Drawing.Point(160, 216)
        Me.ListBox16.Name = "ListBox16"
        Me.ListBox16.Size = New System.Drawing.Size(80, 17)
        Me.ListBox16.TabIndex = 23
        Me.ListBox16.Visible = False
        '
        'ListBox15
        '
        Me.ListBox15.Location = New System.Drawing.Point(8, 216)
        Me.ListBox15.Name = "ListBox15"
        Me.ListBox15.Size = New System.Drawing.Size(120, 17)
        Me.ListBox15.TabIndex = 22
        Me.ListBox15.Visible = False
        '
        'ListBox14
        '
        Me.ListBox14.HorizontalScrollbar = True
        Me.ListBox14.Location = New System.Drawing.Point(160, 144)
        Me.ListBox14.Name = "ListBox14"
        Me.ListBox14.Size = New System.Drawing.Size(88, 69)
        Me.ListBox14.TabIndex = 21
        '
        'ListBox13
        '
        Me.ListBox13.HorizontalScrollbar = True
        Me.ListBox13.Location = New System.Drawing.Point(8, 144)
        Me.ListBox13.Name = "ListBox13"
        Me.ListBox13.Size = New System.Drawing.Size(144, 69)
        Me.ListBox13.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 128)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 16)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Registro de los Proveedores"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(512, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(232, 16)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Registro de los Vendedores"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox12
        '
        Me.ListBox12.Location = New System.Drawing.Point(656, 104)
        Me.ListBox12.Name = "ListBox12"
        Me.ListBox12.Size = New System.Drawing.Size(88, 17)
        Me.ListBox12.TabIndex = 17
        Me.ListBox12.Visible = False
        '
        'ListBox11
        '
        Me.ListBox11.Location = New System.Drawing.Point(504, 104)
        Me.ListBox11.Name = "ListBox11"
        Me.ListBox11.Size = New System.Drawing.Size(120, 17)
        Me.ListBox11.TabIndex = 16
        Me.ListBox11.Visible = False
        '
        'ListBox10
        '
        Me.ListBox10.HorizontalScrollbar = True
        Me.ListBox10.Location = New System.Drawing.Point(656, 32)
        Me.ListBox10.Name = "ListBox10"
        Me.ListBox10.Size = New System.Drawing.Size(88, 69)
        Me.ListBox10.TabIndex = 15
        '
        'ListBox9
        '
        Me.ListBox9.HorizontalScrollbar = True
        Me.ListBox9.Location = New System.Drawing.Point(504, 32)
        Me.ListBox9.Name = "ListBox9"
        Me.ListBox9.Size = New System.Drawing.Size(144, 69)
        Me.ListBox9.TabIndex = 14
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(408, 104)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(88, 17)
        Me.ListBox8.TabIndex = 13
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(256, 104)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(120, 17)
        Me.ListBox7.TabIndex = 12
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(408, 32)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(88, 69)
        Me.ListBox6.TabIndex = 11
        '
        'ListBox5
        '
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(256, 32)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(144, 69)
        Me.ListBox5.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(264, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(224, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Registro de las Clases"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Registro de las Sucursales"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(160, 104)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(88, 17)
        Me.ListBox4.TabIndex = 3
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(8, 104)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(120, 17)
        Me.ListBox3.TabIndex = 2
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(160, 32)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(88, 69)
        Me.ListBox2.TabIndex = 1
        '
        'ListBox1
        '
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(8, 32)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(144, 69)
        Me.ListBox1.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.TextBox4)
        Me.GroupBox2.Controls.Add(Me.TextBox3)
        Me.GroupBox2.Controls.Add(Me.ComboBox2)
        Me.GroupBox2.Controls.Add(Me.ComboBox1)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 450)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(752, 88)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(328, 28)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox2.TabIndex = 32
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(152, 27)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 31
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(592, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 13)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Documento"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(664, 56)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(80, 20)
        Me.TextBox4.TabIndex = 26
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(664, 24)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(80, 20)
        Me.TextBox3.TabIndex = 25
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Items.AddRange(New Object() {"", "activo", "anulado"})
        Me.ComboBox2.Location = New System.Drawing.Point(528, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(72, 21)
        Me.ComboBox2.TabIndex = 10
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"", "contado", "cr�dito"})
        Me.ComboBox1.Location = New System.Drawing.Point(392, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(72, 21)
        Me.ComboBox1.TabIndex = 9
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(232, 24)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker2.TabIndex = 8
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(56, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(616, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Nombre"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(480, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(43, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Status"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(360, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Tipo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(184, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Fec Fin"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Fec Ini"
        '
        'Timer1
        '
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.cmdExportar)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(752, 55)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 33
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(75, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmdExportar
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.FontBold = True
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.FontBold = True
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'cmdReportes
        '
        Me.cmdReportes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdReportes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdReportes.Image = CType(resources.GetObject("cmdReportes.Image"), System.Drawing.Image)
        Me.cmdReportes.ImageFixedSize = New System.Drawing.Size(30, 30)
        Me.cmdReportes.Location = New System.Drawing.Point(395, 60)
        Me.cmdReportes.Name = "cmdReportes"
        Me.cmdReportes.Size = New System.Drawing.Size(55, 36)
        Me.cmdReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdReportes.TabIndex = 36
        Me.cmdReportes.Tooltip = "<b><font color=""#17365D"">Generar Reporte</font></b>"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(4, 71)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 35
        Me.Label18.Text = "Reportes"
        '
        'cbReportes
        '
        Me.cbReportes.DisplayMember = "Text"
        Me.cbReportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReportes.FormattingEnabled = True
        Me.cbReportes.ItemHeight = 15
        Me.cbReportes.Location = New System.Drawing.Point(68, 68)
        Me.cbReportes.Name = "cbReportes"
        Me.cbReportes.Size = New System.Drawing.Size(324, 21)
        Me.cbReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbReportes.TabIndex = 34
        '
        'frmRptDetVtasArt
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(752, 551)
        Me.Controls.Add(Me.cmdReportes)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cbReportes)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRptDetVtasArt"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes de la Facturaci�n"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmRptDetVtasArt"
    Private Sub frmRptDetVtasArt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Me.Top = 0
            'Me.Left = 0
            blnClear = False
            Iniciar()
            Limpiar()
            RNReportes.CargarComboReportes(cbReportes, nIdUsuario, nIdModulo)
            If intAccesos(102) = 0 Then
                'MenuItem1.Visible = False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub frmRptDetVtasArt_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try

            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F5 Then
                If Label13.Visible = True Then
                    intListadoAyuda = 3
                    Dim frmNew As New frmListadoAyuda
                    Timer1.Interval = 200
                    Timer1.Enabled = True
                    frmNew.ShowDialog()
                ElseIf Label14.Visible = True Then
                    intListadoAyuda = 2
                    Dim frmNew As New frmListadoAyuda
                    Timer1.Interval = 200
                    Timer1.Enabled = True
                    frmNew.ShowDialog()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub Iniciar()

        Try

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Agencias Where Estado <> 2 order by Registro"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox1.Items.Add(dtrAgro2K.GetValue(1))
                ListBox3.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)

            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Clases Where Estado <> 2 order by Registro"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox5.Items.Add(dtrAgro2K.GetValue(1))
                ListBox7.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = ""
            strQuery = "Select Registro, Nombre From prm_Vendedores Where Estado <> 2 order by Nombre"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox9.Items.Add(dtrAgro2K.GetValue(1))
                ListBox11.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)

            strQuery = ""
            strQuery = "Select Registro, Nombre From prm_Proveedores Where Estado <> 2 order by Nombre"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox13.Items.Add(dtrAgro2K.GetValue(1))
                ListBox15.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)

            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_SubClases Where Estado <> 2 order by descripcion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox21.Items.Add(dtrAgro2K.GetValue(1))
                ListBox23.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)

            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Departamentos Where Estado <> 2 order by descripcion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox25.Items.Add(dtrAgro2K.GetValue(1))
                ListBox27.Items.Add(dtrAgro2K.GetValue(0))
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)

            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub Limpiar()

        DateTimePicker1.Value = DateSerial(Year(Now), Month(Now), 1)
        DateTimePicker2.Value = Now
        ListBox2.Items.Clear()
        ListBox4.Items.Clear()
        ListBox6.Items.Clear()
        ListBox8.Items.Clear()
        ListBox10.Items.Clear()
        ListBox12.Items.Clear()
        ListBox14.Items.Clear()
        ListBox16.Items.Clear()
        ListBox17.Items.Clear()
        ListBox18.Items.Clear()
        ListBox19.Items.Clear()
        ListBox20.Items.Clear()
        ListBox22.Items.Clear()
        ListBox24.Items.Clear()
        ListBox26.Items.Clear()
        ListBox28.Items.Clear()
        ListBox29.Items.Clear()
        ListBox30.Items.Clear()
        ListBox31.Items.Clear()
        ListBox32.Items.Clear()
        ComboBox1.SelectedIndex = 0
        ComboBox2.SelectedIndex = 1
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        CheckBox1.Checked = True
        CheckBox2.Checked = True
        intRptExportar = 0
        Label13.Visible = False
        Label14.Visible = False

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox9.DoubleClick, ListBox10.DoubleClick, ListBox13.DoubleClick, ListBox14.DoubleClick, ListBox17.DoubleClick, ListBox19.DoubleClick, ListBox21.DoubleClick, ListBox22.DoubleClick, ListBox25.DoubleClick, ListBox26.DoubleClick, ListBox29.DoubleClick, ListBox30.DoubleClick
        Try

            Select Case sender.name.ToString
                Case "ListBox1"
                    If ListBox1.SelectedItem IsNot Nothing Then
                        ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
                    End If

                Case "ListBox2"
                    If ListBox2.SelectedIndex >= 0 Then
                        ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                    End If
                Case "ListBox5"
                    If ListBox5.SelectedItem IsNot Nothing Then
                        ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem)
                    End If

                Case "ListBox6"
                    If ListBox6.SelectedIndex >= 0 Then
                        ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                    End If
                Case "ListBox9"
                    If ListBox9.SelectedItem IsNot Nothing Then
                        ListBox10.Items.Add(ListBox9.SelectedItem) : ListBox11.SelectedIndex = ListBox9.SelectedIndex : ListBox12.Items.Add(ListBox11.SelectedItem)
                    End If

                Case "ListBox10"
                    If ListBox10.SelectedIndex >= 0 Then
                        ListBox12.Items.RemoveAt(ListBox10.SelectedIndex) : ListBox10.Items.RemoveAt(ListBox10.SelectedIndex)
                    End If
                Case "ListBox13"
                    If ListBox13.SelectedItem IsNot Nothing Then
                        ListBox14.Items.Add(ListBox13.SelectedItem) : ListBox15.SelectedIndex = ListBox13.SelectedIndex : ListBox16.Items.Add(ListBox15.SelectedItem)
                    End If

                Case "ListBox14"
                    If ListBox14.SelectedIndex >= 0 Then
                        ListBox16.Items.RemoveAt(ListBox14.SelectedIndex) : ListBox14.Items.RemoveAt(ListBox14.SelectedIndex)
                    End If
                Case "ListBox17"
                    If ListBox17.SelectedIndex >= 0 Then
                        ListBox18.Items.RemoveAt(ListBox17.SelectedIndex) : ListBox17.Items.RemoveAt(ListBox17.SelectedIndex)
                    End If
                Case "ListBox19"
                    If ListBox19.SelectedIndex >= 0 Then
                        ListBox20.Items.RemoveAt(ListBox19.SelectedIndex) : ListBox19.Items.RemoveAt(ListBox19.SelectedIndex)
                    End If
                Case "ListBox21"
                    If ListBox21.SelectedItem IsNot Nothing Then
                        ListBox22.Items.Add(ListBox21.SelectedItem) : ListBox23.SelectedIndex = ListBox21.SelectedIndex : ListBox24.Items.Add(ListBox23.SelectedItem)
                    End If

                Case "ListBox22"
                    If ListBox22.SelectedIndex >= 0 Then
                        ListBox24.Items.RemoveAt(ListBox22.SelectedIndex) : ListBox22.Items.RemoveAt(ListBox22.SelectedIndex)
                    End If
                Case "ListBox25"
                    If ListBox25.SelectedItem IsNot Nothing Then
                        ListBox26.Items.Add(ListBox25.SelectedItem) : ListBox27.SelectedIndex = ListBox25.SelectedIndex : ListBox28.Items.Add(ListBox27.SelectedItem) : lngRegistro = CInt(ListBox27.SelectedItem) : UbicarMunicipios()
                    End If

                Case "ListBox26"
                    If ListBox26.SelectedIndex >= 0 Then
                        ListBox28.Items.RemoveAt(ListBox26.SelectedIndex) : ListBox26.Items.RemoveAt(ListBox26.SelectedIndex)
                    End If
                Case "ListBox29"
                    If ListBox29.SelectedItem IsNot Nothing Then
                        ListBox30.Items.Add(ListBox29.SelectedItem) : ListBox31.SelectedIndex = ListBox29.SelectedIndex : ListBox32.Items.Add(ListBox31.SelectedItem)
                    End If

                Case "ListBox30"
                    If ListBox30.SelectedIndex >= 0 Then
                        ListBox32.Items.RemoveAt(ListBox30.SelectedIndex) : ListBox30.Items.RemoveAt(ListBox30.SelectedIndex)
                    End If
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus

        If sender.name = "TextBox1" Then
            Label13.Visible = True
        ElseIf sender.name = "TextBox2" Then
            Label14.Visible = True
        End If

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus, TextBox2.LostFocus

        If sender.name = "TextBox1" Then
            Label13.Visible = False
        ElseIf sender.name = "TextBox2" Then
            Label14.Visible = False
        End If

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown

        If e.KeyCode = Keys.Enter Then
            If sender.name = "TextBox1" Then
                If UbicarProducto() = False Then
                    MsgBox("C�digo de producto no est� registrado.   Verifique.", MsgBoxStyle.Critical, "Extracci�n de Registro")
                End If
                TextBox1.Text = ""
            ElseIf sender.name = "TextBox2" Then
                If UbicarCliente() = False Then
                    MsgBox("C�digo de cliente no est� registrado.   Verifique.", MsgBoxStyle.Critical, "Extracci�n de Registro")
                End If
                TextBox2.Text = ""
            End If
        End If

    End Sub

    Function UbicarProducto() As Boolean

        lngRegistro = 0
        strQuery = ""
        strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P Where P.Codigo = '" & TextBox1.Text & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            lngRegistro = dtrAgro2K.GetValue(0)
            ListBox17.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
            ListBox18.Items.Add(lngRegistro)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If lngRegistro = 0 Then
            UbicarProducto = False
            Exit Function
        End If
        UbicarProducto = True

    End Function

    Function UbicarCliente() As Boolean

        lngRegistro = 0
        strQuery = ""
        strQuery = "Select C.Registro, C.Codigo, C.Nombre from prm_Clientes C Where C.Codigo = '" & TextBox2.Text & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            lngRegistro = dtrAgro2K.GetValue(0)
            ListBox19.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
            ListBox20.Items.Add(lngRegistro)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If lngRegistro = 0 Then
            UbicarCliente = False
            Exit Function
        End If
        UbicarCliente = True

    End Function

    Sub UbicarMunicipios()

        ListBox29.Items.Clear()
        ListBox31.Items.Clear()
        strQuery = ""
        strQuery = "Select registro, descripcion from prm_Municipios Where registro2 = " & lngRegistro & " "
        strQuery = strQuery + "order by descripcion"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox29.Items.Add(dtrAgro2K.GetValue(1))
            ListBox31.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub GenerarQuery()

        Dim lngFecha As Long

        If ListBox18.Items.Count > 0 Then                                       'Productos
            If intRptFactura = 6 Then
                strQuery = strQuery + "'and D.ProdRegistro in ("
            Else
                strQuery = strQuery + "'and P.Registro in ("
            End If
            If ListBox18.Items.Count = 1 Then
                ListBox18.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox18.SelectedItem & ") ', "
            ElseIf ListBox18.Items.Count > 1 Then
                For intIncr = 0 To ListBox18.Items.Count - 2
                    ListBox18.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ", "
                Next
                ListBox18.SelectedIndex = ListBox18.Items.Count - 1
                strQuery = strQuery + "" & ListBox18.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox4.Items.Count > 0 Then                                        'Agencias
            If intRptFactura = 12 Then
                strQuery = strQuery + "'"
            Else
                strQuery = strQuery + "'and S.Registro in ("
            End If
            If ListBox4.Items.Count = 1 Then
                ListBox4.SelectedIndex = 0
                If intRptFactura = 12 Then
                    strQuery = strQuery + "" & ListBox4.SelectedItem & "', "
                Else
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                End If

            ElseIf ListBox4.Items.Count > 1 Then
                For intIncr = 0 To ListBox4.Items.Count - 2
                    ListBox4.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                Next
                ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox8.Items.Count > 0 Then                                        'Clases
            strQuery = strQuery + "'and C.Registro in ("
            If ListBox8.Items.Count = 1 Then
                ListBox8.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
            ElseIf ListBox8.Items.Count > 1 Then
                For intIncr = 0 To ListBox8.Items.Count - 2
                    ListBox8.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ", "
                Next
                ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox20.Items.Count > 0 Then                                       'Clientes
            If intRptFactura <> 51 And intRptFactura <> 52 Then
                strQuery = strQuery + "'and F.CliRegistro in ("
            Else
                strQuery = strQuery + "'and L.Registro in ("
            End If
            If ListBox20.Items.Count = 1 Then
                ListBox20.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox20.SelectedItem & ") ', "
            ElseIf ListBox20.Items.Count > 1 Then
                For intIncr = 0 To ListBox20.Items.Count - 2
                    ListBox20.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox20.SelectedItem & ", "
                Next
                ListBox20.SelectedIndex = ListBox20.Items.Count - 1
                strQuery = strQuery + "" & ListBox20.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox12.Items.Count > 0 Then                                       'Vendedores
            If intRptFactura = 1 Or intRptFactura = 2 Or intRptFactura = 6 Or intRptFactura = 8 Or intRptFactura = 10 Or intRptFactura = 13 Then
                strQuery = strQuery + "'and V.Registro in ("
            Else
                If intRptFactura = 12 Then
                    strQuery = strQuery + "'"
                Else
                    strQuery = strQuery + "'and F.VendRegistro in ("
                End If

            End If
            If ListBox12.Items.Count = 1 Then
                ListBox12.SelectedIndex = 0
                If intRptFactura = 12 Then
                    strQuery = strQuery + "" & ListBox12.SelectedItem & " ', "
                Else
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                End If

            ElseIf ListBox12.Items.Count > 1 Then
                For intIncr = 0 To ListBox12.Items.Count - 2
                    ListBox12.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ", "
                Next
                ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                If intRptFactura = 12 Then
                    strQuery = strQuery + "" & ListBox12.SelectedItem & "', "
                Else
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                End If

            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox16.Items.Count > 0 Then                                       'Proveedores
            strQuery = strQuery + "'and P.RegProveedor in ("
            If ListBox16.Items.Count = 1 Then
                ListBox16.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox16.SelectedItem & ") ', "
            ElseIf ListBox16.Items.Count > 1 Then
                For intIncr = 0 To ListBox16.Items.Count - 2
                    ListBox16.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox16.SelectedItem & ", "
                Next
                ListBox16.SelectedIndex = ListBox16.Items.Count - 1
                strQuery = strQuery + "" & ListBox16.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox24.Items.Count > 0 Then                                       'SubClases
            strQuery = strQuery + "'and P.RegSubClase in ("
            If ListBox24.Items.Count = 1 Then
                ListBox24.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox24.SelectedItem & ") ', "
            ElseIf ListBox24.Items.Count > 1 Then
                For intIncr = 0 To ListBox24.Items.Count - 2
                    ListBox24.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox24.SelectedItem & ", "
                Next
                ListBox24.SelectedIndex = ListBox24.Items.Count - 1
                strQuery = strQuery + "" & ListBox24.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        lngFecha = 0
        strFechaInicial = ""
        strFechaFinal = ""
        If CheckBox1.Checked = True Then                                        'Fecha Inicial
            lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaInicial = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        Else
            lngFecha = 20001101
            strFechaInicial = "01-Nov-2000"
        End If
        If intRptFactura = 12 Then
            strQuery = strQuery + "'" & lngFecha & " ', "
        Else
            strQuery = strQuery + "'and F.NumFechaIng >= " & lngFecha & " ', "
        End If
        If CheckBox2.Checked = True Then                                        'Fecha Final
            lngFecha = Format(DateTimePicker2.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "dd-MMM-yyyy")
        Else
            lngFecha = Format(Now, "yyyyMMdd")
            strFechaFinal = Format(Now, "dd-MMM-yyyy")
        End If
        If intRptFactura = 12 Then
            strQuery = strQuery + "'" & lngFecha & "', "
        Else
            strQuery = strQuery + "'and F.NumFechaIng <= " & lngFecha & " ', "
        End If

        If Trim(TextBox3.Text) <> "" Then                                       'Nombre del Cliente
            strQuery = strQuery + "'and F.CliNombre like ''%" & TextBox3.Text & "%'' ', "
        Else
            strQuery = strQuery + "' ', "
        End If
        If ComboBox2.SelectedIndex > 0 Then                                     'Status de la Factura
            strQuery = strQuery + "'and F.Status = " & ComboBox2.SelectedIndex - 1 & " ', "
        Else
            strQuery = strQuery + "' ', "
        End If
        If ComboBox1.SelectedIndex > 0 Then                                     'Tipo de Factura
            strQuery = strQuery + "'and F.TipoFactura = " & ComboBox1.SelectedIndex - 1 & " ', "
        Else
            strQuery = strQuery + "' ', "
        End If
        If Trim(TextBox4.Text) <> "" Then                                       'Numero de Factura
            strQuery = strQuery + "'and F.Numero = ''" & TextBox4.Text & "'' ', "
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox28.Items.Count > 0 Then                                       'Departamentos
            strQuery = strQuery + "'and M.Registro in ("
            If ListBox28.Items.Count = 1 Then
                ListBox28.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox28.SelectedItem & ") ', "
            ElseIf ListBox28.Items.Count > 1 Then
                For intIncr = 0 To ListBox28.Items.Count - 2
                    ListBox28.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox28.SelectedItem & ", "
                Next
                ListBox28.SelectedIndex = ListBox28.Items.Count - 1
                strQuery = strQuery + "" & ListBox28.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox32.Items.Count > 0 Then                                       'Municipios
            If intRptFactura = 6 Or intRptFactura = 15 Then
                strQuery = strQuery + "'and C.MunRegistro in ("
            Else
                strQuery = strQuery + "'and L.MunRegistro in ("
            End If
            If ListBox32.Items.Count = 1 Then
                ListBox32.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox32.SelectedItem & ") ' "
            ElseIf ListBox32.Items.Count > 1 Then
                For intIncr = 0 To ListBox32.Items.Count - 2
                    ListBox32.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox32.SelectedItem & ", "
                Next
                ListBox32.SelectedIndex = ListBox32.Items.Count - 1
                strQuery = strQuery + "" & ListBox32.SelectedItem & ") ' "
            End If
        Else
            strQuery = strQuery + "' ' "
        End If
        If ConfigurationManager.AppSettings("MuestraDebug").ToString() = "1" Then
            MsgBox(strQuery)
        End If
    End Sub

    Sub ObtenerRegistroVendedor()
        If ListBox12.Items.Count > 0 Then                                       'Vendedores
            If intRptFactura = 1 Or intRptFactura = 2 Or intRptFactura = 6 Or intRptFactura = 8 Or intRptFactura = 10 Or intRptFactura = 17 Then
                strQuery = strQuery + "'and V.Registro in ("
            End If
            If ListBox12.Items.Count = 1 Then
                ListBox12.SelectedIndex = 0
                If intRptFactura = 12 Then
                    strQuery = strQuery + "" & ListBox12.SelectedItem & " ', "
                Else
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ' "
                End If

            ElseIf ListBox12.Items.Count > 1 Then
                For intIncr = 0 To ListBox12.Items.Count - 2
                    ListBox12.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ", "
                Next
                ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                If intRptFactura = 12 Then
                    strQuery = strQuery + "" & ListBox12.SelectedItem & "', "
                Else
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ")' "
                End If

            End If
        Else
            strQuery = strQuery + "' ' "
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If strUbicar <> "" Then
            Timer1.Enabled = False
            If intListadoAyuda = 2 Then
                TextBox2.Text = strUbicar : UbicarCliente() : ListBox19.Focus() : TextBox2.Text = ""
            ElseIf intListadoAyuda = 3 Then
                TextBox1.Text = strUbicar : UbicarProducto() : ListBox17.Focus() : TextBox1.Text = ""
            End If
        End If

    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.DoubleClick, TextBox2.DoubleClick

        If sender.name = "TextBox1" Then
            intListadoAyuda = 3
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        ElseIf sender.name = "TextBox2" Then
            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Public Sub MostrarReporte()
        Dim frmNew As New actrptViewer
        Dim strAgencias As String = String.Empty
        Dim strAgencias2 As String = String.Empty
        Dim strTipoFactura As String = "-1"

        Try


            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            strQuery = ""
            intRptOtros = 0
            intRptPresup = 0
            intRptCtasCbr = 0
            intRptFactura = cbReportes.SelectedValue
            intRptInventario = 0
            intRptImpFactura = 0
            intRptTipoPrecio = 0
            intRptCheques = 0
            intRptImpRecibos = 0
            strFechaInicial = ""
            strFechaFinal = ""

            strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")

            If (intRptFactura > 0 And intRptFactura <= 14) Or (intRptFactura = 51) Or (intRptFactura = 52) Or (intRptFactura = 62) Then
                strQuery = "exec sp_RptFacturas " & intRptFactura & ", "
                GenerarQuery()
                strFechaReporte = "Del " & strFechaInicial & " Al " & strFechaFinal & ""
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            ElseIf intRptFactura = 15 Then

                strTipoFactura = ComboBox1.SelectedItem
                If strTipoFactura.Trim().Length = 0 Then
                    strTipoFactura = "-1"
                End If

                If ListBox4.Items.Count = 0 Then
                    MsgBox("Debe escoger al menos una agencia", MsgBoxStyle.Critical, "Validacion")
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Sub
                End If
                If ListBox4.Items.Count = 1 Then
                    strAgencias = ListBox4.Items(0)
                    strAgencias2 = ListBox4.Items(0)
                End If
                If ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 1
                        ListBox4.SelectedIndex = intIncr
                        strAgencias = strAgencias + "" & ListBox4.SelectedItem & ","
                    Next
                    strAgencias2 = Microsoft.VisualBasic.Left(strAgencias, Len(strAgencias) - 1)
                End If

                If Len(strFechaInicial) <= 0 Then
                    strFechaInicial = 19000101
                End If

                If Len(strFechaFinal) <= 0 Then
                    strFechaFinal = 19000101
                End If

                LlamaReporteRS(strFechaInicial, strFechaFinal, strAgencias2, strTipoFactura, String.Empty)
                Exit Sub
            ElseIf intRptFactura = 16 Then

                If ListBox4.Items.Count = 0 Then
                    MsgBox("Debe escoger al menos una agencia", MsgBoxStyle.Critical, "Validacion")
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Sub
                End If
                If ListBox4.Items.Count = 1 Then
                    strAgencias = ListBox4.Items(0)
                    strAgencias2 = ListBox4.Items(0)
                End If
                If ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 1
                        ListBox4.SelectedIndex = intIncr
                        strAgencias = strAgencias + "" & ListBox4.SelectedItem & ","
                    Next
                    strAgencias2 = Microsoft.VisualBasic.Left(strAgencias, Len(strAgencias) - 1)
                End If

                If Len(strFechaInicial) <= 0 Then
                    strFechaInicial = 19000101
                End If

                If Len(strFechaFinal) <= 0 Then
                    strFechaFinal = 19000101
                End If

                strQuery = "exec rptIngresoMensualRecibo '" & strFechaInicial & "','" & strFechaFinal & "', '" & strAgencias2 & "'"
                'GenerarQuery()
                strFechaReporte = "Del " & strFechaInicial & " Al " & strFechaFinal & ""
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            ElseIf intRptFactura = 17 Then

                strTipoFactura = ComboBox1.SelectedItem
                If strTipoFactura.Trim().Length = 0 Then
                    strTipoFactura = "-1"
                End If

                If ListBox4.Items.Count = 0 Then
                    MsgBox("Debe escoger al menos una agencia", MsgBoxStyle.Critical, "Validacion")
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Sub
                End If
                If ListBox4.Items.Count = 1 Then
                    strAgencias = ListBox4.Items(0)
                    strAgencias2 = ListBox4.Items(0)
                End If
                If ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 1
                        ListBox4.SelectedIndex = intIncr
                        strAgencias = strAgencias + "" & ListBox4.SelectedItem & ","
                    Next
                    strAgencias2 = Microsoft.VisualBasic.Left(strAgencias, Len(strAgencias) - 1)
                End If

                If Len(strFechaInicial) <= 0 Then
                    strFechaInicial = 19000101
                End If

                If Len(strFechaFinal) <= 0 Then
                    strFechaFinal = 19000101
                End If

                'strQuery = "exec rptGananciaFacturasContadoyCredito '" & strFechaInicial & "','" & strFechaFinal & "', '" & strAgencias2 & "', '" & strTipoFactura & "'"
                strQuery = "exec rptGananciaFacturasContadoyCredito '" & strFechaInicial & "','" & strFechaFinal & "', '" & strAgencias2 & "', '" & strTipoFactura & "', "
                ObtenerRegistroVendedor()
                strFechaReporte = "Del " & Format(DateTimePicker1.Value, "dd-MMM-yyyy") & " Al " & Format(DateTimePicker2.Value, "dd-MMM-yyyy") & ""
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            ElseIf intRptFactura = 61 Then

                strTipoFactura = ComboBox1.SelectedItem
                If strTipoFactura.Trim().Length = 0 Then
                    strTipoFactura = "-1"
                End If

                If ListBox4.Items.Count = 1 Then
                    strAgencias = ListBox4.Items(0)
                    strAgencias2 = ListBox4.Items(0)
                End If
                If ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 1
                        ListBox4.SelectedIndex = intIncr
                        strAgencias = strAgencias + "" & ListBox4.SelectedItem & ","
                    Next
                    strAgencias2 = Microsoft.VisualBasic.Left(strAgencias, Len(strAgencias) - 1)
                End If

                If Len(strFechaInicial) <= 0 Then
                    strFechaInicial = 19000101
                End If

                If Len(strFechaFinal) <= 0 Then
                    strFechaFinal = 19000101
                End If

                strQuery = "exec rptFacturasAnuladas '" & strFechaInicial & "','" & strFechaFinal & "', '" & strAgencias2 & "'"
                strFechaReporte = "Del " & strFechaInicial & " Al " & strFechaFinal & ""
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub
    Private Sub LlamaReporteRS(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psSucursal As String,
                                                       ByVal psTipoFactura As String, ByVal psVendedor As String)
        Dim lsNombreYRutaReporte As String = String.Empty
        Dim lffrmNew As frmRptRSVisor = Nothing
        Dim llocalReport As LocalReport = Nothing
        Dim lpTipo As ReportParameter = Nothing
        Try
            lffrmNew = New frmRptRSVisor()
            lffrmNew.ReportViewer1.ProcessingMode = ProcessingMode.Local
            lsNombreYRutaReporte = "Sistemas_Gerenciales.FacturaVentasConsecutivo.rdlc"

            llocalReport = lffrmNew.ReportViewer1.LocalReport
            llocalReport.ReportEmbeddedResource = lsNombreYRutaReporte

            Dim dsListadoProducto As New ReportDataSource()
            dsListadoProducto.Name = "dsFacturasConsecutivas"
            dsListadoProducto.Value = RNReportes.ReporteFacturasConsecutivas(psFechaInicio, psFechaFin, psSucursal, psTipoFactura, psVendedor)

            llocalReport.DataSources.Add(dsListadoProducto)
            'Refresh the report  
            lffrmNew.ReportViewer1.RefreshReport()

            lffrmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            lffrmNew.Show()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Boton OK")
        End Try
    End Sub
    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        Try
            MostrarReporte()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdiExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiExcel.Click
        intRptExportar = 1
    End Sub

    Private Sub cmdiHtml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiHtml.Click
        intRptExportar = 2
    End Sub

    Private Sub cmdiPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiPdf.Click
        intRptExportar = 3
    End Sub

    Private Sub cmdiRtf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiRtf.Click
        intRptExportar = 4
    End Sub

    Private Sub cmdiTiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiTiff.Click
        intRptExportar = 5
    End Sub

    Private Sub cmdExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExportar.Click
        intRptExportar = 0
    End Sub

End Class
