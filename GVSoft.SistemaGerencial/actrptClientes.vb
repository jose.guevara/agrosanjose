Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptClientes
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Label2 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Private WithEvents lblEmpresa As DataDynamics.ActiveReports.Label
    Friend WithEvents Line1 As DataDynamics.ActiveReports.Line
    Friend WithEvents Line6 As DataDynamics.ActiveReports.Line
    Friend WithEvents Line2 As DataDynamics.ActiveReports.Line
    Friend WithEvents Label10 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalProductos As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Label7 As Label
    Private TextBox21 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptClientes))
        Me.Detail = New DataDynamics.ActiveReports.Detail()
        Me.TextBox = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox()
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox5 = New DataDynamics.ActiveReports.TextBox()
        Me.lblEmpresa = New DataDynamics.ActiveReports.Label()
        Me.Line1 = New DataDynamics.ActiveReports.Line()
        Me.Line6 = New DataDynamics.ActiveReports.Line()
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox()
        Me.Line2 = New DataDynamics.ActiveReports.Line()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.txtTotalProductos = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox6 = New DataDynamics.ActiveReports.TextBox()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox, Me.TextBox1, Me.TextBox2, Me.TextBox3, Me.TextBox6})
        Me.Detail.Height = 0.1875!
        Me.Detail.Name = "Detail"
        '
        'TextBox
        '
        Me.TextBox.DataField = "Codigo"
        Me.TextBox.Height = 0.1875!
        Me.TextBox.Left = 0.0625!
        Me.TextBox.Name = "TextBox"
        Me.TextBox.Text = "TextBox"
        Me.TextBox.Top = 0!
        Me.TextBox.Width = 1.0055!
        '
        'TextBox1
        '
        Me.TextBox1.DataField = "Descripcion"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 2.85!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Text = "TextBox1"
        Me.TextBox1.Top = 0!
        Me.TextBox1.Width = 2.938!
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "Estado"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 5.982!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Text = "TextBox2"
        Me.TextBox2.Top = 0!
        Me.TextBox2.Width = 0.8600001!
        '
        'TextBox3
        '
        Me.TextBox3.DataField = "Estado2"
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 5.982!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Text = "TextBox3"
        Me.TextBox3.Top = 0!
        Me.TextBox3.Width = 0.8600001!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label, Me.Label1, Me.Label2, Me.Label4, Me.Label5, Me.Label6, Me.TextBox4, Me.TextBox5, Me.lblEmpresa, Me.Line1, Me.Line6, Me.Label7})
        Me.PageHeader.Height = 1.270833!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label
        '
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.0625!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label.Text = "C�digo"
        Me.Label.Top = 0.875!
        Me.Label.Width = 0.5625!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 2.85!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label1.Text = "Descripci�n"
        Me.Label1.Top = 0.875!
        Me.Label1.Width = 2.6875!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 5.992!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label2.Text = "Estado"
        Me.Label2.Top = 0.8750001!
        Me.Label2.Width = 0.5!
        '
        'Label4
        '
        Me.Label4.Height = 0.25!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.0625!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: left; font-weight: bold; font-size: 14.25pt"
        Me.Label4.Text = "Label4"
        Me.Label4.Top = 0.4375!
        Me.Label4.Width = 4.4375!
        '
        'Label5
        '
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 4.5625!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label5.Text = "Fecha de Impresi�n"
        Me.Label5.Top = 0.0625!
        Me.Label5.Width = 1.35!
        '
        'Label6
        '
        Me.Label6.Height = 0.2!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 4.5625!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label6.Text = "Hora de Impresi�n"
        Me.Label6.Top = 0.25!
        Me.Label6.Width = 1.35!
        '
        'TextBox4
        '
        Me.TextBox4.Height = 0.2!
        Me.TextBox4.Left = 5.9375!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Style = "text-align: right"
        Me.TextBox4.Text = Nothing
        Me.TextBox4.Top = 0.0625!
        Me.TextBox4.Width = 0.9!
        '
        'TextBox5
        '
        Me.TextBox5.Height = 0.2!
        Me.TextBox5.Left = 5.9375!
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Style = "text-align: right"
        Me.TextBox5.Text = Nothing
        Me.TextBox5.Top = 0.25!
        Me.TextBox5.Width = 0.9!
        '
        'lblEmpresa
        '
        Me.lblEmpresa.Height = 0.25!
        Me.lblEmpresa.HyperLink = Nothing
        Me.lblEmpresa.Left = 0.0625!
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Style = "text-align: left; font-weight: bold; font-size: 14.25pt"
        Me.lblEmpresa.Text = ""
        Me.lblEmpresa.Top = 0.125!
        Me.lblEmpresa.Width = 4.4375!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.25!
        Me.Line1.Width = 7.0625!
        Me.Line1.X1 = 0!
        Me.Line1.X2 = 7.0625!
        Me.Line1.Y1 = 1.25!
        Me.Line1.Y2 = 1.25!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 0!
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 0.8125!
        Me.Line6.Width = 7.0625!
        Me.Line6.X1 = 0!
        Me.Line6.X2 = 7.0625!
        Me.Line6.Y1 = 0.8125!
        Me.Line6.Y2 = 0.8125!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.TextBox21, Me.Line2, Me.Label10, Me.txtTotalProductos})
        Me.PageFooter.Height = 0.40625!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label13
        '
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 5.625!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label13.Text = "P�gina"
        Me.Label13.Top = 0.125!
        Me.Label13.Width = 0.5500001!
        '
        'TextBox21
        '
        Me.TextBox21.Height = 0.2!
        Me.TextBox21.Left = 6.1875!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: right"
        Me.TextBox21.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox21.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0.125!
        Me.TextBox21.Width = 0.3499999!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.0625!
        Me.Line2.Width = 7.0625!
        Me.Line2.X1 = 0!
        Me.Line2.X2 = 7.0625!
        Me.Line2.Y1 = 0.0625!
        Me.Line2.Y2 = 0.0625!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0.1875!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold"
        Me.Label10.Text = "Total Productos:"
        Me.Label10.Top = 0.125!
        Me.Label10.Width = 1.25!
        '
        'txtTotalProductos
        '
        Me.txtTotalProductos.DataField = "Codigo"
        Me.txtTotalProductos.Height = 0.1979167!
        Me.txtTotalProductos.Left = 1.5625!
        Me.txtTotalProductos.Name = "txtTotalProductos"
        Me.txtTotalProductos.Style = "font-weight: bold"
        Me.txtTotalProductos.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.txtTotalProductos.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalProductos.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalProductos.Text = Nothing
        Me.txtTotalProductos.Top = 0.125!
        Me.txtTotalProductos.Width = 1.0!
        '
        'TextBox6
        '
        Me.TextBox6.DataField = "num_id"
        Me.TextBox6.Height = 0.1875!
        Me.TextBox6.Left = 1.15!
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Text = "TextBox"
        Me.TextBox6.Top = 0!
        Me.TextBox6.Width = 1.625!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 1.15!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label7.Text = "Idntificaci�n"
        Me.Label7.Top = 0.875!
        Me.Label7.Width = 1.125!
        '
        'actrptClientes
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.25!
        Me.PageSettings.Margins.Right = 0.05!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.114583!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" &
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptClientes_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        'Label3.Visible = False
        TextBox3.Visible = False
        If intModulo = 1 Then
            Me.Label.Text = "N�mero"
            Me.Label1.Text = "Nombre de la Agencia"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 3 Then
            Me.Label2.Visible = True
            Me.Label2.Text = "Departamento"
            Me.TextBox2.Visible = True
        ElseIf intModulo = 5 Then
            Me.Label1.Text = "Nombre"
        ElseIf intModulo = 10 Then
            'Me.Label3.Visible = True
            Me.TextBox3.Visible = True
            Me.Label2.Text = "Tipo"
            'Me.Label3.Text = "Estado"
        ElseIf intModulo = 19 Then
            Me.Label.Text = "Fecha"
            Me.Label1.Text = "Tipo de Cambio"
            Me.Label2.Visible = False
            Me.TextBox2.Visible = False
        ElseIf intModulo = 23 Then
            Me.Label.Text = "Cuenta"
            Me.Label1.Text = "Descripci�n"
            Me.Label2.Visible = False
            Me.TextBox2.Visible = False
        ElseIf intModulo = 27 Then
            Me.Label1.Text = "Nombres"
        ElseIf intModulo = 28 Then
            Me.Label.Text = "Cuenta"
            Me.Label1.Text = "Descripci�n"
        ElseIf intModulo = 29 Then
            'Me.Label3.Visible = True
            Me.TextBox3.Visible = True
            Me.Label.Text = "Estado"
            Me.Label1.Text = "Cuenta Bancaria"
            Me.Label2.Text = "Ck Inicial"
            'Me.Label3.Text = "Ck Final"
        ElseIf intModulo = 35 Then
            Me.TextBox3.Visible = False
            Me.Label.Text = "Vendedor"
            Me.Label1.Text = "Nombre"
            Me.Label2.Text = "Estrato"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
            Me.TextBox2.Size = New System.Drawing.SizeF(2.91, 0.188)
        ElseIf intModulo = 11 Then
            ' Me.txtPrecioCosto.Visible = True
            'Me.txtPrecioVentaMayoreo.Visible = True
            'Me.txtPrecioVentaPublico.Visible = True
            'Me.lblPrecioCosto.Visible = True
            'Me.lblPrecioVentaMayoreo.Visible = True
            'Me.lblPrecioVentaPublico.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 43 Then
            'Me.txtPrecioCosto.Visible = True
            ' Me.lblPrecioCosto.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
            Me.Label2.Left = 5.25!
            Me.TextBox2.Left = 5.25!
        ElseIf intModulo = 44 Then
            'Me.txtPrecioVentaPublico.Visible = True
            'Me.lblPrecioVentaPublico.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 45 Then
            ' Me.txtPrecioVentaMayoreo.Visible = True
            ' Me.lblPrecioVentaMayoreo.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 50 Then
            Me.TextBox3.Visible = False
            Me.Label.Text = "Usuario"
            Me.Label1.Text = "Nombre"
            Me.Label2.Text = "Estado"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
            Me.TextBox2.Size = New System.Drawing.SizeF(2.91, 0.188)
        End If
        TextBox4.Text = Format(Now, "dd-MMM-yyyy")
        TextBox5.Text = Format(Now, "hh:mm:ss tt")
        Select Case intModulo
            Case 1 : Label4.Text = "REPORTE DE LAS AGENCIAS"
            Case 2 : Label4.Text = "REPORTE DE LOS DEPARTAMENTOS"
            Case 3 : Label4.Text = "REPORTE DE LOS MUNICIPIOS"
            Case 4 : Label4.Text = "REPORTE DE LOS VENDEDORES"
            Case 5 : Label4.Text = "REPORTE DE LOS CLIENTES"
            Case 6 : Label4.Text = "REPORTE DE LAS CLASES"
            Case 7 : Label4.Text = "REPORTE DE LAS SUBCLASES"
            Case 8 : Label4.Text = "REPORTE DE LOS EMPAQUES"
            Case 9 : Label4.Text = "REPORTE DE LAS UNIDADES"
            Case 10 : Label4.Text = "REPORTE DE LOS PROVEEDORES"
            Case 11 : Label4.Text = "LISTADO DE PRECIOS"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 13 : Label4.Text = "REPORTE DE LOS NEGOCIOS"
            Case 17 : Label4.Text = "REPORTE DE LAS RAZONES PARA ANULAR"
            Case 19 : Label4.Text = "REPORTE DE LOS TIPO DE CAMBIO"
            Case 22 : Label4.Text = "REPORTE DE LOS BANCOS"
            Case 23 : Label4.Text = "REPORTE DE LAS CUENTAS CONTABLES"
            Case 27 : Label4.Text = "REPORTE DE LOS EMPLEADOS"
            Case 28 : Label4.Text = "REPORTE DE LAS CUENTAS BANCARIAS"
            Case 29 : Label4.Text = "REPORTE DE LAS CHEQUERAS"
            Case 35 : Label4.Text = "REPORTE DE VENDEDORES Y SUS COMISIONES"
            Case 42 : Label4.Text = "REPORTE DE LOS BENEFICIARIOS"
            Case 43 : Label4.Text = "REPORTE DE PRECIO DE COSTO"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 44 : Label4.Text = "REPORTE DE PRECIO DE VENTAS MINORISTA"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 45 : Label4.Text = "REPORTE DE PRECIO DE VENTAS MAYORISTA"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 50 : Label4.Text = "REPORTE DE USUARIOS"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "

        End Select
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.5F

        Me.Document.Printer.PrinterName = ""

    End Sub
End Class
