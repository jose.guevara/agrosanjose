<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCierrePeriodo
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCierrePeriodo))
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SuperTabItem_Buscar = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Label_periodoCerrar = New DevComponents.DotNetBar.LabelX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.DateF_FechaFin = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.DateF_FechaIni = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Bar2 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_EjecutarPrecierre = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem_msg_Precierre = New DevComponents.DotNetBar.LabelItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Label_tabcierres = New DevComponents.DotNetBar.LabelX()
        Me.ComboB_mes_TabCierres = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_ano_TabCierres = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_Buscarmes = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_TabCierre_cerrarmes = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_TabCierre_abrirmes = New DevComponents.DotNetBar.ButtonItem()
        Me.SuperTabItem_detalle = New DevComponents.DotNetBar.SuperTabItem()
        Me.LabelItem_pantalla = New DevComponents.DotNetBar.LabelItem()
        Me.ButtonItem_menuprincipal = New DevComponents.DotNetBar.ButtonItem()
        Me.BalloonTip_cierre = New DevComponents.DotNetBar.BalloonTip()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.ButtonItem_cerrarpantalla = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemPanel1 = New DevComponents.DotNetBar.ItemPanel()
        Me.expandablePanel1 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ToolbarB_BuscarAsiento = New DevComponents.DotNetBar.ButtonItem()
        Me.CI_totalCompro = New DevComponents.Editors.ComboItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ComboItem1 = New DevComponents.Editors.ComboItem()
        Me.ComboItem2 = New DevComponents.Editors.ComboItem()
        Me.ComboItem3 = New DevComponents.Editors.ComboItem()
        Me.ComboItem4 = New DevComponents.Editors.ComboItem()
        Me.ExpandablePanel3 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.GridP_SaldosN = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ExpandablePanel5 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.GridP_CierreDefinitivo = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ItemPanel2 = New DevComponents.DotNetBar.ItemPanel()
        Me.SuperTabControlPanel2.SuspendLayout()
        CType(Me.DateF_FechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateF_FechaIni, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel1.SuspendLayout()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.ItemPanel1.SuspendLayout()
        Me.expandablePanel1.SuspendLayout()
        Me.ExpandablePanel3.SuspendLayout()
        CType(Me.GridP_SaldosN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExpandablePanel5.SuspendLayout()
        CType(Me.GridP_CierreDefinitivo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ItemPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SuperTabItem_Buscar
        '
        Me.SuperTabItem_Buscar.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem_Buscar.GlobalItem = False
        Me.SuperTabItem_Buscar.Name = "SuperTabItem_Buscar"
        Me.SuperTabItem_Buscar.Text = "Pre Cierre Periodo"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Label_periodoCerrar)
        Me.SuperTabControlPanel2.Controls.Add(Me.LabelX27)
        Me.SuperTabControlPanel2.Controls.Add(Me.LabelX26)
        Me.SuperTabControlPanel2.Controls.Add(Me.DateF_FechaFin)
        Me.SuperTabControlPanel2.Controls.Add(Me.DateF_FechaIni)
        Me.SuperTabControlPanel2.Controls.Add(Me.Bar2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(1032, 140)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem_Buscar
        '
        'Label_periodoCerrar
        '
        Me.Label_periodoCerrar.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_periodoCerrar.BackgroundStyle.Class = ""
        Me.Label_periodoCerrar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_periodoCerrar.Location = New System.Drawing.Point(12, 34)
        Me.Label_periodoCerrar.Name = "Label_periodoCerrar"
        Me.Label_periodoCerrar.Size = New System.Drawing.Size(604, 23)
        Me.Label_periodoCerrar.TabIndex = 51
        Me.Label_periodoCerrar.Text = "Per�odo a Cerrar"
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.Class = ""
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(12, 96)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(75, 23)
        Me.LabelX27.TabIndex = 50
        Me.LabelX27.Text = "Cierre Hasta"
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.Class = ""
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(11, 63)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(75, 23)
        Me.LabelX26.TabIndex = 49
        Me.LabelX26.Text = "Cierre Desde"
        '
        'DateF_FechaFin
        '
        '
        '
        '
        Me.DateF_FechaFin.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaFin.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaFin.ButtonDropDown.Visible = True
        Me.DateF_FechaFin.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaFin.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaFin.IsPopupCalendarOpen = False
        Me.DateF_FechaFin.Location = New System.Drawing.Point(109, 98)
        Me.DateF_FechaFin.MaxDate = New Date(9998, 12, 30, 0, 0, 0, 0)
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaFin.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaFin.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaFin.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaFin.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaFin.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaFin.Name = "DateF_FechaFin"
        Me.DateF_FechaFin.Size = New System.Drawing.Size(412, 21)
        Me.DateF_FechaFin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaFin.TabIndex = 48
        '
        'DateF_FechaIni
        '
        '
        '
        '
        Me.DateF_FechaIni.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaIni.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaIni.ButtonDropDown.Visible = True
        Me.DateF_FechaIni.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaIni.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaIni.IsPopupCalendarOpen = False
        Me.DateF_FechaIni.Location = New System.Drawing.Point(106, 64)
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaIni.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaIni.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaIni.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaIni.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaIni.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaIni.Name = "DateF_FechaIni"
        Me.DateF_FechaIni.Size = New System.Drawing.Size(415, 21)
        Me.DateF_FechaIni.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaIni.TabIndex = 47
        '
        'Bar2
        '
        Me.Bar2.AccessibleDescription = "DotNetBar Bar (Bar2)"
        Me.Bar2.AccessibleName = "DotNetBar Bar"
        Me.Bar2.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar2.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar2.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_EjecutarPrecierre, Me.LabelItem_msg_Precierre})
        Me.Bar2.Location = New System.Drawing.Point(2, 2)
        Me.Bar2.MenuBar = True
        Me.Bar2.Name = "Bar2"
        Me.Bar2.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar2.Size = New System.Drawing.Size(1018, 26)
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar2.TabIndex = 46
        Me.Bar2.TabStop = False
        Me.Bar2.Text = "Bar2"
        '
        'ToolbarB_EjecutarPrecierre
        '
        Me.ToolbarB_EjecutarPrecierre.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_EjecutarPrecierre.ForeColor = System.Drawing.Color.White
        ' Me.ToolbarB_EjecutarPrecierre.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.MSN_MESSENGER_15
        Me.ToolbarB_EjecutarPrecierre.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Recyclebin_Empty
        Me.ToolbarB_EjecutarPrecierre.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_EjecutarPrecierre.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_EjecutarPrecierre.Name = "ToolbarB_EjecutarPrecierre"
        Me.ToolbarB_EjecutarPrecierre.Text = "Ejecutar PreCierre"
        '
        'LabelItem_msg_Precierre
        '
        Me.LabelItem_msg_Precierre.DividerStyle = True
        Me.LabelItem_msg_Precierre.ForeColor = System.Drawing.Color.Yellow
        Me.LabelItem_msg_Precierre.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.LabelItem_msg_Precierre.Name = "LabelItem_msg_Precierre"
        Me.LabelItem_msg_Precierre.Stretch = True
        Me.LabelItem_msg_Precierre.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Label_tabcierres)
        Me.SuperTabControlPanel1.Controls.Add(Me.ComboB_mes_TabCierres)
        Me.SuperTabControlPanel1.Controls.Add(Me.ComboB_ano_TabCierres)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX25)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX24)
        Me.SuperTabControlPanel1.Controls.Add(Me.Bar1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(1032, 140)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem_detalle
        '
        'Label_tabcierres
        '
        Me.Label_tabcierres.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_tabcierres.BackgroundStyle.Class = ""
        Me.Label_tabcierres.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_tabcierres.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_tabcierres.Location = New System.Drawing.Point(30, 94)
        Me.Label_tabcierres.Name = "Label_tabcierres"
        Me.Label_tabcierres.Size = New System.Drawing.Size(586, 23)
        Me.Label_tabcierres.TabIndex = 52
        Me.Label_tabcierres.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ComboB_mes_TabCierres
        '
        Me.ComboB_mes_TabCierres.DisplayMember = "Text"
        Me.ComboB_mes_TabCierres.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_mes_TabCierres.FormattingEnabled = True
        Me.ComboB_mes_TabCierres.ItemHeight = 15
        Me.ComboB_mes_TabCierres.Location = New System.Drawing.Point(76, 70)
        Me.ComboB_mes_TabCierres.Name = "ComboB_mes_TabCierres"
        Me.ComboB_mes_TabCierres.Size = New System.Drawing.Size(434, 21)
        Me.ComboB_mes_TabCierres.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_mes_TabCierres.TabIndex = 48
        '
        'ComboB_ano_TabCierres
        '
        Me.ComboB_ano_TabCierres.DisplayMember = "Text"
        Me.ComboB_ano_TabCierres.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_ano_TabCierres.FormattingEnabled = True
        Me.ComboB_ano_TabCierres.ItemHeight = 15
        Me.ComboB_ano_TabCierres.Location = New System.Drawing.Point(76, 35)
        Me.ComboB_ano_TabCierres.Name = "ComboB_ano_TabCierres"
        Me.ComboB_ano_TabCierres.Size = New System.Drawing.Size(434, 21)
        Me.ComboB_ano_TabCierres.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_ano_TabCierres.TabIndex = 47
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.Class = ""
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(26, 68)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(44, 23)
        Me.LabelX25.TabIndex = 50
        Me.LabelX25.Text = "Mes"
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.Class = ""
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(26, 33)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(44, 23)
        Me.LabelX24.TabIndex = 49
        Me.LabelX24.Text = "A�o"
        '
        'Bar1
        '
        Me.Bar1.AccessibleDescription = "DotNetBar Bar (Bar1)"
        Me.Bar1.AccessibleName = "DotNetBar Bar"
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar1.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_Buscarmes, Me.ToolbarB_TabCierre_cerrarmes, Me.ToolbarB_TabCierre_abrirmes})
        Me.Bar1.Location = New System.Drawing.Point(1, 0)
        Me.Bar1.MenuBar = True
        Me.Bar1.Name = "Bar1"
        Me.Bar1.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar1.Size = New System.Drawing.Size(617, 26)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar1.TabIndex = 46
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'ToolbarB_Buscarmes
        '
        Me.ToolbarB_Buscarmes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_Buscarmes.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_Buscarmes.Image = CType(resources.GetObject("ToolbarB_Buscarmes.Image"), System.Drawing.Image)
        Me.ToolbarB_Buscarmes.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_Buscarmes.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_Buscarmes.Name = "ToolbarB_Buscarmes"
        Me.ToolbarB_Buscarmes.Text = "Buscar Mes"
        '
        'ToolbarB_TabCierre_cerrarmes
        '
        Me.ToolbarB_TabCierre_cerrarmes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_TabCierre_cerrarmes.ForeColor = System.Drawing.Color.White
        ' Me.ToolbarB_TabCierre_cerrarmes.Image = Global.WinForm_Conta.My.Resources.Resources.Security
        Me.ToolbarB_TabCierre_cerrarmes.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ToolbarB_TabCierre_cerrarmes.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_TabCierre_cerrarmes.Name = "ToolbarB_TabCierre_cerrarmes"
        Me.ToolbarB_TabCierre_cerrarmes.Text = "Cerrar Mes Definitivamente"
        '
        'ToolbarB_TabCierre_abrirmes
        '
        Me.ToolbarB_TabCierre_abrirmes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_TabCierre_abrirmes.ForeColor = System.Drawing.Color.White
        'Me.ToolbarB_TabCierre_abrirmes.Image = Global.WinForm_Conta.My.Resources.Resources.systemcpl_dll_01_14
        Me.ToolbarB_TabCierre_cerrarmes.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ToolbarB_TabCierre_abrirmes.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_TabCierre_abrirmes.Name = "ToolbarB_TabCierre_abrirmes"
        Me.ToolbarB_TabCierre_abrirmes.Text = "Volver Abrir Mes"
        '
        'SuperTabItem_detalle
        '
        Me.SuperTabItem_detalle.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem_detalle.GlobalItem = False
        Me.SuperTabItem_detalle.Name = "SuperTabItem_detalle"
        Me.SuperTabItem_detalle.Text = "Cierre Periodo"
        '
        'LabelItem_pantalla
        '
        Me.LabelItem_pantalla.Enabled = False
        Me.LabelItem_pantalla.Name = "LabelItem_pantalla"
        Me.LabelItem_pantalla.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ButtonItem_menuprincipal
        '
        Me.ButtonItem_menuprincipal.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_menuprincipal.Image = CType(resources.GetObject("ButtonItem_menuprincipal.Image"), System.Drawing.Image)
        Me.ButtonItem_menuprincipal.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_menuprincipal.Name = "ButtonItem_menuprincipal"
        Me.ButtonItem_menuprincipal.Text = "Men� Principal"
        Me.ButtonItem_menuprincipal.Tooltip = "Retorna al Men� Principal de la Contabilidad"
        '
        'BalloonTip_cierre
        '
        Me.BalloonTip_cierre.AutoClose = False
        ' Me.BalloonTip_cierre.CaptionImage = Global.WinForm_Conta.My.Resources.Resources.Archivo_Asientos
        Me.BalloonTip_cierre.CaptionImage = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.BalloonTip_cierre.ShowBalloonOnFocus = True
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(1032, 166)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.SuperTabControl1.TabIndex = 17
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem_menuprincipal, Me.ButtonItem_cerrarpantalla, Me.SuperTabItem_Buscar, Me.SuperTabItem_detalle, Me.LabelItem_pantalla})
        Me.SuperTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'ButtonItem_cerrarpantalla
        '
        Me.ButtonItem_cerrarpantalla.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_cerrarpantalla.Image = CType(resources.GetObject("ButtonItem_cerrarpantalla.Image"), System.Drawing.Image)
        Me.ButtonItem_cerrarpantalla.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_cerrarpantalla.Name = "ButtonItem_cerrarpantalla"
        Me.ButtonItem_cerrarpantalla.Text = "Cerrar"
        Me.ButtonItem_cerrarpantalla.Tooltip = "Cierra la Ventana y Sale de la Aplicaci�n"
        '
        'ItemPanel1
        '
        '
        '
        '
        Me.ItemPanel1.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel1.ContainerControlProcessDialogKey = True
        Me.ItemPanel1.Controls.Add(Me.expandablePanel1)
        Me.ItemPanel1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_BuscarAsiento})
        Me.ItemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel1.Location = New System.Drawing.Point(2, 5)
        Me.ItemPanel1.Name = "ItemPanel1"
        Me.ItemPanel1.Size = New System.Drawing.Size(1021, 166)
        Me.ItemPanel1.TabIndex = 4
        Me.ItemPanel1.Text = "ItemPanel1"
        '
        'expandablePanel1
        '
        Me.expandablePanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.expandablePanel1.CollapseDirection = DevComponents.DotNetBar.eCollapseDirection.RightToLeft
        Me.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.expandablePanel1.Controls.Add(Me.SuperTabControl1)
        Me.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.expandablePanel1.ExpandButtonVisible = False
        Me.expandablePanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.expandablePanel1.Location = New System.Drawing.Point(0, 0)
        Me.expandablePanel1.Name = "expandablePanel1"
        Me.expandablePanel1.Size = New System.Drawing.Size(1086, 166)
        Me.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.expandablePanel1.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile
        Me.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.expandablePanel1.Style.GradientAngle = 90
        Me.expandablePanel1.Style.WordWrap = True
        Me.expandablePanel1.TabIndex = 13
        Me.expandablePanel1.TitleStyle.BackColor1.Color = System.Drawing.Color.Transparent
        Me.expandablePanel1.TitleStyle.BackColor2.Color = System.Drawing.Color.Transparent
        Me.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.expandablePanel1.TitleStyle.BorderColor.Color = System.Drawing.Color.Transparent
        Me.expandablePanel1.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.expandablePanel1.TitleStyle.BorderSide = DevComponents.DotNetBar.eBorderSide.None
        Me.expandablePanel1.TitleStyle.BorderWidth = 0
        Me.expandablePanel1.TitleStyle.CornerDiameter = 1
        Me.expandablePanel1.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.expandablePanel1.TitleStyle.GradientAngle = 90
        Me.expandablePanel1.TitleStyle.MarginLeft = 6
        Me.expandablePanel1.TitleText = "Title Bar"
        '
        'ToolbarB_BuscarAsiento
        '
        Me.ToolbarB_BuscarAsiento.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_BuscarAsiento.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_BuscarAsiento.Image = CType(resources.GetObject("ToolbarB_BuscarAsiento.Image"), System.Drawing.Image)
        Me.ToolbarB_BuscarAsiento.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_BuscarAsiento.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_BuscarAsiento.Name = "ToolbarB_BuscarAsiento"
        Me.ToolbarB_BuscarAsiento.Text = "Buscar Asiento  "
        '
        'CI_totalCompro
        '
        Me.CI_totalCompro.Text = "Totales por Comprobante"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem1.ForeColor = System.Drawing.Color.White
        ' Me.ButtonItem1.Image = Global.WinForm_Conta.My.Resources.Resources.MSN_MESSENGER_15
        Me.ButtonItem1.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ButtonItem1.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Text = "Ejecutar PreCierre"
        '
        'ComboItem1
        '
        Me.ComboItem1.Text = "Totales por Comprobante"
        '
        'ComboItem2
        '
        Me.ComboItem2.Text = "Totales por Comprobante"
        '
        'ComboItem3
        '
        Me.ComboItem3.Text = "Totales por Comprobante"
        '
        'ComboItem4
        '
        Me.ComboItem4.Text = "Totales por Comprobante"
        '
        'ExpandablePanel3
        '
        Me.ExpandablePanel3.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel3.Controls.Add(Me.GridP_SaldosN)
        Me.ExpandablePanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExpandablePanel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ExpandablePanel3.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel3.Name = "ExpandablePanel3"
        Me.ExpandablePanel3.Size = New System.Drawing.Size(1038, 305)
        Me.ExpandablePanel3.Style.Alignment = System.Drawing.StringAlignment.Far
        Me.ExpandablePanel3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel3.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile
        Me.ExpandablePanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel3.Style.GradientAngle = 90
        Me.ExpandablePanel3.TabIndex = 14
        Me.ExpandablePanel3.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel3.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel3.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel3.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel3.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel3.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel3.TitleText = "RESUMEN DE SALDOS"
        '
        'GridP_SaldosN
        '
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_SaldosN.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle17
        Me.GridP_SaldosN.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_SaldosN.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.GridP_SaldosN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_SaldosN.DefaultCellStyle = DataGridViewCellStyle19
        Me.GridP_SaldosN.EnableHeadersVisualStyles = False
        Me.GridP_SaldosN.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_SaldosN.Location = New System.Drawing.Point(6, 30)
        Me.GridP_SaldosN.Name = "GridP_SaldosN"
        Me.GridP_SaldosN.ReadOnly = True
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_SaldosN.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.GridP_SaldosN.RowHeadersWidth = 20
        Me.GridP_SaldosN.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_SaldosN.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_SaldosN.Size = New System.Drawing.Size(1020, 251)
        Me.GridP_SaldosN.TabIndex = 1
        '
        'ExpandablePanel5
        '
        Me.ExpandablePanel5.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel5.Controls.Add(Me.GridP_CierreDefinitivo)
        Me.ExpandablePanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExpandablePanel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ExpandablePanel5.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel5.Name = "ExpandablePanel5"
        Me.ExpandablePanel5.Size = New System.Drawing.Size(1038, 305)
        Me.ExpandablePanel5.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel5.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel5.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel5.Style.GradientAngle = 90
        Me.ExpandablePanel5.TabIndex = 15
        Me.ExpandablePanel5.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel5.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel5.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel5.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel5.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel5.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel5.TitleText = "CIERRES CONTABLES DEFINITIVOS"
        '
        'GridP_CierreDefinitivo
        '
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_CierreDefinitivo.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle21
        Me.GridP_CierreDefinitivo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_CierreDefinitivo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle22
        Me.GridP_CierreDefinitivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_CierreDefinitivo.DefaultCellStyle = DataGridViewCellStyle23
        Me.GridP_CierreDefinitivo.EnableHeadersVisualStyles = False
        Me.GridP_CierreDefinitivo.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_CierreDefinitivo.Location = New System.Drawing.Point(3, 22)
        Me.GridP_CierreDefinitivo.Name = "GridP_CierreDefinitivo"
        Me.GridP_CierreDefinitivo.ReadOnly = True
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_CierreDefinitivo.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.GridP_CierreDefinitivo.RowHeadersWidth = 20
        Me.GridP_CierreDefinitivo.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_CierreDefinitivo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_CierreDefinitivo.Size = New System.Drawing.Size(1023, 252)
        Me.GridP_CierreDefinitivo.TabIndex = 2
        '
        'ItemPanel2
        '
        '
        '
        '
        Me.ItemPanel2.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel2.ContainerControlProcessDialogKey = True
        Me.ItemPanel2.Controls.Add(Me.ExpandablePanel5)
        Me.ItemPanel2.Controls.Add(Me.ExpandablePanel3)
        Me.ItemPanel2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel2.Location = New System.Drawing.Point(-4, 191)
        Me.ItemPanel2.Name = "ItemPanel2"
        Me.ItemPanel2.Size = New System.Drawing.Size(1038, 305)
        Me.ItemPanel2.TabIndex = 5
        Me.ItemPanel2.Text = "ItemPanel2"
        '
        'frmCierrePeriodo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1029, 471)
        Me.Controls.Add(Me.ItemPanel2)
        Me.Controls.Add(Me.ItemPanel1)
        Me.DoubleBuffered = True
        Me.Name = "frmCierrePeriodo"
        Me.Text = "frmCierrePeriodo"
        Me.SuperTabControlPanel2.ResumeLayout(False)
        CType(Me.DateF_FechaFin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateF_FechaIni, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel1.ResumeLayout(False)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.ItemPanel1.ResumeLayout(False)
        Me.expandablePanel1.ResumeLayout(False)
        Me.ExpandablePanel3.ResumeLayout(False)
        CType(Me.GridP_SaldosN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExpandablePanel5.ResumeLayout(False)
        CType(Me.GridP_CierreDefinitivo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ItemPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SuperTabItem_Buscar As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents Label_periodoCerrar As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DateF_FechaFin As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents DateF_FechaIni As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_EjecutarPrecierre As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem_msg_Precierre As DevComponents.DotNetBar.LabelItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents Label_tabcierres As DevComponents.DotNetBar.LabelX
    Friend WithEvents ComboB_mes_TabCierres As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboB_ano_TabCierres As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_Buscarmes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_TabCierre_cerrarmes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_TabCierre_abrirmes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents SuperTabItem_detalle As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents LabelItem_pantalla As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ButtonItem_menuprincipal As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents BalloonTip_cierre As DevComponents.DotNetBar.BalloonTip
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents ButtonItem_cerrarpantalla As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ItemPanel1 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents expandablePanel1 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents CI_totalCompro As DevComponents.Editors.ComboItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ComboItem1 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem2 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem3 As DevComponents.Editors.ComboItem
    Private WithEvents ToolbarB_BuscarAsiento As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ComboItem4 As DevComponents.Editors.ComboItem
    Friend WithEvents ExpandablePanel3 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents GridP_SaldosN As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ExpandablePanel5 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents GridP_CierreDefinitivo As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ItemPanel2 As DevComponents.DotNetBar.ItemPanel
End Class
