﻿Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports DevComponents.DotNetBar

Public Class Frm_RPT_DiarioGeneral
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPT_DiarioGeneral"

    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load

        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty
        Dim sqlconexion As SqlConnection = Nothing 'conexión
        REM declaramos las tablas del dataset***DATAADAPTERS
        'Dim DAView_catalogoconta As SqlDataAdapter = Nothing
        'Dim DACataNivel1 As SqlDataAdapter = Nothing
        'Dim DACataNivel2 As SqlDataAdapter = Nothing
        'Dim DACataNivel3 As SqlDataAdapter = Nothing
        'Dim DACataNivel4 As SqlDataAdapter = Nothing
        'Dim DADasientoDG As SqlDataAdapter = Nothing
        'Dim sql_View_catalogoconta As String = String.Empty
        'Dim sql_CataNivel1 As String = String.Empty
        'Dim sql_CataNivel2 As String = String.Empty
        'Dim sql_CataNivel3 As String = String.Empty
        'Dim sql_CataNivel4 As String = String.Empty
        Dim dtCatalogoNivel1 As DataTable = Nothing
        Dim dtCatalogoNivel2 As DataTable = Nothing
        Dim dtCatalogoNivel3 As DataTable = Nothing
        Dim dtCatalogoNivel4 As DataTable = Nothing
        Dim dtAsientosDG As DataTable = Nothing
        Dim dtViewCatalogoConta As DataTable = Nothing

        'Dim dset_DasientoDG As New DS_DiarioGeneral
        Dim dset_DasientoDG As DataSet = Nothing
        Dim sql_DasientosDG As String = String.Empty

        Try
            dset_DasientoDG = New DataSet
            'sqlconexion = New SqlConnection(SIMF_CONTA.ConnectionStringconta)
            'dtCatalogoNivel1 = RNCuentaContable.ObtienerCatalogoNivel1()
            'dtCatalogoNivel1.TableName = "CataNivel1"
            'dset_DasientoDG.Tables.Add(dtCatalogoNivel1.Clone())

            'dtCatalogoNivel2 = RNCuentaContable.ObtienerCatalogoNivel2()
            'dtCatalogoNivel2.TableName = "CataNivel2"
            'dset_DasientoDG.Tables.Add(dtCatalogoNivel2.Clone())

            'dtCatalogoNivel3 = RNCuentaContable.ObtienerCatalogoNivel3()
            'dtCatalogoNivel3.TableName = "CataNivel3"
            'dset_DasientoDG.Tables.Add(dtCatalogoNivel3.Clone())

            'dtCatalogoNivel4 = RNCuentaContable.ObtienerCatalogoNivel4()
            'dtCatalogoNivel4.TableName = "CataNivel4"
            'dset_DasientoDG.Tables.Add(dtCatalogoNivel4.Clone())



            dtAsientosDG = RNCuentaContable.rptObtenerAsientoDiarioxFecha(SIMF_CONTA.formula(3), SIMF_CONTA.formula(4))
            dtAsientosDG.TableName = "Dasientos"
            dset_DasientoDG.Tables.Add(dtAsientosDG.Clone())

            'dtViewCatalogoConta = RNCuentaContable.ObtienerViewCatalogoConta()
            'dtViewCatalogoConta.TableName = "View_catalogoconta"
            'dset_DasientoDG.Tables.Add(dtViewCatalogoConta.Clone())

            'sql_View_catalogoconta = "select * from View_catalogoconta"
            'sql_CataNivel1 = "Select * From CataNivel1"
            'sql_CataNivel2 = "Select * From CataNivel2"
            'sql_CataNivel3 = "Select * From CataNivel3"
            'sql_CataNivel4 = "Select * From CataNivel4"
            'sql_DasientosDG = "select cdgocuenta,numeasiento,numedoc,isnull(fechaDoc,'') fechacrea,desclinea,debelocal,haberlocal from dasientos"
            'sql_DasientosDG += " where isnull(fechadoc,fechacrea) between " & SIMF_CONTA.
            'sql_DasientosDG += " group by cdgocuenta,anomes,numeasiento,numedoc,isnull(fechaDoc,''),desclinea,debelocal,haberlocal"

            'DACataNivel1 = New SqlDataAdapter(sql_CataNivel1, sqlconexion)
            'DACataNivel2 = New SqlDataAdapter(sql_CataNivel2, sqlconexion)
            'DACataNivel3 = New SqlDataAdapter(sql_CataNivel3, sqlconexion)
            'DACataNivel4 = New SqlDataAdapter(sql_CataNivel4, sqlconexion)
            'DAView_catalogoconta = New SqlDataAdapter(sql_View_catalogoconta, sqlconexion)
            'DADasientoDG = New SqlDataAdapter(sql_DasientosDG, sqlconexion)

            'DACataNivel1.Fill(dset_DasientoDG, "CataNivel1")
            'DACataNivel2.Fill(dset_DasientoDG, "CataNivel2")
            'DACataNivel3.Fill(dset_DasientoDG, "CataNivel3")
            'DACataNivel4.Fill(dset_DasientoDG, "CataNivel4")
            'DAView_catalogoconta.Fill(dset_DasientoDG, "View_catalogoconta")
            'DADasientoDG.Fill(dset_DasientoDG, "Dasientos_DG")

            Dim report_DiarioGeneral As New RPT_DiarioGeneral
            report_DiarioGeneral.SetDataSource(dset_DasientoDG)
            '' '' ''report_balanza_saldos.SetDatabaseLogon(SIMF_CONTA.usuario, SIMF_CONTA.pws, SIMF_CONTA.server, SIMF_CONTA.base)
            ' establecer la fórmula de selección de registros
            ' CrystalReportViewer1.ParameterFieldInfo = jackcrystal.lista_parametros
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_DiarioGeneral.SetParameterValue(nombreformula, valor)
                End If
            Next
            report_DiarioGeneral.RecordSelectionFormula = ""
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_DiarioGeneral
            Me.CrystalReportViewer1.Refresh()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("CrystalReportViewer1_Load - Error: " + ex.Message.ToString(), "Frm_RPT_DiarioGeneral", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

End Class