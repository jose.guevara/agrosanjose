Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmRptCtsCbrSlds
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ListBox12 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox11 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox10 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox9 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ListBox13 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox15 As System.Windows.Forms.ListBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem20 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem21 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem23 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem24 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem25 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem26 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem27 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem28 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem31 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem32 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem33 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem29 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem30 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem34 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem35 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem36 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem37 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem38 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem39 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem40 As System.Windows.Forms.MenuItem
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ListBox14 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox16 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox17 As System.Windows.Forms.ListBox
    Friend WithEvents MenuItem41 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem42 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem43 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem44 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem45 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem46 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem47 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem48 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem49 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem50 As System.Windows.Forms.MenuItem
    Friend WithEvents cmdReportes As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cbReportes As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ListBox19 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox20 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox21 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox22 As System.Windows.Forms.ListBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox23 As System.Windows.Forms.ListBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ListBox24 As System.Windows.Forms.ListBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents ListBox25 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox26 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox27 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox28 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox29 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox30 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox31 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox32 As System.Windows.Forms.ListBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents ListBox33 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox34 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox35 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox36 As System.Windows.Forms.ListBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ListBox18 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRptCtsCbrSlds))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ListBox18 = New System.Windows.Forms.ListBox
        Me.ListBox17 = New System.Windows.Forms.ListBox
        Me.ListBox16 = New System.Windows.Forms.ListBox
        Me.ListBox14 = New System.Windows.Forms.ListBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.ListBox15 = New System.Windows.Forms.ListBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.ListBox13 = New System.Windows.Forms.ListBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.ListBox12 = New System.Windows.Forms.ListBox
        Me.ListBox11 = New System.Windows.Forms.ListBox
        Me.ListBox10 = New System.Windows.Forms.ListBox
        Me.ListBox9 = New System.Windows.Forms.ListBox
        Me.ListBox8 = New System.Windows.Forms.ListBox
        Me.ListBox7 = New System.Windows.Forms.ListBox
        Me.ListBox6 = New System.Windows.Forms.ListBox
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.MenuItem19 = New System.Windows.Forms.MenuItem
        Me.MenuItem20 = New System.Windows.Forms.MenuItem
        Me.MenuItem22 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem21 = New System.Windows.Forms.MenuItem
        Me.MenuItem24 = New System.Windows.Forms.MenuItem
        Me.MenuItem25 = New System.Windows.Forms.MenuItem
        Me.MenuItem23 = New System.Windows.Forms.MenuItem
        Me.MenuItem26 = New System.Windows.Forms.MenuItem
        Me.MenuItem27 = New System.Windows.Forms.MenuItem
        Me.MenuItem41 = New System.Windows.Forms.MenuItem
        Me.MenuItem42 = New System.Windows.Forms.MenuItem
        Me.MenuItem44 = New System.Windows.Forms.MenuItem
        Me.MenuItem45 = New System.Windows.Forms.MenuItem
        Me.MenuItem43 = New System.Windows.Forms.MenuItem
        Me.MenuItem46 = New System.Windows.Forms.MenuItem
        Me.MenuItem47 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem28 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem31 = New System.Windows.Forms.MenuItem
        Me.MenuItem32 = New System.Windows.Forms.MenuItem
        Me.MenuItem33 = New System.Windows.Forms.MenuItem
        Me.MenuItem38 = New System.Windows.Forms.MenuItem
        Me.MenuItem29 = New System.Windows.Forms.MenuItem
        Me.MenuItem30 = New System.Windows.Forms.MenuItem
        Me.MenuItem34 = New System.Windows.Forms.MenuItem
        Me.MenuItem39 = New System.Windows.Forms.MenuItem
        Me.MenuItem35 = New System.Windows.Forms.MenuItem
        Me.MenuItem36 = New System.Windows.Forms.MenuItem
        Me.MenuItem37 = New System.Windows.Forms.MenuItem
        Me.MenuItem40 = New System.Windows.Forms.MenuItem
        Me.MenuItem48 = New System.Windows.Forms.MenuItem
        Me.MenuItem49 = New System.Windows.Forms.MenuItem
        Me.MenuItem50 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.cbReportes = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.ListBox19 = New System.Windows.Forms.ListBox
        Me.ListBox20 = New System.Windows.Forms.ListBox
        Me.ListBox21 = New System.Windows.Forms.ListBox
        Me.ListBox22 = New System.Windows.Forms.ListBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.ListBox23 = New System.Windows.Forms.ListBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.ListBox24 = New System.Windows.Forms.ListBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.ListBox25 = New System.Windows.Forms.ListBox
        Me.ListBox26 = New System.Windows.Forms.ListBox
        Me.ListBox27 = New System.Windows.Forms.ListBox
        Me.ListBox28 = New System.Windows.Forms.ListBox
        Me.ListBox29 = New System.Windows.Forms.ListBox
        Me.ListBox30 = New System.Windows.Forms.ListBox
        Me.ListBox31 = New System.Windows.Forms.ListBox
        Me.ListBox32 = New System.Windows.Forms.ListBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.ListBox33 = New System.Windows.Forms.ListBox
        Me.ListBox34 = New System.Windows.Forms.ListBox
        Me.ListBox35 = New System.Windows.Forms.ListBox
        Me.ListBox36 = New System.Windows.Forms.ListBox
        Me.cmdReportes = New DevComponents.DotNetBar.ButtonX
        Me.Label9 = New System.Windows.Forms.Label
        Me.cmbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ListBox18)
        Me.GroupBox1.Controls.Add(Me.ListBox17)
        Me.GroupBox1.Controls.Add(Me.ListBox16)
        Me.GroupBox1.Controls.Add(Me.ListBox14)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ListBox15)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.ListBox13)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ListBox12)
        Me.GroupBox1.Controls.Add(Me.ListBox11)
        Me.GroupBox1.Controls.Add(Me.ListBox10)
        Me.GroupBox1.Controls.Add(Me.ListBox9)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 114)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(744, 374)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'ListBox18
        '
        Me.ListBox18.Location = New System.Drawing.Point(632, 160)
        Me.ListBox18.Name = "ListBox18"
        Me.ListBox18.Size = New System.Drawing.Size(88, 17)
        Me.ListBox18.TabIndex = 45
        Me.ListBox18.Visible = False
        '
        'ListBox17
        '
        Me.ListBox17.Location = New System.Drawing.Point(504, 160)
        Me.ListBox17.Name = "ListBox17"
        Me.ListBox17.Size = New System.Drawing.Size(112, 17)
        Me.ListBox17.TabIndex = 44
        Me.ListBox17.Visible = False
        '
        'ListBox16
        '
        Me.ListBox16.HorizontalScrollbar = True
        Me.ListBox16.Location = New System.Drawing.Point(632, 32)
        Me.ListBox16.Name = "ListBox16"
        Me.ListBox16.Size = New System.Drawing.Size(104, 121)
        Me.ListBox16.TabIndex = 43
        '
        'ListBox14
        '
        Me.ListBox14.HorizontalScrollbar = True
        Me.ListBox14.Location = New System.Drawing.Point(504, 32)
        Me.ListBox14.Name = "ListBox14"
        Me.ListBox14.Size = New System.Drawing.Size(128, 121)
        Me.ListBox14.TabIndex = 42
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(504, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(232, 16)
        Me.Label8.TabIndex = 41
        Me.Label8.Text = "Registro de los Municipios"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(256, 224)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(120, 16)
        Me.Label13.TabIndex = 40
        Me.Label13.Text = "<F5> AYUDA"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(256, 200)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(128, 20)
        Me.TextBox1.TabIndex = 39
        '
        'ListBox15
        '
        Me.ListBox15.Location = New System.Drawing.Point(384, 352)
        Me.ListBox15.Name = "ListBox15"
        Me.ListBox15.Size = New System.Drawing.Size(64, 17)
        Me.ListBox15.TabIndex = 38
        Me.ListBox15.Visible = False
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(256, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 16)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Registro de los Clientes"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox13
        '
        Me.ListBox13.HorizontalScrollbar = True
        Me.ListBox13.Location = New System.Drawing.Point(384, 200)
        Me.ListBox13.Name = "ListBox13"
        Me.ListBox13.Size = New System.Drawing.Size(104, 147)
        Me.ListBox13.TabIndex = 35
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 178)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(224, 16)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Registro de los Vendedores"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox12
        '
        Me.ListBox12.Location = New System.Drawing.Point(136, 352)
        Me.ListBox12.Name = "ListBox12"
        Me.ListBox12.Size = New System.Drawing.Size(88, 17)
        Me.ListBox12.TabIndex = 32
        Me.ListBox12.Visible = False
        '
        'ListBox11
        '
        Me.ListBox11.Location = New System.Drawing.Point(8, 352)
        Me.ListBox11.Name = "ListBox11"
        Me.ListBox11.Size = New System.Drawing.Size(120, 17)
        Me.ListBox11.TabIndex = 31
        Me.ListBox11.Visible = False
        '
        'ListBox10
        '
        Me.ListBox10.HorizontalScrollbar = True
        Me.ListBox10.Location = New System.Drawing.Point(136, 200)
        Me.ListBox10.Name = "ListBox10"
        Me.ListBox10.Size = New System.Drawing.Size(104, 147)
        Me.ListBox10.TabIndex = 30
        '
        'ListBox9
        '
        Me.ListBox9.HorizontalScrollbar = True
        Me.ListBox9.Location = New System.Drawing.Point(8, 200)
        Me.ListBox9.Name = "ListBox9"
        Me.ListBox9.Size = New System.Drawing.Size(128, 147)
        Me.ListBox9.TabIndex = 29
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(384, 160)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(80, 17)
        Me.ListBox8.TabIndex = 28
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(256, 160)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(112, 17)
        Me.ListBox7.TabIndex = 27
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(384, 32)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(104, 121)
        Me.ListBox6.TabIndex = 26
        '
        'ListBox5
        '
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(256, 32)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(128, 121)
        Me.ListBox5.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(256, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(224, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Registro de los Departamentos"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(264, 16)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Registro de los Negocios"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(136, 160)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(80, 17)
        Me.ListBox4.TabIndex = 22
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(8, 160)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(112, 17)
        Me.ListBox3.TabIndex = 21
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(136, 32)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(104, 121)
        Me.ListBox2.TabIndex = 20
        '
        'ListBox1
        '
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(8, 32)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(128, 121)
        Me.ListBox1.TabIndex = 19
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem15})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem19, Me.MenuItem20, Me.MenuItem22})
        Me.MenuItem1.Text = "Exportar"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Excel"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "Html"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Pdf"
        '
        'MenuItem19
        '
        Me.MenuItem19.Index = 3
        Me.MenuItem19.Text = "Rtf"
        '
        'MenuItem20
        '
        Me.MenuItem20.Index = 4
        Me.MenuItem20.Text = "Tiff"
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 5
        Me.MenuItem22.Text = "Ninguno"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem14, Me.MenuItem41, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12, Me.MenuItem8, Me.MenuItem9, Me.MenuItem10, Me.MenuItem13, Me.MenuItem31, Me.MenuItem29, Me.MenuItem35, Me.MenuItem48, Me.MenuItem49, Me.MenuItem50})
        Me.MenuItem2.Text = "Reportes"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 0
        Me.MenuItem14.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem21, Me.MenuItem23})
        Me.MenuItem14.Text = "Estado de Cuentas"
        '
        'MenuItem21
        '
        Me.MenuItem21.Index = 0
        Me.MenuItem21.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem24, Me.MenuItem25})
        Me.MenuItem21.Text = "COR"
        '
        'MenuItem24
        '
        Me.MenuItem24.Index = 0
        Me.MenuItem24.Text = "Sin Deslizamiento"
        '
        'MenuItem25
        '
        Me.MenuItem25.Index = 1
        Me.MenuItem25.Text = "Con Deslizamiento"
        '
        'MenuItem23
        '
        Me.MenuItem23.Index = 1
        Me.MenuItem23.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem26, Me.MenuItem27})
        Me.MenuItem23.Text = "USD"
        '
        'MenuItem26
        '
        Me.MenuItem26.Index = 0
        Me.MenuItem26.Text = "Sin Intereses"
        '
        'MenuItem27
        '
        Me.MenuItem27.Index = 1
        Me.MenuItem27.Text = "Con Intereses"
        '
        'MenuItem41
        '
        Me.MenuItem41.Index = 1
        Me.MenuItem41.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem42, Me.MenuItem43})
        Me.MenuItem41.Text = "Cobro Cliente"
        '
        'MenuItem42
        '
        Me.MenuItem42.Index = 0
        Me.MenuItem42.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem44, Me.MenuItem45})
        Me.MenuItem42.Text = "COR"
        '
        'MenuItem44
        '
        Me.MenuItem44.Index = 0
        Me.MenuItem44.Text = "Cobro Sin Deslizamiento"
        '
        'MenuItem45
        '
        Me.MenuItem45.Index = 1
        Me.MenuItem45.Text = "Cobro Con Deslizamiento"
        '
        'MenuItem43
        '
        Me.MenuItem43.Index = 1
        Me.MenuItem43.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem46, Me.MenuItem47})
        Me.MenuItem43.Text = "USD"
        '
        'MenuItem46
        '
        Me.MenuItem46.Index = 0
        Me.MenuItem46.Text = "Sin Deslizamiento"
        '
        'MenuItem47
        '
        Me.MenuItem47.Index = 1
        Me.MenuItem47.Text = "Con Deslizamiento"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem28})
        Me.MenuItem4.Text = "An�lisis de Antiguedad"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Saldos Generales Detallados"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Detallada del Vendedor"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Saldos Resumidos por Vendedor"
        '
        'MenuItem28
        '
        Me.MenuItem28.Index = 3
        Me.MenuItem28.Text = "Resumen General del Vendedor"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.Text = "Vendedores y sus Clientes Asignados"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Clientes y los Departamentos Asignados"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 5
        Me.MenuItem8.Text = "Facturas Pendientes de los Clientes"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 6
        Me.MenuItem9.Text = "Saldos Pendientes de los Clientes"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 7
        Me.MenuItem10.Text = "Movimiento Hist�rico de un Cliente"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 8
        Me.MenuItem13.Text = "Movimiento Intereses y Mant. de Valor"
        '
        'MenuItem31
        '
        Me.MenuItem31.Index = 9
        Me.MenuItem31.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem32, Me.MenuItem33, Me.MenuItem38})
        Me.MenuItem31.Text = "Recibos de Caja"
        '
        'MenuItem32
        '
        Me.MenuItem32.Index = 0
        Me.MenuItem32.Text = "Por Fecha"
        '
        'MenuItem33
        '
        Me.MenuItem33.Index = 1
        Me.MenuItem33.Text = "Por Vendedor"
        '
        'MenuItem38
        '
        Me.MenuItem38.Index = 2
        Me.MenuItem38.Text = "Por Cliente"
        '
        'MenuItem29
        '
        Me.MenuItem29.Index = 10
        Me.MenuItem29.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem30, Me.MenuItem34, Me.MenuItem39})
        Me.MenuItem29.Text = "Notas de Cr�dito"
        '
        'MenuItem30
        '
        Me.MenuItem30.Index = 0
        Me.MenuItem30.Text = "Por Fecha"
        '
        'MenuItem34
        '
        Me.MenuItem34.Index = 1
        Me.MenuItem34.Text = "Por Vendedor"
        '
        'MenuItem39
        '
        Me.MenuItem39.Index = 2
        Me.MenuItem39.Text = "Por Cliente"
        '
        'MenuItem35
        '
        Me.MenuItem35.Index = 11
        Me.MenuItem35.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem36, Me.MenuItem37, Me.MenuItem40})
        Me.MenuItem35.Text = "Notas de D�bito"
        '
        'MenuItem36
        '
        Me.MenuItem36.Index = 0
        Me.MenuItem36.Text = "Por Fecha"
        '
        'MenuItem37
        '
        Me.MenuItem37.Index = 1
        Me.MenuItem37.Text = "Por Vendedor"
        '
        'MenuItem40
        '
        Me.MenuItem40.Index = 2
        Me.MenuItem40.Text = "Por Cliente"
        '
        'MenuItem48
        '
        Me.MenuItem48.Index = 12
        Me.MenuItem48.Text = "Facturas Pendientes de mas de un a�o"
        '
        'MenuItem49
        '
        Me.MenuItem49.Index = 13
        Me.MenuItem49.Text = "Facturas Pendientes de mas de 4 meses"
        '
        'MenuItem50
        '
        Me.MenuItem50.Index = 14
        Me.MenuItem50.Text = "Recibos Anulados"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 3
        Me.MenuItem15.Text = "Salir"
        '
        'Timer1
        '
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 484)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(568, 57)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(448, 26)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(112, 20)
        Me.TextBox2.TabIndex = 50
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(384, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 13)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "RecProvis"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(256, 24)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 48
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(72, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(200, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "FecFinal"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "FecInicial"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(4, 73)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 39
        Me.Label18.Text = "Reportes"
        '
        'cbReportes
        '
        Me.cbReportes.DisplayMember = "Text"
        Me.cbReportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReportes.FormattingEnabled = True
        Me.cbReportes.ItemHeight = 15
        Me.cbReportes.Location = New System.Drawing.Point(68, 70)
        Me.cbReportes.Name = "cbReportes"
        Me.cbReportes.Size = New System.Drawing.Size(324, 21)
        Me.cbReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbReportes.TabIndex = 38
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.cmdExportar)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(744, 55)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 37
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(75, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmdExportar
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.FontBold = True
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.FontBold = True
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(448, 26)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(112, 20)
        Me.TextBox3.TabIndex = 50
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(384, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 13)
        Me.Label10.TabIndex = 49
        Me.Label10.Text = "RecProvis"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker3.Location = New System.Drawing.Point(256, 24)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker3.TabIndex = 48
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker4.Location = New System.Drawing.Point(72, 24)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker4.TabIndex = 47
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(200, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 13)
        Me.Label11.TabIndex = 46
        Me.Label11.Text = "FecFinal"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(8, 24)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(62, 13)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "FecInicial"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ListBox19
        '
        Me.ListBox19.Location = New System.Drawing.Point(632, 160)
        Me.ListBox19.Name = "ListBox19"
        Me.ListBox19.Size = New System.Drawing.Size(88, 17)
        Me.ListBox19.TabIndex = 45
        Me.ListBox19.Visible = False
        '
        'ListBox20
        '
        Me.ListBox20.Location = New System.Drawing.Point(504, 160)
        Me.ListBox20.Name = "ListBox20"
        Me.ListBox20.Size = New System.Drawing.Size(112, 17)
        Me.ListBox20.TabIndex = 44
        Me.ListBox20.Visible = False
        '
        'ListBox21
        '
        Me.ListBox21.HorizontalScrollbar = True
        Me.ListBox21.Location = New System.Drawing.Point(632, 32)
        Me.ListBox21.Name = "ListBox21"
        Me.ListBox21.Size = New System.Drawing.Size(104, 121)
        Me.ListBox21.TabIndex = 43
        '
        'ListBox22
        '
        Me.ListBox22.HorizontalScrollbar = True
        Me.ListBox22.Location = New System.Drawing.Point(504, 32)
        Me.ListBox22.Name = "ListBox22"
        Me.ListBox22.Size = New System.Drawing.Size(128, 121)
        Me.ListBox22.TabIndex = 42
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(504, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(232, 16)
        Me.Label14.TabIndex = 41
        Me.Label14.Text = "Registro de los Municipios"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(256, 224)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 16)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "<F5> AYUDA"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(256, 200)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(128, 20)
        Me.TextBox4.TabIndex = 39
        '
        'ListBox23
        '
        Me.ListBox23.Location = New System.Drawing.Point(384, 352)
        Me.ListBox23.Name = "ListBox23"
        Me.ListBox23.Size = New System.Drawing.Size(64, 17)
        Me.ListBox23.TabIndex = 38
        Me.ListBox23.Visible = False
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(256, 184)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(232, 16)
        Me.Label16.TabIndex = 36
        Me.Label16.Text = "Registro de los Clientes"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox24
        '
        Me.ListBox24.HorizontalScrollbar = True
        Me.ListBox24.Location = New System.Drawing.Point(384, 200)
        Me.ListBox24.Name = "ListBox24"
        Me.ListBox24.Size = New System.Drawing.Size(104, 147)
        Me.ListBox24.TabIndex = 35
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(16, 178)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(224, 16)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "Registro de los Vendedores"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox25
        '
        Me.ListBox25.Location = New System.Drawing.Point(136, 352)
        Me.ListBox25.Name = "ListBox25"
        Me.ListBox25.Size = New System.Drawing.Size(88, 17)
        Me.ListBox25.TabIndex = 32
        Me.ListBox25.Visible = False
        '
        'ListBox26
        '
        Me.ListBox26.Location = New System.Drawing.Point(8, 352)
        Me.ListBox26.Name = "ListBox26"
        Me.ListBox26.Size = New System.Drawing.Size(120, 17)
        Me.ListBox26.TabIndex = 31
        Me.ListBox26.Visible = False
        '
        'ListBox27
        '
        Me.ListBox27.HorizontalScrollbar = True
        Me.ListBox27.Location = New System.Drawing.Point(136, 200)
        Me.ListBox27.Name = "ListBox27"
        Me.ListBox27.Size = New System.Drawing.Size(104, 147)
        Me.ListBox27.TabIndex = 30
        '
        'ListBox28
        '
        Me.ListBox28.HorizontalScrollbar = True
        Me.ListBox28.Location = New System.Drawing.Point(8, 200)
        Me.ListBox28.Name = "ListBox28"
        Me.ListBox28.Size = New System.Drawing.Size(128, 147)
        Me.ListBox28.TabIndex = 29
        '
        'ListBox29
        '
        Me.ListBox29.Location = New System.Drawing.Point(384, 160)
        Me.ListBox29.Name = "ListBox29"
        Me.ListBox29.Size = New System.Drawing.Size(80, 17)
        Me.ListBox29.TabIndex = 28
        Me.ListBox29.Visible = False
        '
        'ListBox30
        '
        Me.ListBox30.Location = New System.Drawing.Point(256, 160)
        Me.ListBox30.Name = "ListBox30"
        Me.ListBox30.Size = New System.Drawing.Size(112, 17)
        Me.ListBox30.TabIndex = 27
        Me.ListBox30.Visible = False
        '
        'ListBox31
        '
        Me.ListBox31.HorizontalScrollbar = True
        Me.ListBox31.Location = New System.Drawing.Point(384, 32)
        Me.ListBox31.Name = "ListBox31"
        Me.ListBox31.Size = New System.Drawing.Size(104, 121)
        Me.ListBox31.TabIndex = 26
        '
        'ListBox32
        '
        Me.ListBox32.HorizontalScrollbar = True
        Me.ListBox32.Location = New System.Drawing.Point(256, 32)
        Me.ListBox32.Name = "ListBox32"
        Me.ListBox32.Size = New System.Drawing.Size(128, 121)
        Me.ListBox32.TabIndex = 25
        '
        'Label19
        '
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(256, 16)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(224, 16)
        Me.Label19.TabIndex = 24
        Me.Label19.Text = "Registro de los Departamentos"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label20
        '
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(16, 16)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(264, 16)
        Me.Label20.TabIndex = 23
        Me.Label20.Text = "Registro de los Negocios"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox33
        '
        Me.ListBox33.Location = New System.Drawing.Point(136, 160)
        Me.ListBox33.Name = "ListBox33"
        Me.ListBox33.Size = New System.Drawing.Size(80, 17)
        Me.ListBox33.TabIndex = 22
        Me.ListBox33.Visible = False
        '
        'ListBox34
        '
        Me.ListBox34.Location = New System.Drawing.Point(8, 160)
        Me.ListBox34.Name = "ListBox34"
        Me.ListBox34.Size = New System.Drawing.Size(112, 17)
        Me.ListBox34.TabIndex = 21
        Me.ListBox34.Visible = False
        '
        'ListBox35
        '
        Me.ListBox35.HorizontalScrollbar = True
        Me.ListBox35.Location = New System.Drawing.Point(136, 32)
        Me.ListBox35.Name = "ListBox35"
        Me.ListBox35.Size = New System.Drawing.Size(104, 121)
        Me.ListBox35.TabIndex = 20
        '
        'ListBox36
        '
        Me.ListBox36.HorizontalScrollbar = True
        Me.ListBox36.Location = New System.Drawing.Point(8, 32)
        Me.ListBox36.Name = "ListBox36"
        Me.ListBox36.Size = New System.Drawing.Size(128, 121)
        Me.ListBox36.TabIndex = 19
        '
        'cmdReportes
        '
        Me.cmdReportes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdReportes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdReportes.Image = CType(resources.GetObject("cmdReportes.Image"), System.Drawing.Image)
        Me.cmdReportes.ImageFixedSize = New System.Drawing.Size(30, 30)
        Me.cmdReportes.Location = New System.Drawing.Point(395, 62)
        Me.cmdReportes.Name = "cmdReportes"
        Me.cmdReportes.Size = New System.Drawing.Size(55, 36)
        Me.cmdReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdReportes.TabIndex = 40
        Me.cmdReportes.Tooltip = "<b><font color=""#17365D"">Generar Reporte</font></b>"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(4, 98)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 13)
        Me.Label9.TabIndex = 54
        Me.Label9.Text = "Agencias"
        Me.Label9.Visible = False
        '
        'cmbAgencias
        '
        Me.cmbAgencias.DisplayMember = "Text"
        Me.cmbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbAgencias.FormattingEnabled = True
        Me.cmbAgencias.ItemHeight = 14
        Me.cmbAgencias.Location = New System.Drawing.Point(68, 94)
        Me.cmbAgencias.Name = "cmbAgencias"
        Me.cmbAgencias.Size = New System.Drawing.Size(324, 20)
        Me.cmbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbAgencias.TabIndex = 53
        Me.cmbAgencias.Visible = False
        '
        'frmRptCtsCbrSlds
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(744, 535)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cmbAgencias)
        Me.Controls.Add(Me.cmdReportes)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cbReportes)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRptCtsCbrSlds"
        Me.Text = "Reporte de Cuentas por Cobrar"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmRptCtsCbrSlds"

    Private Sub frmRptCtsCbrSlds_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try

            blnClear = False
            Iniciar()
            Limpiar()
            RNReportes.CargarComboReportes(cbReportes, nIdUsuario, nIdModulo)
            UbicarAgencia(lngRegUsuario)
            LlenarComboSucursales()
            'If intAccesos(105) = 0 Then
            '    MenuItem1.Visible = False
            'End If
            'If intAccesos(106) = 0 Then
            '    MenuItem14.Visible = False
            'End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub LlenarComboSucursales()
        RNAgencias.CargarComboAgencias(cmbAgencias, lngRegUsuario)
        'cmbAgencias.ValueMember = lngRegAgencia
    End Sub

    Private Sub frmRptCtsCbrSlds_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try


            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F5 Then
                If Label13.Visible = True Then
                    intListadoAyuda = 2
                    Dim frmNew As New frmListadoAyuda
                    Timer1.Interval = 200
                    Timer1.Enabled = True
                    frmNew.ShowDialog()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub Iniciar()
        Try

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Negocios Where Estado <> 2 order by descripcion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox1.Items.Add(dtrAgro2K.GetValue(1))
                ListBox3.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Departamentos Where Estado <> 2 order by descripcion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox5.Items.Add(dtrAgro2K.GetValue(1))
                ListBox7.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = ""
            strQuery = "Select Registro, Nombre From prm_Vendedores Where Estado <> 2 order by Nombre"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox9.Items.Add(dtrAgro2K.GetValue(1))
                ListBox11.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'cmdAgro2K.Connection.Close()
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub Limpiar()
        Try


            ListBox2.Items.Clear()
            ListBox4.Items.Clear()
            ListBox6.Items.Clear()
            ListBox8.Items.Clear()
            ListBox10.Items.Clear()
            ListBox12.Items.Clear()
            ListBox13.Items.Clear()
            ListBox15.Items.Clear()
            ListBox14.Items.Clear()
            ListBox16.Items.Clear()
            ListBox17.Items.Clear()
            ListBox18.Items.Clear()
            intRptExportar = 0
            Label13.Visible = False
            DateTimePicker1.Value = DateSerial(Year(Now), Month(Now), 1)
            DateTimePicker2.Value = Now
            TextBox2.Text = ""
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox9.DoubleClick, ListBox10.DoubleClick, ListBox13.DoubleClick, ListBox14.DoubleClick, ListBox16.DoubleClick
        Try
            Select Case sender.name.ToString
                Case "ListBox1"
                    If ListBox1.SelectedItem IsNot Nothing Then
                        ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
                    End If

                Case "ListBox2"
                    If ListBox2.SelectedIndex >= 0 Then
                        ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                    End If
                Case "ListBox5"
                    If ListBox5.SelectedItem IsNot Nothing Then
                        ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem) : lngRegistro = CInt(ListBox7.SelectedItem) : UbicarMunicipios()
                    End If

                Case "ListBox6"
                    If ListBox6.SelectedIndex >= 0 Then
                        ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                    End If
                Case "ListBox9"
                    If ListBox9.SelectedItem IsNot Nothing Then
                        ListBox10.Items.Add(ListBox9.SelectedItem) : ListBox11.SelectedIndex = ListBox9.SelectedIndex : ListBox12.Items.Add(ListBox11.SelectedItem)
                    End If

                Case "ListBox10"
                    If ListBox10.SelectedIndex >= 0 Then
                        ListBox12.Items.RemoveAt(ListBox10.SelectedIndex) : ListBox10.Items.RemoveAt(ListBox10.SelectedIndex)
                    End If
                Case "ListBox13"
                    If ListBox13.SelectedIndex >= 0 Then
                        ListBox15.Items.RemoveAt(ListBox13.SelectedIndex) : ListBox13.Items.RemoveAt(ListBox13.SelectedIndex)
                    End If
                Case "ListBox14"
                    If ListBox14.SelectedItem IsNot Nothing Then
                        ListBox16.Items.Add(ListBox14.SelectedItem) : ListBox17.SelectedIndex = ListBox14.SelectedIndex : ListBox18.Items.Add(ListBox17.SelectedItem)
                    End If

                Case "ListBox16"
                    If ListBox16.SelectedIndex >= 0 Then
                        ListBox18.Items.RemoveAt(ListBox16.SelectedIndex) : ListBox16.Items.RemoveAt(ListBox16.SelectedIndex)
                    End If
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        Try


            If e.KeyCode = Keys.Enter Then
                If UbicarCliente() = False Then
                    MsgBox("C�digo de cliente no est� registrado.   Verifique.", MsgBoxStyle.Critical, "Extracci�n de Registro")
                End If
                TextBox1.Text = ""
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus

        Label13.Visible = True

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus

        Label13.Visible = False

    End Sub

    Function UbicarCliente() As Boolean
        Try
            lngRegistro = 0
            strQuery = ""
            strQuery = "Select C.Registro, C.Codigo, C.Nombre from prm_Clientes C Where C.Codigo = '" & TextBox1.Text & "'"
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            SUFunciones.CierraComando(cmdAgro2K)
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                lngRegistro = dtrAgro2K.GetValue(0)
                ListBox13.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
                ListBox15.Items.Add(lngRegistro)
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarCliente = False
                Exit Function
            End If
            UbicarCliente = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Function

    Sub UbicarMunicipios()

        Try
            If ListBox14.Items.Count > 0 Then
                ListBox14.Items.Clear()
            End If
            If ListBox17.Items.Count > 0 Then
                ListBox17.Items.Clear()
            End If

            strQuery = ""
            strQuery = "Select registro, descripcion from prm_Municipios Where registro2 = " & lngRegistro & " "
            strQuery = strQuery + "order by descripcion"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)

            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox14.Items.Add(dtrAgro2K.GetValue(1))
                ListBox17.Items.Add(dtrAgro2K.GetValue(0))
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub GenerarQuery()
        Try


            strQueryParte01 = ""
            strQueryParte02 = ""
            strQueryParte03 = ""
            strQueryParte04 = ""
            strQueryParte05 = ""
            If ListBox15.Items.Count > 0 Then                                       'Clientes
                strQueryParte01 = strQueryParte01 + "'and C.Registro in ("
                If ListBox15.Items.Count = 1 Then
                    ListBox15.SelectedIndex = 0
                    strQueryParte01 = strQueryParte01 + "" & ListBox15.SelectedItem & ") ', "
                ElseIf ListBox15.Items.Count > 1 Then
                    For intIncr = 0 To ListBox15.Items.Count - 2
                        ListBox15.SelectedIndex = intIncr
                        strQueryParte01 = strQueryParte01 + "" & ListBox15.SelectedItem & ", "
                    Next
                    ListBox15.SelectedIndex = ListBox15.Items.Count - 1
                    strQueryParte01 = strQueryParte01 + "" & ListBox15.SelectedItem & ") ', "
                End If
            Else
                strQueryParte01 = strQueryParte01 + "' ', "
            End If
            If ListBox12.Items.Count > 0 Then                                        'Vendedores
                strQueryParte02 = strQueryParte02 + "'and C.VendRegistro in ("
                If ListBox12.Items.Count = 1 Then
                    ListBox12.SelectedIndex = 0
                    strQueryParte02 = strQueryParte02 + "" & ListBox12.SelectedItem & ") ', "
                ElseIf ListBox12.Items.Count > 1 Then
                    For intIncr = 0 To ListBox12.Items.Count - 2
                        ListBox12.SelectedIndex = intIncr
                        strQueryParte02 = strQueryParte02 + "" & ListBox12.SelectedItem & ", "
                    Next
                    ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                    strQueryParte02 = strQueryParte02 + "" & ListBox12.SelectedItem & ") ', "
                End If
            Else
                strQueryParte02 = strQueryParte02 + "' ', "
            End If
            If ListBox4.Items.Count > 0 Then                                        'Negocios
                strQueryParte03 = strQueryParte03 + "'and C.NegRegistro in ("
                If ListBox4.Items.Count = 1 Then
                    ListBox4.SelectedIndex = 0
                    strQueryParte03 = strQueryParte03 + "" & ListBox4.SelectedItem & ") ', "
                ElseIf ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 2
                        ListBox4.SelectedIndex = intIncr
                        strQueryParte03 = strQueryParte03 + "" & ListBox4.SelectedItem & ", "
                    Next
                    ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                    strQueryParte03 = strQueryParte03 + "" & ListBox4.SelectedItem & ") ', "
                End If
            Else
                strQueryParte03 = strQueryParte03 + "' ', "
            End If
            If ListBox8.Items.Count > 0 Then                                        'Departamentos
                strQueryParte04 = strQueryParte04 + "'and C.DeptRegistro in ("
                If ListBox8.Items.Count = 1 Then
                    ListBox8.SelectedIndex = 0
                    strQueryParte04 = strQueryParte04 + "" & ListBox8.SelectedItem & ") ', 0, 0, '', '', "
                ElseIf ListBox8.Items.Count > 1 Then
                    For intIncr = 0 To ListBox8.Items.Count - 2
                        ListBox8.SelectedIndex = intIncr
                        strQueryParte04 = strQueryParte04 + "" & ListBox8.SelectedItem & ", "
                    Next
                    ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                    strQueryParte04 = strQueryParte04 + "" & ListBox8.SelectedItem & ") ', 0, 0, '', '', "
                End If
            Else
                strQueryParte04 = strQueryParte04 + "' ', 0, 0, '', '', "
            End If
            strQueryParte04 = strQueryParte04 + " '', "
            If ListBox18.Items.Count > 0 Then                                        'Municipios
                strQueryParte05 = strQueryParte05 + "'and C.MunRegistro in ("
                If ListBox18.Items.Count = 1 Then
                    ListBox18.SelectedIndex = 0
                    strQueryParte05 = strQueryParte05 + "" & ListBox18.SelectedItem & ") '"
                ElseIf ListBox18.Items.Count > 1 Then
                    For intIncr = 0 To ListBox18.Items.Count - 2
                        ListBox18.SelectedIndex = intIncr
                        strQueryParte05 = strQueryParte05 + "" & ListBox18.SelectedItem & ", "
                    Next
                    ListBox18.SelectedIndex = ListBox18.Items.Count - 1
                    strQueryParte05 = strQueryParte05 + "" & ListBox18.SelectedItem & ") ' "
                End If
            Else
                strQueryParte05 = strQueryParte05 + "' ' "
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub GenerarQuery2()
        Try


            If ListBox15.Items.Count > 0 Then                                       'Clientes
                strQuery = strQuery + "'and C.Registro in ("
                If ListBox15.Items.Count = 1 Then
                    ListBox15.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox15.SelectedItem & ") ', "
                ElseIf ListBox15.Items.Count > 1 Then
                    For intIncr = 0 To ListBox15.Items.Count - 2
                        ListBox15.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox15.SelectedItem & ", "
                    Next
                    ListBox15.SelectedIndex = ListBox15.Items.Count - 1
                    strQuery = strQuery + "" & ListBox15.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox12.Items.Count > 0 Then                                        'Vendedores
                strQuery = strQuery + "'and C.VendRegistro in ("
                If ListBox12.Items.Count = 1 Then
                    ListBox12.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                ElseIf ListBox12.Items.Count > 1 Then
                    For intIncr = 0 To ListBox12.Items.Count - 2
                        ListBox12.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox12.SelectedItem & ", "
                    Next
                    ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox4.Items.Count > 0 Then                                        'Negocios
                strQuery = strQuery + "'and C.NegRegistro in ("
                If ListBox4.Items.Count = 1 Then
                    ListBox4.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                ElseIf ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 2
                        ListBox4.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                    Next
                    ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox8.Items.Count > 0 Then                                        'Departamentos
                strQuery = strQuery + "'and C.DeptRegistro in ("
                If ListBox8.Items.Count = 1 Then
                    ListBox8.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', " & intRptCtasCbr & ", 0, "
                ElseIf ListBox6.Items.Count > 1 Then
                    For intIncr = 0 To ListBox8.Items.Count - 2
                        ListBox8.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox8.SelectedItem & ", " & intRptCtasCbr & ", 0, "
                    Next
                    ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', " & intRptCtasCbr & ", 0, "
                End If
            Else
                strQuery = strQuery + "' ', " & intRptCtasCbr & ", 0, "
            End If
            strQuery = strQuery + "'" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "', "
            strQuery = strQuery + "'" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "', "
            strQuery = strQuery + "'" & TextBox2.Text & "', "
            If ListBox18.Items.Count > 0 Then                                        'Municipios
                strQuery = strQuery + "'and C.MunRegistro in ("
                If ListBox18.Items.Count = 1 Then
                    ListBox18.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ") ' "
                ElseIf ListBox18.Items.Count > 1 Then
                    For intIncr = 0 To ListBox18.Items.Count - 2
                        ListBox18.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox18.SelectedItem & ", "
                    Next
                    ListBox18.SelectedIndex = ListBox18.Items.Count - 1
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ") ' "
                End If
            Else
                strQuery = strQuery + "' ' "
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem15.Click, MenuItem16.Click, MenuItem17.Click, MenuItem18.Click, MenuItem19.Click, MenuItem20.Click, MenuItem22.Click, MenuItem24.Click, MenuItem25.Click, MenuItem26.Click, MenuItem27.Click, MenuItem28.Click

        Dim frmNew As New frmEscogerClientes

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            MenuItem16.Checked = False
            MenuItem17.Checked = False
            MenuItem18.Checked = False
            MenuItem19.Checked = False
            MenuItem20.Checked = False
            MenuItem22.Checked = False
            Select Case sender.text.ToString
                Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
                Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
                Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
                Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
                Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
                Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
                Case "Sin Deslizamiento" : intRptCtasCbr = 1
                Case "Con Deslizamiento" : intRptCtasCbr = 2
                Case "Sin Intereses" : intRptCtasCbr = 3
                Case "Con Intereses" : intRptCtasCbr = 4
                Case "Saldos Generales Detallados" : intRptCtasCbr = 7
                Case "Detallada del Vendedor" : intRptCtasCbr = 8
                Case "Saldos Resumidos por Vendedor" : intRptCtasCbr = 9
                Case "Vendedores y sus Clientes Asignados" : intRptCtasCbr = 10
                Case "Clientes y los Departamentos Asignados" : intRptCtasCbr = 11
                Case "Resumen General del Vendedor" : intRptCtasCbr = 12
                Case "Facturas Pendientes de los Clientes" : intRptCtasCbr = 13
                Case "Saldos Pendientes de los Clientes" : intRptCtasCbr = 14
                Case "Movimiento Hist�rico de un Cliente" : intRptCtasCbr = 15
                Case "Movimiento Intereses y Mant. de Valor" : intRptCtasCbr = 16
                Case "Limpiar" : Limpiar()
                Case "Salir" : Me.Close()
            End Select
            If intRptCtasCbr > 0 And intRptCtasCbr < 15 Then
                strQueryParte00 = ""
                strQueryParte00 = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
                GenerarQuery()
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
            ElseIf intRptCtasCbr = 15 Or intRptCtasCbr = 16 Then
                If ListBox15.Items.Count = 0 Then
                    MsgBox("No digito el cliente al quien se requiere generar su historico de movimientos.", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
                strQuery = ""
                strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", ' ', ' ', ' ', ' ', 0, "
                ListBox15.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox15.SelectedItem & ", '" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "', "
                strQuery = strQuery + "'" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "', '" & TextBox2.Text & "', ''"
                Dim frmNew2 As New actrptViewer
                frmNew2.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew2.Show()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            If strUbicar <> "" Then
                Timer1.Enabled = False
                TextBox1.Text = strUbicar : UbicarCliente() : ListBox13.Focus() : TextBox1.Text = ""
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub MenuItem32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem32.Click, MenuItem33.Click, MenuItem38.Click

        Dim frmNew As New actrptViewer

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        If sender.text = "Por Fecha" Then
            intRptCtasCbr = 17
        ElseIf sender.text = "Por Vendedor" Then
            intRptCtasCbr = 18
        ElseIf sender.text = "Por Cliente" Then
            intRptCtasCbr = 19
        End If

        strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
        GenerarQuery2()
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub MenuItem30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem30.Click, MenuItem34.Click, MenuItem39.Click

        Dim frmNew As New actrptViewer

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        If sender.text = "Por Fecha" Then
            intRptCtasCbr = 20
        ElseIf sender.text = "Por Vendedor" Then
            intRptCtasCbr = 21
        ElseIf sender.text = "Por Cliente" Then
            intRptCtasCbr = 22
        End If

        strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
        GenerarQuery2()
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub MenuItem36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem36.Click, MenuItem37.Click, MenuItem40.Click

        Dim frmNew As New actrptViewer

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptTipoPrecio = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        If sender.text = "Por Fecha" Then
            intRptCtasCbr = 23
        ElseIf sender.text = "Por Vendedor" Then
            intRptCtasCbr = 24
        ElseIf sender.text = "Por Cliente" Then
            intRptCtasCbr = 25
        End If

        strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
        GenerarQuery2()
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.DoubleClick
        Try
            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub MenuItem42_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem42.Click

    End Sub

    Private Sub MenuItem44_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem44.Click
        Dim frmNew As New frmEscogerClientes

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem22.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
            Case "Sin Deslizamiento" : intRptCtasCbr = 1
            Case "Con Deslizamiento" : intRptCtasCbr = 2
            Case "Cobro Sin Deslizamiento" : intRptCtasCbr = 30
            Case "Cobro Con Deslizamiento" : intRptCtasCbr = 31
            Case "Sin Intereses" : intRptCtasCbr = 3
            Case "Con Intereses" : intRptCtasCbr = 4
            Case "Saldos Generales Detallados" : intRptCtasCbr = 7
            Case "Detallada del Vendedor" : intRptCtasCbr = 8
            Case "Saldos Resumidos por Vendedor" : intRptCtasCbr = 9
            Case "Vendedores y sus Clientes Asignados" : intRptCtasCbr = 10
            Case "Clientes y los Departamentos Asignados" : intRptCtasCbr = 11
            Case "Resumen General del Vendedor" : intRptCtasCbr = 12
            Case "Facturas Pendientes de los Clientes" : intRptCtasCbr = 13
            Case "Saldos Pendientes de los Clientes" : intRptCtasCbr = 14
            Case "Movimiento Hist�rico de un Cliente" : intRptCtasCbr = 15
            Case "Movimiento Intereses y Mant. de Valor" : intRptCtasCbr = 16
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptCtasCbr > 0 And intRptCtasCbr < 31 Then
            strQueryParte00 = ""
            'strQueryParte00 = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
            strQueryParte00 = "exec sp_rptCuentasCobrar " & 1 & ", "
            GenerarQuery()
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        ElseIf intRptCtasCbr = 15 Or intRptCtasCbr = 16 Then
            If ListBox15.Items.Count = 0 Then
                MsgBox("No digito el cliente al quien se requiere generar su historico de movimientos.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            strQuery = ""
            'strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", ' ', ' ', ' ', ' ', 0, "
            strQuery = "exec sp_rptCuentasCobrar " & 1 & ", ' ', ' ', ' ', ' ', 0, "
            ListBox15.SelectedIndex = 0
            strQuery = strQuery + "" & ListBox15.SelectedItem & ", '" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "', "
            strQuery = strQuery + "'" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "', '" & TextBox2.Text & "', ''"
            Dim frmNew2 As New actrptViewer
            frmNew2.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew2.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub MenuItem45_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem45.Click
        Dim frmNew As New frmEscogerClientes

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem22.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
            Case "Sin Deslizamiento" : intRptCtasCbr = 1
            Case "Con Deslizamiento" : intRptCtasCbr = 2
            Case "Cobro Sin Deslizamiento" : intRptCtasCbr = 30
            Case "Cobro Con Deslizamiento" : intRptCtasCbr = 31
            Case "Sin Intereses" : intRptCtasCbr = 3
            Case "Con Intereses" : intRptCtasCbr = 4
            Case "Saldos Generales Detallados" : intRptCtasCbr = 7
            Case "Detallada del Vendedor" : intRptCtasCbr = 8
            Case "Saldos Resumidos por Vendedor" : intRptCtasCbr = 9
            Case "Vendedores y sus Clientes Asignados" : intRptCtasCbr = 10
            Case "Clientes y los Departamentos Asignados" : intRptCtasCbr = 11
            Case "Resumen General del Vendedor" : intRptCtasCbr = 12
            Case "Facturas Pendientes de los Clientes" : intRptCtasCbr = 13
            Case "Saldos Pendientes de los Clientes" : intRptCtasCbr = 14
            Case "Movimiento Hist�rico de un Cliente" : intRptCtasCbr = 15
            Case "Movimiento Intereses y Mant. de Valor" : intRptCtasCbr = 16
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptCtasCbr > 0 And intRptCtasCbr < 32 Then
            strQueryParte00 = ""
            'strQueryParte00 = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
            strQueryParte00 = "exec sp_rptCuentasCobrar " & 1 & ", "
            GenerarQuery()
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        ElseIf intRptCtasCbr = 15 Or intRptCtasCbr = 16 Then
            If ListBox15.Items.Count = 0 Then
                MsgBox("No digito el cliente al quien se requiere generar su historico de movimientos.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            strQuery = ""
            'strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", ' ', ' ', ' ', ' ', 0, "
            strQuery = "exec sp_rptCuentasCobrar " & 1 & ", ' ', ' ', ' ', ' ', 0, "
            ListBox15.SelectedIndex = 0
            strQuery = strQuery + "" & ListBox15.SelectedItem & ", '" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "', "
            strQuery = strQuery + "'" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "', '" & TextBox2.Text & "', ''"
            Dim frmNew2 As New actrptViewer
            frmNew2.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew2.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub MenuItem48_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem48.Click
        Dim frmNew As New frmEscogerClientes

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem22.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
            Case "Sin Deslizamiento" : intRptCtasCbr = 1
            Case "Con Deslizamiento" : intRptCtasCbr = 2
            Case "Sin Intereses" : intRptCtasCbr = 3
            Case "Con Intereses" : intRptCtasCbr = 4
            Case "Saldos Generales Detallados" : intRptCtasCbr = 7
            Case "Detallada del Vendedor" : intRptCtasCbr = 8
            Case "Saldos Resumidos por Vendedor" : intRptCtasCbr = 9
            Case "Vendedores y sus Clientes Asignados" : intRptCtasCbr = 10
            Case "Clientes y los Departamentos Asignados" : intRptCtasCbr = 11
            Case "Resumen General del Vendedor" : intRptCtasCbr = 12
            Case "Facturas Pendientes de los Clientes" : intRptCtasCbr = 13
            Case "Saldos Pendientes de los Clientes" : intRptCtasCbr = 14
            Case "Movimiento Hist�rico de un Cliente" : intRptCtasCbr = 15
            Case "Movimiento Intereses y Mant. de Valor" : intRptCtasCbr = 16
            Case "Facturas Pendientes de mas de un a�o" : intRptCtasCbr = 27
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptCtasCbr = 27 Then
            strQueryParte00 = ""
            strQueryParte00 = "exec rptFacturasPendienteMasDeanio " & ""
            GenerarQuery()
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub MenuItem49_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem49.Click
        Dim frmNew As New frmEscogerClientes

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem22.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
            Case "Sin Deslizamiento" : intRptCtasCbr = 1
            Case "Con Deslizamiento" : intRptCtasCbr = 2
            Case "Sin Intereses" : intRptCtasCbr = 3
            Case "Con Intereses" : intRptCtasCbr = 4
            Case "Saldos Generales Detallados" : intRptCtasCbr = 7
            Case "Detallada del Vendedor" : intRptCtasCbr = 8
            Case "Saldos Resumidos por Vendedor" : intRptCtasCbr = 9
            Case "Vendedores y sus Clientes Asignados" : intRptCtasCbr = 10
            Case "Clientes y los Departamentos Asignados" : intRptCtasCbr = 11
            Case "Resumen General del Vendedor" : intRptCtasCbr = 12
            Case "Facturas Pendientes de los Clientes" : intRptCtasCbr = 13
            Case "Saldos Pendientes de los Clientes" : intRptCtasCbr = 14
            Case "Movimiento Hist�rico de un Cliente" : intRptCtasCbr = 15
            Case "Movimiento Intereses y Mant. de Valor" : intRptCtasCbr = 16
            Case "Facturas Pendientes de mas de un a�o" : intRptCtasCbr = 27
            Case "Facturas Pendientes de mas de 4 meses" : intRptCtasCbr = 28
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptCtasCbr = 28 Then
            strQueryParte00 = ""
            strQueryParte00 = "exec rptFacturasPendienteMasCuatroMeses " & ""
            GenerarQuery()
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub MenuItem50_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem50.Click
        Dim frmNew As New actrptViewer
        strFechaInicial = ""
        strFechaFinal = ""

        strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
        strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem22.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
            Case "Sin Deslizamiento" : intRptCtasCbr = 1
            Case "Con Deslizamiento" : intRptCtasCbr = 2
            Case "Sin Intereses" : intRptCtasCbr = 3
            Case "Con Intereses" : intRptCtasCbr = 4
            Case "Saldos Generales Detallados" : intRptCtasCbr = 7
            Case "Detallada del Vendedor" : intRptCtasCbr = 8
            Case "Saldos Resumidos por Vendedor" : intRptCtasCbr = 9
            Case "Vendedores y sus Clientes Asignados" : intRptCtasCbr = 10
            Case "Clientes y los Departamentos Asignados" : intRptCtasCbr = 11
            Case "Resumen General del Vendedor" : intRptCtasCbr = 12
            Case "Facturas Pendientes de los Clientes" : intRptCtasCbr = 13
            Case "Saldos Pendientes de los Clientes" : intRptCtasCbr = 14
            Case "Movimiento Hist�rico de un Cliente" : intRptCtasCbr = 15
            Case "Movimiento Intereses y Mant. de Valor" : intRptCtasCbr = 16
            Case "Facturas Pendientes de mas de un a�o" : intRptCtasCbr = 27
            Case "Facturas Pendientes de mas de 4 meses" : intRptCtasCbr = 28
            Case "Recibos Anulados" : intRptCtasCbr = 29
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptCtasCbr = 29 Then
            strQueryParte00 = ""
            strQuery = ""
            strQuery = "exec rptRecibosAnuladas " & "'" & strFechaInicial & "','" & strFechaFinal & "',''"
            'GenerarQuery()
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Public Sub MostrarReporte()
        'Dim frmNew As New actrptViewer
        'Dim frmNew2 As Form
        'Dim frmNew As New frmEscogerClientes
        Dim frmNew As Form
        Dim strAgencias As String = String.Empty
        Dim strAgencias2 As String = String.Empty
        Try

            intRptOtros = 0
            intRptPresup = 0
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptTipoPrecio = 0
            intRptCheques = 0
            intRptImpRecibos = 0

            intRptCtasCbr = cbReportes.SelectedValue

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If (intRptCtasCbr >= 0 And intRptCtasCbr < 15) Or
                (intRptCtasCbr > 29 And intRptCtasCbr < 32) Then

                strQueryParte00 = ""
                'strQuery = ""
                'strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
                If intRptCtasCbr = 30 Or intRptCtasCbr = 31 Then
                    strQueryParte00 = "exec sp_rptCuentasCobrar " & 1 & ", "
                Else
                    strQueryParte00 = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
                End If
                'strQueryParte00 = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
                GenerarQuery()
                'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew = New frmEscogerClientes
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
            If intRptCtasCbr = 15 Or intRptCtasCbr = 16 Then
                If ListBox15.Items.Count = 0 Then
                    MsgBox("No digito el cliente al quien se requiere generar su historico de movimientos.", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
                strQuery = ""
                strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", ' ', ' ', ' ', ' ', 0, "
                ListBox15.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox15.SelectedItem & ", '" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "', "
                strQuery = strQuery + "'" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "', '" & TextBox2.Text & "', ''"

                frmNew = New actrptViewer
                ' frmNew2.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
            If (intRptCtasCbr >= 17 And intRptCtasCbr <= 25) Then
                'Dim frmNew3 As New actrptViewer
                frmNew = New actrptViewer

                strQuery = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "
                GenerarQuery2()
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
            If intRptCtasCbr = 27 Then
                strQuery = ""
                strQueryParte00 = ""
                strQueryParte00 = "exec rptFacturasPendienteMasDeanio " & ""
                'strQuery = "exec rptFacturasPendienteMasDeanio " & ""
                'GenerarQuery2()
                GenerarQuery()
                'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew = New frmEscogerClientes
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
            If intRptCtasCbr = 28 Then
                strQueryParte00 = ""
                strQueryParte00 = "exec rptFacturasPendienteMasCuatroMeses " & ""
                GenerarQuery()
                'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew = New frmEscogerClientes
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
            If intRptCtasCbr = 29 Then

                'strFechaInicial = Format(DateTimePicker1.Value, "MM/dd/yyyy")
                'strFechaFinal = Format(DateTimePicker2.Value, "MM/dd/yyyy")

                strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
                strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")

                strQuery = ""
                strQuery = "exec rptRecibosAnuladas " & "'" & strFechaInicial & "','" & strFechaFinal & "','1,2,3,4'"
                'GenerarQuery()
                'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If

            If intRptCtasCbr = 32 Then

                strQueryParte00 = ""

                strQueryParte00 = "exec sp_rptCuentasCobrar " & intRptCtasCbr & ", "

                GenerarQuery()

                frmNew = New frmEscogerClientes
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        Try
            MostrarReporte()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdiExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiExcel.Click
        intRptExportar = 1
    End Sub

    Private Sub cmdiHtml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiHtml.Click
        intRptExportar = 2
    End Sub

    Private Sub cmdiPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiPdf.Click
        intRptExportar = 3
    End Sub

    Private Sub cmdiRtf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiRtf.Click
        intRptExportar = 4
    End Sub

    Private Sub cmdiTiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiTiff.Click
        intRptExportar = 5
    End Sub

    Private Sub cmdExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExportar.Click
        intRptExportar = 0
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

End Class
