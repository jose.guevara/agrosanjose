<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportesProformas
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportesProformas))
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ddlSerie = New System.Windows.Forms.ComboBox
        Me.lblSerie = New System.Windows.Forms.Label
        Me.txtNumeroFactura = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.cmbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmdReportes = New DevComponents.DotNetBar.ButtonX
        Me.Label18 = New System.Windows.Forms.Label
        Me.cbReportes = New DevComponents.DotNetBar.Controls.ComboBoxEx
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.cmdExportar)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(521, 55)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 39
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.AutoExpandOnClick = True
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(75, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmdExportar
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.FontBold = True
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.FontBold = True
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ddlSerie)
        Me.GroupBox2.Controls.Add(Me.lblSerie)
        Me.GroupBox2.Controls.Add(Me.txtNumeroFactura)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.cmbAgencias)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(-1, 112)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(514, 92)
        Me.GroupBox2.TabIndex = 51
        Me.GroupBox2.TabStop = False
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Location = New System.Drawing.Point(45, 23)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(48, 21)
        Me.ddlSerie.TabIndex = 54
        Me.ddlSerie.Tag = "Serie por agencia"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(9, 26)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(36, 13)
        Me.lblSerie.TabIndex = 56
        Me.lblSerie.Text = "Serie"
        '
        'txtNumeroFactura
        '
        Me.txtNumeroFactura.BackColor = System.Drawing.Color.White
        Me.txtNumeroFactura.Location = New System.Drawing.Point(157, 23)
        Me.txtNumeroFactura.Name = "txtNumeroFactura"
        Me.txtNumeroFactura.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroFactura.TabIndex = 55
        Me.txtNumeroFactura.Tag = "N�mero de Factura"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(105, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "N�mero"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label14.Location = New System.Drawing.Point(9, 99)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(59, 13)
        Me.Label14.TabIndex = 52
        Me.Label14.Text = "Agencias"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(328, 56)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 48
        '
        'cmbAgencias
        '
        Me.cmbAgencias.DisplayMember = "Text"
        Me.cmbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbAgencias.FormattingEnabled = True
        Me.cmbAgencias.ItemHeight = 14
        Me.cmbAgencias.Location = New System.Drawing.Point(74, 92)
        Me.cmbAgencias.Name = "cmbAgencias"
        Me.cmbAgencias.Size = New System.Drawing.Size(370, 20)
        Me.cmbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbAgencias.TabIndex = 51
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(72, 56)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(256, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "FecFinal"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 62)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "FecInicial"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdReportes
        '
        Me.cmdReportes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdReportes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdReportes.Image = CType(resources.GetObject("cmdReportes.Image"), System.Drawing.Image)
        Me.cmdReportes.ImageFixedSize = New System.Drawing.Size(30, 30)
        Me.cmdReportes.Location = New System.Drawing.Point(458, 70)
        Me.cmdReportes.Name = "cmdReportes"
        Me.cmdReportes.Size = New System.Drawing.Size(55, 36)
        Me.cmdReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdReportes.TabIndex = 50
        Me.cmdReportes.Tooltip = "<b><font color=""#17365D"">Generar Reporte</font></b>"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(-3, 81)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 49
        Me.Label18.Text = "Reportes"
        '
        'cbReportes
        '
        Me.cbReportes.DisplayMember = "Text"
        Me.cbReportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReportes.FormattingEnabled = True
        Me.cbReportes.ItemHeight = 15
        Me.cbReportes.Location = New System.Drawing.Point(61, 78)
        Me.cbReportes.Name = "cbReportes"
        Me.cbReportes.Size = New System.Drawing.Size(382, 21)
        Me.cbReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbReportes.TabIndex = 48
        '
        'frmReportesProformas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 222)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.cmdReportes)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cbReportes)
        Me.Controls.Add(Me.bHerramientas)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportesProformas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes de Proformas"
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmdReportes As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cbReportes As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents txtNumeroFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
