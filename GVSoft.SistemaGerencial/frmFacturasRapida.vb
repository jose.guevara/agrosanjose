Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Printing

Public Class frmFacturasRapida
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtImpVenta As System.Windows.Forms.TextBox
    Friend WithEvents txtRetencion As System.Windows.Forms.TextBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdVendedor As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdClientes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProductos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdRefrescar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblRetencion As System.Windows.Forms.Label
    Friend WithEvents lblTipoPrecio As System.Windows.Forms.Label
    Friend WithEvents DPkFechaFactura As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNumeroFactura As System.Windows.Forms.TextBox
    Friend WithEvents txtDias As System.Windows.Forms.TextBox
    Friend WithEvents txtVence As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents ddlRetencion As System.Windows.Forms.ComboBox
    Friend WithEvents ddlTipoPrecio As System.Windows.Forms.ComboBox
    Friend WithEvents txtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartamento As System.Windows.Forms.TextBox
    Friend WithEvents txtNegocio As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents txtVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPrecioCosto As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Cantidad As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Unidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Producto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Valor As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Total As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents IdProducto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdImportProforma As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdImprimirProforma As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cbNumerosProformas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents PrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDialogo As System.Windows.Forms.PrintDialog
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents Label20 As Windows.Forms.Label
    Friend WithEvents txtMoneda As Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFacturasRapida))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Unidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Producto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Total = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IdProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.txtRetencion = New System.Windows.Forms.TextBox()
        Me.txtImpVenta = New System.Windows.Forms.TextBox()
        Me.txtSubTotal = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.cmdVendedor = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdClientes = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdProductos = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdRefrescar = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImportProforma = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdImprimirProforma = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblRetencion = New System.Windows.Forms.Label()
        Me.lblTipoPrecio = New System.Windows.Forms.Label()
        Me.DPkFechaFactura = New System.Windows.Forms.DateTimePicker()
        Me.txtNumeroFactura = New System.Windows.Forms.TextBox()
        Me.txtDias = New System.Windows.Forms.TextBox()
        Me.txtVence = New System.Windows.Forms.TextBox()
        Me.txtNombreVendedor = New System.Windows.Forms.TextBox()
        Me.txtNombreCliente = New System.Windows.Forms.TextBox()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.ddlRetencion = New System.Windows.Forms.ComboBox()
        Me.ddlTipoPrecio = New System.Windows.Forms.ComboBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.txtNumeroVendedor = New System.Windows.Forms.TextBox()
        Me.txtNumeroCliente = New System.Windows.Forms.TextBox()
        Me.txtDepartamento = New System.Windows.Forms.TextBox()
        Me.txtNegocio = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblSerie = New System.Windows.Forms.Label()
        Me.ddlSerie = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtMoneda = New System.Windows.Forms.TextBox()
        Me.cbNumerosProformas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtDireccion = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPrecioCosto = New System.Windows.Forms.TextBox()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.PrintDialogo = New System.Windows.Forms.PrintDialog()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX()
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvDetalle)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtTotal)
        Me.GroupBox2.Controls.Add(Me.txtRetencion)
        Me.GroupBox2.Controls.Add(Me.txtImpVenta)
        Me.GroupBox2.Controls.Add(Me.txtSubTotal)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 207)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(809, 266)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Cantidad, Me.Unidad, Me.Codigo, Me.Producto, Me.Impuesto, Me.Valor, Me.Total, Me.IdProducto, Me.PrecioCosto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(5, 13)
        Me.dgvDetalle.Name = "dgvDetalle"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(795, 142)
        Me.dgvDetalle.TabIndex = 93
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Increment = 1.0R
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.Width = 70
        '
        'Unidad
        '
        Me.Unidad.HeaderText = "Unidad"
        Me.Unidad.Name = "Unidad"
        Me.Unidad.ReadOnly = True
        Me.Unidad.Width = 60
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Producto
        '
        Me.Producto.HeaderText = "Producto"
        Me.Producto.Name = "Producto"
        Me.Producto.ReadOnly = True
        Me.Producto.Width = 280
        '
        'Impuesto
        '
        Me.Impuesto.HeaderText = "Impuesto"
        Me.Impuesto.Name = "Impuesto"
        Me.Impuesto.ReadOnly = True
        Me.Impuesto.Width = 65
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Increment = 1.0R
        Me.Valor.Name = "Valor"
        Me.Valor.Width = 90
        '
        'Total
        '
        '
        '
        '
        Me.Total.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Total.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Total.HeaderText = "Total"
        Me.Total.Increment = 1.0R
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'IdProducto
        '
        Me.IdProducto.HeaderText = "IdProducto"
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.ReadOnly = True
        Me.IdProducto.Visible = False
        '
        'PrecioCosto
        '
        Me.PrecioCosto.HeaderText = "PrecioCosto"
        Me.PrecioCosto.Name = "PrecioCosto"
        Me.PrecioCosto.ReadOnly = True
        Me.PrecioCosto.Visible = False
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(64, 155)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 65
        Me.Label19.Text = "Label19"
        Me.Label19.Visible = False
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(8, 155)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 64
        Me.Label18.Text = "Label18"
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(541, 214)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(64, 16)
        Me.Label17.TabIndex = 63
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(296, 195)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(93, 20)
        Me.Label16.TabIndex = 62
        Me.Label16.Text = "ANULADA"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(304, 171)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 13)
        Me.Label15.TabIndex = 61
        Me.Label15.Text = "<F5> AYUDA"
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(673, 237)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(96, 20)
        Me.txtTotal.TabIndex = 60
        Me.txtTotal.TabStop = False
        Me.txtTotal.Tag = "Cantidad a Facturar"
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRetencion
        '
        Me.txtRetencion.Location = New System.Drawing.Point(673, 213)
        Me.txtRetencion.Name = "txtRetencion"
        Me.txtRetencion.Size = New System.Drawing.Size(96, 20)
        Me.txtRetencion.TabIndex = 59
        Me.txtRetencion.TabStop = False
        Me.txtRetencion.Tag = "Cantidad a Facturar"
        Me.txtRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpVenta
        '
        Me.txtImpVenta.Location = New System.Drawing.Point(673, 189)
        Me.txtImpVenta.Name = "txtImpVenta"
        Me.txtImpVenta.Size = New System.Drawing.Size(96, 20)
        Me.txtImpVenta.TabIndex = 58
        Me.txtImpVenta.TabStop = False
        Me.txtImpVenta.Tag = "Cantidad a Facturar"
        Me.txtImpVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Location = New System.Drawing.Point(673, 165)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.Size = New System.Drawing.Size(96, 20)
        Me.txtSubTotal.TabIndex = 57
        Me.txtSubTotal.TabStop = False
        Me.txtSubTotal.Tag = "Cantidad a Facturar"
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(609, 237)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 56
        Me.Label14.Text = "Total Fact"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(609, 213)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 55
        Me.Label13.Text = "Retenci�n"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(609, 189)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 54
        Me.Label12.Text = "Imp Venta"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(609, 165)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Sub Total"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 5000
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.ButtonX1)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdImportProforma, Me.cmdImprimirProforma, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(814, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 33
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.AutoExpandOnClick = True
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Image = CType(resources.GetObject("ButtonX1.Image"), System.Drawing.Image)
        Me.ButtonX1.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.ButtonX1.Location = New System.Drawing.Point(6, 8)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 54)
        Me.ButtonX1.SplitButton = True
        Me.ButtonX1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdVendedor, Me.cmdClientes, Me.cmdProductos, Me.cmdRefrescar})
        Me.ButtonX1.TabIndex = 0
        Me.ButtonX1.Tooltip = "<b><font color=""#17365D"">Listado</font></b>"
        '
        'cmdVendedor
        '
        Me.cmdVendedor.GlobalItem = False
        Me.cmdVendedor.Image = CType(resources.GetObject("cmdVendedor.Image"), System.Drawing.Image)
        Me.cmdVendedor.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdVendedor.Name = "cmdVendedor"
        Me.cmdVendedor.Text = "Vendedores"
        '
        'cmdClientes
        '
        Me.cmdClientes.GlobalItem = False
        Me.cmdClientes.Image = CType(resources.GetObject("cmdClientes.Image"), System.Drawing.Image)
        Me.cmdClientes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdClientes.Name = "cmdClientes"
        Me.cmdClientes.Text = "Clientes"
        '
        'cmdProductos
        '
        Me.cmdProductos.GlobalItem = False
        Me.cmdProductos.Image = CType(resources.GetObject("cmdProductos.Image"), System.Drawing.Image)
        Me.cmdProductos.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdProductos.Name = "cmdProductos"
        Me.cmdProductos.Text = "Productos"
        '
        'cmdRefrescar
        '
        Me.cmdRefrescar.GlobalItem = False
        Me.cmdRefrescar.Image = CType(resources.GetObject("cmdRefrescar.Image"), System.Drawing.Image)
        Me.cmdRefrescar.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdRefrescar.Name = "cmdRefrescar"
        Me.cmdRefrescar.Text = "Refrescar"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.ButtonX1
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdImportProforma
        '
        Me.cmdImportProforma.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdImportProforma.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Upload_256x256
        Me.cmdImportProforma.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportProforma.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportProforma.Name = "cmdImportProforma"
        Me.cmdImportProforma.Text = "Importar Proforma"
        '
        'cmdImprimirProforma
        '
        Me.cmdImprimirProforma.Image = CType(resources.GetObject("cmdImprimirProforma.Image"), System.Drawing.Image)
        Me.cmdImprimirProforma.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimirProforma.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimirProforma.Name = "cmdImprimirProforma"
        Me.cmdImprimirProforma.Text = "Imprimir Proforma"
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 461)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 665
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(814, 23)
        Me.UltraStatusBar1.TabIndex = 34
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(245, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "N�mero"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(375, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "D�as"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(456, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Vence"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(165, 113)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Cliente"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(165, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Producto"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 84)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Cantidad"
        '
        'lblRetencion
        '
        Me.lblRetencion.AutoSize = True
        Me.lblRetencion.Location = New System.Drawing.Point(333, 84)
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Size = New System.Drawing.Size(65, 13)
        Me.lblRetencion.TabIndex = 8
        Me.lblRetencion.Text = "Retenci�n"
        '
        'lblTipoPrecio
        '
        Me.lblTipoPrecio.AutoSize = True
        Me.lblTipoPrecio.Location = New System.Drawing.Point(452, 84)
        Me.lblTipoPrecio.Name = "lblTipoPrecio"
        Me.lblTipoPrecio.Size = New System.Drawing.Size(72, 13)
        Me.lblTipoPrecio.TabIndex = 9
        Me.lblTipoPrecio.Text = "Tipo Precio"
        '
        'DPkFechaFactura
        '
        Me.DPkFechaFactura.CustomFormat = "dd-MMM-yyyy"
        Me.DPkFechaFactura.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkFechaFactura.Location = New System.Drawing.Point(48, 16)
        Me.DPkFechaFactura.Name = "DPkFechaFactura"
        Me.DPkFechaFactura.Size = New System.Drawing.Size(104, 20)
        Me.DPkFechaFactura.TabIndex = 0
        Me.DPkFechaFactura.Tag = "Fecha de Ingreso"
        '
        'txtNumeroFactura
        '
        Me.txtNumeroFactura.Location = New System.Drawing.Point(293, 16)
        Me.txtNumeroFactura.Name = "txtNumeroFactura"
        Me.txtNumeroFactura.ReadOnly = True
        Me.txtNumeroFactura.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroFactura.TabIndex = 2
        Me.txtNumeroFactura.Tag = "N�mero de Factura"
        '
        'txtDias
        '
        Me.txtDias.Location = New System.Drawing.Point(407, 16)
        Me.txtDias.Name = "txtDias"
        Me.txtDias.Size = New System.Drawing.Size(40, 20)
        Me.txtDias.TabIndex = 3
        Me.txtDias.Tag = "D�as de Cr�dito"
        Me.txtDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVence
        '
        Me.txtVence.Location = New System.Drawing.Point(496, 16)
        Me.txtVence.Name = "txtVence"
        Me.txtVence.ReadOnly = True
        Me.txtVence.Size = New System.Drawing.Size(88, 20)
        Me.txtVence.TabIndex = 4
        Me.txtVence.Tag = "Fecha de Vencimiento"
        '
        'txtNombreVendedor
        '
        Me.txtNombreVendedor.Location = New System.Drawing.Point(292, 109)
        Me.txtNombreVendedor.Name = "txtNombreVendedor"
        Me.txtNombreVendedor.ReadOnly = True
        Me.txtNombreVendedor.Size = New System.Drawing.Size(232, 20)
        Me.txtNombreVendedor.TabIndex = 6
        Me.txtNombreVendedor.TabStop = False
        Me.txtNombreVendedor.Tag = "Vendedor"
        '
        'txtNombreCliente
        '
        Me.txtNombreCliente.Location = New System.Drawing.Point(135, 48)
        Me.txtNombreCliente.Name = "txtNombreCliente"
        Me.txtNombreCliente.ReadOnly = True
        Me.txtNombreCliente.Size = New System.Drawing.Size(224, 20)
        Me.txtNombreCliente.TabIndex = 7
        Me.txtNombreCliente.Tag = ""
        '
        'txtProducto
        '
        Me.txtProducto.Location = New System.Drawing.Point(229, 80)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(91, 20)
        Me.txtProducto.TabIndex = 9
        Me.txtProducto.Tag = "C�digo del Producto"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(72, 80)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 20)
        Me.txtCantidad.TabIndex = 8
        Me.txtCantidad.Tag = "Cantidad a Facturar"
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ddlRetencion
        '
        Me.ddlRetencion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlRetencion.Items.AddRange(New Object() {"No", "Si"})
        Me.ddlRetencion.Location = New System.Drawing.Point(397, 80)
        Me.ddlRetencion.Name = "ddlRetencion"
        Me.ddlRetencion.Size = New System.Drawing.Size(48, 21)
        Me.ddlRetencion.TabIndex = 10
        Me.ddlRetencion.Tag = "Tiene Retenci�n de Impuesto"
        '
        'ddlTipoPrecio
        '
        Me.ddlTipoPrecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlTipoPrecio.Location = New System.Drawing.Point(524, 80)
        Me.ddlTipoPrecio.Name = "ddlTipoPrecio"
        Me.ddlTipoPrecio.Size = New System.Drawing.Size(193, 21)
        Me.ddlTipoPrecio.TabIndex = 11
        Me.ddlTipoPrecio.Tag = "Tipo de Precio"
        '
        'txtPrecio
        '
        Me.txtPrecio.Location = New System.Drawing.Point(72, 109)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.ReadOnly = True
        Me.txtPrecio.Size = New System.Drawing.Size(87, 20)
        Me.txtPrecio.TabIndex = 12
        Me.txtPrecio.Tag = "Valor del Precio"
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNumeroVendedor
        '
        Me.txtNumeroVendedor.Location = New System.Drawing.Point(723, 16)
        Me.txtNumeroVendedor.Name = "txtNumeroVendedor"
        Me.txtNumeroVendedor.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroVendedor.TabIndex = 21
        Me.txtNumeroVendedor.TabStop = False
        Me.txtNumeroVendedor.Tag = ""
        Me.txtNumeroVendedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroVendedor.Visible = False
        '
        'txtNumeroCliente
        '
        Me.txtNumeroCliente.Location = New System.Drawing.Point(723, 40)
        Me.txtNumeroCliente.Name = "txtNumeroCliente"
        Me.txtNumeroCliente.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroCliente.TabIndex = 22
        Me.txtNumeroCliente.TabStop = False
        Me.txtNumeroCliente.Tag = ""
        Me.txtNumeroCliente.Text = "Cliente"
        Me.txtNumeroCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroCliente.Visible = False
        '
        'txtDepartamento
        '
        Me.txtDepartamento.Location = New System.Drawing.Point(723, 64)
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.Size = New System.Drawing.Size(40, 20)
        Me.txtDepartamento.TabIndex = 23
        Me.txtDepartamento.TabStop = False
        Me.txtDepartamento.Tag = ""
        Me.txtDepartamento.Text = "Departamento"
        Me.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDepartamento.Visible = False
        '
        'txtNegocio
        '
        Me.txtNegocio.Location = New System.Drawing.Point(723, 88)
        Me.txtNegocio.Name = "txtNegocio"
        Me.txtNegocio.Size = New System.Drawing.Size(40, 20)
        Me.txtNegocio.TabIndex = 24
        Me.txtNegocio.TabStop = False
        Me.txtNegocio.Tag = ""
        Me.txtNegocio.Text = "Negocio"
        Me.txtNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNegocio.Visible = False
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(691, 16)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(24, 20)
        Me.TextBox17.TabIndex = 25
        Me.TextBox17.TabStop = False
        Me.TextBox17.Text = "0"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox17.Visible = False
        '
        'txtVendedor
        '
        Me.txtVendedor.Location = New System.Drawing.Point(229, 109)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Size = New System.Drawing.Size(64, 20)
        Me.txtVendedor.TabIndex = 5
        Me.txtVendedor.Tag = "Vendedor"
        '
        'txtCliente
        '
        Me.txtCliente.Location = New System.Drawing.Point(72, 48)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(64, 20)
        Me.txtCliente.TabIndex = 6
        Me.txtCliente.Tag = "Cliente"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(26, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Precio"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(158, 19)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(36, 13)
        Me.lblSerie.TabIndex = 27
        Me.lblSerie.Text = "Serie"
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Location = New System.Drawing.Point(194, 16)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(48, 21)
        Me.ddlSerie.TabIndex = 1
        Me.ddlSerie.Tag = "Serie por agencia"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtMoneda)
        Me.GroupBox1.Controls.Add(Me.cbNumerosProformas)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.txtPrecioCosto)
        Me.GroupBox1.Controls.Add(Me.ddlSerie)
        Me.GroupBox1.Controls.Add(Me.lblSerie)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.txtVendedor)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.txtNegocio)
        Me.GroupBox1.Controls.Add(Me.txtDepartamento)
        Me.GroupBox1.Controls.Add(Me.txtNumeroCliente)
        Me.GroupBox1.Controls.Add(Me.txtNumeroVendedor)
        Me.GroupBox1.Controls.Add(Me.txtPrecio)
        Me.GroupBox1.Controls.Add(Me.ddlTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.ddlRetencion)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.txtNombreCliente)
        Me.GroupBox1.Controls.Add(Me.txtNombreVendedor)
        Me.GroupBox1.Controls.Add(Me.txtVence)
        Me.GroupBox1.Controls.Add(Me.txtDias)
        Me.GroupBox1.Controls.Add(Me.txtNumeroFactura)
        Me.GroupBox1.Controls.Add(Me.DPkFechaFactura)
        Me.GroupBox1.Controls.Add(Me.lblTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.lblRetencion)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(1, 66)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(809, 135)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(529, 113)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(52, 13)
        Me.Label20.TabIndex = 46
        Me.Label20.Text = "Moneda"
        '
        'txtMoneda
        '
        Me.txtMoneda.Location = New System.Drawing.Point(582, 109)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.ReadOnly = True
        Me.txtMoneda.Size = New System.Drawing.Size(217, 20)
        Me.txtMoneda.TabIndex = 45
        Me.txtMoneda.Tag = "Moneda"
        '
        'cbNumerosProformas
        '
        Me.cbNumerosProformas.DisplayMember = "Text"
        Me.cbNumerosProformas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbNumerosProformas.FormattingEnabled = True
        Me.cbNumerosProformas.ItemHeight = 14
        Me.cbNumerosProformas.Location = New System.Drawing.Point(590, 16)
        Me.cbNumerosProformas.Name = "cbNumerosProformas"
        Me.cbNumerosProformas.Size = New System.Drawing.Size(173, 20)
        Me.cbNumerosProformas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.cbNumerosProformas.TabIndex = 42
        Me.cbNumerosProformas.WatermarkColor = System.Drawing.Color.Black
        Me.cbNumerosProformas.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbNumerosProformas.WatermarkText = "Numero de Proformas"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(383, 53)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Direcci�n"
        '
        'txtDireccion
        '
        '
        '
        '
        Me.txtDireccion.Border.Class = "TextBoxBorder"
        Me.txtDireccion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDireccion.Location = New System.Drawing.Point(450, 50)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.ReadOnly = True
        Me.txtDireccion.Size = New System.Drawing.Size(264, 20)
        Me.txtDireccion.TabIndex = 29
        '
        'txtPrecioCosto
        '
        Me.txtPrecioCosto.Location = New System.Drawing.Point(721, 109)
        Me.txtPrecioCosto.Name = "txtPrecioCosto"
        Me.txtPrecioCosto.ReadOnly = True
        Me.txtPrecioCosto.Size = New System.Drawing.Size(42, 20)
        Me.txtPrecioCosto.TabIndex = 28
        Me.txtPrecioCosto.Tag = "Valor del Precio"
        Me.txtPrecioCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioCosto.Visible = False
        '
        'PrintDoc
        '
        Me.PrintDoc.DocumentName = "Proformas"
        '
        'PrintDialogo
        '
        Me.PrintDialogo.AllowCurrentPage = True
        Me.PrintDialogo.UseEXDialog = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "&Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "L&impiar"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Vendedores"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Clientes"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Productos"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Refrescar"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem12.Text = "&Listado"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 4
        Me.MenuItem4.Text = "&Salir"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12, Me.MenuItem4})
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(72, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.GlobalItem = False
        Me.ButtonItem1.Image = CType(resources.GetObject("ButtonItem1.Image"), System.Drawing.Image)
        Me.ButtonItem1.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Text = "Excel"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.GlobalItem = False
        Me.ButtonItem2.Image = CType(resources.GetObject("ButtonItem2.Image"), System.Drawing.Image)
        Me.ButtonItem2.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Text = "HTML"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.GlobalItem = False
        Me.ButtonItem3.Image = CType(resources.GetObject("ButtonItem3.Image"), System.Drawing.Image)
        Me.ButtonItem3.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.Text = "PDF"
        '
        'ButtonItem4
        '
        Me.ButtonItem4.GlobalItem = False
        Me.ButtonItem4.Image = CType(resources.GetObject("ButtonItem4.Image"), System.Drawing.Image)
        Me.ButtonItem4.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.Text = "RTF"
        '
        'ButtonItem5
        '
        Me.ButtonItem5.GlobalItem = False
        Me.ButtonItem5.Image = CType(resources.GetObject("ButtonItem5.Image"), System.Drawing.Image)
        Me.ButtonItem5.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.Text = "TIFF"
        '
        'frmFacturasRapida
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(814, 484)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmFacturasRapida"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmFacturasRapida"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim sSerie As String = String.Empty
    Dim inExisteFactura As Integer = 0
    Dim IndicaDebeBuscar As Boolean = True
    Dim strUnidad As String
    Dim strFacturas As String
    Dim strDescrip As String
    Dim intImpuesto As Integer
    Dim intImprimir As Integer
    Dim intRow, intCol As Integer
    Dim lngRegistro2 As Long
    Dim IndicaImprimir As Integer = 0
    Dim valorAnt As Double = 0
    Dim gnCantidadAnterior As Decimal = 0
    Public txtCollection As New Collection
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmFacturasRapida"
    Private gbIndicaCierre As Boolean = False

    Private prtFont As System.Drawing.Font
    Private lineaActual As Integer

    Dim strNombAlmacenar As String
    Dim strNombArchivo As String

    Private Sub frmFacturasRapida_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim frmNew As frmEscogerAgencia = Nothing
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnTasaCambioDelDia As Decimal = 0


        Try
            intDiaFact = 0
            strMesFact = ""
            lngYearFact = 0
            strVendedor = ""
            strNumeroFact = ""
            strCodCliFact = ""
            strClienteFact = ""

            frmNew = New frmEscogerAgencia
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambioDelDia = RNTipoCambio.ObtieneTipoCambioDelDia()

            'Me.Top = 0
            'Me.Left = 0
            strUnidad = ""
            strDescrip = ""
            lngRegistro = 0
            intImprimir = 0
            txtCollection.Add(txtNumeroFactura)
            txtCollection.Add(txtDias)
            txtCollection.Add(txtVence)
            txtCollection.Add(txtNombreVendedor)
            txtCollection.Add(txtNombreCliente)
            txtCollection.Add(txtProducto)
            txtCollection.Add(txtCantidad)
            txtCollection.Add(txtPrecio)
            txtCollection.Add(txtSubTotal)
            txtCollection.Add(txtImpVenta)
            txtCollection.Add(txtRetencion)
            txtCollection.Add(txtTotal)
            txtCollection.Add(txtNumeroVendedor)
            txtCollection.Add(txtNumeroCliente)
            txtCollection.Add(txtDepartamento)
            txtCollection.Add(txtNegocio)
            CargaTipoPrecio()
            UbicarAgencia(lngRegUsuario)
            If (lngRegAgencia = 0) Then
                lngRegAgencia = 0
                frmNew.ShowDialog()
                If lngRegAgencia = 0 Then
                    MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
                End If
            End If
            Limpiar()
            MenuItem8.Visible = False
            If lnTasaCambioDelDia <= 0 Then
                MsgBox("No se ha ingresado la tasa del d�a, favor ingresar la tasa del d�a antes de realizar operaciones ", MsgBoxStyle.Critical, "Error de Datos")
                gbIndicaCierre = True
                'Me.Close()
            End If
            txtMoneda.Text = RNMoneda.ObtieneDescripcionMoneda(objParametro.MonedaFacturaContado)

            'If intTipoFactura = 3 Or intTipoFactura = 6 Then
            '    ddlSerie.Focus()
            'End If
            'If intTipoFactura = 2 Or intTipoFactura = 5 Then
            '    ddlSerie.Visible = False
            '    lblSerie.Visible = False
            '    MenuItem8.Visible = True
            '    CambiaIcoImpresion(cmdOK)
            '    UbicarPendientes()
            'End If
            Select Case intTipoFactura
                Case ENTipoFactura.FACTURA_CONTADO_ANULACION
                    ddlSerie.Focus()
                    'dgvDetalle.Enabled = False
                    dgvDetalle.Columns("Cantidad").ReadOnly = True
                    dgvDetalle.Columns("Valor").ReadOnly = True
                    cbNumerosProformas.Visible = False
                    cmdImportProforma.Visible = False
                    cmdImprimirProforma.Visible = False
                Case ENTipoFactura.FACTURA_CONTADO_IMPRESION
                    ddlSerie.Visible = False
                    lblSerie.Visible = False
                    MenuItem8.Visible = True
                    CambiaIcoImpresion(cmdOK)
                    UbicarPendientes()
                    'dgvDetalle.Enabled = False
                    dgvDetalle.Columns("Cantidad").ReadOnly = True
                    dgvDetalle.Columns("Valor").ReadOnly = True
                    cbNumerosProformas.Visible = False
                    cmdImportProforma.Visible = False
                    cmdImprimirProforma.Visible = False
                Case ENTipoFactura.FACTURA_CONTADO_CONSULTAR
                    CambiaIcoImpresion(cmdOK)
                    'dgvDetalle.Enabled = False
                    dgvDetalle.Columns("Cantidad").ReadOnly = True
                    dgvDetalle.Columns("Valor").ReadOnly = True
                    cbNumerosProformas.Visible = False
                    cmdImportProforma.Visible = False
                    cmdImprimirProforma.Visible = False
                    'Case ENTipoFactura.FACTURA_CONTADO, ENTipoFactura.FACTURA_CONTADO_MANUAL
                    '    If objParametro.MonedaFacturaContado = 2 And lnTasaCambioDelDia <= 0 Then
                    '        MsgBox("No se ha ingresado la tasa del d�a, favor ingresar la tasa del d�a antes de realizar operaciones ", MsgBoxStyle.Critical, "Error de Datos")
                    '        'Me.Close()
                    '        gbIndicaCierre = True
                    '    End If
                    '    txtMoneda.Text = RNMoneda.ObtieneDescripcionMoneda(objParametro.MonedaFacturaContado)
                    'Case ENTipoFactura.FACTURA_CREDITO, ENTipoFactura.FACTURA_CREDITO_MANUAL
            End Select

            Label16.Visible = False
            Label18.Text = intTipoFactura
            Label19.Text = lngRegAgencia
            ObtieneSeriexAgencia()
            ObtieneVendedorPorDefecto()
            ObtengoDatosListadoAyuda()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0

        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection IsNot Nothing Then
                If cmdAgro2K.Connection.State = ConnectionState.Open Then
                    cmdAgro2K.Connection.Close()
                End If
            End If

            ddlSerie.SelectedItem = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            Select Case intTipoFactura
                Case ENTipoFactura.FACTURA_CONTADO,
                    ENTipoFactura.FACTURA_CONTADO_IMPRESION,
                    ENTipoFactura.FACTURA_CONTADO_ANULACION,
                    ENTipoFactura.FACTURA_CONTADO_MANUAL,
                    ENTipoFactura.FACTURA_CONTADO_CONSULTAR
                    strQuery = "ObtieneSerieParametrosFacturaRapida " & lngRegAgencia & ",1"
            End Select
            If strQuery.Trim.Length > 0 Then
                ddlSerie.Items.Clear()
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                    IndicaObtieneRegistro = 1
                End While
            End If
            If IndicaObtieneRegistro = 1 Then
                ddlSerie.SelectedIndex = 0
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            If cmdAgro2K.Connection IsNot Nothing Then
                If cmdAgro2K.Connection.State = ConnectionState.Open Then
                    cmdAgro2K.Connection.Close()
                End If
            End If
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub ObtieneVendedorPorDefecto()
        Dim registroVendor As Integer
        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection IsNot Nothing Then
                If cmdAgro2K.Connection.State = ConnectionState.Open Then
                    cmdAgro2K.Connection.Close()
                End If
            End If
            ddlSerie.SelectedItem = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select IdVendedor From prm_agencias where registro =" & lngRegAgencia
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                registroVendor = ConvierteAInt(dtrAgro2K.GetValue(0))
                txtNumeroVendedor.Text = ConvierteAInt(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select registro,codigo,nombre From prm_vendedores where registro =" & registroVendor
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtVendedor.Text = dtrAgro2K.GetValue(1)
                txtNombreVendedor.Text = dtrAgro2K.GetValue(2)
            End While

            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            If cmdAgro2K.Connection IsNot Nothing Then
                If cmdAgro2K.Connection.State = ConnectionState.Open Then
                    cmdAgro2K.Connection.Close()
                End If
            End If
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmFacturasRapida_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)

                Select Case intTipoFactura
                    Case ENTipoFactura.FACTURA_CONTADO_ANULACION,
                        ENTipoFactura.FACTURA_CREDITO_ANULACION
                        If dgvDetalle.Rows.Count < 1 Then
                            MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                            Exit Sub
                        End If
                        Anular()
                        MsgBox("Factura fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Factura")
                        ddlSerie.Focus()
                    Case ENTipoFactura.FACTURA_CONTADO_IMPRESION,
                    ENTipoFactura.FACTURA_CREDITO_IMPRESION,
                        ENTipoFactura.FACTURA_CONTADO_MANUAL,
                        ENTipoFactura.FACTURA_CREDITO_MANUAL,
                        ENTipoFactura.FACTURA_CREDITO_CONSULTAR,
                        ENTipoFactura.FACTURA_CONTADO_CONSULTAR
                        If dgvDetalle.Rows.Count < 1 Then
                            MsgBox("No hay productos a facturar", MsgBoxStyle.Critical, "Error de Datos")
                            txtCantidad.Focus()
                            Exit Sub
                        End If
                        Guardar()
                End Select
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If MenuItem5.Visible = True Then
                    intListadoAyuda = 1
                ElseIf MenuItem6.Visible = True Then
                    intListadoAyuda = 2
                ElseIf MenuItem7.Visible = True Then
                    intListadoAyuda = 3
                End If
                Dim frmNew As New frmListadoAyuda
                frmNew.ShowDialog()
            ElseIf e.KeyCode = Keys.F7 Then
                If dgvDetalle.Rows.Count < 1 Then
                    MsgBox("No hay productos a facturar", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                Guardar()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub CargaTipoPrecio()
        Try
            ddlTipoPrecio.Items.Add(New ListItem("Precio venta publico", 0))
            ddlTipoPrecio.Items.Add(New ListItem("Precio venta distribuidor", 1))
            If RNFacturas.ValidaPermisoVerCosto(lngRegUsuario) = 1 Then
                ddlTipoPrecio.Items.Add(New ListItem("Precio costo", 2))
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click

        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            Select Case sender.text.ToString
                Case "Imprimir"
                    If dgvDetalle.Rows.Count < 1 Then
                        MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                        txtCantidad.Focus()
                        Exit Sub
                    End If
                    Guardar()
                    'UbicarPendientes()
                Case "&Guardar"
                    If dgvDetalle.Rows.Count < 1 Then
                        MsgBox("No hay productos a facturar", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    Guardar()
                Case "Anular"
                    If dgvDetalle.Rows.Count < 1 Then
                        MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    If intTipoFactura = ENTipoFactura.FACTURA_CONTADO_ANULACION Or
                       intTipoFactura = ENTipoFactura.FACTURA_CREDITO_ANULACION Then
                        intResp = MsgBox("�Est� Seguro de Anular esta Factura?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Acci�n")
                        If intResp = 6 Then
                            Anular()
                            MsgBox("Factura fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Factura")
                            ddlSerie.Focus()
                        End If
                    End If
                Case "&Cancelar", "L&impiar" : Limpiar()
                Case "Vendedores" : intListadoAyuda = 1
                Case "Clientes" : intListadoAyuda = 2
                Case "Productos" : intListadoAyuda = 3
                Case "&Salir" : Me.Close()
            End Select
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumeroFactura.GotFocus, txtDias.GotFocus, txtVence.GotFocus, txtNombreVendedor.GotFocus, txtNombreCliente.GotFocus, txtProducto.GotFocus, txtCantidad.GotFocus, txtPrecio.GotFocus, txtVendedor.GotFocus, txtCliente.GotFocus, ddlRetencion.GotFocus, ddlTipoPrecio.GotFocus, DPkFechaFactura.GotFocus, txtCliente.GotFocus

        Dim strCampo As String

        Try
            Label15.Visible = False
            MenuItem5.Visible = False
            MenuItem6.Visible = False
            MenuItem7.Visible = False
            MenuItem12.Visible = False
            strCampo = ""
            Select Case sender.name.ToString
                Case "DPkFechaFactura" : strCampo = "Fecha"
                Case "txtNumeroFactura" : strCampo = "N�mero"
                    'If intTipoFactura = 2 Or _
                    '   intTipoFactura = 5 Then
                    If intTipoFactura = ENTipoFactura.FACTURA_CONTADO_IMPRESION Or
                       intTipoFactura = ENTipoFactura.FACTURA_CREDITO_IMPRESION Then
                        MenuItem12.Visible = True
                    End If
                Case "txtDia" : strCampo = "D�as Cr�dito"
                Case "txtVencimiento" : strCampo = "Vencimiento"
                Case "txtVendedor" : strCampo = "Vendedor" : MenuItem12.Visible = True : MenuItem5.Visible = True : Label15.Visible = True
                Case "txtCliente" : strCampo = "Cliente"
                    'If intTipoFactura = 4 Or intTipoFactura = 8 Then
                    If intTipoFactura = ENTipoFactura.FACTURA_CREDITO Or
                       intTipoFactura = ENTipoFactura.FACTURA_CREDITO_MANUAL Then
                        MenuItem12.Visible = True
                        MenuItem6.Visible = True
                        Label15.Visible = True
                    End If
                Case "txtNombreCliente" : strCampo = "Cliente"
                    If intTipoFactura = ENTipoFactura.FACTURA_CONTADO Or
                       intTipoFactura = ENTipoFactura.FACTURA_CONTADO_MANUAL Then
                        MenuItem6.Visible = True
                    End If
                Case "txtProducto" : strCampo = "Producto" : MenuItem12.Visible = True : MenuItem7.Visible = True : Label15.Visible = True
                Case "txtCantidad" : strCampo = "Cantidad"
                Case "TextBox8" : strCampo = "Tipo de Precio"
                Case "ddlRentencion" : strCampo = "Retenci�n"
                Case "ddlTipoPrecio" : strCampo = "Tipo Precio"
            End Select
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            'StatusBar1.Panels.Item(0).Text = strCampo
            'StatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name <> "DPkFechaFactura" Then
                sender.selectall()
            End If
            If blnUbicar Then
                ObtengoDatosListadoAyuda()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub ExtraerFactura()

        Me.Cursor = Cursors.WaitCursor
        Dim lngFecha As Long
        Dim strGenNumFact As String
        Dim dteFechaIng As Date

        Try
            strNumeroUbicarFactura = ""
            strFacturas = ""
            ' If intTipoFactura <> 2 And intTipoFactura <> 5 Then
            If intTipoFactura <> ENTipoFactura.FACTURA_CONTADO_IMPRESION And intTipoFactura <> ENTipoFactura.FACTURA_CREDITO_IMPRESION Then
                If IsNumeric(txtNumeroFactura.Text) = False Then
                    MsgBox("Tiene que ingresar un n�mero de factura a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
                If Trim(txtNumeroFactura.Text) = "" Then
                    MsgBox("Tiene que ingresar un n�mero de factura a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else
                If Trim(txtNumeroFactura.Text) = "" Then
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If

            ' Nueva Modalidad de Busqueda y Verificacion por Facturas Nuevas (I)
            If intFechaCambioFactura >= 0 And (intTipoFactura = ENTipoFactura.FACTURA_CREDITO_ANULACION Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_CONSULTAR) Then
                strGenNumFact = ""
                If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "C" Or Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                    txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                End If
                strGenNumFact = "('" & Format(CLng(txtNumeroFactura.Text), "0000000") & "C','" & Format(CLng(txtNumeroFactura.Text), "0000000") & "A')"

                Try
                    dtrAgro2K = RNFacturas.UbicarFacturas(lngRegAgencia, strGenNumFact)
                Catch exc As Exception
                    MsgBox(exc.Message.ToString)
                    dtrAgro2K.Close()
                    Exit Sub
                End Try
                If intFechaCambioFactura = 0 Then
                    txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "C"
                Else
                    txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "A"
                End If

                If (intTipoFactura = 6 Or intTipoFactura = 10) And intFechaCambioFactura > 0 Then
                    If dtrAgro2K.Read() Then
                        strNumeroUbicarFactura = ""
                        dtrAgro2K.Close()
                        dtrAgro2K = RNFacturas.UbicarFacturas(lngRegAgencia, strGenNumFact)
                        Dim frmNew As New frmUbicarFacturas
                        frmNew.ShowDialog()
                        txtNumeroFactura.Text = strNumeroUbicarFactura
                    End If
                End If
            End If
            ' Nueva Modalidad de Busqueda y Verificacion por Facturas Nuevas (F)

            If intFechaCambioFactura >= 0 And (intTipoFactura = 6 Or intTipoFactura = 8 Or intTipoFactura = 10) Then
                If strNumeroUbicarFactura = "" Then
                    If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "C" Or Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                        txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                    End If
                    If IsNumeric((Microsoft.VisualBasic.Left(txtNumeroFactura.Text, 1))) = False Then
                        txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 2, Len(txtNumeroFactura.Text))
                        If IsNumeric((Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1))) = False Then
                            txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                        End If
                    Else
                        If IsNumeric((Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1))) = False Then
                            txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                        End If
                    End If
                    If intFechaCambioFactura = 0 Then
                        txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "C"
                    Else
                        txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "A"
                    End If
                End If
            End If
            strNumeroFact = txtNumeroFactura.Text
            If IsNumeric((Microsoft.VisualBasic.Left(strNumeroFact, 1))) = False Then
                strNumeroFact = Mid(strNumeroFact, 2, Len(strNumeroFact))
                If IsNumeric((Microsoft.VisualBasic.Right(strNumeroFact, 1))) = False Then
                    strNumeroFact = Mid(strNumeroFact, 1, Len(strNumeroFact) - 1)
                End If
            Else
                If IsNumeric((Microsoft.VisualBasic.Right(strNumeroFact, 1))) = False Then
                    strNumeroFact = Mid(strNumeroFact, 1, Len(strNumeroFact) - 1)
                End If
            End If
            If intFechaCambioFactura = 0 Then
                strNumeroFact = ddlSerie.SelectedItem & Format(CLng(strNumeroFact), "0000000") & "C"
            Else
                strNumeroFact = ddlSerie.SelectedItem & Format(CLng(strNumeroFact), "0000000") & "A"
            End If

            If intTipoFactura = 3 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
                txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000")
            End If
            If IsNumeric((Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1))) = False Then
                strNumeroFact = txtNumeroFactura.Text
            Else
                'strNumeroFact = ddlSerie.SelectedItem & txtNumeroFactura.Text.Trim()
                strNumeroFact = txtNumeroFactura.Text.Trim()
            End If

            dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, strNumeroFact, intTipoFactura)
            If intTipoFactura = 7 Or intTipoFactura = 8 Then
                strFacturas = txtNumeroFactura.Text
                dteFechaIng = DPkFechaFactura.Value
                sSerie = ddlSerie.SelectedItem
            End If
            If intTipoFactura <> 7 And intTipoFactura <> 8 Then
                Limpiar()
            End If
            lngFecha = 0
            If intTipoFactura = 2 Or intTipoFactura = 3 Or intTipoFactura = 9 Then
                If dtrAgro2K.IsClosed Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, strNumeroFact, intTipoFactura)
                End If
                While dtrAgro2K.Read
                    ddlSerie.SelectedItem = dtrAgro2K.GetValue(22)
                    txtNumeroCliente.Text = dtrAgro2K.GetValue(21)
                    TextBox17.Text = dtrAgro2K.GetValue(0)
                    lngFecha = dtrAgro2K.GetValue(19)
                    DPkFechaFactura.Value = DefinirFecha(lngFecha)
                    DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                    txtNumeroFactura.Text = dtrAgro2K.GetValue(2)
                    txtVendedor.Text = dtrAgro2K.GetValue(3)
                    txtNombreVendedor.Text = dtrAgro2K.GetValue(4)
                    txtNombreCliente.Text = dtrAgro2K.GetValue(5)
                    txtDireccion.Text = dtrAgro2K.Item("Direccion").ToString

                    dgvDetalle.Rows.Add(Format(dtrAgro2K.GetValue(6), "#,##0.#0"), dtrAgro2K.GetValue(7), dtrAgro2K.GetValue(8), dtrAgro2K.GetValue(9), IIf(dtrAgro2K.GetValue(10) = 0, "No", "Si"), Format(dtrAgro2K.GetValue(11), "#,##0.#0"), Format(dtrAgro2K.GetValue(12), "#,##0.#0"), dtrAgro2K.GetValue(13))

                    txtSubTotal.Text = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                    txtImpVenta.Text = Format(dtrAgro2K.GetValue(15), "#,##0.#0")
                    txtRetencion.Text = Format(dtrAgro2K.GetValue(16), "#,##0.#0")
                    txtTotal.Text = Format(dtrAgro2K.GetValue(17), "#,##0.#0")
                    dblTipoCambio = Format(ConvierteADouble(dtrAgro2K.GetValue(20)), "###0.#0")
                    If dtrAgro2K.GetValue(18) = 1 Then
                        Label16.Visible = True
                    Else
                        Label16.Visible = False
                    End If
                End While
            ElseIf intTipoFactura = 5 Or intTipoFactura = 6 Or intTipoFactura = 10 Then
                If dtrAgro2K.IsClosed Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, strNumeroFact, intTipoFactura)
                End If
                While dtrAgro2K.Read
                    txtNumeroCliente.Text = dtrAgro2K.GetValue(23)
                    TextBox17.Text = dtrAgro2K.GetValue(0)
                    lngFecha = dtrAgro2K.GetValue(21)
                    DPkFechaFactura.Value = DefinirFecha(lngFecha)
                    txtNumeroFactura.Text = dtrAgro2K.GetValue(2)
                    txtDias.Text = dtrAgro2K.GetValue(3)
                    txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                    DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                    txtVendedor.Text = dtrAgro2K.GetValue(4)
                    txtNombreVendedor.Text = dtrAgro2K.GetValue(5)
                    If dtrAgro2K.GetValue(6) = "" Then
                        txtCliente.Text = ""
                    Else
                        txtCliente.Text = dtrAgro2K.GetValue(6)
                    End If
                    txtNombreCliente.Text = dtrAgro2K.GetValue(7)
                    txtDireccion.Text = dtrAgro2K.Item("Direccion").ToString
                    Dim Val1, Val2 As Double

                    dblTipoCambio = SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(22))
                    If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                        Val1 = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(13)) / dblTipoCambio, "#,##0.#0")
                    Else
                        Val1 = Format(dtrAgro2K.GetValue(13), "#,##0.#0")
                    End If
                    If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                        Val2 = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(14)) / dblTipoCambio, "#,##0.#0")
                    Else
                        Val2 = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                    End If

                    dgvDetalle.Rows.Add(Format(dtrAgro2K.GetValue(8), "#,##0.#0"), dtrAgro2K.GetValue(9), dtrAgro2K.GetValue(10), dtrAgro2K.GetValue(11), IIf(dtrAgro2K.GetValue(12) = 0, "No", "Si"), Val1, Val2, dtrAgro2K.GetValue(15))

                    If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                        txtSubTotal.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(16)) / dblTipoCambio, "#,##0.#0")
                        txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(17)) / dblTipoCambio, "#,##0.#0")
                        txtRetencion.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(18)) / dblTipoCambio, "#,##0.#0")
                        txtTotal.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(19)) / dblTipoCambio, "#,##0.#0")
                    Else
                        txtSubTotal.Text = Format(dtrAgro2K.GetValue(16), "#,##0.#0")
                        txtImpVenta.Text = Format(dtrAgro2K.GetValue(17), "#,##0.#0")
                        txtRetencion.Text = Format(dtrAgro2K.GetValue(18), "#,##0.#0")
                        txtTotal.Text = Format(dtrAgro2K.GetValue(19), "#,##0.#0")
                    End If
                    If dtrAgro2K.GetValue(20) = 1 Then
                        Label16.Visible = True
                    Else
                        Label16.Visible = False
                    End If
                End While
            ElseIf intTipoFactura = 7 Or intTipoFactura = 8 Then
                inExisteFactura = 0
                While dtrAgro2K.Read
                    inExisteFactura = 1
                    txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                End While
                DPkFechaFactura.Value = dteFechaIng
            End If
            dtrAgro2K.Close()
            If intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 6 Or intTipoFactura = 10 Then
                UbicarVendedor(txtVendedor.Text)
                UbicarCliente(txtCliente.Text)
            End If
            Me.Cursor = Cursors.Default
            If intTipoFactura = 9 Or intTipoFactura = 10 Then
                txtNumeroFactura.Focus()
                txtNumeroFactura.SelectAll()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroFactura.KeyDown, txtDias.KeyDown, txtVence.KeyDown, txtNombreVendedor.KeyDown, txtNombreCliente.KeyDown, txtProducto.KeyDown, txtCantidad.KeyDown, txtPrecio.KeyDown, txtVendedor.KeyDown, txtCliente.KeyDown, ddlRetencion.KeyDown, ddlTipoPrecio.KeyDown, ddlSerie.KeyDown, DPkFechaFactura.KeyDown, txtCliente.KeyDown
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnTotalItem As Decimal = 0
        Dim lnImpuesto As Decimal = 0
        Try
            If e.KeyCode = Keys.Enter Then
                objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                Select Case sender.name.ToString
                    Case "DPkFechaFactura"
                        Select Case intTipoFactura
                            Case 1, 2 : ddlSerie.Focus()
                            Case 3, 4, 5, 6 : ddlSerie.Focus()
                            Case 7, 8 : ddlSerie.Focus()
                        End Select
                    Case "txtNumeroFactura"
                        IndicaDebeBuscar = False
                        Select Case intTipoFactura
                            Case 3, 6 : ExtraerFactura()
                                If txtNumeroFactura.Text = "" Then
                                    MsgBox("El N�mero Ingresado no Pertenece a una Factura Emitida o la Factura est� Anulada.", MsgBoxStyle.Information, "Factura No Emitida o Anulada")
                                    Limpiar()
                                    ddlSerie.Focus()
                                    Exit Sub
                                End If
                            Case 7, 8 : ExtraerFactura()
                                If inExisteFactura = 1 Then
                                    MsgBox("El N�mero Ingresado ya Pertenece a una Factura Emitida o la Factura est� Anulada.", MsgBoxStyle.Information, "Factura Emitida o Anulada")
                                    txtNumeroFactura.Focus()
                                    txtNumeroFactura.Text = ""
                                    Exit Sub
                                End If
                                txtNumeroFactura.Text = strFacturas
                                If intTipoFactura = 7 Then
                                    txtNombreCliente.Focus()
                                ElseIf intTipoFactura = 8 Then
                                    txtDias.Focus()
                                End If
                            Case 9, 10 : ExtraerFactura()
                                If txtNumeroFactura.Text = "" Then
                                    MsgBox("El N�mero Ingresado no Pertenece a una Factura Emitida.", MsgBoxStyle.Information, "Factura No Emitida")
                                    Limpiar()
                                    txtNumeroFactura.Focus()
                                    Exit Sub
                                End If
                        End Select
                    Case "txtDias"
                        If IsNumeric(Trim(txtDias.Text)) = False Then
                            txtDias.Text = "0"
                        End If
                        txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                        txtCliente.Focus()
                    Case "txtVendedor"
                        blnUbicar = False
                        If UbicarVendedor(txtVendedor.Text) = False Then
                            MsgBox("El C�digo de Vendedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtVendedor.Focus()
                            Exit Sub
                        End If
                        If txtCliente.Visible = True Then
                            txtCliente.Focus()
                        Else
                            txtNombreCliente.Focus()
                        End If
                    Case "txtCliente"
                        If intTipoFactura = 4 Or intTipoFactura = 8 Then
                            blnUbicar = False
                            If UbicarCliente(txtCliente.Text) = False Then
                                MsgBox("El C�digo de Cliente no est� registrado o el vendedor no es el asignado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                                If txtVendedor.Text = "" Then
                                    txtVendedor.Focus()
                                Else
                                    txtCliente.Focus()
                                End If
                                Exit Sub
                            End If
                        End If
                        txtCantidad.Focus()
                    Case "txtNombreCliente" : txtDireccion.Focus()
                    Case "ddlSerie"
                        Select Case intTipoFactura
                            Case 3, 6 : txtNumeroFactura.Focus()
                            Case 4 : txtDias.Focus()
                            Case 7, 8 : txtNumeroFactura.Focus()
                            Case Else : txtNombreCliente.Focus()
                        End Select
                    Case "txtProducto"
                        blnUbicar = False
                        If UbicarProducto(txtProducto.Text) = False Then
                            MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtProducto.Focus()
                            Exit Sub
                        End If
                        ddlRetencion.Focus()
                    Case "txtCantidad"
                        If dgvDetalle.Rows.Count > 0 And Trim(txtCantidad.Text) = "0" Then
                            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                                If ValidarFacturasExistente() = 1 Then
                                    MessageBoxEx.Show("Ya Existe el N�mero De Factura:  " & ddlSerie.SelectedItem & txtNumeroFactura.Text, " Validar Numero de Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Exit Sub
                                Else
                                    Guardar()
                                End If
                                Exit Sub
                            End If
                        Else
                            txtProducto.Focus()
                        End If
                    Case "ddlRetencion" : Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%" : ddlTipoPrecio.Focus() : IIf(intFechaCambioFactura > 0, ddlTipoPrecio.SelectedIndex = 2, ddlTipoPrecio.SelectedIndex = 0) : UbicarPrecio()
                    Case "ddlTipoPrecio" : UbicarPrecio() : txtPrecio.Focus()
                    Case "txtPrecio"
                        If objParametro IsNot Nothing Then
                            If objParametro.CantidadItemFactura > 0 Then
                                'If dgvDetalle.Rows.Count >= 16 Then
                                If (dgvDetalle.Rows.Count + 1) > objParametro.CantidadItemFactura Then
                                    ' MsgBox("Cantidad maxima para una factura es 16 Item", MsgBoxStyle.Critical, "Favor validar")
                                    MsgBox("Cantidad maxima para una factura es " & objParametro.CantidadItemFactura.ToString() & " Items ", MsgBoxStyle.Critical, "Favor validar")
                                    DPkFechaFactura.Focus()
                                    Exit Sub
                                End If
                            End If
                        End If
                        lnTotalItem = 0
                        lnImpuesto = 0
                        If SUConversiones.ConvierteADouble(txtCantidad.Text) > 0 Then
                            Dim lnExistencia As Integer = 0
                            lnTotalItem = 0
                            lnImpuesto = 0
                            lnExistencia = 0
                            lnExistencia = RNProduto.ObtieneExistenciaActualProducto(lngRegAgencia, lngRegistro)
                            If objParametro.PermiteFacturarSinExistenciaProducto = 0 Then
                                If SUConversiones.ConvierteADouble(txtCantidad.Text) > lnExistencia Then
                                    MessageBoxEx.Show("La cantidad solicitada del producto es mayor que la existencia actual ", " Validacion de datos para Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    Exit Sub
                                End If
                            End If
                            lnTotalItem = SUConversiones.ConvierteADecimal(txtPrecio.Text) * SUConversiones.ConvierteADecimal(txtCantidad.Text)
                            lnTotalItem = SUFunciones.RedondeoNumero(lnTotalItem, "R")
                            dgvDetalle.Rows.Add(Format(SUConversiones.ConvierteADouble(txtCantidad.Text), "####0.#0"), UCase(strUnidad), UCase(txtProducto.Text), UCase(strDescrip), IIf(intImpuesto = 0, "NO", "SI"), txtPrecio.Text, Format(lnTotalItem, "#,##0.#0"), lngRegistro, Format(SUConversiones.ConvierteADouble(txtPrecioCosto.Text), "#,##0.#0"))
                            txtSubTotal.Text = Format(Format(lnTotalItem, "#,##0.#0") + SUConversiones.ConvierteADouble(txtSubTotal.Text), "#,##0.#0")

                            If intImpuesto = 0 Then
                                txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                            ElseIf intImpuesto = 1 Then
                                lnImpuesto = SUFunciones.RedondeoNumero((lnTotalItem * dblPorcParam), "R")
                                'txtImpVenta.Text = Format(Format((Format((SUConversiones.ConvierteADouble(txtPrecio.Text) * SUConversiones.ConvierteADouble(txtCantidad.Text)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                                txtImpVenta.Text = Format(Format((lnImpuesto), "#,##0.#0") + SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                            End If

                            If ddlRetencion.SelectedIndex = 0 Then
                                txtRetencion.Text = "0.00"
                            ElseIf ddlRetencion.SelectedIndex = 1 Then
                                txtRetencion.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) * dblRetParam, "#,##0.#0")
                            End If

                            txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                            Limpiar2()
                            txtCantidad.Select()
                            txtCantidad.Focus()
                        Else
                            MsgBox("No puede vender un producto con cantidad 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                            txtCantidad.Select()
                            txtCantidad.Focus()
                        End If
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub Guardar()
        Dim objFacturas As SEEncabezadoFacturas
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleFacturas As List(Of SEDetalleFacturas) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Dim objReqCampo As Object = Nothing
        Dim objTotalesFactura As SETotalesFactura = Nothing
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing

        Timer1.Enabled = False
        For intIncr = 0 To 25
            For intIncr2 = 0 To 6
                arrCredito(intIncr, intIncr2) = ""
            Next intIncr2
        Next intIncr
        For intIncr = 0 To 25
            For intIncr2 = 0 To 6
                arrContado(intIncr, intIncr2) = ""
            Next intIncr2
        Next intIncr
        For intIncr = 0 To dgvDetalle.Rows.Count - 1
            If IsNumeric(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value) = False Then
                MsgBox("No es un valor numerico la cantidad a vender", MsgBoxStyle.Critical, "Error de Dato")
                Timer1.Enabled = True
                Exit Sub
            ElseIf SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value) <= 0 Then
                MsgBox("La cantidad a vender debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                Timer1.Enabled = True
                Exit Sub
            End If
        Next intIncr

        If intTipoFactura = ENTipoFactura.FACTURA_CREDITO Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_MANUAL Then
            blnUbicar = False
            If UbicarCliente(txtCliente.Text) = False Then
                MsgBox("El C�digo de Cliente no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                txtCliente.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If
        End If

        Dim frmNew As New actrptViewer

        If UbicarVendedor(txtVendedor.Text) = False Then
            MsgBox("El C�digo de Vendedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
            Timer1.Enabled = True
            Exit Sub
        End If
        If SUConversiones.ConvierteAInt(txtNumeroVendedor.Text) = -1 Then
            MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del vendedor.", MsgBoxStyle.Critical, "Error de Ingreso")
            txtNumeroVendedor.Focus()
            Timer1.Enabled = True
            Exit Sub
        End If
        intResp = 0
        intImprimir = 0
        'If intTipoFactura = 4 Or intTipoFactura = 8 Then
        '    If SUConversiones.ConvierteAInt(txtDias.Text) <= 0 Then
        '        MsgBox("No puede generar una factura sin d�as de cr�dito.", MsgBoxStyle.Critical, "Error en D�as")
        '        txtDias.Focus()
        '        Timer1.Enabled = True
        '        Exit Sub
        '    End If
        '    If txtNombreCliente.Text = "" Then
        '        MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del cliente.", MsgBoxStyle.Critical, "Error de Ingreso")
        '        txtCliente.Focus()
        '        Timer1.Enabled = True
        '        Exit Sub
        '    End If
        'End If

        If intTipoFactura = ENTipoFactura.FACTURA_CREDITO Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_MANUAL Then
            If SUConversiones.ConvierteAInt(txtDias.Text) <= 0 Then
                MsgBox("No puede generar una factura sin d�as de cr�dito.", MsgBoxStyle.Critical, "Error en D�as")
                txtDias.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If
            If txtNombreCliente.Text = "" Then
                MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del cliente.", MsgBoxStyle.Critical, "Error de Ingreso")
                txtCliente.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If
        End If
        objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()
        'If intTipoFactura = 1 Or intTipoFactura = 4 Then        ' Del D�a
        If intTipoFactura = ENTipoFactura.FACTURA_CONTADO Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO Then        ' Del D�a
            If lngRegAgencia = 1 Then
                intResp = MsgBox("�Desea imprimir la factura en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Impresi�n de Factura")
            Else
                intResp = MsgBox("�Desea imprimir la factura en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Factura")
            End If
            If intResp = 2 Then     'Cancelar
                txtCantidad.Focus()
                Timer1.Enabled = True
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            ElseIf intResp = 7 Then 'No
                intImprimir = 1
            End If
            'ElseIf intTipoFactura = 2 Or intTipoFactura = 5 Then    ' Imprimir Factura
        ElseIf intTipoFactura = ENTipoFactura.FACTURA_CREDITO_IMPRESION Or intTipoFactura = ENTipoFactura.FACTURA_CONTADO_IMPRESION Then    ' Imprimir Factura
            intResp = MsgBox("�Desea imprimir la factura en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Factura")
            If intResp <> 6 Then 'Cancelar o No
                txtCantidad.Focus()
                Timer1.Enabled = True
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            End If
            ' ElseIf intTipoFactura = 3 Or intTipoFactura = 6 Then    ' Anular Factura
        ElseIf intTipoFactura = ENTipoFactura.FACTURA_CONTADO_ANULACION Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_ANULACION Then    ' Anular Factura
            intResp = MsgBox("�Desea anular la factura en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Anulaci�n de Factura")
            If intResp <> 6 Then 'No
                txtNumeroFactura.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If
            'ElseIf intTipoFactura = 7 Or intTipoFactura = 8 Then    ' Ingreso Manual
        ElseIf intTipoFactura = ENTipoFactura.FACTURA_CONTADO_MANUAL Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_MANUAL Then    ' Ingreso Manual
            '            txtSerie.Enabled = True
            intResp = MsgBox("�Desea guardar la factura en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Factura")
            If intResp <> 6 Then    'Cancelar o No
                txtCantidad.Focus()
                Timer1.Enabled = True
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            End If
            'ElseIf intTipoFactura = 9 Or intTipoFactura = 10 Then   ' Consultar Factura
        ElseIf intTipoFactura = ENTipoFactura.FACTURA_CONTADO_CONSULTAR Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_CONSULTAR Then   ' Consultar Factura
            intResp = MsgBox("�Desea imprimir la factura en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Factura")
            If intResp <> 6 Then 'Cancelar o No
                txtNumeroFactura.Focus()
                Timer1.Enabled = True
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
                strDireccionClienteFact = txtDireccion.Text
            End If
        End If

        Me.Cursor = Cursors.WaitCursor
        'If intTipoFactura = 1 Or intTipoFactura = 4 Then

        '    txtNumeroFactura.Text = ExtraerNumeroFacturaRapida(lngRegAgencia, intTipoFactura, ddlSerie.SelectedItem)
        '    ActualizarNumeroFactura(lngRegAgencia, intTipoFactura, ddlSerie.SelectedItem)
        'End If
        intIncr = 0
        intIncr2 = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        dblSubtotal = 0
        dblImpuesto = 0
        dblRetencion = 0
        dblTotal = 0
        strFecVencFact = ""
        strVendedor = txtVendedor.Text + " " + txtNombreVendedor.Text
        strNumeroFact = ddlSerie.SelectedItem + txtNumeroFactura.Text
        'If intTipoFactura = 1 Or intTipoFactura = 4 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
        If intTipoFactura = 1 Or intTipoFactura = 4 Then
            intDiaFact = Format(Now, "dd")
            strMesFact = Format(Now, "MMM")
            lngYearFact = Format(Now, "yyyy")
            lngFechaFactura = Format(Now, "yyyyMMdd")
            'ElseIf intTipoFactura <> 1 And intTipoFactura <> 4 Then
        Else
            intDiaFact = Format(DPkFechaFactura.Value, "dd")
            strMesFact = Format(DPkFechaFactura.Value, "MMM")
            lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
            lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")
        End If
        If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
            strClienteFact = txtNombreCliente.Text
            For intIncr = 0 To dgvDetalle.Rows.Count - 1
                For intIncr2 = 0 To 6
                    If intIncr2 = 5 Or intIncr2 = 6 Then
                        'arrContado(intIncr - 1, intIncr2) = "U$ " & MSFlexGrid2.Text   // TipoCambio
                        arrContado(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                    Else
                        arrContado(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                    End If
                Next intIncr2
            Next intIncr
            'intRptImpFactura = 1
            intRptImpFactura = 12
        ElseIf intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
            strCodCliFact = txtCliente.Text
            strClienteFact = txtNombreCliente.Text
            strFecVencFact = txtVence.Text
            strDiasFact = txtDias.Text
            For intIncr = 0 To dgvDetalle.Rows.Count - 1
                For intIncr2 = 0 To 6
                    If intIncr2 = 5 Or intIncr2 = 6 Then
                        'arrCredito(intIncr - 1, intIncr2) = "U$ " & MSFlexGrid2.Text   // TipoCambio
                        arrCredito(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                    Else
                        arrCredito(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                    End If
                Next intIncr2
            Next intIncr
            'intRptImpFactura = 4
            intRptImpFactura = 12
        End If
        If txtNombreCliente.Text = "" Then
            txtNombreCliente.Text = "Contado"
        End If

        objTotalesFactura = CalculaTotalasFactura()
        If objTotalesFactura.Total = 0 Then
            MsgBox("No se pudo obtener los totales de la factura.", MsgBoxStyle.Critical, "Error de Ingreso")
            txtCliente.Focus()
            Timer1.Enabled = True
            Exit Sub

        End If
        dblSubtotal = objTotalesFactura.SubTotal
        dblImpuesto = objTotalesFactura.TotalImpuesto
        dblRetencion = objTotalesFactura.TotalRetencion
        dblTotal = objTotalesFactura.Total

        txtSubTotal.Text = Format(dblSubtotal, "#,##0.#0")
        txtImpVenta.Text = Format(dblImpuesto, "#,##0.#0")
        txtRetencion.Text = Format(dblRetencion, "#,##0.#0")
        txtTotal.Text = Format(dblTotal, "#,##0.#0")

        'dblSubtotal = SUConversiones.ConvierteADouble(txtSubTotal.Text)
        'dblImpuesto = SUConversiones.ConvierteADouble(txtImpVenta.Text)
        'dblRetencion = SUConversiones.ConvierteADouble(txtRetencion.Text)
        'dblTotal = SUConversiones.ConvierteADouble(txtTotal.Text)
        'strValorFactCOR = ""
        'strValorFactCOR = "C$ " & Format((dblTotal * dblTipoCambio), "#,##0.#0")

        strValorFactCOR = String.Empty
        Select Case intTipoFactura
            Case ENTipoFactura.FACTURA_CONTADO,
                ENTipoFactura.FACTURA_CONTADO_IMPRESION,
                ENTipoFactura.FACTURA_CONTADO_ANULACION,
                ENTipoFactura.FACTURA_CONTADO_MANUAL,
                ENTipoFactura.FACTURA_CONTADO_CONSULTAR
                gnMonedaFactura = objParametroEmpresa.MonedaFacturaContado
            Case ENTipoFactura.FACTURA_CREDITO,
                ENTipoFactura.FACTURA_CREDITO_IMPRESION,
                ENTipoFactura.FACTURA_CREDITO_ANULACION,
                ENTipoFactura.FACTURA_CREDITO_MANUAL,
                ENTipoFactura.FACTURA_CREDITO_CONSULTAR
                gnMonedaFactura = objParametroEmpresa.MonedaFacturaCredito
        End Select

        If gnMonedaFactura = 2 Then
            strValorFactCOR = "US$ " & Format((dblTotal), "#,##0.#0")
        Else
            strValorFactCOR = "C$ " & Format((dblTotal * dblTipoCambio), "#,##0.#0")
        End If

        If intTipoFactura <> 3 And intTipoFactura <> 6 And intTipoFactura <> 7 And intTipoFactura <> 8 Then
            If intImprimir = 0 Then
                If intTipoFactura = 9 Or intTipoFactura = 10 Then
                    IndicaImprimir = 1
                    strQuery = "sp_ExtraerFactura " & lngRegAgencia & ",'" & txtNumeroFactura.Text & "'," & 9
                    frmNew.Show()
                    frmNew.WindowState = FormWindowState.Minimized
                    Me.Cursor = Cursors.Default
                    frmNew.Close()
                    Timer1.Enabled = True
                    Exit Sub
                End If
            End If
        End If

        Dim lngNumFecha As Long

        Try
            lngNumFecha = 0
            lngRegistro = 0
            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                lngNumFecha = Format(Now, "yyyyMMdd")
            ElseIf intTipoFactura <> 1 And intTipoFactura <> 4 Then
                lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
            End If

            objFacturas = New SEEncabezadoFacturas()
            objFacturas.registro = 0
            strDireccionClienteFact = txtDireccion.Text

            If IsNumeric((Microsoft.VisualBasic.Left(txtNumeroFactura.Text, 1))) = False Then
                objFacturas.numero = txtNumeroFactura.Text
            Else
                objFacturas.numero = ddlSerie.SelectedItem & txtNumeroFactura.Text
            End If
            objFacturas.numfechaing = lngNumFecha
            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                objFacturas.fechaingreso = Format(Now, "dd-MMM-yyyy")
            ElseIf intTipoFactura <> 1 And intTipoFactura <> 4 Then
                objFacturas.fechaingreso = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
            End If
            objFacturas.vendregistro = SUConversiones.ConvierteAInt(txtNumeroVendedor.Text)
            If intTipoFactura <> 4 And intTipoFactura <> 8 Then
                objFacturas.cliregistro = lngClienteContado
            ElseIf intTipoFactura = 4 Or intTipoFactura = 8 Then
                objFacturas.cliregistro = SUConversiones.ConvierteAInt(txtNumeroCliente.Text)
            End If

            'If intTipoFactura = ENTipoFactura.FACTURA_CONTADO And intTipoFactura = ENTipoFactura.FACTURA_CONTADO_MANUAL Then
            '    objFacturas.IdMonedaFactura = objParametroEmpresa.MonedaFacturaContado
            'End If
            'If intTipoFactura = ENTipoFactura.FACTURA_CREDITO And intTipoFactura = ENTipoFactura.FACTURA_CREDITO_MANUAL Then
            '    objFacturas.IdMonedaFactura = objParametroEmpresa.MonedaFacturaCredito
            'End If
            objFacturas.IdMonedaFactura = gnMonedaFactura

            objFacturas.clinombre = StrConv(txtNombreCliente.Text, VbStrConv.Uppercase)
            If intFechaCambioFactura > 0 Then
                objFacturas.subtotal = Format((txtSubTotal.Text * dblTipoCambio), "#####0.#0")
            Else
                objFacturas.subtotal = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text), "#####0.#0")
            End If
            If intFechaCambioFactura > 0 Then
                objFacturas.impuesto = Format((txtImpVenta.Text * dblTipoCambio), "#####0.#0")
            Else
                objFacturas.impuesto = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text), "#####0.#0")
            End If
            If intFechaCambioFactura > 0 Then
                objFacturas.retencion = Format((txtRetencion.Text * dblTipoCambio), "#####0.#0")
            Else
                objFacturas.retencion = Format(SUConversiones.ConvierteADouble(txtRetencion.Text), "#####0.#0")
            End If
            If intFechaCambioFactura > 0 Then
                objFacturas.total = Format((txtTotal.Text * dblTipoCambio), "#####0.#0")
            Else
                objFacturas.total = Format(SUConversiones.ConvierteADouble(txtTotal.Text), "#####0.#0")
            End If
            objFacturas.userregistro = lngRegUsuario
            objFacturas.agenregistro = lngRegAgencia
            objFacturas.impreso = intImprimir
            If intTipoFactura <> 4 And intTipoFactura <> 8 Then
                objFacturas.deptregistro = lngDepartContado
            ElseIf intTipoFactura = 4 Or intTipoFactura = 8 Then
                objFacturas.deptregistro = SUConversiones.ConvierteAInt(txtDepartamento.Text)
            End If
            If intTipoFactura <> 4 And intTipoFactura <> 8 Then
                objFacturas.negregistro = lngNegocioContado
            ElseIf intTipoFactura = 4 Or intTipoFactura = 8 Then
                objFacturas.negregistro = SUConversiones.ConvierteAInt(txtNegocio.Text)
            End If
            If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
                objFacturas.tipofactura = 0
            ElseIf intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
                objFacturas.tipofactura = 1
            End If
            objFacturas.diascredito = SUConversiones.ConvierteAInt(txtDias.Text)
            dblTipoCambio = 0
            dblTipoCambio = UbicarTipoCambio(lngNumFecha)
            objFacturas.tipocambio = dblTipoCambio
            If intTipoFactura = 2 Or intTipoFactura = 5 Then
                objFacturas.status = 2
            ElseIf intTipoFactura = 3 Or intTipoFactura = 6 Then
                objFacturas.status = 1
            Else
                objFacturas.status = 0
            End If
            objFacturas.Direccion = txtDireccion.Text

            lstDetalleFacturas = CargarDetalleFacturas()

            If (objFacturas Is Nothing) And (lstDetalleFacturas Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Factura", " Guardar Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Timer1.Enabled = True
                Return
            End If

            RNFacturas.IngresaTransaccionFacturas(objFacturas, lstDetalleFacturas, intTipoFactura)
            'MessageBoxEx.Show("Factura Guardada exitosamente. ", " Guardar Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Limpiar()

            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                If sSerie.ToString <> "" Then
                    'ActualizarNumeroFactura(lngRegAgencia, intTipoFactura, sSerie)
                    ActualizarNumeroFacturaRapida(lngRegAgencia, intTipoFactura, sSerie)
                End If
            End If
            MessageBoxEx.Show("Factura Guardada exitosamente. ", " Guardar Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            If intTipoFactura <> 3 And intTipoFactura <> 6 And intTipoFactura <> 7 And intTipoFactura <> 8 Then
                If intImprimir = 0 Then
                    IndicaImprimir = 1
                    strQuery = "sp_ExtraerFactura " & lngRegAgencia & ",'" & ddlSerie.SelectedItem & txtNumeroFactura.Text & "'," & 9
                    If intTipoFactura = 4 Then
                        UbicarCliente(txtCliente.Text)
                        strQuery = "sp_ExtraerFactura " & lngRegAgencia & ",'" & ddlSerie.SelectedItem & txtNumeroFactura.Text & "'," & 10
                    End If
                    frmNew.Show()
                    If intImpresoraAsignada = 0 Then
                        frmNew.WindowState = FormWindowState.Minimized
                        frmNew.Close()
                    End If
                End If
            End If
            Timer1.Enabled = True
            Limpiar()
            Limpiar2()
            If intTipoFactura = 1 Then
                txtNombreCliente.Focus()
            ElseIf intTipoFactura = 4 Then
                txtCliente.Focus()
            End If
            If intTipoFactura = 2 Or intTipoFactura = 5 Then
                UbicarPendientes()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Timer1.Enabled = True
            MessageBoxEx.Show("Error al Guardar la Factura: " & ex.Message, " Guardar Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Cursor = Cursors.Default

    End Sub

    Private Function CargarDetalleFacturas() As List(Of SEDetalleFacturas)
        Dim lstDetalleFact As List(Of SEDetalleFacturas) = Nothing
        Dim objDetalleFact As SEDetalleFacturas
        Dim i As Integer

        objDetalleFact = New SEDetalleFacturas()
        lstDetalleFact = New List(Of SEDetalleFacturas)
        Try
            If dgvDetalle.Rows.Count > 0 Then
                For i = 0 To dgvDetalle.Rows.Count - 1
                    objDetalleFact.prodregistro = dgvDetalle.Rows(i).Cells("IdProducto").Value
                    objDetalleFact.cantidad = dgvDetalle.Rows(i).Cells("Cantidad").Value
                    objDetalleFact.PrecioCosto = dgvDetalle.Rows(i).Cells("PrecioCosto").Value
                    objDetalleFact.precio = dgvDetalle.Rows(i).Cells("Valor").Value
                    objDetalleFact.valor = dgvDetalle.Rows(i).Cells("Total").Value
                    'objDetalleFact.Igv = dgvDetalle.Rows(i).Cells("Igv").Value
                    lstDetalleFact.Add(New SEDetalleFacturas(objDetalleFact))
                Next
            End If
            Return lstDetalleFact
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Function

    Sub Limpiar()

        Try
            DPkFechaFactura.Value = Now
            intIncr = 0
            For intIncr = 1 To 6
                txtCollection.Item(intIncr).Text = ""
            Next intIncr
            intIncr = 0
            For intIncr = 7 To 12
                txtCollection.Item(intIncr).Text = "0.00"
            Next intIncr
            For intIncr = 13 To txtCollection.Count
                txtCollection.Item(intIncr).Text = "0"
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            txtDias.Text = "30"
            txtVendedor.Text = ""
            txtCliente.Text = ""
            txtDireccion.Text = ""
            txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
            ddlRetencion.SelectedIndex = 0
            ddlSerie.SelectedIndex = -1
            If intFechaCambioFactura > 0 Then
                ddlTipoPrecio.SelectedIndex = 2
            Else
                ddlTipoPrecio.SelectedIndex = 0
            End If
            DPkFechaFactura.Focus()
            Select Case intTipoFactura
                Case 1, 2, 3, 7, 9 : Label3.Visible = False : Label4.Visible = False : txtDias.Visible = False : txtVence.Visible = False : txtNombreCliente.Focus()
                    txtCliente.Visible = False : txtNombreCliente.ReadOnly = False : txtDireccion.ReadOnly = False : txtNombreCliente.Width = 280 : txtNombreCliente.Location = New Point(56, 48)
                Case 4, 5, 6, 8, 10 : Label3.Visible = True : Label4.Visible = True : txtDias.Visible = True : txtVence.Visible = True : txtCliente.Focus()
            End Select
            Select Case intTipoFactura
                Case 1, 4 'ObtieneSeriexAgencia() ': txtNumeroFactura.Text = ExtraerNumeroFacturaRapida(lngRegAgencia, intTipoFactura, ddlSerie.SelectedItem)
                Case 2, 3, 5, 6 : BloquearCampos()
                Case 7, 8 : txtNumeroFactura.ReadOnly = False
                Case 9, 10 : BloquearCampos() : txtNumeroFactura.ReadOnly = False
            End Select

            dgvDetalle.Rows.Clear()
            DPkFechaFactura.Focus()
            ObtieneSeriexAgencia()
            ObtieneVendedorPorDefecto()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub Limpiar2()

        txtProducto.Text = ""
        txtCantidad.Text = "0"
        txtPrecio.Text = "0.00"
        intRptImpFactura = 0

    End Sub

    Sub BloquearCampos()

        Try
            For intIncr = 1 To txtCollection.Count - 1
                txtCollection.Item(intIncr).readonly = True
            Next
            DPkFechaFactura.Enabled = False
            ddlRetencion.Enabled = False
            ddlTipoPrecio.Enabled = False
            txtVendedor.ReadOnly = True
            txtCliente.ReadOnly = True
            If intTipoFactura = 3 Or intTipoFactura = 6 Then
                txtNumeroFactura.ReadOnly = False
                'txtSerie.ReadOnly = False
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub Anular()
        Dim objFacturas As SEEncabezadoFacturas
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleFacturas As List(Of SEDetalleFacturas) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Dim objReqCampo As Object = Nothing

        If Label16.Visible = True Then
            MsgBox("No puede anular una factura anulada.  Veriique.", MsgBoxStyle.Critical, "Factura Anulada")
            Exit Sub
        End If
        If Year(DPkFechaFactura.Value) = Year(Now) And Month(DPkFechaFactura.Value) = Month(Now) Then
        Else
            MsgBox("La factura a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Factura Inv�lida")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor

        Dim lngNumFecha As Long
        Try
            lngNumFecha = 0
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")

            objFacturas = New SEEncabezadoFacturas()
            objFacturas.registro = 0
            If IsNumeric((Microsoft.VisualBasic.Left(txtNumeroFactura.Text, 1))) = False Then
                objFacturas.numero = txtNumeroFactura.Text
            Else
                objFacturas.numero = ddlSerie.SelectedItem & txtNumeroFactura.Text
            End If

            objFacturas.numfechaing = lngNumFecha
            objFacturas.fechaingreso = ""

            objFacturas.vendregistro = 0
            If intTipoFactura = 3 Then
                objFacturas.cliregistro = lngClienteContado
            ElseIf intTipoFactura = 6 Then
                objFacturas.cliregistro = SUConversiones.ConvierteAInt(txtNumeroCliente.Text)
            End If
            objFacturas.clinombre = ""
            objFacturas.subtotal = 0
            objFacturas.impuesto = 0
            objFacturas.retencion = 0
            If intFechaCambioFactura > 0 Then
                objFacturas.total = Format((txtTotal.Text * dblTipoCambio), "#####0.#0")
            Else
                objFacturas.total = Format(SUConversiones.ConvierteADouble(txtTotal.Text), "#####0.#0")
            End If
            objFacturas.userregistro = 0
            objFacturas.agenregistro = lngRegAgencia
            objFacturas.impreso = 0
            objFacturas.deptregistro = 0
            objFacturas.negregistro = 0
            If intTipoFactura = 3 Then
                objFacturas.tipofactura = 0
            ElseIf intTipoFactura = 6 Then
                objFacturas.tipofactura = 1
            End If

            objFacturas.diascredito = 0
            objFacturas.tipocambio = 0
            objFacturas.status = 1
            objFacturas.Direccion = 1

            lstDetalleFacturas = CargarDetalleFacturas()

            If (objFacturas Is Nothing) And (lstDetalleFacturas Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Factura", " Guardar Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            RNFacturas.IngresaTransaccionFacturas(objFacturas, lstDetalleFacturas, intTipoFactura)
            MessageBoxEx.Show("Factura Anulada Exitosamente. ", " Anular Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("La Factura no se pudo Anular " & ex.Message, " Anular Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Function UbicarProducto(ByVal strDato As String) As Boolean
        Try
            strDescrip = ""
            strUnidad = ""
            lngRegistro = 0
            intImpuesto = 0
            strQuery = ""
            strQuery = "Select P.Descripcion, U.Codigo, P.Registro, P.Impuesto from prm_Productos P, prm_Unidades U "
            strQuery = strQuery + "Where P.Codigo = '" & strDato & "' AND P.RegUnidad = U.Registro AND P.Estado = 0"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                strDescrip = dtrAgro2K.GetValue(0)
                strUnidad = dtrAgro2K.GetValue(1)
                lngRegistro = dtrAgro2K.GetValue(2)
                intImpuesto = dtrAgro2K.GetValue(3)
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Function UbicarVendedor(ByVal strDato As String) As Boolean

        Try
            txtNumeroVendedor.Text = "-1"
            strQuery = ""
            strQuery = "Select Nombre, Registro from prm_Vendedores Where Codigo = '" & txtVendedor.Text.Trim() & "' "
            If intTipoFactura = 9 Or intTipoFactura = 10 Then
                strQuery = strQuery + "And registro <> -1"
            Else
                strQuery = strQuery + "And Estado = 0 And registro <> -1"
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            UbicarVendedor = False
            While dtrAgro2K.Read
                txtNombreVendedor.Text = dtrAgro2K.GetValue(0)
                txtNumeroVendedor.Text = dtrAgro2K.GetValue(1)
                UbicarVendedor = True
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Function UbicarCliente(ByVal strDato As String) As Boolean
        Try
            strDirecFact = ""
            txtNumeroCliente.Text = "0"
            strQuery = ""
            strQuery = "Select C.nombre, C.registro, C.deptregistro, C.negregistro, C.vendregistro, "
            strQuery = strQuery + "C.direccion, C.dias_credito from prm_Clientes C Where C.Codigo = "
            strQuery = strQuery + "'" & txtCliente.Text & "' AND C.Estado = 0"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtNombreCliente.Text = dtrAgro2K.GetValue(0)
                txtNumeroCliente.Text = dtrAgro2K.GetValue(1)
                txtDepartamento.Text = dtrAgro2K.GetValue(2)
                txtNegocio.Text = dtrAgro2K.GetValue(3)
                txtDireccion.Text = dtrAgro2K.GetValue(5).ToString()
                txtDias.Text = dtrAgro2K.GetValue(6)
                txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                strDirecFact = dtrAgro2K.GetValue(5)
                If intTipoFactura = 4 Or intTipoFactura = 8 Then
                    If dtrAgro2K.GetValue(4) <> SUConversiones.ConvierteAInt(txtNumeroVendedor.Text) Then
                        UbicarCliente = False
                        txtVendedor.Text = ""
                        txtNombreVendedor.Text = ""
                        Exit Function
                    End If
                End If
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If txtNumeroCliente.Text = "0" Then
                UbicarCliente = False
                Exit Function
            End If
            UbicarCliente = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Sub UbicarPrecio()
        Try
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.UbicarPrecio(lngRegistro, ddlTipoPrecio.SelectedIndex, intTipoFactura, lngRegAgencia)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    txtPrecio.Text = dtrAgro2K.Item("Precio")
                    txtPrecioCosto.Text = dtrAgro2K.Item("PrecioCosto")
                End While
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            dtrAgro2K = Nothing

            'strQuery = ""
            'Select Case ddlTipoPrecio.SelectedIndex
            '    Case 0 : strQuery = "Select (case when P.Pvpc=0 then P.Pvpu else P.Pvpc end),dbo.fnObtienePrecioCosto(P.registro) PrecioCosto from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 1 : strQuery = "Select (case when P.Pvdc=0 then P.Pvdu else P.Pvdc end),dbo.fnObtienePrecioCosto(P.registro) PrecioCosto from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 2 : strQuery = "Select dbo.fnObtienePrecioCosto(P.registro),dbo.fnObtienePrecioCosto(P.registro) from prm_Productos P Where P.Registro = " & lngRegistro & ""
            'End Select
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            'While dtrAgro2K.Read
            '    txtPrecio.Text = dtrAgro2K.GetValue(0)
            '    txtPrecioCosto.Text = dtrAgro2K.GetValue(1)
            'End While
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ObtengoDatosListadoAyuda()

        Try
            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 1 : txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                            If txtCliente.Visible = True Then
                                txtCliente.Focus()
                            Else
                                txtNombreCliente.Focus()
                            End If
                        Case 2 : txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text)
                            If txtVendedor.Text = "" Then
                                MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                                txtVendedor.Focus()
                            End If
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : ddlRetencion.Focus()
                        Case 4 : txtNumeroFactura.Text = strUbicar : ExtraerFactura()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)

            sSerie = ddlSerie.SelectedItem
            If sSerie Is Nothing Then
                sSerie = String.Empty
            End If

            'Dar formata al numero de factura cuando es de contado
            'If intTipoFactura = 1 Or intTipoFactura = 4 Then
            If intTipoFactura = ENTipoFactura.FACTURA_CONTADO Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO Then
                UbicarAgencia(lngRegUsuario)
                'txtNumeroFactura.Text = ExtraerNumeroFacturaRapida(lngRegAgencia, intTipoFactura, sSerie)
                txtNumeroFactura.Text = ExtraerNumeroFacturaRapida(lngRegAgencia, intTipoFactura, sSerie)
                'If sSerie.ToString <> "" Then
                '    ActualizarNumeroFactura(lngRegAgencia, intTipoFactura, sSerie)
                'End If
                If inExisteFactura = 1 Then
                    Exit Sub
                End If
                If intTipoFactura = 1 Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 7)
                ElseIf intTipoFactura = 4 Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 8)
                End If
                inExisteFactura = 0
                While dtrAgro2K.Read
                    inExisteFactura = 1
                    'txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                End While
                If inExisteFactura = 1 Then
                    MessageBoxEx.Show("Ya Existe el N�mero De Factura:  " & ddlSerie.SelectedItem & txtNumeroFactura.Text, " Validar Numero de Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'txtNumeroFactura.Focus()
                    'txtNumeroFactura.Text = ""
                End If
                dtrAgro2K.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRetencion.SelectedIndexChanged, ddlTipoPrecio.SelectedIndexChanged
        Try
            If sender.name = "ddlRentencion" Then
                Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%"
                If sender.selectedindex = 0 Then
                    txtRetencion.Text = "0.00"
                    txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                ElseIf sender.selectedindex = 1 Then
                    If intTipoFactura = 4 Or intTipoFactura = 8 Then
                        sender.selectedindex = 0
                    Else
                        txtRetencion.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) * dblRetParam, "#,##0.#0")
                        txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                    End If
                End If
            ElseIf sender.name = "ComboBox2" And txtProducto.Text <> "" Then
                UbicarPrecio()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombreCliente.LostFocus

        txtNombreCliente.Text = StrConv(txtNombreCliente.Text, VbStrConv.ProperCase)

    End Sub

    Sub UbicarPendientes()

        Me.Cursor = Cursors.WaitCursor

        Try
            dtrAgro2K = RNFacturas.UbicarPendientes(lngRegAgencia, intTipoFactura)

            While dtrAgro2K.Read
                txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                Exit While
            End While
            dtrAgro2K.Close()
            ExtraerFactura()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DPkFechaFactura.LostFocus
        Try
            If IndicaImprimir = 0 Then
                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If
                cnnAgro2K.Open()
                cmdAgro2K.Connection = cnnAgro2K

                strQuery = ""
                strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(DPkFechaFactura.Value, "yyyyMMdd") & " "
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    dblTipoCambio = dtrAgro2K.GetValue(0)
                End While
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox18_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.DoubleClick, txtProducto.DoubleClick
        Try
            intListadoAyuda = 0
            If MenuItem5.Visible = True Then
                intListadoAyuda = 1
            ElseIf MenuItem6.Visible = True Then
                intListadoAyuda = 2
            ElseIf MenuItem7.Visible = True Then
                intListadoAyuda = 3
            End If
            If intListadoAyuda <> 0 Then
                Dim frmNew As New frmListadoAyuda
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click

        If intTipoFactura = 2 Or intTipoFactura = 5 Then
            UbicarPendientes()
        End If

    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged

        Try
            sSerie = ddlSerie.SelectedItem
            If sSerie Is Nothing Then
                sSerie = String.Empty
            End If

            If intTipoFactura = 1 Or intTipoFactura = 4 Then

                txtNumeroFactura.Text = ExtraerNumeroFacturaRapida(lngRegAgencia, intTipoFactura, sSerie)
                'If sSerie.ToString <> "" Then
                '    ActualizarNumeroFactura(lngRegAgencia, intTipoFactura, sSerie)
                'End If
                If inExisteFactura = 1 Then
                    Exit Sub
                End If
                If intTipoFactura = 1 Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 7)
                ElseIf intTipoFactura = 4 Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 8)
                End If
                'dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, ddlSerie.SelectedItem & txtNumeroFactura.Text, 8)
                inExisteFactura = 0
                While dtrAgro2K.Read
                    inExisteFactura = 1
                    'txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                End While
                If inExisteFactura = 1 Then
                    MessageBoxEx.Show("Ya Existe el N�mero De Factura:  " & ddlSerie.SelectedItem & txtNumeroFactura.Text, " Validar Numero de Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'txtNumeroFactura.Focus()
                    'txtNumeroFactura.Text = ""
                End If
                dtrAgro2K.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim strQ As String = ""
        Dim ValidoAnul, IdC As Integer
        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)

            If intTipoFactura = 3 Or intTipoFactura = 6 Then
                If dgvDetalle.Rows.Count <= 0 Then
                    MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                    Exit Sub
                End If
                intResp = MsgBox("�Est� Seguro de Anular esta Factura?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Anulaci�n")
                If intResp = 6 Then
                    IdC = SUConversiones.ConvierteAInt(txtNumeroCliente.Text)
                    strQ = "EXEC spValidarVentaSinRecibo '" & txtNumeroFactura.Text.Trim() & "', " & IdC
                    If cnnAgro2K.State = ConnectionState.Open Then
                        cnnAgro2K.Close()
                    End If

                    cnnAgro2K.Open()
                    Try
                        cmdAgro2K.CommandText = strQ
                        dtrAgro2K = cmdAgro2K.ExecuteReader()
                        While dtrAgro2K.Read
                            ValidoAnul = dtrAgro2K.Item(0)
                        End While
                        dtrAgro2K.Close()

                        If ValidoAnul = 0 Then
                            Anular()
                            ddlSerie.Focus()
                        Else
                            MsgBox("Factura no Puede se Anulada, Hay Recibos Emitidos..", MsgBoxStyle.Critical, "Anulaci�n de Compra")
                        End If
                    Catch exc As Exception
                        intError = 1
                        MsgBox(exc.Message.ToString)
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End Try
                End If
            Else
                If dgvDetalle.Rows.Count <= 0 Then
                    MsgBox("No hay productos a facturar", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                If ValidarFacturasExistente() = 1 Then
                    MessageBoxEx.Show("Ya Existe el N�mero De Factura:  " & ddlSerie.SelectedItem & txtNumeroFactura.Text, " Validar Numero de Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                Else
                    Guardar()
                    If intTipoFactura = 1 Then
                        txtNombreCliente.Focus()
                    ElseIf intTipoFactura = 4 Then
                        txtCliente.Focus()
                    End If
                    If intFechaCambioFactura > 0 Then
                        ddlTipoPrecio.SelectedIndex = 2
                    Else
                        ddlTipoPrecio.SelectedIndex = 0
                    End If
                End If

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Limpiar()
    End Sub

    Private Sub cmdVendedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdVendedor.Click
        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            intListadoAyuda = 1
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdClientes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClientes.Click

        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            intListadoAyuda = 2
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdProductos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProductos.Click

        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            intListadoAyuda = 3
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub CambiaIcoImpresion(ByVal Boton As DevComponents.DotNetBar.ButtonItem)
        Try
            Boton.Tooltip = "Imprimir"
            Boton.Text = "Imprimir<F7>"
            Boton.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
            Boton.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
            Boton.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
            Boton.ImageFixedSize = New System.Drawing.Size(40, 40)
            Boton.Image = ObtieneImagen("multifunction_printer_icon.png")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdRefrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefrescar.Click
        If intTipoFactura = 2 Or intTipoFactura = 5 Then
            UbicarPendientes()
        End If
    End Sub

    Private Sub cmdiSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Function ObtieneImagen(ByVal NombreImagen As String) As System.Drawing.Image
        Dim imagen As System.Drawing.Image
        Try
            'Dim file_name As String = Application.
            Dim NombreCarpeta As String
            Dim NombreArchivo As String = String.Empty
            NombreCarpeta = System.IO.Directory.GetCurrentDirectory()
            If NombreCarpeta.EndsWith("\bin\Debug") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin\Debug", "")
            End If
            If NombreCarpeta.EndsWith("\bin") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin", "")
            End If
            NombreCarpeta = NombreCarpeta & "\Imagenes\"
            NombreArchivo = NombreCarpeta & NombreImagen

            'imagen = Image.FromFile(NombreArchivo)
            imagen = System.Drawing.Image.FromFile(NombreArchivo)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Return Nothing
        End Try
        Return imagen
    End Function

    Private Sub cmdiLimpiar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub txtDireccion_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDireccion.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtCantidad.Focus()
        End If
    End Sub

    Sub EliminarRegistroGrid()
        'Dim strCodigo As String
        'Dim strPrecio As String
        'Dim strCantidad As String
        Dim dblCantidad As Double
        'Dim dblPrecio As Double
        'Dim dblValor As Double
        'Dim dblImpuesto As Double

        Try
            If dgvDetalle.Rows.Count > 0 Then
                intResp = MsgBox("�Est� Seguro de Eliminar el Registro?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Eliminaci�n de Registros")

                If intResp = 6 Then

                    dblCantidad = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value)
                    txtSubTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) - (dblCantidad * SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value)), "#,##0.#0")
                    If dgvDetalle.CurrentRow.Cells("Impuesto").Value = "Si" Then
                        txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text) - Format((SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value) * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    End If
                    txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)

                End If
            End If
            txtCantidad.Focus()
            txtCantidad.SelectAll()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        Dim i As Integer
        Dim TotalAnterior, ImpuestoAnterior As Double
        Dim PventaNvo, PCosto As Double
        Dim Val, Cant As Double
        Dim lnExistenciaActual As Integer = 0
        Dim lnIdProducto As Integer = 0
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing
        Dim frmLogin2 As frmLoginSuprvisor
        Dim lnIdMoneda As Integer = 0
        Dim PMinimoFactura As Double = 0
        Dim lCodigoProducto As String = String.Empty
        Dim lbFacturaAMenosPrecioMinimo As Boolean = False
        Dim lbFacturaAMenosPrecioCosto As Boolean = False
        Try
            objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            '        If dgvDetalle.CurrentCell.ColumnIndex = 0 Then
            If dgvDetalle.Columns.IndexOf(dgvDetalle.Columns("Cantidad")) = e.ColumnIndex Then
                If objParametroEmpresa.PermiteFacturarSinExistenciaProducto = 0 Then
                    lnExistenciaActual = RNProduto.ObtieneExistenciaActualProducto(lngRegAgencia, lnIdProducto)
                    lnIdProducto = SUConversiones.ConvierteADecimal(dgvDetalle.CurrentRow.Cells("IdProducto").Value)
                    If SUConversiones.ConvierteADouble(SUConversiones.ConvierteADecimal(dgvDetalle.CurrentRow.Cells("Cantidad").Value)) > lnExistenciaActual Then
                        MessageBoxEx.Show("La cantidad solicitada del producto es mayor que la existencia actual ", " Validacion de datos para Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        dgvDetalle.CurrentRow.Cells("Cantidad").Value = Format(gnCantidadAnterior, "#,##0.#0")
                        Exit Sub
                    End If
                End If
                dgvDetalle.CurrentRow.Cells("Total").Value = Format((dgvDetalle.CurrentRow.Cells("Cantidad").Value * dgvDetalle.CurrentRow.Cells("Valor").Value), "#,##0.#0")
                TotalAnterior = 0
                txtSubTotal.Text = 0
                ImpuestoAnterior = 0
                txtImpVenta.Text = 0
                For i = 0 To dgvDetalle.Rows.Count - 1
                    TotalAnterior = ConvierteADouble(txtSubTotal.Text)
                    txtSubTotal.Text = Format(Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") + TotalAnterior, "#,##0.#0")
                    If intImpuesto = 0 Then
                        txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                    ElseIf intImpuesto = 1 Then
                        ImpuestoAnterior = ConvierteADouble(txtImpVenta.Text)
                        txtImpVenta.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ImpuestoAnterior, "#,##0.#0")
                    End If

                    txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")

                Next
                Cant = dgvDetalle.CurrentRow.Cells("Cantidad").Value
                dgvDetalle.CurrentRow.Cells("Cantidad").Value = Format(Cant, "#,##0.#0")
                Limpiar2()
                'ElseIf dgvDetalle.CurrentCell.ColumnIndex = 5 Then
            ElseIf dgvDetalle.Columns.IndexOf(dgvDetalle.Columns("Valor")) = e.ColumnIndex Then
                PventaNvo = dgvDetalle.CurrentRow.Cells("Valor").Value
                PCosto = dgvDetalle.CurrentRow.Cells("PrecioCosto").Value
                lCodigoProducto = String.Empty
                lCodigoProducto = dgvDetalle.CurrentRow.Cells("Codigo").Value
                lnIdMoneda = 1
                If intTipoFactura = ENTipoFactura.FACTURA_CREDITO Or intTipoFactura = ENTipoFactura.FACTURA_CREDITO_MANUAL Then
                    lnIdMoneda = objParametroEmpresa.MonedaFacturaCredito
                End If
                If intTipoFactura = ENTipoFactura.FACTURA_CONTADO Or intTipoFactura = ENTipoFactura.FACTURA_CONTADO_MANUAL Then
                    lnIdMoneda = objParametroEmpresa.MonedaFacturaContado
                End If
                PMinimoFactura = RNProduto.ObtienePrecionMinimoxMoneda(lCodigoProducto, lnIdMoneda)
                PCosto = RNProduto.ObtienePrecioCostoxMoneda(lCodigoProducto, lnIdMoneda)
                If PMinimoFactura <= 0 Then
                    MsgBox("Este producto no tiene configurado precio m�nimo de venta, favor configurar el precio m�nimo de venta para facturar.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Modificaci�n de Precio")
                    dgvDetalle.CurrentRow.Cells("Valor").Value = Format(valorAnt, "#,##0.#0")
                    Exit Sub
                End If
                If PCosto <= 0 Then
                    MsgBox("Este producto no tiene configurado precio de costo, favor configurar el precio m�nimo de venta para facturar.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Modificaci�n de Precio")
                    dgvDetalle.CurrentRow.Cells("Valor").Value = Format(valorAnt, "#,##0.#0")
                    Exit Sub
                End If
                Dim lsMensajePrecio As String = String.Empty
                lbFacturaAMenosPrecioMinimo = False

                If PventaNvo < PMinimoFactura Then
                    lbFacturaAMenosPrecioMinimo = True
                    lsMensajePrecio = "�Usted esta facturando a bajo del precio m�nimo a facturar, Desea Continuar?"
                End If
                lbFacturaAMenosPrecioCosto = False
                If PventaNvo < PCosto Then
                    lbFacturaAMenosPrecioCosto = True
                    lsMensajePrecio = "�Usted esta facturando a bajo del precio costo, Desea Continuar?"
                End If

                'If PCosto = PMinimoFactura And (lbFacturaAMenosPrecioMinimo Or lbFacturaAMenosPrecioCosto) Then
                If (lbFacturaAMenosPrecioMinimo And lbFacturaAMenosPrecioCosto) Then
                    lbFacturaAMenosPrecioMinimo = False
                    lbFacturaAMenosPrecioCosto = True
                    lsMensajePrecio = "�Usted esta facturando a bajo del precio costo, Desea Continuar?"
                End If

                ' If PventaNvo < PMinimoFactura Then
                If lbFacturaAMenosPrecioMinimo Or lbFacturaAMenosPrecioCosto Then

                    If lbFacturaAMenosPrecioMinimo Then
                        intResp = MsgBox(lsMensajePrecio, MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Modificaci�n de Precio")
                        lsMensajePrecio = "Usted esta facturando a bajo del precio m�nimo a facturar, no puede proceder"
                    End If

                    If lbFacturaAMenosPrecioCosto Then
                        intResp = MsgBox(lsMensajePrecio, MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Modificaci�n de Precio")
                        lsMensajePrecio = "Usted esta facturando a bajo del precio costo, no puede proceder"
                    End If

                    If intResp <> 6 Then
                        dgvDetalle.CurrentRow.Cells("Valor").Value = Format(valorAnt, "#,##0.#0")
                        Exit Sub
                    Else

                        If RNFacturas.ValidaPermisoFacturarBajoCosto(lngRegUsuario) = 0 Then
                            If objParametroEmpresa IsNot Nothing Then
                                Select Case objParametroEmpresa.PermiteFacturarBajoCosto.ToUpper
                                    Case "N"
                                        MsgBox(lsMensajePrecio, MsgBoxStyle.Critical)
                                        dgvDetalle.CurrentRow.Cells("Valor").Value = Format(valorAnt, "#,##0.#0")
                                        Exit Sub
                                    Case "S"
                                        frmLogin2 = New frmLoginSuprvisor()
                                        ' frmLogin2.MdiParent = Me
                                        'frmLogin2.Show()
                                        frmLogin2.ShowDialog()
                                        If RNFacturas.ValidaPermisoFacturarBajoCosto(lngRegUsuarioRelogin) = 0 Then
                                            MsgBox(lsMensajePrecio, MsgBoxStyle.Critical)
                                            dgvDetalle.CurrentRow.Cells("Valor").Value = Format(valorAnt, "#,##0.#0")
                                            Exit Sub
                                        End If
                                End Select
                            End If

                        End If
                        dgvDetalle.CurrentRow.Cells("Total").Value = Format((dgvDetalle.CurrentRow.Cells("Cantidad").Value * dgvDetalle.CurrentRow.Cells("Valor").Value), "#,##0.#0")
                        TotalAnterior = 0
                        txtSubTotal.Text = 0
                        ImpuestoAnterior = 0
                        txtImpVenta.Text = 0
                        For i = 0 To dgvDetalle.Rows.Count - 1
                            TotalAnterior = ConvierteADouble(txtSubTotal.Text)
                            txtSubTotal.Text = Format(Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") + TotalAnterior, "#,##0.#0")
                            If intImpuesto = 0 Then
                                txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                            ElseIf intImpuesto = 1 Then
                                ImpuestoAnterior = ConvierteADouble(txtImpVenta.Text)
                                txtImpVenta.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ImpuestoAnterior, "#,##0.#0")
                            End If
                            txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                        Next
                        Val = dgvDetalle.CurrentRow.Cells("Valor").Value
                        dgvDetalle.CurrentRow.Cells("Valor").Value = Format(Val, "#,##0.#0")
                        Limpiar2()
                    End If
                Else
                    dgvDetalle.CurrentRow.Cells("Total").Value = Format((dgvDetalle.CurrentRow.Cells("Cantidad").Value * dgvDetalle.CurrentRow.Cells("Valor").Value), "#,##0.#0")
                    TotalAnterior = 0
                    txtSubTotal.Text = 0
                    ImpuestoAnterior = 0
                    txtImpVenta.Text = 0
                    For i = 0 To dgvDetalle.Rows.Count - 1
                        TotalAnterior = ConvierteADouble(txtSubTotal.Text)
                        txtSubTotal.Text = Format(Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") + TotalAnterior, "#,##0.#0")
                        If intImpuesto = 0 Then
                            txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                        ElseIf intImpuesto = 1 Then
                            ImpuestoAnterior = ConvierteADouble(txtImpVenta.Text)
                            txtImpVenta.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ImpuestoAnterior, "#,##0.#0")
                        End If

                        txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")

                    Next
                    Val = dgvDetalle.CurrentRow.Cells("Valor").Value
                    dgvDetalle.CurrentRow.Cells("Valor").Value = Format(Val, "#,##0.#0")
                    Limpiar2()
                End If
            End If
            txtCantidad.Focus()
            txtCantidad.SelectAll()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & objError.descripcion, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub dgvDetalle_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDetalle.DoubleClick
        Try
            If intTipoFactura = 1 Or intTipoFactura = 4 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
                EliminarRegistroGrid()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & objError.descripcion, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub dgvDetalle_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvDetalle.CellBeginEdit
        Try
            valorAnt = dgvDetalle.CurrentRow.Cells("Valor").Value
            gnCantidadAnterior = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & objError.descripcion, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Function ValidarFacturasExistente() As Integer
        Try
            sSerie = ddlSerie.SelectedItem
            If sSerie Is Nothing Then
                sSerie = String.Empty
            End If

            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                UbicarAgencia(lngRegUsuario)
                If intTipoFactura = 1 Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 7)
                ElseIf intTipoFactura = 4 Then
                    dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 8)
                End If
                'dtrAgro2K = RNFacturas.ExtraerFactura(lngRegAgencia, sSerie & txtNumeroFactura.Text, 8)
                inExisteFactura = 0
                While dtrAgro2K.Read
                    inExisteFactura = 1
                    'txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                End While
                If inExisteFactura = 1 Then
                    'MessageBoxEx.Show("Ya Existe el N�mero De Factura:  " & ddlSerie.SelectedItem & txtNumeroFactura.Text, " Validar Numero de Facturas ", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'inExisteFactura = 0
                    dtrAgro2K.Close()
                    Return 1
                    Exit Function
                Else
                    Return 0
                End If
                dtrAgro2K.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Private Sub frmFacturasRapida_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated

        If Label18.Text = "Label18" Then
            Exit Sub
        End If

        Try
            Timer1.Enabled = True
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            sSerie = ddlSerie.SelectedItem
            If sSerie Is Nothing Then
                sSerie = String.Empty
            End If

            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                txtNumeroFactura.Text = ExtraerNumeroFacturaRapida(lngRegAgencia, intTipoFactura, sSerie)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdImportProforma_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportProforma.Click
        Try
            ObtenerArchivo()
            CargarComboProformas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub ObtenerArchivo()

        'Dim myStream As Stream
        Dim dlgNew As New OpenFileDialog
        'Dim strFile, strLine, strDrive As String
        Dim strLine, strDrive As String

        Dim lngRegistro As Long
        Dim strFacturas As String
        Dim intFechaIng As Integer
        Dim strFechaIng As String
        Dim strVendCodigo As String
        Dim strCliCodigo As String
        Dim strCliNombre As String
        Dim dblSubTotalCosto As Double
        Dim dblSubTotal As Double
        Dim dblImpuesto As Double
        Dim dblRetencion As Double
        Dim dblTotal As Double
        Dim strUsrCodigo As String
        Dim bytAgenCodigo As Byte
        Dim bytStatus As Byte
        Dim bytImpreso As Byte
        Dim bytTipoFactura As Byte
        Dim intDiasCredito As Integer
        Dim intNumFecAnul As Integer
        Dim strProdCodigo As String
        Dim strProdDescrip As String
        Dim dblCantidad As Double
        Dim dblPrecio As Double
        Dim dblPrecioCosto As Double
        Dim dblValor As Double
        Dim dblIva As Double
        Dim intNumeroArqueo As Integer
        Dim intRegDetalle As Integer
        Dim strArchivo, strArchivo2, strClave, strComando As String
        Dim strDireccion As String
        Dim Columnas As String()

        Dim existe, er As Integer
        Dim strQueryVal As String = ""

        Try
            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MessageBoxEx.Show("No hay archivo que importar. Verifique", "Error en la Importaci�n", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            strDrive = ""
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "Prof_")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MessageBoxEx.Show("No es un archivo que contiene las proformas de una agencia. Verifique.", "Error en Archivo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            cbNumerosProformas.Items.Clear()

            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            strNombAlmacenar = ""
            strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "truncate table tmp_FacturasImp"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            strNombArchivo = ""
            strNombArchivo = strArchivo2
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            intIncr = 0
            intIncr = InStrRev(strNombArchivo, "\")
            strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
            strQuery = ""

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            intRegDetalle = 0
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                Columnas = strLine.Trim().Split("|")
                intIncr = InStr(1, strLine, "|")

                lngRegistro = 0
                lngRegistro = CLng(Columnas(0).Trim())
                strFacturas = ""
                strFacturas = Columnas(1).Trim()
                intFechaIng = 0
                intFechaIng = SUConversiones.ConvierteAInt(Columnas(2).Trim())
                strFechaIng = ""
                strFechaIng = Columnas(3).Trim()
                strVendCodigo = ""
                strVendCodigo = Columnas(4).Trim()
                strCliCodigo = ""
                strCliCodigo = Columnas(5).Trim()
                strCliNombre = ""
                strCliNombre = Columnas(6).Trim()
                dblSubTotalCosto = 0
                dblSubTotalCosto = SUConversiones.ConvierteADouble(Columnas(7).Trim())
                dblSubTotal = 0
                dblSubTotal = SUConversiones.ConvierteADouble(Columnas(8).Trim())
                dblImpuesto = 0
                dblImpuesto = SUConversiones.ConvierteADouble(Columnas(9).Trim())
                dblRetencion = 0
                dblRetencion = SUConversiones.ConvierteADouble(Columnas(10).Trim())
                dblTotal = 0
                dblTotal = SUConversiones.ConvierteADouble(Columnas(11).Trim())
                strUsrCodigo = ""
                strUsrCodigo = Columnas(12).Trim()
                bytAgenCodigo = 0
                bytAgenCodigo = CByte(Columnas(13).Trim())
                bytStatus = 0
                bytStatus = CByte(Columnas(14).Trim())
                bytImpreso = 0
                bytImpreso = CByte(Columnas(15).Trim())
                bytTipoFactura = 0
                bytTipoFactura = CByte(Columnas(16).Trim())
                intDiasCredito = 0
                intDiasCredito = SUConversiones.ConvierteAInt(Columnas(17).Trim())
                intNumFecAnul = 0
                intNumFecAnul = SUConversiones.ConvierteAInt(Columnas(18).Trim())
                strProdCodigo = ""
                strProdCodigo = Columnas(19).Trim()
                strProdDescrip = ""
                strProdDescrip = Columnas(20).Trim()
                dblCantidad = 0
                dblCantidad = SUConversiones.ConvierteADouble(Columnas(21).Trim())
                dblPrecio = 0
                dblPrecio = SUConversiones.ConvierteADouble(Columnas(22).Trim())
                dblPrecioCosto = 0
                dblPrecioCosto = SUConversiones.ConvierteADouble(Columnas(23).Trim())
                dblValor = 0
                dblValor = SUConversiones.ConvierteADouble(Columnas(24).Trim())
                dblIva = 0
                dblIva = SUConversiones.ConvierteADouble(Columnas(25).Trim())
                intNumeroArqueo = 0
                intNumeroArqueo = SUConversiones.ConvierteAInt(Columnas(26).Trim())
                strDireccion = ""
                strDireccion = Columnas(27).Trim()

                intRegDetalle = intRegDetalle + 1

                er = existe
                strQuery = ""
                strQuery = "insert into tmp_facturasimp values (" & lngRegistro & ", '" & strFacturas & "', "
                strQuery = strQuery + "" & intFechaIng & ", '" & strFechaIng & "', '" & strVendCodigo & "', "
                strQuery = strQuery + "'" & strCliCodigo & "', '" & strCliNombre & "', " & dblSubTotalCosto & ", " & dblSubTotal & ", "
                strQuery = strQuery + "" & dblImpuesto & ", " & dblRetencion & ", " & dblTotal & ", "
                strQuery = strQuery + "'" & strUsrCodigo & "', " & bytAgenCodigo & ", " & bytStatus & ", "
                strQuery = strQuery + "" & bytImpreso & ", " & bytTipoFactura & ", " & intDiasCredito & ", "
                strQuery = strQuery + "" & intNumFecAnul & "," & intNumeroArqueo & ", '" & strProdCodigo & "', "
                strQuery = strQuery + "'" & strProdDescrip & "', " & dblCantidad & ", " & dblPrecio & ", " & dblPrecioCosto & ", "
                strQuery = strQuery + "" & dblValor & "," & dblIva & ",'" & strDireccion & "', " & intRegDetalle & ",'Sin Error')"
                Try
                    cmdAgro2K.CommandText = strQuery
                    cmdAgro2K.ExecuteNonQuery()
                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try

            Loop
            sr.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If
            Me.Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub CargarComboProformas()
        Try
            cbNumerosProformas.Items.Clear()
            strQuery = ""
            strQuery = "select distinct numero from tmp_FacturasImp order by Numero"
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    cbNumerosProformas.Items.Add(dtrAgro2K.Item("numero"))
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub CargarDetallePedido()
        ' Dim subT, Im, R, T As Double
        'Dim IdA, fech As Integer
        Dim NomCli As String = String.Empty
        Dim Direc As String = String.Empty

        Try
            dtrAgro2K = RNInventario.ObtieneDetalleProformasAFacturar(cbNumerosProformas.Text)

            While dtrAgro2K.Read
                NomCli = dtrAgro2K.Item("clinombre")
                Direc = dtrAgro2K.Item("Direccion")

                blnUbicar = False
                If txtCliente.Text = "" Then
                    txtCliente.Text = dtrAgro2K.Item("clicodigo")
                Else
                    If txtCliente.Text <> dtrAgro2K.Item("clicodigo") Then
                        MessageBoxEx.Show("El Codigo del Cliente de la Proforma no coincide con el que esta en el encabezado de factura..", "Factura", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If
                End If

                dgvDetalle.Rows.Add(Format(dtrAgro2K.Item("cantidad"), "#,##0.#0"), dtrAgro2K.Item("Unidad"), dtrAgro2K.Item("prodcodigo"),
                                    dtrAgro2K.Item("proddescrip"), IIf(dtrAgro2K.Item("impuesto") > 0, "Si", "No"), Format(dtrAgro2K.Item("precio"), "#,##0.#0"),
                                    Format(dtrAgro2K.Item("valor"), "#,##0.#0"), dtrAgro2K.Item("IdProducto"))

                txtSubTotal.Text = Format(dtrAgro2K.Item("subtotal"), "#,##0.#0")
                txtImpVenta.Text = Format(dtrAgro2K.Item("impuesto"), "#,##0.#0")
                txtRetencion.Text = Format(dtrAgro2K.Item("retencion"), "#,##0.#0")
                txtTotal.Text = Format(dtrAgro2K.Item("total"), "#,##0.#0")

            End While

            If UbicarCliente(txtCliente.Text) = False Then
                MsgBox("El C�digo de Cliente no est� registrado o el vendedor no es el asignado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                txtNombreCliente.Text = NomCli
                txtDireccion.Text = Direc

                If txtVendedor.Text = "" Then
                    txtVendedor.Focus()
                Else
                    txtCliente.Focus()
                End If
                Exit Sub
            End If

            Limpiar2()
            txtCantidad.Select()
            txtCantidad.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cbNumerosProformas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNumerosProformas.SelectedIndexChanged
        Try
            dgvDetalle.Rows.Clear()
            CargarDetallePedido()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try

    End Sub

    Private Sub cmdImprimirProforma_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimirProforma.Click
        Try
            If dgvDetalle.Rows.Count = 0 Then
                MessageBoxEx.Show("No hay Detalle de Proforma a Imprimir..", "Imprimir Proforma", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                If PrintDialogo.ShowDialog = Windows.Forms.DialogResult.OK Then
                    PrintDoc.PrinterSettings = PrintDialogo.PrinterSettings
                    PrintDoc.Print()
                Else
                    imprimir(True)
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try

    End Sub

    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        ' Este evento se produce cada vez que se va a imprimir una p�gina
        Dim lineHeight As Single
        Dim yPos As Single = e.MarginBounds.Top
        'Dim leftMargin As Single = e.MarginBounds.Left
        Dim leftMargin As Single = 45
        Dim printFont As System.Drawing.Font
        Dim sb As System.Text.StringBuilder
        'Dim lvi As ListViewItem
        Dim anchoColumnasImp() As Integer
        'Dim anchoTotal As Integer
        '
        ' Asignar el tipo de letra
        'printFont = New System.Drawing.Font("Courier New", 11)

        Try
            prtFont = New System.Drawing.Font("Courier New", 11)
            printFont = prtFont
            lineHeight = printFont.GetHeight(e.Graphics)
            '
            ' imprimir la cabecera de la p�gina
            yPos += lineHeight
            e.Graphics.DrawString("Proforma: " & cbNumerosProformas.Text, printFont, Brushes.Black, leftMargin, yPos)

            yPos += lineHeight + 5
            e.Graphics.DrawString("Sucursal: " & sNombreAgencia, printFont, Brushes.Black, leftMargin, yPos)

            lineaActual = 0
            ReDim anchoColumnasImp(6)
            ' el n�mero total de caracteres a mostrar
            ' (incluyendo uno de separaci�n)

            anchoColumnasImp(0) = 10
            anchoColumnasImp(1) = 7
            anchoColumnasImp(2) = 7
            anchoColumnasImp(3) = 40
            anchoColumnasImp(4) = 10
            anchoColumnasImp(5) = 15
            anchoColumnasImp(6) = 20

            sb = New System.Text.StringBuilder
            For i As Integer = 0 To 6 'dgvDetalle.ColumnCount - 1
                sb.AppendFormat("{0} ", ajustar(dgvDetalle.Columns(i).HeaderText, anchoColumnasImp(i), HorizontalAlignment.Left))
            Next
            yPos += lineHeight + 20
            e.Graphics.DrawString(sb.ToString, printFont, Brushes.Black, leftMargin, yPos)
            yPos += 5

            ' imprimir cada una de las l�neas de esta p�gina
            Do
                yPos += lineHeight
                sb = New System.Text.StringBuilder

                For j As Integer = 0 To 6 'dgvDetalle.Rows.Count - 1
                    sb.AppendFormat("{0} ", ajustar(dgvDetalle.Rows(lineaActual).Cells(j).Value, anchoColumnasImp(j), HorizontalAlignment.Left))
                Next
                e.Graphics.DrawString(sb.ToString, printFont, Brushes.Black, leftMargin, yPos)
                lineaActual += 1
            Loop Until yPos >= e.MarginBounds.Bottom OrElse lineaActual >= Me.dgvDetalle.Rows.Count
            '
            If lineaActual < Me.dgvDetalle.Rows.Count Then
                e.HasMorePages = True
            Else
                e.HasMorePages = False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try
    End Sub

    Private Sub imprimir(ByVal esPreview As Boolean)
        ' imprimir o mostrar el PrintPreview
        Dim prtDoc As PrintDocument = Nothing
        Dim prtSettings As PrinterSettings = Nothing

        Try
            If prtSettings Is Nothing Then
                prtSettings = New PrinterSettings
            End If

            If prtDoc Is Nothing Then
                prtDoc = New System.Drawing.Printing.PrintDocument
                AddHandler prtDoc.PrintPage, AddressOf PrintDoc_PrintPage
            End If

            ' la l�nea actual
            lineaActual = 0
            prtSettings.DefaultPageSettings.Landscape = True
            prtDoc.DefaultPageSettings.Landscape = True
            ' la configuraci�n a usar en la impresi�n
            prtDoc.PrinterSettings = prtSettings

            If esPreview Then
                Dim prtPrev As New PrintPreviewDialog
                prtPrev.Document = prtDoc

                prtPrev.Text = "Previsualizar documento"
                prtPrev.ShowDialog()
            Else
                prtDoc.Print()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try

    End Sub

    Private Function CalculaTotalasFactura() As SETotalesFactura
        Dim objTotalesFactura As SETotalesFactura = Nothing
        Dim dblSubtotal As Double = 0
        Dim dblTotal As Double = 0
        Dim dblImpuesto As Double = 0
        Dim dblRetencion As Double = 0
        Dim lnCantidad As Double = 0
        Dim intIncr As Integer = 0
        Dim lnImpuesto As Double = 0
        Dim lsIndicaImpuesto As String = String.Empty
        Dim lnPrecioUnitario As Double = 0
        Try

            If dgvDetalle.Rows.Count > 0 Then
                objTotalesFactura = New SETotalesFactura()

                For intIncr = 0 To dgvDetalle.Rows.Count - 1
                    lnImpuesto = 0
                    lsIndicaImpuesto = String.Empty
                    lnCantidad = 0
                    lnPrecioUnitario = 0
                    lnCantidad = lnCantidad + SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value)
                    lnPrecioUnitario = lnPrecioUnitario + SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Valor").Value)
                    dblSubtotal = dblSubtotal + (lnCantidad * lnPrecioUnitario)
                    lsIndicaImpuesto = dgvDetalle.Rows(intIncr).Cells("Impuesto").Value
                    If lsIndicaImpuesto.Trim().ToUpper = "SI" Then
                        lnImpuesto = (lnCantidad * lnPrecioUnitario) * dblPorcParam
                    End If
                    dblImpuesto = dblImpuesto + lnImpuesto
                Next
            End If

            dblRetencion = 0
            If ddlRetencion.SelectedIndex = 1 Then
                dblRetencion = dblSubtotal * dblRetParam
            End If
            ' dblRetencion = SUConversiones.ConvierteADouble(dblSubtotal) * dblRetParam
            dblTotal = dblSubtotal + dblImpuesto - dblRetencion

            objTotalesFactura.SubTotal = dblSubtotal
            objTotalesFactura.TotalImpuesto = dblImpuesto
            objTotalesFactura.TotalRetencion = dblRetencion
            objTotalesFactura.Total = dblTotal
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Timer1.Enabled = True
            objTotalesFactura.SubTotal = 0
            objTotalesFactura.TotalImpuesto = 0
            objTotalesFactura.TotalRetencion = 0
            objTotalesFactura.Total = 0
        End Try
        Return objTotalesFactura
    End Function
    Private Function ajustar(ByVal cadena As String, ByVal ancho As Integer, ByVal alinear As HorizontalAlignment) As String
        Try
            ' devuelve una cadena con el ancho indicado
            If cadena = Nothing OrElse cadena.Length = 0 Then
                Return New String(" "c, ancho)
            End If
            '

            If alinear = HorizontalAlignment.Right Then
                If ancho > cadena.Length Then cadena = New String(" "c, ancho - cadena.Length) & cadena
                Return cadena.Substring(cadena.Length - ancho, ancho)
            Else
                If ancho > cadena.Length Then cadena &= New String(" "c, ancho - cadena.Length)
                Return cadena.Substring(0, ancho)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            Return String.Empty
            'Exit Function
        End Try

    End Function

    Private Sub frmFacturasRapida_VisibleChanged(sender As Object, e As EventArgs) Handles MyBase.VisibleChanged
        If gbIndicaCierre Then
            Me.Close()
        End If
    End Sub

    Private Sub frmFacturasRapida_Deactivate(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Deactivate
        Timer1.Enabled = False
    End Sub

End Class
