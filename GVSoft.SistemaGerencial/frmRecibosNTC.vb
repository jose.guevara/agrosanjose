Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv

Public Class frmRecibosNTC
    Inherits DevComponents.DotNetBar.Office2007Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RichTextBox3 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents CmdAnular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdBuscar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImprimir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents dgvxPagosFacturas As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents lblTipoCambio As Windows.Forms.Label
    Friend WithEvents cbMoneda As ComboBox
    Friend WithEvents Label26 As Windows.Forms.Label
    Friend WithEvents Label11 As Windows.Forms.Label
    Friend WithEvents lblDisponibleUSD As Windows.Forms.Label
    Friend WithEvents Label13 As Windows.Forms.Label
    Friend WithEvents NumFactura As DataGridViewTextBoxColumn
    Friend WithEvents Moneda As DataGridViewTextBoxColumn
    Friend WithEvents FechaEmis As DataGridViewTextBoxColumn
    Friend WithEvents FechaVenc As DataGridViewTextBoxColumn
    Friend WithEvents SaldoFact As DataGridViewTextBoxColumn
    Friend WithEvents Monto As DataGridViewDoubleInputColumn
    Friend WithEvents NvoSaldo As DataGridViewTextBoxColumn
    Friend WithEvents IdMoneda As DataGridViewIntegerInputColumn
    Friend WithEvents Label23 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecibosNTC))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim UltraStatusPanel7 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel8 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblDisponibleUSD = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblTipoCambio = New System.Windows.Forms.Label()
        Me.cbMoneda = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgvxPagosFacturas = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.CmdAnular = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdBuscar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImprimir = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.NumFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Moneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEmis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaVenc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoFact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.NvoSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdMoneda = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvxPagosFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblDisponibleUSD)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.lblTipoCambio)
        Me.GroupBox3.Controls.Add(Me.cbMoneda)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Controls.Add(Me.TextBox11)
        Me.GroupBox3.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox3.Controls.Add(Me.TextBox12)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 202)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(831, 79)
        Me.GroupBox3.TabIndex = 44
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Nota de Cr�dito"
        '
        'lblDisponibleUSD
        '
        Me.lblDisponibleUSD.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblDisponibleUSD.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDisponibleUSD.Location = New System.Drawing.Point(281, 51)
        Me.lblDisponibleUSD.Name = "lblDisponibleUSD"
        Me.lblDisponibleUSD.Size = New System.Drawing.Size(102, 16)
        Me.lblDisponibleUSD.TabIndex = 48
        Me.lblDisponibleUSD.Text = "0.00"
        Me.lblDisponibleUSD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(180, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(96, 13)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Disponible USD"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(536, 51)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(95, 13)
        Me.Label11.TabIndex = 46
        Me.Label11.Text = "Tipo de Cambio"
        '
        'lblTipoCambio
        '
        Me.lblTipoCambio.Location = New System.Drawing.Point(639, 51)
        Me.lblTipoCambio.Name = "lblTipoCambio"
        Me.lblTipoCambio.Size = New System.Drawing.Size(85, 16)
        Me.lblTipoCambio.TabIndex = 45
        '
        'cbMoneda
        '
        Me.cbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbMoneda.Items.AddRange(New Object() {"efectivo", "cheque", "tarj. cr�dito"})
        Me.cbMoneda.Location = New System.Drawing.Point(635, 24)
        Me.cbMoneda.Name = "cbMoneda"
        Me.cbMoneda.Size = New System.Drawing.Size(181, 21)
        Me.cbMoneda.TabIndex = 44
        Me.cbMoneda.Tag = "Forma de Pago del Recibo a Elaborar"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(579, 26)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(52, 13)
        Me.Label26.TabIndex = 43
        Me.Label26.Text = "Moneda"
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(307, 54)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(74, 16)
        Me.Label23.TabIndex = 33
        Me.Label23.Visible = False
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(392, 51)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 16)
        Me.Label7.TabIndex = 23
        Me.Label7.Text = "Label7"
        Me.Label7.Visible = False
        '
        'Label15
        '
        Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(71, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(105, 16)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "0.00"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(2, 51)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 13)
        Me.Label14.TabIndex = 21
        Me.Label14.Text = "Disponible"
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(360, 40)
        Me.TextBox13.MaxLength = 7995
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox13.Size = New System.Drawing.Size(8, 20)
        Me.TextBox13.TabIndex = 19
        Me.TextBox13.TabStop = False
        Me.TextBox13.Tag = "Descripci�n/Concepto"
        Me.TextBox13.Visible = False
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(71, 24)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(105, 20)
        Me.TextBox11.TabIndex = 9
        Me.TextBox11.Tag = "Monto del Recibo a Elaborar"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = ""
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(279, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 10
        Me.DateTimePicker1.Tag = "Fecha del Recibo a Elaborar"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(465, 24)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(107, 20)
        Me.TextBox12.TabIndex = 11
        Me.TextBox12.Tag = "N�mero del Recibo a Elaborar"
        Me.TextBox12.Text = "0.00"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(412, 26)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(42, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Monto"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(180, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Fecha"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(2, 26)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "N�mero"
        '
        'Timer1
        '
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RichTextBox3)
        Me.GroupBox2.Controls.Add(Me.RichTextBox2)
        Me.GroupBox2.Controls.Add(Me.RichTextBox1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 287)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(831, 115)
        Me.GroupBox2.TabIndex = 42
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Descripciones y Conceptos"
        '
        'RichTextBox3
        '
        Me.RichTextBox3.Location = New System.Drawing.Point(652, 16)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RichTextBox3.Size = New System.Drawing.Size(112, 91)
        Me.RichTextBox3.TabIndex = 30
        Me.RichTextBox3.Text = "RichTextBox3"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Location = New System.Drawing.Point(539, 16)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RichTextBox2.Size = New System.Drawing.Size(112, 91)
        Me.RichTextBox2.TabIndex = 29
        Me.RichTextBox2.Text = "RichTextBox2"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(8, 16)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(531, 91)
        Me.RichTextBox1.TabIndex = 28
        Me.RichTextBox1.Text = "RichTextBox1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox15)
        Me.GroupBox1.Controls.Add(Me.TextBox14)
        Me.GroupBox1.Controls.Add(Me.TextBox9)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 72)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(831, 126)
        Me.GroupBox1.TabIndex = 41
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(656, 73)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(8, 20)
        Me.TextBox15.TabIndex = 36
        Me.TextBox15.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox15.Visible = False
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(640, 73)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(8, 20)
        Me.TextBox14.TabIndex = 35
        Me.TextBox14.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox14.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(504, 96)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(208, 20)
        Me.TextBox9.TabIndex = 7
        Me.TextBox9.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(432, 99)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(67, 13)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Saldo US$"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(203, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 24)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "<F5> AYUDA"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(448, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Negocio"
        Me.Label4.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(504, 73)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(208, 20)
        Me.TextBox5.TabIndex = 4
        Me.TextBox5.Tag = "Vendedor del Cliente"
        Me.TextBox5.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(96, 96)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(283, 20)
        Me.TextBox7.TabIndex = 7
        Me.TextBox7.Tag = "Saldo total del Cliente en Moneda Nacional"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(506, 73)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(207, 20)
        Me.TextBox6.TabIndex = 12
        Me.TextBox6.Tag = "Vendedor del Cliente"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(96, 73)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(283, 20)
        Me.TextBox4.TabIndex = 3
        Me.TextBox4.Tag = "Departamento donde reside negocio/habitaci�n del cliente"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(96, 50)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(625, 20)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Tag = "Direcci�n del Cliente"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(203, 24)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(520, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Nombre del Cliente"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(96, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(102, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "C�digo del Cliente"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Saldo C$"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(442, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Departamento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Direcci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.Text = "Clientes"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvxPagosFacturas)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 408)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(831, 143)
        Me.GroupBox4.TabIndex = 45
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Pagos a Facturas"
        '
        'dgvxPagosFacturas
        '
        Me.dgvxPagosFacturas.AllowUserToAddRows = False
        Me.dgvxPagosFacturas.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvxPagosFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxPagosFacturas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumFactura, Me.Moneda, Me.FechaEmis, Me.FechaVenc, Me.SaldoFact, Me.Monto, Me.NvoSaldo, Me.IdMoneda})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxPagosFacturas.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvxPagosFacturas.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxPagosFacturas.Location = New System.Drawing.Point(8, 15)
        Me.dgvxPagosFacturas.Name = "dgvxPagosFacturas"
        Me.dgvxPagosFacturas.Size = New System.Drawing.Size(757, 121)
        Me.dgvxPagosFacturas.TabIndex = 55
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 554)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel8.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel7, UltraStatusPanel8})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(837, 23)
        Me.UltraStatusBar1.TabIndex = 47
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.LabelItem1, Me.CmdAnular, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem4, Me.cmdBuscar, Me.LabelItem5, Me.cmdImprimir, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(837, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 48
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        Me.cmdOK.Visible = False
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'CmdAnular
        '
        Me.CmdAnular.Image = CType(resources.GetObject("CmdAnular.Image"), System.Drawing.Image)
        Me.CmdAnular.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.CmdAnular.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.CmdAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.CmdAnular.Name = "CmdAnular"
        Me.CmdAnular.Text = "Anular<F3>"
        Me.CmdAnular.Tooltip = "Anular Recibo"
        Me.CmdAnular.Visible = False
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'cmdBuscar
        '
        Me.cmdBuscar.Image = CType(resources.GetObject("cmdBuscar.Image"), System.Drawing.Image)
        Me.cmdBuscar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdBuscar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdBuscar.Name = "cmdBuscar"
        Me.cmdBuscar.Text = "Buscar<F6>"
        Me.cmdBuscar.Tooltip = "Extraer datos del recibo"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = "  "
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Image = CType(resources.GetObject("cmdImprimir.Image"), System.Drawing.Image)
        Me.cmdImprimir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimir.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.cmdImprimir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Text = "Imprimir<F8>"
        Me.cmdImprimir.Tooltip = "Imprimir recibos"
        Me.cmdImprimir.Visible = False
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "  "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'NumFactura
        '
        Me.NumFactura.HeaderText = "Numero Factura"
        Me.NumFactura.Name = "NumFactura"
        Me.NumFactura.ReadOnly = True
        '
        'Moneda
        '
        Me.Moneda.HeaderText = "Moneda"
        Me.Moneda.Name = "Moneda"
        Me.Moneda.ReadOnly = True
        '
        'FechaEmis
        '
        Me.FechaEmis.HeaderText = "Fecha Emis."
        Me.FechaEmis.Name = "FechaEmis"
        Me.FechaEmis.ReadOnly = True
        '
        'FechaVenc
        '
        Me.FechaVenc.HeaderText = "Fecha Vence"
        Me.FechaVenc.Name = "FechaVenc"
        Me.FechaVenc.ReadOnly = True
        '
        'SaldoFact
        '
        Me.SaldoFact.HeaderText = "Saldo Factura"
        Me.SaldoFact.Name = "SaldoFact"
        Me.SaldoFact.ReadOnly = True
        '
        'Monto
        '
        '
        '
        '
        Me.Monto.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.Monto.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Monto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Monto.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Increment = 1.0R
        Me.Monto.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Monto.Name = "Monto"
        Me.Monto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'NvoSaldo
        '
        Me.NvoSaldo.HeaderText = "Nvo. Saldo"
        Me.NvoSaldo.Name = "NvoSaldo"
        Me.NvoSaldo.ReadOnly = True
        '
        'IdMoneda
        '
        '
        '
        '
        Me.IdMoneda.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.IdMoneda.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IdMoneda.HeaderText = "IdMoneda"
        Me.IdMoneda.Name = "IdMoneda"
        Me.IdMoneda.ReadOnly = True
        Me.IdMoneda.Visible = False
        '
        'frmRecibosNTC
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(837, 577)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRecibosNTC"
        Me.Text = "frmRecibosNTC"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvxPagosFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public txtCollection As New Collection
    Dim SaldoAnterior As Double
    Dim dblMontoRecibo, dblInteresRecibo, dblMntVlrRecibo, dblPendienteRecibo As Double
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmRecibosNTC"
    Private gnCantidadDecimal As Integer = 2
    Dim gbIndicaCierre As Boolean = False
    Private Sub frmRecibosNTC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnVistaPrevia = False
        dblMontoReciboNTDNTC = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnTasaCambioDelDia As Decimal = 0
        Try
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambioDelDia = RNTipoCambio.ObtieneTipoCambioDelDia()
            lblTipoCambio.Text = lnTasaCambioDelDia

            If lnTasaCambioDelDia <= 0 Then
                MsgBox("No se ha ingresado la tasa del d�a, favor ingresar la tasa del d�a antes de realizar operaciones ", MsgBoxStyle.Critical, "Error de Datos")
                gbIndicaCierre = True
                'Me.Close()
            End If

            Select Case intTipoReciboCaja
                Case ENTipoNotas.NOTA_CREDITO_ELABORAR
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                    cmdBuscar.Visible = False
                Case ENTipoNotas.NOTA_CREDITO_CONSULTA
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                Case ENTipoNotas.NOTA_CREDITO_ANULAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
            End Select
            RNMoneda.CargaComboMoneda(cbMoneda)
            txtCollection.Add(TextBox1)
            txtCollection.Add(TextBox2)
            txtCollection.Add(TextBox3)
            txtCollection.Add(TextBox4)
            txtCollection.Add(TextBox5)
            txtCollection.Add(TextBox6)
            txtCollection.Add(TextBox7)
            '     txtCollection.Add(TextBox8)
            txtCollection.Add(TextBox9)
            '    txtCollection.Add(TextBox10)
            txtCollection.Add(TextBox11)
            txtCollection.Add(TextBox12)
            txtCollection.Add(TextBox13)
            txtCollection.Add(TextBox14)
            txtCollection.Add(TextBox15)
            Limpiar()
            If intTipoReciboCaja = 10 Then
                'Me.ToolBar1.Buttons.Item(0).Enabled = False
                cmdOK.Visible = False
            End If
            Label7.Text = intTipoReciboCaja
            For intIncr = 0 To 8
                strVistaPreviaNTCNTD(intIncr) = ""
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub
    Private Sub CalculaDisponibleRecibo()
        Dim dblMontoRecibo As Decimal = 0
        Dim lnMonedaRecibo As Integer = 0
        Dim lnMonedaFactura As Integer = 0
        Dim lnTipoCambio As Decimal = 0
        Dim lnMontoTotalAPagar As Decimal = 0
        Dim lnMontoTotalInteresAPagar As Decimal = 0
        Dim lnMontoDescuentoAPagar As Decimal = 0
        Dim lnMontoTotalMantenimientoValorAPagar As Decimal = 0
        Dim lnMontoTotal As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnCantidadDecimal As Int16 = 2
        Try
            lnMonedaRecibo = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
            dblMontoRecibo = SUConversiones.ConvierteADecimal(TextBox12.Text)
            lnTipoCambio = SUConversiones.ConvierteADecimal(lblTipoCambio.Text)
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lblTipoCambio.Text = lnTipoCambio

            dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, lnCantidadDecimal)
            'lnTipoCambio = Math.Round(lnTipoCambio, 2)
            lnTipoCambio = lnTipoCambio
            lnMontoTotalAPagar = 0
            lnMontoTotalInteresAPagar = 0
            If dgvxPagosFacturas.Rows.Count >= 1 Then
                For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                    lnMonedaFactura = 0
                    ' If dblMontoRecibo > 0 Then
                    lnMontoTotalAPagar = 0
                    lnMontoTotalInteresAPagar = 0
                    lnMontoTotalMantenimientoValorAPagar = 0
                    lnMontoDescuentoAPagar = 0
                    lnMonedaFactura = 0
                    lnMonedaFactura = SUConversiones.ConvierteAInt(dgvxPagosFacturas.Rows(intIncr).Cells("IdMoneda").Value)
                    lnMontoTotalAPagar = lnMontoTotalAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)

                    lnMontoTotal = 0
                    'lnMontoTotal = lnMontoTotalAPagar + lnMontoDescuentoAPagar + lnMontoTotalInteresAPagar + lnMontoTotalMantenimientoValorAPagar
                    lnMontoTotal = lnMontoTotalAPagar + lnMontoTotalInteresAPagar + lnMontoTotalMantenimientoValorAPagar
                    lnMontoTotal = ObtieneMontoSegunMonedaRecibo(lnMontoTotal, lnMonedaFactura)
                    dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal) - SUFunciones.RedondeoNumero(lnMontoTotal, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)

                Next intIncr

                dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                Label15.Text = Format(dblMontoRecibo, "#,##0.####")
                lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(Label15.Text), "#,##0.####")
                If lnTipoCambio <> 0 Then
                    If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                        lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
                            Label15.Text = 0
                        Else
                            ' Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
                            Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        End If
                    Else
                        Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        If SUConversiones.ConvierteADecimal(Label15.Text.Trim()) = 0 Then
                            lblDisponibleUSD.Text = 0
                        Else
                            lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        End If

                    End If

                End If
                If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
                    Label15.Text = "0.00"
                End If
                If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                    Label15.ForeColor = System.Drawing.Color.Red
                Else
                    Label15.ForeColor = System.Drawing.Color.Black
                End If
                If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                    lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                Else
                    lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default
    End Sub
    Private Function ObtieneMontoSegunMonedaRecibo(ByVal pnMontoAConvertir As Decimal, ByVal pnMonedaFactura As Double) As Decimal
        Dim lnMontoConvertido As Decimal = 0
        Dim lnMonedaRecibo As Integer = 0
        Dim lnTasaCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambio = SUConversiones.ConvierteADecimal(lblTipoCambio.Text)
            lnMonedaRecibo = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
            If lnMonedaRecibo <> pnMonedaFactura Then
                If lnMonedaRecibo = 1 Then
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir * lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                Else
                    'lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                End If
            Else
                lnMontoConvertido = pnMontoAConvertir
            End If
            Return lnMontoConvertido
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function
    Private Sub frmRecibosNTC_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing

        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
                If intTipoReciboCaja = 5 Then

                    objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()

                    If objParametroEmpresa IsNot Nothing Then
                        If objParametroEmpresa.IndicaAnulaReciboMesAnterior = 0 Then
                            If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
                            Else
                                MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                                Exit Sub
                            End If
                        End If
                    End If
                    'If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
                    'Else
                    '    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    '    Exit Sub
                    'End If
                    Anular()
                ElseIf intTipoReciboCaja = 2 Then
                    Guardar()
                End If
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If Label17.Visible = True Then
                    intListadoAyuda = 2
                    Dim frmNew As New frmListadoAyuda
                    'Timer1.Interval = 200
                    'Timer1.Enabled = True
                    frmNew.ShowDialog()
                End If
            ElseIf e.KeyCode = Keys.F6 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
                If intTipoReciboCaja <> 2 Then
                    Extraer()
                End If
            ElseIf e.KeyCode = Keys.F7 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
                If intTipoReciboCaja = 2 Then
                    VistaPrevia()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub VistaPrevia()

        Try
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            Dim frmNew As New actrptViewer
            intRptImpRecibos = 10
            blnVistaPrevia = True
            strQuery = ""
            strVistaPreviaNTCNTD(0) = UCase(TextBox11.Text)
            strVistaPreviaNTCNTD(1) = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            strVistaPreviaNTCNTD(2) = UCase(TextBox2.Text)
            strVistaPreviaNTCNTD(3) = UCase(TextBox1.Text)
            strVistaPreviaNTCNTD(4) = UCase(TextBox3.Text)
            strVistaPreviaNTCNTD(5) = RichTextBox1.Text
            strVistaPreviaNTCNTD(6) = RichTextBox2.Text
            strVistaPreviaNTCNTD(7) = RichTextBox3.Text
            strVistaPreviaNTCNTD(8) = Label23.Text
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
            For intIncr = 0 To 8
                strVistaPreviaNTCNTD(intIncr) = ""
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem12.Click, MenuItem15.Click

        intListadoAyuda = 0
        intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Clientes" : intListadoAyuda = 2
            Case "&Salir" : Me.Close()
        End Select
        If intListadoAyuda <> 0 Then
            Dim frmNew As New frmListadoAyuda
            'Timer1.Interval = 200
            'Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)

    End Sub

    Sub Limpiar()

        Try
            For intIncr = 1 To 13
                txtCollection.Item(intIncr).text = ""
            Next
            TextBox7.Text = "0.00"
            'TextBox8.Text = "0.00"
            TextBox9.Text = "0.00"
            'TextBox10.Text = "0.00"
            TextBox12.Text = "0.00"
            TextBox14.Text = "0"
            TextBox15.Text = "0"
            If intTipoReciboCaja = 5 Then
                For intIncr = 1 To 13
                    txtCollection.Item(intIncr).readonly = True
                Next
                TextBox1.ReadOnly = False
                TextBox11.ReadOnly = False
            End If
            RichTextBox1.Text = ""
            RichTextBox2.Text = ""
            RichTextBox3.Text = ""
            If dgvxPagosFacturas.Rows.Count > 0 Then
                dgvxPagosFacturas.Rows.Clear()
            End If

            TextBox1.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub Extraer()
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
            'If TextBox14.Text = "0" Then
            '    MsgBox("Tiene que digitar el codigo del cliente.", MsgBoxStyle.Critical, "Error de Dato")
            '    Exit Sub
            'End If
            If TextBox11.Text = "" Then
                MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            Me.Cursor = Cursors.WaitCursor
            TextBox12.Text = "0.00"
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "exec ObtieneNotaDeCredito '" & UCase(TextBox11.Text) & "'"
            'strQuery = strQuery + "e.registro = d.registro and e.CliRegistro = " & SUConversiones.ConvierteAInt(TextBox14.Text) & " "
            'strQuery = strQuery + "And e.Numero = '" & UCase(TextBox11.Text) & "' And e.TipoRecibo = 2"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                UbicarCliente(SUConversiones.ConvierteAString(dtrAgro2K.Item("CodigoCliente")))
                TextBox12.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("monto")), "##,##0.#0")
                ' DateTimePicker1.Value = dtrAgro2K.GetValue(16)
                DateTimePicker1.Value = dtrAgro2K.Item("fecha")
                RichTextBox1.Text = dtrAgro2K.Item("descr01")
                RichTextBox2.Text = dtrAgro2K.Item("descr02")
                RichTextBox3.Text = dtrAgro2K.Item("descr03")
                LlenarGridConsultaNotaCredito(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("DesMoneda"), dtrAgro2K.Item("fechaing"), dtrAgro2K.Item("fechaven"), dtrAgro2K.Item("SaldoFactura"), dtrAgro2K.Item("MontoPagado"), dtrAgro2K.Item("IdMoneda"))
                Label23.Text = ""
                If dtrAgro2K.Item("estado") = 1 Then
                    Label23.Text = "ANULADA"
                End If
                If intTipoReciboCaja = 5 Then
                    If dtrAgro2K.Item("estado") = 1 Then
                        'dtrAgro2K.Close()
                        SUFunciones.CierraConexioDR(dtrAgro2K)
                        MsgBox("La Nota de Cr�dito ya fue anulada.", MsgBoxStyle.Information, "Recibo Anulado")
                        Limpiar()
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End If
                End If
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            If TextBox12.Text = String.Empty Or TextBox12.Text = "0.00" Then
                MsgBox("El n�mero de la Nota de Cr�dito no es v�lido.  Verifique.", MsgBoxStyle.Critical, "Error de Datos")
                Exit Sub
            End If
            Me.Cursor = Cursors.Default
            If intTipoReciboCaja = 2 Or intTipoReciboCaja = 10 Then
                ImprimirRecibo()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox11.GotFocus, TextBox12.GotFocus

        Dim strCampo As String

        Try
            MenuItem15.Visible = False
            strCampo = ""
            Select Case sender.name.ToString
                Case "TextBox1" : Label17.Visible = True : strCampo = "C�digo" : MenuItem11.Visible = True : MenuItem15.Visible = True
                Case "TextBox11" : strCampo = "Recibo"
                Case "TextBox12" : strCampo = "Monto"
                Case "TextBox13""Descripci�n"
            End Select
            'StatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            'StatusBar1.Panels.Item(1).Text = sender.tag
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            sender.selectall()
            If blnUbicar Then
                ObtieneDatosListadoAyuda()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox11.KeyDown, TextBox12.KeyDown
        Try
            If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
                Select Case sender.name.ToString
                    Case "TextBox1"
                        If UbicarCliente(TextBox1.Text) = True Then
                            TextBox11.Focus()
                            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Then
                                LlenarGrid(TextBox1.Text)
                            End If
                        End If
                    Case "TextBox11"
                        TextBox12.Focus()
                        If intTipoReciboCaja = 2 Then
                            UbicarRecibo()
                        ElseIf intTipoReciboCaja <> 2 Then
                            Extraer()
                        End If
                    Case "TextBox12"
                        If TextBox12.Text = "0.00" Or Len(TextBox12.Text) = 0 Then
                            MsgBox("Tiene que ingresar monto a acreditarle al cliente.", MsgBoxStyle.Critical, "Error de Datos")
                            TextBox12.Focus()
                            Exit Sub
                        End If
                        If IsNumeric(TextBox12.Text) = False Then
                            MsgBox("Tiene que ingresar valor n�merico a acreditarle al cliente.", MsgBoxStyle.Critical, "Error de Datos")
                            TextBox12.Focus()
                            Exit Sub
                        End If
                        dblMontoReciboNTDNTC = ConvierteADouble(TextBox12.Text)
                        TextBox12.Text = Format(ConvierteADouble(dblMontoReciboNTDNTC), "#,##0.#0")
                        RichTextBox1.Focus()
                        dblMontoRecibo = ConvierteADouble(TextBox12.Text)
                        If dgvxPagosFacturas.Rows.Count >= 1 Then
                            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                                dblMontoRecibo = dblMontoRecibo - ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                            Next intIncr
                            Label15.Text = Format(dblMontoRecibo, "#,##0.#0")
                            If ConvierteADouble(Label15.Text) < 0 Then
                                Label15.ForeColor = System.Drawing.Color.Red
                            Else
                                Label15.ForeColor = System.Drawing.Color.Black
                            End If
                        End If
                    Case "TextBox13"
                        TextBox1.Focus()
                End Select
                CalculaDisponibleRecibo()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub UbicarRecibo()
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            SUFunciones.CierraConexionBD(cnnAgro2K)
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = " exec ConsultarNotaDeCredito '" & UCase(TextBox11.Text.Trim()) & "' "
            'strQuery = "select Numero, Estado from tbl_Recibos_Enc E where "
            'strQuery = strQuery + "Numero = '" & UCase(TextBox11.Text) & "' And E.TipoRecibo = 2"
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(1) = 0 Then
                        MsgBox("La nota de cr�dito ya fue emitida.", MsgBoxStyle.Critical, "Nota de Cr�dito Emitida")
                    ElseIf dtrAgro2K.GetValue(1) = 1 Then
                        MsgBox("La nota de cr�dito ya fue anulada.", MsgBoxStyle.Critical, "Nota de Cr�dito Anulada")
                    End If
                    TextBox11.Focus()
                    TextBox11.Text = ""
                    Exit While
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexioDR(dtrAgro2K)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus

        Label17.Visible = False

    End Sub

    Function UbicarCliente(ByVal strDato As String) As Boolean
        Dim ldrDatos As IDataReader = Nothing
        Dim strVendedor As String = String.Empty
        Dim dtVendedor As DataTable = Nothing
        Try
            TextBox14.Text = "0"
            TextBox15.Text = "0"
            ldrDatos = RNCliente.ObtieneClientexCodigo(strDato)
            If ldrDatos IsNot Nothing Then
                While ldrDatos.Read
                    If IsDBNull(ldrDatos.GetValue(0)) = False Then
                        TextBox14.Text = ldrDatos.GetValue(0)
                    End If
                End While
                SUFunciones.CierraConexioDR(ldrDatos)
                '  If cnnAgro2K.State = ConnectionState.Open Then
                'cnnAgro2K.Close()
                'End If
                'dtrAgro2K = Nothing
                ldrDatos = RNCliente.UbicarClientexIdCliente(SUConversiones.ConvierteALong(TextBox14.Text))
                If ldrDatos IsNot Nothing Then
                    ' Limpiar()
                    strVendedor = ""
                    While ldrDatos.Read
                        TextBox1.Text = ldrDatos.Item("CodigoCliente")
                        TextBox2.Text = ldrDatos.Item("NombreCliente")
                        TextBox3.Text = ldrDatos.Item("DireccionCliente")
                        TextBox4.Text = ldrDatos.Item("NombreDepartamento")
                        TextBox5.Text = ldrDatos.Item("DescripcionNegocio")
                        TextBox6.Text = ldrDatos.Item("NombreVendedor")
                        TextBox7.Text = Format(ldrDatos.Item("Saldo"), "#,##0.#0")
                        strVendedor = ldrDatos.Item("CodigoVendedor")
                        TextBox14.Text = ldrDatos.Item("Registro")
                        TextBox9.Text = Format(ldrDatos.Item("SaldoUSD"), "#,##0.#0")

                    End While
                    SUFunciones.CierraConexioDR(ldrDatos)
                End If

                If TextBox2.Text = "" Then
                    UbicarCliente = False
                    'cmdAgro2K.Connection.Close()
                    'cnnAgro2K.Close()
                    Exit Function
                End If

                dtVendedor = RNVendedor.ObtieneVendedorCodigoVendedor(strVendedor)
                If SUFUnciones.ValidaDataTable(dtVendedor) Then
                    TextBox15.Text = dtVendedor.Rows(0)(0)
                    UbicarCliente = True
                End If
                'If ldrDatos IsNot Nothing Then
                '        While ldrDatos.Read
                '            If IsDBNull(ldrDatos.GetValue(0)) = False Then
                '                TextBox15.Text = ldrDatos.GetValue(0)
                '            End If
                '        End While
                '        SUFunciones.CierraConexioDR(ldrDatos)
                '    UbicarCliente = True
                'End If
            End If
            ' End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Sub LlenarGrid(ByVal strDato As String)
        Try

            dtrAgro2K = RNRecibos.ObtieneFacturasPendientesxCliente(SUConversiones.ConvierteAInt(TextBox14.Text))
            While dtrAgro2K.Read
                dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("Numero"), dtrAgro2K.Item("DesMoneda"), dtrAgro2K.Item("fechaing"), dtrAgro2K.Item("fechaven"), Format(dtrAgro2K.Item("Saldo"), "#,##0.#0"), 0, Format(dtrAgro2K.Item("Saldo"), "#,##0.#0"), dtrAgro2K.Item("IdMoneda"))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al intentar llenar el grid " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub LlenarGridConsultaNotaCredito(ByVal ldrNotaCredito As IDataReader)
        Try
            dgvxPagosFacturas.Columns("SaldoFact").Visible = False
            dgvxPagosFacturas.Columns("NvoSaldo").Visible = False
            While ldrNotaCredito.Read
                dgvxPagosFacturas.Rows.Add(ldrNotaCredito.Item("NumeroFactura"), ldrNotaCredito.Item("DesMoneda"), ldrNotaCredito.Item("fechaing"), dtrAgro2K.Item("fechaven"), Format(dtrAgro2K.Item("SaldoFactura"), "#,##0.#0"), Format(dtrAgro2K.Item("MontoPagado"), "#,##0.#0"), dtrAgro2K.Item("IdMoneda"))
            End While

            'SUFunciones.CierraConexioDR(ldrNotaCredito)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al intentar llenar el grid " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Sub LlenarGridConsultaNotaCredito(ByVal psNumeroFctura As String, ByVal psMoneda As String, ByVal psFechaIngreso As String, ByVal psFechaVencimiento As String, ByVal pnSaldoFactura As Decimal, ByVal pnMontoPagado As Decimal, ByVal pnIdMoneda As Integer)
        Try
            dgvxPagosFacturas.Columns("SaldoFact").Visible = False
            dgvxPagosFacturas.Columns("NvoSaldo").Visible = False

            dgvxPagosFacturas.Rows.Add(psNumeroFctura, psMoneda, psFechaIngreso, psFechaVencimiento, Format(pnSaldoFactura, "#,##0.#0"), Format(pnMontoPagado, "#,##0.#0"), pnIdMoneda)

            'SUFunciones.CierraConexioDR(ldrNotaCredito)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al intentar llenar el grid " & ex.Message, " Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Sub Guardar()
        Dim objRecibos As SERecibosEncabezado
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleRecibo As List(Of SERecibosDetalle) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Dim objReqCampo As Object = Nothing

        blnVistaPrevia = False
        intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
        If intTipoReciboCaja = 2 Then
            If TextBox11.Text <> "" Then
                UbicarRecibo()
            End If
        End If
        If TextBox14.Text = "0" Then
            MsgBox("Tiene que digitar el codigo del cliente.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox11.Text = "" Then
            MsgBox("Tiene que digitar el n�mero de la nota de cr�dito.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox12.Text = "" Then
            MsgBox("Tiene que digitar el monto de la nota de cr�dito.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox12.Text = "0.00" Then
            MsgBox("Tiene que digitar el monto de la nota de cr�dito.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If IsNumeric(TextBox12.Text) = False Then
            MsgBox("El monto de la nota de cr�dito debe ser numerico.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If

        If SUConversiones.ConvierteADouble(TextBox12.Text) <= 0 Then
            MsgBox("El monto de la nota de cr�dito debe ser mayor que 0.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
            MsgBox("La sumatoria de los pagos no pueden ser mayor que el monto del Recibo.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If SUConversiones.ConvierteADouble(Label15.Text) > 0 Then
            MsgBox("No puede haber Saldo Disponible para un cliente.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        'Dim cmdTmp As New SqlCommand("sp_IngRecibosEnc", cnnAgro2K)

        If TextBox1.Text = "" Then
            MsgBox("Tiene que digitar el cliente a quien aplicar la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If Len(Trim(TextBox12.Text)) = 0 Then
            MsgBox("Tiene que digitar el monto de la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If IsNumeric(TextBox12.Text) = False Then
            MsgBox("Tiene que digitar el monto de la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If ConvierteADouble(TextBox12.Text) <= 0 Then
            MsgBox("El monto de la transacci�n debe ser mayor que 0.00", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor
        Try
            TextBox1.Focus()
            objRecibos = New SERecibosEncabezado()
            objRecibos.registro = 0
            objRecibos.fecha = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            objRecibos.numfecha = Format(DateTimePicker1.Value, "yyyyMMdd")
            objRecibos.cliregistro = SUConversiones.ConvierteAInt(TextBox14.Text)
            objRecibos.vendregistro = SUConversiones.ConvierteAInt(TextBox15.Text)
            objRecibos.numero = UCase(TextBox11.Text)
            objRecibos.monto = SUConversiones.ConvierteADouble(TextBox12.Text)
            objRecibos.interes = 0
            objRecibos.retencion = 0
            objRecibos.formapago = 0
            objRecibos.numchqtarj = ""
            objRecibos.nombrebanco = ""
            objRecibos.reciboprov = ""
            objRecibos.descripcion = ""
            objRecibos.usrregistro = lngRegUsuario
            objRecibos.tiporecibo = intTipoReciboCaja
            objRecibos.fecha_ing = DateTimePicker1.Value
            objRecibos.saldofavor = 0
            objRecibos.descr01 = RichTextBox1.Text
            objRecibos.descr02 = RichTextBox2.Text
            objRecibos.descr03 = RichTextBox3.Text
            objRecibos.IdMoneda = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
            objRecibos.TipoCambio = SUConversiones.ConvierteADecimal(lblTipoCambio.Text)

            lstDetalleRecibo = CargarDetalleRecibos()

            If (objRecibos Is Nothing) And (lstDetalleRecibo Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el Recibo", " Guardar Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            RNRecibos.IngresaTransaccionNotaCredito(objRecibos, lstDetalleRecibo)
            MessageBoxEx.Show("Registro Ingresado/Actualizado", "Aplicaci�n Satisfactoria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'ImprimirRecibo()
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error para Ingresar la Nota de Credito " & ex.Message, " Guardar Nota de Credito ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Function CargarDetalleRecibos() As List(Of SERecibosDetalle)
        Dim lstDetalleRec As List(Of SERecibosDetalle) = Nothing
        Dim objDetalleRec As SERecibosDetalle
        Dim i As Integer
        Dim dblAbono As Double
        Dim strRecibo As String

        objDetalleRec = New SERecibosDetalle()
        lstDetalleRec = New List(Of SERecibosDetalle)
        Try
            If dgvxPagosFacturas.Rows.Count > 0 Then
                For i = 0 To dgvxPagosFacturas.Rows.Count - 1
                    dblAbono = dgvxPagosFacturas.Rows(i).Cells("Monto").Value
                    If dblAbono <> 0 Then
                        objDetalleRec.registro = 0
                        objDetalleRec.numero = 0
                        If SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(i).Cells("NvoSaldo").Value) = 0 Then
                            strRecibo = "Cancelaci�n de la Factura " & dgvxPagosFacturas.Rows(i).Cells("NumFactura").Value
                        Else
                            strRecibo = "Abono a la Factura " & dgvxPagosFacturas.Rows(i).Cells("NumFactura").Value
                        End If
                        objDetalleRec.numero = dgvxPagosFacturas.Rows(i).Cells("NumFactura").Value
                        objDetalleRec.descripcion = strRecibo
                        objDetalleRec.monto = dgvxPagosFacturas.Rows(i).Cells("Monto").Value
                        lstDetalleRec.Add(New SERecibosDetalle(objDetalleRec))
                    End If
                Next
            End If
            Return lstDetalleRec
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Function

    Sub Anular()

        intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
        Me.Cursor = Cursors.WaitCursor
        Dim lngNumFecha As Integer
        lngNumFecha = 0
        lngNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")

        Try
            RNRecibos.AnulaNotaCredito(SUConversiones.ConvierteAInt(TextBox14.Text), TextBox11.Text, lngNumFecha, ConvierteADouble(TextBox12.Text), 0, intTipoReciboCaja)
            MsgBox("Recibo Anulado", MsgBoxStyle.Information, "Anulaci�n Satisfactoria")
            Limpiar()
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            Me.Cursor = Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally

        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub CalcularMontosGrid()

        Dim strMonto As String = String.Empty
        Dim dblMonto As Double = 0
        Dim dblMontoRecibo As Double = 0
        Dim dblSaldo As Double = 0
        Dim strMtoAnt As String = String.Empty
        Dim dblMtoAnt As Decimal = 0
        Dim intResp As Integer = 0
        Dim lnTotalMonto As Decimal = 0
        Dim lnMontoRecibo As Decimal = 0
        Dim lnSaldoFactura As Decimal = 0
        Dim lnMontoActual As Decimal = 0
        Dim lnIdMOnedaFactura As Decimal = 0
        Dim lnTipoCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            lnTipoCambio = 0
            lnTipoCambio = SUConversiones.ConvierteADecimal(lblTipoCambio.Text)
            lnMontoRecibo = 0
            lnMontoRecibo = SUConversiones.ConvierteADouble(TextBox12.Text)
            If lnMontoRecibo > 0 Then
                If dgvxPagosFacturas.Rows.Count >= 1 Then
                    If dgvxPagosFacturas.IsCurrentCellDirty Then
                        dgvxPagosFacturas.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    End If
                    objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                    'If dgvxPagosFacturas.Columns.IndexOf(dgvxPagosFacturas.Columns("Monto")) = 6 Then
                    lnIdMOnedaFactura = 0
                    lnIdMOnedaFactura = dgvxPagosFacturas.CurrentRow.Cells("IdMoneda").Value
                    'CalcularMontosGrid()
                    lnSaldoFactura = 0
                    lnSaldoFactura = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)


                    'lnSaldoFactura = SUConversiones.ConvierteADouble(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
                    'lnSaldoFactura = ObtieneMontoSegunMoneda(lnSaldoFactura, lnIdMOnedaFactura)
                    lnMontoActual = 0
                    lnMontoActual = SUConversiones.ConvierteADouble(dgvxPagosFacturas.CurrentRow.Cells("Monto").Value)
                    ''   lnMontoActual = ObtieneMontoSegunMoneda(lnMontoActual, lnIdMOnedaFactura)

                    If lnMontoActual <= 0 Then
                        MsgBox("El monto a abonar debe ser mayor o igual a 0.00", MsgBoxStyle.Critical, "Error de Dato")
                        lnMontoActual = 0
                        dgvxPagosFacturas.CurrentRow.Cells("Monto").Value = 0
                        Exit Sub
                    End If
                    If (lnMontoActual) > lnSaldoFactura Then
                        MsgBox("No es permitido pagar de m�s una factura.", MsgBoxStyle.Critical, "Error de Dato")
                        lnMontoActual = 0
                        dgvxPagosFacturas.CurrentRow.Cells("Monto").Value = 0
                        Exit Sub
                    End If
                    dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(lnSaldoFactura - (lnMontoActual), "###,##0.####")


                    'End If
                    lnTotalMonto = 0

                End If
            Else
                MsgBox("Favor digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                TextBox12.Focus()
                Exit Sub
            End If

            CalculaDisponibleRecibo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub
    Private Function ObtieneMontoSegunMoneda(ByVal pnMontoAConvertir As Decimal, ByVal pnMonedaFactura As Double) As Decimal
        Dim lnMontoConvertido As Decimal = 0
        Dim lnMonedaRecibo As Integer = 0
        Dim lnTasaCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambio = SUConversiones.ConvierteADecimal(lblTipoCambio.Text)
            lnMonedaRecibo = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
            If lnMonedaRecibo <> pnMonedaFactura Then
                If pnMonedaFactura = 1 Then
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir * lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                Else
                    'lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                    lnMontoConvertido = SUFunciones.RedondeoNumero((pnMontoAConvertir / lnTasaCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                End If
            Else
                lnMontoConvertido = pnMontoAConvertir
            End If
            Return lnMontoConvertido
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function
    'Private Sub CalcularMontosGrid()

    '    Dim strMonto As String
    '    Dim dblMonto As Double
    '    Dim dblSaldo As Double
    '    Dim strMtoAnt As String
    '    Dim dblMtoAnt As Double
    '    Dim intResp As Integer = 0

    '    Try
    '        If dgvxPagosFacturas.Rows.Count >= 1 Then
    '            If dgvxPagosFacturas.CurrentCell.ColumnIndex = 5 Then   'Celda en la que se ingresa el monto del recibo
    '                dblMonto = 0
    '                strMonto = ""
    '                dblMtoAnt = 0
    '                strMtoAnt = ""
    '                ''strMtoAnt = dgvxPagosFacturas.CurrentCell.Value
    '                strMtoAnt = SaldoAnterior
    '                strMonto = dgvxPagosFacturas.CurrentCell.Value
    '                dblMtoAnt = SUConversiones.ConvierteADouble(strMtoAnt)

    '                If IsNumeric(Trim(strMonto)) = False Then
    '                    Exit Sub
    '                End If
    '                dblMonto = SUConversiones.ConvierteADouble(strMonto)
    '                If dblMonto < 0 Then
    '                    MsgBox("El monto a abonar debe ser mayor o igual a 0.00", MsgBoxStyle.Critical, "Error de Dato")
    '                    dgvxPagosFacturas.CurrentRow.Cells("Monto").Value = SaldoAnterior
    '                    Exit Sub
    '                End If

    '                'MontoDesc = dgvxPagosFacturas.CurrentRow.Cells("MontoDescuentoFact").Value

    '                dblSaldo = SUConversiones.ConvierteADouble(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
    '                If dblSaldo - dblMonto < 0 Then
    '                    MsgBox("No es permitido pagar de m�s una factura.", MsgBoxStyle.Critical, "Error de Dato")
    '                    dblMontoRecibo = 0
    '                    dgvxPagosFacturas.CurrentRow.Cells("Monto").Value = SaldoAnterior
    '                    'dgvxPagosFacturas.CurrentCell.Value = 0
    '                    Exit Sub
    '                End If

    '                dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(dblSaldo - dblMonto, "###,##0.#0")
    '                dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
    '                Label15.Text = Format(dblMontoRecibo, "###,##0.#0")
    '                If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
    '                    Label15.ForeColor = System.Drawing.Color.Red
    '                Else
    '                    Label15.ForeColor = System.Drawing.Color.Black
    '                End If

    '            End If
    '        End If
    '        CalculaDisponibleRecibo()
    '    Catch ex As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = ex.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        Throw ex
    '        'MsgBox("Error: ", MsgBoxStyle.Critical, "Notas de credito")
    '    End Try

    'End Sub

    Sub ImprimirRecibo()

        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
            intRptOtros = 0
            intRptPresup = 0
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptExportar = 0
            intRptCheques = 0
            intRptImpRecibos = 0
            Dim frmNew As New actrptViewer
            intRptImpRecibos = intTipoReciboCaja
            strVistaPreviaNTCNTD(8) = Label23.Text
            strQuery = ""
            strQuery = "exec sp_ExtraerReciboRpt " & intRptImpRecibos & ", " & SUConversiones.ConvierteAInt(TextBox14.Text) & ", "
            strQuery = strQuery + "'" & UCase(TextBox11.Text) & "', " & Format(DateTimePicker1.Value, "yyyyMMdd") & ""
            'MsgBox(strQuery)
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
            If intTipoReciboCaja = 2 Then
                frmNew.WindowState = FormWindowState.Minimized
                frmNew.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub ObtieneDatosListadoAyuda()
        Try
            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 2 : TextBox1.Text = strUbicar
                            If UbicarCliente(TextBox1.Text) = True Then
                                TextBox11.Focus()
                                If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Then
                                    LlenarGrid(TextBox1.Text)
                                End If
                            End If
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Try
            If blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 2 : TextBox1.Text = strUbicar
                            If UbicarCliente(TextBox1.Text) = True Then
                                TextBox11.Focus()
                                If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Then
                                    LlenarGrid(TextBox1.Text)
                                End If
                            End If
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try


    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.DoubleClick
        Try
            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            frmNew.ShowDialog()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub TextBox12_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.LostFocus

        Try
            If TextBox1.Text <> "" And TextBox11.Text <> "" Then
                If IsNumeric(sender.text) = True Then
                    sender.text = Format(ConvierteADouble(sender.text), "#,##0.#0")
                Else
                    MsgBox("Tiene que ser un valor num�rico v�lido. Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                    sender.focus()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub cmdBuscar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
            If intTipoReciboCaja = 2 Then
                VistaPrevia()
            Else
                Extraer()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub CmdAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAnular.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
            Dim objParametroEmpresa As SEParametroEmpresa = Nothing
            'If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
            'Else
            '    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
            '    Exit Sub
            'End If
            objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()

            If objParametroEmpresa IsNot Nothing Then
                If objParametroEmpresa.IndicaAnulaReciboMesAnterior = 0 Then
                    If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
                    Else
                        MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                        Exit Sub
                    End If
                End If
            End If
            Anular()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        Try
            ImprimirRecibo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub cmdOK_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label7.Text)
            Guardar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub dgvxPagosFacturas_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvxPagosFacturas.CellBeginEdit
        Try
            SaldoAnterior = dgvxPagosFacturas.CurrentCell.Value
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub dgvxPagosFacturas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvxPagosFacturas.KeyPress
        Try
            If e.KeyChar = Keys.Enter.ToString Then
                CalcularMontosGrid()
            ElseIf e.KeyChar = Keys.Tab.ToString Then
                CalcularMontosGrid()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub frmRecibosNTC_VisibleChanged(sender As Object, e As EventArgs) Handles MyBase.VisibleChanged
        Try
            If gbIndicaCierre Then
                Me.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvxPagosFacturas.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                CalcularMontosGrid()
            ElseIf e.KeyCode = Keys.Tab Then
                CalcularMontosGrid()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvxPagosFacturas.CellEndEdit
        Try
            CalcularMontosGrid()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try

    End Sub

    Private Sub TextBox12_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox12.Leave
        Try
            If TextBox12.Text.Trim().Length > 0 Then
                If IsNumeric(TextBox12.Text) = False Then
                    MsgBox("Tiene que ingresar valor n�merico a acreditarle al cliente.", MsgBoxStyle.Critical, "Error de Datos")
                    TextBox12.Focus()
                    Exit Sub
                End If
                dblMontoReciboNTDNTC = ConvierteADouble(TextBox12.Text)
                TextBox12.Text = Format(ConvierteADouble(dblMontoReciboNTDNTC), "#,##0.#0")
                RichTextBox1.Focus()
                dblMontoRecibo = ConvierteADouble(TextBox12.Text)
                If dgvxPagosFacturas.Rows.Count >= 1 Then
                    For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                        dblMontoRecibo = dblMontoRecibo - ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                    Next intIncr
                    Label15.Text = Format(dblMontoRecibo, "#,##0.#0")
                    If ConvierteADouble(Label15.Text) < 0 Then
                        Label15.ForeColor = System.Drawing.Color.Red
                    Else
                        Label15.ForeColor = System.Drawing.Color.Black
                    End If
                End If

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Notas de credito")
        End Try
    End Sub
End Class
