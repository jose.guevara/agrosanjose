Imports System.Windows.Forms
' Imports Microsoft.re
Imports CrystalDecisions.CrystalReports
' Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Entidades
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Utilidades

Public Class Frm_RPT_catalogoconta
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPT_catalogoconta"
    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load
        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty
        Dim lsConexion As String = String.Empty
        Dim sqlconexion As SqlConnection = Nothing 'conexi�n
        Dim sql_catalogo As String = String.Empty
        Dim DACatalogo As SqlDataAdapter = Nothing
        Dim report_catalogo As RPT_catalogoconta = Nothing
        Dim dset_catalogo As DS_Catalogo = Nothing
        Dim report_catalogo_conta As New RPT_catalogoconta

        Try
            dset_catalogo = New DS_Catalogo
            lsConexion = RNConeccion.ObtieneCadenaConexion()
            'sqlconexion = New SqlConnection(SIMF_CONTA.ConnectionStringconta)
            sqlconexion = New SqlConnection(lsConexion)
            sql_catalogo = "select * from catalogoconta"

            DACatalogo = New SqlDataAdapter(sql_catalogo, sqlconexion)
            DACatalogo.Fill(dset_catalogo, "catalogoconta")


            ' instanciar el objeto informe
            report_catalogo = New RPT_catalogoconta
            'For i = 0 To val_max - 1
            '    If Not SIMF_CONTA.formula(i) = Nothing Then
            '        jackcrystal.retorna_parametro(nombreformula, valor, i)
            '        report_catalogo_conta.SetParameterValue(nombreformula, valor)
            '    End If
            'Next

            'report_catalogo.SetDatabaseLogon(SIMF_CONTA.usuario, SIMF_CONTA.pws, SIMF_CONTA.server, SIMF_CONTA.base)
            ' ddd.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            report_catalogo.SetDataSource(dset_catalogo)
            report_catalogo.SetParameterValue("compania", SIMF_CONTA.formula(0))
            ' establecer la f�rmula de selecci�n de registros
            report_catalogo.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_catalogo
            Me.CrystalReportViewer1.Refresh()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("agregar_cuenta - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
End Class