﻿Imports System.Windows.Forms

Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports CrystalDecisions.Shared

Public Class Frm_RPT_BGER
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Dim jackreportes As New ReportesConta

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "Frm_RPT_BGER"

    '    select substring(CatalogoConta.cdgocuenta,1,5) as 'cuenta',CataNivel2.desccuenta as 'descuenta',sum(saldoinilocal) as 'saldoinilocal', 
    'SUM(debelocal)  as 'debelocal',SUM(haberlocal) as 'haberlocal',SUM(saldofinlocal) as 'saldofinlocal' from catalogoconta  inner join CataNivel2 
    'on substring(CatalogoConta.cdgocuenta,1,5)=CataNivel2.cdgocuenta  where tipo = 2 group by substring(CatalogoConta.cdgocuenta,1,5),CataNivel2.desccuenta 
    '  having not (sum(saldoinilocal)=0 AND SUM(debelocal)=0 or SUM(haberlocal)=0 AND SUM(saldofinlocal)=0 AND SUM(saldoiniext)=0 
    'and SUM(debeext)=0 AND SUM(haberext)=0 AND SUM(saldofinext)=0) order by substring(CatalogoConta.cdgocuenta,1,5)

    Private Sub CrystalReportViewer1_Load(sender As System.Object, e As System.EventArgs) Handles CrystalReportViewer1.Load

        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim formula As String = String.Empty
        Dim report_BG_form As New RPT_BG_form
        Dim sql_reader_report_formulas As SqlDataReader = Nothing
        Try
            ' sql_reader_report_formulas = jackconsulta.RetornaReader("select * from report_formulas where Cdgo_reporte = 'RPT_BG_form'", True)
            sql_reader_report_formulas = RNCuentaContable.ObtenerFormulasReportes("RPT_BG_form")
            If sql_reader_report_formulas.HasRows Then
                While sql_reader_report_formulas.Read
                    nombreformula = sql_reader_report_formulas!nomb_formula
                    Dim clave As String = sql_reader_report_formulas!suma_formula
                    Dim inertir_signo As String = IIf(IsDBNull(sql_reader_report_formulas!Mod_Signo), False, sql_reader_report_formulas!Mod_Signo)

                    If nombreformula.Contains("_SI") Then
                        valor = jackreportes.Report_Damemonto_form(clave, False, inertir_signo, "SI")
                    Else
                        valor = jackreportes.Report_Damemonto_form(clave, False, inertir_signo, "SF")
                    End If

                    report_BG_form.SetParameterValue(nombreformula, valor)

                End While

            End If
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_BG_form.SetParameterValue(nombreformula, valor)

                End If
            Next
            ' report_BG_form.SetParameterValue()

            report_BG_form.RecordSelectionFormula = ""
            ' asignar el objeto informe al control visualizado
            Me.CrystalReportViewer1.ReportSource = report_BG_form
            Me.CrystalReportViewer1.Refresh()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
End Class