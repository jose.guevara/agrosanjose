/****** Object:  Table [dbo].[tbl_ArchivosImp]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ArchivosImp]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ArchivosImp]
GO
/****** Object:  Table [dbo].[cat_Generos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_Generos]') AND type in (N'U'))
DROP TABLE [dbo].[cat_Generos]
GO
/****** Object:  Table [dbo].[tbl_CierreAnual]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CierreAnual]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_CierreAnual]
GO
/****** Object:  Table [dbo].[cat_EstadoCivil]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_EstadoCivil]') AND type in (N'U'))
DROP TABLE [dbo].[cat_EstadoCivil]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptTipoPrecios]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptTipoPrecios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptTipoPrecios]
GO
/****** Object:  Table [dbo].[cat_Identificacion]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_Identificacion]') AND type in (N'U'))
DROP TABLE [dbo].[cat_Identificacion]
GO
/****** Object:  Table [dbo].[prm_MesCierre]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_MesCierre]') AND type in (N'U'))
DROP TABLE [dbo].[prm_MesCierre]
GO
/****** Object:  Table [dbo].[prm_UsuarAgenc]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuarAgenc]') AND type in (N'U'))
DROP TABLE [dbo].[prm_UsuarAgenc]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProductoMovim]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProductoMovim]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngProductoMovim]
GO
/****** Object:  Table [dbo].[tmp_Presupuestos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_Presupuestos]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_Presupuestos]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptEstadisticas]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptEstadisticas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptEstadisticas]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptInventario]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptInventario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptInventario]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptPresupuestos]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptPresupuestos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptPresupuestos]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptPresupuestos2]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptPresupuestos2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptPresupuestos2]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptPresupuestos3]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptPresupuestos3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptPresupuestos3]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngChequesMovimientos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngChequesMovimientos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngChequesMovimientos]
GO
/****** Object:  StoredProcedure [dbo].[sp_Bitacora]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Bitacora]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Bitacora]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptCheques]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptCheques]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptCheques]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptOtrosModulos]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptOtrosModulos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptOtrosModulos]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptCuentasCobrar]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptCuentasCobrar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptCuentasCobrar]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptFacturas]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptFacturas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptFacturas]
GO
/****** Object:  StoredProcedure [dbo].[sp_UbicarFacturas]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UbicarFacturas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_UbicarFacturas]
GO
/****** Object:  StoredProcedure [dbo].[sp_Reportes]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reportes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Reportes]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPuestos]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPuestos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngPuestos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosDet]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosDet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngRecibosDet]
GO
/****** Object:  Table [dbo].[tbl_Cheques_Det]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cheques_Det]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Cheques_Det]
GO
/****** Object:  StoredProcedure [dbo].[sp_RptComisiones]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptComisiones]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_RptComisiones]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosEnc]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosEnc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngRecibosEnc]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngClientes]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngClientes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngClientes]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngClases]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngClases]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngClases]
GO
/****** Object:  StoredProcedure [dbo].[sp_PrcEstadoInventario]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PrcEstadoInventario]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PrcEstadoInventario]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngBeneficiarios]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngBeneficiarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngBeneficiarios]
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerFactura]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerFactura]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ExtraerFactura]
GO
/****** Object:  StoredProcedure [dbo].[sp_General]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_General]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_General]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProductos]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProductos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngProductos]
GO
/****** Object:  StoredProcedure [dbo].[sp_DetalleProducto]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DetalleProducto]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DetalleProducto]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngDepartamentos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngDepartamentos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngDepartamentos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngEmpaques]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngEmpaques]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngEmpaques]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngEmpresa]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngEmpresa]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngEmpresa]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngNegocios]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngNegocios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngNegocios]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngLetrasCambio]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngLetrasCambio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngLetrasCambio]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProveedores]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProveedores]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngProveedores]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCtasContables]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCtasContables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngCtasContables]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPorcentajes]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPorcentajes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngPorcentajes]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngBitacora]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngBitacora]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngBitacora]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUnidades]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUnidades]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngUnidades]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUsuariosPermisos]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUsuariosPermisos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngUsuariosPermisos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUsuarios]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUsuarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngUsuarios]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngAgencias]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngAgencias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngAgencias]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngVendDept]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngVendDept]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngVendDept]
GO
/****** Object:  StoredProcedure [dbo].[sp_VerificarSaldoCliente]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerificarSaldoCliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_VerificarSaldoCliente]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUsuariosAgencias]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUsuariosAgencias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngUsuariosAgencias]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngFacturasEnc]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngFacturasEnc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngFacturasEnc]
GO
/****** Object:  StoredProcedure [dbo].[sp_ExportarArchivos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExportarArchivos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ExportarArchivos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosAnular]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosAnular]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngRecibosAnular]
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerReciboRpt]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerReciboRpt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ExtraerReciboRpt]
GO
/****** Object:  Table [dbo].[tmp_InventarioAgencia]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_InventarioAgencia]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_InventarioAgencia]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosVarios]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosVarios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngRecibosVarios]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngVendedores]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngVendedores]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngVendedores]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngFacturasCorr]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngFacturasCorr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngFacturasCorr]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngFacturasDet]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngFacturasDet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngFacturasDet]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngConversiones]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngConversiones]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngConversiones]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProductoConv]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProductoConv]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngProductoConv]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCtasContRecibos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCtasContRecibos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngCtasContRecibos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCambiarNumero]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCambiarNumero]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngCambiarNumero]
GO
/****** Object:  StoredProcedure [dbo].[sp_NumFactura]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_NumFactura]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_NumFactura]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngMunicipios]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngMunicipios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngMunicipios]
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerFactPend]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerFactPend]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ExtraerFactPend]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngComisiones]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngComisiones]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngComisiones]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngTipoCambio]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngTipoCambio]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngTipoCambio]
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerAbonosSubRpt]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerAbonosSubRpt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_ExtraerAbonosSubRpt]
GO
/****** Object:  StoredProcedure [dbo].[sp_DetalleProductoFactura]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DetalleProductoFactura]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DetalleProductoFactura]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngLiqProdEnc]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngLiqProdEnc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngLiqProdEnc]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCambioPrecios]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCambioPrecios]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngCambioPrecios]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRazonesAnular]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRazonesAnular]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngRazonesAnular]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngSubClases]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngSubClases]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngSubClases]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngAreas]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngAreas]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngAreas]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngLiqProdDet]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngLiqProdDet]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngLiqProdDet]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngChequeras]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngChequeras]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngChequeras]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPresupuestos]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPresupuestos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngPresupuestos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCtasBancarias]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCtasBancarias]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngCtasBancarias]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngBancos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngBancos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngBancos]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngEmpleados]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngEmpleados]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngEmpleados]
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPorcentajes2]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPorcentajes2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_IngPorcentajes2]
GO
/****** Object:  StoredProcedure [dbo].[sp_PrcFacturasImp]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PrcFacturasImp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PrcFacturasImp]
GO
/****** Object:  StoredProcedure [dbo].[sp_PrcCierreAnual]    Script Date: 05/10/2009 21:20:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PrcCierreAnual]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_PrcCierreAnual]
GO
/****** Object:  Table [dbo].[tbl_Producto_Movimiento]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Producto_Movimiento]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Producto_Movimiento]
GO
/****** Object:  Table [dbo].[prm_Agencias]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Agencias]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Agencias]
GO
/****** Object:  Table [dbo].[prm_Productos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Productos]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Productos]
GO
/****** Object:  Table [dbo].[prm_SubClases]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_SubClases]') AND type in (N'U'))
DROP TABLE [dbo].[prm_SubClases]
GO
/****** Object:  Table [dbo].[prm_Negocios]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Negocios]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Negocios]
GO
/****** Object:  Table [dbo].[prm_Departamentos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Departamentos]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Departamentos]
GO
/****** Object:  Table [dbo].[prm_Clases]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Clases]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Clases]
GO
/****** Object:  Table [dbo].[prm_Clientes]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Clientes]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Clientes]
GO
/****** Object:  Table [dbo].[prm_Vendedores]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Vendedores]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Vendedores]
GO
/****** Object:  Table [dbo].[prm_TipoCambio]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_TipoCambio]') AND type in (N'U'))
DROP TABLE [dbo].[prm_TipoCambio]
GO
/****** Object:  Table [dbo].[prm_TipoMov]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_TipoMov]') AND type in (N'U'))
DROP TABLE [dbo].[prm_TipoMov]
GO
/****** Object:  Table [dbo].[prm_Unidades]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Unidades]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Unidades]
GO
/****** Object:  Table [dbo].[prm_Proveedores]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Proveedores]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Proveedores]
GO
/****** Object:  Table [dbo].[tbl_Cheques_Enc]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cheques_Enc]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Cheques_Enc]
GO
/****** Object:  Table [dbo].[prm_Bancos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Bancos]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Bancos]
GO
/****** Object:  Table [dbo].[prm_CtasBancarias]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasBancarias]') AND type in (N'U'))
DROP TABLE [dbo].[prm_CtasBancarias]
GO
/****** Object:  Table [dbo].[cat_Monedas]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_Monedas]') AND type in (N'U'))
DROP TABLE [dbo].[cat_Monedas]
GO
/****** Object:  Table [dbo].[tbl_Cliente_Oblig]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cliente_Oblig]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Cliente_Oblig]
GO
/****** Object:  Table [dbo].[tbl_Recibos_Det]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Det]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Recibos_Det]
GO
/****** Object:  Table [dbo].[tbl_Recibos_Enc]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Enc]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Recibos_Enc]
GO
/****** Object:  Table [dbo].[prm_Puestos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Puestos]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Puestos]
GO
/****** Object:  Table [dbo].[prm_Usuarios]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Usuarios]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Usuarios]
GO
/****** Object:  Table [dbo].[prm_UsuariosAgencias]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuariosAgencias]') AND type in (N'U'))
DROP TABLE [dbo].[prm_UsuariosAgencias]
GO
/****** Object:  Table [dbo].[prm_Porcentajes]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Porcentajes]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Porcentajes]
GO
/****** Object:  Table [dbo].[tbl_Facturas_Enc]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Enc]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Facturas_Enc]
GO
/****** Object:  Table [dbo].[prm_Empaques]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empaques]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Empaques]
GO
/****** Object:  Table [dbo].[prm_Comisiones]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Comisiones]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Comisiones]
GO
/****** Object:  Table [dbo].[tmp_EstadoInvent]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_EstadoInvent]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_EstadoInvent]
GO
/****** Object:  Table [dbo].[prm_Beneficiarios]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Beneficiarios]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Beneficiarios]
GO
/****** Object:  Table [dbo].[tbl_Facturas_Det]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Det]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Facturas_Det]
GO
/****** Object:  Table [dbo].[prm_Empresa]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empresa]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Empresa]
GO
/****** Object:  Table [dbo].[prm_LetrasCambio]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_LetrasCambio]') AND type in (N'U'))
DROP TABLE [dbo].[prm_LetrasCambio]
GO
/****** Object:  Table [dbo].[prm_CtasContables]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasContables]') AND type in (N'U'))
DROP TABLE [dbo].[prm_CtasContables]
GO
/****** Object:  Table [dbo].[tbl_Bitacora]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Bitacora]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Bitacora]
GO
/****** Object:  Table [dbo].[prm_UsuariosPermisos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuariosPermisos]') AND type in (N'U'))
DROP TABLE [dbo].[prm_UsuariosPermisos]
GO
/****** Object:  Table [dbo].[prm_VendDept]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_VendDept]') AND type in (N'U'))
DROP TABLE [dbo].[prm_VendDept]
GO
/****** Object:  Table [dbo].[tbl_RecibosVarios]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_RecibosVarios]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_RecibosVarios]
GO
/****** Object:  Table [dbo].[tbl_Facturas_Corregidas]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Corregidas]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Facturas_Corregidas]
GO
/****** Object:  Table [dbo].[prm_Conversiones]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Conversiones]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Conversiones]
GO
/****** Object:  Table [dbo].[tbl_ProductoConv]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProductoConv]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_ProductoConv]
GO
/****** Object:  Table [dbo].[prm_CtasContRecibos]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasContRecibos]') AND type in (N'U'))
DROP TABLE [dbo].[prm_CtasContRecibos]
GO
/****** Object:  Table [dbo].[tbl_UltimoNumero]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UltimoNumero]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_UltimoNumero]
GO
/****** Object:  Table [dbo].[tbl_CambiarNumFact]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CambiarNumFact]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_CambiarNumFact]
GO
/****** Object:  Table [dbo].[prm_Municipios]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Municipios]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Municipios]
GO
/****** Object:  Table [dbo].[tbl_LiqProd_Enc]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LiqProd_Enc]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LiqProd_Enc]
GO
/****** Object:  Table [dbo].[tbl_CambiarPrecios]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CambiarPrecios]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_CambiarPrecios]
GO
/****** Object:  Table [dbo].[prm_Razones_Anular]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Razones_Anular]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Razones_Anular]
GO
/****** Object:  Table [dbo].[prm_Areas]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Areas]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Areas]
GO
/****** Object:  Table [dbo].[tbl_LiqProd_Det]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LiqProd_Det]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_LiqProd_Det]
GO
/****** Object:  Table [dbo].[prm_Chequeras]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Chequeras]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Chequeras]
GO
/****** Object:  Table [dbo].[tbl_Presupuestos]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Presupuestos]') AND type in (N'U'))
DROP TABLE [dbo].[tbl_Presupuestos]
GO
/****** Object:  Table [dbo].[prm_Empleados]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empleados]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Empleados]
GO
/****** Object:  Table [dbo].[prm_Porcentajes2]    Script Date: 05/10/2009 21:20:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Porcentajes2]') AND type in (N'U'))
DROP TABLE [dbo].[prm_Porcentajes2]
GO
/****** Object:  Table [dbo].[tmp_FacturasImp]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_FacturasImp]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_FacturasImp]
GO
/****** Object:  Table [dbo].[tmp_CierreAnualDif]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_CierreAnualDif]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_CierreAnualDif]
GO
/****** Object:  Table [dbo].[tmp_CierreAnual]    Script Date: 05/10/2009 21:20:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_CierreAnual]') AND type in (N'U'))
DROP TABLE [dbo].[tmp_CierreAnual]
GO
/****** Object:  User [Agro2K]    Script Date: 05/10/2009 21:20:43 ******/
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Agro2K')
DROP USER [Agro2K]
GO
/****** Object:  User [Agro2K]    Script Date: 05/10/2009 21:20:43 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'Agro2K')
CREATE USER [Agro2K] FOR LOGIN [Agro2K] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[prm_UsuariosAgencias]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuariosAgencias]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_UsuariosAgencias](
	[usrregistro] [tinyint] NOT NULL,
	[agenregistro] [tinyint] NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuariosAgencias]') AND name = N'IX_prm_UsuariosAgencias')
CREATE CLUSTERED INDEX [IX_prm_UsuariosAgencias] ON [dbo].[prm_UsuariosAgencias] 
(
	[usrregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tmp_FacturasImp]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_FacturasImp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_FacturasImp](
	[registro] [bigint] NOT NULL,
	[numero] [varchar](12) NOT NULL,
	[numfechaing] [int] NOT NULL,
	[fechaingreso] [varchar](12) NOT NULL,
	[vendcodigo] [varchar](12) NOT NULL,
	[clicodigo] [varchar](12) NOT NULL,
	[clinombre] [varchar](80) NOT NULL,
	[subtotal] [numeric](12, 2) NOT NULL,
	[impuesto] [numeric](12, 2) NOT NULL,
	[retencion] [numeric](12, 2) NOT NULL,
	[total] [numeric](12, 2) NOT NULL,
	[codusuario] [varchar](12) NOT NULL,
	[agencodigo] [tinyint] NOT NULL,
	[status] [tinyint] NOT NULL,
	[impreso] [tinyint] NOT NULL,
	[tipofactura] [tinyint] NOT NULL,
	[diascredito] [smallint] NOT NULL,
	[numfechaanul] [int] NOT NULL,
	[prodcodigo] [varchar](12) NOT NULL,
	[proddescrip] [varchar](80) NOT NULL,
	[cantidad] [numeric](12, 2) NOT NULL,
	[precio] [numeric](12, 2) NOT NULL,
	[valor] [numeric](12, 2) NOT NULL,
	[iva] [numeric](12, 2) NOT NULL,
	[registrodet] [smallint] NOT NULL,
	[registroerror] [varchar](200) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tmp_FacturasImp]') AND name = N'IX_tmp_FacturasImp')
CREATE CLUSTERED INDEX [IX_tmp_FacturasImp] ON [dbo].[tmp_FacturasImp] 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Cliente_Oblig]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cliente_Oblig]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Cliente_Oblig](
	[registro] [int] NOT NULL,
	[cliregistro] [smallint] NOT NULL,
	[vendregistro] [tinyint] NOT NULL,
	[numero] [varchar](20) NOT NULL,
	[fechaing] [varchar](12) NOT NULL,
	[fechaven] [varchar](12) NOT NULL,
	[monto] [numeric](18, 2) NOT NULL,
	[abono] [numeric](18, 2) NOT NULL,
	[saldo] [numeric](18, 2) NOT NULL,
	[estado] [tinyint] NOT NULL,
	[numfechaing] [int] NOT NULL,
	[numfechaven] [int] NOT NULL,
	[datefechaing] [smalldatetime] NOT NULL,
	[datefechaven] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_tbl_Cliente_Oblig] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cliente_Oblig]') AND name = N'IX_tbl_Cliente_Oblig')
CREATE CLUSTERED INDEX [IX_tbl_Cliente_Oblig] ON [dbo].[tbl_Cliente_Oblig] 
(
	[cliregistro] ASC,
	[numero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CierreAnual]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CierreAnual]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_CierreAnual](
	[years] [smallint] NOT NULL,
	[agenregistro] [tinyint] NOT NULL,
	[prodregistro] [smallint] NOT NULL,
	[cantidad] [numeric](18, 2) NOT NULL,
	[costo] [numeric](18, 2) NOT NULL,
	[monto] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[cat_EstadoCivil]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_EstadoCivil]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cat_EstadoCivil](
	[registro] [tinyint] NOT NULL,
	[descripcion] [varchar](16) NOT NULL,
 CONSTRAINT [PK_cat_EstadoCivil] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_cat_EstadoCivil] UNIQUE NONCLUSTERED 
(
	[descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Clases]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Clases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Clases](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Clases] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Clases]') AND name = N'IX_prm_Clases')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Clases] ON [dbo].[prm_Clases] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_ArchivosImp]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ArchivosImp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ArchivosImp](
	[archivo] [varchar](20) NOT NULL,
	[fechaingreso] [varchar](12) NOT NULL,
	[numfechaing] [int] NOT NULL,
	[usrregistro] [tinyint] NOT NULL,
 CONSTRAINT [PK_tbl_ArchivosImp] PRIMARY KEY CLUSTERED 
(
	[archivo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cat_Generos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_Generos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cat_Generos](
	[registro] [tinyint] NOT NULL,
	[descripcion] [varchar](12) NOT NULL,
 CONSTRAINT [PK_cat_Generos] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_cat_Generos] UNIQUE NONCLUSTERED 
(
	[descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Productos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Productos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Productos](
	[registro] [smallint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](65) NOT NULL,
	[regclase] [tinyint] NOT NULL,
	[regsubclase] [tinyint] NOT NULL,
	[regunidad] [tinyint] NOT NULL,
	[regempaque] [tinyint] NOT NULL,
	[regproveedor] [tinyint] NOT NULL,
	[pvpc] [numeric](10, 2) NOT NULL,
	[pvdc] [numeric](10, 2) NOT NULL,
	[pvpu] [numeric](10, 2) NOT NULL,
	[pvdu] [numeric](10, 2) NOT NULL,
	[impuesto] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Productos] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Productos]') AND name = N'IX_prm_Productos')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Productos] ON [dbo].[prm_Productos] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cat_Identificacion]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_Identificacion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cat_Identificacion](
	[registro] [tinyint] NOT NULL,
	[descripcion] [varchar](20) NOT NULL,
 CONSTRAINT [PK_cat_Identificacion] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_cat_Identificacion] UNIQUE NONCLUSTERED 
(
	[descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Departamentos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Departamentos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Departamentos](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[codcorto] [char](1) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Departamentos] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Departamentos]') AND name = N'IX_prm_Departamentos')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Departamentos] ON [dbo].[prm_Departamentos] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Empaques]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empaques]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Empaques](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Empaques] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empaques]') AND name = N'IX_prm_Empaques')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Empaques] ON [dbo].[prm_Empaques] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_MesCierre]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_MesCierre]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_MesCierre](
	[agenregistro] [tinyint] NOT NULL,
	[years_months] [int] NOT NULL,
 CONSTRAINT [PK_prm_MesCierre] PRIMARY KEY CLUSTERED 
(
	[agenregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[prm_Negocios]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Negocios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Negocios](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[estado] [tinyint] NOT NULL,
	[dias] [int] NOT NULL CONSTRAINT [DF_prm_Negocios_dias]  DEFAULT (0),
	[negociable] [tinyint] NOT NULL CONSTRAINT [DF_prm_Negocios_negociable]  DEFAULT (0),
	[maximo] [int] NOT NULL CONSTRAINT [DF_prm_Negocios_maximo]  DEFAULT (0),
 CONSTRAINT [PK_prm_Negocios] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Negocios]') AND name = N'IX_prm_Negocios')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Negocios] ON [dbo].[prm_Negocios] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_LetrasCambio]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_LetrasCambio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_LetrasCambio](
	[orden] [varchar](40) NOT NULL,
	[nombre] [varchar](40) NOT NULL,
	[tasainteres] [numeric](5, 2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Proveedores]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Proveedores]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Proveedores](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[nombre] [varchar](40) NOT NULL,
	[tipo] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Proveedores] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Proveedores]') AND name = N'IX_prm_Proveedores')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Proveedores] ON [dbo].[prm_Proveedores] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Porcentajes]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Porcentajes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Porcentajes](
	[porc_iva] [numeric](8, 2) NOT NULL CONSTRAINT [DF_prm_Porcentajes_porc_iva]  DEFAULT (0),
	[porc_ret] [numeric](8, 2) NOT NULL CONSTRAINT [DF_prm_Porcentajes_porc_ret]  DEFAULT (0),
	[dias_validez] [numeric](3, 0) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[prm_TipoMov]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_TipoMov]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_TipoMov](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](3) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_TipoMov] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_TipoMov]') AND name = N'IX_prm_TipoMov')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_TipoMov] ON [dbo].[prm_TipoMov] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Bitacora]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Bitacora]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Bitacora](
	[tabla] [tinyint] NOT NULL,
	[registro] [int] NOT NULL,
	[fecha] [varchar](12) NOT NULL,
	[tiempo] [varchar](12) NOT NULL,
	[campo] [varchar](15) NOT NULL,
	[antes] [varchar](65) NOT NULL,
	[despues] [varchar](65) NOT NULL,
	[numfecha] [int] NOT NULL,
	[numtiempo] [int] NOT NULL,
	[codusuario] [varchar](12) NOT NULL,
	[regusuario] [tinyint] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Bitacora]') AND name = N'IX_tbl_Bitacora')
CREATE CLUSTERED INDEX [IX_tbl_Bitacora] ON [dbo].[tbl_Bitacora] 
(
	[tabla] ASC,
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Unidades]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Unidades]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Unidades](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Unidades] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Unidades]') AND name = N'IX_prm_Unidades')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Unidades] ON [dbo].[prm_Unidades] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_UsuarAgenc]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuarAgenc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_UsuarAgenc](
	[regusuario] [tinyint] NOT NULL,
	[regagencia] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_UsuarAgenc] PRIMARY KEY CLUSTERED 
(
	[regusuario] ASC,
	[regagencia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[prm_Usuarios]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Usuarios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Usuarios](
	[registro] [tinyint] NOT NULL,
	[codusuario] [varchar](12) NOT NULL,
	[nombre] [varchar](30) NOT NULL,
	[apellido] [varchar](30) NOT NULL,
	[direccion] [varchar](100) NOT NULL,
	[telefono] [varchar](16) NOT NULL,
	[cargo] [varchar](30) NOT NULL,
	[clave] [varchar](12) NOT NULL,
	[estado] [tinyint] NOT NULL,
	[impresora] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Usuarios] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Usuarios]') AND name = N'IX_prm_Usuarios')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Usuarios] ON [dbo].[prm_Usuarios] 
(
	[codusuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Agencias]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Agencias]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Agencias](
	[registro] [tinyint] NOT NULL,
	[codigo] [tinyint] NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[traslados] [tinyint] NOT NULL,
	[defecto] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
	[facturas] [tinyint] NOT NULL,
	[fechacambiofactura] [int] NOT NULL,
	[deptregistro] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Agencias] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Agencias]') AND name = N'IX_prm_Agencias')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Agencias] ON [dbo].[prm_Agencias] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_VendDept]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_VendDept]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_VendDept](
	[regvendedor] [tinyint] NOT NULL,
	[regdepartamento] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_VendDept] PRIMARY KEY CLUSTERED 
(
	[regvendedor] ASC,
	[regdepartamento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[prm_Clientes]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Clientes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Clientes](
	[registro] [smallint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[tipocli] [tinyint] NOT NULL,
	[tipo_id] [tinyint] NOT NULL,
	[num_id] [varchar](20) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[fechaingreso] [varchar](12) NOT NULL,
	[numfechaing] [numeric](8, 0) NOT NULL,
	[direccion] [varchar](80) NOT NULL,
	[negregistro] [tinyint] NOT NULL,
	[vendregistro] [tinyint] NOT NULL,
	[deptregistro] [tinyint] NOT NULL,
	[munregistro] [tinyint] NOT NULL,
	[telefono] [varchar](30) NOT NULL,
	[fax] [varchar](30) NOT NULL,
	[correo] [varchar](30) NOT NULL,
	[negocio] [varchar](50) NOT NULL,
	[negdirec] [varchar](80) NOT NULL,
	[contacto01] [varchar](50) NOT NULL,
	[contacto02] [varchar](50) NOT NULL,
	[limite] [numeric](18, 2) NOT NULL,
	[saldo] [numeric](18, 2) NOT NULL,
	[dias_credito] [int] NOT NULL,
	[cta_contable] [varchar](16) NOT NULL,
	[estado] [tinyint] NOT NULL,
	[saldofavor] [numeric](18, 2) NOT NULL CONSTRAINT [DF_prm_Clientes_sld_fvr]  DEFAULT (0),
 CONSTRAINT [PK_prm_Clientes] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Clientes]') AND name = N'IX_prm_Clientes')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Clientes] ON [dbo].[prm_Clientes] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RecibosVarios]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_RecibosVarios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_RecibosVarios](
	[registro] [bigint] NOT NULL,
	[ctacontable] [smallint] NOT NULL,
	[fechaing] [varchar](12) NOT NULL,
	[numfechaing] [int] NOT NULL,
	[numero] [varchar](12) NOT NULL,
	[monto] [numeric](12, 2) NOT NULL,
	[concepto] [text] NOT NULL,
	[estado] [tinyint] NOT NULL,
	[numfechaanul] [int] NOT NULL,
	[dtafechaing] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_tbl_RecibosVarios] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_RecibosVarios]') AND name = N'IX_tbl_RecibosVarios')
CREATE CLUSTERED INDEX [IX_tbl_RecibosVarios] ON [dbo].[tbl_RecibosVarios] 
(
	[ctacontable] ASC,
	[numero] ASC,
	[numfechaing] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Vendedores]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Vendedores]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Vendedores](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[localidad] [varchar](10) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Vendedores] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Vendedores]') AND name = N'IX_prm_Vendedores')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Vendedores] ON [dbo].[prm_Vendedores] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_UsuariosPermisos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuariosPermisos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_UsuariosPermisos](
	[cliregistro] [tinyint] NOT NULL,
	[permiso] [tinyint] NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_UsuariosPermisos]') AND name = N'IX_prm_UsuariosPermisos')
CREATE CLUSTERED INDEX [IX_prm_UsuariosPermisos] ON [dbo].[prm_UsuariosPermisos] 
(
	[cliregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Facturas_Det]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Det]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Facturas_Det](
	[registro] [bigint] NOT NULL,
	[prodregistro] [smallint] NOT NULL,
	[cantidad] [numeric](12, 2) NOT NULL,
	[precio] [numeric](12, 2) NOT NULL,
	[valor] [numeric](12, 2) NOT NULL,
	[igv] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Det]') AND name = N'IX_tbl_Facturas_Det')
CREATE CLUSTERED INDEX [IX_tbl_Facturas_Det] ON [dbo].[tbl_Facturas_Det] 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Conversiones]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Conversiones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Conversiones](
	[registro] [tinyint] NOT NULL,
	[de_cantidad] [numeric](12, 2) NOT NULL,
	[de_unidad] [tinyint] NOT NULL,
	[a_cantidad] [numeric](12, 2) NOT NULL,
	[a_unidad] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Conversiones] PRIMARY KEY CLUSTERED 
(
	[de_unidad] ASC,
	[a_unidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Conversiones]') AND name = N'IX_prm_Conversiones')
CREATE NONCLUSTERED INDEX [IX_prm_Conversiones] ON [dbo].[prm_Conversiones] 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tmp_CierreAnual]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_CierreAnual]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_CierreAnual](
	[agenregistro] [tinyint] NOT NULL,
	[prodcodigo] [varchar](12) NOT NULL,
	[proddescrip] [varchar](80) NOT NULL,
	[produnidad] [varchar](12) NOT NULL,
	[cantidad] [numeric](18, 2) NOT NULL,
	[costo] [numeric](18, 2) NOT NULL,
	[monto] [numeric](18, 2) NOT NULL,
	[doccierre] [varchar](12) NOT NULL,
	[docinicial] [varchar](12) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_ProductoConv]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProductoConv]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_ProductoConv](
	[registro] [bigint] NOT NULL,
	[fechaconversion] [varchar](12) NOT NULL,
	[numfecha] [numeric](8, 0) NOT NULL,
	[documento] [varchar](12) NOT NULL,
	[cantidad01] [numeric](10, 2) NOT NULL,
	[prodregistro01] [smallint] NOT NULL,
	[agenregistro01] [tinyint] NOT NULL,
	[cantidad02] [numeric](10, 2) NOT NULL,
	[prodregistro02] [smallint] NOT NULL,
	[agenregistro02] [tinyint] NOT NULL,
 CONSTRAINT [PK_tbl_ProductoConv] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ProductoConv]') AND name = N'IX_tbl_ProductoConv')
CREATE NONCLUSTERED INDEX [IX_tbl_ProductoConv] ON [dbo].[tbl_ProductoConv] 
(
	[documento] ASC,
	[numfecha] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_CtasContRecibos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasContRecibos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_CtasContRecibos](
	[RecCajCaj] [smallint] NOT NULL,
	[RecCajInt] [smallint] NOT NULL,
	[RecCajRet] [smallint] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[cat_Monedas]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cat_Monedas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[cat_Monedas](
	[registro] [tinyint] NOT NULL,
	[descripcion] [varchar](12) NOT NULL,
	[signo] [varchar](4) NOT NULL,
	[descripcionc] [varchar](6) NOT NULL,
 CONSTRAINT [PK_cat_Monedas] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmp_CierreAnualDif]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_CierreAnualDif]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_CierreAnualDif](
	[agencia] [tinyint] NOT NULL,
	[prodcodigo] [varchar](12) NOT NULL,
	[cntfisico] [numeric](18, 2) NOT NULL,
	[cntsistema] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_UltimoNumero]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_UltimoNumero]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_UltimoNumero](
	[agenregistro] [tinyint] NOT NULL,
	[contado] [int] NOT NULL,
	[credito] [int] NOT NULL,
 CONSTRAINT [PK_tbl_UltimoNumero] PRIMARY KEY CLUSTERED 
(
	[agenregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tmp_Presupuestos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_Presupuestos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_Presupuestos](
	[codigo] [varchar](12) NULL,
	[enero] [varchar](12) NULL,
	[febrero] [varchar](12) NULL,
	[marzo] [varchar](12) NULL,
	[abril] [varchar](12) NULL,
	[mayo] [varchar](12) NULL,
	[junio] [varchar](12) NULL,
	[julio] [varchar](12) NULL,
	[agosto] [varchar](12) NULL,
	[septiembre] [varchar](12) NULL,
	[octubre] [varchar](12) NULL,
	[noviembre] [varchar](12) NULL,
	[diciembre] [varchar](12) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Puestos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Puestos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Puestos](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Puestos] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Puestos] UNIQUE CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_RptPresupuestos]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptPresupuestos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_RptPresupuestos] @intTipoReporte as tinyint, @lngYears as smallint, @strVendedores as varchar(100), @strClases as varchar(100), 
			@strSubClases as varchar(100), @strFecIni as varchar(100), @strFecFin as varchar(100), @intCantMonet as tinyint

As

Declare	@strComando01	varchar(200),
	@strComando02		varchar(200),
	@strComando03		varchar(200),
	@strComando04		varchar(200),
	@strComando05		varchar(200),
	@strComando06		varchar(200),
	@strComando07		varchar(200),
	@strComando08		varchar(200),
	@strComando09		varchar(200),
	@strComando10		varchar(200),
	@strComando11		varchar(200),
	@strComando12		varchar(200),
	@dblPorcentaje		numeric(18,2),
	@VendRegistro		tinyint,
	@ProdRegistro		smallint,
	@months				tinyint,
	@valor				numeric(18,2),
	@claseregistro		tinyint,
	@mto_real_vtas		numeric(18,2),
	@mto_real_prsp		numeric(18,2),
	@clase_regist		tinyint,
	@clase_descrip		varchar(60),
	@lista_vend			varchar(200),
	@codigoclase		varchar(12),
	@codigoagencia		varchar(12),
	@porcent_grupo		numeric(18,2)

begin
	set nocount on
	select @strComando01 = ''''
	select @strComando02 = ''''
	select @strComando03 = ''''
	select @strComando04 = ''''
	select @strComando05 = ''''
	select @strComando06 = ''''
	select @strComando07 = ''''
	select @strComando08 = ''''
	select @strComando09 = ''''
	if @intTipoReporte = 1				-- Vendedores y sus Presupuestos
		begin
			set @strComando01 = ''select years, v.codigo, v.nombre, p.codigo, p.descripcion, t.prodprecio, c.codigo, c.descripcion, ''
			if @intCantMonet = 0
				begin
					set @strComando02 = ''t.enero, t.febrero, t.marzo, t.abril, t.mayo, t.junio, t.julio, t.agosto, t.septiembre, t.octubre, t.noviembre, t.diciembre  ''
					set @strComando07 = ''order by t.years, v.codigo, c.codigo, p.codigo ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando02 = ''(t.enero * t.prodprecio) enero, (t.febrero * t.prodprecio) febrero, (t.marzo * t.prodprecio) marzo, (t.abril * t.prodprecio) abril, ''
					set @strComando03 = ''(t.mayo * t.prodprecio) mayo, (t.junio * t.prodprecio) junio, (t.julio * t.prodprecio) julio, (t.agosto * t.prodprecio) agosto, ''
					set @strComando04 = ''(t.septiembre * t.prodprecio) septiembre, (t.octubre * t.prodprecio) octubre, (t.noviembre * t.prodprecio) noviembre, (t.diciembre * t.prodprecio) diciembre  ''
					set @strComando07 = ''order by t.years, v.codigo, c.codigo, p.codigo ''
				end
			set @strComando05 = ''from tbl_presupuestos t, prm_productos p, prm_clases c, prm_vendedores v where p.registro = t.prodregistro  ''
			set @strComando06 = ''and c.registro = p.regclase and v.registro = t.vendregistro and years = ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + 
				@lngYears + @strVendedores + @strClases + @strSubClases + @strComando07)
		end
	else if @intTipoReporte = 2			-- % de Cumplimiento Mensual del Presupuesto por Vendedor
		begin
			create table #tmpTabla01
			(
				years			smallint,
				enero			numeric(18,2),
				febrero			numeric(18,2),
				marzo			numeric(18,2),
				abril			numeric(18,2),
				mayo			numeric(18,2),
				junio			numeric(18,2),
				julio			numeric(18,2),
				agosto			numeric(18,2),
				septiembre		numeric(18,2),
				octubre			numeric(18,2),
				noviembre		numeric(18,2),
				diciembre		numeric(18,2),
				vendregistro	tinyint
			)
			set @strComando01 = ''insert into #tmpTabla01 select t.years, sum(t.enero * t.prodprecio) enero, sum(t.febrero * t.prodprecio) febrero, ''
			set @strComando02 = ''sum(t.marzo * t.prodprecio) marzo, sum(t.abril * t.prodprecio) abril, sum(t.mayo * prodprecio) mayo, sum(junio * t.prodprecio) junio, ''
			set @strComando03 = ''sum(t.julio * t.prodprecio) julio, sum(t.agosto * t.prodprecio) agosto, sum(t.septiembre * t.prodprecio) septiembre, ''
			set @strComando04 = ''sum(t.octubre * t.prodprecio) octubre, sum(t.noviembre * t.prodprecio) noviembre, sum(t.diciembre * t.prodprecio) diciembre, ''
			set @strComando05 = ''t.vendregistro from tbl_presupuestos t, prm_productos p where p.registro = t.prodregistro and t.years = ''
			set @strComando07 = ''group by t.years, t.vendregistro order by t.years, t.vendregistro''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @lngYears + @strVendedores + 
				@strClases + @strSubClases + @strComando07)

			create table #tmpTabla02
			(
				years			smallint,
				months			tinyint,
				vendregistro	tinyint,
				valor			numeric(18,2)
			)
			set @strComando01 = ''insert into #tmpTabla02 select convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)) years, convert(numeric(8), substring(convert(varchar(8), ''
			set @strComando02 = ''e.numfechaing), 5, 2)) months, e.vendregistro, sum(d.valor) valor from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos P, tbl_Presupuestos t where ''
			set @strComando03 = ''e.registro = d.registro and D.ProdRegistro = P.Registro and e.vendregistro = t.vendregistro and p.registro = t.prodregistro and d.prodregistro = t.prodregistro and e.status=0 ''
			set @strComando05 = ''group by convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)), convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 5, 2)), e.vendregistro ''
			set @strComando06 = ''order by convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)), convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 5, 2)), e.vendregistro''
			exec (@strComando01 + @strComando02 + @strComando03 + @strFecIni + @strFecFin + @strVendedores + @strClases + @strSubClases + 
				@strComando05 + @strComando06)
			create table #tmpTabla03
			(
				vendregistro	tinyint,
				years_presp		smallint,
				mes_presp		varchar(12),
				mes_presp02		tinyint,
				mto_presp		numeric(18,2),
				mto_vtas		numeric(18,2),
				por_global		numeric(18,2),
				por_global02	numeric(18,2)
			)

			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''enero'', 1, t1.enero, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''febrero'', 2, t1.febrero, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''marzo'', 3, t1.marzo, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''abril'', 4, t1.abril, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''mayo'', 5, t1.mayo, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''junio'', 6, t1.junio, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''julio'', 7, t1.julio, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''agosto'', 8, t1.agosto, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''septiembre'', 9, t1.septiembre, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''octubre'', 10, t1.octubre, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''noviembre'', 11, t1.noviembre, 0, 0, 0 from #tmptabla01 t1
			insert into #tmpTabla03 select t1.vendregistro, t1.years, ''diciembre'', 12, t1.diciembre, 0, 0, 0 from #tmptabla01 t1

			declare	tmpC cursor for
				select distinct mes_presp02, vendregistro from #tmpTabla03
			open tmpC
			fetch next from tmpC into @months, @claseregistro
			while @@fetch_status = 0
				begin
					set @valor = 0
					select @valor = isnull(sum(valor), 0) from #tmpTabla02 where months = @months and vendregistro = @claseregistro 
					update #tmpTabla03 set mto_vtas = @valor where mes_presp02 = @months and vendregistro = @claseregistro 
					fetch next from tmpC into @months, @claseregistro
				end
			close tmpC
			deallocate tmpC
			select distinct months into #tmptablaMV from #tmpTabla02
			declare	tmpC cursor for
				select distinct vendregistro from #tmpTabla03
			open tmpC
			fetch next from tmpC into @claseregistro
			while @@fetch_status = 0
				begin
					set @mto_real_vtas = 0
					set @mto_real_prsp = 0
					select @mto_real_vtas = isnull(sum(mto_vtas), 0) from #tmpTabla03 t, #tmptablaMV m where m.months = t.mes_presp02 and vendregistro = @claseregistro 
					select @mto_real_prsp = isnull(sum(mto_presp), 0) from #tmpTabla03 t, #tmptablaMV m where m.months = t.mes_presp02 and vendregistro = @claseregistro
					if @mto_real_vtas <> 0 and @mto_real_prsp <> 0
						begin
							update #tmpTabla03 set por_global = ((@mto_real_vtas / @mto_real_prsp) * 100) where vendregistro = @claseregistro 
						end
					fetch next from tmpC into @claseregistro
				end
			close tmpC
			deallocate tmpC
			select years_presp, mes_presp, vendregistro, nombre, mto_vtas, mto_presp,
				''%_Avance'' = 
					case 
						when mto_vtas <> 0 and mto_presp <> 0 then convert(numeric(10,2), ((mto_vtas / mto_presp) * 100))
						when mto_vtas = 0 and mto_presp <> 0 then convert(numeric(10,2), ((mto_vtas / mto_presp) * 100))
						else 0 end, 
				por_global 
				from #tmptabla03 t, prm_vendedores v, #tmptablaMV m where m.months = t.mes_presp02 and v.registro = t.vendregistro 
				order by years_presp, vendregistro, mes_presp02
		end
	else if @intTipoReporte = 3			-- % de Cumplimiento Mensual del Presupuesto por Clase
		begin
			create table #tmpTabla04
			(
				years			smallint,
				enero			numeric(18,2),
				febrero			numeric(18,2),
				marzo			numeric(18,2),
				abril			numeric(18,2),
				mayo			numeric(18,2),
				junio			numeric(18,2),
				julio			numeric(18,2),
				agosto			numeric(18,2),
				septiembre		numeric(18,2),
				octubre			numeric(18,2),
				noviembre		numeric(18,2),
				diciembre		numeric(18,2),
				claseregistro	tinyint
			)
			set @strComando01 = ''insert into #tmpTabla04 select t.years, sum(t.enero * t.prodprecio) enero, sum(t.febrero * t.prodprecio) febrero, ''
			set @strComando02 = ''sum(t.marzo * t.prodprecio) marzo, sum(t.abril * t.prodprecio) abril, sum(t.mayo * prodprecio) mayo, sum(junio * t.prodprecio) junio, ''
			set @strComando03 = ''sum(t.julio * t.prodprecio) julio, sum(t.agosto * t.prodprecio) agosto, sum(t.septiembre * t.prodprecio) septiembre, ''
			set @strComando04 = ''sum(t.octubre * t.prodprecio) octubre, sum(t.noviembre * t.prodprecio) noviembre, sum(t.diciembre * t.prodprecio) diciembre, ''
			set @strComando05 = ''p.regclase from tbl_presupuestos t, prm_productos p where p.registro = t.prodregistro and t.years = ''
			set @strComando07 = ''group by t.years, p.regclase order by t.years, p.regclase''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @lngYears + @strVendedores + 
				@strClases + @strSubClases + @strComando07)

			create table #tmpTabla05
			(
				years			smallint,
				months			tinyint,
				claseregistro	tinyint,
				vendregistro	tinyint,
				valor			numeric(18,2)
			)
			set @strComando01 = ''insert into #tmpTabla05 select convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)) years, convert(numeric(8), substring(convert(varchar(8), ''
			set @strComando02 = ''e.numfechaing), 5, 2)) months, p.regclase, e.vendregistro, sum(d.valor) valor from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos P, ''
			set @strComando03 = ''tbl_Presupuestos t where e.registro = d.registro and D.ProdRegistro = P.Registro and e.vendregistro = t.vendregistro and p.registro = t.prodregistro and e.status = 0 ''
			set @strComando05 = ''group by convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)), convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 5, 2)), ''
			set @strComando06 = ''p.regclase, e.vendregistro order by convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)), convert(numeric(8), ''
			set @strComando07 = ''substring(convert(varchar(8), e.numfechaing), 5, 2)), p.regclase, e.vendregistro''
			exec (@strComando01 + @strComando02 + @strComando03 + @strFecIni + @strFecFin + @strVendedores + @strClases + @strSubClases + 
				@strComando05 + @strComando06 + @strComando07)

			create table #tmpTabla06
			(
				claseregistro	tinyint,
				years_presp		smallint,
				mes_presp		varchar(12),
				mes_presp02		tinyint,
				mto_presp		numeric(18,2),
				mto_vtas		numeric(18,2),
				por_global		numeric(18,2),
				por_global02	numeric(18,2)
			)
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''enero'', 1, t1.enero, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''febrero'', 2, t1.febrero, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''marzo'', 3, t1.marzo, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''abril'', 4, t1.abril, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''mayo'', 5, t1.mayo, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''junio'', 6, t1.junio, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''julio'', 7, t1.julio, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''agosto'', 8, t1.agosto, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''septiembre'', 9, t1.septiembre, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''octubre'', 10, t1.octubre, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''noviembre'', 11, t1.noviembre, 0, 0, 0 from #tmptabla04 t1
			insert into #tmpTabla06 select t1.claseregistro, t1.years, ''diciembre'', 12, t1.diciembre, 0, 0, 0 from #tmptabla04 t1

			declare	tmpC cursor for
				select distinct mes_presp02, claseregistro from #tmpTabla06
			open tmpC
			fetch next from tmpC into @months, @claseregistro
			while @@fetch_status = 0
				begin
					set @valor = 0
					select @valor = isnull(sum(valor), 0) from #tmpTabla05 where months = @months and claseregistro = @claseregistro 
					update #tmpTabla06 set mto_vtas = @valor where mes_presp02 = @months and claseregistro = @claseregistro 
					fetch next from tmpC into @months, @claseregistro
				end
			close tmpC
			deallocate tmpC
			select distinct months into #tmptablaMC from #tmpTabla05
			declare	tmpC cursor for
				select distinct claseregistro from #tmpTabla06
			open tmpC
			fetch next from tmpC into @claseregistro
			while @@fetch_status = 0
				begin
					set @mto_real_vtas = 0
					set @mto_real_prsp = 0
					select @mto_real_vtas = isnull(sum(mto_vtas), 0) from #tmpTabla06 t, #tmptablaMC m where m.months = t.mes_presp02 and claseregistro = @claseregistro 
					select @mto_real_prsp = isnull(sum(mto_presp), 0) from #tmpTabla06 t, #tmptablaMC m where m.months = t.mes_presp02 and claseregistro = @claseregistro
					if @mto_real_vtas <> 0 and @mto_real_prsp <> 0
						begin
							update #tmpTabla06 set por_global = ((@mto_real_vtas / @mto_real_prsp) * 100) where claseregistro = @claseregistro 
						end
					fetch next from tmpC into @claseregistro
				end
			close tmpC
			deallocate tmpC
			select years_presp, mes_presp, claseregistro, descripcion, mto_vtas, mto_presp,
				''%_Avance'' = 
					case 
						when mto_vtas <> 0 and mto_presp <> 0 then convert(numeric(10,2), ((mto_vtas / mto_presp) * 100))
						when mto_vtas = 0 and mto_presp <> 0 then convert(numeric(10,2), ((mto_vtas / mto_presp) * 100))
						else 0 end, 
				por_global 
				from #tmpTabla06 t, prm_clases c, #tmpTablaMC m where m.months = t.mes_presp02 and c.registro = t.claseregistro
				order by years_presp, claseregistro, mes_presp02
		end
	else if @intTipoReporte = 4			-- % de Cumplimiento Mensual del Presupuesto por SubClase
		begin
			create table #tmpTabla07
			(
				years				smallint,
				enero				numeric(18,2),
				febrero				numeric(18,2),
				marzo				numeric(18,2),
				abril				numeric(18,2),
				mayo				numeric(18,2),
				junio				numeric(18,2),
				julio				numeric(18,2),
				agosto				numeric(18,2),
				septiembre			numeric(18,2),
				octubre				numeric(18,2),
				noviembre			numeric(18,2),
				diciembre			numeric(18,2),
				subclaseregistro	tinyint
			)
			set @strComando01 = ''insert into #tmpTabla07 select t.years, sum(t.enero * t.prodprecio) enero, sum(t.febrero * t.prodprecio) febrero, ''
			set @strComando02 = ''sum(t.marzo * t.prodprecio) marzo, sum(t.abril * t.prodprecio) abril, sum(t.mayo * prodprecio) mayo, sum(junio * t.prodprecio) junio, ''
			set @strComando03 = ''sum(t.julio * t.prodprecio) julio, sum(t.agosto * t.prodprecio) agosto, sum(t.septiembre * t.prodprecio) septiembre, ''
			set @strComando04 = ''sum(t.octubre * t.prodprecio) octubre, sum(t.noviembre * t.prodprecio) noviembre, sum(t.diciembre * t.prodprecio) diciembre, ''
			set @strComando05 = ''p.regsubclase from tbl_presupuestos t, prm_productos p where p.registro = t.prodregistro and t.years = ''
			set @strComando07 = ''group by t.years, p.regsubclase order by t.years, p.regsubclase''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @lngYears + @strVendedores + 
				@strClases + @strSubClases + @strComando07)
			create table #tmpTabla08
			(
				years				smallint,
				months				tinyint,
				subclaseregistro	tinyint,
				vendregistro		tinyint,
				valor				numeric(18,2)
			)
			set @strComando01 = ''insert into #tmpTabla08 select convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)) years, convert(numeric(8), substring(convert(varchar(8), ''
			set @strComando02 = ''e.numfechaing), 5, 2)) months, p.regsubclase, e.vendregistro, sum(d.valor) valor from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos P, ''
			set @strComando03 = ''tbl_Presupuestos t where e.registro = d.registro and D.ProdRegistro = P.Registro and e.vendregistro = t.vendregistro and p.registro = t.prodregistro and e.status = 0 ''
			set @strComando05 = ''group by convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)), convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 5, 2)), ''
			set @strComando06 = ''p.regsubclase, e.vendregistro order by convert(numeric(8), substring(convert(varchar(8), e.numfechaing), 1, 4)), convert(numeric(8), ''
			set @strComando07 = ''substring(convert(varchar(8), e.numfechaing), 5, 2)), p.regsubclase, e.vendregistro''
			exec (@strComando01 + @strComando02 + @strComando03 + @strFecIni + @strFecFin + @strVendedores + @strClases + @strSubClases + 
				@strComando05 + @strComando06 + @strComando07)

			create table #tmpTabla09
			(
				subclaseregistro	tinyint,
				years_presp			smallint,
				mes_presp			varchar(12),
				mes_presp02			tinyint,
				mto_presp			numeric(18,2),
				mto_vtas			numeric(18,2),
				por_global			numeric(18,2),
				por_global02		numeric(18,2)
			)

			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''enero'', 1, t1.enero, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''febrero'', 2, t1.febrero, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''marzo'', 3, t1.marzo, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''abril'', 4, t1.abril, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''mayo'', 5, t1.mayo, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''junio'', 6, t1.junio, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''julio'', 7, t1.julio, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''agosto'', 8, t1.agosto, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''septiembre'', 9, t1.septiembre, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''octubre'', 10, t1.octubre, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''noviembre'', 11, t1.noviembre, 0, 0, 0 from #tmptabla07 t1
			insert into #tmpTabla09 select t1.subclaseregistro, t1.years, ''diciembre'', 12, t1.diciembre, 0, 0, 0 from #tmptabla07 t1

			declare	tmpC cursor for
				select distinct mes_presp02, subclaseregistro from #tmpTabla09
			open tmpC
			fetch next from tmpC into @months, @claseregistro
			while @@fetch_status = 0
				begin
					set @valor = 0
					select @valor = isnull(sum(valor), 0) from #tmpTabla08 where months = @months and subclaseregistro = @claseregistro 
					update #tmpTabla09 set mto_vtas = @valor where mes_presp02 = @months and subclaseregistro = @claseregistro 
					fetch next from tmpC into @months, @claseregistro
				end
			close tmpC
			deallocate tmpC
			select distinct months into #tmptablaMS from #tmpTabla08
			declare	tmpC cursor for
				select distinct subclaseregistro from #tmpTabla09
			open tmpC
			fetch next from tmpC into @claseregistro
			while @@fetch_status = 0
				begin
					set @mto_real_vtas = 0
					set @mto_real_prsp = 0
					select @mto_real_vtas = isnull(sum(mto_vtas), 0) from #tmpTabla09 t, #tmptablaMS m where m.months = t.mes_presp02 and subclaseregistro = @claseregistro 
					select @mto_real_prsp = isnull(sum(mto_presp), 0) from #tmpTabla09 t, #tmptablaMS m where m.months = t.mes_presp02 and subclaseregistro = @claseregistro
					if @mto_real_vtas <> 0 and @mto_real_prsp <> 0
						begin
							update #tmpTabla09 set por_global = ((@mto_real_vtas / @mto_real_prsp) * 100) where subclaseregistro = @claseregistro 
						end
					fetch next from tmpC into @claseregistro
				end
			close tmpC
			deallocate tmpC
			select years_presp, mes_presp, subclaseregistro, descripcion, mto_vtas, mto_presp, 
				''%_Avance'' = 
					case 
						when mto_vtas <> 0 and mto_presp <> 0 then convert(numeric(10,2), ((mto_vtas / mto_presp) * 100))
						when mto_vtas = 0 and mto_presp <> 0 then convert(numeric(10,2), ((mto_vtas / mto_presp) * 100))
						else 0 end, 
				por_global 
				from #tmpTabla09 t, prm_subclases c, #tmpTablaMS m where m.months = t.mes_presp02 and c.registro = t.subclaseregistro 
				order by years_presp, subclaseregistro, mes_presp02
		end
	else if @intTipoReporte = 5			-- % de Cumplimiento Mensual del Presupuesto por Productos
		begin
			create table #tmpTabla10
			(
				mes				tinyint,
				vendregistro	tinyint,
				prodregistro	smallint,
				claseregistro	tinyint,
				valor			numeric(18,2),
				discriminar		tinyint
			)
			set @strComando01 = ''insert into #tmpTabla10 select convert(numeric(2), substring(convert(varchar(8), e.numfechaing), 5, 2)), e.vendregistro, t.prodregistro, p.regclase, ''
			if @intCantMonet = 0
				begin
					set @strComando02 = ''sum(d.cantidad), 0 from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, tbl_presupuestos t where e.registro = d.registro ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando02 = ''sum(d.valor), 0 from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, tbl_presupuestos t where e.registro = d.registro ''
				end
			set @strComando03 = ''and d.prodregistro = p.registro and e.vendregistro = t.vendregistro and p.registro = t.prodregistro and d.prodregistro = t.prodregistro ''
			set @strComando04 = ''and e.status = 0 ''
			set @strComando05 = ''group by convert(numeric(2), substring(convert(varchar(8), e.numfechaing), 5, 2)), t.prodregistro, e.vendregistro, p.codigo, p.descripcion, p.regclase '' 
			set @strComando06 = ''order by e.vendregistro, convert(numeric(2), substring(convert(varchar(8), e.numfechaing), 5, 2)), p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strFecIni + @strFecFin + @strVendedores + @strClases + 
				@strSubClases + @strComando05 + @strComando06)

			set @strVendedores = replace(@strVendedores, ''t.vendregistro'',''e.vendregistro'')
			set @strComando01 = ''insert into #tmpTabla10 select convert(numeric(2), substring(convert(varchar(8), e.numfechaing), 5, 2)), e.vendregistro, d.prodregistro, p.regclase, ''
			if @intCantMonet = 0
				begin
					set @strComando02 = ''sum(d.cantidad), 1 from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p where e.registro = d.registro ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando02 = ''sum(d.valor), 1 from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p where e.registro = d.registro ''
				end
			set @strComando03 = ''and d.prodregistro = p.registro and e.status = 0 ''
			set @strComando04 = ''and d.prodregistro not in (select distinct e.prodregistro from tbl_presupuestos e where years = ''
			set @strComando05 = '') group by convert(numeric(2), substring(convert(varchar(8), e.numfechaing), 5, 2)), d.prodregistro, e.vendregistro, p.codigo, p.descripcion, p.regclase '' 
			set @strComando06 = ''order by e.vendregistro, convert(numeric(2), substring(convert(varchar(8), e.numfechaing), 5, 2)), p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strFecIni + @strFecFin + @strVendedores + @strClases + 
				@strSubClases + @strComando04 + @lngYears + @strVendedores + @strComando05 + @strComando06)

			set @strVendedores = replace(@strVendedores, ''e.vendregistro'',''t.vendregistro'')
			create clustered index tabla10ix on #tmpTabla10 (mes, vendregistro, prodregistro)
			create table #tmpTabla11
			(
				claseregistro	tinyint,
				clasedescrip	varchar(50),
				prodregistro	smallint,
				prodcodigo		varchar(12),
				proddescrip		varchar(80),
				mes				tinyint,
				mesdescrip		varchar(15),
				mto_prsp		numeric(18,2),
				mto_vtas		numeric(18,2),
				mto_difr		numeric(18,2),
				prc_avnc		numeric(18,2),
				vendregistro	tinyint,
				vendcodigo		varchar(12),
				venddescrip		varchar(50),
				provcodigo		varchar(12),
				provnombre		varchar(70),
				signooperac		varchar(1),
				padre01			numeric(18,2),
				padre02			numeric(18,2),
				padre03			numeric(18,2),
				padre04			numeric(18,2),
				hijo01			numeric(18,2),
				hijo02			numeric(18,2),
				hijo03			numeric(18,2),
				hijo04			numeric(18,2),
				nieto01			numeric(18,2),
				nieto02			numeric(18,2),
				nieto03			numeric(18,2),
				nieto04			numeric(18,2),
				discriminar		tinyint
			)
			create clustered index tabla11ix on #tmptabla11 (mes, vendregistro, prodregistro)
			set @strComando02 = ''v.nombre, d.codigo, d.nombre, '''''''', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 from tbl_presupuestos t, prm_productos p, prm_vendedores v, prm_proveedores d, ''
			set @strComando03 = ''prm_clases c where p.registro = t.prodregistro and v.registro = t.vendregistro and d.registro = p.regproveedor and c.registro = p.regclase and t.years = ''
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 1, ''''Enero'''', t.Enero, 0, 0, 0, t.vendregistro, v.codigo, ''		
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 1, ''''Enero'''', (t.Enero * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''		
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 2, ''''Febrero'''', t.Febrero, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 2, ''''Febrero'''', (t.Febrero * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 3, ''''Marzo'''', t.Marzo, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 3, ''''Marzo'''', (t.Marzo * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 4, ''''Abril'''', t.Abril, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 4, ''''Abril'''', (t.Abril * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 5, ''''Mayo'''', t.Mayo, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 5, ''''Mayo'''', (t.Mayo * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 6, ''''Junio'''', t.Junio, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 6, ''''Junio'''', (t.Junio * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 7, ''''Julio'''', t.Julio, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 7, ''''Julio'''', (t.Julio * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 8, ''''Agosto'''', t.Agosto, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 8, ''''Agosto'''', (t.Agosto * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 9, ''''Septiembre'''', t.Septiembre, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 9, ''''Septiembre'''', (t.Septiembre * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 10, ''''Octubre'''', t.Octubre, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 10, ''''Octubre'''', (t.Octubre * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 11, ''''Noviembre'''', t.Noviembre, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 11, ''''Noviembre'''', (t.Noviembre * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			if @intCantMonet = 0
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 12, ''''Diciembre'''', t.Diciembre, 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			else if @intCantMonet = 1
				begin
					set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, t.prodregistro, p.codigo, p.descripcion, 12, ''''Diciembre'''', (t.Diciembre * t.prodprecio), 0, 0, 0, t.vendregistro, v.codigo, ''
				end
			exec (@strComando01 + @strComando02 + @strComando03 + @lngYears + @strVendedores + @strClases + @strSubClases)
			declare tmpC cursor for
				select distinct mes, vendregistro, prodregistro from #tmpTabla11
			open tmpC
			fetch next from tmpC into @months, @vendregistro, @prodregistro
			while @@fetch_status = 0
				begin
					set @valor = 0
					select @valor = valor from #tmpTabla10 where mes = @months and vendregistro = @vendregistro and prodregistro = @prodregistro
					if @valor <> 0
						begin
							update #tmpTabla11 set mto_vtas = @valor,  mto_difr = (mto_prsp - @valor) 
								where mes = @months and vendregistro = @vendregistro and prodregistro = @prodregistro
						end
					fetch next from tmpC into @months, @vendregistro, @prodregistro
				end
			close tmpC
			deallocate tmpC
			update #tmpTabla11 set prc_avnc = ((mto_vtas / mto_prsp) * 100) where mto_vtas <> 0 and mto_prsp <> 0
			update #tmpTabla11 set prc_avnc = 100 where mto_vtas <> 0 and mto_prsp = 0
			update #tmpTabla11 set mto_difr = mto_prsp where mto_vtas = 0
			update #tmpTabla11 set signooperac = ''''
			update #tmpTabla11 set signooperac = ''+'' where mto_difr < 0
			update #tmpTabla11 set mto_difr = abs(mto_difr)
			select distinct mes into #tmptablaMP from #tmpTabla10

			set @strComando01 = ''insert into #tmpTabla11 select c.codigo, c.descripcion, p.registro, p.codigo, p.descripcion, t.mes, ''''mes'''' = case when mes = 1 then ''''Enero'''' when mes = 2 then ''''Febrero'''' ''
			set @strComando02 = ''when mes = 3 then ''''Marzo'''' when mes = 4 then ''''Abril'''' when mes = 5 then ''''Mayo'''' when mes = 6 then ''''Junio'''' when mes = 7 then ''''Julio'''' when mes = 8 then ''''Agosto'''' ''
			set @strComando03 = ''when mes = 9 then ''''Septiembre'''' when mes = 10 then ''''Octubre'''' when mes = 11 then ''''Noviembre'''' when mes = 12 then ''''Diciembre'''' end, 0, t.valor, t.valor, 100, ''
			set @strComando04 = ''(select registro from prm_vendedores where registro = t.vendregistro), (select codigo from prm_vendedores where registro = t.vendregistro), ''
			set @strComando05 = ''(select nombre from prm_vendedores where registro = t.vendregistro), (select r.codigo from prm_proveedores r, prm_productos p where r.registro = p.regproveedor ''
			set @strComando06 = ''and p.registro = t.prodregistro), (select r.nombre from prm_proveedores r, prm_productos p where r.registro = p.regproveedor and p.registro = t.prodregistro), ''
			set @strComando07 = ''''''+'''', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 from #tmpTabla10 t, prm_productos p, prm_clases c where t.prodregistro = p.registro and t.claseregistro = p.regclase ''
			set @strComando08 = ''and t.claseregistro = c.registro and p.regclase = c.registro and t.discriminar = 1 ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strComando07 + @strComando08)

			create table #tmpTotal01
			(
				vend	tinyint,
				mes	tinyint,
				clase	tinyint,
				valor1	numeric(18,2),
				valor2	numeric(18,2),
				valor3	numeric(18,2),
				valor4	numeric(18,2)
			)
			insert into #tmpTotal01 select t1.vendregistro, t1.mes, t1.claseregistro, sum(t1.mto_prsp), sum(t1.mto_vtas), abs(sum(t1.mto_prsp) - sum(t1.mto_vtas)), 0
				from #tmpTabla11 t1, #tmpTablaMP t3 where t1.mes = t3.mes group by t1.vendregistro, t1.mes, t1.claseregistro
			update #tmpTabla11 set nieto01 = l.valor1, nieto02 = l.valor2, nieto03 = l.valor3, nieto04 = l.valor4 from #tmpTabla11 t, #tmpTotal01 l
				where t.vendregistro = l.vend and t.mes = l.mes and t.claseregistro = l.clase
			truncate table #tmpTotal01
			insert into #tmpTotal01 select t1.vendregistro, t1.mes, 0, sum(t1.mto_prsp), sum(t1.mto_vtas), abs(sum(t1.mto_prsp) - sum(t1.mto_vtas)), 0
				from #tmpTabla11 t1, #tmpTablaMP t3 where t1.mes = t3.mes group by t1.vendregistro, t1.mes
			update #tmpTabla11 set hijo01 = l.valor1, hijo02 = l.valor2, hijo03 = l.valor3, hijo04 = l.valor4 from #tmpTabla11 t, #tmpTotal01 l
				where t.vendregistro = l.vend and t.mes = l.mes
			truncate table #tmpTotal01
			insert into #tmpTotal01 select t1.vendregistro, 0, 0, sum(t1.mto_prsp), sum(t1.mto_vtas), abs(sum(t1.mto_prsp) - sum(t1.mto_vtas)), 0
				from #tmpTabla11 t1, #tmpTablaMP t3 where t1.mes = t3.mes group by t1.vendregistro
			update #tmpTabla11 set padre01 = l.valor1, padre02 = l.valor2, padre03 = l.valor3, padre04 = l.valor4 from #tmpTabla11 t, #tmpTotal01 l
				where t.vendregistro = l.vend

			update #tmpTabla11 set nieto04 = ((nieto02 / nieto01) * 100) where nieto01 <> 0 and nieto02 <> 0
			update #tmpTabla11 set hijo04 = ((hijo02 / hijo01) * 100) where hijo01 <> 0 and hijo02 <> 0
			update #tmpTabla11 set padre04 = ((padre02 / padre01) * 100) where padre01 <> 0 and padre02 <> 0

			update #tmpTabla11 set nieto04 = 100 where nieto01 = 0 and nieto02 <> 0
			update #tmpTabla11 set hijo04 = 100 where hijo01 = 0 and hijo02 <> 0
			update #tmpTabla11 set padre04 = 100 where padre01 = 0 and padre02 <> 0

			select t2.* from #tmpTablaMP t3, #tmpTabla11 t2	where t3.mes = t2.mes order by t2.vendregistro, t2.mes, t2.claseregistro, t2.proddescrip
		end
	else if @intTipoReporte = 6			-- Cumplimiento de Ventas
		begin
			create table #tmpC
			(
				registro	tinyint,
				descrip		varchar(60),
				int_mes		tinyint,
				ventas		numeric(18,2)
			)
			create clustered index tmpC_ix on #tmpC (registro)
			create table #tmpCumplir
			(
				regis		tinyint,
				clase		varchar(60),
				ventas		numeric(18,2),
				ppto		numeric(18,2),
				variac		numeric(18,2),
				signo		char(1),
				porcen		numeric(18,2),
				mfact		numeric(18,2)
			)
			create clustered index tmpCumplir_ix on #tmpCumplir (regis)
			-- Ingresar las Clases a Consultar
			set @strComando01 = ''insert into #tmpCumplir select registro, descripcion, 0, 0, 0, '''' '''', 0, 0 from prm_clases c where estado <> 2  ''
			exec (@strComando01 + @strClases + ''order by c.registro'')

			-- Ingresar las Ventas por Clases
			set @strComando01 = ''insert into #tmpC select c.registro, c.descripcion, convert(numeric(2,0), substring(convert(varchar(8), e.numfechaing),5,2)), sum(d.valor) ventas ''
			set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_vendedores v, prm_productos p, ''
			set @strComando03 = ''prm_clases c where e.registro = d.registro and e.vendregistro = v.registro and d.prodregistro = p.registro and p.regclase = c.registro and e.status = 0 ''
			set @strComando04 = ''group by c.registro, c.descripcion, convert(numeric(2,0), substring(convert(varchar(8), e.numfechaing),5,2)) order by c.registro, c.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strFecIni + @strFecFin + @strVendedores + @strClases + @strComando04)
			select registro, sum(ventas) valores into #tmpCC from #tmpC group by registro
			update #tmpCumplir set ventas = c.valores from #tmpCumplir t, #tmpCC c where t.regis = c.registro
			select distinct int_mes into #tmpMeses from #tmpC

			-- Ingresar los Valores por Presupuesto
			create table #tmpP
			(
				years			smallint,
				enero			numeric(18,2),
				febrero			numeric(18,2),
				marzo			numeric(18,2),
				abril			numeric(18,2),
				mayo			numeric(18,2),
				junio			numeric(18,2),
				julio			numeric(18,2),
				agosto			numeric(18,2),
				septiembre		numeric(18,2),
				octubre			numeric(18,2),
				noviembre		numeric(18,2),
				diciembre		numeric(18,2),
				claseregistro	tinyint
			)
			set @lista_vend = @strVendedores
			set @lista_vend = replace(@lista_vend,''v.registro'',''t.vendregistro'')
			set @strComando01 = ''insert into #tmpP select t.years, sum(t.enero * t.prodprecio) enero, sum(t.febrero * t.prodprecio) febrero, ''
			set @strComando02 = ''sum(t.marzo * t.prodprecio) marzo, sum(t.abril * t.prodprecio) abril, sum(t.mayo * prodprecio) mayo, sum(junio * t.prodprecio) junio, ''
			set @strComando03 = ''sum(t.julio * t.prodprecio) julio, sum(t.agosto * t.prodprecio) agosto, sum(t.septiembre * t.prodprecio) septiembre, ''
			set @strComando04 = ''sum(t.octubre * t.prodprecio) octubre, sum(t.noviembre * t.prodprecio) noviembre, sum(t.diciembre * t.prodprecio) diciembre, ''
			set @strComando05 = ''p.regclase from tbl_presupuestos t, prm_productos p where p.registro = t.prodregistro and t.years = ''
			set @strComando07 = ''group by t.years, p.regclase order by t.years, p.regclase''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @lngYears + @lista_vend + 
				@strClases + @strComando07)
			create table #tmpPC
			(
				claseregistro	tinyint,
				mes_presp02		smallint,
				mto_presp		numeric(18,2)
			)
			insert into #tmpPC select t1.claseregistro, 1, t1.enero from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 2, t1.febrero from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 3, t1.marzo from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 4, t1.abril from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 5, t1.mayo from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 6, t1.junio from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 7, t1.julio from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 8, t1.agosto from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 9, t1.septiembre from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 10, t1.octubre from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 11, t1.noviembre from #tmpP t1
			insert into #tmpPC select t1.claseregistro, 12, t1.diciembre from #tmpP t1
			select claseregistro, sum(mto_presp) valores into #tmpPresp from #tmpPC where mes_presp02 in (select int_mes from #tmpMeses) group by claseregistro
			update #tmpCumplir set ppto = c.valores from #tmpCumplir t, #tmpPresp c where t.regis = c.claseregistro

			-- Ingresar valor en Variac y Porcentaje
			update #tmpCumplir set variac = (ppto - ventas)
			update #tmpCumplir set porcen = ((ventas / ppto) * 100) where ventas <> 0 and ppto <> 0
			update #tmpCumplir set porcen = 100 where ventas > 0 and ppto = 0
			update #tmpCumplir set variac = abs(variac), signo = ''+'' where variac < 0

			-- Obtener Datos para la Columna de Costos
			create table #tmpCosto_01
			(
				codigosuc		varchar(12),
				nombresuc		varchar(60),
				registroclase	tinyint,
				codigoclase		varchar(12),
				nombreclase		varchar(60),
				codigoprod		varchar(12),
				nombreprod		varchar(60),
				cantidad		numeric(18,2),
				prod_mto		numeric(18,2)
			)
			create clustered index tmpCosto01_ix on #tmpCosto_01 (codigosuc, codigoprod)
			create table #tmpCosto_02
			(
				codigosuc		varchar(12),
				nombresuc		varchar(60),
				registroclase	tinyint,
				codigoclase		varchar(12),
				nombreclase		varchar(60),
				codigoprod		varchar(12),
				nombreprod		varchar(60),
				cantidad		numeric(18,2),
				prod_mto		numeric(18,2)
			)
			create clustered index tmpCosto02_ix on #tmpCosto_02 (codigosuc, codigoprod)
			create table #tmpCosto_03
			(
				codigosuc		varchar(12),
				nombresuc		varchar(60),
				registroclase	tinyint,
				codigoclase		varchar(12),
				nombreclase		varchar(60),
				codigoprod		varchar(12),
				nombreprod		varchar(60),
				cantidad		numeric(18,2),
				prod_vta		numeric(18,2),
				prod_cto		numeric(18,2),
				prod_utl		numeric(18,2),
				prod_porc		numeric(18,2),
				porc_clase		numeric(18,2),
				porc_agencia	numeric(18,2)
			)
			create clustered index tmpCosto03_ix on #tmpCosto_03 (codigosuc, codigoprod)
			set @strComando01 = ''insert into #tmpCosto_01 select s.codigo CodSuc, s.descripcion Sucursal, c.registro, c.codigo CodClase, c.descripcion Clase, p.codigo CodProd, p.descripcion Producto, ''
			set @strComando02 = ''sum(e.debitos_cnt) - sum(e.creditos_cnt) Deb_Cnt, sum(e.debitos_mto) - sum(e.creditos_mto) Debitos_Mto from tbl_producto_movimiento e, prm_productos p,  ''
			set @strComando03 = ''prm_agencias s, prm_clases c where e.agenregistro = s.registro and e.prodregistro = p.registro and p.regclase = c.registro and e.Origen = 1 ''
			set @strComando05 = ''group by s.codigo, s.descripcion, c.registro, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			set @strComando06 = ''order by s.codigo, s.descripcion, c.registro, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strClases +  @strFecIni + @strFecFin + @strComando05 + @strComando06)

			set @strComando01 = ''insert into #tmpCosto_02 select S.Codigo, S.Descripcion AS Sucursal, c.registro, C.Codigo , C.Descripcion as Clase, P.Codigo, ''
			set @strComando02 = ''P.Descripcion AS Producto, convert(numeric(10,2), Sum(D.Cantidad)) as Cantidad, convert(numeric(10,2), Sum(D.Valor)) as Monto ''
			set @strComando03 = ''from tbl_Facturas_Enc e, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Vendedores V where e.Registro = D.Registro and ''
			set @strComando04 = ''e.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and e.Status = 0 and e.VendRegistro = V.Registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, c.registro, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			set @strComando06 = ''order by S.Codigo, S.Descripcion, c.registro, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strClases + @strFecIni + @strFecFin + @strComando05 + @strComando06)

			insert into #tmpCosto_03 select t1.codigosuc, t1.nombresuc, t1.registroclase, t1.codigoclase, t1.nombreclase, t1.codigoprod, t1.nombreprod, t1.cantidad, t2.prod_mto, t1.prod_mto, t2.prod_mto - t1.prod_mto, 
				(((t2.prod_mto - t1.prod_mto) / t2.prod_mto) * 100), 0, 0 from #tmpCosto_01 t1, #tmpCosto_02 t2 where t1.codigosuc = t2.codigosuc and t1.codigoprod = t2.codigoprod and t2.prod_mto != 0
			insert into #tmpCosto_03 select t1.codigosuc, t1.nombresuc, t1.registroclase, t1.codigoclase, t1.nombreclase, t1.codigoprod, t1.nombreprod, t1.cantidad, t2.prod_mto, t1.prod_mto, t2.prod_mto - t1.prod_mto, 
				0, 0, 0 from #tmpCosto_01 t1, #tmpCosto_02 t2 where t1.codigosuc = t2.codigosuc and t1.codigoprod = t2.codigoprod and t2.prod_mto = 0

			declare tmpCursor cursor for
				select distinct codigosuc, codigoclase from #tmpCosto_03 order by codigosuc, codigoclase
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia, @codigoclase
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					select @porcent_grupo = isnull(sum(prod_vta), 0) from
						#tmpCosto_03 where codigosuc = @codigoagencia and codigoclase = @codigoclase
					if @porcent_grupo <> 0
						select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
							#tmpCosto_03 where codigosuc = @codigoagencia and codigoclase = @codigoclase
					update #tmpCosto_03 set porc_clase = @porcent_grupo where codigosuc = @codigoagencia 
						and codigoclase = @codigoclase
					fetch next from tmpCursor into @codigoagencia, @codigoclase
				end
			close tmpCursor
			deallocate tmpCursor

			declare tmpCursor cursor for
				select distinct codigosuc from #tmpCosto_03 order by codigosuc
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					select @porcent_grupo = isnull(sum(prod_vta), 0) from
						#tmpCosto_03 where codigosuc = @codigoagencia
					if @porcent_grupo <> 0
						select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
							#tmpCosto_03 where codigosuc = @codigoagencia
					update #tmpCosto_03 set porc_agencia = @porcent_grupo where codigosuc = @codigoagencia
					fetch next from tmpCursor into @codigoagencia
				end
			close tmpCursor
			deallocate tmpCursor
			select codigosuc, codigoclase, registroclase, porc_clase, sum(prod_vta) prod_vta, sum(prod_cto) prod_cto, sum(prod_utl) prod_utl 
				into #tmpCostoSum from #tmpCosto_03 
				group by codigosuc, codigoclase, registroclase, porc_clase order by codigosuc, codigoclase, registroclase, porc_clase
			update #tmpCumplir set mfact = c.prod_utl from #tmpCumplir t, #tmpCostoSum c where t.regis = c.registroclase

			-- Extraer Datos
			select * from #tmpCumplir order by regis
		end
	else if @intTipoReporte = 7				-- Presupuesto Consolidado
		begin
			set @strComando01 = ''select years, p.codigo, p.descripcion, u.descripcion, c.codigo, c.descripcion, ''
			--set @strComando01 = ''select years, p.codigo, p.descripcion, u.descripcion, x.codigo, x.nombre, ''
			set @strComando02 = ''sum(t.enero), sum(t.febrero), sum(t.marzo), sum(t.abril), sum(t.mayo), sum(t.junio), sum(t.julio), sum(t.agosto), sum(t.septiembre), sum(t.octubre), sum(t.noviembre), sum(t.diciembre), ''
			set @strComando03 = ''(sum(t.enero) + sum(t.febrero) + sum(t.marzo) + sum(t.abril) + sum(t.mayo) + sum(t.junio) + sum(t.julio) + sum(t.agosto) + sum(t.septiembre) + sum(t.octubre) + sum(t.noviembre) ''
			set @strComando04 = ''+ sum(t.diciembre)), t.prodprecio, sum(t.enero * t.prodprecio), sum(t.febrero * t.prodprecio), sum(t.marzo * t.prodprecio), sum(t.abril * t.prodprecio), ''
			set @strComando05 = ''sum(t.mayo * t.prodprecio), sum(t.junio * t.prodprecio), sum(t.julio * t.prodprecio), sum(t.agosto * t.prodprecio), ''
			set @strComando06 = ''sum(t.septiembre * t.prodprecio), sum(t.octubre * t.prodprecio), sum(t.noviembre * t.prodprecio), sum(t.diciembre * t.prodprecio), ''
			set @strComando07 = ''(sum(t.enero * t.prodprecio) + sum(t.febrero * t.prodprecio) + sum(t.marzo * t.prodprecio) + sum(t.abril * t.prodprecio) + ''
			set @strComando08 = ''sum(t.mayo * t.prodprecio) + sum(t.junio * t.prodprecio) + sum(t.julio * t.prodprecio) + sum(t.agosto * t.prodprecio) + ''
			set @strComando09 = ''sum(t.septiembre * t.prodprecio) + sum(t.octubre * t.prodprecio) + sum(t.noviembre * t.prodprecio) + sum(t.diciembre * t.prodprecio)) ''
			set @strComando10 = ''from tbl_presupuestos t, prm_productos p, prm_clases c, prm_unidades u where p.registro = t.prodregistro ''
			--set @strComando10 = ''from tbl_presupuestos t, prm_productos p, prm_clases c, prm_unidades u, prm_proveedores x where p.registro = t.prodregistro and p.regproveedor = x.registro ''
			set @strComando11 = ''and c.registro = p.regclase and u.registro = p.regunidad and years = ''
			set @strComando12 = ''group by years, p.codigo, p.descripcion, u.descripcion, c.codigo, c.descripcion, t.prodprecio order by years, c.codigo, p.descripcion ''
			--set @strComando12 = ''group by years, p.codigo, p.descripcion, u.descripcion, x.codigo, x.nombre, t.prodprecio order by years, x.nombre, p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + @strComando11 + 
				@lngYears + @strVendedores + @strClases + @strSubClases + @strFecIni + @strFecFin + @strComando12)
		end
	else if @intTipoReporte = 8				-- Presupuesto Consolidado por Vendedor
		begin
			set @strComando01 = ''select years, v.codigo, v.nombre, ''
			set @strComando02 = ''sum(t.enero * t.prodprecio), sum(t.febrero * t.prodprecio), sum(t.marzo * t.prodprecio), sum(t.abril * t.prodprecio), ''
			set @strComando03 = ''sum(t.mayo * t.prodprecio), sum(t.junio * t.prodprecio), sum(t.julio * t.prodprecio), sum(t.agosto * t.prodprecio), ''
			set @strComando04 = ''sum(t.septiembre * t.prodprecio), sum(t.octubre * t.prodprecio), sum(t.noviembre * t.prodprecio), sum(t.diciembre * t.prodprecio), ''
			set @strComando05 = ''(sum(t.enero * t.prodprecio) + sum(t.febrero * t.prodprecio) + sum(t.marzo * t.prodprecio) + sum(t.abril * t.prodprecio) + ''
			set @strComando06 = ''sum(t.mayo * t.prodprecio) + sum(t.junio * t.prodprecio) + sum(t.julio * t.prodprecio) + sum(t.agosto * t.prodprecio) + ''
			set @strComando07 = ''sum(t.septiembre * t.prodprecio) + sum(t.octubre * t.prodprecio) + sum(t.noviembre * t.prodprecio) + sum(t.diciembre * t.prodprecio)) ''
			set @strComando08 = ''from tbl_presupuestos t, prm_productos p, prm_clases c, prm_vendedores v where v.registro = t.vendregistro and p.registro = t.prodregistro ''
			set @strComando09 = ''and c.registro = p.regclase and years = ''
			set @strComando10 = ''group by t.years, v.codigo, v.nombre order by t.years, v.codigo, v.nombre ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strComando07 + @strComando08 + @strComando09 + 
				@lngYears + @strVendedores + @strClases + @strSubClases + @strComando10)
		end
	else if @intTipoReporte = 9				-- Presupuesto Consolidado Trimestral
		begin
			--set @strComando01 = ''select years, p.codigo, p.descripcion, u.descripcion, c.codigo, c.descripcion, ''
			set @strComando01 = ''select years, p.codigo, p.descripcion, u.descripcion, x.codigo, x.nombre, ''
			set @strComando02 = ''sum(t.enero), sum(t.febrero), sum(t.marzo), sum(t.abril), sum(t.mayo), sum(t.junio), sum(t.julio), sum(t.agosto), sum(t.septiembre), sum(t.octubre), sum(t.noviembre), sum(t.diciembre), ''
			set @strComando03 = ''(sum(t.enero) + sum(t.febrero) + sum(t.marzo) + sum(t.abril) + sum(t.mayo) + sum(t.junio) + sum(t.julio) + sum(t.agosto) + sum(t.septiembre) + sum(t.octubre) + sum(t.noviembre) ''
			set @strComando04 = ''+ sum(t.diciembre)), t.prodprecio, sum(t.enero * t.prodprecio), sum(t.febrero * t.prodprecio), sum(t.marzo * t.prodprecio), sum(t.abril * t.prodprecio), ''
			set @strComando05 = ''sum(t.mayo * t.prodprecio), sum(t.junio * t.prodprecio), sum(t.julio * t.prodprecio), sum(t.agosto * t.prodprecio), ''
			set @strComando06 = ''sum(t.septiembre * t.prodprecio), sum(t.octubre * t.prodprecio), sum(t.noviembre * t.prodprecio), sum(t.diciembre * t.prodprecio), ''
			set @strComando07 = ''(sum(t.enero * t.prodprecio) + sum(t.febrero * t.prodprecio) + sum(t.marzo * t.prodprecio) + sum(t.abril * t.prodprecio) + ''
			set @strComando08 = ''sum(t.mayo * t.prodprecio) + sum(t.junio * t.prodprecio) + sum(t.julio * t.prodprecio) + sum(t.agosto * t.prodprecio) + ''
			set @strComando09 = ''sum(t.septiembre * t.prodprecio) + sum(t.octubre * t.prodprecio) + sum(t.noviembre * t.prodprecio) + sum(t.diciembre * t.prodprecio)) ''
			--set @strComando10 = ''from tbl_presupuestos t, prm_productos p, prm_clases c, prm_unidades u where p.registro = t.prodregistro ''
			set @strComando10 = ''from tbl_presupuestos t, prm_productos p, prm_clases c, prm_unidades u, prm_proveedores x where p.registro = t.prodregistro and p.regproveedor = x.registro ''
			set @strComando11 = ''and c.registro = p.regclase and u.registro = p.regunidad and years = ''
			--set @strComando12 = ''group by years, p.codigo, p.descripcion, u.descripcion, c.codigo, c.descripcion, t.prodprecio order by years, c.descripcion, p.descripcion ''
			set @strComando12 = ''group by years, p.codigo, p.descripcion, u.descripcion, x.codigo, x.nombre, t.prodprecio order by years, x.nombre, p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + @strComando11 + 
				@lngYears + @strVendedores + @strClases + @strSubClases + @strFecIni + @strFecFin + @strComando12)
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Bitacora]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Bitacora]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_Bitacora] @intModulo as tinyint, @lngRegistro as numeric(6)
As

Declare	@strComando01 as varchar(160)

begin
	set nocount on
	select @strComando01 = ''Select fecha, tiempo, codusuario, campo, antes, despues from tbl_Bitacora Where tabla = ''
	exec (@strComando01 + @intModulo + ''and registro = '' + @lngRegistro + '' order by numfecha desc, numtiempo desc'')
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptCheques]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptCheques]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_RptCheques] @intTipoReporte tinyint, @strBancos varchar(100), @strCuentas varchar(100), 
		@strProveedores varchar(100), @intFecIni int, @intFecFin int, @strDocumento varchar(50), 
		@strMoneda varchar(30), @strEstado varchar(25)

As

Declare	@strComando01 as varchar(175),
	@strComando02 as varchar(175),
	@strComando03 as varchar(175),
	@strComando04 as varchar(175),
	@strComando05 as varchar(175)

begin
	set nocount on
	set @strComando01 = ''''
	set @strComando02 = ''''
	set @strComando03 = ''''
	set @strComando04 = ''''
	set @strComando05 = ''''
	if @intTipoReporte = 1
		begin
			set @strComando01 = ''select c.registro, b.descripcion, c.codigo, e.registro, e.documento, substring(replace(convert(varchar(12), e.fechacheque, 113), '''' '''', ''''-''''), 1, 11) fec_mov, ''
			set @strComando02 = ''e.beneficiario, e.descripcion, e.monto, t.cuenta, t.descripcion descrip, d.montoD, d.montoH, m.descripcion mon from tbl_cheques_enc e, prm_ctasbancarias c, prm_bancos b, ''
			set @strComando03 = ''tbl_cheques_det d, prm_ctascontables t, cat_monedas m where e.registro = d.registro and e.documento = d.documento and ''
			set @strComando04 = ''e.ctabanregistro = c.registro and c.bancoregistro = b.registro and d.ctacontregistro = t.registro and e.numfechacheque ''
			set @strComando05 = ''and e.tipomovim = 1 and c.monregistro = (m.registro -1) order by c.registro, e.registro ''
		end
	else if @intTipoReporte = 2
		begin
			set @strComando01 = ''select c.registro, b.descripcion, c.codigo, e.registro, e.documento, substring(replace(convert(varchar(12), e.fechacheque, 113), '''' '''', ''''-''''), 1, 11) ''
			set @strComando02 = ''fec_mov, e.descripcion, e.monto, m.descripcion mon from tbl_cheques_enc e, prm_ctasbancarias c, prm_bancos b, cat_monedas m where e.ctabanregistro = c.registro and ''
			set @strComando03 = ''c.bancoregistro = b.registro and e.numfechacheque ''
			set @strComando05 = ''and e.tipomovim = 2 and c.monregistro = (m.registro -1) order by c.registro, e.registro ''
		end
	else if @intTipoReporte = 3
		begin
			set @strComando01 = ''select c.registro, b.descripcion, c.codigo, e.registro, e.documento, substring(replace(convert(varchar(12), e.fechacheque, 113), '''' '''', ''''-''''), 1, 11) ''
			set @strComando02 = ''fec_mov, e.descripcion, e.monto, m.descripcion mon from tbl_cheques_enc e, prm_ctasbancarias c, prm_bancos b, cat_monedas m where e.ctabanregistro = c.registro and ''
			set @strComando03 = ''c.bancoregistro = b.registro and e.numfechacheque ''
			set @strComando05 = ''and e.tipomovim = 3 and c.monregistro = (m.registro -1) order by c.registro, e.registro ''
		end
	else if @intTipoReporte = 4
		begin
			set @strComando01 = ''select c.registro, b.descripcion, c.codigo, e.registro, e.documento, substring(replace(convert(varchar(12), e.fechacheque, 113), '''' '''', ''''-''''), 1, 11) ''
			set @strComando02 = ''fec_mov, e.descripcion, e.monto, m.descripcion mon from tbl_cheques_enc e, prm_ctasbancarias c, prm_bancos b, cat_monedas m where e.ctabanregistro = c.registro and ''
			set @strComando03 = ''c.bancoregistro = b.registro and e.numfechacheque ''
			set @strComando05 = ''and e.tipomovim = 4 and c.monregistro = (m.registro -1) order by c.registro, e.registro ''
		end
	exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + '' between '' + @intFecIni + '' and '' 
		+ @intFecFin + @strBancos + @strCuentas + @strProveedores + @strMoneda + @strDocumento + @strEstado 
		+ @strComando05)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptOtrosModulos]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptOtrosModulos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_RptOtrosModulos] @intTipoReporte as tinyint, @strAgencias as varchar(50), @lngUsuario as varchar(50),  @strProductos as varchar(50), 
				@intTipoFactura as varchar(50), @strFecIni as varchar(30), @strFecFin as varchar(30)
As

declare	@strComando01 varchar(190),
	@strComando02 varchar(190),
	@strComando03 varchar(130),
	@strComando04 varchar(100)

begin
	if @intTipoReporte = 1			-- Cambio de Precios
		begin
			select @strComando01 = ''select c.fecha, a.codigo codigosuc, a.descripcion agencia, c.numfactura, u.codusuario, u.nombre usuario, p.codigo codigoprod, p.descripcion producto, c.numprecio, c.precio_orig, ''
			select @strComando02 = ''c.precio_mod from tbl_cambiarprecios c, prm_agencias a, prm_usuarios u, prm_productos p where a.registro = c.agenregistro and u.registro = c.userregistro and p.registro = c.prodregistro ''
			select @strComando03 = ''order by a.codigo, u.codusuario, c.numfechaing, c.numfactura, p.codigo''
			exec (@strComando01 + @strComando02 + @strAgencias + @lngUsuario + @strProductos + @intTipoFactura + @strFecIni + @strFecFin + @strComando03)
		end
	else if @intTipoReporte = 2		-- Correcci?n en las Facturas
		begin
			select @strComando01 = ''select c.fechaingreso, a.codigo codigosuc, a.descripcion agencia, c.numerofactura, u.codusuario, u.nombre usuario, (select codigo + '''' '''' + descripcion from ''
			select @strComando02 = ''prm_productos where registro = c.prodregistro_old) viejo, (select codigo + '''' '''' + descripcion from prm_productos where registro = c.prodregistro_new) nuevo ''
			select @strComando03 = ''from tbl_facturas_corregidas c, prm_agencias a, prm_usuarios u where a.registro = c.agenregistro and u.registro = c.userregistro   ''
			select @strComando04 = ''order by a.codigo, u.codusuario, c.numfechaing, c.numerofactura ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strAgencias + @strFecIni + @strFecFin + @lngUsuario + @strProductos + @intTipoFactura + @strComando04)
		end
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptFacturas]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptFacturas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_RptFacturas] @intTipoReporte as int, @strProductos as varchar(100), @strAgencias as varchar(100), @strClases as varchar(100), @strClientes as varchar(100), 
			@strVendedores as varchar(100), @strProveedores as varchar(100), @strSubClase as varchar(100), @strFecIni as varchar(30), @strFecFin as varchar(30), 
			@strNombre as varchar(35), @intFactStatus as varchar(25), @intFactTipo as varchar(25), @strNumero as varchar(25), @strDepartamento as varchar(100), @strMunicipios as varchar(100)

As

Declare	@strComando01	varchar(175),
	@strComando02		varchar(175),
	@strComando03		varchar(175),
	@strComando04		varchar(175),
	@strComando05		varchar(175),
	@strComando06		varchar(175),
	@strComando07		varchar(175),
	@strComando08		varchar(175),
	@codigosuc			varchar(12),
	@codigoclase		varchar(12),
	@codigocliente		varchar(12),
	@nombrecliente		varchar(60),
	@codigodept			varchar(12),
	@codigoneg			varchar(12),
	@codigovend			varchar(12),
	@codigoagencia		varchar(12),
	@porcent_grupo		numeric(12,2),
	@strComando01E		varchar(175),
	@strComando02E		varchar(175),
	@strComando03E		varchar(175),
	@dblporc_vtas		numeric(12,2),
	@dblporc_util		numeric(12,2)

begin
	set nocount on
	select @strComando01 = ''''
	select @strComando02 = ''''
	select @strComando03 = ''''
	select @strComando04 = ''''
	select @strComando05 = ''''
	select @strComando06 = ''''
	select @strComando07 = ''''
	select @strComando08 = ''''
	if @intTipoReporte = 1				-- Ventas Detallado Vtas Articulos
		begin
			set @strDepartamento = ''''
			set @strComando01 = ''SELECT S.Codigo AS CodigoSucursal, S.Descripcion Sucursal, C.Codigo CodigoClase, C.Descripcion Clase, P.Codigo CodigoProducto, P.Descripcion Producto, ''
			set @strComando02 = ''U.Codigo CodigoUnidad, F.Numero Documento, F.FechaIngreso Fecha, D.Cantidad Cantidad, D.Precio Precio, D.Valor Monto, V.Codigo Vendedor, L.Nombre, L.Codigo Cliente ''
			set @strComando03 = ''FROM tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Productos P, prm_Clases C, prm_Unidades U, prm_Vendedores V, prm_Clientes L, prm_Departamentos M WHERE ''
			set @strComando04 = ''F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and U.Registro = P.RegUnidad and F.VendRegistro = ''
			set @strComando05 = ''V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			--set @strComando06 = ''ORDER BY S.Codigo, Left(F.NumFechaIng,6), C.Codigo, P.Codigo, F.NumFechaIng, F.Numero ''
			set @strComando06 = ''ORDER BY S.Codigo, Left(F.NumFechaIng,6), C.Codigo, P.Descripcion, F.NumFechaIng, F.Numero ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando06)
		end
	else if @intTipoReporte = 2				-- Ventas Detallado Vtas Vendedores
		begin
			set @strComando01 = ''SELECT S.Codigo as CodigoSucursal, S.Descripcion AS Sucursal, V.Codigo AS CodigoVendedor, V.Nombre AS Vendedor, C.Codigo AS CodigoClase, C.Descripcion AS Clase, ''
			set @strComando02 = ''P.codigo AS CodigoProducto, P.descripcion AS Producto, Sum(D.Cantidad) AS Cantidad, Avg(D.Precio) AS Costo, Sum(D.Valor) AS Monto FROM tbl_Facturas_Enc F, ''
			set @strComando03 = ''tbl_Facturas_Det D, prm_Agencias S, prm_Vendedores V, prm_Productos P, prm_Clases C, prm_Clientes L, prm_Departamentos M WHERE F.Registro = D.Registro and ''
			set @strComando04 = ''F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and F.VendRegistro = V.Registro and F.cliregistro = L.registro and ''
			set @strComando05 = ''L.deptregistro = M.registro ''
			set @strComando06 = ''GROUP BY S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			--set @strComando07 = ''ORDER BY S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			set @strComando07 = ''ORDER BY S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, P.Descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strComando06+ @strSubClase + @strDepartamento + @strMunicipios + @strComando07)
		end
	else if @intTipoReporte = 3			-- Ventas por Articulo
		begin
			set @strComando01 = ''select S.Codigo as CodigoSucursal, S.Descripcion AS Sucursal, C.Codigo as CodigoClase, C.Descripcion as Clase, P.Codigo as CodigoProducto, P.Descripcion AS Producto, ''
			set @strComando02 = ''convert(numeric(10,2), Sum(D.Cantidad)) as Cantidad, convert(numeric(10,2), Sum(D.Valor) / Sum(D.Cantidad)) as Costo, convert(numeric(10,2), Sum(D.Valor)) as Monto ''
			set @strComando03 = ''from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Vendedores V, prm_Clientes L, prm_Departamentos M WHERE ''
			set @strComando04 = ''F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and F.VendRegistro = V.Registro and ''
			set @strComando05 = ''F.cliregistro = L.registro and L.deptregistro = M.registro ''
			--set @strComando06 = ''group by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			set @strComando06 = ''group by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando06)
		end
	else if @intTipoReporte = 4			-- Ventas por Clases
		begin
			create table #tmp01
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp02
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp01 select S.Codigo as CodigoSucursal, S.Descripcion as Sucursal, C.Codigo as CodigoClase, C.Descripcion as Clase, F.tipofactura, Sum(D.Valor) as Valor, ''
			set @strComando02 = ''Sum(D.Igv) as Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Vendedores V, prm_Clientes L, ''
			set @strComando03 = ''prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and ''
			set @strComando04 = ''C.Registro = P.RegClase and F.VendRegistro = V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, F.TipoFactura order by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigoclase from #tmp01 order by codigosuc, codigoclase
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigoclase
			while @@fetch_status = 0
				begin
					insert into #tmp02
					select distinct codigosuc, nombresuc, codigoclase, nombreclase, 
						(select valor from #tmp01 where tipofactura = 1 and codigosuc = @codigosuc and codigoclase = @codigoclase),
						(select valor from #tmp01 where tipofactura = 0 and codigosuc = @codigosuc and codigoclase = @codigoclase),						
						(select sum(impuesto) from #tmp01 where codigosuc = @codigosuc and codigoclase = @codigoclase), 
						(select sum(valor) + sum(impuesto) from #tmp01 where codigosuc = @codigosuc and codigoclase = @codigoclase)
					from #tmp01
					where codigosuc = @codigosuc and codigoclase = @codigoclase
					fetch next from tmpFact into @codigosuc, @codigoclase
				end
			close tmpFact
			deallocate tmpFact
			select * from #tmp02
		end
	else if @intTipoReporte = 51			-- Ventas por Clientes
		begin
			create table #tmp03
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigocliente	varchar(12),
				nombrecliente	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp04
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigocliente	varchar(12),
				nombrecliente	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp03 select S.Codigo as CodigoSucursal, S.Descripcion as Sucursal, L.Codigo as CodigoCliente, F.Clinombre as Cliente, F.tipofactura, Sum(D.Valor) as Valor, ''
			set @strComando02 = ''Sum(D.Igv) as Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Vendedores V, prm_Productos P, prm_Clientes L, ''
			set @strComando03 = ''prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase ''
			set @strComando04 = ''and F.CliRegistro = L.Registro and F.VendRegistro = V.Registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, L.Codigo, F.Clinombre, F.TipoFactura order by S.Codigo, S.Descripcion, L.Codigo, F.Clinombre, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo +@strNumero +  @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigocliente, nombrecliente from #tmp03 order by codigosuc, codigocliente, nombrecliente
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigocliente, @nombrecliente
			while @@fetch_status = 0
				begin
					insert into #tmp04
					select distinct codigosuc, nombresuc, codigocliente, nombrecliente,
						(select valor from #tmp03 where tipofactura = 1 and codigosuc = @codigosuc and codigocliente = @codigocliente and nombrecliente = @nombrecliente),
						(select valor from #tmp03 where tipofactura = 0 and codigosuc = @codigosuc and codigocliente = @codigocliente and nombrecliente = @nombrecliente),						
						(select sum(impuesto) from #tmp03 where codigosuc = @codigosuc and codigocliente = @codigocliente and nombrecliente = @nombrecliente),
						(select sum(valor) + sum(impuesto) from #tmp03 where codigosuc = @codigosuc and codigocliente = @codigocliente and nombrecliente = @nombrecliente)
					from #tmp03
					where codigosuc = @codigosuc and codigocliente = @codigocliente and nombrecliente = @nombrecliente
					fetch next from tmpFact into @codigosuc, @codigocliente, @nombrecliente
				end
			close tmpFact
			deallocate tmpFact
			select * from #tmp04

		end
	else if @intTipoReporte = 52			-- Ventas por Clientes Detallado
		begin
			set @strComando01 = ''select S.Codigo CodigoSucursal, S.Descripcion Sucursal, L.Codigo CodigoCliente, F.Clinombre Cliente, C.Codigo CodigoClase, C.Descripcion Clase, P.Codigo CodigoProducto, ''
			set @strComando02 = ''P.Descripcion Producto, convert(numeric(10,2), Sum(D.Cantidad)) Cantidad, convert(numeric(10,2), Sum(D.Valor) / Sum(D.Cantidad)) Costo, convert(numeric(10,2), Sum(D.Valor)) ''
			set @strComando03 = ''Monto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Clientes L, prm_Vendedores V, prm_Departamentos M where F.Registro ''
			set @strComando04 = ''= D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and F.CliRegistro = L.Registro and F.VendRegistro = V.Registro ''
			set @strComando05 = ''and L.deptregistro = M.registro ''
			--set @strComando06 = ''group by S.Codigo, S.Descripcion, L.Codigo, F.Clinombre, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, L.Codigo, F.Clinombre, C.Codigo, P.Codigo''
			set @strComando06 = ''group by S.Codigo, S.Descripcion, L.Codigo, F.Clinombre, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, L.Codigo, F.Clinombre, C.Codigo, P.Descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando06)
		end
	else if @intTipoReporte = 6 or @intTipoReporte = 15	-- Ventas por Facturas
		begin
			set @strComando01 = ''select F.FechaIngreso, S.Codigo as CodigoSucursal, S.Descripcion AS Sucursal, F.Numero, C.Codigo as CodigoCliente, F.Clinombre as Cliente, V.Codigo as Vendedor, F.TipoFactura, ''
			set @strComando02 = ''Contado = Case TipoFactura when 0 then Sum(F.Subtotal) else 0 end, TotCont = Case TipoFactura when 0 then (Sum(F.Subtotal) - Sum(F.Retencion) + Sum(F.Impuesto)) else 0 end, ''
			set @strComando03 = ''Credito = Case TipoFactura when 1 then Sum(F.Subtotal) else 0 end, TotCred = Case TipoFactura when 1 then (Sum(F.Subtotal) - Sum(F.Retencion) + Sum(F.Impuesto)) else 0 end, ''
			set @strComando04 = ''convert(numeric(10,2), Sum(F.Retencion)) as Retencion, convert(numeric(10,2), Sum(F.Impuesto)) as Impuesto, ''''Anulado'''' = case when F.numfechaanul <> 0 then ''''A'''' else '''''''' end, ''
			set @strComando05 = ''F.NumFechaIng from tbl_Facturas_Enc F, prm_Agencias S, prm_Clientes C, prm_Vendedores V, prm_Departamentos M where ''
			set @strComando06 = ''F.agenregistro = S.registro and F.cliregistro = C.registro and F.vendregistro = V.registro and C.deptregistro = M.registro ''
			set @strComando07 = ''group by S.Codigo, S.Descripcion, F.FechaIngreso, F.Numero, C.Codigo, F.Clinombre, V.Codigo, F.TipoFactura, F.numfechaanul, F.NumFechaIng ''
			set @strComando08 = ''order by S.Codigo, S.Descripcion, F.NumFechaIng, F.Numero, C.Codigo, F.Clinombre, V.Codigo, F.TipoFactura ''
			if @intTipoReporte = 6
				begin
					exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strAgencias + @strClientes + 
						@strVendedores +  @strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strDepartamento + @strMunicipios + @strComando07 + @strComando08)
				end
			else if @intTipoReporte = 15
				begin
					exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strAgencias + @strClientes + 
						@strVendedores +  @strFecIni + @strFecFin + @strNombre + @intFactTipo + @strNumero + @strDepartamento + @strMunicipios + @strComando07 + @strComando08)
				end
		end
	else if @intTipoReporte =7			-- Ventas por Negocios
		begin
			create table #tmp05
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoneg	varchar(12),
				nombreneg	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp06
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoneg	varchar(12),
				nombreneg	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp05 select S.Codigo as CodigoSucursal, S.Descripcion as Sucursal, N.Codigo as CodigoNeg, N.Descripcion as Negocio, F.tipofactura, Sum(D.Valor) as Valor, ''
			set @strComando02 = ''Sum(D.Igv) as Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Negocios N, prm_Vendedores V, ''
			set @strComando03 = ''prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and ''
			set @strComando04 = ''F.negregistro = N.registro and F.VendRegistro = V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, N.Codigo, N.Descripcion, F.TipoFactura order by S.Codigo, S.Descripcion, N.Codigo, N.Descripcion, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigoneg from #tmp05 order by codigosuc, codigoneg
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigoneg
			while @@fetch_status = 0
				begin
					insert into #tmp06
					select distinct codigosuc, nombresuc, codigoneg, nombreneg, 
						(select valor from #tmp05 where tipofactura = 1 and codigosuc = @codigosuc and codigoneg = @codigoneg),
						(select valor from #tmp05 where tipofactura = 0 and codigosuc = @codigosuc and codigoneg = @codigoneg),						
						(select sum(impuesto) from #tmp05 where codigosuc = @codigosuc and codigoneg = @codigoneg), 
						(select sum(valor) + sum(impuesto) from #tmp05 where codigosuc = @codigosuc and codigoneg = @codigoneg)
					from #tmp05
					where codigosuc = @codigosuc and codigoneg = @codigoneg
					fetch next from tmpFact into @codigosuc, @codigoneg
				end
			close tmpFact
			deallocate tmpFact
			select * from #tmp06
		end
	else if @intTipoReporte = 8			-- Ventas por Vendedores
		begin
			create table #tmp07
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp08
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp07 select S.Codigo as CodigoSucursal, S.Descripcion as Sucursal, V.Codigo as CodigoVend, V.Nombre as Vendedor, F.tipofactura, Sum(D.Valor) as Valor, ''
			set @strComando02 = ''Sum(D.Igv) as Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Vendedores V, ''
			set @strComando03 = ''prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and ''
			set @strComando04 = ''F.vendregistro = V.registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, V.Codigo, V.Nombre, F.TipoFactura order by S.Codigo, S.Descripcion, V.Codigo,V.Nombre, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigovend from #tmp07 order by codigosuc, codigovend
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigovend
			while @@fetch_status = 0
				begin
					insert into #tmp08
					select distinct codigosuc, nombresuc, codigovend, nombrevend, 
						(select valor from #tmp07 where tipofactura = 1 and codigosuc = @codigosuc and codigovend = @codigovend),
						(select valor from #tmp07 where tipofactura = 0 and codigosuc = @codigosuc and codigovend = @codigovend),						
						(select sum(impuesto) from #tmp07 where codigosuc = @codigosuc and codigovend = @codigovend), 
						(select sum(valor) + sum(impuesto) from #tmp07 where codigosuc = @codigosuc and codigovend = @codigovend)
					from #tmp07
					where codigosuc = @codigosuc and codigovend = @codigovend
					fetch next from tmpFact into @codigosuc, @codigovend
				end
			close tmpFact
			deallocate tmpFact
			select * from #tmp08
		end
	else if @intTipoReporte = 9			-- Ventas por Departamentos
		begin
			create table #tmp09
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigodept	varchar(12),
				nombredept	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp10
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigodept	varchar(12),
				nombredept	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp09 select S.Codigo CodigoSucursal, S.Descripcion Sucursal, M.Codigo CodigoDept, M.Descripcion Departamento, F.tipofactura, Sum(D.Valor) Valor, ''
			set @strComando02 = ''Sum(D.Igv) Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clientes L, prm_Clases C, prm_Productos P, prm_Departamentos M, ''
			set @strComando03 = ''prm_Vendedores V where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and ''
			set @strComando04 = ''F.VendRegistro = V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, M.Codigo, M.Descripcion, F.TipoFactura order by S.Codigo, S.Descripcion, M.Codigo, M.Descripcion, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigodept from #tmp09 order by codigosuc, codigodept
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigodept
			while @@fetch_status = 0
				begin
					insert into #tmp10
					select distinct codigosuc, nombresuc, codigodept, nombredept, 
						(select valor from #tmp09 where tipofactura = 1 and codigosuc = @codigosuc and codigodept = @codigodept),
						(select valor from #tmp09 where tipofactura = 0 and codigosuc = @codigosuc and codigodept = @codigodept),						
						(select sum(impuesto) from #tmp09 where codigosuc = @codigosuc and codigodept = @codigodept), 
						(select sum(valor) + sum(impuesto) from #tmp09 where codigosuc = @codigosuc and codigodept = @codigodept)
					from #tmp09
					where codigosuc = @codigosuc and codigodept = @codigodept
					fetch next from tmpFact into @codigosuc, @codigodept
				end
			close tmpFact
			deallocate tmpFact
			select * from #tmp10
		end
	else if @intTipoReporte = 10			-- Ventas por Vendedor y Clase
		begin
			create table #tmp11
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp12
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp11 select S.Codigo as CodigoSucursal, S.Descripcion as Sucursal, V.Codigo as CodigoVendedor, V.Nombre as Vendedor, C.Codigo as CodigoClase, ''
			set @strComando02 = ''C.Descripcion as Clase, F.tipofactura, Sum(D.Valor) as Valor, Sum(D.Igv) as Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, ''
			set @strComando03 = ''prm_Clases C, prm_Productos P, prm_Vendedores V, prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and ''
			set @strComando04 = ''D.ProdRegistro = P.Registro and C.Registro = P.RegClase and F.vendregistro = V.registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, F.TipoFactura order by S.Codigo, V.Codigo, C.Codigo, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigovend, codigoclase from #tmp11 order by codigosuc, codigovend, codigoclase
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigovend, @codigoclase
			while @@fetch_status = 0
				begin
					insert into #tmp12
					select distinct codigosuc, nombresuc, codigovend, nombrevend, codigoclase, nombreclase,
						(select valor from #tmp11 where tipofactura = 1 and codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase),
						(select valor from #tmp11 where tipofactura = 0 and codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase),
						(select sum(impuesto) from #tmp11 where codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase), 
						(select sum(valor) + sum(impuesto) from #tmp11 where codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase)
					from #tmp11
					where codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase
					fetch next from tmpFact into @codigosuc, @codigovend, @codigoclase
				end
			close tmpFact
			deallocate tmpFact
			select * from #tmp12 order by codigosuc, codigovend  compute sum(valorcredito) compute sum(valorcontado) compute sum(impuesto) compute sum(montototal)
		end
	else if @intTipoReporte = 11			-- Reporte de Costos
		begin
			create table #tmpCosto_01
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				codigoprod	varchar(12),
				nombreprod	varchar(60),
				cantidad	numeric(12,2),
				prod_mto	numeric(12,2)
			)
			create table #tmpCosto_02
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				codigoprod	varchar(12),
				nombreprod	varchar(60),
				cantidad	numeric(12,2),
				prod_mto	numeric(12,2)
			)
			create table #tmpCosto_03
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				codigoprod	varchar(12),
				nombreprod	varchar(60),
				cantidad	numeric(12,2),
				prod_vta	numeric(12,2),
				prod_cto	numeric(12,2),
				prod_utl		numeric(12,2),
				prod_porc	numeric(12,2),
				porc_clase	numeric(12,2),
				porc_agencia	numeric(12,2),
				porc_reporte	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmpCosto_02 select S.Codigo CodigoSucursal, S.Descripcion Sucursal, C.Codigo CodigoClase, C.Descripcion Clase, P.Codigo CodigoProducto, P.Descripcion Producto, ''
			set @strComando02 = ''convert(numeric(10,2), Sum(D.Cantidad)) Cantidad, convert(numeric(10,2), Sum(D.Valor)) Monto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, ''
			set @strComando03 = ''prm_Productos P, prm_Vendedores V, prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and ''
			set @strComando04 = ''C.Registro = P.RegClase and F.Status = 0 and F.VendRegistro = V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)
			set @strComando01E = ''and f.numdocumento in (select distinct f.numero from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Vendedores V, ''
			set @strComando02E = ''prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and ''
			set @strComando03E = ''F.Status = 0 and F.VendRegistro = V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando08 = '')''

			set @strComando01 = ''insert into #tmpCosto_01 select s.codigo CodSuc, s.descripcion Sucursal, c.codigo CodClase, c.descripcion Clase, p.codigo CodProd, p.descripcion Producto, ''
			set @strComando02 = ''sum(f.debitos_cnt) - sum(f.creditos_cnt) Deb_Cnt, sum(f.debitos_mto) - sum(f.creditos_mto) Debitos_Mto ''
			set @strComando03 = ''from tbl_producto_movimiento f, prm_productos p, ''
			set @strComando04 = ''prm_agencias s, prm_clases c where f.agenregistro = s.registro and f.prodregistro = p.registro and p.regclase = c.registro and f.Origen = 1 ''
			set @strComando05 = ''group by s.codigo, s.descripcion, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			set @strComando06 = ''order by s.codigo, s.descripcion, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strProveedores + 
				@strFecIni + @strFecFin + @strComando01E + @strComando02E + @strComando03E + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando08 + @strComando05 + @strComando06)

			insert into #tmpCosto_03 select t1.codigosuc, t1.nombresuc, t1.codigoclase, t1.nombreclase, t1.codigoprod, t1.nombreprod, t1.cantidad, t2.prod_mto, t1.prod_mto, t2.prod_mto - t1.prod_mto, 
				(((t2.prod_mto - t1.prod_mto) / t2.prod_mto) * 100), 0, 0, 0 from #tmpCosto_01 t1, #tmpCosto_02 t2 where t1.codigosuc = t2.codigosuc and t1.codigoprod = t2.codigoprod and t2.prod_mto != 0
			insert into #tmpCosto_03 select t1.codigosuc, t1.nombresuc, t1.codigoclase, t1.nombreclase, t1.codigoprod, t1.nombreprod, t1.cantidad, t2.prod_mto, t1.prod_mto, t2.prod_mto - t1.prod_mto, 
				0, 0, 0, 0 from #tmpCosto_01 t1, #tmpCosto_02 t2 where t1.codigosuc = t2.codigosuc and t1.codigoprod = t2.codigoprod and t2.prod_mto = 0

			declare tmpCursor cursor for
				select distinct codigosuc, codigoclase from #tmpCosto_03 order by codigosuc, codigoclase
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia, @codigoclase
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					set @intTipoReporte = 0
					select @intTipoReporte = sum(prod_vta) from 
						#tmpCosto_03 where codigosuc = @codigoagencia and codigoclase = @codigoclase and prod_vta != 0
					if @intTipoReporte <> 0
						begin
							select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
								#tmpCosto_03 where codigosuc = @codigoagencia and codigoclase = @codigoclase and prod_vta != 0
						end				
					update #tmpCosto_03 set porc_clase = @porcent_grupo where codigosuc = @codigoagencia 
						and codigoclase = @codigoclase
					fetch next from tmpCursor into @codigoagencia, @codigoclase
				end
			close tmpCursor
			deallocate tmpCursor

			declare tmpCursor cursor for
				select distinct codigosuc from #tmpCosto_03 order by codigosuc
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					set @intTipoReporte = 0
					select @intTipoReporte = sum(prod_vta) from 
						#tmpCosto_03 where codigosuc = @codigoagencia
					if @intTipoReporte <> 0
						begin
							select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
								#tmpCosto_03 where codigosuc = @codigoagencia and prod_vta != 0
						end
					update #tmpCosto_03 set porc_agencia = @porcent_grupo where codigosuc = @codigoagencia
					fetch next from tmpCursor into @codigoagencia
				end
			close tmpCursor
			deallocate tmpCursor
			set @dblporc_vtas = 0
			set @dblporc_util = 0
			select @dblporc_vtas = sum(prod_vta) from #tmpCosto_03
			select @dblporc_util = sum(prod_utl) from #tmpCosto_03
			update #tmpCosto_03 set porc_reporte = (@dblporc_util / @dblporc_vtas) * 100
			--select * from #tmpCosto_03 order by codigosuc, codigoclase, codigoprod
			select * from #tmpCosto_03 order by codigosuc, codigoclase, nombreprod
		end
	else if @intTipoReporte = 12			-- Reporte de Bonificaciones
		begin
			set @strComando01 = ''select S.Codigo codigosuc, S.Descripcion nombresuc, C.Codigo codigoclase, C.Descripcion nombreclase, P.Codigo codigoprod, P.Descripcion nombreprod, convert(numeric(10,2), ''
			set @strComando02 = ''Sum(D.Cantidad)) cantidad, 0 prod_vta, 0 prod_cto, 0 prod_utl, 0 prod_porc from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, ''
			set @strComando03 = ''prm_Vendedores V, prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and ''
			set @strComando04 = ''C.Registro = P.RegClase and F.Status = 0 and D.Valor = 0 and F.VendRegistro = V.Registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			--set @strComando05 = ''group by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion order by S.Codigo, S.Descripcion, C.Codigo, C.Descripcion, P.Descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactTipo + @strNumero + @strSubClase + @strDepartamento + @strMunicipios + @strComando05)

		end
	else if @intTipoReporte = 13			-- Ventas por Vendedor y Clase por Agencias
		begin
			create table #tmp13
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				tipofactura	tinyint,
				valor		numeric(12,2),
				impuesto	numeric(12,2)
			)
			create table #tmp14
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				valorcredito	numeric(12,2),
				valorcontado	numeric(12,2),
				impuesto	numeric(12,2),
				montototal	numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp13 select S.Codigo CodigoSucursal, S.Descripcion Sucursal, V.Codigo CodigoVendedor, V.Nombre Vendedor, C.Codigo CodigoClase, C.Descripcion Clase, ''
			set @strComando02 = ''F.tipofactura, Sum(D.Valor) Valor, Sum(D.Igv) Impuesto from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, ''
			set @strComando03 = ''prm_Vendedores V, prm_Clientes L, prm_Departamentos M where F.Registro = D.Registro and F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and ''
			set @strComando04 = ''C.Registro = P.RegClase and F.vendregistro = V.registro and F.cliregistro = L.registro and L.deptregistro = M.registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, F.TipoFactura order by S.Codigo, V.Codigo, C.Codigo, F.TipoFactura''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactStatus + @intFactTipo + @strSubClase + @strNumero + @strDepartamento + @strMunicipios + @strComando05)
			declare tmpFact cursor for
				select distinct codigosuc, codigovend, codigoclase from #tmp13 order by codigosuc, codigovend, codigoclase
			open tmpFact
			fetch next from tmpFact into @codigosuc, @codigovend, @codigoclase
			while @@fetch_status = 0
				begin
					insert into #tmp14
					select distinct codigosuc, nombresuc, codigovend, nombrevend, codigoclase, nombreclase,
						(select valor from #tmp13 where tipofactura = 1 and codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase),
						(select valor from #tmp13 where tipofactura = 0 and codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase),
						(select sum(impuesto) from #tmp13 where codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase), 
						(select sum(valor) + sum(impuesto) from #tmp13 where codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase)
					from #tmp13
					where codigosuc = @codigosuc and codigovend = @codigovend and codigoclase = @codigoclase
					fetch next from tmpFact into @codigosuc, @codigovend, @codigoclase
				end
			close tmpFact
			deallocate tmpFact
			select codigosuc codigovend, nombresuc nombrevend, codigovend codigosuc, nombrevend nombresuc, codigoclase, nombreclase, valorcredito, 
				valorcontado, impuesto, montototal from #tmp14 order by codigovend, codigosuc
		end
	else if @intTipoReporte = 14			-- Reporte de Costos
		begin
			create table #tmpCosto_04
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				codigoprod	varchar(12),
				nombreprod	varchar(60),
				cantidad	numeric(12,2),
				prod_mto	numeric(12,2)
			)
			create table #tmpCosto_05
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				codigoprod	varchar(12),
				nombreprod	varchar(60),
				cantidad	numeric(12,2),
				prod_mto	numeric(12,2)
			)
			create table #tmpCosto_06
			(
				codigosuc	varchar(12),
				nombresuc	varchar(60),
				codigovend	varchar(12),
				nombrevend	varchar(60),
				codigoclase	varchar(12),
				nombreclase	varchar(60),
				codigoprod	varchar(12),
				nombreprod	varchar(60),
				cantidad	numeric(12,2),
				prod_vta	numeric(12,2),
				prod_cto	numeric(12,2),
				prod_utl		numeric(12,2),
				prod_porc	numeric(5,2),
				porc_clase	numeric(5,2),
				porc_vend	numeric(5,2),
				porc_agencia	numeric(5,2)
			)
			set @strComando01 = ''insert into #tmpCosto_04 select s.codigo CodSuc, s.descripcion Sucursal, c.codigo CodClase, c.descripcion Clase, p.codigo CodProd, p.descripcion Producto, ''
			set @strComando02 = ''sum(f.debitos_cnt) - sum(f.creditos_cnt) Deb_Cnt, sum(f.debitos_mto) - sum(f.creditos_mto) Debitos_Mto ''
			set @strComando03 = ''from tbl_producto_movimiento f, prm_productos p, ''
			set @strComando04 = ''prm_agencias s, prm_clases c where f.agenregistro = s.registro and f.prodregistro = p.registro and p.regclase = c.registro and f.Origen = 1 ''
			set @strComando05 = ''group by s.codigo, s.descripcion, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			set @strComando06 = ''order by s.codigo, s.descripcion, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strProveedores + 
				@strFecIni + @strFecFin + @strSubClase + @strComando05 + @strComando06)

			set @strComando01 = ''insert into #tmpCosto_05 select S.Codigo as CodigoSucursal, S.Descripcion AS Sucursal, V.Codigo, V.Nombre, C.Codigo as CodigoClase, C.Descripcion as Clase, ''
			set @strComando02 = ''P.Codigo as CodigoProducto, P.Descripcion AS Producto, convert(numeric(10,2), Sum(D.Cantidad)) as Cantidad, convert(numeric(10,2), Sum(D.Valor)) as Monto ''
			set @strComando03 = ''from tbl_Facturas_Enc F, tbl_Facturas_Det D, prm_Agencias S, prm_Clases C, prm_Productos P, prm_Vendedores V where F.Registro = D.Registro and ''
			set @strComando04 = ''F.AgenRegistro = S.Registro and D.ProdRegistro = P.Registro and C.Registro = P.RegClase and F.Status = 0 and F.VendRegistro = V.Registro ''
			set @strComando05 = ''group by S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion ''
			set @strComando06 = ''order by S.Codigo, S.Descripcion, V.Codigo, V.Nombre, C.Codigo, C.Descripcion, P.Codigo, P.Descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strClientes + @strVendedores + @strProveedores + 
				@strFecIni + @strFecFin + @strNombre + @intFactTipo + @strSubClase + @strNumero + @strComando05)

			insert into #tmpCosto_06 select t1.codigosuc, t1.nombresuc, t2.codigovend, t2.nombrevend, t1.codigoclase, t1.nombreclase, t1.codigoprod, t1.nombreprod, t1.cantidad, t2.prod_mto, t1.prod_mto, t2.prod_mto - t1.prod_mto, 
				(((t2.prod_mto - t1.prod_mto) / t2.prod_mto) * 100), 0, 0, 0 from #tmpCosto_04 t1, #tmpCosto_05 t2 where t1.codigosuc = t2.codigosuc and t1.codigoprod = t2.codigoprod and t2.prod_mto != 0
			insert into #tmpCosto_06 select t1.codigosuc, t1.nombresuc, t2.codigovend, t2.nombrevend, t1.codigoclase, t1.nombreclase, t1.codigoprod, t1.nombreprod, t1.cantidad, t2.prod_mto, t1.prod_mto, t2.prod_mto - t1.prod_mto, 
				0, 0, 0, 0 from #tmpCosto_04 t1, #tmpCosto_05 t2 where t1.codigosuc = t2.codigosuc and t1.codigoprod = t2.codigoprod and t2.prod_mto = 0

			declare tmpCursor cursor for
				select distinct codigosuc, codigoclase from #tmpCosto_06 order by codigosuc, codigoclase
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia, @codigoclase
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
						#tmpCosto_06 where codigosuc = @codigoagencia and codigoclase = @codigoclase
					update #tmpCosto_06 set porc_clase = @porcent_grupo where codigosuc = @codigoagencia 
						and codigoclase = @codigoclase
					fetch next from tmpCursor into @codigoagencia, @codigoclase
				end
			close tmpCursor
			deallocate tmpCursor

			declare tmpCursor cursor for
				select distinct codigosuc, codigovend from #tmpCosto_06 order by codigosuc, codigovend
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia, @codigoclase
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
						#tmpCosto_06 where codigosuc = @codigoagencia and codigoclase = @codigoclase
					update #tmpCosto_06 set porc_vend = @porcent_grupo where codigosuc = @codigoagencia 
						and codigovend = @codigoclase
					fetch next from tmpCursor into @codigoagencia, @codigoclase
				end
			close tmpCursor
			deallocate tmpCursor

			declare tmpCursor cursor for
				select distinct codigosuc from #tmpCosto_06 order by codigosuc
			open tmpCursor
			fetch next from tmpCursor into @codigoagencia
			while @@fetch_status = 0
				begin
					set @porcent_grupo = 0
					select @porcent_grupo = (((sum(prod_vta) - sum(prod_cto)) / sum(prod_vta)) * 100) from
						#tmpCosto_06 where codigosuc = @codigoagencia
					update #tmpCosto_06 set porc_agencia = @porcent_grupo where codigosuc = @codigoagencia
					fetch next from tmpCursor into @codigoagencia
				end
			close tmpCursor
			deallocate tmpCursor
			--select * from #tmpCosto_06 order by codigosuc, codigovend, codigoclase, codigoprod
			select * from #tmpCosto_06 order by codigosuc, codigovend, codigoclase, nombreprod
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UbicarFacturas]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_UbicarFacturas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_UbicarFacturas] @lngAgencia as tinyint, @strNumero as varchar(40)
As

declare	@strCommand1 as varchar(75),
		@strCommand2 as varchar(75),
		@strCommand3 as varchar(75)

begin
	set nocount on
	set @strCommand1 = ''''
	set @strCommand2 = ''''
	set @strCommand1 = ''select e.numero, c.codigo, c.nombre, e.fechaingreso, e.total from ''
	set @strCommand2 = ''tbl_facturas_enc e, prm_clientes c where e.agenregistro = ''
	set @strCommand3 = ''and e.cliregistro = c.registro and e.numero in ''
	exec (@strCommand1 + @strCommand2 + @lngAgencia + @strCommand3 + @strNumero)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Reportes]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Reportes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_Reportes] @intModulo as tinyint
As
Declare	@strComando01 as varchar(255),
	@strComando02 as varchar(255),
	@strComando03 as varchar(255)
begin
	set nocount on
	select @strComando01 = ''''
	select @strComando02 = ''''
	select @strComando03 = ''''
	if @intModulo = 0
		begin
			select @strComando01 = ''Select CodUsuario, Nombre, Apellido, Cargo, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Usuarios''
		end
	else if @intModulo = 1
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Agencias''
		end
	else if @intModulo = 2
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Departamentos''
		end
	else if @intModulo = 3
		begin
			select @strComando01 = ''Select M.Codigo, M.Descripcion, D.Descripcion ''''estado'''' from prm_Municipios M, prm_Departamentos D where M.registro2 = D.registro ''
			select @strComando02 = ''order by D.Descripcion, M.Descripcion''
		end
	else if @intModulo = 4
		begin
			select @strComando01 = ''Select Codigo, Nombre Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Vendedores''
		end
	else if @intModulo = 5
		begin
			select @strComando01 = ''Select Codigo, Nombre Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Clientes''
		end
	else if @intModulo = 6
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Clases''
		end
	else if @intModulo = 7
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_SubClases''
		end
	else if @intModulo = 8
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Empaques''
		end
	else if @intModulo = 9
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Unidades''
		end
	else if @intModulo = 10
		begin
			select @strComando01 = ''Select Codigo, Nombre  Descripcion, ''''Estado'''' = case when tipo = 0 then ''''Extranjero'''' when tipo = 1 then ''''Nacional'''' end, ''''Estado2'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Proveedores''
		end
	else if @intModulo = 11
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Productos''
		end
	else if @intModulo = 12
		begin
			select @strComando01 = ''Select de_cantidad, (select Descripcion from prm_Unidades where registro = de_unidad) de_unidad, a_cantidad, (select Descripcion from prm_Unidades where registro = a_unidad) a_unidad ''
			select @strComando02 = ''from prm_Conversiones''
		end
	else if @intModulo = 13
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Negocios''
		end
	else if @intModulo = 15
		begin
			select @strComando01 = ''Select FechaConversion, Documento, (select Descripcion from prm_Agencias where registro = agenregistro01) sucursal01, (select Descripcion from prm_Productos where registro = prodregistro01) producto01, ''
			select @strComando02 = ''(select Descripcion from prm_Agencias where registro = agenregistro01) sucursal02, (select Descripcion from prm_Productos where registro = prodregistro02) producto02 ''
			select @strComando03 = ''from tbl_ProductoConv order by registro''
		end
	else if @intModulo = 19
		begin
			select @strComando01 = ''Select convert(varchar(12), fecha, 113) codigo, tipo_cambio descripcion, '''' '''' as estado from prm_TipoCambio''
		end
	else if @intModulo = 22
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Bancos''
		end
	else if @intModulo = 23
		begin
			select @strComando01 = ''Select Cuenta Codigo, Descripcion from prm_CtasContables order by registro''
		end
	else if @intModulo = 24
		begin
			select @strComando01 = ''Select Numero as Codigo, (select Descripcion from prm_Agencias where registro = agenregistro) Descripcion, ''
			select @strComando02 = ''''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Procesado'''' end from tbl_CambiarNumFact''
		end
	else if @intModulo = 25
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Areas''
		end
	else if @intModulo = 26
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Puestos''
		end
	else if @intModulo = 27
		begin
			select @strComando01 = ''Select Codigo, Nombres + '''' '''' + PriApellido Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' ''
			select @strComando02 = ''else ''''Eliminado'''' end from prm_Empleados''
		end
	else if @intModulo = 28
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' ''
			select @strComando02 = ''else ''''Eliminado'''' end from prm_CtasBancarias''
		end
	else if @intModulo = 29
		begin
			select @strComando01 = ''Select ''''codigo'''' = case when c.estado = 0 then ''''Activo'''' when c.estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end , c.CkInicial Estado, ''
			select @strComando02 = ''c.CkFinal Estado2, (b.codigo + ''''  '''' + b.descripcion) descripcion from prm_Chequeras c, prm_CtasBancarias b where c.ctabancregistro = b.registro''
		end
	else if @intModulo = 35
		begin
			select @strComando01 = ''Select v.Codigo, v.nombre Descripcion, c.descripcion estado from prm_Comisiones c, prm_Vendedores v where c.vendregistro = v.registro''
		end
	else if @intModulo = 42
		begin
			select @strComando01 = ''Select Codigo, Nombre Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Beneficiarios''
		end
	exec (@strComando01 + @strComando02 + @strComando03)
	set nocount off
end
' 
END
GO
/****** Object:  Table [dbo].[tbl_Cheques_Enc]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cheques_Enc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Cheques_Enc](
	[registro] [bigint] NOT NULL,
	[ctabanregistro] [tinyint] NOT NULL,
	[benefregistro] [smallint] NOT NULL,
	[beneficiario] [varchar](80) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[fechacheque] [smalldatetime] NOT NULL,
	[fechaingreso] [smalldatetime] NOT NULL,
	[documento] [varchar](12) NOT NULL,
	[monto] [numeric](18, 2) NOT NULL,
	[tipomovim] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
	[numfechacheque] [int] NOT NULL,
	[numfechaingreso] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cheques_Enc]') AND name = N'IX_tbl_Cheques_Enc_1')
CREATE CLUSTERED INDEX [IX_tbl_Cheques_Enc_1] ON [dbo].[tbl_Cheques_Enc] 
(
	[numfechacheque] ASC,
	[tipomovim] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cheques_Enc]') AND name = N'IX_tbl_Cheques_Enc')
CREATE NONCLUSTERED INDEX [IX_tbl_Cheques_Enc] ON [dbo].[tbl_Cheques_Enc] 
(
	[registro] ASC,
	[tipomovim] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Cheques_Det]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Cheques_Det]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Cheques_Det](
	[registro] [bigint] NOT NULL,
	[numcomprobante] [varchar](12) NOT NULL,
	[descomprobante] [varchar](80) NOT NULL,
	[documento] [varchar](12) NOT NULL,
	[descripcion] [varchar](200) NOT NULL,
	[ctacontregistro] [smallint] NOT NULL,
	[montoD] [numeric](18, 2) NOT NULL,
	[montoH] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Facturas_Enc]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Enc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Facturas_Enc](
	[registro] [bigint] NOT NULL,
	[numero] [varchar](12) NOT NULL,
	[numfechaing] [int] NOT NULL,
	[fechaingreso] [varchar](12) NOT NULL,
	[vendregistro] [tinyint] NOT NULL,
	[cliregistro] [smallint] NOT NULL,
	[clinombre] [varchar](50) NOT NULL,
	[subtotal] [numeric](12, 2) NOT NULL,
	[impuesto] [numeric](12, 2) NOT NULL,
	[retencion] [numeric](12, 2) NOT NULL,
	[total] [numeric](12, 2) NOT NULL,
	[userregistro] [tinyint] NOT NULL,
	[agenregistro] [tinyint] NOT NULL,
	[status] [tinyint] NOT NULL,
	[impreso] [tinyint] NOT NULL,
	[deptregistro] [tinyint] NOT NULL,
	[negregistro] [tinyint] NOT NULL,
	[tipofactura] [tinyint] NOT NULL,
	[diascredito] [tinyint] NOT NULL,
	[tipocambio] [numeric](13, 4) NOT NULL,
	[numfechaanul] [int] NOT NULL,
 CONSTRAINT [PK_tbl_Facturas_Enc] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Enc]') AND name = N'IX_tbl_Facturas_Enc')
CREATE CLUSTERED INDEX [IX_tbl_Facturas_Enc] ON [dbo].[tbl_Facturas_Enc] 
(
	[agenregistro] ASC,
	[numfechaing] ASC,
	[numero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Enc]') AND name = N'IX_tbl_Facturas_Enc_1')
CREATE NONCLUSTERED INDEX [IX_tbl_Facturas_Enc_1] ON [dbo].[tbl_Facturas_Enc] 
(
	[agenregistro] ASC,
	[numero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Enc]') AND name = N'IX_tbl_Facturas_Enc_2')
CREATE NONCLUSTERED INDEX [IX_tbl_Facturas_Enc_2] ON [dbo].[tbl_Facturas_Enc] 
(
	[numfechaing] ASC,
	[registro] ASC,
	[vendregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Beneficiarios]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Beneficiarios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Beneficiarios](
	[registro] [smallint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[nombre] [varchar](65) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Beneficiarios] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Beneficiarios]') AND name = N'IX_prm_Beneficiarios')
CREATE UNIQUE CLUSTERED INDEX [IX_prm_Beneficiarios] ON [dbo].[prm_Beneficiarios] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Municipios]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Municipios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Municipios](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
	[registro2] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Municipios] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Municipios] UNIQUE CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[sp_General]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_General]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_General] @intModulo as tinyint
As
Declare	@strComando01 as varchar(175),
	@strComando02 as varchar(128),
	@strComando03 as varchar(128),
	@strComando04 as varchar(128),
	@strComando05 as varchar(128)
begin
	set nocount on
	set @strComando01 = ''''
	set @strComando02 = ''''
	set @strComando03 = ''''
	set @strComando04 = ''''
	set @strComando05 = ''''
	if @intModulo = 0
		begin
			select @strComando01 = ''Select CodUsuario, Nombre, Apellido, Cargo, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end ''
			select @strComando02 = ''from prm_Usuarios''
		end
	else if @intModulo = 1
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Agencias''
		end
	else if @intModulo = 2
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end ''
			select @strComando02 = ''from prm_Departamentos''
		end
	else if @intModulo = 3
		begin
			select @strComando01 = ''Select M.Codigo, M.Descripcion, D.Descripcion from prm_Municipios M, prm_Departamentos D where M.registro2 = D.registro ''
			select @strComando02 = ''order by D.Descripcion, M.Descripcion''
		end
	else if @intModulo = 4
		begin
			select @strComando01 = ''Select Codigo, Nombre, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Vendedores''
		end
	else if @intModulo = 5
		begin
			select @strComando01 = ''Select Codigo, Nombre, Cta_contable, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Clientes''
		end
	else if @intModulo = 6
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Clases''
		end
	else if @intModulo = 7
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_SubClases''
		end
	else if @intModulo = 8
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Empaques''
		end
	else if @intModulo = 9
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Unidades''
		end
	else if @intModulo = 10
		begin
			select @strComando01 = ''Select Codigo, Nombre, ''''tipo'''' = case when tipo = 0 then ''''Extranjero'''' when tipo = 1 then ''''Nacional'''' end, ''
			select @strComando02 = ''''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Proveedores''
		end
	else if @intModulo = 11
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Productos''
		end
	else if @intModulo = 12
		begin
			select @strComando01 = ''Select de_cantidad, (select Descripcion from prm_Unidades where registro = de_unidad) de_unidad, a_cantidad, ''
			select @strComando02 = ''(select Descripcion from prm_Unidades where registro = a_unidad) a_unidad, ''
			select @strComando03 = ''''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Conversiones''
		end
	else if @intModulo = 13
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Negocios''
		end
	else if @intModulo = 15
		begin
			select @strComando01 = ''Select FechaConversion, Documento, (select Descripcion from prm_Agencias where registro = agenregistro01) sucursal01, ''
			select @strComando02 = ''(select Descripcion from prm_Productos where registro = prodregistro01) producto01, ''
			select @strComando03 = ''(select Descripcion from prm_Agencias where registro = agenregistro01) sucursal02, ''
			select @strComando04 = ''(select Descripcion from prm_Productos where registro = prodregistro02) producto02 ''
			select @strComando05 = ''from tbl_ProductoConv order by registro''
		end
	else if @intModulo = 19
		begin
			select @strComando01 = ''Select convert(varchar(12), fecha, 113) fecha, tipo_cambio from prm_TipoCambio'' 
		end
	else if @intModulo = 22
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Bancos''
		end
	else if @intModulo = 23
		begin
			select @strComando01 = ''Select Cuenta Codigo, Descripcion from prm_CtasContables order by registro''
		end
	else if @intModulo = 24
		begin
			select @strComando01 = ''Select ''''TipoFactura'''' = case when tipofactura = 0 then ''''contado'''' when tipofactura = 1 then ''''credito'''' end, ''
			select @strComando02 = ''(select Descripcion from prm_Agencias where registro = agenregistro) agencia, ''
			select @strComando03 = ''Numero, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Procesado'''' ''
			select @strComando04 = ''when estado = 2 then ''''Cancelado'''' end, Registro from tbl_CambiarNumFact''
		end
	else if @intModulo = 25
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Areas''
		end
	else if @intModulo = 26
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Puestos''
		end
	else if @intModulo = 27
		begin
			select @strComando01 = ''Select Codigo, Nombres + '''' '''' + PriApellido Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' ''
			select @strComando02 = ''else ''''Eliminado'''' end from prm_Empleados''
		end
	else if @intModulo = 28
		begin
			select @strComando01 = ''Select Codigo, Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' ''
			select @strComando02 = ''else ''''Eliminado'''' end from prm_CtasBancarias''
		end
	else if @intModulo = 29
		begin
			select @strComando01 = ''Select c.CkInicial, c.CkFinal, (b.codigo + ''''  '''' + b.descripcion) descripcion from prm_Chequeras c, prm_CtasBancarias b where c.ctabancregistro = b.registro''
		end
	else if @intModulo = 35
		begin
			select @strComando01 = ''Select v.Codigo, v.nombre Descripcion, c.descripcion estado from prm_Comisiones c, prm_Vendedores v where c.vendregistro = v.registro''
		end
	else if @intModulo = 42
		begin
			select @strComando01 = ''Select Codigo, Nombre Descripcion, ''''estado'''' = case when estado = 0 then ''''Activo'''' when estado = 1 then ''''Inactivo'''' else ''''Eliminado'''' end from prm_Beneficiarios''
		end
	exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05)
	set nocount off
end
' 
END
GO
/****** Object:  Table [dbo].[prm_Comisiones]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Comisiones]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Comisiones](
	[registro] [tinyint] NOT NULL,
	[vendregistro] [tinyint] NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[dia01] [smallint] NOT NULL,
	[dia02] [smallint] NOT NULL,
	[por01] [numeric](5, 2) NOT NULL,
	[dia03] [smallint] NOT NULL,
	[dia04] [smallint] NOT NULL,
	[por02] [numeric](5, 2) NOT NULL,
	[dia05] [smallint] NOT NULL,
	[dia06] [smallint] NOT NULL,
	[por03] [numeric](5, 2) NOT NULL,
	[clases] [varchar](255) NULL,
	[subclases] [varchar](255) NULL,
	[inclprod] [varchar](255) NULL,
	[exclprod] [varchar](255) NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Comisiones] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_Comisiones]') AND name = N'IX_prm_Comisiones')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_Comisiones] ON [dbo].[prm_Comisiones] 
(
	[vendregistro] ASC,
	[descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_TipoCambio]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_TipoCambio]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_TipoCambio](
	[fecha] [varchar](12) NOT NULL,
	[tipo_cambio] [numeric](13, 4) NOT NULL,
	[numfecha] [int] NOT NULL,
 CONSTRAINT [PK_prm_TipoCambio] PRIMARY KEY CLUSTERED 
(
	[numfecha] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_CtasContables]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasContables]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_CtasContables](
	[registro] [smallint] NOT NULL CONSTRAINT [DF_prm_CtasContables_registro]  DEFAULT (0),
	[cuenta] [varchar](16) NOT NULL CONSTRAINT [DF_prm_CtasContables_cuenta]  DEFAULT (0),
	[descripcion] [varchar](80) NULL,
 CONSTRAINT [PK_prm_CtasContables] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasContables]') AND name = N'IX_prm_CtasContables')
CREATE UNIQUE CLUSTERED INDEX [IX_prm_CtasContables] ON [dbo].[prm_CtasContables] 
(
	[cuenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Producto_Movimiento]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Producto_Movimiento]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Producto_Movimiento](
	[registro] [bigint] NOT NULL,
	[numfechaing] [numeric](8, 0) NOT NULL,
	[numtiempo] [int] NOT NULL,
	[agenregistro] [tinyint] NOT NULL,
	[prodregistro] [smallint] NOT NULL,
	[fechaingreso] [varchar](12) NOT NULL,
	[numdocumento] [varchar](12) NOT NULL,
	[codigomovim] [varchar](3) NOT NULL,
	[codigomoneda] [char](3) NOT NULL,
	[totalfactura] [numeric](12, 2) NOT NULL,
	[creditos_cnt] [numeric](12, 2) NOT NULL,
	[creditos_cto] [numeric](12, 2) NOT NULL,
	[creditos_mto] [numeric](12, 2) NOT NULL,
	[debitos_cnt] [numeric](12, 2) NOT NULL,
	[debitos_cto] [numeric](12, 2) NOT NULL,
	[debitos_mto] [numeric](12, 2) NOT NULL,
	[saldo_cnt] [numeric](12, 2) NOT NULL,
	[saldo_cto] [numeric](12, 2) NOT NULL,
	[saldo_mto] [numeric](12, 2) NOT NULL,
	[cuenta_debe] [varchar](15) NULL,
	[cuenta_haber] [varchar](15) NULL,
	[userregistro] [tinyint] NOT NULL,
	[origen] [tinyint] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Producto_Movimiento]') AND name = N'IX_tbl_Producto_Movimiento')
CREATE CLUSTERED INDEX [IX_tbl_Producto_Movimiento] ON [dbo].[tbl_Producto_Movimiento] 
(
	[agenregistro] ASC,
	[prodregistro] ASC,
	[numfechaing] ASC,
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Producto_Movimiento]') AND name = N'IX_tbl_Producto_Movimiento_1')
CREATE NONCLUSTERED INDEX [IX_tbl_Producto_Movimiento_1] ON [dbo].[tbl_Producto_Movimiento] 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Producto_Movimiento]') AND name = N'IX_tbl_Producto_Movimiento_2')
CREATE NONCLUSTERED INDEX [IX_tbl_Producto_Movimiento_2] ON [dbo].[tbl_Producto_Movimiento] 
(
	[numdocumento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LiqProd_Enc]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LiqProd_Enc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_LiqProd_Enc](
	[registro] [numeric](14, 0) NOT NULL,
	[fechafactura] [varchar](12) NOT NULL,
	[numfecha] [numeric](8, 0) NOT NULL,
	[cantdias] [int] NOT NULL,
	[numpedido] [varchar](12) NOT NULL,
	[provregistro] [tinyint] NOT NULL,
	[tipopedido] [tinyint] NOT NULL,
	[numfactura] [varchar](12) NOT NULL,
	[monto] [numeric](12, 2) NOT NULL,
	[dai] [numeric](12, 2) NOT NULL,
	[iva] [numeric](12, 2) NOT NULL,
	[tsim] [numeric](12, 2) NOT NULL,
	[constante] [numeric](12, 2) NOT NULL,
	[costointrod] [numeric](12, 2) NOT NULL,
 CONSTRAINT [PK_tbl_LiqProd_Enc] PRIMARY KEY CLUSTERED 
(
	[numpedido] ASC,
	[provregistro] ASC,
	[numfactura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LiqProd_Enc]') AND name = N'IX_tbl_LiqProd_Enc')
CREATE NONCLUSTERED INDEX [IX_tbl_LiqProd_Enc] ON [dbo].[tbl_LiqProd_Enc] 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tmp_InventarioAgencia]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_InventarioAgencia]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_InventarioAgencia](
	[prodregistro] [smallint] NOT NULL,
	[prodclasereg] [tinyint] NOT NULL,
	[prodcodigo] [varchar](12) NOT NULL,
	[proddescrip] [varchar](60) NOT NULL,
	[produnidad] [varchar](12) NOT NULL,
	[prodcantidad] [numeric](12, 2) NOT NULL,
	[proddescrip2] [varchar](60) NOT NULL,
	[produnidad2] [varchar](12) NOT NULL,
	[prodcantidad2] [numeric](12, 2) NOT NULL,
 CONSTRAINT [PK_tmp_InventarioAgencia] PRIMARY KEY CLUSTERED 
(
	[prodregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recibos_Enc]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Enc]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Recibos_Enc](
	[registro] [bigint] NOT NULL,
	[fecha] [varchar](12) NOT NULL,
	[numfecha] [int] NOT NULL,
	[cliregistro] [smallint] NOT NULL,
	[vendregistro] [tinyint] NOT NULL,
	[numero] [varchar](16) NOT NULL,
	[monto] [numeric](12, 2) NOT NULL,
	[interes] [numeric](12, 2) NOT NULL,
	[retencion] [numeric](12, 2) NOT NULL,
	[formapago] [tinyint] NOT NULL,
	[numchqtarj] [varchar](30) NULL,
	[nombrebanco] [varchar](50) NULL,
	[reciboprov] [varchar](12) NOT NULL,
	[descripcion] [text] NOT NULL,
	[usrregistro] [tinyint] NOT NULL,
	[tiporecibo] [tinyint] NOT NULL,
	[fecha_ing] [smalldatetime] NOT NULL,
	[saldofavor] [numeric](18, 0) NOT NULL CONSTRAINT [DF_tbl_Recibos_Enc_saldofavor]  DEFAULT (0),
	[estado] [tinyint] NULL,
	[descr01] [text] NULL,
	[descr02] [text] NULL,
	[descr03] [text] NULL,
 CONSTRAINT [PK_tbl_Recibos_Enc] PRIMARY KEY CLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Enc]') AND name = N'IX_tbl_Recibos_Enc')
CREATE NONCLUSTERED INDEX [IX_tbl_Recibos_Enc] ON [dbo].[tbl_Recibos_Enc] 
(
	[cliregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Enc]') AND name = N'IX_tbl_Recibos_Enc_1')
CREATE NONCLUSTERED INDEX [IX_tbl_Recibos_Enc_1] ON [dbo].[tbl_Recibos_Enc] 
(
	[cliregistro] ASC,
	[numero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tmp_EstadoInvent]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmp_EstadoInvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tmp_EstadoInvent](
	[prodcod01] [varchar](12) NOT NULL,
	[proddesc01] [varchar](65) NOT NULL,
	[produnid01] [varchar](50) NOT NULL,
	[prodcant01] [numeric](12, 2) NOT NULL,
	[prodesc02] [varchar](65) NOT NULL,
	[produnid02] [varchar](50) NOT NULL,
	[prodcant02] [numeric](12, 2) NOT NULL,
	[agencia] [tinyint] NOT NULL,
	[numfecha] [numeric](8, 0) NOT NULL,
 CONSTRAINT [PK_tmp_EstadoInvent] PRIMARY KEY CLUSTERED 
(
	[prodcod01] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Empresa]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empresa]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Empresa](
	[nombre] [varchar](60) NOT NULL,
	[telefono] [varchar](8) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_CambiarNumFact]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CambiarNumFact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_CambiarNumFact](
	[registro] [numeric](6, 0) NOT NULL,
	[agenregistro] [tinyint] NOT NULL,
	[tipofactura] [tinyint] NOT NULL,
	[numero] [varchar](12) NOT NULL,
	[numfactura] [numeric](7, 0) NOT NULL,
	[userregistro] [tinyint] NOT NULL,
	[fechaing] [varchar](12) NOT NULL,
	[numfechaing] [numeric](8, 0) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_tbl_CambiarNumFact] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CambiarNumFact]') AND name = N'IX_tbl_CambiarNumFact')
CREATE CLUSTERED INDEX [IX_tbl_CambiarNumFact] ON [dbo].[tbl_CambiarNumFact] 
(
	[agenregistro] ASC,
	[numfactura] ASC,
	[tipofactura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CambiarPrecios]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CambiarPrecios]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_CambiarPrecios](
	[registro] [numeric](6, 0) NOT NULL,
	[fecha] [varchar](12) NOT NULL,
	[agenregistro] [tinyint] NOT NULL,
	[userregistro] [tinyint] NOT NULL,
	[prodregistro] [numeric](4, 0) NOT NULL,
	[tipofactura] [tinyint] NOT NULL,
	[numfactura] [varchar](12) NOT NULL,
	[numprecio] [tinyint] NOT NULL,
	[precio_orig] [numeric](8, 2) NOT NULL,
	[precio_mod] [numeric](8, 2) NOT NULL,
	[numfechaing] [numeric](8, 0) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_CambiarPrecios]') AND name = N'IX_tbl_CambiarPrecios_1')
CREATE CLUSTERED INDEX [IX_tbl_CambiarPrecios_1] ON [dbo].[tbl_CambiarPrecios] 
(
	[agenregistro] ASC,
	[numfechaing] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Razones_Anular]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Razones_Anular]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Razones_Anular](
	[registro] [tinyint] NOT NULL,
	[modulo] [tinyint] NOT NULL,
	[descripcion] [varchar](60) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Razones_Anular] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Razones_Anular] UNIQUE CLUSTERED 
(
	[modulo] ASC,
	[descripcion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_SubClases]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_SubClases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_SubClases](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](45) NOT NULL,
	[registro2] [tinyint] NOT NULL,
	[facturar] [tinyint] NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_SubClases] PRIMARY KEY CLUSTERED 
(
	[registro2] ASC,
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[prm_SubClases]') AND name = N'IX_prm_SubClases')
CREATE UNIQUE NONCLUSTERED INDEX [IX_prm_SubClases] ON [dbo].[prm_SubClases] 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Facturas_Corregidas]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Corregidas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Facturas_Corregidas](
	[agenregistro] [tinyint] NOT NULL,
	[userregistro] [tinyint] NOT NULL,
	[numfechaing] [numeric](8, 0) NOT NULL,
	[fechaingreso] [varchar](12) NOT NULL,
	[tipofactura] [tinyint] NOT NULL,
	[numerofactura] [varchar](12) NOT NULL,
	[prodregistro_old] [smallint] NOT NULL,
	[prodregistro_new] [smallint] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Facturas_Corregidas]') AND name = N'IX_tbl_Facturas_Corregidas')
CREATE CLUSTERED INDEX [IX_tbl_Facturas_Corregidas] ON [dbo].[tbl_Facturas_Corregidas] 
(
	[agenregistro] ASC,
	[numfechaing] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Areas]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Areas]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Areas](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Areas] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Areas] UNIQUE CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_LiqProd_Det]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LiqProd_Det]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_LiqProd_Det](
	[registro] [numeric](10, 0) NOT NULL,
	[prodregistro] [smallint] NOT NULL,
	[prodcantidad] [numeric](10, 2) NOT NULL,
	[montofob] [numeric](12, 2) NOT NULL,
	[estado] [tinyint] NOT NULL,
	[registro2] [numeric](14, 0) NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LiqProd_Det]') AND name = N'IX_tbl_LiqProd_Det')
CREATE CLUSTERED INDEX [IX_tbl_LiqProd_Det] ON [dbo].[tbl_LiqProd_Det] 
(
	[registro2] ASC,
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Chequeras]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Chequeras]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Chequeras](
	[registro] [smallint] NOT NULL,
	[serie] [tinyint] NOT NULL,
	[ckinicial] [int] NOT NULL,
	[ckfinal] [int] NOT NULL,
	[emitidos] [int] NOT NULL,
	[ctabancregistro] [tinyint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Chequeras] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Chequeras] UNIQUE CLUSTERED 
(
	[serie] ASC,
	[ckinicial] ASC,
	[ckfinal] ASC,
	[ctabancregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[tbl_Presupuestos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Presupuestos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Presupuestos](
	[years] [int] NOT NULL,
	[vendregistro] [tinyint] NOT NULL,
	[prodregistro] [smallint] NOT NULL,
	[prodprecio] [numeric](8, 2) NOT NULL,
	[enero] [numeric](12, 2) NOT NULL,
	[febrero] [numeric](12, 2) NOT NULL,
	[marzo] [numeric](12, 2) NOT NULL,
	[abril] [numeric](12, 2) NOT NULL,
	[mayo] [numeric](12, 2) NOT NULL,
	[junio] [numeric](12, 2) NOT NULL,
	[julio] [numeric](12, 2) NOT NULL,
	[agosto] [numeric](12, 2) NOT NULL,
	[septiembre] [numeric](12, 2) NOT NULL,
	[octubre] [numeric](12, 2) NOT NULL,
	[noviembre] [numeric](12, 2) NOT NULL,
	[diciembre] [numeric](12, 2) NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Presupuestos]') AND name = N'IX_tbl_Presupuestos')
CREATE CLUSTERED INDEX [IX_tbl_Presupuestos] ON [dbo].[tbl_Presupuestos] 
(
	[years] ASC,
	[vendregistro] ASC,
	[prodregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_CtasBancarias]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_CtasBancarias]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_CtasBancarias](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](20) NOT NULL,
	[descripcion] [varchar](40) NOT NULL,
	[bancoregistro] [tinyint] NOT NULL,
	[monregistro] [tinyint] NOT NULL,
	[numfecaper] [int] NOT NULL,
	[blninicial] [numeric](18, 2) NOT NULL,
	[blnactual] [numeric](18, 2) NOT NULL,
	[ctacontregistro] [smallint] NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_CtasBancarias] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_CtasBancarias] UNIQUE CLUSTERED 
(
	[codigo] ASC,
	[bancoregistro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Bancos]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Bancos]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Bancos](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[descripcion] [varchar](40) NOT NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Bancos] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Bancos] UNIQUE CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Recibos_Det]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Det]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tbl_Recibos_Det](
	[registro] [bigint] NOT NULL,
	[numero] [varchar](20) NOT NULL,
	[descripcion] [text] NOT NULL,
	[monto] [numeric](12, 2) NOT NULL,
	[interes] [numeric](12, 2) NOT NULL,
	[mantvlr] [numeric](12, 2) NOT NULL,
	[pendiente] [numeric](12, 2) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Det]') AND name = N'IX_tbl_Recibos_Det')
CREATE CLUSTERED INDEX [IX_tbl_Recibos_Det] ON [dbo].[tbl_Recibos_Det] 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Recibos_Det]') AND name = N'IX_tbl_Recibos_Det_1')
CREATE NONCLUSTERED INDEX [IX_tbl_Recibos_Det_1] ON [dbo].[tbl_Recibos_Det] 
(
	[numero] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prm_Empleados]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Empleados]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Empleados](
	[registro] [tinyint] NOT NULL,
	[codigo] [varchar](12) NOT NULL,
	[nombres] [varchar](30) NOT NULL,
	[priapellido] [varchar](30) NULL,
	[segapellido] [varchar](30) NOT NULL,
	[soltapellido] [varchar](30) NULL,
	[direccion] [varchar](80) NULL,
	[telefono] [varchar](20) NULL,
	[genregistro] [tinyint] NOT NULL,
	[civilregistro] [tinyint] NOT NULL,
	[dependientes] [tinyint] NOT NULL,
	[fechanac] [varchar](12) NULL,
	[numfechanac] [int] NULL,
	[dtefechanac] [smalldatetime] NULL,
	[fechaing] [varchar](12) NULL,
	[numfechaing] [int] NULL,
	[dtefechaing] [smalldatetime] NULL,
	[identregistro] [tinyint] NULL,
	[numidentif] [varchar](20) NULL,
	[numregidentif] [varchar](20) NULL,
	[numfechaemi] [int] NULL,
	[numfechaven] [int] NULL,
	[numinss] [varchar](20) NULL,
	[arearegistro] [tinyint] NULL,
	[jefe] [varchar](60) NULL,
	[pstoregistro] [tinyint] NULL,
	[estado] [tinyint] NOT NULL,
 CONSTRAINT [PK_prm_Empleados] PRIMARY KEY NONCLUSTERED 
(
	[registro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_prm_Empleados] UNIQUE CLUSTERED 
(
	[codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prm_Porcentajes2]    Script Date: 05/10/2009 21:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prm_Porcentajes2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[prm_Porcentajes2](
	[porc_reten] [numeric](4, 2) NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPuestos]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPuestos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngPuestos] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @intEstado as tinyint, @intVerificar as tinyint
As
declare	@lngExiste varchar(12)
begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Puestos where codigo = @strCodigo
		end
	else
		begin
			set @lngExiste = ''''
			select @lngExiste = codigo from prm_Puestos where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					set @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Puestos
					insert into prm_Puestos values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_Puestos set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUsuariosAgencias]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUsuariosAgencias]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngUsuariosAgencias] @strCodigo as varchar(12), @intPermiso as tinyint

As

declare	@lngRegistro as smallint

begin
	set nocount on
	set @lngRegistro = 0
	select @lngRegistro = registro from prm_usuarios where codusuario = @strCodigo
	if @intPermiso = 254
		delete from prm_usuariosagencias where usrregistro = @lngRegistro
	else
		insert into prm_usuariosagencias values (@lngRegistro, @intPermiso)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PrcFacturasImp]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PrcFacturasImp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_PrcFacturasImp] @intAccion as tinyint
As
declare	@lngregistro		bigint,
	@lngcliregistro		smallint,
	@lngprodregistro	smallint,
	@lngvendregistro	tinyint,
	@lngdeptregistro	tinyint,
	@lngnegregistro		tinyint,
	@lngusrregistro		tinyint,
	@intagenregistro	tinyint,
	@lngfactregistro		bigint,
	@strnumero		varchar(12),
	@lngnumfechaing	int,
	@strnumfechaing	varchar(12),
	@strvendcodigo		varchar(12),
	@strclicodigo		varchar(12),
	@strclinombre		varchar(60),
	@dblsubtotal		numeric(12,2),
	@dblimpuesto		numeric(12,2),
	@dblretencion		numeric(12,2),
	@dbltotal		numeric(12,2),
	@strcodusuario		varchar(12),
	@intagencodigo		tinyint,
	@intstatus		tinyint,
	@intimpreso		tinyint,
	@tipofactura		tinyint,
	@diascredito		smallint,
	@numfechaanul		int,
	@intregistrodet		int,
	@strerrores		varchar(200),
	@intencontrarprod	smallint,
	@intencontrarcliente	smallint,
	@intencontrarvend	tinyint,
	@intencontrarusuar	tinyint,
	@intencontrarusrvend	smallint,
	@inthayerrores		tinyint,
	@inttipofactura		tinyint,
	@dbltipocambio		numeric(8,4),
	@dtefechaingreso	smalldatetime,
	@dtefechavencim	smalldatetime,
	@lngnumtiempo		numeric(6),
	@lngnumfechavenc	int,
	@strnumfechavenc	varchar(12),
	@strprodcodigo		varchar(12),
	@dblcantidad		numeric(12,2),
	@dblprecio		numeric(12,2),
	@dblvalor		numeric(12,2),
	@dbliva			numeric(12,2)
begin
	set nocount on
	if @intAccion = 0
		begin
			set @inthayerrores = 0
			set @inthayerrores = 254
			declare tmpB cursor for
				select distinct registro, retencion + total from tmp_facturasimp
			open tmpB
			fetch next from tmpB into @lngfactregistro, @dblvalor
			while @@fetch_status = 0
				begin
					set @strerrores = ''''
					set @dblprecio = 0
					select @dblprecio = sum(valor + iva) from tmp_facturasimp where registro = @lngfactregistro
					set @dblvalor = convert(numeric(12, 2),@dblvalor)
					set @dblprecio = convert(numeric(12, 2),@dblprecio)
					if @dblvalor <> @dblprecio
						begin
							set @inthayerrores = 1
							set @strerrores = ''total de productos diferente al total factura  '' + convert(varchar(12), @dblvalor) + '' '' + convert(varchar(12), @dblprecio)
							update tmp_facturasimp set registroerror = @strerrores where registro = @lngfactregistro
						end
					fetch next from tmpB into @lngfactregistro, @dblvalor
				end
			close tmpB
			deallocate tmpB
			declare tmpC cursor for
				select distinct registro, clicodigo, vendcodigo, codusuario, prodcodigo, registrodet, tipofactura, impreso from tmp_facturasimp
			open tmpC
			fetch next from tmpC into @lngfactregistro, @strclicodigo, @strvendcodigo, @strcodusuario, @strprodcodigo, @intregistrodet, @inttipofactura, @intimpreso
			while @@fetch_status = 0
				begin
					set @strerrores = ''''
					set @intencontrarprod = 0
					set @intencontrarcliente = 0
					set @intencontrarvend = 0
					set @intencontrarusuar = 0
					set @intencontrarusrvend = 1
					select @intencontrarprod = isnull(registro, 0) from prm_productos where codigo = @strprodcodigo
					select @intencontrarcliente = isnull(registro, 0) from prm_clientes where codigo = @strclicodigo
					select @intencontrarvend = isnull(registro, 0) from prm_vendedores where codigo = @strvendcodigo
					select @intencontrarusuar = isnull(registro, 0) from prm_usuarios where codusuario = @strcodusuario
					if @inttipofactura = 1
						select @intencontrarusrvend = isnull(registro, 0) from prm_clientes where registro = @intencontrarcliente and vendregistro = @intencontrarvend
					if @intencontrarcliente = 0
						set @strerrores = @strerrores + ''cliente no existe en el catálogo  ''
					if @intencontrarprod = 0
						set @strerrores = @strerrores + ''producto no existe en el catálogo  ''
					if @intencontrarvend = 0
						set @strerrores = @strerrores + ''vendedor no existe en el catálogo  ''
					if @intencontrarusuar = 0
						set @strerrores = @strerrores + ''usuario no existe en el catálogo  ''
					if @intencontrarusrvend = 0
						set @strerrores = @strerrores + ''vendedor no es el asignado al cliente  ''
					if @inttipofactura = 0
						begin
							if @intencontrarcliente = 0 or @intencontrarprod = 0 or @intencontrarvend = 0 or @intencontrarusuar = 0
								begin
									set @inthayerrores = 1
									update tmp_facturasimp set registroerror = @strerrores where registro = @lngfactregistro and registrodet = @intregistrodet
								end
							else if @inthayerrores = 0
								begin
									update tmp_facturasimp set registroerror = ''Sin Error'' where registro = @lngfactregistro and registrodet = @intregistrodet
								end
						end
					else if @inttipofactura = 1
						begin
							if @intencontrarcliente = 0 or @intencontrarprod = 0 or @intencontrarvend = 0 or @intencontrarusuar = 0 or @intencontrarusrvend = 0
								begin
									set @inthayerrores = 1
									update tmp_facturasimp set registroerror = @strerrores where registro = @lngfactregistro and registrodet = @intregistrodet
								end
							else if @inthayerrores = 0
								begin
									update tmp_facturasimp set registroerror = ''Sin Error'' where registro = @lngfactregistro and registrodet = @intregistrodet
								end
						end
					fetch next from tmpC into @lngfactregistro, @strclicodigo, @strvendcodigo, @strcodusuario, @strprodcodigo, @intregistrodet, @inttipofactura, @intimpreso
				end
			close tmpC
			deallocate tmpC
			select @inthayerrores
		end
	else if @intAccion = 1
		begin
			create table #tmp1
			(
				subtotal		numeric(12,2),
				impuesto	numeric(12,2),
				retencion	numeric(12,2),
				total		numeric(12,2),
				cantidad	int
			)
			select distinct numero, subtotal, impuesto, retencion, total into #tmp11 from tmp_facturasimp where tipofactura = 0 and status = 0
			set @intregistrodet = 0
			select @intregistrodet = count(*) from #tmp11
			insert into #tmp1 select sum(subtotal), sum(impuesto), sum(retencion), sum(total), @intregistrodet from #tmp11
			select * from #tmp1
		end
	else if @intAccion = 2
		begin
			create table #tmp2
			(
				subtotal		numeric(12,2),
				impuesto	numeric(12,2),
				retencion	numeric(12,2),
				total		numeric(12,2),
				cantidad	int
			)
			select distinct numero, subtotal, impuesto, retencion, total into #tmp12 from tmp_facturasimp where tipofactura = 1 and status = 0
			set @intregistrodet = 0
			select @intregistrodet = count(*) from #tmp12
			insert into #tmp2 select sum(subtotal), sum(impuesto), sum(retencion), sum(total), @intregistrodet from #tmp12
			select * from #tmp2
		end
	else if @intAccion = 3
		begin
			set @lngnumfechaing = 0
			set rowcount 1
			select @lngnumfechaing = numfechaing from tmp_facturasimp
			set rowcount 0
			set @dbltipocambio = 0
			select @dbltipocambio = tipo_cambio from prm_tipocambio where numfecha = @lngnumfechaing
			select @lngregistro = 0
			select @lngregistro = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
			declare tmpC cursor for
				select distinct registro, numero, numfechaing, fechaingreso, vendcodigo, clicodigo, clinombre, subtotal, impuesto, retencion, total, codusuario, agencodigo, status, 
					impreso, tipofactura, diascredito, numfechaanul from tmp_facturasimp
			open tmpC
			fetch next from tmpC into @lngfactregistro, @strnumero, @lngnumfechaing, @strnumfechaing, @strvendcodigo, @strclicodigo, @strclinombre, @dblsubtotal, @dblimpuesto, 
				@dblretencion, 	@dbltotal, @strcodusuario, @intagencodigo, @intstatus, @intimpreso, @tipofactura, @diascredito, @numfechaanul
			while @@fetch_status = 0
				begin
					set @lngregistro = @lngregistro + 1
					set @lngvendregistro = 0
					set @lngcliregistro = 0
					set @lngusrregistro = 0
					set @intagenregistro = 0
					set @lngdeptregistro = 0
					set @lngnegregistro = 0
					select @lngvendregistro = registro from prm_vendedores where codigo = @strvendcodigo
					select @lngusrregistro = registro from prm_usuarios where codusuario = @strcodusuario
					select @intagenregistro = registro from prm_agencias where codigo = @intagencodigo
					if @tipofactura = 0
						begin
							select @lngcliregistro = registro from prm_clientes where codigo = @strclicodigo
							set @lngnegregistro = 2
							set @lngdeptregistro = 5
						end
					else
						begin
							select @lngcliregistro = registro, @strclinombre = nombre, @lngnegregistro = negregistro, @lngdeptregistro = deptregistro from prm_clientes 
								where codigo = @strclicodigo
						end
					insert into tbl_facturas_enc values (@lngregistro, @strnumero, @lngnumfechaing, @strnumfechaing, @lngvendregistro, @lngcliregistro, @strclinombre, 
						@dblsubtotal, @dblimpuesto, @dblretencion, @dbltotal, @lngusrregistro, @intagenregistro, @intstatus, 0, @lngdeptregistro, @lngnegregistro, 
						@tipofactura, @diascredito, @dbltipocambio, @numfechaanul)
					update prm_clientes set saldo = saldo + @dbltotal where registro = @lngcliregistro
					if @intstatus = 0 and @tipofactura = 1
						begin
							set @dtefechaingreso = ''''
							set @dtefechavencim = ''''
							set @lngnumfechavenc = 0
							set @strnumfechavenc = ''''
							set @dtefechaingreso = substring(convert(varchar(8), @lngnumfechaing), 5, 2) + ''/'' + 
								substring(convert(varchar(8), @lngnumfechaing), 7, 2) + ''/'' +substring(convert(varchar(8), @lngnumfechaing), 1, 4)
							set @dtefechavencim = substring(convert(varchar(8), @lngnumfechaing), 5, 2) + ''/'' + 
								substring(convert(varchar(8), @lngnumfechaing), 7, 2) + ''/'' +substring(convert(varchar(8), @lngnumfechaing), 1, 4)
							set @dtefechavencim = dateadd(day, @diascredito, @dtefechaingreso)
							set @strnumfechavenc = substring(replace(convert(varchar(12), @dtefechavencim, 113), '' '', ''-''), 1, 11)
							set @lngnumfechavenc = convert(varchar(8), @dtefechavencim, 112)
							set @intregistrodet = 0
							select @intregistrodet = max(registro) + 1 from tbl_cliente_oblig
							insert into tbl_cliente_oblig values (@intregistrodet, @lngcliregistro, @lngvendregistro, @strnumero, @strnumfechaing, 
								@strnumfechavenc, @dbltotal, 0, @dbltotal, 0, @lngnumfechaing, @lngnumfechavenc, @dtefechaingreso, @dtefechavencim)
						end
					declare tmpD cursor for
						select prodcodigo, cantidad, precio, valor, iva from tmp_facturasimp where registro = @lngfactregistro order by registrodet
					open tmpD
					fetch next from tmpD into @strprodcodigo, @dblcantidad, @dblprecio, @dblvalor, @dbliva
					while @@fetch_status = 0
						begin
							set @lngprodregistro = 0
							select @lngprodregistro = registro from prm_productos where codigo = @strprodcodigo
							insert into tbl_facturas_det values (@lngregistro, @lngprodregistro, @dblcantidad, @dblprecio, @dblvalor, @dbliva)
							if @intstatus = 0
								begin
									set @lngnumtiempo = 0
									set @lngnumtiempo = convert(numeric(6), substring(replace(convert(varchar(20), getdate(), 114), '':'',''''), 1, 6))
									if @intagencodigo = 1
										set @lngnumfechaing = convert(numeric(8), convert(varchar(8), getdate(), 112))
									exec sp_IngProductoMovim 0, @lngnumfechaing, @lngnumtiempo, @intagencodigo, @lngprodregistro, @strnumfechaing, 
										@strnumero, ''SF'', ''COR'', @dbltotal, @dblcantidad, 0, 0, '''', '''', @lngusrregistro, 0, 0, 0, 1
									if @intagencodigo = 1
										begin
											exec sp_IngProductoMovim 0, @lngnumfechaing, @lngnumtiempo, 3, @lngprodregistro, 
												@strnumfechaing, @strnumero, ''SF'', ''COR'', @dbltotal, @dblcantidad, 0, 0, '''', '''', @lngusrregistro, 0, 0, 0, 1
										end
								end
							fetch next from tmpD into @strprodcodigo, @dblcantidad, @dblprecio, @dblvalor, @dbliva
						end
					close tmpD
					deallocate tmpD
					fetch next from tmpC into @lngfactregistro, @strnumero, @lngnumfechaing, @strnumfechaing, @strvendcodigo, @strclicodigo, @strclinombre, 
						@dblsubtotal, @dblimpuesto, @dblretencion, @dbltotal, @strcodusuario, @intagencodigo, @intstatus, @intimpreso, @tipofactura, @diascredito, 
						@numfechaanul
				end
			close tmpC
			deallocate tmpC
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngClientes]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngClientes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngClientes] @lngRegistro as smallint, @strCodigo as varchar(12), @intTipoCliente as tinyint, @intTipoID as tinyint, @strNumID as varchar(20), 
			@strNombre as varchar(50), @strFechaIng as varchar(12), @lngNumFechaIng as int, @strDireccion as varchar(80), @intNegRegistro as tinyint, @intVendRegistro as tinyint, 
			@intDeptRegistro as tinyint, @intMuniRegistro as tinyint, @strTelefono as varchar(30), @strFax as varchar(30), @strCorreo as varchar(30), @strNegocio as varchar(50), 
			@strNegDirec as varchar(80), @strContacto01 as varchar(50), @strContacto02 as varchar(50), @dblLimite as numeric(12,2), @dblSaldo as numeric(12,2), 
			@intDiasCred as smallint, @strCtaContable as varchar(16), @intEstado as tinyint, @dblSaldoFavor as numeric(12,2), @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Clientes where codigo = @strCodigo
		end
	else
		begin
			set @lngRegistro = 0
			select @lngRegistro = isnull(registro, 0) from prm_Clientes where codigo = @strCodigo
			if @lngRegistro = 0
				begin
					set @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Clientes
					insert into prm_Clientes values (@lngRegistro, @strCodigo, @intTipoCliente, @intTipoID, @strNumID, @strNombre, @strFechaIng, @lngNumFechaIng, 
						@strDireccion, @intNegRegistro, @intVendRegistro, @intDeptRegistro, @intMuniRegistro, @strTelefono, @strFax, @strCorreo, @strNegocio, 
						@strNegDirec, @strContacto01, @strContacto02, @dblLimite, @dblSaldo, @intDiasCred, @strCtaContable, @intEstado, @dblSaldoFavor)
				end
			else
				begin
					update prm_Clientes set tipocli = @intTipoCliente, 
						tipo_id = @intTipoID, 
						num_id = @strNumID, nombre = @strNombre, 
						fechaingreso = @strFechaIng, 
						numfechaing = @lngNumFechaIng, 
						direccion = @strDireccion, 
						negregistro = @intNegRegistro, vendregistro = @intVendRegistro, deptregistro = @intDeptRegistro, munregistro = @intMuniRegistro, 
						telefono = @strTelefono, fax = @strFax, correo = @strCorreo, 
						negocio = @strNegocio, 	negdirec = @strNegDirec, contacto01 = @strContacto01, contacto02 = @strContacto02, limite = @dblLimite, 
						saldo = @dblSaldo, dias_credito = @intDiasCred, cta_contable = @strCtaContable, estado = @intEstado, saldofavor = @dblSaldoFavor
						where registro = @lngRegistro
					if @intVerificar = 2
						begin
							update tbl_cliente_oblig set vendregistro = @intVendRegistro where cliregistro = @lngRegistro and estado = 0
						end
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosEnc]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosEnc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngRecibosEnc] @lngRegistro as tinyint, @strFechaFactura as varchar(12), @lngNumFecha as int, @lngCliente as smallint, @lngVendedor as tinyint, 
				@strNumero as varchar(12), @dblMonto as numeric(12,2), @dblInteres as numeric(12,2), @dblRetencion as numeric(12,2), @intFormaPago as tinyint, 
				@strNumChqTarj as varchar(20), @strBanco as varchar(25), @strRecProv as varchar(12), @strDescrip as text, @lngUsuario as tinyint, 
				@intTipoRecibo as tinyint, @FechaIng as smalldatetime, @dblSaldoFavor as numeric(12,2), @strDescrip01 as text, @strDescrip02 as text, @strDescrip03 as text

as

Declare	@lngRegistroTbl as bigint,
	@strFechaVen as varchar(12),
	@dteFechaIng as smalldatetime,
	@dteFechaVen as smalldatetime,
	@lngRegistroSlds as numeric(5)

begin
	set nocount on
	set @lngRegistroTbl = 0
	select @lngRegistroTbl = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
	insert into tbl_recibos_enc values (@lngRegistroTbl, @strFechaFactura, @lngNumFecha, @lngCliente, @lngVendedor, @strNumero, @dblMonto, @dblInteres, @dblRetencion, 
		@intFormaPago, @strNumChqTarj, @strbanco, @strRecProv, @strDescrip, @lngUsuario, @intTipoRecibo, @FechaIng, @dblSaldoFavor, 0, @strDescrip01, @strDescrip02, @strDescrip03)
	if @intTipoRecibo = 1 and @dblSaldoFavor <> 0				-- Recibos de Caja
		begin
			update prm_clientes set saldofavor = (saldofavor + @dblSaldoFavor) where registro = @lngCliente
		end
	else if @intTipoRecibo = 3						-- Recibos NTD
		begin
			set @strFechaVen = ''''
			set @dteFechaIng = ''''
			set @dteFechaVen = ''''
			set @dteFechaIng = substring(convert(varchar(8), @lngNumFecha), 5, 2) + ''/'' + 
				substring(convert(varchar(8), @lngNumFecha), 7, 2) + ''/'' +substring(convert(varchar(8), @lngNumFecha), 1, 4)
			set @dteFechaVen = substring(convert(varchar(8), @lngNumFecha), 5, 2) + ''/'' + 
				substring(convert(varchar(8), @lngNumFecha), 7, 2) + ''/'' +substring(convert(varchar(8), @lngNumFecha), 1, 4)
			set @dteFechaVen = dateadd(day, 30, @dteFechaIng)
			set @strFechaVen = substring(replace(convert(varchar(12), @dteFechaVen, 113), '' '', ''-''), 1, 11)
			set @lngRegistroSlds = 0
			select @lngRegistroSlds = max(registro) + 1 from tbl_cliente_oblig
			insert into tbl_cliente_oblig values (@lngRegistroSlds, @lngCliente, @lngVendedor, @strNumero, @strFechaFactura, @strFechaVen, 
				@dblMonto, 0, @dblMonto, 0, @lngNumFecha, convert(varchar(8), @dteFechaVen, 112), @dteFechaIng, @dteFechaVen)
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngFacturasEnc]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngFacturasEnc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngFacturasEnc] @lngRegistro as tinyint, @strNumero as varchar(12), @lngNumFecha as numeric(8), @strFechaFactura as varchar(12), 
				@lngVendedor as tinyint, @lngCliente as smallint, @strCliente as varchar(50), @dblSubTotal as numeric(12,2), @dblImpuesto as numeric(12,2), 
				@dblRetencion as numeric(12,2), @dblTotal as numeric(12,2), @lngUsuario as tinyint, @lngAgencia as tinyint, @intImpreso as tinyint, 
				@lngDepart as tinyint, @lngNegocio as tinyint, @intTipoFactura as tinyint, @intCredito as tinyint, @dblCambio as numeric(13,4), @intStatus as tinyint
As

declare	@lngAnulado as numeric(8),
	@lngRegistroTbl as bigint,
	@lngRegistroSlds as numeric(5),
	@strFechaVen as varchar(12),
	@dteFechaIng as smalldatetime,
	@dteFechaVen as smalldatetime

begin
	set nocount on
	update prm_porcentajes set porc_ret = 2
	select @lngAnulado = 0
	if @intStatus = 0				-- Ingresar
		begin
			select @lngRegistroTbl = 0
			select @lngRegistroTbl = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
			insert into tbl_facturas_enc values (@lngRegistroTbl, @strNumero, @lngNumFecha, @strFechaFactura, @lngVendedor, @lngCliente, @strCliente, @dblSubTotal, 
					@dblImpuesto, @dblRetencion, @dblTotal, @lngUsuario, @lngAgencia, @intStatus, @intImpreso, @lngDepart, @lngNegocio, @intTipoFactura, 
					@intCredito, @dblCambio, @lngAnulado)
			if @intTipoFactura = 1
				begin
					if @dblTotal <> 0
						begin
							update prm_clientes set saldo = saldo + @dblTotal where registro = @lngCliente
							set @strFechaVen = ''''
							set @dteFechaIng = ''''
							set @dteFechaVen = ''''
							set @dteFechaIng = substring(convert(varchar(8), @lngNumFecha), 5, 2) + ''/'' + 
								substring(convert(varchar(8), @lngNumFecha), 7, 2) + ''/'' +substring(convert(varchar(8), @lngNumFecha), 1, 4)
							set @dteFechaVen = substring(convert(varchar(8), @lngNumFecha), 5, 2) + ''/'' + 
								substring(convert(varchar(8), @lngNumFecha), 7, 2) + ''/'' +substring(convert(varchar(8), @lngNumFecha), 1, 4)
							set @dteFechaVen = dateadd(day, @intCredito, @dteFechaIng)
							set @strFechaVen = substring(replace(convert(varchar(12), @dteFechaVen, 113), '' '', ''-''), 1, 11)
							set @lngRegistroSlds = 0
							select @lngRegistroSlds = isnull(max(registro), 0) + 1 from tbl_cliente_oblig
							insert into tbl_cliente_oblig values (@lngRegistroSlds, @lngCliente, @lngVendedor, @strNumero, @strFechaFactura, @strFechaVen, 
								@dblTotal, 0, @dblTotal, 0, @lngNumFecha, convert(varchar(8), @dteFechaVen, 112), @dteFechaIng, @dteFechaVen)
						end
				end
		end
	else if @intStatus = 1			-- Anular
		begin
			select @lngAnulado = convert(char(8), getdate(), 112)
			update tbl_facturas_enc set status = 1, numfechaanul = @lngAnulado where agenregistro = @lngAgencia and numfechaing = @lngNumFecha and numero = @strNumero
			update prm_clientes set saldo = saldo - @dblTotal where registro = @lngCliente
			if @intTipoFactura = 1
				begin
					update tbl_cliente_oblig set estado = 2 where cliregistro = @lngCliente and numero = @strNumero
				end
		end
	else if @intStatus = 2			-- Factura Pendiente
		begin
			update tbl_facturas_enc set impreso = 0 where agenregistro = @lngAgencia and numfechaing = @lngNumFecha and numero = @strNumero
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptComisiones]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptComisiones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_RptComisiones] @VendRegistro as tinyint, @NumFechaIni as int, @NumFechaFin as int
as
declare	@numrecibo	varchar(12),
	@fecrecibo	smalldatetime,
	@numfactura	varchar(12),
	@descripcion	varchar(45),
	@mtorecibo	numeric(12,2),
-------------------------------------------------------
	@diasentre	int,
	@cliregistro	smallint,
	@clicodigo	varchar(12),
	@clinombre	varchar(60),
	@venregistro	tinyint,
	@vendcodigo	varchar(12),
	@vendnombre	varchar(60),
	@agenregistro	tinyint,
	@agencodigo	int,
	@fecfactura	smalldatetime,
	@estadofac	tinyint,
	@facregistro	bigint,
	@impuesto	numeric(12,2),
	@estrato1	numeric(12,2),
	@estrato2	numeric(12,2),
	@estrato3	numeric(12,2),
	@estrato1x	numeric(12,2),
	@estrato2x	numeric(12,2),
	@estrato3x	numeric(12,2),
	@porcentaje1	numeric(5,2),
	@porcentaje2	numeric(5,2),
	@porcentaje3	numeric(5,2),
	@dias_maximo1	int,
	@dias_maximo2	int,
	@dias_maximo3	int,
	@registro	smallint,
	@xxnum_rec	varchar(12), 
	@xxfec_rec	smalldatetime,
	@xxnum_fac	varchar(12),
	@xxfec_fac	smalldatetime,
	@xxdias	int,
	@xxcod_cli	varchar(12),
	@xxmonto	numeric(12,2),
	@xxestrato	numeric(12,2),
	@xxclases	varchar(150),
	@xxsubclases	varchar(150),
	@xxinclprod	varchar(150),
	@xxexclprod	varchar(150),
	@strExecute1	varchar(250),
	@strExecute2	varchar(250),
	@strExecute3	varchar(250),
	@strExecute4	varchar(250)

begin
	set nocount on
	create table #tmp
	(
		num_rec	varchar(12),
		fec_rec		smalldatetime,
		num_fac	varchar(12),
		fec_fac		smalldatetime,
		dias		int,
		cod_cli		varchar(12),
		nom_cli		varchar(60),
		vendedor	varchar(12),
		des_rec		varchar(40),
		agencia		int,
		pendiente	numeric(12,2),
		monto		numeric(12,2),
		impuesto	numeric(12,2),
		estrato1		numeric(12,2),
		estrato2		numeric(12,2),
		estrato3		numeric(12,2),
		porcent1	numeric(5,2),
		porcent2	numeric(5,2),
		porcent3	numeric(5,2),
		vendnombre	varchar(60),
		total		numeric(12,2)
	)
	create table #tmpValor
	(
		valor	numeric(18,2)
	)
	declare tmpC cursor for
		select e.numero, e.fecha_ing, d.numero, d.descripcion, d.monto, e.cliregistro from tbl_recibos_enc e, tbl_recibos_det d 
			where e.registro = d.registro and (e.tiporecibo = 1 or e.tiporecibo = 2) and e.numfecha between @NumFechaIni and @NumFechaFin
			and (substring(d.descripcion, 1, 9) = ''Cancelaci'' or substring(d.descripcion, 1, 5) = ''Abono'')
			and d.monto <> 0 and e.estado = 0 and e.vendregistro = @VendRegistro
			order by e.fecha_ing, e.numero
	open tmpC
	fetch next from tmpC into @numrecibo, @fecrecibo, @numfactura, @descripcion, @mtorecibo, @cliregistro
	while @@fetch_status = 0
		begin
			set @diasentre = 0
			set @clicodigo = ''''
			set @clinombre = ''''
			set @venregistro = 0
			set @vendcodigo = ''''
			set @vendnombre = ''''
			set @agenregistro = 0
			set @agencodigo = 0
			set @fecfactura = ''''
			set @estadofac = 0
			set @impuesto = 0
			set @facregistro = 0
			select @vendnombre = nombre from prm_vendedores where registro = @VendRegistro 
			select @fecfactura = datefechaing, @venregistro = vendregistro, @estadofac = estado from tbl_cliente_oblig where cliregistro = @cliregistro and numero = @numfactura
			set @diasentre = datediff(day, @fecfactura, @fecrecibo)
			select @clicodigo = codigo, @clinombre = nombre from prm_clientes where registro = @cliregistro
			select @vendcodigo = codigo from prm_vendedores where registro = @venregistro
			select @facregistro = registro, @agenregistro = agenregistro, @impuesto = impuesto from tbl_facturas_enc where numfechaing = convert(varchar(12), @fecfactura, 112) and cliregistro = @cliregistro and numero = @numfactura
			select @agencodigo = codigo from prm_agencias where registro = @agenregistro
			set @registro = 0
			-- Primer Estrato (I)
			set @strExecute1 = ''''
			set @strExecute2 = ''''
			set @strExecute3 = ''''
			set @strExecute4 = ''''
			set @xxclases = ''''
			set @xxsubclases = ''''
			set @xxinclprod = ''''
			set @xxexclprod = ''''
			set @estrato1 = 0
			set @registro = 0
			set rowcount 1
			select @registro = isnull(registro, 0), @xxclases = clases, @xxsubclases = subclases, 
					@xxinclprod = inclprod, @xxexclprod = exclprod from prm_Comisiones where 
					vendregistro = @VendRegistro order by registro
			set rowcount 0
			truncate table #tmpValor
			set @strExecute1 = ''insert into #tmpValor select isnull(sum(f.valor), 0) from tbl_facturas_det f, prm_productos p where f.registro = ''
			--  + @facregistro
			set @strExecute2 = ''and f.prodregistro = p.registro ''
			if @xxclases <> ''''
					set @strExecute2 = @strExecute2 + ''and (p.regclase in '' + ltrim(rtrim(@xxclases))
			if @xxclases <> '''' and @xxsubclases = ''''
					set @strExecute2 = @strExecute2 + '')''
			else if @xxclases <> '''' and @xxsubclases <> ''''
					set @strExecute2 = @strExecute2 + '' or p.regsubclase in '' + ltrim(rtrim(@xxsubclases)) + '')''
			else if @xxclases = '''' and @xxsubclases <> ''''
					set @strExecute2 = @strExecute2 + ''and (p.regsubclase in '' + ltrim(rtrim(@xxsubclases)) + '')''
			if @xxinclprod <> ''''
					begin
							if @xxclases <> '''' or @xxsubclases <> ''''
								set @strExecute2 = substring(@strExecute2, 1, len(@strExecute2) - 1) + '' or p.registro in '' + ltrim(rtrim(@xxinclprod)) + '')''
							else
								set @strExecute3 = ''and (p.registro in '' + ltrim(rtrim(@xxinclprod)) + '')''
					end
			if @xxexclprod <> ''''
					begin
							if @xxclases <> '''' or @xxsubclases <> '''' or @xxinclprod <> ''''
								set @strExecute2 = @strExecute2 + '' and (p.registro not in '' + ltrim(rtrim(@xxexclprod)) + '')''
							else
								set @strExecute4 = ''and (p.registro not in '' + ltrim(rtrim(@xxexclprod)) + '')''
					end
			if @xxclases <> '''' or @xxsubclases <> '''' or @xxinclprod <> '''' or @xxexclprod <> ''''
				execute (@strExecute1 + @facregistro + @strExecute2 + @strExecute3 + @strExecute4)
			select @estrato1 = isnull(sum(valor), 0) from #tmpValor
			-- Primer Estrato (F)
			-- Segundo Estrato (I)
			set @strExecute1 = ''''
			set @strExecute2 = ''''
			set @strExecute3 = ''''
			set @strExecute4 = ''''
			set @xxclases = ''''
			set @xxsubclases = ''''
			set @xxinclprod = ''''
			set @xxexclprod = ''''
			set @estrato2 = 0
			set rowcount 1
			select @registro = isnull(registro, 0), @xxclases = clases, @xxsubclases = subclases, 
					@xxinclprod = inclprod, @xxexclprod = exclprod from prm_Comisiones where 
					vendregistro = @VendRegistro and registro > @registro
			set rowcount 0
			truncate table #tmpValor
			set @strExecute1 = ''insert into #tmpValor select isnull(sum(f.valor), 0) from tbl_facturas_det f, prm_productos p where f.registro = ''
			--  + @facregistro
			set @strExecute2 = ''and f.prodregistro = p.registro ''
			if @xxclases <> ''''
					set @strExecute2 = @strExecute2 + ''and (p.regclase in '' + ltrim(rtrim(@xxclases))
			if @xxclases <> '''' and @xxsubclases = ''''
					set @strExecute2 = @strExecute2 + '')''
			else if @xxclases <> '''' and @xxsubclases <> ''''
					set @strExecute2 = @strExecute2 + '' or p.regsubclase in '' + ltrim(rtrim(@xxsubclases)) + '')''
			else if @xxclases = '''' and @xxsubclases <> ''''
					set @strExecute2 = @strExecute2 + ''and (p.regsubclase in '' + ltrim(rtrim(@xxsubclases)) + '')''
			if @xxinclprod <> ''''
					begin
							if @xxclases <> '''' or @xxsubclases <> ''''
								set @strExecute2 = substring(@strExecute2, 1, len(@strExecute2) - 1) + '' or p.registro in '' + ltrim(rtrim(@xxinclprod)) + '')''
							else
								set @strExecute3 = ''and (p.registro in '' + ltrim(rtrim(@xxinclprod)) + '')''
					end
			if @xxexclprod <> ''''
					begin
							if @xxclases <> '''' or @xxsubclases <> '''' or @xxinclprod <> ''''
								set @strExecute2 = @strExecute2 + '' and (p.registro not in '' + ltrim(rtrim(@xxexclprod)) + '')''
							else
								set @strExecute4 = ''and (p.registro not in '' + ltrim(rtrim(@xxexclprod)) + '')''
					end
			if @xxclases <> '''' or @xxsubclases <> '''' or @xxinclprod <> '''' or @xxexclprod <> ''''
				execute (@strExecute1 + @facregistro + @strExecute2 + @strExecute3 + @strExecute4)
			select @estrato2 = isnull(sum(valor), 0) from #tmpValor
			-- Segundo Estrato (F)
			-- Tercer Estrato (I)
			set @strExecute1 = ''''
			set @strExecute2 = ''''
			set @strExecute3 = ''''
			set @strExecute4 = ''''
			set @xxclases = ''''
			set @xxsubclases = ''''
			set @xxinclprod = ''''
			set @xxexclprod = ''''
			set @estrato3 = 0
			set rowcount 1
			select @registro = isnull(registro, 0), @xxclases = clases, @xxsubclases = subclases, 
					@xxinclprod = inclprod, @xxexclprod = exclprod from prm_Comisiones where 
					vendregistro = @VendRegistro and registro > @registro
			set rowcount 0
			truncate table #tmpValor
			set @strExecute1 = ''insert into #tmpValor select isnull(sum(f.valor), 0) from tbl_facturas_det f, prm_productos p where f.registro = ''
			--  + @facregistro
			set @strExecute2 = ''and f.prodregistro = p.registro ''
			if @xxclases <> ''''
					set @strExecute2 = @strExecute2 + ''and (p.regclase in '' + ltrim(rtrim(@xxclases))
			if @xxclases <> '''' and @xxsubclases = ''''
					set @strExecute2 = @strExecute2 + '')''
			else if @xxclases <> '''' and @xxsubclases <> ''''
					set @strExecute2 = @strExecute2 + '' or p.regsubclase in '' + ltrim(rtrim(@xxsubclases)) + '')''
			else if @xxclases = '''' and @xxsubclases <> ''''
					set @strExecute2 = @strExecute2 + ''and (p.regsubclase in '' + ltrim(rtrim(@xxsubclases)) + '')''
			if @xxinclprod <> ''''
					begin
							if @xxclases <> '''' or @xxsubclases <> ''''
								set @strExecute2 = substring(@strExecute2, 1, len(@strExecute2) - 1) + '' or p.registro in '' + ltrim(rtrim(@xxinclprod)) + '')''
							else
								set @strExecute3 = ''and (p.registro in '' + ltrim(rtrim(@xxinclprod)) + '')''
					end
			if @xxexclprod <> ''''
					begin
							if @xxclases <> '''' or @xxsubclases <> '''' or @xxinclprod <> ''''
								set @strExecute2 = @strExecute2 + '' and (p.registro not in '' + ltrim(rtrim(@xxexclprod)) + '')''
							else
								set @strExecute4 = ''and (p.registro not in '' + ltrim(rtrim(@xxexclprod)) + '')''
					end
			if @xxclases <> '''' or @xxsubclases <> '''' or @xxinclprod <> '''' or @xxexclprod <> ''''
				execute (@strExecute1 + @facregistro + @strExecute2 + @strExecute3 + @strExecute4)
			select @estrato3 = isnull(sum(valor), 0) from #tmpValor
			-- Tercer Estrato (F)
			set @estrato1x = 0
			set @estrato2x = 0
			set @estrato3x = 0
			if @estrato1 > @mtorecibo
				set @estrato1x = @mtorecibo
			if @estrato2 > @mtorecibo
				set @estrato2x = @mtorecibo
			if @estrato3 > @mtorecibo
				set @estrato3x = @mtorecibo
			if substring(@descripcion, 1, 9) = ''Cancelaci''
				begin
					insert into #tmp values (@numrecibo, @fecrecibo, @numfactura, @fecfactura, @diasentre, 
						@clicodigo, @clinombre,	@vendcodigo, @descripcion, @agencodigo, 0, @mtorecibo, @impuesto, 
						@estrato1x, @estrato2x, @estrato3x, 0, 0, 0, @vendnombre, 0)
					insert into #tmp select e.numero, e.fecha_ing, d.numero, @fecfactura, datediff(day, @fecfactura, e.fecha_ing),
						@clicodigo, @clinombre, @vendcodigo, d.descripcion, @agencodigo, 0, d.monto, @impuesto,
						''estrato1'' = case when @estrato1x > d.monto then d.monto else @estrato1x end,
						''estrato2'' = case when @estrato2x > d.monto then d.monto else @estrato2x end,
						''estrato3'' = case when @estrato3x > d.monto then d.monto else @estrato3x end,
						0, 0, 0, @vendnombre, 0
						from tbl_recibos_enc e, tbl_recibos_det d 
						where e.registro = d.registro and (e.tiporecibo = 1 or e.tiporecibo = 2) and e.numfecha < @NumFechaIni
						and (substring(d.descripcion, 1, 9) = ''Cancelaci'' or 
							substring(d.descripcion, 1, 5) = ''Abono'') and d.numero = @numfactura
						and e.cliregistro = @cliregistro
					update #tmp set impuesto = 0 where num_fac = @numfactura and des_rec not like ''Cancelaci%''
--/*
					-- Calcular Montos Correctos para Aplicar en Cada Estrato por Facturas Canceladas
					declare tmpXX cursor for
						select num_rec, fec_rec, num_fac, (monto - impuesto) from #tmp where num_fac = @numfactura
							and cod_cli = @clicodigo order by num_rec, fec_rec
						open tmpXX
						fetch next from tmpXX into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxmonto
						while @@fetch_status = 0
							begin
								if @estrato3 > 0
									begin
										if @estrato3 >= @xxmonto
											begin
												set @estrato3 = @estrato3 - @xxmonto
												update #tmp set estrato1 = 0, estrato2 = 0, 
													estrato3 = @xxmonto where num_rec = @xxnum_rec
													and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
													and cod_cli = @clicodigo
											end
										else if @estrato2 > 0
											begin
												set @xxmonto = @xxmonto - @estrato3
												if @estrato2 >= @xxmonto
													begin
														set @estrato2 = @estrato2 - @xxmonto
														update #tmp set estrato1 = 0, estrato2 = @xxmonto, 
															estrato3 = @estrato3 where num_rec = @xxnum_rec
															and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
															and cod_cli = @clicodigo
													end
												else if @estrato1 > 0
													begin
														set @xxmonto = @xxmonto - @estrato3 - @estrato2
														if @estrato1 >= @xxmonto
															begin
																set @estrato1 = @estrato1 - @xxmonto
																update #tmp set estrato1 = @xxmonto, 
																	estrato2 = @estrato2, 
																	estrato3 = @estrato3 where 
																	num_rec = @xxnum_rec
																	and fec_rec = @xxfec_rec and 
																	num_fac = @xxnum_fac
																	and cod_cli = @clicodigo
															end
													end
												set @estrato3 = 0
											end
										else if @estrato1 > 0
											begin
												set @xxmonto = @xxmonto - @estrato3 - @estrato2
												update #tmp set estrato1 = @xxmonto, 
													estrato2 = @estrato2, estrato3 = @estrato3 where 
													num_rec = @xxnum_rec and fec_rec = @xxfec_rec and 
													num_fac = @xxnum_fac and cod_cli = @clicodigo
												set @estrato1 = @estrato1 - @xxmonto
												set @estrato2 = 0
												set @estrato3 = 0
											end
									end
								else if @estrato2 > 0
									begin
										if @estrato2 >= @xxmonto
											begin
												set @estrato2 = @estrato2 - @xxmonto
												update #tmp set estrato1 = 0, estrato2 = @xxmonto, 
													estrato3 = @estrato3 where num_rec = @xxnum_rec
													and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
													and cod_cli = @clicodigo
											end
										else if @estrato1 > 0
											begin
												set @xxmonto = @xxmonto - @estrato2
												if @estrato1 >= @xxmonto
													begin
														set @estrato1 = @estrato1 - @xxmonto
														update #tmp set estrato1 = @xxmonto, 
															estrato2 = @estrato2, 
															estrato3 = @estrato3 where 
															num_rec = @xxnum_rec
															and fec_rec = @xxfec_rec and 
															num_fac = @xxnum_fac
															and cod_cli = @clicodigo
													end
											end
										set @estrato2 = 0
									end
							else if @estrato1 > 0
								begin
									if @estrato1 >= @xxmonto
										begin
											set @estrato1 = @estrato1 - @xxmonto
											update #tmp set estrato1 = @xxmonto, estrato2 = 0, 
												estrato3 = 0 where num_rec = @xxnum_rec
												and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
												and cod_cli = @clicodigo
										end
									else if @estrato1 > 0
										begin
											update #tmp set estrato1 = @estrato1, estrato2 = 0, 
												estrato3 = 0 where num_rec = @xxnum_rec
												and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
												and cod_cli = @clicodigo
										end
								end
								fetch next from tmpXX into @xxnum_rec, @xxfec_rec, @xxnum_fac, 
									@xxmonto
							end
						close tmpXX
						deallocate tmpXX
--*/
				end
			else
				begin
					if @estadofac = 0
						begin
							insert into #tmp values (@numrecibo, @fecrecibo, @numfactura, @fecfactura, 
								@diasentre, @clicodigo, @clinombre, @vendcodigo, @descripcion, 
								@agencodigo, @mtorecibo, @impuesto, 0, 0, 0, 0, 0, 0, 0, @vendnombre, 0)
							update #tmp set impuesto = 0 where num_fac = @numfactura and des_rec not like ''Cancelaci%''
						end
					else if @estadofac = 1
						begin
							insert into #tmp values (@numrecibo, @fecrecibo, @numfactura, @fecfactura, 
								@diasentre, @clicodigo, @clinombre, @vendcodigo, @descripcion, 
								@agencodigo, 0, @mtorecibo, @impuesto, @estrato1x, @estrato2x, @estrato3x,
								0, 0, 0, @vendnombre, 0)
							update #tmp set impuesto = 0 where num_fac = @numfactura and des_rec not like ''Cancelaci%''
--/*
							-- Calcular Montos Correctos para Aplicar en Cada Estrato por Facturas Canceladas
							declare tmpXX cursor for
								select num_rec, fec_rec, num_fac, (monto - impuesto) from #tmp where num_fac = @numfactura
									and cod_cli = @clicodigo order by num_rec, fec_rec
								open tmpXX
								fetch next from tmpXX into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxmonto
								while @@fetch_status = 0
									begin
										if @estrato3 > 0
											begin
												if @estrato3 >= @xxmonto
													begin
														set @estrato3 = @estrato3 - @xxmonto
														update #tmp set estrato1 = 0, estrato2 = 0, 
															estrato3 = @xxmonto where num_rec = @xxnum_rec
															and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
															and cod_cli = @clicodigo
													end
												else if @estrato2 > 0
													begin
														set @xxmonto = @xxmonto - @estrato3
														if @estrato2 >= @xxmonto
															begin
																set @estrato2 = @estrato2 - @xxmonto
																update #tmp set estrato1 = 0, estrato2 = @xxmonto, 
																	estrato3 = @estrato3 where num_rec = @xxnum_rec
																	and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
																	and cod_cli = @clicodigo
															end
														else if @estrato1 > 0
															begin
																set @xxmonto = @xxmonto - @estrato3 - @estrato2
																if @estrato1 >= @xxmonto
																	begin
																		set @estrato1 = @estrato1 - @xxmonto
																		update #tmp set estrato1 = @xxmonto, 
																			estrato2 = @estrato2, 
																			estrato3 = @estrato3 where 
																			num_rec = @xxnum_rec
																			and fec_rec = @xxfec_rec and 
																			num_fac = @xxnum_fac
																			and cod_cli = @clicodigo
																	end
															end
														set @estrato3 = 0
													end
												else if @estrato1 > 0
													begin
														set @xxmonto = @xxmonto - @estrato3 - @estrato2
														update #tmp set estrato1 = @xxmonto, 
															estrato2 = @estrato2, estrato3 = @estrato3 where 
															num_rec = @xxnum_rec and fec_rec = @xxfec_rec and 
															num_fac = @xxnum_fac and cod_cli = @clicodigo
														set @estrato1 = @estrato1 - @xxmonto
														set @estrato2 = 0
														set @estrato3 = 0
													end
											end
										else if @estrato2 > 0
											begin
												if @estrato2 >= @xxmonto
													begin
														set @estrato2 = @estrato2 - @xxmonto
														update #tmp set estrato1 = 0, estrato2 = @xxmonto, 
															estrato3 = @estrato3 where num_rec = @xxnum_rec
															and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
															and cod_cli = @clicodigo
													end
												else if @estrato1 > 0
													begin
														set @xxmonto = @xxmonto - @estrato2
														if @estrato1 >= @xxmonto
															begin
																set @estrato1 = @estrato1 - @xxmonto
																update #tmp set estrato1 = @xxmonto, 
																	estrato2 = @estrato2, 
																	estrato3 = @estrato3 where 
																	num_rec = @xxnum_rec
																	and fec_rec = @xxfec_rec and 
																	num_fac = @xxnum_fac
																	and cod_cli = @clicodigo
															end
													end
												set @estrato2 = 0
											end
									else if @estrato1 > 0
										begin
											if @estrato1 >= @xxmonto
												begin
													set @estrato1 = @estrato1 - @xxmonto
													update #tmp set estrato1 = @xxmonto, estrato2 = 0, 
														estrato3 = 0 where num_rec = @xxnum_rec
														and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
														and cod_cli = @clicodigo
												end
											else if @estrato1 > 0
												begin
													update #tmp set estrato1 = @estrato1, estrato2 = 0, 
														estrato3 = 0 where num_rec = @xxnum_rec
														and fec_rec = @xxfec_rec and num_fac = @xxnum_fac
														and cod_cli = @clicodigo
												end
										end
										fetch next from tmpXX into @xxnum_rec, @xxfec_rec, @xxnum_fac, 
											@xxmonto
									end
								close tmpXX
								deallocate tmpXX
--*/
						end
				end
			fetch next from tmpC into @numrecibo, @fecrecibo, @numfactura, @descripcion, @mtorecibo, @cliregistro
		end
	close tmpC
	deallocate tmpC

	declare tmpC cursor for
		select distinct num_fac, dias, cod_cli, vendedor from #tmp where des_rec like ''%cancelac%''
	open tmpC
	fetch next from tmpC into @numfactura, @diasentre, @clicodigo, @vendcodigo
	while @@fetch_status = 0
		begin
			update #tmp set dias = @diasentre where num_fac = @numfactura and cod_cli = @clicodigo
				and vendedor = @vendcodigo
			fetch next from tmpC into @numfactura, @diasentre, @clicodigo, @vendcodigo
		end
	close tmpC
	deallocate tmpC

	-- Determinar que porcentaje aplicara segun la cantidad de dias de atraso y el estrato PI (I)
	set @porcentaje1 = 0
	set @porcentaje2 = 0
	set @porcentaje3 = 0
	set @registro = 0
	set @dias_maximo1 = 0
	set @dias_maximo2 = 0
	set @dias_maximo3 = 0
	set rowcount 1
	select @registro = registro, @porcentaje1 = por01, @porcentaje2 = por02, @porcentaje3 = por03,
		@dias_maximo1 = dia02, @dias_maximo2 = dia04, @dias_maximo3 = dia06
		from prm_Comisiones where vendregistro = @VendRegistro order by registro
	set rowcount 0

	declare tmpC cursor for
		select distinct num_rec, fec_rec, num_fac, fec_fac, dias, cod_cli, monto, estrato1
			from #tmp
	open tmpC
	fetch next from tmpC into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxfec_fac, @xxdias, @xxcod_cli, 
		@xxmonto, @xxestrato
	while @@fetch_status = 0
		begin
			if @xxdias < @dias_maximo1 and @xxestrato <> 0
				update #tmp set porcent1 = @porcentaje1 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else if @xxdias < @dias_maximo2 and @xxestrato <> 0
				update #tmp set porcent1 = @porcentaje2 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else if @xxestrato <> 0
				update #tmp set porcent1 = @porcentaje3 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else
				update #tmp set porcent1 = 0 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			fetch next from tmpC into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxfec_fac, @xxdias, 
				@xxcod_cli, @xxmonto, @xxestrato
		end
	close tmpC
	deallocate tmpC
	-- Determinar que porcentaje aplicara segun la cantidad de dias de atraso y el estrato PI (F)

	-- Determinar que porcentaje aplicara segun la cantidad de dias de atraso y el estrato PII (I)
	set @porcentaje1 = 0
	set @porcentaje2 = 0
	set @porcentaje3 = 0
	set @dias_maximo1 = 0
	set @dias_maximo2 = 0
	set @dias_maximo3 = 0
	set rowcount 1
	select @registro = registro, @porcentaje1 = por01, @porcentaje2 = por02, @porcentaje3 = por03,
		@dias_maximo1 = dia02, @dias_maximo2 = dia04, @dias_maximo3 = dia06
		from prm_Comisiones where vendregistro = @VendRegistro and registro > @registro
	set rowcount 0

	declare tmpC cursor for
		select distinct num_rec, fec_rec, num_fac, fec_fac, dias, cod_cli, monto, estrato2
			from #tmp
	open tmpC
	fetch next from tmpC into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxfec_fac, @xxdias, @xxcod_cli, 
		@xxmonto, @xxestrato
	while @@fetch_status = 0
		begin
			if @xxdias < @dias_maximo1 and @xxestrato <> 0
				update #tmp set porcent2 = @porcentaje1 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else if @xxdias < @dias_maximo2 and @xxestrato <> 0
				update #tmp set porcent2 = @porcentaje2 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else if @xxestrato <> 0
				update #tmp set porcent2 = @porcentaje3 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else
				update #tmp set porcent2 = 0 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			fetch next from tmpC into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxfec_fac, @xxdias, 
				@xxcod_cli, @xxmonto, @xxestrato
		end
	close tmpC
	deallocate tmpC
	-- Determinar que porcentaje aplicara segun la cantidad de dias de atraso y el estrato PII (F)

	-- Determinar que porcentaje aplicara segun la cantidad de dias de atraso y el estrato PII (I)
	set @porcentaje1 = 0
	set @porcentaje2 = 0
	set @porcentaje3 = 0
	set @dias_maximo1 = 0
	set @dias_maximo2 = 0
	set @dias_maximo3 = 0
	set rowcount 1
	select @registro = registro, @porcentaje1 = por01, @porcentaje2 = por02, @porcentaje3 = por03,
		@dias_maximo1 = dia02, @dias_maximo2 = dia04, @dias_maximo3 = dia06
		from prm_Comisiones where vendregistro = @VendRegistro and registro > @registro
	set rowcount 0

	declare tmpC cursor for
		select distinct num_rec, fec_rec, num_fac, fec_fac, dias, cod_cli, monto, estrato3
			from #tmp
	open tmpC
	fetch next from tmpC into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxfec_fac, @xxdias, @xxcod_cli, 
		@xxmonto, @xxestrato
	while @@fetch_status = 0
		begin
			if @xxdias < @dias_maximo1 and @xxestrato <> 0
				update #tmp set porcent3 = @porcentaje1 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else if @xxdias < @dias_maximo2 and @xxestrato <> 0
				update #tmp set porcent3 = @porcentaje2 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else if @xxestrato <> 0
				update #tmp set porcent3 = @porcentaje3 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			else
				update #tmp set porcent3 = 0 where num_rec = @xxnum_rec and 
					fec_rec = @xxfec_rec and num_fac = @xxnum_fac and fec_fac = @xxfec_fac and
					dias = @xxdias and cod_cli = @xxcod_cli and monto = @xxmonto
			fetch next from tmpC into @xxnum_rec, @xxfec_rec, @xxnum_fac, @xxfec_fac, @xxdias, 
				@xxcod_cli, @xxmonto, @xxestrato
		end
	close tmpC
	deallocate tmpC
	-- Determinar que porcentaje aplicara segun la cantidad de dias de atraso y el estrato PII (F)
	update #tmp set total = (estrato1 * (porcent1 / 100)) + (estrato2 * (porcent2 / 100)) + (estrato3 * (porcent3 / 100))
	select num_rec, convert(varchar(12), fec_rec, 110) fec_rec, num_fac, 
		convert(varchar(12), fec_fac, 110) fec_fac, dias, cod_cli, nom_cli, vendedor, 
		des_rec, agencia, pendiente, monto, impuesto, estrato1, estrato2, estrato3, porcent1, 
		porcent2, porcent3, total
		from #tmp where num_rec not like ''NC-%''
		order by vendedor, cod_cli, num_fac, num_rec, fec_rec
	set nocount off
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosDet]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosDet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngRecibosDet] @lngRegistro as tinyint, @strFactura as varchar(20), @strDescrip as text, @dblAbono as numeric(12,2), @lngCliRegistro as smallint,
				@strNumero as varchar(12), @dblInteres as numeric(12,2), @dblMantVlr as numeric(12,2), @dblPendiente as numeric(12,2), @intTipoRecibo as tinyint
As

Declare @monto as numeric(12,2),
	@lngRegistroTbl as bigint

begin
	set nocount on
	set @lngRegistroTbl = 0
	select @lngRegistroTbl = registro from tbl_Recibos_Enc where cliregistro = @lngCliRegistro and numero = @strNumero
	insert into tbl_Recibos_Det values (@lngRegistroTbl, @strFactura, @strDescrip, @dblAbono, @dblInteres, @dblMantVlr, @dblPendiente)
	if @intTipoRecibo = 1 or @intTipoRecibo = 2
		begin
			update prm_Clientes set saldo = saldo - @dblAbono, saldofavor = saldofavor + @dblPendiente where registro = @lngCliRegistro
		end
	else if @intTipoRecibo = 3
		begin
			update prm_Clientes set saldo = saldo + @dblAbono where registro = @lngCliRegistro
		end
	set @monto = 0
	if @intTipoRecibo = 1 or @intTipoRecibo = 2
		begin
			select @monto = saldo from tbl_cliente_oblig where cliregistro = @lngCliRegistro and numero = @strFactura
			if @monto != 0
				begin
					if @monto - @dblAbono = 0
						begin
							update tbl_cliente_oblig set abono = monto, saldo = 0, estado = 1 where cliregistro = @lngCliRegistro and numero = @strFactura
						end
					else
						begin
							update tbl_cliente_oblig set abono = abono + @dblAbono, saldo = saldo - @dblAbono where cliregistro = @lngCliRegistro and numero = @strFactura
						end
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosAnular]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosAnular]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngRecibosAnular] @lngCliente as smallint, @strNumero as varchar(12), @lngNumFecha as int, @dblMontoRec as numeric(12,2), @dblSaldoFavor as numeric(12,2),
			@intTipoRecibo as tinyint
as

Declare	@lngRegistroTbl as bigint,
	@strNumeroDet as varchar(12)

begin
	set nocount on
	set @lngRegistroTbl = 0
	select @lngRegistroTbl = registro from tbl_recibos_enc where cliregistro = @lngCliente and numero = @strNumero and numfecha = @lngNumFecha
	if @@rowcount != 0
		begin
			update tbl_recibos_enc set estado = 1 where cliregistro = @lngCliente and numero = @strNumero and numfecha = @lngNumFecha
			if @intTipoRecibo = 4 or @intTipoRecibo = 5
				begin
					update prm_clientes set saldo = saldo + @dblMontoRec, saldofavor = saldofavor - @dblSaldoFavor where registro = @lngCliente
					set @strNumero = ''''
					set @dblMontoRec = 0
					declare tmpC cursor for
						select numero, (monto + interes + mantvlr) monto  from tbl_recibos_det where registro = @lngRegistroTbl
					open tmpC
					fetch next from tmpC into @strNumero, @dblMontoRec
					while @@fetch_status = 0
						begin
							update tbl_cliente_oblig set abono = abono - @dblMontoRec, saldo = saldo + @dblMontoRec, estado = 0
								where cliregistro = @lngCliente and numero = @strNumero
							fetch next from tmpC into @strNumero, @dblMontoRec
						end
					close tmpC
					deallocate tmpC
				end
			else if @intTipoRecibo = 6
				begin
					update prm_clientes set saldo = saldo - @dblMontoRec where registro = @lngCliente
					update tbl_cliente_oblig set estado = 2 where cliregistro = @lngCliente and numero = @strNumero
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptCuentasCobrar]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptCuentasCobrar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_RptCuentasCobrar] @intTipoReporte as tinyint, @strClientes as varchar(600), @strVendedores as varchar(100), @strNegocios as varchar(100), 
			@strDepartamentos as varchar(100), @intAccion as tinyint, @intCliente as smallint, @dteFechaIni as smalldatetime, @dteFechaFin as smalldatetime,
			@strRecProv as varchar(12), @strMunicipios as varchar(100)

As

Declare	@strComando01 as varchar(275),
	@strComando02 as varchar(275),
	@strComando03 as varchar(275),
	@strComando04 as varchar(275),
	@strComando04a as varchar(10),
	@strComando05 as varchar(275),
	@strComando05a as varchar(10),
	@strComando06 as varchar(275),
	@strComando06a as varchar(10),
	@strComando07 as varchar(275),
	@strComando07a as varchar(10),
	@strComando08 as varchar(275),
	@strComando08a as varchar(10),
	@strComando09 as varchar(275),
	@cliregistro as smallint,
	@numero	as varchar(12),
	@abn_fecha 	as varchar(12),
	@abn_numero	as varchar(12),
	@abn_monto	as numeric(12,2),
	@enc_abono	as tinyint,
	@saldo		as numeric(12,2),
	@tot_montos	as numeric(12,2),
	@tot_saldos	as numeric(12,2),
	@tot_interes	as numeric(12,2),
	@monto	as numeric(12,2),
	@est_noven	as numeric(12,2),
	@est_01_30	as numeric(12,2),
	@est_31_60	as numeric(12,2),
	@est_61myr	as numeric(12,2),
	@dias		as smallint,
	@interes	as numeric(12,2),
--	Con Interes y Mantenimiento de Valor
	@clireg 		as smallint,
	@clireg2 	as smallint,
	@codigo 	as varchar(12),
	@codvend 	as varchar(12),
	@nombre 	as varchar(100),
	@direcc 	as varchar(100),
	@telefn 	as varchar(30),
	@fax 		as varchar(30),
	@numfac 	as varchar(12),
	@numfac2 	as varchar(12),
	@fecing 	as varchar(12),
	@dteing 	as smalldatetime,
	@fecven 	as varchar(12),
	@dteven 	as smalldatetime,
	@mtofac 	as numeric(12,2),
	@sldfac 	as numeric(12,2),
	@inters 		as numeric(12,2),
	@mntvlr 	as numeric(12,2),
	@cntdia 	as smallint,
	@fecrec 	as smalldatetime,
	@intCiclo 	as int,
	@dteRec01 	as smalldatetime,
	@dteRec02 	as smalldatetime,
	@dteRec 	as smalldatetime,
	@codcli 	as varchar(12),
	@credito 	as numeric(12,2),
	@debito 	as numeric(12,2),
	@tpcmb	as numeric(13,4),
	@sld_01	as numeric(12,2),
	@sld_02	as numeric(12,2),
	@mtofacU 	as numeric(12,2),
	@total_cli	as numeric(12,2),
	@total_facs	as numeric(12,2),
-- Movimiento Historico
	@registro	bigint,
	@cli_regis	smallint,
	@saldo_cli	numeric(12,2),
	@saldo_reg	numeric(12,2),
	@fecha_reg	smalldatetime,
	@fecha_ini	smalldatetime,
	@fecha_fin	smalldatetime,
	@numfecini	int,
	@numfecfin	int,
	@numfecreg	int,
	@contador	smallint,
	@fecha_rec	varchar(12),
	@numero_rec	varchar(12),
	@tipo_rec	tinyint,
	@monto_rec	numeric(12,2),
	@numero_fac	varchar(12),
	@desc_fac	varchar(40),
	@monto_det	numeric(12,2),
	@inter_det	numeric(12,2), 
	@mntvlr_det	numeric(12,2),
	@reciboprov	varchar(12),
	@clicodigo	varchar(12),
	@clinombre	varchar(80),
	@clicontable	varchar(20),
	@real_dias	smallint,
	@real_dias2	smallint,
	@limite_cred	numeric(12,2)

begin
	set nocount on
	select @strComando01 = ''''
	select @strComando02 = ''''
	select @strComando03 = ''''
	select @strComando04 = ''''
	select @strComando05 = ''''
	select @strComando06 = ''''
	select @strComando07 = ''''
	select @strComando08 = ''''
	select @strComando09= ''''
	if @intTipoReporte = 1						-- Cordobas Sin Deslizamiento
		begin
			set @est_noven	= 0
			set @est_01_30	= 0
			set @est_31_60	= 0
			set @est_61myr	= 0
			create table #tmp1x
			(
				cliregistro smallint,
				vendcod varchar(12)
			)
			set @strComando01 = ''insert into #tmp1x select distinct c.registro, v.codigo from prm_clientes c, tbl_cliente_oblig o, prm_vendedores v where c.registro = o.cliregistro and 
				c.vendregistro = v.registro and o.saldo <> 0''
			exec (@strComando01 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			create table #tmp1
			(
				registro	smallint,
				codigo 	varchar(12),
				nombre 	varchar(100),
				direcc 	varchar(100),
				telefn 	varchar(30),
				fax	varchar(30),
				vended	varchar(12),
				numfac 	varchar(12),
				fecing 	varchar(12),
				fecven 	varchar(12),
				dteven	smalldatetime,
				mtofac 	numeric(12,2),
				mtofcU	numeric(12,2),
				sldfac	numeric(12,2),
				fecrec 	varchar(12),
				numfec	int,
				dterec	smalldatetime,
				numrec 	varchar(12),
				dias	smallint,
				interes	numeric(12,2),
				mantvlr	numeric(12,2),
				debito	numeric(12,2),
				credito	numeric(12,2),
				saldo	numeric(12,2),
				tpcmb	numeric(13,4),
				no_vencido numeric(12,2),
				e1_30	numeric(12,2),
				e31_60	numeric(12,2),
				e61_mas numeric(12,2),
				total_cli	numeric(12,2),
				total_facs numeric(12,2),
				real_dias smallint,
				limite	numeric(12,2)
			)
			create clustered index #tmp1ix on #tmp1 (codigo, dteven, numfac, dterec)
			create index #tmp1ix2 on #tmp1 (registro)
			set @clireg = 0
			set @codigo = ''''
			set @nombre = ''''
			set @direcc = ''''
			set @telefn = ''''
			set @fax = ''''
			set @numfac = ''''
			set @fecing = ''''
			set @fecven = ''''
			set @mtofac = 0
			set @sldfac = 0
			set @mtofacU = 0
			set @inters = 0
			set @mntvlr = 0
			set @cntdia = 0
			set @tpcmb = 0
			set @total_cli = 0
			set @total_facs = 0
			declare tmpX cursor for
				select distinct cliregistro, vendcod from #tmp1x
			open tmpX
			fetch next from tmpX into @clireg, @codvend
			while @@fetch_status = 0
				begin
					select @codigo = codigo, @nombre = (nombre + '' ('' + codigo + '')''), @direcc = direccion, @telefn = telefono, @fax = fax, @limite_cred = limite from prm_clientes where registro = @clireg
					declare tmpC cursor for
						select numero, fechaing, fechaven, monto, saldo, datefechaing, datefechaven, numfechaing from tbl_cliente_oblig where cliregistro = @clireg and saldo <> 0 and estado <> 2
					open tmpC
					fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
					while @@fetch_status = 0
						begin
							select @tpcmb = tipo_cambio from prm_tipocambio where numfecha = convert(varchar(8), @dteing, 112)
							set @mtofacU = (@mtofac / @tpcmb)
							insert into #tmp1 select @clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, @mtofac, @mtofacU, 
								@sldfac, e.fecha, e.numfecha, e.fecha_ing, e.numero, 0, 0, 0, 0, d.monto, 0, t.tipo_cambio, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred
								from tbl_recibos_enc e, tbl_recibos_det d, prm_tipocambio t
								where e.registro = d.registro and e.numfecha = t.numfecha and d.numero = @numfac and cliregistro = @clireg and e.estado = 0 and e.numfecha >= @numfecini
								order by e.numfecha, e.numero
							if @@rowcount = 0
								begin
									insert into #tmp1 values (@clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, 
										@mtofac, @mtofacU, @sldfac, '''', 0, '''', '''', 0, 0, 0, 0, 0, @mtofac, 0, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred)
								end
							fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
						end
					close tmpC
					deallocate tmpC
					set @intCiclo = 0
					set @dteRec01 = ''''
					set @dteRec02 = ''''
					set @sld_01 = 0
					set @sld_02 = 0
					set @numfac2 = ''''
					set @clireg2 = 0
					set @est_noven	= 0
					set @est_01_30	= 0
					set @est_31_60	= 0
					set @est_61myr	= 0
					set @total_facs = 0
					declare tmpC cursor for
						select codigo, numfac, dteven, mtofac, mtofcU, dterec, credito, tpcmb, real_dias from #tmp1 where registro = @clireg
					open tmpC
					fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
					while @@fetch_status = 0
						begin
							if @numfac2 = ''''
								begin
									set @numfac2 = @numfac
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							if @numfac2 != @numfac
								begin
									set @numfac2 = @numfac
									set @intCiclo = 0
									if @real_dias2 <= 0
										set @est_noven	= @est_noven + @sld_01
									else if @real_dias2 >= 1 and @real_dias2 <= 30
										set @est_01_30	= @est_01_30 + @sld_01
									else if @real_dias2 >= 31 and @real_dias2 <= 60
										set @est_31_60	= @est_31_60 + @sld_01
									else if @real_dias2 >= 61 
										set @est_61myr	= @est_61myr + @sld_01
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							set @inters = 0
							set @mntvlr = 0
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @dteRec01 = @dteven
											set @dteRec02 = @dterec
											set @sld_01 = @mtofac
											set @sld_02 = @mtofacU 
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											if @dteRec02 > @dteRec01
												begin
													set @dteRec01 = @dteRec02
												end
											else
												begin
													set @dteRec01 = @dteven
												end
											set @inters = 0
											set @mntvlr = 0
											if @cntDia < 0
												begin
													set @cntDia = 0
												end
										end
									else
										begin
											select @tpcmb = tipo_cambio from prm_tipocambio where 
												numfecha = convert(varchar(8), getdate(), 112)
											set @dteRec01 = @dteven
											set @dteRec02 = getdate()
											set @sld_01 = @mtofac
											set @sld_02 = @mtofacU 
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											set @inters = 0
											set @mntvlr = 0
											if @cntDia < 0
												begin
													set @cntDia = 0
												end
										end
								end
							else
								begin
									set @dteRec02 = @dterec
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											set @inters = 0
											set @mntvlr = 0
											if @cntDia < 0
												begin
													set @cntDia = 0
												end
										end
								end
							update #tmp1 set dias = @cntDia, interes = @inters, mantvlr = @mntvlr, debito = (@inters + @mntvlr), 
								saldo = (@sld_01 + @inters + @mntvlr - @credito), no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60,
									e61_mas = @est_61myr where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							set @sld_01 = @sld_01 + @inters + @mntvlr - @credito
							if @tpcmb != 0
								begin
									set @sld_02 = (@sld_01 / @tpcmb)
								end
							fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
						end
					if @real_dias2 <= 0
						set @est_noven	= @est_noven + @sld_01
					else if @real_dias2 >= 1 and @real_dias2 <= 30
						set @est_01_30	= @est_01_30 + @sld_01
					else if @real_dias2 >= 31 and @real_dias2 <= 60
						set @est_31_60	= @est_31_60 + @sld_01
					else if @real_dias2 >= 61 
						set @est_61myr	= @est_61myr + @sld_01
					set @total_cli = @est_noven + @est_01_30 + @est_31_60 + @est_61myr
					update #tmp1 set no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60, e61_mas = @est_61myr, 
--						total_cli = @total_cli, total_facs = @total_facs where codigo = @codcli
						total_cli = @total_cli, total_facs = @total_facs where registro = @clireg
					close tmpC
					deallocate tmpC
					set @saldo = 0
					set @intCiclo = 0
					set @numero = ''''
					declare tmpC cursor for
						select numfac, dterec, mtofac, debito, credito from #tmp1 where registro = @clireg
					open tmpC
					fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
					while @@fetch_status = 0
						begin
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									set @saldo = @sldfac
									set @numero = @numfac
								end
							if @numero != @numfac
								begin
									set @numero = @numfac
									set @saldo = @saldo + @sldfac
								end
							set @saldo = (@saldo + @debito - @credito)
							update #tmp1 set saldo = @saldo where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
						end
					close tmpC
					deallocate tmpC
					fetch next from tmpX into @clireg, @codvend
				end
			close tmpX
			deallocate tmpX
			if @intAccion = 0
				select distinct registro, nombre, limite from #tmp1 order by nombre
			else
--				begin
--					declare tmpC cursor for
--						select distinct codigo from prm_clientes
--					open tmpC
--					fetch next from tmpC into @codigo
--					while @@fetch_status = 0
--						begin
--							set @nombre = ''xxx''
--							select @nombre = nombre from #tmp1 where codigo = @codigo
--							if @nombre = ''xxx''
--								update prm_clientes set saldo = 0 where codigo = @codigo
--							fetch next from tmpC into @codigo
--						end
--					close tmpC
--					deallocate tmpC
--					declare tmpC cursor for
--						select distinct codigo, total_cli from #tmp1
--					open tmpC
--					fetch next from tmpC into @codigo, @saldo_cli
--					while @@fetch_status = 0
--						begin
--							update prm_clientes set saldo = @saldo_cli where codigo = @codigo
--							fetch next from tmpC into @codigo, @saldo_cli
--						end
--					close tmpC
--					deallocate tmpC
				select * from #tmp1 order by nombre, dteven, numfac, dterec
--				end
		end
	else if @intTipoReporte = 2					-- Cordobas Con Deslizamiento
		begin
			set @est_noven	= 0
			set @est_01_30	= 0
			set @est_31_60	= 0
			set @est_61myr	= 0
			create table #tmp2x
			(
				cliregistro smallint,
				vendcod varchar(12)
			)
			set @strComando01 = ''insert into #tmp2x select distinct c.registro, v.codigo from prm_clientes c, tbl_cliente_oblig o, prm_vendedores v where c.registro = o.cliregistro and 
				c.vendregistro = v.registro and o.saldo <> 0 ''
			exec (@strComando01 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			create table #tmp2
			(
				registro	smallint,
				codigo 	varchar(12),
				nombre 	varchar(100),
				direcc 	varchar(100),
				telefn 	varchar(30),
				fax	varchar(30),
				vended	varchar(12),
				numfac 	varchar(12),
				fecing 	varchar(12),
				fecven 	varchar(12),
				dteven	smalldatetime,
				mtofac 	numeric(12,2),
				mtofcU	numeric(12,2),
				sldfac	numeric(12,2),
				fecrec 	varchar(12),
				numfec	int,
				dterec	smalldatetime,
				numrec 	varchar(12),
				dias	smallint,
				interes	numeric(12,2),
				mantvlr	numeric(12,2),
				debito	numeric(12,2),
				credito	numeric(12,2),
				saldo	numeric(12,2),
				tpcmb	numeric(13,4),
				no_vencido numeric(12,2),
				e1_30	numeric(12,2),
				e31_60	numeric(12,2),
				e61_mas numeric(12,2),
				total_cli	numeric(12,2),
				total_facs numeric(12,2),
				real_dias smallint,
				limite	numeric(12,2)
			)
			create clustered index #tmp2ix on #tmp2 (codigo, dteven, numfac, dterec)
			create index #tmp2ix2 on #tmp2 (registro)
			set @clireg = 0
			set @codigo = ''''
			set @nombre = ''''
			set @direcc = ''''
			set @telefn = ''''
			set @fax = ''''
			set @numfac = ''''
			set @fecing = ''''
			set @fecven = ''''
			set @mtofac = 0
			set @sldfac = 0
			set @mtofacU = 0
			set @inters = 0
			set @mntvlr = 0
			set @cntdia = 0
			set @tpcmb = 0
			set @total_cli = 0
			set @total_facs = 0
			declare tmpX cursor for
				select distinct cliregistro, vendcod from #tmp2x
			open tmpX
			fetch next from tmpX into @clireg, @codvend
			while @@fetch_status = 0
				begin
					select @codigo = codigo, @nombre = (nombre + '' ('' + codigo + '')''), @direcc = direccion, @telefn = telefono, @fax = fax, @limite_cred = limite from prm_clientes where registro = @clireg
					declare tmpC cursor for
						select numero, fechaing, fechaven, monto, saldo, datefechaing, datefechaven, numfechaing from tbl_cliente_oblig where cliregistro = @clireg and saldo <> 0 and estado <> 2
					open tmpC
					fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
					while @@fetch_status = 0
						begin
							select @tpcmb = tipo_cambio from prm_tipocambio where numfecha = convert(varchar(8), @dteing, 112)
							set @mtofacU = (@mtofac / @tpcmb)
							insert into #tmp2 select @clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, @mtofac, @mtofacU, 
								@sldfac, e.fecha, e.numfecha, e.fecha_ing, e.numero, 0, 0, 0, 0, d.monto, 0, t.tipo_cambio, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred
								from tbl_recibos_enc e, tbl_recibos_det d, prm_tipocambio t
								where e.registro = d.registro and e.numfecha = t.numfecha and d.numero = @numfac and cliregistro = @clireg and e.estado = 0 and e.numfecha >= @numfecini
								order by e.numfecha, e.numero
							if @@rowcount = 0
								begin
									insert into #tmp2 values (@clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, 
										@mtofac, @mtofacU, @sldfac, '''', 0, '''', '''', 0, 0, 0, 0, 0, @mtofac, @tpcmb, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred)
								end
							fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
						end
					close tmpC
					deallocate tmpC
					set @intCiclo = 0
					set @dteRec01 = ''''
					set @dteRec02 = ''''
					set @sld_01 = 0
					set @sld_02 = 0
					set @numfac2 = ''''
					set @clireg2 = 0
					set @est_noven	= 0
					set @est_01_30	= 0
					set @est_31_60	= 0
					set @est_61myr	= 0
					set @total_facs = 0
					declare tmpC cursor for
						select codigo, numfac, dteven, mtofac, mtofcU, dterec, credito, tpcmb, real_dias from #tmp2 where registro = @clireg
					open tmpC
					fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
					while @@fetch_status = 0
						begin
							if @numfac2 = ''''
								begin
									set @numfac2 = @numfac
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							if @numfac2 != @numfac
								begin
									set @numfac2 = @numfac
									set @intCiclo = 0
									if @real_dias2 <= 0
										set @est_noven	= @est_noven + @sld_01
									else if @real_dias2 >= 1 and @real_dias2 <= 30
										set @est_01_30	= @est_01_30 + @sld_01
									else if @real_dias2 >= 31 and @real_dias2 <= 60
										set @est_31_60	= @est_31_60 + @sld_01
									else if @real_dias2 >= 61 
										set @est_61myr	= @est_61myr + @sld_01
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							set @inters = 0
							set @mntvlr = 0
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @dteRec01 = @dteven
											set @dteRec02 = @dterec
											set @sld_01 = @mtofac
											set @sld_02 = @mtofacU 
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											if @dteRec02 > @dteRec01
												begin
													set @dteRec01 = @dteRec02
												end
											else
												begin
													set @dteRec01 = @dteven
												end
											if @cntDia > 0
												begin
													set @inters = (0.0006666667 * @sld_01) * @cntDia
													set @mntvlr = (@sld_02 - (@sld_01 / @tpcmb)) * @tpcmb
												end
											else
												begin
													set @cntDia = 0
													set @inters = 0
													set @mntvlr = 0
												end
										end
									else
										begin
											select @tpcmb = tipo_cambio from prm_tipocambio where 
												numfecha = convert(varchar(8), getdate(), 112)
											set @dteRec01 = @dteven
											set @dteRec02 = getdate()
											set @sld_01 = @mtofac
											set @sld_02 = @mtofacU 
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											if @cntDia > 0
												begin
													set @inters = (0.0006666667 * @sld_01) * @cntDia
													set @mntvlr = (@sld_02 - (@sld_01 / @tpcmb)) * @tpcmb
												end
											else
												begin
													set @cntDia = 0
													set @inters = 0
													set @mntvlr = 0
												end
										end
								end
							else
								begin
									set @dteRec02 = @dterec
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											if @cntDia > 0
												begin
													set @inters = (0.0006666667 * @sld_01) * @cntDia
													set @mntvlr = (@sld_02 - (@sld_01 / @tpcmb)) * @tpcmb
												end
											else
												begin
													set @cntDia = 0
													set @inters = 0
													set @mntvlr = 0
												end
										end
								end
							update #tmp2 set dias = @cntDia, interes = @inters, mantvlr = @mntvlr, debito = (@inters + @mntvlr), 
								saldo = (@sld_01 + @inters + @mntvlr - @credito), no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60,
									e61_mas = @est_61myr where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							set @sld_01 = @sld_01 + @inters + @mntvlr - @credito
							if @tpcmb != 0
								begin
									set @sld_02 = (@sld_01 / @tpcmb)
								end
							fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
						end
					if @real_dias2 <= 0
						set @est_noven	= @est_noven + @sld_01
					else if @real_dias2 >= 1 and @real_dias2 <= 30
						set @est_01_30	= @est_01_30 + @sld_01
					else if @real_dias2 >= 31 and @real_dias2 <= 60
						set @est_31_60	= @est_31_60 + @sld_01
					else if @real_dias2 >= 61 
						set @est_61myr	= @est_61myr + @sld_01
					set @total_cli = @est_noven + @est_01_30 + @est_31_60 + @est_61myr
					update #tmp2 set no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60, e61_mas = @est_61myr, 
--						total_cli = @total_cli, total_facs = @total_facs where codigo = @codcli
						total_cli = @total_cli, total_facs = @total_facs where registro = @clireg
					close tmpC
					deallocate tmpC
					set @saldo = 0
					set @intCiclo = 0
					set @numero = ''''
					declare tmpC cursor for
						select numfac, dterec, mtofac, debito, credito  from #tmp2 where registro = @clireg
					open tmpC
					fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
					while @@fetch_status = 0
						begin
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									set @saldo = @sldfac
									set @numero = @numfac
								end
							if @numero != @numfac
								begin
									set @numero = @numfac
									set @saldo = @saldo + @sldfac
								end
							set @saldo = (@saldo + @debito - @credito)
							update #tmp2 set saldo = @saldo where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
						end
					close tmpC
					deallocate tmpC
					fetch next from tmpX into @clireg, @codvend
				end
			close tmpX
			deallocate tmpX
			if @intAccion = 0
				select distinct registro, nombre, limite from #tmp2 order by nombre
			else
				select * from #tmp2 order by nombre, dteven, numfac, dterec
		end
	else if @intTipoReporte = 3					-- Dolares Sin Intereses
		begin
			set @est_noven	= 0
			set @est_01_30	= 0
			set @est_31_60	= 0
			set @est_61myr	= 0
			create table #tmp3x
			(
				cliregistro smallint,
				vendcod varchar(12)
			)
			set @strComando01 = ''insert into #tmp3x select distinct c.registro, v.codigo from prm_clientes c, tbl_cliente_oblig o, prm_vendedores v where c.registro = o.cliregistro and 
				c.vendregistro = v.registro and o.saldo <> 0 ''
			exec (@strComando01 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			create table #tmp3
			(
				registro	smallint,
				codigo 	varchar(12),
				nombre 	varchar(100),
				direcc 	varchar(100),
				telefn 	varchar(30),
				fax	varchar(30),
				vended	varchar(12),
				numfac 	varchar(12),
				fecing 	varchar(12),
				fecven 	varchar(12),
				dteven	smalldatetime,
				mtofac 	numeric(12,2),
				mtofcU	numeric(12,2),
				sldfac	numeric(12,2),
				fecrec 	varchar(12),
				numfec	int,
				dterec	smalldatetime,
				numrec 	varchar(12),
				dias	smallint,
				interes	numeric(12,2),
				mantvlr	numeric(12,2),
				debito	numeric(12,2),
				credito	numeric(12,2),
				saldo	numeric(12,2),
				tpcmb	numeric(13,4),
				no_vencido numeric(12,2),
				e1_30	numeric(12,2),
				e31_60	numeric(12,2),
				e61_mas numeric(12,2),
				total_cli	numeric(12,2),
				total_facs numeric(12,2),
				real_dias smallint,
				limite	numeric(12,2)
			)
			create clustered index #tmp3ix on #tmp3 (codigo, dteven, numfac, dterec)
			create index #tmp3ix2 on #tmp3 (registro)
			set @clireg = 0
			set @codigo = ''''
			set @nombre = ''''
			set @direcc = ''''
			set @telefn = ''''
			set @fax = ''''
			set @numfac = ''''
			set @fecing = ''''
			set @fecven = ''''
			set @mtofac = 0
			set @sldfac = 0
			set @mtofacU = 0
			set @inters = 0
			set @mntvlr = 0
			set @cntdia = 0
			set @tpcmb = 0
			set @total_cli = 0
			set @total_facs = 0
			declare tmpX cursor for
				select distinct cliregistro, vendcod from #tmp3x
			open tmpX
			fetch next from tmpX into @clireg, @codvend
			while @@fetch_status = 0
				begin
					select @codigo = codigo, @nombre = (nombre + '' ('' + codigo + '')''), @direcc = direccion, @telefn = telefono, @fax = fax, @limite_cred = limite from prm_clientes where registro = @clireg
					declare tmpC cursor for
						select numero, fechaing, fechaven, monto, saldo, datefechaing, datefechaven, numfechaing from tbl_cliente_oblig where cliregistro = @clireg and saldo <> 0 and estado <> 2
					open tmpC
					fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
					while @@fetch_status = 0
						begin
							select @tpcmb = tipo_cambio from prm_tipocambio where numfecha = convert(varchar(8), @dteing, 112)
							set @mtofacU = (@mtofac / @tpcmb)
							insert into #tmp3 select @clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, @mtofacU, @mtofacU, 
								@sldfac, e.fecha, e.numfecha, e.fecha_ing, e.numero, 0, 0, 0, 0, d.monto, 0, t.tipo_cambio, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred
								from tbl_recibos_enc e, tbl_recibos_det d, prm_tipocambio t
								where e.registro = d.registro and e.numfecha = t.numfecha and d.numero = @numfac and cliregistro = @clireg and e.estado = 0 and e.numfecha >= @numfecini
								order by e.numfecha, e.numero
							if @@rowcount = 0
								begin
									select @tpcmb = tipo_cambio from prm_tipocambio where numfecha = convert(varchar(8), getdate(), 112)
									insert into #tmp3 values (@clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, 
										@mtofacU, @mtofacU, @sldfac, '''', 0, '''', '''', 0, 0, 0, 0, 0, @mtofac, 0, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred)
								end
							fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
						end
					close tmpC
					deallocate tmpC
					update #tmp3 set credito = (credito / tpcmb) where tpcmb <> 0 and registro = @clireg
					set @intCiclo = 0
					set @dteRec01 = ''''
					set @dteRec02 = ''''
					set @sld_01 = 0
					set @sld_02 = 0
					set @numfac2 = ''''
					set @clireg2 = 0
					set @est_noven	= 0
					set @est_01_30	= 0
					set @est_31_60	= 0
					set @est_61myr	= 0
					set @total_facs = 0
					declare tmpC cursor for
						select codigo, numfac, dteven, mtofac, mtofcU, dterec, credito, tpcmb, real_dias from #tmp3 where registro = @clireg
					open tmpC
					fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
					while @@fetch_status = 0
						begin
							if @numfac2 = ''''
								begin
									set @numfac2 = @numfac
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							if @numfac2 != @numfac
								begin
									set @numfac2 = @numfac
									set @intCiclo = 0
									if @real_dias2 <= 0
										set @est_noven	= @est_noven + @sld_01
									else if @real_dias2 >= 1 and @real_dias2 <= 30
										set @est_01_30	= @est_01_30 + @sld_01
									else if @real_dias2 >= 31 and @real_dias2 <= 60
										set @est_31_60	= @est_31_60 + @sld_01
									else if @real_dias2 >= 61 
										set @est_61myr	= @est_61myr + @sld_01
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							set @inters = 0
							set @mntvlr = 0
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @dteRec01 = @dteven
											set @dteRec02 = @dterec
											set @sld_01 = @mtofacU
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											if @dteRec02 > @dteRec01
												begin
													set @dteRec01 = @dteRec02
												end
											else
												begin
													set @dteRec01 = @dteven
												end
											set @inters = 0
											set @mntvlr = 0
											if @cntDia < 0
												begin
													set @cntDia = 0
												end
										end
									else
										begin
											set @dteRec01 = @dteven
											set @dteRec02 = getdate()
											set @sld_01 = @mtofacU
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											set @inters = 0
											set @mntvlr = 0
											if @cntDia < 0
												begin
													set @cntDia = 0
												end
										end
								end
							else
								begin
									set @dteRec02 = @dterec
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											set @inters = 0
											set @mntvlr = 0
											if @cntDia < 0
												begin
													set @cntDia = 0
												end
										end
								end
							update #tmp3 set dias = @cntDia, interes = @inters, mantvlr = @mntvlr, debito = (@inters + @mntvlr), 
								saldo = (@sld_01 + @inters + @mntvlr - @credito), no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60,
									e61_mas = @est_61myr where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							set @sld_01 = @sld_01 + @inters + @mntvlr - @credito
							fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
						end
					if @real_dias2 <= 0
						set @est_noven	= @est_noven + @sld_01
					else if @real_dias2 >= 1 and @real_dias2 <= 30
						set @est_01_30	= @est_01_30 + @sld_01
					else if @real_dias2 >= 31 and @real_dias2 <= 60
						set @est_31_60	= @est_31_60 + @sld_01
					else if @real_dias2 >= 61 
						set @est_61myr	= @est_61myr + @sld_01
					set @total_cli = @est_noven + @est_01_30 + @est_31_60 + @est_61myr
					update #tmp3 set no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60, e61_mas = @est_61myr, 
--						total_cli = @total_cli, total_facs = @total_facs where codigo = @codcli
						total_cli = @total_cli, total_facs = @total_facs where registro = @clireg
					close tmpC
					deallocate tmpC
					set @saldo = 0
					set @intCiclo = 0
					set @numero = ''''
					declare tmpC cursor for
						select numfac, dterec, mtofac, debito, credito  from #tmp3 where registro = @clireg
					open tmpC
					fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
					while @@fetch_status = 0
						begin
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									set @saldo = @sldfac
									set @numero = @numfac
								end
							if @numero != @numfac
								begin
									set @numero = @numfac
									set @saldo = @saldo + @sldfac
								end
							set @saldo = (@saldo + @debito - @credito)
							update #tmp3 set saldo = @saldo where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
						end
					close tmpC
					deallocate tmpC
					fetch next from tmpX into @clireg, @codvend
				end
			close tmpX
			deallocate tmpX
			if @intAccion = 0
				select distinct registro, nombre, limite from #tmp3 order by nombre
			else
				select * from #tmp3 order by nombre, dteven, numfac, dterec
		end
	else if @intTipoReporte = 4					-- Dolares Con Intereses
		begin
			set @est_noven	= 0
			set @est_01_30	= 0
			set @est_31_60	= 0
			set @est_61myr	= 0
			create table #tmp4x
			(
				cliregistro smallint,
				vendcod varchar(12)
			)
			set @strComando01 = ''insert into #tmp4x select distinct c.registro, v.codigo from prm_clientes c, tbl_cliente_oblig o, prm_vendedores v where c.registro = o.cliregistro and 
				c.vendregistro = v.registro and o.saldo <> 0 ''
			exec (@strComando01 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			create table #tmp4
			(
				registro	smallint,
				codigo 	varchar(12),
				nombre 	varchar(100),
				direcc 	varchar(100),
				telefn 	varchar(30),
				fax	varchar(30),
				vended	varchar(12),
				numfac 	varchar(12),
				fecing 	varchar(12),
				fecven 	varchar(12),
				dteven	smalldatetime,
				mtofac 	numeric(12,2),
				mtofcU	numeric(12,2),
				sldfac	numeric(12,2),
				fecrec 	varchar(12),
				numfec	int,
				dterec	smalldatetime,
				numrec 	varchar(12),
				dias	smallint,
				interes	numeric(12,2),
				mantvlr	numeric(12,2),
				debito	numeric(12,2),
				credito	numeric(12,2),
				saldo	numeric(12,2),
				tpcmb	numeric(13,4),
				no_vencido numeric(12,2),
				e1_30	numeric(12,2),
				e31_60	numeric(12,2),
				e61_mas numeric(12,2),
				total_cli	numeric(12,2),
				total_facs numeric(12,2),
				real_dias smallint,
				limite	numeric(12,2)
			)
			create clustered index #tmp4ix on #tmp4 (codigo, dteven, numfac, dterec)
			create index #tmp4ix2 on #tmp4 (registro)
			set @clireg = 0
			set @codigo = ''''
			set @nombre = ''''
			set @direcc = ''''
			set @telefn = ''''
			set @fax = ''''
			set @numfac = ''''
			set @fecing = ''''
			set @fecven = ''''
			set @mtofac = 0
			set @sldfac = 0
			set @mtofacU = 0
			set @inters = 0
			set @mntvlr = 0
			set @cntdia = 0
			set @tpcmb = 0
			set @total_cli = 0
			set @total_facs = 0
			declare tmpX cursor for
				select distinct cliregistro, vendcod from #tmp4x
			open tmpX
			fetch next from tmpX into @clireg, @codvend
			while @@fetch_status = 0
				begin
					select @codigo = codigo, @nombre = (nombre + '' ('' + codigo + '')''), @direcc = direccion, @telefn = telefono, @fax = fax, @limite_cred = limite from prm_clientes where registro = @clireg
					declare tmpC cursor for
						select numero, fechaing, fechaven, monto, saldo, datefechaing, datefechaven, numfechaing from tbl_cliente_oblig where cliregistro = @clireg and saldo <> 0 and estado <> 2
					open tmpC
					fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
					while @@fetch_status = 0
						begin
							select @tpcmb = tipo_cambio from prm_tipocambio where numfecha = convert(varchar(8), @dteing, 112)
							set @mtofacU = (@mtofac / @tpcmb)
							insert into #tmp4 select @clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, @mtofacU, @mtofacU, 
								@sldfac, e.fecha, e.numfecha, e.fecha_ing, e.numero, 0, 0, 0, 0, d.monto, 0, t.tipo_cambio, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred
								from tbl_recibos_enc e, tbl_recibos_det d, prm_tipocambio t
								where e.registro = d.registro and e.numfecha = t.numfecha and d.numero = @numfac and cliregistro = @clireg and e.estado = 0 and e.numfecha >= @numfecini
								order by e.numfecha, e.numero
							if @@rowcount = 0
								begin
									insert into #tmp4 values (@clireg, @codigo, @nombre, @direcc, @telefn, @fax, @codvend, @numfac, @fecing, @fecven, @dteven, 
										@mtofacU, @mtofacU, @sldfac, '''', 0, '''', '''', 0, 0, 0, 0, 0, @mtofac, 0, 0, 0, 0, 0, 0, 0, datediff(day, @dteven, getdate()), @limite_cred)
								end
							fetch next from tmpC into @numfac, @fecing, @fecven, @mtofac, @sldfac, @dteing, @dteven, @numfecini
						end
					close tmpC
					deallocate tmpC
					update #tmp4 set credito = (credito / tpcmb) where tpcmb <> 0 and registro = @clireg
					set @intCiclo = 0
					set @dteRec01 = ''''
					set @dteRec02 = ''''
					set @sld_01 = 0
					set @sld_02 = 0
					set @numfac2 = ''''
					set @clireg2 = 0
					set @est_noven	= 0
					set @est_01_30	= 0
					set @est_31_60	= 0
					set @est_61myr	= 0
					set @total_facs = 0
					declare tmpC cursor for
						select codigo, numfac, dteven, mtofac, mtofcU, dterec, credito, tpcmb, real_dias from #tmp4 where registro = @clireg
					open tmpC
					fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
					while @@fetch_status = 0
						begin
							if @numfac2 = ''''
								begin
									set @numfac2 = @numfac
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							if @numfac2 != @numfac
								begin
									set @numfac2 = @numfac
									set @intCiclo = 0
									if @real_dias2 <= 0
										set @est_noven	= @est_noven + @sld_01
									else if @real_dias2 >= 1 and @real_dias2 <= 30
										set @est_01_30	= @est_01_30 + @sld_01
									else if @real_dias2 >= 31 and @real_dias2 <= 60
										set @est_31_60	= @est_31_60 + @sld_01
									else if @real_dias2 >= 61 
										set @est_61myr	= @est_61myr + @sld_01
									set @total_facs = @total_facs + @mtofac
									set @real_dias2 = @real_dias
								end
							set @inters = 0
							set @mntvlr = 0
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @dteRec01 = @dteven
											set @dteRec02 = @dterec
											set @sld_01 = @mtofacU
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											if @dteRec02 > @dteRec01
												begin
													set @dteRec01 = @dteRec02
												end
											else
												begin
													set @dteRec01 = @dteven
												end
											if @cntDia > 0
												begin
													set @inters = (0.0006666667 * @sld_01) * @cntDia
												end
											else
												begin
													set @cntDia = 0
													set @inters = 0
													set @mntvlr = 0
												end
										end
									else
										begin
											select @tpcmb = tipo_cambio from prm_tipocambio where 
												numfecha = convert(varchar(8), getdate(), 112)
											set @dteRec01 = @dteven
											set @dteRec02 = getdate()
											set @sld_01 = @mtofacU
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											if @cntDia > 0
												begin
													set @inters = (0.0006666667 * @sld_01) * @cntDia
												end
											else
												begin
													set @cntDia = 0
													set @inters = 0
													set @mntvlr = 0
												end
										end
								end
							else
								begin
									set @dteRec02 = @dterec
									if @dterec != ''Jan  1 1900 12:00AM''
										begin
											set @cntDia = datediff(day, @dteRec01, @dteRec02)
											set @dteRec01 = @dteRec02
											if @cntDia > 0
												begin
													set @inters = (0.0006666667 * @sld_01) * @cntDia
												end
											else
												begin
													set @cntDia = 0
													set @inters = 0
													set @mntvlr = 0
												end
										end
								end
							update #tmp4 set dias = @cntDia, interes = @inters, mantvlr = @mntvlr, debito = (@inters + @mntvlr), 
								saldo = (@sld_01 + @inters + @mntvlr - @credito), no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60,
									e61_mas = @est_61myr where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							set @sld_01 = @sld_01 + @inters + @mntvlr - @credito
							if @tpcmb != 0
								begin
									set @sld_02 = (@sld_01 / @tpcmb)
								end
							fetch next from tmpC into @codcli, @numfac, @dteven, @mtofac, @mtofacU, @dterec, @credito, @tpcmb, @real_dias
						end
					if @real_dias2 <= 0
						set @est_noven	= @est_noven + @sld_01
					else if @real_dias2 >= 1 and @real_dias2 <= 30
						set @est_01_30	= @est_01_30 + @sld_01
					else if @real_dias2 >= 31 and @real_dias2 <= 60
						set @est_31_60	= @est_31_60 + @sld_01
					else if @real_dias2 >= 61 
						set @est_61myr	= @est_61myr + @sld_01
					set @total_cli = @est_noven + @est_01_30 + @est_31_60 + @est_61myr
					update #tmp4 set no_vencido = @est_noven, e1_30 = @est_01_30, e31_60 = @est_31_60, e61_mas = @est_61myr, 
--						total_cli = @total_cli, total_facs = @total_facs where codigo = @codcli
						total_cli = @total_cli, total_facs = @total_facs where registro = @clireg
					close tmpC
					deallocate tmpC
					set @saldo = 0
					set @intCiclo = 0
					set @numero = ''''
					declare tmpC cursor for
						select numfac, dterec, mtofac, debito, credito  from #tmp4 where registro = @clireg
					open tmpC
					fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
					while @@fetch_status = 0
						begin
							set @intCiclo = @intCiclo + 1
							if @intCiclo = 1
								begin
									set @saldo = @sldfac
									set @numero = @numfac
								end
							if @numero != @numfac
								begin
									set @numero = @numfac
									set @saldo = @saldo + @sldfac
								end
							set @saldo = (@saldo + @debito - @credito)
							update #tmp4 set saldo = @saldo where codigo = @codcli and numfac = @numfac and dterec = @dterec and credito = @credito
							fetch next from tmpC into @numfac, @dteRec, @sldfac, @debito, @credito
						end
					close tmpC
					deallocate tmpC
					fetch next from tmpX into @clireg, @codvend
				end
			close tmpX
			deallocate tmpX
			if @intAccion = 0
				select distinct registro, nombre, limite from #tmp4 order by nombre
			else
				select * from #tmp4 order by nombre, dteven, numfac, dterec
		end
	else if @intTipoReporte = 7					-- Analisis de Antiguedad Saldos Generales Detallados
		begin
			create table #tmp7
			(
				registro		smallint,
				cliente		varchar(80),
				codigo		varchar(12),
				fechaing	varchar(12),
				numero		varchar(12),
				dias		smallint,
				no_vencido	numeric(12,2),
				e1_30		numeric(12,2),
				e31_60		numeric(12,2),
				e61_mas	numeric(12,2),
				total		numeric(12,2),
				vendregistro	tinyint,
				vendedor	varchar(100),
				limite		numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp7 select c.registro, c.nombre, c.codigo, o.fechaing, o.numero, ''''dias'''' = case when datediff(day, datefechaven, getdate()) >= 0 ''
			set @strComando02 = ''then convert(smallint, datediff(day, datefechaven, getdate())) else 0 end, ''''no_venc'''' = case when datediff(day, datefechaven, getdate()) <= 0 then o.saldo else 0 end, ''
			set @strComando03 = ''''''de_1_a_30'''' = case when datediff(day, datefechaven, getdate()) between 1 and 30 then o.saldo else 0 end, ''
			set @strComando04 = ''''''de_31_a_60'''' = case when datediff(day, datefechaven, getdate()) between 31 and 60 then o.saldo else 0 end, ''
			set @strComando05 = ''''''de_61_a_mas'''' = case when datediff(day, datefechaven, getdate()) > 60 then o.saldo else 0 end, 0, c.vendregistro, '''' '''', c.limite ''
			set @strComando06 = ''from prm_clientes c, tbl_cliente_oblig o where c.registro = o.cliregistro and o.saldo <> 0 and o.estado <> 2 ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			update #tmp7 set total = (no_vencido + e1_30 + e31_60 + e61_mas)
			update #tmp7 set vendedor = (v.codigo + '' '' + v.nombre) from #tmp7 t, prm_vendedores v where t.vendregistro = v.registro
			if @intAccion = 0
				select distinct registro, cliente, limite from #tmp7 order by cliente
			else
				select * from #tmp7 order by cliente, numero
		end
	else if @intTipoReporte = 8					-- Analisis de Antiguedad Detallado del Vendedor
		begin
			create table #tmp8
			(
				registro		smallint,
				cliente		varchar(80),
				codigo		varchar(12),
				fechaing	varchar(12),
				numero		varchar(12),
				dias		smallint,
				no_vencido	numeric(12,2),
				e1_30		numeric(12,2),
				e31_60		numeric(12,2),
				e61_mas	numeric(12,2),
				total		numeric(12,2),
				vendregistro	tinyint,
				vendedor	varchar(100),
				limite		numeric(12,2),
				telefono	varchar(30),
				direccion	varchar(80)
			)
			set @strComando01 = ''insert into #tmp8 select c.registro, c.nombre, c.codigo, o.fechaing, o.numero, ''''dias'''' = case when datediff(day, datefechaven, getdate()) >= 0 ''
			set @strComando02 = ''then convert(smallint, datediff(day, datefechaven, getdate())) else 0 end, ''''no_venc'''' = case when datediff(day, datefechaven, getdate()) <= 0 then o.saldo else 0 end, ''
			set @strComando03 = ''''''de_1_a_30'''' = case when datediff(day, datefechaven, getdate()) between 1 and 30 then o.saldo else 0 end, ''
			set @strComando04 = ''''''de_31_a_60'''' = case when datediff(day, datefechaven, getdate()) between 31 and 60 then o.saldo else 0 end, ''
			set @strComando05 = ''''''de_61_a_mas'''' = case when datediff(day, datefechaven, getdate()) > 60 then o.saldo else 0 end, 0, c.vendregistro, '''' '''', c.limite, ''
			set @strComando06 = ''c.telefono, c.direccion from prm_clientes c, tbl_cliente_oblig o where c.registro = o.cliregistro and o.saldo <> 0 and o.estado <> 2 ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			update #tmp8 set total = (no_vencido + e1_30 + e31_60 + e61_mas)
			update #tmp8 set vendedor = (v.codigo + '' '' + v.nombre) from #tmp8 t, prm_vendedores v where t.vendregistro = v.registro
			if @intAccion = 0
				select distinct registro, cliente, limite from #tmp8 order by cliente
			else
				select * from #tmp8 order by vendedor, cliente, numero
		end
	else if @intTipoReporte = 9					-- Analisis de Antiguedad General Resumen
		begin
			create table #tmp9
			(
				registro		smallint,
				cliente		varchar(80),
				codigo		varchar(12),
				no_vencido	numeric(12,2),
				e1_30		numeric(12,2),
				e31_60		numeric(12,2),
				e61_mas	numeric(12,2),
				total		numeric(12,2),
				vendcod	varchar(12),
				vendnomb	varchar(60),
				limite		numeric(12,2)
			)
			create table #tmp9a
			(
				registro		smallint,
				cliente		varchar(80),
				codigo		varchar(12),
				vendcod	varchar(12),
				vendnomb	varchar(60),
				no_vencido	numeric(12,2),
				e1_30		numeric(12,2),
				e31_60		numeric(12,2),
				e61_mas	numeric(12,2),
				total		numeric(12,2),
				limite		numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp9 select c.registro, c.nombre, c.codigo, ''
			set @strComando02 = ''''''no_venc'''' = case when datediff(day, datefechaven, getdate()) <= 0 then o.saldo else 0 end, ''
			set @strComando03 = ''''''de_1_a_30'''' = case when datediff(day, datefechaven, getdate()) between 1 and 30 then o.saldo else 0 end, ''
			set @strComando04 = ''''''de_31_a_60'''' = case when datediff(day, datefechaven, getdate()) between 31 and 60 then o.saldo else 0 end, ''
			set @strComando05 = ''''''de_61_a_mas'''' = case when datediff(day, datefechaven, getdate()) > 60 then o.saldo else 0 end, 0, ''
			set @strComando06 = ''v.codigo, v.nombre, c.limite from prm_clientes c, tbl_cliente_oblig o, prm_vendedores v where c.registro = o.cliregistro ''
			set @strComando07 = ''and v.registro = c.vendregistro and o.saldo <> 0 and o.estado <> 2 ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strComando07 +
					@strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			update #tmp9 set total = (no_vencido + e1_30 + e31_60 + e61_mas)
			insert into #tmp9a select registro, cliente, codigo, vendcod, vendnomb, sum(no_vencido), sum(e1_30), sum(e31_60), sum(e61_mas), sum(total), limite from #tmp9 group by registro, cliente, codigo, vendcod, vendnomb, limite
			if @intAccion = 0
				select distinct registro, cliente, limite from #tmp9a order by cliente
			else
				select * from #tmp9a order by vendcod, cliente
		end
	else if @intTipoReporte = 10					-- Vendedores y Clientes Asignados
		begin
			if @intAccion = 0
				begin
					set @strComando01 = ''select distinct c.registro, c.nombre, c.limite from prm_clientes c, ''
					set @strComando02 = ''prm_departamentos d, prm_vendedores v where c.deptregistro = d.registro and c.vendregistro = v.registro ''
					set @strComando03 = ''order by c.nombre  ''
				end
			else
				begin
					set @strComando01 = ''select (v.codigo + '''' '''' + v.nombre) vendedor, c.codigo codcli, c.nombre, d.codigo, c.direccion, c.telefono, v.codigo vendcod, c.limite ''
					set @strComando02 = ''from prm_clientes c, prm_departamentos d, prm_vendedores v where c.deptregistro = d.registro and c.vendregistro = v.registro ''
					set @strComando03 = ''order by v.codigo, c.nombre, c.codigo  ''
				end
			exec (@strComando01 + @strComando02 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios + @strComando03)
		end
	else if @intTipoReporte = 11					-- Clientes y los Departamentos Asignados
		begin
			if @intAccion = 0
				begin
					set @strComando01 = ''select c.registro, c.nombre, c.limite from prm_clientes c, ''
					set @strComando02 = ''prm_departamentos d where c.deptregistro = d.registro ''
					set @strComando03 = ''order by c.nombre  ''
				end
			else
				begin
					set @strComando01 = ''select (d.codigo + '''' '''' + d.descripcion) departamento, c.codigo codcli, c.nombre, d.codigo, c.direccion, c.telefono, v.codigo vendcod, c.limite ''
					set @strComando02 = ''from prm_clientes c, prm_departamentos d, prm_vendedores v where c.deptregistro = d.registro and c.vendregistro = v.registro ''
					set @strComando03 = ''order by d.codigo, c.nombre  ''
				end
			exec (@strComando01 + @strComando02 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios + @strComando03)
		end
	else if @intTipoReporte = 12					-- Vendedores y sus Clientes Asignados
		begin
			create table #tmp12
			(
				registro		smallint,
				cliente		varchar(80),
				direccion	varchar(100),
				telefono		varchar(30),
				fax		varchar(30),
				numero		varchar(12),
				fechaing	varchar(12),
				fechaven	varchar(12),
				monto		numeric(12,2),
				dias		smallint,
				saldo		numeric(12,2),
				interes		numeric(12,2),
				no_vencido	numeric(12,2),
				e1_30		numeric(12,2),
				e31_60		numeric(12,2),
				e61_mas	numeric(12,2),
				total		numeric(12,2),
				vendregistro	tinyint,
				vendnombre	varchar(100),
				vendcodigo	varchar(12),
				limite		numeric(12,2)
			)
			set @strComando01 = ''insert into #tmp12 select c.registro, (c.nombre + '''' ('''' + c.codigo + '''')'''') cliente, c.direccion, c.telefono, c.fax, o.numero, o.fechaing, o.fechaven, o.monto, ''
			set @strComando02 = ''''''dias'''' = case when datediff(day, datefechaven, getdate()) >= 0 then convert(smallint, datediff(day, datefechaven, getdate())) ''
			set @strComando03 = ''else 0 end, o.saldo, 0 ''''interes'''', ''
			set @strComando04 = ''''''no_venc'''' = case when datediff(day, datefechaven, getdate()) <= 0 then o.saldo else 0 end, ''
			set @strComando05 = ''''''de_1_a_30'''' = case when datediff(day, datefechaven, getdate()) between 1 and 30 then o.saldo else 0 end, ''
			set @strComando06 = ''''''de_31_a_60'''' = case when datediff(day, datefechaven, getdate()) between 31 and 60 then o.saldo else 0 end, ''
			set @strComando07 = ''''''de_61_a_mas'''' = case when datediff(day, datefechaven, getdate()) > 60 then o.saldo else 0 end, 0, c.vendregistro, '''' '''', '''' '''', c.limite ''
			set @strComando08 = ''from prm_clientes c, tbl_cliente_oblig o where c.registro = o.cliregistro and o.saldo <> 0 and o.estado <> 2 ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + @strComando06 + @strComando07 + 
				@strComando08 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			update #tmp12 set vendnombre = v.nombre, vendcodigo = v.codigo from #tmp12 t, prm_vendedores v where t.vendregistro = v.registro
			update #tmp12 set total = no_vencido + e1_30 + e31_60 + e61_mas
			if @intAccion = 0
				begin
					select distinct registro, cliente, limite from #tmp12 order by cliente
				end
			else
				begin
					select vendnombre, vendcodigo, sum(no_vencido) no_vencido, sum(e1_30) e1_30, sum(e31_60) e31_60, sum(e61_mas) e61_mas, sum(total) total 
						from #tmp12 group by vendnombre, vendcodigo order by vendnombre
				end
		end
	else if @intTipoReporte = 13						-- Facturas Pendientes
		begin
			create table #tmp13
			(
				cliregistro	smallint,
				cliente		varchar(80),
				codigo		varchar(12),
				numero		varchar(12),
				fechaing	varchar(12),
				monto		numeric(12,2),
				abono		numeric(12,2),
				fechaven	varchar(12),
				vendregistro	tinyint,
				vendcodigo	varchar(12),
				limite		numeric(12,2)
			)
			create clustered index #tmp13ix on #tmp13 (cliregistro, numero)
			set @strComando01 = ''insert into #tmp13 select c.registro, c.nombre, c.codigo, o.numero, o.fechaing, o.monto, o.saldo, o.fechaven, c.vendregistro, ''
			set @strComando02 = '''''' '''', c.limite from prm_clientes c, tbl_cliente_oblig o where c.registro = o.cliregistro and o.saldo <> 0 and o.estado <> 2 ''
			exec (@strComando01 + @strComando02 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios)
			update #tmp13 set vendcodigo = v.codigo from #tmp13 t, prm_vendedores v where t.vendregistro = v.registro
			declare tmpC cursor for
				select distinct cliregistro, numero from #tmp13 order by cliregistro, numero
			open tmpC
			fetch next from tmpC into @cliregistro, @numero
			while @@fetch_status = 0
				begin
					set @saldo = 0
					select @saldo = isnull(sum(d.monto), 0) from tbl_recibos_enc e, tbl_recibos_det d where e.registro = d.registro and 
						e.cliregistro = @cliregistro and d.numero = @numero
					update #tmp13 set abono = @saldo where cliregistro = @cliregistro and numero = @numero
					fetch next from tmpC into @cliregistro, @numero
				end
			close tmpC
			deallocate tmpC
			if @intAccion = 0
				select distinct cliregistro, cliente, limite from #tmp13 order by cliente
			else
				select * from #tmp13 order by cliente, numero
		end
	else if @intTipoReporte = 14						-- Saldos Pendientes
		begin
			if @intAccion = 0
				begin
					set @strComando01 = ''select distinct c.registro, c.nombre, c.limite from prm_clientes c, tbl_cliente_oblig o ''
					set @strComando02 = ''where c.registro = o.cliregistro and o.saldo <> 0 and o.estado <> 2 ''
					set @strComando03 = ''order by c.nombre ''
				end
			else
				begin
					set @strComando01 = ''select c.nombre, c.codigo, c.direccion, c.telefono, sum(o.saldo) saldo from prm_clientes c, ''
					set @strComando02 = ''tbl_cliente_oblig o where c.registro = o.cliregistro and o.saldo <> 0 and o.estado <> 2 ''
					set @strComando03 = ''group by c.nombre, c.codigo, c.direccion, c.telefono order by c.nombre''
				end
			exec (@strComando01 + @strComando02 + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios + @strComando03)
		end
	else if @intTipoReporte = 15						-- Historico
		begin
			create table #tmp15
			(
				registro		bigint,
				cliregistro	smallint,
				fecha_rec	varchar(12),
				numfecrec	int,
				numero_rec	varchar(12),
				tipo_rec		tinyint,
				monto_rec	numeric(12,2),
				numero_fac	varchar(12),
				descrip_fac	varchar(40),
				monto_det	numeric(12,2),
				interes_det	numeric(12,2),
				mantvlr_det	numeric(12,2),
				reciboprov	varchar(12),
				saldo_cli	numeric(12,2),
				saldo_ini	numeric(12,2),
				clicodigo	varchar(12),
				clinombre	varchar(80),
				clicontable	varchar(20)
			)
			create clustered index #tmp15ix on #tmp15(registro)
			set @registro	= 0
			set @cli_regis	= 0
			set @saldo_cli	= 0
			set @saldo_reg	= 0
			set @fecha_reg	= ''''
			set @fecha_ini	= ''''
			set @fecha_fin	= ''''
			set @numfecini	= 0
			set @numfecfin	= 0
			set @numfecreg	= 0
			set @contador	= 0
			set @fecha_rec	= ''''
			set @numero_rec = ''''
			set @tipo_rec	= 0
			set @monto_rec	= 0
			set @numero_fac = ''''
			set @desc_fac	= ''''
			set @monto_det	= 0
			set @inter_det	= 0
			set @mntvlr_det	= 0
			set @clicodigo = ''''
			set @clinombre = ''''
			set @clicontable = ''''
		
			set @cli_regis	= @intCliente
			select @clicodigo = codigo, @clinombre = nombre, @clicontable = cta_contable from prm_clientes where registro = @intCliente
			set @fecha_ini 	= ''01/01/2000''
			set @fecha_fin 	= @dteFechaFin
			set @numfecini	= convert(int, convert(varchar(12), @fecha_ini, 112))
			set @numfecfin 	= convert(int, convert(varchar(12), @fecha_fin, 112))
			select @saldo_cli = saldo from prm_clientes where registro = @cli_regis
			set @saldo_reg 	= @saldo_cli
			select @numfecreg = max(numfecha) from tbl_recibos_enc where cliregistro = @cli_regis
			if @numfecfin > @numfecreg
				set @numfecreg = @numfecfin
			set @fecha_reg	= substring(convert(varchar(8), @numfecreg), 5, 2) + ''/'' + substring(convert(varchar(8), @numfecreg), 7, 2) + ''/'' + substring(convert(varchar(8), @numfecreg), 1, 4)
			set @fecha_ini = DATEADD(day, -1, @fecha_ini)
			set @numfecini 	= convert(int, convert(varchar(12), @fecha_ini, 112))
			while @fecha_reg > @fecha_ini
				begin
		-- Ingresar los Pagos del D?a que Hizo el Cliente
					declare tmpC cursor for
						select e.fecha, e.numero, e.tiporecibo, e.monto, d.numero, convert(varchar(40), d.descripcion) descripcion, (d.monto + d.pendiente), 
							d.interes, d.mantvlr, e.reciboprov
						from tbl_recibos_enc e, tbl_recibos_det d where e.registro = d.registro and e.cliregistro = @cli_regis and 
							e.numfecha = @numfecreg and e.estado = 0
							and d.descripcion not like ''%retenci%''
						order by e.registro desc, d.numero desc
					open tmpC
					fetch next from tmpC into @fecha_rec, @numero_rec, @tipo_rec, @monto_rec, @numero_fac, @desc_fac, @monto_det, @inter_det, 
						@mntvlr_det, @reciboprov
					while @@fetch_status = 0
						begin
							set @registro = @registro + 1
							if ltrim(rtrim(@desc_fac)) != ''''
								begin
									if @tipo_rec = 1 or @tipo_rec = 2
										begin
											set @saldo_reg = @saldo_reg + @monto_det
										end
									insert into #tmp15 values (@registro, @cli_regis, @fecha_rec, @numfecreg, @numero_rec, @tipo_rec, @monto_rec, @numero_fac, 
										@desc_fac, @monto_det, @inter_det, @mntvlr_det, @reciboprov, @saldo_cli, 0, @clicodigo, @clinombre, @clicontable)
									set @saldo_cli = @saldo_reg
								end
							fetch next from tmpC into @fecha_rec, @numero_rec, @tipo_rec, @monto_rec, @numero_fac, @desc_fac, @monto_det, 
								@inter_det, @mntvlr_det, @reciboprov
						end
					close tmpC
					deallocate tmpC
		-- Ingresar las Facturas del D?a que Hizo el Cliente
					declare tmpC cursor for
						select fechaing, numero, monto from tbl_cliente_oblig where cliregistro = @cli_regis and numfechaing = @numfecreg and estado != 2
							order by numero desc
					open tmpC
					fetch next from tmpC into @fecha_rec, @numero_rec, @monto_rec
					while @@fetch_status = 0
						begin
							set @registro = @registro + 1
							set @saldo_reg = @saldo_reg - @monto_rec
							insert into #tmp15 values (@registro, @cli_regis, @fecha_rec, @numfecreg, @numero_rec, 4, @monto_rec, @numero_rec, 
								@numero_rec, @monto_rec, 0, 0, null, @saldo_cli, 0, @clicodigo, @clinombre, @clicontable)
							set @saldo_cli = @saldo_reg
							fetch next from tmpC into @fecha_rec, @numero_rec, @monto_rec
						end
					close tmpC
					deallocate tmpC
					set @fecha_reg = DATEADD(day, -1, @fecha_reg)
					set @numfecreg = convert(int, convert(varchar(12), @fecha_reg, 112))
				end
			set @fecha_ini 	=@dteFechaIni
			set @numfecini	= convert(int, convert(varchar(12), @fecha_ini, 112))
			set @registro = 0
			select @registro = isnull(min(registro), 0) from #tmp15 where numfecrec < @numfecini
			if @registro != 0
				begin
					set @monto_rec = 0
					select @monto_rec = saldo_cli from #tmp15 where registro = @registro
					update #tmp15 set saldo_ini = @monto_rec
				end
			if  @strRecProv = ''''
				begin
					select fecha_rec, numero_rec, tipo_rec, numero_fac, descrip_fac, monto_det monto, saldo_cli, clicodigo, clinombre, clicontable
						from #tmp15 where numfecrec between @numfecini and @numfecfin order by registro desc, numero_fac
				end
			else
				begin
					select fecha_rec, numero_rec, tipo_rec, numero_fac, descrip_fac, monto_det monto, saldo_cli, clicodigo, clinombre, clicontable
						from #tmp15 where numfecrec between @numfecini and @numfecfin and reciboprov = @strRecProv
						order by registro desc, numero_fac
				end
		end
	else if @intTipoReporte = 16						-- Historico Intereses
		begin
			create table #tmp16
			(
				registro		bigint,
				cliregistro	smallint,
				fecha_rec	varchar(12),
				numfecrec	int,
				numero_rec	varchar(12),
				tipo_rec		tinyint,
				monto_rec	numeric(12,2),
				numero_fac	varchar(12),
				descrip_fac	varchar(40),
				monto_det	numeric(12,2),
				interes_det	numeric(12,2),
				mantvlr_det	numeric(12,2),
				reciboprov	varchar(12),
				saldo_cli	numeric(12,2),
				saldo_ini	numeric(12,2),
				clicodigo	varchar(12),
				clinombre	varchar(80),
				clicontable	varchar(20)
			)
			set @registro	= 0
			set @cli_regis	= 0
			set @saldo_cli	= 0
			set @saldo_reg	= 0
			set @fecha_reg	= ''''
			set @fecha_ini	= ''''
			set @fecha_fin	= ''''
			set @numfecini	= 0
			set @numfecfin	= 0
			set @numfecreg	= 0
			set @contador	= 0
			set @fecha_rec	= ''''
			set @numero_rec = ''''
			set @tipo_rec	= 0
			set @monto_rec	= 0
			set @numero_fac = ''''
			set @desc_fac	= ''''
			set @monto_det	= 0
			set @inter_det	= 0
			set @mntvlr_det	= 0
			set @clicodigo = ''''
			set @clinombre = ''''
			set @clicontable = ''''
		
			set @cli_regis	= @intCliente
			select @clicodigo = codigo, @clinombre = nombre, @clicontable = cta_contable from prm_clientes where registro = @intCliente
			set @fecha_ini 	= ''01/01/2000''
			set @fecha_fin 	= @dteFechaFin
			set @numfecini	= convert(int, convert(varchar(12), @fecha_ini, 112))
			set @numfecfin 	= convert(int, convert(varchar(12), @fecha_fin, 112))
			select @saldo_cli = saldo from prm_clientes where registro = @cli_regis
			set @saldo_reg 	= @saldo_cli
			select @numfecreg = max(numfecha) from tbl_recibos_enc where cliregistro = @cli_regis
			set @fecha_reg	= substring(convert(varchar(8), @numfecreg), 5, 2) + ''/'' + substring(convert(varchar(8), @numfecreg), 7, 2) + ''/'' + substring(convert(varchar(8), @numfecreg), 1, 4)
			set @fecha_ini = DATEADD(day, -1, @fecha_ini)
			set @numfecini 	= convert(int, convert(varchar(12), @fecha_ini, 112))
			while @fecha_reg > @fecha_ini
				begin
		-- Ingresar los Pagos del D?a que Hizo el Cliente
					declare tmpC cursor for
						select e.fecha, e.numero, e.tiporecibo, e.monto, d.numero, convert(varchar(40), d.descripcion) descripcion, (d.monto + d.pendiente), 
							d.interes, d.mantvlr, e.reciboprov
						from tbl_recibos_enc e, tbl_recibos_det d where e.registro = d.registro and e.cliregistro = @cli_regis and 
							e.numfecha = @numfecreg and e.estado = 0
							and d.descripcion not like ''%retenci%''
						order by e.registro desc, d.numero desc
					open tmpC
					fetch next from tmpC into @fecha_rec, @numero_rec, @tipo_rec, @monto_rec, @numero_fac, @desc_fac, @monto_det, @inter_det, 
						@mntvlr_det, @reciboprov
					while @@fetch_status = 0
						begin
							set @registro = @registro + 1
							if @tipo_rec = 1 or @tipo_rec = 2
								begin
									set @saldo_reg = @saldo_reg + @monto_det
								end
							insert into #tmp16 values (@registro, @cli_regis, @fecha_rec, @numfecreg, @numero_rec, @tipo_rec, @monto_rec, @numero_fac, 
								@desc_fac, @monto_det, @inter_det, @mntvlr_det, @reciboprov, @saldo_cli, 0, @clicodigo, @clinombre, @clicontable)
							set @saldo_cli = @saldo_reg
							fetch next from tmpC into @fecha_rec, @numero_rec, @tipo_rec, @monto_rec, @numero_fac, @desc_fac, @monto_det, 
								@inter_det, @mntvlr_det, @reciboprov
						end
					close tmpC
					deallocate tmpC
		-- Ingresar las Facturas del D?a que Hizo el Cliente
					declare tmpC cursor for
						select fechaing, numero, monto from tbl_cliente_oblig where cliregistro = @cli_regis and numfechaing = @numfecreg and estado != 2
							order by numero desc
					open tmpC
					fetch next from tmpC into @fecha_rec, @numero_rec, @monto_rec
					while @@fetch_status = 0
						begin
							set @registro = @registro + 1
							set @saldo_reg = @saldo_reg - @monto_rec
							insert into #tmp15 values (@registro, @cli_regis, @fecha_rec, @numfecreg, @numero_rec, 4, @monto_rec, @numero_rec, 
								@numero_rec, @monto_rec, 0, 0, null, @saldo_cli, 0, @clicodigo, @clinombre, @clicontable)
							set @saldo_cli = @saldo_reg
							fetch next from tmpC into @fecha_rec, @numero_rec, @monto_rec
						end
					close tmpC
					deallocate tmpC
					set @fecha_reg = DATEADD(day, -1, @fecha_reg)
					set @numfecreg = convert(int, convert(varchar(12), @fecha_reg, 112))
				end
			set @fecha_ini 	=@dteFechaIni
			set @numfecini	= convert(int, convert(varchar(12), @fecha_ini, 112))
			set @registro = 0
			select @registro = isnull(min(registro), 0) from #tmp16 where numfecrec < @numfecini
			if @registro != 0
				begin
					set @monto_rec = 0
					select @monto_rec = saldo_cli from #tmp16 where registro = @registro
					update #tmp16 set saldo_ini = @monto_rec
				end
			if  @strRecProv = ''''
				begin
					select fecha_rec, numero_rec, tipo_rec, numero_fac, descrip_fac, monto_det, interes_det, mantvlr_det, clicodigo, clinombre, clicontable
						from #tmp16 where numfecrec between @numfecini and @numfecfin and (interes_det <> 0 or mantvlr_det <> 0) order by registro desc, numero_fac
				end
			else
				begin
					select fecha_rec, numero_rec, tipo_rec, numero_fac, descrip_fac, monto_det, interes_det, mantvlr_det, clicodigo, clinombre, clicontable
						from #tmp16 where numfecrec between @numfecini and @numfecfin and (interes_det <> 0 or mantvlr_det <> 0)
						and reciboprov = @strRecProv order by registro desc, numero_fac
				end
		end
	else if @intTipoReporte = 17 or @intTipoReporte = 20 or @intTipoReporte = 23	-- Recibos de Caja Consolidado por Fecha/Vendedor/Cliente
		begin
			set @strComando01 = ''''
			set @strComando02 = ''''
			set @strComando03 = ''''
			set @strComando01 = ''select e.numfecha, e.fecha, e.numero, e.reciboprov, c.nombre, c.codigo codigoc, e.monto, v.codigo codigov from tbl_recibos_enc e, prm_clientes c, prm_vendedores v ''
			if @intAccion = 17
				set @strComando02 = ''where e.cliregistro = c.registro and e.vendregistro = v.registro and e.tiporecibo = 1 and e.numfecha between ''
			else if @intAccion = 20
				set @strComando02 = ''where e.cliregistro = c.registro and e.vendregistro = v.registro and e.tiporecibo = 2 and e.numfecha between ''
			else if @intAccion =23
				set @strComando02 = ''where e.cliregistro = c.registro and e.vendregistro = v.registro and e.tiporecibo = 3 and e.numfecha between ''
			set @strComando03 = ''and e.estado <> 1 order by e.registro''
			set @numfecini = convert(varchar(8), @dteFechaIni, 112)
			set @numfecfin = convert(varchar(8), @dteFechaFin, 112)
			exec (@strComando01 + @strComando02 + @numfecini + ''and '' + @numfecfin + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios + 
				@strComando03)

		end
	else if @intTipoReporte = 18 or @intTipoReporte = 21 or @intTipoReporte = 24	-- Recibos de Caja Detallado por Fecha/Vendedor
		begin
			create table #tmp18
			(
				numfecha	int,
				fecha		varchar(12),
				numero		varchar(12),
				reciboprov	varchar(12),
				nombre		varchar(60),
				codigo		varchar(12),
				monto		numeric(12,2),
				numerof		varchar(12),
				montod		numeric(12,2),
				codigov		varchar(12),
				nombrev		varchar(60),
				fechaing2	smalldatetime,
				fechaing	varchar(12),
				totalvend	numeric(12,2)
			)
			set @strComando01 = ''''
			set @strComando02 = ''''
			set @strComando03 = ''''
			set @strComando01 = ''insert into #tmp18 select e.numfecha, e.fecha, e.numero, e.reciboprov, c.nombre, c.codigo, e.monto, d.numero numerof, d.monto montod, v.codigo codigov, v.nombre nombrev, o.datefechaing, o.fechaing, 0 from tbl_recibos_enc e, ''
			if @intAccion = 18
				set @strComando02 = ''tbl_recibos_det d, prm_clientes c, prm_vendedores v, tbl_cliente_oblig o where e.registro = d.registro and e.cliregistro = c.registro and e.vendregistro = v.registro and d.numero = o.numero and e.cliregistro = o.cliregistro and e.tiporecibo = 1 and e.numfecha between ''
			else if @intAccion = 21
				set @strComando02 = ''tbl_recibos_det d, prm_clientes c, prm_vendedores v, tbl_cliente_oblig o where e.registro = d.registro and e.cliregistro = c.registro and e.vendregistro = v.registro and d.numero = o.numero and e.cliregistro = o.cliregistro and e.tiporecibo = 2 and e.numfecha between ''
			else if @intAccion = 24
				set @strComando02 = ''tbl_recibos_det d, prm_clientes c, prm_vendedores v, tbl_cliente_oblig o where e.registro = d.registro and e.cliregistro = c.registro and e.vendregistro = v.registro and e.numero = o.numero and e.cliregistro = o.cliregistro and e.tiporecibo = 3 and e.numfecha between ''
			set @strComando03 = ''and o.estado <> 2 and e.estado = 0 order by v.registro, e.registro''
			set @numfecini = convert(varchar(8), @dteFechaIni, 112)
			set @numfecfin = convert(varchar(8), @dteFechaFin, 112)
			exec (@strComando01 + @strComando02 + @numfecini + ''and '' + @numfecfin + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios + 
				@strComando03)
			select distinct numfecha, codigov, numero, monto into #tmp19 from #tmp18
			declare tmpC cursor for
				select distinct numfecha, codigov from #tmp19
			open tmpC
			fetch next from tmpC into @numfecreg, @codvend
			while @@fetch_status = 0
				begin
					set @abn_monto = 0
					select @abn_monto = sum(monto) from #tmp19 where numfecha = @numfecreg and codigov = @codvend
					update #tmp18 set totalvend = @abn_monto where numfecha = @numfecreg and codigov = @codvend
					fetch next from tmpC into @numfecreg, @codvend
				end
			close tmpC
			deallocate tmpC
			select * from #tmp18

		end
	else if @intTipoReporte = 19 or @intTipoReporte = 22 or @intTipoReporte = 25	-- Recibos de Caja Detallado por Fecha/Cliente
		begin
			set @strComando01 = ''''
			set @strComando02 = ''''
			set @strComando03 = ''''
			set @strComando01 = ''select e.numfecha, e.fecha, e.numero, e.reciboprov, c.nombre, c.codigo, e.monto, d.numero numerof, d.monto montod, o.fechaing from tbl_recibos_enc e, tbl_recibos_det d, prm_clientes c, ''
			if @intAccion = 19
				set @strComando02 = ''tbl_cliente_oblig o where e.registro = d.registro and e.cliregistro = c.registro and d.numero = o.numero and e.cliregistro = o.cliregistro and e.tiporecibo = 1 and e.numfecha between ''
			else if @intAccion = 22
				set @strComando02 = ''tbl_cliente_oblig o where e.registro = d.registro and e.cliregistro = c.registro and d.numero = o.numero and e.cliregistro = o.cliregistro and e.tiporecibo = 2 and e.numfecha between ''
			else if @intAccion = 25
				set @strComando02 = ''tbl_cliente_oblig o where e.registro = d.registro and e.cliregistro = c.registro and e.numero = o.numero and e.cliregistro = o.cliregistro and e.tiporecibo = 3 and e.numfecha between ''
			set @strComando03 = ''and o.estado <> 2 and e.estado = 0 order by c.registro, e.registro''
			set @numfecini = convert(varchar(8), @dteFechaIni, 112)
			set @numfecfin = convert(varchar(8), @dteFechaFin, 112)
			exec (@strComando01 + @strComando02 + @numfecini + ''and '' + @numfecfin + @strClientes + @strVendedores + @strNegocios + @strDepartamentos + @strMunicipios + 
				@strComando03)

		end
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptEstadisticas]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptEstadisticas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_RptEstadisticas] @intTipoReporte as tinyint, @strClases as varchar(200), @strClientes as varchar(200), @strDepartam as varchar(200), 
			@strNegocios as varchar(200), @strAgencias as varchar(200), @strVendedores as varchar(200), @strProductos as varchar(600), @strSubClases as varchar(200), 
			@strFecIni as varchar(35), @strFecFin as varchar(35), @intFactStatus as varchar(25), @intFactTipo as varchar(25), @intDesplegar as tinyint,
			@strProveedores as varchar(200)

As

Declare	@strComando01 as varchar(255),
	@strComando02 as varchar(200),
	@strComando03 as varchar(200),
	@strComando04 as varchar(200),
	@strComando05 as varchar(255),
	@codigo_orig	as varchar(12),
	@codigo	as varchar(12),
	@nombre_orig	as varchar(60),
	@nombre	as varchar(60),
	@mes_orig	as tinyint,
	@mes		as tinyint,
	@total_reg	as numeric(12,2),
	@totales	as numeric(12,2),
	@mes01	as numeric(12,2),
	@mes02	as numeric(12,2),
	@mes03	as numeric(12,2),
	@mes04	as numeric(12,2),
	@mes05	as numeric(12,2),
	@mes06	as numeric(12,2),
	@mes07	as numeric(12,2),
	@mes08	as numeric(12,2),
	@mes09	as numeric(12,2),
	@mes10	as numeric(12,2),
	@mes11	as numeric(12,2),
	@mes12	as numeric(12,2),
	@agenregistro	as tinyint,
	@prodregistro	as smallint,
	@prodcodigo	as varchar(12),
	@num_registro	as bigint,
	@agen_nombre	as varchar(50),
	@ag_nom_new	as varchar(50),
	@prod_nombre	as varchar(50),
	@saldo_fecha	as numeric(12,2),
	@claseregistro	as tinyint,
	@clasecodigo	as varchar(12),
	@clasenombre	as varchar(50),
	@registro	numeric(10),
	@registro_orig	numeric(10)

begin
	set nocount on
	select @strComando01 = ''''
	select @strComando02 = ''''
	select @strComando03 = ''''
	select @strComando04 = ''''
	select @strComando05 = ''''

	if @intTipoReporte >= 1 and @intTipoReporte <= 8
		begin
			create table #tmpVtas
			(
				registro	smallint,
				valores	numeric(12,2)
			)
			if @intTipoReporte = 1				-- Ventas por Clases
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select p.regclase, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select p.regclase, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by p.regclase order by p.regclase''
				end
			else if @intTipoReporte = 2			-- Ventas por Clientes
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select e.cliregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select e.cliregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by e.cliregistro order by e.cliregistro''
				end
			else if @intTipoReporte = 3			-- Ventas por Departamentos
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select e.deptregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select e.deptregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by e.deptregistro order by e.deptregistro''
				end
			else if @intTipoReporte = 4			-- Ventas por Negocios
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select e.negregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select e.negregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by e.negregistro order by e.negregistro''
				end
			else if @intTipoReporte = 5			-- Ventas por Agencias
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select e.agenregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select e.agenregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by e.agenregistro order by e.agenregistro''
				end
			else if @intTipoReporte = 6			-- Ventas por Vendedores
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select e.vendregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select e.vendregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by e.vendregistro order by e.vendregistro''
				end
			else if @intTipoReporte = 7			-- Ventas por SubClases
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select p.regsubclase, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select p.regsubclase, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by p.regsubclase order by p.regsubclase''
				end
			else if @intTipoReporte = 8			-- Ventas por Productos
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #tmpVtas select p.registro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #tmpVtas select p.registro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by p.registro order by p.registro''
				end
			set @strComando02 = ''prm_proveedores x where e.registro = d.registro and d.prodregistro = p.registro and p.regproveedor = x.registro ''
			exec (@strComando01 + @strComando02 + @strFecIni + @strFecFin + @strClases + @strClientes + @strDepartam + @strNegocios + @strAgencias + 
				@strVendedores + @strProductos + @strSubClases + @intFactStatus + @intFactTipo + @strProveedores + @strComando03)
			if @intTipoReporte = 1				-- Ventas por Clases
				begin
					select c.codigo, c.descripcion, t.valores from #tmpVtas t, prm_clases c where t.registro = c.registro
				end
			else if @intTipoReporte = 2			-- Ventas por Clientes
				begin
					select c.codigo, c.nombre, t.valores from #tmpVtas t, prm_clientes c where t.registro = c.registro
				end
			else if @intTipoReporte = 3			-- Ventas por Departamentos
				begin
					select c.codigo, c.descripcion, t.valores from #tmpVtas t, prm_departamentos c where t.registro = c.registro
				end
			else if @intTipoReporte = 4			-- Ventas por Negocios
				begin
					select c.codigo, c.descripcion, t.valores from #tmpVtas t, prm_negocios c where t.registro = c.registro
				end
			else if @intTipoReporte = 5			-- Ventas por Agencias
				begin
					select c.codigo, c.descripcion, t.valores from #tmpVtas t, prm_agencias c where t.registro = c.registro
				end
			else if @intTipoReporte = 6			-- Ventas por Vendedores
				begin
					select c.codigo, c.nombre, t.valores from #tmpVtas t, prm_vendedores c where t.registro = c.registro
				end
			else if @intTipoReporte = 7			-- Ventas por SubClases
				begin
					select c.codigo, c.descripcion, t.valores from #tmpVtas t, prm_subclases c where t.registro = c.registro
				end
			else if @intTipoReporte = 8			-- Ventas por Productos
				begin
					select c.codigo, c.descripcion, t.valores from #tmpVtas t, prm_productos c where t.registro = c.registro
				end
		end
	else if @intTipoReporte >= 9 and @intTipoReporte <= 16
		begin
			create table #t1
			(
				mes		tinyint,
				registro		smallint,
				totales		numeric(12,2)
			)
			create clustered index t1_ix on #t1 (registro, mes)
			create table #tmp
			(
				codigo		varchar(12),
				nombre		varchar(60),
				enero		numeric(12,2),
				febrero		numeric(12,2),
				marzo		numeric(12,2),
				abril		numeric(12,2),
				mayo		numeric(12,2),
				junio		numeric(12,2),
				julio		numeric(12,2),
				agosto		numeric(12,2),
				septiembre	numeric(12,2),
				octubre		numeric(12,2),
				noviembre	numeric(12,2),
				diciembre	numeric(12,2),
				totales		numeric(12,2)
			)
			if @intTipoReporte = 9				-- Ventas por Clases
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, p.regclase, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, p.regclase, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), p.regclase order by substring(convert(char(8), e.numfechaing), 5, 2), p.regclase''
				end
			else if @intTipoReporte = 10			-- Ventas por Clientes
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.cliregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.cliregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), e.cliregistro order by substring(convert(char(8), e.numfechaing), 5, 2), e.cliregistro''
				end
			else if @intTipoReporte = 11			-- Ventas por Departamentos
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.deptregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.deptregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), e.deptregistro order by substring(convert(char(8), e.numfechaing), 5, 2), e.deptregistro''
				end
			else if @intTipoReporte = 12			-- Ventas por Negocios
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.negregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.negregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), e.negregistro order by substring(convert(char(8), e.numfechaing), 5, 2), e.negregistro''
				end
			else if @intTipoReporte = 13			-- Ventas por Agencias
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.agenregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.agenregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), e.agenregistro order by substring(convert(char(8), e.numfechaing), 5, 2), e.agenregistro''
				end
			else if @intTipoReporte = 14			-- Ventas por Vendedores
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.vendregistro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, e.vendregistro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), e.vendregistro order by substring(convert(char(8), e.numfechaing), 5, 2), e.vendregistro''
				end
			else if @intTipoReporte = 15			-- Ventas por SubClases
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, p.regsubclase, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, p.regsubclase, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), p.regsubclase order by substring(convert(char(8), e.numfechaing), 5, 2), p.regsubclase''
				end
			else if @intTipoReporte = 16			-- Ventas por Productos
				begin
					if @intDesplegar = 0
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, p.registro, sum(d.valor) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					else if @intDesplegar = 1
						begin
							set @strComando01 = ''insert into #t1 select substring(convert(char(8), e.numfechaing), 5, 2) mes, p.registro, sum(d.cantidad) totales from tbl_facturas_enc e, tbl_facturas_det d, prm_productos p, ''
						end
					set @strComando03 = ''group by substring(convert(char(8), e.numfechaing), 5, 2), p.registro order by substring(convert(char(8), e.numfechaing), 5, 2), p.registro''
				end
			set @strComando02 = ''prm_proveedores x where e.registro = d.registro and d.prodregistro = p.registro and p.regproveedor = x.registro ''
			exec (@strComando01 + @strComando02 + @strFecIni + @strFecFin + @strClases + @strClientes + @strDepartam + @strNegocios + @strAgencias + 
				@strVendedores + @strProductos + @strSubClases + @intFactStatus + @intFactTipo + @strProveedores + @strComando03)
			set @registro_orig = -1
			set @mes_orig = 0
			set @mes01 = 0
			set @mes02 = 0
			set @mes03 = 0
			set @mes04 = 0
			set @mes05 = 0
			set @mes06 = 0
			set @mes07 = 0
			set @mes08 = 0
			set @mes09 = 0
			set @mes10 = 0
			set @mes11 = 0
			set @mes12 = 0
			set @total_reg = 0
			declare tmp cursor for
				select registro, mes, totales from #t1
			open tmp
			fetch next from tmp into @registro, @mes, @totales
			while @@fetch_status = 0
				begin
					if @registro_orig = -1
						begin
							set @registro_orig = @registro
						end
					if @registro_orig != @registro
						begin
							set @codigo = ''''
							set @nombre = ''''
							if @intTipoReporte = 9
								begin
									select @codigo = codigo, @nombre = descripcion from prm_clases where registro = @registro_orig
								end
							else if @intTipoReporte = 10
								begin
									select @codigo = codigo, @nombre = nombre from prm_clientes where registro = @registro_orig
								end
							else if @intTipoReporte = 11
								begin
									select @codigo = codigo, @nombre = descripcion from prm_departamentos where registro = @registro_orig
								end
							else if @intTipoReporte = 12
								begin
									select @codigo = codigo, @nombre = descripcion from prm_negocios where registro = @registro_orig
								end
							else if @intTipoReporte = 13
								begin
									select @codigo = codigo, @nombre = descripcion from prm_agencias where registro = @registro_orig
								end
							else if @intTipoReporte = 14
								begin
									select @codigo = codigo, @nombre = nombre from prm_vendedores where registro = @registro_orig
								end
							else if @intTipoReporte = 15
								begin
									select @codigo = codigo, @nombre = descripcion from prm_subclases where registro = @registro_orig
								end
							else if @intTipoReporte = 16
								begin
									select @codigo = codigo, @nombre = descripcion from prm_productos where registro = @registro_orig
								end
							set @total_reg = @mes01 + @mes02 + @mes03 + @mes04 + @mes05 + @mes06 + @mes07 + @mes08 + @mes09 + @mes10 + @mes11 + @mes12
							insert into #tmp values (@codigo, @nombre, @mes01, @mes02, @mes03, @mes04, @mes05, @mes06, @mes07, @mes08, @mes09, @mes10, @mes11, @mes12, @total_reg)
							set @registro_orig = @registro
							set @mes01 = 0
							set @mes02 = 0
							set @mes03 = 0
							set @mes04 = 0
							set @mes05 = 0
							set @mes06 = 0
							set @mes07 = 0
							set @mes08 = 0
							set @mes09 = 0
							set @mes10 = 0
							set @mes11 = 0
							set @mes12 = 0
							set @total_reg = 0
						end
					if @mes = 1
						begin
							set @mes01 = @totales
						end
					else if @mes = 2
						begin
							set @mes02 = @totales
						end
					else if @mes = 3
						begin
							set @mes03 = @totales
						end
					else if @mes = 4
						begin
							set @mes04 = @totales
						end
					else if @mes = 5
						begin
							set @mes05 = @totales
						end
					else if @mes = 6
						begin
							set @mes06 = @totales
						end
					else if @mes = 7
						begin
							set @mes07 = @totales
						end
					else if @mes = 8
						begin
							set @mes08 = @totales
						end
					else if @mes = 9
						begin
							set @mes09 = @totales
						end
					else if @mes = 10
						begin
							set @mes10 = @totales
						end
					else if @mes = 11
						begin
							set @mes11 = @totales
						end
					else if @mes = 12
						begin
							set @mes12 = @totales
						end
					
					fetch next from tmp into @registro, @mes, @totales
				end
			close tmp
			set @codigo = ''''
			set @nombre = ''''
			if @intTipoReporte = 9
				begin
					select @codigo = codigo, @nombre = descripcion from prm_clases where registro = @registro_orig
				end
			else if @intTipoReporte = 10
				begin
					select @codigo = codigo, @nombre = nombre from prm_clientes where registro = @registro_orig
				end
			else if @intTipoReporte = 11
				begin
					select @codigo = codigo, @nombre = descripcion from prm_departamentos where registro = @registro_orig
				end
			else if @intTipoReporte = 12
				begin
					select @codigo = codigo, @nombre = descripcion from prm_negocios where registro = @registro_orig
				end
			else if @intTipoReporte = 13
				begin
					select @codigo = codigo, @nombre = descripcion from prm_agencias where registro = @registro_orig
				end
			else if @intTipoReporte = 14
				begin
					select @codigo = codigo, @nombre = nombre from prm_vendedores where registro = @registro_orig
				end
			else if @intTipoReporte = 15
				begin
					select @codigo = codigo, @nombre = descripcion from prm_subclases where registro = @registro_orig
				end
			else if @intTipoReporte = 16
				begin
					select @codigo = codigo, @nombre = descripcion from prm_productos where registro = @registro_orig
				end
			set @total_reg = @mes01 + @mes02 + @mes03 + @mes04 + @mes05 + @mes06 + @mes07 + @mes08 + @mes09 + @mes10 + @mes11 + @mes12
			insert into #tmp values (@codigo, @nombre, @mes01, @mes02, @mes03, @mes04, @mes05, @mes06, @mes07, @mes08, @mes09, @mes10, @mes11, @mes12, @total_reg)
			deallocate tmp
			select * from #tmp order by codigo
		end
	else if @intTipoReporte = 17				-- Producto Sucursales (A)
		begin
			create table #tmpSuc
			(
				agenregistro	tinyint,
				agennombre	varchar(50)
			)
			set @strComando01 = ''insert into #tmpSuc select distinct a.registro, a.descripcion from tbl_producto_movimiento m, prm_productos p, ''
			set @strComando02 = ''prm_agencias a, prm_proveedores x where a.registro = m.agenregistro and p.registro = m.prodregistro and ''
			set @strComando03 = ''p.regproveedor = x.registro and m.numfechaing <= ''+ @strFecFin +'' ''
			set @strComando04 = ''group by a.registro, a.descripcion order by a.registro, a.descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strAgencias + @strProductos + @strClases + @strSubClases + @strVendedores + @strComando04)
			select * from #tmpSuc order by agenregistro
		end
	else if @intTipoReporte = 18				-- Producto Sucursales (B)
		begin
			set @saldo_fecha = 0
			set @agen_nombre = ''''
			create table #tmpSucProd
			(
				agenregistro	tinyint,
				prodregistro	smallint,
				movregistro	bigint
			)
			create table #tmpSucCons
			(
				agenregistro	tinyint,
				agennombre	varchar(50),
				prodcodigo	varchar(12),
				prodnombre	varchar(50),
				clasecodigo	varchar(12),
				clasenombre	varchar(50),
				saldo_fecha	numeric(18,2)
			)
			create table #tmpResult
			(
				clasecodigo varchar(12),
				clasenombre varchar(60),
				prodcodigo varchar(12),
				prodnombre varchar(60)
			)
			set @strComando01 = ''insert into #tmpSucProd select m.agenregistro, m.prodregistro, max(m.registro) from tbl_producto_movimiento m, prm_productos p, ''
			set @strComando02 = ''prm_agencias a, prm_proveedores x where a.registro = m.agenregistro and p.registro = m.prodregistro and ''
			set @strComando03 = ''p.regproveedor = x.registro and m.numfechaing <= ''+ @strFecFin +'' ''
			set @strComando04 = ''group by m.agenregistro, m.prodregistro order by m.agenregistro, m.prodregistro''
			exec (@strComando01 + @strComando02 + @strComando03 + @strAgencias + @strProductos + @strClases + @strSubClases + @strVendedores + @strComando04)
			declare tmpS cursor for
				select agenregistro, prodregistro, movregistro from #tmpSucProd order by agenregistro, prodregistro
			open tmpS
			fetch next from tmpS into @agenregistro, @prodregistro, @num_registro
			while @@fetch_status = 0
				begin
					set @saldo_fecha = 0
					set @agen_nombre = ''''
					set @prodcodigo = ''''
					set @prod_nombre = ''''
					set @claseregistro = 0
					select @agen_nombre = descripcion from prm_agencias where registro = @agenregistro
					select @prodcodigo = codigo, @prod_nombre = descripcion, @claseregistro = regclase from prm_productos where registro = @prodregistro
					select @clasecodigo = codigo, @clasenombre = descripcion from prm_clases where registro = @claseregistro
					if @intDesplegar = 0
						begin
							select @saldo_fecha = isnull(saldo_mto, 0) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro and registro = @num_registro
						end
					else if @intDesplegar = 1
						begin
							select @saldo_fecha = isnull(saldo_cnt, 0) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro and registro = @num_registro
						end
					insert into #tmpSucCons values (@agenregistro, @agen_nombre, @prodcodigo, @prod_nombre, @clasecodigo, @clasenombre, @saldo_fecha)
					fetch next from tmpS into @agenregistro, @prodregistro, @num_registro
				end
			close tmpS
			deallocate tmpS
			create clustered index #tmpSCIndex on #tmpSucCons(agenregistro)
			declare tmpProd cursor for
				select distinct prodcodigo, prodnombre, clasecodigo, clasenombre from #tmpSucCons where saldo_fecha <> 0 order by prodcodigo
			open tmpProd
			fetch next from tmpProd into @prodcodigo, @prod_nombre, @clasecodigo, @clasenombre
			while @@fetch_status = 0
				begin
					insert into #tmpResult (clasecodigo, clasenombre, prodcodigo, prodnombre) values (@clasecodigo, @clasenombre, @prodcodigo, @prod_nombre)
					fetch next from tmpProd into @prodcodigo, @prod_nombre, @clasecodigo, @clasenombre
				end
			close tmpProd
			deallocate tmpProd
			create clustered index #tmpRIndex on #tmpResult(prodcodigo)
			declare tmpAgen cursor for
				select distinct agenregistro, agennombre from #tmpSucCons order by agenregistro
			open tmpAgen
			fetch next from tmpAgen into @agenregistro, @agen_nombre
			while @@fetch_status = 0
				begin
					set @ag_nom_new = ''''
					set @strComando01 = ''''
					set @ag_nom_new = @agen_nombre
					set @ag_nom_new= replace(@ag_nom_new,''('','''')
					set @ag_nom_new= replace(@ag_nom_new,'')'','''')
					set @ag_nom_new= replace(@ag_nom_new,''.'','''')
					set @ag_nom_new= replace(@ag_nom_new,'' '','''')
					set @strComando01 = ''alter table #tmpResult add ''+ @ag_nom_new +'' numeric(12,2)''
					exec (@strComando01)
					declare tmpProd cursor for
						select prodcodigo, saldo_fecha from #tmpSucCons where saldo_fecha <> 0 and agenregistro = @agenregistro order by prodcodigo
					open tmpProd
					fetch next from tmpProd into @prodcodigo, @saldo_fecha
					while @@fetch_status = 0
						begin
							set @strComando01 = ''''
							set @strComando01 = ''update #tmpResult set ''+ @ag_nom_new +'' = ''+ convert(varchar(12), @saldo_fecha) +'' where prodcodigo = ''''''+ @prodcodigo +''''''''
							exec (@strComando01)
							fetch next from tmpProd into @prodcodigo, @saldo_fecha
						end
					close tmpProd
					deallocate tmpProd
					set @strComando01 = ''update #tmpResult set ''+ @ag_nom_new +'' = 0 where ''+ @ag_nom_new +'' is null''
					exec (@strComando01)
					fetch next from tmpAgen into @agenregistro, @agen_nombre
				end
			close tmpAgen
			deallocate tmpAgen
			select * from #tmpResult order by clasecodigo, prodnombre
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ExportarArchivos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExportarArchivos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_ExportarArchivos] @intExportar as tinyint, @intAgencia as tinyint, @lngNumFechaIni as int, @lngNumFechaFin as int
As
Declare	@prodregistro 	as smallint,
	@prodcodigo   	as varchar(12),
	@proddescrip  	as varchar(60),
	@uniddescrip   	as varchar(60),
	@prodcantidad 	as numeric(12,2),
	@moviregistro	as bigint
begin
	set nocount on
	set @prodregistro = 0
	set @prodcodigo = ''''
	set @proddescrip = ''''
	set @uniddescrip = ''''
	set @prodcantidad = 0
	if @intExportar = 0					-- Estado de Inventario
		begin
			create table #tmpProd01
			(
				prodcodigo   	varchar(12), 
				proddescrip	varchar(60),
				uniddescrip	varchar(60),
				cantidad	numeric(12,2)
			)
			declare tmpProd cursor for
				select distinct p.registro, p.codigo, p.descripcion, u.descripcion from prm_productos p, prm_unidades u, tbl_producto_movimiento t
						where agenregistro = @intAgencia and t.prodregistro = p.registro and p.regunidad = u.registro and p.estado = 0 order by p.registro
			open tmpProd
			fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @uniddescrip
			while @@fetch_status = 0
				begin
					set @moviregistro = 0
					select @moviregistro = isnull(max(registro),0) from tbl_producto_movimiento
						where agenregistro = @intAgencia and prodregistro = @prodregistro and numfechaing <= @lngNumFechaFin
					if @moviregistro > 0
						begin
							set @prodcantidad = 0
							select @prodcantidad = saldo_cnt from tbl_producto_movimiento
								where agenregistro = @intAgencia and prodregistro = @prodregistro and registro = @moviregistro and numfechaing <= @lngNumFechaFin
							insert into #tmpProd01 values (@prodcodigo, @proddescrip, @uniddescrip, @prodcantidad)
						end
					fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @uniddescrip
				end
			close tmpProd
			deallocate tmpProd
			select ltrim(rtrim(prodcodigo)) + ''|'' + ltrim(rtrim(proddescrip)) + ''|'' + ltrim(rtrim(uniddescrip)) + ''|'' + ltrim(rtrim(cantidad)) + ''|'' + ltrim(rtrim(@intAgencia)) + ''|'' + ltrim(rtrim(@lngNumFechaFin)) from #tmpProd01 order by proddescrip
		end
	else if @intExportar = 1					-- Tasas de Cambio
		begin
			select fecha + ''|'' + convert(varchar(8), numfecha) + ''|'' + convert(varchar(14), tipo_cambio) from prm_tipocambio where numfecha between @lngNumFechaIni and @lngNumFechaFin
				order by numfecha
		end
	else if @intExportar = 2					-- Clientes
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(tipocli)) + ''|'' + ltrim(rtrim(tipo_id)) + ''|'' + ltrim(rtrim(num_id)) + ''|'' + ltrim(rtrim(nombre)) + ''|'' + ltrim(rtrim(fechaingreso))
				+ ''|'' + ltrim(rtrim(numfechaing)) + ''|'' + ltrim(rtrim(direccion)) + ''|'' + ltrim(rtrim(negregistro)) + ''|'' + ltrim(rtrim(vendregistro)) + ''|'' + ltrim(rtrim(deptregistro)) + ''|'' + ltrim(rtrim(munregistro))
				+ ''|'' + ltrim(rtrim(telefono)) + ''|'' + ltrim(rtrim(fax)) + ''|'' + ltrim(rtrim(correo)) + ''|'' + ltrim(rtrim(negocio)) + ''|'' + ltrim(rtrim(negdirec)) + ''|'' + ltrim(rtrim(contacto01)) + ''|'' + ltrim(rtrim(contacto02))
				+ ''|'' + ltrim(rtrim(limite)) + ''|'' + ltrim(rtrim(saldo)) + ''|'' + ltrim(rtrim(dias_credito)) + ''|'' + ltrim(rtrim(cta_contable)) + ''|'' + ltrim(rtrim(estado)) + ''|'' + ltrim(rtrim(saldofavor)) from prm_clientes
				order by registro
		end
	else if @intExportar = 3					-- Clases
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(descripcion)) + ''|'' + ltrim(rtrim(estado)) from prm_clases order by registro
		end
	else if @intExportar = 4					-- SubClases
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(descripcion)) + ''|'' + ltrim(rtrim(registro2)) + ''|'' + ltrim(rtrim(facturar)) + ''|'' + ltrim(rtrim(estado)) from prm_subclases order by registro
		end
	else if @intExportar = 5					-- Unidades
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(descripcion)) + ''|'' + ltrim(rtrim(estado)) from prm_unidades order by registro
		end
	else if @intExportar = 6					-- Empaques
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(descripcion)) + ''|'' + ltrim(rtrim(estado)) from prm_empaques order by registro
		end
	else if @intExportar = 7					-- Proveedores
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(nombre)) + ''|'' + ltrim(rtrim(tipo)) + ''|'' + ltrim(rtrim(estado)) from prm_proveedores order by registro
		end
	else if @intExportar = 8					-- Productos
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(descripcion)) + ''|'' + ltrim(rtrim(regclase)) + ''|'' + ltrim(rtrim(regsubclase)) + ''|'' + ltrim(rtrim(regunidad)) + ''|'' + ltrim(rtrim(regempaque))
				+ ''|'' + ltrim(rtrim(regproveedor)) + ''|'' + ltrim(rtrim(pvpc)) + ''|'' + ltrim(rtrim(pvdc)) + ''|'' + ltrim(rtrim(pvpu)) + ''|'' + ltrim(rtrim(pvdu)) + ''|'' + ltrim(rtrim(impuesto)) + ''|'' + ltrim(rtrim(estado)) from prm_productos
				order by registro
		end
	else if @intExportar = 9					-- Vendedores
		begin
			select ltrim(rtrim(registro)) + ''|'' + ltrim(rtrim(codigo)) + ''|'' + ltrim(rtrim(nombre)) + ''|'' + ltrim(rtrim(localidad)) + ''|'' + ltrim(rtrim(estado)) from prm_vendedores order by registro
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngClases]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngClases]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngClases] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Clases where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Clases where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Clases
					insert into prm_Clases values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_Clases set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PrcEstadoInventario]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PrcEstadoInventario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_PrcEstadoInventario]

As

declare	@intagenregistro	tinyint,
	@prodregistro 		smallint,
	@strprodcodigo		varchar(12),
	@strproddescrip		varchar(50),
	@strprodunidad		varchar(50),
	@dblcantidad		numeric(12,2),
	@moviregistro		bigint,
	@numfechaing		numeric(8),
	@intunidadreg		tinyint

begin
	set nocount on
	declare tmpC cursor for
		select prodcod01, agencia, numfecha  from tmp_estadoinvent
	open tmpC
	fetch next from tmpC into @strprodcodigo, @intagenregistro, @numfechaing
	while @@fetch_status = 0
		begin
			set @intunidadreg = 0
			set @prodregistro = 0
			set @strproddescrip = ''''
			set @strprodunidad = ''''
			select @prodregistro = registro, @strproddescrip = descripcion, @intunidadreg = regunidad from prm_productos where codigo = @strprodcodigo
			select @strprodunidad = descripcion from prm_unidades u where registro = @intunidadreg
			set @moviregistro = 0
			select @moviregistro = isnull(max(registro),0) from tbl_producto_movimiento
				where agenregistro = @intagenregistro and prodregistro = @prodregistro and numfechaing <= @numfechaing
			if @moviregistro > 0
				begin
					set @dblcantidad = 0
					select @dblcantidad = saldo_cnt from tbl_producto_movimiento
						where agenregistro = @intagenregistro and prodregistro = @prodregistro and registro = @moviregistro
					update tmp_estadoinvent set prodesc02 = @strproddescrip, produnid02 = @strprodunidad, prodcant02 = @dblcantidad where prodcod01 = @strprodcodigo
				end
			fetch next from tmpC into @strprodcodigo, @intagenregistro, @numfechaing
		end
	close tmpC
	deallocate tmpC
	select c.codigo, c.descripcion, t.* from tmp_estadoinvent t, prm_productos p, prm_clases c where t.prodcod01 = p.codigo and p.regclase = c.registro and t.prodcant01 <> t.prodcant02
		order by c.codigo, t.proddesc01
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerFactura]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerFactura]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_ExtraerFactura] @lngAgencia as tinyint, @strNumero as varchar(12), @intTipoFactura as tinyint
As

begin
	set nocount on
	if right(@strNumero, 1) != ''C'' and right(@strNumero, 1) != ''A''		-- Contado
		begin
			if @intTipoFactura = 2
				begin
					Select 0, E.FechaIngreso, E.Numero, V.Codigo, V.Nombre, E.CliNombre, D.Cantidad, U.Codigo, P.Codigo, P.Descripcion, 
						D.Igv, D.Precio,
						D.Valor, D.ProdRegistro,
						E.SubTotal, E.Impuesto, 
						E.Retencion, E.Total, 
						E.Status, E.NumFechaIng, t.tipo_cambio, e.cliregistro
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Vendedores V, prm_Productos P, prm_Unidades U, prm_TipoCambio t
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Status = 0 And E.Impreso = 1 And E.Registro = D.Registro And 
						E.VendRegistro = V.Registro And D.ProdRegistro = P.Registro And P.RegUnidad = U.Registro and t.numfecha = e.numfechaing
				end
			else if @intTipoFactura = 3
				begin
					Select 0, E.FechaIngreso, E.Numero, V.Codigo, V.Nombre, E.CliNombre, D.Cantidad, U.Codigo, P.Codigo, P.Descripcion, 
						D.Igv, D.Precio, 
						D.Valor, D.ProdRegistro,
						E.SubTotal, E.Impuesto, 
						E.Retencion, E.Total, 
						E.Status, E.NumFechaIng, t.tipo_cambio, e.cliregistro
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Vendedores V, prm_Productos P, prm_Unidades U, prm_TipoCambio t
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Status = 0 And E.Registro = D.Registro And 
						E.VendRegistro = V.Registro And D.ProdRegistro = P.Registro And P.RegUnidad = U.Registro and t.numfecha = e.numfechaing
				end
			else if @intTipoFactura = 7
				begin
					Select E.Numero From tbl_Facturas_Enc E Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero
				end
			else if @intTipoFactura = 9
				begin
					Select 0, E.FechaIngreso, E.Numero, V.Codigo, V.Nombre, E.CliNombre, D.Cantidad, U.Codigo, P.Codigo, P.Descripcion, 
						D.Igv, D.Precio, 
						D.Valor, D.ProdRegistro, 
						E.SubTotal, E.Impuesto, 
						E.Retencion, E.Total, 
						E.Status, E.NumFechaIng, t.tipo_cambio, e.cliregistro
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Vendedores V, prm_Productos P, prm_Unidades U, prm_TipoCambio t
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero  And E.Registro = D.Registro And E.VendRegistro = V.Registro And 
						D.ProdRegistro = P.Registro And P.RegUnidad = U.Registro and t.numfecha = e.numfechaing
				end
			else if @intTipoFactura = 11
				begin
					Select P.Codigo, P.Descripcion, D.Cantidad, D.Precio, D.Valor, D.ProdRegistro, D.Registro, E.Numero, E.NumFechaIng
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Productos P 
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Status = 0 And E.Impreso = 0 And E.Registro = D.Registro And 
						D.ProdRegistro = P.Registro
				end
		end
	else if right(@strNumero, 1) = ''C'' or right(@strNumero, 1) = ''A''		-- Credito
		begin
			if @intTipoFactura = 5
				begin
					Select 0, E.FechaIngreso, E.Numero, E.DiasCredito, V.Codigo, V.Nombre, 
							(select C.Codigo from prm_clientes c where registro = e.cliregistro) ''C.Codigo'', 
						E.CliNombre, D.Cantidad, U.Codigo, P.Codigo, P.Descripcion, 
						D.Igv, D.Precio, 
						D.Valor, D.ProdRegistro, 
						E.SubTotal, E.Impuesto, 
						E.Retencion, E.Total, 
						E.Status, E.NumFechaIng, t.tipo_cambio, e.cliregistro
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Vendedores V, prm_Productos P, prm_Unidades U, prm_TipoCambio t
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Status = 0 And E.Impreso = 1 And E.Registro = D.Registro And 
						E.VendRegistro = V.Registro And D.ProdRegistro = P.Registro And P.RegUnidad = U.Registro and t.numfecha = e.numfechaing
				end
			else if @intTipoFactura = 6
				begin
					Select 0, E.FechaIngreso, E.Numero, E.DiasCredito, V.Codigo, V.Nombre, 
							(select C.Codigo from prm_clientes c where registro = e.cliregistro) ''C.Codigo'', 
						E.CliNombre, D.Cantidad, U.Codigo, P.Codigo, P.Descripcion, 
						D.Igv, D.Precio, 
						D.Valor, D.ProdRegistro, 
						E.SubTotal, E.Impuesto, 
						E.Retencion, E.Total, 
						E.Status, E.NumFechaIng, t.tipo_cambio, e.cliregistro
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Vendedores V, prm_Productos P, prm_Unidades U, prm_TipoCambio t
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Status = 0 And E.Registro = D.Registro And 
						E.VendRegistro = V.Registro And D.ProdRegistro = P.Registro And P.RegUnidad = U.Registro and t.numfecha = e.numfechaing
				end
			else if @intTipoFactura = 8
				begin
					Select E.Numero From tbl_Facturas_Enc E Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero
				end
			else if @intTipoFactura = 10
				begin
					Select 0, E.FechaIngreso, E.Numero, E.DiasCredito, V.Codigo, V.Nombre, 
							(select C.Codigo from prm_clientes c where registro = e.cliregistro) ''C.Codigo'', 
						E.CliNombre, D.Cantidad, U.Codigo, P.Codigo, P.Descripcion, 
						D.Igv, D.Precio, 
						D.Valor, D.ProdRegistro, 
						E.SubTotal, E.Impuesto, 
						E.Retencion, E.Total, 
						E.Status, E.NumFechaIng, t.tipo_cambio, e.cliregistro
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Vendedores V, prm_Productos P, prm_Unidades U, prm_TipoCambio t
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Registro = D.Registro And E.VendRegistro = V.Registro And 
						D.ProdRegistro = P.Registro And P.RegUnidad = U.Registro and t.numfecha = e.numfechaing
				end
			else if @intTipoFactura = 12
				begin
					Select P.Codigo, P.Descripcion, D.Cantidad, D.Precio, D.Valor, D.ProdRegistro, D.Registro, E.Numero, E.NumFechaIng
					From tbl_Facturas_Enc E, tbl_Facturas_Det D, prm_Productos P 
					Where E.AgenRegistro = @lngAgencia And E.Numero = @strNumero And E.Status = 0 And E.Impreso = 0 And E.Registro = D.Registro And 
						D.ProdRegistro = P.Registro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProductos]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProductos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngProductos] @lngRegistro as smallint, @strCodigo as varchar(12), @strDescripcion as varchar(65), @lngClase as tinyint, @lngSubClase as tinyint, 
					@lngUnidad as tinyint, @lngEmpaque as tinyint, @dblPVPC as numeric(10,2), @dblPVDC as numeric(10,2), @dblPVPU as numeric(10,2), 
					@dblPVDU as numeric(10,2), @intImpuesto as tinyint, @intEstado as tinyint, @intVerificar as tinyint, @lngProveedor as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Productos where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Productos where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Productos
					insert into prm_Productos values (@lngRegistro, @strCodigo, @strDescripcion, @lngClase, @lngSubClase, @lngUnidad, @lngEmpaque, @lngProveedor, 
								@dblPVPC, @dblPVDC, @dblPVPU, @dblPVDU, @intImpuesto, @intEstado)
				end
			else
				begin
					update prm_Productos set descripcion = @strDescripcion, regclase = @lngClase, regsubclase = @lngSubClase, regunidad = @lngUnidad, 
						regempaque = @lngEmpaque, regproveedor = @lngProveedor, pvpc = @dblPVPC, pvdc = @dblPVDC, pvpu = @dblPVPU, pvdu = @dblPVDU, impuesto = @intImpuesto,
						estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DetalleProducto]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DetalleProducto]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_DetalleProducto] @prodregistro as smallint, @agenciareg as tinyint
As

declare	@agenregistro	tinyint,
	@agen_codigos	varchar(12),
	@agen_nombres varchar(50),
	@prod_codigos	varchar(12),
	@prod_nombres	varchar(80),
	@fecha_1ra_en	varchar(12),
	@fecha_1ra_sl	varchar(12),
	@fecha_ult_en	varchar(12),
	@fecha_ult_sl	varchar(12),
	@cantd_1ra_en	varchar(12),
	@cantd_1ra_sl	varchar(12),
	@cantd_ult_en	varchar(12),
	@cantd_ult_sl	varchar(12),
	@costo_1ra_en	numeric(10,2),
	@costo_1ra_sl	numeric(10,2),
	@costo_ult_en	numeric(10,2),
	@costo_ult_sl	numeric(10,2),
	@monto_1ra_en	numeric(10,2),
	@monto_1ra_sl	numeric(10,2),
	@monto_ult_en	numeric(10,2),
	@monto_ult_sl	numeric(10,2),
	@docum_1ra_en varchar(12),
	@docum_1ra_sl	varchar(12),
	@docum_ult_en	varchar(12),
	@docum_ult_sl	varchar(12),
	@producto_cnt	numeric(12,2),
	@producto_cto	numeric(12,2),
	@producto_mto	numeric(12,2),
	@num_registro	bigint

begin
	set nocount on
	set @agenregistro = 0
	set @agen_codigos = ''''
	set @agen_nombres = ''''
	set @prod_codigos = ''''
	set @prod_nombres = ''''
	set @fecha_1ra_en = ''''
	set @fecha_1ra_sl = ''''
	set @fecha_ult_en = ''''
	set @fecha_ult_sl = ''''
	set @cantd_1ra_en = 0
	set @cantd_1ra_sl = 0
	set @cantd_ult_en = 0
	set @cantd_ult_sl = 0
	set @costo_1ra_en = 0
	set @costo_1ra_sl = 0
	set @costo_ult_en = 0
	set @costo_ult_sl = 0
	set @monto_1ra_en = 0
	set @monto_1ra_sl = 0
	set @monto_ult_en = 0
	set @monto_ult_sl = 0
	set @docum_1ra_en = ''''
	set @docum_1ra_sl = ''''
	set @docum_ult_en = ''''
	set @docum_ult_sl = ''''
	set @num_registro = 0
	set @producto_cnt = 0
	set @producto_cto = 0
	set @producto_mto = 0

	if @agenciareg = 0
		begin
			create table #tmp
			(
				agen_codigos	varchar(12),
				agen_nombres	varchar(50),
				prod_codigos	varchar(12),
				prod_nombres	varchar(80),
				fecha_1ra_en	varchar(12),
				docum_1ra_en	varchar(12),
				cantd_1ra_en	numeric(10,2),
				costo_1ra_en	numeric(10,2),
				monto_1ra_en	numeric(10,2),
				fecha_1ra_sl	varchar(12),
				docum_1ra_sl	varchar(12),
				cantd_1ra_sl	numeric(10,2),
				costo_1ra_sl	numeric(10,2),
				monto_1ra_sl	numeric(10,2),
				fecha_ult_en	varchar(12),
				docum_ult_en	varchar(12),
				cantd_ult_en	numeric(10,2),
				costo_ult_en	numeric(10,2),
				monto_ult_en	numeric(10,2),
				fecha_ult_sl	varchar(12),
				docum_ult_sl	varchar(12),
				cantd_ult_sl	numeric(10,2),
				costo_ult_sl	numeric(10,2),
				monto_ult_sl	numeric(10,2),
				saldo_cant	numeric(12,2),
				saldo_csto	numeric(12,2),
				saldo_mnto	numeric(12,2)
			)
			select @prod_codigos = codigo, @prod_nombres = descripcion from prm_productos where registro = @prodregistro
		
			declare tmpAgen cursor for
				select registro, codigo, descripcion from prm_agencias
		
			open tmpAgen
			fetch next from tmpAgen into @agenregistro, @agen_codigos, @agen_nombres
			while @@fetch_status = 0
				begin
					set @fecha_1ra_en = ''''
					set @fecha_1ra_sl = ''''
					set @fecha_ult_en = ''''
					set @fecha_ult_sl = ''''
					set @cantd_1ra_en = 0
					set @cantd_1ra_sl = 0
					set @cantd_ult_en = 0
					set @cantd_ult_sl = 0
					set @costo_1ra_en = 0
					set @costo_1ra_sl = 0
					set @costo_ult_en = 0
					set @costo_ult_sl = 0
					set @monto_1ra_en = 0
					set @monto_1ra_sl = 0
					set @monto_ult_en = 0
					set @monto_ult_sl = 0
					set @docum_1ra_en = ''''
					set @docum_1ra_sl = ''''
					set @docum_ult_en = ''''
					set @docum_ult_sl = ''''
					set @num_registro = 0
					set @producto_cnt = 0
					set @producto_cto = 0
					set @producto_mto = 0
					-- Primera Entrada
					select @num_registro = min(registro) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro
						and creditos_cnt <> 0
					select @fecha_1ra_en = fechaingreso, @docum_1ra_en = numdocumento, @cantd_1ra_en = creditos_cnt, @costo_1ra_en = creditos_cto, 
						@monto_1ra_en = creditos_mto from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro 
						and registro = @num_registro
					-- Primera Salida
					select @num_registro = min(registro) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro
						and debitos_cnt <> 0
					select @fecha_1ra_sl = fechaingreso, @docum_1ra_sl = numdocumento, @cantd_1ra_sl = debitos_cnt, @costo_1ra_sl = debitos_cto, 
						@monto_1ra_sl = debitos_mto from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro 
						and registro = @num_registro
					-- Ultima Entrada
					select @num_registro = max(registro) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro
						and creditos_cnt <> 0
					select @fecha_ult_en = fechaingreso, @docum_ult_en = numdocumento, @cantd_ult_en = creditos_cnt, @costo_ult_en = creditos_cto, 
						@monto_ult_en = creditos_mto from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro 
						and registro = @num_registro
					-- Ultima Salida
					select @num_registro = max(registro) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro
						and debitos_cnt <> 0
					select @fecha_ult_sl = fechaingreso, @docum_ult_sl = numdocumento, @cantd_ult_sl = debitos_cnt, @costo_ult_sl = debitos_cto, 
						@monto_ult_sl = debitos_mto from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro 
						and registro = @num_registro
					-- Saldo del Producto
					select @num_registro = max(registro) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro
					select @producto_cnt = saldo_cnt, @producto_cto = saldo_cto, @producto_mto = saldo_mto from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro 
						and registro = @num_registro
					-- Ingresar Datos en la Tabla Temporal
					insert into #tmp values (@agen_codigos, @agen_nombres, @prod_codigos, @prod_nombres, @fecha_1ra_en, @docum_1ra_en, @cantd_1ra_en, @costo_1ra_en,
						@monto_1ra_en, @fecha_1ra_sl, @docum_1ra_sl, @cantd_1ra_sl, @costo_1ra_sl, @monto_1ra_sl, @fecha_ult_en, @docum_ult_en, @cantd_ult_en, 
						@costo_ult_en, @monto_ult_en, @fecha_ult_sl, @docum_ult_sl, @cantd_ult_sl, @costo_ult_sl, @monto_ult_sl, @producto_cnt, @producto_cto, @producto_mto)
					fetch next from tmpAgen into @agenregistro, @agen_codigos, @agen_nombres
				end
			close tmpAgen
			deallocate tmpAgen
			select * from #tmp
			drop table #tmp
		end
	else
		begin
			select @num_registro = max(registro) from tbl_producto_movimiento where agenregistro = @agenciareg and prodregistro = @prodregistro
			select @producto_cto = saldo_cto from tbl_producto_movimiento where agenregistro = @agenciareg and prodregistro = @prodregistro and registro = @num_registro
			select @producto_cto
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptTipoPrecios]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptTipoPrecios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_RptTipoPrecios] @intTipoReporte as tinyint, @strProveedores as varchar(100), @strClases as varchar(100), @strSubClases as varchar(100)
As

declare	@strComando01 varchar(190),
	@strComando02 varchar(190),
	@strComando03 varchar(190),
	@strComando04 varchar(190),
	@strComando05 varchar(190),
	@prodregistro	smallint,
	@agenregistro	tinyint,
	@maxregistro	bigint,
	@numfechaing	int,
	@cant_final		numeric(18,2)

begin
	set nocount on
	create table #tmp
	(
		prodregistro	smallint
	)
	set @numfechaing = 0
	set @numfechaing = convert(int, convert(varchar(8), getdate(), 112))
	declare tmpA cursor for
		select registro from prm_agencias where estado = 0 and registro not in (3,4)
	declare tmpC cursor for
		select registro from prm_productos where estado = 0
	open tmpC
	fetch next from tmpC into @prodregistro
	while @@fetch_status = 0
		begin
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @numfechaing
					select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @numfechaing and registro = @maxregistro
					if @cant_final <> 0
						begin
							insert into #tmp values (@prodregistro)
							break
						end
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			fetch next from tmpC into @prodregistro
		end
	close tmpC
	deallocate tmpC
	deallocate tmpA
	if @intTipoReporte = 1
		begin
			set @strComando01 = ''select v.registro, v.codigo vcodigo, v.nombre vnombre, c.registro, c.codigo ccodigo, c.descripcion cdescripcion, s.registro, s.codigo scodigo, s.descripcion sdescripcion, ''
			set @strComando02 = ''p.codigo pcodigo, p.descripcion pdescripcion, p.pvpc, p.pvdc, u.codigo ucodigo from prm_proveedores v, prm_clases c, prm_subclases s, prm_productos p, ''
			set @strComando03 = ''prm_unidades u where c.registro = s.registro2 and c.registro = p.regclase and s.registro = p.regsubclase and v.registro = p.regproveedor and u.registro = p.regunidad ''
			set @strComando04 = ''and (p.pvpc <> 0 or p.pvdc <> 0) and p.registro in (select prodregistro from #tmp) order by v.registro, c.registro, s.registro, p.descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strProveedores + @strClases + @strSubClases + @strComando04)
		end
	else if @intTipoReporte = 2
		begin
			set @strComando01 = ''select c.registro, c.codigo ccodigo, c.descripcion cdescripcion, p.codigo pcodigo, p.descripcion pdescripcion, p.pvpc, p.pvdc, u.codigo ucodigo from prm_clases c, ''
			set @strComando02 = ''prm_productos p, prm_unidades u where c.registro = p.regclase and u.registro = p.regunidad ''
			set @strComando03 = ''and (p.pvpc <> 0 or p.pvdc <> 0) and p.registro in (select prodregistro from #tmp) order by c.registro, p.descripcion''
			exec (@strComando01 + @strComando02 + @strProveedores + @strClases + @strSubClases + @strComando03)
		end
	else if @intTipoReporte = 3
		begin
			set @strComando01 = ''select c.registro, c.codigo ccodigo, c.descripcion cdescripcion, s.registro, s.codigo scodigo, s.descripcion sdescripcion, p.codigo pcodigo, p.descripcion pdescripcion, ''
			set @strComando02 = ''p.pvpc, p.pvdc, u.codigo ucodigo from prm_clases c, prm_subclases s, prm_productos p, prm_unidades u where c.registro = s.registro2 and c.registro = p.regclase ''
			set @strComando03 = ''and s.registro = p.regsubclase and u.registro = p.regunidad ''
			set @strComando04 = ''and (p.pvpc <> 0 or p.pvdc <> 0) and p.registro in (select prodregistro from #tmp) order by c.registro, s.registro, p.descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strProveedores + @strClases + @strSubClases + @strComando04)
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptInventario]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptInventario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_RptInventario] @intTipoReporte as tinyint, @strProductos as varchar(100), @strAgencias as varchar(50), @strClases as varchar(100), 
			@strSubClases as varchar(100), @strProveedores as varchar(100), @strFecIni as varchar(30), @strFecFin as varchar(30), @strDocumento as varchar(100),
			@strTipoMovim as varchar(200), @dblCantidad as numeric(12,2)

As

Declare	@strComando01	as varchar(200),
	@strComando02		as varchar(200),
	@strComando03		as varchar(200),
	@strComando04		as varchar(200),
	@strComando05		as varchar(175),
	@strComando06		as varchar(175),
	@agenregistro 		as tinyint,
	@prodregistro 		as smallint,
	@moviregistro 		as bigint,
	@agencodigo   		as varchar(12),
	@agendescrip  		as varchar(60),
	@prodcodigo   		as varchar(12),
	@proddescrip  		as varchar(60),
	@clascodigo   		as varchar(12),
	@clasdescrip  		as varchar(60),
	@unidcodigo   		as varchar(12),
	@unidcodigo02		as varchar(12),
	@prodcantidad 		as numeric(12,2),
	@prodcosto    		as numeric(12,2),
	@prodmonto    		as numeric(12,2),
	@years_months		as int,
	@years_months_01	as int,
	@years_months_02	as int,
	@saldo_cant			as numeric(12,2),
	@saldo_csto			as numeric(12,2),
	@saldo_mnto			as numeric(12,2),
	@codigomovim		as varchar(3),
	@descripmovim		as varchar(50),
	@provdescrip		as varchar(60),
	@agen03cant			as numeric(12,2),
	@agen03mnto			as numeric(12,2),
	@agen04cant			as numeric(12,2),
	@agen04mnto			as numeric(12,2),
	@fecha_inicial		as numeric(8),
	@fecha_final		as numeric(8),
	@cant_final			as numeric(18,2),
	@cantidad			as numeric(18,2),
	@numfechaing		as numeric(8),
	@fecha_ult_cmp		as varchar(12),
	@numtiempo			as int,
	@cantidad_fact		as numeric(18,2),
	@cant_fnl_fact		as numeric(18,2)

begin
	set nocount on
	select @strComando01 = ''''
	select @strComando02 = ''''
	select @strComando03 = ''''
	select @strComando04 = ''''
	select @strComando05 = ''''
	select @strComando06 = ''''
	select @agenregistro = 0
	select @agencodigo = ''''
	select @agendescrip = ''''
	select @prodregistro = 0
	select @prodcodigo = ''''
	select @proddescrip = ''''
	select @clascodigo = ''''
	select @clasdescrip = ''''
	select @unidcodigo = ''''
	select @unidcodigo02 = ''''
	select @prodcantidad = 0
	select @prodcosto = 0
	select @prodmonto = 0
	select @years_months = 0
	select @years_months_01 = 0
	select @years_months_02 = 0
	select @saldo_cant = 0
	select @saldo_csto = 0
	select @saldo_mnto = 0
	select @moviregistro = 0
	select @codigomovim = ''''
	select @descripmovim = ''''
	select @fecha_inicial = 0
	select @fecha_final = 0

	if @intTipoReporte <> 3 and @intTipoReporte <> 18 and @intTipoReporte <> 19
		begin
			create table #tmpProdReg
			(
				registro	smallint
			)
			exec (''insert into #tmpProdReg select registro from prm_Productos where registro > 0 '' + @strProductos + @strClases + @strSubClases + @strProveedores)
			if @intTipoReporte <> 12 and @intTipoReporte <> 13
				begin
					declare tmpProd cursor for
						select p.registro, p.codigo, p.descripcion, c.codigo, c.descripcion, u.descripcion from prm_productos p, prm_clases c, prm_unidades u, 
							#tmpProdReg t where p.regclase = c.registro and p.regunidad = u.registro and p.registro = t.registro
				end
			else
				begin
					declare tmpProd cursor for
						select p.registro, p.codigo, p.descripcion, ''ccodigo'' as codigo, ''cdescrip'' as descripcion, ''udescrip'' as descripcion from prm_productos p where p.registro in (select registro from #tmpProdReg)
				end
			if @strAgencias != '' ''
				begin
					create table #tmpSuc
					(
						registro	tinyint
					)
					exec (''insert into #tmpSuc select registro from prm_Agencias where registro in '' + @strAgencias)
					declare tmpSuc cursor for
						select Registro from #tmpSuc
				end
			else
				begin
					declare tmpSuc cursor for
						select Registro from prm_Agencias
				end
		end
	if @intTipoReporte = 1					-- Kardex
		begin		
			select @fecha_inicial = convert(numeric(8), @strFecIni)
			select @fecha_final = convert(numeric(8), @strFecFin)
			create table #tmpProd
			(
				years_months	int,
				years		smallint,
				months		tinyint,
				agencodigo	varchar(12),
				agendescrip	varchar(60),
				prodregistro	smallint,
				prodcodigo   	varchar(12), 
				proddescrip	varchar(60),
				unidcodigo	varchar(12),
				saldo_cnt	numeric(12,2),
				saldo_cto	numeric(12,2),
				saldo_mto	numeric(12,2),
				fechaingreso	varchar(12),
				documento	varchar(12),
				codigomovim	varchar(3),
				creditos_cnt	numeric(12,2),
				creditos_cto	numeric(12,2),
				creditos_mto	numeric(12,2),
				debitos_cnt	numeric(12,2),
				debitos_cto	numeric(12,2),
				debitos_mto	numeric(12,2),
				totales_cnt	numeric(12,2),
				totales_cto	numeric(12,2),
				totales_mto	numeric(12,2),
				mov_registro	bigint
			)
			open tmpSuc
			fetch next from tmpSuc into @agenregistro
			while @@fetch_status = 0
				begin
					select @agencodigo = ''''
					select @agendescrip = ''''
					select @agencodigo = codigo, @agendescrip = descripcion from prm_Agencias where registro = @agenregistro
					open tmpProd
					fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
					while @@fetch_status = 0
						begin
							declare tmpFecha cursor for
								select distinct(substring(convert(char(8), numfechaing),1,6)) year_month from tbl_producto_movimiento
									where agenregistro = @agenregistro and prodregistro = @prodregistro and numfechaing between @fecha_inicial and @fecha_final
							open tmpFecha
							fetch next from tmpFecha into @years_months
							while @@fetch_status = 0
								begin
									select @saldo_cant = 0
									select @saldo_csto = 0
									select @saldo_mnto = 0
									select @moviregistro = 0
									select @years_months_01 = 0
									select @years_months_02 = 0
									select @years_months_01 = (@years_months * 100) + 1
									select @years_months_02 = (@years_months * 100) + 31
									select @moviregistro = max(registro) from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro and numfechaing < @years_months_01
									select @saldo_cant = isnull(saldo_cnt, 0), @saldo_csto = isnull(saldo_cto, 0), @saldo_mnto = isnull(saldo_mto, 0) from tbl_producto_movimiento where registro = @moviregistro and agenregistro = @agenregistro and prodregistro = @prodregistro
									insert into #tmpProd
									select	@years_months, substring(convert(char(6), @years_months), 1, 4), substring(convert(char(6), @years_months), 5, 2), @agencodigo, @agendescrip, 
										@prodregistro, @prodcodigo, @proddescrip, @unidcodigo, @saldo_cant, @saldo_csto, @saldo_mnto, fechaingreso, numdocumento, codigomovim, 
										creditos_cnt, creditos_cto, creditos_mto, debitos_cnt, debitos_cto, debitos_mto, saldo_cnt, saldo_cto, saldo_mto, registro
									from	tbl_producto_movimiento
									where	agenregistro = @agenregistro and prodregistro = @prodregistro and numfechaing between @years_months_01 and @years_months_02

									fetch next from tmpFecha into @years_months
								end
							close tmpFecha
							deallocate tmpFecha
							fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
						end
					close tmpProd
					fetch next from tmpSuc into @agenregistro
				end
			close tmpSuc
			deallocate tmpSuc
			deallocate tmpProd
			set @strComando01 = ''''
			if @strTipoMovim = ''''
				begin
					set @strComando01 = ''select * from #tmpProd order by agencodigo, prodcodigo, mov_registro''
				end
			else
				begin
					set @strComando01 = ''select * from #tmpProd where codigomovim in '' + @strTipoMovim +'' order by agencodigo, prodcodigo, mov_registro''
				end
			exec (@strComando01)
		end
	else if @intTipoReporte = 2					-- Generar Movimientos
		begin
			select @fecha_inicial = convert(numeric(8), @strFecIni)
			select @fecha_final = convert(numeric(8), @strFecFin)
			create table #tmpProd01
			(
				years_months	int,
				years		smallint,
				months		tinyint,
				agencodigo	varchar(12),
				agendescrip	varchar(60),
				clascodigo	varchar(12),
				clasdescrip	varchar(60),
				prodregistro	smallint,
				prodcodigo   	varchar(12), 
				proddescrip	varchar(60),
				unidcodigo	varchar(12),
				codigomovim	varchar(3),
				cantidad	numeric(12,2),
				monto		numeric(12,2),
				documento	varchar(12),
				fechaingreso	varchar(12),
				cuentadebe	varchar(15),
				cuentahaber	varchar(15),
				registro		bigint
			)
			open tmpSuc
			fetch next from tmpSuc into @agenregistro
			while @@fetch_status = 0
				begin
					select @agencodigo = ''''
					select @agendescrip = ''''
					select @agencodigo = codigo, @agendescrip = descripcion from prm_Agencias where registro = @agenregistro
					open tmpProd
					fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
					while @@fetch_status = 0
						begin
							if @strDocumento = ''''
								begin
									insert into #tmpProd01 select substring(convert(char(8), numfechaing), 1, 6), substring(convert(char(8), numfechaing), 1, 4), substring(convert(char(8), numfechaing), 5, 2), 
										@agencodigo, @agendescrip, @clascodigo, @clasdescrip, @prodregistro, @prodcodigo, @proddescrip, @unidcodigo, codigomovim, 
										 ''cantidad'' = 
											case 
												when debitos_cnt != 0 then debitos_cnt
												else creditos_cnt
											end,
										 ''monto'' = 
											case 
												when debitos_cnt != 0 then debitos_mto
												else creditos_mto
											end,
										numdocumento, fechaingreso, cuenta_debe, cuenta_haber, registro
									from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro and numfechaing between @fecha_inicial and @fecha_final
								end
							else
								begin
									insert into #tmpProd01 select substring(convert(char(8), numfechaing), 1, 6), substring(convert(char(8), numfechaing), 1, 4), substring(convert(char(8), numfechaing), 5, 2), 
										@agencodigo, @agendescrip, @clascodigo, @clasdescrip, @prodregistro, @prodcodigo, @proddescrip, @unidcodigo, codigomovim, 
										 ''cantidad'' = 
											case 
												when debitos_cnt != 0 then debitos_cnt
												else creditos_cnt
											end,
										 ''monto'' = 
											case 
												when debitos_cnt != 0 then debitos_mto
												else creditos_mto
											end,
										numdocumento, fechaingreso, cuenta_debe, cuenta_haber, registro
									from tbl_producto_movimiento where agenregistro = @agenregistro and prodregistro = @prodregistro and numfechaing between @fecha_inicial and @fecha_final and numdocumento = @strDocumento
								end
							fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
						end
					close tmpProd
					fetch next from tmpSuc into @agenregistro
				end
			close tmpSuc
			deallocate tmpProd
			deallocate tmpSuc
			set @strComando01 = ''''
			if @strTipoMovim = ''''
				begin
					set @strComando01 = ''select * from #tmpProd01 order by agencodigo, clascodigo, prodcodigo, codigomovim, registro''
				end
			else
				begin
					set @strComando01 = ''select * from #tmpProd01 where codigomovim in '' + @strTipoMovim +'' order by agencodigo, clascodigo, prodcodigo, codigomovim, registro''
				end
			exec (@strComando01)
		end
	else if @intTipoReporte = 3				--- Generar Costos
		begin
			set @strComando01 = ''select a.codigo CodSuc, a.descripcion Sucursal, c.codigo CodClase, c.descripcion Clase, p.codigo CodProd, p.descripcion Producto, sum(m.debitos_cnt) Deb_Cnt, sum(m.debitos_mto) Debitos_Mto, ''
			set @strComando02 = ''sum(m.creditos_cnt) Creditos_Cnt, sum(m.creditos_mto) Creditos_Mto, sum(m.debitos_cnt) - sum(m.creditos_cnt) Cant_Total, sum(m.debitos_mto) - sum(m.creditos_mto) Mto_Total ''
			set @strComando03 = ''from tbl_producto_movimiento m, prm_productos p, prm_agencias a, prm_clases c ''
			set @strComando04 = ''where m.agenregistro = a.registro and m.prodregistro = p.registro and p.regclase = c.registro and M.Origen = 1 ''
			set @strComando05 = ''group by a.codigo, a.descripcion, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			set @strComando06 = ''order by a.codigo, a.descripcion, c.codigo, c.descripcion, p.codigo, p.descripcion ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strProductos + @strAgencias + @strClases + @strSubClases + @strProveedores + @strFecIni + @strFecFin + @strComando05 + @strComando06)
		end
	else if @intTipoReporte = 4
		-- Generar Reporte Productos sin Movimientos
		begin
			select @fecha_inicial = convert(numeric(8), @strFecIni)
			select @fecha_final = convert(numeric(8), @strFecFin)
			create table #tmpProd02
			(
				years_months	int,
				years		smallint,
				months		tinyint,
				agencodigo	varchar(12),
				agendescrip	varchar(60),
				prodregistro	smallint,
				prodcodigo   	varchar(12), 
				proddescrip	varchar(60),
				clascodigo	varchar(12),
				clasdescrip	varchar(60),
				unidcodigo	varchar(12),
				cantidad	numeric(12,2),
				costo		numeric(12,2),
				monto		numeric(12,2)
			)
			open tmpSuc
			fetch next from tmpSuc into @agenregistro
			while @@fetch_status = 0
				begin
					select @agencodigo = ''''
					select @agendescrip = ''''
					select @agencodigo = codigo, @agendescrip = descripcion from prm_Agencias where registro = @agenregistro
					open tmpProd
					fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
					while @@fetch_status = 0
						begin
							select @moviregistro = 0
							select @moviregistro = isnull(max(registro),0) from tbl_producto_movimiento 
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
									and numfechaing < @fecha_final
							if @moviregistro > 0
								begin
									select @prodcantidad = saldo_cnt from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
											and registro = @moviregistro
									if @prodcantidad <> 0
										begin
											select @moviregistro = 0
											select @moviregistro = isnull(max(registro),0) from tbl_producto_movimiento 
												where agenregistro = @agenregistro and prodregistro = @prodregistro 
													and numfechaing between @fecha_inicial and @fecha_final
											if @moviregistro = 0
											begin
												select @prodcantidad = 0
												select @prodcosto    = 0
												select @prodmonto    = 0
												select @years_months_01 = 0
												select @unidcodigo02 = ''''
												select @unidcodigo02 = codigo from prm_unidades where descripcion = @unidcodigo
												insert into #tmpProd02 values (0, 0, 0, @agencodigo, @agendescrip, @prodregistro, 
													@prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo02, 
													@prodcantidad, @prodcosto, @prodmonto)
											end
										end
								end
							fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
						end
					close tmpProd
					fetch next from tmpSuc into @agenregistro
				end
			close tmpSuc
			deallocate tmpProd
			deallocate tmpSuc
			select * from #tmpProd02 where cantidad = 0 order by agencodigo, clascodigo, proddescrip
		end
	else if @intTipoReporte = 5 or @intTipoReporte = 6 or @intTipoReporte = 7 or @intTipoReporte = 8 or @intTipoReporte = 11 or @intTipoReporte = 12 or @intTipoReporte = 13 or @intTipoReporte = 14 or @intTipoReporte = 15 or @intTipoReporte = 16 or @intTipoReporte = 17
		-- Generar Reporte (COR y USD)                     --- Listado Existencias     --- Productos Cant 0      -- Reporte para Revision	-- Reporte Consolidado Agencias (COR y USD)	-- Inventario Laboratorio	-- Verificacion Costos	--- Clases en Agencias (COR y USD)
		begin
			select @fecha_inicial = convert(numeric(8), @strFecIni)
			select @fecha_final = convert(numeric(8), @strFecFin)
			create table #tmpProd03
			(
				years_months	int,
				years		smallint,
				months		tinyint,
				agencodigo	varchar(12),
				agendescrip	varchar(60),
				prodregistro	smallint,
				prodcodigo   	varchar(12), 
				proddescrip	varchar(60),
				clascodigo	varchar(12),
				clasdescrip	varchar(60),
				unidcodigo	varchar(12),
				cantidad	numeric(12,2),
				costo		numeric(12,2),
				monto		numeric(12,2),
				codigomovim	varchar(3),
				descripmovim	varchar(50),
				provdescrip	varchar(60)
			)
			create clustered index #tmprod03ix on #tmpProd03 (agencodigo, prodregistro)
			open tmpSuc
			fetch next from tmpSuc into @agenregistro
			while @@fetch_status = 0
				begin
					select @agencodigo = ''''
					select @agendescrip = ''''
					select @agencodigo = codigo, @agendescrip = descripcion from prm_Agencias where registro = @agenregistro
					open tmpProd
					fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
					while @@fetch_status = 0
						begin
							select @moviregistro = 0
							select @moviregistro = isnull(max(registro),0) from tbl_producto_movimiento 
								where agenregistro = @agenregistro and prodregistro = @prodregistro and numfechaing <= @fecha_final
							if @moviregistro > 0
								begin
									select @prodcantidad = 0
									select @prodcosto    = 0
									select @prodmonto    = 0
									select @years_months_01 = 0
									select @provdescrip = ''''
									select @unidcodigo02 = ''''
									select @unidcodigo02 = codigo from prm_unidades where descripcion = @unidcodigo
									select @provdescrip = v.nombre from prm_productos p, prm_proveedores v where p.regproveedor = v.registro
										and p.registro = @prodregistro
									if @intTipoReporte = 6 or @intTipoReporte = 10 or @intTipoReporte = 13
										begin
											select @years_months_01 = numfechaing, @prodcantidad = saldo_cnt, @prodcosto = (saldo_cto / t.tipo_cambio), @prodmonto = (saldo_mto / t.tipo_cambio), @codigomovim = codigomovim
												from tbl_producto_movimiento m, prm_tipocambio t where agenregistro = @agenregistro and prodregistro = @prodregistro and registro = @moviregistro and m.numfechaing = t.numfecha
										end
									else
										begin
											select @years_months_01 = numfechaing, @prodcantidad = saldo_cnt, @prodcosto = saldo_cto, @prodmonto = saldo_mto, @codigomovim = codigomovim from tbl_producto_movimiento
												where agenregistro = @agenregistro and prodregistro = @prodregistro and registro = @moviregistro
										end
									select @descripmovim = ''''
									select @descripmovim = descripcion from prm_tipomov where codigo = @codigomovim
									if @intTipoReporte >= 5 or @intTipoReporte <= 8
										begin
											insert into #tmpProd03 values (substring(convert(char(8), @years_months_01), 1, 6), substring(convert(char(8), @years_months_01), 1, 4), substring(convert(char(8), @years_months_01), 5, 2), 
												@agencodigo, @agendescrip, @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo02, @prodcantidad, @prodcosto, @prodmonto, @codigomovim, @descripmovim,@provdescrip)
										end
									else
										begin
											insert into #tmpProd03 values (substring(convert(char(8), @years_months_01), 1, 6), substring(convert(char(8), @years_months_01), 1, 4), substring(convert(char(8), @years_months_01), 5, 2), 
												@agencodigo, @agendescrip, @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo, @prodcantidad, @prodcosto, @prodmonto, @codigomovim, @descripmovim,@provdescrip)
										end
								end
							fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
						end
					close tmpProd
					fetch next from tmpSuc into @agenregistro
				end
			close tmpSuc
			deallocate tmpProd
			deallocate tmpSuc
			if @intTipoReporte = 5 or @intTipoReporte = 6 or @intTipoReporte = 7
				begin
					select * from #tmpProd03 where cantidad != 0 order by agencodigo, clascodigo, proddescrip
				end
			else if @intTipoReporte = 8
				begin
					select * from #tmpProd03 where cantidad <= @dblCantidad order by agencodigo, clascodigo, proddescrip
				end
			else if @intTipoReporte = 11
				begin
					select agencodigo, agendescrip, clascodigo, clasdescrip, prodcodigo, proddescrip, unidcodigo, '''' as cantidad, '''' as observaciones from #tmpProd03 where cantidad != 0 order by agencodigo, clascodigo, proddescrip
				end
			else if @intTipoReporte = 12 or @intTipoReporte = 13
				begin
					declare tmpCursor cursor for
						select prodregistro, cantidad from #tmpProd03 where agencodigo = 3
					open tmpCursor
					fetch next from tmpCursor into @prodregistro, @saldo_cant
					while @@fetch_status = 0
						begin
							select @prodcosto = 0
							select @prodmonto = 0
							select @prodcosto = costo from #tmpProd03 where agencodigo = 1 and prodregistro = @prodregistro
							select @prodmonto = @prodcosto * @saldo_cant
							update #tmpProd03 set costo = @prodcosto, monto = @prodmonto where agencodigo = 3 and prodregistro = @prodregistro
							fetch next from tmpCursor into @prodregistro, @saldo_cant		
						end
					close tmpCursor
					deallocate tmpCursor
					create table #tmpProd04
					(
						agencodigo	varchar(12),
						agendescrip	varchar(80),
						monto		numeric(12,2),
						total		numeric(12,2)
					)
					insert into #tmpProd04 select agencodigo, agendescrip, sum(monto), 0 from #tmpProd03 
						group by agencodigo, agendescrip order by agencodigo, agendescrip

					set @prodcosto = 0
					set @prodmonto = 0
					select @prodcosto = monto from #tmpProd04 where agencodigo = 1
					select @prodmonto = monto from #tmpProd04 where agencodigo = 3				
					update #tmpProd04 set monto = (@prodcosto - @prodmonto) where agencodigo = 4

					set @prodcantidad = 0
					select @prodcantidad = sum(monto) from #tmpProd04 where agencodigo <> 1
					update #tmpProd04 set total = @prodcantidad
					select * from #tmpProd04 order by agencodigo
					drop table #tmpProd04
				end
			else if @intTipoReporte = 14
				begin
					select provdescrip, sum(monto) as monto from #tmpProd03 where monto <> 0 group by provdescrip order by sum(monto) desc
				end
			else if @intTipoReporte = 15
				begin
					create table #tmp05
					(
						clascodigo	varchar(12),
						clasdescrip	varchar(50),
						prodcodigo	varchar(12),
						proddescrip	varchar(50),
						agen_01_cnt	numeric(12,2),
						agen_01_cto	numeric(12,2),
						agen_01_mto	numeric(12,2),
						agen_03_cnt	numeric(12,2),
						agen_03_mto	numeric(12,2),
						agen_04_cnt	numeric(12,2),
						agen_04_mto	numeric(12,2)
					)
					declare tmpCursor cursor for
						select prodregistro, clascodigo, clasdescrip, prodcodigo, proddescrip, cantidad, costo, monto from #tmpProd03 where agencodigo = 1
					open tmpCursor
					fetch next from tmpCursor into @prodregistro, @clascodigo, @clasdescrip, @prodcodigo, @proddescrip, @saldo_cant, @saldo_csto, @saldo_mnto
					while @@fetch_status = 0
						begin
							set @agen03cant = 0
							set @agen03mnto = 0
							set @agen04cant = 0
							set @agen04mnto = 0
							select @agen03cant = cantidad from #tmpProd03 where agencodigo = 3 and prodregistro = @prodregistro
							select @agen04cant = cantidad from #tmpProd03 where agencodigo = 4 and prodregistro = @prodregistro
							select @agen03mnto = @agen03cant * @saldo_csto
							select @agen04mnto = @agen04cant * @saldo_csto
							insert into #tmp05 values (@clascodigo, @clasdescrip, @prodcodigo, @proddescrip, @saldo_cant, @saldo_csto,  @saldo_mnto,  @agen03cant, @agen03mnto, @agen04cant, @agen04mnto)
							fetch next from tmpCursor into @prodregistro, @clascodigo, @clasdescrip, @prodcodigo, @proddescrip, @saldo_cant, @saldo_csto, @saldo_mnto
						end
					close tmpCursor
					select * from #tmp05 where agen_01_cnt <> 0 or agen_03_cnt <> 0 or agen_04_cnt <> 0 order by clascodigo, proddescrip
					drop table #tmpProd05
				end
			else if @intTipoReporte = 16 or @intTipoReporte = 17
				begin
					select a.registro, t.agencodigo, t.agendescrip, t.clascodigo, t.clasdescrip, sum(t.monto) as monto from #tmpProd03 t, prm_agencias a
						where t.agencodigo = a.codigo
						group by a.registro, t.agencodigo, t.agendescrip, t.clascodigo, t.clasdescrip
						order by a.registro, t.agencodigo, t.agendescrip, t.clascodigo, t.clasdescrip
				end
			drop table #tmpProd03
		end
	else if @intTipoReporte = 9 or @intTipoReporte = 10
		--- Clases y Movimientos (COR y USD)
		begin
			select @fecha_inicial = convert(numeric(8), @strFecIni)
			select @fecha_final = convert(numeric(8), @strFecFin)
			create table #tmpProd06
			(
				agencodigo	varchar(12),
				agendescrip	varchar(60),
				prodregistro	smallint,
				clascodigo	varchar(12),
				clasdescrip	varchar(60),
				codigomovim	varchar(3),
				descripmovim	varchar(50),
				montoCrd	numeric(12,2),
				montoDeb	numeric(12,2)
			)
			create clustered index #tmprod06ix on #tmpProd06 (agencodigo, prodregistro)
			open tmpSuc
			fetch next from tmpSuc into @agenregistro
			while @@fetch_status = 0
				begin
					open tmpProd
					fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
					while @@fetch_status = 0
						begin
							set @prodcantidad = 0
							set @prodmonto    = 0
							if @intTipoReporte = 10
								begin
									insert into #tmpProd06 select a.codigo, a.descripcion, p.registro, c.codigo, c.descripcion, t.codigo, t.descripcion, 
										m.creditos_mto / b.tipo_cambio, m.debitos_mto / b.tipo_cambio from prm_agencias a, prm_productos p, 
										prm_clases c, prm_tipomov t, tbl_producto_movimiento m, prm_tipocambio b where a.registro = m.agenregistro and 
										p.registro = m.prodregistro and p.regclase = c.registro and m.codigomovim = t.codigo and m.numfechaing = b.numfecha 
										and m.agenregistro = @agenregistro and m.prodregistro = @prodregistro and m.numfechaing 
										between @fecha_inicial and @fecha_final
								end
							else
								begin
									insert into #tmpProd06 select a.codigo, a.descripcion, p.registro, c.codigo, c.descripcion, t.codigo, t.descripcion, 
										m.creditos_mto, m.debitos_mto from prm_agencias a, prm_productos p, prm_clases c, prm_tipomov t, 
										tbl_producto_movimiento m, prm_tipocambio b where a.registro = m.agenregistro and p.registro = m.prodregistro and 
										p.regclase = c.registro and m.codigomovim = t.codigo and m.numfechaing = b.numfecha and 
										m.agenregistro = @agenregistro and m.prodregistro = @prodregistro and m.numfechaing between 
										@fecha_inicial and @fecha_final
								end
							fetch next from tmpProd into @prodregistro, @prodcodigo, @proddescrip, @clascodigo, @clasdescrip, @unidcodigo
						end
					close tmpProd
					fetch next from tmpSuc into @agenregistro
				end
			close tmpSuc
			deallocate tmpProd
			deallocate tmpSuc
			if @intTipoReporte = 9 or @intTipoReporte = 10
				begin
					select t.agencodigo, t.agendescrip, t.clascodigo, t.clasdescrip, t.codigomovim, t.descripmovim, sum(t.montoCrd) montoCrd, sum(t.montoDeb) montoDeb 
						from #tmpProd06 t
						group by t.agencodigo, t.agendescrip, t.clascodigo, t.clasdescrip, t.codigomovim, t.descripmovim
						order by t.agencodigo, t.agendescrip, t.clascodigo, t.clasdescrip, t.codigomovim, t.descripmovim
				end
			drop table #tmpProd06
		end
	else if @intTipoReporte = 18 or @intTipoReporte = 19
		--- Productos, Ultima Compra, Facturas y Movimientos Acumulados
		begin
			set @strAgencias = ''''
			set @strAgencias = ''(''
			declare tmpA cursor for
				select registro from prm_agencias where estado = 0 and registro not in (3,4)
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @strAgencias = @strAgencias + convert(varchar(3), @agenregistro) + '',''
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			set @strAgencias = substring(@strAgencias, 1, len(@strAgencias) - 1) + '')''
			set @fecha_final = convert(numeric(8), @strFecFin)
			create table #tmpProdReg11
			(
				reg_clase		tinyint,	
				cod_clase		varchar(12),
				des_clase		varchar(60),
				reg_producto	smallint,	
				cod_producto	varchar(12),
				des_producto	varchar(60),
				cant_ant_cmp	numeric(18, 2),
				fecha_utl_cmp	varchar(12),
				cant_ult_cmp	numeric(18, 2),
				cant_facturas	numeric(18, 2),
				totales			numeric(18, 2),
				exist_actual	numeric(18, 2)
			)
			create clustered index #ix_tmpProdReg11 on #tmpProdReg11(reg_producto)
			create table #tmpProdReg11F
			(
				numfechaing		numeric(8)
			)
			create table #tmpProdReg11R
			(
				registro		bigint
			)
			create table #tmpProdReg11C
			(
				cantidad		numeric(12, 2),
				fecha			varchar(12),
				numfechaing		numeric(8),
				numtiempo		int
			)
			if @strProductos <> ''''
				set @strProductos = replace(@strProductos, ''Registro'', ''P.Registro'')
			exec (''insert into #tmpProdReg11 select c.registro, c.codigo, c.descripcion, p.registro, 
				p.codigo, p.descripcion, 0, '''''''', 0, 0, 0, 0 from prm_Productos p, prm_Clases c where 
				p.regclase = c.registro and p.estado = 0 '' + @strProductos + @strClases + @strSubClases + 
				@strProveedores)
			declare tmpC cursor for
				select reg_producto from #tmpProdReg11
			open tmpC
			fetch next from tmpC into @prodregistro
			while @@fetch_status = 0
				begin
					open tmpA
					fetch next from tmpA into @agenregistro
					set @cantidad = 0
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @moviregistro = 0
							set @numfechaing = 0
							select @moviregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing <= convert(numeric(8), convert(varchar(8), getdate(), 112))
							if @moviregistro <> 0
								select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
									where agenregistro = @agenregistro and prodregistro = @prodregistro
									and registro = @moviregistro
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpProdReg11 set exist_actual = @cantidad where reg_producto = @prodregistro
						end
					set @cantidad = 0
					set @fecha_ult_cmp = ''''
					set @moviregistro = 0
					set @numfechaing = 0
					set @numtiempo = 0
					truncate table #tmpProdReg11F
					exec(''insert into #tmpProdReg11F select isnull(max(numfechaing), 0) from 
						tbl_producto_movimiento where agenregistro in ''+ @strAgencias +'' and 
						prodregistro = ''+ @prodregistro +'' and numfechaing <= ''+ @fecha_final +'' and 
						codigomovim = ''''EC'''''')
					select @numfechaing = numfechaing from #tmpProdReg11F
					if @numfechaing <> 0
						begin
							truncate table #tmpProdReg11R
							exec(''insert into #tmpProdReg11R select isnull(max(registro), 0) from 
								tbl_producto_movimiento where agenregistro in ''+ @strAgencias +'' and 
								prodregistro = ''+ @prodregistro +'' and numfechaing = ''+ @numfechaing +'' 
								and codigomovim = ''''EC'''''')
							select @moviregistro = registro from #tmpProdReg11R
							if @moviregistro <> 0
									begin
										truncate table #tmpProdReg11C
										exec(''insert into #tmpProdReg11C select isnull(creditos_cnt, 0), 
											fechaingreso, isnull(numfechaing, 0), isnull(numtiempo, 0) 
											from tbl_producto_movimiento where agenregistro in 
											''+ @strAgencias +'' and prodregistro = ''+ @prodregistro +'' 
											and numfechaing = ''+ @numfechaing +'' and registro = ''+ 
											@moviregistro +'' and codigomovim = ''''EC'''''')
										select @cantidad = cantidad, @fecha_ult_cmp = fecha, @fecha_inicial 
											= numfechaing, @numtiempo = numtiempo from #tmpProdReg11C
										update #tmpProdReg11 set fecha_utl_cmp = @fecha_ult_cmp,
											cant_ult_cmp = @cantidad where reg_producto = @prodregistro
										set @cantidad = 0
										set @cantidad_fact = 0
										open tmpA
										fetch next from tmpA into @agenregistro
										while @@fetch_status = 0
											begin
												set @cant_final = 0
												set @cant_fnl_fact = 0
												set @moviregistro = 0
												set @numfechaing = 0
												select @numfechaing = isnull(max(numfechaing), 0) from 
													tbl_producto_movimiento where agenregistro = 
													@agenregistro and prodregistro = @prodregistro and
													numfechaing < @fecha_inicial and registro <> @moviregistro
												if @numfechaing <> 0
													begin
														select @moviregistro = isnull(max(registro), 0) from 
															tbl_producto_movimiento where agenregistro = 
															@agenregistro and prodregistro = @prodregistro 
															and numfechaing = @numfechaing and registro <> 
															@moviregistro
														if @moviregistro <> 0
															select @cant_final = isnull(saldo_cnt, 0) from 
																tbl_producto_movimiento where agenregistro = 
																@agenregistro and prodregistro = 
																@prodregistro and numfechaing = @numfechaing
																and registro = @moviregistro
													end
												set @cantidad = @cantidad + @cant_final
												select @cant_fnl_fact = isnull(sum(debitos_cnt), 0) from 
													tbl_producto_movimiento where agenregistro = @agenregistro and 
													numfechaing > @fecha_inicial and prodregistro = 
													@prodregistro and codigomovim = ''SF'' and origen = 1
												set @cantidad_fact = @cantidad_fact + @cant_fnl_fact
												set @cant_fnl_fact = 0
												select @cant_fnl_fact = isnull(sum(creditos_cnt), 0) from 
													tbl_producto_movimiento where agenregistro = @agenregistro and 
													numfechaing > @fecha_inicial and prodregistro = 
													@prodregistro and codigomovim = ''AF'' and origen = 1
												set @cantidad_fact = @cantidad_fact - @cant_fnl_fact
												fetch next from tmpA into @agenregistro
											end
										close tmpA
										update #tmpProdReg11 set cant_ant_cmp = @cantidad, cant_facturas =  
											@cantidad_fact where reg_producto = @prodregistro
									end
						end
					fetch next from tmpC into @prodregistro
				end
			close tmpC
			deallocate tmpC
			deallocate tmpA
			update #tmpProdReg11 set totales = (cant_ant_cmp + cant_ult_cmp)
			if @intTipoReporte = 18
				select * from #tmpProdReg11 where cant_ant_cmp <> 0 or cant_ult_cmp <> 0 or 
					exist_actual <> 0 order by cod_clase, des_producto
			else if @intTipoReporte = 19
				select * from #tmpProdReg11 where exist_actual <> 0 order by cod_clase, des_producto
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_PrcCierreAnual]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_PrcCierreAnual]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_PrcCierreAnual] @intTipo tinyint, @agenregistro tinyint, @prodcodigo varchar(12), 
		@proddescrip varchar(80), @produnidad varchar(12), @cantidad numeric(18,2), @costo numeric(18,2),
		@monto numeric(18,2), @doccierre varchar(12), @docinicial varchar(12), @strnumfechaing varchar(12), 
		@lngnumfechaing numeric(8), @lngnumtiempo numeric(6), @intExcelCosto as tinyint

as

declare	@prodregistro	smallint,
		@movimregistro	bigint,
		@strmovimiento	varchar(3),
		@cntfisico		numeric(18, 2)

begin
	set nocount on
	if @intTipo = 1
		begin
				if @intExcelCosto = 0
					begin
						set @costo = 0
						set @prodregistro = 0
						select @prodregistro = isnull(registro, 0) from prm_productos where 
							codigo = @prodcodigo
						if @prodregistro <> 0
							begin
								set rowcount 1
								select @costo = creditos_cto from tbl_producto_movimiento where 
										agenregistro = @agenregistro and prodregistro = @prodregistro
										and codigomovim = ''EC'' order by registro desc
								set rowcount 0
							end
					end
				insert into tmp_CierreAnual values (@agenregistro, @prodcodigo, @proddescrip, @produnidad, 
					@cantidad, @costo, @monto, @doccierre, @docinicial)
				select @costo
		end
	else if @intTipo = 2
		begin
			truncate table tmp_CierreAnualDif
			set @agenregistro = 0
			set @doccierre = ''''
			set @movimregistro = 0
			set @cantidad = 0
			set @strmovimiento = ''''
			set rowcount 1
			select @agenregistro = agenregistro, @doccierre = doccierre from tmp_CierreAnual
			set rowcount 0
			declare tmpC cursor for
				select distinct prodregistro from tbl_producto_movimiento where 
					agenregistro = @agenregistro and saldo_cnt <> 0 order by prodregistro
			open tmpC
			fetch next from tmpC into @prodregistro
			while @@fetch_status = 0
				begin
					set @movimregistro = 0
					select @movimregistro = isnull(max(registro), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro
					if @movimregistro <> 0
						begin
							select @cantidad = saldo_cnt, @costo = saldo_cto, @monto = saldo_mto from
								tbl_producto_movimiento where agenregistro = @agenregistro and 
								prodregistro = @prodregistro and registro = @movimregistro
							if @cantidad < 0
									set @strmovimiento = ''AJE''
							else if @cantidad > 0
									set @strmovimiento = ''AJS''
							set @cantidad = abs(@cantidad)
							set @costo = abs(@costo)
							set @monto = abs(@monto)
							if @cantidad <> 0
								begin
									set @prodcodigo = ''''
									select @prodcodigo = codigo from prm_productos where 
										registro = @prodregistro
									if @prodcodigo <> ''''
										begin
											set @cntfisico = 0
											select @cntfisico = cantidad from tmp_CierreAnual
												where prodcodigo = @prodcodigo
											if @@ROWCOUNT != 0 and @cantidad <> @cntfisico
												insert into tmp_CierreAnualDif values (@agenregistro,
													@prodcodigo, @cntfisico, @cantidad)
										end
									exec sp_IngProductoMovim 0, @lngnumfechaing, @lngnumtiempo, 
										@agenregistro, @prodregistro, @strnumfechaing, 
										@doccierre, @strmovimiento, ''COR'', @monto, @cantidad, 
										@costo, @monto, '''', '''', 0, 0, 0, 0, 0
								end
						end
					fetch next from tmpC into @prodregistro
				end
			close tmpC
			deallocate tmpC
		end
	else if @intTipo = 3
		begin
				declare tmpC cursor for
					select distinct c.agenregistro, p.registro, c.cantidad, c.costo, c.monto, 
						c.docinicial from tmp_cierreanual c, prm_productos p where 
						c.prodcodigo = p.codigo
				open tmpC
				fetch next from tmpC into @agenregistro, @prodregistro, @cantidad, @costo, @monto,
						@docinicial
				while @@fetch_status = 0
					begin
						exec sp_IngProductoMovim 0, @lngnumfechaing, @lngnumtiempo, @agenregistro, 
							@prodregistro, @strnumfechaing, @docinicial, ''II'', ''COR'', @monto, 
							@cantidad, @costo, @monto, '''', '''', 0, 0, 0, 0, 0
						fetch next from tmpC into @agenregistro, @prodregistro, @cantidad, @costo, 
							@monto, @docinicial
					end
				close tmpC
				deallocate tmpC
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngDepartamentos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngDepartamentos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngDepartamentos] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @strCodCorto as char(1), @intEstado as tinyint, 
			@intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Departamentos where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Departamentos where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Departamentos
					insert into prm_Departamentos values (@lngRegistro, @strCodigo, @strDescripcion, @strCodCorto, @intEstado)
				end
			else
				begin
					update prm_Departamentos set descripcion = @strDescripcion, codcorto = @strCodCorto, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngEmpaques]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngEmpaques]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngEmpaques] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Empaques where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Empaques where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Empaques
					insert into prm_Empaques values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_Empaques set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngNegocios]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngNegocios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngNegocios] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @intEstado as tinyint, @intVerificar as tinyint, 
				@intDias as tinyint, @intNegociable as tinyint, @intMaximo as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Negocios where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Negocios where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Negocios
					insert into prm_Negocios values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado, @intDias, @intNegociable, @intMaximo)
				end
			else
				begin
					update prm_Negocios set descripcion = @strDescripcion, estado = @intEstado, dias = @intDias, negociable = @intNegociable, maximo = @intMaximo where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngLetrasCambio]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngLetrasCambio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngLetrasCambio] @strOrden varchar(40), @strNombre varchar(40), @dblTasa numeric(5,2)

As

begin
	set nocount on
	truncate table prm_LetrasCambio
	insert into prm_LetrasCambio values (@strOrden, @strNombre, @dblTasa)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProveedores]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProveedores]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngProveedores] @lngRegistro as tinyint, @strCodigo as varchar(12), @strNombre as varchar(50), @intTipo as tinyint, @intEstado as tinyint, 
				@intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Proveedores where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Proveedores where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Proveedores
					insert into prm_Proveedores values (@lngRegistro, @strCodigo, @strNombre, @intTipo, @intEstado)
				end
			else
				begin
					update prm_Proveedores set nombre = @strNombre, tipo = @intTipo, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPorcentajes]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPorcentajes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngPorcentajes] @dblIva as numeric(8,2), @dblRet as numeric(8,2), @intDias as tinyint
As

begin
	set nocount on
	truncate table prm_Porcentajes
	insert into prm_Porcentajes values (@dblIva, @dblRet, @intDias)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngBitacora]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngBitacora]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngBitacora] @strTabla as tinyint, @lngRegistro as int, @strFecha as varchar(12), @strTiempo as varchar(12), @strCampo as varchar(15), 
				@strAntes as varchar(65), @strDespues as varchar(65), @lngFecha as int, @lngTiempo as int, @strUsuario as varchar(12), 
				@lngRegUsuario as tinyint
As

begin
	set nocount on
	insert into tbl_Bitacora values (@strTabla, @lngRegistro, @strFecha, @strTiempo, @strCampo, @strAntes, @strDespues, @lngFecha, @lngTiempo, @strUsuario, @lngRegUsuario)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUnidades]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUnidades]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngUnidades] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Unidades where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Unidades where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Unidades
					insert into prm_Unidades values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_Unidades set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUsuariosPermisos]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUsuariosPermisos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngUsuariosPermisos] @strCodigo as varchar(12), @intPermiso as tinyint

As

declare	@lngRegistro as smallint

begin
	set nocount on
	set @lngRegistro = 0
	select @lngRegistro = registro from prm_usuarios where codusuario = @strCodigo
	if @intPermiso = 254
		begin
			delete from prm_usuariospermisos where cliregistro = @lngRegistro
		end
	else
		insert into prm_usuariospermisos values (@lngRegistro, @intPermiso)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngUsuarios]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngUsuarios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngUsuarios] @lngRegistro as tinyint, @strCodigo as varchar(12), @strNombre as varchar(30), @strApellido as varchar(30), @strDireccion as varchar(100), 
		@strTelefono as varchar(16), @strCargo as varchar(30), @strClave as varchar(12), @intEstado as tinyint, @intImpresora as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codusuario from prm_Usuarios where codusuario = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codusuario from prm_Usuarios where codusuario = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Usuarios
					insert into prm_Usuarios values (@lngRegistro, @strCodigo, @strNombre, @strApellido, @strDireccion, @strTelefono, @strCargo, @strClave, 
						@intEstado, @intImpresora)
				end
			else
				begin
					update prm_Usuarios set nombre = @strNombre, apellido = @strApellido, Direccion = @strDireccion, Telefono = @strTelefono, Cargo = @strCargo, 
						Clave = @strClave, estado = @intEstado, impresora = @intImpresora where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngAgencias]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngAgencias]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngAgencias] @lngRegistro as tinyint, @strCodigo as tinyint, @strDescripcion as varchar(30), @intTraslado as tinyint, @intDefecto as tinyint, 
			@intEstado as tinyint, @intFactura as tinyint, @intFechaCambio as int, @intDepartamento as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Agencias where codigo = @strCodigo
		end
	else
		begin
			if @intDefecto = 0
				begin
					update prm_Agencias set defecto = 1
				end
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Agencias where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					set @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Agencias
					insert into prm_Agencias values (@lngRegistro, @strCodigo, @strDescripcion, @intTraslado, @intDefecto, @intEstado, @intFactura, 0, @intDepartamento)
				end
			else
				begin
					update prm_Agencias set descripcion = @strDescripcion, traslados = @intTraslado, defecto = @intDefecto, estado = @intEstado,
						facturas = @intFactura, fechacambiofactura = @intFechaCambio, deptregistro = @intDepartamento where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptPresupuestos2]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptPresupuestos2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_RptPresupuestos2] @intTipoReporte tinyint, @intYears smallint, @strVendedor varchar(255),
		@strClases varchar(255), @strSubClases varchar(255), @strLaboratorios varchar(255), @intFecha1 numeric(8), 
		@intFecha2 numeric(8), @strVendedor2 varchar(255)

as

declare	@vendregistro	tinyint, 
		@prodregistro	smallint,
		@regclase		tinyint, 
		@meses			tinyint, 
		@valor			numeric(18, 2),
		@strComando01	varchar(200),
		@strComando02	varchar(200),
		@strComando03	varchar(200),
		@strComando04	varchar(200),
		@strComando05	varchar(200),
		@strComando06	varchar(200),
		@strComando07	varchar(200),
		@strComando08	varchar(200),
		@strComando09	varchar(200),
		@strComando10	varchar(200),
		@strComando11	varchar(200),
		@strComando12	varchar(200),
		@strComando13	varchar(200),
		@strComando14	varchar(200),
		@strComando15	varchar(200),
		@strComando16	varchar(200),
		@strComando17	varchar(200),
		@strComando18	varchar(200),
		@strComando19	varchar(200),
		@strComando20	varchar(200),
		@agenregistro	tinyint,
		@maxregistro	bigint,
		@cant_final		numeric(18,2),
		@cantidad		numeric(18,2),
		@numfechaing	numeric(8)

begin
	set nocount on
	set @strComando01 = ''''
	set @strComando02 = ''''
	set @strComando03 = ''''
	set @strComando04 = ''''
	set @strComando05 = ''''
	set @strComando06 = ''''
	set @strComando07 = ''''
	set @strComando08 = ''''
	set @strComando09 = ''''
	set @strComando10 = ''''
	set @strComando11 = ''''
	set @strComando12 = ''''
	set @strComando13 = ''''
	set @strComando14 = ''''
	set @strComando15 = ''''
	set @strComando16 = ''''
	set @strComando17 = ''''
	set @strComando18 = ''''
	set @strComando19 = ''''
	set @strComando20 = ''''
	if @intTipoReporte = 10 or @intTipoReporte = 11 or @intTipoReporte = 12 or @intTipoReporte = 13
		begin
			create table #tmpP
			(
				vendregistro		tinyint,
				codigo_vendedor		varchar(12),
				vendedor			varchar(60),
				claseregistro		tinyint,
				codigo_clase		varchar(12),
				clase				varchar(60),
				enero_presup		numeric(18, 2),
				enero_ventas		numeric(18, 2),
				enero_cumplim		numeric(18, 2),
				febrero_presup		numeric(18, 2),
				febrero_ventas		numeric(18, 2),
				febrero_cumplim		numeric(18, 2),
				marzo_presup		numeric(18, 2),
				marzo_ventas		numeric(18, 2),
				marzo_cumplim		numeric(18, 2),
				abril_presup		numeric(18, 2),
				abril_ventas		numeric(18, 2),
				abril_cumplim		numeric(18, 2),
				mayo_presup			numeric(18, 2),
				mayo_ventas			numeric(18, 2),
				mayo_cumplim		numeric(18, 2),
				junio_presup		numeric(18, 2),
				junio_ventas		numeric(18, 2),
				junio_cumplim		numeric(18, 2),
				julio_presup		numeric(18, 2),
				julio_ventas		numeric(18, 2),
				julio_cumplim		numeric(18, 2),
				agosto_presup		numeric(18, 2),
				agosto_ventas		numeric(18, 2),
				agosto_cumplim		numeric(18, 2),
				septiembre_presup	numeric(18, 2),
				septiembre_ventas	numeric(18, 2),
				septiembre_cumplim	numeric(18, 2),
				octubre_presup		numeric(18, 2),
				octubre_ventas		numeric(18, 2),
				octubre_cumplim		numeric(18, 2),
				noviembre_presup	numeric(18, 2),
				noviembre_ventas	numeric(18, 2),
				noviembre_cumplim	numeric(18, 2),
				diciembre_presup	numeric(18, 2),
				diciembre_ventas	numeric(18, 2),
				diciembre_cumplim	numeric(18, 2),
				total_presup		numeric(18, 2),
				total_ventas		numeric(18, 2),
				total_cumplim		numeric(18, 2),
			)
			create clustered index #ixtmpP on #tmpP (vendregistro)
			set @strComando01 = ''insert into #tmpP select v.registro vendregistro, v.codigo codigo_vendedor, v.nombre vendedor, c.registro claseregistro, c.codigo codigo_clase, c.descripcion clase, ''
			set @strComando02 = ''sum(r.prodprecio * r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
			set @strComando03 = ''sum(r.prodprecio * r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
			set @strComando04 = ''sum(r.prodprecio * r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
			set @strComando05 = ''sum(r.prodprecio * r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
			set @strComando06 = ''sum(r.prodprecio * r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
			set @strComando07 = ''sum(r.prodprecio * r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
			set @strComando08 = ''sum(r.prodprecio * r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
			set @strComando09 = ''sum(r.prodprecio * r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
			set @strComando10 = ''sum(r.prodprecio * r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
			set @strComando11 = ''sum(r.prodprecio * r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
			set @strComando12 = ''sum(r.prodprecio * r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
			set @strComando13 = ''sum(r.prodprecio * r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
			set @strComando14 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
			set @strComando15 = ''from tbl_presupuestos r, prm_clases c, prm_vendedores v, prm_productos p ''
			set @strComando16 = ''where r.vendregistro = v.registro and r.prodregistro = p.registro and p.regclase = c.registro ''
			set @strComando17 = ''group by v.registro, v.codigo, v.nombre, c.registro, c.codigo, c.descripcion ''
			set @strComando18 = ''order by v.registro, v.codigo, v.nombre, c.registro, c.codigo ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + 
				@strComando11 + @strComando12 + @strComando13 + @strComando14 + @strComando15 + 
				@strComando16 + ''and r.years = '' + @intYears + '' '' + @strVendedor + @strClases + 
				@strSubClases + @strLaboratorios + @strVendedor2 + @strComando17 + @strComando18)
			update #tmpP set total_presup = (enero_presup + febrero_presup + marzo_presup + abril_presup + mayo_presup +
				junio_presup + julio_presup + agosto_presup + septiembre_presup + octubre_presup + noviembre_presup + 
				diciembre_presup)
			create table #tmpV
			(
				vendregistro	tinyint,
				regclase		tinyint,
				months			tinyint,
				valor			numeric(18, 2)
			)
			create clustered index #ixtmpV on #tmpV (vendregistro, regclase, months)
			set @strComando01 = ''insert into #tmpV select e.vendregistro, p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.valor) valor ''
			set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos p, tbl_Presupuestos r where e.registro = d.registro and d.ProdRegistro = ''
			set @strComando03 = ''P.Registro and e.vendregistro = r.vendregistro and p.registro = r.prodregistro and d.prodregistro = r.prodregistro and e.status = 0 ''
			set @strComando04 = ''group by e.vendregistro, p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			set @strComando05 = ''order by e.vendregistro, p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			exec (@strComando01 + @strComando02 + @strComando03 + ''and e.numfechaing between '' + 
				@intFecha1 + ''and '' + @intFecha2 + '' '' + @strVendedor + @strClases + @strSubClases + 
				@strLaboratorios + @strVendedor2 + @strComando04 + @strComando05)
			declare tmpC cursor for
				select vendregistro, regclase, months, valor from #tmpV
			open tmpC
			fetch next from tmpC into @vendregistro, @regclase, @meses, @valor
			while @@fetch_status = 0
				begin
					if @meses = 1
						update #tmpP set enero_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 2
						update #tmpP set febrero_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 3
						update #tmpP set marzo_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 4
						update #tmpP set abril_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 5
						update #tmpP set mayo_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 6
						update #tmpP set junio_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 7
						update #tmpP set julio_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 8
						update #tmpP set agosto_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 9
						update #tmpP set septiembre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 10
						update #tmpP set octubre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 11
						update #tmpP set noviembre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 12
						update #tmpP set diciembre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					fetch next from tmpC into @vendregistro, @regclase, @meses, @valor
				end
			close tmpC
			deallocate tmpC
			update #tmpP set total_ventas = (enero_ventas + febrero_ventas + marzo_ventas + abril_ventas + mayo_ventas +
				junio_ventas + julio_ventas + agosto_ventas + septiembre_ventas + octubre_ventas + noviembre_ventas + 
				diciembre_ventas)
			update #tmpP set enero_cumplim = ((enero_ventas / enero_presup) * 100) where enero_presup <> 0
			update #tmpP set febrero_cumplim = ((febrero_ventas / febrero_presup) * 100) where febrero_presup <> 0
			update #tmpP set marzo_cumplim = ((marzo_ventas / marzo_presup) * 100) where marzo_presup <> 0
			update #tmpP set abril_cumplim = ((abril_ventas / abril_presup) * 100) where abril_presup <> 0
			update #tmpP set mayo_cumplim = ((mayo_ventas / mayo_presup) * 100) where mayo_presup <> 0
			update #tmpP set junio_cumplim = ((junio_ventas / junio_presup) * 100) where junio_presup <> 0
			update #tmpP set julio_cumplim = ((julio_ventas / julio_presup) * 100) where julio_presup <> 0
			update #tmpP set agosto_cumplim = ((agosto_ventas / agosto_presup) * 100) where agosto_presup <> 0
			update #tmpP set septiembre_cumplim = ((septiembre_ventas / septiembre_presup) * 100) where septiembre_presup <> 0
			update #tmpP set octubre_cumplim = ((octubre_ventas / octubre_presup) * 100) where octubre_presup <> 0
			update #tmpP set noviembre_cumplim = ((noviembre_ventas / noviembre_presup) * 100) where noviembre_presup <> 0
			update #tmpP set diciembre_cumplim = ((diciembre_ventas / diciembre_presup) * 100) where diciembre_presup <> 0
			update #tmpP set total_cumplim = ((total_ventas / total_presup) * 100) where total_presup <> 0
			create table #tmpVC
			(
				vendregistro	tinyint,
				vendcodigo		varchar(12),
				vendnombre		varchar(60),
				claseregistro	tinyint,
				clasecodigo		varchar(12),
				clasenombre		varchar(60),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			create clustered index #ixtmpVC on #tmpVC (vendregistro, claseregistro)
			insert into #tmpVC select vendregistro, codigo_vendedor, vendedor, claseregistro, codigo_clase, clase + '' PRESUP'', enero_presup, febrero_presup, marzo_presup, abril_presup, mayo_presup, junio_presup, julio_presup, agosto_presup, septiembre_presup, octubre_presup, noviembre_presup, diciembre_presup, total_presup from #tmpP
			insert into #tmpVC select vendregistro, codigo_vendedor, vendedor, claseregistro, codigo_clase, clase + '' VENTAS'', enero_ventas, febrero_ventas, marzo_ventas, abril_ventas, mayo_ventas, junio_ventas, julio_ventas, agosto_ventas, septiembre_ventas, octubre_ventas, noviembre_ventas, diciembre_ventas, total_ventas from #tmpP
			create table #tmpGTVC
			(
				clasenombre		varchar(25),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			insert into #tmpGTVC select ''GRAN TOTAL PRESUPUESTO'', sum(enero_presup), sum(febrero_presup), sum(marzo_presup), sum(abril_presup), sum(mayo_presup), sum(junio_presup), sum(julio_presup), sum(agosto_presup), sum(septiembre_presup), sum(octubre_presup), sum(noviembre_presup), sum(diciembre_presup), sum(total_presup) from #tmpP
			insert into #tmpGTVC select ''GRAN TOTAL VENTAS REALES'', sum(enero_ventas), sum(febrero_ventas), sum(marzo_ventas), sum(abril_ventas), sum(mayo_ventas), sum(junio_ventas), sum(julio_ventas), sum(agosto_ventas), sum(septiembre_ventas), sum(octubre_ventas), sum(noviembre_ventas), sum(diciembre_ventas), sum(total_ventas) from #tmpP
			if @intTipoReporte = 10
				begin
					select * from #tmpP
				end
			else if @intTipoReporte = 11
				begin
					select * from #tmpVC
				end
			else if @intTipoReporte = 12
				begin
					select * from #tmpGTVC
				end
			else if @intTipoReporte = 13
				begin
					select distinct vendregistro, codigo_vendedor from #tmpP where vendregistro > 0 order by codigo_vendedor
				end
		end
	else if @intTipoReporte = 20 or @intTipoReporte = 21 or @intTipoReporte = 22 or @intTipoReporte = 23
		begin
			create table #tmpC
			(
				claseregistro		tinyint,
				codigo_clase		varchar(12),
				clase				varchar(60),
				enero_presup		numeric(18, 2),
				enero_ventas		numeric(18, 2),
				enero_cumplim		numeric(18, 2),
				febrero_presup		numeric(18, 2),
				febrero_ventas		numeric(18, 2),
				febrero_cumplim		numeric(18, 2),
				marzo_presup		numeric(18, 2),
				marzo_ventas		numeric(18, 2),
				marzo_cumplim		numeric(18, 2),
				abril_presup		numeric(18, 2),
				abril_ventas		numeric(18, 2),
				abril_cumplim		numeric(18, 2),
				mayo_presup			numeric(18, 2),
				mayo_ventas			numeric(18, 2),
				mayo_cumplim		numeric(18, 2),
				junio_presup		numeric(18, 2),
				junio_ventas		numeric(18, 2),
				junio_cumplim		numeric(18, 2),
				julio_presup		numeric(18, 2),
				julio_ventas		numeric(18, 2),
				julio_cumplim		numeric(18, 2),
				agosto_presup		numeric(18, 2),
				agosto_ventas		numeric(18, 2),
				agosto_cumplim		numeric(18, 2),
				septiembre_presup	numeric(18, 2),
				septiembre_ventas	numeric(18, 2),
				septiembre_cumplim	numeric(18, 2),
				octubre_presup		numeric(18, 2),
				octubre_ventas		numeric(18, 2),
				octubre_cumplim		numeric(18, 2),
				noviembre_presup	numeric(18, 2),
				noviembre_ventas	numeric(18, 2),
				noviembre_cumplim	numeric(18, 2),
				diciembre_presup	numeric(18, 2),
				diciembre_ventas	numeric(18, 2),
				diciembre_cumplim	numeric(18, 2),
				total_presup		numeric(18, 2),
				total_ventas		numeric(18, 2),
				total_cumplim		numeric(18, 2),
			)
			create clustered index #ixtmpC on #tmpC (claseregistro)
			set @strComando01 = ''insert into #tmpC select c.registro claseregistro, c.codigo codigo_clase, c.descripcion clase, ''
			set @strComando02 = ''sum(r.prodprecio * r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
			set @strComando03 = ''sum(r.prodprecio * r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
			set @strComando04 = ''sum(r.prodprecio * r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
			set @strComando05 = ''sum(r.prodprecio * r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
			set @strComando06 = ''sum(r.prodprecio * r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
			set @strComando07 = ''sum(r.prodprecio * r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
			set @strComando08 = ''sum(r.prodprecio * r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
			set @strComando09 = ''sum(r.prodprecio * r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
			set @strComando10 = ''sum(r.prodprecio * r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
			set @strComando11 = ''sum(r.prodprecio * r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
			set @strComando12 = ''sum(r.prodprecio * r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
			set @strComando13 = ''sum(r.prodprecio * r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
			set @strComando14 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
			set @strComando15 = ''from tbl_presupuestos r, prm_clases c, prm_vendedores v, prm_productos p ''
			set @strComando16 = ''where r.vendregistro = v.registro and r.prodregistro = p.registro and p.regclase = c.registro ''
			set @strComando17 = ''group by c.registro, c.codigo, c.descripcion ''
			set @strComando18 = ''order by c.registro, c.codigo ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + 
				@strComando11 + @strComando12 + @strComando13 + @strComando14 + @strComando15 + 
				@strComando16 + ''and r.years = '' + @intYears + '' '' + @strVendedor + @strClases + 
				@strSubClases + @strLaboratorios + @strVendedor2 + @strComando17 + @strComando18)
			update #tmpC set total_presup = (enero_presup + febrero_presup + marzo_presup + abril_presup + mayo_presup +
				junio_presup + julio_presup + agosto_presup + septiembre_presup + octubre_presup + noviembre_presup + 
				diciembre_presup)
			create table #tmpCV
			(
				regclase		tinyint,
				months			tinyint,
				valor			numeric(18, 2)
			)
			create clustered index #ixtmpCV on #tmpCV (regclase, months)
			set @strComando01 = ''insert into #tmpCV select p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.valor) valor ''
			set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos p, tbl_Presupuestos r where e.registro = d.registro and d.ProdRegistro = ''
			set @strComando03 = ''P.Registro and e.vendregistro = r.vendregistro and p.registro = r.prodregistro and d.prodregistro = r.prodregistro and e.status = 0 ''
			set @strComando04 = ''group by p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			set @strComando05 = ''order by p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			exec (@strComando01 + @strComando02 + @strComando03 + ''and e.numfechaing between '' + 
				@intFecha1 + ''and '' + @intFecha2 + '' '' + @strVendedor + @strClases + @strSubClases + 
				@strLaboratorios + @strVendedor2 + @strComando04 + @strComando05)
			declare tmpC cursor for
				select regclase, months, valor from #tmpCV
			open tmpC
			fetch next from tmpC into @regclase, @meses, @valor
			while @@fetch_status = 0
				begin
					if @meses = 1
						update #tmpC set enero_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 2
						update #tmpC set febrero_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 3
						update #tmpC set marzo_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 4
						update #tmpC set abril_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 5
						update #tmpC set mayo_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 6
						update #tmpC set junio_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 7
						update #tmpC set julio_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 8
						update #tmpC set agosto_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 9
						update #tmpC set septiembre_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 10
						update #tmpC set octubre_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 11
						update #tmpC set noviembre_ventas = @valor where
							claseregistro = @regclase
					else if @meses = 12
						update #tmpC set diciembre_ventas = @valor where
							claseregistro = @regclase
					fetch next from tmpC into @regclase, @meses, @valor
				end
			close tmpC
			deallocate tmpC
			update #tmpC set total_ventas = (enero_ventas + febrero_ventas + marzo_ventas + abril_ventas + mayo_ventas +
				junio_ventas + julio_ventas + agosto_ventas + septiembre_ventas + octubre_ventas + noviembre_ventas + 
				diciembre_ventas)
			update #tmpC set enero_cumplim = ((enero_ventas / enero_presup) * 100) where enero_presup <> 0
			update #tmpC set febrero_cumplim = ((febrero_ventas / febrero_presup) * 100) where febrero_presup <> 0
			update #tmpC set marzo_cumplim = ((marzo_ventas / marzo_presup) * 100) where marzo_presup <> 0
			update #tmpC set abril_cumplim = ((abril_ventas / abril_presup) * 100) where abril_presup <> 0
			update #tmpC set mayo_cumplim = ((mayo_ventas / mayo_presup) * 100) where mayo_presup <> 0
			update #tmpC set junio_cumplim = ((junio_ventas / junio_presup) * 100) where junio_presup <> 0
			update #tmpC set julio_cumplim = ((julio_ventas / julio_presup) * 100) where julio_presup <> 0
			update #tmpC set agosto_cumplim = ((agosto_ventas / agosto_presup) * 100) where agosto_presup <> 0
			update #tmpC set septiembre_cumplim = ((septiembre_ventas / septiembre_presup) * 100) where septiembre_presup <> 0
			update #tmpC set octubre_cumplim = ((octubre_ventas / octubre_presup) * 100) where octubre_presup <> 0
			update #tmpC set noviembre_cumplim = ((noviembre_ventas / noviembre_presup) * 100) where noviembre_presup <> 0
			update #tmpC set diciembre_cumplim = ((diciembre_ventas / diciembre_presup) * 100) where diciembre_presup <> 0
			update #tmpC set total_cumplim = ((total_ventas / total_presup) * 100) where total_presup <> 0
			create table #tmpVCC
			(
				claseregistro	tinyint,
				clasecodigo		varchar(12),
				clasenombre		varchar(60),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			create clustered index #ixtmpVCC on #tmpVCC (claseregistro)
			insert into #tmpVCC select claseregistro, codigo_clase, clase + '' PRESUP'', enero_presup, febrero_presup, marzo_presup, abril_presup, mayo_presup, junio_presup, julio_presup, agosto_presup, septiembre_presup, octubre_presup, noviembre_presup, diciembre_presup, total_presup from #tmpC
			insert into #tmpVCC select claseregistro, codigo_clase, clase + '' VENTAS'', enero_ventas, febrero_ventas, marzo_ventas, abril_ventas, mayo_ventas, junio_ventas, julio_ventas, agosto_ventas, septiembre_ventas, octubre_ventas, noviembre_ventas, diciembre_ventas, total_ventas from #tmpC
			create table #tmpGTVCC
			(
				clasenombre		varchar(25),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			insert into #tmpGTVCC select ''GRAN TOTAL PRESUPUESTO'', sum(enero_presup), sum(febrero_presup), sum(marzo_presup), sum(abril_presup), sum(mayo_presup), sum(junio_presup), sum(julio_presup), sum(agosto_presup), sum(septiembre_presup), sum(octubre_presup), sum(noviembre_presup), sum(diciembre_presup), sum(total_presup) from #tmpC
			insert into #tmpGTVCC select ''GRAN TOTAL VENTAS REALES'', sum(enero_ventas), sum(febrero_ventas), sum(marzo_ventas), sum(abril_ventas), sum(mayo_ventas), sum(junio_ventas), sum(julio_ventas), sum(agosto_ventas), sum(septiembre_ventas), sum(octubre_ventas), sum(noviembre_ventas), sum(diciembre_ventas), sum(total_ventas) from #tmpC
			if @intTipoReporte = 20
				begin
					select * from #tmpC
				end
			else if @intTipoReporte = 21
				begin
					select * from #tmpVCC
				end
			else if @intTipoReporte = 22
				begin
					select * from #tmpGTVCC
				end
			else if @intTipoReporte = 23
				begin
					select distinct claseregistro, codigo_clase from #tmpC where claseregistro > 0 order by codigo_clase
				end
		end
	else if @intTipoReporte = 30 or @intTipoReporte = 31 or @intTipoReporte = 32 or @intTipoReporte = 33
		begin
			create table #tmpD
			(
				vendregistro		tinyint,
				codigo_vendedor		varchar(12),
				vendedor			varchar(60),
				claseregistro		tinyint,
				codigo_clase		varchar(12),
				clase				varchar(60),
				enero_presup		numeric(18, 2),
				enero_ventas		numeric(18, 2),
				enero_cumplim		numeric(18, 2),
				febrero_presup		numeric(18, 2),
				febrero_ventas		numeric(18, 2),
				febrero_cumplim		numeric(18, 2),
				marzo_presup		numeric(18, 2),
				marzo_ventas		numeric(18, 2),
				marzo_cumplim		numeric(18, 2),
				abril_presup		numeric(18, 2),
				abril_ventas		numeric(18, 2),
				abril_cumplim		numeric(18, 2),
				mayo_presup			numeric(18, 2),
				mayo_ventas			numeric(18, 2),
				mayo_cumplim		numeric(18, 2),
				junio_presup		numeric(18, 2),
				junio_ventas		numeric(18, 2),
				junio_cumplim		numeric(18, 2),
				julio_presup		numeric(18, 2),
				julio_ventas		numeric(18, 2),
				julio_cumplim		numeric(18, 2),
				agosto_presup		numeric(18, 2),
				agosto_ventas		numeric(18, 2),
				agosto_cumplim		numeric(18, 2),
				septiembre_presup	numeric(18, 2),
				septiembre_ventas	numeric(18, 2),
				septiembre_cumplim	numeric(18, 2),
				octubre_presup		numeric(18, 2),
				octubre_ventas		numeric(18, 2),
				octubre_cumplim		numeric(18, 2),
				noviembre_presup	numeric(18, 2),
				noviembre_ventas	numeric(18, 2),
				noviembre_cumplim	numeric(18, 2),
				diciembre_presup	numeric(18, 2),
				diciembre_ventas	numeric(18, 2),
				diciembre_cumplim	numeric(18, 2),
				total_presup		numeric(18, 2),
				total_ventas		numeric(18, 2),
				total_cumplim		numeric(18, 2),
			)
			create clustered index #ixtmpD on #tmpD (vendregistro)
			set @strComando01 = ''insert into #tmpD select v.registro vendregistro, v.codigo codigo_vendedor, v.nombre vendedor, c.registro claseregistro, c.codigo codigo_clase, c.descripcion clase, ''
			set @strComando02 = ''sum(r.prodprecio * r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
			set @strComando03 = ''sum(r.prodprecio * r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
			set @strComando04 = ''sum(r.prodprecio * r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
			set @strComando05 = ''sum(r.prodprecio * r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
			set @strComando06 = ''sum(r.prodprecio * r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
			set @strComando07 = ''sum(r.prodprecio * r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
			set @strComando08 = ''sum(r.prodprecio * r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
			set @strComando09 = ''sum(r.prodprecio * r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
			set @strComando10 = ''sum(r.prodprecio * r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
			set @strComando11 = ''sum(r.prodprecio * r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
			set @strComando12 = ''sum(r.prodprecio * r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
			set @strComando13 = ''sum(r.prodprecio * r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
			set @strComando14 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
			set @strComando15 = ''from tbl_presupuestos r, prm_clases c, prm_proveedores v, prm_productos p ''
			set @strComando16 = ''where p.regproveedor = v.registro and r.prodregistro = p.registro and p.regclase = c.registro ''
			set @strComando17 = ''group by v.registro, v.codigo, v.nombre, c.registro, c.codigo, c.descripcion ''
			set @strComando18 = ''order by v.registro, v.codigo, v.nombre, c.registro, c.codigo ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + 
				@strComando11 + @strComando12 + @strComando13 + @strComando14 + @strComando15 + 
				@strComando16 + ''and r.years = '' + @intYears + '' '' + @strVendedor + @strClases + 
				@strSubClases + @strLaboratorios + @strVendedor2 + @strComando17 + @strComando18)
			update #tmpD set total_presup = (enero_presup + febrero_presup + marzo_presup + abril_presup + mayo_presup +
				junio_presup + julio_presup + agosto_presup + septiembre_presup + octubre_presup + noviembre_presup + 
				diciembre_presup)
			create table #tmpW
			(
				vendregistro	tinyint,
				regclase		tinyint,
				months			tinyint,
				valor			numeric(18, 2)
			)
			create clustered index #ixtmpW on #tmpW (vendregistro, regclase, months)
			set @strComando01 = ''insert into #tmpW select p.regproveedor, p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.valor) valor ''
			set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos p, tbl_Presupuestos r where e.registro = d.registro and d.ProdRegistro = ''
			set @strComando03 = ''P.Registro and e.vendregistro = r.vendregistro and p.registro = r.prodregistro and d.prodregistro = r.prodregistro and e.status = 0 ''
			set @strComando04 = ''group by p.regproveedor, p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			set @strComando05 = ''order by p.regproveedor, p.regclase, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			exec (@strComando01 + @strComando02 + @strComando03 + ''and e.numfechaing between '' + 
				@intFecha1 + ''and '' + @intFecha2 + '' '' + @strVendedor + @strClases + @strSubClases + 
				@strLaboratorios + @strVendedor2 + @strComando04 + @strComando05)
			declare tmpC cursor for
				select vendregistro, regclase, months, valor from #tmpW
			open tmpC
			fetch next from tmpC into @vendregistro, @regclase, @meses, @valor
			while @@fetch_status = 0
				begin
					if @meses = 1
						update #tmpD set enero_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 2
						update #tmpD set febrero_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 3
						update #tmpD set marzo_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 4
						update #tmpD set abril_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 5
						update #tmpD set mayo_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 6
						update #tmpD set junio_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 7
						update #tmpD set julio_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 8
						update #tmpD set agosto_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 9
						update #tmpD set septiembre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 10
						update #tmpD set octubre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 11
						update #tmpD set noviembre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					else if @meses = 12
						update #tmpD set diciembre_ventas = @valor where vendregistro = @vendregistro and 
							claseregistro = @regclase
					fetch next from tmpC into @vendregistro, @regclase, @meses, @valor
				end
			close tmpC
			deallocate tmpC
			update #tmpD set total_ventas = (enero_ventas + febrero_ventas + marzo_ventas + abril_ventas + mayo_ventas +
				junio_ventas + julio_ventas + agosto_ventas + septiembre_ventas + octubre_ventas + noviembre_ventas + 
				diciembre_ventas)
			update #tmpD set enero_cumplim = ((enero_ventas / enero_presup) * 100) where enero_presup <> 0
			update #tmpD set febrero_cumplim = ((febrero_ventas / febrero_presup) * 100) where febrero_presup <> 0
			update #tmpD set marzo_cumplim = ((marzo_ventas / marzo_presup) * 100) where marzo_presup <> 0
			update #tmpD set abril_cumplim = ((abril_ventas / abril_presup) * 100) where abril_presup <> 0
			update #tmpD set mayo_cumplim = ((mayo_ventas / mayo_presup) * 100) where mayo_presup <> 0
			update #tmpD set junio_cumplim = ((junio_ventas / junio_presup) * 100) where junio_presup <> 0
			update #tmpD set julio_cumplim = ((julio_ventas / julio_presup) * 100) where julio_presup <> 0
			update #tmpD set agosto_cumplim = ((agosto_ventas / agosto_presup) * 100) where agosto_presup <> 0
			update #tmpD set septiembre_cumplim = ((septiembre_ventas / septiembre_presup) * 100) where septiembre_presup <> 0
			update #tmpD set octubre_cumplim = ((octubre_ventas / octubre_presup) * 100) where octubre_presup <> 0
			update #tmpD set noviembre_cumplim = ((noviembre_ventas / noviembre_presup) * 100) where noviembre_presup <> 0
			update #tmpD set diciembre_cumplim = ((diciembre_ventas / diciembre_presup) * 100) where diciembre_presup <> 0
			update #tmpD set total_cumplim = ((total_ventas / total_presup) * 100) where total_presup <> 0
			create table #tmpDC
			(
				vendregistro	tinyint,
				vendcodigo		varchar(12),
				vendnombre		varchar(60),
				claseregistro	tinyint,
				clasecodigo		varchar(12),
				clasenombre		varchar(60),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			create clustered index #ixtmpDC on #tmpDC (vendregistro, claseregistro)
			insert into #tmpDC select vendregistro, codigo_vendedor, vendedor, claseregistro, codigo_clase, clase + '' PRESUP'', enero_presup, febrero_presup, marzo_presup, abril_presup, mayo_presup, junio_presup, julio_presup, agosto_presup, septiembre_presup, octubre_presup, noviembre_presup, diciembre_presup, total_presup from #tmpD
			insert into #tmpDC select vendregistro, codigo_vendedor, vendedor, claseregistro, codigo_clase, clase + '' VENTAS'', enero_ventas, febrero_ventas, marzo_ventas, abril_ventas, mayo_ventas, junio_ventas, julio_ventas, agosto_ventas, septiembre_ventas, octubre_ventas, noviembre_ventas, diciembre_ventas, total_ventas from #tmpD
			create table #tmpGTDC
			(
				clasenombre		varchar(25),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			insert into #tmpGTDC select ''GRAN TOTAL PRESUPUESTO'', sum(enero_presup), sum(febrero_presup), sum(marzo_presup), sum(abril_presup), sum(mayo_presup), sum(junio_presup), sum(julio_presup), sum(agosto_presup), sum(septiembre_presup), sum(octubre_presup), sum(noviembre_presup), sum(diciembre_presup), sum(total_presup) from #tmpD
			insert into #tmpGTDC select ''GRAN TOTAL VENTAS REALES'', sum(enero_ventas), sum(febrero_ventas), sum(marzo_ventas), sum(abril_ventas), sum(mayo_ventas), sum(junio_ventas), sum(julio_ventas), sum(agosto_ventas), sum(septiembre_ventas), sum(octubre_ventas), sum(noviembre_ventas), sum(diciembre_ventas), sum(total_ventas) from #tmpD
			if @intTipoReporte = 30
				begin
					select * from #tmpD
				end
			else if @intTipoReporte = 31
				begin
					select * from #tmpDC
				end
			else if @intTipoReporte = 32
				begin
					select * from #tmpGTDC
				end
			else if @intTipoReporte = 33
				begin
					select distinct vendregistro, codigo_vendedor from #tmpD where vendregistro > 0 order by codigo_vendedor
				end
		end
	else if @intTipoReporte = 40 or @intTipoReporte = 41 or @intTipoReporte = 42 or @intTipoReporte = 43
		begin
			create table #tmpL
			(
				vendregistro		tinyint,
				codigo_vendedor		varchar(12),
				vendedor			varchar(60),
				enero_presup		numeric(18, 2),
				enero_ventas		numeric(18, 2),
				enero_cumplim		numeric(18, 2),
				febrero_presup		numeric(18, 2),
				febrero_ventas		numeric(18, 2),
				febrero_cumplim		numeric(18, 2),
				marzo_presup		numeric(18, 2),
				marzo_ventas		numeric(18, 2),
				marzo_cumplim		numeric(18, 2),
				abril_presup		numeric(18, 2),
				abril_ventas		numeric(18, 2),
				abril_cumplim		numeric(18, 2),
				mayo_presup			numeric(18, 2),
				mayo_ventas			numeric(18, 2),
				mayo_cumplim		numeric(18, 2),
				junio_presup		numeric(18, 2),
				junio_ventas		numeric(18, 2),
				junio_cumplim		numeric(18, 2),
				julio_presup		numeric(18, 2),
				julio_ventas		numeric(18, 2),
				julio_cumplim		numeric(18, 2),
				agosto_presup		numeric(18, 2),
				agosto_ventas		numeric(18, 2),
				agosto_cumplim		numeric(18, 2),
				septiembre_presup	numeric(18, 2),
				septiembre_ventas	numeric(18, 2),
				septiembre_cumplim	numeric(18, 2),
				octubre_presup		numeric(18, 2),
				octubre_ventas		numeric(18, 2),
				octubre_cumplim		numeric(18, 2),
				noviembre_presup	numeric(18, 2),
				noviembre_ventas	numeric(18, 2),
				noviembre_cumplim	numeric(18, 2),
				diciembre_presup	numeric(18, 2),
				diciembre_ventas	numeric(18, 2),
				diciembre_cumplim	numeric(18, 2),
				total_presup		numeric(18, 2),
				total_ventas		numeric(18, 2),
				total_cumplim		numeric(18, 2),
			)
			create clustered index #ixtmpL on #tmpL (vendregistro)
			set @strComando01 = ''insert into #tmpL select v.registro vendregistro, v.codigo codigo_vendedor, v.nombre vendedor, ''
			set @strComando02 = ''sum(r.prodprecio * r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
			set @strComando03 = ''sum(r.prodprecio * r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
			set @strComando04 = ''sum(r.prodprecio * r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
			set @strComando05 = ''sum(r.prodprecio * r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
			set @strComando06 = ''sum(r.prodprecio * r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
			set @strComando07 = ''sum(r.prodprecio * r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
			set @strComando08 = ''sum(r.prodprecio * r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
			set @strComando09 = ''sum(r.prodprecio * r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
			set @strComando10 = ''sum(r.prodprecio * r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
			set @strComando11 = ''sum(r.prodprecio * r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
			set @strComando12 = ''sum(r.prodprecio * r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
			set @strComando13 = ''sum(r.prodprecio * r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
			set @strComando14 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
			set @strComando15 = ''from tbl_presupuestos r, prm_clases c, prm_proveedores v, prm_productos p ''
			set @strComando16 = ''where p.regproveedor = v.registro and r.prodregistro = p.registro and p.regclase = c.registro ''
			set @strComando17 = ''group by v.registro, v.codigo, v.nombre ''
			set @strComando18 = ''order by v.registro, v.codigo, v.nombre ''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + 
				@strComando11 + @strComando12 + @strComando13 + @strComando14 + @strComando15 + 
				@strComando16 + ''and r.years = '' + @intYears + '' '' + @strVendedor + @strClases + 
				@strSubClases + @strLaboratorios + @strVendedor2 + @strComando17 + @strComando18)
			update #tmpL set total_presup = (enero_presup + febrero_presup + marzo_presup + abril_presup + mayo_presup +
				junio_presup + julio_presup + agosto_presup + septiembre_presup + octubre_presup + noviembre_presup + 
				diciembre_presup)
			create table #tmpLV
			(
				vendregistro	tinyint,
				months			tinyint,
				valor			numeric(18, 2)
			)
			create clustered index #ixtmpLV on #tmpLV (vendregistro, months)
			set @strComando01 = ''insert into #tmpLV select p.regproveedor, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.valor) valor ''
			set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos p, tbl_Presupuestos r where e.registro = d.registro and d.ProdRegistro = ''
			set @strComando03 = ''P.Registro and e.vendregistro = r.vendregistro and p.registro = r.prodregistro and d.prodregistro = r.prodregistro and e.status = 0 ''
			set @strComando04 = ''group by p.regproveedor, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			set @strComando05 = ''order by p.regproveedor, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			exec (@strComando01 + @strComando02 + @strComando03 + ''and e.numfechaing between '' + 
				@intFecha1 + ''and '' + @intFecha2 + '' '' + @strVendedor + @strClases + @strSubClases + 
				@strLaboratorios + @strVendedor2 + @strComando04 + @strComando05)
			declare tmpC cursor for
				select vendregistro, months, valor from #tmpLV
			open tmpC
			fetch next from tmpC into @vendregistro, @meses, @valor
			while @@fetch_status = 0
				begin
					if @meses = 1
						update #tmpL set enero_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 2
						update #tmpL set febrero_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 3
						update #tmpL set marzo_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 4
						update #tmpL set abril_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 5
						update #tmpL set mayo_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 6
						update #tmpL set junio_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 7
						update #tmpL set julio_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 8
						update #tmpL set agosto_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 9
						update #tmpL set septiembre_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 10
						update #tmpL set octubre_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 11
						update #tmpL set noviembre_ventas = @valor where vendregistro = @vendregistro
					else if @meses = 12
						update #tmpL set diciembre_ventas = @valor where vendregistro = @vendregistro
					fetch next from tmpC into @vendregistro, @meses, @valor
				end
			close tmpC
			deallocate tmpC
			update #tmpL set total_ventas = (enero_ventas + febrero_ventas + marzo_ventas + abril_ventas + mayo_ventas +
				junio_ventas + julio_ventas + agosto_ventas + septiembre_ventas + octubre_ventas + noviembre_ventas + 
				diciembre_ventas)
			update #tmpL set enero_cumplim = ((enero_ventas / enero_presup) * 100) where enero_presup <> 0
			update #tmpL set febrero_cumplim = ((febrero_ventas / febrero_presup) * 100) where febrero_presup <> 0
			update #tmpL set marzo_cumplim = ((marzo_ventas / marzo_presup) * 100) where marzo_presup <> 0
			update #tmpL set abril_cumplim = ((abril_ventas / abril_presup) * 100) where abril_presup <> 0
			update #tmpL set mayo_cumplim = ((mayo_ventas / mayo_presup) * 100) where mayo_presup <> 0
			update #tmpL set junio_cumplim = ((junio_ventas / junio_presup) * 100) where junio_presup <> 0
			update #tmpL set julio_cumplim = ((julio_ventas / julio_presup) * 100) where julio_presup <> 0
			update #tmpL set agosto_cumplim = ((agosto_ventas / agosto_presup) * 100) where agosto_presup <> 0
			update #tmpL set septiembre_cumplim = ((septiembre_ventas / septiembre_presup) * 100) where septiembre_presup <> 0
			update #tmpL set octubre_cumplim = ((octubre_ventas / octubre_presup) * 100) where octubre_presup <> 0
			update #tmpL set noviembre_cumplim = ((noviembre_ventas / noviembre_presup) * 100) where noviembre_presup <> 0
			update #tmpL set diciembre_cumplim = ((diciembre_ventas / diciembre_presup) * 100) where diciembre_presup <> 0
			update #tmpL set total_cumplim = ((total_ventas / total_presup) * 100) where total_presup <> 0
			create table #tmpLVV
			(
				vendregistro	tinyint,
				vendcodigo		varchar(12),
				vendnombre		varchar(60),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			create clustered index #ixtmpLVV on #tmpLVV (vendregistro)
			insert into #tmpLVV select vendregistro, codigo_vendedor, vendedor + '' PRESUP'', enero_presup, febrero_presup, marzo_presup, abril_presup, mayo_presup, junio_presup, julio_presup, agosto_presup, septiembre_presup, octubre_presup, noviembre_presup, diciembre_presup, total_presup from #tmpL
			insert into #tmpLVV select vendregistro, codigo_vendedor, vendedor + '' VENTAS'', enero_ventas, febrero_ventas, marzo_ventas, abril_ventas, mayo_ventas, junio_ventas, julio_ventas, agosto_ventas, septiembre_ventas, octubre_ventas, noviembre_ventas, diciembre_ventas, total_ventas from #tmpL
			create table #tmpGTLV
			(
				clasenombre		varchar(25),
				enero			numeric(18, 2),
				febrero			numeric(18, 2),
				marzo			numeric(18, 2),
				abril			numeric(18, 2),
				mayo			numeric(18, 2),
				junio			numeric(18, 2),
				julio			numeric(18, 2),
				agosto			numeric(18, 2),
				septiembre		numeric(18, 2),
				octubre			numeric(18, 2),
				noviembre		numeric(18, 2),
				diciembre		numeric(18, 2),
				totales			numeric(18, 2)
			)
			insert into #tmpGTLV select ''GRAN TOTAL PRESUPUESTO'', sum(enero_presup), sum(febrero_presup), sum(marzo_presup), sum(abril_presup), sum(mayo_presup), sum(junio_presup), sum(julio_presup), sum(agosto_presup), sum(septiembre_presup), sum(octubre_presup), sum(noviembre_presup), sum(diciembre_presup), sum(total_presup) from #tmpL
			insert into #tmpGTLV select ''GRAN TOTAL VENTAS REALES'', sum(enero_ventas), sum(febrero_ventas), sum(marzo_ventas), sum(abril_ventas), sum(mayo_ventas), sum(junio_ventas), sum(julio_ventas), sum(agosto_ventas), sum(septiembre_ventas), sum(octubre_ventas), sum(noviembre_ventas), sum(diciembre_ventas), sum(total_ventas) from #tmpL
			if @intTipoReporte = 40
				begin
					select * from #tmpL
				end
			else if @intTipoReporte = 41
				begin
					select * from #tmpLVV
				end
			else if @intTipoReporte = 42
				begin
					select * from #tmpGTLV
				end
			else if @intTipoReporte = 43
				begin
					select distinct vendregistro, codigo_vendedor from #tmpL where vendregistro > 0 order by codigo_vendedor
				end
		end
	else if @intTipoReporte = 50 or @intTipoReporte = 60
		begin
			create table #tmpVLP
			(
				vendregistro		tinyint,
				codigo_vendedor		varchar(12),
				vendedor			varchar(60),
				provregistro		smallint,
				codigo_proveedor	varchar(12),
				proveedor			varchar(60),
				prodregistro		smallint,
				codigo_producto		varchar(12),
				producto			varchar(60),
				exist_actual		numeric(18, 2),
				enero_exist			numeric(18, 2),
				enero_presup		numeric(18, 2),
				enero_ventas		numeric(18, 2),
				enero_cumplim		numeric(18, 2),
				febrero_exist		numeric(18, 2),
				febrero_presup		numeric(18, 2),
				febrero_ventas		numeric(18, 2),
				febrero_cumplim		numeric(18, 2),
				marzo_exist			numeric(18, 2),
				marzo_presup		numeric(18, 2),
				marzo_ventas		numeric(18, 2),
				marzo_cumplim		numeric(18, 2),
				abril_exist			numeric(18, 2),
				abril_presup		numeric(18, 2),
				abril_ventas		numeric(18, 2),
				abril_cumplim		numeric(18, 2),
				mayo_exist			numeric(18, 2),
				mayo_presup			numeric(18, 2),
				mayo_ventas			numeric(18, 2),
				mayo_cumplim		numeric(18, 2),
				junio_exist			numeric(18, 2),
				junio_presup		numeric(18, 2),
				junio_ventas		numeric(18, 2),
				junio_cumplim		numeric(18, 2),
				julio_exist			numeric(18, 2),
				julio_presup		numeric(18, 2),
				julio_ventas		numeric(18, 2),
				julio_cumplim		numeric(18, 2),
				agosto_exist		numeric(18, 2),
				agosto_presup		numeric(18, 2),
				agosto_ventas		numeric(18, 2),
				agosto_cumplim		numeric(18, 2),
				septiembre_exist	numeric(18, 2),
				septiembre_presup	numeric(18, 2),
				septiembre_ventas	numeric(18, 2),
				septiembre_cumplim	numeric(18, 2),
				octubre_exist		numeric(18, 2),
				octubre_presup		numeric(18, 2),
				octubre_ventas		numeric(18, 2),
				octubre_cumplim		numeric(18, 2),
				noviembre_exist		numeric(18, 2),
				noviembre_presup	numeric(18, 2),
				noviembre_ventas	numeric(18, 2),
				noviembre_cumplim	numeric(18, 2),
				diciembre_exist		numeric(18, 2),
				diciembre_presup	numeric(18, 2),
				diciembre_ventas	numeric(18, 2),
				diciembre_cumplim	numeric(18, 2),
				total_presup		numeric(18, 2),
				total_ventas		numeric(18, 2),
				total_cumplim		numeric(18, 2)
			)
			create clustered index #ixtmpVLP on #tmpVLP (vendregistro, prodregistro)
			create index #ixtmpVLP2 on #tmpVLP (prodregistro)
			set @strComando01 = ''insert into #tmpVLP select v.registro vendregistro, v.codigo codigo_vendedor, v.nombre vendedor, d.registro provregistro, d.codigo codigo_proveedor, d.nombre proveedor, ''
			set @strComando02 = ''p.registro prodregistro, p.codigo codigo_producto, p.descripcion producto, convert(numeric(18,2), 0.00) exist_actual, ''
			set @strComando03 = ''convert(numeric(18,2), 0.00) enero_exist, sum(r.prodprecio * r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
			set @strComando04 = ''convert(numeric(18,2), 0.00) febrero_exist, sum(r.prodprecio * r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
			set @strComando05 = ''convert(numeric(18,2), 0.00) marzo_exist, sum(r.prodprecio * r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
			set @strComando06 = ''convert(numeric(18,2), 0.00) abril_exist, sum(r.prodprecio * r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
			set @strComando07 = ''convert(numeric(18,2), 0.00) mayo_exist, sum(r.prodprecio * r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
			set @strComando08 = ''convert(numeric(18,2), 0.00) junio_exist, sum(r.prodprecio * r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
			set @strComando09 = ''convert(numeric(18,2), 0.00) julio_exist, sum(r.prodprecio * r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
			set @strComando10 = ''convert(numeric(18,2), 0.00) agosto_exist, sum(r.prodprecio * r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
			set @strComando11 = ''convert(numeric(18,2), 0.00) septiembre_exist, sum(r.prodprecio * r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
			set @strComando12 = ''convert(numeric(18,2), 0.00) octubre_exist, sum(r.prodprecio * r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
			set @strComando13 = ''convert(numeric(18,2), 0.00) noviembre_exist, sum(r.prodprecio * r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
			set @strComando14 = ''convert(numeric(18,2), 0.00) diciembre_exist, sum(r.prodprecio * r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
			set @strComando15 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
			set @strComando16 = ''from tbl_presupuestos r, prm_vendedores v, prm_productos p, prm_proveedores d where r.vendregistro = v.registro and r.prodregistro = p.registro and p.regproveedor = d.registro and r.years = ''
			set @strComando17 = ''group by v.registro, v.codigo, v.nombre, d.registro, d.codigo, d.nombre, p.registro, p.codigo, p.descripcion order by v.nombre, d.nombre, p.descripcion''
			exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
				@strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + 
				@strComando11 + @strComando12 + @strComando13 + @strComando14 + @strComando15 + 
				@strComando16 + @intYears + '' '' + @strVendedor + @strClases + 
				@strSubClases + @strLaboratorios + @strVendedor2 + @strComando17)
			update #tmpVLP set total_presup = (enero_presup + febrero_presup + marzo_presup + abril_presup + mayo_presup +
				junio_presup + julio_presup + agosto_presup + septiembre_presup + octubre_presup + noviembre_presup + 
				diciembre_presup)
			create table #tmpVLP2
			(
				vendregistro	tinyint,
				prodregistro	smallint,
				months			tinyint,
				valor			numeric(18, 2)
			)
			create clustered index #ixtmpVLP2 on #tmpVLP2 (vendregistro, prodregistro, months)
			set @strComando01 = ''insert into #tmpVLP2 select e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.valor) valor ''
			set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos p, tbl_Presupuestos r where e.registro = d.registro and d.ProdRegistro = ''
			set @strComando03 = ''P.Registro and e.vendregistro = r.vendregistro and p.registro = r.prodregistro and d.prodregistro = r.prodregistro and e.status = 0 ''
			set @strComando04 = ''group by e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			set @strComando05 = ''order by e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
			exec (@strComando01 + @strComando02 + @strComando03 + ''and e.numfechaing between '' + 
				@intFecha1 + ''and '' + @intFecha2 + '' '' + @strVendedor + @strClases + @strSubClases + 
				@strLaboratorios + @strVendedor2 + @strComando04 + @strComando05)
			declare tmpC cursor for
				select vendregistro, prodregistro, months, valor from #tmpVLP2
			open tmpC
			fetch next from tmpC into @vendregistro, @prodregistro, @meses, @valor
			while @@fetch_status = 0
				begin
					if @meses = 1
						update #tmpVLP set enero_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 2
						update #tmpVLP set febrero_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 3
						update #tmpVLP set marzo_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 4
						update #tmpVLP set abril_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 5
						update #tmpVLP set mayo_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 6
						update #tmpVLP set junio_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 7
						update #tmpVLP set julio_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 8
						update #tmpVLP set agosto_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 9
						update #tmpVLP set septiembre_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 10
						update #tmpVLP set octubre_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 11
						update #tmpVLP set noviembre_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					else if @meses = 12
						update #tmpVLP set diciembre_ventas = @valor where vendregistro = @vendregistro and 
							prodregistro = @prodregistro
					fetch next from tmpC into @vendregistro, @prodregistro, @meses, @valor
				end
			close tmpC
			deallocate tmpC
			update #tmpVLP set total_ventas = (enero_ventas + febrero_ventas + marzo_ventas + abril_ventas + mayo_ventas +
				junio_ventas + julio_ventas + agosto_ventas + septiembre_ventas + octubre_ventas + noviembre_ventas + 
				diciembre_ventas)
			update #tmpVLP set enero_cumplim = ((enero_ventas / enero_presup) * 100) where enero_presup <> 0
			update #tmpVLP set febrero_cumplim = ((febrero_ventas / febrero_presup) * 100) where febrero_presup <> 0
			update #tmpVLP set marzo_cumplim = ((marzo_ventas / marzo_presup) * 100) where marzo_presup <> 0
			update #tmpVLP set abril_cumplim = ((abril_ventas / abril_presup) * 100) where abril_presup <> 0
			update #tmpVLP set mayo_cumplim = ((mayo_ventas / mayo_presup) * 100) where mayo_presup <> 0
			update #tmpVLP set junio_cumplim = ((junio_ventas / junio_presup) * 100) where junio_presup <> 0
			update #tmpVLP set julio_cumplim = ((julio_ventas / julio_presup) * 100) where julio_presup <> 0
			update #tmpVLP set agosto_cumplim = ((agosto_ventas / agosto_presup) * 100) where agosto_presup <> 0
			update #tmpVLP set septiembre_cumplim = ((septiembre_ventas / septiembre_presup) * 100) where septiembre_presup <> 0
			update #tmpVLP set octubre_cumplim = ((octubre_ventas / octubre_presup) * 100) where octubre_presup <> 0
			update #tmpVLP set noviembre_cumplim = ((noviembre_ventas / noviembre_presup) * 100) where noviembre_presup <> 0
			update #tmpVLP set diciembre_cumplim = ((diciembre_ventas / diciembre_presup) * 100) where diciembre_presup <> 0
			update #tmpVLP set total_cumplim = ((total_ventas / total_presup) * 100) where total_presup <> 0
			declare tmpA cursor for
				select registro from prm_agencias where estado = 0 and registro not in (3,4)
			declare tmpC cursor for
				select distinct prodregistro from #tmpVLP
			open tmpC
			fetch next from tmpC into @prodregistro
			while @@fetch_status = 0
				begin
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set exist_actual = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 131
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set enero_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 231
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set febrero_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 331
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set marzo_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 431
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set abril_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 531
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set mayo_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 631
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set junio_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 731
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set julio_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 831
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set agosto_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 931
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set septiembre_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 1031
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set octubre_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 1131
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set noviembre_exist = @cantidad where prodregistro = @prodregistro
						end
					set @intFecha2 = @intYears * 10000 + 1231
					set @cantidad = 0
					open tmpA
					fetch next from tmpA into @agenregistro
					while @@fetch_status = 0
						begin
							set @cant_final = 0
							set @maxregistro = 0
							set @numfechaing = 0
							select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro and
								numfechaing <= @intFecha2
							if @numfechaing <> 0
								begin
									select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
										where agenregistro = @agenregistro and prodregistro = @prodregistro 
										and numfechaing = @numfechaing
									if @maxregistro <> 0
										select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
											where agenregistro = @agenregistro and prodregistro = @prodregistro
											and registro = @maxregistro
								end
							set @cantidad = @cantidad + @cant_final
							fetch next from tmpA into @agenregistro
						end
					close tmpA
					if @cantidad <> 0
						begin
							update #tmpVLP set diciembre_exist = @cantidad where prodregistro = @prodregistro
						end
					fetch next from tmpC into @prodregistro
				end
			close tmpC
			deallocate tmpC
			deallocate tmpA
			if @intTipoReporte = 50
				begin
					select * from #tmpVLP order by vendregistro, provregistro, prodregistro
				end
			else if @intTipoReporte = 60
				begin
					select * from #tmpVLP order by provregistro, vendregistro, prodregistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_RptPresupuestos3]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_RptPresupuestos3]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_RptPresupuestos3] @intTipoReporte tinyint, @intYears smallint, @strVendedores varchar(255),
		@strClases varchar(255), @strSubClases varchar(255), @strProveedores varchar(600), @strProductos varchar(600),
		@intFecha1 numeric(8), @intFecha2 numeric(8), @intValorCantidad as tinyint

as

declare	@vendregistro	tinyint, 
		@prodregistro	smallint,
		@regclase		tinyint, 
		@meses			tinyint, 
		@valor			numeric(18, 2),
		@strComando01	varchar(200),
		@strComando02	varchar(200),
		@strComando03	varchar(200),
		@strComando04	varchar(200),
		@strComando05	varchar(200),
		@strComando06	varchar(200),
		@strComando07	varchar(200),
		@strComando08	varchar(200),
		@strComando09	varchar(200),
		@strComando10	varchar(200),
		@strComando11	varchar(200),
		@strComando12	varchar(200),
		@strComando13	varchar(200),
		@strComando14	varchar(200),
		@strComando15	varchar(200),
		@strComando16	varchar(200),
		@strComando17	varchar(200),
		@strComando18	varchar(200),
		@strComando19	varchar(200),
		@strComando20	varchar(200),
		@agenregistro	tinyint,
		@maxregistro	bigint,
		@cant_final		numeric(18,2),
		@cantidad		numeric(18,2),
		@numfechaing	numeric(8)

begin
	set nocount on
	set @strComando01 = ''''
	set @strComando02 = ''''
	set @strComando03 = ''''
	set @strComando04 = ''''
	set @strComando05 = ''''
	set @strComando06 = ''''
	set @strComando07 = ''''
	set @strComando08 = ''''
	set @strComando09 = ''''
	set @strComando10 = ''''
	set @strComando11 = ''''
	set @strComando12 = ''''
	set @strComando13 = ''''
	set @strComando14 = ''''
	set @strComando15 = ''''
	set @strComando16 = ''''
	set @strComando17 = ''''
	set @strComando18 = ''''
	set @strComando19 = ''''
	set @strComando20 = ''''
	create table #tmpVLP
	(
		vendregistro		tinyint,
		codigo_vendedor		varchar(12),
		vendedor			varchar(60),
		provregistro		smallint,
		codigo_proveedor	varchar(12),
		proveedor			varchar(60),
		prodregistro		smallint,
		codigo_producto		varchar(12),
		producto			varchar(60),
		exist_actual		numeric(18, 2),
		enero_exist			numeric(18, 2),
		enero_presup		numeric(18, 2),
		enero_ventas		numeric(18, 2),
		enero_cumplim		numeric(18, 2),
		febrero_exist		numeric(18, 2),
		febrero_presup		numeric(18, 2),
		febrero_ventas		numeric(18, 2),
		febrero_cumplim		numeric(18, 2),
		marzo_exist			numeric(18, 2),
		marzo_presup		numeric(18, 2),
		marzo_ventas		numeric(18, 2),
		marzo_cumplim		numeric(18, 2),
		abril_exist			numeric(18, 2),
		abril_presup		numeric(18, 2),
		abril_ventas		numeric(18, 2),
		abril_cumplim		numeric(18, 2),
		mayo_exist			numeric(18, 2),
		mayo_presup			numeric(18, 2),
		mayo_ventas			numeric(18, 2),
		mayo_cumplim		numeric(18, 2),
		junio_exist			numeric(18, 2),
		junio_presup		numeric(18, 2),
		junio_ventas		numeric(18, 2),
		junio_cumplim		numeric(18, 2),
		julio_exist			numeric(18, 2),
		julio_presup		numeric(18, 2),
		julio_ventas		numeric(18, 2),
		julio_cumplim		numeric(18, 2),
		agosto_exist		numeric(18, 2),
		agosto_presup		numeric(18, 2),
		agosto_ventas		numeric(18, 2),
		agosto_cumplim		numeric(18, 2),
		septiembre_exist	numeric(18, 2),
		septiembre_presup	numeric(18, 2),
		septiembre_ventas	numeric(18, 2),
		septiembre_cumplim	numeric(18, 2),
		octubre_exist		numeric(18, 2),
		octubre_presup		numeric(18, 2),
		octubre_ventas		numeric(18, 2),
		octubre_cumplim		numeric(18, 2),
		noviembre_exist		numeric(18, 2),
		noviembre_presup	numeric(18, 2),
		noviembre_ventas	numeric(18, 2),
		noviembre_cumplim	numeric(18, 2),
		diciembre_exist		numeric(18, 2),
		diciembre_presup	numeric(18, 2),
		diciembre_ventas	numeric(18, 2),
		diciembre_cumplim	numeric(18, 2),
		total_presup		numeric(18, 2),
		total_ventas		numeric(18, 2),
		total_cumplim		numeric(18, 2)
	)
	create clustered index #ixtmpVLP on #tmpVLP (vendregistro, prodregistro)
	create index #ixtmpVLP2 on #tmpVLP (prodregistro)
	set @strComando01 = ''insert into #tmpVLP select v.registro vendregistro, v.codigo codigo_vendedor, v.nombre vendedor, d.registro provregistro, d.codigo codigo_proveedor, d.nombre proveedor, ''
	set @strComando02 = ''p.registro prodregistro, p.codigo codigo_producto, p.descripcion producto, convert(numeric(18,2), 0.00) exist_actual, ''
	set @strComando03 = ''convert(numeric(18,2), 0.00) enero_exist, sum(r.prodprecio * r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
	set @strComando04 = ''convert(numeric(18,2), 0.00) febrero_exist, sum(r.prodprecio * r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
	set @strComando05 = ''convert(numeric(18,2), 0.00) marzo_exist, sum(r.prodprecio * r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
	set @strComando06 = ''convert(numeric(18,2), 0.00) abril_exist, sum(r.prodprecio * r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
	set @strComando07 = ''convert(numeric(18,2), 0.00) mayo_exist, sum(r.prodprecio * r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
	set @strComando08 = ''convert(numeric(18,2), 0.00) junio_exist, sum(r.prodprecio * r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
	set @strComando09 = ''convert(numeric(18,2), 0.00) julio_exist, sum(r.prodprecio * r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
	set @strComando10 = ''convert(numeric(18,2), 0.00) agosto_exist, sum(r.prodprecio * r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
	set @strComando11 = ''convert(numeric(18,2), 0.00) septiembre_exist, sum(r.prodprecio * r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
	set @strComando12 = ''convert(numeric(18,2), 0.00) octubre_exist, sum(r.prodprecio * r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
	set @strComando13 = ''convert(numeric(18,2), 0.00) noviembre_exist, sum(r.prodprecio * r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
	set @strComando14 = ''convert(numeric(18,2), 0.00) diciembre_exist, sum(r.prodprecio * r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
	set @strComando15 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
	if @intValorCantidad = 1
		begin
			set @strComando03 = ''convert(numeric(18,2), 0.00) enero_exist, sum(r.enero) enero_presup, convert(numeric(18,2), 0.00) enero_ventas, convert(numeric(18,2), 0.00) enero_cumplim, ''
			set @strComando04 = ''convert(numeric(18,2), 0.00) febrero_exist, sum(r.febrero) febrero_presup, convert(numeric(18,2), 0.00) febrero_ventas, convert(numeric(18,2), 0.00) febrero_cumplim, ''
			set @strComando05 = ''convert(numeric(18,2), 0.00) marzo_exist, sum(r.marzo) marzo_presup, convert(numeric(18,2), 0.00) marzo_ventas, convert(numeric(18,2), 0.00) marzo_cumplim, ''
			set @strComando06 = ''convert(numeric(18,2), 0.00) abril_exist, sum(r.abril) abril_presup, convert(numeric(18,2), 0.00) abril_ventas, convert(numeric(18,2), 0.00) abril_cumplim, ''
			set @strComando07 = ''convert(numeric(18,2), 0.00) mayo_exist, sum(r.mayo) mayo_presup, convert(numeric(18,2), 0.00) mayo_ventas, convert(numeric(18,2), 0.00) mayo_cumplim, ''
			set @strComando08 = ''convert(numeric(18,2), 0.00) junio_exist, sum(r.junio) junio_presup, convert(numeric(18,2), 0.00) junio_ventas, convert(numeric(18,2), 0.00) junio_cumplim, ''
			set @strComando09 = ''convert(numeric(18,2), 0.00) julio_exist, sum(r.julio) julio_presup, convert(numeric(18,2), 0.00) julio_ventas, convert(numeric(18,2), 0.00) julio_cumplim, ''
			set @strComando10 = ''convert(numeric(18,2), 0.00) agosto_exist, sum(r.agosto) agosto_presup, convert(numeric(18,2), 0.00) agosto_ventas, convert(numeric(18,2), 0.00) agosto_cumplim, ''
			set @strComando11 = ''convert(numeric(18,2), 0.00) septiembre_exist, sum(r.septiembre) septiembre_presup, convert(numeric(18,2), 0.00) septiembre_ventas, convert(numeric(18,2), 0.00) septiembre_cumplim, ''
			set @strComando12 = ''convert(numeric(18,2), 0.00) octubre_exist, sum(r.octubre) octubre_presup, convert(numeric(18,2), 0.00) octubre_ventas, convert(numeric(18,2), 0.00) octubre_cumplim, ''
			set @strComando13 = ''convert(numeric(18,2), 0.00) noviembre_exist, sum(r.noviembre) noviembre_presup, convert(numeric(18,2), 0.00) noviembre_ventas, convert(numeric(18,2), 0.00) noviembre_cumplim, ''
			set @strComando14 = ''convert(numeric(18,2), 0.00) diciembre_exist, sum(r.diciembre) diciembre_presup, convert(numeric(18,2), 0.00) diciembre_ventas, convert(numeric(18,2), 0.00) diciembre_cumplim, ''
			set @strComando15 = ''convert(numeric(18,2), 0.00) total_presup, convert(numeric(18,2), 0.00) total_ventas, convert(numeric(18,2), 0.00) total_cumplim ''
		end
	set @strComando16 = ''from tbl_presupuestos r, prm_vendedores v, prm_productos p, prm_proveedores d where r.vendregistro = v.registro and r.prodregistro = p.registro and p.regproveedor = d.registro and r.years = ''
	set @strComando17 = ''group by v.registro, v.codigo, v.nombre, d.registro, d.codigo, d.nombre, p.registro, p.codigo, p.descripcion order by v.nombre, d.nombre, p.descripcion''
	exec (@strComando01 + @strComando02 + @strComando03 + @strComando04 + @strComando05 + 
		@strComando06 + @strComando07 + @strComando08 + @strComando09 + @strComando10 + 
		@strComando11 + @strComando12 + @strComando13 + @strComando14 + @strComando15 + 
		@strComando16 + @intYears + '' '' + @strVendedores + @strClases + 
		@strSubClases + @strProveedores + @strProductos + @strComando17)
	update #tmpVLP set total_presup = (enero_presup + febrero_presup + marzo_presup + abril_presup + mayo_presup +
		junio_presup + julio_presup + agosto_presup + septiembre_presup + octubre_presup + noviembre_presup + 
		diciembre_presup)
	create table #tmpVLP2
	(
		vendregistro	tinyint,
		prodregistro	smallint,
		months			tinyint,
		valor			numeric(18, 2)
	)
	create clustered index #ixtmpVLP2 on #tmpVLP2 (vendregistro, prodregistro, months)
	set @strComando01 = ''insert into #tmpVLP2 select e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.valor) valor ''
	if @intValorCantidad = 1
		begin
			set @strComando01 = ''insert into #tmpVLP2 select e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) months, sum(d.cantidad) valor ''
		end
	set @strComando02 = ''from tbl_facturas_enc e, tbl_facturas_det d, prm_Productos p, tbl_Presupuestos r where e.registro = d.registro and d.ProdRegistro = ''
	set @strComando03 = ''P.Registro and e.vendregistro = r.vendregistro and p.registro = r.prodregistro and d.prodregistro = r.prodregistro and e.status = 0 ''
	set @strComando04 = ''group by e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
	set @strComando05 = ''order by e.vendregistro, p.registro, convert(numeric(2), substring(convert(varchar(8),e.numfechaing), 5, 2)) ''
	exec (@strComando01 + @strComando02 + @strComando03 + ''and e.numfechaing between '' + 
		@intFecha1 + ''and '' + @intFecha2 + '' '' + @strVendedores + @strClases + @strSubClases + 
		@strProveedores + @strProductos + @strComando04 + @strComando05)
	declare tmpC cursor for
		select vendregistro, prodregistro, months, valor from #tmpVLP2
	open tmpC
	fetch next from tmpC into @vendregistro, @prodregistro, @meses, @valor
	while @@fetch_status = 0
		begin
			if @meses = 1
				update #tmpVLP set enero_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 2
				update #tmpVLP set febrero_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 3
				update #tmpVLP set marzo_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 4
				update #tmpVLP set abril_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 5
				update #tmpVLP set mayo_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 6
				update #tmpVLP set junio_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 7
				update #tmpVLP set julio_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 8
				update #tmpVLP set agosto_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 9
				update #tmpVLP set septiembre_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 10
				update #tmpVLP set octubre_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 11
				update #tmpVLP set noviembre_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			else if @meses = 12
				update #tmpVLP set diciembre_ventas = @valor where vendregistro = @vendregistro and 
					prodregistro = @prodregistro
			fetch next from tmpC into @vendregistro, @prodregistro, @meses, @valor
		end
	close tmpC
	deallocate tmpC
	update #tmpVLP set total_ventas = (enero_ventas + febrero_ventas + marzo_ventas + abril_ventas + mayo_ventas +
		junio_ventas + julio_ventas + agosto_ventas + septiembre_ventas + octubre_ventas + noviembre_ventas + 
		diciembre_ventas)
	update #tmpVLP set enero_cumplim = ((enero_ventas / enero_presup) * 100) where enero_presup <> 0
	update #tmpVLP set febrero_cumplim = ((febrero_ventas / febrero_presup) * 100) where febrero_presup <> 0
	update #tmpVLP set marzo_cumplim = ((marzo_ventas / marzo_presup) * 100) where marzo_presup <> 0
	update #tmpVLP set abril_cumplim = ((abril_ventas / abril_presup) * 100) where abril_presup <> 0
	update #tmpVLP set mayo_cumplim = ((mayo_ventas / mayo_presup) * 100) where mayo_presup <> 0
	update #tmpVLP set junio_cumplim = ((junio_ventas / junio_presup) * 100) where junio_presup <> 0
	update #tmpVLP set julio_cumplim = ((julio_ventas / julio_presup) * 100) where julio_presup <> 0
	update #tmpVLP set agosto_cumplim = ((agosto_ventas / agosto_presup) * 100) where agosto_presup <> 0
	update #tmpVLP set septiembre_cumplim = ((septiembre_ventas / septiembre_presup) * 100) where septiembre_presup <> 0
	update #tmpVLP set octubre_cumplim = ((octubre_ventas / octubre_presup) * 100) where octubre_presup <> 0
	update #tmpVLP set noviembre_cumplim = ((noviembre_ventas / noviembre_presup) * 100) where noviembre_presup <> 0
	update #tmpVLP set diciembre_cumplim = ((diciembre_ventas / diciembre_presup) * 100) where diciembre_presup <> 0
	update #tmpVLP set total_cumplim = ((total_ventas / total_presup) * 100) where total_presup <> 0
	update #tmpVLP set enero_cumplim = 100 where enero_presup = 0 and enero_ventas <> 0
	update #tmpVLP set febrero_cumplim = 100 where febrero_presup = 0 and febrero_ventas <> 0
	update #tmpVLP set marzo_cumplim = 100 where marzo_presup = 0 and marzo_ventas <> 0
	update #tmpVLP set abril_cumplim = 100 where abril_presup = 0 and abril_ventas <> 0
	update #tmpVLP set mayo_cumplim = 100 where mayo_presup = 0 and mayo_ventas <> 0
	update #tmpVLP set junio_cumplim = 100 where junio_presup = 0 and junio_ventas <> 0
	update #tmpVLP set julio_cumplim = 100 where julio_presup = 0 and julio_ventas <> 0
	update #tmpVLP set agosto_cumplim = 100 where agosto_presup = 0 and agosto_ventas <> 0
	update #tmpVLP set septiembre_cumplim = 100 where septiembre_presup = 0 and septiembre_ventas <> 0
	update #tmpVLP set octubre_cumplim = 100 where octubre_presup = 0 and octubre_ventas <> 0
	update #tmpVLP set noviembre_cumplim = 100 where noviembre_presup = 0 and noviembre_ventas <> 0
	update #tmpVLP set diciembre_cumplim = 100 where diciembre_presup = 0 and diciembre_ventas <> 0
	update #tmpVLP set total_cumplim = 100 where total_presup = 0 and total_ventas <> 0
	declare tmpA cursor for
		select registro from prm_agencias where estado = 0 and registro not in (3,4)
	declare tmpC cursor for
		select distinct prodregistro from #tmpVLP
	open tmpC
	fetch next from tmpC into @prodregistro
	while @@fetch_status = 0
		begin
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set exist_actual = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 131
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set enero_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 231
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set febrero_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 331
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set marzo_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 431
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set abril_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 531
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set mayo_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 631
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set junio_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 731
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set julio_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 831
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set agosto_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 931
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set septiembre_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 1031
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set octubre_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 1131
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set noviembre_exist = @cantidad where prodregistro = @prodregistro
				end
			set @intFecha2 = @intYears * 10000 + 1231
			set @cantidad = 0
			open tmpA
			fetch next from tmpA into @agenregistro
			while @@fetch_status = 0
				begin
					set @cant_final = 0
					set @maxregistro = 0
					set @numfechaing = 0
					select @numfechaing = isnull(max(numfechaing), 0) from tbl_producto_movimiento
						where agenregistro = @agenregistro and prodregistro = @prodregistro and
						numfechaing <= @intFecha2
					if @numfechaing <> 0
						begin
							select @maxregistro = isnull(max(registro), 0) from tbl_producto_movimiento
								where agenregistro = @agenregistro and prodregistro = @prodregistro 
								and numfechaing = @numfechaing
							if @maxregistro <> 0
								if @intValorCantidad = 0
									select @cant_final = isnull(saldo_mto, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
								else if @intValorCantidad = 1
									select @cant_final = isnull(saldo_cnt, 0) from tbl_producto_movimiento 
										where agenregistro = @agenregistro and prodregistro = @prodregistro
										and registro = @maxregistro
						end
					set @cantidad = @cantidad + @cant_final
					fetch next from tmpA into @agenregistro
				end
			close tmpA
			if @cantidad <> 0
				begin
					update #tmpVLP set diciembre_exist = @cantidad where prodregistro = @prodregistro
				end
			fetch next from tmpC into @prodregistro
		end
	close tmpC
	deallocate tmpC
	deallocate tmpA
	if @intTipoReporte = 50
		begin
			select * from #tmpVLP order by proveedor, vendedor, producto
		end
	else if @intTipoReporte = 60
		begin
			select * from #tmpVLP order by vendedor, proveedor, producto
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngVendDept]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngVendDept]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngVendDept] @lngVendedor as tinyint, @lngDepartamento as tinyint, @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select regvendedor from prm_VendDept where regvendedor = @lngVendedor and regdepartamento = @lngDepartamento
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = regvendedor from prm_VendDept where regvendedor = @lngVendedor and regdepartamento = @lngDepartamento
			if @lngExiste = ''''
				begin
					insert into prm_VendDept values (@lngVendedor, @lngDepartamento, @intEstado)
				end
			else
				begin
					update prm_VendDept set estado = @intEstado where regvendedor = @lngVendedor and regdepartamento = @lngDepartamento
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_VerificarSaldoCliente]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_VerificarSaldoCliente]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_VerificarSaldoCliente] @lngCliente as smallint, @dblTotal as numeric(18,2)
As
declare	@saldo	numeric(18,2),
	@limite	numeric(18,2)
begin
	set nocount on
	set @saldo = 0
	set @limite = 0
	select @limite = limite , @saldo = saldo from prm_clientes where registro = @lngCliente
	if @saldo + @dblTotal > @limite
		begin
			select codigo from prm_clientes where registro = @lngCliente
		end
	else
		begin
			select codigo from prm_clientes where registro = -1
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerReciboRpt]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerReciboRpt]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_ExtraerReciboRpt] @intTipoRecibo as tinyint, @lngCliRegistro as smallint, @strNumeroRec as varchar(12), @lngNumFecha as int
As

begin
	set nocount on
	if @intTipoRecibo = 1 or @intTipoRecibo = 2 or @intTipoRecibo = 3
		begin
			select e.fecha, e.numero, e.monto montorec, e.descripcion descrec, c.codigo, c.nombre, c.direccion, c.cta_contable, d.descripcion, d.monto, d.interes, d.mantvlr, 
				e.numchqtarj, e.nombrebanco, e.reciboprov, e.descr01, e.descr02, e.descr03, e.estado
				from tbl_recibos_enc e, tbl_recibos_det d, prm_clientes c where e.registro = d.registro  and e.cliregistro = c.registro and e.cliregistro = @lngCliRegistro and 
				e.numero = @strNumeroRec and e.numfecha = @lngNumFecha and e.tiporecibo = @intTipoRecibo
		end
	else if @intTipoRecibo = 4 or @intTipoRecibo = 5 or @intTipoRecibo = 6
		begin
			select e.fecha, e.numero, e.monto montorec, e.descripcion descrec, c.codigo, c.nombre, c.direccion, c.cta_contable, d.descripcion, d.monto, d.interes, d.mantvlr, 
				e.numchqtarj, e.nombrebanco, e.reciboprov, e.descr01, e.descr02, e.descr03, e.estado
				from tbl_recibos_enc e, tbl_recibos_det d, prm_clientes c where e.registro = d.registro  and e.cliregistro = c.registro and e.cliregistro = @lngCliRegistro and 
				e.numero = @strNumeroRec and e.tiporecibo = (@intTipoRecibo - 3)
		end
	else if @intTipoRecibo = 9 or @intTipoRecibo = 10 or @intTipoRecibo = 11
		begin
			select e.fecha, e.numero, e.monto montorec, e.descripcion descrec, c.codigo, c.nombre, c.direccion, c.cta_contable, d.descripcion, d.monto, d.interes, d.mantvlr, 
				e.numchqtarj, e.nombrebanco, e.reciboprov, e.descr01, e.descr02, e.descr03, e.estado
				from tbl_recibos_enc e, tbl_recibos_det d, prm_clientes c where e.registro = d.registro  and e.cliregistro = c.registro and e.cliregistro = @lngCliRegistro and 
				e.numero = @strNumeroRec and e.tiporecibo = (@intTipoRecibo - 8)
		end
	else if @intTipoRecibo = 7 or @intTipoRecibo = 8 or @intTipoRecibo = 12
		begin
			select c.cuenta, c.descripcion, v.* from tbl_recibosvarios v, prm_ctascontables c where c.registro = v.ctacontable and v.ctacontable = @lngCliRegistro and 
				v.numero = @strNumeroRec
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRecibosVarios]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRecibosVarios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngRecibosVarios] @lngRegistro as tinyint, @strFechaFactura as varchar(12), @lngNumFecha as int, @lngCuenta as smallint, @strNumero as varchar(12), 
				@dblMonto as numeric(12,2), @strDescrip01 as text, @intAccion as tinyint, @dteFecha as smalldatetime

as

Declare	@lngRegistroTbl as bigint

begin
	set nocount on
	set @lngRegistroTbl = 0
	if @intAccion = 0
		begin
			select @lngRegistroTbl = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
			insert into tbl_recibosvarios values (@lngRegistroTbl, @lngCuenta, @strFechaFactura, @lngNumFecha, @strNumero, @dblMonto, @strDescrip01, 0, 0, @dteFecha)
		end
	else if @intAccion = 1
		begin
			select @lngRegistroTbl = registro from tbl_recibosvarios where ctacontable = @lngCuenta and numero = @strNumero and numfechaing = @lngNumFecha
			update tbl_recibosvarios set estado = 1, numfechaanul = @lngNumFecha where registro = @lngRegistroTbl
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngVendedores]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngVendedores]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngVendedores] @lngRegistro as tinyint, @strCodigo as varchar(12), @strNombre as varchar(50), @strLocalidad as varchar(10), @intEstado as tinyint, 
				@intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Vendedores where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Vendedores where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Vendedores
					insert into prm_Vendedores values (@lngRegistro, @strCodigo, @strNombre, @strLocalidad, @intEstado)
				end
			else
				begin
					update prm_Vendedores set nombre = @strNombre, localidad = @strLocalidad, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngFacturasDet]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngFacturasDet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngFacturasDet] @lngRegistro as tinyint, @lngProducto as smallint, @dblCantidad as numeric(12,2), @dblPrecio as numeric(12,2), 
				@dblValor as numeric(12,2), @dblIgv as numeric(12,2), @lngAgencia as tinyint, @lngNumFechaIng as numeric(8), @strNumero as varchar(12)
As

Declare	@lngRegistroTbl as bigint

begin
	set nocount on
	set @lngRegistroTbl = 0
	select @lngRegistroTbl = registro from tbl_Facturas_Enc where agenregistro = @lngAgencia and numfechaing = @lngNumFechaIng and numero = @strNumero
	insert into tbl_Facturas_Det values (@lngRegistroTbl, @lngProducto, @dblCantidad, @dblPrecio, @dblValor, @dblIgv)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngFacturasCorr]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngFacturasCorr]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngFacturasCorr] @lngAgencia as tinyint, @lngUsuario as tinyint, @lngFechaIng as int, @strFechaIng as varchar(12), @intTipoFactura as tinyint, 
				@strFactura as varchar(12), @lngProdRegistro_Old as smallint, @lngProdRegistro_New as smallint, @lngFechaFactura as int, 
				@dblCantidad as numeric(12,2), @lngNumTiempo as int
As

Declare	@lngRegistroTbl as bigint

begin
	set nocount on
	set @lngRegistroTbl = 0
	select @lngRegistroTbl = registro from tbl_Facturas_Enc where agenregistro = @lngAgencia and numfechaing = @lngFechaFactura and numero = @strFactura
	update tbl_facturas_det set prodregistro = @lngProdRegistro_New where registro = @lngRegistroTbl and prodregistro = @lngProdRegistro_Old
	insert into tbl_Facturas_Corregidas values (@lngAgencia, @lngUsuario, @lngFechaIng, @strFechaIng, @intTipoFactura, @strFactura, @lngProdRegistro_Old, @lngProdRegistro_New)
	exec sp_IngProductoMovim 0, @lngFechaIng, @lngNumTiempo, @lngAgencia, @lngProdRegistro_Old, @strFechaIng, @strFactura, ''CFE'', ''COR'', 0, @dblCantidad, 0, 0, '' '', '' '', @lngUsuario, 0, 0, 0, 1
	exec sp_IngProductoMovim 0, @lngFechaIng, @lngNumTiempo, @lngAgencia, @lngProdRegistro_New, @strFechaIng, @strFactura, ''CFS'', ''COR'', 0, @dblCantidad, 0, 0, '' '', '' '', @lngUsuario, 0, 0, 0, 1
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngConversiones]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngConversiones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngConversiones] @lngRegistro as tinyint, @dblCantidad01 as numeric(12,2), @lngUnidad01 as tinyint, @dblCantidad02 as numeric(12,2), 
					@lngUnidad02 as tinyint, @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste as tinyint

begin
	set nocount on
	if @intVerificar = 0
		begin
			select registro from prm_Conversiones where de_unidad = @lngUnidad01 and a_unidad = @lngUnidad02
		end
	else
		begin
			select @lngExiste = 0
			select @lngExiste = registro from prm_Conversiones where de_unidad = @lngUnidad01 and a_unidad = @lngUnidad02
			if @lngExiste = 0
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Conversiones
					insert into prm_Conversiones values (@lngRegistro, @dblCantidad01, @lngUnidad01, @dblCantidad02, @lngUnidad02, @intEstado)
				end
			else
				begin
					update prm_Conversiones set de_cantidad = @dblCantidad01, a_cantidad = @dblCantidad02 where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProductoConv]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProductoConv]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngProductoConv] @lngRegistro as tinyint, @strFechaConversion as varchar(12), @lngNumFecha as numeric(8), @strDocumento as varchar(12), 
					@dblCantidad01 as numeric(10,2), @lngProducto01 as smallint, @lngAgencia01 as tinyint, @dblCantidad02 as numeric(10,2), 
					@lngProducto02 as smallint, @lngAgencia02 as tinyint
As

Declare	@lngRegistroTbl as bigint

begin
	set nocount on
	select @lngRegistroTbl = 0
	select @lngRegistroTbl = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
	insert into tbl_ProductoConv values (@lngRegistroTbl, @strFechaConversion, @lngNumFecha, @strDocumento, @dblCantidad01, @lngProducto01, @lngAgencia01, @dblCantidad02, @lngProducto02, @lngAgencia02)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCtasContRecibos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCtasContRecibos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngCtasContRecibos] @intTipo as tinyint, @intRecCajCaj as smallint, @intRecCajInt as smallint, @intRecCajRet as smallint
As

begin
	set nocount on
	if @intTipo = 0
		begin
			truncate table prm_CtasContRecibos
			insert into prm_CtasContRecibos values (@intRecCajCaj, @intRecCajInt, @intRecCajRet)
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngChequesMovimientos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngChequesMovimientos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngChequesMovimientos] @intTipoCheque tinyint, @strregistro varchar(17), 
	@ctabanregistro tinyint, @benefregistro smallint, @beneficiario varchar(80), @descripcion varchar(200),
	@fechacheque smalldatetime, @fechaingreso smalldatetime, @documento varchar(12), @monto numeric(18,2),
	@numfechacheque int, @numfechaingreso int
as
declare	@lngregistro	bigint

begin
	-- 1, 5,  9, 13 son Cheques
	-- 2, 6, 10, 14 son Depositos
	-- 3, 7, 11, 15 son NTD
	-- 4, 8, 12, 16 son NTC
	set nocount on
	set @lngRegistro = 0
	if @intTipoCheque >= 1 and @intTipoCheque <= 4
		set @lngregistro = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
	else if @intTipoCheque >= 5 and @intTipoCheque <= 8
		set @lngregistro = convert(bigint, @strregistro)
	if @intTipoCheque >= 1 and @intTipoCheque <= 4
		insert into tbl_cheques_enc values (@lngregistro, @ctabanregistro, @benefregistro, 
			@beneficiario, @descripcion, @fechacheque, getdate(), @documento, @monto, 
			@intTipoCheque, 0, @numfechacheque, convert(numeric(8), convert(varchar(8), getdate(), 112)))
	else if @intTipoCheque = 5
		select c.registro, b.descripcion, c.codigo, e.registro, e.benefregistro, e.beneficiario,
			e.documento, e.fechacheque, e.descripcion, e.monto, m.descripcion, m.signo, e.estado from 
			tbl_cheques_enc e, prm_ctasbancarias c, prm_bancos b, cat_monedas m where 
			e.ctabanregistro = c.registro and c.bancoregistro = b.registro and e.registro = @lngRegistro 
			and e.tipomovim = 1 and c.monregistro = (m.registro -1)
	else if @intTipoCheque >= 6 and @intTipoCheque <= 8
		select c.registro, b.descripcion, c.codigo, e.registro, e.documento, e.fechacheque, e.descripcion, 
			e.monto, m.descripcion, e.beneficiario, e.estado from tbl_cheques_enc e, prm_ctasbancarias c, 
			prm_bancos b, cat_monedas m where e.ctabanregistro = c.registro and c.bancoregistro = b.registro 
			and e.registro = @lngRegistro and e.tipomovim = (@intTipoCheque - 4) and c.monregistro = (m.registro -1)
	else if @intTipoCheque = 9
		print ''hola''
	else if @intTipoCheque = 10
		print ''hola''
	else if @intTipoCheque = 11
		print ''hola''
	else if @intTipoCheque = 12
		print ''hola''
	else if @intTipoCheque = 13
		print ''hola''
	else if @intTipoCheque = 14
		print ''hola''
	else if @intTipoCheque = 15
		print ''hola''
	else if @intTipoCheque = 16
		print ''hola''
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCambiarNumero]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCambiarNumero]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngCambiarNumero] @lngRegistro as numeric(6), @lngAgencia as tinyint, @lngNumero as numeric(8), @strNumero as varchar(12), 
				@intTipoFactura as tinyint, @lngUsuario as tinyint, @intAccion as tinyint
As
declare	@lngRegistroTbl as varchar(20),
	@strEnc as varchar(12),
	@strFecha as varchar(12),
	@lngFecha as numeric(8)
begin
	set nocount on
	if @intAccion = 0
		begin
			set @strEnc = ''''
			set @strFecha = ''''
			set @lngFecha = 0
			set @strFecha = convert(varchar(12), getdate(), 113)
			set @lngFecha = convert(varchar(12), getdate(), 112)
			set @strFecha = REPLACE(@strFecha,'' '',''-'')
			set @strFecha = left(@strFecha, 11)
			select @strEnc = numero from tbl_facturas_enc where agenregistro = @lngAgencia and numero = @strNumero
			if @strEnc = ''''				-- Ingresar
				begin
					set @lngNumero = @lngNumero - 1
					set @lngRegistroTbl = ''''
					select @lngRegistroTbl = replace(substring(convert(varchar(20), getdate(), 113), 13, 8), '':'', '''')
					set @lngRegistro = convert(numeric(6), @lngRegistroTbl)
					if @intTipoFactura = 1
						begin
							update tbl_ultimonumero set contado = @lngNumero where agenregistro = @lngAgencia
							if @@rowcount = 0
								begin
									insert into tbl_ultimonumero values (@lngAgencia, @lngNumero, 0)
								end
							insert into tbl_cambiarnumfact values (@lngRegistro, @lngAgencia, 0, @strNumero, @lngNumero, @lngUsuario, @strFecha, @lngFecha, 0)
						end
					else if @intTipoFactura = 4
						begin
							update tbl_ultimonumero set credito = @lngNumero where agenregistro = @lngAgencia
							if @@rowcount = 0
								begin
									insert into tbl_ultimonumero values (@lngAgencia, 0, @lngNumero)
								end
							insert into tbl_cambiarnumfact values (@lngRegistro, @lngAgencia, 1, @strNumero, @lngNumero, @lngUsuario, @strFecha, @lngFecha, 0)
						end
					select 0
				end
			else
				begin
					select 1
				end
		end
	else
		begin
			update tbl_cambiarnumfact set estado = @intAccion where agenregistro = @lngAgencia and numero = @strNumero
			select 2
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_NumFactura]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_NumFactura]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_NumFactura] @lngAgencia as tinyint, @intTipo as tinyint
As

begin
	set nocount on
	if @intTipo = 1 or @intTipo = 2
		begin
			Update tbl_UltimoNumero Set Contado = (Contado + 1) Where AgenRegistro = @lngAgencia
			if @@rowcount = 0
				begin
					insert into tbl_UltimoNumero values (@lngAgencia, 1, 0)
				end
		end
	else if @intTipo = 4 or @intTipo = 5
		begin
			Update tbl_UltimoNumero Set Credito = (Credito + 1) Where AgenRegistro = @lngAgencia
			if @@rowcount = 0
				begin
					insert into tbl_UltimoNumero values (@lngAgencia, 0, 1)
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngMunicipios]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngMunicipios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngMunicipios] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(45), @lngRegistro2 as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Municipios where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Municipios where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Municipios
					insert into prm_Municipios values (@lngRegistro, @strCodigo, @strDescripcion, @lngRegistro2)
				end
			else
				begin
					update prm_Municipios set descripcion = @strDescripcion, registro2 = @lngRegistro2 where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerFactPend]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerFactPend]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_ExtraerFactPend] @lngAgencia as tinyint, @intTipoFactura as tinyint
As

begin
	set nocount on
	if @intTipoFactura = 2
		begin
			Select Numero, FechaIngreso, CliNombre From tbl_facturas_enc Where AgenRegistro = @lngAgencia and Impreso = 1 and Status = 0 and TipoFactura = 0 Order By NumFechaIng, Numero
		end
	else if @intTipoFactura = 5
		begin
			Select Numero, FechaIngreso, CliNombre From tbl_facturas_enc Where AgenRegistro = @lngAgencia and Impreso = 1 and Status = 0 and TipoFactura = 1 Order By NumFechaIng, Numero
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngBeneficiarios]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngBeneficiarios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_IngBeneficiarios] @lngRegistro as tinyint, @strCodigo as varchar(12), @strNombre as varchar(65), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Beneficiarios where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Beneficiarios where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Beneficiarios
					insert into prm_Beneficiarios values (@lngRegistro, @strCodigo, @strNombre, @intEstado)
				end
			else
				begin
					update prm_Beneficiarios set nombre = @strNombre, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngComisiones]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngComisiones]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngComisiones] @lngRegistro as tinyint, @intVendedor as tinyint, @strDescripcion as varchar(50), @intDia01 as smallint, @intDia02 as smallint, 
			@intPor01 as numeric(5,2), @intDia03 as smallint, @intDia04 as smallint, @intPor02 as numeric(5,2), @intDia05 as smallint, @intDia06 as smallint, @intPor03 as numeric(5,2), 
			@strClases as varchar(255), @strSubClases as varchar(255), @strInclProd as varchar(255), @strExclProd as varchar(255), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@strExiste varchar(50)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select descripcion from prm_Comisiones where descripcion = @strDescripcion and vendregistro = @intVendedor
		end
	else
		begin
			select @strExiste = ''''
			select @strExiste = descripcion from prm_Comisiones where descripcion = @strDescripcion and vendregistro = @intVendedor
			if @strExiste = '''' or @strExiste is null
				begin
					set @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Comisiones
					insert into prm_Comisiones values (@lngRegistro, @intVendedor, @strDescripcion, @intDia01, @intDia02, @intPor01, @intDia03, 
						@intDia04, @intPor02, @intDia05, @intDia06, @intPor03, @strClases, @strSubClases, @strInclProd, @strExclProd, @intEstado)
				end
			else
				begin
					update prm_Comisiones set descripcion = @strDescripcion, Dia01 = @intDia01, Dia02 = @intDia02, Por01 = @intPor01, 
						Dia03 = @intDia03, Dia04 = @intDia04, Por02 = @intPor02, Dia05 = @intDia05, Dia06 = @intDia06, Por03 = @intPor03, 
						Clases = @strClases, SubClases = @strSubClases, InclProd = @strInclProd, ExclProd = @strExclProd, estado = @intEstado 
						where descripcion = @strDescripcion and vendregistro = @intVendedor
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngTipoCambio]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngTipoCambio]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngTipoCambio] @strFecha as varchar(12), @dblCambio as numeric(13,4), @lngFecha as int, @intVerificar as tinyint
As

declare	@lngExiste numeric(8,0)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select numfecha from prm_TipoCambio where numfecha = @lngFecha
		end
	else
		begin
			select @lngExiste = 0
			select @lngExiste = numfecha from prm_TipoCambio where numfecha = @lngFecha
			if @lngExiste = 0
				begin
					insert into prm_TipoCambio values (@strFecha, @dblCambio, @lngFecha)
				end
			else
				begin
					update prm_TipoCambio set tipo_cambio = @dblCambio where numfecha = @lngFecha
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_ExtraerAbonosSubRpt]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_ExtraerAbonosSubRpt]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_ExtraerAbonosSubRpt] @lngCliRegistro as smallint, @strNumero as varchar(12), @intTipoReporte as tinyint
As
begin
	set nocount on
	if @intTipoReporte = 1 or @intTipoReporte = 2
		begin
			select e.fecha, e.numero, d.monto from tbl_recibos_enc e, tbl_recibos_det d where e.registro = d.registro and 
				e.cliregistro = @lngCliRegistro and d.numero = @strNumero order by e.numfecha
		end
	else if @intTipoReporte = 3 or @intTipoReporte = 4
		begin
			select e.fecha, e.numero, (d.monto / t.tipo_cambio) monto from tbl_recibos_enc e, tbl_recibos_det d, prm_TipoCambio t where e.registro = d.registro and 
				e.numfecha = t.numfecha and e.cliregistro = @lngCliRegistro and d.numero = @strNumero order by e.numfecha
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCtasContables]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCtasContables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngCtasContables] @lngRegistro as smallint, @strCuenta as varchar(12), 
		@strDescripcion as varchar(60), @intVerificar as tinyint
As

declare	@strExiste 	varchar(16)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select cuenta from prm_CtasContables where cuenta = @strCuenta
		end
	else
		begin
			select @strExiste = ''''
			select @strExiste = cuenta from prm_CtasContables where cuenta = @strCuenta
			if @strExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_CtasContables
					insert into prm_CtasContables values (@lngRegistro, @strCuenta, @strDescripcion)
				end
			else
				begin
					update prm_CtasContables set descripcion = @strDescripcion where 
						registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngProductoMovim]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngProductoMovim]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[sp_IngProductoMovim] @lngRegistro as tinyint, @lngNumFecha as numeric(8), @lngNumTiempo as numeric(6), @lngAgencia as tinyint, @lngProducto as smallint, 
				@strFecha as varchar(12), @strDocumento as varchar(12), @strTipoMov as varchar(3), @strMoneda as varchar(3), @dblFactura as numeric(12,2), 
				@dblCant as numeric(12,2), @dblCosto as numeric(12,2), @dblSaldo as numeric(12,2), @strDebe as varchar(15), @strHaber as varchar(15), 
				@lngUsuario as tinyint, @dblCantConv as numeric(12,2),  @lngProductoConv as smallint, @lngAgenciaConv as tinyint, @intOrigen as tinyint
As

declare	@dblCant01 as numeric(12,2),
	@dblCosto01 as numeric(12,2),
	@dblSaldo01 as numeric(12,2),
	@dblCant02 as numeric(12,2),
	@dblCosto02 as numeric(12,2),
	@dblSaldo02 as numeric(12,2),
	@dblCant03 as numeric(12,2),
	@dblCosto03 as numeric(12,2),
	@dblSaldo03 as numeric(12,2),
	@lngRegistroEnc as bigint,
	@dblCantFinal as numeric(12,2),
	@dblCostoFinal as numeric(12,2),
	@dblMontoFinal as numeric(12,2),
	@lngRegistroTbl as bigint

begin
	set nocount on
	set @dblCant01 = 0
	set @dblCant02 = 0
	set @dblCant03 = 0
	set @dblCosto01 = 0
	set @dblCosto02 = 0
	set @dblCosto03 = 0
	set @dblSaldo01 = 0
	set @dblSaldo02 = 0
	set @dblSaldo03 = 0
	set @lngRegistroEnc = 0
	set @dblCantFinal = 0
	set @dblCostoFinal = 0
	set @dblMontoFinal = 0
	set @lngRegistroTbl = 0
	select @lngRegistroTbl = (convert(numeric(8), convert(varchar(8), getdate(), 112)) * 1000000000) + convert(numeric(9), replace(replace(convert(varchar(13), getdate(), 114),'':'',''''),''.'',''''))
	if @strTipoMov = ''SF'' or @strTipoMov = ''SB'' or @strTipoMov = ''RC'' or @strTipoMov = ''CFS'' or @strTipoMov = ''SA'' or @strTipoMov = ''CKS''	-- Salidas por Facturas y/o Salidas por Baja y/o Remision de Clientes y/o Correccion de Factura Salida y/o Salida Agencia y/o Correccion Kardex Salida
		begin
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgencia and prodregistro = @lngProducto
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgencia and prodregistro = @lngProducto and registro = @lngRegistroEnc
					set @dblCant02 = @dblCant
					set @dblSaldo02 = (@dblCostoFinal * @dblCant)
					set @dblCosto02 = (@dblSaldo02 / @dblCant)
					set @dblCant03 = (@dblCantFinal - @dblCant)
					set @dblSaldo03 = (@dblMontoFinal - @dblSaldo02)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant02 = @dblCant
					set @dblSaldo02 = 0
					set @dblCosto02 = 0
					set @dblCant03 = (0 - @dblCant)
					set @dblSaldo03 = 0
					set @dblCosto03 = 0
				end
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblFactura, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''TO''						-- Salidas por Traslado
		begin
			-- Ingreso del Registro Salida (TO)
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgencia and prodregistro = @lngProducto
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgencia and prodregistro = @lngProducto and registro = @lngRegistroEnc
					set @dblCant02 = @dblCant
					set @dblSaldo02 = (@dblCostoFinal * @dblCant)
					set @dblCosto02 = (@dblSaldo02 / @dblCant)
					set @dblCant03 = (@dblCantFinal - @dblCant)
					set @dblSaldo03 = (@dblMontoFinal - @dblSaldo02)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant02 = @dblCant
					set @dblSaldo02 = 0
					set @dblCosto02 = 0
					set @dblCant03 = (0 - @dblCant)
					set @dblSaldo03 = 0
					set @dblCosto03 = 0
				end
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblSaldo02, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)

			-- Ingreso del Registro Entrada (TD)
			set @strTipoMov = ''''
			set @strTipoMov = ''TD''
			set @dblCant01 = 0
			set @dblCant03 = 0
			set @dblCosto01 = 0
			set @dblCosto03 = 0
			set @dblSaldo01 = 0
			set @dblSaldo03 = 0
			set @lngRegistroEnc = 0
			set @dblCantFinal = 0
			set @dblCostoFinal = 0
			set @dblMontoFinal = 0
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgenciaConv and prodregistro = @lngProductoConv
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgenciaConv and prodregistro = @lngProductoConv and registro = @lngRegistroEnc
					set @dblCant01 = @dblCant02
					set @dblSaldo01 = @dblSaldo02
					set @dblCosto01 = @dblCosto02
					set @dblCant03 = (@dblCantFinal + @dblCant01)
					set @dblSaldo03 = (@dblMontoFinal + @dblSaldo01)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant01 = @dblCant02
					set @dblSaldo01 = @dblSaldo02
					set @dblCosto01 = @dblCosto02
					set @dblCant03 = @dblCant02
					set @dblSaldo03 = @dblSaldo02
					set @dblCosto03 = @dblCosto02
				end
			set @dblCant02 = 0
			set @dblCosto02 = 0
			set @dblSaldo02 = 0
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgenciaConv, @lngProductoConv, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblSaldo01, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''CO''						-- Salidas por Conversion
		begin
			-- Ingreso del Registro Salida (CD)
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgencia and prodregistro = @lngProducto
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgencia and prodregistro = @lngProducto and registro = @lngRegistroEnc
					set @dblCant02 = @dblCant
					set @dblSaldo02 = (@dblCostoFinal * @dblCant)
					set @dblCosto02 = (@dblSaldo02 / @dblCant)
					set @dblCant03 = (@dblCantFinal - @dblCant)
					set @dblSaldo03 = (@dblMontoFinal - @dblSaldo02)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant02 = @dblCant
					set @dblSaldo02 = 0
					set @dblCosto02 = 0
					set @dblCant03 = (0 - @dblCant)
					set @dblSaldo03 = 0
					set @dblCosto03 = 0
				end
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblSaldo02, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)

			-- Ingreso del Registro Entrada (CD)
			set @strTipoMov = ''''
			set @strTipoMov = ''CD''
			set @dblCant01 = 0
			set @dblCant03 = 0
			set @dblCosto01 = 0
			set @dblCosto03 = 0
			set @dblSaldo01 = 0
			set @dblSaldo03 = 0
			set @lngRegistroEnc = 0
			set @dblCantFinal = 0
			set @dblCostoFinal = 0
			set @dblMontoFinal = 0
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgenciaConv and prodregistro = @lngProductoConv
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgenciaConv and prodregistro = @lngProductoConv and registro = @lngRegistroEnc
					set @dblCant01 = @dblCantConv
					set @dblSaldo01 =  @dblSaldo02
					set @dblCosto01 = (@dblSaldo01 / @dblCantConv)
					set @dblCant03 = (@dblCantFinal + @dblCantConv)
					set @dblSaldo03 = (@dblMontoFinal + @dblSaldo01)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant01 = @dblCantConv
					set @dblSaldo01 = @dblSaldo02
					set @dblCosto01 = (@dblSaldo01 / @dblCantConv)
					set @dblCant03 = @dblCantConv
					set @dblSaldo03 = @dblSaldo01
					set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			set @dblCant02 = 0
			set @dblCosto02 = 0
			set @dblSaldo02 = 0
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgenciaConv, @lngProductoConv, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblSaldo01, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''EC'' or @strTipoMov = ''EA'' or @strTipoMov = ''CKE''						-- Entradas por Compras y/o Entrada Agencia y/o Correccion Kardex Entrada
		begin
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgencia and prodregistro = @lngProducto
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgencia and prodregistro = @lngProducto and registro = @lngRegistroEnc
					set @dblCant01 = @dblCant
					set @dblSaldo01 = @dblSaldo
					set @dblCosto01 = @dblCosto
					set @dblCant03 = (@dblCantFinal + @dblCant)
					set @dblSaldo03 = (@dblMontoFinal + @dblSaldo01)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						set @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant01 = @dblCant
					set @dblSaldo01 = @dblSaldo
					set @dblCosto01 = @dblCosto
					set @dblCant03 = @dblCant
					set @dblSaldo03 = @dblSaldo
					set @dblCosto03 = @dblCosto
				end
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblFactura, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''AF'' or @strTipoMov = ''DC'' or @strTipoMov = ''CFE''			-- Entradas por Anulacion de Facturas y/o Devolucion del Cliente y/o Correccion de Factura Entrada
		begin
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgencia and prodregistro = @lngProducto
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgencia and prodregistro = @lngProducto and registro = @lngRegistroEnc
					set @dblCant01 = @dblCant
					set @dblSaldo01 = (@dblCostoFinal * @dblCant)
					set @dblCosto01 = (@dblSaldo01 / @dblCant)
					set @dblCant03 = (@dblCantFinal + @dblCant)
					set @dblSaldo03 = (@dblMontoFinal + @dblSaldo01)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						select @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant01 = @dblCant
					set @dblSaldo01 = @dblSaldo
					set @dblCosto01 = @dblCosto
					set @dblCant03 = @dblCant
					set @dblSaldo03 = @dblSaldo
					set @dblCosto03 = @dblCosto
				end
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblFactura, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''CC''								-- Corregir Costos
		begin
			select @lngRegistroEnc = isnull(max(registro), 0) from tbl_producto_movimiento where agenregistro = @lngAgencia and prodregistro = @lngProducto
			if @lngRegistroEnc > 0
				begin
					select @dblCantFinal = saldo_cnt, @dblCostoFinal = saldo_cto, @dblMontoFinal = saldo_mto from tbl_producto_movimiento where 
											agenregistro = @lngAgencia and prodregistro = @lngProducto and registro = @lngRegistroEnc
					set @dblCant01 = 0
					set @dblSaldo01 = @dblSaldo
					set @dblCosto01 = 0
					set @dblCant03 = @dblCantFinal
					set @dblSaldo03 = (@dblMontoFinal + @dblSaldo01)
					if @dblCant03 = 0
						begin
							set @dblCosto03 = @dblCostoFinal
							set @dblSaldo03 = 0
						end
					else
						select @dblCosto03 = (@dblSaldo03 / @dblCant03)
				end
			else
				begin
					set @dblCant01 = @dblCant
					set @dblSaldo01 = @dblSaldo
					set @dblCosto01 = @dblCosto
					set @dblCant03 = @dblCant
					set @dblSaldo03 = @dblSaldo
					set @dblCosto03 = @dblCosto
				end
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, 
					@strDocumento, @strTipoMov, @strMoneda, @dblFactura, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02,
					@dblCant03, @dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''AJE'' or @strTipoMov = ''AJS''						-- Ajuste Entrada y/o Ajuste Salida
		begin
			if @strTipoMov = ''AJS''
				begin
						set @dblCant02 = @dblCant
						set @dblSaldo02 = @dblSaldo
						set @dblCosto02 = @dblCosto
				end
			else if @strTipoMov = ''AJE''
				begin
						set @dblCant01 = @dblCant
						set @dblSaldo01 = @dblSaldo
						set @dblCosto01 = @dblCosto
				end
				set @dblCant03 = 0
				set @dblSaldo03 = 0
				set @dblCosto03 = @dblCosto
				insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, @lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, @strDocumento, 
						@strTipoMov, @strMoneda, @dblFactura, @dblCant01, @dblCosto01, @dblSaldo01, @dblCant02, @dblCosto02, @dblSaldo02, @dblCant03, 
						@dblCosto03, @dblSaldo03, @strDebe, @strHaber, @lngUsuario, @intOrigen)
		end
	else if @strTipoMov = ''II''						-- Inventario Inicial
		begin
			insert into tbl_Producto_Movimiento values (@lngRegistroTbl, @lngNumFecha, 
					@lngNumTiempo, @lngAgencia, @lngProducto, @strFecha, @strDocumento, 
					@strTipoMov, @strMoneda, @dblFactura, @dblCant, @dblCosto, 
					@dblSaldo, 0, 0, 0, @dblCant, @dblCosto, @dblSaldo, @strDebe, 
					@strHaber, @lngUsuario, @intOrigen)
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DetalleProductoFactura]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DetalleProductoFactura]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_DetalleProductoFactura] @strDescrip as varchar(15), @intAgencia as tinyint
As

declare	@dblCantidad	numeric(12,2),
	@intProdReg	smallint,
	@intRegistro	bigint,
	@intFecha	numeric(8)

begin
	set nocount on
	set @dblCantidad = 0
	set @intProdReg = 0
	set @intRegistro = 0
	set @intFecha = 0
	set @strDescrip = @strDescrip + ''%''
	create table #tmp
	(
		regist	smallint,
		codigo	varchar(12),
		descrip	varchar(60),
		unidad	varchar(12),
		cantid	numeric(12,2),
		pvpc	numeric(12,2),
		pvdc	numeric(12,2),
		pvpu	numeric(12,2),
		pvdu	numeric(12,2)
	)
	insert into #tmp select P.Registro, P.Codigo, P.Descripcion, U.Codigo, 0, P.PVPC, P.PVDC, P.PVPU, P.PVDU From prm_Productos p, 
		prm_Unidades u Where p.RegUnidad = u.registro and p.Estado = 0 And p.Descripcion Like @strDescrip Order By p.Descripcion
	declare tmpC cursor for
		select regist from #tmp
	open tmpC
	fetch next from tmpC into @intProdReg
	while @@fetch_status = 0
		begin
			set @intFecha = 0
			set @dblCantidad = 0
			set @intRegistro = 0
			select @intFecha = max(numfechaing) from tbl_producto_movimiento where agenregistro = @intAgencia and prodregistro = @intProdReg
			select @intRegistro = max(registro) from tbl_producto_movimiento where agenregistro = @intAgencia and prodregistro = @intProdReg and numfechaing = @intFecha
			if @intRegistro <> 0
				select @dblCantidad = saldo_cnt from tbl_producto_movimiento where agenregistro = @intAgencia and prodregistro = @intProdReg and numfechaing = @intFecha 
					and registro = @intRegistro
			update #tmp set cantid = @dblCantidad where regist = @intProdReg
			fetch next from tmpC into @intProdReg
		end
	close tmpC
	deallocate tmpC
	select codigo, descrip, unidad, cantid, pvpc, pvdc, pvpu, pvdu from #tmp order by descrip
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngLiqProdEnc]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngLiqProdEnc]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngLiqProdEnc] @lngRegistro as numeric(14), @strFechaFactura as varchar(12), @lngNumFecha as numeric(8), @intCantDias as tinyint, 
					@strNumPedido as varchar(12), @lngProveedor as tinyint, @intTipoPedido as tinyint, @strNumFactura as varchar(12), 
					@dblMonto as numeric(10,2), @dblDAI as numeric(10,2), @dblIVA as numeric(10,2), @dblTSIM as numeric(10,2), 
					@dblConstante as numeric(10,2), @dblCstoIntrod as numeric(10,2), @intVerificar as tinyint
As

declare	@strExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select numfactura from tbl_LiqProd_Enc where numfactura = @strNumFactura and provregistro = @lngProveedor
		end
	else
		begin
			select @strExiste = ''''
			select @strExiste = numfactura from tbl_LiqProd_Enc where numfactura = @strNumFactura and provregistro = @lngProveedor
			if @strExiste = ''''
				begin
					insert into tbl_LiqProd_Enc values (@lngRegistro, @strFechaFactura, @lngNumFecha, @intCantDias, @strNumPedido, @lngProveedor, @intTipoPedido, 
								@strNumFactura, @dblMonto, @dblDAI, @dblIVA, @dblTSIM, @dblConstante, @dblCstoIntrod)
				end
			else
				begin
					update tbl_LiqProd_Enc set fechafactura = @strFechaFactura, numfecha = @lngNumFecha, cantdias = @intCantDias, numpedido = @strNumPedido, provregistro = @lngProveedor,
								tipopedido = @intTipoPedido, numfactura = @strNumFactura, monto = @dblMonto, dai = @dblDAI, iva = @dblIVA, tsim = @dblTSIM, constante = @dblConstante, 
								costointrod = @dblCstoIntrod where numfactura = @strNumFactura and provregistro = @lngProveedor
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngEmpresa]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngEmpresa]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngEmpresa] @strNombre varchar(60), @strTelefono varchar(8)

As

begin
	set nocount on
	truncate table prm_Empresa
	insert into prm_Empresa values (@strNombre, @strTelefono)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCambioPrecios]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCambioPrecios]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngCambioPrecios] @lngRegistro as numeric(6), @strFechaFactura as varchar(12), @lngAgencia as tinyint, @lngUsuario as tinyint,  
					@lngProducto as smallint, @intTipoFactura as tinyint, @strNumero as varchar(12), @intNumPrecio as tinyint, 
					@dblPrecioOrig as numeric(12,2), @dblPrecioMod as numeric(12,2), @lngNumFecha as numeric(8)
As

begin
	set nocount on
	insert into tbl_CambiarPrecios values (@lngRegistro, @strFechaFactura, @lngAgencia, @lngUsuario, @lngProducto, @intTipoFactura, @strNumero, @intNumPrecio, @dblPrecioOrig, @dblPrecioMod, @lngNumFecha)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngRazonesAnular]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngRazonesAnular]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngRazonesAnular] @lngRegistro as tinyint, @intModulo as tinyint, @strDescripcion as varchar(60), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste as tinyint

begin
	set nocount on
	if @intVerificar = 0
		begin
			select registro from prm_RazonesAnular where modulo = @intModulo and descripcion = @strDescripcion
		end
	else
		begin
			select @lngExiste = 0
			select @lngExiste = registro from prm_RazonesAnular where modulo = @intModulo and descripcion = @strDescripcion
			if @lngExiste = 0
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Razones_Anular
					insert into prm_RazonesAnular values (@lngRegistro, @intModulo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_RazonesAnular set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngSubClases]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngSubClases]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngSubClases] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(45), @lngRegistro2 as tinyint, @intFacturar as tinyint, 
				@intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_SubClases where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_SubClases where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_SubClases
					insert into prm_SubClases values (@lngRegistro, @strCodigo, @strDescripcion, @lngRegistro2, @intFacturar, @intEstado)
				end
			else
				begin
					update prm_SubClases set descripcion = @strDescripcion, facturar = @intFacturar, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngAreas]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngAreas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngAreas] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(30), @intEstado as tinyint, @intVerificar as tinyint
As
declare	@lngExiste varchar(12)
begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Areas where codigo = @strCodigo
		end
	else
		begin
			set @lngExiste = ''''
			select @lngExiste = codigo from prm_Areas where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					set @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Areas
					insert into prm_Areas values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_Areas set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngLiqProdDet]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngLiqProdDet]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngLiqProdDet] @lngRegistro as numeric(10), @lngProducto as smallint, @dblCantidad as numeric(10,2), @dblMontoFob as numeric(12,2), 
					@intEstado as tinyint, @lngRegistro2 as numeric(14)
As

begin
	set nocount on
	insert into tbl_LiqProd_Det values (@lngRegistro, @lngProducto, @dblCantidad, @dblMontoFob, @intEstado, @lngRegistro2)
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngChequeras]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngChequeras]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngChequeras] @lngRegistro as tinyint, @intSerie as tinyint, @intInicial as int, @intFinal as int, @intCtaBanc as tinyint, @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste tinyint

begin
	set nocount on
	if @intVerificar = 0
		begin
			select serie from prm_Chequeras where serie = @intSerie and ckinicial = @intInicial and ckfinal = @intFinal and ctabancregistro = @intCtaBanc
		end
	else
		begin
			select @lngExiste = 50
			select @lngExiste = serie from prm_Chequeras where serie = @intSerie and ckinicial = @intInicial and ckfinal = @intFinal and ctabancregistro = @intCtaBanc
			print @lngExiste 
			if @lngExiste = 50
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Chequeras
					insert into prm_Chequeras values (@lngRegistro, @intSerie, @intInicial, @intFinal, 0, @intCtaBanc, @intEstado)
				end
			else
				begin
					update prm_Chequeras set ctabancregistro = @intCtaBanc, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPresupuestos]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPresupuestos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp_IngPresupuestos] @intYears as smallint, @lngVendRegistro as tinyint, @lngProdRegistro as smallint, @intAccion as tinyint, 
		@dblMes01 as numeric(12,2), @dblMes02 as numeric(12,2), @dblMes03 as numeric(12,2), @dblMes04 as numeric(12,2), @dblMes05 as numeric(12,2), @dblMes06 as numeric(12,2),
		@dblMes07 as numeric(12,2), @dblMes08 as numeric(12,2), @dblMes09 as numeric(12,2), @dblMes10 as numeric(12,2), @dblMes11 as numeric(12,2), @dblMes12 as numeric(12,2),
		@dblPrecio as numeric(8,2)

AS

BEGIN
	set nocount on
	if @intAccion = 0
		begin
			select * from tbl_presupuestos where years = @intYears and vendregistro = @lngVendRegistro
		end
	else if @intAccion = 1
		begin
			select vendregistro from tbl_presupuestos where years = @intYears and vendregistro = @lngVendRegistro
		end
	else if @intAccion = 2
		begin
			delete from tbl_presupuestos where years = @intYears and vendregistro = @lngVendRegistro
		end
	else if @intAccion = 3
		begin
			delete from tbl_presupuestos where years = @intYears and vendregistro = @lngVendRegistro
				and prodregistro = @lngProdRegistro
			insert into tbl_presupuestos values (@intYears, @lngVendRegistro, @lngProdRegistro, @dblPrecio, @dblMes01, @dblMes02, @dblMes03, @dblMes04,
				@dblMes05, @dblMes06, @dblMes07, @dblMes08, @dblMes09, @dblMes10, @dblMes11, @dblMes12)
		end
	set nocount off
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngCtasBancarias]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngCtasBancarias]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngCtasBancarias] @lngRegistro as tinyint, @strCodigo as varchar(20), @strDescripcion as varchar(40), @intBanco as tinyint, @intMoneda as tinyint, 
			@intFecApertura as int, @dblBlnInicial as numeric(18,2), @dblBlnActual as numeric(18,2), @intCtaContable as smallint, @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_CtasBancarias where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_CtasBancarias where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_CtasBancarias
					insert into prm_CtasBancarias values (@lngRegistro, @strCodigo, @strDescripcion, @intBanco, @intMoneda, @intFecApertura, @dblBlnInicial, 
						@dblBlnActual, @intCtaContable, @intEstado)
				end
			else
				begin
					update prm_CtasBancarias set descripcion = @strDescripcion, bancoregistro = @intBanco, monregistro = @intMoneda, numfecaper = @intFecApertura, 
						blninicial = @dblBlnInicial, blnactual = @dblBlnActual, ctacontregistro = @intCtaContable, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngBancos]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngBancos]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngBancos] @lngRegistro as tinyint, @strCodigo as varchar(12), @strDescripcion as varchar(50), @intEstado as tinyint, @intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Bancos where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Bancos where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Bancos
					insert into prm_Bancos values (@lngRegistro, @strCodigo, @strDescripcion, @intEstado)
				end
			else
				begin
					update prm_Bancos set descripcion = @strDescripcion, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngEmpleados]    Script Date: 05/10/2009 21:20:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngEmpleados]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngEmpleados] @lngRegistro as tinyint, @strCodigo as varchar(12), @strNombres as varchar(30), @strPriApellido as varchar(30), @strSegApellido as varchar(30), 
			@strSolApellido as varchar(30),@strDireccion as varchar(80), @strTelefono as varchar(20), @intGenero as tinyint, @intCivil as tinyint, @intDepend as tinyint, 
			@strFechaNac as varchar(12), @intNumFechaNac as int, @dteFechaNac as smalldatetime, @strFechaIng as varchar(12), @intNumFechaIng as int, 
			@dteFechaIng as smalldatetime,  @intIdentificacion as tinyint, @strNumIdentif as varchar(20), @strNumRegistro as varchar(20), @intNumFecEmi as int, 
			@intNumFecVen as int, @strNumInss as varchar(20), @intAreaRegistro as tinyint, @strJefe as varchar(60), @intPuestoRegistro as tinyint, @intEstado as tinyint, 
			@intVerificar as tinyint
As

declare	@lngExiste varchar(12)

begin
	set nocount on
	if @intVerificar = 0
		begin
			select codigo from prm_Empleados where codigo = @strCodigo
		end
	else
		begin
			select @lngExiste = ''''
			select @lngExiste = codigo from prm_Empleados where codigo = @strCodigo
			if @lngExiste = ''''
				begin
					select @lngRegistro = 0
					select @lngRegistro = isnull(max(registro), 0) + 1 from prm_Empleados
					insert into prm_Empleados values (@lngRegistro, @strCodigo, @strNombres, @strPriApellido, @strSegApellido, @strSolApellido, @strDireccion, 
						@strTelefono, @intGenero, @intCivil, @intDepend, @strFechaNac, @intNumFechaNac, @dteFechaNac, @strFechaIng, @intNumFechaIng,
						@dteFechaIng, @intIdentificacion, @strNumIdentif, @strNumRegistro, @intNumFecEmi, @intNumFecVen, @strNumInss, @intAreaRegistro, 
						@strJefe, @intPuestoRegistro, @intEstado)
				end
			else
				begin
					update prm_Empleados set nombres = @strNombres, priapellido = @strPriApellido, segapellido = @strSegApellido, soltapellido = @strSolApellido,
						direccion = @strDireccion, telefono = @strTelefono, genregistro = @intGenero, civilregistro = @intCivil, dependientes = @intDepend,
						fechanac = @strFechaNac, numfechanac = @intNumFechaNac, dtefechanac = @dteFechaNac, fechaing = @strFechaIng,
						numfechaing = @intNumFechaIng, dtefechaing = @dteFechaIng, identregistro = @intIdentificacion, numidentif = @strNumIdentif, 
						numregidentif = @strNumRegistro, numfechaemi = @intNumFecEmi, numfechaven = @intNumFecVen, numinss = @strNumInss, 
						arearegistro = @intAreaRegistro, jefe = @strJefe, pstoregistro = @intPuestoRegistro, estado = @intEstado where registro = @lngRegistro
				end
		end
	set nocount off
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_IngPorcentajes2]    Script Date: 05/10/2009 21:20:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_IngPorcentajes2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[sp_IngPorcentajes2] @dblRet as numeric(8,2)
As

begin
	set nocount on
	truncate table prm_Porcentajes2
	insert into prm_Porcentajes2 values (@dblRet)
	set nocount off
end
' 
END
GO
