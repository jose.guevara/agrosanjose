-- Agencias		Departamentos	Unidades	Clases		Empaques		Negocios		Vendedores		Clientes		Proveedores		TipoCambio
-- Usuarios		Productos		VendDepart	UsuarAgen	Conversiones	ProdMovimiento	ProdConversion	CambiarPrecios	Facturas		FacturasDetalle
-- UltimaFactura

set nocount on
go
truncate table agrocentro2k..prm_agencias
go
truncate table agrocentro2k..prm_departamentos
go
truncate table agrocentro2k..prm_unidades
go
truncate table agrocentro2k..prm_clases
go
truncate table agrocentro2k..prm_empaques
go
truncate table agrocentro2k..prm_negocios
go
truncate table agrocentro2k..prm_vendedores
go
truncate table agrocentro2k..prm_clientes
go
truncate table agrocentro2k..prm_proveedores
go
truncate table agrocentro2k..prm_tipocambio
go
truncate table agrocentro2k..prm_usuarios
go
truncate table agrocentro2k..prm_productos
go
truncate table agrocentro2k..prm_conversiones
go
truncate table agrocentro2k..prm_venddept
go
truncate table agrocentro2k..prm_usuaragenc
go
truncate table agrocentro2k..tbl_producto_movimiento
go
truncate table agrocentro2k..tbl_ProductoConv
go
truncate table agrocentro2k..tbl_CambiarPrecios
go
truncate table agrocentro2k..tbl_facturas_enc
go
truncate table agrocentro2k..tbl_facturas_det
go
truncate table agrocentro2k..tbl_ultimonumero
go
truncate table agrocentro2k..tbl_recibos_enc
go
truncate table agrocentro2k..tbl_recibos_det
go
truncate table agrocentro2k..tbl_cliente_oblig
go
print 'Importar Datos de las Agencias'
declare	@registro	tinyint,
	@codigo		int,
	@descripcion	varchar(30),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpAgencia cursor for
		select CodigoSucursal, Nombre from agrocentro..sucursales

	open tmpAgencia
	fetch next from tmpAgencia into @codigo, @descripcion
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			if @registro = 1
				insert into agrocentro2k..prm_agencias values(@registro, @codigo, @descripcion, 0, 0, 0)
			else
				insert into agrocentro2k..prm_agencias values(@registro, @codigo, @descripcion, 1, 1, 0)
			fetch next from tmpAgencia into @codigo, @descripcion
		end
	close tmpAgencia
	deallocate tmpAgencia
end
go
print 'Importar Datos de los Departamentos'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@desc_corto	varchar(12)

begin
	set @registro = 0
	declare tmpUnidades cursor for
		select CodigoDep, Departamento, CodigoCorto from agrocentro..departamentos

	open tmpUnidades
	fetch next from tmpUnidades into @codigo, @descripcion, @desc_corto
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_departamentos values(@registro, @codigo, @descripcion, @desc_corto, 0)
			fetch next from tmpUnidades into @codigo, @descripcion, @desc_corto
		end
	close tmpUnidades
	deallocate tmpUnidades
end
go
print 'Importar Datos de las Unidades'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpUnidades cursor for
		select CodigoUnidad, Unidad from agrocentro..unidades

	open tmpUnidades
	fetch next from tmpUnidades into @codigo, @descripcion
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_unidades values(@registro, @codigo, @descripcion, 0)
			fetch next from tmpUnidades into @codigo, @descripcion
		end
	close tmpUnidades
	deallocate tmpUnidades
end
go
print 'Importar Datos de las Clases'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpClases cursor for
		select CodigoClase, Nombre from agrocentro..clases

	open tmpClases
	fetch next from tmpClases into @codigo, @descripcion
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_clases values(@registro, @codigo, @descripcion, 0)
			fetch next from tmpClases into @codigo, @descripcion
		end
	close tmpClases
	deallocate tmpClases
end
insert into agrocentro2k..prm_clases values(0, '00', 'GENERICOS', 0)
insert into agrocentro2k..prm_clases values(13, '13', 'REPUESTOS', 0)
go
print 'Importar Datos de los Empaques'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpEmpaques cursor for
		select CodigoEmpaque, Nombre from agrocentro..empaques

	open tmpEmpaques
	fetch next from tmpEmpaques into @codigo, @descripcion
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_empaques values(@registro, @codigo, @descripcion, 0)
			fetch next from tmpEmpaques into @codigo, @descripcion
		end
	close tmpEmpaques
	deallocate tmpEmpaques
end
go
print 'Importar Datos de los Negocios'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpNegocios cursor for
		select CodigoNegocio, Nombre from agrocentro..negocios

	open tmpNegocios
	fetch next from tmpNegocios into @codigo, @descripcion
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_negocios values(@registro, @codigo, @descripcion, 0, 0, 0, 0)
			fetch next from tmpNegocios into @codigo, @descripcion
		end
	close tmpNegocios
	deallocate tmpNegocios
end
insert into agrocentro2k..prm_negocios values (0, '0','GENERICO',0, 0, 0, 0)
go
print 'Importar Datos de los Vendedores'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@estado		varchar(12)

begin
	set @registro = 0
	declare tmpVendedores cursor for
		select CodigoVendedor, Vendedor, Estado from agrocentro..vendedores where estado = 'activo'

	open tmpVendedores
	fetch next from tmpVendedores into @codigo, @descripcion, @estado
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_vendedores values(@registro, @codigo, @descripcion, 'V. RUTA', 0)
			fetch next from tmpVendedores into @codigo, @descripcion, @estado
		end
	close tmpVendedores
	deallocate tmpVendedores
	declare tmpVendedores cursor for
		select CodigoVendedor, Vendedor, Estado from agrocentro..vendedores where estado = 'inactivo'

	open tmpVendedores
	fetch next from tmpVendedores into @codigo, @descripcion, @estado
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_vendedores values(@registro, @codigo, @descripcion, 'V. RUTA', 2)
			fetch next from tmpVendedores into @codigo, @descripcion, @estado
		end
	close tmpVendedores
	deallocate tmpVendedores
end
insert into agrocentro2k..prm_vendedores values (0, '00','GENERICO','C. MATRIZ',0)
go
print 'Importar Datos de los Clientes'
declare	@registro 	smallint,
	@codigo 	varchar(12),
	@cliente 	varchar(50),
	@direccion 	varchar(60),
	@depart 	varchar(12),
	@deptreg 	tinyint,
	@telefono	varchar(30),
	@fax		varchar(30),
	@email		varchar(30),
	@limite		numeric(18,2),
	@dias_cre	tinyint,
	@saldo		numeric(18,2),
	@vendedor 	varchar(12),
	@vendreg 	tinyint,
	@fecha		smalldatetime,
	@numfecha	numeric(8),
	@cta_cont	varchar(16),
	@negocio 	varchar(12),
	@negreg 	tinyint,
	@tipo_id	tinyint,
	@num_id		varchar(20)

begin
	set @registro = 0
	set @tipo_id = 0
	declare tmpCliente cursor for
		select cod_cli, nom_cli, dir_cli, cod_dep, tel_cli, fax_cli, email_cli, lim_cli, dia_cre, sal_cli, 
			cod_ven, fec_ing, cta_con, cod_neg, num_ced from agrocentro2k..mae_cli

	open tmpCliente
	fetch next from tmpCliente into @codigo, @cliente, @direccion, @depart, @telefono, @fax, @email, @limite, @dias_cre, @saldo, 
			@vendedor, @fecha, @cta_cont, @negocio, @num_id
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			set @deptreg = 0
			set @vendreg = 0
			set @negreg = 0
			set @numfecha = 0
			set @tipo_id = 0
			select @deptreg = isnull(registro,0) from agrocentro2k..prm_Departamentos where codigo = @depart
			select @negreg = isnull(registro,0) from agrocentro2k..prm_Negocios where codigo = @negocio
			select @vendreg = isnull(registro,0) from agrocentro2k..prm_Vendedores where codigo = @vendedor
			select @numfecha = convert(numeric(8), convert(varchar(12), @fecha, 112))
			if @num_id <> ''
				set @tipo_id = 1
			if @num_id is null
				set @num_id = ''
			if @telefono is null
				set @telefono = ''
			if @email is null
				set @email = ''
			insert into agrocentro2k..prm_clientes Values (@registro, @codigo, 0, @tipo_id, @num_id, @cliente, 
					convert(varchar(12), @fecha, 113), @numfecha, @direccion, @negreg, @vendreg, @deptreg, 0, 
					@telefono, @fax, @email, '', '', '', '', @limite, @saldo, @dias_cre, @cta_cont, 0, 0)
			fetch next from tmpCliente into @codigo, @cliente, @direccion, @depart, @telefono, @fax, @email, @limite, @dias_cre, @saldo, 
					@vendedor, @fecha, @cta_cont, @negocio, @num_id
		end
	close tmpCliente
	deallocate tmpCliente
	insert into agrocentro2k..prm_clientes Values (@registro + 1, 'CONT', 1, 5, 'ruc', 'AGROCENTRO', '01-Jan-2000', 20000101, 'CASA MATRIZ', 
		2, 0, 11, 0, '0', '0', '', '', '', '', '', 0, 0, 0, 0, 0, 0)
end
go
update agrocentro2k..prm_clientes set fechaingreso = substring(replace(fechaingreso, ' ', '-'),1,11)
go
-- select * from agrocentro2k..prm_clientes
update agrocentro2k..prm_clientes set deptregistro = 15 where deptregistro = 0
go
print 'Importar Datos de los Proveedores'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@descripcion	varchar(30),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpProveedores cursor for
		select CodigoProveedor, Nombre from agrocentro..proveedores

	open tmpProveedores
	fetch next from tmpProveedores into @codigo, @descripcion
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_proveedores values(@registro, @codigo, @descripcion, 0, 0)
			fetch next from tmpProveedores into @codigo, @descripcion
		end
	close tmpProveedores
	deallocate tmpProveedores
end
go
insert into agrocentro2k..prm_proveedores values(0, 'GEN', 'GENERICO', 0, 0)
go
go
print 'Importar Datos de los Tipos de Cambio'
declare	@fecha		varchar(12),
	@tipo_cambio	numeric(12,4),
	@numfecha	numeric(8)

begin
	declare tmpTipoCambio cursor for
		select Fecha, TipoCambio, NumFecha from agrocentro..Deslizamiento

	open tmpTipoCambio
	fetch next from tmpTipoCambio into @fecha, @tipo_cambio, @numfecha
	while @@fetch_status = 0
		begin
			insert into agrocentro2k..prm_tipocambio values(@fecha, @tipo_cambio, @numfecha)
			fetch next from tmpTipoCambio into @fecha, @tipo_cambio, @numfecha
		end
	close tmpTipoCambio
	deallocate tmpTipoCambio
end
go
print 'Importar Datos de los Usuarios'
declare	@registro	tinyint,
	@codigo		varchar(12),
	@nombres	varchar(30),
	@apellidos	varchar(30),
	@direccion	varchar(100),
	@telefono	varchar(16),
	@cargos		varchar(30),
	@clave		varchar(12),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpUsuarios cursor for
		select CodigoUsuario, Nombres, Apellidos, Direccion, Telefono, Cargo, Contraseņa from agrocentro..usuarios where estado = 'activo'

	open tmpUsuarios
	fetch next from tmpUsuarios into @codigo, @nombres, @apellidos, @direccion, @telefono, @cargos, @clave
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_usuarios values(@registro, @codigo, @nombres, @apellidos, @direccion, @telefono, @cargos, @clave, 0)
			fetch next from tmpUsuarios into @codigo, @nombres, @apellidos, @direccion, @telefono, @cargos, @clave
		end
	close tmpUsuarios
	deallocate tmpUsuarios
	declare tmpUsuarios cursor for
		select CodigoUsuario, Nombres, Apellidos, Direccion, Telefono, Cargo, Contraseņa from agrocentro..usuarios where estado = 'desactivo'

	open tmpUsuarios
	fetch next from tmpUsuarios into @codigo, @nombres, @apellidos, @direccion, @telefono, @cargos, @clave
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			insert into agrocentro2k..prm_usuarios values(@registro, @codigo, @nombres, @apellidos, @direccion, @telefono, @cargos, @clave, 1)
			fetch next from tmpUsuarios into @codigo, @nombres, @apellidos, @direccion, @telefono, @cargos, @clave
		end
	close tmpUsuarios
	deallocate tmpUsuarios
	set @registro = @registro + 1
	insert into agrocentro2k..prm_usuarios values(@registro, 'CMASAYA', 'CMASAYA', 'CMASAYA', 'CMASAYA', ' ', ' ', 'xxxxxxa', 0)
	set @registro = @registro + 1
	insert into agrocentro2k..prm_usuarios values(@registro, 'NGUINEA', 'NGUINEA', 'NGUINEA', 'NGUINEA', ' ', ' ', 'xxxxxxb', 0)
	set @registro = @registro + 1
	insert into agrocentro2k..prm_usuarios values(@registro, 'JUIGALPA', 'JUIGALPA', 'JUIGALPA', 'JUIGALPA', ' ', ' ', 'xxxxxxc', 0)
	set @registro = @registro + 1
	insert into agrocentro2k..prm_usuarios values(@registro, 'SEBACO', 'SEBACO', 'SEBACO', 'SEBACO', ' ', ' ', 'xxxxxxd', 0)
end
insert into agrocentro2k..prm_usuarios values(0, 'SA', 'ADMINISTRADOR', 'SISTEMA GERENCIALES', 'AGROCENTRO CASA MATRIZ', '0', 'ADMINISTRADOR', 'agro2k_2007', 0)
go
print 'Importar Datos de los Productos'
declare	@registro	smallint,
	@codigo		varchar(12),
	@descripcion	varchar(65),
	@regclase	tinyint,
	@clase		varchar(12),
	@regsubclase	tinyint,
	@regunidad	tinyint,
	@unidad		varchar(12),
	@regempaque	tinyint,
	@empaque	varchar(12),
	@regproveedor	tinyint,
	@proveedor	varchar(12),
	@pvpc		numeric(10,2),
	@pvdc		numeric(10,2),
	@pvpu		numeric(10,2),
	@pvdu		numeric(10,2),
	@estado		tinyint

begin
	set @registro = 0
	declare tmpProductos cursor for
		select CodigoProducto, Descripcion, CodigoClase, CodigoUnidad, CodigoEmpaque, CodigoProveedor, TipoPrecio1, TipoPrecio2, TipoPrecio3, TipoPrecio4 
			from agrocentro..productos where estado = 'activo' and impuesto = 'N'

	open tmpProductos
	fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			set @regclase = 0
			set @regsubclase = 0
			set @regunidad = 0
			set @regempaque = 0
			set @regproveedor = 0
			select @regclase = registro from agrocentro2k..prm_clases where codigo = @clase
			select @regunidad = registro from agrocentro2k..prm_unidades where codigo = @unidad
			select @regempaque = registro from agrocentro2k..prm_empaques where codigo = @empaque
			select @regproveedor = registro from agrocentro2k..prm_proveedores where codigo = @proveedor
			insert into agrocentro2k..prm_productos values(@registro, @codigo, @descripcion, @regclase, @regsubclase, @regunidad, @regempaque, @regproveedor, 
				@pvpc, @pvdc, @pvpu, @pvdu, 0, 0)
			fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
		end
	close tmpProductos
	deallocate tmpProductos
	declare tmpProductos cursor for
		select CodigoProducto, Descripcion, CodigoClase, CodigoUnidad, CodigoEmpaque, CodigoProveedor, TipoPrecio1, TipoPrecio2, TipoPrecio3, TipoPrecio4 
			from agrocentro..productos where estado = 'activo' and impuesto = 'S'

	open tmpProductos
	fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			set @regclase = 0
			set @regsubclase = 0
			set @regunidad = 0
			set @regempaque = 0
			set @regproveedor = 0
			select @regclase = registro from agrocentro2k..prm_clases where codigo = @clase
			select @regunidad = registro from agrocentro2k..prm_unidades where codigo = @unidad
			select @regempaque = registro from agrocentro2k..prm_empaques where codigo = @empaque
			select @regproveedor = registro from agrocentro2k..prm_proveedores where codigo = @proveedor
			insert into agrocentro2k..prm_productos values(@registro, @codigo, @descripcion, @regclase, @regsubclase, @regunidad, @regempaque, @regproveedor, 
				@pvpc, @pvdc, @pvpu, @pvdu, 1, 0)
			fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
		end
	close tmpProductos
	deallocate tmpProductos

	declare tmpProductos cursor for
		select CodigoProducto, Descripcion, CodigoClase, CodigoUnidad, CodigoEmpaque, CodigoProveedor, TipoPrecio1, TipoPrecio2, TipoPrecio3, TipoPrecio4 
			from agrocentro..productos where estado = 'desactivo' and impuesto = 'N'

	open tmpProductos
	fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			set @regclase = 0
			set @regsubclase = 0
			set @regunidad = 0
			set @regempaque = 0
			set @regproveedor = 0
			select @regclase = registro from agrocentro2k..prm_clases where codigo = @clase
			select @regunidad = registro from agrocentro2k..prm_unidades where codigo = @unidad
			select @regempaque = registro from agrocentro2k..prm_empaques where codigo = @empaque
			select @regproveedor = registro from agrocentro2k..prm_proveedores where codigo = @proveedor
			insert into agrocentro2k..prm_productos values(@registro, @codigo, @descripcion, @regclase, @regsubclase, @regunidad, @regempaque, @regproveedor, 
				@pvpc, @pvdc, @pvpu, @pvdu, 0, 1)
			fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
		end
	close tmpProductos
	deallocate tmpProductos
	declare tmpProductos cursor for
		select CodigoProducto, Descripcion, CodigoClase, CodigoUnidad, CodigoEmpaque, CodigoProveedor, TipoPrecio1, TipoPrecio2, TipoPrecio3, TipoPrecio4 
			from agrocentro..productos where estado = 'desactivo' and impuesto = 'S'

	open tmpProductos
	fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			set @regclase = 0
			set @regsubclase = 0
			set @regunidad = 0
			set @regempaque = 0
			set @regproveedor = 0
			select @regclase = registro from agrocentro2k..prm_clases where codigo = @clase
			select @regunidad = registro from agrocentro2k..prm_unidades where codigo = @unidad
			select @regempaque = registro from agrocentro2k..prm_empaques where codigo = @empaque
			select @regproveedor = registro from agrocentro2k..prm_proveedores where codigo = @proveedor
			insert into agrocentro2k..prm_productos values(@registro, @codigo, @descripcion, @regclase, @regsubclase, @regunidad, @regempaque, @regproveedor, 
				@pvpc, @pvdc, @pvpu, @pvdu, 1, 1)
			fetch next from tmpProductos into @codigo, @descripcion, @clase, @unidad, @empaque, @proveedor, @pvpc, @pvdc, @pvpu, @pvdu
		end
	close tmpProductos
	deallocate tmpProductos
end
update agrocentro2k..prm_productos set regclase = 1 where regclase = 10
go
update agrocentro2k..prm_productos set regclase = 1 where regclase = 11
go
update agrocentro2k..prm_productos set regclase = 9 where regclase = 7
go
update agrocentro2k..prm_productos set regsubclase = 1, regclase = 1 where codigo in ('SE78','SE302','SE292','SE303','SE79','SE304','SE80','SE307','SE275','SE296','SE297','SE306','SE81','SE305','SE276','SE308','SE294')
go
update agrocentro2k..prm_productos set regsubclase = 2, regclase = 1 where codigo in ('SE34','SE107','SE295','SE76','SE75','SE213','SE212','SE285','SE283','SE289','SE288','SE290','SE82','SE93','SE273','SE98','SE238','SE265','SE29','SE272','SE74','SE46','SE281','SE95','SE47','SE16','SE96','SE94','SE184','SE286','SE101','SE274','SE109','SE287','SE97','SE33','SE282','SE284','SE14','SE104','SE105','SE52','SE64','SE100','SE63')
go
update agrocentro2k..prm_productos set regsubclase = 3, regclase = 1 where codigo in ('H7','H8','H6','SE70','SE40','SE60','SE10','SE90','SE50','H640','H581','SE41','SE01','H591','SE91','H381','H151','H411','H541','SE71','SE11','SE51','H441','SE21','H222','SE02','SE62','SE22','SE92','H412','SE42','SE72','H442','SE32','SE43','H233','SE03','SE23','SE13','SE83','SE44','SE84','H154','SE24','SE54','SE35','SE85','H305','SE25','SE05',
	'SE55','SE56','SE06','SE86','SE66','SE26','SE36','SE87','H357','H417','SE27','SE57','H497','H547','H437','SE37','SE17','SE67','SE88','SE28','H308','H498','H418','SE48','H448','SE38','SE18','SE68','H359','SE39','SE89','SE49','SE69','SE09','SE99','SE19','SE30','H27','H97','H20','H22','H77','H51','SE200','SE300','SE210','H4410','SE120','SE220','SE130','H2330','H1730','SE230','H17302','SE240','H9340','H2840','H28406','SE150',
	'SE250','SE260','SE160','SE170','SE270','SE180','SE190','H4101','H2501','H3001','H1101','SE301','H2801','H11014','H11016','H2411','SE111','SE211','H4411','SE221','H27216','H2731','SE231','SE131','H4241','SE141','SE241','H42416','SE251','SE261','SE171','SE271','SE181','H2881','H28816','H28817','SE291','SE191','H2721','H1202','H2002','H4402','H4412','SE112','SE222','H3732','SE132','SE232','H0932','H09322','H37326','H09327',
	'SE142','SE242','SE152','H2752','SE252','H5262','SE262','SE172','SE182','SE192','H2003','H1003','H1803','H3003','H1603','H18036','H16036','SE113','SE223','SE233','H0933','H09337','SE243','H2753','SE253','SE163','SE263','SE173','SE183','SE193','SE293','H4493','H1704','SE114','SE214','H3314','H2814','H28142','H33146','SE224','SE124','H2824','SE234','H0934','SE244','SE154','SE254','SE264','SE164','SE174','SE194','H2005','H2505',
	'H20056','H2415','SE215','SE115','H2815','H28156','SE125','SE225','SE135','SE235','H16357','H1745','SE245','SE245','H17456','H2755','SE155','SE255','SE175','SE185','SE195','H4106','SE106','SE216','SE116','H3316','H4416','H2026','SE126','SE226','H20267','H20268''SE236','SE246','SE146','SE256','SE266','SE176','SE186','SE196','SE207','SE217','SE127','SE227','SE137','SE237','SE247','SE147','SE257','SE267','SE177','SE277','H2887',
	'H28873','H28877','SE197','SE208','SE108','SE118','SE218','SE128','SE228','SE138','SE248','SE158','H2758','SE258','H27586','SE268','SE278','H28785','H28786','SE188','SE198','SE298','SE209','SE219','SE129','SE229','SE139','SE239','SE149','SE249','SE259','SE159','SE169','SE269','SE279','SE179','SE189','SE199','SE299')
go
update agrocentro2k..prm_productos set regsubclase = 4, regclase = 1 where codigo in ('F8','FA04','FC04','FC05','FD02','FF01','FG01','FG05','FG02','FG09','FG04','FG08','FG07','FI01','FL01','FL03','FM01','FM03','FM04','FM02','FM05','FN01','FN03','FP02','FP05','FP06','FS02','FS04','FT03','FT01','FT04','FV03','FV05','F5014','FV04','F5011','FZ02','FZ01','FZ03','FZ05','FA15','FA13','FA14','FA11','FC11','FC12','FC13','FG10','FG11',
	'FG12','FG15','FG13','F5103','F5104','F5210','F5215','FC23','F5224','F5229','F5231','F5232','F5235','F5270','F5237','F5256','F5260','F4501','F4504','F4505','F4601','F4603','F4606','F4604','F4602','F4703','F4701','F4702','F4802','F4902')
go
update agrocentro2k..prm_productos set regsubclase = 5, regclase = 2 where codigo in ('FE48','FE47','FE81','FE04','FE05','FE01','FE16','FE13','FE27','FE61','FE26','FE29','FE08','FE17','FE09','FE03','FE28','FE20','FE12','FE53','FE70','FE52','FE71','FE72','FE80','FE54','FE85','FE07','FE67','FE84','FE68','FE06','FE59','FE60','FE02')
go
update agrocentro2k..prm_productos set regsubclase = 6, regclase = 2 where codigo in ('FE69','FE14','FE10','FE23','FE50','FE11','FE39','FE25','FE19','FE18','FE45','FE15','FE82','FE83','FE33','FE32','FE35','FE34','FE41','FE40','FE51','FE43','FE46','FE44','FE42','FE37','FE36','FE22','FE38','FE24','FE21','FE55','FE65','FE77','FE66','FE79','FE56','FE78','FE74','FE58','FE86','FE76','FE75','FE73','FE63','FE49','FE62','FE64','FE57','FE31','FE30')
go
update agrocentro2k..prm_productos set regsubclase = 7, regclase = 4 where codigo in ('IN06','IN119','IN115','IN130','IN132','IN122','IN111','IN110','IN135','IN134','IN128','IN120','IN13','IN114','IN15','IN107','IN103','IN108','IN102','IN106','IN124','IN14','IN101','IN100','IN104','IN109','IN125','IN12','IN112','IN123','IN113','IN105','IN121','IN116','IN117','IN28','IN44','IN43','IN49','IN59','IN55','IN50','IN58','IN56','IN61','IN60','IN67',
	'IN65','IN64','IN71','IN76','IN77','IN73','IN78','IN74','IN80','IN82','IN81','IN83','IN88','IN85','IN84','IN89','IN86','IN87','IN93','IN99','IN90','IN91','IN92','IN98','IN95','IN96','IN94')
go
update agrocentro2k..prm_productos set regsubclase = 8, regclase = 4 where codigo in ('OT557','OT141','IN118','OT236','OT140','OT169','OT613','OT87','IN19','IN97','IN126','IN127','IN131','IN129','IN133','IN66','OT241')
go
update agrocentro2k..prm_productos set regsubclase = 18, regclase = 8 where codigo in ('AS41','AS14','AS44')
go
update agrocentro2k..prm_productos set regsubclase = 19, regclase = 8 where codigo in ('AS33','AS45','AS28','AS34','AS35','AS31','AS32','AS11','AS12','AS13','AS15','AS23','AS39','AS36','AS40','AS43','AS37','AS42','AS46','AS29','AS05','AS38','AS24','AS30')
go
update agrocentro2k..prm_productos set regsubclase = 20, regclase = 13 where codigo in ('OT30','OT31','OT32','OT33','OT35','OT26','OT27','OT28','OT29','OT230','OT530','OT450','OT460','OT421','OT521','OT451','OT522','OT442','OT452','OT472','OT523','OT453','OT524','OT454','OT574','OT525','OT445','OT455','OT516','OT526','OT456','OT127','OT527','OT457','OT418','OT128','OT528','OT438','OT448','OT458','OT229','OT529','OT449','OT459')
go
update agrocentro2k..prm_productos set regsubclase = 21, regclase = 13 where codigo in ('OT15','OT25','OT22','OT14','OT18','OT23','OT34','OT12','OT24','OT13','OT21','OT17','OT16','OT19','OT310','OT540','OT260','OT430','OT240','OT120','OT250','OT350','OT320','OT330','OT410','OT300','OT520','OT470','OT480','OT500','OT280','OT270','OT261','OT471','OT431','OT351','OT411','OT231','OT301','OT531','OT541','OT501','OT121','OT391','OT321','OT311',
	'OT481','OT271','OT251','OT131','OT122','OT272','OT392','OT312','OT302','OT252','OT432','OT352','OT232','OT412','OT322','OT502','OT402','OT262','OT542','OT532','OT263','OT473','OT253','OT513','OT273','OT353','OT323','OT303','OT393','OT403','OT413','OT313','OT533','OT503','OT443','OT543','OT433','OT123','OT233','OT354','OT434','OT444','OT474','OT404','OT504','OT324','OT254','OT514','OT274','OT234','OT314','OT544','OT124','OT304','OT534',
	'OT414','OT264','OT224','OT265','OT475','OT355','OT435','OT405','OT235','OT295','OT505','OT415','OT255','OT515','OT315','OT545','OT425','OT305','OT535','OT325','OT275','OT125','OT536','OT326','OT506','OT226','OT556','OT256','OT546','OT426','OT296','OT126','OT446','OT266','OT436','OT406','OT306','OT346','OT416','OT316','OT466','OT476','OT276','OT277','OT227','OT517','OT537','OT427','OT407','OT447','OT317','OT467','OT327','OT237','OT587',
	'OT267','OT257','OT307','OT297','OT437','OT477','OT507','OT217','OT347','OT498','OT518','OT278','OT328','OT428','OT318','OT218','OT408','OT268','OT538','OT478','OT348','OT308','OT468','OT298','OT258','OT118','OT469','OT269','OT109','OT299','OT129','OT119','OT499','OT519','OT259','OT219','OT409','OT279','OT349','OT329','OT319','OT429','OT539','OT479','OT239','OT309')
go
update agrocentro2k..prm_productos set regsubclase = 22, regclase = 13 where codigo in ('VE776','VE785','VE786','VE829','VE830','VE831','VE832','VE833','VE834','VE835','VE836','VE837','VE838','VE787','VE839','VE840','VE841','VE788','VE789','VE790','VE791','VE792','VE777','VE793','VE794','VE795','VE796','VE778','VE797','VE798','VE799','VE800','VE801','VE802','VE803','VE804','VE805','VE806','VE779','VE807','VE808','VE809','VE810','VE811','VE812',
	'VE813','VE780','VE814','VE815','VE816','VE817','VE818','VE819','VE781','VE782','VE820','VE821','VE783','VE822','VE823','VE824','VE825','VE826','VE827','VE828','VE784')
go
update agrocentro2k..prm_productos set regsubclase = 23, regclase = 9 where codigo in ('OT387','OT386','OT464','OT110','OT462','OT463','OT186','OT184','OT111','OT117','OT115','OT130','OT185','OT571','OT688','OT508','OT586','OT509','OT511','OT687','OT510','OT497','OT554','OT461','OT465','OT555','OT112','OT388','OT364','OT361','OT360','OT365','OT366','OT362','OT363','OT368','OT367','OT369','OT370','OT357','OT356','OT358','OT359','OT60','OT384',
	'OT139','OT191','OT189','OT190','OT385','OT575','OT389','OT390','OT588','OT576')
go
update agrocentro2k..prm_productos set regsubclase = 24, regclase = 9 where codigo in ('OT78','OT55','OT56','OT57','OT54','OT52','OT53','OT37','OT79','OT11','OT36','OT58','OT80','OT57','OT150','OT440','OT380','OT420','OT340','OT170','OT400','OT580','OT151','OT341','OT171','OT581','OT441','OT401','OT371','OT281','OT381','OT152','OT482','OT192','OT342','OT382','OT172','OT492','OT282','OT372','OT153','OT343','OT383','OT283','OT483','OT193','OT493',
	'OT373','OT173','OT103','OT113','OT394','OT484','OT494','OT194','OT344','OT284','OT374','OT104','OT154','OT195','OT285','OT345','OT375','OT105','OT495','OT485','OT395','OT145','OT486','OT496','OT376','OT196','OT286','OT176','OT396','OT106','OT146','OT207','OT337','OT377','OT197','OT187','OT287','OT487','OT147','OT397','OT188','OT378','OT338','OT198','OT208','OT138','OT398','OT148','OT138','OT228','OT379','OT339','OT419','OT199','OT439',
	'OT209','OT579','OT149','OT399')
go
update agrocentro2k..prm_productos set regsubclase = 25, regclase = 9 where codigo in ('OT210','OT211','OT212','OT213','OT214','OT220','OT221','OT222')
go
update agrocentro2k..prm_productos set regsubclase = 27 where regclase = 1 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 28 where regclase = 2 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 29 where regclase = 3 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 30 where regclase = 4 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 31 where regclase = 5 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 32 where regclase = 6 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 33 where regclase = 7 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 34 where regclase = 8 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 35 where regclase = 9 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 36 where regclase = 10 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 37 where regclase = 11 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 38 where regclase = 12 and regsubclase = 0
update agrocentro2k..prm_productos set regsubclase = 39 where regclase = 13 and regsubclase = 0
go
print 'Importar Datos de los Vendedores Departamentos'
declare	@codigovend1	varchar(12),
	@codigodept1	varchar(30),
	@codigovend2	tinyint,
	@codigodept2	tinyint

begin
	declare tmpVendDept cursor for
		select distinct CodigoVendedor, CodigoDepart from agrocentro..Vendedores_Depart where estado = 'activo'

	open tmpVendDept
	fetch next from tmpVendDept into @codigovend1, @codigodept1
	while @@fetch_status = 0
		begin
			set @codigovend2 = 0
			set @codigodept2 = 0
			select @codigovend2 = registro from agrocentro2k..prm_vendedores where codigo = @codigovend1
			select @codigodept2 = registro from agrocentro2k..prm_departamentos where codigo = @codigodept1
			insert into agrocentro2k..prm_venddept values(@codigovend2, @codigodept2, 0)
			fetch next from tmpVendDept into @codigovend1, @codigodept1
		end
	close tmpVendDept
	deallocate tmpVendDept
end
go
print 'Importar Datos de los Usuarios Agencias'
declare	@codigouser1	varchar(12),
	@codigoagen1	varchar(30),
	@codigouser2	tinyint,
	@codigoagen2	tinyint

begin
	declare tmpUsuarioAgencia cursor for
		select distinct CodigoUsuario, CodigoSucursal from agrocentro..Usuarios_Sucursales where NumFechaRevocado = 0

	open tmpUsuarioAgencia
	fetch next from tmpUsuarioAgencia into @codigouser1, @codigoagen1
	while @@fetch_status = 0
		begin
			set @codigouser2 = 0
			set @codigoagen2 = 0
			select @codigouser2 = registro from agrocentro2k..prm_usuarios where codusuario = @codigouser1
			select @codigoagen2 = registro from agrocentro2k..prm_agencias where codigo = @codigoagen1
			insert into agrocentro2k..prm_usuaragenc values(@codigouser2, @codigoagen2, 0)
			fetch next from tmpUsuarioAgencia into @codigouser1, @codigoagen1
		end
	close tmpUsuarioAgencia
	deallocate tmpUsuarioAgencia
end
go
print 'Importar Datos de las Conversiones'
declare	@registro	tinyint,
	@cantidad01	numeric(12,2),
	@unidad01	varchar(12),
	@regunidad01	tinyint,
	@cantidad02	numeric(12,2),
	@unidad02	varchar(12),
	@regunidad02	tinyint

begin
	set @registro = 0
	declare tmpConversion cursor for
		select De_Cantidad, De_Unidad, A_Cantidad, A_Unidad from agrocentro..Conversion

	open tmpConversion
	fetch next from tmpConversion into @cantidad01, @unidad01, @cantidad02, @unidad02
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			set @regunidad01 = 0
			set @regunidad02 = 0
			select @regunidad01 = registro from agrocentro2k..prm_unidades where codigo = @unidad01
			select @regunidad02 = registro from agrocentro2k..prm_unidades where codigo = @unidad02
			insert into agrocentro2k..prm_conversiones values(@registro, @cantidad01, @regunidad01, @cantidad02, @regunidad02, 0)
			fetch next from tmpConversion into @cantidad01, @unidad01, @cantidad02, @unidad02
		end
	close tmpConversion
	deallocate tmpConversion
end
go
print 'Importar Datos del Numero de Ultima Factura'
insert into agrocentro2k..tbl_ultimonumero select * from agrocentro..ultimonumero
go
print 'Importar Datos de los Recibos de Caja'
declare	@contador	int,
	@registro	numeric(7),
	@fecha		smalldatetime,
	@numfecha	numeric(8),
	@codcliente	varchar(12),
	@cliregistro	smallint,
	@codvendedor	varchar(12),
	@vendregistro	tinyint,
	@numero		varchar(12),
	@monto		numeric(12,2),
	@tot_int	numeric(12,2),
	@interes	numeric(12,2),
	@tot_ret	numeric(12,2),
	@retencion	numeric(12,2),
	@total		numeric(12,2),
	@formapago	tinyint,
	@numchqtarj	varchar(30),
	@nombrebanco	varchar(50),
	@reciboprov	varchar(12),
	@descripcion	varchar(100),
	@usrregistro	tinyint,
	@tiporecibo	tinyint,
	@descripcion2	varchar(100),
	@monto2		numeric(12,2),
	@numero_fact	varchar(20)

begin
	set nocount on
	set @contador = 0
	set @registro = 0
	declare tmpC cursor for
		select distinct num_rec, mon_rec, fec_rec, cod_cli, cod_ven from agrocentro2k..mov_cre order by fec_rec, num_rec, mon_rec asc
	open tmpC
	fetch next from tmpC into @numero, @monto, @fecha, @codcliente, @codvendedor
	while @@fetch_status = 0
		begin
			if @contador % 5000 = 0
				begin
					print @contador
				end
			set @contador = @contador + 1
			set @registro = @registro + 1
			set @tot_int = 0
			set @tot_ret = 0
			declare tmpC2 cursor for
				select mon_abo, 'des_rec' = case when des_rec is null then num_fac else des_rec end, tip_mov, che_num, nom_ban, 
					interes, mon_ret, num_fac from agrocentro2k..mov_cre where num_rec = @numero and mon_rec = @monto and 
					fec_rec = @fecha and cod_cli = @codcliente and cod_ven = @codvendedor
			open tmpC2
			fetch next from tmpC2 into @monto2, @descripcion2, @tiporecibo, @numchqtarj, @nombrebanco, @interes, @retencion, @numero_fact
			while @@fetch_status = 0
				begin
					set @tot_int = @tot_int + @interes
					set @tot_ret = @tot_ret + @retencion
					set @formapago = 0
					if @nombrebanco != ''
						begin
							set @formapago = 1
						end
					if @retencion <> 0
						begin
							set @monto2 = @retencion
						end
					insert into agrocentro2k..tbl_recibos_det values (@registro, @numero_fact, @descripcion2, @monto2, @interes, 0, 0)
					fetch next from tmpC2 into @monto2, @descripcion2, @tiporecibo, @numchqtarj, @nombrebanco, @interes, @retencion, @numero_fact
				end
			close tmpC2
			deallocate tmpC2
			set @cliregistro = 0
			set @vendregistro = 0
			set @tiporecibo = 0
			if substring(@numero, 1, 1) = 'R'
				begin
					set @tiporecibo = 1
				end
			else if substring(@numero, 1, 2) = 'ND'
				begin
					set @tiporecibo = 3
				end
			else
				begin
					set @tiporecibo = 2
				end
			select @cliregistro = registro from agrocentro2k..prm_clientes where codigo = @codcliente
			select @vendregistro = registro from agrocentro2k..prm_vendedores where codigo = @codvendedor
			insert into agrocentro2k..tbl_recibos_enc values (@registro, convert(varchar(12), @fecha, 113), 
				convert(varchar(8), @fecha, 112), @cliregistro, @vendregistro, @numero, @monto, @tot_int, @tot_ret, 
				@formapago, @numchqtarj, @nombrebanco, '', '', 0, @tiporecibo, @fecha, 0, 0, '', '', '')
			fetch next from tmpC into @numero, @monto, @fecha, @codcliente, @codvendedor
		end
	close tmpC
	deallocate tmpC
	set nocount off
end
go
update agrocentro2k..tbl_recibos_enc set fecha = substring(replace(fecha, ' ', '-'),1,11)
go
print 'Importar Datos de las Obligaciones del Cliente'
go
declare	@registro	int,
	@codcliente	varchar(12),
	@cliregistro	smallint,
	@codvendedor	varchar(12),
	@vendregistro	tinyint,
	@numero		varchar(12),
	@monto		numeric(12,2),
	@abono		numeric(12,2),
	@saldo		numeric(12,2),
	@estado		char(1),
	@estado2	tinyint,
	@fechaing	smalldatetime,
	@fechaven	smalldatetime

begin
	set nocount on
	set @registro = 0
	declare tmpC cursor for
		select cod_cli, cod_ven, num_fac, mon_fac, abo_fac, Sal_fac, Est_fac, Fec_Fac, Fec_Ven 
			from agrocentro2k..mae_cre order by fec_fac, cod_cli, num_fac asc
	open tmpC
	fetch next from tmpC into @codcliente, @codvendedor, @numero, @monto, @abono, @saldo, @estado, @fechaing, @fechaven
	while @@fetch_status = 0
		begin
			set @registro = @registro + 1
			if @registro % 5000 = 0
				begin
					print @registro
				end
			set @cliregistro = 0
			set @vendregistro = 0
			select @cliregistro = registro from agrocentro2k..prm_clientes where codigo = @codcliente
			select @vendregistro = registro from agrocentro2k..prm_vendedores where codigo = @codvendedor
			if @estado = 'P'			-- Pendiente
				begin
					set @estado2 = 0
				end
			else if @estado = 'C'			-- Cancelado
				begin
					set @estado2 = 1
				end
			else					-- Anulados
				begin
					set @estado2 = 2
				end
			insert into agrocentro2k..tbl_cliente_oblig values (@registro, @cliregistro, @vendregistro, @numero, 
				convert(varchar(12), @fechaing, 113), convert(varchar(12), @fechaven, 113), 
				@monto, @abono, @saldo, @estado2, 
				convert(varchar(8), @fechaing, 112), convert(varchar(8), @fechaven, 112), @fechaing, @fechaven)
			fetch next from tmpC into @codcliente, @codvendedor, @numero, @monto, @abono, @saldo, @estado, @fechaing, @fechaven
		end
	close tmpC
	deallocate tmpC
	set nocount off
end
go
update agrocentro2k..tbl_cliente_oblig set fechaing = substring(replace(fechaing, ' ', '-'),1,11)
go
update agrocentro2k..tbl_cliente_oblig set fechaven = substring(replace(fechaven, ' ', '-'),1,11)
go
delete from agrocentro2k..tbl_cliente_oblig where monto = 0
go
select * from agrocentro2k..tbl_cliente_oblig where cliregistro = 0
go
set nocount off
go

