Imports System.Data.SqlClient
Imports System.Text

Public Class frmMovCheques
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton5 As System.Windows.Forms.ToolBarButton
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMovCheques))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton5 = New System.Windows.Forms.ToolBarButton
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.TextBox21 = New System.Windows.Forms.TextBox
        Me.TextBox20 = New System.Windows.Forms.TextBox
        Me.TextBox19 = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.TextBox18 = New System.Windows.Forms.TextBox
        Me.TextBox17 = New System.Windows.Forms.TextBox
        Me.TextBox16 = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.TextBox15 = New System.Windows.Forms.TextBox
        Me.TextBox14 = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton5})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(602, 57)
        Me.ToolBar1.TabIndex = 17
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton5
        '
        Me.ToolBarButton5.ImageIndex = 6
        Me.ToolBarButton5.Name = "ToolBarButton5"
        Me.ToolBarButton5.Text = "<ESC>"
        Me.ToolBarButton5.ToolTipText = "Salir"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Limpiar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7})
        Me.MenuItem3.Text = "Listado"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Extraer"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Chequeras"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Proveedores"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.Text = "Salir"
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 379)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(602, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 18
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Name = "StatusBarPanel1"
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Name = "StatusBarPanel2"
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 500
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(600, 72)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(560, 40)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 24)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Label17"
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(520, 40)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(32, 24)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Label16"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(392, 40)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(104, 20)
        Me.TextBox5.TabIndex = 8
        Me.TextBox5.Tag = "Moneda de la cuenta"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(296, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Tipo de Moneda"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(120, 40)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(168, 20)
        Me.TextBox4.TabIndex = 6
        Me.TextBox4.Tag = "N�mero de Cuenta"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "N�mero de Cuenta"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(361, 16)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(231, 20)
        Me.TextBox3.TabIndex = 4
        Me.TextBox3.Tag = "Nombre de la instituci�n financiera"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(323, 16)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(32, 20)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Tag = "C�digo de chequera"
        Me.TextBox2.Text = "0"
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(252, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = " Chequera"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(107, 16)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(139, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Tag = "N�mero de transacci�n"
        Me.TextBox1.Text = "0"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No. Transacci�n"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox10)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.TextBox8)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.TextBox7)
        Me.GroupBox2.Controls.Add(Me.TextBox6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 136)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(600, 96)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(80, 64)
        Me.TextBox10.MaxLength = 80
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(416, 20)
        Me.TextBox10.TabIndex = 14
        Me.TextBox10.Tag = "Descripci�n del cheque"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Descripci�n"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(368, 40)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 12
        Me.DateTimePicker1.Tag = "Fecha de elaboraci�n"
        Me.DateTimePicker1.Value = New Date(2009, 2, 16, 19, 3, 0, 0)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(258, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(110, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Fecha del Cheque"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(158, 41)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(88, 20)
        Me.TextBox9.TabIndex = 10
        Me.TextBox9.Tag = "Monto del cheque"
        Me.TextBox9.Text = "0.00"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(120, 41)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(32, 20)
        Me.TextBox8.TabIndex = 9
        Me.TextBox8.Tag = "Signo de la moneda de la cuenta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(110, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Monto Documento"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(142, 16)
        Me.TextBox7.MaxLength = 60
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(360, 20)
        Me.TextBox7.TabIndex = 7
        Me.TextBox7.Tag = "Nombre del beneficiario"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(80, 16)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(56, 20)
        Me.TextBox6.TabIndex = 6
        Me.TextBox6.Tag = "C�digo del beneficiario"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Beneficiario"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.TextBox12)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.TextBox11)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 232)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(120, 144)
        Me.GroupBox3.TabIndex = 21
        Me.GroupBox3.TabStop = False
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(8, 112)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.Size = New System.Drawing.Size(104, 20)
        Me.TextBox13.TabIndex = 15
        Me.TextBox13.Tag = "Saldo de la cuenta en el momento del cheque"
        Me.TextBox13.Text = "0.00"
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 96)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Saldo Actual"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(8, 72)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.ReadOnly = True
        Me.TextBox12.Size = New System.Drawing.Size(104, 20)
        Me.TextBox12.TabIndex = 13
        Me.TextBox12.Tag = "Monto del cheque"
        Me.TextBox12.Text = "0.00"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Este Cheque"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(8, 32)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.ReadOnly = True
        Me.TextBox11.Size = New System.Drawing.Size(104, 20)
        Me.TextBox11.TabIndex = 11
        Me.TextBox11.Tag = "Saldo anterior de la cuenta"
        Me.TextBox11.Text = "0.00"
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(87, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Saldo Anterior"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.TextBox21)
        Me.GroupBox4.Controls.Add(Me.TextBox20)
        Me.GroupBox4.Controls.Add(Me.TextBox19)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.TextBox18)
        Me.GroupBox4.Controls.Add(Me.TextBox17)
        Me.GroupBox4.Controls.Add(Me.TextBox16)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.TextBox15)
        Me.GroupBox4.Controls.Add(Me.TextBox14)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(120, 232)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(480, 144)
        Me.GroupBox4.TabIndex = 22
        Me.GroupBox4.TabStop = False
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(8, 112)
        Me.TextBox21.MaxLength = 80
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.ReadOnly = True
        Me.TextBox21.Size = New System.Drawing.Size(464, 20)
        Me.TextBox21.TabIndex = 22
        Me.TextBox21.Tag = "Descripci�n del cheque"
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(8, 88)
        Me.TextBox20.MaxLength = 60
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(464, 20)
        Me.TextBox20.TabIndex = 21
        Me.TextBox20.Tag = "Nombre de la instituci�n financiera"
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(80, 64)
        Me.TextBox19.MaxLength = 300
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.Size = New System.Drawing.Size(392, 20)
        Me.TextBox19.TabIndex = 20
        Me.TextBox19.Tag = "Valor del cheque en letras"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 64)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 13)
        Me.Label15.TabIndex = 19
        Me.Label15.Text = "La suma de"
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(384, 40)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(88, 20)
        Me.TextBox18.TabIndex = 18
        Me.TextBox18.Tag = "Monto del cheque"
        Me.TextBox18.Text = "0.00"
        Me.TextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(352, 40)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(32, 20)
        Me.TextBox17.TabIndex = 17
        Me.TextBox17.Tag = "Signo de la moneda de la cuenta"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(80, 40)
        Me.TextBox16.MaxLength = 60
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(264, 20)
        Me.TextBox16.TabIndex = 16
        Me.TextBox16.Tag = "Nombre del proveedor/beneficiario"
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(8, 32)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 24)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Pagase a o a su Orden"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(384, 16)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(88, 20)
        Me.TextBox15.TabIndex = 14
        Me.TextBox15.Tag = "Fecha de elaboraci�n"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(304, 16)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(72, 20)
        Me.TextBox14.TabIndex = 13
        Me.TextBox14.Tag = "N�mero de documento"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(208, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(97, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Cheque N�mero"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(8, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(192, 16)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "NOMBRE DE LA EMPRESA"
        '
        'frmMovCheques
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(602, 403)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.ToolBar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmMovCheques"
        Me.Text = "frmMovCheques"
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmMovCheques_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Limpiar()
        Label12.Text = strNombEmpresa
        Label16.Text = intTipoCheque
        If Label16.Text = "5" Or Label16.Text = "9" Or Label16.Text = "13" Then
            TextBox6.ReadOnly = True
            TextBox7.ReadOnly = True
            TextBox9.ReadOnly = True
            TextBox10.ReadOnly = True
            TextBox1.ReadOnly = False
        Else
            TextBox2.Focus()
        End If

    End Sub

    Private Sub frmMovCheques_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Extraer()
        ElseIf e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                If intTipoCheque = 1 Then
                    'ingresar un cheque
                    Guardar()
                ElseIf intTipoCheque = 9 Then
                    'Reversar un cheque
                End If
            Case 1
                Limpiar()
            Case 3
                Me.Close()
        End Select

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem5.Click, MenuItem6.Click

        If sender.text = "Guardar" Then
            Guardar()
        ElseIf sender.text = "Limpiar" Then
            Limpiar()
        ElseIf sender.text = "Extraer" And TextBox1.Text <> "0" Then
            Extraer()
        ElseIf sender.text = "Chequeras" Then
            intListadoAyuda = 5
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Sub Limpiar()

        TextBox1.Text = "0"
        TextBox2.Text = "0"
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = "0.00"
        TextBox10.Text = ""
        TextBox11.Text = "0.00"
        TextBox12.Text = "0.00"
        TextBox13.Text = "0.00"
        TextBox14.Text = ""
        TextBox15.Text = ""
        TextBox16.Text = ""
        TextBox17.Text = ""
        TextBox18.Text = "0.00"
        TextBox19.Text = ""
        TextBox20.Text = ""
        TextBox21.Text = ""
        DateTimePicker1.Value = Now
        TextBox15.Text = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        TextBox19.Text = NumPalabra(CDbl(TextBox9.Text))
        Label17.Text = "0"
        TextBox1.Focus()

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, TextBox3.KeyDown, TextBox4.KeyDown, TextBox5.KeyDown, TextBox6.KeyDown, TextBox7.KeyDown, TextBox8.KeyDown, TextBox9.KeyDown, TextBox10.KeyDown, TextBox11.KeyDown, TextBox12.KeyDown, TextBox13.KeyDown, TextBox14.KeyDown, TextBox15.KeyDown, TextBox16.KeyDown, TextBox17.KeyDown, TextBox18.KeyDown, TextBox19.KeyDown, TextBox20.KeyDown, TextBox21.KeyDown, DateTimePicker1.KeyDown

        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
            Select Case sender.name.ToString
                Case "TextBox1"
                    If TextBox1.Text <> "0" Then
                        Extraer()
                    End If
                    TextBox2.Focus()

                Case "TextBox2" : UbicarChequera() : TextBox6.Focus()
                Case "TextBox3" : TextBox6.Focus()
                Case "TextBox4" : TextBox6.Focus()
                Case "TextBox5" : TextBox6.Focus()
                Case "TextBox6" : UbicarProveedor() : TextBox7.Focus()
                Case "TextBox7" : TextBox16.Text = TextBox7.Text : TextBox9.Focus()
                Case "TextBox8" : TextBox9.Focus()
                Case "TextBox9"
                    TextBox9.Text = Format(CDbl(TextBox9.Text), "#,##0.#0")
                    TextBox19.Text = NumPalabra(CDbl(TextBox9.Text)) : DateTimePicker1.Focus()
                    If intTipoCheque = 1 Then 'Resta al balance actual el valor del cheque para ingresar cheque
                        TextBox12.Text = TextBox9.Text
                        TextBox13.Text = Format(CDbl(TextBox11.Text) - CDbl(TextBox12.Text), "#,##0.#0")
                        TextBox18.Text = TextBox9.Text
                    ElseIf intTipoCheque = 2 Then 'solo se consulta un cheque
                        TextBox12.Text = TextBox9.Text
                    ElseIf intTipoCheque = 9 Then 'Suma cantidad actual en la cuenta con el valor del cheque para reversar
                        TextBox12.Text = TextBox9.Text
                        TextBox13.Text = Format(CDbl(TextBox11.Text) + CDbl(TextBox12.Text), "#,##0.#0")
                        TextBox18.Text = TextBox9.Text
                    End If
                Case "DateTimePicker1" : TextBox10.Focus()
                Case "TextBox10" : TextBox21.Text = TextBox10.Text : TextBox14.Focus()
                Case "TextBox11" : TextBox1.Focus()
                Case "TextBox12" : TextBox1.Focus()
                Case "TextBox13" : TextBox1.Focus()
                Case "TextBox14" : TextBox1.Focus()
                Case "TextBox15" : TextBox1.Focus()
                Case "TextBox16" : TextBox1.Focus()
                Case "TextBox17" : TextBox1.Focus()
                Case "TextBox18" : TextBox1.Focus()
                Case "TextBox19" : TextBox1.Focus()
                Case "TextBox20" : TextBox1.Focus()
                Case "TextBox21" : TextBox1.Focus()
            End Select
        End If

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, TextBox3.GotFocus, TextBox4.GotFocus, TextBox5.GotFocus, TextBox6.GotFocus, TextBox7.GotFocus, TextBox8.GotFocus, TextBox9.GotFocus, TextBox10.GotFocus, TextBox11.GotFocus, TextBox12.GotFocus, TextBox13.GotFocus, TextBox14.GotFocus, TextBox15.GotFocus, TextBox16.GotFocus, TextBox17.GotFocus, TextBox18.GotFocus, TextBox19.GotFocus, TextBox20.GotFocus, TextBox21.GotFocus, DateTimePicker1.GotFocus

        Dim strCampo As String = String.Empty

        MenuItem6.Visible = False
        MenuItem7.Visible = False
        If sender.name = "TextBox2" Then MenuItem6.Visible = True
        If sender.name = "TextBox6" Then MenuItem7.Visible = True
        Select Case sender.name.ToString
            Case "TextBox1" : strCampo = "Transacci�n"
            Case "TextBox2" : strCampo = "Chequera"
            Case "TextBox3" : strCampo = "Instituci�n"
            Case "TextBox4" : strCampo = "Cuenta"
            Case "TextBox5" : strCampo = "Moneda"
            Case "TextBox6" : strCampo = "C�digo"
            Case "TextBox7" : strCampo = "Beneficiario"
            Case "TextBox8" : strCampo = "Signo"
            Case "TextBox9" : strCampo = "Monto"
            Case "TextBox10" : strCampo = "Descripci�n"
            Case "TextBox11" : strCampo = "Saldo"
            Case "TextBox12" : strCampo = "Monto"
            Case "TextBox13" : strCampo = "Saldo"
            Case "TextBox14" : strCampo = "N�mero"
            Case "TextBox15" : strCampo = "Fecha"
            Case "TextBox16" : strCampo = "Beneficiario"
            Case "TextBox17" : strCampo = "Signo"
            Case "TextBox18" : strCampo = "Monto"
            Case "TextBox19" : strCampo = "Valor"
            Case "TextBox20" : strCampo = "Instituci�n"
            Case "TextBox21" : strCampo = "Descripci�n"
            Case "DateTimePicker1" : strCampo = "Fecha"
        End Select
        If sender.name <> "DateTimePicker1" Then
            sender.selectall()
        End If
        StatusBar1.Panels.Item(0).Text = strCampo
        If Label16.Text = "5" Or Label16.Text = "9" Or Label16.Text = "13" Then
            If Label17.Text = "0" Then
                StatusBar1.Panels.Item(1).Text = sender.tag
            ElseIf Label17.Text = "1" Then
                StatusBar1.Panels.Item(1).Text = "        CHEQUE HA SIDO ANULADO"
            ElseIf Label17.Text = "2" Then
                StatusBar1.Panels.Item(1).Text = "        CHEQUE HA SIDO REVERTIDO"
            End If
        Else
            StatusBar1.Panels.Item(1).Text = sender.tag
        End If

    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click

        intListadoAyuda = 5
        Dim frmNew As New frmListadoAyuda
        Timer1.Interval = 200
        Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click

        intListadoAyuda = 6
        Dim frmNew As New frmListadoAyuda
        Timer1.Interval = 200
        Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If intListadoAyuda = 5 Then
                UbicarChequera()
                strUbicar = ""
            ElseIf intListadoAyuda = 6 Then
                TextBox6.Text = strUbicar
                TextBox7.Text = strUbicar02
                strUbicar = ""
                strUbicar02 = ""
            End If
            intListadoAyuda = 0
        End If

    End Sub

    Sub UbicarChequera()

        strQuery = ""
        strQuery = "select c.registro, b.descripcion, c.codigo, m.descripcion, m.signo, c.blnactual "
        strQuery = strQuery + "from prm_ctasbancarias c, prm_bancos b, cat_monedas m where "
        strQuery = strQuery + "c.bancoregistro = b.registro and c.monregistro = m.registro - 1 "
        If intListadoAyuda = 5 Then
            strQuery = strQuery + "and c.codigo = '" & strUbicar & "' and c.estado = 0"
        Else
            strQuery = strQuery + "and c.registro = " & CInt(TextBox2.Text) & " and c.estado = 0"
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox2.Text = dtrAgro2K.GetValue(0)
            TextBox3.Text = dtrAgro2K.GetValue(1)
            TextBox4.Text = dtrAgro2K.GetValue(2)
            TextBox5.Text = dtrAgro2K.GetValue(3)
            TextBox8.Text = dtrAgro2K.GetValue(4)
            TextBox11.Text = Format(dtrAgro2K.GetValue(5), "#,##0.#0") 'saldo anterior
            TextBox13.Text = Format(CDbl(TextBox11.Text) - CDbl(TextBox12.Text), "#,##0.#0") 'saldo actual
            TextBox17.Text = TextBox8.Text
            TextBox20.Text = TextBox3.Text
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub UbicarProveedor()

        strQuery = ""
        strQuery = "select codigo, nombre from prm_beneficiarios where "
        strQuery = strQuery + "codigo = '" & TextBox6.Text & "' and estado = 0"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox6.Text = dtrAgro2K.GetValue(0)
            TextBox7.Text = dtrAgro2K.GetValue(1)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Extraer()

        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngChequesMovimientos", cnnAgro2K) 'se establece la coneccion a la base de datos con el procedimiento almacenado
        Dim prmTmp01 As New SqlParameter 'declaracion de parametros temporales de tipo sql parametros
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter

        lngRegistro = 0
        With prmTmp01 'se le asignan los valores a los parametros
            .ParameterName = "@intTipoCheque" 'nombre del parametro
            .SqlDbType = SqlDbType.TinyInt 'tipo de datos
            .Value = intTipoCheque '5 'valor asignado al parametro
        End With
        With prmTmp02
            .ParameterName = "@strregistro"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp03
            .ParameterName = "@ctabanregistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp04
            .ParameterName = "@benefregistro"
            .SqlDbType = SqlDbType.SmallInt
            .Value = 0
        End With
        With prmTmp05
            .ParameterName = "@beneficiario"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp06
            .ParameterName = "@descripcion"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp07
            .ParameterName = "@fechacheque"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With prmTmp08
            .ParameterName = "@fechaingreso"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With prmTmp09
            .ParameterName = "@documento"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp10
            .ParameterName = "@monto"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp11
            .ParameterName = "@numfechacheque"
            .SqlDbType = SqlDbType.Int
            .Value = 0
        End With
        With prmTmp12
            .ParameterName = "@numfechaingreso"
            .SqlDbType = SqlDbType.Int
            .Value = 0
        End With
        With cmdTmp  'agregar parametros
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .CommandType = CommandType.StoredProcedure 'comando que ejecuta un procedimiento almacenado
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            dtrAgro2K = cmdTmp.ExecuteReader
            Limpiar()
            While dtrAgro2K.Read
                TextBox1.Text = dtrAgro2K.GetValue(3)
                TextBox2.Text = dtrAgro2K.GetValue(0)
                TextBox3.Text = dtrAgro2K.GetValue(1)
                TextBox4.Text = dtrAgro2K.GetValue(2)
                TextBox5.Text = dtrAgro2K.GetValue(10)
                TextBox6.Text = dtrAgro2K.GetValue(4)
                TextBox7.Text = dtrAgro2K.GetValue(5)
                TextBox8.Text = dtrAgro2K.GetValue(11)
                TextBox9.Text = Format(CDbl(dtrAgro2K.GetValue(9)), "#,##0.#0")
                TextBox10.Text = dtrAgro2K.GetValue(8)
                DateTimePicker1.Value = dtrAgro2K.GetValue(7)
                If intTipoCheque = 2 Then 'solo de consulta de 
                    TextBox13.Text = Format(CDbl(dtrAgro2K.GetValue(13)), "#,##0.#0")
                ElseIf intTipoCheque = 9 Then 'ubica saldo actual para reversar para reversar
                    TextBox11.Text = Format(CDbl(dtrAgro2K.GetValue(13)), "#,##0.#0")
                    TextBox12.Text = TextBox9.Text
                    TextBox13.Text = Format(CDbl(TextBox11.Text) + CDbl(TextBox12.Text), "#,##0.#0")
                    TextBox18.Text = TextBox9.Text
                End If
                TextBox14.Text = IIf(IsDBNull(dtrAgro2K.GetValue(6)) = True, "", dtrAgro2K.GetValue(6))
                TextBox15.Text = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
                TextBox16.Text = IIf(IsDBNull(dtrAgro2K.GetValue(5)) = True, "", dtrAgro2K.GetValue(5))
                TextBox17.Text = dtrAgro2K.GetValue(11)
                TextBox18.Text = Format(CDbl(dtrAgro2K.GetValue(9)), "#,##0.#0")
                TextBox19.Text = NumPalabra(CDbl(TextBox9.Text))
                TextBox20.Text = dtrAgro2K.GetValue(1)
                TextBox21.Text = dtrAgro2K.GetValue(8)
                Label17.Text = dtrAgro2K.GetValue(12)
                Exit While
            End While
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Guardar()

        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngChequesMovimientos", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter

        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@intTipoCheque"
            .SqlDbType = SqlDbType.TinyInt
            .Value = CInt(Label16.Text)
        End With
        With prmTmp02
            .ParameterName = "@strregistro"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp03
            .ParameterName = "@ctabanregistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = CInt(TextBox2.Text)
        End With
        With prmTmp04
            .ParameterName = "@benefregistro"
            .SqlDbType = SqlDbType.SmallInt
            .Value = 0
        End With
        With prmTmp05
            .ParameterName = "@beneficiario"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox7.Text
        End With
        With prmTmp06
            .ParameterName = "@descripcion"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox10.Text
        End With
        With prmTmp07
            .ParameterName = "@fechacheque"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With prmTmp08
            .ParameterName = "@fechaingreso"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With prmTmp09
            .ParameterName = "@documento"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox14.Text
        End With
        With prmTmp10
            .ParameterName = "@monto"
            .SqlDbType = SqlDbType.Decimal
            .Value = CDbl(TextBox9.Text)
        End With
        With prmTmp11
            .ParameterName = "@numfechacheque"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With prmTmp12
            .ParameterName = "@numfechaingreso"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .CommandType = CommandType.StoredProcedure 'comando que ejecuta un procedimiento almacenado
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            cmdTmp.ExecuteNonQuery()
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            'Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try


        'Modificar el registro de las cuentas bancarias
        cmdTmp = New SqlCommand("sp_ModifCtasBancarias", cnnAgro2K)
        With prmTmp13
            .ParameterName = "@ctabanregistro"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox4.Text
        End With
        With prmTmp14
            .ParameterName = "@blnactual"
            .SqlDbType = SqlDbType.Float
            .Value = TextBox13.Text
        End With
        With cmdTmp
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .CommandType = CommandType.StoredProcedure 'comando que ejecuta un procedimiento almacenado
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            cmdTmp.ExecuteNonQuery()
            'MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

End Class
