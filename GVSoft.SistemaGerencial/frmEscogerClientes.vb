Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmEscogerClientes
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents dgClientes As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents IdCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Generar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Limite As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEscogerClientes))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.dgClientes = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.IdCliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Generar = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cliente = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Limite = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgClientes)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(672, 200)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes del Reporte a Generarse"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ListBox2)
        Me.GroupBox2.Controls.Add(Me.ListBox1)
        Me.GroupBox2.Controls.Add(Me.Button4)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(8, 216)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(672, 64)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Acciones sobre los Clientes"
        '
        'ListBox2
        '
        Me.ListBox2.Location = New System.Drawing.Point(416, 40)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(64, 17)
        Me.ListBox2.TabIndex = 5
        Me.ListBox2.Visible = False
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(416, 16)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(64, 17)
        Me.ListBox1.TabIndex = 4
        Me.ListBox1.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(328, 24)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(80, 32)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "No Generar Reporte"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(224, 24)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(80, 32)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Generar Reporte"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(120, 24)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(80, 32)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Seleccionar Ninguno"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(16, 24)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 32)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Seleccionar Todos"
        '
        'dgClientes
        '
        Me.dgClientes.AllowUserToAddRows = False
        Me.dgClientes.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.dgClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgClientes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdCliente, Me.Generar, Me.Cliente, Me.Limite})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgClientes.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgClientes.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgClientes.Location = New System.Drawing.Point(9, 19)
        Me.dgClientes.Name = "dgClientes"
        Me.dgClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgClientes.Size = New System.Drawing.Size(655, 173)
        Me.dgClientes.TabIndex = 54
        '
        'IdCliente
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.IdCliente.DefaultCellStyle = DataGridViewCellStyle1
        Me.IdCliente.HeaderText = "IdCliente"
        Me.IdCliente.Name = "IdCliente"
        Me.IdCliente.ReadOnly = True
        Me.IdCliente.Visible = False
        '
        'Generar
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Generar.DefaultCellStyle = DataGridViewCellStyle2
        Me.Generar.HeaderText = "Generar"
        Me.Generar.Name = "Generar"
        Me.Generar.ReadOnly = True
        Me.Generar.Width = 60
        '
        'Cliente
        '
        Me.Cliente.HeaderText = "Cliente"
        Me.Cliente.Name = "Cliente"
        Me.Cliente.ReadOnly = True
        Me.Cliente.Width = 450
        '
        'Limite
        '
        Me.Limite.HeaderText = "Limite"
        Me.Limite.Name = "Limite"
        Me.Limite.ReadOnly = True
        '
        'frmEscogerClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(680, 285)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEscogerClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Escoger Clientes"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim blnGenerar As Boolean
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmEscogerClientes"

    Private Sub frmEscogerClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            blnGenerar = True
            'MSFlexGrid2.FormatString = "|^Generar  |<Cliente                                                                                                        |>Limite            "
            ListBox1.Items.Clear()
            ListBox2.Items.Clear()
            strQuery = strQueryParte00 + strQueryParte01 + strQueryParte02 + strQueryParte03 + strQueryParte04 + strQueryParte05
            'MsgBox("Hola 1: " & strQuery)
            CargarDatos()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try

    End Sub

    Private Sub frmEscogerClientes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Sub CargarDatos()
        Try

            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                dgClientes.Rows.Add(dtrAgro2K.GetValue(0), "X", dtrAgro2K.GetValue(1), Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(2)), "#,##0.#0"))
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click, Button3.Click, Button4.Click
        Dim i As Integer = 0
        Try

            Select Case sender.name.ToString
                Case "Button1"
                    If dgClientes.Rows.Count > 0 Then
                        For i = 0 To dgClientes.Rows.Count - 1
                            dgClientes.Rows(i).Cells("Generar").Value = "X"
                        Next
                    End If
                    ListBox2.Items.Clear()
                Case "Button2"
                    If dgClientes.Rows.Count > 0 Then
                        For i = 0 To dgClientes.Rows.Count - 1
                            dgClientes.Rows(i).Cells("Generar").Value = ""
                        Next
                    End If
                Case "Button3"
                    If dgClientes.Rows.Count <= 0 Then
                        MsgBox("No hay clientes en el listado.  No se generara un estado de cuenta.", MsgBoxStyle.Information, "Generar Datos")
                        Exit Sub
                    End If
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    SeleccionarClientes()
                    strQuery = strQueryParte00 + strQueryParte01 + strQueryParte02 + strQueryParte03 + strQueryParte04 + strQueryParte05
                    strQuery = Replace(strQuery, "', 0", "', 1")
                    If blnGenerar = True Then
                        Dim frmNew As New actrptViewer
                        'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                        frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                        frmNew.Show()
                        Me.Close()
                    ElseIf blnGenerar = False Then
                        MsgBox("No Puede Generar el Reporte sin Datos.", MsgBoxStyle.Critical, "Error de Datos")
                    End If
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                Case "Button4" : Me.Close()
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Sub SeleccionarClientes()

        Dim intContador01 As Integer = 0
        Dim intContador02 As Integer = 0

        Try

            If dgClientes.Rows.Count <= 0 Then
                Exit Sub
            End If

            intContador01 = 0
            intContador02 = 0
            ListBox1.Items.Clear()
            ListBox2.Items.Clear()

            For intIncr = 0 To dgClientes.Rows.Count - 1
                If dgClientes.Rows(intIncr).Cells("Generar").Value = "X" Then
                    intContador01 = intContador01 + 1
                    ListBox1.Items.Add(dgClientes.Rows(intIncr).Cells("IdCliente").Value)
                Else
                    intContador02 = intContador02 + 1
                    ListBox2.Items.Add(dgClientes.Rows(intIncr).Cells("IdCliente").Value)
                End If
            Next
            blnGenerar = True
            If intContador01 = 0 Then
                blnGenerar = False
            ElseIf intContador02 = 0 Then
            Else
                strQueryParte01 = ""
                If intContador01 >= intContador02 Then
                    If ListBox2.Items.Count > 0 Then                                       'Clientes
                        strQueryParte01 = strQueryParte01 + "'and C.Registro not in ("
                        If ListBox2.Items.Count = 1 Then
                            ListBox2.SelectedIndex = 0
                            strQueryParte01 = strQueryParte01 + "" & ListBox2.SelectedItem & ") ', "
                        ElseIf ListBox2.Items.Count > 1 Then
                            For intIncr = 0 To ListBox2.Items.Count - 2
                                ListBox2.SelectedIndex = intIncr
                                strQueryParte01 = strQueryParte01 + "" & ListBox2.SelectedItem & ", "
                            Next
                            ListBox2.SelectedIndex = ListBox2.Items.Count - 1
                            strQueryParte01 = strQueryParte01 + "" & ListBox2.SelectedItem & ") ', "
                        End If
                    Else
                        strQueryParte01 = strQueryParte01 + "' ', "
                    End If
                ElseIf intContador02 >= intContador01 Then
                    If ListBox1.Items.Count > 0 Then                                       'Clientes
                        strQueryParte01 = strQueryParte01 + "'and C.Registro in ("
                        If ListBox1.Items.Count = 1 Then
                            ListBox1.SelectedIndex = 0
                            strQueryParte01 = strQueryParte01 + "" & ListBox1.SelectedItem & ") ', "
                        ElseIf ListBox1.Items.Count > 1 Then
                            For intIncr = 0 To ListBox1.Items.Count - 2
                                ListBox1.SelectedIndex = intIncr
                                strQueryParte01 = strQueryParte01 + "" & ListBox1.SelectedItem & ", "
                            Next
                            ListBox1.SelectedIndex = ListBox1.Items.Count - 1
                            strQueryParte01 = strQueryParte01 + "" & ListBox1.SelectedItem & ") ', "
                        End If
                    Else
                        strQueryParte01 = strQueryParte01 + "' ', "
                    End If
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub dgClientes_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgClientes.CellClick
        Try


            If dgClientes.Columns.IndexOf(dgClientes.Columns("Generar")) = e.ColumnIndex Then
                If dgClientes.CurrentCell.Value.ToString = "X" Then
                    dgClientes.CurrentCell.Value = ""
                Else
                    dgClientes.CurrentCell.Value = "X"
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

End Class
