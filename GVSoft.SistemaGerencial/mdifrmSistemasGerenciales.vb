Imports System.Data.SqlClient
Imports System.Text
Imports System.Windows.Forms

Public Class mdifrmSistemasGerenciales
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel3 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel4 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents cntmnuInv As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem39 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem53 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem54 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuFac As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem55 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem56 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem59 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem60 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem61 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem62 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem63 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem64 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem65 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem66 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem67 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuPrms As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem68 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem69 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem70 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem71 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem72 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem73 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem74 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem75 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem76 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem77 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem78 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem79 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem80 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem81 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem82 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem83 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem84 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuCxC As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuComis As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuReportes As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents ToolBarButton5 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton8 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton6 As System.Windows.Forms.ToolBarButton
    Friend WithEvents cntmnuPresupuestos As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem20 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem21 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem23 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem25 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem26 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem28 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem29 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem31 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem24 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem27 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem30 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem32 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem33 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem34 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem35 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem36 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem37 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem38 As System.Windows.Forms.MenuItem
    Friend WithEvents ToolBarButton10 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton9 As System.Windows.Forms.ToolBarButton
    Friend WithEvents cntmnuExportar As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem40 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem41 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem42 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem43 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem44 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem45 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem46 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem47 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem48 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem49 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem50 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem51 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem52 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem58 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem85 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem86 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem87 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem88 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem89 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem90 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem57 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem91 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem92 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem93 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem96 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem97 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem98 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem99 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem100 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem101 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem102 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem103 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem104 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem94 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem95 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem105 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem106 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem107 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuContab As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem108 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem109 As System.Windows.Forms.MenuItem
    Friend WithEvents ToolBarButton11 As System.Windows.Forms.ToolBarButton
    Friend WithEvents cntmnuCheques As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem110 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem111 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem113 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem114 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem115 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem117 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem118 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem119 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem121 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem122 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem123 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem125 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem126 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem112 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem116 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem120 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem124 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem127 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem128 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem129 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem130 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem132 As System.Windows.Forms.MenuItem
    Friend WithEvents ToolBarButton12 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ctmCompras As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem133 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem134 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem137 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem138 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem139 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem140 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem143 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem144 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem146 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem147 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem148 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem135 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem136 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem141 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem142 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem145 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem149 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem150 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem151 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem152 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem153 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem154 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem155 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem131 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mdifrmSistemasGerenciales))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel3 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel4 = New System.Windows.Forms.StatusBarPanel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuInv = New System.Windows.Forms.ContextMenu
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem39 = New System.Windows.Forms.MenuItem
        Me.MenuItem53 = New System.Windows.Forms.MenuItem
        Me.MenuItem145 = New System.Windows.Forms.MenuItem
        Me.MenuItem150 = New System.Windows.Forms.MenuItem
        Me.MenuItem153 = New System.Windows.Forms.MenuItem
        Me.MenuItem151 = New System.Windows.Forms.MenuItem
        Me.MenuItem152 = New System.Windows.Forms.MenuItem
        Me.MenuItem154 = New System.Windows.Forms.MenuItem
        Me.MenuItem155 = New System.Windows.Forms.MenuItem
        Me.MenuItem54 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuFac = New System.Windows.Forms.ContextMenu
        Me.MenuItem55 = New System.Windows.Forms.MenuItem
        Me.MenuItem60 = New System.Windows.Forms.MenuItem
        Me.MenuItem61 = New System.Windows.Forms.MenuItem
        Me.MenuItem62 = New System.Windows.Forms.MenuItem
        Me.MenuItem63 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem56 = New System.Windows.Forms.MenuItem
        Me.MenuItem64 = New System.Windows.Forms.MenuItem
        Me.MenuItem65 = New System.Windows.Forms.MenuItem
        Me.MenuItem66 = New System.Windows.Forms.MenuItem
        Me.MenuItem67 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem27 = New System.Windows.Forms.MenuItem
        Me.MenuItem59 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton12 = New System.Windows.Forms.ToolBarButton
        Me.ctmCompras = New System.Windows.Forms.ContextMenu
        Me.MenuItem133 = New System.Windows.Forms.MenuItem
        Me.MenuItem134 = New System.Windows.Forms.MenuItem
        Me.MenuItem137 = New System.Windows.Forms.MenuItem
        Me.MenuItem138 = New System.Windows.Forms.MenuItem
        Me.MenuItem139 = New System.Windows.Forms.MenuItem
        Me.MenuItem140 = New System.Windows.Forms.MenuItem
        Me.MenuItem143 = New System.Windows.Forms.MenuItem
        Me.MenuItem144 = New System.Windows.Forms.MenuItem
        Me.MenuItem146 = New System.Windows.Forms.MenuItem
        Me.MenuItem147 = New System.Windows.Forms.MenuItem
        Me.MenuItem148 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuContab = New System.Windows.Forms.ContextMenu
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem108 = New System.Windows.Forms.MenuItem
        Me.MenuItem109 = New System.Windows.Forms.MenuItem
        Me.MenuItem131 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton11 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuCheques = New System.Windows.Forms.ContextMenu
        Me.MenuItem110 = New System.Windows.Forms.MenuItem
        Me.MenuItem111 = New System.Windows.Forms.MenuItem
        Me.MenuItem112 = New System.Windows.Forms.MenuItem
        Me.MenuItem113 = New System.Windows.Forms.MenuItem
        Me.MenuItem127 = New System.Windows.Forms.MenuItem
        Me.MenuItem114 = New System.Windows.Forms.MenuItem
        Me.MenuItem115 = New System.Windows.Forms.MenuItem
        Me.MenuItem116 = New System.Windows.Forms.MenuItem
        Me.MenuItem117 = New System.Windows.Forms.MenuItem
        Me.MenuItem128 = New System.Windows.Forms.MenuItem
        Me.MenuItem122 = New System.Windows.Forms.MenuItem
        Me.MenuItem123 = New System.Windows.Forms.MenuItem
        Me.MenuItem124 = New System.Windows.Forms.MenuItem
        Me.MenuItem125 = New System.Windows.Forms.MenuItem
        Me.MenuItem130 = New System.Windows.Forms.MenuItem
        Me.MenuItem118 = New System.Windows.Forms.MenuItem
        Me.MenuItem119 = New System.Windows.Forms.MenuItem
        Me.MenuItem120 = New System.Windows.Forms.MenuItem
        Me.MenuItem121 = New System.Windows.Forms.MenuItem
        Me.MenuItem129 = New System.Windows.Forms.MenuItem
        Me.MenuItem126 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuCxC = New System.Windows.Forms.ContextMenu
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem20 = New System.Windows.Forms.MenuItem
        Me.MenuItem23 = New System.Windows.Forms.MenuItem
        Me.MenuItem35 = New System.Windows.Forms.MenuItem
        Me.MenuItem25 = New System.Windows.Forms.MenuItem
        Me.MenuItem142 = New System.Windows.Forms.MenuItem
        Me.MenuItem21 = New System.Windows.Forms.MenuItem
        Me.MenuItem26 = New System.Windows.Forms.MenuItem
        Me.MenuItem36 = New System.Windows.Forms.MenuItem
        Me.MenuItem28 = New System.Windows.Forms.MenuItem
        Me.MenuItem22 = New System.Windows.Forms.MenuItem
        Me.MenuItem29 = New System.Windows.Forms.MenuItem
        Me.MenuItem37 = New System.Windows.Forms.MenuItem
        Me.MenuItem31 = New System.Windows.Forms.MenuItem
        Me.MenuItem30 = New System.Windows.Forms.MenuItem
        Me.MenuItem32 = New System.Windows.Forms.MenuItem
        Me.MenuItem38 = New System.Windows.Forms.MenuItem
        Me.MenuItem33 = New System.Windows.Forms.MenuItem
        Me.MenuItem135 = New System.Windows.Forms.MenuItem
        Me.MenuItem136 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton5 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuComis = New System.Windows.Forms.ContextMenu
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton6 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuPresupuestos = New System.Windows.Forms.ContextMenu
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuPrms = New System.Windows.Forms.ContextMenu
        Me.MenuItem107 = New System.Windows.Forms.MenuItem
        Me.MenuItem68 = New System.Windows.Forms.MenuItem
        Me.MenuItem70 = New System.Windows.Forms.MenuItem
        Me.MenuItem69 = New System.Windows.Forms.MenuItem
        Me.MenuItem19 = New System.Windows.Forms.MenuItem
        Me.MenuItem73 = New System.Windows.Forms.MenuItem
        Me.MenuItem24 = New System.Windows.Forms.MenuItem
        Me.MenuItem76 = New System.Windows.Forms.MenuItem
        Me.MenuItem77 = New System.Windows.Forms.MenuItem
        Me.MenuItem78 = New System.Windows.Forms.MenuItem
        Me.MenuItem79 = New System.Windows.Forms.MenuItem
        Me.MenuItem80 = New System.Windows.Forms.MenuItem
        Me.MenuItem71 = New System.Windows.Forms.MenuItem
        Me.MenuItem75 = New System.Windows.Forms.MenuItem
        Me.MenuItem132 = New System.Windows.Forms.MenuItem
        Me.MenuItem74 = New System.Windows.Forms.MenuItem
        Me.MenuItem72 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem81 = New System.Windows.Forms.MenuItem
        Me.MenuItem34 = New System.Windows.Forms.MenuItem
        Me.MenuItem97 = New System.Windows.Forms.MenuItem
        Me.MenuItem98 = New System.Windows.Forms.MenuItem
        Me.MenuItem99 = New System.Windows.Forms.MenuItem
        Me.MenuItem100 = New System.Windows.Forms.MenuItem
        Me.MenuItem82 = New System.Windows.Forms.MenuItem
        Me.MenuItem101 = New System.Windows.Forms.MenuItem
        Me.MenuItem102 = New System.Windows.Forms.MenuItem
        Me.MenuItem103 = New System.Windows.Forms.MenuItem
        Me.MenuItem104 = New System.Windows.Forms.MenuItem
        Me.MenuItem57 = New System.Windows.Forms.MenuItem
        Me.MenuItem91 = New System.Windows.Forms.MenuItem
        Me.MenuItem96 = New System.Windows.Forms.MenuItem
        Me.MenuItem83 = New System.Windows.Forms.MenuItem
        Me.MenuItem84 = New System.Windows.Forms.MenuItem
        Me.MenuItem94 = New System.Windows.Forms.MenuItem
        Me.MenuItem95 = New System.Windows.Forms.MenuItem
        Me.MenuItem141 = New System.Windows.Forms.MenuItem
        Me.MenuItem149 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton8 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuReportes = New System.Windows.Forms.ContextMenu
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton9 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuExportar = New System.Windows.Forms.ContextMenu
        Me.MenuItem40 = New System.Windows.Forms.MenuItem
        Me.MenuItem41 = New System.Windows.Forms.MenuItem
        Me.MenuItem42 = New System.Windows.Forms.MenuItem
        Me.MenuItem46 = New System.Windows.Forms.MenuItem
        Me.MenuItem47 = New System.Windows.Forms.MenuItem
        Me.MenuItem48 = New System.Windows.Forms.MenuItem
        Me.MenuItem49 = New System.Windows.Forms.MenuItem
        Me.MenuItem50 = New System.Windows.Forms.MenuItem
        Me.MenuItem51 = New System.Windows.Forms.MenuItem
        Me.MenuItem52 = New System.Windows.Forms.MenuItem
        Me.MenuItem92 = New System.Windows.Forms.MenuItem
        Me.MenuItem43 = New System.Windows.Forms.MenuItem
        Me.MenuItem44 = New System.Windows.Forms.MenuItem
        Me.MenuItem45 = New System.Windows.Forms.MenuItem
        Me.MenuItem58 = New System.Windows.Forms.MenuItem
        Me.MenuItem85 = New System.Windows.Forms.MenuItem
        Me.MenuItem86 = New System.Windows.Forms.MenuItem
        Me.MenuItem87 = New System.Windows.Forms.MenuItem
        Me.MenuItem88 = New System.Windows.Forms.MenuItem
        Me.MenuItem89 = New System.Windows.Forms.MenuItem
        Me.MenuItem90 = New System.Windows.Forms.MenuItem
        Me.MenuItem93 = New System.Windows.Forms.MenuItem
        Me.MenuItem105 = New System.Windows.Forms.MenuItem
        Me.MenuItem106 = New System.Windows.Forms.MenuItem
        Me.ToolBarButton10 = New System.Windows.Forms.ToolBarButton
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        Me.ImageList1.Images.SetKeyName(7, "")
        Me.ImageList1.Images.SetKeyName(8, "")
        Me.ImageList1.Images.SetKeyName(9, "")
        Me.ImageList1.Images.SetKeyName(10, "")
        Me.ImageList1.Images.SetKeyName(11, "")
        '
        'StatusBar1
        '
        resources.ApplyResources(Me.StatusBar1, "StatusBar1")
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2, Me.StatusBarPanel3, Me.StatusBarPanel4})
        Me.StatusBar1.ShowPanels = True
        '
        'StatusBarPanel1
        '
        resources.ApplyResources(Me.StatusBarPanel1, "StatusBarPanel1")
        '
        'StatusBarPanel2
        '
        resources.ApplyResources(Me.StatusBarPanel2, "StatusBarPanel2")
        '
        'StatusBarPanel3
        '
        resources.ApplyResources(Me.StatusBarPanel3, "StatusBarPanel3")
        '
        'StatusBarPanel4
        '
        resources.ApplyResources(Me.StatusBarPanel4, "StatusBarPanel4")
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton12, Me.ToolBarButton3, Me.ToolBarButton11, Me.ToolBarButton4, Me.ToolBarButton5, Me.ToolBarButton6, Me.ToolBarButton7, Me.ToolBarButton8, Me.ToolBarButton9, Me.ToolBarButton10})
        resources.ApplyResources(Me.ToolBar1, "ToolBar1")
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Name = "ToolBar1"
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.DropDownMenu = Me.cntmnuInv
        resources.ApplyResources(Me.ToolBarButton1, "ToolBarButton1")
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuInv
        '
        Me.cntmnuInv.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem3, Me.MenuItem39, Me.MenuItem53, Me.MenuItem145, Me.MenuItem54})
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 0
        resources.ApplyResources(Me.MenuItem3, "MenuItem3")
        '
        'MenuItem39
        '
        Me.MenuItem39.Index = 1
        resources.ApplyResources(Me.MenuItem39, "MenuItem39")
        '
        'MenuItem53
        '
        Me.MenuItem53.Index = 2
        resources.ApplyResources(Me.MenuItem53, "MenuItem53")
        '
        'MenuItem145
        '
        Me.MenuItem145.Index = 3
        Me.MenuItem145.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem150, Me.MenuItem153, Me.MenuItem151, Me.MenuItem152, Me.MenuItem154, Me.MenuItem155})
        resources.ApplyResources(Me.MenuItem145, "MenuItem145")
        '
        'MenuItem150
        '
        Me.MenuItem150.Index = 0
        resources.ApplyResources(Me.MenuItem150, "MenuItem150")
        '
        'MenuItem153
        '
        Me.MenuItem153.Index = 1
        resources.ApplyResources(Me.MenuItem153, "MenuItem153")
        '
        'MenuItem151
        '
        Me.MenuItem151.Index = 2
        resources.ApplyResources(Me.MenuItem151, "MenuItem151")
        '
        'MenuItem152
        '
        Me.MenuItem152.Index = 3
        resources.ApplyResources(Me.MenuItem152, "MenuItem152")
        '
        'MenuItem154
        '
        Me.MenuItem154.Index = 4
        resources.ApplyResources(Me.MenuItem154, "MenuItem154")
        '
        'MenuItem155
        '
        Me.MenuItem155.Index = 5
        resources.ApplyResources(Me.MenuItem155, "MenuItem155")
        '
        'MenuItem54
        '
        Me.MenuItem54.Index = 4
        resources.ApplyResources(Me.MenuItem54, "MenuItem54")
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.DropDownMenu = Me.cntmnuFac
        resources.ApplyResources(Me.ToolBarButton2, "ToolBarButton2")
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuFac
        '
        Me.cntmnuFac.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem55, Me.MenuItem56, Me.MenuItem13, Me.MenuItem5, Me.MenuItem27, Me.MenuItem59})
        '
        'MenuItem55
        '
        Me.MenuItem55.Index = 0
        Me.MenuItem55.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem60, Me.MenuItem61, Me.MenuItem62, Me.MenuItem63, Me.MenuItem6})
        resources.ApplyResources(Me.MenuItem55, "MenuItem55")
        '
        'MenuItem60
        '
        Me.MenuItem60.Index = 0
        resources.ApplyResources(Me.MenuItem60, "MenuItem60")
        '
        'MenuItem61
        '
        Me.MenuItem61.Index = 1
        resources.ApplyResources(Me.MenuItem61, "MenuItem61")
        '
        'MenuItem62
        '
        Me.MenuItem62.Index = 2
        resources.ApplyResources(Me.MenuItem62, "MenuItem62")
        '
        'MenuItem63
        '
        Me.MenuItem63.Index = 3
        resources.ApplyResources(Me.MenuItem63, "MenuItem63")
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        resources.ApplyResources(Me.MenuItem6, "MenuItem6")
        '
        'MenuItem56
        '
        Me.MenuItem56.Index = 1
        Me.MenuItem56.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem64, Me.MenuItem65, Me.MenuItem66, Me.MenuItem67, Me.MenuItem7})
        resources.ApplyResources(Me.MenuItem56, "MenuItem56")
        '
        'MenuItem64
        '
        Me.MenuItem64.Index = 0
        resources.ApplyResources(Me.MenuItem64, "MenuItem64")
        '
        'MenuItem65
        '
        Me.MenuItem65.Index = 1
        resources.ApplyResources(Me.MenuItem65, "MenuItem65")
        '
        'MenuItem66
        '
        Me.MenuItem66.Index = 2
        resources.ApplyResources(Me.MenuItem66, "MenuItem66")
        '
        'MenuItem67
        '
        Me.MenuItem67.Index = 3
        resources.ApplyResources(Me.MenuItem67, "MenuItem67")
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 4
        resources.ApplyResources(Me.MenuItem7, "MenuItem7")
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 2
        resources.ApplyResources(Me.MenuItem13, "MenuItem13")
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        resources.ApplyResources(Me.MenuItem5, "MenuItem5")
        '
        'MenuItem27
        '
        Me.MenuItem27.Index = 4
        resources.ApplyResources(Me.MenuItem27, "MenuItem27")
        '
        'MenuItem59
        '
        Me.MenuItem59.Index = 5
        resources.ApplyResources(Me.MenuItem59, "MenuItem59")
        '
        'ToolBarButton12
        '
        Me.ToolBarButton12.DropDownMenu = Me.ctmCompras
        resources.ApplyResources(Me.ToolBarButton12, "ToolBarButton12")
        Me.ToolBarButton12.Name = "ToolBarButton12"
        Me.ToolBarButton12.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'ctmCompras
        '
        Me.ctmCompras.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem133, Me.MenuItem139, Me.MenuItem146, Me.MenuItem147, Me.MenuItem148})
        '
        'MenuItem133
        '
        Me.MenuItem133.Index = 0
        Me.MenuItem133.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem134, Me.MenuItem137, Me.MenuItem138})
        resources.ApplyResources(Me.MenuItem133, "MenuItem133")
        '
        'MenuItem134
        '
        Me.MenuItem134.Index = 0
        resources.ApplyResources(Me.MenuItem134, "MenuItem134")
        '
        'MenuItem137
        '
        Me.MenuItem137.Index = 1
        resources.ApplyResources(Me.MenuItem137, "MenuItem137")
        '
        'MenuItem138
        '
        Me.MenuItem138.Index = 2
        resources.ApplyResources(Me.MenuItem138, "MenuItem138")
        '
        'MenuItem139
        '
        Me.MenuItem139.Index = 1
        Me.MenuItem139.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem140, Me.MenuItem143, Me.MenuItem144})
        resources.ApplyResources(Me.MenuItem139, "MenuItem139")
        '
        'MenuItem140
        '
        Me.MenuItem140.Index = 0
        resources.ApplyResources(Me.MenuItem140, "MenuItem140")
        '
        'MenuItem143
        '
        Me.MenuItem143.Index = 1
        resources.ApplyResources(Me.MenuItem143, "MenuItem143")
        '
        'MenuItem144
        '
        Me.MenuItem144.Index = 2
        resources.ApplyResources(Me.MenuItem144, "MenuItem144")
        '
        'MenuItem146
        '
        Me.MenuItem146.Index = 2
        resources.ApplyResources(Me.MenuItem146, "MenuItem146")
        '
        'MenuItem147
        '
        Me.MenuItem147.Index = 3
        resources.ApplyResources(Me.MenuItem147, "MenuItem147")
        '
        'MenuItem148
        '
        Me.MenuItem148.Index = 4
        resources.ApplyResources(Me.MenuItem148, "MenuItem148")
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.DropDownMenu = Me.cntmnuContab
        resources.ApplyResources(Me.ToolBarButton3, "ToolBarButton3")
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuContab
        '
        Me.cntmnuContab.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem108, Me.MenuItem109, Me.MenuItem131})
        resources.ApplyResources(Me.MenuItem2, "MenuItem2")
        '
        'MenuItem108
        '
        Me.MenuItem108.Index = 0
        resources.ApplyResources(Me.MenuItem108, "MenuItem108")
        '
        'MenuItem109
        '
        Me.MenuItem109.Index = 1
        resources.ApplyResources(Me.MenuItem109, "MenuItem109")
        '
        'MenuItem131
        '
        Me.MenuItem131.Index = 2
        resources.ApplyResources(Me.MenuItem131, "MenuItem131")
        '
        'ToolBarButton11
        '
        Me.ToolBarButton11.DropDownMenu = Me.cntmnuCheques
        resources.ApplyResources(Me.ToolBarButton11, "ToolBarButton11")
        Me.ToolBarButton11.Name = "ToolBarButton11"
        Me.ToolBarButton11.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuCheques
        '
        Me.cntmnuCheques.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem110, Me.MenuItem114, Me.MenuItem122, Me.MenuItem118, Me.MenuItem126})
        '
        'MenuItem110
        '
        Me.MenuItem110.Index = 0
        Me.MenuItem110.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem111, Me.MenuItem112, Me.MenuItem113, Me.MenuItem127})
        resources.ApplyResources(Me.MenuItem110, "MenuItem110")
        '
        'MenuItem111
        '
        Me.MenuItem111.Index = 0
        resources.ApplyResources(Me.MenuItem111, "MenuItem111")
        '
        'MenuItem112
        '
        Me.MenuItem112.Index = 1
        resources.ApplyResources(Me.MenuItem112, "MenuItem112")
        '
        'MenuItem113
        '
        Me.MenuItem113.Index = 2
        resources.ApplyResources(Me.MenuItem113, "MenuItem113")
        '
        'MenuItem127
        '
        Me.MenuItem127.Index = 3
        resources.ApplyResources(Me.MenuItem127, "MenuItem127")
        '
        'MenuItem114
        '
        Me.MenuItem114.Index = 1
        Me.MenuItem114.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem115, Me.MenuItem116, Me.MenuItem117, Me.MenuItem128})
        resources.ApplyResources(Me.MenuItem114, "MenuItem114")
        '
        'MenuItem115
        '
        Me.MenuItem115.Index = 0
        resources.ApplyResources(Me.MenuItem115, "MenuItem115")
        '
        'MenuItem116
        '
        Me.MenuItem116.Index = 1
        resources.ApplyResources(Me.MenuItem116, "MenuItem116")
        '
        'MenuItem117
        '
        Me.MenuItem117.Index = 2
        resources.ApplyResources(Me.MenuItem117, "MenuItem117")
        '
        'MenuItem128
        '
        Me.MenuItem128.Index = 3
        resources.ApplyResources(Me.MenuItem128, "MenuItem128")
        '
        'MenuItem122
        '
        Me.MenuItem122.Index = 2
        Me.MenuItem122.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem123, Me.MenuItem124, Me.MenuItem125, Me.MenuItem130})
        resources.ApplyResources(Me.MenuItem122, "MenuItem122")
        '
        'MenuItem123
        '
        Me.MenuItem123.Index = 0
        resources.ApplyResources(Me.MenuItem123, "MenuItem123")
        '
        'MenuItem124
        '
        Me.MenuItem124.Index = 1
        resources.ApplyResources(Me.MenuItem124, "MenuItem124")
        '
        'MenuItem125
        '
        Me.MenuItem125.Index = 2
        resources.ApplyResources(Me.MenuItem125, "MenuItem125")
        '
        'MenuItem130
        '
        Me.MenuItem130.Index = 3
        resources.ApplyResources(Me.MenuItem130, "MenuItem130")
        '
        'MenuItem118
        '
        Me.MenuItem118.Index = 3
        Me.MenuItem118.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem119, Me.MenuItem120, Me.MenuItem121, Me.MenuItem129})
        resources.ApplyResources(Me.MenuItem118, "MenuItem118")
        '
        'MenuItem119
        '
        Me.MenuItem119.Index = 0
        resources.ApplyResources(Me.MenuItem119, "MenuItem119")
        '
        'MenuItem120
        '
        Me.MenuItem120.Index = 1
        resources.ApplyResources(Me.MenuItem120, "MenuItem120")
        '
        'MenuItem121
        '
        Me.MenuItem121.Index = 2
        resources.ApplyResources(Me.MenuItem121, "MenuItem121")
        '
        'MenuItem129
        '
        Me.MenuItem129.Index = 3
        resources.ApplyResources(Me.MenuItem129, "MenuItem129")
        '
        'MenuItem126
        '
        Me.MenuItem126.Index = 4
        resources.ApplyResources(Me.MenuItem126, "MenuItem126")
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuCxC
        resources.ApplyResources(Me.ToolBarButton4, "ToolBarButton4")
        Me.ToolBarButton4.Name = "ToolBarButton4"
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuCxC
        '
        Me.cntmnuCxC.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem4, Me.MenuItem1})
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 0
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem20, Me.MenuItem21, Me.MenuItem22, Me.MenuItem30, Me.MenuItem135, Me.MenuItem136})
        resources.ApplyResources(Me.MenuItem4, "MenuItem4")
        '
        'MenuItem20
        '
        Me.MenuItem20.Index = 0
        Me.MenuItem20.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem23, Me.MenuItem35, Me.MenuItem25, Me.MenuItem142})
        resources.ApplyResources(Me.MenuItem20, "MenuItem20")
        '
        'MenuItem23
        '
        Me.MenuItem23.Index = 0
        resources.ApplyResources(Me.MenuItem23, "MenuItem23")
        '
        'MenuItem35
        '
        Me.MenuItem35.Index = 1
        resources.ApplyResources(Me.MenuItem35, "MenuItem35")
        '
        'MenuItem25
        '
        Me.MenuItem25.Index = 2
        resources.ApplyResources(Me.MenuItem25, "MenuItem25")
        '
        'MenuItem142
        '
        Me.MenuItem142.Index = 3
        resources.ApplyResources(Me.MenuItem142, "MenuItem142")
        '
        'MenuItem21
        '
        Me.MenuItem21.Index = 1
        Me.MenuItem21.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem26, Me.MenuItem36, Me.MenuItem28})
        resources.ApplyResources(Me.MenuItem21, "MenuItem21")
        '
        'MenuItem26
        '
        Me.MenuItem26.Index = 0
        resources.ApplyResources(Me.MenuItem26, "MenuItem26")
        '
        'MenuItem36
        '
        Me.MenuItem36.Index = 1
        resources.ApplyResources(Me.MenuItem36, "MenuItem36")
        '
        'MenuItem28
        '
        Me.MenuItem28.Index = 2
        resources.ApplyResources(Me.MenuItem28, "MenuItem28")
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 2
        Me.MenuItem22.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem29, Me.MenuItem37, Me.MenuItem31})
        resources.ApplyResources(Me.MenuItem22, "MenuItem22")
        '
        'MenuItem29
        '
        Me.MenuItem29.Index = 0
        resources.ApplyResources(Me.MenuItem29, "MenuItem29")
        '
        'MenuItem37
        '
        Me.MenuItem37.Index = 1
        resources.ApplyResources(Me.MenuItem37, "MenuItem37")
        '
        'MenuItem31
        '
        Me.MenuItem31.Index = 2
        resources.ApplyResources(Me.MenuItem31, "MenuItem31")
        '
        'MenuItem30
        '
        Me.MenuItem30.Index = 3
        Me.MenuItem30.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem32, Me.MenuItem38, Me.MenuItem33})
        resources.ApplyResources(Me.MenuItem30, "MenuItem30")
        '
        'MenuItem32
        '
        Me.MenuItem32.Index = 0
        resources.ApplyResources(Me.MenuItem32, "MenuItem32")
        '
        'MenuItem38
        '
        Me.MenuItem38.Index = 1
        resources.ApplyResources(Me.MenuItem38, "MenuItem38")
        '
        'MenuItem33
        '
        Me.MenuItem33.Index = 2
        resources.ApplyResources(Me.MenuItem33, "MenuItem33")
        '
        'MenuItem135
        '
        Me.MenuItem135.Index = 4
        resources.ApplyResources(Me.MenuItem135, "MenuItem135")
        '
        'MenuItem136
        '
        Me.MenuItem136.Index = 5
        resources.ApplyResources(Me.MenuItem136, "MenuItem136")
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 1
        resources.ApplyResources(Me.MenuItem1, "MenuItem1")
        '
        'ToolBarButton5
        '
        Me.ToolBarButton5.DropDownMenu = Me.cntmnuComis
        resources.ApplyResources(Me.ToolBarButton5, "ToolBarButton5")
        Me.ToolBarButton5.Name = "ToolBarButton5"
        Me.ToolBarButton5.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuComis
        '
        Me.cntmnuComis.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem11})
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 0
        resources.ApplyResources(Me.MenuItem11, "MenuItem11")
        '
        'ToolBarButton6
        '
        Me.ToolBarButton6.DropDownMenu = Me.cntmnuPresupuestos
        resources.ApplyResources(Me.ToolBarButton6, "ToolBarButton6")
        Me.ToolBarButton6.Name = "ToolBarButton6"
        Me.ToolBarButton6.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuPresupuestos
        '
        Me.cntmnuPresupuestos.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15, Me.MenuItem16, Me.MenuItem17})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        resources.ApplyResources(Me.MenuItem15, "MenuItem15")
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 1
        resources.ApplyResources(Me.MenuItem16, "MenuItem16")
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 2
        resources.ApplyResources(Me.MenuItem17, "MenuItem17")
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.DropDownMenu = Me.cntmnuPrms
        resources.ApplyResources(Me.ToolBarButton7, "ToolBarButton7")
        Me.ToolBarButton7.Name = "ToolBarButton7"
        Me.ToolBarButton7.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuPrms
        '
        Me.cntmnuPrms.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem107, Me.MenuItem68, Me.MenuItem70, Me.MenuItem69, Me.MenuItem19, Me.MenuItem73, Me.MenuItem24, Me.MenuItem76, Me.MenuItem79, Me.MenuItem80, Me.MenuItem71, Me.MenuItem75, Me.MenuItem132, Me.MenuItem74, Me.MenuItem72, Me.MenuItem8, Me.MenuItem81, Me.MenuItem34, Me.MenuItem82, Me.MenuItem57, Me.MenuItem91, Me.MenuItem96, Me.MenuItem83, Me.MenuItem84, Me.MenuItem94, Me.MenuItem95, Me.MenuItem141, Me.MenuItem149})
        '
        'MenuItem107
        '
        Me.MenuItem107.Index = 0
        resources.ApplyResources(Me.MenuItem107, "MenuItem107")
        '
        'MenuItem68
        '
        Me.MenuItem68.Index = 1
        resources.ApplyResources(Me.MenuItem68, "MenuItem68")
        '
        'MenuItem70
        '
        Me.MenuItem70.Index = 2
        resources.ApplyResources(Me.MenuItem70, "MenuItem70")
        '
        'MenuItem69
        '
        Me.MenuItem69.Index = 3
        resources.ApplyResources(Me.MenuItem69, "MenuItem69")
        '
        'MenuItem19
        '
        Me.MenuItem19.Index = 4
        resources.ApplyResources(Me.MenuItem19, "MenuItem19")
        '
        'MenuItem73
        '
        Me.MenuItem73.Index = 5
        resources.ApplyResources(Me.MenuItem73, "MenuItem73")
        '
        'MenuItem24
        '
        Me.MenuItem24.Index = 6
        resources.ApplyResources(Me.MenuItem24, "MenuItem24")
        '
        'MenuItem76
        '
        Me.MenuItem76.Index = 7
        Me.MenuItem76.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem77, Me.MenuItem78})
        resources.ApplyResources(Me.MenuItem76, "MenuItem76")
        '
        'MenuItem77
        '
        Me.MenuItem77.Index = 0
        resources.ApplyResources(Me.MenuItem77, "MenuItem77")
        '
        'MenuItem78
        '
        Me.MenuItem78.Index = 1
        resources.ApplyResources(Me.MenuItem78, "MenuItem78")
        '
        'MenuItem79
        '
        Me.MenuItem79.Index = 8
        resources.ApplyResources(Me.MenuItem79, "MenuItem79")
        '
        'MenuItem80
        '
        Me.MenuItem80.Index = 9
        resources.ApplyResources(Me.MenuItem80, "MenuItem80")
        '
        'MenuItem71
        '
        Me.MenuItem71.Index = 10
        resources.ApplyResources(Me.MenuItem71, "MenuItem71")
        '
        'MenuItem75
        '
        Me.MenuItem75.Index = 11
        resources.ApplyResources(Me.MenuItem75, "MenuItem75")
        '
        'MenuItem132
        '
        Me.MenuItem132.Index = 12
        resources.ApplyResources(Me.MenuItem132, "MenuItem132")
        '
        'MenuItem74
        '
        Me.MenuItem74.Index = 13
        resources.ApplyResources(Me.MenuItem74, "MenuItem74")
        '
        'MenuItem72
        '
        Me.MenuItem72.Index = 14
        resources.ApplyResources(Me.MenuItem72, "MenuItem72")
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 15
        resources.ApplyResources(Me.MenuItem8, "MenuItem8")
        '
        'MenuItem81
        '
        Me.MenuItem81.Index = 16
        resources.ApplyResources(Me.MenuItem81, "MenuItem81")
        '
        'MenuItem34
        '
        Me.MenuItem34.Index = 17
        Me.MenuItem34.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem97, Me.MenuItem98, Me.MenuItem99, Me.MenuItem100})
        resources.ApplyResources(Me.MenuItem34, "MenuItem34")
        '
        'MenuItem97
        '
        Me.MenuItem97.Index = 0
        resources.ApplyResources(Me.MenuItem97, "MenuItem97")
        '
        'MenuItem98
        '
        Me.MenuItem98.Index = 1
        resources.ApplyResources(Me.MenuItem98, "MenuItem98")
        '
        'MenuItem99
        '
        Me.MenuItem99.Index = 2
        resources.ApplyResources(Me.MenuItem99, "MenuItem99")
        '
        'MenuItem100
        '
        Me.MenuItem100.Index = 3
        resources.ApplyResources(Me.MenuItem100, "MenuItem100")
        '
        'MenuItem82
        '
        Me.MenuItem82.Index = 18
        Me.MenuItem82.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem101, Me.MenuItem102, Me.MenuItem103, Me.MenuItem104})
        resources.ApplyResources(Me.MenuItem82, "MenuItem82")
        '
        'MenuItem101
        '
        Me.MenuItem101.Index = 0
        resources.ApplyResources(Me.MenuItem101, "MenuItem101")
        '
        'MenuItem102
        '
        Me.MenuItem102.Index = 1
        resources.ApplyResources(Me.MenuItem102, "MenuItem102")
        '
        'MenuItem103
        '
        Me.MenuItem103.Index = 2
        resources.ApplyResources(Me.MenuItem103, "MenuItem103")
        '
        'MenuItem104
        '
        Me.MenuItem104.Index = 3
        resources.ApplyResources(Me.MenuItem104, "MenuItem104")
        '
        'MenuItem57
        '
        Me.MenuItem57.Index = 19
        resources.ApplyResources(Me.MenuItem57, "MenuItem57")
        '
        'MenuItem91
        '
        Me.MenuItem91.Index = 20
        resources.ApplyResources(Me.MenuItem91, "MenuItem91")
        '
        'MenuItem96
        '
        Me.MenuItem96.Index = 21
        resources.ApplyResources(Me.MenuItem96, "MenuItem96")
        '
        'MenuItem83
        '
        Me.MenuItem83.Index = 22
        resources.ApplyResources(Me.MenuItem83, "MenuItem83")
        '
        'MenuItem84
        '
        Me.MenuItem84.Index = 23
        resources.ApplyResources(Me.MenuItem84, "MenuItem84")
        '
        'MenuItem94
        '
        Me.MenuItem94.Index = 24
        resources.ApplyResources(Me.MenuItem94, "MenuItem94")
        '
        'MenuItem95
        '
        Me.MenuItem95.Index = 25
        resources.ApplyResources(Me.MenuItem95, "MenuItem95")
        '
        'MenuItem141
        '
        Me.MenuItem141.Index = 26
        resources.ApplyResources(Me.MenuItem141, "MenuItem141")
        '
        'MenuItem149
        '
        Me.MenuItem149.Index = 27
        resources.ApplyResources(Me.MenuItem149, "MenuItem149")
        '
        'ToolBarButton8
        '
        Me.ToolBarButton8.DropDownMenu = Me.cntmnuReportes
        resources.ApplyResources(Me.ToolBarButton8, "ToolBarButton8")
        Me.ToolBarButton8.Name = "ToolBarButton8"
        Me.ToolBarButton8.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuReportes
        '
        Me.cntmnuReportes.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem9, Me.MenuItem10, Me.MenuItem12, Me.MenuItem14, Me.MenuItem18})
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 0
        resources.ApplyResources(Me.MenuItem9, "MenuItem9")
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 1
        resources.ApplyResources(Me.MenuItem10, "MenuItem10")
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 2
        resources.ApplyResources(Me.MenuItem12, "MenuItem12")
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 3
        resources.ApplyResources(Me.MenuItem14, "MenuItem14")
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 4
        resources.ApplyResources(Me.MenuItem18, "MenuItem18")
        '
        'ToolBarButton9
        '
        Me.ToolBarButton9.DropDownMenu = Me.cntmnuExportar
        resources.ApplyResources(Me.ToolBarButton9, "ToolBarButton9")
        Me.ToolBarButton9.Name = "ToolBarButton9"
        Me.ToolBarButton9.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        '
        'cntmnuExportar
        '
        Me.cntmnuExportar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem40, Me.MenuItem43, Me.MenuItem105})
        '
        'MenuItem40
        '
        Me.MenuItem40.Index = 0
        Me.MenuItem40.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem41, Me.MenuItem42, Me.MenuItem46, Me.MenuItem47, Me.MenuItem48, Me.MenuItem49, Me.MenuItem50, Me.MenuItem51, Me.MenuItem52, Me.MenuItem92})
        resources.ApplyResources(Me.MenuItem40, "MenuItem40")
        '
        'MenuItem41
        '
        Me.MenuItem41.Index = 0
        resources.ApplyResources(Me.MenuItem41, "MenuItem41")
        '
        'MenuItem42
        '
        Me.MenuItem42.Index = 1
        resources.ApplyResources(Me.MenuItem42, "MenuItem42")
        '
        'MenuItem46
        '
        Me.MenuItem46.Index = 2
        resources.ApplyResources(Me.MenuItem46, "MenuItem46")
        '
        'MenuItem47
        '
        Me.MenuItem47.Index = 3
        resources.ApplyResources(Me.MenuItem47, "MenuItem47")
        '
        'MenuItem48
        '
        Me.MenuItem48.Index = 4
        resources.ApplyResources(Me.MenuItem48, "MenuItem48")
        '
        'MenuItem49
        '
        Me.MenuItem49.Index = 5
        resources.ApplyResources(Me.MenuItem49, "MenuItem49")
        '
        'MenuItem50
        '
        Me.MenuItem50.Index = 6
        resources.ApplyResources(Me.MenuItem50, "MenuItem50")
        '
        'MenuItem51
        '
        Me.MenuItem51.Index = 7
        resources.ApplyResources(Me.MenuItem51, "MenuItem51")
        '
        'MenuItem52
        '
        Me.MenuItem52.Index = 8
        resources.ApplyResources(Me.MenuItem52, "MenuItem52")
        '
        'MenuItem92
        '
        Me.MenuItem92.Index = 9
        resources.ApplyResources(Me.MenuItem92, "MenuItem92")
        '
        'MenuItem43
        '
        Me.MenuItem43.Index = 1
        Me.MenuItem43.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem44, Me.MenuItem45, Me.MenuItem58, Me.MenuItem85, Me.MenuItem86, Me.MenuItem87, Me.MenuItem88, Me.MenuItem89, Me.MenuItem90, Me.MenuItem93})
        resources.ApplyResources(Me.MenuItem43, "MenuItem43")
        '
        'MenuItem44
        '
        Me.MenuItem44.Index = 0
        resources.ApplyResources(Me.MenuItem44, "MenuItem44")
        '
        'MenuItem45
        '
        Me.MenuItem45.Index = 1
        resources.ApplyResources(Me.MenuItem45, "MenuItem45")
        '
        'MenuItem58
        '
        Me.MenuItem58.Index = 2
        resources.ApplyResources(Me.MenuItem58, "MenuItem58")
        '
        'MenuItem85
        '
        Me.MenuItem85.Index = 3
        resources.ApplyResources(Me.MenuItem85, "MenuItem85")
        '
        'MenuItem86
        '
        Me.MenuItem86.Index = 4
        resources.ApplyResources(Me.MenuItem86, "MenuItem86")
        '
        'MenuItem87
        '
        Me.MenuItem87.Index = 5
        resources.ApplyResources(Me.MenuItem87, "MenuItem87")
        '
        'MenuItem88
        '
        Me.MenuItem88.Index = 6
        resources.ApplyResources(Me.MenuItem88, "MenuItem88")
        '
        'MenuItem89
        '
        Me.MenuItem89.Index = 7
        resources.ApplyResources(Me.MenuItem89, "MenuItem89")
        '
        'MenuItem90
        '
        Me.MenuItem90.Index = 8
        resources.ApplyResources(Me.MenuItem90, "MenuItem90")
        '
        'MenuItem93
        '
        Me.MenuItem93.Index = 9
        resources.ApplyResources(Me.MenuItem93, "MenuItem93")
        '
        'MenuItem105
        '
        Me.MenuItem105.Index = 2
        Me.MenuItem105.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem106})
        resources.ApplyResources(Me.MenuItem105, "MenuItem105")
        '
        'MenuItem106
        '
        Me.MenuItem106.Index = 0
        resources.ApplyResources(Me.MenuItem106, "MenuItem106")
        '
        'ToolBarButton10
        '
        resources.ApplyResources(Me.ToolBarButton10, "ToolBarButton10")
        Me.ToolBarButton10.Name = "ToolBarButton10"
        '
        'mdifrmSistemasGerenciales
        '
        resources.ApplyResources(Me, "$this")
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.StatusBar1)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.Name = "mdifrmSistemasGerenciales"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub mdifrmSistemasGerenciales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Timer1.Enabled = True
        Timer1.Interval = 10
        StatusBar1.Panels.Item(1).Text = Format(Now, "dddd, dd-MMM-yyyy")
        StatusBar1.Panels.Item(2).Text = Format(Now, "hh:mm:ss tt")
        StatusBar1.Panels.Item(3).Text = strNombreUsuario
        blnMainMenu = True
        intModulo = 0
        lngRegistro = 0
        lngRegAgencia = 0
        intTipoFactura = 0
        intExportarModulo = 0
        blnUbicar = False
        strUbicar = ""
        DarAccesoUsuario()
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptExportar = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        intListadoAyuda = 0
        strFechaInicial = ""
        strFechaFinal = ""
        strFechaReporte = ""
        intTipoReciboCaja = 0
        dblMontoReciboNTDNTC = 0

    End Sub

    Private Sub mdifrmSistemasGerenciales_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        Application.Exit()

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 11
                Me.Close()
        End Select

    End Sub

    Sub DarAccesoUsuario()

        ' Usuarios
        If intAccesos(1) = 0 And intAccesos(2) = 0 And intAccesos(3) = 0 Then
            MenuItem68.Visible = False
        End If
        ' Agencias
        If intAccesos(5) = 0 And intAccesos(6) = 0 And intAccesos(7) = 0 Then
            MenuItem70.Visible = False
        End If
        ' Departamentos
        If intAccesos(9) = 0 And intAccesos(10) = 0 And intAccesos(11) = 0 Then
            MenuItem69.Visible = False
        End If
        ' Municipios
        If intAccesos(13) = 0 And intAccesos(14) = 0 And intAccesos(15) = 0 Then
            MenuItem19.Visible = False
        End If
        ' Vendedores
        If intAccesos(17) = 0 And intAccesos(18) = 0 And intAccesos(19) = 0 Then
            MenuItem73.Visible = False
        End If
        ' Clientes
        If intAccesos(21) = 0 And intAccesos(22) = 0 And intAccesos(23) = 0 And intAccesos(24) = 0 And intAccesos(25) = 0 And intAccesos(26) = 0 Then
            MenuItem24.Visible = False
        End If
        ' Clases
        If intAccesos(28) = 0 And intAccesos(29) = 0 And intAccesos(30) = 0 Then
            MenuItem77.Visible = False
        End If
        ' SubClases
        If intAccesos(32) = 0 And intAccesos(33) = 0 And intAccesos(34) = 0 Then
            MenuItem78.Visible = False
        End If
        If MenuItem77.Visible = False And MenuItem78.Visible = False Then
            MenuItem76.Visible = False
        End If
        ' Empaques
        If intAccesos(36) = 0 And intAccesos(37) = 0 And intAccesos(38) = 0 Then
            MenuItem79.Visible = False
        End If
        ' Unidades
        If intAccesos(40) = 0 And intAccesos(41) = 0 And intAccesos(42) = 0 Then
            MenuItem80.Visible = False
        End If
        ' Proveedores
        If intAccesos(44) = 0 And intAccesos(45) = 0 And intAccesos(46) = 0 Then
            MenuItem71.Visible = False
        End If
        ' Productos
        If intAccesos(48) = 0 And intAccesos(49) = 0 And intAccesos(50) = 0 And intAccesos(51) = 0 Then
            MenuItem75.Visible = False
        End If
        ' Conversiones
        If intAccesos(53) = 0 And intAccesos(54) = 0 And intAccesos(55) = 0 Then
            MenuItem74.Visible = False
        End If
        ' Negocios
        If intAccesos(57) = 0 And intAccesos(58) = 0 And intAccesos(59) = 0 Then
            MenuItem72.Visible = False
        End If
        ' Movimientos Inventario
        If intAccesos(61) = 0 Then
            MenuItem3.Visible = False
        End If
        ' Movimientos Conversiones
        If intAccesos(63) = 0 Then
            MenuItem39.Visible = False
        End If
        ' Presupuestos
        If intAccesos(65) = 0 And intAccesos(66) = 0 Then
            MenuItem15.Visible = False
            MenuItem16.Visible = False
        End If
        ' Razones para Anular
        If intAccesos(68) = 0 And intAccesos(69) = 0 And intAccesos(70) = 0 Then
            MenuItem8.Visible = False
        End If
        ' Facturaci�n (Contado, Credito, Correcciones, Exportar e Importar)
        If intAccesos(73) = 0 And intAccesos(74) = 0 And intAccesos(75) = 0 And intAccesos(76) = 0 And intAccesos(77) = 0 And _
                intAccesos(79) = 0 And intAccesos(80) = 0 And intAccesos(81) = 0 And intAccesos(82) = 0 And intAccesos(83) = 0 And _
                intAccesos(85) = 0 And intAccesos(87) = 0 And intAccesos(89) = 0 And intAccesos(90) = 0 And intAccesos(91) = 0 Then
            MenuItem55.Visible = False
            MenuItem56.Visible = False
            MenuItem13.Visible = False
            MenuItem5.Visible = False
            MenuItem27.Visible = False
        Else
            ' Facturas Contado
            If intAccesos(73) = 0 And intAccesos(74) = 0 And intAccesos(75) = 0 And intAccesos(76) = 0 And intAccesos(77) = 0 Then
                MenuItem55.Visible = False
            Else
                If intAccesos(73) = 0 Then
                    MenuItem60.Visible = False
                End If
                If intAccesos(74) = 0 Then
                    MenuItem61.Visible = False
                End If
                If intAccesos(75) = 0 Then
                    MenuItem62.Visible = False
                End If
                If intAccesos(76) = 0 Then
                    MenuItem63.Visible = False
                End If
                If intAccesos(77) = 0 Then
                    MenuItem6.Visible = False
                End If
            End If
            ' Facturas Credito
            If intAccesos(79) = 0 And intAccesos(80) = 0 And intAccesos(81) = 0 And intAccesos(82) = 0 And intAccesos(83) = 0 Then
                MenuItem56.Visible = False
            Else
                If intAccesos(79) = 0 Then
                    MenuItem64.Visible = False
                End If
                If intAccesos(80) = 0 Then
                    MenuItem65.Visible = False
                End If
                If intAccesos(81) = 0 Then
                    MenuItem66.Visible = False
                End If
                If intAccesos(82) = 0 Then
                    MenuItem67.Visible = False
                End If
                If intAccesos(83) = 0 Then
                    MenuItem7.Visible = False
                End If
            End If
            ' Correcciones
            If intAccesos(85) = 0 Then
                MenuItem13.Visible = False
            End If
            ' Exportar
            If intAccesos(87) = 0 Then
                MenuItem5.Visible = False
            End If
            ' Importar
            If intAccesos(89) = 0 Or intAccesos(90) = 0 Or intAccesos(91) = 0 Then
                MenuItem27.Visible = False
            End If
        End If
        ' Tipo de Cambio
        If intAccesos(93) = 0 And intAccesos(94) = 0 And intAccesos(95) = 0 Then
            MenuItem81.Visible = False
        End If
        ' Reportes de Inventario
        If intAccesos(98) = 0 Then
            MenuItem54.Visible = False
        End If
        ' Reportes de Facturaci�n
        If intAccesos(101) = 0 Then
            MenuItem59.Visible = False
        End If
        ' Reportes de Cuentas por Cobrar
        If intAccesos(104) = 0 And intAccesos(106) = 0 Then
            MenuItem1.Visible = False
        End If
        ' Reportes de Presupuestos
        If intAccesos(108) = 0 Then
            MenuItem17.Visible = False
        End If
        ' Reportes Generales
        If intAccesos(111) = 0 Then
            ToolBar1.Buttons.Item(8).Visible = False
        End If
        ' Cuentas por Cobrar (Recibos Caja, NTC, NTD y Varios)
        If intAccesos(115) = 0 And intAccesos(116) = 0 And intAccesos(117) = 0 And intAccesos(119) = 0 And intAccesos(120) = 0 And _
        intAccesos(121) = 0 And intAccesos(123) = 0 And intAccesos(124) = 0 And intAccesos(125) = 0 And intAccesos(127) = 0 And _
        intAccesos(128) = 0 And intAccesos(129) = 0 Then
            MenuItem4.Visible = False
        Else
            ' Recibos de Caja
            If intAccesos(115) = 0 And intAccesos(116) = 0 And intAccesos(117) = 0 Then
                MenuItem20.Visible = False
            Else
                ' Elaborar Recibos Caja
                If intAccesos(115) = 0 Then
                    MenuItem23.Visible = False
                End If
                ' Consultar Recibos Caja
                If intAccesos(116) = 0 Then
                    MenuItem35.Visible = False
                End If
                ' Anular Recibos Caja
                If intAccesos(117) = 0 Then
                    MenuItem25.Visible = False
                End If
            End If
            ' Notas de Credito
            If intAccesos(119) = 0 And intAccesos(120) = 0 And intAccesos(121) = 0 Then
                MenuItem21.Visible = False
            Else
                ' Elaborar Notas de Credito
                If intAccesos(119) = 0 Then
                    MenuItem26.Visible = False
                End If
                ' Consultar Notas de Credito
                If intAccesos(120) = 0 Then
                    MenuItem36.Visible = False
                End If
                ' Anular Notas de Credito
                If intAccesos(121) = 0 Then
                    MenuItem28.Visible = False
                End If
            End If
            ' Notas de Debito
            If intAccesos(123) = 0 And intAccesos(124) = 0 And intAccesos(125) = 0 Then
                MenuItem22.Visible = False
            Else
                ' Elaborar Notas de Debito
                If intAccesos(123) = 0 Then
                    MenuItem29.Visible = False
                End If
                ' Consultar Notas de Debito
                If intAccesos(124) = 0 Then
                    MenuItem37.Visible = False
                End If
                ' Anular Notas de Debito
                If intAccesos(125) = 0 Then
                    MenuItem31.Visible = False
                End If
            End If
            ' Recibos Varios
            If intAccesos(127) = 0 And intAccesos(128) = 0 And intAccesos(129) = 0 Then
                MenuItem30.Visible = False
            Else
                ' Elaborar Recibos Varios
                If intAccesos(127) = 0 Then
                    MenuItem32.Visible = False
                End If
                ' Consultar Recibos Varios
                If intAccesos(128) = 0 Then
                    MenuItem38.Visible = False
                End If
                ' Anular Recibos Varios
                If intAccesos(129) = 0 Then
                    MenuItem33.Visible = False
                End If
            End If
        End If
        ' Bancos
        If intAccesos(131) = 0 And intAccesos(132) = 0 And intAccesos(133) = 0 Then
            MenuItem97.Visible = False
        End If
        ' Cat�logo Contable
        If intAccesos(135) = 0 And intAccesos(136) = 0 And intAccesos(137) = 0 Then
            MenuItem101.Visible = False
        End If
        ' Exportar Parametros
        If intAccesos(139) = 0 And intAccesos(140) = 0 And intAccesos(141) = 0 And intAccesos(142) = 0 And _
        intAccesos(143) = 0 And intAccesos(144) = 0 And intAccesos(145) = 0 And intAccesos(146) = 0 And _
        intAccesos(147) = 0 And intAccesos(148) = 0 Then
            MenuItem40.Visible = False
        Else
            If intAccesos(139) = 0 Then
                MenuItem41.Visible = False
            End If
            If intAccesos(140) = 0 Then
                MenuItem42.Visible = False
            End If
            If intAccesos(141) = 0 Then
                MenuItem46.Visible = False
            End If
            If intAccesos(142) = 0 Then
                MenuItem47.Visible = False
            End If
            If intAccesos(143) = 0 Then
                MenuItem48.Visible = False
            End If
            If intAccesos(144) = 0 Then
                MenuItem49.Visible = False
            End If
            If intAccesos(145) = 0 Then
                MenuItem50.Visible = False
            End If
            If intAccesos(146) = 0 Then
                MenuItem51.Visible = False
            End If
            If intAccesos(147) = 0 Then
                MenuItem52.Visible = False
            End If
            If intAccesos(148) = 0 Then
                MenuItem92.Visible = False
            End If
        End If
        ' Importar Parametros
        If intAccesos(150) = 0 And intAccesos(151) = 0 And intAccesos(152) = 0 And intAccesos(153) = 0 And _
        intAccesos(154) = 0 And intAccesos(155) = 0 And intAccesos(156) = 0 And intAccesos(157) = 0 And _
        intAccesos(158) = 0 And intAccesos(159) = 0 Then
            MenuItem43.Visible = False
        Else
            If intAccesos(150) = 0 Then
                MenuItem44.Visible = False
            End If
            If intAccesos(151) = 0 Then
                MenuItem45.Visible = False
            End If
            If intAccesos(152) = 0 Then
                MenuItem58.Visible = False
            End If
            If intAccesos(153) = 0 Then
                MenuItem85.Visible = False
            End If
            If intAccesos(154) = 0 Then
                MenuItem86.Visible = False
            End If
            If intAccesos(155) = 0 Then
                MenuItem87.Visible = False
            End If
            If intAccesos(156) = 0 Then
                MenuItem88.Visible = False
            End If
            If intAccesos(157) = 0 Then
                MenuItem89.Visible = False
            End If
            If intAccesos(158) = 0 Then
                MenuItem90.Visible = False
            End If
            If intAccesos(159) = 0 Then
                MenuItem93.Visible = False
            End If
        End If
        ' Areas
        If intAccesos(161) = 0 And intAccesos(162) = 0 And intAccesos(163) = 0 Then
            MenuItem57.Visible = False
        End If
        ' Puestos
        If intAccesos(165) = 0 And intAccesos(166) = 0 And intAccesos(167) = 0 Then
            MenuItem91.Visible = False
        End If
        ' Cambiar Numero de Factura
        If intAccesos(168) = 0 And intAccesos(169) = 0 Then
            MenuItem83.Visible = False
        End If
        ' Cambiar Porcentajes de Venta
        If intAccesos(170) = 0 And intAccesos(171) = 0 Then
            MenuItem84.Visible = False
        End If
        ' Empleados
        If intAccesos(173) = 0 And intAccesos(174) = 0 And intAccesos(175) = 0 Then
            MenuItem96.Visible = False
        End If
        ' Cuentas Bancarias
        If intAccesos(177) = 0 And intAccesos(178) = 0 And intAccesos(179) = 0 Then
            MenuItem98.Visible = False
        End If
        ' Chequeras
        If intAccesos(181) = 0 And intAccesos(182) = 0 And intAccesos(183) = 0 Then
            MenuItem99.Visible = False
        End If
        ' Cambiar Retenci�n de Chequeras
        If intAccesos(184) = 0 And intAccesos(185) = 0 Then
            MenuItem100.Visible = False
        End If
        ' Parametros Recibos de Caja
        If intAccesos(186) = 0 And intAccesos(187) = 0 And intAccesos(188) = 0 Then
            MenuItem102.Visible = False
        End If
        ' Parametros Notas de Cr�dito
        If intAccesos(189) = 0 And intAccesos(190) = 0 And intAccesos(191) = 0 Then
            MenuItem103.Visible = False
        End If
        ' Parametros Notas de D�bito
        If intAccesos(192) = 0 And intAccesos(193) = 0 And intAccesos(194) = 0 Then
            MenuItem104.Visible = False
        End If
        ' Parametros Definici�n de Comisiones
        If intAccesos(196) = 0 And intAccesos(197) = 0 Then
            MenuItem94.Visible = False
        End If
        If intAccesos(198) = 0 Then
            MenuItem11.Visible = False
        End If
        ' Parametros Letras de Cambio
        If intAccesos(199) = 0 And intAccesos(200) = 0 Then
            MenuItem95.Visible = False
        End If
        ' Cierre Anual del Inventario
        If intAccesos(201) = 0 And intAccesos(202) = 0 Then
            MenuItem104.Visible = False
        End If
        ' Parametros Empresa
        If intAccesos(203) = 0 And intAccesos(204) = 0 Then
            MenuItem107.Visible = False
        End If
        ' Partidas Contables
        If intAccesos(206) = 0 And intAccesos(207) = 0 And intAccesos(208) = 0 Then
            MenuItem2.Visible = False
        End If
        ' Cheques, Depositos, NTC y NTD
        If intAccesos(210) = 0 Then
            MenuItem111.Visible = False
        End If
        If intAccesos(211) = 0 Then
            MenuItem112.Visible = False
        End If
        If intAccesos(212) = 0 Then
            MenuItem113.Visible = False
        End If
        If intAccesos(213) = 0 Then
            MenuItem127.Visible = False
        End If
        If intAccesos(215) = 0 Then
            MenuItem115.Visible = False
        End If
        If intAccesos(216) = 0 Then
            MenuItem116.Visible = False
        End If
        If intAccesos(217) = 0 Then
            MenuItem117.Visible = False
        End If
        If intAccesos(218) = 0 Then
            MenuItem128.Visible = False
        End If
        If intAccesos(220) = 0 Then
            MenuItem119.Visible = False
        End If
        If intAccesos(221) = 0 Then
            MenuItem120.Visible = False
        End If
        If intAccesos(222) = 0 Then
            MenuItem121.Visible = False
        End If
        If intAccesos(223) = 0 Then
            MenuItem129.Visible = False
        End If
        If intAccesos(225) = 0 Then
            MenuItem123.Visible = False
        End If
        If intAccesos(226) = 0 Then
            MenuItem124.Visible = False
        End If
        If intAccesos(227) = 0 Then
            MenuItem125.Visible = False
        End If
        If intAccesos(228) = 0 Then
            MenuItem130.Visible = False
        End If
        If MenuItem111.Visible = False And MenuItem112.Visible = False And MenuItem113.Visible = False And MenuItem127.Visible = False Then
            MenuItem110.Visible = False
        End If
        If MenuItem115.Visible = False And MenuItem116.Visible = False And MenuItem117.Visible = False And MenuItem128.Visible = False Then
            MenuItem114.Visible = False
        End If
        If MenuItem119.Visible = False And MenuItem120.Visible = False And MenuItem121.Visible = False And MenuItem129.Visible = False Then
            MenuItem118.Visible = False
        End If
        If MenuItem123.Visible = False And MenuItem124.Visible = False And MenuItem125.Visible = False And MenuItem130.Visible = False Then
            MenuItem122.Visible = False
        End If
        If intAccesos(231) = 0 And intAccesos(232) = 0 Then
            MenuItem126.Visible = False
        End If
        If MenuItem110.Visible = False And MenuItem114.Visible = False And MenuItem118.Visible = False And MenuItem122.Visible = False And MenuItem126.Visible = False Then
            ToolBar1.Buttons.Item(3).Visible = False
        End If
        ' Beneficiarios
        If intAccesos(234) = 0 And intAccesos(235) = 0 And intAccesos(236) = 0 Then
            MenuItem132.Visible = False
        End If

    End Sub

    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

    '    StatusBar1.Panels.Item(2).Text = Format(Now, "hh:mm:ss tt")

    'End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click

        Dim frmNew As New frmProductoMovim
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem39_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem39.Click

        Dim frmNew As New frmProductoConv
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem53_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem53.Click

        'Dim frmNew As New frmLiqProd
        'frmNew.MdiParent = Me
        'frmNew.Show()

    End Sub

    Private Sub MenuItem54_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem54.Click

        Dim frmNew As New frmRptMovInvent
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem60_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem60.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 1
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Generaci�n de Facturas al Contado (" & strNombreSuc & ")"

    End Sub

    Private Sub MenuItem61_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem61.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 2
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Impresi�n de Facturas Pendientes al Contado (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Imprimir"
        'frmNew.ToolBarButton1.Text = "<F7>"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Imprimir"

    End Sub

    Private Sub MenuItem63_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem63.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 3
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anulaci�n de Facturas al Contado (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"

    End Sub

    Private Sub MenuItem64_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem64.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 4
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Generaci�n de Facturas al Cr�dito (" & strNombreSuc & ")"

    End Sub

    Private Sub MenuItem65_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem65.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 5
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Impresi�n de Facturas Pendientes al Cr�dito (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Imprimir"
        'frmNew.ToolBarButton1.Text = "<F7>"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Imprimir"

    End Sub

    Private Sub MenuItem67_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem67.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 6
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anulaci�n de Facturas al Cr�dito (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"

    End Sub

    Private Sub MenuItem59_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem59.Click

        Dim frmNew As New frmRptDetVtasArt
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem68_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem68.Click

        Dim frmNew As New frmUsuarios
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem69_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem69.Click

        Dim frmNew As New frmDepartamentos
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem70_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem70.Click

        Dim frmNew As New frmAgencias
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem71_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem71.Click

        Dim frmNew As New frmProveedores
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem72_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem72.Click

        Dim frmNew As New frmNegocios
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem73_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem73.Click

        Dim frmNew As New frmVendedores
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem74_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem74.Click

        Dim frmNew As New frmConversiones
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem75_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem75.Click

        Dim frmNew As New frmProductos
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem77_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem77.Click

        Dim frmNew As New frmClases
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem78_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem78.Click

        Dim frmNew As New frmSubClases
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem79_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem79.Click

        Dim frmNew As New frmEmpaques
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem80_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem80.Click

        Dim frmNew As New frmUnidades
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem81_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem81.Click

        Dim frmNew As New frmTipoCambio
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem84_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem84.Click

        Dim frmNew As New frmPorcentajes
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem62_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem62.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 7
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Ingreso Manual de Facturas al Contado (" & strNombreSuc & ")"

    End Sub

    Private Sub MenuItem66_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem66.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 8
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Ingreso Manual de Facturas al Cr�dito (" & strNombreSuc & ")"

    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 9
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consultar Facturas al Contado (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Imprimir"
        'frmNew.ToolBarButton1.Text = "<F7>"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Imprimir"

    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click

        Dim frmNew As New frmFacturas
        intTipoFactura = 10
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consultar Facturas al Cr�dito (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Imprimir"
        'frmNew.ToolBarButton1.Text = "<F7>"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Imprimir"

    End Sub

    Private Sub MenuItem83_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem83.Click

        Dim frmNew As New frmCambNumFact
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click

        Dim frmNew As New frmRptCtsCbrSlds
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem13.Click

        'Dim frmNew As New frmFactCorr
        'frmNew.MdiParent = Me
        'frmNew.Show()

    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem10.Click, MenuItem14.Click

        Dim frmNew As New frmRptCambiarPrecios
        frmNew.MdiParent = Me
        If sender.text = "Cambio Precios" Then
            intRptOtros = 1
            frmNew.Text = "Reporte de Cambios de Precios en las Facturas"
        ElseIf sender.text = "Correcciones Facturas" Then
            intRptOtros = 2
            frmNew.Text = "Reporte de Cambios de Productos en las Facturas"
        End If
        frmNew.Show()

    End Sub

    Private Sub MenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem15.Click

        'Dim frmNew As New frmPresupuestos
        'frmNew.MdiParent = Me
        'frmNew.Show()

    End Sub

    Private Sub MenuItem17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem17.Click

        Dim frmNew As New frmRptPresupuestos
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()

    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click

        Dim frmNew As New frmRptTipoPrecios
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()

    End Sub

    Private Sub MenuItem19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem19.Click

        Dim frmNew As New frmMunicipios
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem23.Click

        intTipoReciboCaja = 1
        Dim frmNew As New frmRecibos
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Recibos de Caja"
        'frmNew.ToolBarButton4.ToolTipText = "Vista Previa"
        'frmNew.ToolBarButton4.Text = "<F7>"

    End Sub

    Private Sub MenuItem26_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem26.Click

        intTipoReciboCaja = 2
        Dim frmNew As New frmRecibosNTC
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Notas de Cr�dito"
        'frmNew.ToolBarButton4.ToolTipText = "Vista Previa"
        'frmNew.ToolBarButton4.Text = "<F7>"

    End Sub

    Private Sub MenuItem29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem29.Click

        intTipoReciboCaja = 3
        Dim frmNew As New frmRecibosNTD
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Notas de D�bito"
        'frmNew.ToolBarButton4.ToolTipText = "Vista Previa"
        'frmNew.ToolBarButton4.Text = "<F7>"

    End Sub

    Private Sub MenuItem32_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem32.Click

        intTipoReciboCaja = 7
        Dim frmNew As New frmRecibosVarios
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Recibos Varios"

    End Sub

    Private Sub MenuItem25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem25.Click

        intTipoReciboCaja = 4
        Dim frmNew As New frmRecibos
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anular Recibos de Caja"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        'frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"

    End Sub

    Private Sub MenuItem28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem28.Click

        intTipoReciboCaja = 5
        Dim frmNew As New frmRecibosNTC
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anular Notas de Cr�dito"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"

    End Sub

    Private Sub MenuItem31_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem31.Click

        intTipoReciboCaja = 6
        Dim frmNew As New frmRecibosNTD
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anular Notas de D�bito"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"

    End Sub

    Private Sub MenuItem33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem33.Click

        intTipoReciboCaja = 8
        Dim frmNew As New frmRecibosVarios
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anular Recibos Varios"
        frmNew.ToolBarButton1.ToolTipText = "Anular"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"

    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click

        Dim frmNew As New frmRazonesAnular
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click

        Dim frmNew As New frmExportarProformas
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem27.Click

        Dim frmNew As New frmFactImportar
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem24.Click

        Dim frmNew As New frmClientes
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem35.Click

        intTipoReciboCaja = 9
        Dim frmNew As New frmRecibos
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consulta de Recibos de Caja"

    End Sub

    Private Sub MenuItem36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem36.Click

        intTipoReciboCaja = 10
        Dim frmNew As New frmRecibosNTC
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consulta de Notas de Cr�dito"

    End Sub

    Private Sub MenuItem37_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem37.Click

        intTipoReciboCaja = 11
        Dim frmNew As New frmRecibosNTD
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consulta de Notas de D�bito"

    End Sub

    Private Sub MenuItem38_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem38.Click

        intTipoReciboCaja = 12
        Dim frmNew As New frmRecibosVarios
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consulta de Recibos Varios"

    End Sub

    Private Sub MenuItem41_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem41.Click

        Dim frmNew As New frmExportarInventario
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem42_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem42.Click

        Dim frmNew As New frmExportarTipoCambio
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem44_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem44.Click

        Dim frmNew As New frmImportarInventario
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem45_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem45.Click

        Dim frmNew As New frmImportarTipoCambio
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem46_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem46.Click, MenuItem47.Click, MenuItem48.Click, MenuItem49.Click, MenuItem50.Click, MenuItem51.Click, MenuItem52.Click, MenuItem92.Click

        Select Case sender.text.ToString
            Case "Clientes" : intExportarModulo = 2
            Case "Clases" : intExportarModulo = 3
            Case "SubClases" : intExportarModulo = 4
            Case "Unidades" : intExportarModulo = 5
            Case "Empaques" : intExportarModulo = 6
            Case "Proveedores" : intExportarModulo = 7
            Case "Productos" : intExportarModulo = 8
            Case "Vendedores" : intExportarModulo = 9
        End Select

        Dim frmNew As New frmExportarParametros
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem58_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem58.Click, MenuItem85.Click, MenuItem86.Click, MenuItem87.Click, MenuItem88.Click, MenuItem89.Click, MenuItem90.Click, MenuItem93.Click

        Select Case sender.text.ToString
            Case "Clientes" : intExportarModulo = 2
            Case "Clases" : intExportarModulo = 3
            Case "SubClases" : intExportarModulo = 4
            Case "Unidades" : intExportarModulo = 5
            Case "Empaques" : intExportarModulo = 6
            Case "Proveedores" : intExportarModulo = 7
            Case "Productos" : intExportarModulo = 8
            Case "Vendedores" : intExportarModulo = 9
        End Select

        Dim frmNew As New frmImportarParametros
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem57_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem57.Click

        Dim frmNew As New frmAreas
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem91_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem91.Click

        Dim frmNew As New frmPuestos
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem94_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem94.Click

        Dim frmNew As New frmComisiones
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem96_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem96.Click

        Dim frmNew As New frmEmpleados
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem97_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem97.Click

        Dim frmNew As New frmBancos
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem98_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem98.Click

        Dim frmNew As New frmCtasBancarias
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem99_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem99.Click

        Dim frmNew As New frmChequeras
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem100_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem100.Click

        Dim frmNew As New frmPorcentajes2
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem101_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem101.Click

        Dim frmNew As New frmCtasContables
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem102_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem102.Click

        Dim frmNew As New frmRecibosParam
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem95_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem95.Click

        Dim frmNew As New frmLetrasCambios
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click

        'Dim frmNew As New frmRptComisiones
        'frmNew.MdiParent = Me
        'frmNew.Show()

    End Sub

    Private Sub MenuItem106_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem106.Click

        Dim frmNew As New frmPrcCierreAnual
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem107_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem107.Click

        Dim frmNew As New frmEmpresa
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem111_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem111.Click

        intTipoCheque = 1
        Dim frmNew As New frmMovCheques 'frmInsertarCheque 
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Generaci�n Cheques"
        frmNew.Show()

    End Sub

    Private Sub MenuItem115_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem115.Click

        intTipoCheque = 2
        Dim frmNew As New frmMovDepositos
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Generaci�n Dep�sitos"
        frmNew.Show()

    End Sub

    Private Sub MenuItem123_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem123.Click

        intTipoCheque = 3
        Dim frmNew As New frmMovNTD
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Generaci�n Notas de D�bito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem119_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem119.Click

        intTipoCheque = 4
        Dim frmNew As New frmMovNTC
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Generaci�n Notas de Cr�dito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem112_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem112.Click

        intTipoCheque = 5
        Dim frmNew As New frmMovCheques
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Consultar Cheques"
        frmNew.Show()

    End Sub

    Private Sub MenuItem116_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem116.Click

        intTipoCheque = 6
        Dim frmNew As New frmMovDepositos
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Consultar Dep�sitos"
        frmNew.Show()

    End Sub

    Private Sub MenuItem124_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem124.Click

        intTipoCheque = 7
        Dim frmNew As New frmMovNTD
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Consultar Notas de D�bito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem120_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem120.Click

        intTipoCheque = 8
        Dim frmNew As New frmMovNTC
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Consultar Notas de Cr�dito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem113_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem113.Click

        intTipoCheque = 9
        Dim frmNew As New frmMovCheques
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Reversar Cheques"
        frmNew.Show()

    End Sub

    Private Sub MenuItem117_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem117.Click

        intTipoCheque = 10
        Dim frmNew As New frmMovDepositos
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Reversar Dep�sitos"
        frmNew.Show()

    End Sub

    Private Sub MenuItem125_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem125.Click

        intTipoCheque = 11
        Dim frmNew As New frmMovNTD
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Reversar Notas de D�bito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem121_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem121.Click

        intTipoCheque = 12
        Dim frmNew As New frmMovNTC
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Reversar Notas de Cr�dito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem127_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem127.Click

        intTipoCheque = 13
        Dim frmNew As New frmMovCheques
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Anular Cheques"
        frmNew.Show()

    End Sub

    Private Sub MenuItem128_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem128.Click

        intTipoCheque = 14
        Dim frmNew As New frmMovDepositos
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Anular Dep�sitos"
        frmNew.Show()

    End Sub

    Private Sub MenuItem130_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem130.Click

        intTipoCheque = 15
        Dim frmNew As New frmMovNTD
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Anular Notas de D�bito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem129_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem129.Click

        intTipoCheque = 16
        Dim frmNew As New frmMovNTC
        frmNew.MdiParent = Me
        frmNew.Text = "M�dulo Anular Notas de Cr�dito"
        frmNew.Show()

    End Sub

    Private Sub MenuItem126_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem126.Click

        Dim frmNew As New frmRptMovCheques
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub

    Private Sub MenuItem132_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem132.Click

        Dim frmNew As New frmBeneficiarios
        frmNew.MdiParent = Me
        frmNew.Show()

    End Sub


    Private Sub MenuItem134_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem134.Click

        Dim frmNew As New frmCompras
        intTipoCompra = 1
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Generaci�n de compras al Contado (" & strNombreSuc & ")"
    End Sub

    Private Sub MenuItem137_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem137.Click
        Dim frmNew As New frmCompras
        intTipoCompra = 3
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anulaci�n de Compras al Contado (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        'frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"
    End Sub

    Private Sub MenuItem138_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem138.Click
        Dim frmNew As New frmCompras
        intTipoCompra = 9
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consultar compras al Contado (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Imprimir"
        'frmNew.ToolBarButton1.Text = "<F7>"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Imprimir"
    End Sub

    Private Sub MenuItem140_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem140.Click
        Dim frmNew As New frmCompras
        intTipoCompra = 4
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Generaci�n de Compras al Cr�dito (" & strNombreSuc & ")"
    End Sub

    Private Sub MenuItem143_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem143.Click
        Dim frmNew As New frmCompras
        intTipoCompra = 6
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Anulaci�n de Compras al Cr�dito (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Anular"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Anular"
    End Sub

    Private Sub MenuItem144_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem144.Click
        Dim frmNew As New frmCompras
        intTipoCompra = 10
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Consultar Compras al Cr�dito (" & strNombreSuc & ")"
        'frmNew.ToolBarButton1.ToolTipText = "Imprimir"
        'frmNew.ToolBarButton1.Text = "<F7>"
        frmNew.MainMenu1.MenuItems.Item(0).Text = "Imprimir"
    End Sub

    Private Sub MenuItem147_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem147.Click
        Dim frmNew As New frmImportarCompra
        frmNew.MdiParent = Me
        frmNew.Show()
    End Sub

    Private Sub MenuItem146_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem146.Click
        Dim frmNew As New frmExportarCompras
        frmNew.MdiParent = Me
        frmNew.Show()
    End Sub


    Private Sub MenuItem135_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem135.Click
        Dim frmNew As New frmExportarRecibos
        frmNew.MdiParent = Me
        frmNew.Show()
    End Sub

    Private Sub MenuItem136_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem136.Click
        Dim frmNew As New frmImportarRecibos
        frmNew.MdiParent = Me
        frmNew.Show()
    End Sub

    Private Sub MenuItem141_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem141.Click
        Dim frmNew As New frmCambioNumeroRecibo
        frmNew.MdiParent = Me
        frmNew.Show()
    End Sub

    Private Sub MenuItem142_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem142.Click
        intTipoReciboCaja = 15
        Dim frmNew As New frmRecibos
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Recibos de Caja Ingreso Manual"
        'frmNew.ToolBarButton4.ToolTipText = "Vista Previa"
        'frmNew.ToolBarButton4.Text = "<F7>"

    End Sub

    Private Sub MenuItem145_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem145.Click

    End Sub

    Private Sub MenuItem149_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem149.Click
        Dim frmNew As New frmCambioNumeroMovimiento
        frmNew.MdiParent = Me
        frmNew.Show()
    End Sub

    Private Sub MenuItem150_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem150.Click
        intTipoFactura = 1
        Dim frmNew As New frmPedidoInventario
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Traslado de productos entre sucursales"
    End Sub

    Private Sub MenuItem153_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem153.Click
        intTipoFactura = 7
        Dim frmNew As New frmPedidoInventario
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Traslado de productos entre sucursales"
    End Sub

    Private Sub MenuItem151_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem151.Click
        intTipoFactura = 3
        Dim frmNew As New frmPedidoInventario
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Traslado de productos entre sucursales"
    End Sub

    Private Sub MenuItem152_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem152.Click
        intTipoFactura = 9
        Dim frmNew As New frmPedidoInventario
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Traslado de productos entre sucursales"
    End Sub

    Private Sub MenuItem154_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem154.Click
        Dim frmNew As New frmExportarPedidos
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Exportar traslados de productos"

    End Sub

    Private Sub MenuItem155_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem155.Click
        Dim frmNew As New frmImportarMovimientoTraslado
        frmNew.MdiParent = Me
        frmNew.Show()
        frmNew.Text = "Importar requisas de productos"
    End Sub

End Class
