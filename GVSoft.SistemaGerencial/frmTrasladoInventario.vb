Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Printing

Public Class frmTrasladoInventario
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTipoPrecio As System.Windows.Forms.Label
    Friend WithEvents DPkFechaFactura As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNumeroFactura As System.Windows.Forms.TextBox
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents ddlTipoPrecio As System.Windows.Forms.ComboBox
    Friend WithEvents txtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartamento As System.Windows.Forms.TextBox
    Friend WithEvents txtNegocio As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents txtAgenciaOrigen As System.Windows.Forms.TextBox
    Friend WithEvents cbeAgenciaDestino As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents CmdAnular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImprimir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cbeAgenciasOrigen As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents cmdImportPedidos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cbNumerosPedidos As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PrintDialogo As System.Windows.Forms.PrintDialog
    Friend WithEvents PrintDoc As System.Drawing.Printing.PrintDocument
    Friend WithEvents cmdImprimirPedido As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Cantidad As DataGridViewDoubleInputColumn
    Friend WithEvents Unidad As DataGridViewTextBoxColumn
    Friend WithEvents Codigo As DataGridViewTextBoxColumn
    Friend WithEvents Producto As DataGridViewTextBoxColumn
    Friend WithEvents Impuesto As DataGridViewTextBoxColumn
    Friend WithEvents Valor As DataGridViewDoubleInputColumn
    Friend WithEvents Total As DataGridViewDoubleInputColumn
    Friend WithEvents IdProducto As DataGridViewTextBoxColumn
    Friend WithEvents PrecioCosto As DataGridViewTextBoxColumn
    Friend WithEvents TipoPrecio As DataGridViewTextBoxColumn
    Friend WithEvents Label19 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTrasladoInventario))
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim UltraStatusPanel5 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel6 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbNumerosPedidos = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbeAgenciasOrigen = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.txtAgenciaOrigen = New System.Windows.Forms.TextBox()
        Me.cbeAgenciaDestino = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ddlSerie = New System.Windows.Forms.ComboBox()
        Me.lblSerie = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.txtNegocio = New System.Windows.Forms.TextBox()
        Me.txtDepartamento = New System.Windows.Forms.TextBox()
        Me.txtNumeroCliente = New System.Windows.Forms.TextBox()
        Me.txtNumeroVendedor = New System.Windows.Forms.TextBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.ddlTipoPrecio = New System.Windows.Forms.ComboBox()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.txtNumeroFactura = New System.Windows.Forms.TextBox()
        Me.DPkFechaFactura = New System.Windows.Forms.DateTimePicker()
        Me.lblTipoPrecio = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.CmdAnular = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImprimir = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImportPedidos = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdImprimirPedido = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.PrintDialogo = New System.Windows.Forms.PrintDialog()
        Me.PrintDoc = New System.Drawing.Printing.PrintDocument()
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Unidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Producto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Total = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IdProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12, Me.MenuItem4})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "&Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "L&impiar"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem12.Text = "&Listado"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Vendedores"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Clientes"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Productos"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Refrescar"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 4
        Me.MenuItem4.Text = "&Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbNumerosPedidos)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cbeAgenciasOrigen)
        Me.GroupBox1.Controls.Add(Me.txtAgenciaOrigen)
        Me.GroupBox1.Controls.Add(Me.cbeAgenciaDestino)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.ddlSerie)
        Me.GroupBox1.Controls.Add(Me.lblSerie)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.txtNegocio)
        Me.GroupBox1.Controls.Add(Me.txtDepartamento)
        Me.GroupBox1.Controls.Add(Me.txtNumeroCliente)
        Me.GroupBox1.Controls.Add(Me.txtNumeroVendedor)
        Me.GroupBox1.Controls.Add(Me.txtPrecio)
        Me.GroupBox1.Controls.Add(Me.ddlTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.txtNumeroFactura)
        Me.GroupBox1.Controls.Add(Me.DPkFechaFactura)
        Me.GroupBox1.Controls.Add(Me.lblTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 69)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(863, 135)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        '
        'cbNumerosPedidos
        '
        Me.cbNumerosPedidos.DisplayMember = "Text"
        Me.cbNumerosPedidos.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbNumerosPedidos.FormattingEnabled = True
        Me.cbNumerosPedidos.ItemHeight = 14
        Me.cbNumerosPedidos.Location = New System.Drawing.Point(618, 16)
        Me.cbNumerosPedidos.Name = "cbNumerosPedidos"
        Me.cbNumerosPedidos.Size = New System.Drawing.Size(236, 20)
        Me.cbNumerosPedidos.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.cbNumerosPedidos.TabIndex = 41
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(484, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(131, 13)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Pedidos de Inventario"
        '
        'cbeAgenciasOrigen
        '
        Me.cbeAgenciasOrigen.DisplayMember = "Text"
        Me.cbeAgenciasOrigen.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbeAgenciasOrigen.FormattingEnabled = True
        Me.cbeAgenciasOrigen.ItemHeight = 14
        Me.cbeAgenciasOrigen.Location = New System.Drawing.Point(102, 45)
        Me.cbeAgenciasOrigen.Name = "cbeAgenciasOrigen"
        Me.cbeAgenciasOrigen.Size = New System.Drawing.Size(375, 20)
        Me.cbeAgenciasOrigen.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.cbeAgenciasOrigen.TabIndex = 39
        Me.cbeAgenciasOrigen.Visible = False
        '
        'txtAgenciaOrigen
        '
        Me.txtAgenciaOrigen.Location = New System.Drawing.Point(102, 46)
        Me.txtAgenciaOrigen.Name = "txtAgenciaOrigen"
        Me.txtAgenciaOrigen.ReadOnly = True
        Me.txtAgenciaOrigen.Size = New System.Drawing.Size(375, 20)
        Me.txtAgenciaOrigen.TabIndex = 38
        '
        'cbeAgenciaDestino
        '
        Me.cbeAgenciaDestino.DisplayMember = "Text"
        Me.cbeAgenciaDestino.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbeAgenciaDestino.FormattingEnabled = True
        Me.cbeAgenciaDestino.ItemHeight = 14
        Me.cbeAgenciaDestino.Location = New System.Drawing.Point(618, 45)
        Me.cbeAgenciaDestino.Name = "cbeAgenciaDestino"
        Me.cbeAgenciaDestino.Size = New System.Drawing.Size(236, 20)
        Me.cbeAgenciaDestino.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbeAgenciaDestino.TabIndex = 37
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(484, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 13)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "A la Agencia"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 49)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 35
        Me.Label10.Text = "De la Agencia"
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Location = New System.Drawing.Point(275, 16)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(48, 21)
        Me.ddlSerie.TabIndex = 1
        Me.ddlSerie.Tag = "Serie por agencia"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(234, 16)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(36, 13)
        Me.lblSerie.TabIndex = 27
        Me.lblSerie.Text = "Serie"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 109)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Precio"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(691, 16)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(24, 20)
        Me.TextBox17.TabIndex = 25
        Me.TextBox17.TabStop = False
        Me.TextBox17.Text = "0"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox17.Visible = False
        '
        'txtNegocio
        '
        Me.txtNegocio.Location = New System.Drawing.Point(723, 88)
        Me.txtNegocio.Name = "txtNegocio"
        Me.txtNegocio.Size = New System.Drawing.Size(40, 20)
        Me.txtNegocio.TabIndex = 24
        Me.txtNegocio.TabStop = False
        Me.txtNegocio.Tag = ""
        Me.txtNegocio.Text = "Negocio"
        Me.txtNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNegocio.Visible = False
        '
        'txtDepartamento
        '
        Me.txtDepartamento.Location = New System.Drawing.Point(723, 64)
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.Size = New System.Drawing.Size(40, 20)
        Me.txtDepartamento.TabIndex = 23
        Me.txtDepartamento.TabStop = False
        Me.txtDepartamento.Tag = ""
        Me.txtDepartamento.Text = "Departamento"
        Me.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDepartamento.Visible = False
        '
        'txtNumeroCliente
        '
        Me.txtNumeroCliente.Location = New System.Drawing.Point(723, 40)
        Me.txtNumeroCliente.Name = "txtNumeroCliente"
        Me.txtNumeroCliente.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroCliente.TabIndex = 22
        Me.txtNumeroCliente.TabStop = False
        Me.txtNumeroCliente.Tag = ""
        Me.txtNumeroCliente.Text = "Cliente"
        Me.txtNumeroCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroCliente.Visible = False
        '
        'txtNumeroVendedor
        '
        Me.txtNumeroVendedor.Location = New System.Drawing.Point(723, 16)
        Me.txtNumeroVendedor.Name = "txtNumeroVendedor"
        Me.txtNumeroVendedor.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroVendedor.TabIndex = 21
        Me.txtNumeroVendedor.TabStop = False
        Me.txtNumeroVendedor.Tag = ""
        Me.txtNumeroVendedor.Text = "Vendedor"
        Me.txtNumeroVendedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroVendedor.Visible = False
        '
        'txtPrecio
        '
        Me.txtPrecio.Location = New System.Drawing.Point(102, 109)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.ReadOnly = True
        Me.txtPrecio.Size = New System.Drawing.Size(170, 20)
        Me.txtPrecio.TabIndex = 12
        Me.txtPrecio.Tag = "Valor del Precio"
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ddlTipoPrecio
        '
        Me.ddlTipoPrecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlTipoPrecio.Location = New System.Drawing.Point(618, 81)
        Me.ddlTipoPrecio.Name = "ddlTipoPrecio"
        Me.ddlTipoPrecio.Size = New System.Drawing.Size(236, 21)
        Me.ddlTipoPrecio.TabIndex = 11
        Me.ddlTipoPrecio.Tag = "Tipo de Precio"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(102, 80)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(170, 20)
        Me.txtCantidad.TabIndex = 8
        Me.txtCantidad.Tag = "Cantidad a Requisar"
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProducto
        '
        Me.txtProducto.Location = New System.Drawing.Point(339, 80)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(139, 20)
        Me.txtProducto.TabIndex = 9
        Me.txtProducto.Tag = "C�digo del Producto"
        '
        'txtNumeroFactura
        '
        Me.txtNumeroFactura.Location = New System.Drawing.Point(385, 16)
        Me.txtNumeroFactura.Name = "txtNumeroFactura"
        Me.txtNumeroFactura.ReadOnly = True
        Me.txtNumeroFactura.Size = New System.Drawing.Size(93, 20)
        Me.txtNumeroFactura.TabIndex = 2
        Me.txtNumeroFactura.Tag = "N�mero de Requisa"
        '
        'DPkFechaFactura
        '
        Me.DPkFechaFactura.CustomFormat = "dd-MMM-yyyy"
        Me.DPkFechaFactura.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkFechaFactura.Location = New System.Drawing.Point(102, 16)
        Me.DPkFechaFactura.Name = "DPkFechaFactura"
        Me.DPkFechaFactura.Size = New System.Drawing.Size(126, 20)
        Me.DPkFechaFactura.TabIndex = 0
        Me.DPkFechaFactura.Tag = "Fecha de Ingreso"
        '
        'lblTipoPrecio
        '
        Me.lblTipoPrecio.AutoSize = True
        Me.lblTipoPrecio.Location = New System.Drawing.Point(484, 81)
        Me.lblTipoPrecio.Name = "lblTipoPrecio"
        Me.lblTipoPrecio.Size = New System.Drawing.Size(72, 13)
        Me.lblTipoPrecio.TabIndex = 9
        Me.lblTipoPrecio.Text = "Tipo Precio"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 80)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Cantidad"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(275, 80)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Producto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(331, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "N�mero"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvDetalle)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.TextBox12)
        Me.GroupBox2.Controls.Add(Me.TextBox11)
        Me.GroupBox2.Controls.Add(Me.TextBox10)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 210)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(863, 225)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Cantidad, Me.Unidad, Me.Codigo, Me.Producto, Me.Impuesto, Me.Valor, Me.Total, Me.IdProducto, Me.PrecioCosto, Me.TipoPrecio})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(6, 13)
        Me.dgvDetalle.Name = "dgvDetalle"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(846, 149)
        Me.dgvDetalle.TabIndex = 94
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(64, 176)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 65
        Me.Label19.Text = "Label19"
        Me.Label19.Visible = False
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(8, 176)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 64
        Me.Label18.Text = "Label18"
        Me.Label18.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(296, 195)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(93, 20)
        Me.Label16.TabIndex = 62
        Me.Label16.Text = "ANULADA"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(304, 171)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 13)
        Me.Label15.TabIndex = 61
        Me.Label15.Text = "<F5> AYUDA"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(736, 191)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(109, 20)
        Me.TextBox12.TabIndex = 60
        Me.TextBox12.TabStop = False
        Me.TextBox12.Tag = "Cantidad a Requisar"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(570, 191)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(102, 20)
        Me.TextBox11.TabIndex = 59
        Me.TextBox11.TabStop = False
        Me.TextBox11.Tag = "Cantidad a Requisar"
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox11.Visible = False
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(570, 165)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(102, 20)
        Me.TextBox10.TabIndex = 58
        Me.TextBox10.TabStop = False
        Me.TextBox10.Tag = "Cantidad a Requisar"
        Me.TextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox10.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(736, 165)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(109, 20)
        Me.TextBox9.TabIndex = 57
        Me.TextBox9.TabStop = False
        Me.TextBox9.Tag = "Cantidad a Requisar"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(679, 191)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 13)
        Me.Label14.TabIndex = 56
        Me.Label14.Text = "Total "
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(509, 191)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 55
        Me.Label13.Text = "Retenci�n"
        Me.Label13.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(509, 167)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 54
        Me.Label12.Text = "Imp Venta"
        Me.Label12.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(679, 167)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Sub Total"
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(439, 438)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(64, 16)
        Me.Label17.TabIndex = 63
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 447)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel6.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel5, UltraStatusPanel6})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(865, 23)
        Me.UltraStatusBar1.TabIndex = 20
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.LabelItem1, Me.CmdAnular, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem5, Me.cmdImprimir, Me.LabelItem2, Me.cmdImportPedidos, Me.cmdImprimirPedido, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(865, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 35
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        Me.cmdOK.Visible = False
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'CmdAnular
        '
        Me.CmdAnular.Image = CType(resources.GetObject("CmdAnular.Image"), System.Drawing.Image)
        Me.CmdAnular.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.CmdAnular.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.CmdAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.CmdAnular.Name = "CmdAnular"
        Me.CmdAnular.Text = "Anular<F3>"
        Me.CmdAnular.Tooltip = "Anular Requisa"
        Me.CmdAnular.Visible = False
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = "  "
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Image = CType(resources.GetObject("cmdImprimir.Image"), System.Drawing.Image)
        Me.cmdImprimir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimir.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.cmdImprimir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Text = "Imprimir<F7>"
        Me.cmdImprimir.Tooltip = "Imprimir requisa"
        Me.cmdImprimir.Visible = False
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "  "
        '
        'cmdImportPedidos
        '
        Me.cmdImportPedidos.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdImportPedidos.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Upload_256x256
        Me.cmdImportPedidos.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportPedidos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportPedidos.Name = "cmdImportPedidos"
        Me.cmdImportPedidos.Text = "Importar Pedidos"
        '
        'cmdImprimirPedido
        '
        Me.cmdImprimirPedido.Image = CType(resources.GetObject("cmdImprimirPedido.Image"), System.Drawing.Image)
        Me.cmdImprimirPedido.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimirPedido.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimirPedido.Name = "cmdImprimirPedido"
        Me.cmdImprimirPedido.Text = "Imprimir Pedido"
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'PrintDialogo
        '
        Me.PrintDialogo.AllowCurrentPage = True
        Me.PrintDialogo.AllowSelection = True
        Me.PrintDialogo.AllowSomePages = True
        Me.PrintDialogo.UseEXDialog = True
        '
        'PrintDoc
        '
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Increment = 1.0R
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.Width = 70
        '
        'Unidad
        '
        Me.Unidad.HeaderText = "Unidad"
        Me.Unidad.Name = "Unidad"
        Me.Unidad.ReadOnly = True
        Me.Unidad.Width = 60
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Producto
        '
        Me.Producto.HeaderText = "Producto"
        Me.Producto.Name = "Producto"
        Me.Producto.ReadOnly = True
        Me.Producto.Width = 320
        '
        'Impuesto
        '
        Me.Impuesto.HeaderText = "Impuesto"
        Me.Impuesto.Name = "Impuesto"
        Me.Impuesto.ReadOnly = True
        Me.Impuesto.Width = 65
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Increment = 1.0R
        Me.Valor.Name = "Valor"
        Me.Valor.Width = 90
        '
        'Total
        '
        '
        '
        '
        Me.Total.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Total.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Total.HeaderText = "Total"
        Me.Total.Increment = 1.0R
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'IdProducto
        '
        Me.IdProducto.HeaderText = "IdProducto"
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.ReadOnly = True
        Me.IdProducto.Visible = False
        '
        'PrecioCosto
        '
        Me.PrecioCosto.HeaderText = "PrecioCosto"
        Me.PrecioCosto.Name = "PrecioCosto"
        Me.PrecioCosto.ReadOnly = True
        Me.PrecioCosto.Visible = False
        '
        'TipoPrecio
        '
        Me.TipoPrecio.HeaderText = "Tipo Precio"
        Me.TipoPrecio.Name = "TipoPrecio"
        Me.TipoPrecio.ReadOnly = True
        Me.TipoPrecio.Visible = False
        '
        'frmTrasladoInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(865, 470)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmTrasladoInventario"
        Me.Text = "frmTrasladoInventario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim sSerie As String = String.Empty
    Dim lCodigoAgenciaxDefecto As Integer = 0
    Dim lAgenciaCasaMatriz As Integer = 0
    Dim lIndicaCasaMatriz As Integer = 0
    Dim inExisteFactura As Integer = 0
    Dim strUnidad As String
    Dim strFacturas As String
    Dim strDescrip As String
    Dim strNombreSucursal As String
    Dim intImpuesto As Integer
    Dim intImprimir As Integer
    Dim intRow, intCol As Integer
    Dim lngRegistro2 As Long
    Dim IndicaImprimir As Integer = 0

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String
    Dim strNombAlmacenar As String

    Private prtFont As System.Drawing.Font
    Private lineaActual As Integer

    Public txtCollection As New Collection
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmTrasladoInventario"

    Private Sub frmTrasladoInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            intDiaFact = 0
            strMesFact = ""
            lngYearFact = 0
            strVendedor = ""
            strNumeroFact = ""
            strCodCliFact = ""
            strClienteFact = ""

            Select Case intTipoFactura
                Case ENTipoTraslado.TRASLADO_ELABORAR,
                ENTipoTraslado.TRASLADO_MANUAL
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                Case ENTipoTraslado.TRASLADO_CONSULTAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    cmdImportPedidos.Visible = False
                    'dgvDetalle.Enabled = False
                    dgvDetalle.Columns("Cantidad").ReadOnly = True
                    dgvDetalle.Columns("Valor").ReadOnly = True
                Case ENTipoTraslado.TRASLADO_ANULAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
                    cmdImportPedidos.Visible = False
                    'dgvDetalle.Enabled = False
                    dgvDetalle.Columns("Cantidad").ReadOnly = True
                    dgvDetalle.Columns("Valor").ReadOnly = True
            End Select

            Me.Top = 0
            Me.Left = 0
            strUnidad = ""
            strDescrip = ""
            lngRegistro = 0
            intImprimir = 0
            txtCollection.Add(txtNumeroFactura)
            'txtCollection.Add(txtDias)
            'txtCollection.Add(txtVence)
            'txtCollection.Add(txtNombreVendedor)
            'txtCollection.Add(txtNombreCliente)
            txtCollection.Add(txtProducto)
            txtCollection.Add(txtCantidad)
            txtCollection.Add(txtPrecio)
            txtCollection.Add(TextBox9)
            txtCollection.Add(TextBox10)
            txtCollection.Add(TextBox11)
            txtCollection.Add(TextBox12)
            txtCollection.Add(txtNumeroVendedor)
            txtCollection.Add(txtNumeroCliente)
            txtCollection.Add(txtDepartamento)
            txtCollection.Add(txtNegocio)
            CargaTipoPrecio()
            ObtieneAgenciaCasaMatriz()
            ObtieneAgenciaxDefecto()
            lngRegAgencia = lCodigoAgenciaxDefecto
            cbeAgenciaDestino.DataSource = ObtieneAgenciaUsuario()
            cbeAgenciaDestino.DisplayMember = "Descripcion"
            cbeAgenciaDestino.ValueMember = "codigo"

            'If lCodigoAgenciaxDefecto = lAgenciaCasaMatriz Then
            '    cbeAgenciaDestino.Enabled = True
            'Else
            '    cbeAgenciaDestino.Enabled = False
            '    cbeAgenciaDestino.SelectedValue = lAgenciaCasaMatriz
            'End If

            'UbicarAgencia(lngRegUsuario)
            'If (lngRegAgencia = 0) Then
            '    lngRegAgencia = 0
            '    frmNew.ShowDialog()
            '    If lngRegAgencia = 0 Then
            '        MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
            '    End If
            'End If
            Limpiar()
            MenuItem8.Visible = False
            If intTipoFactura = 3 Or intTipoFactura = 6 Then
                ddlSerie.Focus()
            End If
            If intTipoFactura = 2 Or intTipoFactura = 5 Then
                ddlSerie.Visible = False
                lblSerie.Visible = False
                MenuItem8.Visible = True
                UbicarPendientes()
            End If
            'If lngRegAgencia = 0 Then
            '    Timer1.Interval = 200
            '    Timer1.Enabled = True
            'End If
            Label16.Visible = False
            Label18.Text = intTipoFactura
            Label19.Text = lngRegAgencia
            ObtieneSeriexAgencia()
            'ObtieneVendedorPorDefecto()
            ObtengoDatosListadoAyuda()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub ObtieneAgenciaxDefecto()
        Try
            'strQuery = ""
            'strQuery = "select top 1 a.registro,a.codigo,a.Descripcion,CasaMatriz from prm_agencias a where defecto=0"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            ''If cmdAgro2K.Connection.State = ConnectionState.Open Then
            ''    cmdAgro2K.Connection.Close()
            ''End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNAgencias.ObtieneAgenciaDefaultDr()
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    lCodigoAgenciaxDefecto = SUConversiones.ConvierteAInt(dtrAgro2K.Item("registro"))
                    txtAgenciaOrigen.Text = dtrAgro2K.Item("Descripcion").ToString
                    lIndicaCasaMatriz = SUConversiones.ConvierteAInt(dtrAgro2K.Item("CasaMatriz").ToString)
                End While
            End If
            dtrAgro2K = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            ' MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub ObtieneAgenciaCasaMatriz()
        Try
            'strQuery = ""
            'strQuery = "select top 1 a.registro,a.codigo,a.Descripcion,CasaMatriz from prm_agencias a where CasaMatriz=1"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            ''If cmdAgro2K.Connection.State = ConnectionState.Open Then
            ''    cmdAgro2K.Connection.Close()
            ''End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNAgencias.ObtieneAgenciaCasaMatriz()
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    lAgenciaCasaMatriz = SUConversiones.ConvierteAInt(dtrAgro2K.Item("registro"))
                    txtAgenciaOrigen.Text = dtrAgro2K.Item("Descripcion").ToString
                End While
            End If
            dtrAgro2K = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            ' MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Function ObtieneAgenciaUsuario() As DataTable

        Dim table As New DataTable("Agencias")
        table.Columns.Add("Codigo")
        table.Columns.Add("Descripcion")

        Try
            'strQuery = ""
            ''strQuery = "select a.registro,a.codigo,a.Descripcion from prm_UsuariosAgencias u,prm_agencias a  Where(u.agenregistro = a.registro)  and u.usrregistro = " & lngRegUsuario & " and a.codigo <> " & lCodigoAgenciaxDefecto
            'strQuery = "select a.registro,a.codigo,a.Descripcion from prm_agencias a  Where  a.registro <> " & lCodigoAgenciaxDefecto
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNAgencias.ObtieneAgenciasxUsuarioExcluyendoParametro(lCodigoAgenciaxDefecto)
            While dtrAgro2K.Read
                table.Rows.Add(New Object() {dtrAgro2K.Item("registro"), dtrAgro2K.Item("Descripcion")})
            End While
            dtrAgro2K = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            ' MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default
        Return table

    End Function

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0
        Try

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            ddlSerie.SelectedItem = -1
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'strQuery = ""
            dtrAgro2K = Nothing
            If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 3 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
                'strQuery = "Select a.IdSerie, Descripcion From catSerieFactura  a,tbl_ultimonumero b where Activo=1 and b.agenregistro =" & lngRegAgencia & " and a.IdSerie = b.IdSerie and Contado >0"
                ' strQuery = "ObtieneSerieParametrosTrasladoInventario " & lngRegAgencia
                dtrAgro2K = RNInventario.ObtieneSerieParametrosTrasladoInventario(lngRegAgencia)
            End If
            If intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 6 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
                'strQuery = "Select a.IdSerie, Descripcion From catSerieFactura  a,tbl_ultimonumero b where Activo=1 and b.agenregistro =" & lngRegAgencia & " and a.IdSerie = b.IdSerie and Credito >0"
                'strQuery = "ObtieneSerieParametrosTrasladoInventario " & lngRegAgencia

                dtrAgro2K = RNInventario.ObtieneSerieParametrosTrasladoInventario(lngRegAgencia)
            End If
            'If strQuery.Trim.Length > 0 Then
            '    ddlSerie.Items.Clear()
            '    cmdAgro2K.CommandText = strQuery
            '    dtrAgro2K = cmdAgro2K.ExecuteReader
            '    While dtrAgro2K.Read
            '        ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
            '        IndicaObtieneRegistro = 1
            '    End While
            'End If
            If dtrAgro2K IsNot Nothing Then
                ddlSerie.Items.Clear()
                While dtrAgro2K.Read
                    ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                    IndicaObtieneRegistro = 1
                End While

            End If
            If IndicaObtieneRegistro = 1 Then
                ddlSerie.SelectedIndex = 0
            End If
            dtrAgro2K = Nothing
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            ' MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub frmTrasladoInventario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try

            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                If intTipoFactura = 3 Or intTipoFactura = 6 Then
                    'If MSFlexGrid2.Rows <= 1 Then
                    If dgvDetalle.Rows.Count <= 0 Then
                        MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    Anular()
                    MsgBox("Requisa fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Requisa")
                    'txtNumeroFactura.Focus()
                    ddlSerie.Focus()
                ElseIf intTipoFactura = 2 Or intTipoFactura = 5 Or intTipoFactura = 7 Or intTipoFactura = 8 Or intTipoFactura = 9 Or intTipoFactura = 10 Then
                    '                txtSerie.Enabled = True
                    'If MSFlexGrid2.Rows <= 1 Then
                    If dgvDetalle.Rows.Count <= 0 Then
                        MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                        txtCantidad.Focus()
                        Exit Sub
                    End If
                    Guardar()
                End If
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If MenuItem5.Visible = True Then
                    intListadoAyuda = 1
                ElseIf MenuItem6.Visible = True Then
                    intListadoAyuda = 2
                ElseIf MenuItem7.Visible = True Then
                    intListadoAyuda = 3
                End If
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            ElseIf e.KeyCode = Keys.F7 Then
                'If MSFlexGrid2.Rows <= 1 Then
                If dgvDetalle.Rows.Count <= 0 Then
                    MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                Guardar()

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub CargaTipoPrecio()
        ddlTipoPrecio.Items.Add(New ListItem("Precio venta publico", 0))
        ddlTipoPrecio.Items.Add(New ListItem("Precio venta distribuidor", 1))
        ddlTipoPrecio.Items.Add(New ListItem("Precio costo", 2))
        ddlTipoPrecio.SelectedIndex = 2
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem12.Click

        Try


            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            Select Case sender.text.ToString
                Case "Imprimir"
                    'If MSFlexGrid2.Rows <= 1 Then
                    If dgvDetalle.Rows.Count <= 0 Then
                        MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                        txtCantidad.Focus()
                        Exit Sub
                    End If
                    Guardar()
                'UbicarPendientes()
                Case "&Guardar"
                    'If MSFlexGrid2.Rows <= 1 Then
                    If dgvDetalle.Rows.Count <= 0 Then
                        MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    Guardar()
                Case "Anular"
                    'If MSFlexGrid2.Rows <= 1 Then
                    If dgvDetalle.Rows.Count <= 0 Then
                        MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    If intTipoFactura = 3 Or intTipoFactura = 6 Then
                        intResp = MsgBox("�Est� Seguro de Anular esta Requisa?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Acci�n")
                        If intResp = 6 Then
                            Anular()
                            MsgBox("Requisa fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Requisa")
                            'txtNumeroFactura.Focus()
                            ddlSerie.Focus()
                        End If
                    End If
                Case "&Cancelar", "L&impiar" : Limpiar()
                Case "Vendedores" : intListadoAyuda = 1
                Case "Clientes" : intListadoAyuda = 2
                Case "Productos" : intListadoAyuda = 3
                Case "&Salir" : Me.Close()
            End Select
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)


    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumeroFactura.GotFocus, txtProducto.GotFocus, txtCantidad.GotFocus, txtPrecio.GotFocus, ddlTipoPrecio.GotFocus, DPkFechaFactura.GotFocus

        Dim strCampo As String = String.Empty
        Try


            Label15.Visible = False
            MenuItem5.Visible = False
            MenuItem6.Visible = False
            MenuItem7.Visible = False
            MenuItem12.Visible = False
            strCampo = ""
            Select Case sender.name.ToString
                Case "DPkFechaFactura" : strCampo = "Fecha"
                Case "txtNumeroFactura" : strCampo = "N�mero"
                    If intTipoFactura = 2 Or intTipoFactura = 5 Then
                        MenuItem12.Visible = True
                    End If
                Case "txtDia" : strCampo = "D�as Cr�dito"
                Case "txtVencimiento" : strCampo = "Vencimiento"
                Case "txtVendedor" : strCampo = "Vendedor" : MenuItem12.Visible = True : MenuItem5.Visible = True : Label15.Visible = True
                Case "txtCliente" : strCampo = "Cliente"
                    If intTipoFactura = 4 Or intTipoFactura = 8 Then
                        MenuItem12.Visible = True
                        MenuItem6.Visible = True
                        Label15.Visible = True
                    End If
                Case "txtProducto" : strCampo = "Producto" : MenuItem12.Visible = True : MenuItem7.Visible = True : Label15.Visible = True
                Case "txtCantidad" : strCampo = "Cantidad"
                Case "TextBox8" : strCampo = "Tipo de Precio"
                Case "ddlRentencion" : strCampo = "Retenci�n"
                Case "ddlTipoPrecio" : strCampo = "Tipo Precio"
            End Select
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name <> "DPkFechaFactura" Then
                sender.selectall()
            End If
            If blnUbicar Then
                ObtengoDatosListadoAyuda()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub ExtraerFactura()

        Me.Cursor = Cursors.WaitCursor
        Dim lngFecha As Long = 0
        Dim strGenNumFact As String = String.Empty
        Dim dteFechaIng As Date

        Try


            strNumeroUbicarFactura = ""
            strFacturas = ""
            If intTipoFactura <> 2 And intTipoFactura <> 5 Then
                If IsNumeric(txtNumeroFactura.Text) = False Then
                    MsgBox("Tiene que ingresar un n�mero de Requisa a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
                If Trim(txtNumeroFactura.Text) = "" Then
                    MsgBox("Tiene que ingresar un n�mero de Requisa a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else
                If Trim(txtNumeroFactura.Text) = "" Then
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If

            ' Nueva Modalidad de Busqueda y Verificacion por Facturas Nuevas (I)
            If intFechaCambioFactura >= 0 And (intTipoFactura = 6 Or intTipoFactura = 10) Then
                strGenNumFact = ""
                If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "C" Or Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                    txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                End If
                strGenNumFact = "('" & Format(CLng(txtNumeroFactura.Text), "0000000") & "C','" & Format(CLng(txtNumeroFactura.Text), "0000000") & "A')"
                Try
                    dtrAgro2K = Nothing
                    dtrAgro2K = RNProduto.UbicarMovimientoInventarioTraslado(lngRegAgencia, strGenNumFact)
                Catch exc As Exception
                    MsgBox(exc.Message.ToString)
                    dtrAgro2K.Close()
                    Exit Sub
                End Try
                If intFechaCambioFactura = 0 Then
                    txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "C"
                Else
                    txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "A"
                End If

                If (intTipoFactura = 6 Or intTipoFactura = 10) And intFechaCambioFactura > 0 Then
                    If dtrAgro2K IsNot Nothing Then
                        If dtrAgro2K.Read() Then
                            strNumeroUbicarFactura = ""
                            'dtrAgro2K.Close()
                            dtrAgro2K = Nothing
                            dtrAgro2K = RNProduto.UbicarMovimientoInventarioTraslado(lngRegAgencia, strGenNumFact)
                            Dim frmNew As New frmUbicarFacturas
                            frmNew.ShowDialog()
                            txtNumeroFactura.Text = strNumeroUbicarFactura
                        End If

                    End If
                End If
            End If
            ' Nueva Modalidad de Busqueda y Verificacion por Facturas Nuevas (F)

            If intFechaCambioFactura >= 0 And (intTipoFactura = 6 Or intTipoFactura = 8 Or intTipoFactura = 10) Then
                If strNumeroUbicarFactura = "" Then
                    If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "C" Or Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                        txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                    End If
                    If IsNumeric((Microsoft.VisualBasic.Left(txtNumeroFactura.Text, 1))) = False Then
                        txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 2, Len(txtNumeroFactura.Text))
                        If IsNumeric((Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1))) = False Then
                            txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                        End If
                    Else
                        If IsNumeric((Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1))) = False Then
                            txtNumeroFactura.Text = Mid(txtNumeroFactura.Text, 1, Len(txtNumeroFactura.Text) - 1)
                        End If
                    End If
                    If intFechaCambioFactura = 0 Then
                        txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "C"
                    Else
                        txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000") & "A"
                    End If
                End If
            End If
            strNumeroFact = txtNumeroFactura.Text
            If IsNumeric((Microsoft.VisualBasic.Left(strNumeroFact, 1))) = False Then
                strNumeroFact = Mid(strNumeroFact, 2, Len(strNumeroFact))
                If IsNumeric((Microsoft.VisualBasic.Right(strNumeroFact, 1))) = False Then
                    strNumeroFact = Mid(strNumeroFact, 1, Len(strNumeroFact) - 1)
                End If
            Else
                If IsNumeric((Microsoft.VisualBasic.Right(strNumeroFact, 1))) = False Then
                    strNumeroFact = Mid(strNumeroFact, 1, Len(strNumeroFact) - 1)
                End If
            End If
            If intFechaCambioFactura = 0 Then
                strNumeroFact = ddlSerie.SelectedItem & Format(CLng(strNumeroFact), "0000000") & "C"
            Else
                strNumeroFact = ddlSerie.SelectedItem & Format(CLng(strNumeroFact), "0000000") & "A"
            End If

            If intTipoFactura = 3 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
                '            txtSerie.Enabled = True
                'txtNumeroFactura.Text = ddlSerie.SelectedItem & Format(CLng(txtNumeroFactura.Text), "0000000")
                txtNumeroFactura.Text = Format(CLng(txtNumeroFactura.Text), "0000000")

            End If
            If IsNumeric((Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1))) = False Then
                strNumeroFact = txtNumeroFactura.Text
            Else
                strNumeroFact = ddlSerie.SelectedItem & txtNumeroFactura.Text.Trim()
            End If
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.ExtraerInventarioTraslado(lngRegAgencia, strNumeroFact, intTipoFactura)
            If intTipoFactura = 7 Or intTipoFactura = 8 Then
                strFacturas = txtNumeroFactura.Text
                dteFechaIng = DPkFechaFactura.Value
                sSerie = ddlSerie.SelectedItem
            End If

            If intTipoFactura <> 7 And intTipoFactura <> 8 Then
                Limpiar()
            End If
            lngFecha = 0
            If intTipoFactura = 2 Or intTipoFactura = 3 Or intTipoFactura = 9 Then
                'If dtrAgro2K.IsClosed Then
                '    dtrAgro2K = RNProduto.ExtraerInventarioTraslado(lngRegAgencia, strNumeroFact, intTipoFactura)
                'End If
                dtrAgro2K = Nothing
                dtrAgro2K = RNProduto.ExtraerInventarioTraslado(lngRegAgencia, strNumeroFact, intTipoFactura)
                If dtrAgro2K IsNot Nothing Then

                    While dtrAgro2K.Read
                        ddlSerie.SelectedItem = dtrAgro2K.GetValue(22)
                        txtNumeroCliente.Text = dtrAgro2K.GetValue(21)
                        TextBox17.Text = dtrAgro2K.GetValue(0)
                        lngFecha = dtrAgro2K.GetValue(19)
                        DPkFechaFactura.Value = DefinirFecha(lngFecha)
                        DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                        txtNumeroFactura.Text = dtrAgro2K.GetValue(2)

                        dgvDetalle.Rows.Add(Format(dtrAgro2K.GetValue(6), "#,##0.#0"), dtrAgro2K.GetValue(7), dtrAgro2K.GetValue(8),
                                        dtrAgro2K.GetValue(9), IIf(dtrAgro2K.GetValue(10) > 0, "Si", "No"), Format(dtrAgro2K.GetValue(11), "#,##0.#0"),
                                        Format(dtrAgro2K.GetValue(12), "#,##0.#0"), dtrAgro2K.GetValue(13))

                        TextBox9.Text = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                        TextBox10.Text = Format(dtrAgro2K.GetValue(15), "#,##0.#0")
                        TextBox11.Text = Format(dtrAgro2K.GetValue(16), "#,##0.#0")
                        TextBox12.Text = Format(dtrAgro2K.GetValue(17), "#,##0.#0")
                        dblTipoCambio = Format(ConvierteADouble(dtrAgro2K.GetValue(20)), "###0.#0")
                        If dtrAgro2K.GetValue(18) = 1 Then
                            Label16.Visible = True
                        Else
                            Label16.Visible = False
                        End If
                    End While
                End If
            ElseIf intTipoFactura = 5 Or intTipoFactura = 6 Or intTipoFactura = 10 Then
                'If dtrAgro2K.IsClosed Then
                '    dtrAgro2K = Nothing
                '    dtrAgro2K = RNProduto.ExtraerInventarioTraslado(lngRegAgencia, strNumeroFact, intTipoFactura)
                'End If
                dtrAgro2K = Nothing
                dtrAgro2K = RNProduto.ExtraerInventarioTraslado(lngRegAgencia, strNumeroFact, intTipoFactura)
                If dtrAgro2K IsNot Nothing Then


                    While dtrAgro2K.Read
                        txtNumeroCliente.Text = dtrAgro2K.GetValue(23)
                        TextBox17.Text = dtrAgro2K.GetValue(0)
                        lngFecha = dtrAgro2K.GetValue(21)
                        DPkFechaFactura.Value = DefinirFecha(lngFecha)
                        txtNumeroFactura.Text = dtrAgro2K.GetValue(2)
                        DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                        dblTipoCambio = ConvierteADouble(dtrAgro2K.GetValue(22))

                        Dim a, b As Double
                        If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                            a = Format(ConvierteADouble(dtrAgro2K.GetValue(13)) / dblTipoCambio, "#,##0.#0")
                        Else
                            a = Format(dtrAgro2K.GetValue(13), "#,##0.#0")
                        End If
                        If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                            b = Format(ConvierteADouble(dtrAgro2K.GetValue(14)) / dblTipoCambio, "#,##0.#0")
                        Else
                            b = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                        End If

                        dgvDetalle.Rows.Add(Format(dtrAgro2K.GetValue(8), "#,##0.#0"), dtrAgro2K.GetValue(9), dtrAgro2K.GetValue(10),
                                    dtrAgro2K.GetValue(11), IIf(dtrAgro2K.GetValue(12) > 0, "Si", "No"), a, b,
                                    dtrAgro2K.GetValue(15))

                        If Microsoft.VisualBasic.Right(txtNumeroFactura.Text, 1) = "A" Then
                            TextBox9.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(16)) / dblTipoCambio, "#,##0.#0")
                            TextBox10.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(17)) / dblTipoCambio, "#,##0.#0")
                            TextBox11.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(18)) / dblTipoCambio, "#,##0.#0")
                            TextBox12.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(19)) / dblTipoCambio, "#,##0.#0")
                        Else
                            TextBox9.Text = Format(dtrAgro2K.GetValue(16), "#,##0.#0")
                            TextBox10.Text = Format(dtrAgro2K.GetValue(17), "#,##0.#0")
                            TextBox11.Text = Format(dtrAgro2K.GetValue(18), "#,##0.#0")
                            TextBox12.Text = Format(dtrAgro2K.GetValue(19), "#,##0.#0")
                        End If
                        If dtrAgro2K.GetValue(20) = 1 Then
                            Label16.Visible = True
                        Else
                            Label16.Visible = False
                        End If
                    End While
                End If
            ElseIf intTipoFactura = 7 Or intTipoFactura = 8 Then
                inExisteFactura = 0
                If dtrAgro2K IsNot Nothing Then
                    While dtrAgro2K.Read
                        inExisteFactura = 1
                        txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                    End While

                End If
                DPkFechaFactura.Value = dteFechaIng
            End If
            dtrAgro2K = Nothing
            Me.Cursor = Cursors.Default
            If intTipoFactura = 9 Or intTipoFactura = 10 Then
                txtNumeroFactura.Focus()
                txtNumeroFactura.SelectAll()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroFactura.KeyDown, txtProducto.KeyDown, txtCantidad.KeyDown, txtPrecio.KeyDown, ddlTipoPrecio.KeyDown, ddlSerie.KeyDown, DPkFechaFactura.KeyDown, cbeAgenciaDestino.KeyDown
        Try


            If e.KeyCode = Keys.Enter Then
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                Select Case sender.name.ToString
                    Case "DPkFechaFactura"
                        Select Case intTipoFactura
                            Case 1, 2 : ddlSerie.Focus()
                            'txtVendedor.Focus()
                            Case 3, 4, 5, 6 : ddlSerie.Focus()
                            Case 7, 8 : ddlSerie.Focus() 'txtNumeroFactura.Focus()
                        End Select
                    Case "txtNumeroFactura"
                        Select Case intTipoFactura
                            Case 3, 6 : ExtraerFactura()
                                If txtNumeroFactura.Text = "" Then
                                    MsgBox("El N�mero Ingresado no Pertenece a una Requisa Emitida o la Requisa est� Anulada.", MsgBoxStyle.Information, "Requisa No Emitida o Anulada")
                                    Limpiar()
                                    ddlSerie.Focus()
                                    'txtNumeroFactura.Focus()
                                    Exit Sub
                                End If
                            Case 7, 8 : ExtraerFactura()
                                If inExisteFactura = 1 Then
                                    'If txtNumeroFactura.Text <> "" Then
                                    MsgBox("El N�mero Ingresado ya Pertenece a una Requisa Emitida o la Requisa est� Anulada.", MsgBoxStyle.Information, "Requisa Emitida o Anulada")
                                    txtNumeroFactura.Focus()
                                    txtNumeroFactura.Text = ""
                                    Exit Sub
                                End If
                                txtNumeroFactura.Text = strFacturas
                            Case 9, 10 : ExtraerFactura()
                                If txtNumeroFactura.Text = "" Then
                                    MsgBox("El N�mero Ingresado no Pertenece a una Requisa Emitida.", MsgBoxStyle.Information, "Requisa No Emitida")
                                    Limpiar()
                                    txtNumeroFactura.Focus()
                                    Exit Sub
                                End If
                        End Select
                    Case "ddlSerie"
                        Select Case intTipoFactura
                            Case 3, 6 : txtNumeroFactura.Focus()
                            'Case 4 : txtDias.Focus()
                            Case 7, 8 : txtNumeroFactura.Focus()
                            Case Else
                                If cbeAgenciaDestino.Enabled = True Then
                                    cbeAgenciaDestino.Focus()
                                Else
                                    txtCantidad.Focus()
                                End If
                        End Select
                    Case "txtProducto"
                        blnUbicar = False
                        If UbicarProducto(txtProducto.Text) = False Then
                            MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtProducto.Focus()
                            Exit Sub
                        End If
                        'ddlRetencion.Focus()
                        ddlTipoPrecio.Focus()
                    Case "txtCantidad"
                        If dgvDetalle.Rows.Count > 0 And Trim(txtCantidad.Text) = "0" Then
                            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                                Guardar()
                                Exit Sub
                            End If
                        Else
                            txtProducto.Focus()
                        End If
                    Case "cbeAgenciaDestino"
                        txtCantidad.Focus()
                    'Case "ComboBox1" : Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%" : ComboBox2.Focus() : ComboBox2.SelectedIndex = 0 : UbicarPrecio()
                    Case "ddlRetencion" : Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%" : ddlTipoPrecio.Focus() : IIf(intFechaCambioFactura > 0, ddlTipoPrecio.SelectedIndex = 2, ddlTipoPrecio.SelectedIndex = 0) : UbicarPrecio()
                    Case "ddlTipoPrecio" : UbicarPrecio() : txtPrecio.Focus()
                    Case "txtPrecio"
                        If (dgvDetalle.Rows.Count + 1) > 16 Then
                            MsgBox("Cantidad maxima para una Requisa es 16 Item", MsgBoxStyle.Critical, "Favor validar")
                            DPkFechaFactura.Focus()
                            Exit Sub
                        End If
                        Dim lnPrecio As Decimal = 0
                        Dim lnTotal As Decimal = 0

                        ' lnPrecio = SUConversiones.ConvierteADecimal(txtPrecio.Text)
                        lnPrecio = 0
                        lnTotal = lnPrecio * SUConversiones.ConvierteADecimal(txtCantidad.Text)
                        'lnTotal = 0
                        If SUConversiones.ConvierteADouble(txtCantidad.Text) > 0 Then
                            DPkFechaFactura.Focus()
                            dgvDetalle.Rows.Add(Format(SUConversiones.ConvierteADouble(txtCantidad.Text), "####0.#0"), UCase(strUnidad), UCase(txtProducto.Text),
                                                UCase(strDescrip), IIf(intImpuesto = 0, "No", "Si"), lnPrecio,
                                                Format(lnTotal, "#,##0.#0"),
                                                lngRegistro, Format(lnPrecio, "#,##0.#0"), ddlTipoPrecio.SelectedIndex)

                            'TextBox9.Text = Format(Format((ConvierteADouble(txtPrecio.Text) * ConvierteADouble(txtCantidad.Text)), "#,##0.#0") + ConvierteADouble(TextBox9.Text), "#,##0.#0")
                            TextBox9.Text = Format(Format((lnTotal), "#,##0.#0") + ConvierteADouble(TextBox9.Text), "#,##0.#0")
                            If intImpuesto = 0 Then
                                TextBox10.Text = Format(ConvierteADouble(TextBox10.Text), "#,##0.#0")
                            ElseIf intImpuesto = 1 Then
                                'TextBox10.Text = Format(Format((Format((ConvierteADouble(txtPrecio.Text) * ConvierteADouble(txtCantidad.Text)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ConvierteADouble(TextBox10.Text), "#,##0.#0")
                                TextBox10.Text = Format(Format((Format((lnTotal), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ConvierteADouble(TextBox10.Text), "#,##0.#0")
                            End If
                            TextBox11.Text = "0.00"
                            TextBox12.Text = Format(ConvierteADouble(TextBox9.Text) + ConvierteADouble(TextBox10.Text) - ConvierteADouble(TextBox11.Text), "#,##0.#0")
                            Limpiar2()
                        Else
                            MsgBox("No puede vender un producto con cantidad 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                            txtCantidad.Focus()
                        End If
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Sub Guardar()
        Dim objMovTraslado As SEMovimientoInventarioEnc = Nothing
        Dim Resultado As Object() = New Object(4) {}
        Dim lstMovTrasladoDet As List(Of SEMovimientoInventarioDet) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Dim objReqCampo As Object = Nothing
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing
        Dim frmLogin2 As frmLoginSuprvisor = Nothing
        Dim ldFechaDelDia As Date = Nothing
        Try

            ldFechaDelDia = New Date(Now.Year, Now.Month, Now.Day)
            objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            If intTipoFactura = 1 Or intTipoFactura = 4 Then        ' Del D�a
                If Format(DPkFechaFactura.Value, "yyyyMMdd") <> Format(ldFechaDelDia, "yyyyMMdd") Then
                    If RNFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(lngRegUsuario) = 0 Then
                        If objParametroEmpresa IsNot Nothing Then
                            intResp = MsgBox("Usuario no tiene privilegio para realizar cambio de fecha. �Desea continuar?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Critical, "Ingreso Factura")
                            Select Case intResp
                        ' Case Windows.Forms.DialogResult.Yes
                                Case 6
                                    Select Case objParametroEmpresa.DesactivaObjetoFechaEnOperaciones.ToUpper
                                        Case "N"
                                            MsgBox("Usuario no tiene permisos para poder realizar cambio de fecha. ", MsgBoxStyle.Critical, "Facturas ")
                                            DPkFechaFactura.Value = ldFechaDelDia
                                            DPkFechaFactura.Focus()
                                            Return
                                        Case "S"
                                            frmLogin2 = New frmLoginSuprvisor()
                                            frmLogin2.ShowDialog()
                                            If lngRegUsuarioRelogin = -1 Then
                                                MsgBox("Usuario no tiene privilegio para cambiar la fecha para esta operaci�n", MsgBoxStyle.Critical)
                                                DPkFechaFactura.Value = ldFechaDelDia
                                                DPkFechaFactura.Focus()
                                                Return
                                                Exit Sub
                                            End If
                                            If RNFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(lngRegUsuarioRelogin) = 0 Then
                                                MsgBox("Usuario no tiene privilegio para cambiar la fecha para esta operaci�n.", MsgBoxStyle.Critical)
                                                DPkFechaFactura.Value = ldFechaDelDia
                                                DPkFechaFactura.Focus()
                                                Return
                                                Exit Sub
                                            End If

                                        Case Else

                                    End Select
                                Case 7
                                    DPkFechaFactura.Value = ldFechaDelDia
                                    DPkFechaFactura.Focus()
                                    Return
                                    Exit Sub
                                Case Else
                            End Select
                        End If
                    End If
                End If
            End If



            If dgvDetalle.IsCurrentCellDirty Then
                dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If (dgvDetalle.Rows.Count + 1) > 16 Then
                MsgBox("Cantidad maxima para una Requisa es 16 Item", MsgBoxStyle.Critical, "Favor validar")
                DPkFechaFactura.Focus()
                Exit Sub
            End If

            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To dgvDetalle.Rows.Count - 1
                If IsNumeric(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value) = False Then
                    MsgBox("No es un valor numerico la cantidad a vender", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                ElseIf SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value) <= 0 Then
                    MsgBox("La cantidad a vender debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
            Next intIncr
            strNombreSucursal = cbeAgenciaDestino.SelectedItem("Descripcion").ToString()
            Dim frmNew As New actrptViewer
            If SUConversiones.ConvierteAInt(txtNumeroVendedor.Text) = -1 Then
                MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del vendedor.", MsgBoxStyle.Critical, "Error de Ingreso")
                txtNumeroVendedor.Focus()
                Exit Sub
            End If
            intResp = 0
            intImprimir = 0
            If intTipoFactura = 1 Or intTipoFactura = 4 Then        ' Del D�a
                If lngRegAgencia = 1 Then
                    intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Impresi�n de Requisa")
                Else
                    intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Requisa")
                End If
                If intResp = 2 Then     'Cancelar
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                ElseIf intResp = 7 Then 'No
                    intImprimir = 1
                End If
            ElseIf intTipoFactura = 2 Or intTipoFactura = 5 Then    ' Imprimir Factura
                intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Requisa")
                If intResp <> 6 Then 'Cancelar o No
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
            ElseIf intTipoFactura = 3 Or intTipoFactura = 6 Then    ' Anular Factura
                intResp = MsgBox("�Desea anular la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Anulaci�n de Requisa")
                If intResp <> 6 Then 'No
                    txtNumeroFactura.Focus()
                    Exit Sub
                End If
            ElseIf intTipoFactura = 7 Or intTipoFactura = 8 Then    ' Ingreso Manual
                '            txtSerie.Enabled = True
                intResp = MsgBox("�Desea guardar la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Requisa")
                If intResp <> 6 Then    'Cancelar o No
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
            ElseIf intTipoFactura = 9 Or intTipoFactura = 10 Then   ' Consultar Factura
                intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Requisa")
                If intResp <> 6 Then 'Cancelar o No
                    txtNumeroFactura.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
            End If

            Me.Cursor = Cursors.WaitCursor
            intIncr = 0
            intIncr2 = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            dblSubtotal = 0
            dblImpuesto = 0
            dblRetencion = 0
            dblTotal = 0
            strFecVencFact = ""
            'strVendedor = txtVendedor.Text + " " + txtNombreVendedor.Text
            strNumeroFact = ddlSerie.SelectedItem + txtNumeroFactura.Text
            'If intTipoFactura = 1 Or intTipoFactura = 4 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                intDiaFact = Format(Now, "dd")
                strMesFact = Format(Now, "MMM")
                lngYearFact = Format(Now, "yyyy")
                lngFechaFactura = Format(Now, "yyyyMMdd")
            ElseIf intTipoFactura <> 1 And intTipoFactura <> 4 Then
                intDiaFact = Format(DPkFechaFactura.Value, "dd")
                strMesFact = Format(DPkFechaFactura.Value, "MMM")
                lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
                lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")
            End If
            If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
                strClienteFact = strNombreSucursal
                For intIncr = 0 To dgvDetalle.Rows.Count - 1
                    For intIncr2 = 0 To 6
                        If intIncr2 = 5 Or intIncr2 = 6 Then
                            'arrContado(intIncr - 1, intIncr2) = "U$ " & MSFlexGrid2.Text   // TipoCambio
                            arrContado(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                        Else
                            arrContado(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                        End If
                    Next intIncr2
                Next intIncr
                intRptImpFactura = 1
            ElseIf intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
                'strCodCliFact = txtCliente.Text
                strClienteFact = strNombreSucursal
                'strFecVencFact = txtVence.Text
                'strDiasFact = txtDias.Text
                For intIncr = 0 To dgvDetalle.Rows.Count - 1
                    For intIncr2 = 0 To 5
                        If intIncr2 = 5 Or intIncr2 = 6 Then
                            'arrCredito(intIncr - 1, intIncr2) = "U$ " & MSFlexGrid2.Text   // TipoCambio
                            arrCredito(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                        Else
                            arrCredito(intIncr, intIncr2) = dgvDetalle.Rows(intIncr).Cells(intIncr2).Value
                        End If
                    Next intIncr2
                Next intIncr
                intRptImpFactura = 4
            End If
            'If txtNombreCliente.Text = "" Then
            '    txtNombreCliente.Text = "Contado"
            'End If
            dblSubtotal = ConvierteADouble(TextBox9.Text)
            dblImpuesto = ConvierteADouble(TextBox10.Text)
            dblRetencion = ConvierteADouble(TextBox11.Text)
            dblTotal = ConvierteADouble(TextBox12.Text)
            strValorFactCOR = ""
            strValorFactCOR = "C$ " & Format((dblTotal * dblTipoCambio), "#,##0.#0")
            If intTipoFactura <> 3 And intTipoFactura <> 6 And intTipoFactura <> 7 And intTipoFactura <> 8 Then
                If intImprimir = 0 Then
                    If intTipoFactura = 9 Or intTipoFactura = 10 Then
                        frmNew.Show()
                        If intImpresoraAsignada = 1 Then
                            Exit Sub
                        End If
                        frmNew.WindowState = FormWindowState.Minimized
                        Me.Cursor = Cursors.Default
                        frmNew.Close()
                        Exit Sub
                    End If
                End If
            End If

            Dim lngNumFecha As Long

            lngNumFecha = 0
            lngRegistro = 0
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")

            objMovTraslado = New SEMovimientoInventarioEnc
            objMovTraslado.registro = 0
            If IsNumeric((Microsoft.VisualBasic.Left(txtNumeroFactura.Text, 1))) = False Then
                objMovTraslado.numero = txtNumeroFactura.Text
            Else
                objMovTraslado.numero = ddlSerie.SelectedItem & txtNumeroFactura.Text
            End If
            objMovTraslado.numfechaing = lngNumFecha
            objMovTraslado.fechaingreso = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
            objMovTraslado.vendregistro = SUConversiones.ConvierteAInt(txtNumeroVendedor.Text)
            If intTipoFactura <> 4 And intTipoFactura <> 8 Then
                objMovTraslado.cliregistro = lngClienteContado
            ElseIf intTipoFactura = 4 Or intTipoFactura = 8 Then
                objMovTraslado.cliregistro = SUConversiones.ConvierteAInt(txtNumeroCliente.Text)
            End If
            objMovTraslado.clinombre = ""
            If intFechaCambioFactura > 0 Then
                objMovTraslado.subtotal = Format((TextBox9.Text * dblTipoCambio), "#####0.#0")
            Else
                objMovTraslado.subtotal = Format(ConvierteADouble(TextBox9.Text), "#####0.#0")
            End If
            If intFechaCambioFactura > 0 Then
                objMovTraslado.impuesto = Format((TextBox10.Text * dblTipoCambio), "#####0.#0")
            Else
                objMovTraslado.impuesto = Format(ConvierteADouble(TextBox10.Text), "#####0.#0")
            End If
            If intFechaCambioFactura > 0 Then
                objMovTraslado.retencion = Format((TextBox11.Text * dblTipoCambio), "#####0.#0")
            Else
                objMovTraslado.retencion = Format(ConvierteADouble(TextBox11.Text), "#####0.#0")
            End If
            If intFechaCambioFactura > 0 Then
                objMovTraslado.total = Format((TextBox12.Text * dblTipoCambio), "#####0.#0")
            Else
                objMovTraslado.total = Format(ConvierteADouble(TextBox12.Text), "#####0.#0")
            End If
            objMovTraslado.userregistro = lngRegUsuario
            objMovTraslado.agenregistroOrigen = lngRegAgencia
            objMovTraslado.impreso = intImprimir
            If intTipoFactura <> 4 And intTipoFactura <> 8 Then
                objMovTraslado.deptregistro = lngDepartContado
            ElseIf intTipoFactura = 4 Or intTipoFactura = 8 Then
                objMovTraslado.deptregistro = SUConversiones.ConvierteAInt(txtDepartamento.Text)
            End If
            If intTipoFactura <> 4 And intTipoFactura <> 8 Then
                objMovTraslado.negregistro = lngNegocioContado
            ElseIf intTipoFactura = 4 Or intTipoFactura = 8 Then
                objMovTraslado.negregistro = SUConversiones.ConvierteAInt(txtNegocio.Text)
            End If
            If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
                objMovTraslado.tipofactura = 0
            ElseIf intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
                objMovTraslado.tipofactura = 1
            End If
            objMovTraslado.diascredito = 0
            dblTipoCambio = 0
            dblTipoCambio = UbicarTipoCambio(lngNumFecha)
            objMovTraslado.tipocambio = dblTipoCambio
            If intTipoFactura = 2 Or intTipoFactura = 5 Then
                objMovTraslado.status = 2
            ElseIf intTipoFactura = 3 Or intTipoFactura = 6 Then
                objMovTraslado.status = 1
            Else
                objMovTraslado.status = 0
            End If
            objMovTraslado.IdMovimiento = "TO"
            objMovTraslado.agenregistroDestino = cbeAgenciaDestino.SelectedValue

            lstMovTrasladoDet = CargarDetalleTraslado()

            If (objMovTraslado Is Nothing) And (lstMovTrasladoDet Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa", " Guardar Traslados ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            RNProduto.IngresaTransaccionTraslado(objMovTraslado, lstMovTrasladoDet, intTipoFactura)

            MsgBox("Registro Ingresado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                If sSerie.ToString <> "" Then
                    ActualizarNumeroInventario(lngRegAgencia, sSerie)
                End If
            End If
            If intTipoFactura <> 3 And intTipoFactura <> 6 And intTipoFactura <> 7 And intTipoFactura <> 8 Then
                If intImprimir = 0 Then
                    IndicaImprimir = 1
                    strQuery = "ExtraerInventarioTraslado " & lngRegAgencia & ",'" & ddlSerie.SelectedItem & txtNumeroFactura.Text & "'," & 9
                    If intTipoFactura = 4 Then
                        'UbicarCliente(txtCliente.Text)
                        strQuery = "ExtraerInventarioTraslado " & lngRegAgencia & ",'" & ddlSerie.SelectedItem & txtNumeroFactura.Text & "'," & 10
                    End If
                    frmNew.Show()
                    If intImpresoraAsignada = 0 Then
                        frmNew.WindowState = FormWindowState.Minimized
                        frmNew.Close()
                    End If
                End If
            End If
            Limpiar()
            If intTipoFactura = 2 Or intTipoFactura = 5 Then
                UbicarPendientes()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Function CargarDetalleTraslado() As List(Of SEMovimientoInventarioDet)
        Dim lstMovTrasladoDet As List(Of SEMovimientoInventarioDet) = Nothing
        Dim objMovTrasladoDet As SEMovimientoInventarioDet = Nothing
        Dim i As Integer = 0

        Try
            objMovTrasladoDet = New SEMovimientoInventarioDet()
            lstMovTrasladoDet = New List(Of SEMovimientoInventarioDet)

            If dgvDetalle.Rows.Count > 0 Then
                For i = 0 To dgvDetalle.Rows.Count - 1
                    objMovTrasladoDet.prodregistro = dgvDetalle.Rows(i).Cells("IdProducto").Value
                    objMovTrasladoDet.cantidad = dgvDetalle.Rows(i).Cells("Cantidad").Value
                    objMovTrasladoDet.PrecioCosto = dgvDetalle.Rows(i).Cells("PrecioCosto").Value
                    objMovTrasladoDet.precio = dgvDetalle.Rows(i).Cells("Valor").Value
                    objMovTrasladoDet.valor = dgvDetalle.Rows(i).Cells("Total").Value
                    'objDetalleFact.Igv = dgvDetalle.Rows(i).Cells("Igv").Value
                    lstMovTrasladoDet.Add(New SEMovimientoInventarioDet(objMovTrasladoDet))
                Next
            End If
            Return lstMovTrasladoDet
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Function

    Sub Limpiar()
        Try

            DPkFechaFactura.Value = Now
            intIncr = 0
            For intIncr = 1 To 6
                txtCollection.Item(intIncr).Text = ""
            Next intIncr
            intIncr = 0
            For intIncr = 7 To 12
                txtCollection.Item(intIncr).Text = "0.00"
            Next intIncr
            For intIncr = 13 To txtCollection.Count
                txtCollection.Item(intIncr).Text = "0"
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            'txtDias.Text = "30"
            'txtVendedor.Text = ""
            'txtCliente.Text = ""
            'txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
            'ddlRetencion.SelectedIndex = 0
            ddlSerie.SelectedIndex = -1
            'If intFechaCambioFactura > 0 Then
            '    ddlTipoPrecio.SelectedIndex = 2
            'Else
            '    ddlTipoPrecio.SelectedIndex = 0
            'End If
            ddlTipoPrecio.SelectedIndex = 2
            DPkFechaFactura.Focus()
            Select Case intTipoFactura
                'Case 1, 2, 3, 7, 9 : Label3.Visible = False : Label4.Visible = False : txtDias.Visible = False : txtVence.Visible = False
                '    txtCliente.Visible = False : txtNombreCliente.ReadOnly = False : txtNombreCliente.Width = 280 : txtNombreCliente.Location = New Point(419, 48)
                'Case 4, 5, 6, 8, 10 : Label3.Visible = True : Label4.Visible = True : txtDias.Visible = True : txtVence.Visible = True
            End Select
            Select Case intTipoFactura
                Case 1, 4 'ObtieneSeriexAgencia() ': txtNumeroFactura.Text = ExtraerNumeroFactura(lngRegAgencia, intTipoFactura, ddlSerie.SelectedItem)
                Case 2, 3, 5, 6 : BloquearCampos()
                Case 7, 8 : txtNumeroFactura.ReadOnly = False
                Case 9, 10 : BloquearCampos() : txtNumeroFactura.ReadOnly = False
            End Select
            'MSFlexGrid2.Clear()
            'MSFlexGrid2.Rows = 1
            'MSFlexGrid2.FormatString = "Cantidad   |<Unidad     |<Codigo     |<Producto                                                               |<Impuesto  |>Precio        |>Valor             |<|>|>|>"
            DPkFechaFactura.Focus()
            ObtieneSeriexAgencia()
            dgvDetalle.Rows.Clear()
            'ObtieneVendedorPorDefecto()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar2()
        Try


            txtProducto.Text = ""
            txtCantidad.Text = "0"
            txtPrecio.Text = "0.00"
            'If intFechaCambioFactura > 0 Then
            '    ddlTipoPrecio.SelectedIndex = 2
            'Else
            '    ddlTipoPrecio.SelectedIndex = 0
            'End If
            ddlTipoPrecio.SelectedIndex = 2
            txtCantidad.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub BloquearCampos()
        Try


            For intIncr = 1 To txtCollection.Count - 1
                txtCollection.Item(intIncr).readonly = True
            Next
            DPkFechaFactura.Enabled = False
            'ddlRetencion.Enabled = False
            ddlTipoPrecio.Enabled = False
            'txtVendedor.ReadOnly = True
            'txtCliente.ReadOnly = True
            If intTipoFactura = 3 Or intTipoFactura = 6 Then
                txtNumeroFactura.ReadOnly = False
                'txtSerie.ReadOnly = False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Anular()
        Dim objMovTraslado As SEMovimientoInventarioEnc
        Dim Resultado As Object() = New Object(4) {}
        Dim lstMovTrasladoDet As List(Of SEMovimientoInventarioDet) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Dim objReqCampo As Object = Nothing

        If Label16.Visible = True Then
            MsgBox("No puede anular una requisa anulada.  Veriique.", MsgBoxStyle.Critical, "Requisa Anulada")
            Exit Sub
        End If
        If Year(DPkFechaFactura.Value) = Year(Now) And Month(DPkFechaFactura.Value) = Month(Now) Then
        Else
            MsgBox("La requisa a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Requisa Inv�lida")
            Exit Sub
        End If

        Dim lngNumFecha As Long
        Try
            lngNumFecha = 0
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")

            objMovTraslado = New SEMovimientoInventarioEnc
            objMovTraslado.registro = 0
            If IsNumeric((Microsoft.VisualBasic.Left(txtNumeroFactura.Text, 1))) = False Then
                objMovTraslado.numero = txtNumeroFactura.Text
            Else
                objMovTraslado.numero = ddlSerie.SelectedItem & txtNumeroFactura.Text
            End If
            objMovTraslado.numfechaing = lngNumFecha
            objMovTraslado.fechaingreso = ""
            objMovTraslado.vendregistro = 0
            If intTipoFactura = 3 Then
                objMovTraslado.cliregistro = lngClienteContado
            ElseIf intTipoFactura = 6 Then
                objMovTraslado.cliregistro = SUConversiones.ConvierteAInt(txtNumeroCliente.Text)
            End If
            objMovTraslado.clinombre = ""

            objMovTraslado.subtotal = 0
            objMovTraslado.impuesto = 0
            objMovTraslado.retencion = 0
            If intFechaCambioFactura > 0 Then
                objMovTraslado.total = Format((TextBox12.Text * dblTipoCambio), "#####0.#0")
            Else
                objMovTraslado.total = Format(ConvierteADouble(TextBox12.Text), "#####0.#0")
            End If
            objMovTraslado.userregistro = 0
            objMovTraslado.agenregistroOrigen = lngRegAgencia
            objMovTraslado.impreso = 0
            objMovTraslado.deptregistro = 0
            objMovTraslado.negregistro = 0
            If intTipoFactura = 3 Then
                objMovTraslado.tipofactura = 0
            ElseIf intTipoFactura = 6 Then
                objMovTraslado.tipofactura = 1
            End If
            objMovTraslado.diascredito = 0
            objMovTraslado.tipocambio = 0
            objMovTraslado.status = 1
            objMovTraslado.IdMovimiento = "TO"
            objMovTraslado.agenregistroDestino = cbeAgenciaDestino.SelectedValue

            lstMovTrasladoDet = CargarDetalleTraslado()

            If (objMovTraslado Is Nothing) And (lstMovTrasladoDet Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa", " Guardar Traslados ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            RNProduto.IngresaTransaccionTraslado(objMovTraslado, lstMovTrasladoDet, intTipoFactura)
            MsgBox("Requisa Anulada", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default


    End Sub

    Function UbicarProducto(ByVal strDato As String) As Boolean

        Try
            strDescrip = String.Empty
            strUnidad = String.Empty
            lngRegistro = 0
            intImpuesto = 0
            'strQuery = String.Empty
            'strQuery = "Select P.Descripcion, U.Codigo, P.Registro, P.Impuesto from prm_Productos P, prm_Unidades U "
            'strQuery = strQuery + "Where P.Codigo = '" & strDato & "' AND P.RegUnidad = U.Registro AND P.Estado = 0"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.UbicarProducto(strDato)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    'strDescrip = dtrAgro2K.GetValue(0)
                    strDescrip = String.Empty
                    strDescrip = dtrAgro2K.Item("Descripcion")
                    'strUnidad = dtrAgro2K.GetValue(1)
                    strUnidad = String.Empty
                    strUnidad = dtrAgro2K.Item("CodigoUnidad")
                    lngRegistro = 0
                    lngRegistro = SUConversiones.ConvierteAInt(dtrAgro2K.Item("Registro"))
                    'lngRegistro = dtrAgro2K.GetValue(2)
                    'intImpuesto = dtrAgro2K.GetValue(3)
                    intImpuesto = 0
                    intImpuesto = SUConversiones.ConvierteAInt(dtrAgro2K.Item("Impuesto"))
                End While
            End If
            dtrAgro2K = Nothing
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Function

    Sub UbicarPrecio()
        Try

            'If intFechaCambioFactura = 0 Then
            '    If ddlTipoPrecio.SelectedIndex = 2 Or ddlTipoPrecio.SelectedIndex = 3 Then
            '        MsgBox("El tipo de precio 3 y 4 no son v�lidos, unicamente el 1 y 2.", MsgBoxStyle.Critical, "Error de Tipo de Precio")
            '        ddlTipoPrecio.SelectedIndex = 0
            '        ddlTipoPrecio.Focus()
            '        Exit Sub
            '    End If
            'ElseIf intFechaCambioFactura > 0 Then
            '    If ddlTipoPrecio.SelectedIndex = 0 Or ddlTipoPrecio.SelectedIndex = 1 Then
            '        MsgBox("El tipo de precio 1 y 2 no son v�lidos, unicamente el 3 y 4.", MsgBoxStyle.Critical, "Error de Tipo de Precio")
            '        ddlTipoPrecio.SelectedIndex = 2
            '        ddlTipoPrecio.Focus()
            '        Exit Sub
            '    End If
            'End If
            strQuery = String.Empty
            'Select Case ddlTipoPrecio.SelectedIndex
            '    Case 0 : strQuery = "Select (case when P.Pvpc=0 then P.Pvpu else P.Pvpc end) from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 1 : strQuery = "Select (case when P.Pvdc=0 then P.Pvdu else P.Pvdc end) from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 2 : strQuery = "Select dbo.fnObtienePrecioCosto(P.registro) from prm_Productos P Where P.Registro = " & lngRegistro & ""
            'End Select
            'Select Case ddlTipoPrecio.SelectedIndex
            '    Case 0 : strQuery = "Select P.Pvpc from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 1 : strQuery = "Select P.Pvdc from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 2 : strQuery = "Select P.Pvpu from prm_Productos P Where P.Registro = " & lngRegistro & ""
            '    Case 3 : strQuery = "Select P.Pvdu from prm_Productos P Where P.Registro = " & lngRegistro & ""
            'End Select
            'If (strQuery.Trim().Length <= 0) Then
            '    MsgBox("Favor seleccione el tipo precio", MsgBoxStyle.Critical)
            '    Exit Sub
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.UbicarPrecio(lngRegistro, ddlTipoPrecio.SelectedIndex, 1, lngRegAgencia)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    txtPrecio.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("Precio"))
                End While

            End If
            dtrAgro2K = Nothing
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default


    End Sub

    Private Sub ObtengoDatosListadoAyuda()
        Try

            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                    'Case 1 : txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                    '    If txtCliente.Visible = True Then
                    '        txtCliente.Focus()
                    '    Else
                    '        txtNombreCliente.Focus()
                    '    End If
                    'Case 2 : txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text)
                    '    If txtVendedor.Text = "" Then
                    '        MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                    '        txtVendedor.Focus()
                    '    End If
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : ddlTipoPrecio.Focus() ' ddlRetencion.Focus()
                        Case 4 : txtNumeroFactura.Text = strUbicar : ExtraerFactura()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Requisa: " + ex.Message.ToString(), " Guardar Traslado ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            If lngRegAgencia = 0 Then
                Timer1.Enabled = False
                Me.Close()
            ElseIf blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                    'Case 1 : txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                    '    If txtCliente.Visible = True Then
                    '        txtCliente.Focus()
                    '    Else
                    '        txtNombreCliente.Focus()
                    '    End If
                    'Case 2 : txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text)
                    '    If txtVendedor.Text = "" Then
                    '        MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                    '        txtVendedor.Focus()
                    '    End If
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : ddlTipoPrecio.Focus() 'ddlRetencion.Focus()
                        Case 4 : txtNumeroFactura.Text = strUbicar : ExtraerFactura()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlTipoPrecio.SelectedIndexChanged
        Try
            If sender.name = "ddlRentencion" Then
                Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%"
                If sender.selectedindex = 0 Then
                    TextBox11.Text = "0.00"
                    TextBox12.Text = Format(ConvierteADouble(TextBox9.Text) + ConvierteADouble(TextBox10.Text) - ConvierteADouble(TextBox11.Text), "#,##0.#0")
                ElseIf sender.selectedindex = 1 Then
                    If intTipoFactura = 4 Or intTipoFactura = 8 Then
                        sender.selectedindex = 0
                    Else
                        TextBox11.Text = Format(ConvierteADouble(TextBox9.Text) * dblRetParam, "#,##0.#0")
                        TextBox12.Text = Format(ConvierteADouble(TextBox9.Text) + ConvierteADouble(TextBox10.Text) - ConvierteADouble(TextBox11.Text), "#,##0.#0")
                    End If
                End If
            ElseIf sender.name = "ComboBox2" And txtProducto.Text <> "" Then
                UbicarPrecio()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

        'txtNombreCliente.Text = StrConv(txtNombreCliente.Text, VbStrConv.ProperCase)

    End Sub

    Sub UbicarPendientes()

        Me.Cursor = Cursors.WaitCursor
        Try

            'Dim cmdTmp As New SqlCommand("sp_ExtraerFactPend", cnnAgro2K)
            'Dim prmTmp01 As New SqlParameter
            'Dim prmTmp02 As New SqlParameter

            'With prmTmp01
            '    .ParameterName = "@lngAgencia"
            '    .SqlDbType = SqlDbType.TinyInt
            '    .Value = lngRegAgencia
            'End With
            'With prmTmp02
            '    .ParameterName = "@intTipoFactura"
            '    .SqlDbType = SqlDbType.TinyInt
            '    .Value = intTipoFactura
            'End With
            'With cmdTmp
            '    .Parameters.Add(prmTmp01)
            '    .Parameters.Add(prmTmp02)
            '    .CommandType = CommandType.StoredProcedure
            'End With

            'If cmdTmp.Connection.State = ConnectionState.Open Then
            '    cmdTmp.Connection.Close()
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdTmp.Connection = cnnAgro2K

            'dtrAgro2K = cmdTmp.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNFacturas.UbicarPendientes(lngRegAgencia, intTipoFactura)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    'txtNumeroFactura.Text = dtrAgro2K.GetValue(0)
                    txtNumeroFactura.Text = dtrAgro2K.Item("numero")
                    Exit While
                End While
            End If
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            ExtraerFactura()
            'MSFlexGrid2.FormatString = "Cantidad   |<Unidad     |<Codigo     |<Producto                                                               |<Impuesto  |>Precio        |>Valor             |<|>|>|>"
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DPkFechaFactura.LostFocus
        Try
            If IndicaImprimir = 0 Then
                'If cnnAgro2K.State = ConnectionState.Open Then
                '    cnnAgro2K.Close()
                'End If
                'cnnAgro2K.Open()
                'cmdAgro2K.Connection = cnnAgro2K

                'strQuery = ""
                'strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(DPkFechaFactura.Value, "yyyyMMdd") & " "
                'cmdAgro2K.CommandText = strQuery
                'dtrAgro2K = cmdAgro2K.ExecuteReader
                'While dtrAgro2K.Read
                '    dblTipoCambio = dtrAgro2K.GetValue(0)
                'End While
                'dtrAgro2K.Close()
                'cmdAgro2K.Connection.Close()
                dblTipoCambio = RNTasaCambio.ObtieneTasaCambio(Format(DPkFechaFactura.Value, "yyyyMMdd"))
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox18_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProducto.DoubleClick
        Try
            intListadoAyuda = 0
            If MenuItem5.Visible = True Then
                intListadoAyuda = 1
            ElseIf MenuItem6.Visible = True Then
                intListadoAyuda = 2
            ElseIf MenuItem7.Visible = True Then
                intListadoAyuda = 3
            End If
            If intListadoAyuda <> 0 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click

        If intTipoFactura = 2 Or intTipoFactura = 5 Then
            UbicarPendientes()
        End If

    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged
        Try
            sSerie = ddlSerie.SelectedItem
            If sSerie Is Nothing Then
                sSerie = String.Empty
            End If

            If intTipoFactura = 1 Or intTipoFactura = 4 Then
                txtNumeroFactura.Text = ExtraerNumeroInventario(lngRegAgencia, sSerie)
                'If sSerie.ToString <> "" Then
                '    ActualizarNumeroInventario(lngRegAgencia, intTipoFactura, sSerie)
                'End If

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub txtCantidad_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCantidad.TextChanged

    End Sub

    Private Sub cmdOK_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            If dgvDetalle.Rows.Count <= 0 Then
                MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                Exit Sub
            End If
            Guardar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default


    End Sub

    Private Sub CmdAnular_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAnular.Click
        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            'If MSFlexGrid2.Rows <= 1 Then
            If dgvDetalle.Rows.Count <= 0 Then
                MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                Exit Sub
            End If
            intResp = MsgBox("�Est� Seguro de Anular esta Requisa?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Anulaci�n")
            If intResp = 6 Then
                Anular()
                MsgBox("Requisa ha sido Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Requisa")
                ddlSerie.Focus()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        Try

            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)

            'If MSFlexGrid2.Rows <= 1 Then
            If dgvDetalle.Rows.Count <= 0 Then
                MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                txtCantidad.Focus()
                Exit Sub
            End If
            Guardar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdiLimpiar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdiSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Sub EliminarRegistroGrid()
        Dim dblCantidad As Double = 0

        Try

            If dgvDetalle.Rows.Count > 0 AndAlso dgvDetalle.CurrentRow IsNot Nothing Then
                intResp = MsgBox("�Est� Seguro de Eliminar el Registro?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Eliminaci�n de Registros")

                If intResp = 6 Then

                    dblCantidad = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value)
                    TextBox9.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) - (dblCantidad * SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value)), "#,##0.#0")
                    If dgvDetalle.CurrentRow.Cells("Impuesto").Value = "Si" Then
                        TextBox10.Text = Format(SUConversiones.ConvierteADouble(TextBox10.Text) - Format((SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value) * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    End If
                    TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.#0")
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)

                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        Dim i As Integer = 0
        Dim TotalAnterior As Double = 0
        Dim ImpuestoAnterior As Double = 0

        Try
            '        If dgvDetalle.CurrentCell.ColumnIndex = 0 Then
            If dgvDetalle.CurrentRow IsNot Nothing Then
                If dgvDetalle.Columns.IndexOf(dgvDetalle.Columns("Cantidad")) = e.ColumnIndex Then
                    dgvDetalle.CurrentRow.Cells("Total").Value = dgvDetalle.CurrentRow.Cells("Cantidad").Value * dgvDetalle.CurrentRow.Cells("Valor").Value
                    TotalAnterior = 0
                    TextBox9.Text = 0
                    ImpuestoAnterior = 0
                    TextBox10.Text = 0
                    For i = 0 To dgvDetalle.Rows.Count - 1
                        TotalAnterior = ConvierteADouble(TextBox9.Text)
                        TextBox9.Text = Format(Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") + TotalAnterior, "#,##0.#0")
                        If intImpuesto = 0 Then
                            TextBox10.Text = Format(SUConversiones.ConvierteADouble(TextBox10.Text), "#,##0.#0")
                        ElseIf intImpuesto = 1 Then
                            ImpuestoAnterior = ConvierteADouble(TextBox10.Text)
                            TextBox10.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ImpuestoAnterior, "#,##0.#0")
                        End If

                        TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.#0")

                    Next
                    Limpiar2()
                    'ElseIf dgvDetalle.CurrentCell.ColumnIndex = 5 Then
                ElseIf dgvDetalle.Columns.IndexOf(dgvDetalle.Columns("Valor")) = e.ColumnIndex Then
                    dgvDetalle.CurrentRow.Cells("Total").Value = dgvDetalle.CurrentRow.Cells("Cantidad").Value * dgvDetalle.CurrentRow.Cells("Valor").Value
                    TotalAnterior = 0
                    TextBox9.Text = 0
                    ImpuestoAnterior = 0
                    TextBox10.Text = 0
                    For i = 0 To dgvDetalle.Rows.Count - 1
                        TotalAnterior = ConvierteADouble(TextBox9.Text)
                        TextBox9.Text = Format(Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") + TotalAnterior, "#,##0.#0")
                        If intImpuesto = 0 Then
                            TextBox10.Text = Format(SUConversiones.ConvierteADouble(TextBox10.Text), "#,##0.#0")
                        ElseIf intImpuesto = 1 Then
                            ImpuestoAnterior = ConvierteADouble(TextBox10.Text)
                            TextBox10.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Valor").Value) * SUConversiones.ConvierteADouble(dgvDetalle.Rows(i).Cells("Cantidad").Value)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + ImpuestoAnterior, "#,##0.#0")
                        End If

                        TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.#0")

                    Next
                    Limpiar2()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub dgvDetalle_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDetalle.DoubleClick
        Try
            If intTipoFactura = 1 Or intTipoFactura = 4 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
                EliminarRegistroGrid()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdImportPedidos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportPedidos.Click
        Try
            ObtenerArchivo()
            CargarComboPedidos()
            MessageBoxEx.Show(" Se Cargo exitosomente la Informacion de los Pedidos", "Elaborar Traslados", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Sub ObtenerArchivo()

        Dim dlgNew As New OpenFileDialog
        Dim strLine, strDrive As String

        Dim IdPedido As Long
        Dim NumeroPedido As String
        Dim NumFechaIng As Integer
        Dim FechaIngreso As String
        Dim dblImpuesto As Double
        Dim dblRetencion As Double
        Dim dblSubTotal As Double
        Dim dblTotal As Double
        Dim TipoCambio As Double
        Dim dblCantidad As Double
        Dim CodigoUnidad As String
        Dim strProdCodigo As String
        Dim strProdDescrip As String
        Dim dblIva As Double
        Dim bytAgenCodigo As Byte
        Dim intAgenciaDestino As Integer = 0
        Dim intRegDetalle As Integer = 0
        Dim strArchivo, strArchivo2, strClave, strComando As String
        Dim Columnas As String()

        Dim existe, er As Integer
        Dim strQueryVal As String = ""

        Try

            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MessageBoxEx.Show("No hay archivo que importar. Verifique", "Error en la Importaci�n", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            strDrive = ""
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "Pedidos_")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MessageBoxEx.Show("No es un archivo que contiene los Pedidos de una agencia. Verifique.", "Error en Archivo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            cbNumerosPedidos.Items.Clear()

            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            strNombAlmacenar = ""
            strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
                Exit Sub
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "truncate table tmpImportarPedidos"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            strNombArchivo = ""
            strNombArchivo = strArchivo2
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            intIncr = 0
            intIncr = InStrRev(strNombArchivo, "\")
            strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
            strQuery = ""

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            intRegDetalle = 0
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                Columnas = strLine.Trim().Split("|")
                intIncr = InStr(1, strLine, "|")

                IdPedido = 0
                IdPedido = ConvierteAInt(Columnas(0).Trim())
                NumeroPedido = ""
                NumeroPedido = Columnas(1).Trim()
                FechaIngreso = ""
                FechaIngreso = Columnas(2).Trim()
                NumFechaIng = 0
                NumFechaIng = SUConversiones.ConvierteAInt(Columnas(3).Trim())
                dblImpuesto = 0
                dblImpuesto = SUConversiones.ConvierteADouble(Columnas(4).Trim())
                dblRetencion = 0
                dblRetencion = SUConversiones.ConvierteADouble(Columnas(5).Trim())
                dblSubTotal = 0
                dblSubTotal = SUConversiones.ConvierteADouble(Columnas(6).Trim())
                dblTotal = 0
                dblTotal = SUConversiones.ConvierteADouble(Columnas(7).Trim())
                TipoCambio = 0
                TipoCambio = SUConversiones.ConvierteADouble(Columnas(8).Trim())
                dblCantidad = 0
                dblCantidad = SUConversiones.ConvierteADouble(Columnas(9).Trim())
                CodigoUnidad = ""
                CodigoUnidad = Columnas(10).Trim()
                strProdCodigo = ""
                strProdCodigo = Columnas(11).Trim()
                strProdDescrip = ""
                strProdDescrip = Columnas(12).Trim()
                dblIva = 0
                dblIva = SUConversiones.ConvierteADouble(Columnas(13).Trim())
                bytAgenCodigo = 0
                bytAgenCodigo = SUConversiones.ConvierteAInt(Columnas(14).Trim())

                intAgenciaDestino = SUConversiones.ConvierteADouble(Columnas(14).Trim())

                intRegDetalle = intRegDetalle + 1


                er = existe
                strQuery = ""
                strQuery = "insert into tmpImportarPedidos values (" & IdPedido & ", '" & NumeroPedido & "', '" & FechaIngreso & "', "
                strQuery = strQuery + "" & NumFechaIng & ", " & dblImpuesto & ", " & dblRetencion & ", "
                strQuery = strQuery + "" & dblSubTotal & ", " & dblTotal & ", " & TipoCambio & ", '" & strProdCodigo & "', "
                strQuery = strQuery + "'" & strProdDescrip & "', " & dblCantidad & ", " & bytAgenCodigo & ",0 ) "

                Try
                    cmdAgro2K.CommandText = strQuery
                    cmdAgro2K.ExecuteNonQuery()
                    intError = 0

                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try

            Loop
            sr.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub CargarComboPedidos()
        Try
            Me.Cursor = System.Windows.Forms.Cursors.Default
            cbNumerosPedidos.Items.Clear()
            'strQuery = ""
            'strQuery = "select distinct NumeroDocumento from tmpImportarPedidos where EstadoProcesado = 0 order by NumeroDocumento"
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNPedido.ObtienePedidosAImportar()
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    cbNumerosPedidos.Items.Add(dtrAgro2K.Item("NumeroDocumento"))
                End While
            End If
            dtrAgro2K = Nothing
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()


        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
    End Sub

    Private Sub cbNumerosPedidos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNumerosPedidos.SelectedIndexChanged
        Try
            dgvDetalle.Rows.Clear()
            CargarDetallePedido()
            MessageBoxEx.Show(" Se Cargo exitosomente la Informacion de los Pedidos", "Elaborar Traslados", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Sub CargarDetallePedido()
        Dim subT, Im, T As Double
        Dim IdA, fech As Integer
        Try

            dtrAgro2K = Nothing
            dtrAgro2K = RNInventario.ObtieneDetalleProductosPedidos(cbNumerosPedidos.Text)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    If (dgvDetalle.Rows.Count + 1) > 16 Then
                        MessageBoxEx.Show("Cantidad maxima para un Pedido es 16 Item", "Favor validar", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        DPkFechaFactura.Focus()
                        Exit Sub
                    End If

                    fech = dtrAgro2K.Item("NumFechaIng")
                    DPkFechaFactura.Value = DefinirFecha(fech)
                    DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")

                    dgvDetalle.Rows.Add(Format(dtrAgro2K.Item("Cantidad"), "#,##0.#0"), dtrAgro2K.Item("Unidad"), dtrAgro2K.Item("CadigoProducto"),
                                dtrAgro2K.Item("Descripcion"), IIf(dtrAgro2K.Item("Impuesto") > 0, "Si", "No"), Format(dtrAgro2K.Item("PrecioCostoCOR"), "#,##0.#0"),
                                Format(dtrAgro2K.Item("CostoTotal"), "#,##0.#0"), dtrAgro2K.Item("IdProducto"))

                    subT = subT + dtrAgro2K.Item("CostoTotal")
                    Im = Im + dtrAgro2K.Item("Impuesto")
                    T = subT + Im
                    IdA = ConvierteAInt(dtrAgro2K.Item("IdAgencia"))
                End While

            End If
            dtrAgro2K = Nothing
            TextBox9.Text = Format(subT, "#,##0.#0")
            TextBox10.Text = Format(Im, "#,##0.#0")
            TextBox11.Text = Format(0, "#,##0.#0")
            TextBox12.Text = Format(T, "#,##0.#0")

            If lngRegAgencia = IdA Then
                MessageBoxEx.Show("La Agencia Destino es la misma que La Agencia de Origen del Pedido", "Favor Verificar", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                cbeAgenciaDestino.SelectedValue = IdA
            End If
            'TextBox12.Text = Format(ConvierteADouble(TextBox9.Text) + ConvierteADouble(TextBox10.Text) - ConvierteADouble(TextBox11.Text), "#,##0.#0")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Private Sub cmdImprimirPedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimirPedido.Click
        Try

            If dgvDetalle.Rows.Count = 0 Then
                MessageBoxEx.Show("No hay Detalle de Pedido a Imprimir..", "Imprimir Pedidos", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                If PrintDialogo.ShowDialog = Windows.Forms.DialogResult.OK Then
                    PrintDoc.PrinterSettings = PrintDialogo.PrinterSettings
                    PrintDoc.Print()
                Else
                    imprimir(True)
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDoc.PrintPage
        ' Este evento se produce cada vez que se va a imprimir una p�gina
        Dim lineHeight As Single
        Dim yPos As Single = e.MarginBounds.Top
        Dim leftMargin As Single = e.MarginBounds.Left
        Dim printFont As System.Drawing.Font
        Dim sb As System.Text.StringBuilder
        ' Dim lvi As ListViewItem
        Dim anchoColumnasImp() As Integer

        Try


            'Dim anchoTotal As Integer
            '
            ' Asignar el tipo de letra
            'printFont = New System.Drawing.Font("Courier New", 11)
            prtFont = New System.Drawing.Font("Courier New", 11)
            printFont = prtFont
            lineHeight = printFont.GetHeight(e.Graphics)
            '
            ' imprimir la cabecera de la p�gina
            yPos += lineHeight
            e.Graphics.DrawString("Pedido: " & cbNumerosPedidos.Text, printFont, Brushes.Black, leftMargin, yPos)

            yPos += lineHeight + 5
            e.Graphics.DrawString("Sucursal Destino: " & cbeAgenciaDestino.Text, printFont, Brushes.Black, leftMargin, yPos)

            lineaActual = 0
            ReDim anchoColumnasImp(3)
            ' el n�mero total de caracteres a mostrar
            ' (incluyendo uno de separaci�n)

            anchoColumnasImp(0) = 10
            anchoColumnasImp(1) = 8
            anchoColumnasImp(2) = 8
            anchoColumnasImp(3) = 200

            sb = New System.Text.StringBuilder
            For i As Integer = 0 To 3 'dgvDetalle.ColumnCount - 1
                sb.AppendFormat("{0} ", ajustar(dgvDetalle.Columns(i).HeaderText, anchoColumnasImp(i), HorizontalAlignment.Left))
            Next
            yPos += lineHeight + 20
            e.Graphics.DrawString(sb.ToString, printFont, Brushes.Black, leftMargin, yPos)
            yPos += 5

            ' imprimir cada una de las l�neas de esta p�gina
            Do
                yPos += lineHeight
                sb = New System.Text.StringBuilder

                For j As Integer = 0 To 3 'dgvDetalle.Rows.Count - 1
                    sb.AppendFormat("{0} ", ajustar(dgvDetalle.Rows(lineaActual).Cells(j).Value, anchoColumnasImp(j), HorizontalAlignment.Left))
                Next
                e.Graphics.DrawString(sb.ToString, printFont, Brushes.Black, leftMargin, yPos)
                lineaActual += 1
            Loop Until yPos >= e.MarginBounds.Bottom OrElse lineaActual >= Me.dgvDetalle.Rows.Count
            '
            If lineaActual < Me.dgvDetalle.Rows.Count Then
                e.HasMorePages = True
            Else
                e.HasMorePages = False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub imprimir(ByVal esPreview As Boolean)
        ' imprimir o mostrar el PrintPreview
        Dim prtDoc As PrintDocument = Nothing
        Dim prtSettings As PrinterSettings = Nothing

        Try


            If prtSettings Is Nothing Then
                prtSettings = New PrinterSettings
            End If

            If prtDoc Is Nothing Then
                prtDoc = New System.Drawing.Printing.PrintDocument
                AddHandler prtDoc.PrintPage, AddressOf PrintDoc_PrintPage
            End If

            ' la l�nea actual
            lineaActual = 0

            ' la configuraci�n a usar en la impresi�n
            prtDoc.PrinterSettings = prtSettings

            If esPreview Then
                Dim prtPrev As New PrintPreviewDialog
                prtPrev.Document = prtDoc

                prtPrev.Text = "Previsualizar documento"
                prtPrev.ShowDialog()
            Else
                prtDoc.Print()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
    End Sub

    Private Sub cbeAgenciasOrigen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbeAgenciasOrigen.SelectedIndexChanged

    End Sub

    Private Sub txtAgenciaOrigen_TextChanged(sender As Object, e As EventArgs) Handles txtAgenciaOrigen.TextChanged

    End Sub

    Private Function ajustar(ByVal cadena As String, ByVal ancho As Integer, ByVal alinear As HorizontalAlignment) As String
        Try
            ' devuelve una cadena con el ancho indicado
            If cadena = Nothing OrElse cadena.Length = 0 Then
                Return New String(" "c, ancho)
            End If
            '
            If alinear = HorizontalAlignment.Right Then
                If ancho > cadena.Length Then cadena = New String(" "c, ancho - cadena.Length) & cadena
                Return cadena.Substring(cadena.Length - ancho, ancho)
            Else
                If ancho > cadena.Length Then cadena &= New String(" "c, ancho - cadena.Length)
                Return cadena.Substring(0, ancho)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Hubo un error al momento de realizar la accion: " + ex.Message.ToString(), " Traslado Inventario ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
    End Function

End Class
