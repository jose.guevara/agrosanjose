﻿Imports System.Windows.Forms
'Imports Microsoft.Reporting.WinForms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Entidades

Public Class Frm_RPT_AsientosDescuadres
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPT_AsientosDescuadres"

    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load
        Dim i As Integer = 0
        Dim val_max As Integer = 0
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim sqlconexion As SqlConnection = Nothing 'conexión
        REM declaramos las tablas del dataset***DATAADAPTERS

        Dim DAMasientosDescua As SqlDataAdapter = Nothing
        'Dim dset_AsientosDescua As New DS_AsientosDescua
        'Dim dset_AsientosDescua As DS_AsientosDescua = Nothing
        Dim dset_AsientosDescua As DataSet = Nothing
        Dim sql_MasientosDescua As String = String.Empty
        Dim report_asientodescuadres As RPT_AsientosDescuadres = Nothing
        Try
            dset_AsientosDescua = New DataSet
            val_max = SIMF_CONTA.numeformulas
            sqlconexion = New SqlConnection(SIMF_CONTA.ConnectionStringconta)
            sql_MasientosDescua = "SELECT numeasiento, fechaasiento, descasiento, debelocal, haberlocal, anomes, numemov, debelocal - haberlocal AS Descuadre"
            sql_MasientosDescua += "  FROM Masientos WHERE (debelocal - haberlocal) <> 0"

            If SIMF_CONTA.filtro_reporte <> "" Then
                sql_MasientosDescua += " and  " & SIMF_CONTA.filtro_reporte
            End If
            'Dim ddd As New DataSet
            'ddd = jackconsulta.retornaDataset(sql_MasientosDescua, True)
            DAMasientosDescua = New SqlDataAdapter(sql_MasientosDescua, sqlconexion)

            DAMasientosDescua.Fill(dset_AsientosDescua, "Masientos")
            ' instanciar el objeto informe
            report_asientodescuadres = New RPT_AsientosDescuadres
            'report_asientodescuadres.SetDatabaseLogon(SIMF_CONTA.usuario, SIMF_CONTA.pws, SIMF_CONTA.server, SIMF_CONTA.base)
            report_asientodescuadres.SetDataSource(dset_AsientosDescua)
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_asientodescuadres.SetParameterValue(nombreformula, valor)
                End If
            Next
            ' establecer la fórmula de selección de registros
            report_asientodescuadres.RecordSelectionFormula = "" '"{dasientos.numeasiento} = 'CD21'"
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_asientodescuadres
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("CrystalReportViewer1_Load - Error: " + ex.Message.ToString(), "Frm_RPTAsientos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class