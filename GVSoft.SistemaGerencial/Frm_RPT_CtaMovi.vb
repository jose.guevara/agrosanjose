﻿Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar

Public Class Frm_RPT_CtaMovi
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPT_CtaMovi"
    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load
        'Dim param As New ParameterField()
        'param.ParameterFieldName = "saldoinicial"
        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty


        Dim sqlconexion As SqlConnection = Nothing
        REM declaramos las tablas del dataset***DATAADAPTERS
        'Dim DAcatalogoconta As SqlDataAdapter
        Dim DADasientos As SqlDataAdapter = Nothing
        Dim DAMasientos As SqlDataAdapter = Nothing

        Dim dset_CtaMovi As DataSet = Nothing
        Dim sql_Dasientos As String = String.Empty
        Dim sql_Masientos As String = String.Empty
        Dim dtDatos As DataTable = Nothing
        Dim report_CtaMovi As RPT_CtaMovi = Nothing
        Try
            dset_CtaMovi = New DataSet
            dtDatos = RNReporte.ObtieneReportexCuenta(SIMF_CONTA.formula(5).ToString(), SIMF_CONTA.formula(6).ToString(), SIMF_CONTA.formula(3).ToString(), SIMF_CONTA.formula(4).ToString())
            dtDatos.TableName = "Dasientos"
            dset_CtaMovi.Tables.Add(dtDatos.Copy())
            'dtDatos = Nothing
            'dtDatos = New DataTable
            'dtDatos.TableName = "Masientos"
            'dset_CtaMovi.Tables.Add(dtDatos.Copy())
            'sqlconexion = New SqlConnection(SIMF_CONTA.ConnectionStringconta)
            'Dim sql_catalogoconta As String = "select * from catalogoconta"
            'Dim sql_Dasientos As String = "Select * From Dasientos where fechadoc >='" & SIMF_CONTA.formula(3) & "' and  fechadoc<='" & SIMF_CONTA.formula(4) & "' order by isnull(fechadoc,''),numeasiento"
            'sql_Dasientos = "Select * From Dasientos order by isnull(fechadoc,''),numeasiento"
            'sql_Masientos = "Select * From Masientos"

            ''DAcatalogoconta = New SqlDataAdapter(sql_catalogoconta, sqlconexion)
            'DADasientos = New SqlDataAdapter(sql_Dasientos, sqlconexion)
            'DAMasientos = New SqlDataAdapter(sql_Masientos, sqlconexion)

            '' DAcatalogoconta.Fill(dset_CtaMovi, "catalogoconta")
            'DADasientos.Fill(dset_CtaMovi, "Dasientos")
            'DAMasientos.Fill(dset_CtaMovi, "Masientos")

            'report_CtaMovi = New RPT_CtaMovi
            report_CtaMovi = New RPT_CtaMovi
            report_CtaMovi.SetDataSource(dset_CtaMovi)
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_CtaMovi.SetParameterValue(nombreformula, valor)
                End If
            Next
            'report_CtaMovi.DataDefinition.ParameterFields = crystal_jack.lista_parametros
            'report_CtaMovi.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_CtaMovi
            Me.CrystalReportViewer1.Refresh()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("CrystalReportViewer1_Load - Error: " + ex.Message.ToString(), "Frm_RPT_DiarioGeneral", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Frm_RPT_CtaMovi_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class