Imports System.Data.SqlClient
Imports System.Text
Imports DevComponents.DotNetBar.Controls
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Public Class frmListadoAyuda
    Inherits DevComponents.DotNetBar.Office2007Form
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreSeguridad As String = "frmListadoAyuda"
    Private Shared gdtListadoAyuda As DataTable = Nothing
    'Inherits System.Windows.Forms.Form
    Dim lnTipoBusqueda As Integer = 0
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    'Friend WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ugbBusqueda As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents dgvBusqueda As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox

    'Propiedades para grid
    Friend WithEvents Cantidad As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents Saldo As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PrecioCostoCOR As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PrecioCostoUSD As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PrecioPublicoCOR As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PrecioPublicoUSD As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PrecioDistribuidorCOR As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PrecioDistribuidorUSD As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cuenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Unidad As System.Windows.Forms.DataGridViewTextBoxColumn

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListadoAyuda))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.ugbBusqueda = New Infragistics.Win.Misc.UltraGroupBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.dgvBusqueda = New DevComponents.DotNetBar.Controls.DataGridViewX()
        CType(Me.ugbBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugbBusqueda.SuspendLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.dgvBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(338, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 16)
        Me.Label1.TabIndex = 54
        Me.Label1.Text = "C�digo:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(389, 26)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(104, 20)
        Me.txtCodigo.TabIndex = 2
        '
        'ugbBusqueda
        '
        Me.ugbBusqueda.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.ugbBusqueda.Controls.Add(Me.txtDescripcion)
        Me.ugbBusqueda.Controls.Add(Me.Label2)
        Me.ugbBusqueda.Controls.Add(Me.txtCodigo)
        Me.ugbBusqueda.Controls.Add(Me.Label1)
        Me.ugbBusqueda.Dock = System.Windows.Forms.DockStyle.Top
        Me.ugbBusqueda.Location = New System.Drawing.Point(0, 0)
        Me.ugbBusqueda.Name = "ugbBusqueda"
        Me.ugbBusqueda.Size = New System.Drawing.Size(743, 55)
        Me.ugbBusqueda.TabIndex = 56
        Me.ugbBusqueda.Text = "Busqueda"
        Me.ugbBusqueda.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(83, 26)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(248, 20)
        Me.txtDescripcion.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(2, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 16)
        Me.Label2.TabIndex = 56
        Me.Label2.Text = "Descripci�n:"
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox1.Controls.Add(Me.dgvBusqueda)
        Me.UltraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox1.Location = New System.Drawing.Point(0, 55)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(743, 203)
        Me.UltraGroupBox1.TabIndex = 57
        Me.UltraGroupBox1.Text = "Detalle"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgvBusqueda
        '
        Me.dgvBusqueda.AllowUserToAddRows = False
        Me.dgvBusqueda.AllowUserToDeleteRows = False
        Me.dgvBusqueda.AllowUserToResizeColumns = False
        Me.dgvBusqueda.AllowUserToResizeRows = False
        Me.dgvBusqueda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvBusqueda.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvBusqueda.BackgroundColor = System.Drawing.SystemColors.ControlLightLight
        Me.dgvBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBusqueda.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBusqueda.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvBusqueda.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvBusqueda.Location = New System.Drawing.Point(3, 20)
        Me.dgvBusqueda.Name = "dgvBusqueda"
        Me.dgvBusqueda.ReadOnly = True
        Me.dgvBusqueda.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.dgvBusqueda.Size = New System.Drawing.Size(737, 177)
        Me.dgvBusqueda.TabIndex = 0
        '
        'frmListadoAyuda
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(743, 258)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.ugbBusqueda)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmListadoAyuda"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmListadoAyuda"
        CType(Me.ugbBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugbBusqueda.ResumeLayout(False)
        Me.ugbBusqueda.PerformLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        CType(Me.dgvBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmListadoAyuda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Limpiar()
        'CreaColumnasGrid()
        gdtListadoAyuda = Nothing
        'gdtListadoAyuda = RNBusqueda.ObtieneDatosListadoAyuda(intListadoAyuda, 0, String.Empty, String.Empty, lngRegAgencia)
        'dgvBusqueda.DataSource = gdtListadoAyuda

    End Sub

    Private Sub frmListadoAyuda_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        blnUbicar = True

    End Sub

    Private Sub frmListadoAyuda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Sub Limpiar()

        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        'txtCodigo.Focus()
        txtDescripcion.Focus()
        'txtCodigo.SelectAll()
        txtDescripcion.SelectAll()
        strUbicar = ""

    End Sub

    'Private Sub CreaColumnasGrid()
    '    Select Case intListadoAyuda
    '        Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
    '            Me.Codigo = New DataGridViewTextBoxColumn
    '            Me.Nombre = New DataGridViewTextBoxColumn
    '            Me.dgvBusqueda.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Nombre})

    '            Me.Codigo.HeaderText = "C�digo"
    '            Me.Codigo.Name = "Codigo"
    '            Me.Codigo.ReadOnly = True
    '            Me.Codigo.Width = 150

    '            Me.Nombre.HeaderText = "Descripci�n"
    '            Me.Nombre.Name = "Nombre"
    '            Me.Nombre.ReadOnly = True
    '            Me.Nombre.Width = 470

    '        Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
    '            Me.Codigo = New DataGridViewTextBoxColumn
    '            Me.Nombre = New DataGridViewTextBoxColumn
    '            Me.Saldo = New DataGridViewDoubleInputColumn
    '            Me.dgvBusqueda.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Nombre, Me.Saldo})

    '            Me.Codigo.HeaderText = "C�digo"
    '            Me.Codigo.Name = "Codigo"
    '            Me.Codigo.ReadOnly = True
    '            Me.Codigo.Width = 150

    '            Me.Nombre.HeaderText = "Nombre"
    '            Me.Nombre.Name = "Nombre"
    '            Me.Nombre.ReadOnly = True
    '            Me.Nombre.Width = 320

    '            Me.Saldo.HeaderText = "Saldo"
    '            Me.Saldo.Name = "Saldo"
    '            Me.Saldo.ReadOnly = True
    '            Me.Saldo.Width = 150

    '        Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
    '            Me.Codigo = New DataGridViewTextBoxColumn
    '            Me.Descripcion = New DataGridViewTextBoxColumn
    '            Me.Unidad = New DataGridViewTextBoxColumn
    '            Me.Cantidad = New DataGridViewIntegerInputColumn
    '            Me.PrecioCostoCOR = New DataGridViewDoubleInputColumn
    '            Me.PrecioCostoUSD = New DataGridViewDoubleInputColumn
    '            Me.PrecioPublicoCOR = New DataGridViewDoubleInputColumn
    '            Me.PrecioPublicoUSD = New DataGridViewDoubleInputColumn
    '            Me.PrecioDistribuidorCOR = New DataGridViewDoubleInputColumn
    '            Me.PrecioDistribuidorUSD = New DataGridViewDoubleInputColumn

    '            Me.dgvBusqueda.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, _
    '                                                                                           Me.Unidad, Me.Cantidad, Me.PrecioCostoCOR, _
    '                                                                                           Me.PrecioCostoUSD, Me.PrecioPublicoCOR, Me.PrecioDistribuidorCOR, _
    '                                                                                           Me.PrecioPublicoUSD, Me.PrecioDistribuidorUSD})

    '            Me.Codigo.HeaderText = "C�digo"
    '            Me.Codigo.Name = "Codigo"
    '            Me.Codigo.ReadOnly = True
    '            Me.Codigo.Width = 100

    '            Me.Descripcion.HeaderText = "Descripci�n"
    '            Me.Descripcion.Name = "descrip"
    '            Me.Descripcion.ReadOnly = True
    '            Me.Descripcion.Width = 200

    '            Me.Unidad.HeaderText = "Unidad"
    '            Me.Unidad.Name = "unidad"
    '            Me.Unidad.ReadOnly = True
    '            Me.Unidad.Width = 50

    '            Me.Cantidad.HeaderText = "Cantidad"
    '            Me.Cantidad.Name = "cantid"
    '            Me.Cantidad.ReadOnly = True
    '            Me.Cantidad.Width = 50

    '            Me.PrecioCostoCOR.HeaderText = "Precio Costo C$"
    '            Me.PrecioCostoCOR.Name = "PrecioCostoCOR"
    '            Me.PrecioCostoCOR.ReadOnly = True
    '            Me.PrecioCostoCOR.Width = 80

    '            Me.PrecioCostoUSD.HeaderText = "Precio Costo US$"
    '            Me.PrecioCostoUSD.Name = "PrecioCostoUSD"
    '            Me.PrecioCostoUSD.ReadOnly = True
    '            Me.PrecioCostoUSD.Width = 80

    '            Me.PrecioPublicoCOR.HeaderText = "Publico C$"
    '            Me.PrecioPublicoCOR.Name = "pvpc"
    '            Me.PrecioPublicoCOR.ReadOnly = True
    '            Me.PrecioPublicoCOR.Width = 80

    '            Me.PrecioDistribuidorCOR.HeaderText = "Distribuidor C$"
    '            Me.PrecioDistribuidorCOR.Name = "pvdc"
    '            Me.PrecioDistribuidorCOR.ReadOnly = True
    '            Me.PrecioDistribuidorCOR.Width = 80

    '            Me.PrecioPublicoUSD.HeaderText = "Publico US$"
    '            Me.PrecioPublicoUSD.Name = "pvpu"
    '            Me.PrecioPublicoUSD.ReadOnly = True
    '            Me.PrecioPublicoUSD.Width = 80

    '            Me.PrecioDistribuidorUSD.HeaderText = "Distribuidor US$"
    '            Me.PrecioDistribuidorUSD.Name = "pvdu"
    '            Me.PrecioDistribuidorUSD.ReadOnly = True
    '            Me.PrecioDistribuidorUSD.Width = 80

    '    End Select
    'End Sub

    Private Sub IndiicaValorBusqueda()
        If txtCodigo.Text.Trim.Length <= 0 And txtDescripcion.Text.Trim.Length <= 0 Then
            lnTipoBusqueda = 0
        End If
        If txtCodigo.Text.Trim.Length > 0 Then
            lnTipoBusqueda = 1
        End If
        If txtCodigo.Text.Trim.Length <= 0 And txtDescripcion.Text.Trim.Length > 0 Then
            lnTipoBusqueda = 2
        End If
        If txtCodigo.Text.Trim.Length > 0 And txtDescripcion.Text.Trim.Length > 0 Then
            lnTipoBusqueda = 1
        End If
    End Sub

    Sub Iniciar()

        Dim strComando As String = String.Empty
        Dim dtDatosBusqueda As DataTable = Nothing
        Dim cmdTmp As SqlCommand = Nothing
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Try


            'Dim cmdTmp As New SqlCommand("sp_DetalleProductoFactura", cnnAgro2K)
            'Dim prmTmp01 As New SqlParameter
            'Dim prmTmp02 As New SqlParameter

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            'MSFlexGrid2.Cols = 2
            'MSFlexGrid2.Rows = 1

            strQuery = ""
            txtCodigo.Text = Trim(txtCodigo.Text)
            IndiicaValorBusqueda()
            dtDatosBusqueda = RNBusqueda.ObtieneDatosListadoAyuda(intListadoAyuda, lnTipoBusqueda, txtCodigo.Text, txtDescripcion.Text, lngRegAgencia, lngRegUsuario)
            'Select Case lnTipoBusqueda
            '    Case 0
            '        Select Case intListadoAyuda
            '            Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
            '                strQuery = "Select Codigo, Nombre From prm_Vendedores Where Estado = 0 Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
            '                strQuery = "Select Codigo, Nombre, isnull(Saldo,0) Saldo From prm_Clientes Where (Nombre Not Like '%Cerrado%' And Nombre Not Like '%Inactiv%') Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
            '                MsgBox("Tiene que digitar parte de la descripci�n del producto.", MsgBoxStyle.Information, "Ingrese Dato")
            '                Exit Sub
            '            Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
            '                MsgBox("Tiene que digitar parte de la descripci�n de la cuenta contable.", MsgBoxStyle.Information, "Ingrese Dato")
            '                Exit Sub
            '            Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
            '                strQuery = "Select Codigo, Descripcion From prm_CtasBancarias Where Estado = 0 Order By Descripcion"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
            '                strQuery = "Select Codigo, Nombre From prm_Beneficiarios Where Estado = 0 Order By Nombre"
            '        End Select
            '    Case 1
            '        Select Case intListadoAyuda
            '            Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
            '                strQuery = "Select Codigo, Nombre From prm_Vendedores Where Estado = 0 And Codigo Like '" & txtCodigo.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
            '                strQuery = "Select Codigo, Nombre, Saldo From prm_Clientes Where Codigo Like '" & txtCodigo.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
            '                strComando = "BuscaProductoFacturaxCodigo"
            '                cmdTmp = New SqlCommand(strComando, cnnAgro2K)

            '                Me.Cursor = Cursors.WaitCursor
            '                With prmTmp01
            '                    .ParameterName = "@strCodigo"
            '                    .SqlDbType = SqlDbType.VarChar
            '                    .Value = "%" & txtCodigo.Text & "%"
            '                End With
            '                With prmTmp02
            '                    .ParameterName = "@intAgencia"
            '                    .SqlDbType = SqlDbType.TinyInt
            '                    .Value = lngRegAgencia
            '                End With
            '                With cmdTmp
            '                    .Parameters.Add(prmTmp01)
            '                    .Parameters.Add(prmTmp02)
            '                    .CommandType = CommandType.StoredProcedure
            '                End With
            '            Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
            '                strQuery = "Select Cuenta, Descripcion From prm_CtasContables Where "
            '                strQuery = strQuery + "Cuenta Like '" & txtCodigo.Text & "%' Order By Cuenta"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
            '                strQuery = "Select Codigo, Descripcion From prm_CtasBancarias Where Estado = 0 And "
            '                strQuery = strQuery + " Codigo Like '" & txtCodigo.Text & "%' Order By Codigo"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
            '                strQuery = "Select Codigo, Nombre From prm_Beneficiarios Where Estado = 0 And "
            '                strQuery = strQuery + " Codigo Like '" & txtCodigo.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
            '                strQuery = "Select Codigo, Nombre, Saldo From prm_proveedores Where Codigo Like '" & txtCodigo.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_AGENCIAS
            '                strQuery = "Select Codigo, descripcion From prm_agencias Where Codigo Like '" & txtCodigo.Text & "%' Order By descripcion"
            '        End Select
            '    Case 2
            '        Select Case intListadoAyuda
            '            Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
            '                strQuery = "Select Codigo, Nombre From prm_Vendedores Where Estado = 0 And Nombre Like '" & txtDescripcion.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
            '                strQuery = "Select Codigo, Nombre, Saldo From prm_Clientes Where Nombre Like '" & txtDescripcion.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
            '                strComando = "sp_DetalleProductoFactura"
            '                cmdTmp = New SqlCommand(strComando, cnnAgro2K)
            '                Me.Cursor = Cursors.WaitCursor
            '                With prmTmp01
            '                    .ParameterName = "@strDescrip"
            '                    .SqlDbType = SqlDbType.VarChar
            '                    .Value = "%" & txtDescripcion.Text & "%"
            '                End With
            '                With prmTmp02
            '                    .ParameterName = "@intAgencia"
            '                    .SqlDbType = SqlDbType.TinyInt
            '                    .Value = lngRegAgencia
            '                End With
            '                With cmdTmp
            '                    .Parameters.Add(prmTmp01)
            '                    .Parameters.Add(prmTmp02)
            '                    .CommandType = CommandType.StoredProcedure
            '                End With
            '            Case ENTListadoAyuda.LISTADO_CUENTA_CONTABLE
            '                strQuery = "Select Cuenta, Descripcion From prm_CtasContables Where "
            '                strQuery = strQuery + "Descripcion Like '" & txtDescripcion.Text & "%' Order By Cuenta"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_BANCARIA
            '                strQuery = "Select Codigo, Descripcion From prm_CtasBancarias Where Estado = 0 And "
            '                strQuery = strQuery + " Descripcion Like '" & txtDescripcion.Text & "%' Order By Codigo"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS
            '                strQuery = "Select Codigo, Nombre From prm_Beneficiarios Where Estado = 0 And "
            '                strQuery = strQuery + " Nombre Like '" & txtDescripcion.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
            '                strQuery = "Select Codigo, Nombre, Saldo From prm_proveedores Where Nombre Like '" & txtDescripcion.Text & "%' Order By Nombre"
            '            Case ENTListadoAyuda.LISTADO_CUENTA_AGENCIAS
            '                strQuery = "Select Codigo, descripcion From prm_agencias Where descripcion Like '" & txtDescripcion.Text & "%' Order By descripcion"
            '        End Select
            'End Select

            'If TextBox1.Text = "" Then
            '    If intListadoAyuda = 1 Then
            '        strQuery = "Select Codigo, Nombre From prm_Vendedores Where Estado = 0 Order By Nombre"
            '    ElseIf intListadoAyuda = 2 Then
            '        strQuery = "Select Codigo, Nombre, isnull(Saldo,0) Saldo From prm_Clientes Where (Nombre Not Like '%Cerrado%' And Nombre Not Like '%Inactiv%') Order By Nombre"
            '        'MSFlexGrid2.Cols = 3
            '    ElseIf intListadoAyuda = 3 Then
            '        MsgBox("Tiene que digitar parte de la descripci�n del producto.", MsgBoxStyle.Information, "Ingrese Dato")
            '        Exit Sub
            '    ElseIf intListadoAyuda = 4 Then
            '        MsgBox("Tiene que digitar parte de la descripci�n de la cuenta contable.", MsgBoxStyle.Information, "Ingrese Dato")
            '        Exit Sub
            '    ElseIf intListadoAyuda = 5 Then
            '        strQuery = "Select Codigo, Descripcion From prm_CtasBancarias Where Estado = 0 Order By Descripcion"
            '    ElseIf intListadoAyuda = 6 Then
            '        strQuery = "Select Codigo, Nombre From prm_Beneficiarios Where Estado = 0 Order By Nombre"
            '    End If
            'ElseIf TextBox1.Text <> "" Then
            '    If intListadoAyuda = 1 Then
            '        strQuery = "Select Codigo, Nombre From prm_Vendedores Where Estado = 0 And Nombre Like '" & TextBox1.Text & "%' Order By Nombre"
            '    ElseIf intListadoAyuda = 2 Then
            '        strQuery = "Select Codigo, Nombre, Saldo From prm_Clientes Where Codigo Like '" & TextBox1.Text & "%' Order By Nombre"

            '    End If
            '    'MSFlexGrid2.Cols = 3
            'ElseIf intListadoAyuda = 3 Then
            '    Me.Cursor = Cursors.WaitCursor
            '    With prmTmp01
            '        .ParameterName = "@strDescrip"
            '        .SqlDbType = SqlDbType.VarChar
            '        .Value = "%" & TextBox1.Text & "%"
            '    End With
            '    With prmTmp02
            '        .ParameterName = "@intAgencia"
            '        .SqlDbType = SqlDbType.TinyInt
            '        .Value = lngRegAgencia
            '    End With
            '    With cmdTmp
            '        .Parameters.Add(prmTmp01)
            '        .Parameters.Add(prmTmp02)
            '        .CommandType = CommandType.StoredProcedure
            '    End With
            '    'MSFlexGrid2.Cols = 10
            'ElseIf intListadoAyuda = 4 Then
            '    strQuery = "Select Cuenta, Descripcion From prm_CtasContables Where "
            '    strQuery = strQuery + "Descripcion Like '" & TextBox1.Text & "%' Order By Cuenta"
            'ElseIf intListadoAyuda = 5 Then
            '    strQuery = "Select Codigo, Descripcion From prm_CtasBancarias Where Estado = 0 And "
            '    strQuery = strQuery + " Descripcion Like '" & TextBox1.Text & "%' Order By Codigo"
            'ElseIf intListadoAyuda = 6 Then
            '    strQuery = "Select Codigo, Nombre From prm_Beneficiarios Where Estado = 0 And "
            '    strQuery = strQuery + " Nombre Like '" & TextBox1.Text & "%' Order By Nombre"
            'ElseIf intListadoAyuda = 7 Then
            '    strQuery = "Select Codigo, Nombre, Saldo From prm_proveedores Where Nombre Like '" & TextBox1.Text & "%' Order By Nombre"
            '    'MSFlexGrid2.Cols = 3
            'ElseIf intListadoAyuda = 8 Then
            '    strQuery = "Select Codigo, descripcion From prm_agencias Where descripcion Like '" & TextBox1.Text & "%' Order By descripcion"
            'End If
            'MSFlexGrid2.Clear()
            If dgvBusqueda.RowCount > 0 Then
                dgvBusqueda.Rows.Clear()
            End If
            dgvBusqueda.DataSource = dtDatosBusqueda
            'Try
            '    If cnnAgro2K.State = ConnectionState.Open Then
            '        cnnAgro2K.Close()
            '    End If
            '    If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '        cmdAgro2K.Connection.Close()
            '    End If
            '    cnnAgro2K.Open()
            '    cmdAgro2K.Connection = cnnAgro2K
            '    If intListadoAyuda <> 3 Then
            '        cmdAgro2K.CommandText = strQuery
            '        dtrAgro2K = cmdAgro2K.ExecuteReader
            '    Else
            '        dtrAgro2K = cmdTmp.ExecuteReader
            '    End If
            '    While dtrAgro2K.Read
            '        'MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
            '        'MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
            '        'MSFlexGrid2.Col = 0

            '        'MSFlexGrid2.Text = dtrAgro2K.GetValue(0)
            '        'MSFlexGrid2.Col = 1
            '        'MSFlexGrid2.Text = dtrAgro2K.GetValue(1)
            '        Select Case intListadoAyuda
            '            Case ENTListadoAyuda.LISTADO_AYUDA_VENDEDORES
            '                dgvBusqueda.Rows.Add(dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("Nombre"))
            '            Case ENTListadoAyuda.LISTADO_AYUDA_CLIENTES
            '                dgvBusqueda.Rows.Add(dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("Nombre"), dtrAgro2K.Item("Saldo"))
            '            Case ENTListadoAyuda.LISTADO_AYUDA_PRODUCTO
            '                dgvBusqueda.Rows.Add(dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("descrip"), dtrAgro2K.Item("unidad"), _
            '                                     dtrAgro2K.Item("cantid"), dtrAgro2K.Item("PrecioCostoCOR"), dtrAgro2K.Item("PrecioCostoUSD"), _
            '                                     dtrAgro2K.Item("pvpc"), dtrAgro2K.Item("pvdc"), dtrAgro2K.Item("pvpu"), dtrAgro2K.Item("pvdu"))
            '            Case ENTListadoAyuda.LISTADO_CUENTA_PROVEEDORES
            '                dgvBusqueda.Rows.Add(dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("Nombre"), dtrAgro2K.Item("Saldo"))
            '        End Select

            '        'If intListadoAyuda = 2 Then
            '        '    MSFlexGrid2.Col = 2
            '        '    MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(2), "#,##0.#0")
            '        'ElseIf intListadoAyuda = 3 Then
            '        '    MSFlexGrid2.Col = 2
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(2)
            '        '    MSFlexGrid2.Col = 3
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(3)
            '        '    MSFlexGrid2.Col = 4
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(4)
            '        '    MSFlexGrid2.Col = 5
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(5)
            '        '    MSFlexGrid2.Col = 6
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(6)
            '        '    MSFlexGrid2.Col = 7
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(7)
            '        '    MSFlexGrid2.Col = 8
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(8)
            '        '    MSFlexGrid2.Col = 9
            '        '    MSFlexGrid2.Text = dtrAgro2K.GetValue(9)
            '        'ElseIf intListadoAyuda = 7 Then
            '        '    MSFlexGrid2.Col = 2
            '        '    MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(2), "#,##0.#0")
            '        'End If
            '    End While
            'Catch exc As Exception
            '    exc.Message.ToString()
            'Finally
            '    If cnnAgro2K.State = ConnectionState.Open Then
            '        cnnAgro2K.Close()
            '    End If
            '    'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    '    cmdAgro2K.Connection.Close()
            '    'End If
            '    'cnnAgro2K.Close()
            'End Try
            Me.Cursor = System.Windows.Forms.Cursors.Default
            'If intListadoAyuda = 1 Then
            '    MSFlexGrid2.FormatString = "<Codigo      |<Nombre                                                                  "
            'ElseIf intListadoAyuda = 2 Then
            '    MSFlexGrid2.FormatString = "<Codigo      |<Nombre                                                                                              |>Saldo             "
            'ElseIf intListadoAyuda = 3 Then
            '    MSFlexGrid2.FormatString = "<Codigo      |<Descripci�n                                              |<Unidad    |>Cantidad  |>Precio Costo C$  |>Precio Costo U$  |>P�blico C$  |>Distrib. C$  |>P�blico U$  |>Distrib. U$"
            'ElseIf intListadoAyuda = 4 Then
            '    MSFlexGrid2.FormatString = "<Cuenta              |<Descripci�n                                                                                              "
            'ElseIf intListadoAyuda = 5 Then
            '    MSFlexGrid2.FormatString = "<Cuenta                         |<Descripci�n                                                                                   "
            'ElseIf intListadoAyuda = 6 Then
            '    MSFlexGrid2.FormatString = "<Codigo      |<Nombre                                                                  "
            'ElseIf intListadoAyuda = 7 Then
            '    MSFlexGrid2.FormatString = "<Codigo      |<Nombre                                                                                              |>Saldo             "
            'End If

            'txtCodigo.Focus()
            'txtCodigo.SelectAll()
            txtDescripcion.Focus()
            txtDescripcion.SelectAll()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)

        End Try
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown

        Try
            If e.KeyCode = Keys.Enter Then
                Iniciar()
                txtDescripcion.Focus()
                txtDescripcion.SelectAll()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    'Private Sub MSFlexGrid2_DblClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles MSFlexGrid2.DblClick

    '    If MSFlexGrid2.Rows > 1 Then
    '        blnUbicar = True
    '        MSFlexGrid2.Col = 0
    '        strUbicar = MSFlexGrid2.Text
    '        If intListadoAyuda = 6 Then
    '            MSFlexGrid2.Col = 1
    '            strUbicar02 = MSFlexGrid2.Text
    '        End If
    '        Me.Close()
    '    End If

    'End Sub

    'Private Sub MSFlexGrid2_KeyDownEvent(ByVal sender As Object, ByVal e As AxMSFlexGridLib.DMSFlexGridEvents_KeyDownEvent) Handles MSFlexGrid2.KeyDownEvent

    '    If e.keyCode = Keys.Enter Then
    '        If MSFlexGrid2.Rows > 1 Then
    '            blnUbicar = True
    '            MSFlexGrid2.Col = 0
    '            strUbicar = MSFlexGrid2.Text
    '            If intListadoAyuda = 6 Then
    '                MSFlexGrid2.Col = 1
    '                strUbicar02 = MSFlexGrid2.Text
    '            End If
    '            Me.Close()
    '        End If
    '    End If

    'End Sub

    Private Sub txtDescripcion_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Iniciar()
                txtDescripcion.Focus()
                txtDescripcion.SelectAll()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Sub dgvBusqueda_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvBusqueda.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                If dgvBusqueda.RowCount > 0 Then
                    blnUbicar = True
                    strUbicar = dgvBusqueda.CurrentRow.Cells(0).Value

                    If intListadoAyuda = ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS Then
                        strUbicar02 = dgvBusqueda.CurrentRow.Cells(1).Value
                    End If
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Sub dgvBusqueda_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvBusqueda.DoubleClick
        Try
            If dgvBusqueda.RowCount > 0 Then
                blnUbicar = True
                strUbicar = dgvBusqueda.CurrentRow.Cells(0).Value

                If intListadoAyuda = ENTListadoAyuda.LISTADO_CUENTA_BENEFICIARIOS Then
                    strUbicar02 = dgvBusqueda.CurrentRow.Cells(1).Value
                End If
                Me.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Sub txtCodigo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.TextChanged
        Dim ldtDatosListadoAyuda As DataTable = Nothing
        Try
            ldtDatosListadoAyuda = Nothing
            ldtDatosListadoAyuda = RNBusqueda.FiltraDatosListadoAyuda(intListadoAyuda, 1, gdtListadoAyuda, txtCodigo.Text)
            dgvBusqueda.DataSource = ldtDatosListadoAyuda
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Sub txtDescripcion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescripcion.TextChanged
        Dim ldtDatosListadoAyuda As DataTable = Nothing
        Try
            ldtDatosListadoAyuda = Nothing
            ldtDatosListadoAyuda = RNBusqueda.FiltraDatosListadoAyuda(intListadoAyuda, 2, gdtListadoAyuda, txtDescripcion.Text)
            dgvBusqueda.DataSource = ldtDatosListadoAyuda
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreSeguridad
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

End Class
