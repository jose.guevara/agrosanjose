Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class actrptFactCredito
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label2 As DataDynamics.ActiveReports.Label = Nothing
    Private Label3 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private Label10 As DataDynamics.ActiveReports.Label = Nothing
    Private Label11 As DataDynamics.ActiveReports.Label = Nothing
    Private Label12 As DataDynamics.ActiveReports.Label = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private Label16 As DataDynamics.ActiveReports.Label = Nothing
    Private Label17 As DataDynamics.ActiveReports.Label = Nothing
    Private Label18 As DataDynamics.ActiveReports.Label = Nothing
    Private Label19 As DataDynamics.ActiveReports.Label = Nothing
    Private Label20 As DataDynamics.ActiveReports.Label = Nothing
    Private Label21 As DataDynamics.ActiveReports.Label = Nothing
    Private Label23 As DataDynamics.ActiveReports.Label = Nothing
    Private Label24 As DataDynamics.ActiveReports.Label = Nothing
    Private Label25 As DataDynamics.ActiveReports.Label = Nothing
    Private Label26 As DataDynamics.ActiveReports.Label = Nothing
    Private Label27 As DataDynamics.ActiveReports.Label = Nothing
    Private Label28 As DataDynamics.ActiveReports.Label = Nothing
    Private Label30 As DataDynamics.ActiveReports.Label = Nothing
    Private Label31 As DataDynamics.ActiveReports.Label = Nothing
    Private Label32 As DataDynamics.ActiveReports.Label = Nothing
    Private Label33 As DataDynamics.ActiveReports.Label = Nothing
    Private Label34 As DataDynamics.ActiveReports.Label = Nothing
    Private Label35 As DataDynamics.ActiveReports.Label = Nothing
    Private Label37 As DataDynamics.ActiveReports.Label = Nothing
    Private Label38 As DataDynamics.ActiveReports.Label = Nothing
    Private Label39 As DataDynamics.ActiveReports.Label = Nothing
    Private Label40 As DataDynamics.ActiveReports.Label = Nothing
    Private Label41 As DataDynamics.ActiveReports.Label = Nothing
    Private Label42 As DataDynamics.ActiveReports.Label = Nothing
    Private Label44 As DataDynamics.ActiveReports.Label = Nothing
    Private Label45 As DataDynamics.ActiveReports.Label = Nothing
    Private Label46 As DataDynamics.ActiveReports.Label = Nothing
    Private Label47 As DataDynamics.ActiveReports.Label = Nothing
    Private Label48 As DataDynamics.ActiveReports.Label = Nothing
    Private Label49 As DataDynamics.ActiveReports.Label = Nothing
    Private Label51 As DataDynamics.ActiveReports.Label = Nothing
    Private Label52 As DataDynamics.ActiveReports.Label = Nothing
    Private Label53 As DataDynamics.ActiveReports.Label = Nothing
    Private Label54 As DataDynamics.ActiveReports.Label = Nothing
    Private Label55 As DataDynamics.ActiveReports.Label = Nothing
    Private Label56 As DataDynamics.ActiveReports.Label = Nothing
    Private Label58 As DataDynamics.ActiveReports.Label = Nothing
    Private Label59 As DataDynamics.ActiveReports.Label = Nothing
    Private Label60 As DataDynamics.ActiveReports.Label = Nothing
    Private Label61 As DataDynamics.ActiveReports.Label = Nothing
    Private Label62 As DataDynamics.ActiveReports.Label = Nothing
    Private Label63 As DataDynamics.ActiveReports.Label = Nothing
    Private Label65 As DataDynamics.ActiveReports.Label = Nothing
    Private Label66 As DataDynamics.ActiveReports.Label = Nothing
    Private Label67 As DataDynamics.ActiveReports.Label = Nothing
    Private Label68 As DataDynamics.ActiveReports.Label = Nothing
    Private Label69 As DataDynamics.ActiveReports.Label = Nothing
    Private Label70 As DataDynamics.ActiveReports.Label = Nothing
    Private Label72 As DataDynamics.ActiveReports.Label = Nothing
    Private Label73 As DataDynamics.ActiveReports.Label = Nothing
    Private Label74 As DataDynamics.ActiveReports.Label = Nothing
    Private Label75 As DataDynamics.ActiveReports.Label = Nothing
    Private Label76 As DataDynamics.ActiveReports.Label = Nothing
    Private Label77 As DataDynamics.ActiveReports.Label = Nothing
    Private Label79 As DataDynamics.ActiveReports.Label = Nothing
    Private Label80 As DataDynamics.ActiveReports.Label = Nothing
    Private Label81 As DataDynamics.ActiveReports.Label = Nothing
    Private Label82 As DataDynamics.ActiveReports.Label = Nothing
    Private Label83 As DataDynamics.ActiveReports.Label = Nothing
    Private Label84 As DataDynamics.ActiveReports.Label = Nothing
    Private Label86 As DataDynamics.ActiveReports.Label = Nothing
    Private Label87 As DataDynamics.ActiveReports.Label = Nothing
    Private Label88 As DataDynamics.ActiveReports.Label = Nothing
    Private Label89 As DataDynamics.ActiveReports.Label = Nothing
    Private Label90 As DataDynamics.ActiveReports.Label = Nothing
    Private Label91 As DataDynamics.ActiveReports.Label = Nothing
    Private Label93 As DataDynamics.ActiveReports.Label = Nothing
    Private Label94 As DataDynamics.ActiveReports.Label = Nothing
    Private Label95 As DataDynamics.ActiveReports.Label = Nothing
    Private Label96 As DataDynamics.ActiveReports.Label = Nothing
    Private Label97 As DataDynamics.ActiveReports.Label = Nothing
    Private Label98 As DataDynamics.ActiveReports.Label = Nothing
    Private Label100 As DataDynamics.ActiveReports.Label = Nothing
    Private Label101 As DataDynamics.ActiveReports.Label = Nothing
    Private Label102 As DataDynamics.ActiveReports.Label = Nothing
    Private Label103 As DataDynamics.ActiveReports.Label = Nothing
    Private Label104 As DataDynamics.ActiveReports.Label = Nothing
    Private Label105 As DataDynamics.ActiveReports.Label = Nothing
    Private Label107 As DataDynamics.ActiveReports.Label = Nothing
    Private Label108 As DataDynamics.ActiveReports.Label = Nothing
    Private Label109 As DataDynamics.ActiveReports.Label = Nothing
    Private Label110 As DataDynamics.ActiveReports.Label = Nothing
    Private Label111 As DataDynamics.ActiveReports.Label = Nothing
    Private Label112 As DataDynamics.ActiveReports.Label = Nothing
    Private Label114 As DataDynamics.ActiveReports.Label = Nothing
    Private Label115 As DataDynamics.ActiveReports.Label = Nothing
    Private Label116 As DataDynamics.ActiveReports.Label = Nothing
    Private Label117 As DataDynamics.ActiveReports.Label = Nothing
    Private Label118 As DataDynamics.ActiveReports.Label = Nothing
    Private Label119 As DataDynamics.ActiveReports.Label = Nothing
    Private Label191 As DataDynamics.ActiveReports.Label = Nothing
    Private Label192 As DataDynamics.ActiveReports.Label = Nothing
    Private Label193 As DataDynamics.ActiveReports.Label = Nothing
    Private Label194 As DataDynamics.ActiveReports.Label = Nothing
    Private Label196 As DataDynamics.ActiveReports.Label = Nothing
    Private Label197 As DataDynamics.ActiveReports.Label = Nothing
    Private Label198 As DataDynamics.ActiveReports.Label = Nothing
    Private Label199 As DataDynamics.ActiveReports.Label = Nothing
    Private WithEvents Label15 As DataDynamics.ActiveReports.Label
    Private WithEvents Label22 As DataDynamics.ActiveReports.Label
    Private WithEvents Label29 As DataDynamics.ActiveReports.Label
    Private WithEvents Label36 As DataDynamics.ActiveReports.Label
    Private WithEvents Label43 As DataDynamics.ActiveReports.Label
    Private WithEvents Label50 As DataDynamics.ActiveReports.Label
    Private WithEvents Label57 As DataDynamics.ActiveReports.Label
    Private WithEvents Label64 As DataDynamics.ActiveReports.Label
    Private WithEvents Label71 As DataDynamics.ActiveReports.Label
    Private WithEvents Label78 As DataDynamics.ActiveReports.Label
    Private WithEvents Label85 As DataDynamics.ActiveReports.Label
    Private WithEvents Label92 As DataDynamics.ActiveReports.Label
    Private WithEvents Label99 As DataDynamics.ActiveReports.Label
    Private WithEvents Label106 As DataDynamics.ActiveReports.Label
    Private WithEvents Label113 As DataDynamics.ActiveReports.Label
    Private WithEvents Label120 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label121 As Label
    Private Label200 As DataDynamics.ActiveReports.Label = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptFactCredito))
        Me.Detail = New DataDynamics.ActiveReports.Detail()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.Label14 = New DataDynamics.ActiveReports.Label()
        Me.Label16 = New DataDynamics.ActiveReports.Label()
        Me.Label17 = New DataDynamics.ActiveReports.Label()
        Me.Label18 = New DataDynamics.ActiveReports.Label()
        Me.Label19 = New DataDynamics.ActiveReports.Label()
        Me.Label20 = New DataDynamics.ActiveReports.Label()
        Me.Label21 = New DataDynamics.ActiveReports.Label()
        Me.Label23 = New DataDynamics.ActiveReports.Label()
        Me.Label24 = New DataDynamics.ActiveReports.Label()
        Me.Label25 = New DataDynamics.ActiveReports.Label()
        Me.Label26 = New DataDynamics.ActiveReports.Label()
        Me.Label27 = New DataDynamics.ActiveReports.Label()
        Me.Label28 = New DataDynamics.ActiveReports.Label()
        Me.Label30 = New DataDynamics.ActiveReports.Label()
        Me.Label31 = New DataDynamics.ActiveReports.Label()
        Me.Label32 = New DataDynamics.ActiveReports.Label()
        Me.Label33 = New DataDynamics.ActiveReports.Label()
        Me.Label34 = New DataDynamics.ActiveReports.Label()
        Me.Label35 = New DataDynamics.ActiveReports.Label()
        Me.Label37 = New DataDynamics.ActiveReports.Label()
        Me.Label38 = New DataDynamics.ActiveReports.Label()
        Me.Label39 = New DataDynamics.ActiveReports.Label()
        Me.Label40 = New DataDynamics.ActiveReports.Label()
        Me.Label41 = New DataDynamics.ActiveReports.Label()
        Me.Label42 = New DataDynamics.ActiveReports.Label()
        Me.Label44 = New DataDynamics.ActiveReports.Label()
        Me.Label45 = New DataDynamics.ActiveReports.Label()
        Me.Label46 = New DataDynamics.ActiveReports.Label()
        Me.Label47 = New DataDynamics.ActiveReports.Label()
        Me.Label48 = New DataDynamics.ActiveReports.Label()
        Me.Label49 = New DataDynamics.ActiveReports.Label()
        Me.Label51 = New DataDynamics.ActiveReports.Label()
        Me.Label52 = New DataDynamics.ActiveReports.Label()
        Me.Label53 = New DataDynamics.ActiveReports.Label()
        Me.Label54 = New DataDynamics.ActiveReports.Label()
        Me.Label55 = New DataDynamics.ActiveReports.Label()
        Me.Label56 = New DataDynamics.ActiveReports.Label()
        Me.Label58 = New DataDynamics.ActiveReports.Label()
        Me.Label59 = New DataDynamics.ActiveReports.Label()
        Me.Label60 = New DataDynamics.ActiveReports.Label()
        Me.Label61 = New DataDynamics.ActiveReports.Label()
        Me.Label62 = New DataDynamics.ActiveReports.Label()
        Me.Label63 = New DataDynamics.ActiveReports.Label()
        Me.Label65 = New DataDynamics.ActiveReports.Label()
        Me.Label66 = New DataDynamics.ActiveReports.Label()
        Me.Label67 = New DataDynamics.ActiveReports.Label()
        Me.Label68 = New DataDynamics.ActiveReports.Label()
        Me.Label69 = New DataDynamics.ActiveReports.Label()
        Me.Label70 = New DataDynamics.ActiveReports.Label()
        Me.Label72 = New DataDynamics.ActiveReports.Label()
        Me.Label73 = New DataDynamics.ActiveReports.Label()
        Me.Label74 = New DataDynamics.ActiveReports.Label()
        Me.Label75 = New DataDynamics.ActiveReports.Label()
        Me.Label76 = New DataDynamics.ActiveReports.Label()
        Me.Label77 = New DataDynamics.ActiveReports.Label()
        Me.Label79 = New DataDynamics.ActiveReports.Label()
        Me.Label80 = New DataDynamics.ActiveReports.Label()
        Me.Label81 = New DataDynamics.ActiveReports.Label()
        Me.Label82 = New DataDynamics.ActiveReports.Label()
        Me.Label83 = New DataDynamics.ActiveReports.Label()
        Me.Label84 = New DataDynamics.ActiveReports.Label()
        Me.Label86 = New DataDynamics.ActiveReports.Label()
        Me.Label87 = New DataDynamics.ActiveReports.Label()
        Me.Label88 = New DataDynamics.ActiveReports.Label()
        Me.Label89 = New DataDynamics.ActiveReports.Label()
        Me.Label90 = New DataDynamics.ActiveReports.Label()
        Me.Label91 = New DataDynamics.ActiveReports.Label()
        Me.Label93 = New DataDynamics.ActiveReports.Label()
        Me.Label94 = New DataDynamics.ActiveReports.Label()
        Me.Label95 = New DataDynamics.ActiveReports.Label()
        Me.Label96 = New DataDynamics.ActiveReports.Label()
        Me.Label97 = New DataDynamics.ActiveReports.Label()
        Me.Label98 = New DataDynamics.ActiveReports.Label()
        Me.Label100 = New DataDynamics.ActiveReports.Label()
        Me.Label101 = New DataDynamics.ActiveReports.Label()
        Me.Label102 = New DataDynamics.ActiveReports.Label()
        Me.Label103 = New DataDynamics.ActiveReports.Label()
        Me.Label104 = New DataDynamics.ActiveReports.Label()
        Me.Label105 = New DataDynamics.ActiveReports.Label()
        Me.Label107 = New DataDynamics.ActiveReports.Label()
        Me.Label108 = New DataDynamics.ActiveReports.Label()
        Me.Label109 = New DataDynamics.ActiveReports.Label()
        Me.Label110 = New DataDynamics.ActiveReports.Label()
        Me.Label111 = New DataDynamics.ActiveReports.Label()
        Me.Label112 = New DataDynamics.ActiveReports.Label()
        Me.Label114 = New DataDynamics.ActiveReports.Label()
        Me.Label115 = New DataDynamics.ActiveReports.Label()
        Me.Label116 = New DataDynamics.ActiveReports.Label()
        Me.Label117 = New DataDynamics.ActiveReports.Label()
        Me.Label118 = New DataDynamics.ActiveReports.Label()
        Me.Label119 = New DataDynamics.ActiveReports.Label()
        Me.Label191 = New DataDynamics.ActiveReports.Label()
        Me.Label192 = New DataDynamics.ActiveReports.Label()
        Me.Label193 = New DataDynamics.ActiveReports.Label()
        Me.Label194 = New DataDynamics.ActiveReports.Label()
        Me.Label197 = New DataDynamics.ActiveReports.Label()
        Me.Label198 = New DataDynamics.ActiveReports.Label()
        Me.Label199 = New DataDynamics.ActiveReports.Label()
        Me.Label200 = New DataDynamics.ActiveReports.Label()
        Me.Label15 = New DataDynamics.ActiveReports.Label()
        Me.Label22 = New DataDynamics.ActiveReports.Label()
        Me.Label29 = New DataDynamics.ActiveReports.Label()
        Me.Label36 = New DataDynamics.ActiveReports.Label()
        Me.Label43 = New DataDynamics.ActiveReports.Label()
        Me.Label50 = New DataDynamics.ActiveReports.Label()
        Me.Label57 = New DataDynamics.ActiveReports.Label()
        Me.Label64 = New DataDynamics.ActiveReports.Label()
        Me.Label71 = New DataDynamics.ActiveReports.Label()
        Me.Label78 = New DataDynamics.ActiveReports.Label()
        Me.Label85 = New DataDynamics.ActiveReports.Label()
        Me.Label92 = New DataDynamics.ActiveReports.Label()
        Me.Label99 = New DataDynamics.ActiveReports.Label()
        Me.Label106 = New DataDynamics.ActiveReports.Label()
        Me.Label113 = New DataDynamics.ActiveReports.Label()
        Me.Label120 = New DataDynamics.ActiveReports.Label()
        Me.Label121 = New DataDynamics.ActiveReports.Label()
        Me.Label196 = New DataDynamics.ActiveReports.Label()
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label192, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label194, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label197, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label198, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label200, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Label14, Me.Label16, Me.Label17, Me.Label18, Me.Label19, Me.Label20, Me.Label21, Me.Label23, Me.Label24, Me.Label25, Me.Label26, Me.Label27, Me.Label28, Me.Label30, Me.Label31, Me.Label32, Me.Label33, Me.Label34, Me.Label35, Me.Label37, Me.Label38, Me.Label39, Me.Label40, Me.Label41, Me.Label42, Me.Label44, Me.Label45, Me.Label46, Me.Label47, Me.Label48, Me.Label49, Me.Label51, Me.Label52, Me.Label53, Me.Label54, Me.Label55, Me.Label56, Me.Label58, Me.Label59, Me.Label60, Me.Label61, Me.Label62, Me.Label63, Me.Label65, Me.Label66, Me.Label67, Me.Label68, Me.Label69, Me.Label70, Me.Label72, Me.Label73, Me.Label74, Me.Label75, Me.Label76, Me.Label77, Me.Label79, Me.Label80, Me.Label81, Me.Label82, Me.Label83, Me.Label84, Me.Label86, Me.Label87, Me.Label88, Me.Label89, Me.Label90, Me.Label91, Me.Label93, Me.Label94, Me.Label95, Me.Label96, Me.Label97, Me.Label98, Me.Label100, Me.Label101, Me.Label102, Me.Label103, Me.Label104, Me.Label105, Me.Label107, Me.Label108, Me.Label109, Me.Label110, Me.Label111, Me.Label112, Me.Label114, Me.Label115, Me.Label116, Me.Label117, Me.Label118, Me.Label119, Me.Label191, Me.Label192, Me.Label193, Me.Label194, Me.Label197, Me.Label198, Me.Label199, Me.Label200, Me.Label15, Me.Label22, Me.Label29, Me.Label36, Me.Label43, Me.Label50, Me.Label57, Me.Label64, Me.Label71, Me.Label78, Me.Label85, Me.Label92, Me.Label99, Me.Label106, Me.Label113, Me.Label120, Me.Label121})
        Me.Detail.Height = 4.322917!
        Me.Detail.Name = "Detail"
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0.4375!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label9.Text = ""
        Me.Label9.Top = 0.0625!
        Me.Label9.Width = 0.5625!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 1.375!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label10.Text = ""
        Me.Label10.Top = 0.0625!
        Me.Label10.Width = 0.5625!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 2.041667!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label11.Text = ""
        Me.Label11.Top = 0.0625!
        Me.Label11.Width = 0.8125!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 2.875!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label12.Text = ""
        Me.Label12.Top = 0.0625!
        Me.Label12.Width = 3.0!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 6.0!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label13.Text = ""
        Me.Label13.Top = 0.0625!
        Me.Label13.Width = 0.875!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 6.9375!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label14.Text = ""
        Me.Label14.Top = 0.0625!
        Me.Label14.Width = 0.8!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0.4375!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label16.Text = ""
        Me.Label16.Top = 0.25!
        Me.Label16.Width = 0.5625!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 1.375!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label17.Text = ""
        Me.Label17.Top = 0.25!
        Me.Label17.Width = 0.5625!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 2.041667!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label18.Text = ""
        Me.Label18.Top = 0.25!
        Me.Label18.Width = 0.8125!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 2.875!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label19.Text = ""
        Me.Label19.Top = 0.25!
        Me.Label19.Width = 3.0!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 6.0!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label20.Text = ""
        Me.Label20.Top = 0.25!
        Me.Label20.Width = 0.875!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 6.9375!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label21.Text = ""
        Me.Label21.Top = 0.25!
        Me.Label21.Width = 0.8!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 0.4375!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label23.Text = ""
        Me.Label23.Top = 0.4375!
        Me.Label23.Width = 0.5625!
        '
        'Label24
        '
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 1.375!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label24.Text = ""
        Me.Label24.Top = 0.4375!
        Me.Label24.Width = 0.5625!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 2.041667!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label25.Text = ""
        Me.Label25.Top = 0.4375!
        Me.Label25.Width = 0.8125!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 2.875!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label26.Text = ""
        Me.Label26.Top = 0.4375!
        Me.Label26.Width = 3.0!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 6.0!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label27.Text = ""
        Me.Label27.Top = 0.4375!
        Me.Label27.Width = 0.875!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 6.9375!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label28.Text = ""
        Me.Label28.Top = 0.4375!
        Me.Label28.Width = 0.8!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 0.4375!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label30.Text = ""
        Me.Label30.Top = 0.625!
        Me.Label30.Width = 0.5625!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 1.375!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label31.Text = ""
        Me.Label31.Top = 0.625!
        Me.Label31.Width = 0.5625!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 2.041667!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label32.Text = ""
        Me.Label32.Top = 0.625!
        Me.Label32.Width = 0.8125!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 2.875!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label33.Text = ""
        Me.Label33.Top = 0.625!
        Me.Label33.Width = 3.0!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 6.0!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label34.Text = ""
        Me.Label34.Top = 0.625!
        Me.Label34.Width = 0.875!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 6.9375!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label35.Text = ""
        Me.Label35.Top = 0.625!
        Me.Label35.Width = 0.8!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0.4375!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label37.Text = ""
        Me.Label37.Top = 0.8125!
        Me.Label37.Width = 0.5625!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 1.375!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label38.Text = ""
        Me.Label38.Top = 0.8125!
        Me.Label38.Width = 0.5625!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 2.041667!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label39.Text = ""
        Me.Label39.Top = 0.8125!
        Me.Label39.Width = 0.8125!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 2.875!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label40.Text = ""
        Me.Label40.Top = 0.8125!
        Me.Label40.Width = 3.0!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 6.0!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label41.Text = ""
        Me.Label41.Top = 0.8125!
        Me.Label41.Width = 0.875!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 6.9375!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label42.Text = ""
        Me.Label42.Top = 0.8125!
        Me.Label42.Width = 0.8!
        '
        'Label44
        '
        Me.Label44.Height = 0.1875!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 0.4375!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label44.Text = ""
        Me.Label44.Top = 1.0!
        Me.Label44.Width = 0.5625!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 1.375!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label45.Text = ""
        Me.Label45.Top = 1.0!
        Me.Label45.Width = 0.5625!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 2.041667!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label46.Text = ""
        Me.Label46.Top = 1.0!
        Me.Label46.Width = 0.8125!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 2.875!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label47.Text = ""
        Me.Label47.Top = 1.0!
        Me.Label47.Width = 3.0!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 6.0!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label48.Text = ""
        Me.Label48.Top = 1.0!
        Me.Label48.Width = 0.875!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 6.9375!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label49.Text = ""
        Me.Label49.Top = 1.0!
        Me.Label49.Width = 0.8!
        '
        'Label51
        '
        Me.Label51.Height = 0.1875!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 0.4375!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label51.Text = ""
        Me.Label51.Top = 1.1875!
        Me.Label51.Width = 0.5625!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 1.375!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label52.Text = ""
        Me.Label52.Top = 1.1875!
        Me.Label52.Width = 0.5625!
        '
        'Label53
        '
        Me.Label53.Height = 0.1875!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 2.041667!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label53.Text = ""
        Me.Label53.Top = 1.1875!
        Me.Label53.Width = 0.8125!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 2.875!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label54.Text = ""
        Me.Label54.Top = 1.1875!
        Me.Label54.Width = 3.0!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 6.0!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label55.Text = ""
        Me.Label55.Top = 1.1875!
        Me.Label55.Width = 0.875!
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 6.9375!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label56.Text = ""
        Me.Label56.Top = 1.1875!
        Me.Label56.Width = 0.8!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 0.4375!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label58.Text = ""
        Me.Label58.Top = 1.375!
        Me.Label58.Width = 0.5625!
        '
        'Label59
        '
        Me.Label59.Height = 0.1875!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 1.375!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label59.Text = ""
        Me.Label59.Top = 1.375!
        Me.Label59.Width = 0.5625!
        '
        'Label60
        '
        Me.Label60.Height = 0.1875!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 2.041667!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label60.Text = ""
        Me.Label60.Top = 1.375!
        Me.Label60.Width = 0.8125!
        '
        'Label61
        '
        Me.Label61.Height = 0.1875!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 2.875!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label61.Text = ""
        Me.Label61.Top = 1.375!
        Me.Label61.Width = 3.0!
        '
        'Label62
        '
        Me.Label62.Height = 0.1875!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 6.0!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label62.Text = ""
        Me.Label62.Top = 1.375!
        Me.Label62.Width = 0.875!
        '
        'Label63
        '
        Me.Label63.Height = 0.1875!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 6.9375!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label63.Text = ""
        Me.Label63.Top = 1.375!
        Me.Label63.Width = 0.8!
        '
        'Label65
        '
        Me.Label65.Height = 0.1875!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 0.4375!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label65.Text = ""
        Me.Label65.Top = 1.5625!
        Me.Label65.Width = 0.5625!
        '
        'Label66
        '
        Me.Label66.Height = 0.1875!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 1.375!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label66.Text = ""
        Me.Label66.Top = 1.5625!
        Me.Label66.Width = 0.5625!
        '
        'Label67
        '
        Me.Label67.Height = 0.1875!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 2.041667!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label67.Text = ""
        Me.Label67.Top = 1.5625!
        Me.Label67.Width = 0.8125!
        '
        'Label68
        '
        Me.Label68.Height = 0.1875!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 2.875!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label68.Text = ""
        Me.Label68.Top = 1.5625!
        Me.Label68.Width = 3.0!
        '
        'Label69
        '
        Me.Label69.Height = 0.1875!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 6.0!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label69.Text = ""
        Me.Label69.Top = 1.5625!
        Me.Label69.Width = 0.875!
        '
        'Label70
        '
        Me.Label70.Height = 0.1875!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 6.9375!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label70.Text = ""
        Me.Label70.Top = 1.5625!
        Me.Label70.Width = 0.8!
        '
        'Label72
        '
        Me.Label72.Height = 0.1875!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 0.4375!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label72.Text = ""
        Me.Label72.Top = 1.75!
        Me.Label72.Width = 0.5625!
        '
        'Label73
        '
        Me.Label73.Height = 0.1875!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 1.375!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label73.Text = ""
        Me.Label73.Top = 1.75!
        Me.Label73.Width = 0.5625!
        '
        'Label74
        '
        Me.Label74.Height = 0.1875!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 2.041667!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label74.Text = ""
        Me.Label74.Top = 1.75!
        Me.Label74.Width = 0.8125!
        '
        'Label75
        '
        Me.Label75.Height = 0.1875!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 2.875!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label75.Text = ""
        Me.Label75.Top = 1.75!
        Me.Label75.Width = 3.0!
        '
        'Label76
        '
        Me.Label76.Height = 0.1875!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 6.0!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label76.Text = ""
        Me.Label76.Top = 1.75!
        Me.Label76.Width = 0.875!
        '
        'Label77
        '
        Me.Label77.Height = 0.1875!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 6.9375!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label77.Text = ""
        Me.Label77.Top = 1.75!
        Me.Label77.Width = 0.8!
        '
        'Label79
        '
        Me.Label79.Height = 0.1875!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 0.4375!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label79.Text = ""
        Me.Label79.Top = 1.9375!
        Me.Label79.Width = 0.5625!
        '
        'Label80
        '
        Me.Label80.Height = 0.1875!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 1.375!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label80.Text = ""
        Me.Label80.Top = 1.9375!
        Me.Label80.Width = 0.5625!
        '
        'Label81
        '
        Me.Label81.Height = 0.1875!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 2.041667!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label81.Text = ""
        Me.Label81.Top = 1.9375!
        Me.Label81.Width = 0.8125!
        '
        'Label82
        '
        Me.Label82.Height = 0.1875!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 2.875!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label82.Text = ""
        Me.Label82.Top = 1.9375!
        Me.Label82.Width = 3.0!
        '
        'Label83
        '
        Me.Label83.Height = 0.1875!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 6.0!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label83.Text = ""
        Me.Label83.Top = 1.9375!
        Me.Label83.Width = 0.875!
        '
        'Label84
        '
        Me.Label84.Height = 0.1875!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 6.9375!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label84.Text = ""
        Me.Label84.Top = 1.9375!
        Me.Label84.Width = 0.8!
        '
        'Label86
        '
        Me.Label86.Height = 0.1875!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 0.4375!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label86.Text = ""
        Me.Label86.Top = 2.125!
        Me.Label86.Width = 0.5625!
        '
        'Label87
        '
        Me.Label87.Height = 0.1875!
        Me.Label87.HyperLink = Nothing
        Me.Label87.Left = 1.375!
        Me.Label87.Name = "Label87"
        Me.Label87.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label87.Text = ""
        Me.Label87.Top = 2.125!
        Me.Label87.Width = 0.5625!
        '
        'Label88
        '
        Me.Label88.Height = 0.1875!
        Me.Label88.HyperLink = Nothing
        Me.Label88.Left = 2.041667!
        Me.Label88.Name = "Label88"
        Me.Label88.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label88.Text = ""
        Me.Label88.Top = 2.125!
        Me.Label88.Width = 0.8125!
        '
        'Label89
        '
        Me.Label89.Height = 0.1875!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 2.875!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label89.Text = ""
        Me.Label89.Top = 2.125!
        Me.Label89.Width = 3.0!
        '
        'Label90
        '
        Me.Label90.Height = 0.1875!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 6.0!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label90.Text = ""
        Me.Label90.Top = 2.125!
        Me.Label90.Width = 0.875!
        '
        'Label91
        '
        Me.Label91.Height = 0.1875!
        Me.Label91.HyperLink = Nothing
        Me.Label91.Left = 6.9375!
        Me.Label91.Name = "Label91"
        Me.Label91.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label91.Text = ""
        Me.Label91.Top = 2.125!
        Me.Label91.Width = 0.8!
        '
        'Label93
        '
        Me.Label93.Height = 0.1875!
        Me.Label93.HyperLink = Nothing
        Me.Label93.Left = 0.4375!
        Me.Label93.Name = "Label93"
        Me.Label93.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label93.Text = ""
        Me.Label93.Top = 2.3125!
        Me.Label93.Width = 0.5625!
        '
        'Label94
        '
        Me.Label94.Height = 0.1875!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 1.375!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label94.Text = ""
        Me.Label94.Top = 2.3125!
        Me.Label94.Width = 0.5625!
        '
        'Label95
        '
        Me.Label95.Height = 0.1875!
        Me.Label95.HyperLink = Nothing
        Me.Label95.Left = 2.041667!
        Me.Label95.Name = "Label95"
        Me.Label95.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label95.Text = ""
        Me.Label95.Top = 2.3125!
        Me.Label95.Width = 0.8125!
        '
        'Label96
        '
        Me.Label96.Height = 0.1875!
        Me.Label96.HyperLink = Nothing
        Me.Label96.Left = 2.875!
        Me.Label96.Name = "Label96"
        Me.Label96.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label96.Text = ""
        Me.Label96.Top = 2.3125!
        Me.Label96.Width = 3.0!
        '
        'Label97
        '
        Me.Label97.Height = 0.1875!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 6.0!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label97.Text = ""
        Me.Label97.Top = 2.3125!
        Me.Label97.Width = 0.875!
        '
        'Label98
        '
        Me.Label98.Height = 0.1875!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 6.9375!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label98.Text = ""
        Me.Label98.Top = 2.3125!
        Me.Label98.Width = 0.8!
        '
        'Label100
        '
        Me.Label100.Height = 0.1875!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 0.4375!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label100.Text = ""
        Me.Label100.Top = 2.5!
        Me.Label100.Width = 0.5625!
        '
        'Label101
        '
        Me.Label101.Height = 0.1875!
        Me.Label101.HyperLink = Nothing
        Me.Label101.Left = 1.375!
        Me.Label101.Name = "Label101"
        Me.Label101.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label101.Text = ""
        Me.Label101.Top = 2.5!
        Me.Label101.Width = 0.5625!
        '
        'Label102
        '
        Me.Label102.Height = 0.1875!
        Me.Label102.HyperLink = Nothing
        Me.Label102.Left = 2.041667!
        Me.Label102.Name = "Label102"
        Me.Label102.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label102.Text = ""
        Me.Label102.Top = 2.5!
        Me.Label102.Width = 0.8125!
        '
        'Label103
        '
        Me.Label103.Height = 0.1875!
        Me.Label103.HyperLink = Nothing
        Me.Label103.Left = 2.875!
        Me.Label103.Name = "Label103"
        Me.Label103.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label103.Text = ""
        Me.Label103.Top = 2.5!
        Me.Label103.Width = 3.0!
        '
        'Label104
        '
        Me.Label104.Height = 0.1875!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 6.0!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label104.Text = ""
        Me.Label104.Top = 2.5!
        Me.Label104.Width = 0.875!
        '
        'Label105
        '
        Me.Label105.Height = 0.1875!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 6.9375!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label105.Text = ""
        Me.Label105.Top = 2.5!
        Me.Label105.Width = 0.8!
        '
        'Label107
        '
        Me.Label107.Height = 0.1875!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 0.4375!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label107.Text = ""
        Me.Label107.Top = 2.6875!
        Me.Label107.Width = 0.5625!
        '
        'Label108
        '
        Me.Label108.Height = 0.1875!
        Me.Label108.HyperLink = Nothing
        Me.Label108.Left = 1.375!
        Me.Label108.Name = "Label108"
        Me.Label108.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label108.Text = ""
        Me.Label108.Top = 2.6875!
        Me.Label108.Width = 0.5625!
        '
        'Label109
        '
        Me.Label109.Height = 0.1875!
        Me.Label109.HyperLink = Nothing
        Me.Label109.Left = 2.041667!
        Me.Label109.Name = "Label109"
        Me.Label109.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label109.Text = ""
        Me.Label109.Top = 2.6875!
        Me.Label109.Width = 0.8125!
        '
        'Label110
        '
        Me.Label110.Height = 0.1875!
        Me.Label110.HyperLink = Nothing
        Me.Label110.Left = 2.875!
        Me.Label110.Name = "Label110"
        Me.Label110.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label110.Text = ""
        Me.Label110.Top = 2.6875!
        Me.Label110.Width = 3.0!
        '
        'Label111
        '
        Me.Label111.Height = 0.1875!
        Me.Label111.HyperLink = Nothing
        Me.Label111.Left = 6.0!
        Me.Label111.Name = "Label111"
        Me.Label111.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label111.Text = ""
        Me.Label111.Top = 2.6875!
        Me.Label111.Width = 0.875!
        '
        'Label112
        '
        Me.Label112.Height = 0.1875!
        Me.Label112.HyperLink = Nothing
        Me.Label112.Left = 6.9375!
        Me.Label112.Name = "Label112"
        Me.Label112.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label112.Text = ""
        Me.Label112.Top = 2.6875!
        Me.Label112.Width = 0.8!
        '
        'Label114
        '
        Me.Label114.Height = 0.1875!
        Me.Label114.HyperLink = Nothing
        Me.Label114.Left = 0.4375!
        Me.Label114.Name = "Label114"
        Me.Label114.Style = "ddo-char-set: 1; text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label114.Text = ""
        Me.Label114.Top = 2.875!
        Me.Label114.Width = 0.5625!
        '
        'Label115
        '
        Me.Label115.Height = 0.1875!
        Me.Label115.HyperLink = Nothing
        Me.Label115.Left = 1.375!
        Me.Label115.Name = "Label115"
        Me.Label115.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label115.Text = ""
        Me.Label115.Top = 2.875!
        Me.Label115.Width = 0.5625!
        '
        'Label116
        '
        Me.Label116.Height = 0.1875!
        Me.Label116.HyperLink = Nothing
        Me.Label116.Left = 2.041667!
        Me.Label116.Name = "Label116"
        Me.Label116.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label116.Text = ""
        Me.Label116.Top = 2.875!
        Me.Label116.Width = 0.8125!
        '
        'Label117
        '
        Me.Label117.Height = 0.1875!
        Me.Label117.HyperLink = Nothing
        Me.Label117.Left = 2.875!
        Me.Label117.Name = "Label117"
        Me.Label117.Style = "ddo-char-set: 1; font-size: 11.25pt; font-family: Arial"
        Me.Label117.Text = ""
        Me.Label117.Top = 2.875!
        Me.Label117.Width = 3.0!
        '
        'Label118
        '
        Me.Label118.Height = 0.1875!
        Me.Label118.HyperLink = Nothing
        Me.Label118.Left = 6.0!
        Me.Label118.Name = "Label118"
        Me.Label118.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label118.Text = ""
        Me.Label118.Top = 2.875!
        Me.Label118.Width = 0.875!
        '
        'Label119
        '
        Me.Label119.Height = 0.1875!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 6.9375!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label119.Text = ""
        Me.Label119.Top = 2.875!
        Me.Label119.Width = 0.8!
        '
        'Label191
        '
        Me.Label191.Height = 0.1875!
        Me.Label191.HyperLink = Nothing
        Me.Label191.Left = 6.9375!
        Me.Label191.Name = "Label191"
        Me.Label191.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label191.Text = ""
        Me.Label191.Top = 3.25!
        Me.Label191.Width = 0.8125!
        '
        'Label192
        '
        Me.Label192.Height = 0.1875!
        Me.Label192.HyperLink = Nothing
        Me.Label192.Left = 6.9375!
        Me.Label192.Name = "Label192"
        Me.Label192.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label192.Text = ""
        Me.Label192.Top = 3.5!
        Me.Label192.Width = 0.8125!
        '
        'Label193
        '
        Me.Label193.Height = 0.1875!
        Me.Label193.HyperLink = Nothing
        Me.Label193.Left = 6.9375!
        Me.Label193.Name = "Label193"
        Me.Label193.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label193.Text = ""
        Me.Label193.Top = 3.75!
        Me.Label193.Width = 0.8125!
        '
        'Label194
        '
        Me.Label194.Height = 0.1500001!
        Me.Label194.HyperLink = Nothing
        Me.Label194.Left = 1.375!
        Me.Label194.Name = "Label194"
        Me.Label194.Style = "text-align: left; font-weight: bold; font-size: 9pt; font-family: Arial"
        Me.Label194.Text = "ESTE IMPORTE ES CON MANTENIMIENTO DE VALOR"
        Me.Label194.Top = 3.9375!
        Me.Label194.Width = 5.125!
        '
        'Label197
        '
        Me.Label197.Height = 0.1875!
        Me.Label197.HyperLink = Nothing
        Me.Label197.Left = 6.9375!
        Me.Label197.Name = "Label197"
        Me.Label197.Style = "text-align: right; font-size: 11.25pt; font-family: Arial"
        Me.Label197.Text = ""
        Me.Label197.Top = 3.9375!
        Me.Label197.Visible = False
        Me.Label197.Width = 0.8125!
        '
        'Label198
        '
        Me.Label198.Height = 0.1875!
        Me.Label198.HyperLink = Nothing
        Me.Label198.Left = 6.667!
        Me.Label198.Name = "Label198"
        Me.Label198.Style = "text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label198.Text = "C$"
        Me.Label198.Top = 3.25!
        Me.Label198.Width = 0.27!
        '
        'Label199
        '
        Me.Label199.Height = 0.1875!
        Me.Label199.HyperLink = Nothing
        Me.Label199.Left = 6.667!
        Me.Label199.Name = "Label199"
        Me.Label199.Style = "text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label199.Text = "C$"
        Me.Label199.Top = 3.5!
        Me.Label199.Width = 0.27!
        '
        'Label200
        '
        Me.Label200.Height = 0.1875!
        Me.Label200.HyperLink = Nothing
        Me.Label200.Left = 6.667!
        Me.Label200.Name = "Label200"
        Me.Label200.Style = "text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label200.Text = "C$"
        Me.Label200.Top = 3.75!
        Me.Label200.Width = 0.27!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 7.75!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label15.Text = " "
        Me.Label15.Top = 0.0625!
        Me.Label15.Visible = False
        Me.Label15.Width = 0.125!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 7.75!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label22.Text = ""
        Me.Label22.Top = 0.25!
        Me.Label22.Visible = False
        Me.Label22.Width = 0.125!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 7.75!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label29.Text = ""
        Me.Label29.Top = 0.4375!
        Me.Label29.Visible = False
        Me.Label29.Width = 0.125!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 7.75!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label36.Text = ""
        Me.Label36.Top = 0.625!
        Me.Label36.Visible = False
        Me.Label36.Width = 0.125!
        '
        'Label43
        '
        Me.Label43.Height = 0.1875!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 7.75!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label43.Text = ""
        Me.Label43.Top = 0.8125!
        Me.Label43.Visible = False
        Me.Label43.Width = 0.125!
        '
        'Label50
        '
        Me.Label50.Height = 0.1875!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 7.75!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label50.Text = ""
        Me.Label50.Top = 1.0!
        Me.Label50.Visible = False
        Me.Label50.Width = 0.125!
        '
        'Label57
        '
        Me.Label57.Height = 0.1875!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 7.75!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label57.Text = ""
        Me.Label57.Top = 1.1875!
        Me.Label57.Visible = False
        Me.Label57.Width = 0.125!
        '
        'Label64
        '
        Me.Label64.Height = 0.1875!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 7.75!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label64.Text = ""
        Me.Label64.Top = 1.375!
        Me.Label64.Visible = False
        Me.Label64.Width = 0.125!
        '
        'Label71
        '
        Me.Label71.Height = 0.1875!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 7.75!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label71.Text = ""
        Me.Label71.Top = 1.5625!
        Me.Label71.Visible = False
        Me.Label71.Width = 0.125!
        '
        'Label78
        '
        Me.Label78.Height = 0.1875!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 7.75!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label78.Text = ""
        Me.Label78.Top = 1.75!
        Me.Label78.Visible = False
        Me.Label78.Width = 0.125!
        '
        'Label85
        '
        Me.Label85.Height = 0.1875!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 7.75!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label85.Text = ""
        Me.Label85.Top = 1.9375!
        Me.Label85.Visible = False
        Me.Label85.Width = 0.125!
        '
        'Label92
        '
        Me.Label92.Height = 0.1875!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 7.75!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label92.Text = ""
        Me.Label92.Top = 2.125!
        Me.Label92.Visible = False
        Me.Label92.Width = 0.125!
        '
        'Label99
        '
        Me.Label99.Height = 0.1875!
        Me.Label99.HyperLink = Nothing
        Me.Label99.Left = 7.75!
        Me.Label99.Name = "Label99"
        Me.Label99.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label99.Text = ""
        Me.Label99.Top = 2.3125!
        Me.Label99.Visible = False
        Me.Label99.Width = 0.125!
        '
        'Label106
        '
        Me.Label106.Height = 0.1875!
        Me.Label106.HyperLink = Nothing
        Me.Label106.Left = 7.75!
        Me.Label106.Name = "Label106"
        Me.Label106.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label106.Text = ""
        Me.Label106.Top = 2.5!
        Me.Label106.Visible = False
        Me.Label106.Width = 0.125!
        '
        'Label113
        '
        Me.Label113.Height = 0.1875!
        Me.Label113.HyperLink = Nothing
        Me.Label113.Left = 7.75!
        Me.Label113.Name = "Label113"
        Me.Label113.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label113.Text = ""
        Me.Label113.Top = 2.6875!
        Me.Label113.Visible = False
        Me.Label113.Width = 0.125!
        '
        'Label120
        '
        Me.Label120.Height = 0.1875!
        Me.Label120.HyperLink = Nothing
        Me.Label120.Left = 7.75!
        Me.Label120.Name = "Label120"
        Me.Label120.Style = "text-align: center; font-size: 8.25pt; font-family: Courier New"
        Me.Label120.Text = ""
        Me.Label120.Top = 2.875!
        Me.Label120.Visible = False
        Me.Label120.Width = 0.125!
        '
        'Label121
        '
        Me.Label121.Height = 0.1875!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 6.6665!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label121.Text = "C$"
        Me.Label121.Top = 3.937!
        Me.Label121.Width = 0.27!
        '
        'Label196
        '
        Me.Label196.Height = 0.1875!
        Me.Label196.HyperLink = Nothing
        Me.Label196.Left = 7.0625!
        Me.Label196.Name = "Label196"
        Me.Label196.Style = "text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label196.Text = "Label196"
        Me.Label196.Top = 0.0625!
        Me.Label196.Width = 0.8125!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label2, Me.Label3, Me.Label4, Me.Label, Me.Label1, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label196})
        Me.PageHeader.Height = 1.90625!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label2
        '
        Me.Label2.Height = 0.15!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 5.8125!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "text-align: center; font-size: 9pt"
        Me.Label2.Text = "Label2"
        Me.Label2.Top = 0.5!
        Me.Label2.Width = 0.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.15!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 6.4375!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "text-align: center; font-size: 9pt"
        Me.Label3.Text = "Label3"
        Me.Label3.Top = 0.5!
        Me.Label3.Width = 0.5!
        '
        'Label4
        '
        Me.Label4.Height = 0.15!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 7.375!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: center; font-size: 9pt"
        Me.Label4.Text = "Label4"
        Me.Label4.Top = 0.5!
        Me.Label4.Width = 0.5!
        '
        'Label
        '
        Me.Label.Height = 0.15!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 1.38!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-size: 9pt"
        Me.Label.Text = "Label"
        Me.Label.Top = 0.8125!
        Me.Label.Width = 4.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 1.5625!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 9pt"
        Me.Label1.Text = "Label1"
        Me.Label1.Top = 1.0625!
        Me.Label1.Width = 4.4375!
        '
        'Label5
        '
        Me.Label5.Height = 0.15!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 6.625!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 9pt"
        Me.Label5.Text = "Label5"
        Me.Label5.Top = 0.875!
        Me.Label5.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.15!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 4.875!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 9pt"
        Me.Label6.Text = "Label6"
        Me.Label6.Top = 1.375!
        Me.Label6.Width = 0.5!
        '
        'Label7
        '
        Me.Label7.Height = 0.15!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 6.875!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 9pt"
        Me.Label7.Text = "Label7"
        Me.Label7.Top = 1.3125!
        Me.Label7.Width = 1.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.15!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 1.5625!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 9pt"
        Me.Label8.Text = "Label8"
        Me.Label8.Top = 1.5625!
        Me.Label8.Width = 4.0!
        '
        'PageFooter
        '
        Me.PageFooter.Height = 0.04166667!
        Me.PageFooter.Name = "PageFooter"
        '
        'actrptFactCredito
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.9!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.958333!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" &
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label192, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label194, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label197, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label198, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label200, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region
    Public lblCollection As New Collection

    Private Sub actrptFactCredito_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Dim intLblIncr As Integer = 0
        Dim objParametro As SEParametroEmpresa = Nothing

        lblCollection.Add(Label9)
        lblCollection.Add(Label10)
        lblCollection.Add(Label11)
        lblCollection.Add(Label12)
        lblCollection.Add(Label15)
        lblCollection.Add(Label13)
        lblCollection.Add(Label14)
        lblCollection.Add(Label16)
        lblCollection.Add(Label17)
        lblCollection.Add(Label18)
        lblCollection.Add(Label19)
        lblCollection.Add(Label22)
        lblCollection.Add(Label20)
        lblCollection.Add(Label21)
        lblCollection.Add(Label23)
        lblCollection.Add(Label24)
        lblCollection.Add(Label25)
        lblCollection.Add(Label26)
        lblCollection.Add(Label29)
        lblCollection.Add(Label27)
        lblCollection.Add(Label28)
        lblCollection.Add(Label30)
        lblCollection.Add(Label31)
        lblCollection.Add(Label32)
        lblCollection.Add(Label33)
        lblCollection.Add(Label36)
        lblCollection.Add(Label34)
        lblCollection.Add(Label35)
        lblCollection.Add(Label37)
        lblCollection.Add(Label38)
        lblCollection.Add(Label39)
        lblCollection.Add(Label40)
        lblCollection.Add(Label43)
        lblCollection.Add(Label41)
        lblCollection.Add(Label42)
        lblCollection.Add(Label44)
        lblCollection.Add(Label45)
        lblCollection.Add(Label46)
        lblCollection.Add(Label47)
        lblCollection.Add(Label50)
        lblCollection.Add(Label48)
        lblCollection.Add(Label49)
        lblCollection.Add(Label51)
        lblCollection.Add(Label52)
        lblCollection.Add(Label53)
        lblCollection.Add(Label54)
        lblCollection.Add(Label57)
        lblCollection.Add(Label55)
        lblCollection.Add(Label56)
        lblCollection.Add(Label58)
        lblCollection.Add(Label59)
        lblCollection.Add(Label60)
        lblCollection.Add(Label61)
        lblCollection.Add(Label64)
        lblCollection.Add(Label62)
        lblCollection.Add(Label63)
        lblCollection.Add(Label65)
        lblCollection.Add(Label66)
        lblCollection.Add(Label67)
        lblCollection.Add(Label68)
        lblCollection.Add(Label71)
        lblCollection.Add(Label69)
        lblCollection.Add(Label70)
        lblCollection.Add(Label72)
        lblCollection.Add(Label73)
        lblCollection.Add(Label74)
        lblCollection.Add(Label75)
        lblCollection.Add(Label78)
        lblCollection.Add(Label76)
        lblCollection.Add(Label77)
        lblCollection.Add(Label79)
        lblCollection.Add(Label80)
        lblCollection.Add(Label81)
        lblCollection.Add(Label82)
        lblCollection.Add(Label85)
        lblCollection.Add(Label83)
        lblCollection.Add(Label84)
        lblCollection.Add(Label86)
        lblCollection.Add(Label87)
        lblCollection.Add(Label88)
        lblCollection.Add(Label89)
        lblCollection.Add(Label92)
        lblCollection.Add(Label90)
        lblCollection.Add(Label91)
        lblCollection.Add(Label93)
        lblCollection.Add(Label94)
        lblCollection.Add(Label95)
        lblCollection.Add(Label96)
        lblCollection.Add(Label99)
        lblCollection.Add(Label97)
        lblCollection.Add(Label98)
        lblCollection.Add(Label100)
        lblCollection.Add(Label101)
        lblCollection.Add(Label102)
        lblCollection.Add(Label103)
        lblCollection.Add(Label106)
        lblCollection.Add(Label104)
        lblCollection.Add(Label105)
        lblCollection.Add(Label107)
        lblCollection.Add(Label108)
        lblCollection.Add(Label109)
        lblCollection.Add(Label110)
        lblCollection.Add(Label113)
        lblCollection.Add(Label111)
        lblCollection.Add(Label112)
        lblCollection.Add(Label114)
        lblCollection.Add(Label115)
        lblCollection.Add(Label116)
        lblCollection.Add(Label117)
        lblCollection.Add(Label120)
        lblCollection.Add(Label118)
        lblCollection.Add(Label119)
        lblCollection.Add(Label15)

        Dim lsNumeroFactura As String = String.Empty


        objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
        If Len(SUConversiones.ConvierteAString(strNumeroFact)) > 3 Then
            If IsNumeric(strNumeroFact.Substring(1, 1)) Then
                lsNumeroFactura = strNumeroFact
            Else
                lsNumeroFactura = Right(strNumeroFact, Len(strNumeroFact) - 1)
            End If

        End If
        Dim dtDatos As DataTable = Nothing
        dtDatos = RNFacturas.ExtraerFacturaDT(lngRegAgencia, lsNumeroFactura, 5)
        If SUFunciones.ValidaDataTable(dtDatos) Then
            Label191.Text = Format(SUConversiones.ConvierteADouble(dtDatos.Rows(0)("SubTotal")), "#,##0.#0")
            'Label192.Text = Format(dblImpuesto, "#,##0.#0")
            Label192.Text = Format(SUConversiones.ConvierteADouble(dtDatos.Rows(0)("Impuesto")), "#,##0.#0")
            Label193.Text = Format(SUConversiones.ConvierteADouble(dtDatos.Rows(0)("Total")), "#,##0.#0")
            strDirecFact = SUConversiones.ConvierteAString(dtDatos.Rows(0)("Direccion"))
        End If

        Label2.Text = intDiaFact
        Label3.Text = strMesFact
        Label4.Text = lngYearFact
        Label.Text = UCase(strClienteFact)
        Label5.Text = UCase(strCodCliFact)
        Label1.Text = UCase(strDirecFact)
        Label6.Text = strDiasFact
        Label7.Text = strFecVencFact
        Label8.Text = UCase(strVendedor)
        'Label191.Text = Format(dblSubtotal, "#,##0.#0")
        ''Label192.Text = Format(dblImpuesto, "#,##0.#0")
        'Label192.Text = Format(dblImpuesto, "#,##0.#0")
        'Label193.Text = Format(dblTotal, "#,##0.#0")
        'Label194.Text = Label194.Text.Trim() & " US$ " & CStr(Format((SUConversiones.ConvierteADouble(dblTotal) / SUConversiones.ConvierteADouble(dblTipoCambio)), "#,##0.#0"))
        Label198.Text = "C$"
        Label199.Text = "C$"
        Label200.Text = "C$"
        Label121.Text = "C$"
        'Label135.Text = "C$"
        'Label133.Text = "C$"
        If gnMonedaFactura = 2 Then
            Label198.Text = "US$"
            Label199.Text = "US$"
            Label200.Text = "US$"
            Label121.Text = "US$"

            'Label194.Text = " ESTA FACTURA EN CORDOBAS C$ ES IGUAL A " & CStr(Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(dblTotal) * SUConversiones.ConvierteADouble(dblTipoCambio)), objParametro.IndicaTipoRedondeo), "#,##0.#0")) & " CAMBIANTE DE ACUERDO AL VALOR DEL DOLAR US$ "
            Label194.Text = "Si se paga en córdobas esta factura se multiplicará por el paralelo a la fecha de pago "
        End If


        If IsNumeric(strNumeroFact.Substring(1, 1)) = 0 Then
            strNumeroFact = strNumeroFact.Substring(1, Len(strNumeroFact) - 1)
        End If
        Label196.Text = strNumeroFact
        Label197.Text = strValorFactCOR
        intLblIncr = 0
        For intIncr = 0 To 25
            If arrCredito(intIncr, 0) = "" Then
                Exit For
            End If
            For intIncr2 = 1 To 7
                intLblIncr = intLblIncr + 1
                If lblCollection.Count >= intLblIncr Then
                    lblCollection(intLblIncr).text = arrCredito(intIncr, intIncr2)
                    If intIncr2 = 6 Then
                        If lblCollection(intLblIncr).text = "Si" Then
                            lblCollection(intLblIncr).text = "*"
                        End If
                    End If
                End If
            Next intIncr2
        Next intIncr
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperWidth = 8.5F
        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actrptFactCredito_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        'Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport

        'pdf.Export(Me.Document, "c:\facturas_actuales.pdf")
        Me.Document.Print(True, False)

    End Sub

End Class