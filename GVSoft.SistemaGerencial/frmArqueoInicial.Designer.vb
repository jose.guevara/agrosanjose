<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmArqueoInicial
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmArqueoInicial))
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle36 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim Appearance15 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance16 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance17 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance18 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance19 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance20 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance22 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance23 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance24 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance26 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance27 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance28 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.cmdGuardar = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdRecalcular = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.UltraGroupBox5 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UltraGroupBox6 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.AdvTree1 = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.TabControl1 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UltraGroupBox2 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.diTasaCambio = New DevComponents.Editors.DoubleInput()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.UltraGroupBox4 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.dgxDetalleDolares = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.DenominacionUSD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantidadUSD = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn()
        Me.TotalUSD = New Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn(Me.components)
        Me.UltraGroupBox3 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.dgxDetalleCordobas = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ugbIngresoDatos = New Infragistics.Win.Misc.UltraGroupBox()
        Me.ddlSerie = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.epBuscarCuadre = New DevComponents.DotNetBar.ExpandablePanel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.txtNumeroCuadreBuscar = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.diOtrosIngreso = New DevComponents.Editors.DoubleInput()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.diDeposito = New DevComponents.Editors.DoubleInput()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.diGastos = New DevComponents.Editors.DoubleInput()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtExNumeroReciboFinal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtExNumeroReciboInicial = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtExNumeroFacturaFinal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtExNumeroFacturaInicial = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtiFechaFinArqueo = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.diSaldoInicial = New DevComponents.Editors.DoubleInput()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbltxtNumeroArqueo = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtiFechaInicioArqueo = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbltxtUsuario = New DevComponents.DotNetBar.LabelX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.diSobranteCaja = New DevComponents.Editors.DoubleInput()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tbiInfoGeneralArqueo = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel()
        Me.UltraGroupBox7 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.dgResumenArqueo = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.utbMonto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Totales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tiResumenArqueo = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel()
        Me.UltraGroupBox8 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.UltraGroupBox9 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.ugDetalleArqueo = New Infragistics.Win.UltraWinGrid.UltraGrid()
        Me.UltraGroupBox12 = New Infragistics.Win.Misc.UltraGroupBox()
        Me.btnXExportarExcel = New DevComponents.DotNetBar.ButtonX()
        Me.tbiDetalleArqueo = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.Denominacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn()
        Me.Total = New Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn(Me.components)
        Me.diTotalDineraoEnCaja = New DevComponents.Editors.DoubleInput()
        Me.Label18 = New System.Windows.Forms.Label()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.UltraGroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox5.SuspendLayout()
        CType(Me.UltraGroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox6.SuspendLayout()
        CType(Me.AdvTree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox2.SuspendLayout()
        CType(Me.diTasaCambio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox4.SuspendLayout()
        CType(Me.dgxDetalleDolares, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox3.SuspendLayout()
        CType(Me.dgxDetalleCordobas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ugbIngresoDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugbIngresoDatos.SuspendLayout()
        Me.epBuscarCuadre.SuspendLayout()
        CType(Me.diOtrosIngreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtiFechaFinArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diSaldoInicial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtiFechaInicioArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diSobranteCaja, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel4.SuspendLayout()
        CType(Me.UltraGroupBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox7.SuspendLayout()
        CType(Me.dgResumenArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel3.SuspendLayout()
        CType(Me.UltraGroupBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox8.SuspendLayout()
        CType(Me.UltraGroupBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox9.SuspendLayout()
        CType(Me.ugDetalleArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox12.SuspendLayout()
        CType(Me.diTotalDineraoEnCaja, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Bar1
        '
        Me.Bar1.AntiAlias = True
        Me.Bar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdGuardar, Me.cmdProcesar, Me.cmdRecalcular, Me.cmdSalir})
        Me.Bar1.Location = New System.Drawing.Point(0, 0)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(1021, 50)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar1.TabIndex = 1
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'cmdGuardar
        '
        Me.cmdGuardar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdGuardar.Image = CType(resources.GetObject("cmdGuardar.Image"), System.Drawing.Image)
        Me.cmdGuardar.ImageFixedSize = New System.Drawing.Size(25, 25)
        Me.cmdGuardar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdGuardar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Text = "Guardar<F3>"
        Me.cmdGuardar.Tooltip = "Guardar Arqueo"
        '
        'cmdProcesar
        '
        Me.cmdProcesar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImageFixedSize = New System.Drawing.Size(25, 25)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Cierre Caja<F4>"
        Me.cmdProcesar.Tooltip = "Realizar Arqueo"
        '
        'cmdRecalcular
        '
        Me.cmdRecalcular.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdRecalcular.Image = CType(resources.GetObject("cmdRecalcular.Image"), System.Drawing.Image)
        Me.cmdRecalcular.ImageFixedSize = New System.Drawing.Size(25, 25)
        Me.cmdRecalcular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdRecalcular.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdRecalcular.Name = "cmdRecalcular"
        Me.cmdRecalcular.Text = "Recalcular Arqueo"
        Me.cmdRecalcular.Tooltip = "Realizar Arqueo"
        '
        'cmdSalir
        '
        Me.cmdSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdSalir.Image = CType(resources.GetObject("cmdSalir.Image"), System.Drawing.Image)
        Me.cmdSalir.ImageFixedSize = New System.Drawing.Size(25, 25)
        Me.cmdSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdSalir.Name = "cmdSalir"
        Me.cmdSalir.Text = "Salir"
        Me.cmdSalir.Tooltip = "Salir"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.UltraGroupBox5)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(0, 22)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(840, 528)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.Style.GradientAngle = 90
        Me.TabControlPanel2.TabIndex = 2
        '
        'UltraGroupBox5
        '
        Me.UltraGroupBox5.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox5.Controls.Add(Me.UltraGroupBox6)
        Me.UltraGroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox5.Location = New System.Drawing.Point(1, 1)
        Me.UltraGroupBox5.Name = "UltraGroupBox5"
        Me.UltraGroupBox5.Size = New System.Drawing.Size(838, 526)
        Me.UltraGroupBox5.TabIndex = 0
        Me.UltraGroupBox5.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraGroupBox6
        '
        Me.UltraGroupBox6.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox6.Controls.Add(Me.AdvTree1)
        Me.UltraGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox6.Location = New System.Drawing.Point(3, 4)
        Me.UltraGroupBox6.Name = "UltraGroupBox6"
        Me.UltraGroupBox6.Size = New System.Drawing.Size(832, 519)
        Me.UltraGroupBox6.TabIndex = 0
        Me.UltraGroupBox6.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'AdvTree1
        '
        Me.AdvTree1.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.AdvTree1.AllowDrop = True
        Me.AdvTree1.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.AdvTree1.BackgroundStyle.Class = "TreeBorderKey"
        Me.AdvTree1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.AdvTree1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AdvTree1.Location = New System.Drawing.Point(3, 4)
        Me.AdvTree1.Name = "AdvTree1"
        Me.AdvTree1.NodesConnector = Me.NodeConnector1
        Me.AdvTree1.NodeStyle = Me.ElementStyle1
        Me.AdvTree1.PathSeparator = ";"
        Me.AdvTree1.Size = New System.Drawing.Size(826, 512)
        Me.AdvTree1.Styles.Add(Me.ElementStyle1)
        Me.AdvTree1.TabIndex = 0
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.Class = ""
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'TabControl1
        '
        Me.TabControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TabControl1.CanReorderTabs = True
        Me.TabControl1.Controls.Add(Me.TabControlPanel1)
        Me.TabControl1.Controls.Add(Me.TabControlPanel4)
        Me.TabControl1.Controls.Add(Me.TabControlPanel3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 50)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.SelectedTabIndex = 1
        Me.TabControl1.Size = New System.Drawing.Size(1021, 546)
        Me.TabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Document
        Me.TabControl1.TabIndex = 3
        Me.TabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl1.Tabs.Add(Me.tbiInfoGeneralArqueo)
        Me.TabControl1.Tabs.Add(Me.tbiDetalleArqueo)
        Me.TabControl1.Tabs.Add(Me.tiResumenArqueo)
        Me.TabControl1.Text = "TabControl1"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.UltraGroupBox1)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 22)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(1021, 524)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbiInfoGeneralArqueo
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox1.Controls.Add(Me.UltraGroupBox2)
        Me.UltraGroupBox1.Controls.Add(Me.ugbIngresoDatos)
        Me.UltraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox1.Location = New System.Drawing.Point(1, 1)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(1019, 522)
        Me.UltraGroupBox1.TabIndex = 0
        Me.UltraGroupBox1.Text = "Informaci�n Arqueo"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraGroupBox2
        '
        Me.UltraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox2.Controls.Add(Me.diTotalDineraoEnCaja)
        Me.UltraGroupBox2.Controls.Add(Me.Label18)
        Me.UltraGroupBox2.Controls.Add(Me.diTasaCambio)
        Me.UltraGroupBox2.Controls.Add(Me.Label16)
        Me.UltraGroupBox2.Controls.Add(Me.UltraGroupBox4)
        Me.UltraGroupBox2.Controls.Add(Me.UltraGroupBox3)
        Me.UltraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox2.Location = New System.Drawing.Point(3, 216)
        Me.UltraGroupBox2.Name = "UltraGroupBox2"
        Me.UltraGroupBox2.Size = New System.Drawing.Size(1013, 303)
        Me.UltraGroupBox2.TabIndex = 3
        Me.UltraGroupBox2.Text = "Detalle Arqueo"
        Me.UltraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'diTasaCambio
        '
        Me.diTasaCambio.AccessibleDescription = "Req"
        Me.diTasaCambio.AccessibleName = "Saldo Inicial"
        '
        '
        '
        Me.diTasaCambio.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diTasaCambio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diTasaCambio.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diTasaCambio.DisplayFormat = "N4"
        Me.diTasaCambio.Increment = 1.0R
        Me.diTasaCambio.Location = New System.Drawing.Point(842, 39)
        Me.diTasaCambio.Name = "diTasaCambio"
        Me.diTasaCambio.Size = New System.Drawing.Size(98, 20)
        Me.diTasaCambio.TabIndex = 13
        Me.diTasaCambio.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.diTasaCambio.WatermarkText = "Tasa de Cambio"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label16.Location = New System.Drawing.Point(839, 23)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(84, 13)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Tasa de Cambio"
        '
        'UltraGroupBox4
        '
        Me.UltraGroupBox4.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox4.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox4.Controls.Add(Me.dgxDetalleDolares)
        Me.UltraGroupBox4.Location = New System.Drawing.Point(419, 19)
        Me.UltraGroupBox4.Name = "UltraGroupBox4"
        Me.UltraGroupBox4.Size = New System.Drawing.Size(411, 278)
        Me.UltraGroupBox4.TabIndex = 2
        Me.UltraGroupBox4.Text = "Detalle Dolares"
        Me.UltraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgxDetalleDolares
        '
        Me.dgxDetalleDolares.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleDolares.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.dgxDetalleDolares.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgxDetalleDolares.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DenominacionUSD, Me.CantidadUSD, Me.TotalUSD})
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgxDetalleDolares.DefaultCellStyle = DataGridViewCellStyle23
        Me.dgxDetalleDolares.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgxDetalleDolares.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgxDetalleDolares.Location = New System.Drawing.Point(3, 20)
        Me.dgxDetalleDolares.Name = "dgxDetalleDolares"
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleDolares.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        Me.dgxDetalleDolares.Size = New System.Drawing.Size(405, 255)
        Me.dgxDetalleDolares.TabIndex = 14
        '
        'DenominacionUSD
        '
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DenominacionUSD.DefaultCellStyle = DataGridViewCellStyle21
        Me.DenominacionUSD.Frozen = True
        Me.DenominacionUSD.HeaderText = "Denominaci�n"
        Me.DenominacionUSD.Name = "DenominacionUSD"
        Me.DenominacionUSD.ReadOnly = True
        Me.DenominacionUSD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DenominacionUSD.Width = 120
        '
        'CantidadUSD
        '
        '
        '
        '
        Me.CantidadUSD.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.CantidadUSD.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.CantidadUSD.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CantidadUSD.BackgroundStyle.TextColor = System.Drawing.Color.Black
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CantidadUSD.DefaultCellStyle = DataGridViewCellStyle22
        Me.CantidadUSD.Frozen = True
        Me.CantidadUSD.HeaderText = "Cantidad"
        Me.CantidadUSD.Name = "CantidadUSD"
        Me.CantidadUSD.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CantidadUSD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CantidadUSD.Width = 90
        '
        'TotalUSD
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalUSD.DefaultCellStyle = DataGridViewCellStyle3
        Me.TotalUSD.DefaultNewRowValue = CType(resources.GetObject("TotalUSD.DefaultNewRowValue"), Object)
        Me.TotalUSD.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.[Default]
        Me.TotalUSD.Frozen = True
        Me.TotalUSD.HeaderText = "Total"
        Me.TotalUSD.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.TotalUSD.MaskInput = Nothing
        Me.TotalUSD.Name = "TotalUSD"
        Me.TotalUSD.PadChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TotalUSD.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.TotalUSD.ReadOnly = True
        Me.TotalUSD.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.TotalUSD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.TotalUSD.SpinButtonAlignment = Infragistics.Win.SpinButtonDisplayStyle.None
        Me.TotalUSD.Width = 150
        '
        'UltraGroupBox3
        '
        Me.UltraGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox3.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox3.Controls.Add(Me.dgxDetalleCordobas)
        Me.UltraGroupBox3.Location = New System.Drawing.Point(5, 19)
        Me.UltraGroupBox3.Name = "UltraGroupBox3"
        Me.UltraGroupBox3.Size = New System.Drawing.Size(411, 278)
        Me.UltraGroupBox3.TabIndex = 1
        Me.UltraGroupBox3.Text = "Detalle C�rdobas"
        Me.UltraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgxDetalleCordobas
        '
        Me.dgxDetalleCordobas.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleCordobas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.dgxDetalleCordobas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Denominacion, Me.Cantidad, Me.Total})
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgxDetalleCordobas.DefaultCellStyle = DataGridViewCellStyle28
        Me.dgxDetalleCordobas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgxDetalleCordobas.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgxDetalleCordobas.Location = New System.Drawing.Point(3, 20)
        Me.dgxDetalleCordobas.Name = "dgxDetalleCordobas"
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleCordobas.RowHeadersDefaultCellStyle = DataGridViewCellStyle29
        Me.dgxDetalleCordobas.Size = New System.Drawing.Size(405, 255)
        Me.dgxDetalleCordobas.TabIndex = 13
        '
        'ugbIngresoDatos
        '
        Me.ugbIngresoDatos.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.ugbIngresoDatos.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.ugbIngresoDatos.Controls.Add(Me.ddlSerie)
        Me.ugbIngresoDatos.Controls.Add(Me.Label15)
        Me.ugbIngresoDatos.Controls.Add(Me.epBuscarCuadre)
        Me.ugbIngresoDatos.Controls.Add(Me.Label13)
        Me.ugbIngresoDatos.Controls.Add(Me.txtObservaciones)
        Me.ugbIngresoDatos.Controls.Add(Me.diOtrosIngreso)
        Me.ugbIngresoDatos.Controls.Add(Me.Label12)
        Me.ugbIngresoDatos.Controls.Add(Me.diDeposito)
        Me.ugbIngresoDatos.Controls.Add(Me.Label10)
        Me.ugbIngresoDatos.Controls.Add(Me.diGastos)
        Me.ugbIngresoDatos.Controls.Add(Me.Label11)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroReciboFinal)
        Me.ugbIngresoDatos.Controls.Add(Me.Label8)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroReciboInicial)
        Me.ugbIngresoDatos.Controls.Add(Me.Label9)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroFacturaFinal)
        Me.ugbIngresoDatos.Controls.Add(Me.Label7)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroFacturaInicial)
        Me.ugbIngresoDatos.Controls.Add(Me.Label6)
        Me.ugbIngresoDatos.Controls.Add(Me.dtiFechaFinArqueo)
        Me.ugbIngresoDatos.Controls.Add(Me.Label5)
        Me.ugbIngresoDatos.Controls.Add(Me.diSaldoInicial)
        Me.ugbIngresoDatos.Controls.Add(Me.Label4)
        Me.ugbIngresoDatos.Controls.Add(Me.lbltxtNumeroArqueo)
        Me.ugbIngresoDatos.Controls.Add(Me.Label3)
        Me.ugbIngresoDatos.Controls.Add(Me.dtiFechaInicioArqueo)
        Me.ugbIngresoDatos.Controls.Add(Me.Label2)
        Me.ugbIngresoDatos.Controls.Add(Me.lbltxtUsuario)
        Me.ugbIngresoDatos.Controls.Add(Me.Label1)
        Me.ugbIngresoDatos.Controls.Add(Me.diSobranteCaja)
        Me.ugbIngresoDatos.Controls.Add(Me.Label17)
        Me.ugbIngresoDatos.Dock = System.Windows.Forms.DockStyle.Top
        Me.ugbIngresoDatos.Location = New System.Drawing.Point(3, 20)
        Me.ugbIngresoDatos.Name = "ugbIngresoDatos"
        Me.ugbIngresoDatos.Size = New System.Drawing.Size(1013, 196)
        Me.ugbIngresoDatos.TabIndex = 2
        Me.ugbIngresoDatos.Text = "Informaci�n General Arqueo"
        Me.ugbIngresoDatos.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'ddlSerie
        '
        Me.ddlSerie.DisplayMember = "Text"
        Me.ddlSerie.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ddlSerie.FormattingEnabled = True
        Me.ddlSerie.ItemHeight = 14
        Me.ddlSerie.Location = New System.Drawing.Point(728, 28)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(52, 20)
        Me.ddlSerie.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ddlSerie.TabIndex = 55
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label15.Location = New System.Drawing.Point(694, 30)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(31, 13)
        Me.Label15.TabIndex = 54
        Me.Label15.Text = "Serie"
        '
        'epBuscarCuadre
        '
        Me.epBuscarCuadre.CanvasColor = System.Drawing.SystemColors.Control
        Me.epBuscarCuadre.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.epBuscarCuadre.Controls.Add(Me.Label14)
        Me.epBuscarCuadre.Controls.Add(Me.cmbAgencias)
        Me.epBuscarCuadre.Controls.Add(Me.txtNumeroCuadreBuscar)
        Me.epBuscarCuadre.Expanded = False
        Me.epBuscarCuadre.ExpandedBounds = New System.Drawing.Rectangle(694, 54, 246, 111)
        Me.epBuscarCuadre.Location = New System.Drawing.Point(694, 54)
        Me.epBuscarCuadre.Name = "epBuscarCuadre"
        Me.epBuscarCuadre.Size = New System.Drawing.Size(246, 26)
        Me.epBuscarCuadre.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.epBuscarCuadre.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.epBuscarCuadre.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.epBuscarCuadre.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.epBuscarCuadre.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.epBuscarCuadre.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.epBuscarCuadre.Style.GradientAngle = 90
        Me.epBuscarCuadre.TabIndex = 15
        Me.epBuscarCuadre.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.epBuscarCuadre.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.epBuscarCuadre.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.epBuscarCuadre.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.epBuscarCuadre.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.epBuscarCuadre.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.epBuscarCuadre.TitleStyle.GradientAngle = 90
        Me.epBuscarCuadre.TitleText = "Busqueda de Cuadre por N�mero"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label14.Location = New System.Drawing.Point(3, 34)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(51, 13)
        Me.Label14.TabIndex = 50
        Me.Label14.Text = "Agencias"
        '
        'cmbAgencias
        '
        Me.cmbAgencias.DisplayMember = "Text"
        Me.cmbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbAgencias.FormattingEnabled = True
        Me.cmbAgencias.ItemHeight = 14
        Me.cmbAgencias.Location = New System.Drawing.Point(19, 50)
        Me.cmbAgencias.Name = "cmbAgencias"
        Me.cmbAgencias.Size = New System.Drawing.Size(206, 20)
        Me.cmbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbAgencias.TabIndex = 16
        '
        'txtNumeroCuadreBuscar
        '
        Me.txtNumeroCuadreBuscar.AccessibleDescription = ""
        Me.txtNumeroCuadreBuscar.AccessibleName = "NumCuadre"
        Me.txtNumeroCuadreBuscar.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtNumeroCuadreBuscar.Border.Class = "TextBoxBorder"
        Me.txtNumeroCuadreBuscar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNumeroCuadreBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroCuadreBuscar.Location = New System.Drawing.Point(19, 77)
        Me.txtNumeroCuadreBuscar.Name = "txtNumeroCuadreBuscar"
        Me.txtNumeroCuadreBuscar.Size = New System.Drawing.Size(206, 20)
        Me.txtNumeroCuadreBuscar.TabIndex = 17
        Me.txtNumeroCuadreBuscar.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumeroCuadreBuscar.WatermarkText = "Numero Cuadre a Buscar <Enter>"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label13.Location = New System.Drawing.Point(45, 166)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(81, 13)
        Me.Label13.TabIndex = 51
        Me.Label13.Text = "Observaciones:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Location = New System.Drawing.Point(131, 163)
        Me.txtObservaciones.MaxLength = 100
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservaciones.Size = New System.Drawing.Size(786, 24)
        Me.txtObservaciones.TabIndex = 12
        '
        'diOtrosIngreso
        '
        Me.diOtrosIngreso.AccessibleDescription = "Req"
        Me.diOtrosIngreso.AccessibleName = "Saldo Inicial"
        '
        '
        '
        Me.diOtrosIngreso.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diOtrosIngreso.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diOtrosIngreso.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diOtrosIngreso.Increment = 1.0R
        Me.diOtrosIngreso.Location = New System.Drawing.Point(467, 54)
        Me.diOtrosIngreso.Name = "diOtrosIngreso"
        Me.diOtrosIngreso.Size = New System.Drawing.Size(212, 20)
        Me.diOtrosIngreso.TabIndex = 2
        Me.diOtrosIngreso.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diOtrosIngreso.WatermarkText = "Otros Ingresos"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label12.Location = New System.Drawing.Point(384, 57)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(78, 13)
        Me.Label12.TabIndex = 49
        Me.Label12.Text = "Otros Ingresos:"
        '
        'diDeposito
        '
        Me.diDeposito.AccessibleDescription = "Req"
        Me.diDeposito.AccessibleName = "De�sitos"
        '
        '
        '
        Me.diDeposito.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diDeposito.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diDeposito.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diDeposito.Increment = 1.0R
        Me.diDeposito.Location = New System.Drawing.Point(817, 134)
        Me.diDeposito.Name = "diDeposito"
        Me.diDeposito.Size = New System.Drawing.Size(180, 20)
        Me.diDeposito.TabIndex = 11
        Me.diDeposito.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diDeposito.WatermarkText = "Monto de Dep�sito"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(753, 136)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(57, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Dep�sitos:"
        '
        'diGastos
        '
        Me.diGastos.AccessibleDescription = "Req"
        Me.diGastos.AccessibleName = "Gastos"
        '
        '
        '
        Me.diGastos.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diGastos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diGastos.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diGastos.Increment = 1.0R
        Me.diGastos.Location = New System.Drawing.Point(817, 109)
        Me.diGastos.Name = "diGastos"
        Me.diGastos.Size = New System.Drawing.Size(180, 20)
        Me.diGastos.TabIndex = 10
        Me.diGastos.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diGastos.WatermarkText = "Monto de Gastos"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label11.Location = New System.Drawing.Point(767, 111)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Gastos:"
        '
        'txtExNumeroReciboFinal
        '
        Me.txtExNumeroReciboFinal.AccessibleDescription = "Req"
        Me.txtExNumeroReciboFinal.AccessibleName = "N�mero Recibo Final"
        Me.txtExNumeroReciboFinal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroReciboFinal.Border.Class = "TextBoxBorder"
        Me.txtExNumeroReciboFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroReciboFinal.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroReciboFinal.Location = New System.Drawing.Point(467, 135)
        Me.txtExNumeroReciboFinal.Name = "txtExNumeroReciboFinal"
        Me.txtExNumeroReciboFinal.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroReciboFinal.TabIndex = 8
        Me.txtExNumeroReciboFinal.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroReciboFinal.WatermarkText = "N�mero Recibo Final"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label8.Location = New System.Drawing.Point(353, 138)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "N�mero Recibo Final:"
        '
        'txtExNumeroReciboInicial
        '
        Me.txtExNumeroReciboInicial.AccessibleDescription = "Req"
        Me.txtExNumeroReciboInicial.AccessibleName = "N�mero Recibo Inicial"
        Me.txtExNumeroReciboInicial.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroReciboInicial.Border.Class = "TextBoxBorder"
        Me.txtExNumeroReciboInicial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroReciboInicial.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroReciboInicial.Location = New System.Drawing.Point(131, 135)
        Me.txtExNumeroReciboInicial.Name = "txtExNumeroReciboInicial"
        Me.txtExNumeroReciboInicial.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroReciboInicial.TabIndex = 7
        Me.txtExNumeroReciboInicial.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroReciboInicial.WatermarkText = "N�mero Recibo Inicial"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label9.Location = New System.Drawing.Point(12, 138)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "N�mero Recibo Inicial:"
        '
        'txtExNumeroFacturaFinal
        '
        Me.txtExNumeroFacturaFinal.AccessibleDescription = "Req"
        Me.txtExNumeroFacturaFinal.AccessibleName = "N�mero Factura Final"
        Me.txtExNumeroFacturaFinal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroFacturaFinal.Border.Class = "TextBoxBorder"
        Me.txtExNumeroFacturaFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroFacturaFinal.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroFacturaFinal.Location = New System.Drawing.Point(467, 108)
        Me.txtExNumeroFacturaFinal.Name = "txtExNumeroFacturaFinal"
        Me.txtExNumeroFacturaFinal.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroFacturaFinal.TabIndex = 6
        Me.txtExNumeroFacturaFinal.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroFacturaFinal.WatermarkText = "N�mero Factura Final"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label7.Location = New System.Drawing.Point(351, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "N�mero Factura Final:"
        '
        'txtExNumeroFacturaInicial
        '
        Me.txtExNumeroFacturaInicial.AccessibleDescription = "Req"
        Me.txtExNumeroFacturaInicial.AccessibleName = "N�mero Factura Inicial"
        Me.txtExNumeroFacturaInicial.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroFacturaInicial.Border.Class = "TextBoxBorder"
        Me.txtExNumeroFacturaInicial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroFacturaInicial.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroFacturaInicial.Location = New System.Drawing.Point(131, 108)
        Me.txtExNumeroFacturaInicial.Name = "txtExNumeroFacturaInicial"
        Me.txtExNumeroFacturaInicial.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroFacturaInicial.TabIndex = 5
        Me.txtExNumeroFacturaInicial.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroFacturaInicial.WatermarkText = "N�mero Factura Inicial"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label6.Location = New System.Drawing.Point(10, 111)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "N�mero Factura Inicial:"
        '
        'dtiFechaFinArqueo
        '
        Me.dtiFechaFinArqueo.AccessibleDescription = "Req"
        Me.dtiFechaFinArqueo.AccessibleName = "Fecha Fin Arqueo"
        '
        '
        '
        Me.dtiFechaFinArqueo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtiFechaFinArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtiFechaFinArqueo.ButtonDropDown.Visible = True
        Me.dtiFechaFinArqueo.CustomFormat = "dd/MM/yyyy"
        Me.dtiFechaFinArqueo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.dtiFechaFinArqueo.IsPopupCalendarOpen = False
        Me.dtiFechaFinArqueo.Location = New System.Drawing.Point(467, 81)
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtiFechaFinArqueo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.DisplayMonth = New Date(2011, 9, 1, 0, 0, 0, 0)
        Me.dtiFechaFinArqueo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtiFechaFinArqueo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.TodayButtonVisible = True
        Me.dtiFechaFinArqueo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtiFechaFinArqueo.Name = "dtiFechaFinArqueo"
        Me.dtiFechaFinArqueo.Size = New System.Drawing.Size(212, 20)
        Me.dtiFechaFinArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtiFechaFinArqueo.TabIndex = 4
        Me.dtiFechaFinArqueo.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.dtiFechaFinArqueo.WatermarkText = "Fecha Fin Arqueo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(368, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Fecha Fin Arqueo:"
        '
        'diSaldoInicial
        '
        Me.diSaldoInicial.AccessibleDescription = "Req"
        Me.diSaldoInicial.AccessibleName = "Saldo Inicial"
        '
        '
        '
        Me.diSaldoInicial.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diSaldoInicial.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diSaldoInicial.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diSaldoInicial.Enabled = False
        Me.diSaldoInicial.Increment = 1.0R
        Me.diSaldoInicial.Location = New System.Drawing.Point(131, 54)
        Me.diSaldoInicial.Name = "diSaldoInicial"
        Me.diSaldoInicial.Size = New System.Drawing.Size(212, 20)
        Me.diSaldoInicial.TabIndex = 1
        Me.diSaldoInicial.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diSaldoInicial.WatermarkText = "Saldo Inicial Arqueo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(59, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Saldo Inicial:"
        '
        'lbltxtNumeroArqueo
        '
        Me.lbltxtNumeroArqueo.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lbltxtNumeroArqueo.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.lbltxtNumeroArqueo.BackgroundStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.lbltxtNumeroArqueo.BackgroundStyle.Class = ""
        Me.lbltxtNumeroArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lbltxtNumeroArqueo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lbltxtNumeroArqueo.Location = New System.Drawing.Point(467, 25)
        Me.lbltxtNumeroArqueo.Name = "lbltxtNumeroArqueo"
        Me.lbltxtNumeroArqueo.Size = New System.Drawing.Size(212, 23)
        Me.lbltxtNumeroArqueo.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(378, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "N�mero Arqueo:"
        '
        'dtiFechaInicioArqueo
        '
        Me.dtiFechaInicioArqueo.AccessibleDescription = "Req"
        Me.dtiFechaInicioArqueo.AccessibleName = "Fecha Inicio Arqueo"
        '
        '
        '
        Me.dtiFechaInicioArqueo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtiFechaInicioArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtiFechaInicioArqueo.ButtonDropDown.Visible = True
        Me.dtiFechaInicioArqueo.CustomFormat = "dd/MM/yyyy"
        Me.dtiFechaInicioArqueo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.dtiFechaInicioArqueo.IsPopupCalendarOpen = False
        Me.dtiFechaInicioArqueo.Location = New System.Drawing.Point(131, 81)
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtiFechaInicioArqueo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.DisplayMonth = New Date(2011, 9, 1, 0, 0, 0, 0)
        Me.dtiFechaInicioArqueo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtiFechaInicioArqueo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.TodayButtonVisible = True
        Me.dtiFechaInicioArqueo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtiFechaInicioArqueo.Name = "dtiFechaInicioArqueo"
        Me.dtiFechaInicioArqueo.Size = New System.Drawing.Size(212, 20)
        Me.dtiFechaInicioArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtiFechaInicioArqueo.TabIndex = 3
        Me.dtiFechaInicioArqueo.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.dtiFechaInicioArqueo.WatermarkText = "Fecha Inicio Arqueo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label2.Location = New System.Drawing.Point(21, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 13)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Fecha Inicio Arqueo:"
        '
        'lbltxtUsuario
        '
        Me.lbltxtUsuario.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lbltxtUsuario.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.lbltxtUsuario.BackgroundStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.lbltxtUsuario.BackgroundStyle.Class = ""
        Me.lbltxtUsuario.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lbltxtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lbltxtUsuario.Location = New System.Drawing.Point(131, 25)
        Me.lbltxtUsuario.Name = "lbltxtUsuario"
        Me.lbltxtUsuario.Size = New System.Drawing.Size(212, 23)
        Me.lbltxtUsuario.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(80, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Usuario:"
        '
        'diSobranteCaja
        '
        Me.diSobranteCaja.AccessibleDescription = "Req"
        Me.diSobranteCaja.AccessibleName = "Saldo Inicial"
        '
        '
        '
        Me.diSobranteCaja.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diSobranteCaja.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diSobranteCaja.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diSobranteCaja.Increment = 1.0R
        Me.diSobranteCaja.Location = New System.Drawing.Point(817, 85)
        Me.diSobranteCaja.Name = "diSobranteCaja"
        Me.diSobranteCaja.Size = New System.Drawing.Size(180, 20)
        Me.diSobranteCaja.TabIndex = 9
        Me.diSobranteCaja.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diSobranteCaja.WatermarkText = "Sobrante en Caja"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label17.Location = New System.Drawing.Point(695, 88)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(117, 13)
        Me.Label17.TabIndex = 57
        Me.Label17.Text = "Sobrante(+)/Faltante(-):"
        '
        'tbiInfoGeneralArqueo
        '
        Me.tbiInfoGeneralArqueo.AttachedControl = Me.TabControlPanel1
        Me.tbiInfoGeneralArqueo.Name = "tbiInfoGeneralArqueo"
        Me.tbiInfoGeneralArqueo.Text = "Ingreso Informaci�n Arqueo"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.UltraGroupBox7)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(0, 22)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(1021, 524)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.Style.GradientAngle = 90
        Me.TabControlPanel4.TabIndex = 3
        Me.TabControlPanel4.TabItem = Me.tiResumenArqueo
        '
        'UltraGroupBox7
        '
        Me.UltraGroupBox7.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox7.Controls.Add(Me.dgResumenArqueo)
        Me.UltraGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox7.Location = New System.Drawing.Point(1, 1)
        Me.UltraGroupBox7.Name = "UltraGroupBox7"
        Me.UltraGroupBox7.Size = New System.Drawing.Size(1019, 522)
        Me.UltraGroupBox7.TabIndex = 1
        Me.UltraGroupBox7.Text = "Informaci�n Detalle Arqueo"
        Me.UltraGroupBox7.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgResumenArqueo
        '
        Me.dgResumenArqueo.AllowUserToAddRows = False
        Me.dgResumenArqueo.AllowUserToDeleteRows = False
        Me.dgResumenArqueo.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgResumenArqueo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle30
        Me.dgResumenArqueo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.utbMonto, Me.Totales})
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle35.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle35.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgResumenArqueo.DefaultCellStyle = DataGridViewCellStyle35
        Me.dgResumenArqueo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgResumenArqueo.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgResumenArqueo.Location = New System.Drawing.Point(3, 20)
        Me.dgResumenArqueo.Name = "dgResumenArqueo"
        Me.dgResumenArqueo.ReadOnly = True
        DataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgResumenArqueo.RowHeadersDefaultCellStyle = DataGridViewCellStyle36
        Me.dgResumenArqueo.Size = New System.Drawing.Size(1013, 499)
        Me.dgResumenArqueo.TabIndex = 11
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle31
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Operaci�n"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.ToolTipText = "Operaci�n"
        Me.DataGridViewTextBoxColumn2.Width = 62
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle32
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Concepto"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.ToolTipText = "Concepto"
        Me.DataGridViewTextBoxColumn3.Width = 59
        '
        'utbMonto
        '
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle33.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle33.NullValue = " "
        Me.utbMonto.DefaultCellStyle = DataGridViewCellStyle33
        Me.utbMonto.Frozen = True
        Me.utbMonto.HeaderText = "Monto"
        Me.utbMonto.Name = "utbMonto"
        Me.utbMonto.ReadOnly = True
        Me.utbMonto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.utbMonto.Width = 200
        '
        'Totales
        '
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Totales.DefaultCellStyle = DataGridViewCellStyle34
        Me.Totales.Frozen = True
        Me.Totales.HeaderText = "Totales"
        Me.Totales.Name = "Totales"
        Me.Totales.ReadOnly = True
        Me.Totales.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Totales.ToolTipText = "Totales"
        Me.Totales.Width = 200
        '
        'tiResumenArqueo
        '
        Me.tiResumenArqueo.AttachedControl = Me.TabControlPanel4
        Me.tiResumenArqueo.Name = "tiResumenArqueo"
        Me.tiResumenArqueo.Text = "Resumen Arqueo"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.UltraGroupBox8)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(0, 22)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(1021, 524)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.Style.GradientAngle = 90
        Me.TabControlPanel3.TabIndex = 2
        Me.TabControlPanel3.TabItem = Me.tbiDetalleArqueo
        '
        'UltraGroupBox8
        '
        Me.UltraGroupBox8.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox8.Controls.Add(Me.UltraGroupBox9)
        Me.UltraGroupBox8.Controls.Add(Me.UltraGroupBox12)
        Me.UltraGroupBox8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox8.Location = New System.Drawing.Point(1, 1)
        Me.UltraGroupBox8.Name = "UltraGroupBox8"
        Me.UltraGroupBox8.Size = New System.Drawing.Size(1019, 522)
        Me.UltraGroupBox8.TabIndex = 1
        Me.UltraGroupBox8.Text = " "
        Me.UltraGroupBox8.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraGroupBox9
        '
        Me.UltraGroupBox9.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox9.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox9.Controls.Add(Me.ugDetalleArqueo)
        Me.UltraGroupBox9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraGroupBox9.Location = New System.Drawing.Point(3, 85)
        Me.UltraGroupBox9.Name = "UltraGroupBox9"
        Me.UltraGroupBox9.Size = New System.Drawing.Size(1013, 434)
        Me.UltraGroupBox9.TabIndex = 3
        Me.UltraGroupBox9.Text = "Informaci�n Detalle Arqueo"
        Me.UltraGroupBox9.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'ugDetalleArqueo
        '
        Appearance15.BackColor = System.Drawing.SystemColors.Window
        Appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption
        Me.ugDetalleArqueo.DisplayLayout.Appearance = Appearance15
        Me.ugDetalleArqueo.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
        Me.ugDetalleArqueo.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.ugDetalleArqueo.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder
        Appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance16.BorderColor = System.Drawing.SystemColors.Window
        Me.ugDetalleArqueo.DisplayLayout.GroupByBox.Appearance = Appearance16
        Appearance17.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ugDetalleArqueo.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance17
        Me.ugDetalleArqueo.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance18.BackColor2 = System.Drawing.SystemColors.Control
        Appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance18.ForeColor = System.Drawing.SystemColors.GrayText
        Me.ugDetalleArqueo.DisplayLayout.GroupByBox.PromptAppearance = Appearance18
        Me.ugDetalleArqueo.DisplayLayout.MaxColScrollRegions = 1
        Me.ugDetalleArqueo.DisplayLayout.MaxRowScrollRegions = 1
        Appearance19.BackColor = System.Drawing.SystemColors.Window
        Appearance19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ugDetalleArqueo.DisplayLayout.Override.ActiveCellAppearance = Appearance19
        Appearance20.BackColor = System.Drawing.SystemColors.Highlight
        Appearance20.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.ugDetalleArqueo.DisplayLayout.Override.ActiveRowAppearance = Appearance20
        Me.ugDetalleArqueo.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No
        Me.ugDetalleArqueo.DisplayLayout.Override.AllowColMoving = Infragistics.Win.UltraWinGrid.AllowColMoving.NotAllowed
        Me.ugDetalleArqueo.DisplayLayout.Override.AllowColSwapping = Infragistics.Win.UltraWinGrid.AllowColSwapping.NotAllowed
        Me.ugDetalleArqueo.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.[False]
        Me.ugDetalleArqueo.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.[False]
        Me.ugDetalleArqueo.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.[False]
        Me.ugDetalleArqueo.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted
        Me.ugDetalleArqueo.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted
        Appearance22.BackColor = System.Drawing.SystemColors.Window
        Me.ugDetalleArqueo.DisplayLayout.Override.CardAreaAppearance = Appearance22
        Appearance23.BorderColor = System.Drawing.Color.Silver
        Appearance23.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter
        Me.ugDetalleArqueo.DisplayLayout.Override.CellAppearance = Appearance23
        Me.ugDetalleArqueo.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.ugDetalleArqueo.DisplayLayout.Override.CellPadding = 0
        Appearance24.BackColor = System.Drawing.SystemColors.Control
        Appearance24.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance24.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance24.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance24.BorderColor = System.Drawing.SystemColors.Window
        Me.ugDetalleArqueo.DisplayLayout.Override.GroupByRowAppearance = Appearance24
        Appearance1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Appearance1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ugDetalleArqueo.DisplayLayout.Override.GroupBySummaryValueAppearance = Appearance1
        Appearance26.TextHAlignAsString = "Left"
        Me.ugDetalleArqueo.DisplayLayout.Override.HeaderAppearance = Appearance26
        Me.ugDetalleArqueo.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.ugDetalleArqueo.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand
        Appearance27.BackColor = System.Drawing.SystemColors.Window
        Appearance27.BorderColor = System.Drawing.Color.Silver
        Me.ugDetalleArqueo.DisplayLayout.Override.RowAppearance = Appearance27
        Me.ugDetalleArqueo.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Me.ugDetalleArqueo.DisplayLayout.Override.SummaryDisplayArea = Infragistics.Win.UltraWinGrid.SummaryDisplayAreas.InGroupByRows
        Appearance2.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Appearance2.BorderColor2 = System.Drawing.Color.White
        Me.ugDetalleArqueo.DisplayLayout.Override.SummaryFooterAppearance = Appearance2
        Appearance3.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Appearance3.BorderColor = System.Drawing.Color.White
        Me.ugDetalleArqueo.DisplayLayout.Override.SummaryFooterCaptionAppearance = Appearance3
        Appearance4.BackColor = System.Drawing.Color.PaleGoldenrod
        Appearance4.BorderColor = System.Drawing.Color.White
        Me.ugDetalleArqueo.DisplayLayout.Override.SummaryValueAppearance = Appearance4
        Appearance28.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ugDetalleArqueo.DisplayLayout.Override.TemplateAddRowAppearance = Appearance28
        Me.ugDetalleArqueo.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.ugDetalleArqueo.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.ugDetalleArqueo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ugDetalleArqueo.Location = New System.Drawing.Point(3, 20)
        Me.ugDetalleArqueo.Name = "ugDetalleArqueo"
        Me.ugDetalleArqueo.Size = New System.Drawing.Size(1007, 411)
        Me.ugDetalleArqueo.TabIndex = 2
        Me.ugDetalleArqueo.Text = "UltraGrid1"
        '
        'UltraGroupBox12
        '
        Me.UltraGroupBox12.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox12.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox12.Controls.Add(Me.btnXExportarExcel)
        Me.UltraGroupBox12.Dock = System.Windows.Forms.DockStyle.Top
        Me.UltraGroupBox12.Location = New System.Drawing.Point(3, 20)
        Me.UltraGroupBox12.Name = "UltraGroupBox12"
        Me.UltraGroupBox12.Size = New System.Drawing.Size(1013, 65)
        Me.UltraGroupBox12.TabIndex = 2
        Me.UltraGroupBox12.Text = "Acciones"
        Me.UltraGroupBox12.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'btnXExportarExcel
        '
        Me.btnXExportarExcel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnXExportarExcel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnXExportarExcel.Image = CType(resources.GetObject("btnXExportarExcel.Image"), System.Drawing.Image)
        Me.btnXExportarExcel.ImageFixedSize = New System.Drawing.Size(45, 45)
        Me.btnXExportarExcel.Location = New System.Drawing.Point(8, 23)
        Me.btnXExportarExcel.Name = "btnXExportarExcel"
        Me.btnXExportarExcel.Size = New System.Drawing.Size(36, 36)
        Me.btnXExportarExcel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnXExportarExcel.TabIndex = 0
        '
        'tbiDetalleArqueo
        '
        Me.tbiDetalleArqueo.AttachedControl = Me.TabControlPanel3
        Me.tbiDetalleArqueo.Name = "tbiDetalleArqueo"
        Me.tbiDetalleArqueo.Text = "Informaci�n Detalle Arqueo"
        '
        'Denominacion
        '
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Denominacion.DefaultCellStyle = DataGridViewCellStyle26
        Me.Denominacion.Frozen = True
        Me.Denominacion.HeaderText = "Denominaci�n"
        Me.Denominacion.Name = "Denominacion"
        Me.Denominacion.ReadOnly = True
        Me.Denominacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Denominacion.ToolTipText = "Denominaci�n"
        Me.Denominacion.Width = 120
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.BackgroundStyle.TextColor = System.Drawing.Color.Black
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cantidad.DefaultCellStyle = DataGridViewCellStyle27
        Me.Cantidad.Frozen = True
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Cantidad.ToolTipText = "Cantidad"
        Me.Cantidad.Width = 90
        '
        'Total
        '
        Appearance6.TextHAlignAsString = "Right"
        Me.Total.CellAppearance = Appearance6
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total.DefaultCellStyle = DataGridViewCellStyle9
        Me.Total.DefaultNewRowValue = CType(resources.GetObject("Total.DefaultNewRowValue"), Object)
        Me.Total.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007
        Me.Total.Frozen = True
        Me.Total.HeaderText = "Total"
        Me.Total.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.Total.MaskInput = Nothing
        Me.Total.Name = "Total"
        Me.Total.PadChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Total.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Total.ReadOnly = True
        Me.Total.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Total.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Total.SpinButtonAlignment = Infragistics.Win.SpinButtonDisplayStyle.None
        Me.Total.Width = 135
        '
        'diTotalDineraoEnCaja
        '
        Me.diTotalDineraoEnCaja.AccessibleDescription = "Req"
        Me.diTotalDineraoEnCaja.AccessibleName = "Saldo Inicial"
        '
        '
        '
        Me.diTotalDineraoEnCaja.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diTotalDineraoEnCaja.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diTotalDineraoEnCaja.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diTotalDineraoEnCaja.DisplayFormat = "N2"
        Me.diTotalDineraoEnCaja.Enabled = False
        Me.diTotalDineraoEnCaja.Increment = 1.0R
        Me.diTotalDineraoEnCaja.Location = New System.Drawing.Point(842, 91)
        Me.diTotalDineraoEnCaja.Name = "diTotalDineraoEnCaja"
        Me.diTotalDineraoEnCaja.Size = New System.Drawing.Size(98, 20)
        Me.diTotalDineraoEnCaja.TabIndex = 15
        Me.diTotalDineraoEnCaja.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.diTotalDineraoEnCaja.WatermarkText = "Tasa de Cambio"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label18.Location = New System.Drawing.Point(839, 75)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(105, 13)
        Me.Label18.TabIndex = 14
        Me.Label18.Text = "Total Dinero En Caja"
        '
        'frmArqueoInicial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1021, 596)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Bar1)
        Me.DoubleBuffered = True
        Me.Name = "frmArqueoInicial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmArqueoInicial"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel2.ResumeLayout(False)
        CType(Me.UltraGroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox5.ResumeLayout(False)
        CType(Me.UltraGroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox6.ResumeLayout(False)
        CType(Me.AdvTree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox2.ResumeLayout(False)
        Me.UltraGroupBox2.PerformLayout()
        CType(Me.diTasaCambio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox4.ResumeLayout(False)
        CType(Me.dgxDetalleDolares, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox3.ResumeLayout(False)
        CType(Me.dgxDetalleCordobas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ugbIngresoDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugbIngresoDatos.ResumeLayout(False)
        Me.ugbIngresoDatos.PerformLayout()
        Me.epBuscarCuadre.ResumeLayout(False)
        Me.epBuscarCuadre.PerformLayout()
        CType(Me.diOtrosIngreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diGastos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtiFechaFinArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diSaldoInicial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtiFechaInicioArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diSobranteCaja, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel4.ResumeLayout(False)
        CType(Me.UltraGroupBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox7.ResumeLayout(False)
        CType(Me.dgResumenArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel3.ResumeLayout(False)
        CType(Me.UltraGroupBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox8.ResumeLayout(False)
        CType(Me.UltraGroupBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox9.ResumeLayout(False)
        CType(Me.ugDetalleArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox12.ResumeLayout(False)
        CType(Me.diTotalDineraoEnCaja, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdGuardar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents UltraGroupBox5 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox6 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents AdvTree1 As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents TabControl1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox2 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox4 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents dgxDetalleDolares As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents UltraGroupBox3 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents dgxDetalleCordobas As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ugbIngresoDatos As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents diDeposito As DevComponents.Editors.DoubleInput
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents diGastos As DevComponents.Editors.DoubleInput
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroReciboFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroReciboInicial As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroFacturaFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroFacturaInicial As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtiFechaFinArqueo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents diSaldoInicial As DevComponents.Editors.DoubleInput
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbltxtNumeroArqueo As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtiFechaInicioArqueo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbltxtUsuario As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbiInfoGeneralArqueo As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents UltraGroupBox8 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox9 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents ugDetalleArqueo As Infragistics.Win.UltraWinGrid.UltraGrid
    Friend WithEvents UltraGroupBox12 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents btnXExportarExcel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tbiDetalleArqueo As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents UltraGroupBox7 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents dgResumenArqueo As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents tiResumenArqueo As DevComponents.DotNetBar.TabItem
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents utbMonto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Totales As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents diOtrosIngreso As DevComponents.Editors.DoubleInput
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents epBuscarCuadre As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents txtNumeroCuadreBuscar As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ddlSerie As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cmdRecalcular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents diTasaCambio As DevComponents.Editors.DoubleInput
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents diSobranteCaja As DevComponents.Editors.DoubleInput
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents DenominacionUSD As DataGridViewTextBoxColumn
    Friend WithEvents CantidadUSD As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents TotalUSD As Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn
    Friend WithEvents Denominacion As DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents Total As Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn
    Friend WithEvents diTotalDineraoEnCaja As DevComponents.Editors.DoubleInput
    Friend WithEvents Label18 As Label
End Class
