﻿Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Data.SqlClient

Public Class frmOpcionesxUsuario
    Dim _RightAlignFileSizeStyle As DevComponents.DotNetBar.ElementStyle
    Dim dtOpciones As DataTable = Nothing
    Dim dtGrupos As DataTable = Nothing
    Dim dtModulos As DataTable = Nothing
    Dim dtDescuento As DataTable = Nothing
    Dim dtAgencias As DataTable = Nothing
    Dim dtModulosReportes As DataTable = Nothing
    Dim dtReportes As DataTable = Nothing
    Dim dtOpcionesxUsuario As DataTable = Nothing
    Dim IdUsuario As Integer = Nothing
    Dim strCambios(8) As String
    Dim objReader As IDataReader
    Public txtCollection As New Collection
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmOpcionesxUsuario"

    Private Function CreaNodoHijo(ByVal IdOpcionMenu As String, ByVal nodeText As String, ByVal PermisoActivo As String, ByVal image As String, ByVal subItemStyle As ElementStyle) As Node
        'Dim NodoHijo As New Node(nodeText)
        Dim NodoHijo As Node = Nothing
        Dim IndicaTienePermiso As Boolean

        Try
            NodoHijo = New Node(nodeText)
            IndicaTienePermiso = False
            If PermisoActivo = "1" Then
                IndicaTienePermiso = True
            End If
            NodoHijo.ImageAlignment = eCellPartAlignment.CenterBottom
            NodoHijo.Image = ObtieneImagen(image)
            NodoHijo.CheckBoxVisible = True
            NodoHijo.Expanded = False
            NodoHijo.NodesIndent = ConvierteAInt(IdOpcionMenu)
            NodoHijo.Checked = IndicaTienePermiso
            NodoHijo.AccessibleRole = Windows.Forms.AccessibleRole.OutlineItem
            NodoHijo.Cells.Add(New Cell(nodeText, subItemStyle))
            AddHandler AdvTree1.AfterCheck, AddressOf AdvTree1_AfterCheck
            Return NodoHijo
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            'Return Nothing
            'Exit Function
            Throw ex
        End Try
    End Function

    Private Sub CreaNodosIniciales()
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node
        Dim subItemStyle As New ElementStyle()

        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near
            groupStyle.WordWrap = True
            AdvTree1.Styles.Add(groupStyle)

            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            AdvTree1.Styles.Add(subItemStyle)

            groupNode = New Node("Opciones", groupStyle)
            groupNode.NodesIndent = 1
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            AdvTree1.Nodes.Add(groupNode)

            groupNode = New Node("Agencias", groupStyle)
            groupNode.NodesIndent = 2
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            AdvTree1.Nodes.Add(groupNode)

            groupNode = New Node("Reportes", groupStyle)
            groupNode.NodesIndent = 3
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            AdvTree1.Nodes.Add(groupNode)

            groupNode = New Node("Descuentos", groupStyle)
            groupNode.NodesIndent = 4
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            AdvTree1.Nodes.Add(groupNode)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            'Return
            'Exit Sub
            Throw ex
        End Try
    End Sub

    Private Sub LlenaNodoDescuentos()
        Dim NodoDescuento As Node = Nothing
        Dim IdDescuento As Integer = 0
        Dim IdGrupo As Integer = 0
        Try
            Dim subItemStyle As New ElementStyle()
            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            subItemStyle.TextAlignment = eStyleTextAlignment.Near
            subItemStyle.TextLineAlignment = eStyleTextAlignment.Near
            subItemStyle.WordWrap = True

            dtDescuento = Nothing
            dtDescuento = RNUsuario.ObtieneDescuentosxUsuarioParaSeguridad(IdUsuario)
            If SUFunciones.ValidaDataTable(dtDescuento) Then
                If (dtDescuento.Rows.Count > 0) Then
                    For j As Integer = 0 To dtDescuento.Rows.Count - 1
                        NodoDescuento = AgregaDescuento(dtDescuento.Rows(j)("IdDescuento").ToString(), dtDescuento.Rows(j)("Descripcion").ToString(), dtDescuento.Rows(j)("IndicaDescuentoActivoUsuario").ToString(), "sign_percent_icon.png")
                    Next
                End If

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            ' Return
            Exit Sub
        End Try
    End Sub
    Private Function AgregaDescuento(ByVal IdNodo As String, ByVal Descripcion As String, ByVal IndicaPermisoDescuento As String, ByVal Image As String) As Node
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node
        Dim subItemStyle As New ElementStyle()
        Dim lbIndicaPermisoAgencia As Boolean = False
        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near
            groupStyle.WordWrap = True
            AdvTree1.Styles.Add(groupStyle)

            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            AdvTree1.Styles.Add(subItemStyle)

            If IndicaPermisoDescuento = "1" Then
                lbIndicaPermisoAgencia = True
            Else
                lbIndicaPermisoAgencia = False
            End If
            groupNode = New Node(Descripcion, groupStyle)
            groupNode.NodesIndent = ConvierteAInt(IdNodo)
            groupNode.Checked = lbIndicaPermisoAgencia
            groupNode.Image = ObtieneImagen(Image)
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            ' AdvTree1.Nodes.Add(groupNode)
            AdvTree1.Nodes.Item(3).Nodes.Add(groupNode)
            Return groupNode
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Throw ex
        End Try
    End Function
    Private Function AgregaModulo(ByVal IdNodo As String, ByVal DescripcionModulo As String, ByVal Image As String, ByVal IndicaTienePermiso As String) As Node
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node
        Dim subItemStyle As New ElementStyle()
        Dim blIndicaTienePermiso As Boolean = False
        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near
            groupStyle.WordWrap = True
            AdvTree1.Styles.Add(groupStyle)

            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            AdvTree1.Styles.Add(subItemStyle)

            If IndicaTienePermiso = "1" Then
                blIndicaTienePermiso = True
            Else
                blIndicaTienePermiso = False
            End If
            groupNode = New Node(DescripcionModulo, groupStyle)
            groupNode.NodesIndent = ConvierteAInt(IdNodo)
            groupNode.Image = ObtieneImagen(Image)
            groupNode.Expanded = False
            groupNode.Checked = blIndicaTienePermiso
            groupNode.CheckBoxVisible = True
            ' AdvTree1.Nodes.Add(groupNode)
            AdvTree1.Nodes.Item(0).Nodes.Add(groupNode)
            Return groupNode
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            'Return Nothing
            'Exit Function
            Throw ex
        End Try
    End Function
    Private Function AgregaAgencia(ByVal IdNodo As String, ByVal DescripcionAgencia As String, ByVal IndicaPermisoAgencia As String, ByVal Image As String) As Node
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node
        Dim subItemStyle As New ElementStyle()
        Dim lbIndicaPermisoAgencia As Boolean = False
        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near
            groupStyle.WordWrap = True
            AdvTree1.Styles.Add(groupStyle)

            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            AdvTree1.Styles.Add(subItemStyle)

            If IndicaPermisoAgencia = "1" Then
                lbIndicaPermisoAgencia = True
            Else
                lbIndicaPermisoAgencia = False
            End If
            groupNode = New Node(DescripcionAgencia, groupStyle)
            groupNode.NodesIndent = ConvierteAInt(IdNodo)
            groupNode.Checked = lbIndicaPermisoAgencia
            groupNode.Image = ObtieneImagen(Image)
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            ' AdvTree1.Nodes.Add(groupNode)
            AdvTree1.Nodes.Item(1).Nodes.Add(groupNode)
            Return groupNode
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Throw ex
        End Try
    End Function

    Private Function AgregaReporte(ByVal NodoPadre As Node, ByVal IdNodo As String, ByVal DescripcionAgencia As String, ByVal Image As String) As Node
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node
        Dim subItemStyle As New ElementStyle()

        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near
            groupStyle.WordWrap = True
            AdvTree1.Styles.Add(groupStyle)

            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            AdvTree1.Styles.Add(subItemStyle)

            groupNode = New Node(DescripcionAgencia, groupStyle)
            groupNode.NodesIndent = ConvierteAInt(IdNodo)
            groupNode.Image = ObtieneImagen(Image)
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            ' AdvTree1.Nodes.Add(groupNode)
            'AdvTree1.Nodes.Item(2).Nodes.Add(groupNode)
            NodoPadre.Nodes.Add(groupNode)
            Return groupNode
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Throw ex
            'Exit Function
        End Try
    End Function
    Private Function AgregaModuloReporte(ByVal IdNodo As String, ByVal DescripcionReporte As String, ByVal Image As String) As Node
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node
        Dim subItemStyle As New ElementStyle()

        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near
            groupStyle.WordWrap = True
            AdvTree1.Styles.Add(groupStyle)

            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            AdvTree1.Styles.Add(subItemStyle)

            groupNode = New Node(DescripcionReporte, groupStyle)
            groupNode.NodesIndent = ConvierteAInt(IdNodo)
            groupNode.Image = ObtieneImagen(Image)
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            ' AdvTree1.Nodes.Add(groupNode)
            AdvTree1.Nodes.Item(2).Nodes.Add(groupNode)
            Return groupNode
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Throw ex
            'Exit Function
        End Try
    End Function
    Private Function AgregaGrupo(ByVal IdGrupo As Integer, ByVal NodoPadre As Node, ByVal DescripcionModulo As String) As Node
        Dim groupStyle As New ElementStyle()
        Dim groupNode As Node

        Try
            groupStyle.TextColor = Color.Navy
            groupStyle.Font = New Font(AdvTree1.Font.FontFamily, 9.5F)
            groupStyle.Name = "groupstyle"
            groupStyle.TextAlignment = eStyleTextAlignment.Near
            groupStyle.TextLineAlignment = eStyleTextAlignment.Near

            AdvTree1.Styles.Add(groupStyle)

            groupNode = New Node(DescripcionModulo, groupStyle)
            groupNode.NodesIndent = ConvierteAInt(IdGrupo)
            groupNode.Expanded = False
            groupNode.CheckBoxVisible = True
            NodoPadre.Nodes.Add(groupNode)
            Return groupNode

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            'Return Nothing
            Throw ex
            'Exit Function
        End Try
    End Function
    Private Sub LlenaNodoPermisos()
        Dim NodoModulo As Node
        Dim IdModulo As Integer = 0
        Dim IdGrupo As Integer = 0
        Try

            AdvTree1.Nodes.Clear()
            AdvTree1.View = eView.Tile

            Dim subItemStyle As New ElementStyle()
            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            subItemStyle.TextAlignment = eStyleTextAlignment.Near
            subItemStyle.TextLineAlignment = eStyleTextAlignment.Near
            subItemStyle.WordWrap = True

            AdvTree1.Styles.Add(subItemStyle)
            CreaNodosIniciales()
            dtModulos = RNSeguridad.ObtieneModulosxUsuarioTodos(IdUsuario)
            If (dtModulos.Rows.Count > 0) Then
                For j As Integer = 0 To dtModulos.Rows.Count - 1
                    NodoModulo = AgregaModulo(dtModulos.Rows(j)("IdModulo").ToString(), dtModulos.Rows(j)("DesModulo").ToString(), dtModulos.Rows(j)("Icono").ToString(), dtModulos.Rows(j)("IndicaTienePermiso").ToString())
                    IdModulo = Convert.ToInt32(dtModulos.Rows(j)("IdModulo").ToString())
                    dtGrupos = RNSeguridad.ObtieneGruposxUsuarioYModuloTodos(IdUsuario, IdModulo)
                    If dtGrupos.Rows.Count > 0 Then
                        For x As Integer = 0 To dtGrupos.Rows.Count - 1
                            NodoModulo.Nodes.Add(CreaNodoHijo(dtGrupos.Rows(x)("IdGrupo").ToString(), dtGrupos.Rows(x)("DesGrupo").ToString(), dtGrupos.Rows(x)("IndicaTienePermiso").ToString(), "add1-64.png", subItemStyle))
                            IdGrupo = Convert.ToInt32(dtGrupos.Rows(x)("IdGrupo").ToString())
                            dtOpciones = RNSeguridad.ObtieneOpcionesxUsuarioYModuloYGrupoTodos(IdUsuario, IdModulo, IdGrupo)
                            If dtOpciones.Rows.Count > 0 Then
                                For i As Integer = 0 To dtOpciones.Rows.Count - 1
                                    NodoModulo.Nodes.Item(x).Nodes.Add(CreaNodoHijo(dtOpciones.Rows(i)("IdOpcionMenu").ToString(), dtOpciones.Rows(i)("DesOpcionMenu").ToString(), dtOpciones.Rows(i)("IndicaTienePermiso").ToString(), dtOpciones.Rows(i)("Icono").ToString(), subItemStyle))
                                Next
                            End If
                        Next
                    End If
                Next
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            ' Return
            'Exit Sub
            Throw ex
        End Try

    End Sub
    Private Sub LlenaNodoAgencias()
        Dim NodoAgencia As Node
        Dim IdAgencia As Integer = 0
        Dim IdGrupo As Integer = 0
        Try
            dtAgencias = RNAgencias.ObtieneAgenciasxUsuarioTodos(IdUsuario)
            If (dtAgencias.Rows.Count > 0) Then
                For j As Integer = 0 To dtAgencias.Rows.Count - 1
                    NodoAgencia = AgregaAgencia(dtAgencias.Rows(j)("registro").ToString(), dtAgencias.Rows(j)("Descripcion").ToString(), dtAgencias.Rows(j)("IndicaAgenciaAsignada").ToString(), "bank_transaction_icon.png")
                Next
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            ' Return
            Exit Sub
        End Try

    End Sub
    Private Sub LlenaNodoReportes()
        Dim NodoModuloReporte As Node
        Dim IdReporte As Integer = 0
        Dim IdGrupo As Integer = 0
        Dim IdModulo As Integer = 0
        Try
            Dim subItemStyle As New ElementStyle()
            subItemStyle.TextColor = Color.Gray
            subItemStyle.Name = "subitemstyle"
            subItemStyle.TextAlignment = eStyleTextAlignment.Near
            subItemStyle.TextLineAlignment = eStyleTextAlignment.Near
            subItemStyle.WordWrap = True

            dtModulosReportes = RNReportes.ObtieneModulosReportesxUsuario(IdUsuario)
            If (dtModulosReportes.Rows.Count > 0) Then
                For j As Integer = 0 To dtModulosReportes.Rows.Count - 1
                    NodoModuloReporte = AgregaModuloReporte(dtModulosReportes.Rows(j)("IdModulo").ToString(), dtModulosReportes.Rows(j)("DesModulo").ToString(), dtModulosReportes.Rows(j)("IconoModulo").ToString())
                    IdModulo = ConvierteAInt(dtModulosReportes.Rows(j)("IdModulo").ToString())
                    dtReportes = RNReportes.ObtieneReportesxModulosyUsuario(IdUsuario, IdModulo)
                    If (dtReportes.Rows.Count > 0) Then
                        For i As Integer = 0 To dtReportes.Rows.Count - 1
                            NodoModuloReporte.Nodes.Add(CreaNodoHijo(dtReportes.Rows(i)("IdReporte").ToString(), dtReportes.Rows(i)("DesReporte").ToString(), dtReportes.Rows(i)("IndicaReporteAsignado").ToString(), "", subItemStyle))
                        Next
                    End If
                Next
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            ' Return
            Exit Sub
        End Try

    End Sub
    Private Function ObtieneImagen(ByVal NombreImagen As String) As Image
        Dim imagen As Image = Nothing
        Try
            'Dim file_name As String = Application.
            Dim NombreCarpeta As String
            Dim NombreArchivo As String = String.Empty
            NombreCarpeta = System.IO.Directory.GetCurrentDirectory()
            If NombreCarpeta.EndsWith("\bin\Debug") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin\Debug", "")
            End If
            If NombreCarpeta.EndsWith("\bin") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin", "")
            End If
            NombreCarpeta = NombreCarpeta & "\Imagenes\TreeView\"
            NombreArchivo = NombreCarpeta & NombreImagen

            If System.IO.File.Exists(NombreArchivo) Then
                imagen = Image.FromFile(NombreArchivo)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            imagen = Nothing
            'Return Nothing
        End Try
        Return imagen
    End Function

    Private Sub frmOpcionesxUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            IngresaUsuario()
            RNUsuario.EliminaPermisosxUsuario(IdUsuario)
            RNAgencias.EliminaAgenciasxUsuario(IdUsuario)
            RNReportes.EliminaReportesxUsuario(IdUsuario)
            IngresaPermisosxUsuario()
            IngresaAgenciasxUsuario()
            IngresaReportesxUsuario()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If
    End Sub

    Private Sub frmOpcionesxUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Top = 0
        Me.Left = 0
        blnClear = False
        txtCollection.Add(txtCodigo)
        txtCollection.Add(txtNombres)
        txtCollection.Add(txtApellidos)
        txtCollection.Add(txtDireccion)
        txtCollection.Add(txtTelefono)
        txtCollection.Add(txtCargo)
        txtCollection.Add(txtClave)
        txtCollection.Add(txtConfirmacion)
        txtCollection.Add(txtRegistro)
        Limpiar()
        intModulo = 0
    End Sub

    Private Sub AdvTree1_AfterCheck(ByVal sender As System.Object, ByVal e As DevComponents.AdvTree.AdvTreeCellEventArgs) Handles AdvTree1.AfterCheck
        Dim Nodo As DevComponents.AdvTree.Node
        Dim NodoHijo As DevComponents.AdvTree.Node
        Dim NodoNieto As DevComponents.AdvTree.Node
        Try
            If Not e Is Nothing Then
                If Not e.Cell.TreeControl Is Nothing Then
                    If Not e.Cell.TreeControl.SelectedNode Is Nothing Then
                        If e.Cell.TreeControl.SelectedNode.Nodes.Count > 0 Then
                            For Each Nodo In e.Cell.TreeControl.SelectedNode.Nodes
                                Nodo.Checked = e.Cell.Parent.Checked
                                If Nodo.Nodes.Count > 0 Then
                                    For Each NodoHijo In Nodo.Nodes
                                        NodoHijo.Checked = Nodo.Checked
                                        If NodoHijo.Nodes.Count > 0 Then
                                            For Each NodoNieto In NodoHijo.Nodes
                                                NodoNieto.Checked = NodoHijo.Checked
                                            Next
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            ' Return
            Exit Sub
        End Try
    End Sub



    Private Sub cmbExEstado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbExEstado.SelectedIndexChanged

    End Sub
    'Private Function MoverRegistro(ByVal StrDato As String) As SqlDataReader
    Private Sub MoverRegistro(ByVal StrDato As String)
        'objReader = Nothing
        Try

            Select Case StrDato
                Case "Primero"
                    'strQuery = "Select Min(CodUsuario) from prm_Usuarios"
                    objReader = RNUsuario.ObtieneInformacionUsuario(txtCodigo.Text, 1)
                Case "Anterior"
                    'strQuery = "Select Max(CodUsuario) from prm_Usuarios Where CodUsuario < '" & txtCodigo.Text & "'"
                    objReader = RNUsuario.ObtieneInformacionUsuario(txtCodigo.Text, 2)
                Case "Siguiente"
                    'strQuery = "Select Min(CodUsuario) from prm_Usuarios Where CodUsuario > '" & txtCodigo.Text & "'"
                    objReader = RNUsuario.ObtieneInformacionUsuario(txtCodigo.Text, 3)
                Case "Ultimo"
                    'strQuery = "Select Max(CodUsuario) from prm_Usuarios"
                    objReader = RNUsuario.ObtieneInformacionUsuario(txtCodigo.Text, 4)
                Case "Igual"
                    'strQuery = "Select CodUsuario from prm_Usuarios Where CodUsuario = '" & txtCodigo.Text & "'"
                    objReader = RNUsuario.ObtieneInformacionUsuario(txtCodigo.Text, 0)
            End Select
            Limpiar()
            AsignaValores(objReader)
            strCodigoTmp = ""
            strCodigoTmp = txtCodigo.Text
            objReader.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            'Return Nothing
            Exit Sub
        End Try
        'UbicarRegistro(StrDato)
        'Return objReader
    End Sub
    Sub Limpiar()
        Dim Nodo As Node
        Dim NodoHijo As Node
        Dim NodoNieto As Node
        Try
            'For intIncr = 1 To txtCollection.Count - 1
            For intIncr = 1 To txtCollection.Count
                txtCollection.Item(intIncr).text = ""
            Next
            txtRegistro.Text = "0"
            For intIncr = 0 To 7
                strCambios(intIncr) = 0
            Next intIncr

            Nodo = Nothing
            NodoHijo = Nothing
            NodoNieto = Nothing
            'Limpia los permisos
            If AdvTree1.Nodes.Count >= 1 Then
                For Each Nodo In AdvTree1.Nodes.Item(0).Nodes
                    Nodo.Checked = False
                    If Nodo.Nodes.Count > 0 Then
                        For Each NodoHijo In Nodo.Nodes
                            NodoHijo.Checked = False
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    NodoNieto.Checked = False
                                Next
                            End If
                        Next
                    End If
                Next
            End If
            Nodo = Nothing
            NodoHijo = Nothing
            NodoNieto = Nothing

            If AdvTree1.Nodes.Count >= 2 Then
                'Limpia las sucursales
                For Each Nodo In AdvTree1.Nodes.Item(1).Nodes
                    Nodo.Checked = False
                    If Nodo.Nodes.Count > 0 Then
                        For Each NodoHijo In Nodo.Nodes
                            NodoHijo.Checked = False
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    NodoNieto.Checked = False
                                Next
                            End If
                        Next
                    End If
                Next

            End If

            If AdvTree1.Nodes.Count >= 3 Then
                'Limpia las sucursales
                For Each Nodo In AdvTree1.Nodes.Item(1).Nodes
                    Nodo.Checked = False
                    If Nodo.Nodes.Count > 0 Then
                        For Each NodoHijo In Nodo.Nodes
                            NodoHijo.Checked = False
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    NodoNieto.Checked = False
                                Next
                            End If
                        Next
                    End If
                Next

            End If

            'cmbExEstado.SelectedIndex = 0
            'cmbExImpresora.SelectedIndex = 0
            If cmbExEstado.Items.Count > 0 Then
                cmbExEstado.SelectedIndex = -1
            End If
            If cmbExImpresora.Items.Count > 0 Then
                cmbExImpresora.SelectedIndex = -1
            End If
            txtCodigo.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub
    Private Sub cmbExEstado_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbExEstado.KeyDown, txtApellidos.KeyDown, txtCargo.KeyDown, txtClave.KeyDown, txtCodigo.KeyDown, txtDireccion.KeyDown, txtNombres.KeyDown, txtConfirmacion.KeyDown, cmbExImpresora.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Select Case sender.name.ToString
                    Case "txtCodigo" : MoverRegistro("Igual") : txtNombres.Focus()
                    Case "txtNombres" : txtApellidos.Focus()
                    Case "txtApellidos" : txtDireccion.Focus()
                    Case "txtDireccion" : txtTelefono.Focus()
                    Case "txtTelefono" : txtCargo.Focus()
                    Case "txtCargo" : txtClave.Focus()
                    Case "txtClave" : txtConfirmacion.Focus()
                    Case "txtConfirmacion" : cmbExEstado.Focus()
                    Case "CmbExEstado" : cmbExImpresora.Focus()
                    Case "CmbExImpresora" : txtCodigo.Focus()
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub

    Private Sub AsignaValores(ByVal drDatos As IDataReader)
        Dim ExisteUsuario As Boolean = False
        Try
            While drDatos.Read
                txtRegistro.Text = drDatos.Item("registro")
                txtCodigo.Text = drDatos.Item("CodUsuario")
                txtNombres.Text = drDatos.Item("Nombre")
                txtApellidos.Text = drDatos.Item("Apellido")
                txtDireccion.Text = drDatos.Item("Direccion")
                txtTelefono.Text = drDatos.Item("Telefono")
                txtCargo.Text = drDatos.Item("Cargo")
                txtClave.Text = drDatos.Item("Clave")
                txtConfirmacion.Text = drDatos.Item("Clave")
                cmbExEstado.SelectedIndex = SUConversiones.ConvierteAInt(drDatos.Item("Estado"))
                cmbExImpresora.SelectedIndex = SUConversiones.ConvierteAInt(drDatos.Item("Impresora"))
                ExisteUsuario = True
                IdUsuario = drDatos.Item("registro")
                For intIncr = 0 To 5
                    strCambios(intIncr) = drDatos.GetValue(intIncr + 2)
                Next intIncr
            End While
            strCambios(6) = cmbExEstado.SelectedValue
            strCambios(7) = cmbExImpresora.SelectedValue
            If Not ExisteUsuario Then
                MsgBox("No existe un registro con ese código en la tabla", MsgBoxStyle.Information, "Extracción de Registro")
                Exit Sub
            End If
            LlenaNodoPermisos()
            LlenaNodoAgencias()
            LlenaNodoReportes()
            LlenaNodoDescuentos()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub

    Private Sub IngresaPermisosxUsuario()
        Dim Nodo As Node = Nothing
        Dim NodoHijo As Node = Nothing
        Dim NodoNieto As Node = Nothing
        Try
            Nodo = Nothing
            NodoHijo = Nothing
            NodoNieto = Nothing
            'Limpia los permisos
            If AdvTree1.Nodes.Count >= 1 Then
                For Each Nodo In AdvTree1.Nodes.Item(0).Nodes
                    If Nodo.Nodes.Count > 0 Then
                        For Each NodoHijo In Nodo.Nodes
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    If NodoNieto.Checked Then
                                        RNUsuario.IngresaPermisoUsuario(IdUsuario, NodoNieto.NodesIndent)
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub

    Private Sub IngresaAgenciasxUsuario()
        Dim Nodo As Node = Nothing
        Dim NodoHijo As Node = Nothing
        Dim NodoNieto As Node = Nothing

        Try
            Nodo = Nothing
            NodoHijo = Nothing
            NodoNieto = Nothing
            'Limpia los permisos
            If AdvTree1.Nodes.Count >= 2 Then
                For Each Nodo In AdvTree1.Nodes.Item(1).Nodes
                    If AdvTree1.Nodes.Item(1).Nodes.Count > 0 Then
                        'If Nodo.Nodes.Count > 0 Then
                        If Nodo.Checked Then
                            RNUsuario.IngresaAgenciaxUsuario(IdUsuario, Nodo.NodesIndent)
                        End If
                        For Each NodoHijo In Nodo.Nodes
                            If NodoHijo.Checked Then
                                RNUsuario.IngresaAgenciaxUsuario(IdUsuario, NodoHijo.NodesIndent)
                            End If
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    If NodoNieto.Checked Then
                                        RNUsuario.IngresaAgenciaxUsuario(IdUsuario, NodoNieto.NodesIndent)
                                    End If
                                Next
                            End If
                        Next
                        'End If
                    End If
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub
    Private Sub IngresaReportesxUsuario()
        Dim Nodo As Node = Nothing
        Dim NodoHijo As Node = Nothing
        Dim NodoNieto As Node = Nothing

        Try
            Nodo = Nothing
            NodoHijo = Nothing
            NodoNieto = Nothing
            'Limpia los permisos
            If AdvTree1.Nodes.Count >= 3 Then
                For Each Nodo In AdvTree1.Nodes.Item(2).Nodes
                    If AdvTree1.Nodes.Item(2).Nodes.Count > 0 Then
                        'If Nodo.Checked Then
                        '    RNUsuario.IngresaReportesxUsuario(IdUsuario, Nodo.NodesIndent)
                        'End If
                        For Each NodoHijo In Nodo.Nodes
                            If NodoHijo.Checked Then
                                RNUsuario.IngresaReportesxUsuario(IdUsuario, NodoHijo.NodesIndent)
                            End If
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    If NodoNieto.Checked Then
                                        RNUsuario.IngresaReportesxUsuario(IdUsuario, NodoNieto.NodesIndent)
                                    End If
                                Next
                            End If
                        Next
                        'End If
                    End If
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try

    End Sub
    Private Sub IngresaUsuario()
        Dim Estado As Integer = 0
        Dim Impresora As Integer = 0
        Try
            Estado = cmbExEstado.SelectedIndex
            Impresora = cmbExImpresora.SelectedIndex
            RNUsuario.IngresaUsuario(0, txtCodigo.Text, txtNombres.Text, txtApellidos.Text, txtDireccion.Text, txtTelefono.Text,
                                     txtCargo.Text, txtClave.Text, Estado, Impresora, 1)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try

    End Sub
    Private Sub IngresaDescuentoxUsuario()
        Dim Nodo As Node = Nothing
        Dim NodoHijo As Node = Nothing
        Dim NodoNieto As Node = Nothing

        Try
            Nodo = Nothing
            NodoHijo = Nothing
            NodoNieto = Nothing
            'Limpia los permisos
            If AdvTree1.Nodes.Count >= 3 Then
                For Each Nodo In AdvTree1.Nodes.Item(3).Nodes
                    If AdvTree1.Nodes.Item(3).Nodes.Count > 0 Then
                        'If Nodo.Nodes.Count > 0 Then
                        If Nodo.Checked Then
                            RNUsuario.IngresaDescuentosxUsuarioParaSeguridad(IdUsuario, Nodo.NodesIndent)
                        End If
                        For Each NodoHijo In Nodo.Nodes
                            If NodoHijo.Checked Then
                                RNUsuario.IngresaDescuentosxUsuarioParaSeguridad(IdUsuario, Nodo.NodesIndent)
                            End If
                            If NodoHijo.Nodes.Count > 0 Then
                                For Each NodoNieto In NodoHijo.Nodes
                                    If NodoNieto.Checked Then
                                        RNUsuario.IngresaDescuentosxUsuarioParaSeguridad(IdUsuario, Nodo.NodesIndent)
                                    End If
                                Next
                            End If
                        Next
                        'End If
                    End If
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            IngresaUsuario()
            RNUsuario.EliminaPermisosxUsuario(IdUsuario)
            RNAgencias.EliminaAgenciasxUsuario(IdUsuario)
            RNReportes.EliminaReportesxUsuario(IdUsuario)
            RNUsuario.EliminaDescuentosxUsuarioParaSeguridad(IdUsuario)
            IngresaPermisosxUsuario()
            IngresaAgenciasxUsuario()
            IngresaReportesxUsuario()
            IngresaDescuentoxUsuario()
            MsgBox("Cambio Aplicado exitosamente")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error en la aplicacion, favor contacte al administrador." & ex.Message, MsgBoxStyle.Critical, "Permisos Usuario")
            Exit Sub
        End Try
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        MoverRegistro("Primero")
    End Sub

    Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
        MoverRegistro("Anterior")
    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        MoverRegistro("Siguiente")
    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        MoverRegistro("Ultimo")
    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        MoverRegistro("Igual")
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        'Dim frmNew As New frmGeneral
        'lngRegistro = txtRegistro.Text
        ''Timer1.Interval = 200
        ''Timer1.Enabled = True
        'frmNew.ShowDialog()
    End Sub

    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Dim frmNew As New frmBitacora
        lngRegistro = txtRegistro.Text
        frmNew.ShowDialog()
    End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Dim frmNew As New actrptViewer
        intRptFactura = 30
        intModulo = 50
        strQuery = "exec sp_Reportes " & 0 & " "
        frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
        frmNew.Show()
    End Sub
End Class