Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmClientes
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cntmnuClientesRg As System.Windows.Forms.ContextMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox11 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnListado As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnBusqueda As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnGeneral As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCambios As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReportes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnRegistros As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrimerRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAnteriorRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSiguienteRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUltimoRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnIgual As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents btnRptClienteCtaCont As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label25 As Label
    Friend WithEvents txtSaldoUSD As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents cmbCuentaContable As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClientes))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.cntmnuClientesRg = New System.Windows.Forms.ContextMenu()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem14 = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItem16 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.ComboBox12 = New System.Windows.Forms.ComboBox()
        Me.ComboBox11 = New System.Windows.Forms.ComboBox()
        Me.ComboBox10 = New System.Windows.Forms.ComboBox()
        Me.ComboBox9 = New System.Windows.Forms.ComboBox()
        Me.ComboBox8 = New System.Windows.Forms.ComboBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtSaldoUSD = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.ComboBox7 = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.btnListado = New DevComponents.DotNetBar.ButtonItem()
        Me.btnBusqueda = New DevComponents.DotNetBar.ButtonItem()
        Me.btnGeneral = New DevComponents.DotNetBar.ButtonItem()
        Me.btnCambios = New DevComponents.DotNetBar.ButtonItem()
        Me.btnReportes = New DevComponents.DotNetBar.ButtonItem()
        Me.btnRptClienteCtaCont = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.btnRegistros = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrimerRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAnteriorRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSiguienteRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUltimoRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnIgual = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.cmbCuentaContable = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8, Me.MenuItem10})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 4
        Me.MenuItem10.Text = "Igual"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem14, Me.MenuItem13, Me.MenuItem15, Me.MenuItem9, Me.MenuItem16})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 0
        Me.MenuItem14.Text = "Buscar"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 1
        Me.MenuItem13.Text = "General"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 2
        Me.MenuItem15.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 3
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 4
        Me.MenuItem16.Text = "Clientes con cuenta contable"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'Timer1
        '
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbAgencias)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.ComboBox12)
        Me.GroupBox1.Controls.Add(Me.ComboBox11)
        Me.GroupBox1.Controls.Add(Me.ComboBox10)
        Me.GroupBox1.Controls.Add(Me.ComboBox9)
        Me.GroupBox1.Controls.Add(Me.ComboBox8)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.ComboBox6)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.ComboBox5)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.ComboBox4)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 76)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(871, 186)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de los Clientes"
        '
        'cmbAgencias
        '
        Me.cmbAgencias.DisplayMember = "Text"
        Me.cmbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbAgencias.FormattingEnabled = True
        Me.cmbAgencias.ItemHeight = 14
        Me.cmbAgencias.Location = New System.Drawing.Point(96, 100)
        Me.cmbAgencias.Name = "cmbAgencias"
        Me.cmbAgencias.Size = New System.Drawing.Size(657, 20)
        Me.cmbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbAgencias.TabIndex = 52
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(8, 100)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(53, 13)
        Me.Label25.TabIndex = 53
        Me.Label25.Text = "Agencia"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(144, 24)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(8, 20)
        Me.TextBox17.TabIndex = 29
        Me.TextBox17.Text = "0"
        Me.TextBox17.Visible = False
        '
        'ComboBox12
        '
        Me.ComboBox12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox12.Location = New System.Drawing.Point(530, 157)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox12.TabIndex = 28
        Me.ComboBox12.Visible = False
        '
        'ComboBox11
        '
        Me.ComboBox11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox11.Location = New System.Drawing.Point(624, 157)
        Me.ComboBox11.Name = "ComboBox11"
        Me.ComboBox11.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox11.TabIndex = 27
        Me.ComboBox11.Visible = False
        '
        'ComboBox10
        '
        Me.ComboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox10.Location = New System.Drawing.Point(555, 157)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox10.TabIndex = 26
        Me.ComboBox10.Visible = False
        '
        'ComboBox9
        '
        Me.ComboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox9.Location = New System.Drawing.Point(654, 157)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(16, 21)
        Me.ComboBox9.TabIndex = 25
        Me.ComboBox9.Visible = False
        '
        'ComboBox8
        '
        Me.ComboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox8.Location = New System.Drawing.Point(336, 8)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox8.TabIndex = 24
        Me.ComboBox8.Visible = False
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(623, 50)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(128, 20)
        Me.DateTimePicker1.TabIndex = 23
        Me.DateTimePicker1.Tag = "Fecha de ingreso"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(559, 50)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(49, 13)
        Me.Label19.TabIndex = 22
        Me.Label19.Text = "Ingreso"
        '
        'ComboBox6
        '
        Me.ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox6.Location = New System.Drawing.Point(460, 156)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(289, 21)
        Me.ComboBox6.TabIndex = 21
        Me.ComboBox6.Tag = "Municipio donde reside el cliente"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(396, 156)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Municipio"
        '
        'ComboBox5
        '
        Me.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox5.Location = New System.Drawing.Point(96, 156)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(292, 21)
        Me.ComboBox5.TabIndex = 19
        Me.ComboBox5.Tag = "Departamento donde reside el cliente"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 156)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Departamento"
        '
        'ComboBox4
        '
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.Location = New System.Drawing.Point(461, 126)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(289, 21)
        Me.ComboBox4.TabIndex = 17
        Me.ComboBox4.Tag = "Vendedore asignado al cliente"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(396, 126)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Vendedor"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Location = New System.Drawing.Point(96, 126)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(292, 21)
        Me.ComboBox3.TabIndex = 15
        Me.ComboBox3.Tag = "Tipo de negocio del cliente"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 126)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Negocio"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(96, 75)
        Me.TextBox4.MaxLength = 80
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(656, 20)
        Me.TextBox4.TabIndex = 13
        Me.TextBox4.Tag = "Direcci�n del cliente"
        Me.TextBox4.Text = "TextBox4"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Direcci�n"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(96, 50)
        Me.TextBox3.MaxLength = 50
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(449, 20)
        Me.TextBox3.TabIndex = 11
        Me.TextBox3.Tag = "Nombre completo del cliente"
        Me.TextBox3.Text = "TextBox3"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 50)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Nombre"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(623, 24)
        Me.TextBox2.MaxLength = 20
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(128, 20)
        Me.TextBox2.TabIndex = 9
        Me.TextBox2.Tag = "N�mero de identificaci�n"
        Me.TextBox2.Text = "TextBox2"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Location = New System.Drawing.Point(416, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(128, 21)
        Me.ComboBox2.TabIndex = 7
        Me.ComboBox2.Tag = "Tipo de identificaci�n"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"natural", "juridico"})
        Me.ComboBox1.Location = New System.Drawing.Point(247, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(115, 21)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.Tag = "Tipo de cliente"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(96, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(114, 20)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.Tag = "C�digo del cliente"
        Me.TextBox1.Text = "TextBox1"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(559, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "N�meroID"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(368, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "TipoID"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(215, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tipo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox11)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.TextBox10)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.TextBox8)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.TextBox7)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.TextBox6)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.TextBox5)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 269)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(871, 151)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de los Clientes"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(96, 119)
        Me.TextBox11.MaxLength = 50
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(654, 20)
        Me.TextBox11.TabIndex = 23
        Me.TextBox11.Tag = "Nombre del contacto"
        Me.TextBox11.Text = "TextBox11"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(8, 119)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(69, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Contacto 2"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(96, 94)
        Me.TextBox10.MaxLength = 50
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(654, 20)
        Me.TextBox10.TabIndex = 21
        Me.TextBox10.Tag = "Nombre del contacto"
        Me.TextBox10.Text = "TextBox10"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(8, 94)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(69, 13)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "Contacto 1"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(96, 69)
        Me.TextBox9.MaxLength = 80
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(654, 20)
        Me.TextBox9.TabIndex = 19
        Me.TextBox9.Tag = "Direcci�n del negocio"
        Me.TextBox9.Text = "TextBox9"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 69)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(61, 13)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = "Direcci�n"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(460, 44)
        Me.TextBox8.MaxLength = 50
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(290, 20)
        Me.TextBox8.TabIndex = 17
        Me.TextBox8.Tag = "Nombre del negocio del cliente"
        Me.TextBox8.Text = "TextBox8"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(396, 44)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 13)
        Me.Label14.TabIndex = 16
        Me.Label14.Text = "Negocio"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(96, 44)
        Me.TextBox7.MaxLength = 30
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(292, 20)
        Me.TextBox7.TabIndex = 15
        Me.TextBox7.Tag = "Correo electr�nico"
        Me.TextBox7.Text = "TextBox7"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 44)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(37, 13)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Email"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(460, 19)
        Me.TextBox6.MaxLength = 30
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(290, 20)
        Me.TextBox6.TabIndex = 13
        Me.TextBox6.Tag = "Fax del cliente"
        Me.TextBox6.Text = "TextBox6"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(396, 19)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(27, 13)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "Fax"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(96, 19)
        Me.TextBox5.MaxLength = 30
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(292, 20)
        Me.TextBox5.TabIndex = 11
        Me.TextBox5.Tag = "Tel�fono del cliente"
        Me.TextBox5.Text = "TextBox5"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 19)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(57, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Tel�fono"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmbCuentaContable)
        Me.GroupBox3.Controls.Add(Me.txtSaldoUSD)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.TextBox16)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.ComboBox7)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.TextBox14)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.TextBox12)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 425)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(871, 88)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos de los Clientes"
        '
        'txtSaldoUSD
        '
        Me.txtSaldoUSD.Location = New System.Drawing.Point(663, 24)
        Me.txtSaldoUSD.MaxLength = 18
        Me.txtSaldoUSD.Name = "txtSaldoUSD"
        Me.txtSaldoUSD.ReadOnly = True
        Me.txtSaldoUSD.Size = New System.Drawing.Size(104, 20)
        Me.txtSaldoUSD.TabIndex = 23
        Me.txtSaldoUSD.Tag = "Saldo del cliente d�lares"
        Me.txtSaldoUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(557, 26)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(67, 13)
        Me.Label26.TabIndex = 22
        Me.Label26.Text = "Saldo US$"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(663, 56)
        Me.TextBox16.MaxLength = 18
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(109, 20)
        Me.TextBox16.TabIndex = 21
        Me.TextBox16.Tag = "Saldo pendiente a favor del cliente"
        Me.TextBox16.Text = "TextBox16"
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(557, 56)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(100, 13)
        Me.Label24.TabIndex = 20
        Me.Label24.Text = "Saldo Pendiente"
        '
        'ComboBox7
        '
        Me.ComboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox7.Items.AddRange(New Object() {"activo", "inactivo", "eliminado"})
        Me.ComboBox7.Location = New System.Drawing.Point(434, 56)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(116, 21)
        Me.ComboBox7.TabIndex = 19
        Me.ComboBox7.Tag = "Estado del cliente en el sistema"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(384, 56)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 13)
        Me.Label23.TabIndex = 18
        Me.Label23.Text = "Estado"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 56)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(80, 13)
        Me.Label22.TabIndex = 16
        Me.Label22.Text = "Cta Contable"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(814, 24)
        Me.TextBox14.MaxLength = 3
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(48, 20)
        Me.TextBox14.TabIndex = 15
        Me.TextBox14.Tag = "D�as de plazo otorgado en facturas de cr�dito"
        Me.TextBox14.Text = "TextBox14"
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(775, 26)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(38, 13)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Plazo"
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(434, 24)
        Me.TextBox13.MaxLength = 18
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ReadOnly = True
        Me.TextBox13.Size = New System.Drawing.Size(116, 20)
        Me.TextBox13.TabIndex = 13
        Me.TextBox13.Tag = "Saldo del cliente"
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(385, 26)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(39, 13)
        Me.Label20.TabIndex = 12
        Me.Label20.Text = "Saldo"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(89, 24)
        Me.TextBox12.MaxLength = 18
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(286, 20)
        Me.TextBox12.TabIndex = 11
        Me.TextBox12.Tag = "Limite del cr�dito otorgado"
        Me.TextBox12.Text = "TextBox12"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 26)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(68, 13)
        Me.Label18.TabIndex = 10
        Me.Label18.Text = "Limite US$"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 527)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(871, 23)
        Me.UltraStatusBar1.TabIndex = 19
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnListado, Me.LabelItem4, Me.btnRegistros, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(871, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 52
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnListado
        '
        Me.btnListado.AutoExpandOnClick = True
        Me.btnListado.Image = CType(resources.GetObject("btnListado.Image"), System.Drawing.Image)
        Me.btnListado.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnListado.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnListado.Name = "btnListado"
        Me.btnListado.OptionGroup = "Listado de productos"
        Me.btnListado.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnBusqueda, Me.btnGeneral, Me.btnCambios, Me.btnReportes, Me.btnRptClienteCtaCont})
        Me.btnListado.Text = "Listados"
        Me.btnListado.Tooltip = "Listados"
        '
        'btnBusqueda
        '
        Me.btnBusqueda.Image = CType(resources.GetObject("btnBusqueda.Image"), System.Drawing.Image)
        Me.btnBusqueda.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnBusqueda.Name = "btnBusqueda"
        Me.btnBusqueda.Text = "Buscar"
        Me.btnBusqueda.Tooltip = "Realiza busqueda"
        '
        'btnGeneral
        '
        Me.btnGeneral.Image = CType(resources.GetObject("btnGeneral.Image"), System.Drawing.Image)
        Me.btnGeneral.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnGeneral.Name = "btnGeneral"
        Me.btnGeneral.Text = "General"
        Me.btnGeneral.Tooltip = "General"
        '
        'btnCambios
        '
        Me.btnCambios.Image = CType(resources.GetObject("btnCambios.Image"), System.Drawing.Image)
        Me.btnCambios.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnCambios.Name = "btnCambios"
        Me.btnCambios.Tag = "Cambios"
        Me.btnCambios.Text = "Cambios"
        Me.btnCambios.Tooltip = "Cambios"
        '
        'btnReportes
        '
        Me.btnReportes.Image = CType(resources.GetObject("btnReportes.Image"), System.Drawing.Image)
        Me.btnReportes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Tag = "Reportes"
        Me.btnReportes.Text = "Reportes"
        Me.btnReportes.Tooltip = "Reportes"
        '
        'btnRptClienteCtaCont
        '
        Me.btnRptClienteCtaCont.Name = "btnRptClienteCtaCont"
        Me.btnRptClienteCtaCont.Text = "Reporte Clientes Cuenta Contable"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'btnRegistros
        '
        Me.btnRegistros.AutoExpandOnClick = True
        Me.btnRegistros.Image = CType(resources.GetObject("btnRegistros.Image"), System.Drawing.Image)
        Me.btnRegistros.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnRegistros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.OptionGroup = "Recorre Registros"
        Me.btnRegistros.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPrimerRegistro, Me.btnAnteriorRegistro, Me.btnSiguienteRegistro, Me.btnUltimoRegistro, Me.btnIgual})
        Me.btnRegistros.Text = "Recorre Registros"
        Me.btnRegistros.Tooltip = "Recorre Registros"
        '
        'btnPrimerRegistro
        '
        Me.btnPrimerRegistro.Image = CType(resources.GetObject("btnPrimerRegistro.Image"), System.Drawing.Image)
        Me.btnPrimerRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrimerRegistro.Name = "btnPrimerRegistro"
        Me.btnPrimerRegistro.Text = "Primer registro"
        Me.btnPrimerRegistro.Tooltip = "Primer registro"
        '
        'btnAnteriorRegistro
        '
        Me.btnAnteriorRegistro.Image = CType(resources.GetObject("btnAnteriorRegistro.Image"), System.Drawing.Image)
        Me.btnAnteriorRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnAnteriorRegistro.Name = "btnAnteriorRegistro"
        Me.btnAnteriorRegistro.Text = "Anterior registro"
        Me.btnAnteriorRegistro.Tooltip = "Anterior registro"
        '
        'btnSiguienteRegistro
        '
        Me.btnSiguienteRegistro.Image = CType(resources.GetObject("btnSiguienteRegistro.Image"), System.Drawing.Image)
        Me.btnSiguienteRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnSiguienteRegistro.Name = "btnSiguienteRegistro"
        Me.btnSiguienteRegistro.Tag = "Siguiente registro"
        Me.btnSiguienteRegistro.Text = "Siguiente registro"
        Me.btnSiguienteRegistro.Tooltip = "Siguiente registro"
        '
        'btnUltimoRegistro
        '
        Me.btnUltimoRegistro.Image = CType(resources.GetObject("btnUltimoRegistro.Image"), System.Drawing.Image)
        Me.btnUltimoRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnUltimoRegistro.Name = "btnUltimoRegistro"
        Me.btnUltimoRegistro.Tag = "Ultimo registro"
        Me.btnUltimoRegistro.Text = "Ultimo registro"
        Me.btnUltimoRegistro.Tooltip = "Ultimo registro"
        '
        'btnIgual
        '
        Me.btnIgual.Image = CType(resources.GetObject("btnIgual.Image"), System.Drawing.Image)
        Me.btnIgual.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnIgual.Name = "btnIgual"
        Me.btnIgual.Tag = "Igual"
        Me.btnIgual.Text = "Igual"
        Me.btnIgual.Tooltip = "Igual"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'cmbCuentaContable
        '
        Me.cmbCuentaContable.DisplayMember = "Text"
        Me.cmbCuentaContable.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbCuentaContable.FormattingEnabled = True
        Me.cmbCuentaContable.ItemHeight = 14
        Me.cmbCuentaContable.Location = New System.Drawing.Point(89, 56)
        Me.cmbCuentaContable.Name = "cmbCuentaContable"
        Me.cmbCuentaContable.Size = New System.Drawing.Size(286, 20)
        Me.cmbCuentaContable.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbCuentaContable.TabIndex = 24
        '
        'frmClientes
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(871, 550)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmClientes"
        Me.Text = "frmClientes"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strCambios(21) As String
    Dim txtCollection As New Collection
    Dim cboCollection As New Collection
    Dim bytNegocio, bytVendedor, bytDepartamento, bytMunicipio As Int16
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmClientes"
    Private Sub frmClientes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            txtCollection.Add(TextBox1)
            txtCollection.Add(TextBox2)
            txtCollection.Add(TextBox3)
            txtCollection.Add(TextBox4)
            txtCollection.Add(TextBox5)
            txtCollection.Add(TextBox6)
            txtCollection.Add(TextBox7)
            txtCollection.Add(TextBox8)
            txtCollection.Add(TextBox9)
            txtCollection.Add(TextBox10)
            txtCollection.Add(TextBox11)
            txtCollection.Add(TextBox12)
            txtCollection.Add(TextBox13)
            txtCollection.Add(TextBox14)
            ' txtCollection.Add(TextBox15)
            cboCollection.Add(ComboBox1)
            cboCollection.Add(ComboBox2)
            cboCollection.Add(ComboBox3)
            cboCollection.Add(ComboBox4)
            cboCollection.Add(ComboBox5)
            cboCollection.Add(ComboBox6)
            cboCollection.Add(ComboBox7)
            cboCollection.Add(ComboBox8)
            cboCollection.Add(ComboBox9)
            cboCollection.Add(ComboBox10)
            cboCollection.Add(ComboBox11)
            cboCollection.Add(ComboBox12)
            CreateMyContextMenu()
            Iniciar()
            Limpiar()
            intModulo = 5
            LlenarComboSucursales()
            LlenarComboCatalogoContable()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Private Sub LlenarComboCatalogoContable()
        Try
            RNCuentaContable.CargarComboCatalogoContable(cmbCuentaContable)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

        'cmbAgencias.ValueMember = lngRegAgencia
    End Sub
    Private Sub frmClientes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Guardar()
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 2
                Dim frmNew As New frmListadoAyuda
                blnUbicar = False
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click
        Try


            Select Case sender.text.ToString
                Case "Guardar" : Guardar()
                Case "Cancelar", "Limpiar" : Limpiar()
                Case "Salir" : Me.Close()
                Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
                Case "General"

                Case "Cambios"
                    Dim frmNew As New frmBitacora
                    lngRegistro = TextBox17.Text
                    frmNew.ShowDialog()
                Case "Reporte"
                    Dim frmNew As New actrptViewer
                    intRptFactura = 30
                    strQuery = "exec sp_Reportes " & intModulo & " "
                    frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                    frmNew.Show()
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub LlenarComboSucursales()
        Try
            RNAgencias.CargarComboAgenciasSinDefault(cmbAgencias, lngRegUsuario)
            cmbAgencias.SelectedValue = RNAgencias.ObtieneAgenciaDefault()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Guardar()
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        MoverRegistro(sender.text)
        '    Case 4
        '        Me.Close()
        'End Select

    End Sub

    Sub MoverRegistro(ByVal StrDato As String)
        Try


            strQuery = ""
            Select Case StrDato
                Case "Primero"
                    strQuery = "Select Min(Codigo) from prm_Clientes"
                Case "Anterior"
                    strQuery = "Select Max(Codigo) from prm_Clientes Where codigo < '" & TextBox1.Text & "'"
                Case "Siguiente"
                    strQuery = "Select Min(Codigo) from prm_Clientes Where codigo > '" & TextBox1.Text & "'"
                Case "Ultimo"
                    strQuery = "Select Max(Codigo) from prm_Clientes"
                Case "Igual"
                    strQuery = "Select Codigo from prm_Clientes Where codigo = '" & TextBox1.Text & "'"
                    strCodigoTmp = ""
                    strCodigoTmp = TextBox1.Text
            End Select
            Limpiar()
            UbicarRegistro(StrDato)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim lngFecha As Long
        Dim strCodigo As String

        Try


            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            SUFunciones.CierraComando(cmdAgro2K)
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            strCodigo = ""
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    strCodigo = dtrAgro2K.GetValue(0)
                End If
            End While
            dtrAgro2K.Close()
            If strCodigo = "" Then
                If StrDato = "Igual" Then
                    TextBox1.Text = strCodigoTmp
                    MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
                Else
                    MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
                End If
                SUFunciones.CierraConexionBD(cnnAgro2K)
                SUFunciones.CierraComando(cmdAgro2K)
                'cmdAgro2K.Connection.Close()
                'cnnAgro2K.Close()
                Exit Sub
            End If
            strQuery = ""
            'strQuery = "Select * From prm_Clientes Where Codigo = '" & strCodigo & "'"
            strQuery = "exec dbo.ConsultaClientesxCodigoCliente '" & strCodigo & "'"

            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                TextBox17.Text = dtrAgro2K.GetValue(0)
                TextBox1.Text = dtrAgro2K.GetValue(1)
                ComboBox1.SelectedIndex = dtrAgro2K.GetValue(2)
                ComboBox2.SelectedIndex = dtrAgro2K.GetValue(3) - 1
                TextBox2.Text = dtrAgro2K.GetValue(4)
                TextBox3.Text = dtrAgro2K.GetValue(5)
                TextBox4.Text = dtrAgro2K.GetValue(8)
                txtSaldoUSD.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("SaldoUSD")), "##,##0.#0")
                bytNegocio = dtrAgro2K.GetValue(9)
                bytVendedor = dtrAgro2K.GetValue(10)
                bytDepartamento = dtrAgro2K.GetValue(11)
                bytMunicipio = dtrAgro2K.GetValue(12)
                For intIncr = 5 To 14
                    txtCollection(intIncr).Text = dtrAgro2K.GetValue(intIncr + 8)
                Next intIncr
                TextBox12.Text = Format(dtrAgro2K.GetValue(20), "##,##0.#0")
                TextBox13.Text = Format(dtrAgro2K.GetValue(21), "##,##0.#0")
                ComboBox7.SelectedIndex = dtrAgro2K.GetValue(24)
                TextBox16.Text = Format(dtrAgro2K.GetValue(25), "##,##0.#0")
                lngFecha = dtrAgro2K.GetValue(7)
                DateTimePicker1.Value = DefinirFecha(lngFecha)
                DateTimePicker1.Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
                cmbAgencias.SelectedValue = SUConversiones.ConvierteAInt(dtrAgro2K.Item("IdSucursal"))
                cmbCuentaContable.SelectedValue = dtrAgro2K.Item("cta_contable")
            End While
            ComboBox9.SelectedIndex = -1
            For intIncr = 0 To ComboBox9.Items.Count - 1
                ComboBox9.SelectedIndex = intIncr
                If ComboBox9.SelectedItem = bytNegocio Then
                    ComboBox3.SelectedIndex = ComboBox9.SelectedIndex
                    Exit For
                End If
            Next intIncr
            ComboBox10.SelectedIndex = -1
            For intIncr = 0 To ComboBox10.Items.Count - 1
                ComboBox10.SelectedIndex = intIncr
                If ComboBox10.SelectedItem = bytVendedor Then
                    ComboBox4.SelectedIndex = ComboBox10.SelectedIndex
                    Exit For
                End If
            Next intIncr
            ComboBox11.SelectedIndex = -1
            For intIncr = 0 To ComboBox11.Items.Count - 1
                ComboBox11.SelectedIndex = intIncr
                If ComboBox11.SelectedItem = bytDepartamento Then
                    ComboBox5.SelectedIndex = ComboBox11.SelectedIndex
                    Exit For
                End If
            Next intIncr
            ComboBox12.SelectedIndex = -1
            For intIncr = 0 To ComboBox12.Items.Count - 1
                ComboBox12.SelectedIndex = intIncr
                If ComboBox12.SelectedItem = bytMunicipio Then
                    ComboBox6.SelectedIndex = ComboBox12.SelectedIndex
                    Exit For
                End If
            Next intIncr
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If TextBox1.Text <> "" Then
                strCambios(0) = ComboBox1.SelectedItem
                strCambios(1) = ComboBox2.SelectedItem
                strCambios(2) = TextBox2.Text
                strCambios(3) = TextBox3.Text
                strCambios(4) = Format(DateTimePicker1.Value, "yyyyMMdd")
                strCambios(5) = TextBox4.Text
                strCambios(6) = ComboBox3.SelectedItem
                strCambios(7) = ComboBox4.SelectedItem
                strCambios(8) = ComboBox5.SelectedItem
                strCambios(9) = ComboBox6.SelectedItem
                strCambios(10) = TextBox5.Text
                strCambios(11) = TextBox6.Text
                strCambios(12) = TextBox7.Text
                strCambios(13) = TextBox8.Text
                strCambios(14) = TextBox9.Text
                strCambios(15) = TextBox10.Text
                strCambios(16) = TextBox11.Text
                strCambios(17) = TextBox12.Text
                strCambios(18) = TextBox14.Text
                'strCambios(19) = TextBox15.Text
                strCambios(19) = cmbCuentaContable.SelectedValue
                strCambios(20) = ComboBox7.SelectedItem
                If ComboBox6.SelectedItem = "" Then
                    strCambios(9) = "Blanco"
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub CreateMyContextMenu()
        Try
            Dim cntmenuItem1 As New MenuItem
            Dim cntmenuItem2 As New MenuItem
            Dim cntmenuItem3 As New MenuItem
            Dim cntmenuItem4 As New MenuItem
            Dim cntmenuItem5 As New MenuItem

            cntmenuItem1.Text = "Primero"
            cntmenuItem2.Text = "Anterior"
            cntmenuItem3.Text = "Siguiente"
            cntmenuItem4.Text = "Ultimo"
            cntmenuItem5.Text = "Igual"

            cntmnuClientesRg.MenuItems.Add(cntmenuItem1)
            cntmnuClientesRg.MenuItems.Add(cntmenuItem2)
            cntmnuClientesRg.MenuItems.Add(cntmenuItem3)
            cntmnuClientesRg.MenuItems.Add(cntmenuItem4)
            cntmnuClientesRg.MenuItems.Add(cntmenuItem5)

            AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            MoverRegistro(sender.text)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Sub Iniciar()
        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select Registro, Descripcion From cat_Identificacion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ComboBox2.Items.Add(dtrAgro2K.GetValue(1))
                ComboBox8.Items.Add(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Negocios"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ComboBox3.Items.Add(dtrAgro2K.GetValue(1))
                ComboBox9.Items.Add(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select Registro, Nombre From prm_Vendedores"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ComboBox4.Items.Add(dtrAgro2K.GetValue(1))
                ComboBox10.Items.Add(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Departamentos"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ComboBox5.Items.Add(dtrAgro2K.GetValue(1))
                ComboBox11.Items.Add(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Municipios"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ComboBox6.Items.Add(dtrAgro2K.GetValue(1))
                ComboBox12.Items.Add(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Sub Limpiar()
        Try


            For intIncr = 1 To 14
                txtCollection(intIncr).text = ""
            Next
            TextBox12.Text = "0.00"
            TextBox13.Text = "0.00"
            TextBox14.Text = "0"
            TextBox16.Text = "0.00"
            TextBox17.Text = "0"
            For intIncr = 1 To 12
                cboCollection(intIncr).selectedindex = -1
            Next
            ComboBox7.SelectedIndex = 0
            For intIncr = 0 To 19
                strCambios(intIncr) = ""
            Next
            TextBox13.Text = String.Empty
            txtSaldoUSD.Text = String.Empty
            TextBox1.Focus()
            If cmbCuentaContable.Items.Count > 0 Then
                cmbCuentaContable.SelectedValue = "-1"
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub Guardar()
        Try
            If cmbAgencias.SelectedValue Is Nothing Then
                MsgBox("Debeg seleccionar la sucursal a la que pertenece el cliente .", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            For intIncr = 1 To 1
                If cboCollection(intIncr).selectedindex = -1 Then
                    MsgBox("Tiene que escoger una de las opciones de la lista desplegable.", MsgBoxStyle.Critical, "Error de Dato")
                    cboCollection(intIncr).focus()
                    Exit Sub
                End If
            Next intIncr
            For intIncr = 3 To 7
                If cboCollection(intIncr).selectedindex = -1 Then
                    MsgBox("Tiene que escoger una de las opciones de la lista desplegable.", MsgBoxStyle.Critical, "Error de Dato")
                    cboCollection(intIncr).focus()
                    Exit Sub
                End If
            Next intIncr

            Me.Cursor = Cursors.WaitCursor
            Dim cmdTmp As New SqlCommand("sp_IngClientes", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim prmTmp07 As New SqlParameter
            Dim prmTmp08 As New SqlParameter
            Dim prmTmp09 As New SqlParameter
            Dim prmTmp10 As New SqlParameter
            Dim prmTmp11 As New SqlParameter
            Dim prmTmp12 As New SqlParameter
            Dim prmTmp13 As New SqlParameter
            Dim prmTmp14 As New SqlParameter
            Dim prmTmp15 As New SqlParameter
            Dim prmTmp16 As New SqlParameter
            Dim prmTmp17 As New SqlParameter
            Dim prmTmp18 As New SqlParameter
            Dim prmTmp19 As New SqlParameter
            Dim prmTmp20 As New SqlParameter
            Dim prmTmp21 As New SqlParameter
            Dim prmTmp22 As New SqlParameter
            Dim prmTmp23 As New SqlParameter
            Dim prmTmp24 As New SqlParameter
            Dim prmTmp25 As New SqlParameter
            Dim prmTmp26 As New SqlParameter
            Dim prmTmp27 As New SqlParameter
            Dim prmTmp28 As New SqlParameter
            Dim prmTmp29 As New SqlParameter

            lngRegistro = 0
            ComboBox8.SelectedIndex = ComboBox2.SelectedIndex
            If ComboBox8.SelectedIndex = -1 Then
                ComboBox8.SelectedIndex = 0
            End If
            ComboBox9.SelectedIndex = ComboBox3.SelectedIndex
            ComboBox10.SelectedIndex = ComboBox4.SelectedIndex
            ComboBox11.SelectedIndex = ComboBox5.SelectedIndex
            ComboBox12.SelectedIndex = ComboBox6.SelectedIndex
            bytNegocio = ComboBox9.SelectedItem
            bytVendedor = ComboBox10.SelectedItem
            bytDepartamento = ComboBox11.SelectedItem
            bytMunicipio = ComboBox12.SelectedItem
            With prmTmp01
                .ParameterName = "@lngRegistro"
                .SqlDbType = SqlDbType.SmallInt
                .Value = 0
            End With
            With prmTmp02
                .ParameterName = "@strCodigo"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox1.Text
            End With
            With prmTmp03
                .ParameterName = "@intTipoCliente"
                .SqlDbType = SqlDbType.TinyInt
                .Value = ComboBox1.SelectedIndex
            End With
            With prmTmp04
                .ParameterName = "@intTipoID"
                .SqlDbType = SqlDbType.TinyInt
                .Value = SUConversiones.ConvierteAInt(ComboBox8.SelectedItem)
            End With
            With prmTmp05
                .ParameterName = "@strNumID"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox2.Text
            End With
            With prmTmp06
                .ParameterName = "@strNombre"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox3.Text
            End With
            With prmTmp07
                .ParameterName = "@strFechaIng"
                .SqlDbType = SqlDbType.VarChar
                .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            End With
            With prmTmp08
                .ParameterName = "@lngNumFechaIng"
                .SqlDbType = SqlDbType.Int
                .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
            End With
            With prmTmp09
                .ParameterName = "@strDireccion"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox4.Text
            End With
            With prmTmp10
                .ParameterName = "@intNegRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = bytNegocio
            End With
            With prmTmp11
                .ParameterName = "@intVendRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = bytVendedor
            End With
            With prmTmp12
                .ParameterName = "@intDeptRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = bytDepartamento
            End With
            With prmTmp13
                .ParameterName = "@intMuniRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = bytMunicipio
            End With
            With prmTmp14
                .ParameterName = "@strTelefono"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox5.Text
            End With
            With prmTmp15
                .ParameterName = "@strFax"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox6.Text
            End With
            With prmTmp16
                .ParameterName = "@strCorreo"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox7.Text
            End With
            With prmTmp17
                .ParameterName = "@strNegocio"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox8.Text
            End With
            With prmTmp18
                .ParameterName = "@strNegDirec"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox9.Text
            End With
            With prmTmp19
                .ParameterName = "@strContacto01"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox10.Text
            End With
            With prmTmp20
                .ParameterName = "@strContacto02"
                .SqlDbType = SqlDbType.VarChar
                .Value = TextBox11.Text
            End With
            With prmTmp21
                .ParameterName = "@dblLimite"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(TextBox12.Text)
            End With
            With prmTmp22
                .ParameterName = "@dblSaldo"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(TextBox13.Text)
            End With
            With prmTmp23
                .ParameterName = "@intDiasCred"
                .SqlDbType = SqlDbType.SmallInt
                .Value = SUConversiones.ConvierteAInt(TextBox14.Text)
            End With
            With prmTmp24
                .ParameterName = "@strCtaContable"
                .SqlDbType = SqlDbType.VarChar
                .Value = cmbCuentaContable.SelectedValue
            End With
            With prmTmp25
                .ParameterName = "@intEstado"
                .SqlDbType = SqlDbType.TinyInt
                .Value = ComboBox7.SelectedIndex
            End With
            With prmTmp26
                .ParameterName = "@dblSaldoFavor"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(TextBox16.Text)
            End With
            With prmTmp27
                .ParameterName = "@intVerificar"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With prmTmp28
                .ParameterName = "@IdSucursal"
                .SqlDbType = SqlDbType.Int
                .Value = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
            End With
            With prmTmp29
                .ParameterName = "@dblSaldoUSD"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtSaldoUSD.Text)
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .Parameters.Add(prmTmp07)
                .Parameters.Add(prmTmp08)
                .Parameters.Add(prmTmp09)
                .Parameters.Add(prmTmp10)
                .Parameters.Add(prmTmp11)
                .Parameters.Add(prmTmp12)
                .Parameters.Add(prmTmp13)
                .Parameters.Add(prmTmp14)
                .Parameters.Add(prmTmp15)
                .Parameters.Add(prmTmp16)
                .Parameters.Add(prmTmp17)
                .Parameters.Add(prmTmp18)
                .Parameters.Add(prmTmp19)
                .Parameters.Add(prmTmp20)
                .Parameters.Add(prmTmp21)
                .Parameters.Add(prmTmp22)
                .Parameters.Add(prmTmp23)
                .Parameters.Add(prmTmp24)
                .Parameters.Add(prmTmp25)
                .Parameters.Add(prmTmp26)
                .Parameters.Add(prmTmp27)
                .Parameters.Add(prmTmp28)
                .Parameters.Add(prmTmp29)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            intResp = 0
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
                lngRegistro = TextBox17.Text
                cmdTmp.Parameters(0).Value = lngRegistro
                Exit While
            End While
            dtrAgro2K.Close()
            If intResp = 7 Then
                dtrAgro2K.Close()
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            cmdTmp.Parameters(26).Value = 1
            If strCambios(7) <> ComboBox4.SelectedItem Then
                cmdTmp.Parameters(26).Value = 2
            End If
            Try
                cmdTmp.ExecuteNonQuery()
                If intResp = 6 Then
                    If strCambios(0) <> ComboBox1.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "TipoCliente", strCambios(0), ComboBox1.SelectedItem)
                    End If
                    If strCambios(1) <> ComboBox2.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "TipoID", strCambios(1), ComboBox2.SelectedItem)
                    End If
                    If strCambios(2) <> TextBox2.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "NumID", strCambios(2), TextBox2.Text)
                    End If
                    If strCambios(3) <> TextBox3.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Nombre", strCambios(3), TextBox3.Text)
                    End If
                    If strCambios(4) <> Format(DateTimePicker1.Value, "yyyyMMdd") Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "FecIngreso", strCambios(4), Format(DateTimePicker1.Value, "dd-MMM-yyyy"))
                    End If
                    If strCambios(5) <> TextBox4.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Direcci�n", strCambios(5), TextBox4.Text)
                    End If
                    If strCambios(6) <> ComboBox3.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Negocio", strCambios(6), ComboBox3.SelectedItem)
                    End If
                    If strCambios(7) <> ComboBox4.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Vendedor", strCambios(7), ComboBox4.SelectedItem)
                    End If
                    If strCambios(8) <> ComboBox5.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Departamento", strCambios(8), ComboBox5.SelectedItem)
                    End If
                    If strCambios(9) <> ComboBox6.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Municipio", strCambios(9), ComboBox6.SelectedItem)
                    End If
                    If strCambios(10) <> TextBox5.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Tel�fono", strCambios(10), TextBox5.Text)
                    End If
                    If strCambios(11) <> TextBox6.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Fax", strCambios(11), TextBox6.Text)
                    End If
                    If strCambios(12) <> TextBox7.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Correo", strCambios(12), TextBox7.Text)
                    End If
                    If strCambios(13) <> TextBox8.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Negocio", strCambios(13), TextBox8.Text)
                    End If
                    If strCambios(14) <> TextBox9.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Direcci�nNeg", strCambios(14), TextBox9.Text)
                    End If
                    If strCambios(15) <> TextBox10.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Contacto01", strCambios(15), TextBox10.Text)
                    End If
                    If strCambios(16) <> TextBox11.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Contacto02", strCambios(16), TextBox11.Text)
                    End If
                    If strCambios(17) <> TextBox12.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Limite", strCambios(17), TextBox12.Text)
                    End If
                    If strCambios(18) <> TextBox14.Text Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Plazo", strCambios(18), TextBox14.Text)
                    End If
                    If strCambios(19) <> cmbCuentaContable.SelectedValue Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "CtaContable", strCambios(19), cmbCuentaContable.SelectedValue)
                    End If
                    If strCambios(20) <> ComboBox7.SelectedItem Then
                        Ing_Bitacora(intModulo, TextBox17.Text, "Estado", strCambios(20), ComboBox7.SelectedItem)
                    End If
                ElseIf intResp = 0 Then
                    Ing_Bitacora(intModulo, lngRegistro, "C�digo", "", TextBox1.Text)
                End If
                MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
                MoverRegistro("Primero")
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Finally
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
            End Try
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, TextBox3.KeyDown, TextBox4.KeyDown, TextBox5.KeyDown, TextBox6.KeyDown, TextBox7.KeyDown, TextBox8.KeyDown, TextBox9.KeyDown, TextBox10.KeyDown, TextBox11.KeyDown, TextBox12.KeyDown, TextBox13.KeyDown, TextBox14.KeyDown, ComboBox1.KeyDown, ComboBox2.KeyDown, ComboBox3.KeyDown, ComboBox4.KeyDown, ComboBox5.KeyDown, ComboBox6.KeyDown, ComboBox7.KeyDown, DateTimePicker1.KeyDown, cmbAgencias.KeyDown, txtSaldoUSD.KeyDown
        Try


            If e.KeyCode = Keys.Enter Then
                Select Case sender.name.ToString
                    Case "TextBox1" : MoverRegistro("Igual") : ComboBox1.Focus()
                    Case "ComboBox1" : ComboBox2.Focus()
                    Case "ComboBox2" : TextBox2.Focus()
                    Case "TextBox2" : TextBox3.Focus()
                    Case "TextBox3" : DateTimePicker1.Focus()
                    Case "DateTimePicker1" : TextBox4.Focus()
                    Case "TextBox4" : cmbAgencias.Focus()
                    Case "cmbAgencias" : ComboBox3.Focus()
                    Case "ComboBox3" : ComboBox4.Focus()
                    Case "ComboBox4" : ComboBox5.Focus()
                    Case "ComboBox5" : ComboBox6.Focus()
                    Case "ComboBox6" : TextBox5.Focus()
                    Case "TextBox5" : TextBox6.Focus()
                    Case "TextBox6" : TextBox7.Focus()
                    Case "TextBox7" : TextBox8.Focus()
                    Case "TextBox8" : TextBox9.Focus()
                    Case "TextBox9" : TextBox10.Focus()
                    Case "TextBox10" : TextBox11.Focus()
                    Case "TextBox11" : TextBox12.Focus()
                    Case "TextBox12" : TextBox13.Focus()
                    Case "TextBox13" : txtSaldoUSD.Focus()
                    Case "txtSaldoUSD" : TextBox14.Focus()
                    Case "TextBox14" : cmbCuentaContable.Focus()
                    Case "TextBox15" : ComboBox7.Focus()
                    Case "ComboBox7" : TextBox1.Focus()
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, TextBox3.GotFocus, TextBox4.GotFocus, TextBox5.GotFocus, TextBox6.GotFocus, TextBox7.GotFocus, TextBox8.GotFocus, TextBox9.GotFocus, TextBox10.GotFocus, TextBox11.GotFocus, TextBox12.GotFocus, TextBox13.GotFocus, TextBox14.GotFocus, ComboBox1.GotFocus, ComboBox2.GotFocus, ComboBox3.GotFocus, ComboBox4.GotFocus, ComboBox5.GotFocus, ComboBox6.GotFocus, ComboBox7.GotFocus, DateTimePicker1.GotFocus, cmbAgencias.GotFocus

        Dim strCampo As String = String.Empty
        Try


            Select Case sender.name.ToString
                Case "TextBox1" : strCampo = "C�digo"
                Case "TextBox2" : strCampo = "N�m. Identif."
                Case "TextBox3" : strCampo = "Nombre"
                Case "TextBox4" : strCampo = "Direcci�n"
                Case "cmbAgencias" : strCampo = "Agencia"
                Case "TextBox5" : strCampo = "Tel�fono"
                Case "TextBox6" : strCampo = "Fax"
                Case "TextBox7" : strCampo = "Correo"
                Case "TextBox8" : strCampo = "Negocio"
                Case "TextBox9" : strCampo = "Direcci�n"
                Case "TextBox10" : strCampo = "Contacto"
                Case "TextBox11" : strCampo = "Contacto"
                Case "TextBox12" : strCampo = "Limite"
                Case "TextBox13" : strCampo = "Saldo"
                Case "txtSaldoUSD" : strCampo = "Saldo US$"
                Case "TextBox14" : strCampo = "Plazo"
                Case "TextBox15" : strCampo = "Cta Contable"
                Case "TextBox16" : strCampo = "Saldo Pend."
                Case "ComboBox1" : strCampo = "TipoCliente"
                Case "ComboBox2" : strCampo = "Tipo Ident."
                Case "ComboBox3" : strCampo = "Negocio"
                Case "ComboBox4" : strCampo = "Vendedor"
                Case "ComboBox5" : strCampo = "Departamento"
                Case "ComboBox6" : strCampo = "Municipio"
                Case "ComboBox7" : strCampo = "Estado"
                Case "DateTimePicker1" : strCampo = "Fec. Ingreso"
            End Select
            If sender.name <> "DateTimePicker1" Then
                sender.selectall()
            End If
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try


            Me.Cursor = Cursors.Default
            If blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    TextBox1.Text = strUbicar
                End If
                MoverRegistro("Igual")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem14.Click
        Try


            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Try


            If TextBox17.Text = "0" Then
                ComboBox9.SelectedIndex = ComboBox3.SelectedIndex
                If ComboBox3.SelectedItem <> "" Then
                    strQuery = ""
                    strQuery = "select * from prm_negocios where registro = " & ComboBox9.SelectedItem & ""
                    If cnnAgro2K.State = ConnectionState.Open Then
                        cnnAgro2K.Close()
                    End If
                    If cmdAgro2K.Connection.State = ConnectionState.Open Then
                        cmdAgro2K.Connection.Close()
                    End If
                    cnnAgro2K.Open()
                    cmdAgro2K.Connection = cnnAgro2K
                    cmdAgro2K.CommandText = strQuery
                    dtrAgro2K = cmdAgro2K.ExecuteReader
                    While dtrAgro2K.Read
                        If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                            TextBox14.Text = dtrAgro2K.GetValue(4)
                        End If
                    End While
                    dtrAgro2K.Close()
                    cmdAgro2K.Connection.Close()
                    cnnAgro2K.Close()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ComboBox5_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox5.SelectedIndexChanged
        Try


            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            ComboBox6.Items.Clear()
            ComboBox12.Items.Clear()
            ComboBox11.SelectedIndex = ComboBox5.SelectedIndex
            If ComboBox5.SelectedItem <> "" Then
                strQuery = ""
                strQuery = "Select Registro, Descripcion From prm_Municipios Where Registro2 = " & SUConversiones.ConvierteAInt(ComboBox11.SelectedItem) & ""
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ComboBox6.Items.Add(dtrAgro2K.GetValue(1))
                    ComboBox12.Items.Add(dtrAgro2K.GetValue(0))
                End While
                dtrAgro2K.Close()
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub btnBusqueda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusqueda.Click
        Try
            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        'Dim frmNew As New frmGeneral
        'lngRegistro = TextBox17.Text
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        'frmNew.ShowDialog()
    End Sub
    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Try
            Dim frmNew As New frmBitacora
            lngRegistro = TextBox17.Text
            frmNew.ShowDialog()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Try
            Dim frmNew As New actrptViewer
            intRptFactura = 30
            strQuery = "exec sp_Reportes " & intModulo & " "
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        Try
            MoverRegistro("Primero")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
        Try
            MoverRegistro("Anterior")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        Try
            MoverRegistro("Siguiente")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub btnRegistros_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistros.Click

    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        Try
            MoverRegistro("Ultimo")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        Try
            MoverRegistro("Igual")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            Guardar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        Try
            Dim frmNew As New actrptViewer
            intRptFactura = 70
            strQuery = "exec rptClientesConCuentaContable "
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnRptClienteCtaCont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRptClienteCtaCont.Click
        Try
            Dim frmNew As New actrptViewer
            intRptFactura = 70
            strQuery = "exec rptClientesConCuentaContable "
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub
End Class
