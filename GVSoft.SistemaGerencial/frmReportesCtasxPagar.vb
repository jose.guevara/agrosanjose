Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
'Imports System.Microsoft.VisualBasic
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmReportesCtasxPagar


    Private Sub frmReportesCtasxPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Top = 0
        'Me.Left = 0
        blnClear = False
        Iniciar()
        Limpiar()
        RNReportes.CargarComboReportes(cbReportes, nIdUsuario, nIdModulo)
        CargarProveedores()
        
    End Sub

    Private Sub frmReportesCtasxPagar_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F5 Then
            If Label13.Visible = True Then
                intListadoAyuda = 7
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        End If
    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Negocios Where Estado <> 2 order by descripcion"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox1.Items.Add(dtrAgro2K.GetValue(1))
            ListBox3.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Departamentos Where Estado <> 2 order by descripcion"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox5.Items.Add(dtrAgro2K.GetValue(1))
            ListBox7.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        ListBox2.Items.Clear()
        ListBox4.Items.Clear()
        ListBox6.Items.Clear()
        ListBox8.Items.Clear()
        ListBox14.Items.Clear()
        ListBox16.Items.Clear()
        ListBox17.Items.Clear()
        ListBox18.Items.Clear()
        intRptExportar = 0
        Label13.Visible = False
        DateTimePicker1.Value = DateSerial(Year(Now), Month(Now), 1)
        DateTimePicker2.Value = Now
        TextBox2.Text = ""

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox14.DoubleClick, ListBox16.DoubleClick

        Select Case sender.name.ToString
            Case "ListBox1" : ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
            Case "ListBox2"
                If ListBox2.SelectedIndex >= 0 Then
                    ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                End If
            Case "ListBox5" : ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem) : lngRegistro = CInt(ListBox7.SelectedItem) : UbicarMunicipios()
            Case "ListBox6"
                If ListBox6.SelectedIndex >= 0 Then
                    ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                End If
            Case "ListBox14" : ListBox16.Items.Add(ListBox14.SelectedItem) : ListBox17.SelectedIndex = ListBox14.SelectedIndex : ListBox18.Items.Add(ListBox17.SelectedItem)
            Case "ListBox16"
                If ListBox16.SelectedIndex >= 0 Then
                    ListBox18.Items.RemoveAt(ListBox16.SelectedIndex) : ListBox16.Items.RemoveAt(ListBox16.SelectedIndex)
                End If
        End Select

    End Sub

    Private Sub txtNombreProveedor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombreProveedor.KeyDown

        If e.KeyCode = Keys.Enter Then

            If dgvProveedores.Rows.Count > 0 Then
                If txtNombreProveedor.Text = "" Then
                    Exit Sub
                Else
                    Dim h As Integer
                    For h = 0 To dgvProveedores.Rows.Count - 1
                        If dgvProveedores.Rows(h).Selected Then
                            If dgvProveedores.Rows(h).Cells("ckSeleccion").Value = 1 Then
                                dgvProveedores.Rows(h).Cells("ckSeleccion").Value = 0
                            Else
                                dgvProveedores.Rows(h).Cells("ckSeleccion").Value = 1
                            End If
                        End If
                    Next
                End If
            Else
                MsgBox("No hay Registro que Seleccionar", MsgBoxStyle.Exclamation, "Reportes Ctas x Pagar")
                Exit Sub
            End If
            txtNombreProveedor.Focus()
            txtNombreProveedor.Text = ""
        End If

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)

        Label13.Visible = True

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

        Label13.Visible = False

    End Sub

    Sub SeleccionarProveedor()
        Dim i, j, Index As Integer
        Dim dtProv As DataTable = Nothing
        Dim dtTmp As DataTable = Nothing

        If txtNombreProveedor.Text <> "" Then 'Verifica que la caja de texto no este vacia
            If dgvProveedores.Rows.Count > 0 Then
                dtTmp = New DataTable()
                dtProv = New DataTable()
                dtProv.Columns.Add(New DataColumn("Codigo", GetType(String)))
                dtProv.Columns.Add(New DataColumn("NombreProveedor", GetType(String)))
                dtProv.Columns.Add(New DataColumn("IdProveedor", GetType(Integer)))

                Dim dr As DataRow
                For i = 0 To dgvProveedores.Rows.Count - 1
                    dr = dtProv.NewRow()
                    dr("IdProveedor") = dgvProveedores.Rows(i).Cells("IdProveedor").Value
                    dr("NombreProveedor") = dgvProveedores.Rows(i).Cells("NombreProveedor").Value
                    dr("Codigo") = dgvProveedores.Rows(i).Cells("Codigo").Value
                    dtProv.Rows.Add(dr)
                    dtProv.AcceptChanges()
                Next
                dtTmp = dtProv.Clone()
            End If

            Dim drRegistro As DataRow() = dtProv.Select("NombreProveedor like '" & txtNombreProveedor.Text & "%' " & " or Codigo like '" & txtNombreProveedor.Text & "%'")

            For Each reg As DataRow In drRegistro
                dtTmp.ImportRow(reg)
            Next

            If dtTmp.Rows.Count <= 0 Then
                MsgBox("Datos no Encontrado", MsgBoxStyle.Information, "Reportes Cts x Pagar")
                dgvProveedores.FirstDisplayedScrollingRowIndex = 0
            Else
                'Index = dtTmp.Rows(0)("IdProveedor")
                For j = 0 To dtTmp.Rows.Count - 1
                    Index = dtTmp.Rows(j)("IdProveedor")
                    For i = 0 To dgvProveedores.Rows.Count - 1
                        If dgvProveedores.Rows(i).Cells("IdProveedor").Value = Index Then
                            dgvProveedores.Rows(i).Selected = True
                            dgvProveedores.FirstDisplayedScrollingRowIndex = i
                        Else
                            dgvProveedores.Rows(i).Selected = False
                        End If

                    Next
                Next

            End If

        End If
    End Sub

    Function UbicarProveedor() As Boolean

        lngRegistro = 0
        strQuery = ""
        strQuery = "Select C.Registro, C.Codigo, C.Nombre, C.Saldo from prm_Proveedores C Where C.Saldo > 0 and (C.Codigo = '" & txtNombreProveedor.Text & "'" & " or C.nombre like '" & txtNombreProveedor.Text & "%')"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        dgvProveedores.Rows.Clear()
        While dtrAgro2K.Read
            lngRegistro = 1
            dgvProveedores.Rows.Add(1, dtrAgro2K.Item("Nombre"), "N/A", dtrAgro2K.Item("Saldo"), dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("Registro"))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If lngRegistro = 0 Then
            UbicarProveedor = False
            Exit Function
        End If

        UbicarProveedor = True

    End Function

    Sub UbicarMunicipios()

        ListBox14.Items.Clear()
        ListBox17.Items.Clear()
        strQuery = ""
        strQuery = "Select registro, descripcion from prm_Municipios Where registro2 = " & lngRegistro & " "
        strQuery = strQuery + "order by descripcion"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox14.Items.Add(dtrAgro2K.GetValue(1))
            ListBox17.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub GenerarQuery()

        strQueryParte01 = ""
        strQueryParte02 = ""
        strQueryParte03 = ""
        strQueryParte04 = ""
        strQueryParte05 = ""


        
        If ListBox4.Items.Count > 0 Then                                        'Negocios
            strQueryParte03 = strQueryParte03 + "'and C.NegRegistro in ("
            If ListBox4.Items.Count = 1 Then
                ListBox4.SelectedIndex = 0
                strQueryParte03 = strQueryParte03 + "" & ListBox4.SelectedItem & ") ', "
            ElseIf ListBox4.Items.Count > 1 Then
                For intIncr = 0 To ListBox4.Items.Count - 2
                    ListBox4.SelectedIndex = intIncr
                    strQueryParte03 = strQueryParte03 + "" & ListBox4.SelectedItem & ", "
                Next
                ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                strQueryParte03 = strQueryParte03 + "" & ListBox4.SelectedItem & ") ', "
            End If
        Else
            strQueryParte03 = strQueryParte03 + "' ', "
        End If
        If ListBox8.Items.Count > 0 Then                                        'Departamentos
            strQueryParte04 = strQueryParte04 + "'and C.DeptRegistro in ("
            If ListBox8.Items.Count = 1 Then
                ListBox8.SelectedIndex = 0
                strQueryParte04 = strQueryParte04 + "" & ListBox8.SelectedItem & ") ', 0, 0, '', '', "
            ElseIf ListBox8.Items.Count > 1 Then
                For intIncr = 0 To ListBox8.Items.Count - 2
                    ListBox8.SelectedIndex = intIncr
                    strQueryParte04 = strQueryParte04 + "" & ListBox8.SelectedItem & ", "
                Next
                ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                strQueryParte04 = strQueryParte04 + "" & ListBox8.SelectedItem & ") ', 0, 0, '', '', "
            End If
        Else
            strQueryParte04 = strQueryParte04 + "' ', 0, 0, '', '', "
        End If
        strQueryParte04 = strQueryParte04 + " '', "
        If ListBox18.Items.Count > 0 Then                                        'Municipios
            strQueryParte05 = strQueryParte05 + "'and C.MunRegistro in ("
            If ListBox18.Items.Count = 1 Then
                ListBox18.SelectedIndex = 0
                strQueryParte05 = strQueryParte05 + "" & ListBox18.SelectedItem & ") '"
            ElseIf ListBox18.Items.Count > 1 Then
                For intIncr = 0 To ListBox18.Items.Count - 2
                    ListBox18.SelectedIndex = intIncr
                    strQueryParte05 = strQueryParte05 + "" & ListBox18.SelectedItem & ", "
                Next
                ListBox18.SelectedIndex = ListBox18.Items.Count - 1
                strQueryParte05 = strQueryParte05 + "" & ListBox18.SelectedItem & ") ' "
            End If
        Else
            strQueryParte05 = strQueryParte05 + "' ' "
        End If

    End Sub

    Sub GenerarQuery2()

        If dgvProveedores.Rows.Count = 1 Then
            If dgvProveedores.Rows(0).Cells("ckSeleccion").Value = 1 Then
                strQueryParte03 = dgvProveedores.Rows(0).Cells("IdProveedor").Value & ")'"
            End If
        Else
            Dim i As Integer
            For i = 0 To dgvProveedores.Rows.Count - 1
                If dgvProveedores.Rows(i).Cells("ckSeleccion").Value = 1 Then
                    strQueryParte03 = strQueryParte03 & dgvProveedores.Rows(i).Cells("IdProveedor").Value & ","
                End If
            Next
            strQueryParte03 = strQueryParte03 & "0)'"
            'strQuery = strQuery & strQueryParte03 & "order by o.IdProveedor"
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If strUbicar <> "" Then
            Timer1.Enabled = False
            txtNombreProveedor.Text = strUbicar : UbicarProveedor() : txtNombreProveedor.Text = ""
        End If

    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

        intListadoAyuda = 7
        Dim frmNew As New frmListadoAyuda
        Timer1.Interval = 200
        Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Private Sub MenuItem42_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Public Sub MostrarReporte()
        Dim frmNew As Form
        Dim strAgencias As String = String.Empty
        Dim strAgencias2 As String = String.Empty
        intRptOtros = 0
        intRptPresup = 0
        intrptCtasPagar = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptTipoPrecio = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        intRptCtasCbr = 0

        intrptCtasPagar = cbReportes.SelectedValue

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        If intrptCtasPagar = 1 Then
            strQuery = ""
            strQuery = "exec spEstadoCuentasProveedores " '& ""
            'GenerarQuery2()
            'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()

        ElseIf intrptCtasPagar = 2 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spDetalleSaldosProveedoresXProveedor "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 3 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spSaldosGeneralesDetallados "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 4 Then
            strFechaInicial = Format(DateTimePicker1.Value, "yyyy-MM-dd") ' & DateTimePicker1.Value.Month & DateTimePicker1.Value.Day     'Format(DateTimePicker1.Value, "dd/mmm/yyyy")
            strFechaFinal = Format(DateTimePicker2.Value, "yyyy-MM-dd") ' & DateTimePicker1.Value.Month & DateTimePicker1.Value.Day     'Format(DateTimePicker2.Value, "dd/mm/yyyy")
            strQuery = ""
            strQuery = "exec rptRecibosxPagarAnulados " & "'" & strFechaInicial & "','" & strFechaFinal & "',''"
            'GenerarQuery()
            'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
        ElseIf intrptCtasPagar = 5 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spRecibosCajaxFecha "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03 & " , '" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "' , '" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "'"

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 6 Then
            Dim IdProv As Integer

            IdProv = CInt(cbReportes.SelectedValue)

            strQuery = ""
            
            strQuery = "spMovimientoHistoricoProveedores "

            If dgvProveedores.Rows.Count > 0 Then

                Dim i As Integer
                For i = 0 To dgvProveedores.Rows.Count - 1
                    If dgvProveedores.Rows(i).Cells("ckSeleccion").Value = 1 Then
                        IdProv = dgvProveedores.Rows(i).Cells("IdProveedor").Value
                    End If
                Next

                strQuery = strQuery & IdProv & " , '" & Format(DateTimePicker1.Value, "MM/dd/yyyy") & "' , '" & Format(DateTimePicker2.Value, "MM/dd/yyyy") & "', ''"

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 7 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spFacturasPendientesProveedores "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 8 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spFacturasPendientesProveedoresMasCuatroMeses "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 9 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spFacturasPendientesProveedoresMasYear "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 10 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spResumenFacturasCompras "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03 & " , " & Format(DateTimePicker1.Value, "yyyyMMdd") & " , " & Format(DateTimePicker2.Value, "yyyyMMdd") & ""

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        ElseIf intrptCtasPagar = 11 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spDescuentoFacturasCompra "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03 & " , " & Format(DateTimePicker1.Value, "yyyyMMdd") & " , " & Format(DateTimePicker2.Value, "yyyyMMdd") & ""

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If
        ElseIf intrptCtasPagar = 12 Then

            strQueryParte03 = ""

            strQueryParte00 = ""
            strQueryParte00 = " 'and p.registro in ("

            strQuery = ""
            strQuery = "spInteresesFacturasCompra "

            If dgvProveedores.Rows.Count > 0 Then

                GenerarQuery2()
                strQuery = strQuery & strQueryParte00 & strQueryParte03 & " , " & Format(DateTimePicker1.Value, "yyyyMMdd") & " , " & Format(DateTimePicker2.Value, "yyyyMMdd") & ""

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If

        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub CargarProveedores()
        'If cbReportes.ValueMember = "" Then
        'Exit Sub
        'Else
        'If cbReportes.SelectedValue = 2 Then
        strQuery = ""
        strQuery = "Select C.Registro, C.Codigo, C.Nombre, C.Saldo from prm_Proveedores C Where C.estado = 0"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        dgvProveedores.Rows.Clear()
        While dtrAgro2K.Read
            dgvProveedores.Rows.Add(0, dtrAgro2K.Item("Nombre"), "N/A", dtrAgro2K.Item("Saldo"), dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("Registro"))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        'End If
        'End If
    End Sub

    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        MostrarReporte()
    End Sub

    Private Sub cmdiExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiExcel.Click
        intRptExportar = 1
    End Sub

    Private Sub cmdiHtml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiHtml.Click
        intRptExportar = 2
    End Sub

    Private Sub cmdiPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiPdf.Click
        intRptExportar = 3
    End Sub

    Private Sub cmdiRtf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiRtf.Click
        intRptExportar = 4
    End Sub

    Private Sub cmdiTiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiTiff.Click
        intRptExportar = 5
    End Sub

    Private Sub cmdExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExportar.Click
        intRptExportar = 0
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub


    Private Sub cbReportes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbReportes.SelectedIndexChanged
        'CargarProveedores()
    End Sub

    Private Sub cmdSeleccion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSeleccion.Click
        If cmdSeleccion.Text = "Seleccionar Todos" Then
            If dgvProveedores.Rows.Count > 0 Then
                Dim i As Integer
                cmdSeleccion.Text = "Quitar Seleccion"
                For i = 0 To dgvProveedores.Rows.Count - 1
                    dgvProveedores.Rows(i).Cells("ckSeleccion").Value = 1
                Next
            Else
                MsgBox("No hay Registro que Seleccionar", MsgBoxStyle.Exclamation, "Reportes Ctas x Pagar")
                Exit Sub
            End If

        ElseIf cmdSeleccion.Text = "Quitar Seleccion" Then
            If dgvProveedores.Rows.Count > 0 Then
                Dim i As Integer
                cmdSeleccion.Text = "Seleccionar Todos"
                For i = 0 To dgvProveedores.Rows.Count - 1
                    dgvProveedores.Rows(i).Cells("ckSeleccion").Value = 0
                Next
            Else
                MsgBox("No hay Registro que quitar Seleccion", MsgBoxStyle.Exclamation, "Reportes Ctas x Pagar")
                Exit Sub
            End If
        End If

    End Sub

    Private Sub dgvProveedores_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvProveedores.CellClick
        If dgvProveedores.CurrentRow.Cells("ckSeleccion").Value = 0 Then
            'dgvProveedores.CurrentCell.Value = 1
            dgvProveedores.CurrentRow.Cells("ckSeleccion").Value = 1
        Else
            'dgvProveedores.CurrentCell.Value = 0
            dgvProveedores.CurrentRow.Cells("ckSeleccion").Value = 0
        End If
    End Sub

    Private Sub txtNombreProveedor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombreProveedor.TextChanged
        SeleccionarProveedor()
    End Sub
End Class