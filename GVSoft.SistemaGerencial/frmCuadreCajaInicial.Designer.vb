﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuadreCajaInicial
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCuadreCajaInicial))
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle51 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle52 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle49 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle50 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Bar1 = New DevComponents.DotNetBar.Bar
        Me.cmdGuardar = New DevComponents.DotNetBar.ButtonItem
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem
        Me.cmdSalir = New DevComponents.DotNetBar.ButtonItem
        Me.ugbIngresoDatos = New Infragistics.Win.Misc.UltraGroupBox
        Me.diDeposito = New DevComponents.Editors.DoubleInput
        Me.Label10 = New System.Windows.Forms.Label
        Me.diGastos = New DevComponents.Editors.DoubleInput
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtExNumeroReciboFinal = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtExNumeroReciboInicial = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtExNumeroFacturaFinal = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtExNumeroFacturaInicial = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label6 = New System.Windows.Forms.Label
        Me.dtiFechaFinArqueo = New DevComponents.Editors.DateTimeAdv.DateTimeInput
        Me.Label5 = New System.Windows.Forms.Label
        Me.diSaldoInicial = New DevComponents.Editors.DoubleInput
        Me.Label4 = New System.Windows.Forms.Label
        Me.lbltxtNumeroArqueo = New DevComponents.DotNetBar.LabelX
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtiFechaInicioArqueo = New DevComponents.Editors.DateTimeAdv.DateTimeInput
        Me.Label2 = New System.Windows.Forms.Label
        Me.lbltxtUsuario = New DevComponents.DotNetBar.LabelX
        Me.Label1 = New System.Windows.Forms.Label
        Me.UltraGroupBox2 = New Infragistics.Win.Misc.UltraGroupBox
        Me.UltraGroupBox4 = New Infragistics.Win.Misc.UltraGroupBox
        Me.dgxDetalleDolares = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UltraNumericEditorColumn1 = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
        Me.UltraNumericEditorColumn2 = New Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn(Me.components)
        Me.UltraGroupBox3 = New Infragistics.Win.Misc.UltraGroupBox
        Me.dgxDetalleCordobas = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.Denominacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
        Me.Total = New Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn(Me.components)
        Me.TabControl1 = New DevComponents.DotNetBar.TabControl
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ugbIngresoDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ugbIngresoDatos.SuspendLayout()
        CType(Me.diDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtiFechaFinArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diSaldoInicial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtiFechaInicioArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox2.SuspendLayout()
        CType(Me.UltraGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox4.SuspendLayout()
        CType(Me.dgxDetalleDolares, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox3.SuspendLayout()
        CType(Me.dgxDetalleCordobas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Bar1
        '
        Me.Bar1.AntiAlias = True
        Me.Bar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdGuardar, Me.cmdProcesar, Me.cmdSalir})
        Me.Bar1.Location = New System.Drawing.Point(0, 0)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(838, 63)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar1.TabIndex = 0
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'cmdGuardar
        '
        Me.cmdGuardar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdGuardar.Image = CType(resources.GetObject("cmdGuardar.Image"), System.Drawing.Image)
        Me.cmdGuardar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdGuardar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdGuardar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Text = "Guardar<F3>"
        Me.cmdGuardar.Tooltip = "Guardar Arqueo"
        '
        'cmdProcesar
        '
        Me.cmdProcesar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Cierre Caja<F4>"
        Me.cmdProcesar.Tooltip = "Realizar Arqueo"
        '
        'cmdSalir
        '
        Me.cmdSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdSalir.Image = CType(resources.GetObject("cmdSalir.Image"), System.Drawing.Image)
        Me.cmdSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdSalir.Name = "cmdSalir"
        Me.cmdSalir.Text = "Salir"
        Me.cmdSalir.Tooltip = "Salir"
        '
        'ugbIngresoDatos
        '
        Me.ugbIngresoDatos.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.ugbIngresoDatos.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.ugbIngresoDatos.Controls.Add(Me.diDeposito)
        Me.ugbIngresoDatos.Controls.Add(Me.Label10)
        Me.ugbIngresoDatos.Controls.Add(Me.diGastos)
        Me.ugbIngresoDatos.Controls.Add(Me.Label11)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroReciboFinal)
        Me.ugbIngresoDatos.Controls.Add(Me.Label8)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroReciboInicial)
        Me.ugbIngresoDatos.Controls.Add(Me.Label9)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroFacturaFinal)
        Me.ugbIngresoDatos.Controls.Add(Me.Label7)
        Me.ugbIngresoDatos.Controls.Add(Me.txtExNumeroFacturaInicial)
        Me.ugbIngresoDatos.Controls.Add(Me.Label6)
        Me.ugbIngresoDatos.Controls.Add(Me.dtiFechaFinArqueo)
        Me.ugbIngresoDatos.Controls.Add(Me.Label5)
        Me.ugbIngresoDatos.Controls.Add(Me.diSaldoInicial)
        Me.ugbIngresoDatos.Controls.Add(Me.Label4)
        Me.ugbIngresoDatos.Controls.Add(Me.lbltxtNumeroArqueo)
        Me.ugbIngresoDatos.Controls.Add(Me.Label3)
        Me.ugbIngresoDatos.Controls.Add(Me.dtiFechaInicioArqueo)
        Me.ugbIngresoDatos.Controls.Add(Me.Label2)
        Me.ugbIngresoDatos.Controls.Add(Me.lbltxtUsuario)
        Me.ugbIngresoDatos.Controls.Add(Me.Label1)
        Me.ugbIngresoDatos.Dock = System.Windows.Forms.DockStyle.Top
        Me.ugbIngresoDatos.Location = New System.Drawing.Point(1, 1)
        Me.ugbIngresoDatos.Name = "ugbIngresoDatos"
        Me.ugbIngresoDatos.Size = New System.Drawing.Size(836, 199)
        Me.ugbIngresoDatos.TabIndex = 1
        Me.ugbIngresoDatos.Text = "Datos Generales Arqueo"
        Me.ugbIngresoDatos.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'diDeposito
        '
        '
        '
        '
        Me.diDeposito.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diDeposito.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diDeposito.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diDeposito.Increment = 1
        Me.diDeposito.Location = New System.Drawing.Point(500, 166)
        Me.diDeposito.Name = "diDeposito"
        Me.diDeposito.Size = New System.Drawing.Size(212, 20)
        Me.diDeposito.TabIndex = 8
        Me.diDeposito.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diDeposito.WatermarkText = "Monto de Depósito"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(443, 166)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Deposito:"
        '
        'diGastos
        '
        '
        '
        '
        Me.diGastos.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diGastos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diGastos.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diGastos.Increment = 1
        Me.diGastos.Location = New System.Drawing.Point(170, 166)
        Me.diGastos.Name = "diGastos"
        Me.diGastos.Size = New System.Drawing.Size(212, 20)
        Me.diGastos.TabIndex = 7
        Me.diGastos.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diGastos.WatermarkText = "Monto de Gastos"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label11.Location = New System.Drawing.Point(122, 166)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Gastos:"
        '
        'txtExNumeroReciboFinal
        '
        Me.txtExNumeroReciboFinal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroReciboFinal.Border.Class = "TextBoxBorder"
        Me.txtExNumeroReciboFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroReciboFinal.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroReciboFinal.Location = New System.Drawing.Point(499, 110)
        Me.txtExNumeroReciboFinal.Name = "txtExNumeroReciboFinal"
        Me.txtExNumeroReciboFinal.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroReciboFinal.TabIndex = 5
        Me.txtExNumeroReciboFinal.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroReciboFinal.WatermarkText = "Número Recibo Final"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label8.Location = New System.Drawing.Point(386, 112)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Número Recibo Final:"
        '
        'txtExNumeroReciboInicial
        '
        Me.txtExNumeroReciboInicial.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroReciboInicial.Border.Class = "TextBoxBorder"
        Me.txtExNumeroReciboInicial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroReciboInicial.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroReciboInicial.Location = New System.Drawing.Point(170, 110)
        Me.txtExNumeroReciboInicial.Name = "txtExNumeroReciboInicial"
        Me.txtExNumeroReciboInicial.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroReciboInicial.TabIndex = 5
        Me.txtExNumeroReciboInicial.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroReciboInicial.WatermarkText = "Número Recibo Inicial"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label9.Location = New System.Drawing.Point(51, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Número Recibo Inicial:"
        '
        'txtExNumeroFacturaFinal
        '
        Me.txtExNumeroFacturaFinal.AccessibleDescription = "Req"
        Me.txtExNumeroFacturaFinal.AccessibleName = "Número Factura Final"
        Me.txtExNumeroFacturaFinal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroFacturaFinal.Border.Class = "TextBoxBorder"
        Me.txtExNumeroFacturaFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroFacturaFinal.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroFacturaFinal.Location = New System.Drawing.Point(499, 82)
        Me.txtExNumeroFacturaFinal.Name = "txtExNumeroFacturaFinal"
        Me.txtExNumeroFacturaFinal.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroFacturaFinal.TabIndex = 4
        Me.txtExNumeroFacturaFinal.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroFacturaFinal.WatermarkText = "Número Factura Final"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label7.Location = New System.Drawing.Point(384, 85)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Número Factura Final:"
        '
        'txtExNumeroFacturaInicial
        '
        Me.txtExNumeroFacturaInicial.AccessibleDescription = "Req"
        Me.txtExNumeroFacturaInicial.AccessibleName = "Número Factura Inicial"
        Me.txtExNumeroFacturaInicial.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroFacturaInicial.Border.Class = "TextBoxBorder"
        Me.txtExNumeroFacturaInicial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroFacturaInicial.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroFacturaInicial.Location = New System.Drawing.Point(170, 82)
        Me.txtExNumeroFacturaInicial.Name = "txtExNumeroFacturaInicial"
        Me.txtExNumeroFacturaInicial.Size = New System.Drawing.Size(212, 20)
        Me.txtExNumeroFacturaInicial.TabIndex = 3
        Me.txtExNumeroFacturaInicial.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.txtExNumeroFacturaInicial.WatermarkText = "Número Factura Inicial"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label6.Location = New System.Drawing.Point(49, 85)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Número Factura Inicial:"
        '
        'dtiFechaFinArqueo
        '
        Me.dtiFechaFinArqueo.AccessibleDescription = "Req"
        Me.dtiFechaFinArqueo.AccessibleName = "Fecha Fin Arqueo"
        '
        '
        '
        Me.dtiFechaFinArqueo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtiFechaFinArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtiFechaFinArqueo.ButtonDropDown.Visible = True
        Me.dtiFechaFinArqueo.CustomFormat = "dd/MM/yyyy"
        Me.dtiFechaFinArqueo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.dtiFechaFinArqueo.IsPopupCalendarOpen = False
        Me.dtiFechaFinArqueo.Location = New System.Drawing.Point(499, 54)
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtiFechaFinArqueo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.DisplayMonth = New Date(2011, 9, 1, 0, 0, 0, 0)
        Me.dtiFechaFinArqueo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtiFechaFinArqueo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.TodayButtonVisible = True
        Me.dtiFechaFinArqueo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtiFechaFinArqueo.Name = "dtiFechaFinArqueo"
        Me.dtiFechaFinArqueo.Size = New System.Drawing.Size(212, 20)
        Me.dtiFechaFinArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtiFechaFinArqueo.TabIndex = 2
        Me.dtiFechaFinArqueo.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.dtiFechaFinArqueo.WatermarkText = "Fecha Fin Arqueo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(401, 58)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Fecha Fin Arqueo:"
        '
        'diSaldoInicial
        '
        '
        '
        '
        Me.diSaldoInicial.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diSaldoInicial.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diSaldoInicial.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diSaldoInicial.Increment = 1
        Me.diSaldoInicial.Location = New System.Drawing.Point(170, 138)
        Me.diSaldoInicial.Name = "diSaldoInicial"
        Me.diSaldoInicial.Size = New System.Drawing.Size(212, 20)
        Me.diSaldoInicial.TabIndex = 6
        Me.diSaldoInicial.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.diSaldoInicial.WatermarkText = "Saldo Inicial Arqueo"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(98, 139)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Saldo Inicial:"
        '
        'lbltxtNumeroArqueo
        '
        Me.lbltxtNumeroArqueo.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lbltxtNumeroArqueo.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.lbltxtNumeroArqueo.BackgroundStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.lbltxtNumeroArqueo.BackgroundStyle.Class = ""
        Me.lbltxtNumeroArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lbltxtNumeroArqueo.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lbltxtNumeroArqueo.Location = New System.Drawing.Point(499, 28)
        Me.lbltxtNumeroArqueo.Name = "lbltxtNumeroArqueo"
        Me.lbltxtNumeroArqueo.Size = New System.Drawing.Size(212, 23)
        Me.lbltxtNumeroArqueo.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(411, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Número Arqueo:"
        '
        'dtiFechaInicioArqueo
        '
        Me.dtiFechaInicioArqueo.AccessibleDescription = "Req"
        Me.dtiFechaInicioArqueo.AccessibleName = "Fecha Inicio Arqueo"
        '
        '
        '
        Me.dtiFechaInicioArqueo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtiFechaInicioArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtiFechaInicioArqueo.ButtonDropDown.Visible = True
        Me.dtiFechaInicioArqueo.CustomFormat = "dd/MM/yyyy"
        Me.dtiFechaInicioArqueo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.dtiFechaInicioArqueo.IsPopupCalendarOpen = False
        Me.dtiFechaInicioArqueo.Location = New System.Drawing.Point(170, 54)
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtiFechaInicioArqueo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.DisplayMonth = New Date(2011, 9, 1, 0, 0, 0, 0)
        Me.dtiFechaInicioArqueo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtiFechaInicioArqueo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.TodayButtonVisible = True
        Me.dtiFechaInicioArqueo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtiFechaInicioArqueo.Name = "dtiFechaInicioArqueo"
        Me.dtiFechaInicioArqueo.Size = New System.Drawing.Size(212, 20)
        Me.dtiFechaInicioArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtiFechaInicioArqueo.TabIndex = 1
        Me.dtiFechaInicioArqueo.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 11.25!)
        Me.dtiFechaInicioArqueo.WatermarkText = "Fecha Inicio Arqueo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label2.Location = New System.Drawing.Point(60, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 13)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Fecha Inicio Arqueo:"
        '
        'lbltxtUsuario
        '
        Me.lbltxtUsuario.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lbltxtUsuario.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.lbltxtUsuario.BackgroundStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.lbltxtUsuario.BackgroundStyle.Class = ""
        Me.lbltxtUsuario.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lbltxtUsuario.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lbltxtUsuario.Location = New System.Drawing.Point(170, 28)
        Me.lbltxtUsuario.Name = "lbltxtUsuario"
        Me.lbltxtUsuario.Size = New System.Drawing.Size(212, 23)
        Me.lbltxtUsuario.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(119, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Usuario:"
        '
        'UltraGroupBox2
        '
        Me.UltraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox2.Controls.Add(Me.UltraGroupBox4)
        Me.UltraGroupBox2.Controls.Add(Me.UltraGroupBox3)
        Me.UltraGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UltraGroupBox2.Location = New System.Drawing.Point(1, 206)
        Me.UltraGroupBox2.Name = "UltraGroupBox2"
        Me.UltraGroupBox2.Size = New System.Drawing.Size(836, 275)
        Me.UltraGroupBox2.TabIndex = 2
        Me.UltraGroupBox2.Text = "Detalle Arqueo"
        Me.UltraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraGroupBox4
        '
        Me.UltraGroupBox4.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox4.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox4.Controls.Add(Me.dgxDetalleDolares)
        Me.UltraGroupBox4.Location = New System.Drawing.Point(419, 22)
        Me.UltraGroupBox4.Name = "UltraGroupBox4"
        Me.UltraGroupBox4.Size = New System.Drawing.Size(411, 278)
        Me.UltraGroupBox4.TabIndex = 2
        Me.UltraGroupBox4.Text = "Detalle Dolares"
        Me.UltraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgxDetalleDolares
        '
        Me.dgxDetalleDolares.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle43.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle43.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleDolares.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle43
        Me.dgxDetalleDolares.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgxDetalleDolares.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.UltraNumericEditorColumn1, Me.UltraNumericEditorColumn2})
        DataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle46.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle46.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle46.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle46.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgxDetalleDolares.DefaultCellStyle = DataGridViewCellStyle46
        Me.dgxDetalleDolares.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgxDetalleDolares.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgxDetalleDolares.Location = New System.Drawing.Point(3, 20)
        Me.dgxDetalleDolares.Name = "dgxDetalleDolares"
        DataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle47.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle47.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle47.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleDolares.RowHeadersDefaultCellStyle = DataGridViewCellStyle47
        Me.dgxDetalleDolares.Size = New System.Drawing.Size(405, 255)
        Me.dgxDetalleDolares.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        DataGridViewCellStyle44.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle44
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Denominación"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 120
        '
        'UltraNumericEditorColumn1
        '
        '
        '
        '
        Me.UltraNumericEditorColumn1.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.UltraNumericEditorColumn1.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.UltraNumericEditorColumn1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.UltraNumericEditorColumn1.BackgroundStyle.TextColor = System.Drawing.Color.Black
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle45.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraNumericEditorColumn1.DefaultCellStyle = DataGridViewCellStyle45
        Me.UltraNumericEditorColumn1.Frozen = True
        Me.UltraNumericEditorColumn1.HeaderText = "Cantidad"
        Me.UltraNumericEditorColumn1.Name = "UltraNumericEditorColumn1"
        Me.UltraNumericEditorColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UltraNumericEditorColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.UltraNumericEditorColumn1.Width = 90
        '
        'UltraNumericEditorColumn2
        '
        DataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltraNumericEditorColumn2.DefaultCellStyle = DataGridViewCellStyle35
        Me.UltraNumericEditorColumn2.DefaultNewRowValue = CType(resources.GetObject("UltraNumericEditorColumn2.DefaultNewRowValue"), Object)
        Me.UltraNumericEditorColumn2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.[Default]
        Me.UltraNumericEditorColumn2.Frozen = True
        Me.UltraNumericEditorColumn2.HeaderText = "Total"
        Me.UltraNumericEditorColumn2.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.UltraNumericEditorColumn2.MaskInput = Nothing
        Me.UltraNumericEditorColumn2.Name = "UltraNumericEditorColumn2"
        Me.UltraNumericEditorColumn2.PadChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.UltraNumericEditorColumn2.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraNumericEditorColumn2.ReadOnly = True
        Me.UltraNumericEditorColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UltraNumericEditorColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.UltraNumericEditorColumn2.SpinButtonAlignment = Infragistics.Win.SpinButtonDisplayStyle.None
        Me.UltraNumericEditorColumn2.Width = 150
        '
        'UltraGroupBox3
        '
        Me.UltraGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox3.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox3.Controls.Add(Me.dgxDetalleCordobas)
        Me.UltraGroupBox3.Location = New System.Drawing.Point(5, 22)
        Me.UltraGroupBox3.Name = "UltraGroupBox3"
        Me.UltraGroupBox3.Size = New System.Drawing.Size(411, 278)
        Me.UltraGroupBox3.TabIndex = 1
        Me.UltraGroupBox3.Text = "Detalle Córdobas"
        Me.UltraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgxDetalleCordobas
        '
        Me.dgxDetalleCordobas.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle48.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleCordobas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle48
        Me.dgxDetalleCordobas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Denominacion, Me.Cantidad, Me.Total})
        DataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle51.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle51.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle51.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle51.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle51.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle51.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgxDetalleCordobas.DefaultCellStyle = DataGridViewCellStyle51
        Me.dgxDetalleCordobas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgxDetalleCordobas.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgxDetalleCordobas.Location = New System.Drawing.Point(3, 20)
        Me.dgxDetalleCordobas.Name = "dgxDetalleCordobas"
        DataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle52.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle52.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle52.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle52.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle52.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleCordobas.RowHeadersDefaultCellStyle = DataGridViewCellStyle52
        Me.dgxDetalleCordobas.Size = New System.Drawing.Size(405, 255)
        Me.dgxDetalleCordobas.TabIndex = 1
        '
        'Denominacion
        '
        DataGridViewCellStyle49.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Denominacion.DefaultCellStyle = DataGridViewCellStyle49
        Me.Denominacion.Frozen = True
        Me.Denominacion.HeaderText = "Denominación"
        Me.Denominacion.Name = "Denominacion"
        Me.Denominacion.ReadOnly = True
        Me.Denominacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Denominacion.ToolTipText = "Denominación"
        Me.Denominacion.Width = 120
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.BackgroundStyle.TextColor = System.Drawing.Color.Black
        DataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle50.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cantidad.DefaultCellStyle = DataGridViewCellStyle50
        Me.Cantidad.Frozen = True
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Cantidad.ToolTipText = "Cantidad"
        Me.Cantidad.Width = 90
        '
        'Total
        '
        Appearance1.TextHAlignAsString = "Right"
        Me.Total.CellAppearance = Appearance1
        DataGridViewCellStyle40.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Total.DefaultCellStyle = DataGridViewCellStyle40
        Me.Total.DefaultNewRowValue = CType(resources.GetObject("Total.DefaultNewRowValue"), Object)
        Me.Total.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2007
        Me.Total.Frozen = True
        Me.Total.HeaderText = "Total"
        Me.Total.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.Total.MaskInput = Nothing
        Me.Total.Name = "Total"
        Me.Total.PadChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Total.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Total.ReadOnly = True
        Me.Total.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Total.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Total.SpinButtonAlignment = Infragistics.Win.SpinButtonDisplayStyle.None
        Me.Total.Width = 150
        '
        'TabControl1
        '
        Me.TabControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TabControl1.CanReorderTabs = True
        Me.TabControl1.Controls.Add(Me.TabControlPanel1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 63)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.SelectedTabIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(838, 508)
        Me.TabControl1.TabIndex = 3
        Me.TabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl1.Tabs.Add(Me.TabItem1)
        Me.TabControl1.Text = "TabControl1"
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel1
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Datos Generales Arqueo"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.ugbIngresoDatos)
        Me.TabControlPanel1.Controls.Add(Me.UltraGroupBox2)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(838, 482)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(142, Byte), Integer), CType(CType(179, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(97, Byte), Integer), CType(CType(156, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.TabItem1
        '
        'frmCuadreCajaInicial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(838, 571)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Bar1)
        Me.Name = "frmCuadreCajaInicial"
        Me.Text = "Arqueo Inicial"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ugbIngresoDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ugbIngresoDatos.ResumeLayout(False)
        Me.ugbIngresoDatos.PerformLayout()
        CType(Me.diDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diGastos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtiFechaFinArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diSaldoInicial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtiFechaInicioArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox2.ResumeLayout(False)
        CType(Me.UltraGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox4.ResumeLayout(False)
        CType(Me.dgxDetalleDolares, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox3.ResumeLayout(False)
        CType(Me.dgxDetalleCordobas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdGuardar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ugbIngresoDatos As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents diDeposito As DevComponents.Editors.DoubleInput
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents diGastos As DevComponents.Editors.DoubleInput
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroReciboFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroReciboInicial As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroFacturaFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroFacturaInicial As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtiFechaFinArqueo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents diSaldoInicial As DevComponents.Editors.DoubleInput
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbltxtNumeroArqueo As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtiFechaInicioArqueo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbltxtUsuario As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UltraGroupBox2 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox3 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents dgxDetalleCordobas As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents UltraGroupBox4 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents dgxDetalleDolares As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Denominacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents Total As Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltraNumericEditorColumn1 As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents UltraNumericEditorColumn2 As Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn
    Friend WithEvents TabControl1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
End Class
