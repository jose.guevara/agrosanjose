Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv

Public Class frmRecibos
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoCliente As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroRecibo As System.Windows.Forms.TextBox
    Friend WithEvents DTPFechaRecibo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroTarjeta As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents ddlFormaPago As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroRecibido As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcionRecibo As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtBanco As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdBuscar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdImprimir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents CmdAnular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents dgvxPagosFacturas As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents txtRefElectronica As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As Windows.Forms.Label
    Friend WithEvents txtTipoCambio As Windows.Forms.TextBox
    Friend WithEvents cbMoneda As ComboBox
    Friend WithEvents lblMoneda As Windows.Forms.Label
    Friend WithEvents lblDisponibleUSD As Windows.Forms.Label
    Friend WithEvents Label28 As Windows.Forms.Label
    Friend WithEvents NumFactura As DataGridViewTextBoxColumn
    Friend WithEvents MonedaFactura As DataGridViewTextBoxColumn
    Friend WithEvents FechaEmis As DataGridViewTextBoxColumn
    Friend WithEvents FechaVenc As DataGridViewTextBoxColumn
    Friend WithEvents SaldoFact As DataGridViewDoubleInputColumn
    Friend WithEvents SaldoFacturaUSD As DataGridViewDoubleInputColumn
    Friend WithEvents Monto As DataGridViewDoubleInputColumn
    Friend WithEvents NvoSaldo As DataGridViewDoubleInputColumn
    Friend WithEvents NvoSaldoUSD As DataGridViewDoubleInputColumn
    Friend WithEvents Interes As DataGridViewDoubleInputColumn
    Friend WithEvents MantValor As DataGridViewDoubleInputColumn
    Friend WithEvents PorAplicar As DataGridViewDoubleInputColumn
    Friend WithEvents registro As DataGridViewIntegerInputColumn
    Friend WithEvents IdMoneda As DataGridViewIntegerInputColumn
    Friend WithEvents Label23 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecibos))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim UltraStatusPanel3 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel4 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.txtCodigoCliente = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblDisponibleUSD = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cbMoneda = New System.Windows.Forms.ComboBox()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        Me.txtRefElectronica = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.ddlSerie = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtBanco = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtDescripcionRecibo = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.txtNumeroRecibido = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ddlFormaPago = New System.Windows.Forms.ComboBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtNumeroTarjeta = New System.Windows.Forms.TextBox()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.DTPFechaRecibo = New System.Windows.Forms.DateTimePicker()
        Me.txtNumeroRecibo = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgvxPagosFacturas = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.CmdAnular = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdBuscar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImprimir = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.NumFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonedaFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEmis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaVenc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoFact = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.SaldoFacturaUSD = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Monto = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.NvoSaldo = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.NvoSaldoUSD = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Interes = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.MantValor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.PorAplicar = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.registro = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn()
        Me.IdMoneda = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvxPagosFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.Text = "Clientes"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox20)
        Me.GroupBox1.Controls.Add(Me.TextBox19)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.TextBox15)
        Me.GroupBox1.Controls.Add(Me.TextBox14)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox8)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.txtCodigoCliente)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 71)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1183, 122)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes"
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(549, 96)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(136, 20)
        Me.TextBox20.TabIndex = 36
        Me.TextBox20.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox20.Visible = False
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(409, 96)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.Size = New System.Drawing.Size(274, 20)
        Me.TextBox19.TabIndex = 35
        Me.TextBox19.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(333, 96)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(67, 13)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Saldo US$"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(349, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 24)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "<F5> AYUDA"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(656, 72)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(8, 20)
        Me.TextBox15.TabIndex = 19
        Me.TextBox15.Tag = "C�digo del Cliente"
        Me.TextBox15.Visible = False
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(640, 72)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(8, 20)
        Me.TextBox14.TabIndex = 18
        Me.TextBox14.Tag = "C�digo del Cliente"
        Me.TextBox14.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(409, 72)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(274, 20)
        Me.TextBox5.TabIndex = 17
        Me.TextBox5.Tag = "Vendedor del Cliente"
        Me.TextBox5.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(354, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Negocio"
        Me.Label4.Visible = False
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(220, 96)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(109, 20)
        Me.TextBox8.TabIndex = 14
        Me.TextBox8.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox8.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(106, 96)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(220, 20)
        Me.TextBox7.TabIndex = 13
        Me.TextBox7.Tag = "Saldo total del Cliente en Moneda Nacional"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(466, 72)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(208, 20)
        Me.TextBox6.TabIndex = 12
        Me.TextBox6.Tag = "Vendedor del Cliente"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(106, 72)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(224, 20)
        Me.TextBox4.TabIndex = 10
        Me.TextBox4.Tag = "Departamento donde reside negocio/habitaci�n del cliente"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(106, 48)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(1047, 20)
        Me.TextBox3.TabIndex = 9
        Me.TextBox3.Tag = "Direcci�n del Cliente"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(409, 24)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(743, 20)
        Me.TextBox2.TabIndex = 8
        Me.TextBox2.Tag = "Nombre del Cliente"
        '
        'txtCodigoCliente
        '
        Me.txtCodigoCliente.Location = New System.Drawing.Point(106, 24)
        Me.txtCodigoCliente.Name = "txtCodigoCliente"
        Me.txtCodigoCliente.Size = New System.Drawing.Size(224, 20)
        Me.txtCodigoCliente.TabIndex = 0
        Me.txtCodigoCliente.Tag = "C�digo del Cliente"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Saldo C$"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(333, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Departamento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Direcci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblDisponibleUSD)
        Me.GroupBox2.Controls.Add(Me.Label28)
        Me.GroupBox2.Controls.Add(Me.cbMoneda)
        Me.GroupBox2.Controls.Add(Me.lblMoneda)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.txtTipoCambio)
        Me.GroupBox2.Controls.Add(Me.txtRefElectronica)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.ddlSerie)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.txtBanco)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.txtDescripcionRecibo)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.TextBox17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.TextBox16)
        Me.GroupBox2.Controls.Add(Me.txtNumeroRecibido)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.ddlFormaPago)
        Me.GroupBox2.Controls.Add(Me.txtDescripcion)
        Me.GroupBox2.Controls.Add(Me.txtNumeroTarjeta)
        Me.GroupBox2.Controls.Add(Me.txtMonto)
        Me.GroupBox2.Controls.Add(Me.DTPFechaRecibo)
        Me.GroupBox2.Controls.Add(Me.txtNumeroRecibo)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 193)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1183, 164)
        Me.GroupBox2.TabIndex = 16
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Recibo"
        '
        'lblDisponibleUSD
        '
        Me.lblDisponibleUSD.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblDisponibleUSD.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDisponibleUSD.Location = New System.Drawing.Point(819, 43)
        Me.lblDisponibleUSD.Name = "lblDisponibleUSD"
        Me.lblDisponibleUSD.Size = New System.Drawing.Size(159, 16)
        Me.lblDisponibleUSD.TabIndex = 43
        Me.lblDisponibleUSD.Text = "0.00"
        Me.lblDisponibleUSD.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(722, 45)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(96, 13)
        Me.Label28.TabIndex = 42
        Me.Label28.Text = "Disponible USD"
        '
        'cbMoneda
        '
        Me.cbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbMoneda.Items.AddRange(New Object() {"efectivo", "cheque", "tarj. cr�dito"})
        Me.cbMoneda.Location = New System.Drawing.Point(819, 14)
        Me.cbMoneda.Name = "cbMoneda"
        Me.cbMoneda.Size = New System.Drawing.Size(158, 21)
        Me.cbMoneda.TabIndex = 41
        Me.cbMoneda.Tag = "Forma de Pago del Recibo a Elaborar"
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(722, 15)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(52, 13)
        Me.lblMoneda.TabIndex = 40
        Me.lblMoneda.Text = "Moneda"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(7, 45)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(77, 13)
        Me.Label26.TabIndex = 39
        Me.Label26.Text = "Tipo Cambio"
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Location = New System.Drawing.Point(106, 43)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.ReadOnly = True
        Me.txtTipoCambio.Size = New System.Drawing.Size(164, 20)
        Me.txtTipoCambio.TabIndex = 38
        Me.txtTipoCambio.Tag = "Descripci�n del Recibo a Elaborar"
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRefElectronica
        '
        Me.txtRefElectronica.Location = New System.Drawing.Point(798, 90)
        Me.txtRefElectronica.MaxLength = 50
        Me.txtRefElectronica.Name = "txtRefElectronica"
        Me.txtRefElectronica.Size = New System.Drawing.Size(158, 20)
        Me.txtRefElectronica.TabIndex = 7
        Me.txtRefElectronica.Tag = "Referencia de la Transferencia Electronica"
        Me.txtRefElectronica.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(722, 95)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(43, 13)
        Me.Label25.TabIndex = 37
        Me.Label25.Text = "Ref. E"
        Me.Label25.Visible = False
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Items.AddRange(New Object() {"efectivo", "cheque", "tarj. cr�dito"})
        Me.ddlSerie.Location = New System.Drawing.Point(106, 15)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(164, 21)
        Me.ddlSerie.TabIndex = 34
        Me.ddlSerie.Tag = "Forma de Pago del Recibo a Elaborar"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(7, 16)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(36, 13)
        Me.Label24.TabIndex = 33
        Me.Label24.Text = "Serie"
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(425, 65)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(176, 24)
        Me.Label23.TabIndex = 32
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(828, 140)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 31
        Me.Label22.Text = "Label22"
        Me.Label22.Visible = False
        '
        'txtBanco
        '
        Me.txtBanco.Location = New System.Drawing.Point(346, 92)
        Me.txtBanco.MaxLength = 50
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(168, 20)
        Me.txtBanco.TabIndex = 5
        Me.txtBanco.Tag = "Banco emisor del cheque"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(273, 95)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Banco"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(518, 140)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(65, 13)
        Me.Label20.TabIndex = 28
        Me.Label20.Text = "Retenci�n"
        '
        'txtDescripcionRecibo
        '
        Me.txtDescripcionRecibo.Location = New System.Drawing.Point(585, 140)
        Me.txtDescripcionRecibo.Name = "txtDescripcionRecibo"
        Me.txtDescripcionRecibo.Size = New System.Drawing.Size(136, 20)
        Me.txtDescripcionRecibo.TabIndex = 9
        Me.txtDescripcionRecibo.Tag = "Descripci�n del Recibo a Elaborar"
        Me.txtDescripcionRecibo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(273, 140)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 13)
        Me.Label19.TabIndex = 26
        Me.Label19.Text = "Mant. Valor"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(346, 140)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(168, 20)
        Me.TextBox17.TabIndex = 25
        Me.TextBox17.Tag = "Descripci�n del Recibo a Elaborar"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 140)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(59, 13)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "Intereses"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(106, 140)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(164, 20)
        Me.TextBox16.TabIndex = 23
        Me.TextBox16.Tag = "Descripci�n del Recibo a Elaborar"
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNumeroRecibido
        '
        Me.txtNumeroRecibido.Location = New System.Drawing.Point(106, 116)
        Me.txtNumeroRecibido.MaxLength = 50
        Me.txtNumeroRecibido.Name = "txtNumeroRecibido"
        Me.txtNumeroRecibido.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtNumeroRecibido.Size = New System.Drawing.Size(849, 20)
        Me.txtNumeroRecibido.TabIndex = 8
        Me.txtNumeroRecibido.Tag = "N�mero del Recibo a Elaborar"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 13)
        Me.Label16.TabIndex = 21
        Me.Label16.Text = "Rcbo Provis"
        '
        'Label15
        '
        Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(585, 43)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(134, 16)
        Me.Label15.TabIndex = 20
        Me.Label15.Text = "0.0000"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(518, 45)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 13)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Disponible"
        '
        'ddlFormaPago
        '
        Me.ddlFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlFormaPago.Items.AddRange(New Object() {"Efectivo", "Cheque", "Tarj. Cr�dito", "Transferencia"})
        Me.ddlFormaPago.Location = New System.Drawing.Point(106, 92)
        Me.ddlFormaPago.Name = "ddlFormaPago"
        Me.ddlFormaPago.Size = New System.Drawing.Size(164, 21)
        Me.ddlFormaPago.TabIndex = 4
        Me.ddlFormaPago.Tag = "Forma de Pago del Recibo a Elaborar"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(106, 68)
        Me.txtDescripcion.MaxLength = 12
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(164, 20)
        Me.txtDescripcion.TabIndex = 3
        Me.txtDescripcion.Tag = "Descripci�n del Recibo a Elaborar"
        '
        'txtNumeroTarjeta
        '
        Me.txtNumeroTarjeta.Location = New System.Drawing.Point(585, 91)
        Me.txtNumeroTarjeta.MaxLength = 16
        Me.txtNumeroTarjeta.Name = "txtNumeroTarjeta"
        Me.txtNumeroTarjeta.Size = New System.Drawing.Size(136, 20)
        Me.txtNumeroTarjeta.TabIndex = 6
        Me.txtNumeroTarjeta.Tag = "N�mero del Cheque o Tarjeta de Cr�dito"
        '
        'txtMonto
        '
        Me.txtMonto.Location = New System.Drawing.Point(346, 43)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(168, 20)
        Me.txtMonto.TabIndex = 2
        Me.txtMonto.Tag = "Monto del Recibo a Elaborar"
        Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DTPFechaRecibo
        '
        Me.DTPFechaRecibo.CustomFormat = ""
        Me.DTPFechaRecibo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPFechaRecibo.Location = New System.Drawing.Point(585, 15)
        Me.DTPFechaRecibo.Name = "DTPFechaRecibo"
        Me.DTPFechaRecibo.Size = New System.Drawing.Size(136, 20)
        Me.DTPFechaRecibo.TabIndex = 2
        Me.DTPFechaRecibo.Tag = "Fecha del Recibo a Elaborar"
        '
        'txtNumeroRecibo
        '
        Me.txtNumeroRecibo.Location = New System.Drawing.Point(346, 15)
        Me.txtNumeroRecibo.Name = "txtNumeroRecibo"
        Me.txtNumeroRecibo.Size = New System.Drawing.Size(168, 20)
        Me.txtNumeroRecibo.TabIndex = 1
        Me.txtNumeroRecibo.Tag = "N�mero del Recibo a Elaborar"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 116)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(83, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Recibimos de"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(518, 95)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(59, 13)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "# Tarjeta"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 92)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Forma"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(273, 45)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(42, 13)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Monto"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(518, 15)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Fecha"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(273, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "N�mero"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvxPagosFacturas)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 359)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1183, 181)
        Me.GroupBox4.TabIndex = 18
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Pagos a Facturas"
        '
        'dgvxPagosFacturas
        '
        Me.dgvxPagosFacturas.AllowUserToAddRows = False
        Me.dgvxPagosFacturas.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvxPagosFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxPagosFacturas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumFactura, Me.MonedaFactura, Me.FechaEmis, Me.FechaVenc, Me.SaldoFact, Me.SaldoFacturaUSD, Me.Monto, Me.NvoSaldo, Me.NvoSaldoUSD, Me.Interes, Me.MantValor, Me.PorAplicar, Me.registro, Me.IdMoneda})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxPagosFacturas.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvxPagosFacturas.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxPagosFacturas.Location = New System.Drawing.Point(6, 19)
        Me.dgvxPagosFacturas.Name = "dgvxPagosFacturas"
        Me.dgvxPagosFacturas.Size = New System.Drawing.Size(1164, 156)
        Me.dgvxPagosFacturas.TabIndex = 54
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 15000
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.LabelItem1, Me.CmdAnular, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem4, Me.cmdBuscar, Me.LabelItem5, Me.cmdImprimir, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(1187, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 34
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        Me.cmdOK.Visible = False
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'CmdAnular
        '
        Me.CmdAnular.Image = CType(resources.GetObject("CmdAnular.Image"), System.Drawing.Image)
        Me.CmdAnular.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.CmdAnular.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.CmdAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.CmdAnular.Name = "CmdAnular"
        Me.CmdAnular.Text = "Anular<F3>"
        Me.CmdAnular.Tooltip = "Anular Recibo"
        Me.CmdAnular.Visible = False
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'cmdBuscar
        '
        Me.cmdBuscar.Image = CType(resources.GetObject("cmdBuscar.Image"), System.Drawing.Image)
        Me.cmdBuscar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdBuscar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdBuscar.Name = "cmdBuscar"
        Me.cmdBuscar.Text = "Buscar<F6>"
        Me.cmdBuscar.Tooltip = "Extraer datos del recibo"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = "  "
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Image = CType(resources.GetObject("cmdImprimir.Image"), System.Drawing.Image)
        Me.cmdImprimir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimir.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.cmdImprimir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Text = "Imprimir<F8>"
        Me.cmdImprimir.Tooltip = "Imprimir recibos"
        Me.cmdImprimir.Visible = False
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "  "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 547)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel3, UltraStatusPanel4})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(1187, 23)
        Me.UltraStatusBar1.TabIndex = 35
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'NumFactura
        '
        Me.NumFactura.HeaderText = "Numero Factura"
        Me.NumFactura.Name = "NumFactura"
        Me.NumFactura.ReadOnly = True
        '
        'MonedaFactura
        '
        Me.MonedaFactura.HeaderText = "Moneda"
        Me.MonedaFactura.Name = "MonedaFactura"
        Me.MonedaFactura.ReadOnly = True
        '
        'FechaEmis
        '
        Me.FechaEmis.HeaderText = "Fecha Emis."
        Me.FechaEmis.Name = "FechaEmis"
        Me.FechaEmis.ReadOnly = True
        '
        'FechaVenc
        '
        Me.FechaVenc.HeaderText = "Fecha Vence"
        Me.FechaVenc.Name = "FechaVenc"
        Me.FechaVenc.ReadOnly = True
        '
        'SaldoFact
        '
        '
        '
        '
        Me.SaldoFact.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.SaldoFact.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.SaldoFact.DisplayFormat = "N4"
        Me.SaldoFact.HeaderText = "Saldo Factura COR"
        Me.SaldoFact.Increment = 1.0R
        Me.SaldoFact.Name = "SaldoFact"
        Me.SaldoFact.ReadOnly = True
        Me.SaldoFact.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'SaldoFacturaUSD
        '
        '
        '
        '
        Me.SaldoFacturaUSD.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.SaldoFacturaUSD.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.SaldoFacturaUSD.DisplayFormat = "N4"
        Me.SaldoFacturaUSD.HeaderText = "Saldo Factura USD"
        Me.SaldoFacturaUSD.Increment = 1.0R
        Me.SaldoFacturaUSD.Name = "SaldoFacturaUSD"
        Me.SaldoFacturaUSD.ReadOnly = True
        Me.SaldoFacturaUSD.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Monto
        '
        '
        '
        '
        Me.Monto.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.Monto.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Monto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Monto.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText
        Me.Monto.DisplayFormat = "N4"
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Increment = 1.0R
        Me.Monto.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Monto.Name = "Monto"
        Me.Monto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'NvoSaldo
        '
        '
        '
        '
        Me.NvoSaldo.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.NvoSaldo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.NvoSaldo.DisplayFormat = "N4"
        Me.NvoSaldo.HeaderText = "Nvo. Saldo COR"
        Me.NvoSaldo.Increment = 1.0R
        Me.NvoSaldo.Name = "NvoSaldo"
        Me.NvoSaldo.ReadOnly = True
        Me.NvoSaldo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'NvoSaldoUSD
        '
        '
        '
        '
        Me.NvoSaldoUSD.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.NvoSaldoUSD.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.NvoSaldoUSD.DisplayFormat = "N4"
        Me.NvoSaldoUSD.HeaderText = "Nvo. Saldo USD"
        Me.NvoSaldoUSD.Increment = 1.0R
        Me.NvoSaldoUSD.Name = "NvoSaldoUSD"
        Me.NvoSaldoUSD.ReadOnly = True
        Me.NvoSaldoUSD.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Interes
        '
        '
        '
        '
        Me.Interes.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.Interes.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Interes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Interes.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText
        Me.Interes.HeaderText = "Interes"
        Me.Interes.Increment = 1.0R
        Me.Interes.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Interes.Name = "Interes"
        Me.Interes.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'MantValor
        '
        '
        '
        '
        Me.MantValor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.MantValor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MantValor.HeaderText = "Mant. Valor"
        Me.MantValor.Increment = 1.0R
        Me.MantValor.Name = "MantValor"
        Me.MantValor.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'PorAplicar
        '
        '
        '
        '
        Me.PorAplicar.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.PorAplicar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.PorAplicar.HeaderText = "Por Aplicar"
        Me.PorAplicar.Increment = 1.0R
        Me.PorAplicar.Name = "PorAplicar"
        Me.PorAplicar.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.PorAplicar.Visible = False
        '
        'registro
        '
        '
        '
        '
        Me.registro.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.registro.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.registro.HeaderText = "registro"
        Me.registro.Name = "registro"
        Me.registro.ReadOnly = True
        Me.registro.Visible = False
        '
        'IdMoneda
        '
        '
        '
        '
        Me.IdMoneda.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.IdMoneda.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IdMoneda.HeaderText = "IdMoneda"
        Me.IdMoneda.Name = "IdMoneda"
        Me.IdMoneda.ReadOnly = True
        Me.IdMoneda.Visible = False
        '
        'frmRecibos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1187, 570)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRecibos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRecibos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvxPagosFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim liTipoReciboCaja As Integer = 0
    Dim strRecibo As String = String.Empty
    Dim nUltimoNumeroRecibo As Integer = 0
    Dim intContar As Integer = 0
    Dim intImprimir As Integer = 0
    Dim SaldoAnterior As Double = 0
    Dim strCodCliente As String = String.Empty
    Dim txtCollection As New Collection
    Dim dblMontoRecibo, dblInteresRecibo, dblMntVlrRecibo, dblPendienteRecibo As Double
    Private gnTipoCambio As Double = 0
    Private gnCantidadDecimal As Integer = 4
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmRecibos"
    Dim frmLogin2 As frmLoginSuprvisor = Nothing
    Dim gbIndicaCierre As Boolean = False
    Private Sub frmRecibos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Top = 0
        'Me.Left = 0
        Dim lnFechaNum As Integer = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnTasaCambioDelDia As Decimal = 0
        Try

            Me.StartPosition = FormStartPosition.CenterScreen 'Posicion del formulario en la pantalla

            liTipoReciboCaja = intTipoReciboCaja
            Label22.Text = intTipoReciboCaja

            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambioDelDia = RNTipoCambio.ObtieneTipoCambioDelDia()

            'If objParametro.MonedaFacturaCredito = 2 And lnTasaCambioDelDia <= 0 Then
            If lnTasaCambioDelDia <= 0 Then
                MsgBox("No se ha ingresado la tasa del d�a, favor ingresar la tasa del d�a antes de realizar operaciones ", MsgBoxStyle.Critical, "Error de Datos")
                gbIndicaCierre = True
                'Me.Close()
            End If
            txtTipoCambio.Text = lnTasaCambioDelDia
            Select Case liTipoReciboCaja
                Case ENTipoRecibo.RECIBO_CAJA,
                ENTipoRecibo.RECIBO_MANUAL
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                    cmdBuscar.Visible = False
                Case ENTipoRecibo.RECIBO_IMPRIMIR
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                    FormatearGridDetalle()
                    'dgvxPagosFacturas.Enabled = False
                    dgvxPagosFacturas.Columns("Monto").ReadOnly = True
                    dgvxPagosFacturas.Columns("Interes").ReadOnly = True
                    dgvxPagosFacturas.Columns("MantValor").ReadOnly = True
                    dgvxPagosFacturas.Columns("PorAplicar").ReadOnly = True
                Case ENTipoRecibo.RECIBO_ANULAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
                    FormatearGridDetalle()
                    'dgvxPagosFacturas.Enabled = False
                    dgvxPagosFacturas.Columns("Monto").ReadOnly = True
                    dgvxPagosFacturas.Columns("Interes").ReadOnly = True
                    dgvxPagosFacturas.Columns("MantValor").ReadOnly = True
                    dgvxPagosFacturas.Columns("PorAplicar").ReadOnly = True
            End Select

            RNMoneda.CargaComboMoneda(cbMoneda)
            DTPFechaRecibo.Value = Now
            lnFechaNum = SUConversiones.ConvierteADecimal(Format(DTPFechaRecibo.Value, "yyyyMMdd"))
            gnTipoCambio = 0
            gnTipoCambio = RNTipoCambio.ObtieneTipoCambio(lnFechaNum)
            txtTipoCambio.Text = gnTipoCambio
            txtCollection.Add(txtCodigoCliente)
            txtCollection.Add(TextBox2)
            txtCollection.Add(TextBox3)
            txtCollection.Add(TextBox4)
            txtCollection.Add(TextBox5)
            txtCollection.Add(TextBox6)
            txtCollection.Add(TextBox7)
            txtCollection.Add(TextBox8)
            txtCollection.Add(txtNumeroRecibo)
            txtCollection.Add(txtMonto)
            txtCollection.Add(txtNumeroTarjeta)
            txtCollection.Add(txtDescripcion)
            txtCollection.Add(txtNumeroRecibido)
            txtCollection.Add(txtTipoCambio)
            Limpiar()

            dblMontoRecibo = 0
            dblInteresRecibo = 0
            dblMntVlrRecibo = 0
            intImprimir = 0
            'For intIncr = 0 To 9
            For intIncr = 0 To 11
                arrRecibosExtra(intIncr) = String.Empty
            Next intIncr
            For intIncr = 0 To 7
                arrRecibo(intIncr, 0) = String.Empty
                arrRecibo(intIncr, 1) = String.Empty
                arrRecibo(intIncr, 2) = String.Empty
                arrRecibo(intIncr, 3) = String.Empty
                arrRecibo(intIncr, 4) = String.Empty
            Next intIncr
            intContar = 0
            ' If intTipoReciboCaja = 1 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Then
                txtNumeroRecibo.ReadOnly = True
            End If
            ' If intTipoReciboCaja = 9 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_IMPRIMIR Then
                'Me.ToolBar1.Buttons.Item(0).Enabled = False
                cmdImprimir.Visible = True
            End If
            Label22.Text = intTipoReciboCaja
            UbicarAgencia(lngRegUsuario)
            If (lngRegAgencia = 0) Then
                lngRegAgencia = 0
                Dim frmNew As New frmEscogerAgencia
                frmNew.ShowDialog()
                If lngRegAgencia = 0 Then
                    MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
                End If
            End If
            ObtieneSeries()

            'If intTipoReciboCaja = 1 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Then
                nUltimoNumeroRecibo = SUConversiones.ConvierteAInt(ExtraerSerieNumeroRecibo(SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem)))
                txtNumeroRecibo.Text = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(nUltimoNumeroRecibo), "0000000")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Public Sub FormatearGridDetalle()
        Try
            dgvxPagosFacturas.Columns("FechaEmis").HeaderText = "Concepto"
            dgvxPagosFacturas.Columns("FechaEmis").Width = 220
            ' dgvxPagosFacturas.Columns("MonedaFactura").Visible = False
            dgvxPagosFacturas.Columns("FechaVenc").Visible = False
            dgvxPagosFacturas.Columns("SaldoFact").Visible = False
            dgvxPagosFacturas.Columns("SaldoFacturaUSD").Visible = False
            dgvxPagosFacturas.Columns("NvoSaldo").Visible = False
            dgvxPagosFacturas.Columns("NvoSaldoUSD").Visible = False
            dgvxPagosFacturas.Columns("PorAplicar").Visible = False

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmRecibos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try

            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                ' If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
                If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ANULAR Then
                    If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                    Else
                        MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                        Exit Sub
                    End If
                    Anular()
                    'ElseIf intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                ElseIf intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                    Guardar()
                End If
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If Label17.Visible = True Then
                    intListadoAyuda = 2
                    Dim frmNew As New frmListadoAyuda
                    'Timer1.Interval = 200
                    'Timer1.Enabled = True
                    frmNew.ShowDialog()
                End If
            ElseIf e.KeyCode = Keys.F6 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                ' If intTipoReciboCaja <> 1 And intTipoReciboCaja <> 15 Then
                If intTipoReciboCaja <> ENTipoRecibo.RECIBO_CAJA And intTipoReciboCaja <> ENTipoRecibo.RECIBO_MANUAL Then
                    Extraer()
                End If
            ElseIf e.KeyCode = Keys.F7 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                    VistaPrevia()
                End If
            ElseIf e.KeyCode = Keys.F8 Then
                'If intTipoReciboCaja = 9 Then
                If intTipoReciboCaja = ENTipoRecibo.RECIBO_IMPRIMIR Then
                    'Me.ToolBarButton5.Visible = True
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    ImprimirRecibo()
                End If
            End If

            ' Select Case intTipoReciboCaja
            Select Case liTipoReciboCaja
                Case ENTipoRecibo.RECIBO_CAJA,
                    ENTipoRecibo.RECIBO_MANUAL
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                    cmdBuscar.Visible = False
                Case ENTipoRecibo.RECIBO_IMPRIMIR
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                Case ENTipoRecibo.RECIBO_ANULAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem12.Click, MenuItem15.Click

        Try
            intListadoAyuda = 0
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            Select Case sender.text.ToString().Trim()
                Case "Guardar" : Guardar()
                Case "Cancelar", "Limpiar" : Limpiar()
                Case "Clientes" : intListadoAyuda = 2
                Case "&Salir" : Me.Close()
            End Select
            If intListadoAyuda <> 0 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)


    End Sub

    Sub Limpiar()

        Try

            For intIncr = 1 To 13
                txtCollection.Item(intIncr).text = ""
            Next
            txtMonto.Text = "0.00"
            TextBox14.Text = "0"
            TextBox15.Text = "0"
            TextBox16.Text = "0.00"
            TextBox17.Text = "0.00"
            txtDescripcionRecibo.Text = "0.00"
            dblMontoRecibo = 0
            dblInteresRecibo = 0
            dblMntVlrRecibo = 0
            DTPFechaRecibo.Value = Now
            If ddlFormaPago.Items.Count > 0 Then
                ddlFormaPago.SelectedIndex = 0
            End If
            If ddlSerie.Items.Count > 0 Then
                ddlSerie.SelectedIndex = 0
            End If
            ' ddlSerie.SelectedIndex = 0
            Label15.Text = dblMontoRecibo
            lblDisponibleUSD.Text = dblMontoRecibo
            If SUConversiones.ConvierteAInt(cbMoneda.SelectedValue) = 2 Then
                If SUConversiones.ConvierteADecimal(txtTipoCambio.Text) <> 0 Then
                    lblDisponibleUSD.Text = SUFunciones.RedondeoNumero((dblMontoRecibo / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), "T", gnCantidadDecimal)
                    Label15.Text = SUFunciones.RedondeoNumero((dblMontoRecibo * SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), "T", gnCantidadDecimal)
                End If

            End If
            TextBox16.Text = dblInteresRecibo
            TextBox17.Text = dblMntVlrRecibo
            ' If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_IMPRIMIR Then
                For intIncr = 1 To 13
                    txtCollection.Item(intIncr).readonly = True
                Next
                txtCodigoCliente.ReadOnly = False
                txtNumeroRecibo.ReadOnly = False
                ddlFormaPago.Enabled = False
                txtDescripcionRecibo.ReadOnly = True
                txtBanco.ReadOnly = True
                DTPFechaRecibo.Enabled = False
            End If
            'txtNumeroRecibo.Text = "R-"
            txtNumeroRecibo.Text = String.Empty
            txtCodigoCliente.Focus()
            If dgvxPagosFacturas.Rows.Count > 0 Then
                dgvxPagosFacturas.Rows.Clear()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub Extraer()
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                If TextBox14.Text = "0" Then
                    MsgBox("Tiene que digitar el codigo del cliente.", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
            End If
            If txtNumeroRecibo.Text = String.Empty Then
                MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            strNumeroFact = String.Empty
            Me.Cursor = Cursors.WaitCursor
            txtMonto.Text = "0.00"
            Label15.Text = "0.0000"
            lblDisponibleUSD.Text = "0.0000"
            ddlFormaPago.SelectedIndex = 0
            txtNumeroTarjeta.Text = String.Empty
            txtDescripcion.Text = String.Empty
            txtNumeroRecibido.Text = String.Empty
            TextBox16.Text = "0.00"
            TextBox17.Text = "0.00"
            txtDescripcionRecibo.Text = "0.00"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            strQuery = String.Empty

            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                dtrAgro2K = RNRecibos.ObtieneInformacionReciboXClienteNumeroRecibos(SUConversiones.ConvierteAInt(TextBox14.Text.Trim()), UCase(txtNumeroRecibo.Text))
            Else
                If SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                    strNumeroFact = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                Else
                    strNumeroFact = txtNumeroRecibo.Text.Trim()
                End If
                dtrAgro2K = RNRecibos.ObtenerInformacionRecibo(strNumeroFact)
            End If
            intContar = 0
            If dgvxPagosFacturas.Rows.Count > 0 Then
                dgvxPagosFacturas.Rows.Clear()
            End If
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    txtCodigoCliente.Text = dtrAgro2K.GetValue(31)
                    'If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then
                    If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_IMPRIMIR Then
                        If dtrAgro2K.Item("DescripcionPago") <> "Retenci�n del Recibo" Then
                            'dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("DescripcionPago"), " ", " ", "", "", Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("MontoAPagarFactura")), "#,##0.#0"), " ", Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("interes")), "#,##0.#0"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("mantvlr")), "#,##0.#0"), 0, "", "")
                            dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("Moneda"), dtrAgro2K.Item("DescripcionPago"), " ", " ", " ", Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("MontoAPagarFactura")), "#,##0.#0"), " ", " ", Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("interes")), "#,##0.#0"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("mantvlr")), "#,##0.#0"), SUConversiones.ConvierteADecimal(dtrAgro2K.Item("registro")), SUConversiones.ConvierteAInt(dtrAgro2K.Item("IdMoneda")))
                            'dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("DescripcionPago"), " ", " ", Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("MontoAPagarFactura")), "#,##0.#0"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("MontoDescuentoDet")), "#,##0.#0"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("InteresDet")), "#,##0.#0"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("mantvlr")), "#,##0.#0"))
                        End If

                    End If
                    If dtrAgro2K.Item("DescripcionPago") <> "Retenci�n del Recibo" Then
                        arrRecibo(intContar, 0) = dtrAgro2K.Item("DescripcionPago")
                        arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("MontoAPagarFactura")), "#,##0.#0")
                        arrRecibo(intContar, 2) = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("Interes")), "#,##0.#0")
                        arrRecibo(intContar, 3) = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("mantvlr")), "#,##0.#0")
                        arrRecibo(intContar, 4) = "C"
                    End If
                    intContar = intContar + 1
                    txtMonto.Text = Format(dtrAgro2K.GetValue(6), "#,##0.#0")
                    TextBox16.Text = dtrAgro2K.GetValue(7)
                    txtDescripcionRecibo.Text = dtrAgro2K.GetValue(8)
                    Label15.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(18)), "#,##0.####")
                    lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(18)), "#,##0.####")
                    If SUConversiones.ConvierteADecimal(dtrAgro2K.Item("IdMoneda")) = 2 Then
                        If SUConversiones.ConvierteADecimal(dtrAgro2K.Item("TipoCambio")) <> 0 Then
                            lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(18))), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
                                Label15.Text = 0
                            Else
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(18)) * SUConversiones.ConvierteADecimal(dtrAgro2K.Item("TipoCambio"))), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            End If


                        End If
                    End If
                    ddlFormaPago.SelectedIndex = dtrAgro2K.GetValue(9)
                    txtNumeroTarjeta.Text = dtrAgro2K.GetValue(10)
                    txtBanco.Text = dtrAgro2K.GetValue(11)
                    txtRefElectronica.Text = dtrAgro2K.GetValue(12)
                    txtDescripcion.Text = dtrAgro2K.GetValue(13)
                    txtNumeroRecibido.Text = dtrAgro2K.GetValue(14)
                    DTPFechaRecibo.Value = dtrAgro2K.GetValue(17)
                    TextBox17.Text = SUConversiones.ConvierteADecimal(TextBox17.Text) + SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(28))
                    Label23.Text = ""
                    strNumRec = strNumeroFact
                    If dtrAgro2K.GetValue(19) = 1 Then
                        Label23.Text = "ANULADO"
                    End If
                    'If intTipoReciboCaja = 4 Then
                    If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Then
                        If dtrAgro2K.GetValue(19) = 1 Then
                            dtrAgro2K.Close()
                            MsgBox("El recibo ya fue anulado.", MsgBoxStyle.Information, "Recibo Anulado")
                            Limpiar()
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    End If
                End While
                SeteaColorRojoEnGrid()
            End If
            TextBox16.Text = Format(SUConversiones.ConvierteADecimal(TextBox16.Text), "#,##0.#0")
            TextBox17.Text = Format(SUConversiones.ConvierteADecimal(TextBox17.Text), "#,##0.#0")
            txtDescripcionRecibo.Text = Format(SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.#0")
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            'If intTipoReciboCaja <> 4 Then
            If intTipoReciboCaja <> ENTipoRecibo.RECIBO_ANULAR Then
                'If txtMonto.Text = "" Or txtMonto.Text = "0.0000" Then
                If SUConversiones.ConvierteADecimal(txtMonto.Text) = 0 Then
                    MsgBox("El n�mero del recibo de caja no es valido.  Verifique.", MsgBoxStyle.Critical, "Error de Datos")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.Default

            ' If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_IMPRIMIR Then
                UbicarCliente(txtCodigoCliente.Text.Trim())
                ' LlenarGrid(txtCodigoCliente.Text.Trim())
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub SeteaColorRojoEnGrid()
        Dim i As Integer = 0
        Try

            If dgvxPagosFacturas.Rows.Count >= 1 Then
                For i = 0 To dgvxPagosFacturas.Rows.Count - 1
                    If dgvxPagosFacturas.Rows(i).Cells("IdMoneda").Value = 1 Then
                        dgvxPagosFacturas.Rows(i).DefaultCellStyle.BackColor = Color.Yellow
                    End If

                Next
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    'Private Sub CalculaDisponibleRecibo()
    '    Dim dblMontoRecibo As Decimal = 0
    '    Dim lnMonedaRecibo As Integer = 0
    '    Dim lnMonedaFactura As Integer = 0
    '    Dim lnTipoCambio As Decimal = 0
    '    Dim lnMontoTotalAPagar As Decimal = 0
    '    Dim lnMontoTotalInteresAPagar As Decimal = 0
    '    Dim lnMontoTotalMantenimientoValorAPagar As Decimal = 0
    '    Dim lnMontoTotal As Decimal = 0
    '    Dim objParametro As SEParametroEmpresa = Nothing
    '    Try
    '        lnMonedaRecibo = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
    '        dblMontoRecibo = SUConversiones.ConvierteADecimal(txtMonto.Text)
    '        lnTipoCambio = SUConversiones.ConvierteADecimal(txtTipoCambio.Text)
    '        objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()

    '        dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '        'lnTipoCambio = Math.Round(lnTipoCambio, 2)
    '        lnTipoCambio = lnTipoCambio
    '        lnMontoTotalAPagar = 0
    '        lnMontoTotalInteresAPagar = 0
    '        If dgvxPagosFacturas.Rows.Count >= 1 Then
    '            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
    '                lnMonedaFactura = 0
    '                ' If dblMontoRecibo > 0 Then
    '                lnMontoTotalAPagar = 0
    '                lnMontoTotalInteresAPagar = 0
    '                lnMontoTotalMantenimientoValorAPagar = 0
    '                lnMonedaFactura = SUConversiones.ConvierteAInt(dgvxPagosFacturas.Rows(intIncr).Cells("IdMoneda").Value)
    '                lnMontoTotalAPagar = lnMontoTotalAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
    '                lnMontoTotalInteresAPagar = lnMontoTotalInteresAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
    '                lnMontoTotalMantenimientoValorAPagar = lnMontoTotalMantenimientoValorAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)

    '                If lnMonedaRecibo <> lnMonedaFactura Then
    '                    If lnMonedaRecibo = 2 Then
    '                        lnMontoTotalAPagar = SUFunciones.RedondeoNumero((lnMontoTotalAPagar / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                        lnMontoTotalInteresAPagar = SUFunciones.RedondeoNumero((lnMontoTotalInteresAPagar / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                        lnMontoTotalMantenimientoValorAPagar = SUFunciones.RedondeoNumero((lnMontoTotalMantenimientoValorAPagar / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                    Else
    '                        'lnMontoTotalAPagar = Math.Round((lnMontoTotalAPagar * lnTipoCambio), 2)
    '                        'lnMontoTotalAPagar = Math.Round((lnMontoTotalAPagar * lnTipoCambio), 2)
    '                        lnMontoTotalAPagar = SUFunciones.RedondeoNumero((lnMontoTotalAPagar * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                        'lnMontoTotalAPagar = SUFunciones.RedondeoNumero((lnMontoTotalAPagar * lnTipoCambio), "R", 2)
    '                        lnMontoTotalInteresAPagar = SUFunciones.RedondeoNumero((lnMontoTotalInteresAPagar * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                        lnMontoTotalMantenimientoValorAPagar = SUFunciones.RedondeoNumero((lnMontoTotalMantenimientoValorAPagar * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                    End If
    '                End If
    '                lnMontoTotal = 0
    '                lnMontoTotal = lnMontoTotalAPagar + lnMontoTotalInteresAPagar + lnMontoTotalMantenimientoValorAPagar
    '                dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal) - SUFunciones.RedondeoNumero(lnMontoTotal, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '                'End If
    '                'If lnMonedaFactura = lnMonedaRecibo Then
    '                '    dblMontoRecibo = dblMontoRecibo
    '                '    'dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
    '                '    'dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
    '                '    'dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
    '                'Else
    '                '    If lnMonedaFactura = 2 Then
    '                '        dblMontoRecibo = Math.Round((dblMontoRecibo * lnTipoCambio), 2)
    '                '    Else
    '                '        dblMontoRecibo = Math.Round((dblMontoRecibo / lnTipoCambio), 2)
    '                '    End If
    '                'End If

    '            Next intIncr

    '            'dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
    '            'dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
    '            dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
    '            Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.####")
    '            lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(Label15.Text), "#,##0.####")
    '            If lnTipoCambio <> 0 Then
    '                If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
    '                    lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
    '                    If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
    '                        Label15.Text = 0
    '                    Else
    '                        ' Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
    '                        Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
    '                    End If
    '                Else
    '                    Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
    '                    If SUConversiones.ConvierteADecimal(Label15.Text.Trim()) = 0 Then
    '                        lblDisponibleUSD.Text = 0
    '                    Else
    '                        lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
    '                    End If

    '                End If

    '            End If
    '            If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
    '                Label15.Text = "0.0000"
    '            End If
    '            If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
    '                Label15.ForeColor = System.Drawing.Color.Red
    '            Else
    '                Label15.ForeColor = System.Drawing.Color.Black
    '            End If
    '            If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
    '                lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
    '            Else
    '                lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
    '            End If
    '        End If
    '    Catch ex As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = ex.Message.ToString()
    '        Me.Cursor = Cursors.Default
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        Me.Cursor = Cursors.Default
    '        Throw ex
    '        'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    '    Me.Cursor = Cursors.Default
    'End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoCliente.KeyDown, txtNumeroRecibo.KeyDown, DTPFechaRecibo.KeyDown, txtMonto.KeyDown, ddlFormaPago.KeyDown, txtNumeroTarjeta.KeyDown, txtDescripcion.KeyDown, txtNumeroRecibido.KeyDown, txtDescripcionRecibo.KeyDown, txtBanco.KeyDown, ddlSerie.KeyDown, txtRefElectronica.KeyDown
        Dim lnTipoCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
                objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                Select Case intTipoReciboCaja
                    Case ENTipoRecibo.RECIBO_CAJA,
                        ENTipoRecibo.RECIBO_MANUAL
                        cmdOK.Visible = True
                        CmdAnular.Visible = False
                        cmdImprimir.Visible = False
                        cmdBuscar.Visible = False
                    Case ENTipoRecibo.RECIBO_IMPRIMIR
                        cmdOK.Visible = False
                        CmdAnular.Visible = False
                        cmdImprimir.Visible = True
                        cmdBuscar.Visible = True
                    Case ENTipoRecibo.RECIBO_ANULAR
                        cmdOK.Visible = False
                        CmdAnular.Visible = True
                        cmdImprimir.Visible = False
                End Select
                Select Case sender.name.ToString()
                    Case "txtCodigoCliente"
                        If UbicarCliente(txtCodigoCliente.Text) Then
                            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                                DTPFechaRecibo.Focus()
                            Else
                                txtNumeroRecibo.Focus()
                            End If
                            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                                LlenarGrid(txtCodigoCliente.Text)
                                strCodCliente = txtCodigoCliente.Text
                            End If
                        End If
                    Case "ddlSerie"
                        'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                        If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                            DTPFechaRecibo.Focus()
                        Else
                            txtNumeroRecibo.Focus()
                        End If

                    Case "txtNumeroRecibo"
                        DTPFechaRecibo.Focus()
                        'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                        If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                            UbicarRecibo()
                            'ElseIf intTipoReciboCaja <> 1 Then
                        ElseIf intTipoReciboCaja <> ENTipoRecibo.RECIBO_CAJA Then
                            Extraer()
                        End If
                    Case "DTPFechaRecibo" : txtMonto.Focus()
                    Case "txtMonto"
                        Label15.Text = "0.0000"
                        lblDisponibleUSD.Text = "0.0000"
                        If IsNumeric(txtMonto.Text) Then
                            txtMonto.Text = Format(SUConversiones.ConvierteADecimal(txtMonto.Text), "#,##0.##")
                            'If intTipoReciboCaja <> 2 And intTipoReciboCaja <> 5 Then
                            If intTipoReciboCaja <> ENTipoNotas.NOTA_CREDITO_ELABORAR And intTipoReciboCaja <> ENTipoNotas.NOTA_CREDITO_ANULAR Then
                                txtDescripcion.Focus()
                            Else
                                txtNumeroRecibido.Focus()
                            End If
                            dblMontoRecibo = SUConversiones.ConvierteADecimal(txtMonto.Text) - SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text)
                            Label15.Text = SUConversiones.ConvierteADecimal(txtMonto.Text)
                            If SUConversiones.ConvierteADecimal(txtTipoCambio.Text) <> 0 Then
                                lblDisponibleUSD.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(txtMonto.Text) / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                            End If
                            CalculaDisponibleRecibo()
                        End If
                    Case "ddlFormaPago"
                        If ddlFormaPago.SelectedIndex = 0 Then
                            txtDescripcion.Focus()
                        ElseIf ddlFormaPago.SelectedIndex = 1 Then
                            txtBanco.Focus()
                        ElseIf ddlFormaPago.SelectedIndex = 2 Then
                            txtNumeroTarjeta.Focus()
                        ElseIf ddlFormaPago.SelectedIndex = 3 Then
                            txtBanco.Focus()
                        End If

                    Case "txtNumeroTarjeta" : txtNumeroRecibido.Focus()
                    Case "txtRefElectronica" : txtNumeroRecibido.Focus()
                    Case "txtDescripcion"
                        If ddlFormaPago.SelectedIndex = 1 Then
                            txtBanco.Focus()
                        ElseIf ddlFormaPago.SelectedIndex = 2 Then
                            txtNumeroTarjeta.Focus()
                        ElseIf ddlFormaPago.SelectedIndex = 3 Then
                            txtBanco.Focus()
                        Else
                            txtNumeroRecibido.Focus()
                        End If
                    Case "txtBanco"
                        If ddlFormaPago.SelectedIndex = 1 Then
                            txtNumeroTarjeta.Focus()
                        ElseIf ddlFormaPago.SelectedIndex = 3 Then
                            txtRefElectronica.Focus()
                        Else
                            txtNumeroRecibido.Focus()
                        End If
                    Case "txtNumeroRecibido" : txtDescripcionRecibo.Focus()
                    Case "txtDescripcionRecibo"
                        lnTipoCambio = 0
                        lnTipoCambio = SUConversiones.ConvierteADecimal(txtTipoCambio.Text)

                        lnTipoCambio = SUFunciones.RedondeoNumero(lnTipoCambio, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                        txtDescripcionRecibo.Text = Format(SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.#0")
                        dblMontoRecibo = SUConversiones.ConvierteADecimal(txtMonto.Text) - SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text)

                        dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                        If dgvxPagosFacturas.Rows.Count >= 2 Then
                            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                                ''MSFlexGrid2.Col = 4
                                dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                                ''MSFlexGrid2.Col = 6
                                dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
                                ''MSFlexGrid2.Col = 7
                                dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
                            Next intIncr
                            Label15.Text = Format(dblMontoRecibo, "#,##0.####")
                            lblDisponibleUSD.Text = Format(dblMontoRecibo, "#,##0.####")
                            If lnTipoCambio <> 0 Then
                                'lblDisponibleUSD.Text = Format(Math.Round((dblMontoRecibo / lnTipoCambio), 4), "#,##0.####")
                                If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                                    lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                    'Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
                                    Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, 4), "#,##0.####")
                                Else
                                    Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                    lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                End If

                            End If
                            If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                                Label15.ForeColor = System.Drawing.Color.Red
                            Else
                                Label15.ForeColor = System.Drawing.Color.Black
                            End If
                            If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                                lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                            Else
                                lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                            End If
                            CalculaDisponibleRecibo()
                        End If
                        dgvxPagosFacturas.Focus()
                End Select
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Sub ObtieneSeries()
        Dim IndicaObtieneRegistro As Integer = 0

        Try
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            ddlSerie.SelectedItem = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "ObtieneSerieParametrosRecibo " & lngRegAgencia
            If strQuery.Trim.Length > 0 Then
                ddlSerie.Items.Clear()
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                    IndicaObtieneRegistro = 1
                End While
            End If
            If IndicaObtieneRegistro = 1 Then
                ddlSerie.SelectedIndex = 0
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MessageBoxEx.Show("Error " & ex.Message, " ObtieneSeries ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub UbicarRecibo()
        Dim strRecibotmp As String = String.Empty
        Dim ldtDatos As DataTable = Nothing
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'strQuery = ""
            If SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim) <> 0 Then
                strRecibotmp = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
            Else
                strRecibotmp = txtNumeroRecibo.Text
            End If
            UCase(txtNumeroRecibo.Text)
            'strQuery = "exec VerificaEstadoNumeroRecibo '" & strRecibotmp & "'"
            Try
                'cmdAgro2K.CommandText = strQuery
                'dtrAgro2K = cmdAgro2K.ExecuteReader
                ' While dtrAgro2K.Read
                ldtDatos = Nothing
                ldtDatos = RNRecibos.VerificaEstadoRecibo(strRecibotmp)
                If SUFunciones.ValidaDataTable(ldtDatos) Then
                    If SUConversiones.ConvierteAInt(ldtDatos.Rows(0).Item("Estado")) = 0 Then
                        ' If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                        If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                            MsgBox("El recibo de caja ya fue emitido.", MsgBoxStyle.Critical, "Recibo de Caja Emitido")
                        End If
                    ElseIf SUConversiones.ConvierteAInt(ldtDatos.Rows(0).Item("Estado")) = 1 Then
                        MsgBox("El recibo de caja ya fue anulado.", MsgBoxStyle.Critical, "Recibo de Caja Anulado")
                    End If
                    'If dtrAgro2K.GetValue(1) = 0 Then
                    '    ' If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                    '    If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                    '        MsgBox("El recibo de caja ya fue emitido.", MsgBoxStyle.Critical, "Recibo de Caja Emitido")
                    '    End If
                    'ElseIf dtrAgro2K.GetValue(1) = 1 Then
                    '    MsgBox("El recibo de caja ya fue anulado.", MsgBoxStyle.Critical, "Recibo de Caja Anulado")
                    'End If
                    txtNumeroRecibo.Focus()
                    txtNumeroRecibo.Text = ""
                End If
                'End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " UbicarRecibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoCliente.GotFocus, txtNumeroRecibo.GotFocus, DTPFechaRecibo.GotFocus, txtMonto.GotFocus, ddlFormaPago.GotFocus, txtNumeroTarjeta.GotFocus, txtDescripcion.GotFocus, txtBanco.GotFocus

        Dim strCampo As String

        Try
            MenuItem15.Visible = False
            strCampo = ""
            Select Case sender.name.ToString().Trim()
                ' Case "txtCodigoCliente" : Label17.Visible = True : strCampo = "C�digo"
                Case "txtCodigoCliente" : Label17.Visible = True : strCampo = "C�digo" : MenuItem11.Visible = True : MenuItem15.Visible = True
                Case "ddlSerie" : strCampo = "Serie"
                Case "txtNumeroRecibo" : strCampo = "N�mero"
                Case "DTPFechaRecibo" : strCampo = "Fecha D�a"
                Case "txtMonto" : strCampo = "Monto"
                Case "ddlFormaPago" : strCampo = "Forma Pago"
                Case "txtNumeroTarjeta" : strCampo = "N�mero"
                Case "txtDescripcion" : strCampo = "Descripci�n"
                Case "ComboBox2" : strCampo = "Facturas"
                Case "TextBox2" : strCampo = "Banco"
            End Select
            'StatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            'StatusBar1.Panels.Item(1).Text = sender.tag
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name.ToString <> "DTPFechaRecibo" And sender.name.ToString <> "DateTimePicker2" And sender.name.ToString <> "DateTimePicker3" Then
                sender.selectall()
            End If
            'If txtNumeroRecibo.Text = "" Then
            '    txtNumeroRecibo.Text = "R-"
            'End If
            If blnUbicar Then
                ObtieneDatosListadoAyuda()
            End If

            'If intTipoReciboCaja = 1 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Then
                nUltimoNumeroRecibo = SUConversiones.ConvierteAInt(ExtraerSerieNumeroRecibo(SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem)))
                txtNumeroRecibo.Text = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(nUltimoNumeroRecibo), "0000000")
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " GotFocus ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoCliente.LostFocus

        Label17.Visible = False

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFormaPago.SelectedIndexChanged

        Try
            If ddlFormaPago.SelectedIndex = 0 Then
                Label12.Visible = False
                txtNumeroTarjeta.Visible = False
                Label7.Visible = False
                txtBanco.Visible = False
                Label25.Visible = False
                txtRefElectronica.Visible = False
            ElseIf ddlFormaPago.SelectedIndex = 1 Then
                Label12.Visible = True
                Label12.Text = "# Cheque"
                txtNumeroTarjeta.Visible = True
                Label7.Visible = True
                txtBanco.Visible = True
                Label25.Visible = False
                txtRefElectronica.Visible = False
            ElseIf ddlFormaPago.SelectedIndex = 2 Then
                Label12.Visible = True
                Label12.Text = "# Tarjeta"
                txtNumeroTarjeta.Visible = True
                Label7.Visible = False
                txtBanco.Visible = False
                Label25.Visible = False
                txtRefElectronica.Visible = False
            ElseIf ddlFormaPago.SelectedIndex = 3 Then
                Label25.Visible = True
                txtRefElectronica.Visible = True
                Label7.Visible = True
                txtBanco.Visible = True
                Label12.Visible = False
                txtNumeroTarjeta.Visible = False
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " SelectedIndexChanged ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Function UbicarCliente(ByVal strDato As String) As Boolean
        Dim strVendedor As String = String.Empty
        Dim dtDatos As DataTable = Nothing
        Dim lnIdCliente As Long = 0
        Me.Cursor = Cursors.WaitCursor
        Try
            TextBox14.Text = "0"
            TextBox15.Text = "0"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'strQuery = ""
            'strQuery = "select * from prm_Clientes where codigo = '" & strDato & "'"
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            'While dtrAgro2K.Read
            '    If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
            '        TextBox14.Text = dtrAgro2K.GetValue(0)
            '    End If
            'End While
            'dtrAgro2K.Close()
            lnIdCliente = 0
            dtDatos = Nothing
            dtDatos = RNCliente.ObtieneClientexCodigoCliente(strDato)
            If SUFunciones.ValidaDataTable(dtDatos) Then
                lnIdCliente = SUConversiones.ConvierteAInt(dtDatos.Rows(0).Item("registro"))
            End If
            TextBox14.Text = lnIdCliente
            'strQuery = ""
            'strQuery = "exec ObtieneDetalleClienteParaRecibo " & SUConversiones.ConvierteAInt(TextBox14.Text.Trim())
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            ' If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                Limpiar()
            End If
            If dblTipoCambio = 0 Then
                dblTipoCambio = 1
            End If
            TextBox14.Text = lnIdCliente
            strVendedor = String.Empty
            dtDatos = Nothing
            'dtDatos = RNCliente.ObtieneDetalleClientexRegistro(SUConversiones.ConvierteAInt(TextBox14.Text.Trim()))
            dtDatos = RNCliente.ObtieneDetalleClientexRegistro(SUConversiones.ConvierteAInt(lnIdCliente))
            If SUFunciones.ValidaDataTable(dtDatos) Then
                txtCodigoCliente.Text = dtDatos.Rows(0).Item(0)
                TextBox2.Text = dtDatos.Rows(0).Item(1)
                txtNumeroRecibido.Text = dtDatos.Rows(0).Item(1)
                TextBox3.Text = dtDatos.Rows(0).Item(2)
                TextBox4.Text = dtDatos.Rows(0).Item(3)
                TextBox5.Text = dtDatos.Rows(0).Item(4)
                TextBox6.Text = dtDatos.Rows(0).Item(5)
                'TextBox7.Text = Format(dtDatos.Rows(0).Item(6), "#,##0.#0")
                TextBox7.Text = Format(dtDatos.Rows(0).Item("Saldo"), "#,##0.####")

                TextBox8.Text = IIf(SUConversiones.ConvierteADecimal(dtDatos.Rows(0).Item(6)) <> 0, Format(SUConversiones.ConvierteADecimal(dtDatos.Rows(0).Item(6)) / dblTipoCambio, "#,##0.#0"), "0.00")
                strVendedor = dtDatos.Rows(0).Item(7)
                TextBox14.Text = dtDatos.Rows(0).Item(8)
                'TextBox19.Text = Format(dtDatos.Rows(0).Item(9), "#,##0.#0")
                TextBox19.Text = Format(dtDatos.Rows(0).Item("SaldoUSD"), "#,##0.####")
                TextBox20.Text = IIf(SUConversiones.ConvierteADecimal(dtDatos.Rows(0).Item(9)) <> 0, Format(SUConversiones.ConvierteADecimal(dtDatos.Rows(0).Item(9)) / dblTipoCambio, "#,##0.#0"), "0.00")

            End If
            'While dtrAgro2K.Read
            '    txtCodigoCliente.Text = dtrAgro2K.GetValue(0)
            '    TextBox2.Text = dtrAgro2K.GetValue(1)
            '    txtNumeroRecibido.Text = dtrAgro2K.GetValue(1)
            '    TextBox3.Text = dtrAgro2K.GetValue(2)
            '    TextBox4.Text = dtrAgro2K.GetValue(3)
            '    TextBox5.Text = dtrAgro2K.GetValue(4)
            '    TextBox6.Text = dtrAgro2K.GetValue(5)
            '    TextBox7.Text = Format(dtrAgro2K.GetValue(6), "#,##0.#0")
            '    TextBox8.Text = IIf(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(6)) <> 0, Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(6)) / dblTipoCambio, "#,##0.#0"), "0.00")
            '    strVendedor = dtrAgro2K.GetValue(7)
            '    TextBox14.Text = dtrAgro2K.GetValue(8)
            '    TextBox19.Text = Format(dtrAgro2K.GetValue(9), "#,##0.#0")
            '    TextBox20.Text = IIf(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(9)) <> 0, Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(9)) / dblTipoCambio, "#,##0.#0"), "0.00")
            'End While
            'dtrAgro2K.Close()
            If TextBox2.Text = "" Then
                UbicarCliente = False
                'cmdAgro2K.Connection.Close()
                'cnnAgro2K.Close()
                Me.Cursor = Cursors.Default
                Exit Function
            End If
            dtDatos = Nothing
            dtDatos = RNVendedor.ObtieneVendedorCodigoVendedor(strVendedor)

            If SUFunciones.ValidaDataTable(dtDatos) Then
                TextBox15.Text = dtDatos.Rows(0).Item(0)
            End If
            UbicarCliente = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            ' MessageBoxEx.Show("Error " & ex.Message, " UbicarCliente ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Function

    Sub LlenarGrid(ByVal strDato As String)

        Try
            strQuery = String.Empty
            strNumeroFact = String.Empty
            If SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                strNumeroFact = ddlSerie.SelectedItem & Format(SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
            Else
                strNumeroFact = txtNumeroRecibo.Text.Trim()
            End If


            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
            dtrAgro2K = Nothing
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                dtrAgro2K = RNRecibos.ObtieneInformacionFacturasXClientes(SUConversiones.ConvierteAInt(TextBox14.Text))
            Else
                dtrAgro2K = RNRecibos.ConsultaDetalleReciboxNumeroRecibo(strNumeroFact)
            End If
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    'dgvxPagosFacturas.Rows.Add(dtrAgro2K.GetValue(0), dtrAgro2K.GetValue(1), dtrAgro2K.GetValue(2), Format(dtrAgro2K.GetValue(3), "#,##0.#0"), 0, 0, 0, Format(dtrAgro2K.GetValue(3), "#,##0.#0"), dtrAgro2K.Item("FechaEmisionDate"), dtrAgro2K.Item("FechaVencimientoDate"))
                    dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("numero"), dtrAgro2K.Item("Moneda"), dtrAgro2K.Item("fechaing"), dtrAgro2K.Item("fechaven"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("saldo")), "#,##0.####"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("saldoUSD")), "#,##0.####"), 0, Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("saldo")), "#,##0.####"), Format(SUConversiones.ConvierteADecimal(dtrAgro2K.Item("saldoUSD")), "#,##0.####"), 0, 0, 0, SUConversiones.ConvierteALong(dtrAgro2K.Item("registro")), SUConversiones.ConvierteALong(dtrAgro2K.Item("IdMoneda")))
                End While
            End If
            SeteaColorRojoEnGrid()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " LlenarGrid ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Sub Guardar()
        Dim objRecibos As SERecibosEncabezado
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleRecibo As List(Of SERecibosDetalle) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Dim objReqCampo As Object = Nothing
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing
        Dim frmLogin2 As frmLoginSuprvisor = Nothing
        Dim ldFechaDelDia As Date = Nothing
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            ldFechaDelDia = New Date(Now.Year, Now.Month, Now.Day)
            objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                If Format(DTPFechaRecibo.Value, "yyyyMMdd") <> Format(ldFechaDelDia, "yyyyMMdd") Then
                    If RNFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(lngRegUsuario) = 0 Then
                        If objParametroEmpresa IsNot Nothing Then
                            intResp = MsgBox("Usuario no tiene privilegio para realizar cambio de fecha. �Desea continuar?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Critical, "Ingreso Factura")
                            Select Case intResp
                        ' Case Windows.Forms.DialogResult.Yes
                                Case 6
                                    Select Case objParametroEmpresa.DesactivaObjetoFechaEnOperaciones.ToUpper
                                        Case "N"
                                            MsgBox("Usuario no tiene permisos para poder realizar cambio de fecha. ", MsgBoxStyle.Critical, "Facturas ")
                                            DTPFechaRecibo.Value = ldFechaDelDia
                                            DTPFechaRecibo.Focus()
                                            Return
                                        Case "S"
                                            frmLogin2 = New frmLoginSuprvisor()
                                            frmLogin2.ShowDialog()
                                            If lngRegUsuarioRelogin = -1 Then
                                                MsgBox("Usuario no tiene privilegio para cambiar la fecha para esta operaci�n", MsgBoxStyle.Critical)
                                                DTPFechaRecibo.Value = ldFechaDelDia
                                                DTPFechaRecibo.Focus()
                                                Return
                                                Exit Sub
                                            End If
                                            If RNFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(lngRegUsuarioRelogin) = 0 Then
                                                MsgBox("Usuario no tiene privilegio para cambiar la fecha para esta operaci�n.", MsgBoxStyle.Critical)
                                                DTPFechaRecibo.Value = ldFechaDelDia
                                                DTPFechaRecibo.Focus()
                                                Return
                                                Exit Sub
                                            End If

                                        Case Else

                                    End Select
                                Case 7
                                    DTPFechaRecibo.Value = ldFechaDelDia
                                    DTPFechaRecibo.Focus()
                                    Return
                                    Exit Sub
                                Case Else
                            End Select
                        End If
                    End If
                End If
            End If


            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                If txtNumeroRecibo.Text <> "" Then
                    UbicarRecibo()
                End If
            End If
            If TextBox14.Text = "0" Then
                TextBox14.Focus()
                MsgBox("Tiene que digitar el codigo del cliente.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If txtNumeroRecibo.Text = "" Then
                txtNumeroRecibo.Focus()
                MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If txtMonto.Text = "" Then
                txtMonto.Focus()
                MsgBox("Tiene que digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If txtMonto.Text = "0.00" Then
                txtMonto.Focus()
                MsgBox("Tiene que digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If IsNumeric(txtMonto.Text) = False Then
                txtMonto.Focus()
                MsgBox("El monto del recibo debe ser num�rico.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If SUConversiones.ConvierteADecimal(txtMonto.Text) <= 0 Then
                txtMonto.Focus()
                MsgBox("El monto del recibo debe ser mayor que 0.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            CalculaDisponibleRecibo()

            If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                MsgBox("La sumatoria de los pagos no pueden ser mayor que el monto del Recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If SUConversiones.ConvierteADecimal(Label15.Text) > 0 Then
                MsgBox("No puede haber Saldo Disponible para un cliente.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            'If intTipoReciboCaja = 2 And SUConversiones.ConvierteADecimal(Label15.Text) > 0 Then
            If intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR And SUConversiones.ConvierteADecimal(Label15.Text) > 0 Then
                MsgBox("No puede haber Saldo Disponible para un cliente cuando est� elaborando una Nota de Credito.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If IsNumeric(txtDescripcionRecibo.Text) = False Then
                txtDescripcionRecibo.Focus()
                txtDescripcionRecibo.Text = "0.00"
            End If

            intContar = 0
            If SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text) > 0 Then
                intContar = intContar + 1
            End If
            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                If dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value <> 0 Then
                    intContar = intContar + 1
                Else
                    If dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value <> 0 Then
                        intContar = intContar + 1
                    Else
                        If dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value <> 0 Then
                            intContar = intContar + 1
                        End If
                    End If
                End If
            Next intIncr
            If intContar > 8 Then
                MsgBox("No puede gestionar en un solo recibo m�s de 8 pagos. Favor de elaborar 2 o m�s recibos.", MsgBoxStyle.Exclamation, "Exceso de Datos")
                Exit Sub
            End If
            txtBanco.Text = UCase(txtBanco.Text)
            objRecibos = New SERecibosEncabezado()
            objRecibos.registro = 0
            objRecibos.fecha = Format(DTPFechaRecibo.Value, "dd-MMM-yyyy")
            objRecibos.numfecha = Format(DTPFechaRecibo.Value, "yyyyMMdd")
            objRecibos.cliregistro = SUConversiones.ConvierteAInt(TextBox14.Text)
            objRecibos.vendregistro = SUConversiones.ConvierteAInt(TextBox15.Text)
            If SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                strNumRec = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
            Else
                strNumRec = UCase(txtNumeroRecibo.Text)
            End If
            objRecibos.IdMoneda = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
            objRecibos.TipoCambio = SUConversiones.ConvierteADecimal(txtTipoCambio.Text)
            objRecibos.numero = strNumRec
            objRecibos.monto = SUConversiones.ConvierteADecimal(txtMonto.Text)
            objRecibos.interes = SUConversiones.ConvierteADecimal(TextBox16.Text)
            objRecibos.retencion = SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text)
            objRecibos.formapago = ddlFormaPago.SelectedIndex
            objRecibos.numchqtarj = UCase(txtNumeroTarjeta.Text)
            objRecibos.nombrebanco = txtBanco.Text
            objRecibos.NoDeposito = txtRefElectronica.Text
            objRecibos.reciboprov = UCase(txtDescripcion.Text)
            objRecibos.descripcion = UCase(txtNumeroRecibido.Text)
            objRecibos.usrregistro = lngRegUsuario
            objRecibos.tiporecibo = intTipoReciboCaja
            objRecibos.fecha_ing = DTPFechaRecibo.Value
            If SUConversiones.ConvierteADecimal(Label15.Text) <> 0 Then
                objRecibos.saldofavor = SUConversiones.ConvierteADecimal(Label15.Text)
            Else
                objRecibos.saldofavor = 0
            End If
            objRecibos.descr01 = ""
            objRecibos.descr02 = ""
            objRecibos.descr03 = ""
            objRecibos.agenRegistro = lngRegAgencia

            lstDetalleRecibo = CargarDetalleRecibos()

            If (objRecibos Is Nothing) And (lstDetalleRecibo Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el Recibo", " Guardar Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lstDetalleRecibo Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el Recibo", " Guardar Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lstDetalleRecibo.Count <= 0) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el Recibo", " Guardar Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            RNRecibos.IngresaTransaccionRecibos(objRecibos, lstDetalleRecibo, SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), dblPendienteRecibo)

            If SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text) <> 0 Then
                arrRecibo(intContar, 0) = "Retenci�n del Recibo"
                arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "D"
                intContar = intContar + 1
                'GuardarRetencion()
            End If
            If dblPendienteRecibo <> 0 Then
                arrRecibo(intContar, 0) = "Saldo Pendiente"
                arrRecibo(intContar, 1) = Format(dblPendienteRecibo, "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "C"
                'GuardarSaldoPendiente()
            End If
            MessageBoxEx.Show("Registro Ingresado/Actualizado", "Aplicaci�n Satisfactoria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            intResp = MsgBox("�Desea imprimir el recibo en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Recibo")
            If intResp = 6 Then     'Cancelar
                ImprimirRecibo()
            End If
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar El Recibos", " Guardar Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        Me.Cursor = Cursors.Default

    End Sub

    Private Function CargarDetalleRecibos() As List(Of SERecibosDetalle)
        Dim lstDetalleRec As List(Of SERecibosDetalle) = Nothing
        Dim objDetalleRec As SERecibosDetalle = Nothing
        Dim i As Integer = 0
        Dim dblAbono As Double = 0
        Dim dblInteres As Double = 0
        Dim dblMantVlr As Double = 0
        Dim dblPendiente As Double = 0
        Dim lnTotalAbono As Double = 0
        Dim lnMontoAAbonar As Double = 0
        Dim lnInteresAAbonar As Double = 0
        Dim lnMantenimientoAAbonar As Double = 0
        Dim lnPagoPorAplicarAAbonar As Double = 0

        Try
            objDetalleRec = New SERecibosDetalle()
            lstDetalleRec = New List(Of SERecibosDetalle)

            If dgvxPagosFacturas.Rows.Count > 0 Then
                If dgvxPagosFacturas.IsCurrentCellDirty Then
                    dgvxPagosFacturas.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                For i = 0 To dgvxPagosFacturas.Rows.Count - 1

                    dblAbono = dgvxPagosFacturas.Rows(i).Cells("Monto").Value
                    dblInteres = dgvxPagosFacturas.Rows(i).Cells("Interes").Value
                    dblMantVlr = dgvxPagosFacturas.Rows(i).Cells("MantValor").Value
                    dblPendiente = dgvxPagosFacturas.Rows(i).Cells("PorAplicar").Value

                    lnTotalAbono = lnTotalAbono + dblAbono
                    If dblAbono <> 0 Or dblInteres <> 0 Or dblMantVlr <> 0 Or dblPendiente <> 0 Then
                        objDetalleRec.registro = 0
                        objDetalleRec.numero = dgvxPagosFacturas.Rows(i).Cells("NumFactura").Value
                        If SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(i).Cells("NvoSaldo").Value) = 0 Then
                            strRecibo = "Cancelaci�n de la Factura " & dgvxPagosFacturas.Rows(i).Cells("NumFactura").Value
                        Else
                            strRecibo = "Abono a la Factura " & dgvxPagosFacturas.Rows(i).Cells("NumFactura").Value
                        End If
                        objDetalleRec.descripcion = strRecibo
                        objDetalleRec.monto = dgvxPagosFacturas.Rows(i).Cells("Monto").Value
                        objDetalleRec.interes = dgvxPagosFacturas.Rows(i).Cells("Interes").Value
                        objDetalleRec.mantvlr = dgvxPagosFacturas.Rows(i).Cells("MantValor").Value
                        objDetalleRec.pendiente = dgvxPagosFacturas.Rows(i).Cells("PorAplicar").Value

                        dblPendienteRecibo = dblPendienteRecibo + objDetalleRec.pendiente
                        arrRecibo(i, 0) = strRecibo
                        arrRecibo(i, 1) = Format(SUConversiones.ConvierteADecimal(objDetalleRec.monto), "#,##0.#0")
                        arrRecibo(i, 2) = Format(SUConversiones.ConvierteADecimal(objDetalleRec.interes), "#,##0.#0")
                        arrRecibo(i, 3) = Format(SUConversiones.ConvierteADecimal(objDetalleRec.mantvlr), "#,##0.#0")
                        arrRecibo(i, 4) = "C"
                        'intContar = intContar + 1

                        lstDetalleRec.Add(New SERecibosDetalle(objDetalleRec))
                    End If
                Next
                If lnTotalAbono = 0 Then
                    If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                        MessageBoxEx.Show("Debe abonar al saldo principal, favor valide ", " Recibos ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return lstDetalleRec
                        Exit Function
                    End If
                End If
            End If
            Return lstDetalleRec
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Function

    'Sub GuardarRetencion()


    'End Sub

    Sub Anular()

        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            Me.Cursor = Cursors.WaitCursor
            Dim cmdTmp As New SqlCommand("sp_IngRecibosAnular", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim lngNumFecha As Long

            lngNumFecha = 0
            lngNumFecha = Format(DTPFechaRecibo.Value, "yyyyMMdd")
            With prmTmp01
                .ParameterName = "@lngCliente"
                .SqlDbType = SqlDbType.SmallInt
                .Value = SUConversiones.ConvierteAInt(TextBox14.Text)
            End With
            With prmTmp02
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                If SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                    .Value = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                Else
                    .Value = UCase(txtNumeroRecibo.Text)
                End If

            End With
            With prmTmp03
                .ParameterName = "@lngNumFecha"
                .SqlDbType = SqlDbType.Int
                .Value = lngNumFecha
            End With
            With prmTmp04
                .ParameterName = "@dblMontoRec"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADecimal(txtMonto.Text)
            End With
            With prmTmp05
                .ParameterName = "@dblSaldoFavor"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADecimal(Label15.Text)
            End With
            With prmTmp06
                .ParameterName = "@intTipoRecibo"
                .SqlDbType = SqlDbType.TinyInt
                .Value = intTipoReciboCaja
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                cmdTmp.ExecuteNonQuery()
                MsgBox("Recibo Anulado", MsgBoxStyle.Information, "Anulaci�n Satisfactoria")
                Limpiar()
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Finally
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
            End Try

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " Anular ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub ImprimirRecibo()
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            intRptPresup = 0
            intRptTipoPrecio = 0
            For intIncr = 0 To 10
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            strQuery = ""
            strQuery = "select nombre, codigo, cta_contable from prm_clientes where registro = " & SUConversiones.ConvierteALong(TextBox14.Text) & ""
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            'arrRecibosExtra(0) = Format(DTPFechaRecibo.Value, "dd-MMM-yyyy")
            arrRecibosExtra(0) = Format(DTPFechaRecibo.Value, "dd-MMM")
            'objEncabezadoRecibo.FechaRecibo = Format(DTPFechaRecibo.Value, "dd-MMM")
            While dtrAgro2K.Read
                'objEncabezadoRecibo.Nombre = dtrAgro2K.GetValue(0)
                'objEncabezadoRecibo.Codigo = dtrAgro2K.GetValue(1)
                'objEncabezadoRecibo.CuentaContable = dtrAgro2K.GetValue(2)
                arrRecibosExtra(1) = dtrAgro2K.GetValue(0)
                arrRecibosExtra(2) = dtrAgro2K.GetValue(1)
                arrRecibosExtra(4) = dtrAgro2K.GetValue(2)
            End While
            dtrAgro2K.Close()

            arrRecibosExtra(3) = Format(SUConversiones.ConvierteADecimal(txtMonto.Text), "#,##0.####")
            arrRecibosExtra(5) = txtNumeroTarjeta.Text
            arrRecibosExtra(6) = txtBanco.Text
            arrRecibosExtra(7) = strNumRec 'UCase(txtNumeroRecibo.Text)
            arrRecibosExtra(8) = UCase(txtDescripcion.Text)
            arrRecibosExtra(9) = UCase(txtNumeroRecibido.Text)
            arrRecibosExtra(10) = ""
            If Label23.Text = "ANULADO" Then
                arrRecibosExtra(10) = "ANULADO"
                'objEncabezadoRecibo.Anulado = "ANULADO"
            End If
            arrRecibosExtra(11) = Format(DTPFechaRecibo.Value, "yy")
            If SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                strNumeroFact = strNumRec  'ddlSerie.SelectedItem & Format(SUConversiones.ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
            Else
                strNumeroFact = strNumRec  'txtNumeroRecibo.Text.Trim()
            End If
            ' frmNew.NumeroRecibo = UCase(txtNumeroRecibo.Text)
            Dim frmNew As New actrptViewer
            frmNew.NumeroRecibo = UCase(strNumeroFact)
            intRptImpRecibos = intTipoReciboCaja
            ' frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
            'If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
            '    frmNew.WindowState = FormWindowState.Minimized
            '    frmNew.Close()
            'End If
            'For intIncr = 0 To 11
            For intIncr = 0 To 10
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            For intIncr = 0 To 7
                arrRecibo(intIncr, 0) = ""
                arrRecibo(intIncr, 1) = ""
                arrRecibo(intIncr, 2) = ""
                arrRecibo(intIncr, 3) = ""
                arrRecibo(intIncr, 4) = ""
            Next intIncr
            Limpiar()
            strNumRec = ""
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " ImprimirRecibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ObtieneDatosListadoAyuda()
        Try
            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 2
                            txtCodigoCliente.Text = strUbicar
                            If UbicarCliente(txtCodigoCliente.Text) = True Then
                                txtNumeroRecibo.Focus()
                                'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                                If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ELABORAR Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                                    LlenarGrid(txtCodigoCliente.Text)
                                End If
                            End If
                    End Select
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " ObtieneDatosListadoAyuda ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoCliente.DoubleClick

        intListadoAyuda = 2
        Dim frmNew As New frmListadoAyuda
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Sub VistaPrevia()
        Try
            intContar = 0
            dblPendienteRecibo = 0

            If SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text) <> 0 Then
                arrRecibo(intContar, 0) = "Retenci�n del Recibo"
                arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "D"
                intContar = intContar + 1
            End If
            If dblPendienteRecibo <> 0 Then
                arrRecibo(intContar, 0) = "Saldo Pendiente"
                arrRecibo(intContar, 1) = Format(dblPendienteRecibo, "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "C"
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            strQuery = ""
            strQuery = "select nombre, codigo, cta_contable from prm_clientes where registro = " & SUConversiones.ConvierteALong(TextBox14.Text) & ""
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            arrRecibosExtra(0) = Format(DTPFechaRecibo.Value, "dd-MMM-yyyy")
            While dtrAgro2K.Read
                arrRecibosExtra(1) = dtrAgro2K.GetValue(0)
                arrRecibosExtra(2) = dtrAgro2K.GetValue(1)
                arrRecibosExtra(4) = dtrAgro2K.GetValue(2)
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            arrRecibosExtra(3) = Format(SUConversiones.ConvierteADecimal(txtMonto.Text), "#,##0.##")
            arrRecibosExtra(5) = txtNumeroTarjeta.Text
            arrRecibosExtra(6) = txtBanco.Text
            arrRecibosExtra(7) = UCase(txtNumeroRecibo.Text)
            arrRecibosExtra(8) = UCase(txtDescripcion.Text)
            arrRecibosExtra(9) = UCase(txtNumeroRecibido.Text)
            Dim frmNew As New actrptViewer
            intRptImpRecibos = 9
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
            For intIncr = 0 To 7
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            For intIncr = 0 To 7
                arrRecibo(intIncr, 0) = ""
                arrRecibo(intIncr, 1) = ""
                arrRecibo(intIncr, 2) = ""
                arrRecibo(intIncr, 3) = ""
                arrRecibo(intIncr, 4) = ""
            Next intIncr
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " VistaPrevia ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged
        Try
            'If intTipoReciboCaja = 1 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Then
                nUltimoNumeroRecibo = SUConversiones.ConvierteAInt(ExtraerSerieNumeroRecibo(SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem)))
                txtNumeroRecibo.Text = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(nUltimoNumeroRecibo), "0000000")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " SelectedIndexChanged ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim NvoSaldo, SaldoAnt As Double
        Try
            SaldoAnt = TextBox7.Text
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            'If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ANULAR Then
                If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            Else
                NvoSaldo = RNRecibos.ObtenerSaldoPendiente(SUConversiones.ConvierteAInt(TextBox14.Text.Trim()))
                If NvoSaldo = SaldoAnt Then
                    Guardar()
                ElseIf NvoSaldo < SaldoAnt Then
                    MessageBoxEx.Show("El saldo del Cliente ha Disminuido: Verifique", "Guardar Recibos", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Limpiar()
                ElseIf NvoSaldo > SaldoAnt Then
                    MessageBoxEx.Show("El saldo del Cliente ha Aumentado: Verifique", "Guardar Recibos", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Limpiar()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " OK Click ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Limpiar Click ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If cmdBuscar.Text <> "<F7>" Then
                Extraer()
            ElseIf cmdBuscar.Text = "<F7>" Then
                'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Or intTipoReciboCaja = ENTipoRecibo.RECIBO_MANUAL Then
                    VistaPrevia()
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub CambiaIcoAnular(ByVal Boton As DevComponents.DotNetBar.ButtonItem)
        Try
            Boton.Tooltip = "Anular recibo"
            Boton.Text = "Anular<F7>"
            Boton.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
            Boton.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
            Boton.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
            Boton.ImageFixedSize = New System.Drawing.Size(40, 40)
            Boton.Image = ObtieneImagen("Remove_48x48.png")

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Function ObtieneImagen(ByVal NombreImagen As String) As System.Drawing.Image
        Dim imagen As System.Drawing.Image = Nothing
        Try
            'Dim file_name As String = Application.
            Dim NombreCarpeta As String
            Dim NombreArchivo As String = String.Empty
            NombreCarpeta = System.IO.Directory.GetCurrentDirectory()
            If NombreCarpeta.EndsWith("\bin\Debug") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin\Debug", "")
            End If
            If NombreCarpeta.EndsWith("\bin") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin", "")
            End If
            NombreCarpeta = NombreCarpeta & "\Imagenes\"
            NombreArchivo = NombreCarpeta & NombreImagen

            'imagen = Image.FromFile(NombreArchivo)
            If System.IO.File.Exists(NombreArchivo) Then
                imagen = System.Drawing.Image.FromFile(NombreArchivo)
            End If


        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Return Nothing
        End Try
        Return imagen
    End Function

    Private Sub cmdImprimir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            'If intTipoReciboCaja = 9 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_IMPRIMIR Then
                ImprimirRecibo()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub CmdAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            'If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ANULAR Then
                If txtNumeroRecibo.Text.Length <= 0 Then
                    MsgBox("Favor buscar el recibo a Anular.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub CmdAnular_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAnular.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            ' If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_ANULAR Or intTipoReciboCaja = ENTipoNotas.NOTA_CREDITO_ANULAR Then
                If txtNumeroRecibo.Text.Length <= 0 Then
                    MsgBox("Favor buscar el recibo a Anular.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Function ValidaFacturaFechaMasAntigua() As Integer
        Dim lnResultado As Integer = 0
        Dim lnRegistro As Long = 0
        Dim lnMonto As Double = 0
        Dim lnSaldo As Double = 0
        Dim lnSaldoActual As Double = 0
        Dim lnMontoActual As Double = 0
        Dim lnRegistroActual As Long = 0
        Dim ldFecha As DateTime = Nothing
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnIdUsuarioII As Integer = 0
        Try
            If dgvxPagosFacturas.Rows.Count >= 1 Then
                objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                If dgvxPagosFacturas.IsCurrentCellDirty Then
                    dgvxPagosFacturas.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                'If dgvxPagosFacturas.CurrentCell.ColumnIndex = 6 Then   'Celda en la que se ingresa el monto del recibo

                'End If
                lnResultado = 0
                If objParametro IsNot Nothing Then
                    If Not objParametro.PermitePagarFacturasRecientesConAntiguaPendiente.Equals("Y") Then
                        lnRegistroActual = 0
                        lnMontoActual = 0
                        lnSaldoActual = 0
                        If dgvxPagosFacturas.CurrentRow IsNot Nothing Then
                            lnRegistroActual = SUConversiones.ConvierteALong(dgvxPagosFacturas.CurrentRow.Cells("registro").Value)
                            lnMontoActual = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.CurrentRow.Cells("monto").Value)
                            lnSaldoActual = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
                        End If

                        For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                            lnMonto = 0
                            lnRegistro = 0
                            lnSaldo = 0
                            lnRegistro = SUConversiones.ConvierteALong(dgvxPagosFacturas.Rows(intIncr).Cells("registro").Value)
                            lnMonto = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                            lnSaldo = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("NvoSaldo").Value)
                            lnResultado = 0
                            If lnRegistroActual > lnRegistro AndAlso lnMontoActual > 0 AndAlso lnSaldo > 0 Then
                                lnResultado = 1
                                Exit For
                            End If
                        Next intIncr
                    End If
                End If
            End If
            If lnResultado = 1 Then
                If objParametro IsNot Nothing Then
                    If objParametro.PermitePagarFacturasRecientesConAntiguaPendiente.ToUpper().Equals("N") Then
                        lnResultado = 1
                        dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = lnSaldoActual
                        dgvxPagosFacturas.CurrentRow.Cells("monto").Value = 0
                        dgvxPagosFacturas.CurrentRow.Cells("monto").Selected = True
                    Else

                        If objParametro.PermitePagarFacturasRecientesConAntiguaPendiente.ToUpper().Equals("S") Then
                            If RNRecibos.ValidaPermisoPagaFacturaRecientesConFacturasAntiguaConSaldo(lngRegUsuario) = 1 Then
                                lnResultado = 0
                            Else
                                frmLogin2 = New frmLoginSuprvisor()
                                frmLogin2.ShowDialog()
                                If lngRegUsuarioRelogin = -1 Then
                                    MsgBox("No tiene privilegio para realizar esta accion", MsgBoxStyle.Critical)
                                    lnResultado = 1
                                    dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = lnSaldoActual
                                    dgvxPagosFacturas.CurrentRow.Cells("monto").Value = 0
                                    dgvxPagosFacturas.CurrentRow.Cells("monto").Selected = True
                                Else
                                    lnResultado = 0
                                    If RNRecibos.ValidaPermisoPagaFacturaRecientesConFacturasAntiguaConSaldo(lngRegUsuarioRelogin) = 0 Then
                                        MsgBox("No tiene privilegio para facturar para clientes sin disponible", MsgBoxStyle.Critical)
                                        lnResultado = 1
                                        dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = lnSaldoActual
                                        dgvxPagosFacturas.CurrentRow.Cells("monto").Value = 0
                                        dgvxPagosFacturas.CurrentRow.Cells("monto").Selected = True
                                    End If

                                End If

                            End If
                        End If
                    End If
                End If

            End If
            CalculaDisponibleRecibo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Return lnResultado
    End Function
    Private Sub CalcularMontosGrid()

        Dim strMonto As String = String.Empty
        Dim PorcDesc As Double = 0
        Dim MontoDesc As Double = 0
        Dim dblMonto As Double = 0
        Dim dblSaldo As Double = 0
        Dim strMtoAnt As String = String.Empty
        Dim dblMtoAnt As Double = 0
        Dim intResp As Integer = 0
        Dim lnRegistroActual As Integer = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnMoneda As Integer = 0
        Dim lnMontoReciboConvertido As Double = 0
        Dim lnNuevoSaldo As Double = 0
        Dim lnNuevoSaldoUSD As Double = 0
        Dim lnTipoCambio As Decimal = 0

        Try
            If dgvxPagosFacturas.Rows.Count >= 1 Then
                lnTipoCambio = SUConversiones.ConvierteADecimal(txtTipoCambio.Text)
                lnTipoCambio = lnTipoCambio
                If dgvxPagosFacturas.IsCurrentCellDirty Then
                    dgvxPagosFacturas.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                dblMontoRecibo = SUConversiones.ConvierteADecimal(txtMonto.Text)
                MontoDesc = SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text)
                'If dgvxPagosFacturas.CurrentCell.ColumnIndex = 6 Then   'Celda en la que se ingresa el monto del recibo
                If dgvxPagosFacturas.CurrentCell IsNot Nothing Then
                    'If dgvxPagosFacturas.CurrentCell.ColumnIndex = 4 Then   'Celda en la que se ingresa el monto del recibo
                    If dgvxPagosFacturas.CurrentCell.ColumnIndex = 6 Then   'Celda en la que se ingresa el monto del recibo
                        objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                        lnRegistroActual = 0
                        If dgvxPagosFacturas.CurrentRow IsNot Nothing Then
                            lnRegistroActual = dgvxPagosFacturas.CurrentRow.Index
                        End If
                        lnRegistroActual = lnRegistroActual + 1
                        If objParametro IsNot Nothing Then
                            If objParametro.CantidadItemFactura > 0 Then
                                'If dgvDetalle.Rows.Count >= 16 Then
                                If lnRegistroActual > objParametro.CantidadItemRecibos Then
                                    ' MsgBox("Cantidad maxima para una factura es 16 Item", MsgBoxStyle.Critical, "Favor validar")
                                    MsgBox("Cantidad maxima para un recibo es " & objParametro.CantidadItemRecibos.ToString() & " Items ", MsgBoxStyle.Critical, "Favor validar")
                                    dgvxPagosFacturas.CurrentRow.Cells("Monto").Value = 0
                                    Exit Sub
                                End If
                            End If
                        End If
                        'dgvxPagosFacturas.CurrentRow.Cells("Monto"). = "###,##0.####"
                        dblMonto = 0
                        strMonto = String.Empty
                        dblMtoAnt = 0
                        strMtoAnt = String.Empty
                        ''strMtoAnt = dgvxPagosFacturas.CurrentCell.Value
                        strMtoAnt = SaldoAnterior
                        strMonto = dgvxPagosFacturas.CurrentCell.Value
                        dblMtoAnt = SUConversiones.ConvierteADecimal(strMtoAnt)
                        lnMoneda = SUConversiones.ConvierteAInt(dgvxPagosFacturas.CurrentRow.Cells("IdMoneda").Value)


                        If IsNumeric(Trim(strMonto)) = False Then
                            Exit Sub
                        End If
                        dblMonto = SUConversiones.ConvierteADecimal(strMonto)
                        If dblMonto < 0 Then
                            MsgBox("El monto a abonar debe ser mayor o igual a 0.00", MsgBoxStyle.Critical, "Error de Dato")
                            Exit Sub
                        End If

                        'MontoDesc = dgvxPagosFacturas.CurrentRow.Cells("MontoDescuentoFact").Value
                        MontoDesc = 0

                        If lnMoneda = 1 Then
                            dblSaldo = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
                        Else
                            dblSaldo = SUConversiones.ConvierteADecimal(dgvxPagosFacturas.CurrentRow.Cells("SaldoFacturaUSD").Value)
                        End If

                        'If SUConversiones.ConvierteAInt(dgvxPagosFacturas.CurrentRow.Cells("IdMoneda").Value) Then
                        '    dblMonto
                        'End If
                        If dblSaldo - (dblMonto + MontoDesc) < 0 Then
                            MsgBox("No es permitido pagar de m�s una factura.", MsgBoxStyle.Critical, "Error de Dato")
                            dblMontoRecibo = 0
                            dgvxPagosFacturas.CurrentCell.Value = 0
                            Exit Sub
                        End If
                        lnNuevoSaldoUSD = 0
                        lnNuevoSaldo = 0
                        lnNuevoSaldo = dblSaldo - (dblMonto + MontoDesc)
                        lnNuevoSaldo = SUFunciones.RedondeoNumero(lnNuevoSaldo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                        'dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(dblSaldo - (dblMonto + MontoDesc), "###,##0.#0")

                        If lnMoneda = 1 Then
                            lnNuevoSaldo = lnNuevoSaldo
                            'lnNuevoSaldoUSD = Math.Round((lnNuevoSaldo / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2)
                            lnNuevoSaldoUSD = SUFunciones.RedondeoNumero((lnNuevoSaldo / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                        Else
                            lnNuevoSaldoUSD = lnNuevoSaldo
                            'lnNuevoSaldo = Math.Round((lnNuevoSaldo * SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2)
                            lnNuevoSaldo = SUFunciones.RedondeoNumero((lnNuevoSaldo * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                        End If
                        dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(lnNuevoSaldo, "###,##0.####")
                        dgvxPagosFacturas.CurrentRow.Cells("NvoSaldoUSD").Value = Format(lnNuevoSaldoUSD, "###,##0.####")
                        lnMontoReciboConvertido = 0
                        If cbMoneda.SelectedValue = 1 Then
                            If lnMoneda = 1 Then
                                lnMontoReciboConvertido = dblMonto
                            Else
                                'lnMontoReciboConvertido = Math.Round((dblMonto / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2)
                                'lnMontoReciboConvertido = Math.Round((dblMonto * SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2)
                                lnMontoReciboConvertido = SUFunciones.RedondeoNumero((dblMonto * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)

                            End If
                        Else
                            If lnMoneda = 1 Then
                                'lnMontoReciboConvertido = Math.Round((dblMonto / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2)
                                lnMontoReciboConvertido = SUFunciones.RedondeoNumero((dblMonto / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                            Else
                                lnMontoReciboConvertido = SUFunciones.RedondeoNumero(dblMonto, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                            End If
                        End If
                        'dblMontoRecibo = SUConversiones.ConvierteADecimal(Label15.Text)
                        'dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                        dblMontoRecibo = dblMontoRecibo + dblMtoAnt - lnMontoReciboConvertido
                        Label15.Text = Format(dblMontoRecibo, "###,##0.####")
                        Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.####")
                        Label15.Text = SUFunciones.RedondeoNumero(SUConversiones.ConvierteADecimal(Label15.Text), objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                        lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(Label15.Text), "#,##0.####")
                        If lnTipoCambio <> 0 Then
                            ' lblDisponibleUSD.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), 4), "#,##0.####")
                            If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                                lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            Else
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            End If

                        End If
                        If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                            Label15.ForeColor = System.Drawing.Color.Red
                        Else
                            Label15.ForeColor = System.Drawing.Color.Black
                        End If
                        If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                        Else
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                        End If
                    ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 9 Then   'Columna para ingresar el monto del interes
                        dblMonto = 0
                        strMtoAnt = ""
                        dblMtoAnt = 0
                        strMtoAnt = ""
                        ''strMtoAnt = MSFlexGrid2.Text
                        strMtoAnt = SaldoAnterior
                        strMonto = dgvxPagosFacturas.CurrentCell.Value
                        dblMtoAnt = SUConversiones.ConvierteADecimal(strMtoAnt)
                        ''strMonto = InputBox("Digite el Monto de Intereses para la Factura", "Monto de Intereses")
                        intResp = MsgBox("�Usted esta abonando a intereses, Desea Continuar?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Recibo")
                        If IsNumeric(Trim(strMonto)) = False Then
                            dgvxPagosFacturas.CurrentCell.Value = 0
                            Exit Sub
                        End If
                        dblMonto = SUConversiones.ConvierteADecimal(strMonto)
                        ''MSFlexGrid2.Text = Format(dblMonto, "###,##0.#0")
                        dgvxPagosFacturas.CurrentCell.Value = Format(dblMonto, "###,##0.####")
                        If intResp <> 6 Then
                            ''MSFlexGrid2.Text = 0
                            dgvxPagosFacturas.CurrentCell.Value = 0
                            dblMonto = 0
                        End If
                        dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                        dblInteresRecibo = dblInteresRecibo - dblMtoAnt + dblMonto
                        Label15.Text = Format(dblMontoRecibo, "###,##0.####")
                        Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.####")
                        lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(Label15.Text), "###,##0.####")
                        'If SUConversiones.ConvierteADecimal(txtTipoCambio.Text) <> 0 Then
                        If lnTipoCambio <> 0 Then
                            ' lblDisponibleUSD.Text = Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), 4)
                            If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                                lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                'Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            Else
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            End If

                        End If
                        If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                            Label15.ForeColor = System.Drawing.Color.Red
                        Else
                            Label15.ForeColor = System.Drawing.Color.Black
                        End If
                        If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                        Else
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                        End If
                        TextBox16.Text = Format(dblInteresRecibo, "###,##0.#0")
                    ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 10 Then   'Columna para ingresar el monto de Mant. al Valor
                        dblMonto = 0
                        strMtoAnt = ""
                        dblMtoAnt = 0
                        strMtoAnt = ""
                        ''strMtoAnt = MSFlexGrid2.Text
                        strMtoAnt = SaldoAnterior
                        strMonto = dgvxPagosFacturas.CurrentCell.Value
                        dblMtoAnt = SUConversiones.ConvierteADecimal(strMtoAnt)
                        ''strMonto = InputBox("Digite el Monto de Mantenimiento de Valor para la Factura", "Monto de Intereses")
                        If IsNumeric(Trim(strMonto)) = False Then
                            Exit Sub
                        End If
                        dblMonto = SUConversiones.ConvierteADecimal(strMonto)
                        ''MSFlexGrid2.Text = Format(dblMonto, "###,##0.#0")
                        dgvxPagosFacturas.CurrentCell.Value = Format(dblMonto, "###,##0.####")
                        dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                        dblMntVlrRecibo = dblMntVlrRecibo - dblMtoAnt + dblMonto
                        Label15.Text = Format(dblMontoRecibo, "###,##0.####")
                        Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.####")
                        lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(Label15.Text), "###,##0.####")

                        'If SUConversiones.ConvierteADecimal(txtTipoCambio.Text) <> 0 Then
                        If lnTipoCambio <> 0 Then
                            'lblDisponibleUSD.Text = Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2)
                            'lblDisponibleUSD.Text = Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), 4)
                            If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                                lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                'Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, 4), "#,##0.####")
                            Else
                                Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                                lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            End If

                        End If
                        If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                            Label15.ForeColor = System.Drawing.Color.Red
                        Else
                            Label15.ForeColor = System.Drawing.Color.Black
                        End If
                        If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                        Else
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                        End If
                        TextBox17.Text = Format(dblMntVlrRecibo, "###,##0.#0")
                    ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 11 Then  'Columna para ingresar el monto de por Aplicar
                        dblMonto = 0
                        strMtoAnt = ""
                        dblMtoAnt = 0
                        strMtoAnt = ""
                        ''strMtoAnt = MSFlexGrid2.Text
                        strMtoAnt = SaldoAnterior
                        strMonto = dgvxPagosFacturas.CurrentCell.Value
                        dblMtoAnt = SUConversiones.ConvierteADecimal(strMtoAnt)
                        ''strMonto = InputBox("Digite el Monto Pendiente a Aplicar para la Factura", "Monto Pendiente")
                        If IsNumeric(Trim(strMonto)) = False Then
                            Exit Sub
                        End If
                        dblMonto = SUConversiones.ConvierteADecimal(strMonto)
                        dgvxPagosFacturas.CurrentCell.Value = Format(dblMonto, "###,##0.####")
                        If SUConversiones.ConvierteADecimal(dgvxPagosFacturas.CurrentRow.Cells("Monto").Value) = 0 Then
                            MsgBox("No puede poner el saldo a aplicar en una factura que no est� siendo abonada o cancelada.", MsgBoxStyle.Critical, "Error de Dato")
                            dgvxPagosFacturas.CurrentCell.Value = Format(dblMtoAnt, "###,##0.####")
                            dblMonto = 0
                        End If
                        dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                        dblPendienteRecibo = dblPendienteRecibo - dblMtoAnt + dblMonto
                        Label15.Text = Format(dblMontoRecibo, "###,##0.####")
                        Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.####")

                        'lblDisponibleUSD.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) / SUConversiones.ConvierteADecimal(txtTipoCambio.Text)), 2), "###,##0.#0")
                        ' lblDisponibleUSD.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), 4), "###,##0.#0")
                        If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                            lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            ' Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
                            Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        Else
                            Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                            lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        End If


                        If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                            Label15.ForeColor = System.Drawing.Color.Red
                        Else
                            Label15.ForeColor = System.Drawing.Color.Black
                        End If
                        If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                        Else
                            lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub dgvxPagosFacturas_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvxPagosFacturas.CellBeginEdit
        SaldoAnterior = dgvxPagosFacturas.CurrentCell.Value
    End Sub

    Private Sub dgvxPagosFacturas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvxPagosFacturas.KeyPress
        Dim lnRegistro As Double = 0
        Try
            If e.KeyChar = Keys.Enter.ToString Then
                CalcularMontosGrid()
            ElseIf e.KeyChar = Keys.Tab.ToString Then
                CalcularMontosGrid()
            End If
            If ValidaFacturaFechaMasAntigua() = 1 Then
                MsgBox("Existe una factura m�s antigua que tiene saldo pendiente, favor realizar pago a facturas mas antigua", MsgBoxStyle.Critical, "Pago Recibo")
                Exit Sub
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " CellEndEdit ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_CellMouseMove(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvxPagosFacturas.CellMouseMove
        Try

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " CellEndEdit ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_MouseEnter(sender As Object, e As EventArgs) Handles dgvxPagosFacturas.MouseEnter

    End Sub

    Private Sub dgvxPagosFacturas_MouseHover(sender As Object, e As EventArgs) Handles dgvxPagosFacturas.MouseHover
        Try

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " MouseHover ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_MouseMove(sender As Object, e As MouseEventArgs) Handles dgvxPagosFacturas.MouseMove
        Try

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " MouseMove ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Label14_Click(sender As Object, e As EventArgs) Handles Label14.Click

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub DTPFechaRecibo_ValueChanged(sender As Object, e As EventArgs) Handles DTPFechaRecibo.ValueChanged
        Dim lnFecha As Integer = 0

        Try
            lnFecha = SUConversiones.ConvierteAInt(Format(DTPFechaRecibo.Value, "yyyyMMdd"))
            gnTipoCambio = RNTipoCambio.ObtieneTipoCambio(lnFecha)
            txtTipoCambio.Text = gnTipoCambio
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Recibos ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cbMoneda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbMoneda.SelectedIndexChanged
        Try
            'If SUConversiones.ConvierteAInt(cbMoneda.SelectedValue) = 2 And SUConversiones.ConvierteADecimal(txtTipoCambio.Text) = 0 Then
            'If SUConversiones.ConvierteADecimal(txtTipoCambio.Text) = 0 Then
            '    MsgBox("No se ha ingresado la tasa del d�a, favor ingresar la tasa del d�a antes de realizar operaciones ", MsgBoxStyle.Critical, "Error de Datos")
            '    cbMoneda.SelectedValue = 1
            '    Return
            'End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Recibos ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmRecibos_VisibleChanged(sender As Object, e As EventArgs) Handles MyBase.VisibleChanged
        Try
            If gbIndicaCierre Then
                Me.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Recibos ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvxPagosFacturas.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                CalcularMontosGrid()
            ElseIf e.KeyCode = Keys.Tab Then
                CalcularMontosGrid()
            End If
            If ValidaFacturaFechaMasAntigua() = 1 Then
                MsgBox("Existe una factura m�s antigua que tiene saldo pendiente, favor realizar pago a facturas mas antigua", MsgBoxStyle.Critical, "Pago Recibo")
                Exit Sub
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvxPagosFacturas.CellEndEdit
        Try
            CalcularMontosGrid()
            'If ValidaFacturaFechaMasAntigua() = 1 Then
            '    MessageBox.Show("No debe pagar una factura antes ")
            'End If
            If ValidaFacturaFechaMasAntigua() = 1 Then
                MsgBox("Existe una factura m�s antigua que tiene saldo pendiente, favor realizar pago a facturas mas antigua", MsgBoxStyle.Critical, "Pago Recibo")
                Exit Sub
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " CellEndEdit ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim NumRecibo As String
        Try
            NumRecibo = txtNumeroRecibo.Text
            'If intTipoReciboCaja = 1 Then
            If intTipoReciboCaja = ENTipoRecibo.RECIBO_CAJA Then
                nUltimoNumeroRecibo = SUConversiones.ConvierteAInt(ExtraerSerieNumeroRecibo(SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem)))
                txtNumeroRecibo.Text = SUFunciones.ObtieneValorDeCombo(ddlSerie.SelectedItem) & Format(SUConversiones.ConvierteAInt(nUltimoNumeroRecibo), "0000000")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Trick ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub CalculaDisponibleRecibo()
        Dim dblMontoRecibo As Decimal = 0
        Dim lnMontoRetencion As Decimal = 0
        Dim lnMonedaRecibo As Integer = 0
        Dim lnMonedaFactura As Integer = 0
        Dim lnTipoCambio As Decimal = 0
        Dim lnMontoTotalAPagar As Decimal = 0
        Dim lnMontoTotalInteresAPagar As Decimal = 0
        Dim lnMontoDescuentoAPagar As Decimal = 0
        Dim lnMontoTotalMantenimientoValorAPagar As Decimal = 0
        Dim lnMontoTotal As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            lnMonedaRecibo = SUConversiones.ConvierteAInt(cbMoneda.SelectedValue)
            dblMontoRecibo = SUConversiones.ConvierteADecimal(txtMonto.Text)
            lnTipoCambio = SUConversiones.ConvierteADecimal(txtTipoCambio.Text)
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()

            dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
            'lnTipoCambio = Math.Round(lnTipoCambio, 2)
            lnMontoRetencion = SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text)
            lnMontoRetencion = SUFunciones.RedondeoNumero(lnMontoRetencion, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
            dblMontoRecibo = dblMontoRecibo + lnMontoRetencion
            lnTipoCambio = lnTipoCambio
            lnMontoTotalAPagar = 0
            lnMontoTotalInteresAPagar = 0
            If dgvxPagosFacturas.Rows.Count >= 1 Then
                For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                    lnMonedaFactura = 0
                    ' If dblMontoRecibo > 0 Then
                    lnMontoTotalAPagar = 0
                    lnMontoTotalInteresAPagar = 0
                    lnMontoTotalMantenimientoValorAPagar = 0
                    lnMontoDescuentoAPagar = 0
                    lnMonedaFactura = 0
                    lnMonedaFactura = SUConversiones.ConvierteAInt(dgvxPagosFacturas.Rows(intIncr).Cells("IdMoneda").Value)
                    lnMontoTotalAPagar = lnMontoTotalAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                    lnMontoTotalInteresAPagar = lnMontoTotalInteresAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
                    lnMontoTotalMantenimientoValorAPagar = lnMontoTotalMantenimientoValorAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
                    ' lnMontoDescuentoAPagar = lnMontoDescuentoAPagar + SUConversiones.ConvierteADecimal(dgvxPagosFacturas.Rows(intIncr).Cells("MontoDescuentoFact").Value)
                    lnMontoDescuentoAPagar = lnMontoDescuentoAPagar + 0

                    lnMontoTotal = 0
                    lnMontoTotal = lnMontoTotalAPagar + lnMontoDescuentoAPagar + lnMontoTotalInteresAPagar + lnMontoTotalMantenimientoValorAPagar
                    'lnMontoTotal = RNRecibos.ObtieneMontoSegunMonedaRecibo(lnMontoTotal, lnMonedaFactura,  )
                    lnMontoTotal = RNRecibos.ObtieneMontoSegunMonedaRecibo(lnMontoTotal, lnMonedaFactura, lnTipoCambio, lnMonedaRecibo, gnCantidadDecimal)
                    dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal) - SUFunciones.RedondeoNumero(lnMontoTotal, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)

                Next intIncr

                dblMontoRecibo = SUFunciones.RedondeoNumero(dblMontoRecibo, objParametro.IndicaTipoRedondeo, gnCantidadDecimal)
                'Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADecimal(txtDescripcionRecibo.Text), "#,##0.####")
                Label15.Text = Format(dblMontoRecibo, "#,##0.####")
                lblDisponibleUSD.Text = Format(SUConversiones.ConvierteADecimal(Label15.Text), "#,##0.####")
                If lnTipoCambio <> 0 Then
                    If SUConversiones.ConvierteADecimal(cbMoneda.SelectedValue) = 2 Then
                        lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")

                        If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
                            Label15.Text = 0
                        Else
                            ' Label15.Text = Format(Math.Round((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), 4), "#,##0.####")
                            Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) * lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        End If
                    Else
                        Label15.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text)), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        If SUConversiones.ConvierteADecimal(Label15.Text.Trim()) = 0 Then
                            lblDisponibleUSD.Text = 0
                        Else
                            lblDisponibleUSD.Text = Format(SUFunciones.RedondeoNumero((SUConversiones.ConvierteADecimal(Label15.Text) / lnTipoCambio), objParametro.IndicaTipoRedondeo, gnCantidadDecimal), "#,##0.####")
                        End If

                    End If

                End If
                If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) = 0 Then
                    Label15.Text = "0.0000"
                End If
                If SUConversiones.ConvierteADecimal(Label15.Text) < 0 Then
                    Label15.ForeColor = System.Drawing.Color.Red
                Else
                    Label15.ForeColor = System.Drawing.Color.Black
                End If
                If SUConversiones.ConvierteADecimal(lblDisponibleUSD.Text) < 0 Then
                    lblDisponibleUSD.ForeColor = System.Drawing.Color.Red
                Else
                    lblDisponibleUSD.ForeColor = System.Drawing.Color.Black
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MessageBoxEx.Show("Error " & ex.Message, " KeyDown ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Me.Cursor = Cursors.Default
    End Sub

End Class