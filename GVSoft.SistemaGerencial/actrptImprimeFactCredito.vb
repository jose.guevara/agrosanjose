Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class actrptImprimeFactCredito
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub PageHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageHeader1.Format

    End Sub

    Private Sub actrptImprimeFactCredito_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart
        LblDia.Text = intDiaFact
        lblMes.Text = strMesFact
        lblAnio.Text = lngYearFact
        lblNombreCliente.Text = UCase(strClienteFact)
        lblDias.Text = strDiasFact
        lblFechaVenceFactura.Text = strFecVencFact
        lblVendedor.Text = UCase(strVendedor)
        lblCodigoCliente.Text = UCase(strCodCliFact)

        lblSubTotal.Text = Format(dblSubtotal, "#,##0.#0")
        lblImpuesto.Text = Format(dblImpuesto, "#,##0.#0")
        lblTotal.Text = Format(dblTotal, "#,##0.#0")
        'lblNumeroFactura.Text = strNumeroFact

        'lblTc.Text = lblTc.Text & " US$ " & CStr(Format((dblTotal / dblTipoCambio), "#,##0.#0"))
        lblTc.Text = "Si se paga en córdobas esta factura se multiplicará por el paralelo a la fecha de pago"

        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperWidth = 8.5F
        Me.Document.Printer.PrinterName = ""
    End Sub
End Class
