Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptVtasCostoVend
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
    Private Label21 As DataDynamics.ActiveReports.Label = Nothing
    Private Label11 As DataDynamics.ActiveReports.Label = Nothing
    Private Label12 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox19 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox20 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private Label15 As DataDynamics.ActiveReports.Label = Nothing
    Private Label16 As DataDynamics.ActiveReports.Label = Nothing
    Private Label17 As DataDynamics.ActiveReports.Label = Nothing
    Private Label18 As DataDynamics.ActiveReports.Label = Nothing
    Private Label19 As DataDynamics.ActiveReports.Label = Nothing
    Private Label20 As DataDynamics.ActiveReports.Label = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private txtCodigoAngecia As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private txtDesAgencia As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtCodigoVendedor As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private txtNombreVendedor As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtNumeroFactura As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtNombreFactura As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtFechaFactura As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtMontoCompra As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtMontoGanancia As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtMontoComision As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private txtTotAgenMontoVenta As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotAgenMontoCompra As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotAgenMontoGanancia As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotAgenMontoComision As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label3 As DataDynamics.ActiveReports.Label = Nothing
    Private txtTotTpFaMontoVenta As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotTpFaMontoCompra As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotTpFactMontoGanancia As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotTpFactMontoComision As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private txtTotalVenta As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotalCompra As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtTotalGanancia As DataDynamics.ActiveReports.TextBox = Nothing
    Private WithEvents Label4 As DataDynamics.ActiveReports.Label
    Private WithEvents lblTipoFactura As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTipoFactura As DataDynamics.ActiveReports.TextBox
    Friend WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents Label1 As DataDynamics.ActiveReports.Label
    Private WithEvents txtTotVendMontoVenta As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTotVendMontoCompra As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTotVendlMontoGanancia As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTotVendMontoComision As DataDynamics.ActiveReports.TextBox
    Friend WithEvents ReportInfo1 As DataDynamics.ActiveReports.ReportInfo
    Private txtTotalComision As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptVtasCostoVend))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtNumeroFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtNombreFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtFechaFactura = New DataDynamics.ActiveReports.TextBox
        Me.TextBox7 = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoCompra = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoGanancia = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoComision = New DataDynamics.ActiveReports.TextBox
        Me.ReportHeader = New DataDynamics.ActiveReports.ReportHeader
        Me.ReportFooter = New DataDynamics.ActiveReports.ReportFooter
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.txtTotalVenta = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalCompra = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalGanancia = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalComision = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.Label21 = New DataDynamics.ActiveReports.Label
        Me.Label11 = New DataDynamics.ActiveReports.Label
        Me.Label12 = New DataDynamics.ActiveReports.Label
        Me.TextBox19 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox20 = New DataDynamics.ActiveReports.TextBox
        Me.Label14 = New DataDynamics.ActiveReports.Label
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.Label17 = New DataDynamics.ActiveReports.Label
        Me.Label18 = New DataDynamics.ActiveReports.Label
        Me.Label19 = New DataDynamics.ActiveReports.Label
        Me.Label20 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label8 = New DataDynamics.ActiveReports.Label
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.lblTipoFactura = New DataDynamics.ActiveReports.Label
        Me.txtTipoFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtCodigoAngecia = New DataDynamics.ActiveReports.TextBox
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.txtDesAgencia = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.txtTotTpFaMontoVenta = New DataDynamics.ActiveReports.TextBox
        Me.txtTotTpFaMontoCompra = New DataDynamics.ActiveReports.TextBox
        Me.txtTotTpFactMontoGanancia = New DataDynamics.ActiveReports.TextBox
        Me.txtTotTpFactMontoComision = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader3 = New DataDynamics.ActiveReports.GroupHeader
        Me.txtCodigoVendedor = New DataDynamics.ActiveReports.TextBox
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.txtNombreVendedor = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter3 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label7 = New DataDynamics.ActiveReports.Label
        Me.txtTotAgenMontoVenta = New DataDynamics.ActiveReports.TextBox
        Me.txtTotAgenMontoCompra = New DataDynamics.ActiveReports.TextBox
        Me.txtTotAgenMontoGanancia = New DataDynamics.ActiveReports.TextBox
        Me.txtTotAgenMontoComision = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader2 = New DataDynamics.ActiveReports.GroupHeader
        Me.GroupFooter2 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.txtTotVendMontoVenta = New DataDynamics.ActiveReports.TextBox
        Me.txtTotVendMontoCompra = New DataDynamics.ActiveReports.TextBox
        Me.txtTotVendlMontoGanancia = New DataDynamics.ActiveReports.TextBox
        Me.txtTotVendMontoComision = New DataDynamics.ActiveReports.TextBox
        Me.ReportInfo1 = New DataDynamics.ActiveReports.ReportInfo
        CType(Me.txtNumeroFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFechaFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoGanancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalGanancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTipoFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTipoFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoAngecia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDesAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotTpFaMontoVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotTpFaMontoCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotTpFactMontoGanancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotTpFactMontoComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombreVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotAgenMontoVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotAgenMontoCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotAgenMontoGanancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotAgenMontoComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotVendMontoVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotVendMontoCompra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotVendlMontoGanancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotVendMontoComision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtNumeroFactura, Me.txtNombreFactura, Me.txtFechaFactura, Me.TextBox7, Me.txtMontoCompra, Me.txtMontoGanancia, Me.txtMontoComision})
        Me.Detail.Height = 0.1979167!
        Me.Detail.Name = "Detail"
        '
        'txtNumeroFactura
        '
        Me.txtNumeroFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.DataField = "NumeroFactura"
        Me.txtNumeroFactura.Height = 0.1875!
        Me.txtNumeroFactura.Left = 0.05!
        Me.txtNumeroFactura.Name = "txtNumeroFactura"
        Me.txtNumeroFactura.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman; "
        Me.txtNumeroFactura.Text = Nothing
        Me.txtNumeroFactura.Top = 0.0!
        Me.txtNumeroFactura.Width = 0.6000001!
        '
        'txtNombreFactura
        '
        Me.txtNombreFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNombreFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNombreFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtNombreFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtNombreFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreFactura.DataField = "NombreFactura"
        Me.txtNombreFactura.Height = 0.1875!
        Me.txtNombreFactura.Left = 0.6875!
        Me.txtNombreFactura.Name = "txtNombreFactura"
        Me.txtNombreFactura.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman; "
        Me.txtNombreFactura.Text = Nothing
        Me.txtNombreFactura.Top = 0.0!
        Me.txtNombreFactura.Width = 2.5!
        '
        'txtFechaFactura
        '
        Me.txtFechaFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFechaFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFechaFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtFechaFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtFechaFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaFactura.DataField = "FechaFactura"
        Me.txtFechaFactura.Height = 0.1875!
        Me.txtFechaFactura.Left = 3.25!
        Me.txtFechaFactura.Name = "txtFechaFactura"
        Me.txtFechaFactura.OutputFormat = resources.GetString("txtFechaFactura.OutputFormat")
        Me.txtFechaFactura.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" & _
            " Roman; "
        Me.txtFechaFactura.Text = Nothing
        Me.txtFechaFactura.Top = 0.0!
        Me.txtFechaFactura.Width = 0.75!
        '
        'TextBox7
        '
        Me.TextBox7.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.DataField = "SubTotal"
        Me.TextBox7.Height = 0.1875!
        Me.TextBox7.Left = 4.1875!
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.OutputFormat = resources.GetString("TextBox7.OutputFormat")
        Me.TextBox7.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" & _
            " Roman; "
        Me.TextBox7.Text = Nothing
        Me.TextBox7.Top = 0.0!
        Me.TextBox7.Width = 0.6875!
        '
        'txtMontoCompra
        '
        Me.txtMontoCompra.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoCompra.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCompra.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoCompra.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCompra.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoCompra.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCompra.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoCompra.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCompra.DataField = "SubTotalCosto"
        Me.txtMontoCompra.Height = 0.1875!
        Me.txtMontoCompra.Left = 5.0!
        Me.txtMontoCompra.Name = "txtMontoCompra"
        Me.txtMontoCompra.OutputFormat = resources.GetString("txtMontoCompra.OutputFormat")
        Me.txtMontoCompra.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" & _
            " Roman; "
        Me.txtMontoCompra.Text = Nothing
        Me.txtMontoCompra.Top = 0.0!
        Me.txtMontoCompra.Width = 0.6624999!
        '
        'txtMontoGanancia
        '
        Me.txtMontoGanancia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoGanancia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoGanancia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoGanancia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoGanancia.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoGanancia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoGanancia.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoGanancia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoGanancia.DataField = "Ganancia"
        Me.txtMontoGanancia.Height = 0.1875!
        Me.txtMontoGanancia.Left = 5.75!
        Me.txtMontoGanancia.Name = "txtMontoGanancia"
        Me.txtMontoGanancia.OutputFormat = resources.GetString("txtMontoGanancia.OutputFormat")
        Me.txtMontoGanancia.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" & _
            " Roman; "
        Me.txtMontoGanancia.Text = Nothing
        Me.txtMontoGanancia.Top = 0.0!
        Me.txtMontoGanancia.Width = 0.75!
        '
        'txtMontoComision
        '
        Me.txtMontoComision.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoComision.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoComision.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoComision.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoComision.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoComision.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoComision.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoComision.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoComision.DataField = "Comision"
        Me.txtMontoComision.Height = 0.1875!
        Me.txtMontoComision.Left = 6.625!
        Me.txtMontoComision.Name = "txtMontoComision"
        Me.txtMontoComision.OutputFormat = resources.GetString("txtMontoComision.OutputFormat")
        Me.txtMontoComision.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" & _
            " Roman; "
        Me.txtMontoComision.Text = Nothing
        Me.txtMontoComision.Top = 0.0!
        Me.txtMontoComision.Width = 0.6875!
        '
        'ReportHeader
        '
        Me.ReportHeader.Height = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label5, Me.txtTotalVenta, Me.txtTotalCompra, Me.txtTotalGanancia, Me.txtTotalComision})
        Me.ReportFooter.Height = 0.3125!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'Label5
        '
        Me.Label5.Border.BottomColor = System.Drawing.Color.Black
        Me.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.LeftColor = System.Drawing.Color.Black
        Me.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.RightColor = System.Drawing.Color.Black
        Me.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.TopColor = System.Drawing.Color.Black
        Me.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0.0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "text-align: right; font-weight: bold; font-size: 9pt; "
        Me.Label5.Text = "Total del Reporte"
        Me.Label5.Top = 0.0!
        Me.Label5.Width = 1.2375!
        '
        'txtTotalVenta
        '
        Me.txtTotalVenta.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalVenta.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalVenta.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalVenta.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalVenta.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalVenta.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalVenta.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalVenta.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalVenta.DataField = "SubTotal"
        Me.txtTotalVenta.Height = 0.1875!
        Me.txtTotalVenta.Left = 4.1875!
        Me.txtTotalVenta.Name = "txtTotalVenta"
        Me.txtTotalVenta.OutputFormat = resources.GetString("txtTotalVenta.OutputFormat")
        Me.txtTotalVenta.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotalVenta.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalVenta.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalVenta.Text = Nothing
        Me.txtTotalVenta.Top = 0.0!
        Me.txtTotalVenta.Width = 0.6875!
        '
        'txtTotalCompra
        '
        Me.txtTotalCompra.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalCompra.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCompra.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalCompra.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCompra.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalCompra.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCompra.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalCompra.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalCompra.DataField = "SubTotalCosto"
        Me.txtTotalCompra.Height = 0.1875!
        Me.txtTotalCompra.Left = 5.0!
        Me.txtTotalCompra.Name = "txtTotalCompra"
        Me.txtTotalCompra.OutputFormat = resources.GetString("txtTotalCompra.OutputFormat")
        Me.txtTotalCompra.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotalCompra.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalCompra.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalCompra.Text = Nothing
        Me.txtTotalCompra.Top = 0.0!
        Me.txtTotalCompra.Width = 0.6875!
        '
        'txtTotalGanancia
        '
        Me.txtTotalGanancia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalGanancia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalGanancia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalGanancia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalGanancia.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalGanancia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalGanancia.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalGanancia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalGanancia.DataField = "Ganancia"
        Me.txtTotalGanancia.Height = 0.1875!
        Me.txtTotalGanancia.Left = 5.875!
        Me.txtTotalGanancia.Name = "txtTotalGanancia"
        Me.txtTotalGanancia.OutputFormat = resources.GetString("txtTotalGanancia.OutputFormat")
        Me.txtTotalGanancia.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotalGanancia.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalGanancia.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalGanancia.Text = Nothing
        Me.txtTotalGanancia.Top = 0.0!
        Me.txtTotalGanancia.Width = 0.6875!
        '
        'txtTotalComision
        '
        Me.txtTotalComision.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalComision.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalComision.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalComision.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalComision.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalComision.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalComision.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalComision.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalComision.DataField = "Comision"
        Me.txtTotalComision.Height = 0.1875!
        Me.txtTotalComision.Left = 6.6875!
        Me.txtTotalComision.Name = "txtTotalComision"
        Me.txtTotalComision.OutputFormat = resources.GetString("txtTotalComision.OutputFormat")
        Me.txtTotalComision.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotalComision.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalComision.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalComision.Text = Nothing
        Me.txtTotalComision.Top = 0.0!
        Me.txtTotalComision.Width = 0.6875!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label21, Me.Label11, Me.Label12, Me.TextBox19, Me.TextBox20, Me.Label14, Me.Label15, Me.Label16, Me.Label17, Me.Label18, Me.Label19, Me.Label20, Me.Label4, Me.Label8})
        Me.PageHeader.Height = 1.21875!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label21
        '
        Me.Label21.Border.BottomColor = System.Drawing.Color.Black
        Me.Label21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.LeftColor = System.Drawing.Color.Black
        Me.Label21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.RightColor = System.Drawing.Color.Black
        Me.Label21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.TopColor = System.Drawing.Color.Black
        Me.Label21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Height = 0.2!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.0625!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label21.Text = "REPORTE DE COMISIONES POR  VENDEDOR"
        Me.Label21.Top = 0.25!
        Me.Label21.Width = 3.2!
        '
        'Label11
        '
        Me.Label11.Border.BottomColor = System.Drawing.Color.Black
        Me.Label11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Border.LeftColor = System.Drawing.Color.Black
        Me.Label11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Border.RightColor = System.Drawing.Color.Black
        Me.Label11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Border.TopColor = System.Drawing.Color.Black
        Me.Label11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Height = 0.2!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 5.15!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label11.Text = "Fecha de Impresi�n"
        Me.Label11.Top = 0.05!
        Me.Label11.Width = 1.35!
        '
        'Label12
        '
        Me.Label12.Border.BottomColor = System.Drawing.Color.Black
        Me.Label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Border.LeftColor = System.Drawing.Color.Black
        Me.Label12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Border.RightColor = System.Drawing.Color.Black
        Me.Label12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Border.TopColor = System.Drawing.Color.Black
        Me.Label12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Height = 0.2!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 5.15!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label12.Text = "Hora de Impresi�n"
        Me.Label12.Top = 0.25!
        Me.Label12.Width = 1.35!
        '
        'TextBox19
        '
        Me.TextBox19.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox19.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox19.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox19.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox19.Height = 0.2!
        Me.TextBox19.Left = 6.5!
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Style = "text-align: right; "
        Me.TextBox19.Text = "TextBox19"
        Me.TextBox19.Top = 0.05!
        Me.TextBox19.Width = 0.9!
        '
        'TextBox20
        '
        Me.TextBox20.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Height = 0.2!
        Me.TextBox20.Left = 6.5!
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Style = "text-align: right; "
        Me.TextBox20.Text = "TextBox20"
        Me.TextBox20.Top = 0.25!
        Me.TextBox20.Width = 0.9!
        '
        'Label14
        '
        Me.Label14.Border.BottomColor = System.Drawing.Color.Black
        Me.Label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.LeftColor = System.Drawing.Color.Black
        Me.Label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.RightColor = System.Drawing.Color.Black
        Me.Label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.TopColor = System.Drawing.Color.Black
        Me.Label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.0!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.Label14.Text = "Factura"
        Me.Label14.Top = 1.0!
        Me.Label14.Width = 0.55!
        '
        'Label15
        '
        Me.Label15.Border.BottomColor = System.Drawing.Color.Black
        Me.Label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.LeftColor = System.Drawing.Color.Black
        Me.Label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.RightColor = System.Drawing.Color.Black
        Me.Label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.TopColor = System.Drawing.Color.Black
        Me.Label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0.6499999!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.Label15.Text = "Nombre Factura"
        Me.Label15.Top = 1.0!
        Me.Label15.Width = 1.45!
        '
        'Label16
        '
        Me.Label16.Border.BottomColor = System.Drawing.Color.Black
        Me.Label16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.LeftColor = System.Drawing.Color.Black
        Me.Label16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.RightColor = System.Drawing.Color.Black
        Me.Label16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.TopColor = System.Drawing.Color.Black
        Me.Label16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.25!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 8.25pt; "
        Me.Label16.Text = "Fecha Factura"
        Me.Label16.Top = 1.0!
        Me.Label16.Width = 0.8125!
        '
        'Label17
        '
        Me.Label17.Border.BottomColor = System.Drawing.Color.Black
        Me.Label17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.LeftColor = System.Drawing.Color.Black
        Me.Label17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.RightColor = System.Drawing.Color.Black
        Me.Label17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.TopColor = System.Drawing.Color.Black
        Me.Label17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 4.125!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label17.Text = "Monto Venta"
        Me.Label17.Top = 1.0!
        Me.Label17.Width = 0.8125!
        '
        'Label18
        '
        Me.Label18.Border.BottomColor = System.Drawing.Color.Black
        Me.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Border.LeftColor = System.Drawing.Color.Black
        Me.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Border.RightColor = System.Drawing.Color.Black
        Me.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Border.TopColor = System.Drawing.Color.Black
        Me.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Height = 0.3125!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 5.0!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label18.Text = "Monto Compra"
        Me.Label18.Top = 0.875!
        Me.Label18.Width = 0.625!
        '
        'Label19
        '
        Me.Label19.Border.BottomColor = System.Drawing.Color.Black
        Me.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.LeftColor = System.Drawing.Color.Black
        Me.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.RightColor = System.Drawing.Color.Black
        Me.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.TopColor = System.Drawing.Color.Black
        Me.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Height = 0.3125!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 5.75!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label19.Text = "Monto Ganancia"
        Me.Label19.Top = 0.875!
        Me.Label19.Width = 0.625!
        '
        'Label20
        '
        Me.Label20.Border.BottomColor = System.Drawing.Color.Black
        Me.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.LeftColor = System.Drawing.Color.Black
        Me.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.RightColor = System.Drawing.Color.Black
        Me.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.TopColor = System.Drawing.Color.Black
        Me.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Height = 0.3125!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 6.625!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label20.Text = "Monto Comision"
        Me.Label20.Top = 0.875!
        Me.Label20.Width = 0.6875!
        '
        'Label4
        '
        Me.Label4.Border.BottomColor = System.Drawing.Color.Black
        Me.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.LeftColor = System.Drawing.Color.Black
        Me.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.RightColor = System.Drawing.Color.Black
        Me.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.TopColor = System.Drawing.Color.Black
        Me.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Height = 0.2!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.075!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label4.Text = ""
        Me.Label4.Top = 0.45!
        Me.Label4.Width = 2.75!
        '
        'Label8
        '
        Me.Label8.Border.BottomColor = System.Drawing.Color.Black
        Me.Label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.LeftColor = System.Drawing.Color.Black
        Me.Label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.RightColor = System.Drawing.Color.Black
        Me.Label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.TopColor = System.Drawing.Color.Black
        Me.Label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Height = 0.2!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.0625!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label8.Text = "REPORTE GENERADO DE LA FACTURACION"
        Me.Label8.Top = 0.0625!
        Me.Label8.Width = 3.0!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.ReportInfo1})
        Me.PageFooter.Height = 0.46875!
        Me.PageFooter.Name = "PageFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblTipoFactura, Me.txtTipoFactura})
        Me.GroupHeader1.DataField = "TipoFactura"
        Me.GroupHeader1.Height = 0.2388889!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lblTipoFactura
        '
        Me.lblTipoFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTipoFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTipoFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTipoFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTipoFactura.Border.RightColor = System.Drawing.Color.Black
        Me.lblTipoFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTipoFactura.Border.TopColor = System.Drawing.Color.Black
        Me.lblTipoFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTipoFactura.Height = 0.1875!
        Me.lblTipoFactura.HyperLink = Nothing
        Me.lblTipoFactura.Left = 0.0!
        Me.lblTipoFactura.Name = "lblTipoFactura"
        Me.lblTipoFactura.Style = "font-weight: bold; font-size: 9pt; "
        Me.lblTipoFactura.Text = "TIPO FACTURA"
        Me.lblTipoFactura.Top = 0.0!
        Me.lblTipoFactura.Width = 1.0!
        '
        'txtTipoFactura
        '
        Me.txtTipoFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTipoFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTipoFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtTipoFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtTipoFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoFactura.DataField = "TipoFactura"
        Me.txtTipoFactura.Height = 0.1875!
        Me.txtTipoFactura.Left = 1.0!
        Me.txtTipoFactura.Name = "txtTipoFactura"
        Me.txtTipoFactura.Style = "font-weight: bold; "
        Me.txtTipoFactura.Text = Nothing
        Me.txtTipoFactura.Top = 0.0!
        Me.txtTipoFactura.Width = 3.25!
        '
        'txtCodigoAngecia
        '
        Me.txtCodigoAngecia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCodigoAngecia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAngecia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCodigoAngecia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAngecia.Border.RightColor = System.Drawing.Color.Black
        Me.txtCodigoAngecia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAngecia.Border.TopColor = System.Drawing.Color.Black
        Me.txtCodigoAngecia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAngecia.DataField = "codigosuc"
        Me.txtCodigoAngecia.Height = 0.1875!
        Me.txtCodigoAngecia.Left = 3.75!
        Me.txtCodigoAngecia.Name = "txtCodigoAngecia"
        Me.txtCodigoAngecia.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtCodigoAngecia.Text = Nothing
        Me.txtCodigoAngecia.Top = 0.0!
        Me.txtCodigoAngecia.Width = 0.375!
        '
        'Label
        '
        Me.Label.Border.BottomColor = System.Drawing.Color.Black
        Me.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.LeftColor = System.Drawing.Color.Black
        Me.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.RightColor = System.Drawing.Color.Black
        Me.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.TopColor = System.Drawing.Color.Black
        Me.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.0!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label.Text = "AGENCIA"
        Me.Label.Top = 0.0!
        Me.Label.Width = 0.6875!
        '
        'txtDesAgencia
        '
        Me.txtDesAgencia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.Border.RightColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.Border.TopColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.DataField = "Agencia"
        Me.txtDesAgencia.Height = 0.1875!
        Me.txtDesAgencia.Left = 0.625!
        Me.txtDesAgencia.Name = "txtDesAgencia"
        Me.txtDesAgencia.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtDesAgencia.Text = Nothing
        Me.txtDesAgencia.Top = 0.0!
        Me.txtDesAgencia.Width = 2.9375!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label3, Me.txtTotTpFaMontoVenta, Me.txtTotTpFaMontoCompra, Me.txtTotTpFactMontoGanancia, Me.txtTotTpFactMontoComision})
        Me.GroupFooter1.Height = 0.3125!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label3
        '
        Me.Label3.Border.BottomColor = System.Drawing.Color.Black
        Me.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.LeftColor = System.Drawing.Color.Black
        Me.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.RightColor = System.Drawing.Color.Black
        Me.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.TopColor = System.Drawing.Color.Black
        Me.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 2.564583!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "text-align: right; font-weight: bold; font-size: 9pt; "
        Me.Label3.Text = "Total de Tipo Factura"
        Me.Label3.Top = 0.0!
        Me.Label3.Width = 1.2375!
        '
        'txtTotTpFaMontoVenta
        '
        Me.txtTotTpFaMontoVenta.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoVenta.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoVenta.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoVenta.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoVenta.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoVenta.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoVenta.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoVenta.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoVenta.DataField = "SubTotal"
        Me.txtTotTpFaMontoVenta.Height = 0.1875!
        Me.txtTotTpFaMontoVenta.Left = 4.1875!
        Me.txtTotTpFaMontoVenta.Name = "txtTotTpFaMontoVenta"
        Me.txtTotTpFaMontoVenta.OutputFormat = resources.GetString("txtTotTpFaMontoVenta.OutputFormat")
        Me.txtTotTpFaMontoVenta.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotTpFaMontoVenta.SummaryGroup = "GroupHeader1"
        Me.txtTotTpFaMontoVenta.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotTpFaMontoVenta.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotTpFaMontoVenta.Text = Nothing
        Me.txtTotTpFaMontoVenta.Top = 0.0!
        Me.txtTotTpFaMontoVenta.Width = 0.6875!
        '
        'txtTotTpFaMontoCompra
        '
        Me.txtTotTpFaMontoCompra.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoCompra.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoCompra.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoCompra.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoCompra.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoCompra.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoCompra.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotTpFaMontoCompra.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFaMontoCompra.DataField = "SubTotalCosto"
        Me.txtTotTpFaMontoCompra.Height = 0.1875!
        Me.txtTotTpFaMontoCompra.Left = 5.0!
        Me.txtTotTpFaMontoCompra.Name = "txtTotTpFaMontoCompra"
        Me.txtTotTpFaMontoCompra.OutputFormat = resources.GetString("txtTotTpFaMontoCompra.OutputFormat")
        Me.txtTotTpFaMontoCompra.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotTpFaMontoCompra.SummaryGroup = "GroupHeader1"
        Me.txtTotTpFaMontoCompra.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotTpFaMontoCompra.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotTpFaMontoCompra.Text = Nothing
        Me.txtTotTpFaMontoCompra.Top = 0.0!
        Me.txtTotTpFaMontoCompra.Width = 0.6875!
        '
        'txtTotTpFactMontoGanancia
        '
        Me.txtTotTpFactMontoGanancia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoGanancia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoGanancia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoGanancia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoGanancia.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoGanancia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoGanancia.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoGanancia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoGanancia.DataField = "Ganancia"
        Me.txtTotTpFactMontoGanancia.Height = 0.1875!
        Me.txtTotTpFactMontoGanancia.Left = 5.8125!
        Me.txtTotTpFactMontoGanancia.Name = "txtTotTpFactMontoGanancia"
        Me.txtTotTpFactMontoGanancia.OutputFormat = resources.GetString("txtTotTpFactMontoGanancia.OutputFormat")
        Me.txtTotTpFactMontoGanancia.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotTpFactMontoGanancia.SummaryGroup = "GroupHeader1"
        Me.txtTotTpFactMontoGanancia.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotTpFactMontoGanancia.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotTpFactMontoGanancia.Text = Nothing
        Me.txtTotTpFactMontoGanancia.Top = 0.0!
        Me.txtTotTpFactMontoGanancia.Width = 0.6875!
        '
        'txtTotTpFactMontoComision
        '
        Me.txtTotTpFactMontoComision.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoComision.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoComision.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoComision.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoComision.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoComision.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoComision.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotTpFactMontoComision.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotTpFactMontoComision.DataField = "Comision"
        Me.txtTotTpFactMontoComision.Height = 0.1875!
        Me.txtTotTpFactMontoComision.Left = 6.625!
        Me.txtTotTpFactMontoComision.Name = "txtTotTpFactMontoComision"
        Me.txtTotTpFactMontoComision.OutputFormat = resources.GetString("txtTotTpFactMontoComision.OutputFormat")
        Me.txtTotTpFactMontoComision.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotTpFactMontoComision.SummaryGroup = "GroupHeader1"
        Me.txtTotTpFactMontoComision.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotTpFactMontoComision.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotTpFactMontoComision.Text = Nothing
        Me.txtTotTpFactMontoComision.Top = 0.0!
        Me.txtTotTpFactMontoComision.Width = 0.6875!
        '
        'GroupHeader3
        '
        Me.GroupHeader3.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtDesAgencia, Me.txtCodigoAngecia, Me.Label})
        Me.GroupHeader3.DataField = "Agencia"
        Me.GroupHeader3.Height = 0.25!
        Me.GroupHeader3.Name = "GroupHeader3"
        '
        'txtCodigoVendedor
        '
        Me.txtCodigoVendedor.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCodigoVendedor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoVendedor.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCodigoVendedor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoVendedor.Border.RightColor = System.Drawing.Color.Black
        Me.txtCodigoVendedor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoVendedor.Border.TopColor = System.Drawing.Color.Black
        Me.txtCodigoVendedor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoVendedor.DataField = "Vendedor"
        Me.txtCodigoVendedor.Height = 0.1875!
        Me.txtCodigoVendedor.Left = 0.8125!
        Me.txtCodigoVendedor.Name = "txtCodigoVendedor"
        Me.txtCodigoVendedor.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtCodigoVendedor.Text = Nothing
        Me.txtCodigoVendedor.Top = 0.0!
        Me.txtCodigoVendedor.Width = 2.8125!
        '
        'Label6
        '
        Me.Label6.Border.BottomColor = System.Drawing.Color.Black
        Me.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.LeftColor = System.Drawing.Color.Black
        Me.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.RightColor = System.Drawing.Color.Black
        Me.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.TopColor = System.Drawing.Color.Black
        Me.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 0.0!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label6.Text = "VENDEDOR"
        Me.Label6.Top = 0.0!
        Me.Label6.Width = 0.75!
        '
        'txtNombreVendedor
        '
        Me.txtNombreVendedor.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNombreVendedor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreVendedor.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNombreVendedor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreVendedor.Border.RightColor = System.Drawing.Color.Black
        Me.txtNombreVendedor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreVendedor.Border.TopColor = System.Drawing.Color.Black
        Me.txtNombreVendedor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombreVendedor.DataField = "NombreVend"
        Me.txtNombreVendedor.Height = 0.1875!
        Me.txtNombreVendedor.Left = 3.875!
        Me.txtNombreVendedor.Name = "txtNombreVendedor"
        Me.txtNombreVendedor.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtNombreVendedor.Text = Nothing
        Me.txtNombreVendedor.Top = 0.0!
        Me.txtNombreVendedor.Width = 2.0625!
        '
        'GroupFooter3
        '
        Me.GroupFooter3.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label7, Me.txtTotAgenMontoVenta, Me.txtTotAgenMontoCompra, Me.txtTotAgenMontoGanancia, Me.txtTotAgenMontoComision})
        Me.GroupFooter3.Height = 0.25!
        Me.GroupFooter3.Name = "GroupFooter3"
        '
        'Label7
        '
        Me.Label7.Border.BottomColor = System.Drawing.Color.Black
        Me.Label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.LeftColor = System.Drawing.Color.Black
        Me.Label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.RightColor = System.Drawing.Color.Black
        Me.Label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.TopColor = System.Drawing.Color.Black
        Me.Label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 2.625!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "text-align: right; font-weight: bold; font-size: 9pt; "
        Me.Label7.Text = "Total de Agencia"
        Me.Label7.Top = 0.0!
        Me.Label7.Width = 1.175!
        '
        'txtTotAgenMontoVenta
        '
        Me.txtTotAgenMontoVenta.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoVenta.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoVenta.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoVenta.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoVenta.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoVenta.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoVenta.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoVenta.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoVenta.DataField = "SubTotal"
        Me.txtTotAgenMontoVenta.Height = 0.1875!
        Me.txtTotAgenMontoVenta.Left = 4.1875!
        Me.txtTotAgenMontoVenta.Name = "txtTotAgenMontoVenta"
        Me.txtTotAgenMontoVenta.OutputFormat = resources.GetString("txtTotAgenMontoVenta.OutputFormat")
        Me.txtTotAgenMontoVenta.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotAgenMontoVenta.SummaryGroup = "GroupHeader3"
        Me.txtTotAgenMontoVenta.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotAgenMontoVenta.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotAgenMontoVenta.Text = Nothing
        Me.txtTotAgenMontoVenta.Top = 0.0!
        Me.txtTotAgenMontoVenta.Width = 0.6875!
        '
        'txtTotAgenMontoCompra
        '
        Me.txtTotAgenMontoCompra.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoCompra.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoCompra.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoCompra.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoCompra.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoCompra.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoCompra.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoCompra.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoCompra.DataField = "SubTotalCosto"
        Me.txtTotAgenMontoCompra.Height = 0.1875!
        Me.txtTotAgenMontoCompra.Left = 5.0!
        Me.txtTotAgenMontoCompra.Name = "txtTotAgenMontoCompra"
        Me.txtTotAgenMontoCompra.OutputFormat = resources.GetString("txtTotAgenMontoCompra.OutputFormat")
        Me.txtTotAgenMontoCompra.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotAgenMontoCompra.SummaryGroup = "GroupHeader3"
        Me.txtTotAgenMontoCompra.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotAgenMontoCompra.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotAgenMontoCompra.Text = Nothing
        Me.txtTotAgenMontoCompra.Top = 0.0!
        Me.txtTotAgenMontoCompra.Width = 0.6624999!
        '
        'txtTotAgenMontoGanancia
        '
        Me.txtTotAgenMontoGanancia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoGanancia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoGanancia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoGanancia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoGanancia.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoGanancia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoGanancia.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoGanancia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoGanancia.DataField = "Ganancia"
        Me.txtTotAgenMontoGanancia.Height = 0.1875!
        Me.txtTotAgenMontoGanancia.Left = 5.8125!
        Me.txtTotAgenMontoGanancia.Name = "txtTotAgenMontoGanancia"
        Me.txtTotAgenMontoGanancia.OutputFormat = resources.GetString("txtTotAgenMontoGanancia.OutputFormat")
        Me.txtTotAgenMontoGanancia.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotAgenMontoGanancia.SummaryGroup = "GroupHeader3"
        Me.txtTotAgenMontoGanancia.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotAgenMontoGanancia.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotAgenMontoGanancia.Text = Nothing
        Me.txtTotAgenMontoGanancia.Top = 0.0!
        Me.txtTotAgenMontoGanancia.Width = 0.6875!
        '
        'txtTotAgenMontoComision
        '
        Me.txtTotAgenMontoComision.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoComision.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoComision.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoComision.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoComision.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoComision.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoComision.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotAgenMontoComision.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotAgenMontoComision.DataField = "Comision"
        Me.txtTotAgenMontoComision.Height = 0.1875!
        Me.txtTotAgenMontoComision.Left = 6.625!
        Me.txtTotAgenMontoComision.Name = "txtTotAgenMontoComision"
        Me.txtTotAgenMontoComision.OutputFormat = resources.GetString("txtTotAgenMontoComision.OutputFormat")
        Me.txtTotAgenMontoComision.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotAgenMontoComision.SummaryGroup = "GroupHeader3"
        Me.txtTotAgenMontoComision.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotAgenMontoComision.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotAgenMontoComision.Text = Nothing
        Me.txtTotAgenMontoComision.Top = 0.0!
        Me.txtTotAgenMontoComision.Width = 0.6875!
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtNombreVendedor, Me.txtCodigoVendedor, Me.Label6})
        Me.GroupHeader2.DataField = "Vendedor"
        Me.GroupHeader2.Height = 0.25!
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label1, Me.txtTotVendMontoVenta, Me.txtTotVendMontoCompra, Me.txtTotVendlMontoGanancia, Me.txtTotVendMontoComision})
        Me.GroupFooter2.Height = 0.25!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'Label1
        '
        Me.Label1.Border.BottomColor = System.Drawing.Color.Black
        Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.LeftColor = System.Drawing.Color.Black
        Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.RightColor = System.Drawing.Color.Black
        Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.TopColor = System.Drawing.Color.Black
        Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 2.625!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-align: right; font-weight: bold; font-size: 9pt; "
        Me.Label1.Text = "Total del Vendedor"
        Me.Label1.Top = 0.0!
        Me.Label1.Width = 1.175!
        '
        'txtTotVendMontoVenta
        '
        Me.txtTotVendMontoVenta.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotVendMontoVenta.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoVenta.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotVendMontoVenta.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoVenta.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotVendMontoVenta.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoVenta.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotVendMontoVenta.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoVenta.DataField = "SubTotal"
        Me.txtTotVendMontoVenta.Height = 0.1875!
        Me.txtTotVendMontoVenta.Left = 4.1875!
        Me.txtTotVendMontoVenta.Name = "txtTotVendMontoVenta"
        Me.txtTotVendMontoVenta.OutputFormat = resources.GetString("txtTotVendMontoVenta.OutputFormat")
        Me.txtTotVendMontoVenta.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotVendMontoVenta.SummaryGroup = "GroupHeader2"
        Me.txtTotVendMontoVenta.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotVendMontoVenta.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotVendMontoVenta.Text = Nothing
        Me.txtTotVendMontoVenta.Top = 0.0!
        Me.txtTotVendMontoVenta.Width = 0.6875!
        '
        'txtTotVendMontoCompra
        '
        Me.txtTotVendMontoCompra.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotVendMontoCompra.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoCompra.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotVendMontoCompra.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoCompra.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotVendMontoCompra.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoCompra.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotVendMontoCompra.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoCompra.DataField = "SubTotalCosto"
        Me.txtTotVendMontoCompra.Height = 0.1875!
        Me.txtTotVendMontoCompra.Left = 5.0!
        Me.txtTotVendMontoCompra.Name = "txtTotVendMontoCompra"
        Me.txtTotVendMontoCompra.OutputFormat = resources.GetString("txtTotVendMontoCompra.OutputFormat")
        Me.txtTotVendMontoCompra.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotVendMontoCompra.SummaryGroup = "GroupHeader2"
        Me.txtTotVendMontoCompra.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotVendMontoCompra.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotVendMontoCompra.Text = Nothing
        Me.txtTotVendMontoCompra.Top = 0.0!
        Me.txtTotVendMontoCompra.Width = 0.6624999!
        '
        'txtTotVendlMontoGanancia
        '
        Me.txtTotVendlMontoGanancia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotVendlMontoGanancia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendlMontoGanancia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotVendlMontoGanancia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendlMontoGanancia.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotVendlMontoGanancia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendlMontoGanancia.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotVendlMontoGanancia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendlMontoGanancia.DataField = "Ganancia"
        Me.txtTotVendlMontoGanancia.Height = 0.1875!
        Me.txtTotVendlMontoGanancia.Left = 5.8125!
        Me.txtTotVendlMontoGanancia.Name = "txtTotVendlMontoGanancia"
        Me.txtTotVendlMontoGanancia.OutputFormat = resources.GetString("txtTotVendlMontoGanancia.OutputFormat")
        Me.txtTotVendlMontoGanancia.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotVendlMontoGanancia.SummaryGroup = "GroupHeader2"
        Me.txtTotVendlMontoGanancia.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotVendlMontoGanancia.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotVendlMontoGanancia.Text = Nothing
        Me.txtTotVendlMontoGanancia.Top = 0.0!
        Me.txtTotVendlMontoGanancia.Width = 0.6875!
        '
        'txtTotVendMontoComision
        '
        Me.txtTotVendMontoComision.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotVendMontoComision.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoComision.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotVendMontoComision.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoComision.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotVendMontoComision.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoComision.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotVendMontoComision.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotVendMontoComision.DataField = "Comision"
        Me.txtTotVendMontoComision.Height = 0.1875!
        Me.txtTotVendMontoComision.Left = 6.625!
        Me.txtTotVendMontoComision.Name = "txtTotVendMontoComision"
        Me.txtTotVendMontoComision.OutputFormat = resources.GetString("txtTotVendMontoComision.OutputFormat")
        Me.txtTotVendMontoComision.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" & _
            "oman; "
        Me.txtTotVendMontoComision.SummaryGroup = "GroupHeader2"
        Me.txtTotVendMontoComision.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotVendMontoComision.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotVendMontoComision.Text = Nothing
        Me.txtTotVendMontoComision.Top = 0.0!
        Me.txtTotVendMontoComision.Width = 0.6875!
        '
        'ReportInfo1
        '
        Me.ReportInfo1.Border.BottomColor = System.Drawing.Color.Black
        Me.ReportInfo1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.ReportInfo1.Border.LeftColor = System.Drawing.Color.Black
        Me.ReportInfo1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.ReportInfo1.Border.RightColor = System.Drawing.Color.Black
        Me.ReportInfo1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.ReportInfo1.Border.TopColor = System.Drawing.Color.Black
        Me.ReportInfo1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.ReportInfo1.FormatString = "P�gina {PageNumber} de {PageCount}"
        Me.ReportInfo1.Height = 0.1875!
        Me.ReportInfo1.Left = 6.0!
        Me.ReportInfo1.Name = "ReportInfo1"
        Me.ReportInfo1.Style = "ddo-char-set: 1; font-weight: bold; "
        Me.ReportInfo1.Top = 0.0625!
        Me.ReportInfo1.Width = 1.3125!
        '
        'actrptVtasCostoVend
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.5!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.489583!
        Me.Sections.Add(Me.ReportHeader)
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.GroupHeader3)
        Me.Sections.Add(Me.GroupHeader2)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter2)
        Me.Sections.Add(Me.GroupFooter3)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter)
        Me.Sections.Add(Me.ReportFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" & _
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        CType(Me.txtNumeroFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFechaFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoGanancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalGanancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTipoFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTipoFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoAngecia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDesAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotTpFaMontoVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotTpFaMontoCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotTpFactMontoGanancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotTpFactMontoComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoVendedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombreVendedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotAgenMontoVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotAgenMontoCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotAgenMontoGanancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotAgenMontoComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotVendMontoVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotVendMontoCompra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotVendlMontoGanancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotVendMontoComision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptVerificarCostosVend_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        TextBox19.Text = Format(Now, "dd-MMM-yyyy")
        TextBox20.Text = Format(Now, "hh:mm:ss tt")

        Me.Document.Printer.PrinterName = ""
    End Sub

    Private Sub GroupHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format

    End Sub
End Class
