﻿Public Class IdMoneda
    Public Const MONEDA_LOCAL As String = "COR"
    Public Const MONEDA_EXTRANJERA As String = "USD"
End Class

Public Class IdEstadoCuadreCaja
    Public Const CUADRE_CAJA_GUARDADO As String = "G"
    Public Const CUADRE_CAJA_CERRADO As String = "C"
    Public Const CUADRE_CAJA_ANULADO As String = "A"
End Class
