Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptInventExistenciaProdCorUsd
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private Label10 As DataDynamics.ActiveReports.Label = Nothing
    Private Label12 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox12 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox13 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label16 As DataDynamics.ActiveReports.Label = Nothing
    Private Line As DataDynamics.ActiveReports.Line = Nothing
    Private Line1 As DataDynamics.ActiveReports.Line = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Label3 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private Label15 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox11 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox21 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox14 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptInventExistenciaProdCorUsd))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox6 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox7 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox8 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox9 = New DataDynamics.ActiveReports.TextBox
        Me.ReportHeader = New DataDynamics.ActiveReports.ReportHeader
        Me.ReportFooter = New DataDynamics.ActiveReports.ReportFooter
        Me.Label14 = New DataDynamics.ActiveReports.Label
        Me.TextBox14 = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.Label9 = New DataDynamics.ActiveReports.Label
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.Label12 = New DataDynamics.ActiveReports.Label
        Me.TextBox12 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox13 = New DataDynamics.ActiveReports.TextBox
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.Line = New DataDynamics.ActiveReports.Line
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.Label7 = New DataDynamics.ActiveReports.Label
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.TextBox = New DataDynamics.ActiveReports.TextBox
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label8 = New DataDynamics.ActiveReports.Label
        Me.TextBox11 = New DataDynamics.ActiveReports.TextBox
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox2, Me.TextBox3, Me.TextBox6, Me.TextBox7, Me.TextBox8, Me.TextBox9})
        Me.Detail.Height = 0.1666667!
        Me.Detail.Name = "Detail"
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "prodcodigo"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 0.0625!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Style = "font-weight: normal; font-size: 9pt"
        Me.TextBox2.Text = "TextBox2"
        Me.TextBox2.Top = 0.0!
        Me.TextBox2.Width = 0.625!
        '
        'TextBox3
        '
        Me.TextBox3.DataField = "proddescrip"
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 0.75!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Style = "font-weight: normal; font-size: 9pt"
        Me.TextBox3.Text = "TextBox3"
        Me.TextBox3.Top = 0.0!
        Me.TextBox3.Width = 3.6875!
        '
        'TextBox6
        '
        Me.TextBox6.DataField = "unidcodigo"
        Me.TextBox6.Height = 0.1875!
        Me.TextBox6.Left = 4.5!
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Style = "font-weight: normal; font-size: 9pt"
        Me.TextBox6.Text = "TextBox6"
        Me.TextBox6.Top = 0.0!
        Me.TextBox6.Width = 0.5625!
        '
        'TextBox7
        '
        Me.TextBox7.DataField = "cantidad"
        Me.TextBox7.Height = 0.1875!
        Me.TextBox7.Left = 5.1875!
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.OutputFormat = resources.GetString("TextBox7.OutputFormat")
        Me.TextBox7.Style = "text-align: right; font-weight: normal; font-size: 9pt"
        Me.TextBox7.Text = "TextBox7"
        Me.TextBox7.Top = 0.0!
        Me.TextBox7.Width = 0.6875!
        '
        'TextBox8
        '
        Me.TextBox8.DataField = "costo"
        Me.TextBox8.Height = 0.1875!
        Me.TextBox8.Left = 5.875!
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.OutputFormat = resources.GetString("TextBox8.OutputFormat")
        Me.TextBox8.Style = "text-align: right; font-weight: normal; font-size: 9pt"
        Me.TextBox8.Text = "TextBox8"
        Me.TextBox8.Top = 0.0!
        Me.TextBox8.Width = 0.6875!
        '
        'TextBox9
        '
        Me.TextBox9.DataField = "monto"
        Me.TextBox9.Height = 0.1875!
        Me.TextBox9.Left = 6.5625!
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat")
        Me.TextBox9.Style = "text-align: right; font-weight: normal; font-size: 9pt"
        Me.TextBox9.Text = "TextBox9"
        Me.TextBox9.Top = 0.0!
        Me.TextBox9.Width = 0.875!
        '
        'ReportHeader
        '
        Me.ReportHeader.Height = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label14, Me.TextBox14})
        Me.ReportFooter.Height = 0.3847222!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 3.0625!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-weight: bold; font-size: 9pt"
        Me.Label14.Text = "Totales del Reporte"
        Me.Label14.Top = 0.0!
        Me.Label14.Width = 1.25!
        '
        'TextBox14
        '
        Me.TextBox14.DataField = "monto"
        Me.TextBox14.Height = 0.1875!
        Me.TextBox14.Left = 6.4375!
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.OutputFormat = resources.GetString("TextBox14.OutputFormat")
        Me.TextBox14.Style = "text-align: right; font-weight: bold; font-size: 9pt"
        Me.TextBox14.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox14.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox14.Text = "TextBox14"
        Me.TextBox14.Top = 0.0!
        Me.TextBox14.Width = 0.875!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label9, Me.Label10, Me.Label12, Me.TextBox12, Me.TextBox13, Me.Label16, Me.Line, Me.Line1, Me.Label1, Me.Label3, Me.Label4, Me.Label5, Me.Label6, Me.Label7, Me.Label15})
        Me.PageHeader.Height = 1.34375!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label9
        '
        Me.Label9.Height = 0.2!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0.0625!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label9.Text = "INVENTARIO DE LOS PRODUCTOS"
        Me.Label9.Top = 0.25!
        Me.Label9.Width = 4.9375!
        '
        'Label10
        '
        Me.Label10.Height = 0.2!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 5.15!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label10.Text = "Fecha de Impresi�n"
        Me.Label10.Top = 0.05!
        Me.Label10.Width = 1.35!
        '
        'Label12
        '
        Me.Label12.Height = 0.2!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 5.15!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label12.Text = "Hora de Impresi�n"
        Me.Label12.Top = 0.25!
        Me.Label12.Width = 1.35!
        '
        'TextBox12
        '
        Me.TextBox12.Height = 0.2!
        Me.TextBox12.Left = 6.5!
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Style = "text-align: right"
        Me.TextBox12.Text = "TextBox12"
        Me.TextBox12.Top = 0.05!
        Me.TextBox12.Width = 0.9!
        '
        'TextBox13
        '
        Me.TextBox13.Height = 0.2!
        Me.TextBox13.Left = 6.5!
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Style = "text-align: right"
        Me.TextBox13.Text = "TextBox13"
        Me.TextBox13.Top = 0.25!
        Me.TextBox13.Width = 0.9!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0.0625!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label16.Text = "INVENTARIO DE CLASES CONSOLIDADO"
        Me.Label16.Top = 0.45!
        Me.Label16.Width = 2.75!
        '
        'Line
        '
        Me.Line.Height = 0.0!
        Me.Line.Left = 0.0625!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 1.0!
        Me.Line.Width = 7.375!
        Me.Line.X1 = 0.0625!
        Me.Line.X2 = 7.4375!
        Me.Line.Y1 = 1.0!
        Me.Line.Y2 = 1.0!
        '
        'Line1
        '
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.0625!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.3125!
        Me.Line1.Width = 7.375!
        Me.Line1.X1 = 0.0625!
        Me.Line1.X2 = 7.4375!
        Me.Line1.Y1 = 1.3125!
        Me.Line1.Y2 = 1.3125!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.0625!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 9pt"
        Me.Label1.Text = "C�digo"
        Me.Label1.Top = 1.0625!
        Me.Label1.Width = 0.625!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.75!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 9pt"
        Me.Label3.Text = "Descripi�n"
        Me.Label3.Top = 1.0625!
        Me.Label3.Width = 3.6875!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 4.5!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold; font-size: 9pt"
        Me.Label4.Text = "Unidad"
        Me.Label4.Top = 1.0625!
        Me.Label4.Width = 0.5625!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 5.3125!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "text-align: right; font-weight: bold; font-size: 9pt"
        Me.Label5.Text = "Cantidad"
        Me.Label5.Top = 1.0625!
        Me.Label5.Width = 0.5625!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 6.125!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: right; font-weight: bold; font-size: 9pt"
        Me.Label6.Text = "Costo"
        Me.Label6.Top = 1.0625!
        Me.Label6.Width = 0.4375!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 7.0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "text-align: right; font-weight: bold; font-size: 9pt"
        Me.Label7.Text = "Monto"
        Me.Label7.Top = 1.0625!
        Me.Label7.Width = 0.4375!
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0.0625!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label15.Text = "REPORTE GENERADO DEL INVENTARIO"
        Me.Label15.Top = 0.0625!
        Me.Label15.Width = 3.0!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.TextBox21})
        Me.PageFooter.Height = 0.7694445!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label13
        '
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 6.45!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label13.Text = "P�gina"
        Me.Label13.Top = 0.1!
        Me.Label13.Width = 0.5500001!
        '
        'TextBox21
        '
        Me.TextBox21.Height = 0.2!
        Me.TextBox21.Left = 6.985417!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: right"
        Me.TextBox21.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox21.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0.1!
        Me.TextBox21.Width = 0.3499999!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox, Me.Label, Me.TextBox1})
        Me.GroupHeader1.DataField = "AgenCodigo"
        Me.GroupHeader1.Height = 0.2597222!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'TextBox
        '
        Me.TextBox.DataField = "AgenCodigo"
        Me.TextBox.Height = 0.1875!
        Me.TextBox.Left = 0.6125!
        Me.TextBox.Name = "TextBox"
        Me.TextBox.Style = "font-weight: bold; font-size: 9pt"
        Me.TextBox.Text = "TextBox"
        Me.TextBox.Top = 0.0!
        Me.TextBox.Width = 0.375!
        '
        'Label
        '
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.05!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-weight: bold; font-size: 9pt"
        Me.Label.Text = "Agencia"
        Me.Label.Top = 0.0!
        Me.Label.Width = 0.5625!
        '
        'TextBox1
        '
        Me.TextBox1.DataField = "AgenDescrip"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 0.9875!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Style = "font-weight: bold; font-size: 9pt"
        Me.TextBox1.Text = "TextBox1"
        Me.TextBox1.Top = 0.0!
        Me.TextBox1.Width = 2.0!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label8, Me.TextBox11})
        Me.GroupFooter1.Height = 0.21875!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 3.0!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-weight: bold; font-size: 9pt"
        Me.Label8.Text = "Totales de la Agencia"
        Me.Label8.Top = 0.0!
        Me.Label8.Width = 1.3125!
        '
        'TextBox11
        '
        Me.TextBox11.DataField = "monto"
        Me.TextBox11.Height = 0.1875!
        Me.TextBox11.Left = 6.75!
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.OutputFormat = resources.GetString("TextBox11.OutputFormat")
        Me.TextBox11.Style = "text-align: right; font-weight: bold; font-size: 9pt"
        Me.TextBox11.SummaryGroup = "GroupHeader1"
        Me.TextBox11.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox11.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox11.Text = "TextBox11"
        Me.TextBox11.Top = 0.0!
        Me.TextBox11.Width = 0.6875!
        '
        'actrptInventExistenciaProdCorUsd
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.5!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.489583!
        Me.Sections.Add(Me.ReportHeader)
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter)
        Me.Sections.Add(Me.ReportFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" & _
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptInventExistenciaProdCorUsd_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        'If intRptInventario = 7 Or intRptInventario = 8 Then
        'Me.Line2.Visible = False
        Me.Label6.Visible = False
        Me.Label7.Visible = False
        Me.Label8.Visible = False
        'Me.Label11.Visible = False
        Me.TextBox8.Visible = False
        Me.TextBox9.Visible = False
        'Me.TextBox10.Visible = False
        Me.TextBox11.Visible = False
        'End If
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        TextBox12.Text = Format(Now, "dd-MMM-yyyy")
        TextBox13.Text = Format(Now, "hh:mm:ss tt")
        Me.Label16.Text = strFechaReporte
        Select Case intRptInventario
            Case 5 : Me.Label9.Text = "Reporte de Productos en COR"
            Case 6 : Me.Label9.Text = "Reporte de Productos en USD"
            Case 7 : Me.Label9.Text = "Listado de Existencia"
            Case 23 : Me.Label9.Text = "Listado de Existencia ordenado por productos"
            Case 8 : Me.Label9.Text = "Productos con Cantidad Menor o Igual a Requerido"
        End Select

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actrptInventExistenciaProdCorUsd_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

    End Sub

End Class
