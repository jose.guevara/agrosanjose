﻿Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Collections.Generic
Imports DevComponents.Editors.DateTimeAdv
Imports DevComponents.DotNetBar.Controls

Public Class frmCuadreCajaInicial
    Private sNombreMetodo As String = String.Empty
    Private objError As SEError = New SEError()
    Private sNombreClase As String = "frmCuadreCajaInicial"
    Private lnNumeroArqueo As Integer = 0
    Private lsNumeroArqueo As String = String.Empty

    Private Sub frmCuadreCajaInicial_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Me.Top = 0
            'Me.Left = 0
            dtiFechaInicioArqueo.Value = DateTime.Today()
            dtiFechaFinArqueo.Value = DateTime.Today()

            UbicarAgencia(lngRegUsuario)
            If (lngRegAgencia = 0) Then
                Dim frmNew As New frmEscogerAgencia
                frmNew.ShowDialog()
                If lngRegAgencia = 0 Then
                    MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
                End If
            End If
            lbltxtUsuario.Text = strNombreUsuario

            LlenaGridDetalleCuadre(dgxDetalleCordobas)
            'LlenaGridDetalleCuadre(dgxDetalleDolares)
            LlenaGridDetalleCuadreDolares(dgxDetalleDolares)

            ObtieneNumeroArqueo()
            lbltxtNumeroArqueo.Text = lsNumeroArqueo
        Catch ex As Exception
            MsgBox("Error al cargar valores iniciales", MsgBoxStyle.Critical, "Cuadre Caja Inicial")
        End Try
    End Sub

    Private Sub dtiFechaInicioArqueo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub LlenaGridDetalleCuadre(ByVal dgxObjoto As DevComponents.DotNetBar.Controls.DataGridViewX)
        dgxObjoto.Rows.Add("500", 0, 0)
        dgxObjoto.Rows.Add("200", 0, 0)
        dgxObjoto.Rows.Add("100", 0, 0)
        dgxObjoto.Rows.Add("50", 0, 0)
        dgxObjoto.Rows.Add("20", 0, 0)
        dgxObjoto.Rows.Add("10", 0, 0)
        dgxObjoto.Rows.Add("5", 0, 0)
        dgxObjoto.Rows.Add("1", 0, 0)
        dgxObjoto.Rows.Add("Total", 0, 0)
        dgxObjoto.Rows.Add("Total Córdobas", 0, 0)
        dgxObjoto.AllowUserToDeleteRows = False
        dgxObjoto.AllowUserToAddRows = False
        dgxObjoto.AllowDrop = False
        dgxObjoto.AllowUserToOrderColumns = False
        If (dgxObjoto.Rows.Count >= 8) Then
            If dgxObjoto.Rows(8).Cells.Count >= 1 Then
                dgxObjoto.Rows(8).Cells(1).ReadOnly = True
            End If
        End If
        If (dgxObjoto.Rows.Count >= 9) Then
            If dgxObjoto.Rows(9).Cells.Count >= 1 Then
                dgxObjoto.Rows(9).Cells(1).ReadOnly = True
            End If
        End If

    End Sub
    Private Sub LlenaGridDetalleCuadreDolares(ByVal dgxObjoto As DevComponents.DotNetBar.Controls.DataGridViewX)
        dgxObjoto.Rows.Add("100", 0, 0)
        dgxObjoto.Rows.Add("50", 0, 0)
        dgxObjoto.Rows.Add("20", 0, 0)
        dgxObjoto.Rows.Add("10", 0, 0)
        dgxObjoto.Rows.Add("5", 0, 0)
        dgxObjoto.Rows.Add("1", 0, 0)
        dgxObjoto.Rows.Add("0.50", 0, 0)
        dgxObjoto.Rows.Add("0.10", 0, 0)
        dgxObjoto.Rows.Add("Total", 0, 0)
        dgxObjoto.Rows.Add("Total Córdobas", 0, 0)
        dgxObjoto.AllowUserToDeleteRows = False
        dgxObjoto.AllowUserToAddRows = False
        dgxObjoto.AllowDrop = False
        dgxObjoto.AllowUserToOrderColumns = False
        If (dgxObjoto.Rows.Count >= 8) Then
            If dgxObjoto.Rows(8).Cells.Count >= 1 Then
                dgxObjoto.Rows(8).Cells(1).ReadOnly = True
            End If
        End If
        If (dgxObjoto.Rows.Count >= 9) Then
            If dgxObjoto.Rows(9).Cells.Count >= 1 Then
                dgxObjoto.Rows(9).Cells(1).ReadOnly = True
            End If
        End If

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub UltraGroupBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub dgxDetalleCordobas_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgxDetalleCordobas.CellValueChanged
        Dim nTotalMonto As Double = 0
        Dim nMonto As Double = 0
        Dim nCantidad As Integer = 0
        Dim nTotalCantidad As Integer = 0
        If e.ColumnIndex = 1 Then
            If Not dgxDetalleCordobas.CurrentRow Is Nothing Then
                If dgxDetalleCordobas.CurrentRow.Cells.Count >= 2 Then
                    If ConvierteAInt(dgxDetalleCordobas.CurrentRow.Cells(1).Value) > 0 Then
                        If ConvierteAInt(dgxDetalleCordobas.CurrentRow.Cells(0).Value) <> 0 And ConvierteAInt(dgxDetalleCordobas.CurrentRow.Cells(1).Value) <> 0 Then
                            dgxDetalleCordobas.CurrentRow.Cells(2).Value = Format(ConvierteADouble(dgxDetalleCordobas.CurrentRow.Cells(0).Value) * ConvierteADouble(dgxDetalleCordobas.CurrentRow.Cells(1).Value), "#,##0.#0")
                        End If

                    Else
                        dgxDetalleCordobas.CurrentRow.Cells(2).Value = 0
                    End If
                    nTotalMonto = 0
                    nMonto = 0
                    nCantidad = 0
                    nTotalCantidad = 0
                    For i As Integer = 0 To dgxDetalleCordobas.Rows.Count - 3
                        nMonto = 0
                        nMonto = ConvierteADouble(dgxDetalleCordobas.Rows(i).Cells(2).Value)
                        nCantidad = 0
                        nCantidad = ConvierteAInt(dgxDetalleCordobas.Rows(i).Cells(1).Value)
                        nTotalMonto = nTotalMonto + nMonto
                        nTotalCantidad = nTotalCantidad + nCantidad
                    Next
                    If dgxDetalleCordobas.Rows.Count >= 8 Then
                        If dgxDetalleCordobas.Rows(8).Cells.Count >= 1 Then
                            If Not dgxDetalleCordobas.Rows(8).Cells(2) Is Nothing Then
                                dgxDetalleCordobas.Rows(8).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                            End If
                            If Not dgxDetalleCordobas.Rows(8).Cells(1) Is Nothing Then
                                dgxDetalleCordobas.Rows(8).Cells(1).Value = nTotalCantidad
                            End If
                        End If
                    End If
                    If dgxDetalleCordobas.Rows.Count >= 9 Then
                        If dgxDetalleCordobas.Rows(9).Cells.Count >= 1 Then
                            If Not dgxDetalleCordobas.Rows(9).Cells(2) Is Nothing Then
                                dgxDetalleCordobas.Rows(9).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                            End If
                            If Not dgxDetalleCordobas.Rows(9).Cells(1) Is Nothing Then
                                dgxDetalleCordobas.Rows(9).Cells(1).Value = nTotalCantidad
                            End If
                        End If
                    End If
                    ' dgxDetalleCordobas.Rows(8).Cells(2).Value = nTotalMonto
                    ' dgxDetalleCordobas.Rows(8).Cells(1).Value = nTotalCantidad
                End If
            End If
        End If
    End Sub
    Private Sub ObtieneNumeroArqueo()
        Try
            lsNumeroArqueo = RNArqueoCaja.ObtenerNumeroArqueo(lngRegAgencia)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub
    Private Sub ValidaNumeroFactura(ByVal psNumeroFactura As String)
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Try
            If psNumeroFactura.Trim().Length > 0 Then
                'lsFechaInicio = Format(dtiFechaInicioArqueo.Value.Day, "00") & "/" & Format(dtiFechaInicioArqueo.Value.Month, "00") & "/" & Format(dtiFechaInicioArqueo.Value.Year, "0000")
                'lsFechaFin = Format(dtiFechaFinArqueo.Value.Day, "00") & "/" & Format(dtiFechaFinArqueo.Value.Month, "00") & "/" & Format(dtiFechaFinArqueo.Value.Year, "0000")

                lsFechaInicio = Format(dtiFechaInicioArqueo.Value.Year, "0000") & "-" & Format(dtiFechaInicioArqueo.Value.Month, "00") & "-" & Format(dtiFechaInicioArqueo.Value.Day, "00")
                lsFechaFin = Format(dtiFechaFinArqueo.Value.Year, "0000") & "-" & Format(dtiFechaFinArqueo.Value.Month, "00") & "-" & Format(dtiFechaFinArqueo.Value.Day, "00")

                Resultado = RNFacturas.VerificaNumeroFactura(psNumeroFactura, lngRegUsuario, lsFechaInicio, lsFechaFin)
                If (Resultado(0) = 1) Then
                    MsgBox(Resultado(1), MsgBoxStyle.Critical, "Valida Numero Factura")
                End If
                If (Resultado(2) = 1) Then
                    MsgBox(Resultado(3), MsgBoxStyle.Critical, "Valida Numero Factura")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub
    Private Sub ValidaNumeroRecibo(ByVal psNumeroRecibo As String)
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Try
            If psNumeroRecibo.Trim().Length > 0 Then
                'lsFechaInicio = Format(dtiFechaInicioArqueo.Value.Day, "00") & "/" & Format(dtiFechaInicioArqueo.Value.Month, "00") & "/" & Format(dtiFechaInicioArqueo.Value.Year, "0000")
                'lsFechaFin = Format(dtiFechaFinArqueo.Value.Day, "00") & "/" & Format(dtiFechaFinArqueo.Value.Month, "00") & "/" & Format(dtiFechaFinArqueo.Value.Year, "0000")

                lsFechaInicio = Format(dtiFechaInicioArqueo.Value.Year, "0000") & "-" & Format(dtiFechaInicioArqueo.Value.Month, "00") & "-" & Format(dtiFechaInicioArqueo.Value.Day, "00")
                lsFechaFin = Format(dtiFechaFinArqueo.Value.Year, "0000") & "-" & Format(dtiFechaFinArqueo.Value.Month, "00") & "-" & Format(dtiFechaFinArqueo.Value.Day, "00")

                Resultado = RNRecibos.VerificaNumeroRecibo(psNumeroRecibo, lngRegUsuario, lsFechaInicio, lsFechaFin)
                If (Resultado(0) = 1) Then
                    MsgBox(Resultado(1), MsgBoxStyle.Critical, "Valida Numero Recibo")
                End If
                If (Resultado(2) = 1) Then
                    MsgBox(Resultado(3), MsgBoxStyle.Critical, "Valida Numero Recibo")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub
    Private Sub dgxDetalleDolares_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgxDetalleDolares.CellValueChanged
        Dim nTotalMonto As Double = 0
        Dim nMonto As Double = 0
        Dim nCantidad As Integer = 0
        Dim nTotalCantidad As Integer = 0
        If e.ColumnIndex = 1 Then
            If Not dgxDetalleDolares.CurrentRow Is Nothing Then
                If dgxDetalleDolares.CurrentRow.Cells.Count >= 2 Then
                    If ConvierteAInt(dgxDetalleDolares.CurrentRow.Cells(1).Value) > 0 Then
                        If ConvierteAInt(dgxDetalleDolares.CurrentRow.Cells(0).Value) <> 0 And ConvierteAInt(dgxDetalleDolares.CurrentRow.Cells(1).Value) <> 0 Then
                            dgxDetalleDolares.CurrentRow.Cells(2).Value = Format(ConvierteADouble(dgxDetalleDolares.CurrentRow.Cells(0).Value) * ConvierteADouble(dgxDetalleDolares.CurrentRow.Cells(1).Value), "#,##0.#0")
                        End If

                    Else
                        dgxDetalleDolares.CurrentRow.Cells(2).Value = 0
                    End If
                    nTotalMonto = 0
                    nMonto = 0
                    nCantidad = 0
                    nTotalCantidad = 0
                    For i As Integer = 0 To dgxDetalleDolares.Rows.Count - 3
                        nMonto = 0
                        nMonto = ConvierteADouble(dgxDetalleDolares.Rows(i).Cells(2).Value)
                        nCantidad = 0
                        nCantidad = ConvierteAInt(dgxDetalleDolares.Rows(i).Cells(1).Value)
                        nTotalMonto = nTotalMonto + nMonto
                        nTotalCantidad = nTotalCantidad + nCantidad
                    Next
                    If dgxDetalleDolares.Rows.Count >= 8 Then
                        If dgxDetalleDolares.Rows(8).Cells.Count >= 1 Then
                            If Not dgxDetalleDolares.Rows(8).Cells(2) Is Nothing Then
                                dgxDetalleDolares.Rows(8).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                            End If
                            If Not dgxDetalleDolares.Rows(8).Cells(1) Is Nothing Then
                                dgxDetalleDolares.Rows(8).Cells(1).Value = nTotalCantidad
                            End If
                        End If
                    End If
                    If dgxDetalleDolares.Rows.Count >= 9 Then
                        If dgxDetalleDolares.Rows(9).Cells.Count >= 1 Then
                            If Not dgxDetalleDolares.Rows(9).Cells(2) Is Nothing Then
                                dgxDetalleDolares.Rows(9).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                            End If
                            If Not dgxDetalleDolares.Rows(9).Cells(1) Is Nothing Then
                                dgxDetalleDolares.Rows(9).Cells(1).Value = nTotalCantidad
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cmdSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalir.Click
        Me.Close()
    End Sub

    Private Sub txtExNumeroFacturaInicial_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroFacturaInicial.Leave
        ValidaNumeroFactura(txtExNumeroFacturaInicial.Text)
    End Sub


    Private Sub txtExNumeroFacturaFinal_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroFacturaFinal.Leave
        ValidaNumeroFactura(txtExNumeroFacturaFinal.Text)
    End Sub

    Private Sub txtExNumeroReciboInicial_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroReciboInicial.Leave
        ValidaNumeroRecibo(txtExNumeroReciboInicial.Text)
    End Sub

    Private Sub txtExNumeroReciboFinal_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroReciboFinal.Leave
        ValidaNumeroRecibo(txtExNumeroReciboFinal.Text)
    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Try
            IngresaCuadreCajaCompleto()
        Catch ex As Exception
            MsgBox("Error Guardando Cambios ", MsgBoxStyle.Critical, "Guardando Cambios Cierre")
        End Try
    End Sub
    Private Sub IngresaCuadreCajaCompleto()
        Dim objMaestroCuadreCaja As SECuadreCajaEncabezado
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        'Dim objReqCampo As Object = New Object(3) {}
        Dim objReqCampo As Object = Nothing
        Try
            objReqCampo = ValidarCampoRequerido()

            If objReqCampo(2) = True Then
                MsgBox(objReqCampo(1), MsgBoxStyle.Information, "Validación de datos sensibles")
                Return
            End If

            objMaestroCuadreCaja = New SECuadreCajaEncabezado()
            objMaestroCuadreCaja.IdMoneda = IdMoneda.MONEDA_LOCAL
            objMaestroCuadreCaja.IdEstado = IdEstadoCuadreCaja.CUADRE_CAJA_GUARDADO
            objMaestroCuadreCaja.IdSucursal = lngRegAgencia
            objMaestroCuadreCaja.FacturaContadoInicial = txtExNumeroFacturaInicial.Text
            objMaestroCuadreCaja.FacturaContadoFinal = txtExNumeroFacturaFinal.Text
            objMaestroCuadreCaja.FacturaCreditoInicial = String.Empty
            objMaestroCuadreCaja.FacturaCreditoFinal = String.Empty
            objMaestroCuadreCaja.ReciboInicialCredito = txtExNumeroReciboInicial.Text
            objMaestroCuadreCaja.ReciboFinalCredito = txtExNumeroReciboFinal.Text
            objMaestroCuadreCaja.ReciboInicialDebito = txtExNumeroReciboInicial.Text
            objMaestroCuadreCaja.ReciboFinalDebito = txtExNumeroReciboFinal.Text

            lsFechaInicio = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            lsFechaFin = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)
            lstDetalleCuadreCaja = RNArqueoCaja.ObtieneFacturasYRecibosARealizarCierre(lngRegUsuario, objMaestroCuadreCaja.FacturaContadoInicial, _
                                                                                       objMaestroCuadreCaja.FacturaContadoFinal, objMaestroCuadreCaja.ReciboInicialCredito, _
                                                                                       objMaestroCuadreCaja.ReciboFinalCredito, lsFechaInicio, lsFechaFin)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub
    Private Function ValidarCampoRequerido() As Object()

        Dim objReqCampo As Object = New Object(3) {}
        Dim sNombreControlReq As String = String.Empty
        Dim ReqCampo As Boolean
        objReqCampo(0) = 0  'No hay campos que necesiten informacion
        objReqCampo(1) = ""
        objReqCampo(2) = False
        For Each controlGB As Control In Me.ugbIngresoDatos.Controls
            If (TypeOf (controlGB) Is DateTimeInput) Then ' Verifico que el control sea un textbox
                If CType(controlGB, DateTimeInput).AccessibleDescription = "Req" Then ' Le cambio el valor a la propiedad
                    If CType(controlGB, DateTimeInput).Value.ToString() = String.Empty Then
                        ' sNombreControlReq = sNombreControlReq & ", " & Mid$(CType(controlGB, DateTimeInput).Name.ToString(), 4, CType(controlGB, DateTimeInput).Name.Length)
                        If sNombreControlReq.Length <= 0 Then
                            sNombreControlReq = CType(controlGB, DateTimeInput).AccessibleName
                        Else
                            sNombreControlReq = sNombreControlReq & ", " & CType(controlGB, DateTimeInput).AccessibleName
                        End If

                        'MsgBox("Mensaje de pruebas " & CType(controlGB, UltraTextEditor).Name.ToString(), MsgBoxStyle.Information, "Campo requerido")
                        ReqCampo = True
                        objReqCampo(0) = 1
                        objReqCampo(1) = "La siguiente información es requerida: " & sNombreControlReq
                        objReqCampo(2) = True
                    End If
                End If
            End If
            If (TypeOf (controlGB) Is TextBoxX) Then ' Verifico que el control sea un textbox
                If CType(controlGB, TextBoxX).AccessibleDescription = "Req" Then ' Le cambio el valor a la propiedad
                    If CType(controlGB, TextBoxX).Text = String.Empty Or CType(controlGB, TextBoxX).Text = "0" Then
                        ' sNombreControlReq = sNombreControlReq & ", " & Mid$(CType(controlGB, DateTimeInput).Name.ToString(), 4, CType(controlGB, DateTimeInput).Name.Length)
                        If sNombreControlReq.Length <= 0 Then
                            sNombreControlReq = CType(controlGB, TextBoxX).AccessibleName
                        Else
                            sNombreControlReq = sNombreControlReq & ", " & CType(controlGB, TextBoxX).AccessibleName
                        End If

                        'MsgBox("Mensaje de pruebas " & CType(controlGB, UltraTextEditor).Name.ToString(), MsgBoxStyle.Information, "Campo requerido")
                        ReqCampo = True
                        objReqCampo(0) = 1
                        objReqCampo(1) = "La siguiente información es requerida: " & sNombreControlReq
                        objReqCampo(2) = True
                    End If
                End If
            End If
        Next
        'If objReqCampo(0) = 1 Then
        '    MsgBox("La siguiente información es requerida: " & sNombreControlReq, MsgBoxStyle.Information, "Validación de datos sensibles")
        'End If

        Return objReqCampo
    End Function
End Class