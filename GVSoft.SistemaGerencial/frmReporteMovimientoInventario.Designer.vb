<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteMovimientoInventario
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReporteMovimientoInventario))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdMostrarDatos = New DevComponents.DotNetBar.ButtonX
        Me.Label15 = New System.Windows.Forms.Label
        Me.ddlSerie = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.txtNumeroBuscar = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label14 = New System.Windows.Forms.Label
        Me.cmbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.dtpFechaFinal = New System.Windows.Forms.DateTimePicker
        Me.dtpFechaInicial = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvTraslados = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.cmdMostrarSeleccion = New DevComponents.DotNetBar.ButtonX
        Me.cmdMostrarTodos = New DevComponents.DotNetBar.ButtonX
        Me.GroupBox2.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvTraslados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdMostrarDatos)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.ddlSerie)
        Me.GroupBox2.Controls.Add(Me.txtNumeroBuscar)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.cmbAgencias)
        Me.GroupBox2.Controls.Add(Me.dtpFechaFinal)
        Me.GroupBox2.Controls.Add(Me.dtpFechaInicial)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 61)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(378, 117)
        Me.GroupBox2.TabIndex = 46
        Me.GroupBox2.TabStop = False
        '
        'cmdMostrarDatos
        '
        Me.cmdMostrarDatos.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdMostrarDatos.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdMostrarDatos.Location = New System.Drawing.Point(287, 77)
        Me.cmdMostrarDatos.Name = "cmdMostrarDatos"
        Me.cmdMostrarDatos.Size = New System.Drawing.Size(78, 33)
        Me.cmdMostrarDatos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdMostrarDatos.TabIndex = 58
        Me.cmdMostrarDatos.Text = "Mostrar Datos"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label15.Location = New System.Drawing.Point(8, 86)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(36, 13)
        Me.Label15.TabIndex = 57
        Me.Label15.Text = "Serie"
        '
        'ddlSerie
        '
        Me.ddlSerie.DisplayMember = "Text"
        Me.ddlSerie.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ddlSerie.FormattingEnabled = True
        Me.ddlSerie.ItemHeight = 14
        Me.ddlSerie.Location = New System.Drawing.Point(43, 82)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(52, 20)
        Me.ddlSerie.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ddlSerie.TabIndex = 54
        '
        'txtNumeroBuscar
        '
        Me.txtNumeroBuscar.AccessibleDescription = ""
        Me.txtNumeroBuscar.AccessibleName = "Num"
        Me.txtNumeroBuscar.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtNumeroBuscar.Border.Class = "TextBoxBorder"
        Me.txtNumeroBuscar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNumeroBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroBuscar.Location = New System.Drawing.Point(101, 82)
        Me.txtNumeroBuscar.Name = "txtNumeroBuscar"
        Me.txtNumeroBuscar.Size = New System.Drawing.Size(180, 20)
        Me.txtNumeroBuscar.TabIndex = 55
        Me.txtNumeroBuscar.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumeroBuscar.WatermarkText = "Numero a Buscar <Enter>"
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label14.Location = New System.Drawing.Point(3, 47)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(79, 27)
        Me.Label14.TabIndex = 56
        Me.Label14.Text = "Agencia Destino"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmbAgencias
        '
        Me.cmbAgencias.DisplayMember = "Text"
        Me.cmbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbAgencias.FormattingEnabled = True
        Me.cmbAgencias.ItemHeight = 14
        Me.cmbAgencias.Location = New System.Drawing.Point(85, 52)
        Me.cmbAgencias.Name = "cmbAgencias"
        Me.cmbAgencias.Size = New System.Drawing.Size(282, 20)
        Me.cmbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbAgencias.TabIndex = 53
        '
        'dtpFechaFinal
        '
        Me.dtpFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinal.Location = New System.Drawing.Point(256, 22)
        Me.dtpFechaFinal.Name = "dtpFechaFinal"
        Me.dtpFechaFinal.Size = New System.Drawing.Size(112, 20)
        Me.dtpFechaFinal.TabIndex = 48
        '
        'dtpFechaInicial
        '
        Me.dtpFechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicial.Location = New System.Drawing.Point(72, 22)
        Me.dtpFechaInicial.Name = "dtpFechaInicial"
        Me.dtpFechaInicial.Size = New System.Drawing.Size(112, 20)
        Me.dtpFechaInicial.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(200, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "FecFinal"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "FecInicial"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.cmdExportar)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(750, 55)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 47
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(75, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmdExportar
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.FontBold = True
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.FontBold = True
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvTraslados)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(2, 184)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(744, 349)
        Me.GroupBox1.TabIndex = 48
        Me.GroupBox1.TabStop = False
        '
        'dgvTraslados
        '
        Me.dgvTraslados.AllowUserToAddRows = False
        Me.dgvTraslados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvTraslados.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvTraslados.BackgroundColor = System.Drawing.SystemColors.Info
        Me.dgvTraslados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.DarkGreen
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTraslados.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvTraslados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Maroon
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Maroon
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTraslados.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvTraslados.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvTraslados.Location = New System.Drawing.Point(6, 14)
        Me.dgvTraslados.Name = "dgvTraslados"
        Me.dgvTraslados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTraslados.Size = New System.Drawing.Size(728, 325)
        Me.dgvTraslados.TabIndex = 47
        '
        'cmdMostrarSeleccion
        '
        Me.cmdMostrarSeleccion.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdMostrarSeleccion.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdMostrarSeleccion.Location = New System.Drawing.Point(384, 67)
        Me.cmdMostrarSeleccion.Name = "cmdMostrarSeleccion"
        Me.cmdMostrarSeleccion.Size = New System.Drawing.Size(78, 53)
        Me.cmdMostrarSeleccion.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdMostrarSeleccion.TabIndex = 46
        Me.cmdMostrarSeleccion.Text = "Mostrar Seleccionado"
        '
        'cmdMostrarTodos
        '
        Me.cmdMostrarTodos.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdMostrarTodos.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdMostrarTodos.Location = New System.Drawing.Point(384, 126)
        Me.cmdMostrarTodos.Name = "cmdMostrarTodos"
        Me.cmdMostrarTodos.Size = New System.Drawing.Size(78, 53)
        Me.cmdMostrarTodos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdMostrarTodos.TabIndex = 49
        Me.cmdMostrarTodos.Text = "Mostrar Todos"
        '
        'frmReporteMovimientoInventario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(750, 547)
        Me.Controls.Add(Me.cmdMostrarTodos)
        Me.Controls.Add(Me.cmdMostrarSeleccion)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReporteMovimientoInventario"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte Traslados de Inventario"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvTraslados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpFechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ddlSerie As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents txtNumeroBuscar As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cmbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvTraslados As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents cmdMostrarSeleccion As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdMostrarTodos As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdMostrarDatos As DevComponents.DotNetBar.ButtonX
End Class
