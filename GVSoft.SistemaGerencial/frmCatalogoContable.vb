Imports System.Data
Imports System.Data.SqlClient
Imports DevComponents.DotNetBar
' Imports CrystalDecisions.CrystalReports.Engine
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmCatalogoContable
    Dim jackvalida As New Valida
    ' Dim jackmensaje As New MegasysJack.Mensaje
    Dim jackcrystal As New crystal_jack
    Dim jackAsientos As New Asientos
    Public Shared nueva_cuenta As Boolean
    Dim jackconsulta As New ConsultasDB
    Friend WithEvents balloonTipFocus As DevComponents.DotNetBar.BalloonTip
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "frmCatalogoContable"

    Private Sub ToolbarB_BuscarCuenta_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_BuscarCuenta.Click
        Try
            buscar_cuenta()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error al tratar de eliminar " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub EliminarRegistros()
        Dim intResp As Integer = 0
        Try
            intResp = MsgBox("�Desea eliminar la cuenta?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Catalogo contable")
            If intResp = 6 Then
                If TextB_CuentaContable.Text.Trim().Length >= 2 Then
                    RNCuentaContable.EliminarCuentaContable(TextB_CuentaContable.Text.Trim())
                End If
                MessageBoxEx.Show("Registro eliminado exitosamente ", " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Information)
                limpia_detalle_cuenta()
1:
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al tratar de eliminar " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Frm_Catalogo_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '  System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-NI")
        Dim dTOS_moneda, dtos_tipocuenta As New DataTable
        Dim datatable_ComboB_codmoneda_TabBusquedas, datatable_ComboB_Tipo_TabBusquedas, datatable_ComboB_categoria As DataTable
        Dim dTOS_moneda_detalle, dtos_tipocuenta_detalle, datatable_ComboB_categoria_new As New DataTable
        Dim datatable_ComboB_codmoneda_Tabdetalle, datatable_ComboB_Tipo_Tabdetalle As DataTable
        Try
            TextB_CuentaContable.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
            TextB_CuentaContable.AutoCompleteMode = AutoCompleteMode.Suggest
            TextB_CuentaContable.AutoCompleteSource = AutoCompleteSource.CustomSource

            TextB_cdgocuenta_Tabdetalle.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
            TextB_cdgocuenta_Tabdetalle.AutoCompleteMode = AutoCompleteMode.Suggest
            TextB_cdgocuenta_Tabdetalle.AutoCompleteSource = AutoCompleteSource.CustomSource


            datatable_ComboB_Tipo_TabBusquedas = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='tipo_cuenta' order by ordenvariable ", dtos_tipocuenta, True)
            jackconsulta.carga_combo_DN(ComboB_Tipo_TabBusquedas, datatable_ComboB_Tipo_TabBusquedas)
            datatable_ComboB_codmoneda_TabBusquedas = jackconsulta.Retornadatatable("select descmoneda,codmoneda from MtipoMoneda ", dTOS_moneda, True)
            jackconsulta.carga_combo_DN(ComboB_codmoneda_TabBusquedas, datatable_ComboB_codmoneda_TabBusquedas)

            datatable_ComboB_Tipo_Tabdetalle = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='tipo_cuenta' order by ordenvariable ", dtos_tipocuenta_detalle, True)
            jackconsulta.carga_combo_DN(ComboB_Tipo_Tabdetalle, datatable_ComboB_Tipo_Tabdetalle)
            datatable_ComboB_codmoneda_Tabdetalle = jackconsulta.Retornadatatable("select descmoneda,codmoneda from MtipoMoneda ", dTOS_moneda_detalle, True)
            jackconsulta.carga_combo_DN(ComboB_codmoneda_Tabdetalle, datatable_ComboB_codmoneda_Tabdetalle)

            datatable_ComboB_categoria = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='categoria_cuenta' order by ordenvariable ", datatable_ComboB_categoria_new, True)
            jackconsulta.carga_combo_DN(ComboB_categoria, datatable_ComboB_categoria)

            ComboB_Tipo_TabBusquedas.SelectedValue = -1
            ComboB_codmoneda_TabBusquedas.SelectedValue = 1

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Load " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#Region "Tab B�squedas"

    Sub buscar_cuenta()
        Dim sql_cuenta As String = "select cdgocuenta 'Cuenta',desccuenta 'Nombre Cuenta', (case tipo when '1' then 'Mayor' when '2' then 'Detalle' when '' then 'Sin Especificar' end )  'Tipo Cuenta',( case codmoneda when '1' then 'Nacional' when '2' then 'Extranjera' when '-1' then 'Sin-Especificar' when '' then 'Sin-Especificar' end) 'Moneda'  from  catalogoconta"
        Dim where As Boolean
        Try

            'Filtro C�digo de Cuenta
            If TextB_CuentaContable.Text <> "" Then
                where = True
                sql_cuenta += " where cdgocuenta like '" & TextB_CuentaContable.Text.Trim & "%'"
            End If
            'Filtro Descripci�n de la Cuenta
            If TextB_Desccuenta.Text <> "" Then
                If where = True Then
                    sql_cuenta += " and "
                Else
                    sql_cuenta += " where "
                    where = True
                End If
                sql_cuenta += " desccuenta like '" & TextB_Desccuenta.Text.Trim & "%'"
            End If
            'Filtro tipo de Cuenta

            If ComboB_Tipo_TabBusquedas.SelectedValue <> -1 And ComboB_Tipo_TabBusquedas.Text <> String.Empty Then
                If where = True Then
                    sql_cuenta += " and "
                Else
                    sql_cuenta += " where "
                    where = True
                End If
                sql_cuenta += " tipo=" & ComboB_Tipo_TabBusquedas.SelectedValue
            End If

            'Filtro Moneda de la Cuenta
            If ComboB_codmoneda_TabBusquedas.Text <> "" And ComboB_codmoneda_TabBusquedas.SelectedValue <> -1 Then
                If where = True Then
                    sql_cuenta += " and "
                Else
                    sql_cuenta += " where "
                    where = True
                End If
                sql_cuenta += " codmoneda = " & ComboB_codmoneda_TabBusquedas.SelectedValue
            End If


            Me.GridP_Catalogo.Show()
            Dim datos_cantidad As New DataTable
            datos_cantidad = jackconsulta.cargaGrid(sql_cuenta, True)
            If datos_cantidad.Rows.Count > 0 Then
                GridP_Catalogo.DataSource = datos_cantidad

            Else
                GridP_Catalogo.Hide()
                'Dim msg As DevComponents.DotNetBar.Balloon = New DevComponents.DotNetBar.Balloon()

                'msg.Style = eBallonStyle.Alert
                '' msg.CaptionImage = CType(BalloonTip_catalogo.CaptionImage.Clone(), Image)
                'msg.CaptionText = "Buscar Cuenta"
                'msg.Text = "No se encontraron Coincidencias"
                'msg.AlertAnimation = eAlertAnimation.TopToBottom
                'msg.AutoClose = True
                'msg.AutoCloseTimeOut = 8
                'msg.AutoResize()
                'msg.Owner = Me
                'msg.Show(ToolbarB_BuscarCuenta, False)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Load " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub GridP_Catalogo_Click(sender As Object, e As System.EventArgs) Handles GridP_Catalogo.Click
        Dim row As DataGridViewRow = Nothing
        Try
            row = GridP_Catalogo.CurrentRow
            If row Is Nothing Then
                Exit Sub
            End If
            lee_cuenta(row.Cells("Cuenta").Value)
            SuperTabControl1.SelectedTabIndex = 1
            Label_reaumensaldos.Text = "Resumen de Saldos Al " & jackconsulta.DameCampo_UltimoCierre(True, False, "fecha_fin")
            'TextB_cdgocuenta_Tabdetalle.ReadOnly = True
            TextB_cdgocuenta_Tabdetalle.Enabled = True
            LabelItem_msg.Text = ""
            nueva_cuenta = False
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridP_Catalogo_Click - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub ToolbarB_limpiarbusqueda_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_limpiarbusqueda.Click
        Try
            limpia_busqueda()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridP_Catalogo_Click - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
#End Region
#Region "Detalle"

    Sub lee_cuenta(ByVal cuenta As String)
        Dim dTOS_moneda_detalle, dtos_tipocuenta_detalle As New DataTable
        Dim datatable_ComboB_codmoneda_Tabdetalle, datatable_ComboB_Tipo_Tabdetalle As DataTable

        Try
            datatable_ComboB_Tipo_Tabdetalle = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='tipo_cuenta' order by ordenvariable ", dtos_tipocuenta_detalle, True)
            jackconsulta.carga_combo_DN(ComboB_Tipo_Tabdetalle, datatable_ComboB_Tipo_Tabdetalle)
            datatable_ComboB_codmoneda_Tabdetalle = jackconsulta.Retornadatatable("select descmoneda,codmoneda from MtipoMoneda ", dTOS_moneda_detalle, True)
            jackconsulta.carga_combo_DN(ComboB_codmoneda_Tabdetalle, datatable_ComboB_codmoneda_Tabdetalle)
            ComboB_codmoneda_Tabdetalle.SelectedValue = 1

            Dim sql_reader_detallecuenta As SqlDataReader = jackconsulta.RetornaReader("select * from catalogoconta where cdgocuenta='" & cuenta & "'", True)
            While sql_reader_detallecuenta.Read

                TextB_cdgocuenta_Tabdetalle.Text = sql_reader_detallecuenta!cdgocuenta
                TextB_Desccuenta_Tabdetalle.Text = sql_reader_detallecuenta!desccuenta
                If IsDBNull(sql_reader_detallecuenta!codmoneda) Then
                    ComboB_codmoneda_Tabdetalle.SelectedValue = -1

                Else
                    ComboB_codmoneda_Tabdetalle.SelectedValue = sql_reader_detallecuenta!codmoneda
                End If
                If IsDBNull(sql_reader_detallecuenta!tipo) Then
                    ComboB_Tipo_Tabdetalle.SelectedValue = -1

                Else
                    ComboB_Tipo_Tabdetalle.SelectedValue = sql_reader_detallecuenta!tipo
                End If
                If IsDBNull(sql_reader_detallecuenta!categoria) Then
                    ComboB_categoria.SelectedValue = -1

                Else
                    ComboB_categoria.SelectedValue = sql_reader_detallecuenta!categoria
                End If
                TextB_saldoiniNac.Text = sql_reader_detallecuenta!saldoinilocal
                TextB_saldoiniNac.Text = Format(CDbl(TextB_saldoiniNac.Text), "###,###,##0.00")
                TextB_debeNac.Text = sql_reader_detallecuenta!debelocal
                TextB_debeNac.Text = Format(CDbl(TextB_debeNac.Text), "###,###,##0.00")
                TextB_haberNac.Text = sql_reader_detallecuenta!haberlocal
                TextB_haberNac.Text = Format(CDbl(TextB_haberNac.Text), "###,###,##0.00")
                TextB_saldofinNac.Text = sql_reader_detallecuenta!saldofinlocal
                TextB_saldofinNac.Text = Format(CDbl(TextB_saldofinNac.Text), "###,###,##0.00")
            End While

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("lee_cuenta - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ToolbarB_GuardarCuenta_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_GuardarCuenta.Click
        Try
            If nueva_cuenta Then
                agregar_cuenta()
                TextB_CuentaContable.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
                TextB_CuentaContable.AutoCompleteMode = AutoCompleteMode.Suggest
                TextB_CuentaContable.AutoCompleteSource = AutoCompleteSource.CustomSource

                TextB_cdgocuenta_Tabdetalle.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
                TextB_cdgocuenta_Tabdetalle.AutoCompleteMode = AutoCompleteMode.Suggest
                TextB_cdgocuenta_Tabdetalle.AutoCompleteSource = AutoCompleteSource.CustomSource

            Else
                actualizar_cuenta()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("lee_cuenta - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub agregar_cuenta()
        Try
            If valida_cuenta(TextB_cdgocuenta_Tabdetalle.Text.Trim) Then
                Dim sql_inserta_cuenta As String = "insert into CatalogoConta (cdgocuenta,desccuenta,tipo,fechcrea,codmoneda,saldoinilocal,debelocal,haberlocal,saldofinlocal,saldoiniext,debeext,haberext,saldofinext, usercrea, categoria)"
                sql_inserta_cuenta += " values ('" & TextB_cdgocuenta_Tabdetalle.Text.Trim & "','" & TextB_Desccuenta_Tabdetalle.Text.Trim & "','" & ComboB_Tipo_Tabdetalle.SelectedValue & "',getdate(),'" & ComboB_codmoneda_Tabdetalle.SelectedValue & "',0,0,0,0,0,0,0,0,1,'" & ComboB_categoria.SelectedValue & "')"
                If jackconsulta.EjecutaSQL(sql_inserta_cuenta, True) <> -1 Then

                    lee_cuenta(TextB_cdgocuenta_Tabdetalle.Text.Trim)
                    Dim nivel_nueva_cuenta As String = jackvalida.RetornaNivel_Cuenta(Len(TextB_cdgocuenta_Tabdetalle.Text.Trim))
                    jackvalida.Inserta_CtaNIvel(TextB_cdgocuenta_Tabdetalle.Text.Trim, TextB_Desccuenta_Tabdetalle.Text.Trim, nivel_nueva_cuenta)
                    ' jackmensaje.msg_izquierda("Creaci�n de la Nueva Cuenta Exitosa", "Validaci�n de Cuenta")
                    LabelItem_msg.Text = "****Creaci�n de la Nueva Cuenta Exitosa****"
                Else
                    ' jackmensaje.msg_izquierda("Fall� la Creaci�n de la Nueva Cuenta", "Validaci�n de Cuenta")
                    LabelItem_msg.Text = "****Fall� la Creaci�n de la Nueva Cuenta****"
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("agregar_cuenta - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub actualizar_cuenta()
        Dim sql_actualiza_cuenta As String = String.Empty
        Try
            If ComboB_Tipo_Tabdetalle.SelectedValue.trim = 1 Then
                If ComboB_categoria.SelectedValue <> "-1" Then
                    LabelItem_msg.Text = "****La Cuenta No puede tener Asignada una Categor�a V�lida si es de Mayor****"
                    Exit Sub

                End If

            End If
            'If Not ComboB_categoria.SelectedValue Is Nothing Then
            '    If ComboB_categoria.SelectedValue.trim <> "R" And ComboB_categoria.SelectedValue.trim <> "I" And ComboB_categoria.SelectedValue.trim <> "D" And ComboB_categoria.SelectedValue.trim <> "-1" Then
            '        LabelItem_msg.Text = "****Seleccione una Categor�a V�lida****"
            '        Exit Sub
            '    Else
            '    End If
            'Else
            '    LabelItem_msg.Text = "****Seleccione una Categor�a V�lida****"
            '    Exit Sub

            'End If
            sql_actualiza_cuenta = "update catalogoconta set desccuenta='" & TextB_Desccuenta_Tabdetalle.Text.Trim & "', tipo=" & ComboB_Tipo_Tabdetalle.SelectedValue & ", categoria='" & ComboB_categoria.SelectedValue.ToString.ToUpper & "',codmoneda=" & ComboB_codmoneda_Tabdetalle.SelectedValue & "  where cdgocuenta='" & TextB_cdgocuenta_Tabdetalle.Text.Trim & "'"
            jackconsulta.EjecutaSQL(sql_actualiza_cuenta, True)
            LabelItem_msg.Text = "****La Cuenta se ha Actualizado Satisfactoriamente****"
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("agregar_cuenta - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Function valida_cuenta(ByVal cuenta As String) As Boolean

        Dim msg As Balloon = Nothing
        Try
            msg = New Balloon()
            If jackvalida.existe_cuenta(cuenta) = True Then
                LabelItem_msg.Text = "****La Cuenta que quiere agregar ya Existe****"
                Return False
                Exit Function
            Else
                LabelItem_msg.Text = ""

            End If
            If jackvalida.formato_cuenta(cuenta, True) = False Then

                LabelItem_msg.Text = jackvalida.msg  '"****Formato de la Cuenta Incorrecto, Introduzca un Formato V�lido****"
                Return False
                Exit Function
            Else
                LabelItem_msg.Text = ""

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("valida_cuenta - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return True
    End Function

    Private Sub TextB_cdgocuenta_Tabdetalle_TextChanged(sender As Object, e As System.EventArgs) Handles TextB_cdgocuenta_Tabdetalle.TextChanged
        Try
            If nueva_cuenta Then
                If valida_cuenta(TextB_cdgocuenta_Tabdetalle.Text.Trim) = False Then
                    TextB_cdgocuenta_Tabdetalle.Focus()

                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cdgocuenta_Tabdetalle_TextChanged - Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
#End Region

#Region "controles comunes"
    Private Sub ButtonItem_cerrarpantalla_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_cerrarpantalla.Click
        Try
            EliminarRegistros()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error al tratar de eliminar " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ButtonItem_menuprincipal_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_menuprincipal.Click
        Try
            limpia_busqueda()
            limpia_detalle_cuenta()
            Me.Close()
            'menu_conta.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem_menuprincipal_Click Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub limpia_busqueda()
        Try
            TextB_CuentaContable.Text = String.Empty
            TextB_Desccuenta.Text = String.Empty
            ComboB_codmoneda_TabBusquedas.SelectedValue = -1
            ComboB_Tipo_TabBusquedas.SelectedValue = -1
            GridP_Catalogo.Hide()
            TextB_CuentaContable.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("limpia_busqueda Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub limpia_detalle_cuenta()
        Try
            TextB_cdgocuenta_Tabdetalle.Text = String.Empty
            TextB_Desccuenta_Tabdetalle.Text = String.Empty
            ComboB_codmoneda_Tabdetalle.SelectedValue = -1
            ComboB_Tipo_Tabdetalle.SelectedValue = -1
            TextB_saldoiniNac.Text = "0.00"
            TextB_debeNac.Text = "0.00"
            TextB_haberNac.Text = "0.00"
            TextB_saldofinNac.Text = "0.00"
            TextB_cdgocuenta_Tabdetalle.Focus()
            LabelItem_msg.Text = ""
            ComboB_categoria.SelectedValue = "-1"
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("limpia_detalle_cuenta Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonItem_NuevaCuenta_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_NuevaCuenta.Click
        Try
            SuperTabControl1.SelectedTabIndex = 1
            limpia_detalle_cuenta()
            nueva_cuenta = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("limpia_detalle_cuenta Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region


    Private Sub ButtonI_ImprimirCatalogo_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_ImprimirCatalogo.Click
        Dim filtro As String = String.Empty
        Dim and_filtro As Boolean
        Try
            If TextB_CuentaContable.Text <> "" Then
                and_filtro = True
                filtro = " {catalogoconta.cdgocuenta} startswith '" & TextB_CuentaContable.Text.Trim & "'"
            End If
            '' '' ''Filtro Descripci�n de la Cuenta
            If TextB_Desccuenta.Text <> "" Then
                If and_filtro = True Then
                    filtro += " and "
                Else
                    and_filtro = True

                End If
                filtro += " {catalogoconta.desccuenta} startswith '" & TextB_Desccuenta.Text.Trim & "'"
            End If
            '' '' ''Filtro tipo de Cuenta

            If ComboB_Tipo_TabBusquedas.SelectedValue <> -1 And ComboB_Tipo_TabBusquedas.Text <> String.Empty Then
                If and_filtro = True Then
                    filtro += " and "
                Else

                    and_filtro = True
                End If
                filtro += " {catalogoconta.tipo}='" & ComboB_Tipo_TabBusquedas.SelectedValue.ToString.Trim & "' "
            End If

            '' '' ''Filtro Moneda de la Cuenta
            If ComboB_codmoneda_TabBusquedas.Text <> "" And ComboB_codmoneda_TabBusquedas.SelectedValue <> -1 Then
                If and_filtro = True Then
                    filtro += " and "
                Else

                    and_filtro = True
                End If
                filtro += " {catalogoconta.codmoneda} = " & ComboB_codmoneda_TabBusquedas.SelectedValue
            End If

            SIMF_CONTA.filtro_reporte = filtro

            'Frm_RPT_catalogoconta.MdiParent = frmPrincipal
            'Frm_RPT_catalogoconta.Text = "Cat�logo Contable de AgroCentro S.A"
            SIMF_CONTA.numeformulas = 1
            SIMF_CONTA.formula(0) = RNEmpresa.ObtieneNombreEmpreaContabilidad()
            Frm_RPT_catalogoconta.Text = "Cat�logo Contable de " & RNEmpresa.ObtieneNombreEmpreaContabilidad()
            Frm_RPT_catalogoconta.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub ToolbarB_limpiardetalle_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_limpiardetalle.Click
        limpia_detalle_cuenta()
    End Sub

    Private Sub btnSalir_Click(sender As System.Object, e As System.EventArgs) Handles btnSalir.Click
        limpia_busqueda()
        limpia_detalle_cuenta()
        Me.Hide()
    End Sub

    Private Sub SuperTabItem_detalle_Click(sender As Object, e As EventArgs) Handles SuperTabItem_detalle.Click
        Try
            SuperTabControl1.SelectedTabIndex = 1
            limpia_detalle_cuenta()
            nueva_cuenta = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("limpia_detalle_cuenta Error " & ex.Message, " Catalogo contable", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class