Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class actRptResumenCompras 
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actRptResumenCompras_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        Label.Text = strNombEmpresa
        TextBox21.Text = Format(Now, "Long Date")
        TextBox23.Text = Format(Now, "Long Time")
        Label25.Text = Label25.Text & " " & Format(Now, "dd-MMM-yyyy")

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actRptResumenCompras_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select
    End Sub
End Class
