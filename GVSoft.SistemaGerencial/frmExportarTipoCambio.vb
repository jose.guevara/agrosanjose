Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmExportarTipoCambio
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha Inicial"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(208, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Fecha Final"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker1.Location = New System.Drawing.Point(88, 16)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 2
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker2.Location = New System.Drawing.Point(280, 16)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker2.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(96, 72)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Exportar"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(224, 72)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 32)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Salir"
        '
        'frmExportarTipoCambio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(392, 117)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DateTimePicker2)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarTipoCambio"
        Me.Text = "frmExportarTasasCambio"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmExportarTipoCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0

    End Sub

    Private Sub frmExportarTipoCambio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click

        If sender.name = "Button1" Then
            ProcesarArchivo()
        ElseIf sender.name = "Button2" Then
            Me.Close()
        End If

    End Sub

    Sub ProcesarArchivo()

        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim lsRutaCompletaArchivoZip As String = String.Empty
        Dim cmdTmp As New SqlCommand("sp_ExportarArchivos", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter

        Me.Cursor = Cursors.WaitCursor
        With prmTmp01
            .ParameterName = "@intExportar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 1
        End With
        With prmTmp02
            .ParameterName = "@intAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intAgenciaDefecto
        End With
        With prmTmp03
            .ParameterName = "@lngNumFechaIni"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With prmTmp04
            .ParameterName = "@lngNumFechaFin"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker2.Value, "yyyyMMdd")
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        strClave = ""
        strClave = "Agro2K_2008"
        strArchivo = ""
        strArchivo = ConfigurationManager.AppSettings("RutaArchivosTipoCambio").ToString() & "tcambio.txt"
        strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosTipoCambio").ToString() & "tcambio"
        If File.Exists(strArchivo) = True Then
            File.Delete(strArchivo)
        End If
        Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosTipoCambio").ToString() & "tcambio.txt")
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            sr.WriteLine(dtrAgro2K.GetValue(0))
        End While
        sr.Close()
        dtrAgro2K.Close()
        cmdTmp.Connection.Close()
        cnnAgro2K.Close()
        Try
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
            'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
            lsRutaCompletaArchivoZip = String.Empty
            lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
            If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                Exit Sub
            End If

            If File.Exists(strArchivo) = True Then
                File.Delete(strArchivo)
            End If
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
            Me.Cursor = Cursors.Default
            Exit Sub
        End Try
        MsgBox("Finaliz� la exportaci�n de las Tasas de Cambio", MsgBoxStyle.Information, "Exportar Datos")
        Me.Cursor = Cursors.Default

    End Sub

End Class
