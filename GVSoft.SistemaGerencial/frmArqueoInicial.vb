Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv

Public Class frmArqueoInicial
    Public ExisteArqueo, ArqueoAbierto, Recalcula, Operacion As Integer 'Variables de Control

    Private sNombreMetodo As String = String.Empty
    Private objError As SEError = New SEError()
    Private sNombreClase As String = "frmArqueoInicial"
    Private lnNumeroArqueo As Integer = 0
    Private lsNumeroArqueo As String = String.Empty
    Private lnPosicionTotalIngreso As Int16 = 3
    Private lnPosicionInicialIngreso As Int16 = 0
    Private lnPosicionTotalEgreso As Int16 = 6
    Private lnPosicionInicialEgreso As Int16 = lnPosicionTotalIngreso + 1
    Private lnPosicionDiferenciaIngresoEgreso As Int16 = 7
    Private lnPosicionTotalDineroEnCaja As Int16 = 8
    Private lnPosicionSaldoFinal As Int16 = 18
    Private lnPosicionMontoFactura As Integer = 1
    Private lnPosicionMontoRecibo As Integer = 2
    Private lnPosicionGastos As Integer = 5
    Private lnPosicionDepositos As Integer = 17
    Private lnPosicionOtrosIngresos As Integer = 12
    Private lnPosicionSobrante As Integer = 13
    Dim dMontoTotalRecibos, dMontoTotalFactura As Double
    Dim IdAgen As Integer

    Private Sub frmArqueoInicial_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Con Valor 1 Ingresamos y con Valor 2 Modificamos..
            Operacion = 1 'Variable que nos controla el tipo de operacion a realizar si es de modificar o ingresar un arqueo

            TabControl1.SelectedTab = TabControl1.Tabs("tbiInfoGeneralArqueo")
            ConfiguracionInicialGridDetalleArqueo()
            UbicarAgencia(lngRegUsuario)
            ExisteArqueo = RNArqueoCaja.ValidaExisteArqueos(lngRegAgencia)
            If ExisteArqueo = 0 Then
                dtiFechaInicioArqueo.Value = DateTime.Today()
                dtiFechaFinArqueo.Value = DateTime.Today()
                diSaldoInicial.Enabled = True
                diOtrosIngreso.Enabled = True
            Else
                diSaldoInicial.Text = RNArqueoCaja.ObtieneSaldoInicial(lngRegAgencia)
            End If
            diTasaCambio.Text = RNTipoCambio.ObtieneTipoCambioDelDia()
            ObtieneNumerFacturaYFechaInicial()
            ObtieneNumerReciboYFechaInicial()
            LlenaGridResumenCuadre()
            'UbicarAgencia(lngRegUsuario)
            lbltxtUsuario.Text = strNombreUsuario

            LlenaGridDetalleCuadre(dgxDetalleCordobas)
            LlenaGridDetalleCuadreDolares(dgxDetalleDolares)

            ObtieneSeriexAgencia()
            ObtieneNumeroArqueo()
            lbltxtNumeroArqueo.Text = lsNumeroArqueo

            ObtenerSaldoTotalFacturaRecibosCuadre()

            LlenarComboSucursales()

            GenerarDetalleArqueo()

        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub LlenarComboSucursales()
        RNAgencias.CargarComboAgencias(cmbAgencias, lngRegUsuario)
        'cmbAgencias.ValueMember = lngRegAgencia
    End Sub

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        ddlSerie.SelectedItem = -1
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",1"

        If strQuery.Trim.Length > 0 Then
            ddlSerie.Items.Clear()
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                IndicaObtieneRegistro = 1
            End While
        End If
        If IndicaObtieneRegistro = 1 Then
            ddlSerie.SelectedIndex = 0
        End If

        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
    End Sub

    Private Sub LlenaGridResumenCuadre()
        Dim i As Integer = 0
        Try
            dgResumenArqueo.Rows.Add("INGRESOS(+)", "", "", "")
            '            dgResumenArqueo.Rows.Add("", "SALDO INICIAL: ", 0)
            dgResumenArqueo.Rows.Add("", "TOTAL FACTURAS: ", "0.00", "")
            dgResumenArqueo.Rows.Add("", "TOTAL RECIBOS: ", "0.00", "")
            dgResumenArqueo.Rows.Add("TOTAL INGRESOS:", "", "", "0.00")
            dgResumenArqueo.Rows.Add("EGRESOS(-)", "", "", "")
            dgResumenArqueo.Rows.Add("", "GASTOS: ", "0.00", "")
            '            dgResumenArqueo.Rows.Add("", "DEPOSITOS: ", 0)
            dgResumenArqueo.Rows.Add("TOTAL EGRESOS:", " ", "", "0.00")
            dgResumenArqueo.Rows.Add("INGRESOS-EGRESOS:", "", "", "0.00")
            dgResumenArqueo.Rows.Add("TOTAL DINERO EN CAJA:", "", "0.00", "")
            dgResumenArqueo.Rows.Add("DIF. +/- EFECTIVO MENOS ING/EGRESOS:", "", "0.00", "")
            dgResumenArqueo.Rows.Add("", "", "", "")
            dgResumenArqueo.Rows.Add("", "SALDO ANTERIOR: ", "0.00", "")
            dgResumenArqueo.Rows.Add("", "OTROS INGRESOS: ", "0.00", "")
            dgResumenArqueo.Rows.Add("", "SOBRANTE(+)/FALTANTE(-): ", "0.00", "")
            'dgResumenArqueo.Rows.Add("", "(+)TOTAL INGRESOS DE HOY: ", "", "0.00")
            dgResumenArqueo.Rows.Add("", "(+)TOTAL INGRESOS DE HOY: ", "0.00", "")
            'dgResumenArqueo.Rows.Add("", "(-)TOTAL GASTOS DE HOY: ", "", "0.00")
            dgResumenArqueo.Rows.Add("", "(-)TOTAL GASTOS DE HOY: ", "0.00", "")
            dgResumenArqueo.Rows.Add("", "=TOTAL EN CAJA: ", "", "")
            'dgResumenArqueo.Rows.Add("", "(-)DEPOSITOS DE HOY: ", "", "0.00")
            dgResumenArqueo.Rows.Add("", "(-)DEPOSITOS DE HOY: ", "0.00", "")
            dgResumenArqueo.Rows.Add("SALDO FINAL HOY:", "", "", "0.00")
            If (dgResumenArqueo.Rows.Count >= lnPosicionInicialEgreso) Then
                For i = 0 To lnPosicionTotalIngreso
                    dgResumenArqueo.Rows(i).DefaultCellStyle.ForeColor = Color.Green
                Next
            End If
            If (dgResumenArqueo.Rows.Count >= lnPosicionDiferenciaIngresoEgreso) Then
                For i = lnPosicionInicialEgreso To lnPosicionTotalEgreso
                    dgResumenArqueo.Rows(i).DefaultCellStyle.ForeColor = Color.Blue
                Next
            End If

            If (dgResumenArqueo.Rows.Count > lnPosicionTotalEgreso) Then
                dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).DefaultCellStyle.ForeColor = Color.DarkOrange
            End If

            If (dgResumenArqueo.Rows.Count > lnPosicionDiferenciaIngresoEgreso) Then
                dgResumenArqueo.Rows(lnPosicionTotalDineroEnCaja).DefaultCellStyle.ForeColor = Color.DarkOrange
            End If

            dgResumenArqueo.AllowUserToDeleteRows = False
            dgResumenArqueo.AllowUserToAddRows = False
            dgResumenArqueo.AllowDrop = False
            dgResumenArqueo.AllowUserToOrderColumns = False
            dgResumenArqueo.AutoSize = True
            dgResumenArqueo.ScrollBars = Windows.Forms.ScrollBars.Both
            dgResumenArqueo.Columns(0).ReadOnly = True
            dgResumenArqueo.Columns(1).ReadOnly = True
            dgResumenArqueo.Columns(2).ReadOnly = True

        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub LlenaGridDetalleCuadre(ByVal dgxObjoto As DevComponents.DotNetBar.Controls.DataGridViewX)
        Try
            dgxObjoto.Rows.Add("500", 0, 0)
            dgxObjoto.Rows.Add("200", 0, 0)
            dgxObjoto.Rows.Add("100", 0, 0)
            dgxObjoto.Rows.Add("50", 0, 0)
            dgxObjoto.Rows.Add("20", 0, 0)
            dgxObjoto.Rows.Add("10", 0, 0)
            dgxObjoto.Rows.Add("5", 0, 0)
            dgxObjoto.Rows.Add("1", 0, 0)
            dgxObjoto.Rows.Add("0.5", 0, 0)
            dgxObjoto.Rows.Add("0.25", 0, 0)
            dgxObjoto.Rows.Add("0.10", 0, 0)
            dgxObjoto.Rows.Add("0.05", 0, 0)
            dgxObjoto.Rows.Add("0.01", 0, 0)
            dgxObjoto.Rows.Add("Total", 0, 0)
            dgxObjoto.Rows.Add("Total C�rdobas", 0, 0)
            dgxObjoto.AllowUserToDeleteRows = False
            dgxObjoto.AllowUserToAddRows = False
            dgxObjoto.AllowDrop = False
            dgxObjoto.AllowUserToOrderColumns = False
            If (dgxObjoto.Rows.Count >= lnPosicionTotalEgreso) Then
                If dgxObjoto.Rows(13).Cells.Count >= 1 Then
                    dgxObjoto.Rows(13).Cells(1).ReadOnly = True
                End If
            End If
            If (dgxObjoto.Rows.Count >= 14) Then
                If dgxObjoto.Rows(14).Cells.Count >= 1 Then
                    dgxObjoto.Rows(14).Cells(1).ReadOnly = True
                End If
            End If

        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub LlenaGridDetalleCuadreDolares(ByVal dgxObjoto As DevComponents.DotNetBar.Controls.DataGridViewX)
        Try
            dgxObjoto.Rows.Add("100", 0, 0)
            dgxObjoto.Rows.Add("50", 0, 0)
            dgxObjoto.Rows.Add("20", 0, 0)
            dgxObjoto.Rows.Add("10", 0, 0)
            dgxObjoto.Rows.Add("5", 0, 0)
            dgxObjoto.Rows.Add("1", 0, 0)
            dgxObjoto.Rows.Add("0.50", 0, 0)
            dgxObjoto.Rows.Add("0.10", 0, 0)
            dgxObjoto.Rows.Add("Total", 0, 0)
            dgxObjoto.Rows.Add("Total C�rdobas", 0, 0)
            dgxObjoto.AllowUserToDeleteRows = False
            dgxObjoto.AllowUserToAddRows = False
            dgxObjoto.AllowDrop = False
            dgxObjoto.AllowUserToOrderColumns = False
            If (dgxObjoto.Rows.Count >= 8) Then
                If dgxObjoto.Rows(8).Cells.Count >= 1 Then
                    dgxObjoto.Rows(8).Cells(1).ReadOnly = True
                End If
            End If
            If (dgxObjoto.Rows.Count >= 9) Then
                If dgxObjoto.Rows(9).Cells.Count >= 1 Then
                    dgxObjoto.Rows(9).Cells(1).ReadOnly = True
                End If
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function ObtieneDetalleArqueo(ByVal lsFacturaInicial As String, ByVal lsFacturaFinal As String,
                                                 ByVal lsReciboInicial As String, ByVal lsReciboFinal As String,
                                                ByVal lsFechaInicial As String, ByVal lsFechaFinal As String) As List(Of SECuadreCajaDetalle)
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle)
        Try

            lstDetalleCuadreCaja = RNArqueoCaja.ObtieneFacturasYRecibosARealizarCierre(lngRegUsuario, lsFacturaInicial, lsFacturaFinal, lsReciboInicial, lsReciboFinal, lsFechaInicial, lsFechaFinal)
            Return lstDetalleCuadreCaja
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ObtieneDetalleArqueoRecalcular(ByVal lsFacturaInicial As String, ByVal lsFacturaFinal As String,
                                                 ByVal lsReciboInicial As String, ByVal lsReciboFinal As String,
                                                ByVal pnIdArqueo As Integer) As List(Of SECuadreCajaDetalle)
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle)
        Try

            lstDetalleCuadreCaja = RNArqueoCaja.ObtieneFacturasYRecibosARecalcular(lngRegUsuario, lsFacturaInicial, lsFacturaFinal, lsReciboInicial, lsReciboFinal, pnIdArqueo)
            Return lstDetalleCuadreCaja
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub LlenaDetalleArqueo(ByVal lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle))
        Try
            ugDetalleArqueo.DataSource = lstDetalleCuadreCaja
            ugDetalleArqueo.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SeteaMontosArqueoEnResumen(ByVal pnMontoTotalFactura As Double, ByVal pnMontoTotalRecibo As Double)
        Dim lnMontoTotalIngreso As Double = 0
        Dim lnMontoTotalEgreso As Double = 0
        Dim i As Integer = 0
        Try
            If dgResumenArqueo.Rows.Count >= 5 Then
                dgResumenArqueo.Rows(lnPosicionMontoFactura).Cells(2).Value = Format(pnMontoTotalFactura, "###,###,###,###.00")
                lnMontoTotalIngreso = 0
                For i = 1 To (lnPosicionTotalIngreso - 1)
                    lnMontoTotalIngreso += ConvierteADouble(dgResumenArqueo.Rows(i).Cells(2).Value)
                Next
                dgResumenArqueo.Rows(lnPosicionTotalIngreso).Cells(3).Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
            End If
            If dgResumenArqueo.Rows.Count >= 5 Then
                dgResumenArqueo.Rows(lnPosicionMontoRecibo).Cells(2).Value = Format(pnMontoTotalRecibo, "###,###,###,###.00")
                dgResumenArqueo.Rows(lnPosicionTotalIngreso).Cells(3).Value = Format(pnMontoTotalFactura + pnMontoTotalRecibo, "###,###,###,###.00")
            End If
            RealizaGranTotalEnResumen()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RealizaGranTotalEnResumen()
        Dim lnMontoTotalIngreso As Double = 0
        Dim lnMontoTotalEgreso As Double = 0
        Dim i As Integer = 0
        Try
            If dgResumenArqueo.Rows.Count >= lnPosicionTotalIngreso Then
                lnMontoTotalIngreso = 0
                For i = 1 To (lnPosicionTotalIngreso - 1)
                    lnMontoTotalIngreso += ConvierteADouble(dgResumenArqueo.Rows(i).Cells(2).Value)
                Next
            End If
            If dgResumenArqueo.Rows.Count >= lnPosicionTotalEgreso Then
                lnMontoTotalEgreso = 0
                For i = (lnPosicionTotalIngreso + 1) To (lnPosicionTotalEgreso - 1)
                    lnMontoTotalEgreso += ConvierteADouble(dgResumenArqueo.Rows(i).Cells(2).Value)
                Next
            End If
            'If lnMontoTotalEgreso > lnMontoTotalIngreso Then
            '    dgResumenArqueo.Rows(7).DefaultCellStyle.ForeColor = Color.Red
            'Else
            '    dgResumenArqueo.Rows(7).DefaultCellStyle.ForeColor = Color.Black
            'End If
            dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells(3).Value = Format(lnMontoTotalIngreso - lnMontoTotalEgreso, "###,###,###,###.00")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SeteaUnValorEnArqueoEnResumen(ByVal pnMonto As Double, ByVal Fila As Integer, ByVal TipoOperacion As String)
        Dim lnMontoTotalIngreso As Double = 0
        Dim lnMontoTotalEgreso As Double = 0
        Dim dSaldoAnteriro, dTotalCaja, dDeposito, dSaldoFinal, dOtrosIngresos, dSobranteCaja As Double
        Dim lnDiferencia As Double = 0
        Dim lnColumna As Integer = 0
        Dim lnFilaTotalIngresos As Integer = 0
        Dim lnFilaTotalEgresos As Integer = 0
        Dim i As Integer = 0
        Try
            lnColumna = 2
            lnFilaTotalIngresos = lnPosicionTotalIngreso
            lnFilaTotalEgresos = lnPosicionTotalEgreso

            If dgResumenArqueo.Rows.Count >= Fila Then
                'dgResumenArqueo.Rows(Fila).Cells(lnColumna).Value = Format(pnMonto, "###,###,###,###.00")
                dgResumenArqueo.Rows(Fila).Cells("utbMonto").Value = Format(pnMonto, "###,###,###,###.00")
                If TipoOperacion = "I" Then 'Ingresa total de dinero en Facturas
                    lnMontoTotalIngreso = 0
                    For i = 1 To (lnPosicionTotalIngreso - 1)
                        lnMontoTotalIngreso += ConvierteADouble(dgResumenArqueo.Rows(i).Cells(lnColumna).Value)
                    Next
                    'dgResumenArqueo.Rows(lnFilaTotalIngresos).Cells(lnColumna).Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                    dgResumenArqueo.Rows(lnFilaTotalIngresos).Cells("Totales").Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                ElseIf TipoOperacion = "E" Then 'Ingresa Totales de Egresos
                    lnMontoTotalIngreso = 0
                    For i = lnPosicionInicialEgreso To (lnPosicionTotalEgreso - 1)
                        lnMontoTotalIngreso += ConvierteADouble(dgResumenArqueo.Rows(i).Cells(lnColumna).Value)
                    Next
                    'dgResumenArqueo.Rows(lnFilaTotalEgresos).Cells(lnColumna).Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                    dgResumenArqueo.Rows(lnFilaTotalEgresos).Cells("Totales").Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                ElseIf TipoOperacion = "R" Then 'Ingresa Total de Recibos
                    lnMontoTotalIngreso = 0
                    For i = 1 To (lnPosicionTotalIngreso - 1)
                        lnMontoTotalIngreso += ConvierteADouble(dgResumenArqueo.Rows(i).Cells(lnColumna).Value)
                    Next
                    'dgResumenArqueo.Rows(lnFilaTotalIngresos).Cells(lnColumna).Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                    dgResumenArqueo.Rows(lnFilaTotalIngresos).Cells("Totales").Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                ElseIf TipoOperacion = "D" Then 'Realiza Calculo de Ingresos - Egresos
                    'lnMontoTotalIngreso = 0
                    lnDiferencia = dgResumenArqueo.Rows(lnPosicionTotalDineroEnCaja).Cells("utbMonto").Value - dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells("Totales").Value

                    If lnDiferencia < 0 Then
                        dgResumenArqueo.Rows(9).DefaultCellStyle.ForeColor = Color.Red  'indicamos saldo en rojo
                    Else
                        dgResumenArqueo.Rows(9).DefaultCellStyle.ForeColor = Color.SteelBlue   'Saldo Positivo
                    End If
                    'dgResumenArqueo.Rows(lnFilaTotalIngresos).Cells(lnColumna).Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                    dgResumenArqueo.Rows(9).Cells("utbMonto").Value = Format(lnDiferencia, "###,###,###,###.00")
                ElseIf TipoOperacion = "SF" Then 'Realiza Calculo de Saldo Final
                    dSaldoAnteriro = ConvierteADouble(diSaldoInicial.Text.Trim()) '+ ConvierteADouble(diOtrosIngreso.Text.Trim())
                    lnMontoTotalIngreso = dgResumenArqueo.Rows(lnPosicionTotalIngreso).Cells("Totales").Value
                    lnMontoTotalEgreso = dgResumenArqueo.Rows(lnPosicionTotalEgreso).Cells("Totales").Value
                    dOtrosIngresos = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionOtrosIngresos).Cells("utbMonto").Value)
                    dSobranteCaja = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionSobrante).Cells("utbMonto").Value)
                    dTotalCaja = (dSaldoAnteriro + lnMontoTotalIngreso + dOtrosIngresos + dSobranteCaja) - lnMontoTotalEgreso
                    dDeposito = ConvierteADouble(diDeposito.Text.Trim())
                    dSaldoFinal = dTotalCaja - dDeposito

                    dgResumenArqueo.Rows(11).Cells("utbMonto").Value = Format(dSaldoAnteriro, "###,###,###,###.00")
                    dgResumenArqueo.Rows(14).Cells("utbMonto").Value = Format(lnMontoTotalIngreso, "###,###,###,###.00")
                    dgResumenArqueo.Rows(15).Cells("utbMonto").Value = Format(lnMontoTotalEgreso, "###,###,###,###.00")
                    dgResumenArqueo.Rows(16).Cells("Totales").Value = Format(dTotalCaja, "###,###,###,###.00")
                    dgResumenArqueo.Rows(17).Cells("utbMonto").Value = Format(dDeposito, "###,###,###,###.00")
                    dgResumenArqueo.Rows(Fila).Cells("utbMonto").Value = ""
                    dgResumenArqueo.Rows(18).Cells("Totales").Value = Format(dSaldoFinal, "###,###,###,###.00")

                End If
                RealizaGranTotalEnResumen()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ObtieneMotoTotalCuadre(ByVal lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle), ByRef pnMontoTotalFactura As Double, ByRef pnMontoTotalRecibo As Double)
        Dim i As Integer = 0
        Try
            pnMontoTotalFactura = 0
            pnMontoTotalRecibo = 0
            If lstDetalleCuadreCaja IsNot Nothing Then
                If lstDetalleCuadreCaja.Count > 0 Then
                    For i = 0 To lstDetalleCuadreCaja.Count - 1
                        If lstDetalleCuadreCaja(i).TipoDocumento = "F" Then
                            pnMontoTotalFactura += lstDetalleCuadreCaja(i).MontoTotalDocumento
                        Else
                            pnMontoTotalRecibo += lstDetalleCuadreCaja(i).MontoTotalDocumento
                        End If
                    Next
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ConfiguracionInicialGridDetalleArqueo()
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Try
            Infragistics.Win.AppStyling.StyleManager.Load("Windows7.isl", True, "windows7")
            ugDetalleArqueo.StyleLibraryName = "windows7"
            ugDetalleArqueo.UseAppStyling = True
            Infragistics.Win.WindowsVistaColorTable.ColorScheme = WindowsVistaColorScheme.Blue
            ugDetalleArqueo.DisplayLayout.ViewStyleBand = UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub

    Function MontoTotalGrupo(ByVal i_oRow As UltraGridGroupByRow) As Double
        Dim MontoTotal As Double = 0
        Try
            If i_oRow.Rows.Count > 0 Then
                For Each oRow As UltraGridRow In i_oRow.Rows
                    MontoTotal += ConvierteADouble(oRow.Cells("MontoTotalDocumento").Value)
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Return MontoTotal
    End Function

    Private Sub ugDetalleArqueo_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles ugDetalleArqueo.InitializeLayout
        Dim layout As UltraGridLayout = Nothing
        Dim band As UltraGridBand = Nothing
        Dim ColumnaMontoTotal As UltraGridColumn = Nothing

        Try
            layout = e.Layout
            band = layout.Bands(0)
            ColumnaMontoTotal = band.Columns("MontoTotalDocumento")
            e.Layout.Override.RowFilterMode = RowFilterMode.SiblingRowsOnly
            band.Override.FilterUIType = Infragistics.Win.UltraWinGrid.FilterUIType.FilterRow

            e.Layout.Override.FilterEvaluationTrigger = Infragistics.Win.UltraWinGrid.FilterEvaluationTrigger.OnCellValueChange

            band.Columns("IdCuadre").Hidden = True
            band.Columns("IdMoneda").Hidden = True
            band.Columns("FechaIngreso").Hidden = True
            band.Columns("FechaIngresoNum").Hidden = True
            band.Columns("FechaDocumentoNum").Hidden = True
            band.Columns("TipoDocumento").Hidden = True
            band.Columns("NumeroDocumento").AllowRowFiltering = DefaultableBoolean.True

            ColumnaMontoTotal.Format = "###,###,###.00"
            layout.ViewStyleBand = ViewStyleBand.OutlookGroupBy
            band.Summaries.Add(SummaryType.Sum, ColumnaMontoTotal, Infragistics.Win.UltraWinGrid.SummaryPosition.UseSummaryPositionColumn)
            band.Override.SummaryDisplayArea = SummaryDisplayAreas.Bottom

            e.Layout.Bands(0).SortedColumns.Add("DescripcionTipoDocumento", False, True)
            e.Layout.GroupByBox.Hidden = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ugDetalleArqueo_InitializeGroupByRow(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeGroupByRowEventArgs) Handles ugDetalleArqueo.InitializeGroupByRow
        Dim grRow As Infragistics.Win.UltraWinGrid.UltraGridGroupByRow = e.Row
        Try
            grRow.Description = "Total " & grRow.ValueAsDisplayText & ": Cantidad: " & Format(grRow.Rows.Count, "###,###,###") & " ;Monto Total: " & Format(MontoTotalGrupo(grRow), "###,###,###.00")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ObtieneNumeroArqueo()
        Try
            lsNumeroArqueo = RNArqueoCaja.ObtenerNumeroArqueo(lngRegAgencia)
            lsNumeroArqueo = ddlSerie.Text + lsNumeroArqueo
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub

    Private Function ValidaNumeroFactura(ByVal psNumeroFactura As String) As Object()
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Try
            If psNumeroFactura.Trim().Length > 0 Then
                lsFechaInicio = Format(dtiFechaInicioArqueo.Value.Year, "0000") & "-" & Format(dtiFechaInicioArqueo.Value.Month, "00") & "-" & Format(dtiFechaInicioArqueo.Value.Day, "00")
                lsFechaFin = Format(dtiFechaFinArqueo.Value.Year, "0000") & "-" & Format(dtiFechaFinArqueo.Value.Month, "00") & "-" & Format(dtiFechaFinArqueo.Value.Day, "00")

                Resultado = RNFacturas.VerificaNumeroFactura(psNumeroFactura, lngRegUsuario, lsFechaInicio, lsFechaFin)
                'If (Resultado(0) = 1) Then
                '    'MsgBox(Resultado(1), MsgBoxStyle.Critical, "Valida Numero Factura")
                '    MessageBoxEx.Show(Resultado(1), " Valida N�mero Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'End If
                'If (Resultado(2) = 1) Then
                '    'MsgBox(Resultado(3), MsgBoxStyle.Critical, "Valida Numero Factura")
                '    MessageBoxEx.Show(Resultado(3), " Valida N�mero Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Return Resultado
    End Function

    Private Function ValidaNumeroRecibo(ByVal psNumeroRecibo As String) As Object()
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Try
            If psNumeroRecibo.Trim().Length > 0 Then
                lsFechaInicio = Format(dtiFechaInicioArqueo.Value.Year, "0000") & "-" & Format(dtiFechaInicioArqueo.Value.Month, "00") & "-" & Format(dtiFechaInicioArqueo.Value.Day, "00")
                lsFechaFin = Format(dtiFechaFinArqueo.Value.Year, "0000") & "-" & Format(dtiFechaFinArqueo.Value.Month, "00") & "-" & Format(dtiFechaFinArqueo.Value.Day, "00")

                Resultado = RNRecibos.VerificaNumeroRecibo(psNumeroRecibo, lngRegUsuario, lsFechaInicio, lsFechaFin)
                'If (Resultado(0) = 1) Then
                '    MessageBoxEx.Show(Resultado(1), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'End If
                'If (Resultado(2) = 1) Then
                '    MessageBoxEx.Show(Resultado(3), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Return Resultado
    End Function

    Private Sub dgxDetalleCordobas_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgxDetalleCordobas.CellValueChanged
        Dim nTotalMonto As Double = 0
        Dim nMonto As Double = 0
        Dim nCantidad As Integer = 0
        Dim nTotalCantidad As Integer = 0
        Dim nDiferenciaIngresoEgreso As Double = 0
        Try
            If e.ColumnIndex = 1 Then
                If Not dgxDetalleCordobas.CurrentRow Is Nothing Then
                    If dgxDetalleCordobas.CurrentRow.Cells.Count >= 2 Then
                        If ConvierteAInt(dgxDetalleCordobas.CurrentRow.Cells(1).Value) > 0 Then
                            If ConvierteADouble(dgxDetalleCordobas.CurrentRow.Cells(0).Value) <> 0 And ConvierteADouble(dgxDetalleCordobas.CurrentRow.Cells(1).Value) <> 0 Then
                                dgxDetalleCordobas.CurrentRow.Cells(2).Value = Format(ConvierteADouble(dgxDetalleCordobas.CurrentRow.Cells(0).Value) * ConvierteADouble(dgxDetalleCordobas.CurrentRow.Cells(1).Value), "#,##0.#0")
                            End If

                        Else
                            dgxDetalleCordobas.CurrentRow.Cells(2).Value = 0
                        End If
                        nTotalMonto = 0
                        nMonto = 0
                        nCantidad = 0
                        nTotalCantidad = 0
                        For i As Integer = 0 To dgxDetalleCordobas.Rows.Count - 3
                            nMonto = 0
                            nMonto = ConvierteADouble(dgxDetalleCordobas.Rows(i).Cells(2).Value)
                            nCantidad = 0
                            nCantidad = ConvierteAInt(dgxDetalleCordobas.Rows(i).Cells(1).Value)
                            nTotalMonto = nTotalMonto + nMonto
                            nTotalCantidad = nTotalCantidad + nCantidad
                        Next
                        If dgxDetalleCordobas.Rows.Count >= 13 Then
                            If dgxDetalleCordobas.Rows(13).Cells.Count >= 1 Then
                                If Not dgxDetalleCordobas.Rows(13).Cells(2) Is Nothing Then
                                    dgxDetalleCordobas.Rows(13).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                                End If
                                If Not dgxDetalleCordobas.Rows(13).Cells(1) Is Nothing Then
                                    dgxDetalleCordobas.Rows(13).Cells(1).Value = nTotalCantidad
                                End If
                            End If
                        End If
                        If dgxDetalleCordobas.Rows.Count >= 14 Then
                            If dgxDetalleCordobas.Rows(14).Cells.Count >= 1 Then
                                If Not dgxDetalleCordobas.Rows(14).Cells(2) Is Nothing Then
                                    dgxDetalleCordobas.Rows(14).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                                End If
                                If Not dgxDetalleCordobas.Rows(14).Cells(1) Is Nothing Then
                                    dgxDetalleCordobas.Rows(14).Cells(1).Value = nTotalCantidad
                                End If
                            End If
                        End If
                        'If dgResumenArqueo.Rows.Count >= (lnPosicionTotalDineroEnCaja + 1) Then
                        '    dgResumenArqueo.Rows(lnPosicionTotalDineroEnCaja).Cells(2).Value = Format(nTotalMonto, "###,###,##0.#0")
                        'End If
                        'If dgResumenArqueo.Rows.Count >= (lnPosicionDiferenciaIngresoEgreso) Then
                        '    'nDiferenciaIngresoEgreso = dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells(2).Value = Format(nTotalMonto, "###,###,##0.#0")
                        '    nDiferenciaIngresoEgreso = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells(2).Value)
                        'End If
                        'If dgResumenArqueo.Rows.Count >= (lnPosicionSaldoFinal) Then
                        '    dgResumenArqueo.Rows(lnPosicionSaldoFinal).Cells(2).Value = Format(nTotalMonto - nDiferenciaIngresoEgreso, "###,###,##0.#0")
                        'End If
                        CalculaSaldoFinal()

                    End If
                End If
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CalculaSaldoFinal()
        Dim nDiferenciaIngresoEgreso As Double = 0
        Dim nTotalMonto As Double = 0
        Dim dTotalMontoDolares As Double = 0
        Dim nMonto As Double = 0
        If dgxDetalleCordobas.Rows.Count > 3 Then
            For i As Integer = 0 To dgxDetalleCordobas.Rows.Count - 3
                nMonto = 0
                nMonto = ConvierteADouble(dgxDetalleCordobas.Rows(i).Cells(2).Value)
                nTotalMonto = nTotalMonto + nMonto
            Next
        End If
        If dgResumenArqueo.Rows.Count >= (lnPosicionTotalDineroEnCaja + 1) Then
            dTotalMontoDolares = dgxDetalleDolares.Rows(9).Cells(2).Value
            nTotalMonto = nTotalMonto + dTotalMontoDolares
            dgResumenArqueo.Rows(lnPosicionTotalDineroEnCaja).Cells(2).Value = Format(nTotalMonto, "###,###,##0.#0")
            diTotalDineraoEnCaja.Text = nTotalMonto
        End If
        diTotalDineraoEnCaja.Text = nTotalMonto
        If dgResumenArqueo.Rows.Count >= (lnPosicionDiferenciaIngresoEgreso) Then
            'nDiferenciaIngresoEgreso = dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells(2).Value = Format(nTotalMonto, "###,###,##0.#0")
            nDiferenciaIngresoEgreso = SUConversiones.ConvierteADouble(dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells(2).Value)
            'nDiferenciaIngresoEgreso = (ConvierteADouble(dgResumenArqueo.Rows(lnPosicionDiferenciaIngresoEgreso).Cells(2).Value) + ConvierteADouble(diGastos.Text))
        End If
        If dgResumenArqueo.Rows.Count >= (lnPosicionSaldoFinal) Then
            dgResumenArqueo.Rows(lnPosicionSaldoFinal).Cells(3).Value = Format(nTotalMonto - nDiferenciaIngresoEgreso, "###,###,##0.#0")
        End If

    End Sub

    Private Sub dgxDetalleDolares_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgxDetalleDolares.CellValueChanged
        Dim nTotalMonto As Double = 0
        Dim nMonto As Double = 0
        Dim nCantidad As Integer = 0
        Dim nTotalCantidad As Integer = 0
        Dim Cambio, CalculoTotal As Double
        Try
            If e.ColumnIndex = 1 Then
                If Not dgxDetalleDolares.CurrentRow Is Nothing Then
                    If dgxDetalleDolares.CurrentRow.Cells.Count >= 2 Then
                        If ConvierteAInt(dgxDetalleDolares.CurrentRow.Cells(1).Value) > 0 Then
                            If ConvierteADouble(dgxDetalleDolares.CurrentRow.Cells(0).Value) <> 0 And ConvierteAInt(dgxDetalleDolares.CurrentRow.Cells(1).Value) <> 0 Then
                                dgxDetalleDolares.CurrentRow.Cells(2).Value = Format(ConvierteADouble(dgxDetalleDolares.CurrentRow.Cells(0).Value) * ConvierteADouble(dgxDetalleDolares.CurrentRow.Cells(1).Value), "#,##0.#0")
                            End If

                        Else
                            dgxDetalleDolares.CurrentRow.Cells(2).Value = 0
                        End If
                        nTotalMonto = 0
                        nMonto = 0
                        nCantidad = 0
                        nTotalCantidad = 0
                        For i As Integer = 0 To dgxDetalleDolares.Rows.Count - 3
                            nMonto = 0
                            nMonto = ConvierteADouble(dgxDetalleDolares.Rows(i).Cells(2).Value)
                            nCantidad = 0
                            nCantidad = ConvierteAInt(dgxDetalleDolares.Rows(i).Cells(1).Value)
                            nTotalMonto = nTotalMonto + nMonto
                            nTotalCantidad = nTotalCantidad + nCantidad
                        Next
                        If dgxDetalleDolares.Rows.Count >= 8 Then
                            If dgxDetalleDolares.Rows(8).Cells.Count >= 1 Then
                                If Not dgxDetalleDolares.Rows(8).Cells(2) Is Nothing Then
                                    dgxDetalleDolares.Rows(8).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                                End If
                                If Not dgxDetalleDolares.Rows(8).Cells(1) Is Nothing Then
                                    dgxDetalleDolares.Rows(8).Cells(1).Value = nTotalCantidad
                                End If
                            End If
                        End If
                        If dgxDetalleDolares.Rows.Count >= 9 Then
                            If dgxDetalleDolares.Rows(9).Cells.Count >= 1 Then
                                If Not dgxDetalleDolares.Rows(9).Cells(2) Is Nothing Then
                                    Cambio = diTasaCambio.Value
                                    CalculoTotal = nTotalMonto * Cambio
                                    'dgxDetalleDolares.Rows(9).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                                    dgxDetalleDolares.Rows(9).Cells(2).Value = Format(CalculoTotal, "#,##0.#0")
                                End If
                                If Not dgxDetalleDolares.Rows(9).Cells(1) Is Nothing Then
                                    dgxDetalleDolares.Rows(9).Cells(1).Value = nTotalCantidad
                                End If

                            End If
                        End If
                    End If
                End If
                CalculaSaldoFinal()
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function ObtieneDetalleMoneda() As List(Of SEInformacionMonetaria)
        Dim i As Integer = 0
        Dim objInformacionMonetaria As SEInformacionMonetaria = Nothing
        Dim lstObjInformacionMonetaria As List(Of SEInformacionMonetaria) = Nothing
        Try
            lstObjInformacionMonetaria = New List(Of SEInformacionMonetaria)
            If dgxDetalleDolares.Rows.Count > 0 Then
                For i = 0 To dgxDetalleDolares.Rows.Count - 2
                    objInformacionMonetaria = New SEInformacionMonetaria()
                    objInformacionMonetaria.Cantidad = SUConversiones.ConvierteAInt(dgxDetalleDolares.Rows(i).Cells(1).Value)
                    objInformacionMonetaria.DenominacionBillete = SUConversiones.ConvierteADouble(dgxDetalleDolares.Rows(i).Cells(0).Value)
                    objInformacionMonetaria.IdMoneda = "USD"
                    objInformacionMonetaria.Total = SUConversiones.ConvierteADouble(dgxDetalleDolares.Rows(i).Cells(2).Value)

                    lstObjInformacionMonetaria.Add(New SEInformacionMonetaria(objInformacionMonetaria))
                Next
            End If

            If dgxDetalleCordobas.Rows.Count > 0 Then
                For i = 0 To dgxDetalleCordobas.Rows.Count - 2
                    objInformacionMonetaria = New SEInformacionMonetaria()
                    objInformacionMonetaria.Cantidad = SUConversiones.ConvierteAInt(dgxDetalleCordobas.Rows(i).Cells(1).Value)
                    objInformacionMonetaria.DenominacionBillete = SUConversiones.ConvierteADouble(dgxDetalleCordobas.Rows(i).Cells(0).Value)
                    objInformacionMonetaria.IdMoneda = "COR"
                    objInformacionMonetaria.Total = SUConversiones.ConvierteADouble(dgxDetalleCordobas.Rows(i).Cells(2).Value)

                    lstObjInformacionMonetaria.Add(New SEInformacionMonetaria(objInformacionMonetaria))
                Next
            End If
            Return lstObjInformacionMonetaria
        Catch ex As Exception
            'MessageBoxEx.Show("Error al obtener informaci�n " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
        End Try
    End Function

    Private Sub cmdSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalir.Click
        Me.Close()
    End Sub

    Private Sub txtExNumeroFacturaInicial_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroFacturaInicial.Leave
        Dim Resultado As Object() = New Object(4) {}
        Try
            If txtExNumeroFacturaFinal.Text = "0" Then
                Exit Sub
            Else
                Resultado = ValidaNumeroFactura(txtExNumeroFacturaInicial.Text)
                If (Resultado(0) = 1) Then
                    MessageBoxEx.Show(Resultado(1), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtExNumeroFacturaInicial.Text = String.Empty
                    Exit Sub
                End If
                If (Resultado(2) = 1) Then
                    MessageBoxEx.Show(Resultado(3), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtExNumeroFacturaInicial.Text = String.Empty
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtExNumeroFacturaFinal_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroFacturaFinal.Leave
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicial As String = String.Empty
        Dim lsFechaFinal As String = String.Empty
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lnMontoTotalFactura As Double = 0
        Dim lnMontoTotalRecibo As Double = 0
        Try
            If txtExNumeroFacturaFinal.Text = "0" Then
                Exit Sub
            Else
                Select Case Operacion
                    Case Accion.Ingresar

                        Resultado = ValidaNumeroFactura(txtExNumeroFacturaFinal.Text)
                        If (Resultado(0) = 1) Then
                            MessageBoxEx.Show(Resultado(1), " Valida N�mero Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            txtExNumeroFacturaFinal.Text = String.Empty
                            Exit Sub
                        End If
                        If (Resultado(2) = 1) Then
                            MessageBoxEx.Show(Resultado(3), " Valida N�mero Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            txtExNumeroFacturaFinal.Text = String.Empty
                            Exit Sub
                        End If
                        lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                        lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                        lstDetalleCuadreCaja = ObtieneDetalleArqueo(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lsFechaInicial, lsFechaFinal)

                    Case Accion.Modificar

                        'lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                        'lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                        lstDetalleCuadreCaja = ObtieneDetalleArqueoRecalcular(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lnNumeroArqueo) 'ConvierteAInt(txtNumeroCuadreBuscar.Text))

                End Select

                LlenaDetalleArqueo(lstDetalleCuadreCaja)

                ObtieneMotoTotalCuadre(lstDetalleCuadreCaja, lnMontoTotalFactura, lnMontoTotalRecibo)
                SeteaMontosArqueoEnResumen(lnMontoTotalFactura, lnMontoTotalRecibo)
                CalculaSaldoFinal()
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtExNumeroReciboInicial_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroReciboInicial.Leave
        Dim Resultado As Object() = New Object(4) {}
        Try
            If txtExNumeroReciboFinal.Text = "0" Then
                Exit Sub
            Else
                Resultado = ValidaNumeroRecibo(txtExNumeroReciboInicial.Text)
                If (Resultado(0) = 1) Then
                    MessageBoxEx.Show(Resultado(1), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtExNumeroReciboInicial.Text = String.Empty
                    Exit Sub
                End If
                If (Resultado(2) = 1) Then
                    MessageBoxEx.Show(Resultado(3), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtExNumeroReciboInicial.Text = String.Empty
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtExNumeroReciboFinal_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExNumeroReciboFinal.Leave
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicial As String = String.Empty
        Dim lsFechaFinal As String = String.Empty
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lnMontoTotalFactura As Double = 0
        Dim lnMontoTotalRecibo As Double = 0
        Try
            If txtExNumeroReciboFinal.Text = "0" Then
                Exit Sub
            Else
                Select Case Operacion
                    Case Accion.Ingresar

                        Resultado = ValidaNumeroRecibo(txtExNumeroReciboInicial.Text)
                        If (Resultado(0) = 1) Then
                            MessageBoxEx.Show(Resultado(1), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            txtExNumeroReciboFinal.Text = String.Empty
                            Exit Sub
                        End If
                        If (Resultado(2) = 1) Then
                            MessageBoxEx.Show(Resultado(3), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            txtExNumeroReciboFinal.Text = String.Empty
                            Exit Sub
                        End If
                        lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                        lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                        lstDetalleCuadreCaja = ObtieneDetalleArqueo(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lsFechaInicial, lsFechaFinal)

                    Case Accion.Modificar

                        'lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                        'lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                        lstDetalleCuadreCaja = ObtieneDetalleArqueoRecalcular(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lnNumeroArqueo) 'ConvierteAInt(txtNumeroCuadreBuscar.Text))

                End Select

                LlenaDetalleArqueo(lstDetalleCuadreCaja)
                ObtieneMotoTotalCuadre(lstDetalleCuadreCaja, lnMontoTotalFactura, lnMontoTotalRecibo)
                SeteaMontosArqueoEnResumen(lnMontoTotalFactura, lnMontoTotalRecibo)
                CalculaSaldoFinal()
            End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GenerarDetalleArqueo()
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicial As String = String.Empty
        Dim lsFechaFinal As String = String.Empty
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lnMontoTotalFactura As Double = 0
        Dim lnMontoTotalRecibo As Double = 0
        Try
            'If (txtExNumeroReciboInicial.Text = "0" And txtExNumeroReciboFinal.Text = "0") Or (txtExNumeroFacturaInicial.Text = "0" And txtExNumeroFacturaFinal.Text = "0") Then
            '    Exit Sub
            'Else
            Select Case Operacion
                Case Accion.Ingresar

                    Resultado = ValidaNumeroRecibo(txtExNumeroReciboInicial.Text)
                    If (Resultado(0) = 1) Then
                        'MessageBoxEx.Show(Resultado(1), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        txtExNumeroReciboFinal.Text = 0 'String.Empty
                        'Exit Sub
                    End If
                    If (Resultado(2) = 1) Then
                        'MessageBoxEx.Show(Resultado(3), " Valida N�mero Recibo ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        txtExNumeroReciboFinal.Text = 0 ' String.Empty
                        'Exit Sub
                    End If
                    lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                    lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                    lstDetalleCuadreCaja = ObtieneDetalleArqueo(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lsFechaInicial, lsFechaFinal)

                Case Accion.Modificar

                    'lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                    'lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                    lstDetalleCuadreCaja = ObtieneDetalleArqueoRecalcular(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lnNumeroArqueo) 'ConvierteAInt(txtNumeroCuadreBuscar.Text))

            End Select

            LlenaDetalleArqueo(lstDetalleCuadreCaja)
            ObtieneMotoTotalCuadre(lstDetalleCuadreCaja, lnMontoTotalFactura, lnMontoTotalRecibo)
            SeteaMontosArqueoEnResumen(lnMontoTotalFactura, lnMontoTotalRecibo)
            CalculaSaldoFinal()
            'End If
        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub IngresaCuadreCajaCompleto()
        Dim objMaestroCuadreCaja As SECuadreCajaEncabezado
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        'Dim objReqCampo As Object = New Object(3) {}
        Dim objReqCampo As Object = Nothing
        Dim lstInformacionMonetaria As List(Of SEInformacionMonetaria) = Nothing
        Dim lnIdArqueo As Integer = 0
        Try
            objReqCampo = ValidarCampoRequerido()

            If objReqCampo(2) = True Then
                'Messa(MsgBox(objReqCampo(1), MsgBoxStyle.Information, "Validaci�n de datos sensibles"))
                MessageBoxEx.Show(objReqCampo(1), " Validaci�n de informaci�n requerida ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            lsFechaInicio = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            lsFechaFin = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            objMaestroCuadreCaja = New SECuadreCajaEncabezado()
            objMaestroCuadreCaja.IdMoneda = IdMoneda.MONEDA_LOCAL
            objMaestroCuadreCaja.IdEstado = IdEstadoCuadreCaja.CUADRE_CAJA_GUARDADO
            objMaestroCuadreCaja.IdSucursal = lngRegAgencia
            objMaestroCuadreCaja.NumeroCuadre = lbltxtNumeroArqueo.Text
            objMaestroCuadreCaja.FacturaContadoInicial = txtExNumeroFacturaInicial.Text
            objMaestroCuadreCaja.FacturaContadoFinal = txtExNumeroFacturaFinal.Text
            objMaestroCuadreCaja.FacturaCreditoInicial = String.Empty
            objMaestroCuadreCaja.FacturaCreditoFinal = String.Empty
            objMaestroCuadreCaja.ReciboInicialCredito = txtExNumeroReciboInicial.Text
            objMaestroCuadreCaja.ReciboFinalCredito = txtExNumeroReciboFinal.Text
            objMaestroCuadreCaja.ReciboInicialDebito = txtExNumeroReciboInicial.Text
            objMaestroCuadreCaja.ReciboFinalDebito = txtExNumeroReciboFinal.Text
            objMaestroCuadreCaja.SaldoInicial = ConvierteADouble(diSaldoInicial.Text)
            objMaestroCuadreCaja.OtrosIngresos = ConvierteADouble(diOtrosIngreso.Text)
            objMaestroCuadreCaja.SobranteCaja = ConvierteADouble(diSobranteCaja.Text)
            'If (dgResumenArqueo.Rows.Count >= lnPosicionSaldoFinal) Then
            '    objMaestroCuadreCaja.SaldoFinal = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionSaldoFinal).Cells("Totales").Value)
            'End If
            objMaestroCuadreCaja.MontoGastos = ConvierteADouble(diGastos.Text)
            objMaestroCuadreCaja.MontoDepositos = ConvierteADouble(diDeposito.Text)
            objMaestroCuadreCaja.IdUsuario = lngRegUsuario
            objMaestroCuadreCaja.FechaInicioCuadre = lsFechaInicio
            objMaestroCuadreCaja.FechaCuadreFin = lsFechaFin
            objMaestroCuadreCaja.IdUsuarioIngresa = lngRegUsuario
            objMaestroCuadreCaja.Observacion = txtObservaciones.Text
            objMaestroCuadreCaja.Tasa = diTasaCambio.Value

            If dgResumenArqueo.Rows.Count >= 3 Then
                objMaestroCuadreCaja.MontoTotalVentas = ConvierteADouble(dgResumenArqueo.Rows(1).Cells("utbMonto").Value)
                objMaestroCuadreCaja.MontoTotalRecibos = ConvierteADouble(dgResumenArqueo.Rows(2).Cells("utbMonto").Value)
            End If

            'If dgResumenArqueo.Rows.Count >= lnPosicionTotalIngreso Then
            '    objMaestroCuadreCaja.TotalIngresos = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionTotalIngreso).Cells("Totales").Value)
            'End If
            objMaestroCuadreCaja.TotalIngresos = objMaestroCuadreCaja.MontoTotalVentas + objMaestroCuadreCaja.MontoTotalRecibos
            'If dgResumenArqueo.Rows.Count >= lnPosicionTotalEgreso Then
            '    objMaestroCuadreCaja.TotalEgresos = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionTotalEgreso).Cells("Totales").Value)
            'End If
            objMaestroCuadreCaja.TotalEgresos = objMaestroCuadreCaja.MontoGastos
            'If dgResumenArqueo.Rows.Count >= lnPosicionTotalDineroEnCaja Then
            '    objMaestroCuadreCaja.MontoTotalDineroCaja = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionTotalDineroEnCaja).Cells("utbMonto").Value)
            'End If
            objMaestroCuadreCaja.MontoTotalDineroCaja = ObtieneDieneraEnCaja()

            objMaestroCuadreCaja.SaldoFinal = (objMaestroCuadreCaja.SaldoInicial + objMaestroCuadreCaja.SobranteCaja + objMaestroCuadreCaja.OtrosIngresos + objMaestroCuadreCaja.TotalIngresos) - (objMaestroCuadreCaja.TotalEgresos + objMaestroCuadreCaja.MontoDepositos)

            lstDetalleCuadreCaja = RNArqueoCaja.ObtieneFacturasYRecibosARealizarCierre(lngRegUsuario, objMaestroCuadreCaja.FacturaContadoInicial,
                                                                                       objMaestroCuadreCaja.FacturaContadoFinal, objMaestroCuadreCaja.ReciboInicialCredito,
                                                                                       objMaestroCuadreCaja.ReciboFinalCredito, lsFechaInicio, lsFechaFin)
            lstInformacionMonetaria = ObtieneDetalleMoneda()
            If (objMaestroCuadreCaja Is Nothing) And (lstDetalleCuadreCaja Is Nothing) And (lstInformacionMonetaria Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el cierre", " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
            lnIdArqueo = RNArqueoCaja.IngresaCuadreCajaCompleto(lngRegAgencia, objMaestroCuadreCaja, lstDetalleCuadreCaja, lstInformacionMonetaria)
            'MessageBoxEx.Show("Cierre Guardado exitosamente. Numero de cirre: " & lnIdArqueo, " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            MessageBoxEx.Show("Cierre Guardado exitosamente. Numero de cirre: " & lbltxtNumeroArqueo.Text, " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LimpiarControles()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el cierre", " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try
    End Sub

    Private Function ObtieneDieneraEnCaja() As Double
        Dim lnDineroEnCaja As Double = 0
        Dim lnMonto As Double = 0
        Dim lnMontoUSD As Double = 0
        Dim lnDineroEnCajaUSD As Double = 0
        Dim lnTasa As Double = 0
        Try
            If dgxDetalleCordobas.Rows.Count > 3 Then
                For i As Integer = 0 To dgxDetalleCordobas.Rows.Count - 3
                    lnMonto = 0
                    lnMonto = SUConversiones.ConvierteADouble(dgxDetalleCordobas.Rows(i).Cells(2).Value)
                    lnDineroEnCaja = lnDineroEnCaja + lnMonto
                Next
            End If

            If dgxDetalleDolares.Rows.Count > 3 Then
                For i As Integer = 0 To dgxDetalleDolares.Rows.Count - 3
                    lnMontoUSD = 0
                    lnMontoUSD = SUConversiones.ConvierteADouble(dgxDetalleDolares.Rows(i).Cells(2).Value)
                    lnDineroEnCajaUSD = lnDineroEnCajaUSD + lnMontoUSD
                Next
            End If
            lnTasa = SUConversiones.ConvierteADouble(diTasaCambio.Text)
            lnDineroEnCajaUSD = Math.Round((lnDineroEnCajaUSD * lnTasa), 2)
            lnDineroEnCaja = lnDineroEnCaja + lnDineroEnCajaUSD
        Catch ex As Exception
            lnDineroEnCaja = 0
        End Try
        Return lnDineroEnCaja
    End Function
    Private Sub RecalcularCuadreCajaCompleto()
        Dim objMaestroCuadreCaja As SECuadreCajaEncabezado
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        'Dim objReqCampo As Object = New Object(3) {}
        Dim objReqCampo As Object = Nothing
        Dim lstInformacionMonetaria As List(Of SEInformacionMonetaria) = Nothing
        Dim lnIdArqueo As Integer = 0
        Try
            objReqCampo = ValidarCampoRequerido()

            If objReqCampo(2) = True Then
                'Messa(MsgBox(objReqCampo(1), MsgBoxStyle.Information, "Validaci�n de datos sensibles"))
                MessageBoxEx.Show(objReqCampo(1), " Validaci�n de informaci�n requerida ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            lsFechaInicio = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            lsFechaFin = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            objMaestroCuadreCaja = New SECuadreCajaEncabezado()
            objMaestroCuadreCaja.IdCuadre = lnNumeroArqueo 'txtNumeroCuadreBuscar.Text
            objMaestroCuadreCaja.FacturaContadoInicial = txtExNumeroFacturaInicial.Text
            objMaestroCuadreCaja.FacturaContadoFinal = txtExNumeroFacturaFinal.Text
            objMaestroCuadreCaja.FacturaCreditoInicial = String.Empty
            objMaestroCuadreCaja.FacturaCreditoFinal = String.Empty
            objMaestroCuadreCaja.ReciboInicialCredito = txtExNumeroReciboInicial.Text
            objMaestroCuadreCaja.ReciboFinalCredito = txtExNumeroReciboFinal.Text
            objMaestroCuadreCaja.ReciboInicialDebito = txtExNumeroReciboInicial.Text
            objMaestroCuadreCaja.ReciboFinalDebito = txtExNumeroReciboFinal.Text
            objMaestroCuadreCaja.SaldoInicial = ConvierteADouble(diSaldoInicial.Text)
            objMaestroCuadreCaja.OtrosIngresos = ConvierteADouble(diOtrosIngreso.Text)
            objMaestroCuadreCaja.SobranteCaja = ConvierteADouble(diSobranteCaja.Text)
            'If (dgResumenArqueo.Rows.Count >= lnPosicionSaldoFinal) Then
            '    objMaestroCuadreCaja.SaldoFinal = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionSaldoFinal).Cells("Totales").Value)
            'End If
            objMaestroCuadreCaja.MontoGastos = ConvierteADouble(diGastos.Text)
            objMaestroCuadreCaja.MontoDepositos = ConvierteADouble(diDeposito.Text)
            objMaestroCuadreCaja.IdUsuario = lngRegUsuario
            objMaestroCuadreCaja.FechaCuadreFin = lsFechaFin
            objMaestroCuadreCaja.IdUsuarioModifica = lngRegUsuario
            objMaestroCuadreCaja.Observacion = txtObservaciones.Text
            objMaestroCuadreCaja.Tasa = diTasaCambio.Value

            If dgResumenArqueo.Rows.Count >= 3 Then
                objMaestroCuadreCaja.MontoTotalVentas = ConvierteADouble(dgResumenArqueo.Rows(1).Cells("utbMonto").Value)
                objMaestroCuadreCaja.MontoTotalRecibos = ConvierteADouble(dgResumenArqueo.Rows(2).Cells("utbMonto").Value)
            End If

            'If dgResumenArqueo.Rows.Count >= lnPosicionTotalIngreso Then
            '    objMaestroCuadreCaja.TotalIngresos = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionTotalIngreso).Cells("Totales").Value)
            'End If
            objMaestroCuadreCaja.TotalIngresos = objMaestroCuadreCaja.MontoTotalRecibos + objMaestroCuadreCaja.MontoTotalVentas
            'If dgResumenArqueo.Rows.Count >= lnPosicionTotalEgreso Then
            '    objMaestroCuadreCaja.TotalEgresos = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionTotalEgreso).Cells("Totales").Value)
            'End If
            objMaestroCuadreCaja.TotalEgresos = objMaestroCuadreCaja.MontoGastos
            'If dgResumenArqueo.Rows.Count >= lnPosicionTotalDineroEnCaja Then
            '    objMaestroCuadreCaja.MontoTotalDineroCaja = ConvierteADouble(dgResumenArqueo.Rows(lnPosicionTotalDineroEnCaja).Cells("utbMonto").Value)
            'End If
            objMaestroCuadreCaja.MontoTotalDineroCaja = ObtieneDieneraEnCaja()

            objMaestroCuadreCaja.SaldoFinal = (objMaestroCuadreCaja.SaldoInicial + objMaestroCuadreCaja.SobranteCaja + objMaestroCuadreCaja.OtrosIngresos + objMaestroCuadreCaja.TotalIngresos) - (objMaestroCuadreCaja.TotalEgresos + objMaestroCuadreCaja.MontoDepositos)

            lstDetalleCuadreCaja = RNArqueoCaja.ObtieneFacturasYRecibosARecalcular(lngRegUsuario, objMaestroCuadreCaja.FacturaContadoInicial,
                                                                                       objMaestroCuadreCaja.FacturaContadoFinal, objMaestroCuadreCaja.ReciboInicialCredito,
                                                                                       objMaestroCuadreCaja.ReciboFinalCredito, lnNumeroArqueo) 'ConvierteAInt(txtNumeroCuadreBuscar.Text))
            lstInformacionMonetaria = ObtieneDetalleMoneda()
            If (objMaestroCuadreCaja Is Nothing) And (lstDetalleCuadreCaja Is Nothing) And (lstInformacionMonetaria Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el cierre", " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
            lnIdArqueo = RNArqueoCaja.RecalcularCuadreCajaCompleto(lngRegAgencia, objMaestroCuadreCaja, lstDetalleCuadreCaja, lstInformacionMonetaria)
            'MessageBoxEx.Show("Cierre Guardado exitosamente. Numero de cirre: " & lnIdArqueo, " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            MessageBoxEx.Show("Cierre Guardado exitosamente. Numero de cirre: " & lbltxtNumeroArqueo.Text, " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LimpiarControles()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar el cierre", " Guardar cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try
    End Sub

    Private Function ValidarCampoRequerido() As Object()

        Dim objReqCampo As Object = New Object(3) {}
        Dim sNombreControlReq As String = String.Empty
        Dim ReqCampo As Boolean
        objReqCampo(0) = 0  'No hay campos que necesiten informacion
        objReqCampo(1) = ""
        objReqCampo(2) = False
        For Each controlGB As Control In Me.ugbIngresoDatos.Controls
            If (TypeOf (controlGB) Is DateTimeInput) Then ' Verifico que el control sea un textbox
                If CType(controlGB, DateTimeInput).AccessibleDescription = "Req" Then ' Le cambio el valor a la propiedad
                    If CType(controlGB, DateTimeInput).Value.ToString() = String.Empty Then
                        ' sNombreControlReq = sNombreControlReq & ", " & Mid$(CType(controlGB, DateTimeInput).Name.ToString(), 4, CType(controlGB, DateTimeInput).Name.Length)
                        If sNombreControlReq.Length <= 0 Then
                            sNombreControlReq = CType(controlGB, DateTimeInput).AccessibleName
                        Else
                            sNombreControlReq = sNombreControlReq & ", " & CType(controlGB, DateTimeInput).AccessibleName
                        End If
                        ReqCampo = True
                        objReqCampo(0) = 1
                        objReqCampo(1) = "La siguiente informaci�n es requerida: " & sNombreControlReq
                        objReqCampo(2) = True
                    End If
                End If
            End If
            If (TypeOf (controlGB) Is TextBoxX) Then ' Verifico que el control sea un textbox
                If CType(controlGB, TextBoxX).AccessibleDescription = "Req" Then ' Le cambio el valor a la propiedad
                    If CType(controlGB, TextBoxX).Text = String.Empty Or CType(controlGB, TextBoxX).Text = "-1" Then
                        ' sNombreControlReq = sNombreControlReq & ", " & Mid$(CType(controlGB, DateTimeInput).Name.ToString(), 4, CType(controlGB, DateTimeInput).Name.Length)
                        If sNombreControlReq.Length <= 0 Then
                            sNombreControlReq = CType(controlGB, TextBoxX).AccessibleName
                        Else
                            sNombreControlReq = sNombreControlReq & ", " & CType(controlGB, TextBoxX).AccessibleName
                        End If

                        ReqCampo = True
                        objReqCampo(0) = 1
                        objReqCampo(1) = "La siguiente informaci�n es requerida: " & sNombreControlReq
                        objReqCampo(2) = True
                    End If
                End If
            End If
        Next
        Return objReqCampo
    End Function

    Private Sub BloquearControles(ByVal Estado As Boolean)

        For Each controlGB As Control In Me.ugbIngresoDatos.Controls
            If (TypeOf (controlGB) Is DateTimeInput) Then ' Verifico que el control sea un textbox
                controlGB.Enabled = Estado
            End If

            If (TypeOf (controlGB) Is TextBoxX) Then ' Verifico que el control sea un textbox
                controlGB.Enabled = Estado
            End If
        Next

        diGastos.Enabled = Estado
        diDeposito.Enabled = Estado

        dgxDetalleCordobas.Enabled = Estado
        dgxDetalleDolares.Enabled = Estado


    End Sub

    Private Sub LimpiarControles()

        For Each controlGB As Control In Me.ugbIngresoDatos.Controls
            If (TypeOf (controlGB) Is DateTimeInput) Then ' Verifico que el control sea un textbox
                controlGB.Text = DateTime.Today()
            End If

            If (TypeOf (controlGB) Is TextBoxX) Then ' Verifico que el control sea un textbox
                controlGB.Text = ""
            End If
        Next

        UbicarAgencia(lngRegUsuario)
        ExisteArqueo = RNArqueoCaja.ValidaExisteArqueos(lngRegAgencia)
        If ExisteArqueo = 0 Then
            dtiFechaInicioArqueo.Value = DateTime.Today()
            dtiFechaFinArqueo.Value = DateTime.Today()
            diSaldoInicial.Enabled = True
            diOtrosIngreso.Enabled = True
        Else
            diSaldoInicial.Text = RNArqueoCaja.ObtieneSaldoInicial(lngRegAgencia)
        End If

        If dgxDetalleCordobas.Rows.Count > 0 Then
            dgxDetalleCordobas.Rows.Clear()
        End If
        If dgxDetalleDolares.Rows.Count > 0 Then
            dgxDetalleDolares.Rows.Clear()
        End If
        If dgResumenArqueo.Rows.Count > 0 Then
            dgResumenArqueo.Rows.Clear()
        End If


        ObtieneNumerFacturaYFechaInicial()
        ObtieneNumerReciboYFechaInicial()

        ConfiguracionInicialGridDetalleArqueo()

        LlenaGridResumenCuadre()

        LlenaGridDetalleCuadre(dgxDetalleCordobas)
        LlenaGridDetalleCuadreDolares(dgxDetalleDolares)
        ObtieneNumeroArqueo()
        lbltxtNumeroArqueo.Text = lsNumeroArqueo

        diGastos.Value = 0
        diDeposito.Value = 0
        diOtrosIngreso.Value = 0
        diSobranteCaja.Value = 0

        BloquearControles(True)


    End Sub

    Private Sub cmdGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGuardar.Click
        Dim objResult As Object = New Object(2) {}
        Try
            If txtExNumeroFacturaFinal.Text = "0" And txtExNumeroFacturaInicial.Text = "0" And txtExNumeroReciboFinal.Text = "0" And txtExNumeroReciboInicial.Text = "0" Then
                MsgBox("No Puede Ingresar un Cuadre sin Recibos y Facturas..!", MsgBoxStyle.Critical, "Ingresar Arqueo de Caja")
                Exit Sub
            Else
                objResult = RNArqueoCaja.ValidaArqueoAbierto()
                Select Case Operacion
                    Case Accion.Ingresar
                        If objResult(0) = 1 Then
                            MsgBox("El Arqueo " + objResult(1).ToString() + " Aun esta Abierto debe cerrarse primero..!", MsgBoxStyle.Exclamation, "Guardar Arqueo de Caja")
                            Exit Sub
                        Else
                            IngresaCuadreCajaCompleto()
                        End If
                    Case Accion.Modificar
                        RecalcularCuadreCajaCompleto()
                End Select

            End If

        Catch ex As Exception
            MessageBoxEx.Show("Error al realizar carga de la forma " & ex.Message, " Arqueo de caja", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub diSaldoInicial_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diSaldoInicial.Leave
        'SeteaUnValorEnArqueoEnResumen(ConvierteADouble(diSaldoInicial.Text), 1, "I")
        'SeteaUnValorEnArqueoEnResumen(dMontoTotalFactura, 1, "I")
    End Sub

    Private Sub diOtrosIngreso_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diOtrosIngreso.Leave
        'SeteaUnValorEnArqueoEnResumen(ConvierteADouble(diOtrosIngreso.Text), lnPosicionMontoRecibo, "R")
        'SeteaUnValorEnArqueoEnResumen(dMontoTotalRecibos, lnPosicionMontoRecibo, "R")
        If diOtrosIngreso.Text = String.Empty Then
            Exit Sub
        Else
            dgResumenArqueo.Rows(12).Cells("utbMonto").Value = Format(ConvierteADouble(diOtrosIngreso.Text), "###,###,###,###.00")
        End If

    End Sub

    Private Sub diGastos_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diGastos.Leave
        SeteaUnValorEnArqueoEnResumen(ConvierteADouble(diGastos.Text), lnPosicionGastos, "E")
    End Sub

    Private Sub diDeposito_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diDeposito.Leave
        'SeteaUnValorEnArqueoEnResumen(ConvierteADouble(diDeposito.Text), 7, "E")
    End Sub

    Private Sub btnXExportarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnXExportarExcel.Click
        Dim dlgNew As New OpenFileDialog
        Try
            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MessageBoxEx.Show("No hay archivo que importar. Verifique", "Error en la Importaci�n", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

        Catch ex As Exception
            ' MessageBoxEx("No se pudo obtener la ruta del archivo",)
        End Try
    End Sub

    Private Sub dtiFechaInicioArqueo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtiFechaInicioArqueo.ValueChanged
        Dim FechaInicial As String = String.Empty
        Dim FechaFinal As String = String.Empty
        Dim Resultado As Object() = New Object(2) {}

        FechaInicial = SUConversiones.FormateaFechaNumerica(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
        FechaFinal = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

        'txtExNumeroFacturaInicial.Text = RNArqueoCaja.ObtenerPrimeraFacturaxFecha(FechaInicial)
        'txtExNumeroFacturaFinal.Text = RNArqueoCaja.ObtenerPrimeraFacturaxFecha(FechaFinal)
    End Sub

    Private Sub ObtieneNumerFacturaYFechaInicial()
        Dim FechaInicial As String = String.Empty
        Dim FechaFinal As String = String.Empty
        Dim ldFechaInicio As Date
        Dim ldFechaFin As Date
        Dim Resultado As Object()
        Try
            Resultado = New Object(2) {}
            FechaInicial = SUConversiones.FormateaFechaNumerica(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            FechaFinal = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            Resultado = RNArqueoCaja.ObtenerPrimeraFactura(lngRegUsuario)

            txtExNumeroFacturaInicial.Text = Resultado(0)

            ldFechaInicio = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            dtiFechaInicioArqueo.Value = ldFechaInicio

            Resultado = RNArqueoCaja.ObtenerUltimaFactura(lngRegUsuario)

            txtExNumeroFacturaFinal.Text = Resultado(0)
            ldFechaFin = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            dtiFechaFinArqueo.Value = ldFechaFin

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ObtieneNumerReciboYFechaInicial()
        Dim FechaInicial As String = String.Empty
        Dim FechaFinal As String = String.Empty
        Dim ldFechaInicio As Date
        Dim ldFechaFin As Date
        Dim Resultado As Object()
        Try
            Resultado = New Object(2) {}
            FechaInicial = SUConversiones.FormateaFechaNumerica(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            FechaFinal = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            Resultado = RNArqueoCaja.ObtenerPrimerRecibo(lngRegUsuario)

            txtExNumeroReciboInicial.Text = Resultado(0)

            'ldFechaInicio = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            ldFechaInicio = RNArqueoCaja.ObtenerFechaGlobal(FechaInicial, Resultado(1).ToString(), "I")

            dtiFechaInicioArqueo.Value = ldFechaInicio

            Resultado = RNArqueoCaja.ObtenerUltimoRecibo(lngRegUsuario)

            txtExNumeroReciboFinal.Text = Resultado(0)
            'ldFechaFin = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            ldFechaFin = RNArqueoCaja.ObtenerFechaGlobal(FechaFinal, Resultado(1).ToString(), "F")
            dtiFechaFinArqueo.Value = ldFechaFin

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ObtenerSaldoTotalFacturaRecibosCuadre()
        Dim Resultado As Object()
        Try
            Resultado = New Object(2) {}

            Resultado = RNArqueoCaja.ObtenerSaldoTotalFacturaRecibosCuadre(lngRegUsuario, txtExNumeroReciboInicial.Text.Trim(), txtExNumeroReciboFinal.Text.Trim(), txtExNumeroFacturaInicial.Text.Trim(), txtExNumeroFacturaFinal.Text.Trim())

            dMontoTotalRecibos = Resultado(0)
            dMontoTotalFactura = Resultado(1)

            SeteaUnValorEnArqueoEnResumen(dMontoTotalFactura, 1, "I")
            SeteaUnValorEnArqueoEnResumen(dMontoTotalRecibos, lnPosicionMontoRecibo, "R")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ObtieneNumeroFacturaInicialYReciboInicialxFecha()
        Dim FechaInicial As String = String.Empty
        Dim FechaFinal As String = String.Empty
        Dim ldFechaInicio As Date
        '    Dim ldFechaFin As Date
        Dim Resultado As Object()
        Try
            Resultado = New Object(2) {}
            FechaInicial = SUConversiones.FormateaFechaNumerica(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            FechaFinal = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            Resultado = RNArqueoCaja.ObtenerPrimeraFacturaxFecha(lngRegUsuario, FechaInicial)

            'txtExNumeroReciboInicial.Text = Resultado(0)
            txtExNumeroFacturaInicial.Text = Resultado(0)
            'ldFechaInicio = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            ldFechaInicio = RNArqueoCaja.ObtenerFechaGlobal(FechaInicial, Resultado(1).ToString(), "I")

            dtiFechaInicioArqueo.Value = ldFechaInicio

            Resultado = RNArqueoCaja.ObtenerPrimerReciboxFecha(lngRegUsuario, FechaInicial)

            'txtExNumeroReciboFinal.Text = Resultado(0)
            txtExNumeroReciboInicial.Text = Resultado(0)
            'ldFechaFin = RNArqueoCaja.ObtenerFechaGlobal(FechaFinal, Resultado(1).ToString(), "F")
            'dtiFechaFinArqueo.Value = ldFechaFin

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ObtieneInformacionCuadreGuardadoACerrar(ByVal IdAgencia As Integer)
        Dim dtInfCuadre As DataTable = Nothing
        Dim Numero As Integer = 0
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Try
            dtInfCuadre = Nothing
            Numero = lnNumeroArqueo 'ConvierteAInt(txtNumeroCuadreBuscar.Text)

            dtInfCuadre = RNArqueoCaja.ObtieneInformacionCuadreGuardadoACerrar(Numero, IdAgencia)
            If dtInfCuadre.Rows.Count = 0 Then
                MsgBox("No se Encontro Informacion para este Numero de Cuadre de Caja Verfique..!", MsgBoxStyle.Exclamation, "Buscar Informacion de Cuadre de Caja")
                BloquearControles(True)
            Else
                If dtInfCuadre.Rows(0).Item("IdEstado").ToString = "C" Then
                    MsgBox("El Arqueo " & dtInfCuadre.Rows(0).Item("NumeroCuadre").ToString & " Esta Cerrado no Puede Realizar Operaciones", MsgBoxStyle.Information, "Cerrar Arqueo de Caja")
                    Exit Sub
                Else
                    diSaldoInicial.Text = dtInfCuadre.Rows(0).Item("SaldoInicial").ToString
                    diOtrosIngreso.Text = dtInfCuadre.Rows(0).Item("OtrosIngresos").ToString
                    diSobranteCaja.Text = dtInfCuadre.Rows(0).Item("SobranteCaja").ToString
                    diGastos.Text = dtInfCuadre.Rows(0).Item("MontoGastos").ToString
                    diDeposito.Text = dtInfCuadre.Rows(0).Item("MontoDepositos").ToString
                    txtExNumeroFacturaInicial.Text = dtInfCuadre.Rows(0).Item("FacturaContadoInicial").ToString
                    txtExNumeroFacturaFinal.Text = dtInfCuadre.Rows(0).Item("FacturaContadoFinal").ToString
                    txtExNumeroReciboInicial.Text = dtInfCuadre.Rows(0).Item("ReciboInicialCredito").ToString
                    txtExNumeroReciboFinal.Text = dtInfCuadre.Rows(0).Item("ReciboFinalCredito").ToString
                    txtObservaciones.Text = dtInfCuadre.Rows(0).Item("Observacion").ToString
                    lbltxtNumeroArqueo.Text = dtInfCuadre.Rows(0).Item("NumeroCuadre").ToString
                    dgResumenArqueo.Rows(12).Cells("utbMonto").Value = Format(dtInfCuadre.Rows(0).Item("OtrosIngresos"), "##,#0.#0")
                    dgResumenArqueo.Rows(13).Cells("utbMonto").Value = Format(dtInfCuadre.Rows(0).Item("SobranteCaja"), "##,#0.#0")
                    dgResumenArqueo.Rows(1).Cells("utbMonto").Value = dtInfCuadre.Rows(0).Item("MontoTotalVentas")
                    dgResumenArqueo.Rows(2).Cells("utbMonto").Value = dtInfCuadre.Rows(0).Item("MontoTotalRecibos")
                    dgResumenArqueo.Rows(3).Cells("Totales").Value = dtInfCuadre.Rows(0).Item("MontoTotalIngreso")
                    dgResumenArqueo.Rows(8).Cells("utbMonto").Value = dtInfCuadre.Rows(0).Item("MontoTotalDineroCaja")

                    dgxDetalleCordobas.Rows(0).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_500")
                    dgxDetalleCordobas.Rows(0).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_500") * dgxDetalleCordobas.Rows(0).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(1).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_200")
                    dgxDetalleCordobas.Rows(1).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_200") * dgxDetalleCordobas.Rows(1).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(2).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_100")
                    dgxDetalleCordobas.Rows(2).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_100") * dgxDetalleCordobas.Rows(2).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(3).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_50")
                    dgxDetalleCordobas.Rows(3).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_50") * dgxDetalleCordobas.Rows(3).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(4).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_20")
                    dgxDetalleCordobas.Rows(4).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_20") * dgxDetalleCordobas.Rows(4).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(5).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_10")
                    dgxDetalleCordobas.Rows(5).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_10") * dgxDetalleCordobas.Rows(5).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(6).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_5")
                    dgxDetalleCordobas.Rows(6).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_5") * dgxDetalleCordobas.Rows(6).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(7).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_1")
                    dgxDetalleCordobas.Rows(7).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_1") * dgxDetalleCordobas.Rows(7).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(8).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_050")
                    dgxDetalleCordobas.Rows(8).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_050") * dgxDetalleCordobas.Rows(8).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(9).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_025")
                    dgxDetalleCordobas.Rows(9).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_010") * dgxDetalleCordobas.Rows(9).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(10).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_010")
                    dgxDetalleCordobas.Rows(10).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_010") * dgxDetalleCordobas.Rows(10).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(11).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_005")
                    dgxDetalleCordobas.Rows(11).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_005") * dgxDetalleCordobas.Rows(11).Cells(0).Value, "#,##0.#0")
                    dgxDetalleCordobas.Rows(12).Cells("Cantidad").Value = dtInfCuadre.Rows(0).Item("DenC_001")
                    dgxDetalleCordobas.Rows(12).Cells("Total").Value = Format(dtInfCuadre.Rows(0).Item("DenC_001") * dgxDetalleCordobas.Rows(12).Cells(0).Value, "#,##0.#0")

                    'dgxDetalleCordobas.Rows(13).Cells("Total").Value = dtInfCuadre.Rows(0).Item("MontoTotalDineroCaja")
                    'dgxDetalleCordobas.Rows(14).Cells("Total").Value = dtInfCuadre.Rows(0).Item("MontoTotalDineroCaja")

                    dgxDetalleCordobas.Rows(13).Cells("Total").Value = dtInfCuadre.Rows(0).Item("TotalEfectivoCOR")
                    dgxDetalleCordobas.Rows(14).Cells("Total").Value = dtInfCuadre.Rows(0).Item("TotalEfectivoCOR")

                    dgxDetalleDolares.Rows(0).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_100")
                    dgxDetalleDolares.Rows(0).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_100") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(0).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(1).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_50")
                    dgxDetalleDolares.Rows(1).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_50") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(1).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(2).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_20")
                    dgxDetalleDolares.Rows(2).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_20") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(2).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(3).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_10")
                    dgxDetalleDolares.Rows(3).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_10") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(3).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(4).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_5")
                    dgxDetalleDolares.Rows(4).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_5") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(4).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(5).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_1")
                    dgxDetalleDolares.Rows(5).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_1") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(5).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(6).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_050")
                    dgxDetalleDolares.Rows(6).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_050") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(6).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(7).Cells("CantidadUSD").Value = dtInfCuadre.Rows(0).Item("DenU_010")
                    dgxDetalleDolares.Rows(7).Cells("TotalUSD").Value = Format(dtInfCuadre.Rows(0).Item("DenU_010") * SUConversiones.ConvierteADecimal(dgxDetalleDolares.Rows(7).Cells(0).Value), "#,##0.#0")
                    dgxDetalleDolares.Rows(8).Cells("TotalUSD").Value = dtInfCuadre.Rows(0).Item("TotalEfectivoUSD")
                    dgxDetalleDolares.Rows(9).Cells("TotalUSD").Value = dtInfCuadre.Rows(0).Item("TotalEfecitvoUSDConvertidoCOR")
                    diTasaCambio.Text = dtInfCuadre.Rows(0).Item("Tasa")

                    SeteaUnValorEnArqueoEnResumen(SUConversiones.ConvierteADouble(diGastos.Text), lnPosicionGastos, "E")

                    CalculaSaldoFinal()
                    lstDetalleCuadreCaja = ObtieneDetalleArqueoRecalcular(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lnNumeroArqueo)
                    LlenaDetalleArqueo(lstDetalleCuadreCaja)
                    ' ObtieneMotoTotalCuadre(lstDetalleCuadreCaja, lnMontoTotalFactura, lnMontoTotalRecibo)

                    BloquearControles(False)

                    lnNumeroArqueo = SUConversiones.ConvierteAInt(txtNumeroCuadreBuscar.Text)
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error  Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try

    End Sub

    Private Sub dtiFechaFinArqueo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtiFechaFinArqueo.TextChanged
        Dim lsFechaFinalFormateada As String = String.Empty
        Dim lsFechaInicialFormateada As String = String.Empty
        Dim Resultado As Object() = New Object(4) {}
        Dim lsFechaInicial As String = String.Empty
        Dim lsFechaFinal As String = String.Empty
        Dim lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle) = Nothing
        Dim lnMontoTotalFactura As Double = 0
        Dim lnMontoTotalRecibo As Double = 0
        Try
            If txtExNumeroFacturaFinal.Text = "0" Then
                Exit Sub
            Else
                Select Case Operacion
                    Case Accion.Modificar
                        Exit Sub
                    Case Accion.Ingresar
                        Resultado = ValidaNumeroFactura(txtExNumeroFacturaFinal.Text)
                        If (Resultado(0) = 1) Then
                            MessageBoxEx.Show(Resultado(1), " Valida N�mero Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            txtExNumeroFacturaFinal.Text = String.Empty
                            Exit Sub
                        End If
                        If (Resultado(2) = 1) Then
                            MessageBoxEx.Show(Resultado(3), " Valida N�mero Factura ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            txtExNumeroFacturaFinal.Text = String.Empty
                            Exit Sub
                        End If

                        lsFechaInicialFormateada = SUConversiones.FormateaFechaNumerica(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                        lsFechaFinalFormateada = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                        'txtExNumeroFacturaInicial.Text = RNArqueoCaja.ObtenerPrimeraFacturaxFecha(lsFechaInicialFormateada)
                        'txtExNumeroFacturaFinal.Text = RNArqueoCaja.ObtenerUltimaFacturaxFecha(lsFechaFinalFormateada)

                        'lsFechaInicial = SUConversiones.FormateaFechaParaBD(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
                        'lsFechaFinal = SUConversiones.FormateaFechaParaBD(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

                        'lstDetalleCuadreCaja = ObtieneDetalleArqueo(txtExNumeroFacturaInicial.Text, txtExNumeroFacturaFinal.Text, txtExNumeroReciboInicial.Text, txtExNumeroReciboFinal.Text, lsFechaInicial, lsFechaFinal)
                        'LlenaDetalleArqueo(lstDetalleCuadreCaja)

                        'ObtieneMotoTotalCuadre(lstDetalleCuadreCaja, lnMontoTotalFactura, lnMontoTotalRecibo)
                        'SeteaMontosArqueoEnResumen(lnMontoTotalFactura, lnMontoTotalRecibo)
                        'CalculaSaldoFinal()
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error  Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try


    End Sub

    Private Sub dtiFechaFinArqueo_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtiFechaFinArqueo.Leave
        Dim FechaInicial As String = String.Empty
        Dim FechaFinal As String = String.Empty

        Try
            FechaInicial = SUConversiones.FormateaFechaNumerica(dtiFechaInicioArqueo.Value.Day, dtiFechaInicioArqueo.Value.Month, dtiFechaInicioArqueo.Value.Year)
            FechaFinal = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            'txtExNumeroFacturaInicial.Text = RNArqueoCaja.ObtenerPrimeraFacturaxFecha(FechaInicial)
            'txtExNumeroFacturaFinal.Text = RNArqueoCaja.ObtenerPrimeraFacturaxFecha(FechaFinal)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try


    End Sub

    Private Sub diSaldoInicial_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diSaldoInicial.ValueChanged

    End Sub

    Private Sub dtiFechaInicioArqueo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtiFechaInicioArqueo.TextChanged

        Try
            If ExisteArqueo <> 0 Then
                ObtieneNumeroFacturaInicialYReciboInicialxFecha()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try


    End Sub

    Private Sub tiResumenArqueo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tiResumenArqueo.Click
        Try
            SeteaUnValorEnArqueoEnResumen(ConvierteADouble(0), 9, "D") 'los valores de los parametros se sustituyen en el metodo
            SeteaUnValorEnArqueoEnResumen(ConvierteADouble(0), 18, "SF") 'los valores de los parametros se sustituyen en el metodo

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try

    End Sub

    Private Sub txtNumeroCuadreBuscar_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    End Sub

    Private Sub txtNumeroCuadreBuscar_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroCuadreBuscar.KeyDown
        Dim IdAgencia As Integer = 0
        Try
            IdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
            IdAgen = IdAgencia
            lnNumeroArqueo = SUConversiones.ConvierteAInt(txtNumeroCuadreBuscar.Text)
            If e.KeyCode = Keys.Enter Then
                ObtieneInformacionCuadreGuardadoACerrar(IdAgencia)
                'GenerarDetalleArqueo()
                'MsgBox("Busqueda de Cuadre de Caja por Numero", MsgBoxStyle.Critical, "Buscar Cuadre")
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try

    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Dim Resp, NumCuadre As Integer
        Dim rpt As New actrptViewer


        Resp = MsgBox("�Esta Seguro de Cerrar el Cuadre de Caja " & lbltxtNumeroArqueo.Text & " ?", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Cierre de Caja")
        Try
            NumCuadre = lnNumeroArqueo 'ConvierteAInt(txtNumeroCuadreBuscar.Text)
            If Resp = 2 Then
                MsgBox("Proceso de Cerrar Cuadre de Caja Cancelado", MsgBoxStyle.Information, "Cerrar Caja")
                Exit Sub
            Else
                If RNArqueoCaja.ValidaArqueoACerrar(lngRegAgencia, lbltxtNumeroArqueo.Text) = 1 Then
                    RNArqueoCaja.CerrarCuadreCaja(lngRegAgencia, NumCuadre, lngRegUsuario)
                    MsgBox("Cerrar Cuadre de Caja Completado con Exito", MsgBoxStyle.Information, "Cerrar Caja")
                    LimpiarControles()

                    intRptCuadreCaja = 1
                    strQuery = "spOtieneInformacionCuadreGuardadoACerrar " & NumCuadre & ", " & IdAgen
                    rpt.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                    rpt.Show()
                Else
                    MsgBox("El Arqueo de Caja que Intenta Cerrar no Existe o ya esta Cerrado..!", MsgBoxStyle.Exclamation, "Cerrar Arqueo de Caja")
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try

    End Sub

    Private Sub cmdRecalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRecalcular.Click
        Dim ldFechaFin As Date
        Dim FechaFinal As String = String.Empty
        Dim Resultado As Object()

        Try


            Resultado = New Object(2) {}

            Operacion = 2 'Valor para Modificar un Arqueo
            Recalcula = 1 'Valor en Uno significa que se recalculara el arqueo y se actualizar�

            BloquearControles(True)

            'LimpiarControles()

            FechaFinal = SUConversiones.FormateaFechaNumerica(dtiFechaFinArqueo.Value.Day, dtiFechaFinArqueo.Value.Month, dtiFechaFinArqueo.Value.Year)

            Resultado = RNArqueoCaja.ObtenerUltimaFacturaRecalcular(lngRegUsuario, lnNumeroArqueo)  'ConvierteAInt(txtNumeroCuadreBuscar.Text))

            txtExNumeroFacturaFinal.Text = Resultado(0)
            ldFechaFin = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            dtiFechaFinArqueo.Value = ldFechaFin

            Resultado = RNArqueoCaja.ObtenerPrimeraFacturaRecalcular(lngRegUsuario, lnNumeroArqueo)  'ConvierteAInt(txtNumeroCuadreBuscar.Text))
            txtExNumeroFacturaInicial.Text = Resultado(0)

            Resultado = RNArqueoCaja.ObtenerUltimoReciboRecalcular(lngRegUsuario, lnNumeroArqueo) ' ConvierteAInt(txtNumeroCuadreBuscar.Text))

            txtExNumeroReciboFinal.Text = Resultado(0)
            'ldFechaFin = New Date(SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(0, 4)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(4, 2)), SUConversiones.ConvierteADouble(Resultado(1).ToString().Substring(6, 2)))
            ldFechaFin = RNArqueoCaja.ObtenerFechaGlobal(FechaFinal, Resultado(1).ToString(), "F")
            dtiFechaFinArqueo.Value = ldFechaFin

            Resultado = RNArqueoCaja.ObtenerPrimerReciboRecalcular(lngRegUsuario, lnNumeroArqueo) ' ConvierteAInt(txtNumeroCuadreBuscar.Text))
            txtExNumeroReciboInicial.Text = Resultado(0)

            'diDeposito.Enabled = True
            'diGastos.Enabled = True
            'dgxDetalleCordobas.Enabled = True
            'dgxDetalleDolares.Enabled = True
            txtExNumeroFacturaInicial.Enabled = False
            'txtExNumeroReciboFinal.Enabled = True
            txtExNumeroReciboInicial.Enabled = False
            diTasaCambio.Enabled = True

            'If dgxDetalleCordobas.Rows.Count > 0 Then
            '    dgxDetalleCordobas.Rows.Clear()
            'End If

            'If dgxDetalleDolares.Rows.Count > 0 Then
            '    dgxDetalleDolares.Rows.Clear()
            'End If


            'LlenaGridDetalleCuadre(dgxDetalleCordobas)
            'LlenaGridDetalleCuadreDolares(dgxDetalleDolares)

            GenerarDetalleArqueo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try


    End Sub

    Private Sub diTasaCambio_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diTasaCambio.Leave
        Dim Cambio, CalculoTotal, nTotalMonto As Double
        Try


            If dgxDetalleDolares.Rows.Count >= 9 Then
                If dgxDetalleDolares.Rows(9).Cells.Count >= 1 Then
                    If Not dgxDetalleDolares.Rows(9).Cells(2) Is Nothing Then
                        Cambio = diTasaCambio.Value
                        nTotalMonto = dgxDetalleDolares.Rows(8).Cells(2).Value
                        CalculoTotal = nTotalMonto * Cambio
                        'dgxDetalleDolares.Rows(9).Cells(2).Value = Format(nTotalMonto, "#,##0.#0")
                        dgxDetalleDolares.Rows(9).Cells(2).Value = Format(CalculoTotal, "#,##0.#0")
                    End If
                    CalculaSaldoFinal()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al Intentar Cerrar Cuadre de Caja..", " Guardar Cierre ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Throw ex
        End Try

    End Sub

    Private Sub diGastos_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles diGastos.KeyDown
        If e.KeyCode = Keys.Enter Then
            diDeposito.Focus()
        End If
    End Sub

    Private Sub diDeposito_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles diDeposito.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtObservaciones.Focus()
        End If
    End Sub

    Private Sub txtObservaciones_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtObservaciones.KeyDown
        If e.KeyCode = Keys.Enter Then
            dgxDetalleCordobas.Focus()
        End If
    End Sub

    Private Sub diSobranteCaja_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles diSobranteCaja.KeyDown
        If e.KeyCode = Keys.Enter Then
            diGastos.Focus()
        End If
    End Sub

    Private Sub diSobranteCaja_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles diSobranteCaja.Leave
        If diSobranteCaja.Text = String.Empty Then
            Exit Sub
        Else
            dgResumenArqueo.Rows(13).Cells("utbMonto").Value = Format(ConvierteADouble(diSobranteCaja.Text), "###,###,###,###.00")
        End If

    End Sub

End Class
