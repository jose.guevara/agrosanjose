<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecibosCuentasXPagar
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecibosCuentasXPagar))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.CmdAnular = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdBuscar = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdImprimir = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox20 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCodigoProveedor = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtRefElectronica = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.ddlSerie = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtBanco = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtDescripcionRecibo = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.txtNumeroRecibido = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ddlFormaPago = New System.Windows.Forms.ComboBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtNumeroTarjeta = New System.Windows.Forms.TextBox()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.DTPFechaRecibo = New System.Windows.Forms.DateTimePicker()
        Me.txtNumeroRecibo = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvxPagosFacturas = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.NumFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEmis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaVenc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoFact = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcDescuentoFact = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.MontoDescuentoFact = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Monto = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.NvoSaldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Interes = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.MantValor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.PorAplicar = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvxPagosFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.CmdAnular, Me.cmdiLimpiar, Me.cmdBuscar, Me.cmdImprimir, Me.cmdiSalir, Me.LabelItem1, Me.LabelItem3, Me.LabelItem4, Me.LabelItem5, Me.LabelItem2})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(1201, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 35
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        Me.cmdOK.Visible = False
        '
        'CmdAnular
        '
        Me.CmdAnular.Image = CType(resources.GetObject("CmdAnular.Image"), System.Drawing.Image)
        Me.CmdAnular.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.CmdAnular.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.CmdAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.CmdAnular.Name = "CmdAnular"
        Me.CmdAnular.Text = "Anular<F3>"
        Me.CmdAnular.Tooltip = "Anular Recibo"
        Me.CmdAnular.Visible = False
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'cmdBuscar
        '
        Me.cmdBuscar.Image = CType(resources.GetObject("cmdBuscar.Image"), System.Drawing.Image)
        Me.cmdBuscar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdBuscar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdBuscar.Name = "cmdBuscar"
        Me.cmdBuscar.Text = "Buscar<F6>"
        Me.cmdBuscar.Tooltip = "Extraer datos del recibo"
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Image = CType(resources.GetObject("cmdImprimir.Image"), System.Drawing.Image)
        Me.cmdImprimir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimir.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.cmdImprimir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Text = "Imprimir<F8>"
        Me.cmdImprimir.Tooltip = "Imprimir recibos"
        Me.cmdImprimir.Visible = False
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = "  "
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "  "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox20)
        Me.GroupBox1.Controls.Add(Me.TextBox19)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.TextBox15)
        Me.GroupBox1.Controls.Add(Me.TextBox14)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox8)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtCodigoProveedor)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(11, 75)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1161, 125)
        Me.GroupBox1.TabIndex = 36
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Proveedor"
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(589, 96)
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(104, 20)
        Me.TextBox20.TabIndex = 8
        Me.TextBox20.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(477, 96)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.Size = New System.Drawing.Size(104, 20)
        Me.TextBox19.TabIndex = 7
        Me.TextBox19.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(357, 96)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(119, 13)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Saldo Pendiente C$"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(201, 21)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(61, 21)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "<F5> AYUDA"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(656, 72)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(8, 20)
        Me.TextBox15.TabIndex = 19
        Me.TextBox15.Tag = "C�digo del Cliente"
        Me.TextBox15.Visible = False
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(640, 72)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(8, 20)
        Me.TextBox14.TabIndex = 18
        Me.TextBox14.Tag = "C�digo del Cliente"
        Me.TextBox14.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(477, 72)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(208, 20)
        Me.TextBox5.TabIndex = 4
        Me.TextBox5.Tag = "Vendedor del Cliente"
        Me.TextBox5.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(361, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Negocio"
        Me.Label4.Visible = False
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(214, 96)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(104, 20)
        Me.TextBox8.TabIndex = 6
        Me.TextBox8.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(96, 96)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(104, 20)
        Me.TextBox7.TabIndex = 5
        Me.TextBox7.Tag = "Saldo total del Cliente en Moneda Nacional"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(477, 72)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(208, 20)
        Me.TextBox6.TabIndex = 12
        Me.TextBox6.Tag = "Vendedor del Cliente"
        Me.TextBox6.Visible = False
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(96, 72)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(224, 20)
        Me.TextBox4.TabIndex = 3
        Me.TextBox4.Tag = "Departamento donde reside negocio/habitaci�n del cliente"
        Me.TextBox4.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(97, 48)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(677, 20)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Tag = "Direcci�n del Cliente"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(268, 22)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(505, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Nombre del Cliente"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Saldo C$"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(336, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        Me.Label5.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Departamento"
        Me.Label3.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Direcci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 110
        Me.Label1.Text = "C�digo"
        '
        'txtCodigoProveedor
        '
        Me.txtCodigoProveedor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoProveedor.Location = New System.Drawing.Point(97, 21)
        Me.txtCodigoProveedor.Name = "txtCodigoProveedor"
        Me.txtCodigoProveedor.Size = New System.Drawing.Size(105, 20)
        Me.txtCodigoProveedor.TabIndex = 0
        Me.txtCodigoProveedor.Tag = "C�digo del Proveedor"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtRefElectronica)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.ddlSerie)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.Label23)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.txtBanco)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.txtDescripcionRecibo)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.TextBox17)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.TextBox16)
        Me.GroupBox2.Controls.Add(Me.txtNumeroRecibido)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.ddlFormaPago)
        Me.GroupBox2.Controls.Add(Me.txtDescripcion)
        Me.GroupBox2.Controls.Add(Me.txtNumeroTarjeta)
        Me.GroupBox2.Controls.Add(Me.txtMonto)
        Me.GroupBox2.Controls.Add(Me.DTPFechaRecibo)
        Me.GroupBox2.Controls.Add(Me.txtNumeroRecibo)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(11, 201)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1161, 136)
        Me.GroupBox2.TabIndex = 37
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Recibo de Cuentas x Pagar"
        '
        'txtRefElectronica
        '
        Me.txtRefElectronica.Location = New System.Drawing.Point(657, 63)
        Me.txtRefElectronica.MaxLength = 50
        Me.txtRefElectronica.Name = "txtRefElectronica"
        Me.txtRefElectronica.Size = New System.Drawing.Size(91, 20)
        Me.txtRefElectronica.TabIndex = 17
        Me.txtRefElectronica.Tag = "Referencia de la Transferencia Electronica"
        Me.txtRefElectronica.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(608, 68)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(43, 13)
        Me.Label25.TabIndex = 35
        Me.Label25.Text = "Ref. E"
        Me.Label25.Visible = False
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Items.AddRange(New Object() {"Efectivo", "Cheque", "Tarj. Cr�dito", "Transf. Electronica"})
        Me.ddlSerie.Location = New System.Drawing.Point(657, 16)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(89, 21)
        Me.ddlSerie.TabIndex = 12
        Me.ddlSerie.Tag = "Forma de Pago del Recibo a Elaborar"
        Me.ddlSerie.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(608, 19)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(36, 13)
        Me.Label24.TabIndex = 33
        Me.Label24.Text = "Serie"
        Me.Label24.Visible = False
        '
        'Label23
        '
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 17.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(458, 40)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(173, 20)
        Me.Label23.TabIndex = 32
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(669, 112)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 16)
        Me.Label22.TabIndex = 31
        Me.Label22.Text = "Label22"
        Me.Label22.Visible = False
        '
        'txtBanco
        '
        Me.txtBanco.Location = New System.Drawing.Point(282, 65)
        Me.txtBanco.MaxLength = 50
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.Size = New System.Drawing.Size(116, 20)
        Me.txtBanco.TabIndex = 15
        Me.txtBanco.Tag = "Banco emisor del cheque"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(201, 68)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Banco"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(401, 112)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(65, 13)
        Me.Label20.TabIndex = 28
        Me.Label20.Text = "Retenci�n"
        '
        'txtDescripcionRecibo
        '
        Me.txtDescripcionRecibo.Location = New System.Drawing.Point(479, 112)
        Me.txtDescripcionRecibo.Name = "txtDescripcionRecibo"
        Me.txtDescripcionRecibo.Size = New System.Drawing.Size(122, 20)
        Me.txtDescripcionRecibo.TabIndex = 21
        Me.txtDescripcionRecibo.Tag = "Descripci�n del Recibo a Elaborar"
        Me.txtDescripcionRecibo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(201, 112)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 13)
        Me.Label19.TabIndex = 26
        Me.Label19.Text = "Mant. Valor"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(282, 112)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(116, 20)
        Me.TextBox17.TabIndex = 20
        Me.TextBox17.Tag = "Descripci�n del Recibo a Elaborar"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 112)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(59, 13)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "Intereses"
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(90, 112)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(104, 20)
        Me.TextBox16.TabIndex = 19
        Me.TextBox16.Tag = "Descripci�n del Recibo a Elaborar"
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNumeroRecibido
        '
        Me.txtNumeroRecibido.Location = New System.Drawing.Point(90, 88)
        Me.txtNumeroRecibido.MaxLength = 50
        Me.txtNumeroRecibido.Name = "txtNumeroRecibido"
        Me.txtNumeroRecibido.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtNumeroRecibido.Size = New System.Drawing.Size(660, 20)
        Me.txtNumeroRecibido.TabIndex = 18
        Me.txtNumeroRecibido.Tag = "N�mero del Recibo a Elaborar"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(401, 18)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 13)
        Me.Label16.TabIndex = 21
        Me.Label16.Text = "Rcbo Provis"
        '
        'Label15
        '
        Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(262, 43)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(104, 16)
        Me.Label15.TabIndex = 20
        Me.Label15.Text = "0.00"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(201, 43)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 13)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Disponible"
        '
        'ddlFormaPago
        '
        Me.ddlFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlFormaPago.Items.AddRange(New Object() {"Efectivo", "Cheque", "Tarj. Cr�dito", "Transf. Electr�nica"})
        Me.ddlFormaPago.Location = New System.Drawing.Point(90, 64)
        Me.ddlFormaPago.Name = "ddlFormaPago"
        Me.ddlFormaPago.Size = New System.Drawing.Size(104, 21)
        Me.ddlFormaPago.TabIndex = 14
        Me.ddlFormaPago.Tag = "Forma de Pago del Recibo a Elaborar"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(479, 15)
        Me.txtDescripcion.MaxLength = 12
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(122, 20)
        Me.txtDescripcion.TabIndex = 11
        Me.txtDescripcion.Tag = "Descripci�n del Recibo a Elaborar"
        '
        'txtNumeroTarjeta
        '
        Me.txtNumeroTarjeta.Location = New System.Drawing.Point(479, 64)
        Me.txtNumeroTarjeta.MaxLength = 16
        Me.txtNumeroTarjeta.Name = "txtNumeroTarjeta"
        Me.txtNumeroTarjeta.Size = New System.Drawing.Size(122, 20)
        Me.txtNumeroTarjeta.TabIndex = 16
        Me.txtNumeroTarjeta.Tag = "N�mero del Cheque o Tarjeta de Cr�dito"
        '
        'txtMonto
        '
        Me.txtMonto.Location = New System.Drawing.Point(90, 40)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(104, 20)
        Me.txtMonto.TabIndex = 13
        Me.txtMonto.Tag = "Monto del Recibo a Elaborar"
        Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DTPFechaRecibo
        '
        Me.DTPFechaRecibo.CustomFormat = ""
        Me.DTPFechaRecibo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTPFechaRecibo.Location = New System.Drawing.Point(282, 15)
        Me.DTPFechaRecibo.Name = "DTPFechaRecibo"
        Me.DTPFechaRecibo.Size = New System.Drawing.Size(112, 20)
        Me.DTPFechaRecibo.TabIndex = 10
        Me.DTPFechaRecibo.Tag = "Fecha del Recibo a Elaborar"
        '
        'txtNumeroRecibo
        '
        Me.txtNumeroRecibo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroRecibo.Location = New System.Drawing.Point(90, 15)
        Me.txtNumeroRecibo.Name = "txtNumeroRecibo"
        Me.txtNumeroRecibo.Size = New System.Drawing.Size(104, 20)
        Me.txtNumeroRecibo.TabIndex = 9
        Me.txtNumeroRecibo.Tag = "N�mero del Recibo a Elaborar"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 88)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Entrgamos A"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(401, 68)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(59, 13)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "# Tarjeta"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 68)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Forma"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 43)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(42, 13)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Monto"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(201, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Fecha"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "N�mero"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 510)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(1201, 23)
        Me.UltraStatusBar1.TabIndex = 38
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvxPagosFacturas)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 343)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1161, 161)
        Me.GroupBox3.TabIndex = 35
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Pagos a Facturas"
        '
        'dgvxPagosFacturas
        '
        Me.dgvxPagosFacturas.AllowUserToAddRows = False
        Me.dgvxPagosFacturas.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvxPagosFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxPagosFacturas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumFactura, Me.FechaEmis, Me.FechaVenc, Me.SaldoFact, Me.PorcDescuentoFact, Me.MontoDescuentoFact, Me.Monto, Me.NvoSaldo, Me.Interes, Me.MantValor, Me.PorAplicar})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxPagosFacturas.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvxPagosFacturas.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxPagosFacturas.Location = New System.Drawing.Point(10, 19)
        Me.dgvxPagosFacturas.Name = "dgvxPagosFacturas"
        Me.dgvxPagosFacturas.Size = New System.Drawing.Size(1151, 136)
        Me.dgvxPagosFacturas.TabIndex = 22
        '
        'NumFactura
        '
        Me.NumFactura.HeaderText = "Numero Factura"
        Me.NumFactura.Name = "NumFactura"
        Me.NumFactura.ReadOnly = True
        '
        'FechaEmis
        '
        Me.FechaEmis.HeaderText = "Fecha Emis."
        Me.FechaEmis.Name = "FechaEmis"
        Me.FechaEmis.ReadOnly = True
        '
        'FechaVenc
        '
        Me.FechaVenc.HeaderText = "Fecha Vence"
        Me.FechaVenc.Name = "FechaVenc"
        Me.FechaVenc.ReadOnly = True
        '
        'SaldoFact
        '
        Me.SaldoFact.HeaderText = "Saldo Factura"
        Me.SaldoFact.Name = "SaldoFact"
        Me.SaldoFact.ReadOnly = True
        '
        'PorcDescuentoFact
        '
        '
        '
        '
        Me.PorcDescuentoFact.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.PorcDescuentoFact.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.PorcDescuentoFact.HeaderText = "Porcentaje Descuento"
        Me.PorcDescuentoFact.Increment = 1.0R
        Me.PorcDescuentoFact.Name = "PorcDescuentoFact"
        Me.PorcDescuentoFact.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'MontoDescuentoFact
        '
        '
        '
        '
        Me.MontoDescuentoFact.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.MontoDescuentoFact.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MontoDescuentoFact.HeaderText = "Monto Descuento"
        Me.MontoDescuentoFact.Increment = 1.0R
        Me.MontoDescuentoFact.Name = "MontoDescuentoFact"
        '
        'Monto
        '
        '
        '
        '
        Me.Monto.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.Monto.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Monto.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Monto.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Increment = 1.0R
        Me.Monto.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Monto.Name = "Monto"
        Me.Monto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'NvoSaldo
        '
        Me.NvoSaldo.HeaderText = "Nvo. Saldo"
        Me.NvoSaldo.Name = "NvoSaldo"
        Me.NvoSaldo.ReadOnly = True
        '
        'Interes
        '
        '
        '
        '
        Me.Interes.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.Interes.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Interes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Interes.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText
        Me.Interes.HeaderText = "Interes"
        Me.Interes.Increment = 1.0R
        Me.Interes.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Interes.Name = "Interes"
        Me.Interes.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'MantValor
        '
        '
        '
        '
        Me.MantValor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.MantValor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.MantValor.HeaderText = "Mant. Valor"
        Me.MantValor.Increment = 1.0R
        Me.MantValor.Name = "MantValor"
        Me.MantValor.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'PorAplicar
        '
        '
        '
        '
        Me.PorAplicar.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.PorAplicar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.PorAplicar.HeaderText = "Por Aplicar"
        Me.PorAplicar.Increment = 1.0R
        Me.PorAplicar.Name = "PorAplicar"
        Me.PorAplicar.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.Text = "Clientes"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'frmRecibosCuentasXPagar
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.ClientSize = New System.Drawing.Size(1201, 533)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRecibosCuentasXPagar"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recibos Cuentas X Pagar"
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvxPagosFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents CmdAnular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdBuscar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImprimir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents txtCodigoProveedor As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtBanco As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtDescripcionRecibo As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroRecibido As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents ddlFormaPago As System.Windows.Forms.ComboBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroTarjeta As System.Windows.Forms.TextBox
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents DTPFechaRecibo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNumeroRecibo As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvxPagosFacturas As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents txtRefElectronica As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents NumFactura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaEmis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaVenc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoFact As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcDescuentoFact As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents MontoDescuentoFact As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Monto As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents NvoSaldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Interes As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents MantValor As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents PorAplicar As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
End Class
