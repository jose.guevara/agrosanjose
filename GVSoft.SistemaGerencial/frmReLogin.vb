Imports System.Data.SqlClient
Imports System.Configuration

Public Class frmReLogin
    Inherits DevComponents.DotNetBar.Office2007Form
    ' Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmReLogin))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.cmdOk = New System.Windows.Forms.Button
        Me.cmdCancelar = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Usuario:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(48, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Clave:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(104, 24)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(88, 20)
        Me.txtCodigo.TabIndex = 2
        Me.txtCodigo.Text = ""
        '
        'txtClave
        '
        Me.txtClave.Location = New System.Drawing.Point(104, 64)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtClave.Size = New System.Drawing.Size(88, 20)
        Me.txtClave.TabIndex = 3
        Me.txtClave.Text = ""
        '
        'cmdOk
        '
        Me.cmdOk.Location = New System.Drawing.Point(24, 112)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(72, 32)
        Me.cmdOk.TabIndex = 4
        Me.cmdOk.Text = "&OK"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(144, 112)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(72, 32)
        Me.cmdCancelar.TabIndex = 5
        Me.cmdCancelar.Text = "&Cancelar"
        '
        'frmReLogin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(238, 155)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso al Sistema"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmReLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim intError As Integer

        intError = 0
        strQuery = ""
        strAgro2K = ""
        strUsuario = ""
        strAppPath = ""
        strNombEmpresa = ""
        lngClienteContado = 0
        lngNegocioContado = 0
        lngDepartContado = 0
        intFechaCambioFactura = 0

        strAppPath = System.Windows.Forms.Application.StartupPath
        'strAgro2K = "data source=transidea;persist security info=True;initial catalog=Agrocentro2K;user id=Agro2K;password=Agro2K_2008"
        strAgro2K = ConfigurationManager.AppSettings("ConSistemaGerencial").ToString()
        'strAgro2K = "data source=work\work;persist security info=True;initial catalog=Agrocentro2K;user id=sa;password=djvallecillo"
        'strAgro2K = "data source=Servidor;persist security info=True;initial catalog=Agrocentro2K;user id=Agro2K;password=Agro2K_2008"
        cnnAgro2K.ConnectionString = strAgro2K
        cnnAgro2KTmp.ConnectionString = strAgro2K

    End Sub

    Private Sub frmReLogin_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
            Application.Exit()
        End If

    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click

        Dim intValor As Integer
        Dim cmdTmp As New SqlCommand

        strQuery = ""
        strQuery = "Select * From prm_Usuarios Where CodUsuario = '" & txtCodigo.Text & "' "
        strQuery = strQuery + "And Clave = '" & txtClave.Text & "' And Estado = 0"

        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            intValor = 0
            lngRegUsuarioRelogin = 0
            nIdUsuarioRelogin = 0
            strUsuarioRelogin = String.Empty
            While dtrAgro2K.Read
                lngRegUsuarioRelogin = dtrAgro2K.GetValue(0)
                nIdUsuarioRelogin = dtrAgro2K.GetValue(0)
                strUsuarioRelogin = dtrAgro2K.GetValue(1)
                intValor = 1
            End While
            If intValor = 0 Then
                MsgBox("No Existe Este Usuario en el Sistema o est� Inactivo. Verifique.", MsgBoxStyle.Critical, "Error de Datos Ingresados")
                txtClave.Text = ""
                txtClave.Focus()
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                Exit Sub
            ElseIf intValor = 1 Then
                intValor = 0
                dtrAgro2K.Close()
                strQuery = ""
                strQuery = "Select * From prm_UsuariosPermisos Where CliRegistro = " & lngRegUsuario & ""
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                cmdTmp.Connection = cnnAgro2K
                While dtrAgro2K.Read
                    intAccesos(dtrAgro2K.GetValue(1)) = dtrAgro2K.GetValue(1)
                    intValor = intValor + 1
                End While
                dtrAgro2K.Close()
                dblTipoCambio = 0
                strQuery = ""
                strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(Now, "yyyyMMdd") & " "
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                intValor = 0
                While dtrAgro2K.Read
                    dblTipoCambio = dtrAgro2K.GetValue(0)
                End While
                dtrAgro2K.Close()
                dblPorcParam = 0
                dblRetParam = 0
                strQuery = ""
                strQuery = "Select porc_iva, porc_ret From prm_Porcentajes"
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                intValor = 0
                While dtrAgro2K.Read
                    dblPorcParam = dtrAgro2K.GetValue(0) / 100
                    dblRetParam = dtrAgro2K.GetValue(1) / 100
                End While
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                Dim frmNew As New frmPrincipal
                frmNew.Show()
                Me.Close()
            End If
        Catch exc As Exception
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            MsgBox(exc.Message)
        End Try

    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click

        Me.Close()
        Application.Exit()

    End Sub

    Function ValidarDatos() As Boolean

        If txtCodigo.Text = "" Or Len(Trim(txtCodigo.Text)) = 0 Then
            MsgBox("Tiene Que Ingresar el Codigo.", MsgBoxStyle.Critical)
            txtCodigo.Focus()
            Return False
        ElseIf txtClave.Text = "" Or Len(Trim(txtClave.Text)) = 0 Then
            MsgBox("Tiene Que Ingresar la Clave.", MsgBoxStyle.Critical)
            txtClave.Focus()
            Return False
        End If
        Return True

    End Function

    Private Sub txtCodigo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigo.GotFocus, txtClave.GotFocus

        sender.selectall()

    End Sub

    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown, txtClave.KeyDown

        If e.KeyCode = Keys.Return Then
            If sender.name = "txtCodigo" Then
                txtClave.Focus()
            Else
                cmdOk.PerformClick()
            End If
        End If

    End Sub

End Class