Imports System.IO
Imports System.Web.UI.WebControls
Imports Infragistics.Win
Imports System.Configuration
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmExportarPedidos
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents utcAgencia As Infragistics.Win.UltraWinGrid.UltraCombo
    Friend WithEvents DPFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents DPFechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Label4 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportarPedidos))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.utcAgencia = New Infragistics.Win.UltraWinGrid.UltraCombo
        Me.Label2 = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.DPFechaFin = New System.Windows.Forms.DateTimePicker
        Me.DPFechaInicio = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.utcAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.utcAgencia)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ProgressBar1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 79)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(459, 116)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas a Exportar"
        '
        'utcAgencia
        '
        Appearance4.BackColor = System.Drawing.SystemColors.Window
        Appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption
        Me.utcAgencia.DisplayLayout.Appearance = Appearance4
        Me.utcAgencia.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
        Me.utcAgencia.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.utcAgencia.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Me.utcAgencia.DisplayLayout.EmptyRowSettings.Style = Infragistics.Win.UltraWinGrid.EmptyRowStyle.AlignWithDataRows
        Appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance1.BorderColor = System.Drawing.SystemColors.Window
        Me.utcAgencia.DisplayLayout.GroupByBox.Appearance = Appearance1
        Appearance2.ForeColor = System.Drawing.SystemColors.GrayText
        Me.utcAgencia.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance2
        Me.utcAgencia.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance3.BackColor2 = System.Drawing.SystemColors.Control
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance3.ForeColor = System.Drawing.SystemColors.GrayText
        Me.utcAgencia.DisplayLayout.GroupByBox.PromptAppearance = Appearance3
        Me.utcAgencia.DisplayLayout.MaxColScrollRegions = 1
        Me.utcAgencia.DisplayLayout.MaxRowScrollRegions = 1
        Appearance12.BackColor = System.Drawing.SystemColors.Window
        Appearance12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.utcAgencia.DisplayLayout.Override.ActiveCellAppearance = Appearance12
        Appearance7.BackColor = System.Drawing.SystemColors.Highlight
        Appearance7.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.utcAgencia.DisplayLayout.Override.ActiveRowAppearance = Appearance7
        Me.utcAgencia.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted
        Me.utcAgencia.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted
        Appearance6.BackColor = System.Drawing.SystemColors.Window
        Me.utcAgencia.DisplayLayout.Override.CardAreaAppearance = Appearance6
        Appearance5.BorderColor = System.Drawing.Color.Silver
        Appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter
        Me.utcAgencia.DisplayLayout.Override.CellAppearance = Appearance5
        Me.utcAgencia.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.utcAgencia.DisplayLayout.Override.CellPadding = 0
        Appearance9.BackColor = System.Drawing.SystemColors.Control
        Appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance9.BorderColor = System.Drawing.SystemColors.Window
        Me.utcAgencia.DisplayLayout.Override.GroupByRowAppearance = Appearance9
        Appearance11.TextHAlignAsString = "Left"
        Me.utcAgencia.DisplayLayout.Override.HeaderAppearance = Appearance11
        Me.utcAgencia.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.utcAgencia.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand
        Appearance10.BackColor = System.Drawing.SystemColors.Window
        Appearance10.BorderColor = System.Drawing.Color.Silver
        Me.utcAgencia.DisplayLayout.Override.RowAppearance = Appearance10
        Me.utcAgencia.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.utcAgencia.DisplayLayout.Override.TemplateAddRowAppearance = Appearance8
        Me.utcAgencia.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.utcAgencia.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.utcAgencia.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.utcAgencia.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Office2010
        Me.utcAgencia.Location = New System.Drawing.Point(88, 55)
        Me.utcAgencia.Name = "utcAgencia"
        Me.utcAgencia.Size = New System.Drawing.Size(330, 22)
        Me.utcAgencia.TabIndex = 29
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Agencia"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(67, 94)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(320, 16)
        Me.ProgressBar1.TabIndex = 2
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        '
        'DPFechaFin
        '
        Me.DPFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DPFechaFin.Location = New System.Drawing.Point(298, 101)
        Me.DPFechaFin.Name = "DPFechaFin"
        Me.DPFechaFin.Size = New System.Drawing.Size(120, 20)
        Me.DPFechaFin.TabIndex = 17
        '
        'DPFechaInicio
        '
        Me.DPFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DPFechaInicio.Location = New System.Drawing.Point(87, 101)
        Me.DPFechaInicio.Name = "DPFechaInicio"
        Me.DPFechaInicio.Size = New System.Drawing.Size(120, 20)
        Me.DPFechaInicio.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 101)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Fecha Inicio"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(219, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Fecha Fin"
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdProcesar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(471, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 37
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdProcesar
        '
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Procesar<F3>"
        Me.cmdProcesar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmExportarPedidos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(471, 206)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.DPFechaFin)
        Me.Controls.Add(Me.DPFechaInicio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarPedidos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Exportar Pedidos de Inventario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.utcAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmExportarPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DPFechaInicio.Value = Now
        UbicarAgencia(lngRegUsuario)
        ObtieneAgencias()
        utcAgencia.Value = lngRegAgencia
    End Sub

    Private Sub frmExportarPedidos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Procesar()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Procesar()
        '    Case 1
        '        Me.Close()
        'End Select

    End Sub

    Sub ObtieneAgencias()
        Dim IndicaObtieneRegistro As Int16
        utcAgencia.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        Dim table As New DataTable("Agencias")
        table.Columns.Add("Codigo")
        table.Columns.Add("Descripcion")

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        'ddlAgencia.SelectedItem = -1
        'utcAgencia.SelectedRow.Selected = -1
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "select a.registro,a.codigo,a.Descripcion from prm_UsuariosAgencias u,prm_agencias a  Where(u.agenregistro = a.registro)  and u.usrregistro = " & lngRegUsuario & ""
        If strQuery.Trim.Length > 0 Then
            'ddlAgencia.Items.Clear()
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                table.Rows.Add(New Object() {dtrAgro2K.GetValue(0), dtrAgro2K.GetValue(2)})
                IndicaObtieneRegistro = 1
            End While
            utcAgencia.DisplayMember = "Descripcion"
            utcAgencia.ValueMember = "codigo"
            utcAgencia.DataSource = table
        End If

    End Sub
    Function ObtieneCodigoAgencias(ByVal lnRegistro As Int16) As Int16
        Dim IndicaObtieneRegistro As Int16
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "exec ObtieneCodigoAgencia " & lnRegistro & ""
        ObtieneCodigoAgencias = 0
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ObtieneCodigoAgencias = dtrAgro2K.GetValue(0)
            IndicaObtieneRegistro = 1
        End While
        Return ObtieneCodigoAgencias
    End Function
    Sub Procesar()

        Me.Cursor = Cursors.WaitCursor
        Dim intCantidad As Integer = 0
        Dim intContador As Integer = 0
        Dim intNumFechaInicio As Integer = 0
        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim lnRegistro As Integer = 0
        Dim lnCodigoAgencia As Integer = 0
        Dim lnAgenciaOrigen As Integer
        Dim lnCdigoAgenciaOrigen As Integer = 0
        Dim intNumFechaFin As Integer = 0
        Dim lsRutaCompletaArchivoZip As String = String.Empty

        'If ddlAgencia.SelectedItem = "" Then
        'If utcAgencia.SelectedRow.Cells.Item("codigo").Text = "" Then
        If utcAgencia.Text = "" Then
            utcAgencia.Focus()
            MessageBoxEx.Show("Tiene que escoger la agencia a exportar.  Verifique.", "Error de Dato", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        lnRegistro = ConvierteAInt(utcAgencia.SelectedRow.Cells.Item("codigo").Value)
        lnCodigoAgencia = ObtieneCodigoAgencias(lnRegistro)

        ProgressBar1.Focus()
        intCantidad = 0
        intContador = 0
        intNumFechaInicio = 0
        intNumFechaInicio = Format(DPFechaInicio.Value, "yyyyMMdd")
        intNumFechaFin = 0
        intNumFechaFin = Format(DPFechaFin.Value, "yyyyMMdd")
        strQuery = ""
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        'strQuery = "exec VerificaRegistrosAExportarCompras " & intNumFecha & "," & lngAgenRegExp
        strQuery = "exec VerificaRegistrosAExportarPedidosInventario " & intNumFechaInicio & "," & intNumFechaFin & "," & lnRegistro
        'strQuery = strQuery + "e.numfechaing = " & intNumFecha & " and e.agenregistro = " & lngAgenRegExp & ""
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                intCantidad = dtrAgro2K.GetValue(0)
                lnAgenciaOrigen = dtrAgro2K.GetValue(1)
            End If
        End While
        dtrAgro2K.Close()
        lnCdigoAgenciaOrigen = ObtieneCodigoAgencias(lnAgenciaOrigen)
        strNombreArchivo = ""
        If intCantidad <> 0 Then
            ProgressBar1.Minimum = 0
            ProgressBar1.Maximum = intCantidad
            ProgressBar1.Value = 0
            strQuery = ""
            'strQuery = " exec ConsultaExportarCompra " & intNumFecha & "," & lngAgenRegExp
            strQuery = " exec spConsultarPedidoAExportar " & intNumFechaInicio & "," & intNumFechaFin & "," & lnRegistro

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            dtrAgro2K.Close()
            cmdAgro2K.CommandText = strQuery
            strClave = ""
            strClave = "Agro2K_2008"
            strArchivo = ""
            strArchivo = ConfigurationManager.AppSettings("RutaArchivosTraslados").ToString() & "Pedidos_" & Format(lnCdigoAgenciaOrigen, "00") & "_" & Format(lnCodigoAgencia, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt"
            strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosTraslados").ToString() & "Pedidos_" & Format(lnCdigoAgenciaOrigen, "00") & "_" & Format(lnCodigoAgencia, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd")
            Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosTraslados").ToString() & "Pedidos_" & Format(lnCdigoAgenciaOrigen, "00") & "_" & Format(lnCodigoAgencia, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt")
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ProgressBar1.Value = ProgressBar1.Value + 1
                sr.WriteLine(dtrAgro2K.Item("IdPedido") & "|" & Trim(dtrAgro2K.Item("NumeroDocumento")) & "|" & dtrAgro2K.Item("FechaIngreso") & "|" & Trim(dtrAgro2K.Item("NumFechaIng")) & "|" & Trim(dtrAgro2K.Item("Impuesto")) & "|" & Trim(dtrAgro2K.Item("Retencion")) & "|" & Trim(dtrAgro2K.Item("SubTotal")) & "|" & _
                             dtrAgro2K.Item("Total") & "|" & dtrAgro2K.Item("TipoCambio") & "|" & dtrAgro2K.Item("Cantidad") & "|" & dtrAgro2K.Item("CadigoUnidad") & "|" & Trim(dtrAgro2K.Item("CadigoProducto")) & "|" & dtrAgro2K.Item("Descripcion") & "|" & dtrAgro2K.Item("IGV") & "|" & dtrAgro2K.Item("CodigoAgencia")) '& "|" & dtrAgro2K.GetValue(15) & "|" & dtrAgro2K.GetValue(16) & "|" & dtrAgro2K.GetValue(17) & "|" & Trim(dtrAgro2K.GetValue(18)) & "|" & Trim(dtrAgro2K.GetValue(19)) & "|" & dtrAgro2K.GetValue(20) & "|" & dtrAgro2K.GetValue(21) & "|" & dtrAgro2K.GetValue(22) & "|" & dtrAgro2K.GetValue(24) & "|" & dtrAgro2K.GetValue(26) & "|" & dtrAgro2K.GetValue(27))
            End While
            sr.Close()
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            intIncr = 0
            Try
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                lsRutaCompletaArchivoZip = String.Empty
                lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
                If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                    MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                    Exit Sub
                End If
                If File.Exists(strArchivo) = True Then
                    File.Delete(strArchivo)
                End If
            Catch exx As Exception
                MsgBox(exx.Message.ToString)
                Me.Cursor = Cursors.Default
                Exit Sub
            End Try
        Else
            MessageBoxEx.Show("No hay Pedidos que exportar para el d�a solicitado.", "Error en Solicitud", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            ProgressBar1.Value = 0
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        MessageBoxEx.Show("Finalizo el proceso de exportaci�n de los Pedidos", "Proceso Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ProgressBar1.Value = 0
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub utcAgencia_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles utcAgencia.InitializeLayout

    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Procesar()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
