Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptGenerales
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Label2 As DataDynamics.ActiveReports.Label = Nothing
    Private Label3 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Friend WithEvents txtPrecioVentaPublico As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtPrecioVentaMayoreo As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblPrecioVentaPublico As DataDynamics.ActiveReports.Label
    Private WithEvents lblPrecioVentaMayoreo As DataDynamics.ActiveReports.Label
    Private WithEvents lblEmpresa As DataDynamics.ActiveReports.Label
    Friend WithEvents Line1 As DataDynamics.ActiveReports.Line
    Friend WithEvents Line6 As DataDynamics.ActiveReports.Line
    Friend WithEvents txtPrecioCosto As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblPrecioCosto As DataDynamics.ActiveReports.Label
    Friend WithEvents Line2 As DataDynamics.ActiveReports.Line
    Friend WithEvents Label10 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalProductos As DataDynamics.ActiveReports.TextBox
    Private TextBox21 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptGenerales))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.TextBox = New DataDynamics.ActiveReports.TextBox
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox
        Me.txtPrecioVentaPublico = New DataDynamics.ActiveReports.TextBox
        Me.txtPrecioVentaMayoreo = New DataDynamics.ActiveReports.TextBox
        Me.txtPrecioCosto = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox5 = New DataDynamics.ActiveReports.TextBox
        Me.lblPrecioVentaPublico = New DataDynamics.ActiveReports.Label
        Me.lblPrecioVentaMayoreo = New DataDynamics.ActiveReports.Label
        Me.lblEmpresa = New DataDynamics.ActiveReports.Label
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Line6 = New DataDynamics.ActiveReports.Line
        Me.lblPrecioCosto = New DataDynamics.ActiveReports.Label
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox
        Me.Line2 = New DataDynamics.ActiveReports.Line
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.txtTotalProductos = New DataDynamics.ActiveReports.TextBox
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecioVentaPublico, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecioVentaMayoreo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecioCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrecioVentaPublico, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrecioVentaMayoreo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrecioCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox, Me.TextBox1, Me.TextBox2, Me.TextBox3, Me.txtPrecioVentaPublico, Me.txtPrecioVentaMayoreo, Me.txtPrecioCosto})
        Me.Detail.Height = 0.1875!
        Me.Detail.Name = "Detail"
        '
        'TextBox
        '
        Me.TextBox.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox.DataField = "Codigo"
        Me.TextBox.Height = 0.1875!
        Me.TextBox.Left = 0.0625!
        Me.TextBox.Name = "TextBox"
        Me.TextBox.Style = ""
        Me.TextBox.Text = "TextBox"
        Me.TextBox.Top = 0.0!
        Me.TextBox.Width = 0.875!
        '
        'TextBox1
        '
        Me.TextBox1.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.DataField = "Descripcion"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 1.0!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Style = ""
        Me.TextBox1.Text = "TextBox1"
        Me.TextBox1.Top = 0.0!
        Me.TextBox1.Width = 2.6875!
        '
        'TextBox2
        '
        Me.TextBox2.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.DataField = "Estado"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 6.5625!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Style = ""
        Me.TextBox2.Text = "TextBox2"
        Me.TextBox2.Top = 0.0!
        Me.TextBox2.Width = 0.5!
        '
        'TextBox3
        '
        Me.TextBox3.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.DataField = "Estado2"
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 6.5625!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Style = ""
        Me.TextBox3.Text = "TextBox3"
        Me.TextBox3.Top = 0.0!
        Me.TextBox3.Width = 0.5!
        '
        'txtPrecioVentaPublico
        '
        Me.txtPrecioVentaPublico.Border.BottomColor = System.Drawing.Color.Black
        Me.txtPrecioVentaPublico.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaPublico.Border.LeftColor = System.Drawing.Color.Black
        Me.txtPrecioVentaPublico.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaPublico.Border.RightColor = System.Drawing.Color.Black
        Me.txtPrecioVentaPublico.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaPublico.Border.TopColor = System.Drawing.Color.Black
        Me.txtPrecioVentaPublico.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaPublico.DataField = "pvpc"
        Me.txtPrecioVentaPublico.Height = 0.1875!
        Me.txtPrecioVentaPublico.Left = 5.625!
        Me.txtPrecioVentaPublico.Name = "txtPrecioVentaPublico"
        Me.txtPrecioVentaPublico.OutputFormat = resources.GetString("txtPrecioVentaPublico.OutputFormat")
        Me.txtPrecioVentaPublico.Style = "text-align: right; "
        Me.txtPrecioVentaPublico.Text = Nothing
        Me.txtPrecioVentaPublico.Top = 0.0!
        Me.txtPrecioVentaPublico.Visible = False
        Me.txtPrecioVentaPublico.Width = 0.8125!
        '
        'txtPrecioVentaMayoreo
        '
        Me.txtPrecioVentaMayoreo.Border.BottomColor = System.Drawing.Color.Black
        Me.txtPrecioVentaMayoreo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaMayoreo.Border.LeftColor = System.Drawing.Color.Black
        Me.txtPrecioVentaMayoreo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaMayoreo.Border.RightColor = System.Drawing.Color.Black
        Me.txtPrecioVentaMayoreo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaMayoreo.Border.TopColor = System.Drawing.Color.Black
        Me.txtPrecioVentaMayoreo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioVentaMayoreo.DataField = "pvdc"
        Me.txtPrecioVentaMayoreo.Height = 0.1875!
        Me.txtPrecioVentaMayoreo.Left = 4.6875!
        Me.txtPrecioVentaMayoreo.Name = "txtPrecioVentaMayoreo"
        Me.txtPrecioVentaMayoreo.OutputFormat = resources.GetString("txtPrecioVentaMayoreo.OutputFormat")
        Me.txtPrecioVentaMayoreo.Style = "text-align: right; "
        Me.txtPrecioVentaMayoreo.Text = Nothing
        Me.txtPrecioVentaMayoreo.Top = 0.0!
        Me.txtPrecioVentaMayoreo.Visible = False
        Me.txtPrecioVentaMayoreo.Width = 0.875!
        '
        'txtPrecioCosto
        '
        Me.txtPrecioCosto.Border.BottomColor = System.Drawing.Color.Black
        Me.txtPrecioCosto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioCosto.Border.LeftColor = System.Drawing.Color.Black
        Me.txtPrecioCosto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioCosto.Border.RightColor = System.Drawing.Color.Black
        Me.txtPrecioCosto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioCosto.Border.TopColor = System.Drawing.Color.Black
        Me.txtPrecioCosto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrecioCosto.DataField = "PrecioCosto"
        Me.txtPrecioCosto.Height = 0.1875!
        Me.txtPrecioCosto.Left = 3.75!
        Me.txtPrecioCosto.Name = "txtPrecioCosto"
        Me.txtPrecioCosto.OutputFormat = resources.GetString("txtPrecioCosto.OutputFormat")
        Me.txtPrecioCosto.Style = "text-align: right; "
        Me.txtPrecioCosto.Text = Nothing
        Me.txtPrecioCosto.Top = 0.0!
        Me.txtPrecioCosto.Visible = False
        Me.txtPrecioCosto.Width = 0.875!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label, Me.Label1, Me.Label2, Me.Label3, Me.Label4, Me.Label5, Me.Label6, Me.TextBox4, Me.TextBox5, Me.lblPrecioVentaPublico, Me.lblPrecioVentaMayoreo, Me.lblEmpresa, Me.Line1, Me.Line6, Me.lblPrecioCosto})
        Me.PageHeader.Height = 1.270833!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label
        '
        Me.Label.Border.BottomColor = System.Drawing.Color.Black
        Me.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.LeftColor = System.Drawing.Color.Black
        Me.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.RightColor = System.Drawing.Color.Black
        Me.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.TopColor = System.Drawing.Color.Black
        Me.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.0625!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label.Text = "C�digo"
        Me.Label.Top = 0.875!
        Me.Label.Width = 0.5625!
        '
        'Label1
        '
        Me.Label1.Border.BottomColor = System.Drawing.Color.Black
        Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.LeftColor = System.Drawing.Color.Black
        Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.RightColor = System.Drawing.Color.Black
        Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.TopColor = System.Drawing.Color.Black
        Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 1.0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label1.Text = "Descripci�n"
        Me.Label1.Top = 0.875!
        Me.Label1.Width = 2.6875!
        '
        'Label2
        '
        Me.Label2.Border.BottomColor = System.Drawing.Color.Black
        Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.LeftColor = System.Drawing.Color.Black
        Me.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.RightColor = System.Drawing.Color.Black
        Me.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.TopColor = System.Drawing.Color.Black
        Me.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 6.5625!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label2.Text = "Estado"
        Me.Label2.Top = 0.875!
        Me.Label2.Width = 0.5!
        '
        'Label3
        '
        Me.Label3.Border.BottomColor = System.Drawing.Color.Black
        Me.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.LeftColor = System.Drawing.Color.Black
        Me.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.RightColor = System.Drawing.Color.Black
        Me.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.TopColor = System.Drawing.Color.Black
        Me.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 6.5625!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label3.Text = "Estado"
        Me.Label3.Top = 0.875!
        Me.Label3.Width = 0.5!
        '
        'Label4
        '
        Me.Label4.Border.BottomColor = System.Drawing.Color.Black
        Me.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.LeftColor = System.Drawing.Color.Black
        Me.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.RightColor = System.Drawing.Color.Black
        Me.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.TopColor = System.Drawing.Color.Black
        Me.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Height = 0.25!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.0625!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: left; font-weight: bold; font-size: 14.25pt; "
        Me.Label4.Text = "Label4"
        Me.Label4.Top = 0.4375!
        Me.Label4.Width = 4.4375!
        '
        'Label5
        '
        Me.Label5.Border.BottomColor = System.Drawing.Color.Black
        Me.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.LeftColor = System.Drawing.Color.Black
        Me.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.RightColor = System.Drawing.Color.Black
        Me.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.TopColor = System.Drawing.Color.Black
        Me.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 4.5625!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label5.Text = "Fecha de Impresi�n"
        Me.Label5.Top = 0.0625!
        Me.Label5.Width = 1.35!
        '
        'Label6
        '
        Me.Label6.Border.BottomColor = System.Drawing.Color.Black
        Me.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.LeftColor = System.Drawing.Color.Black
        Me.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.RightColor = System.Drawing.Color.Black
        Me.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.TopColor = System.Drawing.Color.Black
        Me.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Height = 0.2!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 4.5625!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label6.Text = "Hora de Impresi�n"
        Me.Label6.Top = 0.25!
        Me.Label6.Width = 1.35!
        '
        'TextBox4
        '
        Me.TextBox4.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Height = 0.2!
        Me.TextBox4.Left = 5.9375!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Style = "text-align: right; "
        Me.TextBox4.Text = Nothing
        Me.TextBox4.Top = 0.0625!
        Me.TextBox4.Width = 0.9!
        '
        'TextBox5
        '
        Me.TextBox5.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Height = 0.2!
        Me.TextBox5.Left = 5.9375!
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Style = "text-align: right; "
        Me.TextBox5.Text = Nothing
        Me.TextBox5.Top = 0.25!
        Me.TextBox5.Width = 0.9!
        '
        'lblPrecioVentaPublico
        '
        Me.lblPrecioVentaPublico.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPrecioVentaPublico.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaPublico.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPrecioVentaPublico.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaPublico.Border.RightColor = System.Drawing.Color.Black
        Me.lblPrecioVentaPublico.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaPublico.Border.TopColor = System.Drawing.Color.Black
        Me.lblPrecioVentaPublico.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaPublico.Height = 0.3125!
        Me.lblPrecioVentaPublico.HyperLink = Nothing
        Me.lblPrecioVentaPublico.Left = 5.625!
        Me.lblPrecioVentaPublico.Name = "lblPrecioVentaPublico"
        Me.lblPrecioVentaPublico.Style = "text-align: center; font-weight: bold; font-size: 9.75pt; "
        Me.lblPrecioVentaPublico.Text = "Precio Venta Publico"
        Me.lblPrecioVentaPublico.Top = 0.875!
        Me.lblPrecioVentaPublico.Visible = False
        Me.lblPrecioVentaPublico.Width = 0.875!
        '
        'lblPrecioVentaMayoreo
        '
        Me.lblPrecioVentaMayoreo.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPrecioVentaMayoreo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaMayoreo.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPrecioVentaMayoreo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaMayoreo.Border.RightColor = System.Drawing.Color.Black
        Me.lblPrecioVentaMayoreo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaMayoreo.Border.TopColor = System.Drawing.Color.Black
        Me.lblPrecioVentaMayoreo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioVentaMayoreo.Height = 0.375!
        Me.lblPrecioVentaMayoreo.HyperLink = Nothing
        Me.lblPrecioVentaMayoreo.Left = 4.6875!
        Me.lblPrecioVentaMayoreo.Name = "lblPrecioVentaMayoreo"
        Me.lblPrecioVentaMayoreo.Style = "text-align: center; font-weight: bold; font-size: 9.75pt; vertical-align: top; "
        Me.lblPrecioVentaMayoreo.Text = "Precio Venta Mayoreo"
        Me.lblPrecioVentaMayoreo.Top = 0.875!
        Me.lblPrecioVentaMayoreo.Visible = False
        Me.lblPrecioVentaMayoreo.Width = 0.875!
        '
        'lblEmpresa
        '
        Me.lblEmpresa.Border.BottomColor = System.Drawing.Color.Black
        Me.lblEmpresa.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblEmpresa.Border.LeftColor = System.Drawing.Color.Black
        Me.lblEmpresa.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblEmpresa.Border.RightColor = System.Drawing.Color.Black
        Me.lblEmpresa.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblEmpresa.Border.TopColor = System.Drawing.Color.Black
        Me.lblEmpresa.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblEmpresa.Height = 0.25!
        Me.lblEmpresa.HyperLink = Nothing
        Me.lblEmpresa.Left = 0.0625!
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Style = "text-align: left; font-weight: bold; font-size: 14.25pt; "
        Me.lblEmpresa.Text = ""
        Me.lblEmpresa.Top = 0.125!
        Me.lblEmpresa.Width = 4.4375!
        '
        'Line1
        '
        Me.Line1.Border.BottomColor = System.Drawing.Color.Black
        Me.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.LeftColor = System.Drawing.Color.Black
        Me.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.RightColor = System.Drawing.Color.Black
        Me.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.TopColor = System.Drawing.Color.Black
        Me.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.25!
        Me.Line1.Width = 7.0625!
        Me.Line1.X1 = 0.0!
        Me.Line1.X2 = 7.0625!
        Me.Line1.Y1 = 1.25!
        Me.Line1.Y2 = 1.25!
        '
        'Line6
        '
        Me.Line6.Border.BottomColor = System.Drawing.Color.Black
        Me.Line6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Border.LeftColor = System.Drawing.Color.Black
        Me.Line6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Border.RightColor = System.Drawing.Color.Black
        Me.Line6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Border.TopColor = System.Drawing.Color.Black
        Me.Line6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Height = 0.0!
        Me.Line6.Left = 0.0!
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 0.8125!
        Me.Line6.Width = 7.0625!
        Me.Line6.X1 = 0.0!
        Me.Line6.X2 = 7.0625!
        Me.Line6.Y1 = 0.8125!
        Me.Line6.Y2 = 0.8125!
        '
        'lblPrecioCosto
        '
        Me.lblPrecioCosto.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPrecioCosto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioCosto.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPrecioCosto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioCosto.Border.RightColor = System.Drawing.Color.Black
        Me.lblPrecioCosto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioCosto.Border.TopColor = System.Drawing.Color.Black
        Me.lblPrecioCosto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecioCosto.Height = 0.3125!
        Me.lblPrecioCosto.HyperLink = Nothing
        Me.lblPrecioCosto.Left = 3.75!
        Me.lblPrecioCosto.Name = "lblPrecioCosto"
        Me.lblPrecioCosto.Style = "text-align: center; font-weight: bold; font-size: 9.75pt; vertical-align: top; "
        Me.lblPrecioCosto.Text = "Precio Costo"
        Me.lblPrecioCosto.Top = 0.875!
        Me.lblPrecioCosto.Visible = False
        Me.lblPrecioCosto.Width = 0.875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.TextBox21, Me.Line2, Me.Label10, Me.txtTotalProductos})
        Me.PageFooter.Height = 0.40625!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label13
        '
        Me.Label13.Border.BottomColor = System.Drawing.Color.Black
        Me.Label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.LeftColor = System.Drawing.Color.Black
        Me.Label13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.RightColor = System.Drawing.Color.Black
        Me.Label13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.TopColor = System.Drawing.Color.Black
        Me.Label13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 5.625!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label13.Text = "P�gina"
        Me.Label13.Top = 0.125!
        Me.Label13.Width = 0.5500001!
        '
        'TextBox21
        '
        Me.TextBox21.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Height = 0.2!
        Me.TextBox21.Left = 6.1875!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: right; "
        Me.TextBox21.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox21.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0.125!
        Me.TextBox21.Width = 0.3499999!
        '
        'Line2
        '
        Me.Line2.Border.BottomColor = System.Drawing.Color.Black
        Me.Line2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Border.LeftColor = System.Drawing.Color.Black
        Me.Line2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Border.RightColor = System.Drawing.Color.Black
        Me.Line2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Border.TopColor = System.Drawing.Color.Black
        Me.Line2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Height = 0.0!
        Me.Line2.Left = 0.0!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.0625!
        Me.Line2.Width = 7.0625!
        Me.Line2.X1 = 0.0!
        Me.Line2.X2 = 7.0625!
        Me.Line2.Y1 = 0.0625!
        Me.Line2.Y2 = 0.0625!
        '
        'Label10
        '
        Me.Label10.Border.BottomColor = System.Drawing.Color.Black
        Me.Label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.LeftColor = System.Drawing.Color.Black
        Me.Label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.RightColor = System.Drawing.Color.Black
        Me.Label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.TopColor = System.Drawing.Color.Black
        Me.Label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0.1875!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold; "
        Me.Label10.Text = "Total Productos:"
        Me.Label10.Top = 0.125!
        Me.Label10.Width = 1.25!
        '
        'txtTotalProductos
        '
        Me.txtTotalProductos.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalProductos.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalProductos.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalProductos.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalProductos.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalProductos.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalProductos.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalProductos.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalProductos.DataField = "Codigo"
        Me.txtTotalProductos.Height = 0.1979167!
        Me.txtTotalProductos.Left = 1.5625!
        Me.txtTotalProductos.Name = "txtTotalProductos"
        Me.txtTotalProductos.Style = "font-weight: bold; "
        Me.txtTotalProductos.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.txtTotalProductos.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalProductos.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalProductos.Text = Nothing
        Me.txtTotalProductos.Top = 0.125!
        Me.txtTotalProductos.Width = 1.0!
        '
        'actrptGenerales
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.25!
        Me.PageSettings.Margins.Right = 0.05!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.114583!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" & _
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecioVentaPublico, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecioVentaMayoreo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecioCosto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrecioVentaPublico, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrecioVentaMayoreo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrecioCosto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptGenerales_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Label3.Visible = False
        TextBox3.Visible = False
        If intModulo = 1 Then
            Me.Label.Text = "N�mero"
            Me.Label1.Text = "Nombre de la Agencia"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 3 Then
            Me.Label2.Visible = True
            Me.Label2.Text = "Departamento"
            Me.TextBox2.Visible = True
        ElseIf intModulo = 5 Then
            Me.Label1.Text = "Nombre"
        ElseIf intModulo = 10 Then
            Me.Label3.Visible = True
            Me.TextBox3.Visible = True
            Me.Label2.Text = "Tipo"
            Me.Label3.Text = "Estado"
        ElseIf intModulo = 19 Then
            Me.Label.Text = "Fecha"
            Me.Label1.Text = "Tipo de Cambio"
            Me.Label2.Visible = False
            Me.TextBox2.Visible = False
        ElseIf intModulo = 23 Then
            Me.Label.Text = "Cuenta"
            Me.Label1.Text = "Descripci�n"
            Me.Label2.Visible = False
            Me.TextBox2.Visible = False
        ElseIf intModulo = 27 Then
            Me.Label1.Text = "Nombres"
        ElseIf intModulo = 28 Then
            Me.Label.Text = "Cuenta"
            Me.Label1.Text = "Descripci�n"
        ElseIf intModulo = 29 Then
            Me.Label3.Visible = True
            Me.TextBox3.Visible = True
            Me.Label.Text = "Estado"
            Me.Label1.Text = "Cuenta Bancaria"
            Me.Label2.Text = "Ck Inicial"
            Me.Label3.Text = "Ck Final"
        ElseIf intModulo = 35 Then
            Me.TextBox3.Visible = False
            Me.Label.Text = "Vendedor"
            Me.Label1.Text = "Nombre"
            Me.Label2.Text = "Estrato"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
            Me.TextBox2.Size = New System.Drawing.SizeF(2.91, 0.188)
        ElseIf intModulo = 11 Then
            Me.txtPrecioCosto.Visible = True
            Me.txtPrecioVentaMayoreo.Visible = True
            Me.txtPrecioVentaPublico.Visible = True
            Me.lblPrecioCosto.Visible = True
            Me.lblPrecioVentaMayoreo.Visible = True
            Me.lblPrecioVentaPublico.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 43 Then
            Me.txtPrecioCosto.Visible = True
            Me.lblPrecioCosto.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
            Me.Label2.Left = 5.25!
            Me.TextBox2.Left = 5.25!
        ElseIf intModulo = 44 Then
            Me.txtPrecioVentaPublico.Visible = True
            Me.lblPrecioVentaPublico.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 45 Then
            Me.txtPrecioVentaMayoreo.Visible = True
            Me.lblPrecioVentaMayoreo.Visible = True
            Me.Label1.Text = "Nombre"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
        ElseIf intModulo = 50 Then
            Me.TextBox3.Visible = False
            Me.Label.Text = "Usuario"
            Me.Label1.Text = "Nombre"
            Me.Label2.Text = "Estado"
            Me.Label2.Visible = True
            Me.TextBox2.Visible = True
            Me.TextBox2.Size = New System.Drawing.SizeF(2.91, 0.188)
        End If
        TextBox4.Text = Format(Now, "dd-MMM-yyyy")
        TextBox5.Text = Format(Now, "hh:mm:ss tt")
        Select Case intModulo
            Case 1 : Label4.Text = "REPORTE DE LAS AGENCIAS"
            Case 2 : Label4.Text = "REPORTE DE LOS DEPARTAMENTOS"
            Case 3 : Label4.Text = "REPORTE DE LOS MUNICIPIOS"
            Case 4 : Label4.Text = "REPORTE DE LOS VENDEDORES"
            Case 5 : Label4.Text = "REPORTE DE LOS CLIENTES"
            Case 6 : Label4.Text = "REPORTE DE LAS CLASES"
            Case 7 : Label4.Text = "REPORTE DE LAS SUBCLASES"
            Case 8 : Label4.Text = "REPORTE DE LOS EMPAQUES"
            Case 9 : Label4.Text = "REPORTE DE LAS UNIDADES"
            Case 10 : Label4.Text = "REPORTE DE LOS PROVEEDORES"
            Case 11 : Label4.Text = "LISTADO DE PRECIOS"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 13 : Label4.Text = "REPORTE DE LOS NEGOCIOS"
            Case 17 : Label4.Text = "REPORTE DE LAS RAZONES PARA ANULAR"
            Case 19 : Label4.Text = "REPORTE DE LOS TIPO DE CAMBIO"
            Case 22 : Label4.Text = "REPORTE DE LOS BANCOS"
            Case 23 : Label4.Text = "REPORTE DE LAS CUENTAS CONTABLES"
            Case 27 : Label4.Text = "REPORTE DE LOS EMPLEADOS"
            Case 28 : Label4.Text = "REPORTE DE LAS CUENTAS BANCARIAS"
            Case 29 : Label4.Text = "REPORTE DE LAS CHEQUERAS"
            Case 35 : Label4.Text = "REPORTE DE VENDEDORES Y SUS COMISIONES"
            Case 42 : Label4.Text = "REPORTE DE LOS BENEFICIARIOS"
            Case 43 : Label4.Text = "REPORTE DE PRECIO DE COSTO"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 44 : Label4.Text = "REPORTE DE PRECIO DE VENTAS MINORISTA"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 45 : Label4.Text = "REPORTE DE PRECIO DE VENTAS MAYORISTA"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "
            Case 50 : Label4.Text = "REPORTE DE USUARIOS"
                lblEmpresa.Text = " VETERINARIA EL GANADERO "

        End Select
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.5F

        Me.Document.Printer.PrinterName = ""

    End Sub
End Class
