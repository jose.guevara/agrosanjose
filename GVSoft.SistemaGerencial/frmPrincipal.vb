﻿Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO

Public Class frmPrincipal
    Dim btnActual As ButtonItem = New ButtonItem()
    'Dim IdUsuario As Integer
    Dim nIdUsuario As Integer
    Dim dtOpciones As DataTable = Nothing
    Dim dtGrupos As DataTable = Nothing
    Dim dtModulos As DataTable = Nothing
    Dim dtOpcionesxUsuario As DataTable = Nothing
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmPrincipal"
    Protected WithEvents PreviousButton As Button


    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        InicializaToolBar(True)

    End Sub

    Private Sub InicializaToolBar(ByVal useRibbon As Boolean)
        nIdUsuario = lngRegUsuario
        'IdUsuario = lngRegUsuario
        creaTabsDinamicos()
    End Sub

    Private Sub creaTabsDinamicos()
        Dim RBPPanelActual As RibbonPanel
        Dim TabActual As RibbonTabItem
        Try
            dtOpcionesxUsuario = RNSeguridad.ObtieneOpcionesxUsuario(nIdUsuario)
            dtModulos = RNSeguridad.ObtieneModulosxUsuario(nIdUsuario)
            If (dtModulos.Rows.Count > 0) Then
                For j As Integer = 0 To dtModulos.Rows.Count - 1
                    RBPPanelActual = New RibbonPanel()
                    RBPPanelActual.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
                    RBPPanelActual.Dock = System.Windows.Forms.DockStyle.Fill
                    RBPPanelActual.DockPadding.Bottom = 1
                    RBPPanelActual.Location = New System.Drawing.Point(0, 25)
                    RBPPanelActual.Name = "Panel" & dtModulos.Rows(j)("DesModulo").ToString()
                    RBPPanelActual.Size = New System.Drawing.Size(640, 85)

                    creaBarraHerramienta(RBPPanelActual, SUConversiones.ConvierteAInt(dtModulos.Rows(j)("IdModulo").ToString()))

                    TabActual = New RibbonTabItem()
                    TabActual.Name = dtModulos.Rows(j)("DesModulo").ToString()
                    TabActual.Panel = RBPPanelActual
                    TabActual.Text = dtModulos.Rows(j)("DesModulo").ToString()

                    RBCPrincipal.Controls.Add(RBPPanelActual)
                    RBCPrincipal.Items.Add(TabActual)

                    TabActual.Checked = True
                Next
            End If

            RBCPrincipal.SelectedRibbonTabItem = RBCPrincipal.Items("Facturación")  'Indica el Tab que se mostrará por defecto 

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Sub creaBarraHerramienta(ByVal RBPPanelActual As RibbonPanel, ByVal IdModulo As Integer)
        Dim RBBarActual As RibbonBar
        Dim nIdGrupo As Integer = 0
        Try
            dtGrupos = RNSeguridad.ObtieneGruposxUsuarioYModulo(nIdUsuario, IdModulo)
            If dtGrupos.Rows.Count > 0 Then
                For x As Integer = 0 To dtGrupos.Rows.Count - 1
                    RBBarActual = New RibbonBar()
                    RBBarActual.AutoOverflowEnabled = True
                    RBBarActual.Dock = System.Windows.Forms.DockStyle.Left
                    RBBarActual.Name = "RBBar" & dtGrupos.Rows(x)("DesGrupo").ToString()
                    RBBarActual.Size = New System.Drawing.Size(136, 84)
                    RBBarActual.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
                    RBBarActual.Text = dtGrupos.Rows(x)("DesGrupo").ToString()
                    nIdGrupo = SUConversiones.ConvierteAInt(dtGrupos.Rows(x)("IdGrupo").ToString())
                    CreaBotonesEnBarra(IdModulo, nIdGrupo, RBBarActual)
                    RBPPanelActual.Controls.Add(RBBarActual)
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Function ObtieneImagen(ByVal NombreImagen As String) As Image
        Dim imagen As Image = Nothing
        Try
            Dim NombreCarpeta As String
            Dim NombreArchivo As String = String.Empty
            NombreCarpeta = Directory.GetCurrentDirectory()
            If NombreCarpeta.EndsWith("\bin\Debug") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin\Debug", "")
            End If
            If NombreCarpeta.EndsWith("\bin") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin", "")
            End If
            NombreCarpeta = NombreCarpeta & "\Imagenes\"
            NombreArchivo = NombreCarpeta & NombreImagen
            If File.Exists(NombreArchivo) Then
                imagen = Image.FromFile(NombreArchivo)
            End If


        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Return Nothing
        End Try
        Return imagen
    End Function

    Private Sub ButtonItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousButton.Click
        Dim NombreBoton As String = String.Empty
        Dim NombreOpcion As String = String.Empty
        Dim IdBoton As String = String.Empty
        intError = 0
        strQuery = ""
        strAgro2K = ""
        'strUsuario = ""
        strAppPath = ""
        intTipoFactura = 0
        cmdAgro2K = New SqlCommand
        strAppPath = System.Windows.Forms.Application.StartupPath
        strAgro2K = ConfigurationManager.AppSettings("ConSistemaGerencial").ToString()
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
            cnnAgro2KTmp.Close()
        Else
            cnnAgro2K.ConnectionString = strAgro2K
            cnnAgro2KTmp.ConnectionString = strAgro2K
        End If

        NombreBoton = DirectCast(sender, DevComponents.DotNetBar.ButtonItem).PlainText
        IdBoton = DirectCast(sender, DevComponents.DotNetBar.ButtonItem).Name
        If Not dtOpcionesxUsuario Is Nothing Then
            If dtOpcionesxUsuario.Rows.Count > 0 Then
                For j As Integer = 0 To dtOpcionesxUsuario.Rows.Count - 1
                    NombreOpcion = dtOpcionesxUsuario.Rows(j)("NombreOpcionMenu").ToString()
                    'If NombreBoton.Trim().ToLower() = NombreOpcion.Trim().ToLower() Then
                    If IdBoton = dtOpcionesxUsuario.Rows(j)("IdOpcionMenu").ToString() Then
                        Select Case dtOpcionesxUsuario.Rows(j)("IdModulo").ToString()
                            Case EnumModulo.Facturacion
                                nIdModulo = EnumModulo.Facturacion
                            Case EnumModulo.Inventario
                                nIdModulo = EnumModulo.Inventario
                                intTipoFactura = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                            Case EnumModulo.Compra
                                nIdModulo = EnumModulo.Compra
                                intTipoCompra = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                            Case EnumModulo.CtaxCobrar
                                nIdModulo = EnumModulo.CtaxCobrar
                                intTipoReciboCaja = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                            Case EnumModulo.Parametros
                                nIdModulo = EnumModulo.Parametros
                            Case EnumModulo.ExportarImportar
                                nIdModulo = EnumModulo.ExportarImportar
                                intExportarModulo = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                            Case EnumModulo.Operaciones
                                nIdModulo = EnumModulo.Operaciones
                            Case EnumModulo.CtasxPagar
                                nIdModulo = EnumModulo.CtasxPagar
                                intTipoReciboCaja = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                            Case EnumModulo.Proformas
                                nIdModulo = EnumModulo.Proformas
                            Case EnumModulo.Pedidos
                                nIdModulo = EnumModulo.Pedidos
                                intTipoFactura = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                        End Select


                        If dtOpcionesxUsuario.Rows(j)("IdModulo").ToString() = "1" Or dtOpcionesxUsuario.Rows(j)("IdModulo").ToString() = "2" Then
                            intTipoFactura = SUConversiones.ConvierteAInt(dtOpcionesxUsuario.Rows(j)("IdSistema").ToString())
                        End If
                        Dim a As [Assembly] = [Assembly].GetEntryAssembly()
                        Dim objType() As Type = a.GetTypes()
                        For Each t As Type In objType
                            If t.Name.Trim().ToLower() = dtOpcionesxUsuario.Rows(j)("Forma").ToString().Trim().ToLower() Then
                                If ValidaVentanaAbierta(dtOpcionesxUsuario.Rows(j)("Forma").ToString()) Then
                                    CreaInstanciaForma(t, dtOpcionesxUsuario.Rows(j)("TituloForma").ToString())
                                    Exit For
                                Else
                                    CreaInstanciaForma(t, dtOpcionesxUsuario.Rows(j)("TituloForma").ToString())
                                    Exit For
                                End If
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If
        End If
    End Sub

    Private Function ValidaVentanaAbierta(ByVal NombreVentana As String) As Boolean
        Dim frm As Form
        For Each frm In Me.MdiChildren
            If frm.Name.Trim().ToLower() = NombreVentana.Trim().ToLower() Then
                frm.Focus()
                Return True
                Exit Function
            End If
        Next
        Return False
    End Function

    Private Sub BrintToForm(ByVal NombreVentana As String)
        Dim frm As Form
        For Each frm In Me.MdiChildren
            If frm.Name.Trim().ToLower() = NombreVentana.Trim().ToLower() Then
                frm.BringToFront()
                Exit Sub
            End If
        Next
    End Sub

    Private Sub CreaInstanciaForma(ByVal t As Type, ByVal TituloForma As String)
        Dim frm As Form = CType(Activator.CreateInstance(t), Form)
        frm.MdiParent = Me
        frm.Show()
        frm.Text = TituloForma & "( " & strNombreSuc & " )"
    End Sub

    Private Sub CierraFormaActual(ByVal NombreVentana As String)
        Dim frm As Form
        For Each frm In Me.MdiChildren
            If frm.Name.Trim().ToLower() = NombreVentana.Trim().ToLower() Then
                frm.Close()
                Exit Sub
            End If
        Next
    End Sub

    Private Sub CreaBotonesEnBarra(ByVal IdModulo As Integer, ByVal IdGrupo As Integer, ByVal RBActual As RibbonBar)
        Dim btnActual As ButtonItem
        Try
            dtOpciones = RNSeguridad.ObtieneOpcionesxUsuarioYModuloYGrupo(nIdUsuario, IdModulo, IdGrupo)
            If dtOpciones.Rows.Count > 0 Then
                For j As Integer = 0 To dtOpciones.Rows.Count - 1
                    btnActual = New ButtonItem()
                    btnActual.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
                    btnActual.Tooltip = dtOpciones.Rows(j)("DesOpcionMenu").ToString()
                    btnActual.ImageFixedSize = New System.Drawing.Size(40, 40)
                    btnActual.Image = ObtieneImagen(dtOpciones.Rows(j)("Icono").ToString())
                    btnActual.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
                    ' btnActual.Name = "Boton" & j
                    btnActual.Text = "<div align=""center"">" & dtOpciones.Rows(j)("NombreOpcionMenu").ToString() & "</div>"
                    btnActual.Name = dtOpciones.Rows(j)("IdOpcionMenu").ToString()
                    RBActual.Items.Add(btnActual)
                    AddHandler btnActual.Click, AddressOf ButtonItemClick
                Next
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'RBCPrincipal.SelectedRibbonTabItem = RBCPrincipal.Items("Facturación") 
        UltraStatusBar1.Panels.Item(1).Text = Format(Now, "dddd, dd-MMM-yyyy")
        UltraStatusBar1.Panels.Item(2).Text = Format(Now, "hh:mm:ss tt")
        UltraStatusBar1.Panels.Item(3).Text = strNombreUsuario
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-NI")
    End Sub

    Private Sub cmdSalirSistema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalirSistema.Click
        Me.Close()
        Application.Exit()
    End Sub

    Private Sub frmPrincipal_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub

End Class