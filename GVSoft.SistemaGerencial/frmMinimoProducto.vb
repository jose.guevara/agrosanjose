Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmMinimoProducto
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnListado As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnGeneral As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCambios As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReportes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnRegistros As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrimerRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAnteriorRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSiguienteRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUltimoRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnIgual As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents gcMinimoProducto As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvMinimoProducto As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CantidadMinima As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Codigo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Activo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents utcAgencia As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label3 As Label
    Friend WithEvents txtProducto As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtNombreProducto As TextBox
    Friend WithEvents speCantidadMinima As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label1 As Label
    Friend WithEvents chkActivo As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents descripcion As DevExpress.XtraGrid.Columns.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMinimoProducto))
        Dim UltraStatusPanel7 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel8 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.speCantidadMinima = New DevExpress.XtraEditors.SpinEdit()
        Me.txtNombreProducto = New System.Windows.Forms.TextBox()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.utcAgencia = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.btnListado = New DevComponents.DotNetBar.ButtonItem()
        Me.btnGeneral = New DevComponents.DotNetBar.ButtonItem()
        Me.btnCambios = New DevComponents.DotNetBar.ButtonItem()
        Me.btnReportes = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.btnRegistros = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrimerRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAnteriorRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSiguienteRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUltimoRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnIgual = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.gcMinimoProducto = New DevExpress.XtraGrid.GridControl()
        Me.gvMinimoProducto = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Codigo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.descripcion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CantidadMinima = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Activo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkActivo = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox1.SuspendLayout()
        CType(Me.speCantidadMinima.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.gcMinimoProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvMinimoProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkActivo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.speCantidadMinima)
        Me.GroupBox1.Controls.Add(Me.txtNombreProducto)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.utcAgencia)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(686, 105)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Tipo de Cambio"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 73)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 13)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "Cantidad M�nima"
        '
        'speCantidadMinima
        '
        Me.speCantidadMinima.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.speCantidadMinima.Location = New System.Drawing.Point(117, 73)
        Me.speCantidadMinima.Name = "speCantidadMinima"
        Me.speCantidadMinima.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.speCantidadMinima.Properties.Mask.EditMask = "N"
        Me.speCantidadMinima.Size = New System.Drawing.Size(166, 20)
        Me.speCantidadMinima.TabIndex = 59
        '
        'txtNombreProducto
        '
        Me.txtNombreProducto.Location = New System.Drawing.Point(286, 45)
        Me.txtNombreProducto.Name = "txtNombreProducto"
        Me.txtNombreProducto.ReadOnly = True
        Me.txtNombreProducto.Size = New System.Drawing.Size(368, 20)
        Me.txtNombreProducto.TabIndex = 58
        Me.txtNombreProducto.Tag = ""
        '
        'txtProducto
        '
        Me.txtProducto.Location = New System.Drawing.Point(117, 45)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(169, 20)
        Me.txtProducto.TabIndex = 57
        Me.txtProducto.Tag = "C�digo del Producto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 49)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Producto"
        '
        'utcAgencia
        '
        Me.utcAgencia.DisplayMember = "Text"
        Me.utcAgencia.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.utcAgencia.FormattingEnabled = True
        Me.utcAgencia.ItemHeight = 14
        Me.utcAgencia.Location = New System.Drawing.Point(117, 19)
        Me.utcAgencia.Name = "utcAgencia"
        Me.utcAgencia.Size = New System.Drawing.Size(534, 20)
        Me.utcAgencia.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.utcAgencia.TabIndex = 55
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Agencia"
        '
        'Timer1
        '
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 453)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel8.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel7, UltraStatusPanel8})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(690, 23)
        Me.UltraStatusBar1.TabIndex = 2
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnListado, Me.LabelItem4, Me.btnRegistros, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(690, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 61
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnListado
        '
        Me.btnListado.AutoExpandOnClick = True
        Me.btnListado.Image = CType(resources.GetObject("btnListado.Image"), System.Drawing.Image)
        Me.btnListado.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnListado.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnListado.Name = "btnListado"
        Me.btnListado.OptionGroup = "Listado de productos"
        Me.btnListado.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnGeneral, Me.btnCambios, Me.btnReportes})
        Me.btnListado.Text = "Listados"
        Me.btnListado.Tooltip = "Listados"
        '
        'btnGeneral
        '
        Me.btnGeneral.Image = CType(resources.GetObject("btnGeneral.Image"), System.Drawing.Image)
        Me.btnGeneral.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnGeneral.Name = "btnGeneral"
        Me.btnGeneral.Text = "General"
        Me.btnGeneral.Tooltip = "General"
        '
        'btnCambios
        '
        Me.btnCambios.Image = CType(resources.GetObject("btnCambios.Image"), System.Drawing.Image)
        Me.btnCambios.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnCambios.Name = "btnCambios"
        Me.btnCambios.Tag = "Cambios"
        Me.btnCambios.Text = "Cambios"
        Me.btnCambios.Tooltip = "Cambios"
        '
        'btnReportes
        '
        Me.btnReportes.Image = CType(resources.GetObject("btnReportes.Image"), System.Drawing.Image)
        Me.btnReportes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Tag = "Reportes"
        Me.btnReportes.Text = "Reportes"
        Me.btnReportes.Tooltip = "Reportes"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'btnRegistros
        '
        Me.btnRegistros.AutoExpandOnClick = True
        Me.btnRegistros.Image = CType(resources.GetObject("btnRegistros.Image"), System.Drawing.Image)
        Me.btnRegistros.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnRegistros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.OptionGroup = "Recorre Registros"
        Me.btnRegistros.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPrimerRegistro, Me.btnAnteriorRegistro, Me.btnSiguienteRegistro, Me.btnUltimoRegistro, Me.btnIgual})
        Me.btnRegistros.Text = "Recorre Registros"
        Me.btnRegistros.Tooltip = "Recorre Registros"
        '
        'btnPrimerRegistro
        '
        Me.btnPrimerRegistro.Image = CType(resources.GetObject("btnPrimerRegistro.Image"), System.Drawing.Image)
        Me.btnPrimerRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrimerRegistro.Name = "btnPrimerRegistro"
        Me.btnPrimerRegistro.Text = "Primer registro"
        Me.btnPrimerRegistro.Tooltip = "Primer registro"
        '
        'btnAnteriorRegistro
        '
        Me.btnAnteriorRegistro.Image = CType(resources.GetObject("btnAnteriorRegistro.Image"), System.Drawing.Image)
        Me.btnAnteriorRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnAnteriorRegistro.Name = "btnAnteriorRegistro"
        Me.btnAnteriorRegistro.Text = "Anterior registro"
        Me.btnAnteriorRegistro.Tooltip = "Anterior registro"
        '
        'btnSiguienteRegistro
        '
        Me.btnSiguienteRegistro.Image = CType(resources.GetObject("btnSiguienteRegistro.Image"), System.Drawing.Image)
        Me.btnSiguienteRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnSiguienteRegistro.Name = "btnSiguienteRegistro"
        Me.btnSiguienteRegistro.Tag = "Siguiente registro"
        Me.btnSiguienteRegistro.Text = "Siguiente registro"
        Me.btnSiguienteRegistro.Tooltip = "Siguiente registro"
        '
        'btnUltimoRegistro
        '
        Me.btnUltimoRegistro.Image = CType(resources.GetObject("btnUltimoRegistro.Image"), System.Drawing.Image)
        Me.btnUltimoRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnUltimoRegistro.Name = "btnUltimoRegistro"
        Me.btnUltimoRegistro.Tag = "Ultimo registro"
        Me.btnUltimoRegistro.Text = "Ultimo registro"
        Me.btnUltimoRegistro.Tooltip = "Ultimo registro"
        '
        'btnIgual
        '
        Me.btnIgual.Image = CType(resources.GetObject("btnIgual.Image"), System.Drawing.Image)
        Me.btnIgual.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnIgual.Name = "btnIgual"
        Me.btnIgual.Text = "Igual"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.gcMinimoProducto)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 192)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(686, 245)
        Me.GroupBox2.TabIndex = 62
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detalle tasa de cambio"
        '
        'gcMinimoProducto
        '
        Me.gcMinimoProducto.Location = New System.Drawing.Point(8, 28)
        Me.gcMinimoProducto.MainView = Me.gvMinimoProducto
        Me.gcMinimoProducto.Name = "gcMinimoProducto"
        Me.gcMinimoProducto.Size = New System.Drawing.Size(656, 201)
        Me.gcMinimoProducto.TabIndex = 0
        Me.gcMinimoProducto.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvMinimoProducto})
        '
        'gvMinimoProducto
        '
        Me.gvMinimoProducto.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Codigo, Me.descripcion, Me.CantidadMinima, Me.Activo})
        Me.gvMinimoProducto.GridControl = Me.gcMinimoProducto
        Me.gvMinimoProducto.Name = "gvMinimoProducto"
        Me.gvMinimoProducto.OptionsView.ShowGroupPanel = False
        '
        'Codigo
        '
        Me.Codigo.Caption = "C�digo Producto"
        Me.Codigo.FieldName = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.OptionsColumn.ReadOnly = True
        Me.Codigo.Tag = "C�digo Producto"
        Me.Codigo.ToolTip = "C�digo Prodcuto"
        Me.Codigo.Visible = True
        Me.Codigo.VisibleIndex = 0
        Me.Codigo.Width = 102
        '
        'descripcion
        '
        Me.descripcion.Caption = "Descripci�n Producto"
        Me.descripcion.FieldName = "descripcion"
        Me.descripcion.Name = "descripcion"
        Me.descripcion.OptionsColumn.ReadOnly = True
        Me.descripcion.Tag = "Descripci�n Producto"
        Me.descripcion.Visible = True
        Me.descripcion.VisibleIndex = 1
        Me.descripcion.Width = 269
        '
        'CantidadMinima
        '
        Me.CantidadMinima.Caption = "Cantidad M�nima"
        Me.CantidadMinima.FieldName = "CantidadMinima"
        Me.CantidadMinima.Name = "CantidadMinima"
        Me.CantidadMinima.OptionsColumn.ReadOnly = True
        Me.CantidadMinima.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.CantidadMinima.Tag = "Cantidad M�nima"
        Me.CantidadMinima.Visible = True
        Me.CantidadMinima.VisibleIndex = 2
        Me.CantidadMinima.Width = 84
        '
        'Activo
        '
        Me.Activo.Caption = "Activo"
        Me.Activo.FieldName = "Activo"
        Me.Activo.Name = "Activo"
        Me.Activo.Tag = "Activo"
        Me.Activo.ToolTip = "Activo"
        Me.Activo.Visible = True
        Me.Activo.VisibleIndex = 3
        Me.Activo.Width = 50
        '
        'chkActivo
        '
        '
        '
        '
        Me.chkActivo.BackgroundStyle.Class = ""
        Me.chkActivo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkActivo.Location = New System.Drawing.Point(292, 73)
        Me.chkActivo.Name = "chkActivo"
        Me.chkActivo.Size = New System.Drawing.Size(84, 20)
        Me.chkActivo.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.chkActivo.TabIndex = 61
        Me.chkActivo.Text = "Activo"
        '
        'frmMinimoProducto
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(690, 476)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMinimoProducto"
        Me.Text = "frmMinimoProducto"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.speCantidadMinima.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.gcMinimoProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvMinimoProducto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim strCambios As String = String.Empty
    Dim strUnidad As String = String.Empty
    Dim strFacturas As String = String.Empty
    Dim strDescrip As String = 0
    Dim intImpuesto As Integer = 0

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmMinimoProducto"
    Private Sub frmMinimoProducto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.Top = 0
            Me.Left = 0
            ObtieneAgencias()
            'ObtieneProductos()
            'Limpiar()
            'CargaGridTipoCambio()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Cantidades M�nimas Producto")
            Exit Sub
        End Try
    End Sub

    Sub ObtieneAgencias()
        Try
            RNAgencias.CargarComboAgenciasConTodos(utcAgencia, lngRegUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    'Sub ObtieneProductos()
    '    Try
    '        RNProduto.CargaComboProductos(cmbProducto)

    '    Catch ex As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = ex.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
    '    End Try

    'End Sub
    Function UbicarProducto(ByVal strDato As String) As Boolean

        strDescrip = String.Empty
        strUnidad = String.Empty
        lngRegistro = 0
        intImpuesto = 0
        strQuery = String.Empty
        Dim lnIdAgencia As Integer = 0
        Try
            'strQuery = "Select P.Descripcion, U.Codigo, P.Registro, P.Impuesto from prm_Productos P, prm_Unidades U "
            'strQuery = strQuery + "Where P.Codigo = '" & strDato & "' AND P.RegUnidad = U.Registro AND P.Estado = 0"
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = RNProduto.UbicarProducto(strDato)
            While dtrAgro2K.Read
                strDescrip = dtrAgro2K.Item("Descripcion")
                strUnidad = dtrAgro2K.Item("CodigoUnidad")
                lngRegistro = SUConversiones.ConvierteAInt(dtrAgro2K.Item("Registro"))
                intImpuesto = SUConversiones.ConvierteAInt(dtrAgro2K.Item("Impuesto"))
                txtNombreProducto.Text = dtrAgro2K.Item("Descripcion").ToString()
                txtProducto.Text = dtrAgro2K.Item("CodigoProducto").ToString()
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            lnIdAgencia = SUConversiones.ConvierteAInt(utcAgencia.SelectedValue)
            gcMinimoProducto.DataSource = RNProduto.ObtieneCantidadMinimaxIdSucursalYCodigoProducto(lnIdAgencia, txtProducto.Text.Trim())
            UbicarProducto = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function
    Private Sub CargaGrid(ByVal pnIdAgencia As Integer)
        Try

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Private Sub ObtengoDatosListadoAyuda()
        Try
            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : speCantidadMinima.Focus()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Private Sub frmMinimoProducto_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Dim frmNew As frmListadoAyuda = Nothing
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Guardar()
            ElseIf e.KeyCode = Keys.F4 Then
                'Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 3
                frmNew = New frmListadoAyuda
                frmNew.ShowDialog()

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tipo de cambio")
            Exit Sub
        End Try
    End Sub

    Private Sub DateTimePicker1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            UltraStatusBar1.Panels.Item(0).Text = IIf(sender.name = "DateTimePicker1", "Fecha", "Tipo de Cambio")
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name <> "DateTimePicker1" Then
                sender.selectall()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tipo de cambio")
            Exit Sub
        End Try

    End Sub

    'Private Sub DateTimePicker1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    Try
    '        If e.KeyCode = Keys.Enter Then
    '            Select Case sender.name.ToString
    '                Case "DateTimePicker1" : TextBox1.Focus()
    '                Case "TextBox1" : DateTimePicker1.Focus()
    '            End Select
    '        End If
    '    Catch ex As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = ex.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tipo de cambio")
    '        Exit Sub
    '    End Try

    'End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Dim lnIdAgencia As Integer = 0
        Dim lnIdProducto As Integer = 0
        Dim lnCantidadMinima As Integer = 0
        Dim lnActivo As Integer = 0
        Try
            lnIdAgencia = SUConversiones.ConvierteAInt(utcAgencia.SelectedValue)

            If lnIdAgencia = 0 Or lnIdAgencia = -1 Then
                MsgBox("Favor seleccionar la agencia ", MsgBoxStyle.Critical, "Cantidad M�nima Producto")
                Return
            End If
            lnActivo = 0
            If chkActivo.Checked Then
                lnActivo = 1
            End If
            lnCantidadMinima = SUConversiones.ConvierteAInt(speCantidadMinima.Text)
            If lngRegistro <= 0 Then
                MsgBox("Favor seleccionar un producto ", MsgBoxStyle.Critical, "Cantidad M�nima Producto")
                Return
            End If
            RNProduto.GuardarMinimosProductos(lnIdAgencia, lnIdProducto, lnCantidadMinima, lnActivo)
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Throw exc
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    'Sub Limpiar()
    '    Try
    '        If utcAgencia.r Then

    '    Catch exc As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = exc.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
    '        Throw exc

    '    Finally

    '    End Try

    'End Sub

    'Sub MoverRegistro(ByVal StrDato As String)
    '    Dim lngFecha As Long = 0
    '    Try
    '        lngFecha = 0
    '        lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
    '        Select Case StrDato
    '            Case "Primero"
    '                strQuery = "Select Max(NumFecha) from prm_TipoCambio"
    '            Case "Anterior"
    '                strQuery = "Select Min(NumFecha) from prm_TipoCambio Where numfecha > " & lngFecha & ""
    '            Case "Siguiente"
    '                strQuery = "Select Max(NumFecha) from prm_TipoCambio Where numfecha < " & lngFecha & ""
    '            Case "Ultimo"
    '                strQuery = "Select Min(NumFecha) from prm_TipoCambio"
    '            Case "Igual"
    '                strQuery = "Select NumFecha from prm_TipoCambio Where numfecha = " & lngFecha & ""
    '        End Select
    '        Limpiar()
    '        UbicarRegistro(StrDato)

    '    Catch exc As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = exc.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
    '        Throw exc
    '    Finally
    '    End Try


    'End Sub

    Private Sub CargaGridTipoCambio()
        Try
            gcMinimoProducto.DataSource = RNTasaCambio.ObtieneTasaCambioTodas()

        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Throw exc
        Finally
        End Try
    End Sub
    'Sub UbicarRegistro(ByVal StrDato As String)

    '    Dim lngCodigo As String = String.Empty
    '    Try
    '        If cnnAgro2K IsNot Nothing Then
    '            If cnnAgro2K.State = ConnectionState.Open Then
    '                cnnAgro2K.Close()
    '            End If
    '        End If
    '        If cmdAgro2K IsNot Nothing Then
    '            If cmdAgro2K.Connection IsNot Nothing Then
    '                If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '                    cmdAgro2K.Connection.Close()
    '                End If
    '            End If
    '        End If
    '        cnnAgro2K.Open()
    '        cmdAgro2K.Connection = cnnAgro2K
    '        cmdAgro2K.CommandText = strQuery
    '        dtrAgro2K = cmdAgro2K.ExecuteReader
    '        lngCodigo = 0
    '        While dtrAgro2K.Read
    '            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
    '                lngCodigo = dtrAgro2K.GetValue(0)
    '            End If
    '        End While
    '        dtrAgro2K.Close()
    '        If lngCodigo = 0 Then
    '            If StrDato = "Igual" Then
    '                MsgBox("No existe un registro con esa fecha en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
    '            Else
    '                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
    '            End If
    '            If cnnAgro2K IsNot Nothing Then
    '                If cnnAgro2K.State = ConnectionState.Open Then
    '                    cnnAgro2K.Close()
    '                End If
    '            End If
    '            If cmdAgro2K IsNot Nothing Then
    '                If cmdAgro2K.Connection IsNot Nothing Then
    '                    If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '                        cmdAgro2K.Connection.Close()
    '                    End If
    '                End If
    '            End If
    '            'cmdAgro2K.Connection.Close()
    '            'cnnAgro2K.Close()
    '            Exit Sub
    '        End If
    '        strQuery = ""
    '        strQuery = "Select * From prm_TipoCambio Where numfecha = " & lngCodigo & ""
    '        cmdAgro2K.CommandText = strQuery
    '        dtrAgro2K = cmdAgro2K.ExecuteReader
    '        While dtrAgro2K.Read
    '            TextBox1.Text = dtrAgro2K.GetValue(1)
    '            strCambios = dtrAgro2K.GetValue(1)
    '            TextBox2.Text = dtrAgro2K.GetValue(2)
    '            DateTimePicker1.Value = DefinirFecha(dtrAgro2K.GetValue(2))
    '            DateTimePicker1.Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
    '        End While
    '        If dtrAgro2K IsNot Nothing Then
    '            dtrAgro2K.Close()
    '        End If
    '        If cnnAgro2K IsNot Nothing Then
    '            If cnnAgro2K.State = ConnectionState.Open Then
    '                cnnAgro2K.Close()
    '            End If
    '        End If
    '        If cmdAgro2K IsNot Nothing Then
    '            If cmdAgro2K.Connection IsNot Nothing Then
    '                If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '                    cmdAgro2K.Connection.Close()
    '                End If
    '            End If
    '        End If
    '        'cmdAgro2K.Connection.Close()
    '        'cnnAgro2K.Close()


    '    Catch exc As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = exc.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
    '        Throw exc
    '    Finally
    '    End Try


    'End Sub


    'Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

    '    MoverRegistro(sender.text)

    'End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            If blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    Dim strFecha As String
                    strFecha = ""
                    strFecha = CDate(strUbicar)
                    'DateTimePicker1.Value = strFecha
                    ' MoverRegistro("Igual")
                End If
            End If

        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally

        End Try

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            Guardar()
            CargaGridTipoCambio()
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            ' Limpiar()
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try

    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        'Dim frmNew As New frmGeneral
        'lngRegistro = TextBox2.Text
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        'frmNew.ShowDialog()
    End Sub

    'Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
    '    Dim frmNew As New frmBitacora
    '    Try
    '        ' lngRegistro = TextBox2.Text
    '        frmNew.ShowDialog()

    '    Catch exc As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = exc.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
    '        'Throw exc
    '    Finally
    '    End Try
    'End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Dim frmNew As New actrptViewer
        Try
            intRptFactura = 30
            strQuery = "exec sp_Reportes " & intModulo & " "
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()

        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try
    End Sub

    Private Sub btnListado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListado.Click

    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        Try
            'MoverRegistro("Primero")
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try

    End Sub

    'Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
    '    Try
    '        MoverRegistro("Anterior")
    '    Catch exc As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = sNombreClase
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = exc.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
    '        SNError.IngresaError(objError)
    '        MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
    '        'Throw exc
    '    Finally
    '    End Try

    'End Sub

    Private Sub btnRegistros_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistros.Click

    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        Try
            'MoverRegistro("Siguiente")
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try

    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        Try
            'MoverRegistro("Ultimo")
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try

    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        Try
            ' MoverRegistro("Igual")
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Try
            Me.Close()
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            'Throw exc
        Finally
        End Try

    End Sub

    Private Sub txtProducto_KeyDown(sender As Object, e As KeyEventArgs) Handles txtProducto.KeyDown, utcAgencia.KeyDown, speCantidadMinima.KeyDown
        Try
            'Select Case 
            If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
                Select Case sender.name.ToString
                    Case "utcAgencia"
                        txtProducto.Focus()
                    Case "txtProducto"
                        blnUbicar = False
                        If UbicarProducto(txtProducto.Text) = False Then
                            MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtProducto.Focus()
                            Exit Sub
                        End If
                        speCantidadMinima.Focus()
                    Case "speCantidadMinima"
                        chkActivo.Focus()
                    Case "chkActivo"
                        chkActivo.Checked = Not chkActivo.Checked
                        utcAgencia.Focus()
                End Select
            End If

        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        End Try

    End Sub

    Private Sub txtProducto_GotFocus(sender As Object, e As EventArgs) Handles txtProducto.GotFocus
        Try
            Select Case sender.name.ToString
                Case "txtProducto"
                    If blnUbicar Then
                        ObtengoDatosListadoAyuda()
                    End If
            End Select
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        End Try
    End Sub

    Private Sub utcAgencia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles utcAgencia.SelectedIndexChanged
        Try

        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        End Try
    End Sub
End Class
