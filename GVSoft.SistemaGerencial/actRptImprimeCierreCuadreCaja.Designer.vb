<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actRptImprimeCierreCuadreCaja 
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actRptImprimeCierreCuadreCaja))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader()
        Me.Label15 = New DataDynamics.ActiveReports.Label()
        Me.Label16 = New DataDynamics.ActiveReports.Label()
        Me.TextBox25 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox()
        Me.Label20 = New DataDynamics.ActiveReports.Label()
        Me.Line1 = New DataDynamics.ActiveReports.Line()
        Me.Line = New DataDynamics.ActiveReports.Line()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.lblAgencia = New DataDynamics.ActiveReports.Label()
        Me.Detail1 = New DataDynamics.ActiveReports.Detail()
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter()
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader()
        Me.Shape2 = New DataDynamics.ActiveReports.Shape()
        Me.Shape1 = New DataDynamics.ActiveReports.Shape()
        Me.Shape3 = New DataDynamics.ActiveReports.Shape()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.Label14 = New DataDynamics.ActiveReports.Label()
        Me.Line2 = New DataDynamics.ActiveReports.Line()
        Me.Label17 = New DataDynamics.ActiveReports.Label()
        Me.Label18 = New DataDynamics.ActiveReports.Label()
        Me.Label19 = New DataDynamics.ActiveReports.Label()
        Me.Label21 = New DataDynamics.ActiveReports.Label()
        Me.Label22 = New DataDynamics.ActiveReports.Label()
        Me.Label23 = New DataDynamics.ActiveReports.Label()
        Me.Label24 = New DataDynamics.ActiveReports.Label()
        Me.Label28 = New DataDynamics.ActiveReports.Label()
        Me.Label29 = New DataDynamics.ActiveReports.Label()
        Me.Label32 = New DataDynamics.ActiveReports.Label()
        Me.Label33 = New DataDynamics.ActiveReports.Label()
        Me.Label34 = New DataDynamics.ActiveReports.Label()
        Me.Label37 = New DataDynamics.ActiveReports.Label()
        Me.Label40 = New DataDynamics.ActiveReports.Label()
        Me.Label43 = New DataDynamics.ActiveReports.Label()
        Me.Label45 = New DataDynamics.ActiveReports.Label()
        Me.Label47 = New DataDynamics.ActiveReports.Label()
        Me.Label50 = New DataDynamics.ActiveReports.Label()
        Me.Label51 = New DataDynamics.ActiveReports.Label()
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox8 = New DataDynamics.ActiveReports.TextBox()
        Me.lblTotalIngresos = New DataDynamics.ActiveReports.TextBox()
        Me.lblTotalEgresos = New DataDynamics.ActiveReports.TextBox()
        Me.lblDifIngEgr = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox()
        Me.lblDineroCaja = New DataDynamics.ActiveReports.TextBox()
        Me.lblDiferenciaEfectivo = New DataDynamics.ActiveReports.TextBox()
        Me.lblSaldoAnterior = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox6 = New DataDynamics.ActiveReports.TextBox()
        Me.lblTotalCaja = New DataDynamics.ActiveReports.TextBox()
        Me.lblGastos = New DataDynamics.ActiveReports.TextBox()
        Me.lblDepositos = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox9 = New DataDynamics.ActiveReports.TextBox()
        Me.Label25 = New DataDynamics.ActiveReports.Label()
        Me.Label26 = New DataDynamics.ActiveReports.Label()
        Me.lblSobranteCaja = New DataDynamics.ActiveReports.TextBox()
        Me.lblOtrosIngresos = New DataDynamics.ActiveReports.TextBox()
        Me.Label30 = New DataDynamics.ActiveReports.Label()
        Me.Label35 = New DataDynamics.ActiveReports.Label()
        Me.Label36 = New DataDynamics.ActiveReports.Label()
        Me.Label38 = New DataDynamics.ActiveReports.Label()
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Line4 = New DataDynamics.ActiveReports.Line()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.Line5 = New DataDynamics.ActiveReports.Line()
        Me.Line6 = New DataDynamics.ActiveReports.Line()
        Me.Label31 = New DataDynamics.ActiveReports.Label()
        Me.ReportHeader1 = New DataDynamics.ActiveReports.ReportHeader()
        Me.ReportFooter1 = New DataDynamics.ActiveReports.ReportFooter()
        Me.TextBox7 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox()
        Me.Label27 = New DataDynamics.ActiveReports.Label()
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox()
        Me.Line3 = New DataDynamics.ActiveReports.Line()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalIngresos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalEgresos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDifIngEgr, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDineroCaja, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDiferenciaEfectivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSaldoAnterior, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalCaja, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDepositos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSobranteCaja, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOtrosIngresos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label15, Me.Label16, Me.TextBox25, Me.TextBox26, Me.Label20, Me.Line1, Me.Line, Me.Label, Me.lblAgencia})
        Me.PageHeader1.Height = 1.125!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 3.895!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label15.Text = "Fecha de Impresi�n"
        Me.Label15.Top = 0.5!
        Me.Label15.Width = 1.32!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.905!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label16.Text = "Hora de Impresi�n"
        Me.Label16.Top = 0.687!
        Me.Label16.Width = 1.28!
        '
        'TextBox25
        '
        Me.TextBox25.Height = 0.2!
        Me.TextBox25.Left = 5.31!
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Style = "text-align: right"
        Me.TextBox25.Text = "TextBox25"
        Me.TextBox25.Top = 0.5!
        Me.TextBox25.Width = 1.08!
        '
        'TextBox26
        '
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 5.31!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right"
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.687!
        Me.TextBox26.Width = 1.09!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.1875!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label20.Text = "Informacion del Cierre de Caja"
        Me.Label20.Top = 0.5!
        Me.Label20.Width = 2.4375!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.625!
        Me.Line1.Width = 7.0625!
        Me.Line1.X1 = 0!
        Me.Line1.X2 = 7.0625!
        Me.Line1.Y1 = 1.625!
        Me.Line1.Y2 = 1.625!
        '
        'Line
        '
        Me.Line.Height = 0!
        Me.Line.Left = 0.0625!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 1.0625!
        Me.Line.Width = 6.3125!
        Me.Line.X1 = 0.0625!
        Me.Line.X2 = 6.375!
        Me.Line.Y1 = 1.0625!
        Me.Line.Y2 = 1.0625!
        '
        'Label
        '
        Me.Label.Height = 0.375!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.5625!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: center; font-weight: bold; font-size: 20.25pt; font-family: Times New" &
    " Roman"
        Me.Label.Text = "Agro Centro, S.A."
        Me.Label.Top = 0!
        Me.Label.Width = 5.4375!
        '
        'lblAgencia
        '
        Me.lblAgencia.DataField = "Sucursal"
        Me.lblAgencia.Height = 0.25!
        Me.lblAgencia.HyperLink = Nothing
        Me.lblAgencia.Left = 0.1875!
        Me.lblAgencia.Name = "lblAgencia"
        Me.lblAgencia.Style = "ddo-char-set: 0; text-align: justify; font-weight: normal; font-style: italic; fo" &
    "nt-size: 12pt; font-family: Times New Roman"
        Me.lblAgencia.Text = "Sucursal:  "
        Me.lblAgencia.Top = 0.75!
        Me.lblAgencia.Width = 3.5925!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0!
        Me.Detail1.Height = 0!
        Me.Detail1.Name = "Detail1"
        '
        'PageFooter1
        '
        Me.PageFooter1.Height = 0.03125!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Shape2, Me.Shape1, Me.Shape3, Me.Label2, Me.Label1, Me.Label7, Me.Label3, Me.Label5, Me.Label6, Me.Label8, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Label14, Me.Line2, Me.Label17, Me.Label18, Me.Label19, Me.Label21, Me.Label22, Me.Label23, Me.Label24, Me.Label28, Me.Label29, Me.Label32, Me.Label33, Me.Label34, Me.Label37, Me.Label40, Me.Label43, Me.Label45, Me.Label47, Me.Label50, Me.Label51, Me.TextBox2, Me.TextBox8, Me.lblTotalIngresos, Me.lblTotalEgresos, Me.lblDifIngEgr, Me.TextBox4, Me.lblDineroCaja, Me.lblDiferenciaEfectivo, Me.lblSaldoAnterior, Me.TextBox6, Me.lblTotalCaja, Me.lblGastos, Me.lblDepositos, Me.TextBox9, Me.Label25, Me.Label26, Me.lblSobranteCaja, Me.lblOtrosIngresos, Me.Label30, Me.Label35, Me.Label36, Me.Label38})
        Me.GroupHeader1.DataField = "IdCuadre"
        Me.GroupHeader1.Height = 5.822917!
        Me.GroupHeader1.KeepTogether = True
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'Shape2
        '
        Me.Shape2.Height = 4.5625!
        Me.Shape2.Left = 0.0625!
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = 9.999999!
        Me.Shape2.Top = 0.6875!
        Me.Shape2.Width = 6.3125!
        '
        'Shape1
        '
        Me.Shape1.Height = 4.5625!
        Me.Shape1.Left = 0.0625!
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = 9.999999!
        Me.Shape1.Top = 0.6875!
        Me.Shape1.Width = 6.3125!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.375!
        Me.Shape3.Left = 0.0625!
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = 9.999999!
        Me.Shape3.Top = 0!
        Me.Shape3.Width = 6.3125!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.107!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label2.Text = "Numero Arqueo:"
        Me.Label2.Top = 0.115!
        Me.Label2.Width = 0.9280001!
        '
        'Label1
        '
        Me.Label1.DataField = "NumeroCuadre"
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 1.066!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label1.Text = ""
        Me.Label1.Top = 0.115!
        Me.Label1.Width = 0.9599998!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.1875!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label7.Text = "Numero Factura"
        Me.Label7.Top = 1.3125!
        Me.Label7.Width = 0.9375!
        '
        'Label3
        '
        Me.Label3.DataField = "FacturaContadoInicial"
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 1.1875!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label3.Text = ""
        Me.Label3.Top = 1.3125!
        Me.Label3.Width = 1.0!
        '
        'Label5
        '
        Me.Label5.DataField = "FacturaContadoFinal"
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 2.5625!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label5.Text = ""
        Me.Label5.Top = 1.3125!
        Me.Label5.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 0.1875!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label6.Text = "Numero Recibo"
        Me.Label6.Top = 1.53125!
        Me.Label6.Width = 0.9375!
        '
        'Label8
        '
        Me.Label8.DataField = "ReciboInicialCredito"
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 1.1875!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label8.Text = ""
        Me.Label8.Top = 1.53125!
        Me.Label8.Width = 1.0!
        '
        'Label10
        '
        Me.Label10.DataField = "ReciboFinalCredito"
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 2.5625!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label10.Text = ""
        Me.Label10.Top = 1.552083!
        Me.Label10.Width = 1.0!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 0.0625!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label11.Text = "Observacion"
        Me.Label11.Top = 5.375!
        Me.Label11.Width = 0.8125!
        '
        'Label12
        '
        Me.Label12.DataField = "Observacion"
        Me.Label12.Height = 0.375!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0.9375!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label12.Text = ""
        Me.Label12.Top = 5.3125!
        Me.Label12.Width = 5.0625!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0.0625!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label13.Text = "Informacion Detalle Arqueo"
        Me.Label13.Top = 0.5!
        Me.Label13.Width = 1.5625!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.1875!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label14.Text = "INGRESOS(+)"
        Me.Label14.Top = 1.0625!
        Me.Label14.Width = 0.8125!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0.0625!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 1.0!
        Me.Line2.Width = 6.3125!
        Me.Line2.X1 = 0.0625!
        Me.Line2.X2 = 6.375!
        Me.Line2.Y1 = 1.0!
        Me.Line2.Y2 = 1.0!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 0.125!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label17.Text = "OPERACION"
        Me.Label17.Top = 0.75!
        Me.Label17.Width = 0.8125!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 2.0!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label18.Text = "CONCEPTO"
        Me.Label18.Top = 0.75!
        Me.Label18.Width = 0.8125!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 3.697917!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "text-align: center; font-weight: bold; font-size: 8.25pt"
        Me.Label19.Text = "MONTO"
        Me.Label19.Top = 0.75!
        Me.Label19.Width = 0.8125!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 5.0625!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label21.Text = "TOTALES"
        Me.Label21.Top = 0.75!
        Me.Label21.Width = 0.8125!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 2.302083!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label22.Text = "AL"
        Me.Label22.Top = 1.3125!
        Me.Label22.Width = 0.1875!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 2.302083!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label23.Text = "AL"
        Me.Label23.Top = 1.53125!
        Me.Label23.Width = 0.1875!
        '
        'Label24
        '
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0.1875!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label24.Text = "TOTAL INGRESOS"
        Me.Label24.Top = 1.8125!
        Me.Label24.Width = 1.125!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0.1875!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label28.Text = "EGRESOS(-)"
        Me.Label28.Top = 2.0625!
        Me.Label28.Width = 0.8125!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 2.0!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label29.Text = "SALDO ANTERIOR"
        Me.Label29.Top = 3.5!
        Me.Label29.Width = 1.125!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 0.1875!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label32.Text = "TOTAL EGRESOS"
        Me.Label32.Top = 2.3125!
        Me.Label32.Width = 1.4375!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0.1875!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label33.Text = "INGRESOS - EGRESOS"
        Me.Label33.Top = 2.6875!
        Me.Label33.Width = 1.3125!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 0.1875!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label34.Text = "DIFERENCIA +/- EFECTIVO MENOS ING/EGRESOS"
        Me.Label34.Top = 3.1875!
        Me.Label34.Width = 2.8125!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0.1875!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label37.Text = "TOTAL DINERO EN CAJA"
        Me.Label37.Top = 2.9375!
        Me.Label37.Width = 1.4375!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 1.5!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label40.Text = "+ TOTAL INGRESOS DE HOY"
        Me.Label40.Top = 4.0625!
        Me.Label40.Width = 1.625!
        '
        'Label43
        '
        Me.Label43.Height = 0.1875!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 1.5!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label43.Text = "- TOTAL GASTOS DE HOY"
        Me.Label43.Top = 4.25!
        Me.Label43.Width = 1.625!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 1.5!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label45.Text = "= TOTAL EN CAJA"
        Me.Label45.Top = 4.4375!
        Me.Label45.Width = 1.625!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 1.5!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label47.Text = "- DEPOSITOS DE HOY"
        Me.Label47.Top = 4.625!
        Me.Label47.Width = 1.625!
        '
        'Label50
        '
        Me.Label50.Height = 0.1875!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 0.1875!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label50.Text = "SALDO FINAL DE HOY"
        Me.Label50.Top = 4.9375!
        Me.Label50.Width = 1.625!
        '
        'Label51
        '
        Me.Label51.Height = 0.1875!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 2.0!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label51.Text = "GASTOS"
        Me.Label51.Top = 2.1875!
        Me.Label51.Width = 0.8125!
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "MontoTotalVentas"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 3.6875!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat")
        Me.TextBox2.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.TextBox2.Text = Nothing
        Me.TextBox2.Top = 1.3125!
        Me.TextBox2.Width = 0.8125!
        '
        'TextBox8
        '
        Me.TextBox8.DataField = "MontoTotalRecibos"
        Me.TextBox8.Height = 0.1875!
        Me.TextBox8.Left = 3.6875!
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.OutputFormat = resources.GetString("TextBox8.OutputFormat")
        Me.TextBox8.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.TextBox8.Text = Nothing
        Me.TextBox8.Top = 1.5625!
        Me.TextBox8.Width = 0.8125!
        '
        'lblTotalIngresos
        '
        Me.lblTotalIngresos.DataField = "MontoTotalIngreso"
        Me.lblTotalIngresos.Height = 0.1875!
        Me.lblTotalIngresos.Left = 5.0625!
        Me.lblTotalIngresos.Name = "lblTotalIngresos"
        Me.lblTotalIngresos.OutputFormat = resources.GetString("lblTotalIngresos.OutputFormat")
        Me.lblTotalIngresos.Style = "ddo-char-set: 0; font-weight: bold; font-style: italic; font-size: 9pt"
        Me.lblTotalIngresos.Text = Nothing
        Me.lblTotalIngresos.Top = 1.8125!
        Me.lblTotalIngresos.Width = 0.8125!
        '
        'lblTotalEgresos
        '
        Me.lblTotalEgresos.DataField = "MontoTotalEgreso"
        Me.lblTotalEgresos.Height = 0.1875!
        Me.lblTotalEgresos.Left = 5.0625!
        Me.lblTotalEgresos.Name = "lblTotalEgresos"
        Me.lblTotalEgresos.OutputFormat = resources.GetString("lblTotalEgresos.OutputFormat")
        Me.lblTotalEgresos.Style = "ddo-char-set: 0; font-weight: bold; font-style: italic; font-size: 9pt"
        Me.lblTotalEgresos.Text = Nothing
        Me.lblTotalEgresos.Top = 2.4375!
        Me.lblTotalEgresos.Width = 0.8125!
        '
        'lblDifIngEgr
        '
        Me.lblDifIngEgr.Height = 0.1875!
        Me.lblDifIngEgr.Left = 5.0625!
        Me.lblDifIngEgr.Name = "lblDifIngEgr"
        Me.lblDifIngEgr.OutputFormat = resources.GetString("lblDifIngEgr.OutputFormat")
        Me.lblDifIngEgr.Style = "ddo-char-set: 0; font-weight: bold; font-style: italic; font-size: 9pt"
        Me.lblDifIngEgr.Text = Nothing
        Me.lblDifIngEgr.Top = 2.6875!
        Me.lblDifIngEgr.Width = 0.8125!
        '
        'TextBox4
        '
        Me.TextBox4.DataField = "MontoGastos"
        Me.TextBox4.Height = 0.1875!
        Me.TextBox4.Left = 3.6875!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat")
        Me.TextBox4.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.TextBox4.Text = Nothing
        Me.TextBox4.Top = 2.1875!
        Me.TextBox4.Width = 0.8125!
        '
        'lblDineroCaja
        '
        Me.lblDineroCaja.DataField = "MontoTotalDineroCaja"
        Me.lblDineroCaja.Height = 0.1875!
        Me.lblDineroCaja.Left = 3.6875!
        Me.lblDineroCaja.Name = "lblDineroCaja"
        Me.lblDineroCaja.OutputFormat = resources.GetString("lblDineroCaja.OutputFormat")
        Me.lblDineroCaja.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblDineroCaja.Text = Nothing
        Me.lblDineroCaja.Top = 2.9375!
        Me.lblDineroCaja.Width = 0.8125!
        '
        'lblDiferenciaEfectivo
        '
        Me.lblDiferenciaEfectivo.Height = 0.1875!
        Me.lblDiferenciaEfectivo.Left = 3.6875!
        Me.lblDiferenciaEfectivo.Name = "lblDiferenciaEfectivo"
        Me.lblDiferenciaEfectivo.OutputFormat = resources.GetString("lblDiferenciaEfectivo.OutputFormat")
        Me.lblDiferenciaEfectivo.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblDiferenciaEfectivo.Text = Nothing
        Me.lblDiferenciaEfectivo.Top = 3.1875!
        Me.lblDiferenciaEfectivo.Width = 0.8125!
        '
        'lblSaldoAnterior
        '
        Me.lblSaldoAnterior.DataField = "SaldoInicial"
        Me.lblSaldoAnterior.Height = 0.1875!
        Me.lblSaldoAnterior.Left = 3.6875!
        Me.lblSaldoAnterior.Name = "lblSaldoAnterior"
        Me.lblSaldoAnterior.OutputFormat = resources.GetString("lblSaldoAnterior.OutputFormat")
        Me.lblSaldoAnterior.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblSaldoAnterior.Text = Nothing
        Me.lblSaldoAnterior.Top = 3.5!
        Me.lblSaldoAnterior.Width = 0.8125!
        '
        'TextBox6
        '
        Me.TextBox6.DataField = "MontoTotalIngreso"
        Me.TextBox6.Height = 0.1875!
        Me.TextBox6.Left = 3.6875!
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat")
        Me.TextBox6.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.TextBox6.Text = Nothing
        Me.TextBox6.Top = 4.0625!
        Me.TextBox6.Width = 0.8125!
        '
        'lblTotalCaja
        '
        Me.lblTotalCaja.Height = 0.1875!
        Me.lblTotalCaja.Left = 5.0625!
        Me.lblTotalCaja.Name = "lblTotalCaja"
        Me.lblTotalCaja.OutputFormat = resources.GetString("lblTotalCaja.OutputFormat")
        Me.lblTotalCaja.Style = "ddo-char-set: 0; font-weight: bold; font-style: italic; font-size: 9pt"
        Me.lblTotalCaja.Text = Nothing
        Me.lblTotalCaja.Top = 4.4375!
        Me.lblTotalCaja.Width = 0.8125!
        '
        'lblGastos
        '
        Me.lblGastos.DataField = "MontoTotalEgreso"
        Me.lblGastos.Height = 0.1875!
        Me.lblGastos.Left = 3.6875!
        Me.lblGastos.Name = "lblGastos"
        Me.lblGastos.OutputFormat = resources.GetString("lblGastos.OutputFormat")
        Me.lblGastos.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblGastos.Text = Nothing
        Me.lblGastos.Top = 4.25!
        Me.lblGastos.Width = 0.8125!
        '
        'lblDepositos
        '
        Me.lblDepositos.DataField = "MontoDepositos"
        Me.lblDepositos.Height = 0.1875!
        Me.lblDepositos.Left = 3.6875!
        Me.lblDepositos.Name = "lblDepositos"
        Me.lblDepositos.OutputFormat = resources.GetString("lblDepositos.OutputFormat")
        Me.lblDepositos.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblDepositos.Text = Nothing
        Me.lblDepositos.Top = 4.625!
        Me.lblDepositos.Width = 0.8125!
        '
        'TextBox9
        '
        Me.TextBox9.DataField = "SaldoFinal"
        Me.TextBox9.Height = 0.1875!
        Me.TextBox9.Left = 4.6875!
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat")
        Me.TextBox9.Style = "ddo-char-set: 0; text-align: right; font-weight: bold; font-size: 9.75pt"
        Me.TextBox9.Text = Nothing
        Me.TextBox9.Top = 4.9375!
        Me.TextBox9.Width = 1.1875!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 2.0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label25.Text = "OTROS INGRESOS"
        Me.Label25.Top = 3.6875!
        Me.Label25.Width = 1.125!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 1.01!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "text-align: left; font-weight: bold; font-size: 8.25pt"
        Me.Label26.Text = "SOBRANTE(+)/FALTANTE(-) EN CAJA"
        Me.Label26.Top = 3.875!
        Me.Label26.Width = 2.115!
        '
        'lblSobranteCaja
        '
        Me.lblSobranteCaja.DataField = "SobranteCaja"
        Me.lblSobranteCaja.Height = 0.1875!
        Me.lblSobranteCaja.Left = 3.6875!
        Me.lblSobranteCaja.Name = "lblSobranteCaja"
        Me.lblSobranteCaja.OutputFormat = resources.GetString("lblSobranteCaja.OutputFormat")
        Me.lblSobranteCaja.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblSobranteCaja.Text = Nothing
        Me.lblSobranteCaja.Top = 3.875!
        Me.lblSobranteCaja.Width = 0.8125!
        '
        'lblOtrosIngresos
        '
        Me.lblOtrosIngresos.DataField = "OtrosIngresos"
        Me.lblOtrosIngresos.Height = 0.1875!
        Me.lblOtrosIngresos.Left = 3.6875!
        Me.lblOtrosIngresos.Name = "lblOtrosIngresos"
        Me.lblOtrosIngresos.OutputFormat = resources.GetString("lblOtrosIngresos.OutputFormat")
        Me.lblOtrosIngresos.Style = "ddo-char-set: 0; font-size: 9pt"
        Me.lblOtrosIngresos.Text = Nothing
        Me.lblOtrosIngresos.Top = 3.6875!
        Me.lblOtrosIngresos.Width = 0.8125!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 4.52!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label30.Text = "Fecha Arqueo:"
        Me.Label30.Top = 0.115!
        Me.Label30.Width = 0.8320003!
        '
        'Label35
        '
        Me.Label35.DataField = "FechaIngreso"
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 5.395!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label35.Text = ""
        Me.Label35.Top = 0.115!
        Me.Label35.Width = 0.915!
        '
        'Label36
        '
        Me.Label36.DataField = "Usuario"
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 2.611!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label36.Text = ""
        Me.Label36.Top = 0.115!
        Me.Label36.Width = 1.83!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 2.092!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label38.Text = "Usuario:"
        Me.Label38.Top = 0.115!
        Me.Label38.Width = 0.4970002!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label4, Me.Line4, Me.Label9, Me.Line5, Me.Line6, Me.Label31})
        Me.GroupFooter1.Height = 0.2708333!
        Me.GroupFooter1.KeepTogether = True
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label4.Text = "Entregu� Conforme:"
        Me.Label4.Top = 0!
        Me.Label4.Width = 1.1875!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 1.1875!
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 0.1875!
        Me.Line4.Width = 1.1875!
        Me.Line4.X1 = 1.1875!
        Me.Line4.X2 = 2.375!
        Me.Line4.Y1 = 0.1875!
        Me.Line4.Y2 = 0.1875!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 2.375!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label9.Text = "Recib� Conforme:"
        Me.Label9.Top = 0!
        Me.Label9.Width = 1.0!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 3.375!
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 0.1875!
        Me.Line5.Width = 1.25!
        Me.Line5.X1 = 3.375!
        Me.Line5.X2 = 4.625!
        Me.Line5.Y1 = 0.1875!
        Me.Line5.Y2 = 0.1875!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 5.1875!
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 0.1875!
        Me.Line6.Width = 1.25!
        Me.Line6.X1 = 5.1875!
        Me.Line6.X2 = 6.4375!
        Me.Line6.Y1 = 0.1875!
        Me.Line6.Y2 = 0.1875!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 4.625!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label31.Text = "Rebisado:"
        Me.Label31.Top = 0!
        Me.Label31.Width = 0.625!
        '
        'ReportHeader1
        '
        Me.ReportHeader1.Height = 0!
        Me.ReportHeader1.Name = "ReportHeader1"
        '
        'ReportFooter1
        '
        Me.ReportFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox7, Me.TextBox3, Me.Label27, Me.TextBox1, Me.Line3})
        Me.ReportFooter1.Height = 1.270833!
        Me.ReportFooter1.Name = "ReportFooter1"
        '
        'TextBox7
        '
        Me.TextBox7.Height = 0.2!
        Me.TextBox7.Left = 0!
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Style = "text-align: center; font-style: italic; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox7.Text = "TextBox7"
        Me.TextBox7.Top = 0.9375!
        Me.TextBox7.Width = 2.1875!
        '
        'TextBox3
        '
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 3.0625!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Style = "text-align: center; font-style: italic; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox3.Text = "TextBox8"
        Me.TextBox3.Top = 0.9375!
        Me.TextBox3.Width = 1.3125!
        '
        'Label27
        '
        Me.Label27.Height = 0.2!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 5.4375!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-weight: bold; font-size: 8.25pt; font-family: Times New Roman"
        Me.Label27.Text = "P�gina"
        Me.Label27.Top = 0.9375!
        Me.Label27.Width = 0.6125001!
        '
        'TextBox1
        '
        Me.TextBox1.Height = 0.2!
        Me.TextBox1.Left = 6.0625!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Style = "text-align: right; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox1.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox1.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox1.Text = "TextBox26"
        Me.TextBox1.Top = 0.9375!
        Me.TextBox1.Width = 0.375!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.875!
        Me.Line3.Width = 6.4375!
        Me.Line3.X1 = 0!
        Me.Line3.X2 = 6.4375!
        Me.Line3.Y1 = 0.875!
        Me.Line3.Y2 = 0.875!
        '
        'actRptImprimeCierreCuadreCaja
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Left = 0.8!
        Me.PageSettings.Margins.Right = 0.15!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 6.53125!
        Me.Sections.Add(Me.ReportHeader1)
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.Sections.Add(Me.ReportFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalIngresos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalEgresos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDifIngEgr, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDineroCaja, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDiferenciaEfectivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSaldoAnterior, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalCaja, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblGastos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDepositos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSobranteCaja, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOtrosIngresos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents Label15 As DataDynamics.ActiveReports.Label
    Private WithEvents Label16 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox25 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox26 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label20 As DataDynamics.ActiveReports.Label
    Private WithEvents Line1 As DataDynamics.ActiveReports.Line
    Private WithEvents Line As DataDynamics.ActiveReports.Line
    Private WithEvents Label As DataDynamics.ActiveReports.Label
    Friend WithEvents ReportHeader1 As DataDynamics.ActiveReports.ReportHeader
    Friend WithEvents ReportFooter1 As DataDynamics.ActiveReports.ReportFooter
    Private WithEvents Label2 As DataDynamics.ActiveReports.Label
    Private WithEvents Label1 As DataDynamics.ActiveReports.Label
    Private WithEvents Label7 As DataDynamics.ActiveReports.Label
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Private WithEvents Label5 As DataDynamics.ActiveReports.Label
    Private WithEvents Label6 As DataDynamics.ActiveReports.Label
    Private WithEvents Label8 As DataDynamics.ActiveReports.Label
    Private WithEvents Label10 As DataDynamics.ActiveReports.Label
    Private WithEvents Label11 As DataDynamics.ActiveReports.Label
    Private WithEvents Label12 As DataDynamics.ActiveReports.Label
    Friend WithEvents Shape1 As DataDynamics.ActiveReports.Shape
    Private WithEvents Label13 As DataDynamics.ActiveReports.Label
    Private WithEvents Label14 As DataDynamics.ActiveReports.Label
    Friend WithEvents Line2 As DataDynamics.ActiveReports.Line
    Private WithEvents Label17 As DataDynamics.ActiveReports.Label
    Private WithEvents Label18 As DataDynamics.ActiveReports.Label
    Private WithEvents Label19 As DataDynamics.ActiveReports.Label
    Private WithEvents Label21 As DataDynamics.ActiveReports.Label
    Private WithEvents Label22 As DataDynamics.ActiveReports.Label
    Private WithEvents Label24 As DataDynamics.ActiveReports.Label
    Private WithEvents Label28 As DataDynamics.ActiveReports.Label
    Private WithEvents Label29 As DataDynamics.ActiveReports.Label
    Private WithEvents Label32 As DataDynamics.ActiveReports.Label
    Private WithEvents Label33 As DataDynamics.ActiveReports.Label
    Private WithEvents Label34 As DataDynamics.ActiveReports.Label
    Private WithEvents Label37 As DataDynamics.ActiveReports.Label
    Private WithEvents Label40 As DataDynamics.ActiveReports.Label
    Private WithEvents Label43 As DataDynamics.ActiveReports.Label
    Private WithEvents Label45 As DataDynamics.ActiveReports.Label
    Private WithEvents Label47 As DataDynamics.ActiveReports.Label
    Friend WithEvents Shape2 As DataDynamics.ActiveReports.Shape
    Friend WithEvents Label50 As DataDynamics.ActiveReports.Label
    Private WithEvents Label51 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox7 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox3 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label27 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents Shape3 As DataDynamics.ActiveReports.Shape
    Private WithEvents Label23 As DataDynamics.ActiveReports.Label
    Private WithEvents Label4 As DataDynamics.ActiveReports.Label
    Private WithEvents Line4 As DataDynamics.ActiveReports.Line
    Private WithEvents Label9 As DataDynamics.ActiveReports.Label
    Private WithEvents Line5 As DataDynamics.ActiveReports.Line
    Private WithEvents Line6 As DataDynamics.ActiveReports.Line
    Private WithEvents Label31 As DataDynamics.ActiveReports.Label
    Friend WithEvents TextBox2 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TextBox8 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalIngresos As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalEgresos As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDifIngEgr As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TextBox4 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDineroCaja As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDiferenciaEfectivo As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblSaldoAnterior As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TextBox6 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblTotalCaja As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblGastos As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblDepositos As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TextBox9 As DataDynamics.ActiveReports.TextBox
    Private WithEvents lblAgencia As DataDynamics.ActiveReports.Label
    Private WithEvents Label25 As DataDynamics.ActiveReports.Label
    Private WithEvents Label26 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblSobranteCaja As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblOtrosIngresos As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label30 As DataDynamics.ActiveReports.Label
    Private WithEvents Label35 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label36 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label38 As DataDynamics.ActiveReports.Label
End Class
