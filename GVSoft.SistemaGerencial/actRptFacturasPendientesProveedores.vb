Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document 

Public Class actRptFacturasPendientesProveedores 
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actRptFacturasPendientesProveedores_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.0F
        Label.Text = strNombEmpresa
        TextBox7.Text = Format(Now, "Long Date")
        TextBox8.Text = Format(Now, "Long Time")

        Select Case intrptCtasPagar
            Case 7
                Label1.Text = "Facturas Pendientes"
            Case 8
                Label1.Text = "Facturas Pendientes de mas de 4 meses"
            Case 9
                Label1.Text = "Facturas Pendientes de mas de un a�o"
        End Select

        Me.Document.Printer.PrinterName = ""
    End Sub

    Private Sub actRptFacturasPendientesProveedores_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select
    End Sub
End Class
