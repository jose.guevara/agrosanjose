Imports System.Data.SqlClient
Imports System.Text

Public Class frmComisiones
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown3 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown4 As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown5 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown6 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown7 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown8 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown9 As System.Windows.Forms.NumericUpDown
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox9 As System.Windows.Forms.ListBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ListBox10 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox11 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox12 As System.Windows.Forms.ListBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents cntmnuComisRg As System.Windows.Forms.ContextMenu
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmComisiones))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.ListBox12 = New System.Windows.Forms.ListBox
        Me.ListBox11 = New System.Windows.Forms.ListBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.ListBox10 = New System.Windows.Forms.ListBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.ListBox9 = New System.Windows.Forms.ListBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.ListBox8 = New System.Windows.Forms.ListBox
        Me.ListBox7 = New System.Windows.Forms.ListBox
        Me.ListBox6 = New System.Windows.Forms.ListBox
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.NumericUpDown9 = New System.Windows.Forms.NumericUpDown
        Me.Label8 = New System.Windows.Forms.Label
        Me.NumericUpDown8 = New System.Windows.Forms.NumericUpDown
        Me.Label7 = New System.Windows.Forms.Label
        Me.NumericUpDown7 = New System.Windows.Forms.NumericUpDown
        Me.Label6 = New System.Windows.Forms.Label
        Me.NumericUpDown6 = New System.Windows.Forms.NumericUpDown
        Me.Label5 = New System.Windows.Forms.Label
        Me.NumericUpDown5 = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.NumericUpDown4 = New System.Windows.Forms.NumericUpDown
        Me.Label3 = New System.Windows.Forms.Label
        Me.NumericUpDown3 = New System.Windows.Forms.NumericUpDown
        Me.Label2 = New System.Windows.Forms.Label
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuComisRg = New System.Windows.Forms.ContextMenu
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 409)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(664, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 11
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 565
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(31, 31)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.ListBox12)
        Me.GroupBox1.Controls.Add(Me.ListBox11)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox10)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.ListBox9)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(664, 344)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos para Estratificaci�n"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Location = New System.Drawing.Point(312, 8)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(20, 21)
        Me.ComboBox3.TabIndex = 46
        Me.ComboBox3.TabStop = False
        Me.ComboBox3.Visible = False
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Location = New System.Drawing.Point(72, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(240, 21)
        Me.ComboBox2.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(8, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(55, 16)
        Me.Label17.TabIndex = 45
        Me.Label17.Text = "Vendedor"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"activo", "inactivo"})
        Me.ComboBox1.Location = New System.Drawing.Point(544, 288)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(112, 21)
        Me.ComboBox1.TabIndex = 19
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(496, 288)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(41, 16)
        Me.Label16.TabIndex = 43
        Me.Label16.Text = "Estado"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(248, 280)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 16)
        Me.Label15.TabIndex = 42
        Me.Label15.Text = "<F5> AYUDA"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(320, 32)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(8, 20)
        Me.TextBox4.TabIndex = 38
        Me.TextBox4.TabStop = False
        Me.TextBox4.Text = ""
        Me.TextBox4.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(400, 24)
        Me.TextBox3.MaxLength = 40
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(256, 20)
        Me.TextBox3.TabIndex = 1
        Me.TextBox3.Text = ""
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(328, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 16)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "Descripci�n"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(264, 216)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(152, 16)
        Me.Label13.TabIndex = 35
        Me.Label13.Text = "Productos a Excluir"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox12
        '
        Me.ListBox12.Location = New System.Drawing.Point(488, 232)
        Me.ListBox12.Name = "ListBox12"
        Me.ListBox12.Size = New System.Drawing.Size(40, 17)
        Me.ListBox12.TabIndex = 34
        Me.ListBox12.TabStop = False
        Me.ListBox12.Visible = False
        '
        'ListBox11
        '
        Me.ListBox11.Location = New System.Drawing.Point(344, 232)
        Me.ListBox11.Name = "ListBox11"
        Me.ListBox11.Size = New System.Drawing.Size(144, 108)
        Me.ListBox11.TabIndex = 18
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(264, 232)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(72, 20)
        Me.TextBox2.TabIndex = 17
        Me.TextBox2.Text = ""
        '
        'ListBox10
        '
        Me.ListBox10.Location = New System.Drawing.Point(232, 232)
        Me.ListBox10.Name = "ListBox10"
        Me.ListBox10.Size = New System.Drawing.Size(32, 17)
        Me.ListBox10.TabIndex = 31
        Me.ListBox10.TabStop = False
        Me.ListBox10.Visible = False
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(8, 216)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(144, 16)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "Productos a Incluir"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox9
        '
        Me.ListBox9.Location = New System.Drawing.Point(88, 232)
        Me.ListBox9.Name = "ListBox9"
        Me.ListBox9.Size = New System.Drawing.Size(144, 108)
        Me.ListBox9.TabIndex = 16
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(8, 232)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(72, 20)
        Me.TextBox1.TabIndex = 15
        Me.TextBox1.Text = ""
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(488, 128)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(40, 17)
        Me.ListBox8.TabIndex = 27
        Me.ListBox8.TabStop = False
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(488, 104)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(40, 17)
        Me.ListBox7.TabIndex = 26
        Me.ListBox7.TabStop = False
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.Location = New System.Drawing.Point(392, 104)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(96, 108)
        Me.ListBox6.TabIndex = 14
        '
        'ListBox5
        '
        Me.ListBox5.Location = New System.Drawing.Point(264, 104)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(120, 108)
        Me.ListBox5.TabIndex = 13
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(272, 88)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(216, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "SubClases"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(232, 128)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(32, 17)
        Me.ListBox4.TabIndex = 22
        Me.ListBox4.TabStop = False
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(232, 104)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(32, 17)
        Me.ListBox3.TabIndex = 21
        Me.ListBox3.TabStop = False
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.Location = New System.Drawing.Point(136, 104)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(96, 108)
        Me.ListBox2.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(16, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(216, 16)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Clases"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(8, 104)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(120, 108)
        Me.ListBox1.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(648, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(14, 16)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "%"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown9
        '
        Me.NumericUpDown9.DecimalPlaces = 2
        Me.NumericUpDown9.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.NumericUpDown9.Location = New System.Drawing.Point(600, 56)
        Me.NumericUpDown9.Name = "NumericUpDown9"
        Me.NumericUpDown9.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown9.TabIndex = 10
        Me.NumericUpDown9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(568, 56)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(26, 16)
        Me.Label8.TabIndex = 15
        Me.Label8.Text = "d�as"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown8
        '
        Me.NumericUpDown8.Location = New System.Drawing.Point(512, 56)
        Me.NumericUpDown8.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown8.Name = "NumericUpDown8"
        Me.NumericUpDown8.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown8.TabIndex = 9
        Me.NumericUpDown8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(496, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(11, 16)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "a"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown7
        '
        Me.NumericUpDown7.Location = New System.Drawing.Point(448, 56)
        Me.NumericUpDown7.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown7.Name = "NumericUpDown7"
        Me.NumericUpDown7.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown7.TabIndex = 8
        Me.NumericUpDown7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(424, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 16)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "%"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown6
        '
        Me.NumericUpDown6.DecimalPlaces = 2
        Me.NumericUpDown6.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.NumericUpDown6.Location = New System.Drawing.Point(376, 56)
        Me.NumericUpDown6.Name = "NumericUpDown6"
        Me.NumericUpDown6.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown6.TabIndex = 7
        Me.NumericUpDown6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(344, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 16)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "d�as"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown5
        '
        Me.NumericUpDown5.Location = New System.Drawing.Point(288, 56)
        Me.NumericUpDown5.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown5.Name = "NumericUpDown5"
        Me.NumericUpDown5.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown5.TabIndex = 6
        Me.NumericUpDown5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(272, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(11, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "a"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown4
        '
        Me.NumericUpDown4.Location = New System.Drawing.Point(224, 56)
        Me.NumericUpDown4.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown4.Name = "NumericUpDown4"
        Me.NumericUpDown4.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown4.TabIndex = 5
        Me.NumericUpDown4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(200, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(14, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "%"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown3
        '
        Me.NumericUpDown3.DecimalPlaces = 2
        Me.NumericUpDown3.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.NumericUpDown3.Location = New System.Drawing.Point(152, 56)
        Me.NumericUpDown3.Name = "NumericUpDown3"
        Me.NumericUpDown3.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown3.TabIndex = 4
        Me.NumericUpDown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(120, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(26, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "d�as"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Location = New System.Drawing.Point(64, 56)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown2.TabIndex = 3
        Me.NumericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(11, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "a"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(8, 56)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(40, 20)
        Me.NumericUpDown1.TabIndex = 2
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(664, 56)
        Me.ToolBar1.TabIndex = 17
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuComisRg
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.ToolBarButton4.ToolTipText = "Registros"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem9})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'frmComisiones
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(664, 433)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusBar1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmComisiones"
        Me.Text = "frmComisiones"
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.NumericUpDown9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim intProducto As Integer
    Public nudCollection As New Collection
    Public lstCollection As New Collection
    Dim strCambios(2) As String

    Private Sub frmComisiones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        intProducto = 0
        nudCollection.Add(NumericUpDown1)
        nudCollection.Add(NumericUpDown2)
        nudCollection.Add(NumericUpDown3)
        nudCollection.Add(NumericUpDown4)
        nudCollection.Add(NumericUpDown5)
        nudCollection.Add(NumericUpDown6)
        nudCollection.Add(NumericUpDown7)
        nudCollection.Add(NumericUpDown8)
        nudCollection.Add(NumericUpDown9)
        lstCollection.Add(ListBox2)
        lstCollection.Add(ListBox4)
        lstCollection.Add(ListBox6)
        lstCollection.Add(ListBox8)
        lstCollection.Add(ListBox9)
        lstCollection.Add(ListBox10)
        lstCollection.Add(ListBox11)
        lstCollection.Add(ListBox12)
        CreateMyContextMenu()
        Limpiar()
        Iniciar()
        intModulo = 35

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = TextBox4.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = TextBox4.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub frmComisiones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        ElseIf e.KeyCode = Keys.F5 Then
            If Label15.Visible = True Then
                intListadoAyuda = 3
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
            Case 3
                MoverRegistro(sender.text)
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"

        cntmnuComisRg.MenuItems.Add(cntmenuItem1)
        cntmnuComisRg.MenuItems.Add(cntmenuItem2)
        cntmnuComisRg.MenuItems.Add(cntmenuItem3)
        cntmnuComisRg.MenuItems.Add(cntmenuItem4)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Nombre From prm_Vendedores Where Estado <> 2"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox2.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox3.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Clases Where Estado <> 2"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox1.Items.Add(dtrAgro2K.GetValue(1))
            ListBox3.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_SubClases Where Estado <> 2"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox5.Items.Add(dtrAgro2K.GetValue(1))
            ListBox7.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = "0"
        ComboBox2.SelectedIndex = -1
        ComboBox3.SelectedIndex = -1
        For intIncr = 1 To 9
            nudCollection(intIncr).value = 0
        Next
        For intIncr = 1 To 8
            lstCollection(intIncr).items.clear()
        Next
        ComboBox1.SelectedIndex = 0
        Label15.Visible = False

    End Sub

    Sub Guardar()

        If Trim(TextBox3.Text) = "" Then
            TextBox3.Focus()
            MsgBox("No ha ingresado la descripci�n del estrato.  Verifique.", MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngComisiones", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter
        Dim prmTmp18 As New SqlParameter
        Dim strClases As String, strSubClases As String
        Dim strProdIncluir As String, strProdExcluir As String

        strClases = ""
        strSubClases = ""
        strProdIncluir = ""
        strProdExcluir = ""
        If ListBox4.Items.Count > 0 Then
            strClases = "("
            If ListBox4.Items.Count = 1 Then
                ListBox4.SelectedIndex = 0
                strClases = strClases + "" & ListBox4.SelectedItem & ") "
            ElseIf ListBox4.Items.Count > 1 Then
                For intIncr = 0 To ListBox4.Items.Count - 2
                    ListBox4.SelectedIndex = intIncr
                    strClases = strClases + "" & ListBox4.SelectedItem & ", "
                Next
                ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                strClases = strClases + "" & ListBox4.SelectedItem & ") "
            End If
        End If
        If ListBox8.Items.Count > 0 Then
            strSubClases = "("
            If ListBox8.Items.Count = 1 Then
                ListBox8.SelectedIndex = 0
                strSubClases = strSubClases + "" & ListBox8.SelectedItem & ") "
            ElseIf ListBox8.Items.Count > 1 Then
                For intIncr = 0 To ListBox8.Items.Count - 2
                    ListBox8.SelectedIndex = intIncr
                    strSubClases = strSubClases + "" & ListBox8.SelectedItem & ", "
                Next
                ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                strSubClases = strSubClases + "" & ListBox8.SelectedItem & ") "
            End If
        End If
        If ListBox10.Items.Count > 0 Then
            strProdIncluir = "("
            If ListBox10.Items.Count = 1 Then
                ListBox10.SelectedIndex = 0
                strProdIncluir = strProdIncluir + "" & ListBox10.SelectedItem & ") "
            ElseIf ListBox10.Items.Count > 1 Then
                For intIncr = 0 To ListBox10.Items.Count - 2
                    ListBox10.SelectedIndex = intIncr
                    strProdIncluir = strProdIncluir + "" & ListBox10.SelectedItem & ", "
                Next
                ListBox10.SelectedIndex = ListBox10.Items.Count - 1
                strProdIncluir = strProdIncluir + "" & ListBox10.SelectedItem & ") "
            End If
        End If
        If ListBox12.Items.Count > 0 Then
            strProdExcluir = "("
            If ListBox12.Items.Count = 1 Then
                ListBox12.SelectedIndex = 0
                strProdExcluir = strProdExcluir + "" & ListBox12.SelectedItem & ") "
            ElseIf ListBox12.Items.Count > 1 Then
                For intIncr = 0 To ListBox12.Items.Count - 2
                    ListBox12.SelectedIndex = intIncr
                    strProdExcluir = strProdExcluir + "" & ListBox12.SelectedItem & ", "
                Next
                ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                strProdExcluir = strProdExcluir + "" & ListBox12.SelectedItem & ") "
            End If
        End If

        lngRegistro = 0
        ComboBox3.SelectedIndex = ComboBox2.SelectedIndex
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@intVendedor"
            .SqlDbType = SqlDbType.TinyInt
            .Value = CInt(ComboBox3.SelectedItem)
        End With
        With prmTmp03
            .ParameterName = "@strDescripcion"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox3.Text
        End With
        With prmTmp04
            .ParameterName = "@intDia01"
            .SqlDbType = SqlDbType.SmallInt
            .Value = NumericUpDown1.Value
        End With
        With prmTmp05
            .ParameterName = "@intDia02"
            .SqlDbType = SqlDbType.SmallInt
            .Value = NumericUpDown2.Value
        End With
        With prmTmp06
            .ParameterName = "@intPor01"
            .SqlDbType = SqlDbType.Decimal
            .Value = NumericUpDown3.Value
        End With
        With prmTmp07
            .ParameterName = "@intDia03"
            .SqlDbType = SqlDbType.SmallInt
            .Value = NumericUpDown4.Value
        End With
        With prmTmp08
            .ParameterName = "@intDia04"
            .SqlDbType = SqlDbType.SmallInt
            .Value = NumericUpDown5.Value
        End With
        With prmTmp09
            .ParameterName = "@intPor02"
            .SqlDbType = SqlDbType.Decimal
            .Value = NumericUpDown6.Value
        End With
        With prmTmp10
            .ParameterName = "@intDia05"
            .SqlDbType = SqlDbType.SmallInt
            .Value = NumericUpDown7.Value
        End With
        With prmTmp11
            .ParameterName = "@intDia06"
            .SqlDbType = SqlDbType.SmallInt
            .Value = NumericUpDown8.Value
        End With
        With prmTmp12
            .ParameterName = "@intPor03"
            .SqlDbType = SqlDbType.Decimal
            .Value = NumericUpDown9.Value
        End With
        With prmTmp13
            .ParameterName = "@strClases"
            .SqlDbType = SqlDbType.VarChar
            .Value = strClases
        End With
        With prmTmp14
            .ParameterName = "@strSubClases"
            .SqlDbType = SqlDbType.VarChar
            .Value = strSubClases
        End With
        With prmTmp15
            .ParameterName = "@strInclProd"
            .SqlDbType = SqlDbType.VarChar
            .Value = strProdIncluir
        End With
        With prmTmp16
            .ParameterName = "@strExclProd"
            .SqlDbType = SqlDbType.VarChar
            .Value = strProdExcluir
        End With
        With prmTmp17
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox1.SelectedIndex
        End With
        With prmTmp18
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .Parameters.Add(prmTmp18)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        cmdTmp.Parameters(17).Value = 1

        Try
            cmdTmp.ExecuteNonQuery()
            If intResp = 6 Then
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox4.Text, "Estado", strCambios(1), ComboBox1.SelectedItem)
                End If
            ElseIf intResp = 0 Then
                Ing_Bitacora(intModulo, lngRegistro, "Descripci�n", "", TextBox1.Text)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default


    End Sub

    Sub MoverRegistro(ByVal StrDato As String)
        strQuery = ""
        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(registro) from prm_Comisiones"
            Case "Anterior"
                strQuery = "Select Max(registro) from prm_Comisiones Where registro < " & CInt(TextBox4.Text) & ""
            Case "Siguiente"
                strQuery = "Select Min(registro) from prm_Comisiones Where registro > " & CInt(TextBox4.Text) & ""
            Case "Ultimo"
                strQuery = "Select Max(registro) from prm_Comisiones"
            Case "Igual"
                strQuery = "Select c.registro from prm_Comisiones c, prm_Vendedores v where c.vendregistro = v.registro "
                strQuery = strQuery + "and v.codigo = '" & strUbicar & "' and c.descripcion = '" & strUbicar02 & "'"
        End Select
        Limpiar()
        UbicarRegistro(StrDato)

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox9.DoubleClick, ListBox11.DoubleClick

        Select Case sender.name.ToString
            Case "ListBox1" : ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
            Case "ListBox2"
                If ListBox2.SelectedIndex >= 0 Then
                    ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                End If
            Case "ListBox5" : ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem)
            Case "ListBox6"
                If ListBox6.SelectedIndex >= 0 Then
                    ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                End If
            Case "ListBox9"
                If ListBox9.SelectedIndex >= 0 Then
                    ListBox10.Items.RemoveAt(ListBox9.SelectedIndex) : ListBox9.Items.RemoveAt(ListBox9.SelectedIndex)
                End If
            Case "ListBox11"
                If ListBox11.SelectedIndex >= 0 Then
                    ListBox12.Items.RemoveAt(ListBox11.SelectedIndex) : ListBox11.Items.RemoveAt(ListBox11.SelectedIndex)
                End If
        End Select

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim intCodigo As Integer = 0
        Dim blnSeguir As Boolean
        Dim strCom_Clases As String = String.Empty
        Dim strCom_SubClases As String = String.Empty
        Dim strCom_ProdIncluir As String = String.Empty
        Dim strCom_ProdExcluir As String = String.Empty

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        intCodigo = 0
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                intCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If intCodigo = 0 Then
            MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From prm_Comisiones Where Registro = " & intCodigo & ""
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox4.Text = dtrAgro2K.GetValue(0)
            intCodigo = dtrAgro2K.GetValue(1)
            TextBox3.Text = dtrAgro2K.GetValue(2)
            NumericUpDown1.Value = dtrAgro2K.GetValue(3)
            NumericUpDown2.Value = dtrAgro2K.GetValue(4)
            NumericUpDown3.Value = dtrAgro2K.GetValue(5)
            NumericUpDown4.Value = dtrAgro2K.GetValue(6)
            NumericUpDown5.Value = dtrAgro2K.GetValue(7)
            NumericUpDown6.Value = dtrAgro2K.GetValue(8)
            NumericUpDown7.Value = dtrAgro2K.GetValue(9)
            NumericUpDown8.Value = dtrAgro2K.GetValue(10)
            NumericUpDown9.Value = dtrAgro2K.GetValue(11)
            strCom_Clases = dtrAgro2K.GetValue(12)
            strCom_SubClases = dtrAgro2K.GetValue(13)
            strCom_ProdIncluir = dtrAgro2K.GetValue(14)
            strCom_ProdExcluir = dtrAgro2K.GetValue(15)
            ComboBox1.SelectedIndex = dtrAgro2K.GetValue(16)
        End While
        For intIncr = 0 To ComboBox3.Items.Count - 1
            ComboBox3.SelectedIndex = intIncr
            If ComboBox3.SelectedItem = intCodigo Then
                ComboBox2.SelectedIndex = ComboBox3.SelectedIndex
                Exit For
            End If
        Next intIncr
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        strCom_Clases = Trim(strCom_Clases)
        strCom_SubClases = Trim(strCom_SubClases)
        strCom_ProdIncluir = Trim(strCom_ProdIncluir)
        strCom_ProdExcluir = Trim(strCom_ProdExcluir)
        If strCom_Clases <> "" Then
            intIncr = 0
            strCom_Clases = Microsoft.VisualBasic.Left(strCom_Clases, Len(strCom_Clases) - 1)
            strCom_Clases = Microsoft.VisualBasic.Right(strCom_Clases, Len(strCom_Clases) - 1)
            blnSeguir = True
            Do While blnSeguir = True
                intIncr = InStr(1, strCom_Clases, ",")
                If intIncr = 0 Then
                    ListBox4.Items.Add(strCom_Clases)
                    blnSeguir = False
                Else
                    ListBox4.Items.Add(Microsoft.VisualBasic.Left(strCom_Clases, intIncr - 1))
                    strCom_Clases = Microsoft.VisualBasic.Right(strCom_Clases, Len(strCom_Clases) - intIncr)
                End If
                strCom_Clases = Trim(strCom_Clases)
            Loop
            For intIncr = 0 To ListBox4.Items.Count - 1
                ListBox4.SelectedIndex = intIncr
                For intCodigo = 0 To ListBox3.Items.Count - 1
                    ListBox3.SelectedIndex = intCodigo
                    If ListBox3.SelectedItem = ListBox4.SelectedItem Then
                        ListBox1.SelectedIndex = ListBox3.SelectedIndex
                        ListBox2.Items.Add(ListBox1.SelectedItem)
                        Exit For
                    End If
                Next intCodigo
            Next intIncr
        End If
        If strCom_SubClases <> "" Then
            intIncr = 0
            strCom_SubClases = Microsoft.VisualBasic.Left(strCom_SubClases, Len(strCom_SubClases) - 1)
            strCom_SubClases = Microsoft.VisualBasic.Right(strCom_SubClases, Len(strCom_SubClases) - 1)
            blnSeguir = True
            Do While blnSeguir = True
                intIncr = InStr(1, strCom_SubClases, ",")
                If intIncr = 0 Then
                    ListBox8.Items.Add(strCom_SubClases)
                    blnSeguir = False
                Else
                    ListBox8.Items.Add(Microsoft.VisualBasic.Left(strCom_SubClases, intIncr - 1))
                    strCom_SubClases = Microsoft.VisualBasic.Right(strCom_SubClases, Len(strCom_SubClases) - intIncr)
                End If
                strCom_SubClases = Trim(strCom_SubClases)
            Loop
            For intIncr = 0 To ListBox8.Items.Count - 1
                ListBox8.SelectedIndex = intIncr
                For intCodigo = 0 To ListBox7.Items.Count - 1
                    ListBox7.SelectedIndex = intCodigo
                    If ListBox7.SelectedItem = ListBox8.SelectedItem Then
                        ListBox5.SelectedIndex = ListBox7.SelectedIndex
                        ListBox6.Items.Add(ListBox5.SelectedItem)
                        Exit For
                    End If
                Next intCodigo
            Next intIncr
        End If
        If strCom_ProdIncluir <> "" Then
            intIncr = 0
            strCom_ProdIncluir = Microsoft.VisualBasic.Left(strCom_ProdIncluir, Len(strCom_ProdIncluir) - 1)
            strCom_ProdIncluir = Microsoft.VisualBasic.Right(strCom_ProdIncluir, Len(strCom_ProdIncluir) - 1)
            blnSeguir = True
            Do While blnSeguir = True
                intIncr = InStr(1, strCom_ProdIncluir, ",")
                If intIncr = 0 Then
                    ListBox10.Items.Add(strCom_ProdIncluir)
                    blnSeguir = False
                Else
                    ListBox10.Items.Add(Microsoft.VisualBasic.Left(strCom_ProdIncluir, intIncr - 1))
                    strCom_ProdIncluir = Microsoft.VisualBasic.Right(strCom_ProdIncluir, Len(strCom_ProdIncluir) - intIncr)
                End If
                strCom_ProdIncluir = Trim(strCom_ProdIncluir)
            Loop
            For intIncr = 0 To ListBox10.Items.Count - 1
                ListBox10.SelectedIndex = intIncr
                strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P "
                strQuery = strQuery + " Where P.Registro = " & CLng(ListBox10.SelectedItem) & ""
                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If
                cnnAgro2K.Open()
                cmdAgro2K.Connection = cnnAgro2K
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ListBox9.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
                End While
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
            Next intIncr
        End If
        If strCom_ProdExcluir <> "" Then
            intIncr = 0
            strCom_ProdExcluir = Microsoft.VisualBasic.Left(strCom_ProdExcluir, Len(strCom_ProdExcluir) - 1)
            strCom_ProdExcluir = Microsoft.VisualBasic.Right(strCom_ProdExcluir, Len(strCom_ProdExcluir) - 1)
            blnSeguir = True
            Do While blnSeguir = True
                intIncr = InStr(1, strCom_ProdExcluir, ",")
                If intIncr = 0 Then
                    ListBox12.Items.Add(strCom_ProdExcluir)
                    blnSeguir = False
                Else
                    ListBox12.Items.Add(Microsoft.VisualBasic.Left(strCom_ProdExcluir, intIncr - 1))
                    strCom_ProdExcluir = Microsoft.VisualBasic.Right(strCom_ProdExcluir, Len(strCom_ProdExcluir) - intIncr)
                End If
                strCom_ProdExcluir = Trim(strCom_ProdExcluir)
            Loop
            For intIncr = 0 To ListBox12.Items.Count - 1
                ListBox12.SelectedIndex = intIncr
                strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P "
                strQuery = strQuery + " Where P.Registro = " & CLng(ListBox12.SelectedItem) & ""
                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If
                cnnAgro2K.Open()
                cmdAgro2K.Connection = cnnAgro2K
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ListBox11.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
                End While
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
            Next intIncr
        End If

    End Sub

    Function UbicarProducto() As Boolean

        lngRegistro = 0
        strQuery = ""
        If intProducto = 1 Then
            strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P Where P.Codigo = '" & TextBox1.Text & "'"
        ElseIf intProducto = 2 Then
            strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P Where P.Codigo = '" & TextBox2.Text & "'"
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            lngRegistro = dtrAgro2K.GetValue(0)
            If intProducto = 1 Then
                ListBox9.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
                ListBox10.Items.Add(lngRegistro)
            ElseIf intProducto = 2 Then
                ListBox11.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
                ListBox12.Items.Add(lngRegistro)
            End If
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If lngRegistro = 0 Then
            UbicarProducto = False
            Exit Function
        End If
        UbicarProducto = True

    End Function

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.DoubleClick, TextBox2.DoubleClick

        If sender.name = "TextBox1" Then
            intProducto = 1
        ElseIf sender.name = "TextBox2" Then
            intProducto = 2
        End If
        intListadoAyuda = 3
        Dim frmNew As New frmListadoAyuda
        Timer1.Interval = 200
        Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, TextBox3.GotFocus

        If sender.name = "TextBox1" Then
            Label15.Visible = True
            intProducto = 1
        ElseIf sender.name = "TextBox2" Then
            Label15.Visible = True
            intProducto = 2
        End If
        sender.selectall()

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If strUbicar <> "" Then
            Timer1.Enabled = False
            If intProducto = 1 Then
                TextBox1.Text = strUbicar : UbicarProducto() : ListBox9.Focus() : TextBox1.Text = ""
            ElseIf intProducto = 2 Then
                TextBox2.Text = strUbicar : UbicarProducto() : ListBox11.Focus() : TextBox2.Text = ""
            Else
                MoverRegistro("Igual")
            End If
        End If

    End Sub

    Private Sub NumericUpDown1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDown1.GotFocus, NumericUpDown2.GotFocus, NumericUpDown3.GotFocus, NumericUpDown4.GotFocus, NumericUpDown5.GotFocus, NumericUpDown6.GotFocus, NumericUpDown7.GotFocus, NumericUpDown8.GotFocus, NumericUpDown9.GotFocus, TextBox3.GotFocus

        Label15.Visible = False

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus, TextBox2.LostFocus

        intProducto = 0
        Label15.Visible = False

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, TextBox3.KeyDown, ComboBox1.KeyDown, ComboBox2.KeyDown, _
        NumericUpDown1.KeyDown, NumericUpDown2.KeyDown, NumericUpDown3.KeyDown, NumericUpDown4.KeyDown, NumericUpDown5.KeyDown, NumericUpDown6.KeyDown, NumericUpDown7.KeyDown, NumericUpDown8.KeyDown, NumericUpDown9.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "ComboBox1" : ComboBox2.Focus()
                Case "ComboBox2" : TextBox3.Focus()
                Case "TextBox3" : NumericUpDown1.Focus()
                Case "NumericUpDown1" : NumericUpDown2.Focus()
                Case "NumericUpDown2" : NumericUpDown3.Focus()
                Case "NumericUpDown3" : NumericUpDown4.Focus()
                Case "NumericUpDown4" : NumericUpDown5.Focus()
                Case "NumericUpDown5" : NumericUpDown6.Focus()
                Case "NumericUpDown6" : NumericUpDown7.Focus()
                Case "NumericUpDown7" : NumericUpDown8.Focus()
                Case "NumericUpDown8" : NumericUpDown9.Focus()
                Case "NumericUpDown9" : TextBox1.Focus()
                Case "TextBox1"
                    UbicarProducto() : ListBox9.Focus() : TextBox1.Text = "" : TextBox1.Focus()
                Case "TextBox2"
                    UbicarProducto() : ListBox11.Focus() : TextBox2.Text = "" : TextBox2.Focus()
            End Select
        End If

    End Sub

End Class
