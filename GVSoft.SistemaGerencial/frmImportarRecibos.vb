Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmImportarRecibos
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents txtMensajeError As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents txtMontoRecibo As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombreCliente As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtMontoTotalRecibosCOR As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTotalCantidaRecibosCOR As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvxDetalleRecibo As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImportar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdEliminarArqueo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtMontoTotalRecibosUSD As TextBox
    Friend WithEvents txtTotalCantidaRecibosUSD As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents NumeroFacturaPagada As DataGridViewTextBoxColumn
    Friend WithEvents Moneda As DataGridViewTextBoxColumn
    Friend WithEvents MontoPagadoFactura As DataGridViewTextBoxColumn
    Friend WithEvents FechaRecibo As DataGridViewTextBoxColumn
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMonedaRecibo As TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportarRecibos))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.txtMensajeError = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtNombreCliente = New System.Windows.Forms.TextBox()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.txtMontoRecibo = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvxDetalleRecibo = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMontoTotalRecibosCOR = New System.Windows.Forms.TextBox()
        Me.txtTotalCantidaRecibosCOR = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImportar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdEliminarArqueo = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMontoTotalRecibosUSD = New System.Windows.Forms.TextBox()
        Me.txtTotalCantidaRecibosUSD = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.NumeroFacturaPagada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Moneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoPagadoFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaRecibo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMonedaRecibo = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvxDetalleRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.txtMensajeError)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 83)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(712, 72)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas en Carga de la Agencia"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(56, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(0, 13)
        Me.Label18.TabIndex = 5
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(58, 43)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox2.TabIndex = 4
        Me.ComboBox2.Text = "ComboBox2"
        Me.ComboBox2.Visible = False
        '
        'txtMensajeError
        '
        Me.txtMensajeError.Location = New System.Drawing.Point(160, 24)
        Me.txtMensajeError.Multiline = True
        Me.txtMensajeError.Name = "txtMensajeError"
        Me.txtMensajeError.ReadOnly = True
        Me.txtMensajeError.Size = New System.Drawing.Size(546, 40)
        Me.txtMensajeError.TabIndex = 3
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(56, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(102, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Recibo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtMonedaRecibo)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.cmdEliminarArqueo)
        Me.GroupBox2.Controls.Add(Me.txtNombreCliente)
        Me.GroupBox2.Controls.Add(Me.txtCliente)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.txtMontoRecibo)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 163)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(712, 88)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Encabezado del Recibo"
        '
        'txtNombreCliente
        '
        Me.txtNombreCliente.Location = New System.Drawing.Point(201, 34)
        Me.txtNombreCliente.Name = "txtNombreCliente"
        Me.txtNombreCliente.Size = New System.Drawing.Size(335, 20)
        Me.txtNombreCliente.TabIndex = 28
        '
        'txtCliente
        '
        Me.txtCliente.Location = New System.Drawing.Point(97, 34)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(104, 20)
        Me.txtCliente.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(49, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Cliente"
        '
        'CheckBox1
        '
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.Location = New System.Drawing.Point(552, 19)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox1.TabIndex = 25
        Me.CheckBox1.Text = "Eliminar"
        Me.CheckBox1.Visible = False
        '
        'txtMontoRecibo
        '
        Me.txtMontoRecibo.Location = New System.Drawing.Point(96, 61)
        Me.txtMontoRecibo.Name = "txtMontoRecibo"
        Me.txtMontoRecibo.Size = New System.Drawing.Size(104, 20)
        Me.txtMontoRecibo.TabIndex = 16
        Me.txtMontoRecibo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 61)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Monto Recibo"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvxDetalleRecibo)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 259)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(712, 157)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle del Recibo"
        '
        'dgvxDetalleRecibo
        '
        Me.dgvxDetalleRecibo.AllowUserToAddRows = False
        Me.dgvxDetalleRecibo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvxDetalleRecibo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxDetalleRecibo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumeroFacturaPagada, Me.Moneda, Me.MontoPagadoFactura, Me.FechaRecibo})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxDetalleRecibo.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvxDetalleRecibo.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxDetalleRecibo.Location = New System.Drawing.Point(5, 16)
        Me.dgvxDetalleRecibo.Name = "dgvxDetalleRecibo"
        Me.dgvxDetalleRecibo.Size = New System.Drawing.Size(698, 136)
        Me.dgvxDetalleRecibo.TabIndex = 54
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Importar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Procesar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        '
        'Timer1
        '
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.txtMontoTotalRecibosCOR)
        Me.GroupBox5.Controls.Add(Me.txtTotalCantidaRecibosCOR)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Location = New System.Drawing.Point(16, 16)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(680, 43)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Recibos COR"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(364, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(125, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Monto Total Recibos"
        '
        'txtMontoTotalRecibosCOR
        '
        Me.txtMontoTotalRecibosCOR.Location = New System.Drawing.Point(502, 15)
        Me.txtMontoTotalRecibosCOR.Name = "txtMontoTotalRecibosCOR"
        Me.txtMontoTotalRecibosCOR.ReadOnly = True
        Me.txtMontoTotalRecibosCOR.Size = New System.Drawing.Size(146, 20)
        Me.txtMontoTotalRecibosCOR.TabIndex = 16
        Me.txtMontoTotalRecibosCOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalCantidaRecibosCOR
        '
        Me.txtTotalCantidaRecibosCOR.Location = New System.Drawing.Point(130, 15)
        Me.txtTotalCantidaRecibosCOR.Name = "txtTotalCantidaRecibosCOR"
        Me.txtTotalCantidaRecibosCOR.ReadOnly = True
        Me.txtTotalCantidaRecibosCOR.Size = New System.Drawing.Size(146, 20)
        Me.txtTotalCantidaRecibosCOR.TabIndex = 14
        Me.txtTotalCantidaRecibosCOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(26, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Cantidad recibos"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 417)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(712, 112)
        Me.GroupBox4.TabIndex = 16
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Resumen de la Importación"
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdImportar, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(712, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 37
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdImportar
        '
        Me.cmdImportar.Image = CType(resources.GetObject("cmdImportar.Image"), System.Drawing.Image)
        Me.cmdImportar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdImportar.Name = "cmdImportar"
        Me.cmdImportar.Text = "Importar<F3>"
        Me.cmdImportar.Tooltip = "Importar"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Procesar<F4>"
        Me.cmdiLimpiar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'cmdEliminarArqueo
        '
        Me.cmdEliminarArqueo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEliminarArqueo.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdEliminarArqueo.Location = New System.Drawing.Point(552, 48)
        Me.cmdEliminarArqueo.Name = "cmdEliminarArqueo"
        Me.cmdEliminarArqueo.Size = New System.Drawing.Size(133, 26)
        Me.cmdEliminarArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdEliminarArqueo.TabIndex = 42
        Me.cmdEliminarArqueo.Text = "Quitar Recibo Existente del Import"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label3)
        Me.GroupBox6.Controls.Add(Me.txtMontoTotalRecibosUSD)
        Me.GroupBox6.Controls.Add(Me.txtTotalCantidaRecibosUSD)
        Me.GroupBox6.Controls.Add(Me.Label4)
        Me.GroupBox6.Location = New System.Drawing.Point(16, 61)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(680, 43)
        Me.GroupBox6.TabIndex = 13
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Recibos USD"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(364, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(125, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Monto Total Recibos"
        '
        'txtMontoTotalRecibosUSD
        '
        Me.txtMontoTotalRecibosUSD.Location = New System.Drawing.Point(502, 15)
        Me.txtMontoTotalRecibosUSD.Name = "txtMontoTotalRecibosUSD"
        Me.txtMontoTotalRecibosUSD.ReadOnly = True
        Me.txtMontoTotalRecibosUSD.Size = New System.Drawing.Size(146, 20)
        Me.txtMontoTotalRecibosUSD.TabIndex = 16
        Me.txtMontoTotalRecibosUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalCantidaRecibosUSD
        '
        Me.txtTotalCantidaRecibosUSD.Location = New System.Drawing.Point(130, 15)
        Me.txtTotalCantidaRecibosUSD.Name = "txtTotalCantidaRecibosUSD"
        Me.txtTotalCantidaRecibosUSD.ReadOnly = True
        Me.txtTotalCantidaRecibosUSD.Size = New System.Drawing.Size(146, 20)
        Me.txtTotalCantidaRecibosUSD.TabIndex = 14
        Me.txtTotalCantidaRecibosUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(26, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Cantidad recibos"
        '
        'NumeroFacturaPagada
        '
        Me.NumeroFacturaPagada.HeaderText = "Numero Factura Pagada"
        Me.NumeroFacturaPagada.Name = "NumeroFacturaPagada"
        Me.NumeroFacturaPagada.ReadOnly = True
        Me.NumeroFacturaPagada.Width = 180
        '
        'Moneda
        '
        Me.Moneda.HeaderText = "Moneda Factura"
        Me.Moneda.Name = "Moneda"
        Me.Moneda.ReadOnly = True
        Me.Moneda.Width = 130
        '
        'MontoPagadoFactura
        '
        Me.MontoPagadoFactura.HeaderText = "Monto Pagado A Factura"
        Me.MontoPagadoFactura.Name = "MontoPagadoFactura"
        Me.MontoPagadoFactura.ReadOnly = True
        Me.MontoPagadoFactura.Width = 180
        '
        'FechaRecibo
        '
        Me.FechaRecibo.HeaderText = "Fecha Recibo"
        Me.FechaRecibo.Name = "FechaRecibo"
        Me.FechaRecibo.ReadOnly = True
        Me.FechaRecibo.Width = 150
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(201, 61)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Moneda"
        '
        'txtMonedaRecibo
        '
        Me.txtMonedaRecibo.Location = New System.Drawing.Point(254, 61)
        Me.txtMonedaRecibo.Name = "txtMonedaRecibo"
        Me.txtMonedaRecibo.Size = New System.Drawing.Size(104, 20)
        Me.txtMonedaRecibo.TabIndex = 44
        Me.txtMonedaRecibo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmImportarRecibos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(712, 531)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmImportarRecibos"
        Me.Text = "frmImportarRecibos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvxDetalleRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String = String.Empty
    Dim strNombAlmacenar As String = String.Empty

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmImportarRecibos"

    Private Sub frmImportarRecibos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            txtCliente.ReadOnly = True
            txtNombreCliente.ReadOnly = True
            txtMontoRecibo.ReadOnly = True
            txtMonedaRecibo.ReadOnly = True
            txtMontoRecibo.Text = String.Empty
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            'ValidarDatosImportados()
            'CargarResumen()
            'CargarRecibos()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmImportarRecibos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                ObtenerArchivo()
            ElseIf e.KeyCode = Keys.F4 Then
                ProcesarArchivo()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 2
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0 : ObtenerArchivo()
        '    Case 1 : ProcesarArchivo()
        '    Case 2 : Me.Close()
        'End Select

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click

        Select Case sender.name.ToString
            Case "Importar" : ObtenerArchivo()
            Case "Procesar"
            Case "Salir" : Me.Close()
        End Select

    End Sub

    Sub LimpiarCompleto()
        Try
            txtMontoTotalRecibosCOR.Text = String.Empty
            txtMontoTotalRecibosUSD.Text = String.Empty
            txtTotalCantidaRecibosCOR.Text = String.Empty
            txtTotalCantidaRecibosUSD.Text = String.Empty
            If ComboBox1.Items.Count > 0 Then
                ComboBox1.Items.Clear()
            End If
            txtMonedaRecibo.Text = String.Empty
            txtMensajeError.Text = String.Empty
            txtCliente.Text = String.Empty
            txtNombreCliente.Text = String.Empty
            'txtDiasCredito.Text = ""
            txtMontoRecibo.Text = String.Empty
            'txtMontoTotalRecibo.Text = ""
            'txtRentencion.Text = ""
            'txtTotal.Text = ""
            dgvxDetalleRecibo.Rows.Clear()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub
    Sub Limpiar()
        Try

            txtMensajeError.Text = String.Empty
            'txtCliente.Text = ""
            'txtNombreCliente.Text = ""
            'txtDiasCredito.Text = ""
            txtMontoRecibo.Text = String.Empty
            'txtMontoTotalRecibo.Text = ""
            'txtRentencion.Text = ""
            'txtTotal.Text = ""
            dgvxDetalleRecibo.Rows.Clear()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub ObtenerArchivo()
        ' Dim myStream As Stream
        Dim dlgNew As New OpenFileDialog
        Dim strLine, strDrive As String
        Dim lngRegistro As Long
        Dim strRecibos As String
        Dim intFechaIng As Integer
        Dim strFechaIng As String
        Dim strVendCodigo As String
        Dim strCliCodigo As String
        Dim strCliNombre As String
        Dim dblImpuesto As Double
        Dim dblRetencion As Double
        Dim dblTotal As Double
        Dim strUsrCodigo As String
        Dim intRegDetalle As Integer
        Dim strArchivo, strArchivo2, strClave, strComando As String
        Dim sNumeroChequeTarjeta As String
        Dim strFormaPago As String
        Dim sNombreBanco As String
        Dim sNoDeposito As String
        Dim sReciboProv As String
        Dim sDescripcion, sDescripcion1, sDescripcion2, sDescripcion3 As String
        Dim nTipoRecibo As Integer
        Dim sFechaIngreso As String
        Dim nSaldoAfavor As Double
        Dim sEstado As String
        Dim sCodigoAgencia As String
        Dim sNumeroFactura As String
        Dim sDescripcionDetalle As String
        Dim nMonto, nInteres, nMantenimientoAlValor As Double
        Dim sPendiente As String
        Dim sFechaAnulacion As String
        Dim sHoraAnulacion As String
        Dim lsCodigoMonedaRecibo As String = String.Empty
        Dim lsCodigoMonedaFactura As String = String.Empty
        Dim lnTipoCambio As Decimal = 0
        Dim Columnas As String()

        Dim existe, er As Integer
        Dim strQueryVal As String = ""

        Try
            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
                Exit Sub
            End If
            strDrive = String.Empty
            strComando = String.Empty
            strArchivo = String.Empty
            strArchivo2 = String.Empty
            strClave = String.Empty
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "recibos_")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MsgBox("No es un archivo que contiene las Recibos de una agencia. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            strNombAlmacenar = ""
            strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            ' intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
                Exit Sub
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "truncate table tmpImportarRecibos"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            strNombArchivo = ""
            strNombArchivo = strArchivo2
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            intIncr = 0
            intIncr = InStrRev(strNombArchivo, "\")
            strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
            strQuery = ""
            strQuery = "select * from detArchivosRecibosImportados where archivo = '" & strNombAlmacenar & "'"
            cmdAgro2K.CommandText = strQuery
            Try
                intError = 0
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    intError = 1
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            If intError = 1 Then
                MsgBox("Archivo ya fue procesado.  Verifique.", MsgBoxStyle.Critical, "Error de Archivo")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            intRegDetalle = 0

            er = 1  ' con uno indica que los Recibos del archivo que se importa ya existen, su valor cambia si al menos un Recibo no existe
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                Columnas = strLine.Trim().Split("|")
                lngRegistro = 0
                lngRegistro = SUConversiones.ConvierteADouble(Columnas(0).ToString())
                strFechaIng = String.Empty
                strFechaIng = Columnas(1).ToString()
                intFechaIng = 0
                intFechaIng = SUConversiones.ConvierteAInt(Columnas(2).ToString())
                strCliCodigo = String.Empty
                strCliCodigo = Columnas(3).ToString()
                strCliNombre = String.Empty
                strCliNombre = Columnas(4).ToString()
                strVendCodigo = String.Empty
                strVendCodigo = Columnas(5).ToString()
                strRecibos = String.Empty
                strRecibos = Columnas(6).ToString()
                dblTotal = 0
                dblTotal = SUConversiones.ConvierteADouble(Columnas(7).ToString())
                dblImpuesto = 0
                dblImpuesto = SUConversiones.ConvierteADouble(Columnas(8).ToString())
                dblRetencion = 0
                dblRetencion = SUConversiones.ConvierteADouble(Columnas(9).ToString())
                strFormaPago = String.Empty
                strFormaPago = Columnas(10).ToString()
                sNumeroChequeTarjeta = String.Empty
                sNumeroChequeTarjeta = Columnas(11).ToString()
                sNombreBanco = String.Empty
                sNombreBanco = Columnas(12).ToString()
                sNoDeposito = String.Empty
                sNoDeposito = Columnas(13).ToString()
                sReciboProv = String.Empty
                sReciboProv = Columnas(14).ToString()
                sDescripcion = String.Empty
                sDescripcion = Columnas(15).ToString()
                strUsrCodigo = String.Empty
                strUsrCodigo = Columnas(16).ToString()
                nTipoRecibo = 0
                nTipoRecibo = SUConversiones.ConvierteAInt(Columnas(17).ToString())
                sFechaIngreso = String.Empty
                sFechaIngreso = SUConversiones.ConvierteAInt(Columnas(18).ToString())
                nSaldoAfavor = 0
                nSaldoAfavor = SUConversiones.ConvierteADouble(ConvierteAInt(Columnas(19).ToString()))
                sEstado = String.Empty
                sEstado = Columnas(20).ToString()
                sDescripcion1 = String.Empty
                sDescripcion1 = Columnas(21).ToString()
                sDescripcion2 = String.Empty
                sDescripcion2 = Columnas(22).ToString()
                sDescripcion3 = String.Empty
                sDescripcion3 = Columnas(23).ToString()
                sCodigoAgencia = String.Empty
                sCodigoAgencia = Columnas(24).ToString()
                intRegDetalle = 0
                intRegDetalle = SUConversiones.ConvierteAInt(Columnas(25).ToString())
                sNumeroFactura = String.Empty
                sNumeroFactura = Columnas(26).ToString()
                sDescripcionDetalle = String.Empty
                sDescripcionDetalle = Columnas(27).ToString()
                nMonto = 0
                nMonto = SUConversiones.ConvierteADouble(Columnas(28).ToString())
                nInteres = 0
                nInteres = SUConversiones.ConvierteADouble(Columnas(29).ToString())
                nMantenimientoAlValor = 0
                nMantenimientoAlValor = SUConversiones.ConvierteADouble(Columnas(30).ToString())
                sPendiente = String.Empty
                sPendiente = SUConversiones.ConvierteADouble(Columnas(31).ToString())
                sFechaAnulacion = String.Empty
                sFechaAnulacion = Columnas(32).ToString()
                sHoraAnulacion = String.Empty
                sHoraAnulacion = Columnas(33).ToString()
                lsCodigoMonedaRecibo = String.Empty
                If Columnas.Length >= 33 Then
                    lsCodigoMonedaRecibo = Columnas(34).ToString()
                End If
                lnTipoCambio = 0
                If Columnas.Length >= 34 Then
                    lnTipoCambio = SUConversiones.ConvierteADecimal(Columnas(35).ToString())
                End If
                lsCodigoMonedaFactura = String.Empty
                If Columnas.Length >= 35 Then
                    lsCodigoMonedaFactura = Columnas(36).ToString()
                End If

                strQueryVal = "exec spValidaRecibosImportar '" & strRecibos & "', '" & sCodigoAgencia & "'"

                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If

                cnnAgro2K.Open()
                Try
                    cmdAgro2K.CommandText = strQueryVal
                    dtrAgro2K = cmdAgro2K.ExecuteReader()
                    While dtrAgro2K.Read
                        existe = dtrAgro2K.GetValue(0)
                    End While
                    dtrAgro2K.Close()

                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try

                If existe = 0 Then  'Cero indica que la requisa no existe en el sitema y por tanto se ingresa, 1 indica que existe y no se ingresa
                    er = existe
                    strQuery = ""
                    'strQuery = "insert into tmp_facturasimp values (" & lngRegistro & ", '" & strRecibos & "', "
                    strQuery = "exec InresaImportaRecibo " & lngRegistro & ", '" & strFechaIng & "', "
                    strQuery = strQuery + "" & intFechaIng & ", '" & strCliCodigo & "', '" & strCliNombre & "', '" & strVendCodigo & "', "
                    strQuery = strQuery + "'" & strRecibos & "', " & dblTotal & ", " & dblImpuesto & ", "
                    strQuery = strQuery + "" & dblRetencion & ", "
                    strQuery = strQuery + "'" & strFormaPago & "', ' " & sNumeroChequeTarjeta & " ', '" & sNombreBanco & " ', '" & sNoDeposito & " ', "
                    strQuery = strQuery + "'" & sReciboProv & "', '" & sDescripcion & "', '" & strUsrCodigo & "', "
                    strQuery = strQuery + "" & nTipoRecibo & ", '" & sFechaIngreso & "', "
                    strQuery = strQuery + "" & nSaldoAfavor & ", '" & sEstado & "', '" & sDescripcion1 & " ','" & sDescripcion2 & " ', "
                    strQuery = strQuery + "'" & sDescripcion3 & "','" & sCodigoAgencia & "', " & intRegDetalle & ", '" & sNumeroFactura & "', "
                    strQuery = strQuery + "'" & sDescripcionDetalle & "'," & nMonto & "," & nInteres & "," & nMantenimientoAlValor & ",'" & sFechaAnulacion & " ','" & sHoraAnulacion & " ','" & sPendiente.Trim() & "','" & lsCodigoMonedaRecibo & "'," & lnTipoCambio & ",'" & lsCodigoMonedaFactura & "','Sin Error',0"

                    Try
                        cmdAgro2K.CommandText = strQuery
                        cmdAgro2K.ExecuteNonQuery()
                    Catch exc As Exception
                        intError = 1
                        MsgBox(exc.Message.ToString)
                        Me.Cursor = Cursors.Default
                        Exit Do
                    End Try
                End If
            Loop
            sr.Close()
            ValidarDatosImportados()
            CargarRecibos()
            CargarResumen()
            If er = 0 Then 'Condicionamos para indicarle al husuario si ya ha ingresado las Facturas de compras
                If intError = 0 Then
                    'ValidarDatosImportados()
                    'CargarResumen()
                    'CargarRecibos()
                    MsgBox("Finalizado satisfactoriamente la importación de las Recibos.", MsgBoxStyle.Information, "Proceso Finalizado")
                Else
                    MsgBox("Hubieron errores al importar el archivo de Recibos.", MsgBoxStyle.Critical, "Error en la Importación")
                End If
            Else
                MsgBox("El archivo de Recibos ya fue Importado.", MsgBoxStyle.Critical, "Error en la Importación")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub ValidarDatosImportados()

        Try
            Dim cmdTmp As New SqlCommand("ImportarRecibos", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter

            intError = 0
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    'If ConvierteAInt(dtrAgro2K.GetValue(0)) = 0 Then
                    '    MsgBox("No hubieron errores en la validación de las Recibos", MsgBoxStyle.Information, "Validación de Datos")
                    'ElseIf ConvierteAInt(dtrAgro2K.GetValue(0)) <> 254 Then
                    '    intError = 1
                    '    MsgBox("Hubieron errores en la validación de las Recibos", MsgBoxStyle.Critical, "Validación de Datos")
                    'End If
                    If SUConversiones.ConvierteAInt(dtrAgro2K.GetValue(0)) = 0 Then
                        intError = 0
                    ElseIf SUConversiones.ConvierteAInt(dtrAgro2K.GetValue(0)) <> 254 Then
                        intError = 1
                    End If
                End While
                SUFunciones.CierraConexioDR(dtrAgro2K)
                'dtrAgro2K.Close()
            Catch exc As Exception
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                objError.Clase = sNombreClase
                objError.Metodo = sNombreMetodo
                objError.descripcion = exc.Message.ToString()
                SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
                SNError.IngresaError(objError)
                MsgBox(exc.Message.ToString)
            End Try
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
    End Sub

    Sub CargarResumen()
        Try

            Dim cmdTmp As New SqlCommand("ImportarRecibos", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 1
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                    ' If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    txtTotalCantidaRecibosCOR.Text = Format(ConvierteADouble(dtrAgro2K.Item("Cantidad")), "##,##0")
                    txtMontoTotalRecibosCOR.Text = Format(ConvierteADouble(dtrAgro2K.Item("MontoTotal")), "##,##0.#0")
                    txtTotalCantidaRecibosUSD.Text = Format(ConvierteADouble(dtrAgro2K.Item("CantidadUSD")), "##,##0")
                    txtMontoTotalRecibosUSD.Text = Format(ConvierteADouble(dtrAgro2K.Item("MontoTotalUSD")), "##,##0.#0")
                    'End If
                End While
                SUFunciones.CierraConexioDR(dtrAgro2K)
                'dtrAgro2K.Close()
            Catch ex As Exception
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Sub CargarRecibos()
        Try
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strQuery = ""
            strQuery = "exec ObtieneRecibosACargar"
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
                    ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
                End While
            Catch exc As Exception
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                objError.Clase = sNombreClase
                objError.Metodo = sNombreMetodo
                objError.descripcion = exc.Message.ToString()
                SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
                SNError.IngresaError(objError)
                'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
                'Throw exc
                MsgBox(exc.Message.ToString)
            End Try
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()


        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Limpiar()
            ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
            txtMensajeError.Text = "Sin Error"
            strQuery = ""
            strQuery = "exec ObtieneDetalleRecibo " & ComboBox2.SelectedItem & ""
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    txtCliente.Text = dtrAgro2K.Item("Codigocliente")
                    txtNombreCliente.Text = dtrAgro2K.Item("NombreCliente")
                    txtMontoRecibo.Text = Format(dtrAgro2K.Item("MontoTotal"), "##,##0.#0")
                    txtMonedaRecibo.Text = dtrAgro2K.Item("CodigoMonedaRecibo")
                    dgvxDetalleRecibo.Rows.Add(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("CodigoMonedaFactura"), Format(dtrAgro2K.Item("monto"), "##,##0.#0"),
                                           dtrAgro2K.Item("FechaIngFormateada"))
                    If dtrAgro2K.Item("IndicaError") Then
                        txtMensajeError.Text = dtrAgro2K.Item("registroError")
                    End If
                End While
            Catch exc As Exception
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                objError.Clase = sNombreClase
                objError.Metodo = sNombreMetodo
                objError.descripcion = exc.Message.ToString()
                SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
                SNError.IngresaError(objError)
                'MsgBox(exc.Message.ToString)
                'dtrAgro2K.Close()
                SUFunciones.CierraConexioDR(dtrAgro2K)
                Me.Cursor = System.Windows.Forms.Cursors.Default
                'Throw exc
                MsgBox(exc.Message.ToString)
                Exit Sub
            End Try
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try

    End Sub

    Function UbicarProducto(ByVal prm01 As String) As Boolean

        Dim intEnc As Integer
        Try
            intEnc = 0
            strQuery = ""
            strQuery = "Select Codigo, Descripcion, Pvpc, Impuesto from prm_Productos Where Codigo = '" & prm01 & "'"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                bytImpuesto = dtrAgro2K.Item("Impuesto")
                intEnc = 1
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If intEnc = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Function

    Sub ProcesarArchivo()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim cmdTmp As New SqlCommand("ImportarRecibos", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim bytContinuar As Byte

            bytContinuar = 0
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(0) = 0 Then
                        MsgBox("No hay errores en la validación de las Recibos. Se procedera a ingresar las Recibos.", MsgBoxStyle.Information, "Validación de Datos")
                    ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                        bytContinuar = 1
                        MsgBox("Continuan los errores en la validación de las Recibos", MsgBoxStyle.Critical, "Validación de Datos")
                        Exit Sub
                    End If
                End While
                'dtrAgro2K.Close()
                SUFunciones.CierraConexioDR(dtrAgro2K)
            Catch exc As Exception
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                objError.Clase = sNombreClase
                objError.Metodo = sNombreMetodo
                objError.descripcion = exc.Message.ToString()
                SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
                SNError.IngresaError(objError)

                'MsgBox(exc.Message.ToString)
                'dtrAgro2K.Close()
                SUFunciones.CierraConexioDR(dtrAgro2K)
                Me.Cursor = System.Windows.Forms.Cursors.Default
                MsgBox(exc.Message.ToString)
                'Throw exc
                Exit Sub
            End Try
            If bytContinuar = 0 Then
                cmdTmp.Parameters(0).Value = 3
                Try
                    cmdTmp.ExecuteNonQuery()
                Catch exc As Exception
                    sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                    objError.Clase = sNombreClase
                    objError.Metodo = sNombreMetodo
                    objError.descripcion = exc.Message.ToString()
                    SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
                    SNError.IngresaError(objError)

                    'MsgBox(exc.Message.ToString)
                    'dtrAgro2K.Close()
                    SUFunciones.CierraConexioDR(dtrAgro2K)
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    MsgBox(exc.Message.ToString)
                    'Throw exc
                    Exit Sub
                End Try
            End If
            strQuery = ""
            strQuery = "insert into tbl_ArchivosImp values ('" & strNombAlmacenar & "', "
            strQuery = strQuery + "'" & Format(Now, "dd-MMM-yyyy") & "', " & Format(Now, "yyyyMMdd") & ", "
            strQuery = strQuery + "" & lngRegUsuario & ")"
            cmdAgro2K.CommandText = strQuery
            Try
                cmdAgro2K.ExecuteNonQuery()
            Catch exc As Exception
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                objError.Clase = sNombreClase
                objError.Metodo = sNombreMetodo
                objError.descripcion = exc.Message.ToString()
                SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
                SNError.IngresaError(objError)
                'Throw exc
                MsgBox(exc.Message.ToString)
            End Try
            strQuery = ""
            strQuery = "truncate table tmp_facturasimp"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            MsgBox("Finalizo el proceso de importar ventar de una agencia a la Casa Matriz.", MsgBoxStyle.Exclamation, "Proceso Finalizado")
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        'If blnUbicar = True Then
        '    blnUbicar = False
        '    Timer1.Enabled = False
        '    If strUbicar <> "" Then
        '        txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text) : txtNombreCliente.Focus()
        '    End If
        'End If

    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            txtNombreCliente.Focus()
        End If

    End Sub

    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

        'UbicarCliente(txtCliente.Text)

    End Sub

    Private Sub cmdImportar_Click(sender As Object, e As EventArgs) Handles cmdImportar.Click
        Try
            LimpiarCompleto()
            ObtenerArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click(sender As Object, e As EventArgs) Handles cmdiLimpiar.Click
        Try
            ProcesarArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try
    End Sub

    Private Sub cmdiSalir_Click(sender As Object, e As EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdEliminarArqueo_Click(sender As Object, e As EventArgs) Handles cmdEliminarArqueo.Click
        Try
            If ComboBox1.Items.Count > 0 Then
                EliminarReciboAImport()

                dgvxDetalleRecibo.Rows.Clear()

                txtCliente.Text = String.Empty
                txtNombreCliente.Text = String.Empty
                txtMontoRecibo.Text = String.Empty
                txtMensajeError.Text = String.Empty
                CargarRecibos()
                CargarResumen()
            Else
                MsgBox("No ha seleccionado un Numero de Recibo para Retirar del Import..", MsgBoxStyle.Exclamation, "Importación de recibos")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Sub EliminarReciboAImport()
        Dim lsNumeroRecibo As String = String.Empty
        Try
            lsNumeroRecibo = ComboBox1.SelectedItem
            RNRecibos.EliminaReciboAImportarxNumeroRecibo(lsNumeroRecibo)
        Catch exc As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", objError.descripcion, EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox(exc.Message.ToString)
        End Try
    End Sub
End Class
