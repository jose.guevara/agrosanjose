﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.SqlClient
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class ConsultasDB
    Friend jackvalida As Valida
    Public ConnectionStringSIMF_Conta As String = String.Empty
    Public Shared MntoCuenta_SI As Double = 0
    Public Shared MntoCuenta_SIext As Double = 0
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ConsultasDB"

    Public Class ErrorEjecutar
        Private _cerror As Integer
        Private _xerror As String

        Public Property Cerror() As Integer
            Get
                Return _cerror
            End Get
            Set(ByVal value As Integer)
                _cerror = value
            End Set
        End Property

        Public Property Xerror() As String
            Get
                Return _xerror
            End Get
            Set(ByVal value As String)
                _xerror = value
            End Set
        End Property
    End Class

    Public Sub New(Optional ByVal construye_conexion As Boolean = False)

        Try
            ConnectionStringSIMF_Conta = RNConeccion.ObtieneCadenaConexion()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Sub


    Public Function Retorna_Campo(ByVal sql As String, Optional ByVal conta As Boolean = False, Optional ByVal no_null As Boolean = False) As Object
        Dim ConnectionString As String = String.Empty
        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim resultado As Object = Nothing
        Try
            If conta Then
                ConnectionString = RNConeccion.ObtieneCadenaConexion()
            End If
            cn = New SqlConnection(ConnectionString)
            cn.Open()
            cmd = New SqlCommand(sql, cn)
            resultado = cmd.ExecuteScalar

            If (IsDBNull(resultado) OrElse IsNothing(resultado)) AndAlso no_null Then
                Retorna_Campo = -1
            Else
                Retorna_Campo = resultado
            End If

            cmd.Connection.Close()
            cmd.Dispose()


            cn.Close()
            cn.Dispose()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return resultado
        End Try

    End Function


    Public Function EjecutaSQL(ByVal sql As String, Optional ByVal conta As Boolean = False) As Integer

        'devuelve los registros afectados
        Dim cn As SqlConnection = Nothing
        'Validación para asignar la cadena de conección
        Dim StrCnn As String = String.Empty
        Dim valor_retorno As Object = Nothing
        Dim IsError As ErrorEjecutar = Nothing
        Dim trans As SqlTransaction = Nothing
        Dim cmd As SqlCommand = Nothing

        If conta Then
            StrCnn = RNConeccion.ObtieneCadenaConexion()
        End If

        IsError = New ErrorEjecutar
        'Inicio de la trasacción Sql
        cn = New SqlConnection(StrCnn)
        cn.Open()

        'Declaracion de la transacción a la base de datos y se inicializa la transacción
        trans = cn.BeginTransaction
        Try

            cmd = New SqlCommand(sql, cn, trans)

            'EjecutaSQL = cmd.ExecuteNonQuery
            EjecutaSQL = cmd.ExecuteScalar
            'Finaliza la transacción
            trans.Commit()
            trans.Dispose()

            If cn.State = ConnectionState.Open Then
                cmd.Connection.Close()
                cmd.Connection.Dispose()
                cn.Close()
                cn.Dispose()
            End If

        Catch ex As Exception
            'En caso de error la trasacción se revierte.
            IsError.Cerror = Err.Number
            IsError.Xerror = ex.Message
            Console.WriteLine(ex.Message)
            trans.Rollback()
            EjecutaSQL = -1
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        Finally

            'Se verifica si se quedo la conección abierta y se cierra.
            If cn.State = ConnectionState.Open Then
                cn.Close()
                cn.Dispose()
            End If

        End Try

    End Function

    Public Function EjecutaSQL_reader(ByVal sql As String, Optional ByVal sisGlobal As Boolean = False) As Data.SqlClient.SqlDataReader


        Dim conx As String = String.Empty
        Dim cn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Try
            If sisGlobal Then
                conx = RNConeccion.ObtieneCadenaConexion()
            Else

            End If

            cn = New SqlConnection(conx)

            cn.Open()
            cmd = New SqlCommand(sql, cn)
            EjecutaSQL_reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try

    End Function

    Public Function EjecutaSQL_datatable(ByVal QueryString As String, ByRef DTable As DataTable, Optional ByVal conta As Boolean = False) As DataTable

        Dim ConnectionString As String = String.Empty
        Dim cn As SqlConnection = Nothing

        If conta Then
            ConnectionString = RNConeccion.ObtieneCadenaConexion()
        End If


        Dim Adaptador As SqlDataAdapter = Nothing
        Dim ResultDataTable As DataTable = DTable
        Try
            cn = New SqlConnection(ConnectionString)
            Adaptador = New SqlDataAdapter(QueryString, cn)
            Adaptador.Fill(ResultDataTable)
            cn.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, QueryString, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            If cn.State = ConnectionState.Open Then
                cn.Close()
            End If
            Throw ex
        End Try
        Return ResultDataTable
    End Function

    ''' <summary>
    ''' Ejecuta una consulta evitando la Inyeccion SQL pasandole parámetros
    ''' </summary>
    ''' <param name="consulta">consulta sql o sp</param>
    ''' <param name="parametros">colección de parametros enviados a la consulta</param>
    ''' <param name="dt">datatable que tiene que retornar</param>
    ''' <param name="tipo">tipo que debe ejecutar, si es una consulta normal dejarla por default pero si es un store procedure enviar sp</param>
    ''' <param name="bSis">la conexión que desea utilizar</param>
    ''' <returns>Devuelve un DataTable</returns>
    ''' <remarks></remarks>
    Public Function EjecutarSelect(ByVal consulta As String, ByVal parametros() As SqlParameter, ByVal dt As DataTable, Optional ByVal tipo As String = "texto", Optional ByVal bSis As Boolean = False) As DataTable
        Dim comando As New SqlCommand
        Dim DBAdapter As New SqlDataAdapter
        Dim DBConnection As Data.SqlClient.SqlConnection = New SqlConnection(IIf(bSis = False, ConnectionStringMundisis, RNConeccion.ObtieneCadenaConexion()))
        Try
            If tipo = "sp" Then
                comando.CommandType = CommandType.StoredProcedure
            End If

            If DBConnection.State = ConnectionState.Closed Or DBConnection.State = ConnectionState.Broken Then
                DBConnection.Open()
            End If

            comando.Connection = DBConnection
            comando.CommandText = consulta

            If Not parametros Is Nothing Then
                comando.Parameters.AddRange(parametros)
            End If

            DBAdapter.SelectCommand = comando
            DBAdapter.Fill(dt)

            If DBConnection.State = ConnectionState.Open Then
                DBConnection.Close()
            End If

            Return dt

        Catch ex As Exception
            ' Close the database connection if it is still open.
            If DBConnection.State = ConnectionState.Open Then
                DBConnection.Close()
            End If
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Function Exportar_Grilla_Excel(ByVal ElGrid As DataGridView, Optional ByVal nombre_hoja As String = "", Optional ByVal nombre_libro As String = "") As Boolean

        'Creamos las variables
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet

        Try
            'Añadimos el Libro al programa, y la hoja al libro
            exLibro = exApp.Workbooks.Add
            exHoja = exLibro.Worksheets.Add()

            ElGrid.AllowUserToAddRows = True

            ' ¿Cuantas columnas y cuantas filas?
            Dim NCol As Integer = ElGrid.ColumnCount
            Dim NRow As Integer = ElGrid.RowCount


            'Aqui recorremos todas las filas, y por cada fila todas las columnas y vamos escribiendo.
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(1, i) = ElGrid.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3

            Next

            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 2, Col + 1) = ElGrid.Rows(Fila).Cells(Col).Value
                Next
            Next
            'Titulo en negrita, Alineado al centro y que el tamaño de la columna se ajuste al texto
            exHoja.Rows.Item(1).Font.Bold = 1
            exHoja.Rows.Item(1).HorizontalAlignment = 3
            exHoja.Columns.AutoFit()


            'Aplicación visible
            exApp.Application.Visible = True
            If nombre_hoja <> "" Then
                exHoja.Name = nombre_hoja
            End If
            If nombre_libro <> "" Then
                'exLibro.Name.ToString = nombre_libro
            End If

            'exHoja.Cells(NRow ,NCol)).Borders.LineStyle = xlContinuous
            'exHoja.Weight = xlThin
            '.ColorIndex = xlAutomatic


            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing

        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return False
        End Try

        Return True

    End Function





    Public Function RetornaReader(ByVal sql As String, ByVal conta As Boolean) As Data.SqlClient.SqlDataReader

        Dim ConnectionString As String = String.Empty
        Dim conexion As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Try
            If conta Then
                ConnectionString = RNConeccion.ObtieneCadenaConexion()
            End If
            conexion = New SqlConnection(ConnectionString)
            conexion.Open()
            cmd = New SqlCommand(sql, conexion)
            RetornaReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return RetornaReader
    End Function

    Public Function Retornadatatable(ByVal sql As String, ByRef DataTable As DataTable, Optional ByVal conta As Boolean = False) As DataTable

        Dim ConnectionString As String = String.Empty
        Dim conexion As SqlConnection = Nothing
        Dim Adaptador As SqlDataAdapter = Nothing
        Dim datos As DataTable = DataTable
        Try

            If conta Then
                ConnectionString = RNConeccion.ObtieneCadenaConexion()
            End If
            conexion = New SqlConnection(ConnectionString)
            Adaptador = New SqlDataAdapter(sql, conexion)
            Adaptador.Fill(datos)
            conexion.Close()
        Catch ex As Exception
            If conexion.State = ConnectionState.Open Then
                conexion.Close()
            End If

            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return datos
    End Function

    Public Function retornaDataset(ByVal query As String, Optional ByVal conta As Boolean = False) As DataSet
        Dim dset As DataSet = Nothing
        Dim conexion As SqlConnection = Nothing
        Dim comando As SqlCommand = Nothing
        Dim adaptador As SqlDataAdapter = Nothing
        Try
            dset = New DataSet
            If conta = True Then
                conexion = New SqlConnection(RNConeccion.ObtieneCadenaConexion())

            End If

            comando = New SqlCommand(query, conexion)
            adaptador = New SqlDataAdapter

            adaptador.SelectCommand = comando
            conexion.Open()
            adaptador.Fill(dset, "dataset")

            conexion.Close()

        Catch ex As Exception

            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return dset

    End Function
    Public Function llenaDataset(ByVal query As String, ByVal tabla As String, Optional ByVal conta As Boolean = False) As DataSet
        Dim dset As DataSet = Nothing
        Dim conexion As SqlConnection = Nothing
        Dim comando As SqlCommand = Nothing
        Dim adaptador As SqlDataAdapter = Nothing
        Try
            dset = New DataSet
            If conta = True Then
                conexion = New SqlConnection(RNConeccion.ObtieneCadenaConexion())

            End If

            comando = New SqlCommand(query, conexion)
            adaptador = New SqlDataAdapter

            adaptador.SelectCommand = comando
            conexion.Open()
            adaptador.Fill(dset, tabla)

            conexion.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return dset

    End Function

    Public Function DameCampo_UltimoCierre(ByVal conta As Boolean, Optional ByVal Cierre_Anual As Boolean = False, Optional ByVal campo As String = "rangocierre") As String
        Dim tipocierre As String = String.Empty
        Dim sql_Mcierres As SqlDataReader = Nothing
        Try
            DameCampo_UltimoCierre = String.Empty
            If Cierre_Anual = True Then
                tipocierre = "A"
            Else
                tipocierre = "M"
            End If
            campo = campo.ToLower
            sql_Mcierres = RetornaReader("select * from Mcierres where tipocierre='" & tipocierre & "'", True)
            If sql_Mcierres.HasRows Then
                While sql_Mcierres.Read
                    Select Case campo
                        Case "fecha_ini"
                            DameCampo_UltimoCierre = Format((CDate(sql_Mcierres!fecha_ini)), "dd/MM/yyyy")
                        Case "fecha_fin"
                            DameCampo_UltimoCierre = Format((CDate(sql_Mcierres!fecha_fin)), "dd/MM/yyyy")
                        Case "fecha_cierre"
                            DameCampo_UltimoCierre = Format((CDate(sql_Mcierres!fecha_cierre)), "dd/MM/yyyy")
                        Case "rangocierre"
                            DameCampo_UltimoCierre = sql_Mcierres!rangocierre
                    End Select

                End While
                sql_Mcierres.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return DameCampo_UltimoCierre

    End Function
    Public Function Dame_DescCuenata(ByVal cuenta As String) As String
        Dim sql_nombrecuenta As String = String.Empty
        Try
            'sql_nombrecuenta = Retorna_Campo("select desccuenta from CatalogoConta where cdgocuenta='" & cuenta & "'", True, True)
            sql_nombrecuenta = RNCuentaContable.ObtenerDescripcionCuenta(cuenta)
            If IsDBNull(sql_nombrecuenta) Or sql_nombrecuenta.ToString = "-1" Then

                Return Nothing
            Else
                Return sql_nombrecuenta
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function retornacompania(ByVal conta As Boolean) As Integer
        Dim conexion As SqlConnection = Nothing
        Try
            conexion = New SqlConnection(RNConeccion.ObtieneCadenaConexion())
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return conta
    End Function

    ''' <summary>
    ''' Retorna la entrada pero con comillas simples
    ''' </summary>
    ''' <param name="entrada"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function c(ByVal entrada As String) As String
        Return "'" & entrada & "'"
    End Function

    ''' <summary>
    ''' Carga los combos ASP.Net
    ''' </summary>
    ''' <param name="consulta">Consulta de la cual se deriva al carga del control</param>
    ''' <param name="texto">Texto a mostra en el combo</param>
    ''' <param name="valor">Valor Asignado al texto seleccionado</param>
    ''' <param name="control">Control que se desea cargar</param>
    Public Sub CargarCombo(ByVal consulta As String, ByVal texto As String, ByVal valor As String, ByVal control As Object, Optional ByVal seleccione As Boolean = True, Optional ByVal sis As Boolean = False)
        Dim conexion As SqlConnection = Nothing
        Dim ds As DataSet = Nothing
        Try

            conexion = New SqlConnection()

            ds = New DataSet
            ds = retornaDataset(consulta, sis)

            If seleccione Then
                Dim dr As Data.DataRow
                dr = ds.Tables(0).NewRow()
                dr(valor) = "-1"
                dr(texto) = "No-Especificado"
                ds.Tables(0).Rows.InsertAt(dr, 0)
            End If

            control.DataSource = ds
            control.DataValueField = valor
            control.DataTextField = texto
            control.DataBind()

        Catch ex As Exception
            'Console.Write(ex.ToString())
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Function cargacombo(ByVal sql As String, Optional ByVal conta As Boolean = False) As DataSet
        Dim ds As New DataSet
        Dim conexion As SqlConnection = Nothing
        Dim comando As SqlCommand = Nothing
        Dim adaptador As SqlDataAdapter = Nothing
        Try
            If conta Then
                conexion = New SqlConnection(RNConeccion.ObtieneCadenaConexion())
            End If

            comando = New SqlCommand(sql, conexion)
            adaptador = New SqlDataAdapter

            adaptador.SelectCommand = comando
            conexion.Open()
            adaptador.Fill(ds, "dataset")
            conexion.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return ds
    End Function
    Public Function carga_combo_DN(ByVal combo As Windows.Forms.ComboBox, ByVal data As DataTable, Optional ByVal sel As Boolean = True) As ComboBox
        Try
            If data.Rows.Count > 0 Then
                combo.DataSource = data
                combo.ValueMember = SUConversiones.ConvierteAString(data.Columns(1))
                combo.DisplayMember = SUConversiones.ConvierteAString(data.Columns(0))
                'combo.Text = ""
                If sel = True Then
                    Dim dr As Data.DataRow
                    dr = data.NewRow()
                    dr.Item(1) = "-1"
                    dr.Item(0) = "No-Especificado"
                    data.Rows.InsertAt(dr, 0)

                End If
            Else
                Dim dr As Data.DataRow
                dr = data.NewRow()
                dr.Item(1) = "-1"
                dr.Item(0) = "No-Especificado"
                data.Rows.InsertAt(dr, 0)
                combo.DataSource = data
                combo.ValueMember = SUConversiones.ConvierteAString(data.Columns(1))
                combo.DisplayMember = SUConversiones.ConvierteAString(data.Columns(0))
            End If
            combo.SelectedIndex = 0

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return combo
    End Function
    Public Function cargacombo_DNbar(ByVal sql As String, ByVal combo As DevComponents.DotNetBar.ComboBoxItem, Optional ByVal conta As Boolean = False) As DataSet
        Dim ds As DataSet = Nothing
        Dim conexion As SqlConnection = Nothing
        Dim comando As SqlCommand = Nothing
        Dim adaptador As SqlDataAdapter = Nothing
        Dim dataSet As DataSet = Nothing
        Dim adapter As SqlDataAdapter = Nothing
        Try
            ds = New DataSet
            If conta = True Then
                conexion = New SqlConnection(RNConeccion.ObtieneCadenaConexion())

            End If

            comando = New SqlCommand(sql, conexion)
            adaptador = New SqlDataAdapter

            dataSet = New DataSet
            adapter = New SqlDataAdapter(sql, conexion)
            adapter.Fill(dataSet)
            combo.Items.Add(dataSet.Tables(0).Columns(0))

            conexion.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return dataSet

    End Function

    Public Function carga_combo_ano_DN(ByVal combo As Windows.Forms.ComboBox, ByVal anos As Integer) As ComboBox
        Dim ano_anterior As Int32 = 0
        Dim ano_fin As Int32 = 0

        Try
            ano_anterior = Now.Year - anos
            ano_fin = Now.Year + anos
            For i As Integer = ano_anterior To ano_fin
                combo.Items.Add(New DevComponents.Editors.ComboItem(ano_anterior, Color.Black, Color.SteelBlue))
                ano_anterior += 1
            Next

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try

        Return combo
    End Function

    Public Function cargaGrid(ByRef Consulta As String, Optional ByRef conta As Boolean = False)
        cargaGrid = True
        Dim Datos As DataTable = Nothing
        Try
            Datos = New DataTable
            Retornadatatable(Consulta, Datos, conta)
            If Datos.Rows.Count = 0 Then
                cargaGrid = False
            End If
            Return Datos

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Function convierte_filtroReporte(ByVal sqlreporte As String, ByVal inner_join As Boolean, ByVal where As Boolean) As String
        Dim filtro As String = String.Empty
        Dim longi As Integer = 0
        Dim partes As Boolean
        REM 1*. Separamos los campos de la cadena desde el select  hasta antes del from 
        Dim campos_sql As String = String.Empty

        Try
            sqlreporte = sqlreporte.ToUpper
            sqlreporte = sqlreporte.Replace(" ", "")
            sqlreporte = sqlreporte.Replace("SELECT", "")
            sqlreporte = sqlreporte.Replace("SUBSTRING", "MID")
            For Each campo As String In sqlreporte.Split(",")
                REM. evaluamos la cadena, si contiene select se lo quitamos
                If campo.Contains("MID") Then
                    campo = Mid(campo, 5, Len(campo))
                    campo = campo.Replace(campo, "({" & campo & "}")
                    partes = True
                End If
                If partes = True Then
                    Dim i As Integer
                    If i < 3 Then
                        campo = campo & ","
                        i = 1 + i
                    Else
                        partes = False
                    End If

                End If
                campos_sql = campos_sql & campo

            Next
            Dim sssss As String = campos_sql
            If where = True Then

            End If
            If inner_join = True Then

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return filtro
    End Function

    Public Function DameSaldoiniCta(ByVal cuenta As String, ByVal Fecha_ini As Date, ByRef saldo_ini As Double, ByRef saldome_ini As Double, ByVal licencia As String) As Boolean
        Dim sql_cuenta As String = String.Empty
        Dim sql_reader_saldos As SqlDataReader = Nothing
        Try
            'sql_cuenta = " SELECT Sum(dasientos.debelocal-dasientos.haberlocal) AS Saldoinilocal, Sum(dasientos.debeext-dasientos.haberext) AS Saldoiniext"
            'sql_cuenta += " FROM dasientos WITH (NOLOCK) INNER JOIN masientos ON dasientos.anomes = masientos.anomes AND dasientos.numeasiento = masientos.numeasiento"
            'sql_cuenta += " WHERE dasientos.estado='V' and masientos.fechaasiento < '" & Format(Fecha_ini, "yyyyMMdd") & "' AND dasientos.cdgocuenta='" & Trim(cuenta) & "';"

            ' sql_reader_saldos = RetornaReader(sql_cuenta, True)
            sql_reader_saldos = RNCuentaContable.ObtenerSaldoInicialCuenta(Trim(cuenta), Format(Fecha_ini, "yyyyMMdd"))
            If sql_reader_saldos.HasRows Then
                With sql_reader_saldos.Read
                    If IsDBNull(sql_reader_saldos!saldoinilocal) = True Then
                        MntoCuenta_SI = 0
                        saldo_ini = 0
                    Else
                        MntoCuenta_SI = sql_reader_saldos!saldoinilocal
                        saldo_ini = sql_reader_saldos!saldoinilocal
                    End If


                    If IsDBNull(sql_reader_saldos!saldoiniext) = True Then
                        MntoCuenta_SIext = 0
                    Else
                        MntoCuenta_SIext = sql_reader_saldos!saldoiniext
                    End If

                End With
                sql_reader_saldos.Close()
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return False
        End Try

    End Function
    Public Function DameMoviCta(ByVal cuenta As String, ByVal Fecha_ini As Date, ByVal Fecha_fin As Date, ByRef debelocal As Double, ByRef haberlocal As Double, ByVal licencia As String) As Boolean

        Dim sql_cuenta As String = String.Empty
        Dim sql_reader_movi As SqlDataReader = Nothing
        Try
            'sql_cuenta = " SELECT Sum(dasientos.debelocal) debelocal,sum(dasientos.haberlocal) haberlocal"
            'sql_cuenta += " FROM dasientos WITH (NOLOCK) INNER JOIN masientos ON dasientos.anomes = masientos.anomes AND dasientos.numeasiento = masientos.numeasiento"
            'sql_cuenta += " WHERE dasientos.estado='V' and masientos.fechaasiento >= '" & Format(Fecha_ini, "yyyyMMdd") & "' AND masientos.fechaasiento <= '" & Format(Fecha_fin, "yyyyMMdd") & "' AND dasientos.cdgocuenta='" & Trim(cuenta) & "';"

            'sql_reader_movi = RetornaReader(sql_cuenta, True)
            sql_reader_movi = RNCuentaContable.ObtenerMovimientoCuenta(Trim(cuenta), Format(Fecha_ini, "yyyyMMdd"), Format(Fecha_fin, "yyyyMMdd"))
            If sql_reader_movi.HasRows Then
                With sql_reader_movi.Read
                    If IsDBNull(sql_reader_movi!debelocal) = True Then
                        debelocal = 0
                    Else
                        debelocal = sql_reader_movi!debelocal
                    End If
                    If IsDBNull(sql_reader_movi!haberlocal) = True Then
                        haberlocal = 0
                    Else
                        haberlocal = sql_reader_movi!haberlocal
                    End If

                End With
                sql_reader_movi.Close()
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try

    End Function
    Public Function retorna_simbolomoneda(ByVal moneda As Integer) As String
        Dim moneda_simbolo As String = String.Empty
        Try
            moneda_simbolo = Retorna_Campo("select simbolo  from mtipomoneda where codmoneda=" & moneda, True, True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return moneda_simbolo
    End Function
    Public Function retorna_descripcionmoneda(ByVal moneda As Integer) As String
        Dim moneda_Descripcion As String = String.Empty
        Try
            moneda_Descripcion = Retorna_Campo("select descmoneda  from mtipomoneda where codmoneda=" & moneda, True, True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return moneda_Descripcion
    End Function
End Class
