﻿Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Data.SqlClient

Public Module DataHelper
    Public Function LoadDataTable(ByVal consulta As String) As DataTable
        Dim dt As New DataTable()
        '''''''''''''''''''''''''''''''
        Dim conn As New SqlConnection
        Dim dReader As SqlDataReader = Nothing
        Try
            'conn.ConnectionString = ConnectionStringSIMF
            conn.ConnectionString = RNConeccion.ObtieneCadenaConexion()
            Dim cmd As New SqlCommand
            cmd.Connection = conn
            cmd.CommandType = CommandType.Text
            'cmd.CommandText = "select * from catalogoconta  order by cdgocuenta"
            cmd.CommandText = consulta
            conn.Open()
            Dim da As New SqlDataAdapter(cmd)

            da.Fill(dt)

            Return dt

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function LoadAutoComplete(ByVal sql As String, ByVal campo As String) As AutoCompleteStringCollection
        Dim dt As DataTable = LoadDataTable(sql)

        Dim stringCol As New AutoCompleteStringCollection()

        For Each row As DataRow In dt.Rows
            'stringCol.Add(Convert.ToString(row("cdgocuenta")))
            stringCol.Add(Convert.ToString(row(campo)))
        Next

        Return stringCol
    End Function
End Module
