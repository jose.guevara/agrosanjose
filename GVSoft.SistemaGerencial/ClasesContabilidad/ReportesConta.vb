﻿Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Data.SqlClient

Public Class ReportesConta
    Friend jackvalida As Valida
    Dim jackconsulta As New ConsultasDB
    REM Conexiones
    Public ConnectionStringSIMF_Conta As String = String.Empty
    Public Shared MntoCuenta_SI As Double = 0
    Public Shared MntoCuenta_SIext As Double = 0
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ReportesConta"
    Public Function Report_Damemonto_form(ByVal cdgo_reporte As String, ByVal bEnMiles As Boolean, ByVal CambiaSigno As Boolean, ByVal wtipoSaldo As String) As Double
        Dim wResultado As Double = 0
        Dim i As Integer, x1 As Integer
        Dim SQL As String = String.Empty
        Dim wSumat As String = String.Empty
        Dim wCta As String = String.Empty
        Dim wCampo As String = String.Empty

        '  Dim rsa As New ADODB.Recordset
        wCampo = "saldofin"
        Try
            Select Case wtipoSaldo
                Case "SF"
                    wCampo = "saldofinlocal"
                Case "SI"
                    wCampo = "saldoinilocal"
                Case "DB"
                    wCampo = "debelocal"
                Case "HB"
                    wCampo = "haberlocal"
                Case "MV"
                    wCampo = "debelocal-haberlocal"
            End Select

            wSumat = cdgo_reporte.Trim
            wSumat = wSumat.Replace(" ", "")
            wResultado = 0
            For i = 1 To cdgo_reporte.Trim.Length
otro:
                If Mid(wSumat, i, 1) = "(" Then
                    x1 = InStr(wSumat, ")")
                    wCta = Mid(wSumat, i + 1, x1 - 2)
                    SQL = "SELECT  SUM(" & wCampo & ") AS Result FROM dbo.catalogoconta WITH (NOLOCK) WHERE dbo.catalogoconta.tipo='2' GROUP BY SUBSTRING(cdgocuenta,1," & Len(wCta) & ") HAVING SUBSTRING(cdgocuenta,1," & Len(wCta) & ")='" & wCta & "';"
                    REM debo validar si existe o no cuentas de detalle

                    Dim mnto_campo As Decimal = Retorna_valorCampo(SQL, "Result", True, True)

                    wResultado = wResultado + mnto_campo
                    'End If
                    wSumat = Mid(wSumat, x1 + 1, Len(wSumat))
                    If wSumat = "" Then GoTo salida Else GoTo otro
                End If
            Next
salida:
            Report_Damemonto_form = wResultado
            If CambiaSigno = True Then Report_Damemonto_form = Report_Damemonto_form * -1

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function Retorna_valorCampo(ByVal sql As String, ByVal campo As String, ByVal conta As Boolean, Optional ByVal Nada As Boolean = False) As Decimal
        Dim sql_campo As SqlDataReader = Nothing
        Dim campo_dato As String = String.Empty
        Try
            sql_campo = jackconsulta.RetornaReader(sql, True)
            If sql_campo.HasRows Then

                While sql_campo.Read
                    Select Case campo
                        Case campo
                            campo_dato = sql_campo!Result

                    End Select

                End While
                sql_campo.Close()
            Else
                If Nada = True Then
                    campo_dato = 0
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return campo_dato

    End Function
End Class
