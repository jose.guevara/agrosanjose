﻿Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports CrystalDecisions.Windows.Forms

Public Class crystal_jack
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "crystal_jack"
    Public Shared lista_parametros As ParameterFields

    Public Function carga_parametros1(ByVal parametro As String, ByVal nparametro As Integer, ByVal valor As String) As ParameterFields
        Dim param As ParameterField = Nothing
        Dim discreteValue As ParameterDiscreteValue = Nothing
        Dim paramFiels As ParameterFields = Nothing
        Try
            param = New ParameterField()
            param.ParameterFieldName = parametro

            discreteValue = New ParameterDiscreteValue()
            discreteValue.Value = valor
            param.CurrentValues.Add(discreteValue)

            ' Asigno el paramametro a la coleccion
            '
            paramFiels = New ParameterFields()

            paramFiels.Add(param)
            lista_parametros = paramFiels
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return paramFiels

    End Function
    Public Sub prueba(ByVal campos As String, ByVal valor As String)
        Dim crViewer As CrystalReportViewer = Nothing
        Dim crParameterFields As ParameterFields = Nothing
        Dim crParameterField As ParameterField = Nothing
        Dim crParameterDiscreteValue As ParameterDiscreteValue = Nothing

        Try
            crParameterFields = New ParameterFields()
            crParameterField = New ParameterField()
            crParameterField.ParameterFieldName = "NOMBRE_DEL_CAMPO"
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = "VALOR_DEL_PARAMETRO"
            crParameterField.CurrentValues.Add(crParameterDiscreteValue)
            crParameterFields.Add(crParameterField)

            crViewer.ParameterFieldInfo = crParameterFields
            ' crViewer.ReportSource = objReportDocument
            crViewer.Refresh()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Function retorna_parametro(ByRef nombreformula As String, ByRef valor As String, ByVal numepara As Integer) As Boolean
        Dim Nlen As Integer = 0
        Dim nom_parametro As String = String.Empty
        Dim discreteValue As ParameterDiscreteValue = Nothing
        Try
            discreteValue = New ParameterDiscreteValue()
            Nlen = InStr(SIMF_CONTA.formula(numepara), ",")
            If Nlen = 0 Then
                nom_parametro = ""
            Else
                nom_parametro = Mid(SIMF_CONTA.formula(numepara), 1, Nlen - 1)
                nom_parametro = Trim(nom_parametro)
                nombreformula = nom_parametro

            End If


            If Len(nom_parametro) > 0 Then
                valor = Mid(SIMF_CONTA.formula(numepara), Nlen + 1, Len(SIMF_CONTA.formula(numepara)) - Nlen)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return True
    End Function
    Public Function carga_parametros(ByVal nparametro As Integer) As ParameterFields
        Dim param As ParameterField = Nothing
        Dim paramFiels As ParameterFields = Nothing
        Dim report As ReportDocument = Nothing
        Dim i As Integer = 0
        Dim Nlen As Integer = 0
        Dim nom_parametro As String = String.Empty
        Dim discreteValue As ParameterDiscreteValue = Nothing
        Try
            param = New ParameterField()
            paramFiels = New ParameterFields()
            report = New ReportDocument
            For i = 0 To nparametro - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    If Len(Trim(SIMF_CONTA.formula(i))) > 0 Then
                        Nlen = 0
                        nom_parametro = String.Empty
                        Nlen = InStr(SIMF_CONTA.formula(i), "=")
                        If Nlen = 0 Then
                            nom_parametro = ""
                        Else
                            nom_parametro = Mid(SIMF_CONTA.formula(i), 1, Nlen - 1)
                            nom_parametro = Trim(nom_parametro)
                            param.ParameterFieldName = nom_parametro

                        End If
                        discreteValue = New ParameterDiscreteValue()
                        If Len(nom_parametro) > 0 Then
                            discreteValue.Value = Mid(SIMF_CONTA.formula(i), Nlen + 1, Len(SIMF_CONTA.formula(i)) - Nlen)
                            param.CurrentValues.Add(discreteValue)
                            'report.RecordSelectionFormula = "{" & nom_parametro & "}=" & discreteValue.Value
                            ' Asigno el paramametro a la coleccion
                            paramFiels.Add(param)
                            ' Asigno el paramametro a la coleccion
                            '
                        End If
                    End If
                End If
            Next
            lista_parametros = paramFiels
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return paramFiels

    End Function

    Public Function carga_formulas(ByVal nparametro As Integer) As FormulaFieldDefinitions
        Dim param As FormulaFieldDefinition = Nothing
        Dim paramFiels As FormulaFieldDefinitions = Nothing

        Return paramFiels
    End Function
    Public Function borra_formulas_reporte() As Boolean
        Dim i As Integer
        For i = 0 To 14
            SIMF_CONTA.formula(i) = String.Empty
        Next
        Return True
    End Function
End Class
