﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class Valida
    Friend jack As New ConsultasDB
    Public msg As String = String.Empty
    Dim longitud_grupo As Integer = 0
    Dim Nivel_cadena As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Valida"


    Public Function existe_cuenta(ByVal cuenta As String) As Boolean
        Dim sql_existecuenta As String = String.Empty
        Dim registros As SqlDataReader = Nothing
        Try
            sql_existecuenta = "select cdgocuenta from CatalogoConta where cdgocuenta='" & cuenta & "'"
            registros = jack.RetornaReader(sql_existecuenta, True)
            If registros.HasRows = False Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function DameTipo_cuenta(ByVal cuenta As String) As String
        Dim sql_tipocuenta As String = String.Empty
        Dim tipo As String = String.Empty
        Try
            sql_tipocuenta = "select tipo from CatalogoConta where cdgocuenta='" & cuenta & "'"
            tipo = jack.Retorna_Campo(sql_tipocuenta, True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return tipo
    End Function
    Public Function formato_cuenta(ByVal cuenta As String, Optional ByVal separador_extra As Boolean = False) As Boolean
        Dim sql_formatocuenta As String = String.Empty
        Dim formato As SqlDataReader = Nothing
        Dim grupo As Integer = 0
        Dim separador As String = String.Empty
        Dim lon_Cuentaconfig As String = String.Empty
        Try
            sql_formatocuenta = "select * from MconfiCuenta"
            formato = jack.RetornaReader(sql_formatocuenta, True)
            If formato.HasRows = True Then
                While formato.Read

                    grupo = 0
                    separador = (formato!separador).ToString.Trim
                    lon_Cuentaconfig = formato!longitud
                    If lon_Cuentaconfig < Len(cuenta) Then
                        msg = "La Longitud de la nueva Cuenta es Incorrecta, Excede la Longitud Máxima establecida de " & lon_Cuentaconfig
                        Return False
                        Exit Function
                    Else

                        If separador_extra = True Then
                            cuenta = cuenta & separador
                        End If
                        While cuenta <> ""
                            Dim iteracion As Integer = 1
                            Dim cdgotran_deduVarias As String
                            Dim x As Integer

                            x = InStr(cuenta, separador)
                            If separador = Mid(cuenta, x, 1) Then
                                cdgotran_deduVarias = Mid(cuenta, 1, x)
                                cdgotran_deduVarias = cdgotran_deduVarias.Replace(separador, " ")
                                REM evalamos la longitud 
                                grupo = grupo + 1
                                Damegrupo_lon(grupo, sql_formatocuenta)
                                If Len(cdgotran_deduVarias.Trim) = longitud_grupo Then
                                Else
                                    msg = "Longitud del Grupo Incorrecto, verifique que sea Longitud " & longitud_grupo & " para el Grupo " & grupo
                                    Return False
                                    Exit Function
                                End If
                                cuenta = Mid(cuenta, x + 1, cuenta.Length - 1)
                            Else
                                msg = "Separador de la Cuenta Incorrecto, verifique que sea (" & separador & ")"
                                Return False
                                Exit Function
                            End If
                        End While

                    End If



                End While
                formato.Close()
            Else
                msg = "No se puede Validar al Cuenta, Comuníquese con el Departamento de Tecnología(TABLA-MconfiCuenta)"
                Return False

                Exit Function
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return True
    End Function

    Sub Damegrupo_lon(ByVal grupo As Integer, ByVal sql As String)
        Dim sql_lon As SqlDataReader = Nothing
        Try
            sql_lon = jack.RetornaReader(sql, True)
            If sql_lon.HasRows = True Then
                While sql_lon.Read
                    Select Case grupo
                        Case "1"
                            longitud_grupo = sql_lon!longrupo1
                        Case "2"
                            longitud_grupo = sql_lon!longrupo2
                        Case "3"
                            longitud_grupo = sql_lon!longrupo3
                        Case "4"
                            longitud_grupo = sql_lon!longrupo4
                        Case "5"
                            longitud_grupo = sql_lon!longrupo5
                        Case "6"
                            longitud_grupo = sql_lon!longrupo6
                        Case "7"
                            longitud_grupo = sql_lon!longrupo7
                        Case "8"
                            longitud_grupo = sql_lon!longrupo8
                        Case "9"
                            longitud_grupo = sql_lon!longrupo9
                        Case "10"
                            longitud_grupo = sql_lon!longrupo10
                    End Select
                End While
                sql_lon.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Function DameNiveles_Cuenta(ByVal nivel As Integer) As String
        Dim sql_Nivel As SqlDataReader = Nothing
        Try
            sql_Nivel = jack.RetornaReader("select * from MconfiCuenta", True)
            If sql_Nivel.HasRows = True Then
                While sql_Nivel.Read
                    Select Case nivel

                        Case "1"
                            If IsDBNull(sql_Nivel!midnivel1) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel1
                            End If
                        Case "2"
                            If IsDBNull(sql_Nivel!midnivel2) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel2
                            End If
                        Case "3"
                            If IsDBNull(sql_Nivel!midnivel3) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel3
                            End If
                        Case "4"
                            If IsDBNull(sql_Nivel!midnivel4) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel4
                            End If
                        Case "5"
                            If IsDBNull(sql_Nivel!midnivel5) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel5
                            End If

                        Case "6"
                            If IsDBNull(sql_Nivel!midnivel6) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel6
                            End If
                        Case "7"
                            If IsDBNull(sql_Nivel!midnivel7) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel7
                            End If
                        Case "8"
                            If IsDBNull(sql_Nivel!midnivel8) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel8
                            End If
                        Case "9"
                            If IsDBNull(sql_Nivel!midnivel9) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel9
                            End If
                        Case "10"
                            If IsDBNull(sql_Nivel!midnivel10) = True Then
                                Nivel_cadena = 0
                            Else
                                Nivel_cadena = sql_Nivel!midnivel10
                            End If
                    End Select
                End While
                sql_Nivel.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return Nivel_cadena
    End Function

    Public Function mascara_cuenta() As String
        Dim sql_mascaracuenta As String = "select * from MconfiCuenta"
        Dim i As Integer = 0
        Dim mascara As String = String.Empty
        Dim formato_mascara As SqlDataReader = Nothing
        Dim grupos As Integer = 0
        Dim separadores As String = String.Empty
        Try
            mascara_cuenta = String.Empty
            formato_mascara = jack.RetornaReader(sql_mascaracuenta, True)
            grupos = formato_mascara!numegrupos
            separadores = formato_mascara!separador
            'For i = 1 To grupos
            '    'falta, lo dejo para luego.
            'Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return mascara_cuenta
    End Function
    Public Sub DameCierre(ByRef Fecha_Cierre As Date, ByRef Fecha_ini As Date, ByRef Fecha_fin As Date)
        ' 1.* Validar si existe la Tabla, si no existe crearla e insertarle 2 registros( 1 pre-cierre, 2Cierre Anual)
        ' 2.* Validar si la tabla tiene los registros Maestros, si no los tiene Insertarlos ( 1 pre-cierre, 2Cierre Anual)
        Dim sql_cierres As String = String.Empty
        Dim cierre As SqlDataReader = Nothing
        Try
            sql_cierres = "select * from Mcierres"
            cierre = jack.RetornaReader(sql_cierres, True)
            While cierre.Read
                Fecha_Cierre = cierre!Fecha_Cierre
                Fecha_ini = cierre!Fecha_ini
                Fecha_fin = cierre!Fecha_fin

            End While
            cierre.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    Public Function DameMescerrado(ByRef anomes As Integer, ByRef conta As Boolean) As Boolean

        Dim sql_mescerrado As String = String.Empty
        Dim cerrado As Boolean = False
        Try
            sql_mescerrado = "select cerrado from Dcierre_Mes where anomes=" & anomes
            cerrado = jack.Retorna_Campo(sql_mescerrado, True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return cerrado
    End Function


    Public Function existeTabla(ByVal tabla As String, Optional ByVal conta As Boolean = False) As Integer

        Dim csb As SqlConnectionStringBuilder = Nothing

        Dim conexion As SqlConnection = Nothing

        Dim sCmd As String = String.Empty
        Dim n As Integer = 0
        Try
            csb = New SqlConnectionStringBuilder()
            If conta = True Then
                conexion = New SqlConnection(ConnectionStringSIMF)
            End If

            sCmd =
            "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES " &
            "WHERE TABLE_TYPE = 'BASE TABLE' " &
            "AND TABLE_NAME = @tabla"
            Using con As New SqlConnection(conexion.ConnectionString)

                con.Open()

                Dim cmd As New SqlCommand(sCmd, con)
                cmd.Parameters.AddWithValue("@tabla", tabla)

                ' Comprobamos si está
                ' Devuelve 1 si ya existe
                ' o 0 si no existe
                n = 0
                n = SUConversiones.ConvierteAInt(cmd.ExecuteScalar())

                con.Close()
                Return n
            End Using
        Catch
            Return False
        End Try
    End Function
    Public Function carga_nivel(ByVal nivel As Integer) As String
        Dim subcadena As String = String.Empty
        Dim sql_saldos_Nivel As String = String.Empty
        Dim sql_procedure As String = String.Empty
        Try
            subcadena = DameNiveles_Cuenta(nivel)
            If subcadena <> 0 Then

                sql_saldos_Nivel = "select substring(CatalogoConta.cdgocuenta," & subcadena & ") as 'cuenta',CataNivel" & nivel & ".desccuenta as 'descuenta',sum(saldoinilocal) as 'saldoinilocal',"
                sql_saldos_Nivel += " SUM(debelocal)  as 'debelocal',SUM(haberlocal) as 'haberlocal',SUM(saldofinlocal) as 'saldofinlocal'"
                ' sql_saldos_Nivel += ",SUM(saldoiniext) as 'saldoiniext',SUM(debeext) as 'debeext',SUM(haberext) 'haberext',SUM(saldofinext) 'saldofinext' "
                sql_saldos_Nivel += " from catalogoconta  inner join CataNivel" & nivel & " on substring(CatalogoConta.cdgocuenta," & subcadena & ")=CataNivel" & nivel & ".cdgocuenta "
                sql_saldos_Nivel += " where tipo = 2"
                sql_saldos_Nivel += " group by substring(CatalogoConta.cdgocuenta," & subcadena & "),CataNivel" & nivel & ".desccuenta "
                sql_saldos_Nivel += "  having not (sum(saldoinilocal)=0 AND SUM(debelocal)=0 AND SUM(haberlocal)=0 AND SUM(saldofinlocal)=0 AND SUM(saldoiniext)=0 and SUM(debeext)=0 AND SUM(haberext)=0 AND SUM(saldofinext)=0)"
                sql_saldos_Nivel += " order by substring(CatalogoConta.cdgocuenta," & subcadena & ")"


                sql_procedure = ""

                sql_procedure = sql_saldos_Nivel
                sql_procedure = sql_procedure.Replace("from", " into saldos_reporte_tmp from ")
                'End If
                If existeTabla("saldos_reporte_tmp", True) = 1 Then
                    sql_procedure = " Drop Table saldos_reporte_tmp;" & sql_procedure
                End If
                jack.EjecutaSQL(sql_procedure, True)
                Return sql_saldos_Nivel
            Else
                Return 1
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function formato_moneda(ByVal texto As String, Optional ByVal formato As String = "###,###,###.##") As String
        Dim valor As String = ""
        If texto = "" Then
            texto = 0
        End If

        valor = Format(CDbl(texto), formato)
        If valor = "" Then
            valor = 0
        End If

        Return valor

    End Function
    Public Function RetornaNivel_Cuenta(ByVal longitud As Integer) As String
        Dim Mid_Cuenta As String = String.Empty
        Dim sql_Nivel As SqlDataReader = Nothing
        Try
            RetornaNivel_Cuenta = String.Empty
            Mid_Cuenta = "1," & longitud
            sql_Nivel = jack.RetornaReader("select * from MconfiCuenta", True)

            If sql_Nivel.HasRows = True Then
                While sql_Nivel.Read
                    If IsDBNull(sql_Nivel!midnivel1) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel1 Then
                            RetornaNivel_Cuenta = 1
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel2) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel2 Then
                            RetornaNivel_Cuenta = 2
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel3) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel3 Then
                            RetornaNivel_Cuenta = 3
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel4) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel4 Then
                            RetornaNivel_Cuenta = 4
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel5) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel5 Then
                            RetornaNivel_Cuenta = 5
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel6) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel6 Then
                            RetornaNivel_Cuenta = 6
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel7) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel7 Then
                            RetornaNivel_Cuenta = 7
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel8) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel8 Then
                            RetornaNivel_Cuenta = 8
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel9) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel9 Then
                            RetornaNivel_Cuenta = 9
                        End If
                    End If
                    If IsDBNull(sql_Nivel!midnivel10) = False Then
                        If Mid_Cuenta = sql_Nivel!midnivel10 Then
                            RetornaNivel_Cuenta = 10
                        End If
                    End If

                End While
                sql_Nivel.Close()

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function Inserta_CtaNIvel(ByVal cta As String, ByVal descc As String, ByVal nivel As String) As Boolean
        Dim sql_insertaNivel As String = String.Empty
        Try
            sql_insertaNivel = "INSERT INTO CATANIVEL" & nivel & "(cdgocuenta,desccuenta) values('" & cta & "','" & descc.Trim & "')"
            If jack.EjecutaSQL(sql_insertaNivel, True) = -1 Then
                Return False
                Exit Function
            End If

            Return True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return False
        End Try

    End Function
End Class
