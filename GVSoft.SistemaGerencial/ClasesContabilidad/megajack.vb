﻿Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Entidades

Public Class megajack
    Public conex As String = String.Empty
    Public conex_temp As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "megajack"

    Public Shared Function ini() As String

        Dim ConnectionString As String = String.Empty
        ini = String.Empty
        Try
            ConnectionString = RNConeccion.ObtieneCadenaConexion()
            ini = ConnectionString
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return ConnectionString
    End Function
    Public Shared Sub conexiones(ByRef clavecompania As String)
        Try
            ConnectionStringSIMF = RNConeccion.ObtieneCadenaConexion()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Function DameCia(ByRef cian() As String, ByRef lic() As String) As Boolean

        DameCia = False
        Dim ConnectionString As String = String.Empty
        Dim i As Short
        Try
            ConnectionString = ini()
            conex_temp = ini()
cargaCias:
            Dim sql As String = String.Empty
            sql = "select * from cias"

            Using connection As New Data.SqlClient.SqlConnection(ConnectionString)
                Dim command As New Data.SqlClient.SqlCommand(sql, connection)
                connection.Open()
                Dim reader As Data.SqlClient.SqlDataReader = command.ExecuteReader
                Do While reader.Read()
                    cian(i) = Trim(reader.Item("desccompania"))
                    lic(i) = Trim(reader.Item("clavecompania"))

                    i = i + 1
                    DameCia = True
                Loop
                reader.Close()
            End Using

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try

    End Function
End Class
