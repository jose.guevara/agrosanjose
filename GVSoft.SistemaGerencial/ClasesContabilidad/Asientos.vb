﻿Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Data.SqlClient

Public Class Asientos

    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Asientos"

    Dim jackconsulta As New ConsultasDB
    Dim jackvalida As New Valida

    Public anomes As Integer
    Public numemov As Integer
    Public numeasientos As String
    Public nlinea As Integer
    Public desclinea As String
    Public debelocal As Double
    Public haberlocal As Double
    Public debeext As Double
    Public haberext As Double
    Public tipocambio As Double
    Public cdgocuenta As String
    Public numedoc As String
    Public cdgocuenta_ant As String
    Public desccuenta As String

    Public anomes_tipomov As Integer
    Public numemov_tipomov As Integer
    Public numeasiento_tipomov As String
    Public numedoc_tipomov As String
    Public nlinea_tipomov As Integer
    Public tipomov As String
    Public idasiento As String
    Public debelocal_tipomov As Double
    Public haberlocal_tipomov As Double
    Public aniomes_linea_tipomov As String
    Public FechaDocumento As String

    Public Function nueva_linea() As Integer
        Try
            If jackvalida.DameMescerrado(anomes, True) = True Then
                nueva_linea = 0
            Else
                nueva_linea = jackconsulta.EjecutaSQL(String.Format("EXEC nueva_linea @anomes='{0}',@numemov='{1}',@numeasientos='{2}',@numelinea=1", anomes, numemov, numeasientos), True)
                If nueva_linea = -1 Then
                    nueva_linea = 0
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
        Return nueva_linea
    End Function
    Public Function busca_asiento(ByVal numeasientos_existe As String, ByVal anomes_existe As Integer, Optional ByVal numemov_existe As String = "", Optional ByVal estado As String = "") As Data.SqlClient.SqlDataReader
        Dim sql_reader_busqueda As String = String.Empty
        Dim sql_buscarnumeasiento_reader As SqlDataReader = Nothing
        Try
            sql_reader_busqueda = "select numeasiento,fechaasiento,descasiento,debelocal,haberlocal,debeext,haberext,anomes,numemov from Masientos where numeasiento='" & numeasientos_existe.Trim & "' and anomes=" & anomes_existe
            If numemov_existe <> String.Empty Then
                sql_reader_busqueda += " and numemov=" & numeasientos_existe
            End If
            If estado <> String.Empty Then
                sql_reader_busqueda += " and estado='" & estado & "'"
            End If

            sql_buscarnumeasiento_reader = jackconsulta.RetornaReader(sql_reader_busqueda, True)
            If sql_buscarnumeasiento_reader.HasRows Then
                Return sql_buscarnumeasiento_reader
            Else
                Return Nothing
            End If
            sql_buscarnumeasiento_reader.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return Nothing
        End Try
    End Function
    Public Function inserta_masicon(ByVal anomes_nuevo_masientos As String, ByVal fechaasiento_nuevo_masientos As Date, ByVal numeasiento_nuevo_masientos As String, Optional ByVal numemov_nuevo_masicon As Integer = 0) As Boolean
        Dim sql_insert_masicon As String = String.Empty
        Try
            'sql_insert_masicon = "insert into masientos(numemov ,numeasiento,anomes,fechaasiento,descasiento,debelocal,haberlocal,debeext,haberext,fechacrea,estado)"
            'sql_insert_masicon += " values('" & numemov_nuevo_masicon & "','" & numeasiento_nuevo_masientos & "','" & anomes_nuevo_masientos & "','" & Format(CDate(fechaasiento_nuevo_masientos), "yyyyMMdd") & "','',0,0,0,0,getdate(),'V') "
            sql_insert_masicon = "exec GrabarMasientos " & numemov_nuevo_masicon & ",'" & numeasiento_nuevo_masientos & "'," & anomes_nuevo_masientos & ",'" & Format(CDate(fechaasiento_nuevo_masientos), "yyyyMMdd") & "'"
            If jackconsulta.EjecutaSQL(sql_insert_masicon, True) = 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return False
        End Try

    End Function
    Public Function elimina_documento(ByVal numedoc As String, ByVal numeasiento As String, ByVal numemov As String, ByVal anomes As String, ByVal opcion_elimina As Boolean) As Boolean
        Dim estado_actualiza As String = String.Empty
        Try
            If opcion_elimina = True Then
                estado_actualiza = "N"
                If jackconsulta.EjecutaSQL("update dasientos set estado='" & estado_actualiza & "' where  numeasiento='" & numeasiento & "' and anomes=" & anomes & " and numemov=" & numemov & " and numedoc='" & numedoc & "'", True) = -1 Then
                    Return False
                Else
                    Return True
                End If
            Else
                estado_actualiza = "V"
                If jackconsulta.EjecutaSQL("update dasientos set estado='" & estado_actualiza & "' where  numeasiento='" & numeasiento & "' and anomes=" & anomes & " and numedoc='" & numedoc & "'", True) = -1 Then
                    Return False
                Else
                    Return True
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return False
        End Try
    End Function
    Public Function elimina_linea() As Boolean
        Dim i As Integer = 0
        Dim Max_linea As Integer = 0
        Try
            Max_linea = jackconsulta.Retorna_Campo("select max(nlinea) from dasientos where  numeasiento='" & numeasientos & "' and anomes=" & anomes & " and numemov=" & numemov, True)
            If jackconsulta.EjecutaSQL("Delete  dasientos where  numeasiento='" & numeasientos & "' and anomes=" & anomes & " and numemov=" & numemov & "  and nlinea=" & nlinea, True) = -1 Then
                Return False
            Else
                For i = nlinea To Max_linea - 1
                    jackconsulta.EjecutaSQL("update dasientos set nlinea=" & i & " where  numeasiento='" & numeasientos & "' and anomes=" & anomes & " and nlinea=" & i + 1, True)
                Next
                Return True
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function actualizar_masientos() As String
        ' Dim ejecuta_resumen As String = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", anomes, numemov_select, TextF_numecompro_detalle.Text), True)
        Return String.Empty
    End Function
    Public Function elimina_asiento(Optional ByVal eliminar As Boolean = True) As Boolean
        Dim sql_EliminaAsiento_Masientos As String = String.Empty
        Dim sql_EliminaAsiento_dasientos As String = String.Empty

        Try
            sql_EliminaAsiento_Masientos = "update masientos set  descasiento=descasiento+ '(Asiento Anulado)' where numeasiento='" & numeasientos & "' and anomes=" & anomes & " and numemov=" & numemov
            sql_EliminaAsiento_dasientos = "update dasientos set  desclinea=desclinea+ '(Asiento Anulado)'"
            If eliminar = True Then
                sql_EliminaAsiento_dasientos += " ,debelocal=0,haberlocal=0,debeext=0,haberext=0"
            End If
            sql_EliminaAsiento_dasientos += "  where numeasiento='" & numeasientos & "' and anomes=" & anomes & " and numemov=" & numemov
            If jackconsulta.EjecutaSQL(sql_EliminaAsiento_dasientos, True) = -1 Then
                Return False
            Else
                If jackconsulta.EjecutaSQL(sql_EliminaAsiento_Masientos, True) = -1 Then
                    Return False
                Else
                    jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", anomes, numemov, numeasientos), True)
                    jackconsulta.EjecutaSQL("update masientos set estado='N' where numeasiento='" & numeasientos & "' and anomes=" & anomes & " and numemov=" & numemov, True)
                    jackconsulta.EjecutaSQL("update dasientos set estado='N' where numeasiento='" & numeasientos & "' and anomes=" & anomes & " and numemov=" & numemov, True)
                    Return True
                End If

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Function genera_preasiento_soli(ByVal numesoli As String) As Boolean
        Dim sql_numesoli_reader As SqlDataReader = Nothing
        Dim mntpago As Double = 0
        Dim mntock As Double = 0
        Dim mntoimpuesto As Double = 0
        Dim cdgocuenta_impuesto As String = String.Empty
        Dim cdgocuenta_chequera As String = String.Empty
        Try
            sql_numesoli_reader = jackconsulta.RetornaReader("select * from solicitudes where numesoli=" & numesoli, True)
            REM numesoli,anomes,benef,concepto,monedasoli,fechasoli,mpago,mretencion,mdeduccion,mimpuesto,mcheque,cta_retencion,cta_deduccion,cta_impuesto,estado_soli,numecheque,cdgobanco,idchequera,cuentachequera"
            If sql_numesoli_reader.HasRows Then
                While sql_numesoli_reader.Read


                End While
                sql_numesoli_reader.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
            Return False
        End Try
        Return True
    End Function

    Public Function existe_asientos_descuadrados(ByVal anomes As String) As Boolean
        Dim sql_buscarinconsistencias As String = String.Empty
        Dim datos_cantidad As DataTable = Nothing

        Try
            datos_cantidad = New DataTable
            sql_buscarinconsistencias = "select M.numeasiento 'No.Asiento',M.fechaasiento 'Fecha Asiento',M.descasiento 'Descripción Asiento',M.debelocal 'Débito',M.haberlocal 'Crédito',M.anomes 'Año-Mes',M.numemov 'No.Movimiento',(debelocal)-(haberlocal) Descuadre from Masientos M where ((debelocal)-(haberlocal))<>0 and M.anomes=" & anomes
            datos_cantidad = jackconsulta.cargaGrid(sql_buscarinconsistencias, True)

            If datos_cantidad.Rows.Count > 0 Then
                Return True
            Else
                Return False

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            Throw ex
        End Try

    End Function
End Class
