﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports DevComponents
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class SIMF_CONTA
    Public Shared server As String
    Public Shared pws As String
    Public Shared base As String
    Public Shared usuario As String
    Public Shared ConnectionStringconta As String = RNConeccion.ObtieneCadenaConexion()
    Public Shared ReportConnectionStringconta As String
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "SIMF_CONTA"

    Public Shared datasetlleno As DataSet
    'Parámetros de longitud de niveles

    'Parámetros para reportes
    Public Shared nombre_reporte As String
    Public Shared filtro_reporte As String
    Public Shared reporte_ubica As String
    Public Shared formula(15) As String
    Public Shared numeformulas As Integer
    Public Shared sql_reporte As String

    'Parámetros Compañ
    Public Shared nombre_cia As String
    Public licencia_cia As String
    Public nume_cia As String


    'Parámetros de inicio de Sesión del Usuario
    Dim usersesion As Integer

    'Dim jack As New MegasysJack.megajack
    Friend jackconsulta As New ConsultasDB
    Friend jackvalida As New Valida
    'Dim jackmensaje As New MegasysJack.Mensaje

    Public Sub cargainicio(ByRef clavecompania As String)
        ' On Error Resume Next
        Dim connectionString As String = String.Empty
        Dim variables As SqlDataReader = Nothing
        Try
            ConnectionStringconta = RNConeccion.ObtieneCadenaConexion()
            jackconsulta.ConnectionStringSIMF_Conta = RNConeccion.ObtieneCadenaConexion()

            variables = jackconsulta.RetornaReader("select * from cias where clavecompania='" & Trim(clavecompania) & "'", True)
            If variables Is Nothing Then
                Exit Sub
            End If

            If variables.HasRows Then
                While variables.Read()
                    server = variables!server
                    base = variables!bdatos
                    usuario = variables!xusuario
                    pws = variables!pass
                    reporte_ubica = variables!reportes

                    nombre_cia = variables!desccompania
                    nume_cia = variables!numecompania
                    licencia_cia = variables!clavecompania
                    ReportConnectionStringconta = usuario & "," & pws & "," & server & "," & base
                End While
                variables.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("carga_nivel - Error " & ex.Message, " Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
        End Try
    End Sub

    Public Function carga_nivel(ByVal nivel As Integer) As String

        Dim subcadena As String = String.Empty
        Dim sql_saldos_Nivel As String = String.Empty
        Dim sql_procedure As String = String.Empty
        Try
            subcadena = jackvalida.DameNiveles_Cuenta(nivel)
            If subcadena <> 0 Then
                sql_saldos_Nivel = String.Empty
                sql_saldos_Nivel = "select substring(CatalogoConta.cdgocuenta," & subcadena & ") as 'cuenta',CataNivel" & nivel & ".desccuenta as 'descuenta',sum(saldoinilocal) as 'saldoinilocal',"
                sql_saldos_Nivel += " SUM(debelocal)  as 'debelocal',SUM(haberlocal) as 'haberlocal',SUM(saldofinlocal) as 'saldofinlocal',SUM(saldoiniext) as 'saldoiniext',SUM(debeext) as 'debeext',"
                sql_saldos_Nivel += "SUM(haberext) 'haberext',SUM(saldofinext) 'saldofinext' from catalogoconta  inner join CataNivel" & nivel
                sql_saldos_Nivel += " on substring(CatalogoConta.cdgocuenta," & subcadena & ")=CataNivel" & nivel & ".cdgocuenta "
                sql_saldos_Nivel += " where tipo = 2"
                sql_saldos_Nivel += " group by substring(CatalogoConta.cdgocuenta," & subcadena & "),CataNivel" & nivel & ".desccuenta "
                sql_saldos_Nivel += "  having not (sum(saldoinilocal)=0 AND SUM(debelocal)=0 or SUM(haberlocal)=0 AND SUM(saldofinlocal)=0 AND SUM(saldoiniext)=0 and SUM(debeext)=0 AND SUM(haberext)=0 AND SUM(saldofinext)=0)"
                sql_saldos_Nivel += " order by substring(CatalogoConta.cdgocuenta," & subcadena & ")"


                sql_procedure = ""

                sql_procedure = sql_saldos_Nivel
                sql_procedure = sql_procedure.Replace("from", " into saldos_reporte_tmp from ")
                'End If
                If jackvalida.existeTabla("saldos_reporte_tmp", True) = 1 Then
                    sql_procedure = " Drop Table saldos_reporte_tmp;" & sql_procedure
                End If
                jackconsulta.EjecutaSQL(sql_procedure, True)
                Return sql_saldos_Nivel
            Else
                Return 1
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("carga_nivel - Error " & ex.Message, " Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw ex
            Return String.Empty
        End Try
    End Function
End Class
