Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class actRptProforma
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actRptProforma_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lsSignoMoneda As String = String.Empty

        objParametro = New SEParametroEmpresa()
        objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()

        lsSignoMoneda = RNMoneda.ObtieneSignoMoneda(objParametro.MonedaFacturaCredito)
        Label13.Text = " Sub Total " & lsSignoMoneda & ": "
        Label14.Text = " Imp. Venta " & lsSignoMoneda & ": "
        Label28.Text = " Retencion " & lsSignoMoneda & ": "
        Label32.Text = " Total " & lsSignoMoneda & ": "

        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        Label.Text = strNombEmpresa
        TextBox21.Text = Format(Now, "Long Date")
        TextBox23.Text = Format(Now, "Long Time")
        lblFechaVence.Text = lblFechaVence.Text & " " & Format(Now, "dd-MMM-yyyy")
        UbicarAgencia(lngRegUsuario)
        lblDireccionAgencia.Text = lblDireccionAgencia.Text & " " & sDireccionAgencia
        lblTelefonoAgencia.Text = lblTelefonoAgencia.Text & " " & sTelefonoAjencia

        lblSubTotal.Text = Format(lblSubTotal.Text, "#,##0.00")
        lblImpVentas.Text = Format(lblImpVentas.Text, "#,##0.00")
        lblRetencion.Text = Format(lblRetencion.Text, "#,##0.00")
        lblTotal.Text = Format(lblTotal.Text, "#,##0.00")
        Me.Document.Printer.PrinterName = ""
    End Sub

    Private Sub actRptProforma_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

    End Sub

    Private Sub GroupHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format
        ''lblFechaVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + CInt(txtDias.Text)), "dd-MMM-yyyy")
    End Sub
End Class
