Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmRptMovInvent
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

    Dim dtReportes As DataTable = Nothing
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmdReportes As DevComponents.DotNetBar.ButtonX
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Dim IdModulo As Integer


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ListBox16 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox15 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox14 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox13 As System.Windows.Forms.ListBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ListBox12 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox11 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox10 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox9 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ListBox18 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox17 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ListBox19 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox20 As System.Windows.Forms.ListBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ListBox21 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox22 As System.Windows.Forms.ListBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents cbReportes As DevComponents.DotNetBar.Controls.ComboBoxEx
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRptMovInvent))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ListBox21 = New System.Windows.Forms.ListBox()
        Me.ListBox22 = New System.Windows.Forms.ListBox()
        Me.ListBox19 = New System.Windows.Forms.ListBox()
        Me.ListBox20 = New System.Windows.Forms.ListBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ListBox18 = New System.Windows.Forms.ListBox()
        Me.ListBox17 = New System.Windows.Forms.ListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ListBox16 = New System.Windows.Forms.ListBox()
        Me.ListBox15 = New System.Windows.Forms.ListBox()
        Me.ListBox14 = New System.Windows.Forms.ListBox()
        Me.ListBox13 = New System.Windows.Forms.ListBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ListBox12 = New System.Windows.Forms.ListBox()
        Me.ListBox11 = New System.Windows.Forms.ListBox()
        Me.ListBox10 = New System.Windows.Forms.ListBox()
        Me.ListBox9 = New System.Windows.Forms.ListBox()
        Me.ListBox8 = New System.Windows.Forms.ListBox()
        Me.ListBox7 = New System.Windows.Forms.ListBox()
        Me.ListBox6 = New System.Windows.Forms.ListBox()
        Me.ListBox5 = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListBox4 = New System.Windows.Forms.ListBox()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.cbReportes = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmdReportes = New DevComponents.DotNetBar.ButtonX()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX()
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.ListBox21)
        Me.GroupBox1.Controls.Add(Me.ListBox22)
        Me.GroupBox1.Controls.Add(Me.ListBox19)
        Me.GroupBox1.Controls.Add(Me.ListBox20)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.ListBox18)
        Me.GroupBox1.Controls.Add(Me.ListBox17)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.ListBox16)
        Me.GroupBox1.Controls.Add(Me.ListBox15)
        Me.GroupBox1.Controls.Add(Me.ListBox14)
        Me.GroupBox1.Controls.Add(Me.ListBox13)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ListBox12)
        Me.GroupBox1.Controls.Add(Me.ListBox11)
        Me.GroupBox1.Controls.Add(Me.ListBox10)
        Me.GroupBox1.Controls.Add(Me.ListBox9)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 115)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(807, 332)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(296, 224)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 16)
        Me.Label13.TabIndex = 41
        Me.Label13.Text = "<F5> AYUDA"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox21
        '
        Me.ListBox21.Location = New System.Drawing.Point(520, 328)
        Me.ListBox21.Name = "ListBox21"
        Me.ListBox21.Size = New System.Drawing.Size(120, 17)
        Me.ListBox21.TabIndex = 39
        Me.ListBox21.Visible = False
        '
        'ListBox22
        '
        Me.ListBox22.Location = New System.Drawing.Point(672, 328)
        Me.ListBox22.Name = "ListBox22"
        Me.ListBox22.Size = New System.Drawing.Size(72, 17)
        Me.ListBox22.TabIndex = 38
        Me.ListBox22.Visible = False
        '
        'ListBox19
        '
        Me.ListBox19.HorizontalScrollbar = True
        Me.ListBox19.Location = New System.Drawing.Point(548, 200)
        Me.ListBox19.Name = "ListBox19"
        Me.ListBox19.Size = New System.Drawing.Size(144, 121)
        Me.ListBox19.TabIndex = 37
        '
        'ListBox20
        '
        Me.ListBox20.HorizontalScrollbar = True
        Me.ListBox20.Location = New System.Drawing.Point(700, 200)
        Me.ListBox20.Name = "ListBox20"
        Me.ListBox20.Size = New System.Drawing.Size(88, 121)
        Me.ListBox20.TabIndex = 36
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(553, 184)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(232, 16)
        Me.Label7.TabIndex = 35
        Me.Label7.Text = "Tipo de Movimientos"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox18
        '
        Me.ListBox18.Location = New System.Drawing.Point(416, 328)
        Me.ListBox18.Name = "ListBox18"
        Me.ListBox18.Size = New System.Drawing.Size(88, 17)
        Me.ListBox18.TabIndex = 34
        Me.ListBox18.Visible = False
        '
        'ListBox17
        '
        Me.ListBox17.HorizontalScrollbar = True
        Me.ListBox17.Location = New System.Drawing.Point(440, 200)
        Me.ListBox17.Name = "ListBox17"
        Me.ListBox17.Size = New System.Drawing.Size(102, 121)
        Me.ListBox17.TabIndex = 33
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(288, 200)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(144, 20)
        Me.TextBox1.TabIndex = 32
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(296, 184)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(224, 16)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Registro de los Art�culos"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox16
        '
        Me.ListBox16.Location = New System.Drawing.Point(160, 328)
        Me.ListBox16.Name = "ListBox16"
        Me.ListBox16.Size = New System.Drawing.Size(88, 17)
        Me.ListBox16.TabIndex = 23
        Me.ListBox16.Visible = False
        '
        'ListBox15
        '
        Me.ListBox15.Location = New System.Drawing.Point(8, 328)
        Me.ListBox15.Name = "ListBox15"
        Me.ListBox15.Size = New System.Drawing.Size(120, 17)
        Me.ListBox15.TabIndex = 22
        Me.ListBox15.Visible = False
        '
        'ListBox14
        '
        Me.ListBox14.HorizontalScrollbar = True
        Me.ListBox14.Location = New System.Drawing.Point(162, 200)
        Me.ListBox14.Name = "ListBox14"
        Me.ListBox14.Size = New System.Drawing.Size(119, 121)
        Me.ListBox14.TabIndex = 21
        '
        'ListBox13
        '
        Me.ListBox13.HorizontalScrollbar = True
        Me.ListBox13.Location = New System.Drawing.Point(8, 200)
        Me.ListBox13.Name = "ListBox13"
        Me.ListBox13.Size = New System.Drawing.Size(150, 121)
        Me.ListBox13.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(224, 16)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Registro de los Proveedores"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(556, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(224, 16)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Registro de las SubClases"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox12
        '
        Me.ListBox12.Location = New System.Drawing.Point(700, 160)
        Me.ListBox12.Name = "ListBox12"
        Me.ListBox12.Size = New System.Drawing.Size(80, 17)
        Me.ListBox12.TabIndex = 17
        Me.ListBox12.Visible = False
        '
        'ListBox11
        '
        Me.ListBox11.Location = New System.Drawing.Point(548, 160)
        Me.ListBox11.Name = "ListBox11"
        Me.ListBox11.Size = New System.Drawing.Size(120, 17)
        Me.ListBox11.TabIndex = 16
        Me.ListBox11.Visible = False
        '
        'ListBox10
        '
        Me.ListBox10.HorizontalScrollbar = True
        Me.ListBox10.Location = New System.Drawing.Point(700, 32)
        Me.ListBox10.Name = "ListBox10"
        Me.ListBox10.Size = New System.Drawing.Size(88, 121)
        Me.ListBox10.TabIndex = 15
        '
        'ListBox9
        '
        Me.ListBox9.HorizontalScrollbar = True
        Me.ListBox9.Location = New System.Drawing.Point(548, 32)
        Me.ListBox9.Name = "ListBox9"
        Me.ListBox9.Size = New System.Drawing.Size(144, 121)
        Me.ListBox9.TabIndex = 14
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(440, 160)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(88, 17)
        Me.ListBox8.TabIndex = 13
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(288, 160)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(120, 17)
        Me.ListBox7.TabIndex = 12
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(440, 32)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(102, 121)
        Me.ListBox6.TabIndex = 11
        '
        'ListBox5
        '
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(288, 32)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(144, 121)
        Me.ListBox5.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(293, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(232, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Registro de las Clases"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(224, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Registro de las Sucursales"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(160, 160)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(80, 17)
        Me.ListBox4.TabIndex = 3
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(8, 160)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(120, 17)
        Me.ListBox3.TabIndex = 2
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(162, 32)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(119, 121)
        Me.ListBox2.TabIndex = 1
        '
        'ListBox1
        '
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(8, 32)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(150, 121)
        Me.ListBox1.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 455)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(807, 48)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(378, 18)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox2.TabIndex = 37
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(180, 19)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 36
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(656, 16)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NumericUpDown1.Minimum = New Decimal(New Integer() {99999, 0, 0, -2147483648})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown1.TabIndex = 35
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(600, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 34
        Me.Label9.Text = "Cantidad"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(488, 16)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(80, 20)
        Me.TextBox2.TabIndex = 33
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(416, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 13)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Documento"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(272, 16)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker2.TabIndex = 8
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(72, 16)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker1.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(216, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Fec Final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Fec Inicial"
        '
        'Timer1
        '
        '
        'cbReportes
        '
        Me.cbReportes.DisplayMember = "Text"
        Me.cbReportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReportes.FormattingEnabled = True
        Me.cbReportes.ItemHeight = 15
        Me.cbReportes.Location = New System.Drawing.Point(69, 77)
        Me.cbReportes.Name = "cbReportes"
        Me.cbReportes.Size = New System.Drawing.Size(324, 21)
        Me.cbReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbReportes.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(5, 80)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 13)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Reportes"
        '
        'cmdReportes
        '
        Me.cmdReportes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdReportes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdReportes.Image = CType(resources.GetObject("cmdReportes.Image"), System.Drawing.Image)
        Me.cmdReportes.ImageFixedSize = New System.Drawing.Size(30, 30)
        Me.cmdReportes.Location = New System.Drawing.Point(396, 69)
        Me.cmdReportes.Name = "cmdReportes"
        Me.cmdReportes.Size = New System.Drawing.Size(55, 36)
        Me.cmdReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdReportes.TabIndex = 31
        Me.cmdReportes.Tooltip = "<b><font color=""#17365D"">Generar Reporte</font></b>"
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.cmdExportar)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(815, 55)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 32
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(75, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmdExportar
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.FontBold = True
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.FontBold = True
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmRptMovInvent
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(815, 515)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.cmdReportes)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cbReportes)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRptMovInvent"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes del Inventario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmRptMovInvent"
    Private Sub frmRptMovInvent_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Top = 0
        'Me.Left = 0
        'Me.StartPosition = FormStartPosition.CenterScreen
        Try
            blnClear = False
            Iniciar()
            Limpiar()
            'CargarComboReportes()
            RNReportes.CargarComboReportes(cbReportes, nIdUsuario, nIdModulo)
            If intAccesos(99) = 0 Then
                'MenuItem1.Visible = False
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmRptMovInvent_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F5 Then
                If Label13.Visible = True Then
                    intListadoAyuda = 3
                    Dim frmNew As New frmListadoAyuda
                    Timer1.Interval = 200
                    Timer1.Enabled = True
                    frmNew.ShowDialog()
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub Iniciar()
        Try

            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Agencias Where Estado <> 2 order by Registro"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox1.Items.Add(dtrAgro2K.GetValue(1))
                ListBox3.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Clases Where Estado <> 2 order by Registro"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox5.Items.Add(dtrAgro2K.GetValue(1))
                ListBox7.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_SubClases Where Estado <> 2 order by descripcion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox9.Items.Add(dtrAgro2K.GetValue(1))
                ListBox11.Items.Add(dtrAgro2K.GetValue(0))
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = ""
            strQuery = "Select Registro, Nombre From prm_Proveedores Where Estado <> 2 order by Nombre"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox13.Items.Add(dtrAgro2K.GetValue(1))
                ListBox15.Items.Add(dtrAgro2K.GetValue(0))
            End While
            ' dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)

            strQuery = ""
            strQuery = "Select Codigo, Descripcion From prm_TipoMov order by descripcion"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ListBox19.Items.Add(dtrAgro2K.GetValue(0) & " - " & dtrAgro2K.GetValue(1))
                ListBox21.Items.Add(dtrAgro2K.GetValue(0))
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub Limpiar()

        Try


            DateTimePicker1.Value = DateSerial(Year(Now), Month(Now), 1)
            DateTimePicker2.Value = Now
            ListBox2.Items.Clear()
            ListBox4.Items.Clear()
            ListBox6.Items.Clear()
            ListBox8.Items.Clear()
            ListBox10.Items.Clear()
            ListBox12.Items.Clear()
            ListBox14.Items.Clear()
            ListBox16.Items.Clear()
            ListBox17.Items.Clear()
            ListBox18.Items.Clear()
            CheckBox1.Checked = True
            CheckBox2.Checked = True
            intRptExportar = 0
            TextBox1.Text = ""
            TextBox2.Text = ""
            Label13.Visible = False
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox9.DoubleClick, ListBox10.DoubleClick, ListBox13.DoubleClick, ListBox14.DoubleClick, ListBox17.DoubleClick, ListBox19.DoubleClick, ListBox20.DoubleClick
        Try

            Select Case sender.name.ToString
                Case "ListBox1"
                    If ListBox1.SelectedItem IsNot Nothing Then
                        ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
                    End If
                    'If ListBox3.SelectedItem IsNot Nothing Then
                    'ListBox4.Items.Add(ListBox3.SelectedItem)
                    'End If

                Case "ListBox2"
                    If ListBox2.SelectedIndex >= 0 Then
                        ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                    End If
                Case "ListBox5"
                    If ListBox5.SelectedItem IsNot Nothing AndAlso ListBox7.SelectedItem IsNot Nothing Then
                        ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem)
                    End If

                Case "ListBox6"
                    If ListBox6.SelectedIndex >= 0 Then
                        ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                    End If
                Case "ListBox9"
                    If ListBox9.SelectedItem IsNot Nothing Then
                        ListBox10.Items.Add(ListBox9.SelectedItem) : ListBox11.SelectedIndex = ListBox9.SelectedIndex : ListBox12.Items.Add(ListBox11.SelectedItem)
                    End If

                Case "ListBox10"
                    If ListBox10.SelectedIndex >= 0 Then
                        ListBox12.Items.RemoveAt(ListBox10.SelectedIndex) : ListBox10.Items.RemoveAt(ListBox10.SelectedIndex)
                    End If
                Case "ListBox13"
                    If ListBox13.SelectedItem IsNot Nothing Then
                        ListBox14.Items.Add(ListBox13.SelectedItem) : ListBox15.SelectedIndex = ListBox13.SelectedIndex : ListBox16.Items.Add(ListBox15.SelectedItem)
                    End If

                Case "ListBox14"
                    If ListBox14.SelectedIndex >= 0 Then
                        ListBox16.Items.RemoveAt(ListBox14.SelectedIndex) : ListBox14.Items.RemoveAt(ListBox14.SelectedIndex)
                    End If
                Case "ListBox17"
                    If ListBox17.SelectedIndex >= 0 Then
                        ListBox18.Items.RemoveAt(ListBox17.SelectedIndex) : ListBox17.Items.RemoveAt(ListBox17.SelectedIndex)
                    End If
                Case "ListBox19"
                    If ListBox19.SelectedItem IsNot Nothing Then
                        ListBox20.Items.Add(ListBox19.SelectedItem) : ListBox21.SelectedIndex = ListBox19.SelectedIndex : ListBox22.Items.Add(ListBox21.SelectedItem)
                    End If

                Case "ListBox20"
                    If ListBox20.SelectedIndex >= 0 Then
                        ListBox22.Items.RemoveAt(ListBox20.SelectedIndex) : ListBox20.Items.RemoveAt(ListBox20.SelectedIndex)
                    End If
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub GenerarQuery()

        Dim lngFecha As Long = 0

        Try


            If ListBox18.Items.Count > 0 Then                                       'Productos
                strQuery = strQuery + "'and Registro in ("
                If ListBox18.Items.Count = 1 Then
                    ListBox18.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ") ', "
                ElseIf ListBox18.Items.Count > 1 Then
                    For intIncr = 0 To ListBox18.Items.Count - 2
                        ListBox18.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox18.SelectedItem & ", "
                    Next
                    ListBox18.SelectedIndex = ListBox18.Items.Count - 1
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            'If intRptInventario = 1 Or intRptInventario = 8 Then                    'Agencias
            If intRptInventario = 8 Then                    'Agencias
                strQuery = strQuery + "'("
                ListBox4.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                'ElseIf intRptInventario <> 1 And intRptInventario <> 8 And intRptInventario <> 15 And intRptInventario <> 18 And intRptInventario <> 19 Then
            ElseIf intRptInventario <> 8 And intRptInventario <> 15 And intRptInventario <> 18 And intRptInventario <> 19 Then
                If ListBox4.Items.Count > 0 Then
                    strQuery = strQuery + "'("
                    If ListBox4.Items.Count = 1 Then
                        ListBox4.SelectedIndex = 0
                        strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                    ElseIf ListBox4.Items.Count > 1 Then
                        For intIncr = 0 To ListBox4.Items.Count - 2
                            ListBox4.SelectedIndex = intIncr
                            strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                        Next
                        ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                        strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                    End If
                Else
                    strQuery = strQuery + "' ', "
                End If
            ElseIf intRptInventario = 15 Then
                strQuery = strQuery + "'(1, 3, 4) ', "
            ElseIf intRptInventario = 18 Or intRptInventario = 19 Then
                strQuery = strQuery + "' ', "
            End If
            If ListBox8.Items.Count > 0 Then                                        'Clases
                strQuery = strQuery + "'and RegClase in ("
                If ListBox8.Items.Count = 1 Then
                    ListBox8.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
                ElseIf ListBox8.Items.Count > 1 Then
                    For intIncr = 0 To ListBox8.Items.Count - 2
                        ListBox8.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox8.SelectedItem & ", "
                    Next
                    ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox12.Items.Count > 0 Then                                       'SubClases
                strQuery = strQuery + "'and RegSubClase in ("
                If ListBox12.Items.Count = 1 Then
                    ListBox12.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                ElseIf ListBox12.Items.Count > 1 Then
                    For intIncr = 0 To ListBox12.Items.Count - 2
                        ListBox12.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox12.SelectedItem & ", "
                    Next
                    ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox16.Items.Count > 0 Then                                       'Proveedores
                strQuery = strQuery + "'and RegProveedor in ("
                If ListBox16.Items.Count = 1 Then
                    ListBox16.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox16.SelectedItem & ") ', "
                ElseIf ListBox16.Items.Count > 1 Then
                    For intIncr = 0 To ListBox16.Items.Count - 2
                        ListBox16.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox16.SelectedItem & ", "
                    Next
                    ListBox16.SelectedIndex = ListBox16.Items.Count - 1
                    strQuery = strQuery + "" & ListBox16.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            lngFecha = 0
            strFechaInicial = ""
            strFechaFinal = ""
            If CheckBox1.Checked = True Then                                        'Fecha Inicial
                lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
                strFechaInicial = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            Else
                lngFecha = 20001101
                strFechaInicial = "01-Nov-2000"
            End If
            strQuery = strQuery + "'" & lngFecha & " ', "
            If CheckBox2.Checked = True Then                                        'Fecha Final
                lngFecha = Format(DateTimePicker2.Value, "yyyyMMdd")
                strFechaFinal = Format(DateTimePicker2.Value, "dd-MMM-yyyy")
            Else
                lngFecha = Format(Now, "yyyyMMdd")
                strFechaFinal = Format(Now, "dd-MMM-yyyy")
            End If
            If intRptInventario = 2 Then
                strQuery = strQuery + "'" & lngFecha & " ', '" & TextBox2.Text & "', "
            Else
                strQuery = strQuery + "'" & lngFecha & " ', ' ', "
            End If
            If ListBox22.Items.Count > 0 Then                                       'Tipo de Movimientos
                strQuery = strQuery + "'("
                If ListBox22.Items.Count = 1 Then
                    ListBox22.SelectedIndex = 0
                    strQuery = strQuery + "''" & ListBox22.SelectedItem & "'') ', "
                ElseIf ListBox22.Items.Count > 1 Then
                    For intIncr = 0 To ListBox22.Items.Count - 2
                        ListBox22.SelectedIndex = intIncr
                        strQuery = strQuery + "''" & ListBox22.SelectedItem & "'', "
                    Next
                    ListBox22.SelectedIndex = ListBox22.Items.Count - 1
                    strQuery = strQuery + "''" & ListBox22.SelectedItem & "'') ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            strQuery = strQuery + "" & NumericUpDown1.Value & " "
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub


    Sub GenerarQuery2()

        Dim lngFecha As Long

        Try


            If ListBox18.Items.Count > 0 Then                                       'Productos
                strQuery = strQuery + "'and P.Registro in ("
                If ListBox18.Items.Count = 1 Then
                    ListBox18.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ") ', "
                ElseIf ListBox18.Items.Count > 1 Then
                    For intIncr = 0 To ListBox18.Items.Count - 2
                        ListBox18.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox18.SelectedItem & ", "
                    Next
                    ListBox18.SelectedIndex = ListBox18.Items.Count - 1
                    strQuery = strQuery + "" & ListBox18.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox4.Items.Count > 0 Then                                        'Agencias
                strQuery = strQuery + "'and A.Registro in ("
                If ListBox4.Items.Count = 1 Then
                    ListBox4.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                ElseIf ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 2
                        ListBox4.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                    Next
                    ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox8.Items.Count > 0 Then                                        'Clases
                strQuery = strQuery + "'and C.Registro in ("
                If ListBox8.Items.Count = 1 Then
                    ListBox8.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
                ElseIf ListBox8.Items.Count > 1 Then
                    For intIncr = 0 To ListBox8.Items.Count - 2
                        ListBox8.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox8.SelectedItem & ", "
                    Next
                    ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox12.Items.Count > 0 Then                                       'SubClases
                strQuery = strQuery + "'and P.RegSubClase in ("
                If ListBox12.Items.Count = 1 Then
                    ListBox12.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                ElseIf ListBox12.Items.Count > 1 Then
                    For intIncr = 0 To ListBox12.Items.Count - 2
                        ListBox12.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox12.SelectedItem & ", "
                    Next
                    ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            If ListBox16.Items.Count > 0 Then                                       'Proveedores
                strQuery = strQuery + "'and P.RegProveedor in ("
                If ListBox16.Items.Count = 1 Then
                    ListBox16.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox16.SelectedItem & ") ', "
                ElseIf ListBox16.Items.Count > 1 Then
                    For intIncr = 0 To ListBox16.Items.Count - 2
                        ListBox16.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox16.SelectedItem & ", "
                    Next
                    ListBox16.SelectedIndex = ListBox16.Items.Count - 1
                    strQuery = strQuery + "" & ListBox16.SelectedItem & ") ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            lngFecha = 0
            strFechaInicial = ""
            strFechaFinal = ""
            If CheckBox1.Checked = True Then                                        'Fecha Inicial
                lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
                strFechaInicial = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            Else
                lngFecha = 20001101
                strFechaInicial = "01-Nov-2000"
            End If
            strQuery = strQuery + "'and M.NumFechaIng >= " & lngFecha & " ', "
            If CheckBox2.Checked = True Then                                        'Fecha Final
                lngFecha = Format(DateTimePicker2.Value, "yyyyMMdd")
                strFechaFinal = Format(DateTimePicker2.Value, "dd-MMM-yyyy")
            Else
                lngFecha = Format(Now, "yyyyMMdd")
                strFechaFinal = Format(Now, "dd-MMM-yyyy")
            End If
            strQuery = strQuery + "'and M.NumFechaIng <= " & lngFecha & " ', ' ', "
            If ListBox22.Items.Count > 0 Then                                       'Tipo de Movimientos
                strQuery = strQuery + "'("
                If ListBox22.Items.Count = 1 Then
                    ListBox22.SelectedIndex = 0
                    strQuery = strQuery + "''" & ListBox22.SelectedItem & "'') ', "
                ElseIf ListBox22.Items.Count > 1 Then
                    For intIncr = 0 To ListBox22.Items.Count - 2
                        ListBox22.SelectedIndex = intIncr
                        strQuery = strQuery + "''" & ListBox22.SelectedItem & "'', "
                    Next
                    ListBox22.SelectedIndex = ListBox22.Items.Count - 1
                    strQuery = strQuery + "''" & ListBox22.SelectedItem & "'') ', "
                End If
            Else
                strQuery = strQuery + "' ', "
            End If
            strQuery = strQuery + "" & NumericUpDown1.Value & " "
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub GenerarQuery3()

        Dim lngFecha As Long
        Try


            lngFecha = 0
            strFechaInicial = ""
            strFechaFinal = ""
            If CheckBox1.Checked = True Then                                        'Fecha Inicial
                lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
                strFechaInicial = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            Else
                lngFecha = 20001101
                strFechaInicial = "01-Nov-2000"
            End If
            strQuery = strQuery + "'" & lngFecha & " ', "
            If CheckBox2.Checked = True Then                                        'Fecha Final
                lngFecha = Format(DateTimePicker2.Value, "yyyyMMdd")
                strFechaFinal = Format(DateTimePicker2.Value, "dd-MMM-yyyy")
            Else
                lngFecha = Format(Now, "yyyyMMdd")
                strFechaFinal = Format(Now, "dd-MMM-yyyy")
            End If
            strQuery = strQuery + "'" & lngFecha & " ',"
            If ListBox4.Items.Count > 0 Then                                        'Agencias
                strQuery = strQuery + "'"
                If ListBox4.Items.Count = 1 Then
                    ListBox4.SelectedIndex = 0
                    strQuery = strQuery + "" & ListBox4.SelectedItem & "'"
                ElseIf ListBox4.Items.Count > 1 Then
                    For intIncr = 0 To ListBox4.Items.Count - 2
                        ListBox4.SelectedIndex = intIncr
                        strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                    Next
                    ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                    strQuery = strQuery + "" & ListBox4.SelectedItem & "'"
                End If
            Else
                strQuery = strQuery + "' '"
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        Try



            If e.KeyCode = Keys.Enter Then
                If UbicarProducto() = False Then
                    MsgBox("C�digo de producto no est� registrado.   Verifique.", MsgBoxStyle.Critical, "Extracci�n de Registro")
                End If
                TextBox1.Text = ""
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus

        Label13.Visible = False

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus

        Label13.Visible = True

    End Sub

    Function UbicarProducto() As Boolean
        Try

            lngRegistro = 0
            strQuery = ""
            strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P Where P.Codigo = '" & TextBox1.Text & "'"
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                lngRegistro = dtrAgro2K.GetValue(0)
                ListBox17.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
                ListBox18.Items.Add(lngRegistro)
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If strUbicar <> "" Then
            Timer1.Enabled = False
            TextBox1.Text = strUbicar : UbicarProducto() : ListBox17.Focus() : TextBox1.Text = ""
        End If

    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.DoubleClick

        intListadoAyuda = 3
        Dim frmNew As New frmListadoAyuda
        Timer1.Interval = 200
        Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    'Public Sub CargarComboReportes()
    '    Dim drReportes As DataRow
    '    Try
    '        dtReportes = RNReportes.ObtieneCatReportesxModuloyUsuario(nIdUsuario, nIdModulo)
    '        drReportes = dtReportes.NewRow()
    '        drReportes("OpcionReporte") = -1
    '        drReportes("Descripcion") = "<Seleccione Reporte>"
    '        dtReportes.Rows.Add(drReportes)

    '        cbReportes.DataSource = dtReportes
    '        cbReportes.DisplayMember = "Descripcion"
    '        cbReportes.ValueMember = "OpcionReporte"
    '        cbReportes.SelectedValue = -1
    '    Catch ex As Exception
    '        MessageBox.Show("Error al cargar Datos " + ex.Message(), "Reportes", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    Public Sub MostrarReporte()

        Dim frmNew As New actrptViewer

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            strQuery = ""
            intRptOtros = 0
            intRptPresup = 0
            intRptCtasCbr = 0
            intRptFactura = 0
            If cbReportes.SelectedValue = -1 Then
                MessageBox.Show("Selecionar el Tipo de Reporte", "Reportes de Inventario", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                intRptInventario = cbReportes.SelectedValue
            End If

            intRptImpFactura = 0
            intRptTipoPrecio = 0
            intRptCheques = 0
            intRptImpRecibos = 0

            If intRptInventario > 0 Then
                If intRptInventario = 1 Or intRptInventario = 8 Or intRptInventario = 20 Then
                    If ListBox4.Items.Count = 0 Then
                        MsgBox("Tiene que Escoger la Sucursal a Consultar.", MsgBoxStyle.Information, "Faltan Datos")
                        Me.Cursor = System.Windows.Forms.Cursors.Default
                        Exit Sub
                    End If
                End If
                If intRptInventario = 23 Then
                    strQuery = "exec sp_RptExistenciaOrdenadoxProducto " & intRptInventario & ", "
                Else
                    strQuery = "exec sp_RptInventario " & intRptInventario & ", "
                End If

                If intRptInventario <> 3 Then
                    GenerarQuery()
                ElseIf intRptInventario = 3 Then
                    GenerarQuery2()
                End If
                strFechaReporte = "Del " & strFechaInicial & " Al " & strFechaFinal & ""
                If intRptInventario >= 5 And intRptInventario <= 23 Then
                    If intRptInventario <> 9 And intRptInventario <> 10 Then
                        strFechaReporte = "Al " & strFechaFinal & ""
                    End If
                End If
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default

            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        Try
            MostrarReporte()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default

            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default

            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdiExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiExcel.Click
        intRptExportar = 1
    End Sub

    Private Sub cmdiHtml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiHtml.Click
        intRptExportar = 2
    End Sub

    Private Sub cmdiPdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiPdf.Click
        intRptExportar = 3
    End Sub

    Private Sub cmdiRtf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiRtf.Click
        intRptExportar = 4
    End Sub

    Private Sub cmdiTiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiTiff.Click
        intRptExportar = 5
    End Sub

    Private Sub cmdExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExportar.Click
        intRptExportar = 0
    End Sub
End Class
