Imports System.Data.SqlClient
Imports System.Text

Public Class frmAgencias
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents cntmnuAgenciasRg As System.Windows.Forms.ContextMenu
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents ddlCompras As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtRegistroVendedor As System.Windows.Forms.TextBox
    Friend WithEvents cmbCasaMatriz As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnListado As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnGeneral As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCambios As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReportes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnRegistros As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrimerRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAnteriorRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSiguienteRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUltimoRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnIgual As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtNumeroRUC As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAgencias))
        Dim UltraStatusPanel3 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel4 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.cntmnuAgenciasRg = New System.Windows.Forms.ContextMenu
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtNumeroRUC = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtTelefono = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtDireccion = New System.Windows.Forms.TextBox
        Me.cmbCasaMatriz = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtRegistroVendedor = New System.Windows.Forms.TextBox
        Me.txtVendedor = New System.Windows.Forms.TextBox
        Me.txtNombreVendedor = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.ddlCompras = New System.Windows.Forms.ComboBox
        Me.ComboBox6 = New System.Windows.Forms.ComboBox
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label8 = New System.Windows.Forms.Label
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.btnListado = New DevComponents.DotNetBar.ButtonItem
        Me.btnGeneral = New DevComponents.DotNetBar.ButtonItem
        Me.btnCambios = New DevComponents.DotNetBar.ButtonItem
        Me.btnReportes = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.btnRegistros = New DevComponents.DotNetBar.ButtonItem
        Me.btnPrimerRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnAnteriorRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnSiguienteRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnUltimoRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnIgual = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtNumeroRUC)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtTelefono)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.cmbCasaMatriz)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtRegistroVendedor)
        Me.GroupBox1.Controls.Add(Me.txtVendedor)
        Me.GroupBox1.Controls.Add(Me.txtNombreVendedor)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.ddlCompras)
        Me.GroupBox1.Controls.Add(Me.ComboBox6)
        Me.GroupBox1.Controls.Add(Me.ComboBox5)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.ComboBox4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 82)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(533, 294)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de las Agencias"
        '
        'txtNumeroRUC
        '
        Me.txtNumeroRUC.Location = New System.Drawing.Point(368, 262)
        Me.txtNumeroRUC.MaxLength = 30
        Me.txtNumeroRUC.Name = "txtNumeroRUC"
        Me.txtNumeroRUC.Size = New System.Drawing.Size(145, 20)
        Me.txtNumeroRUC.TabIndex = 30
        Me.txtNumeroRUC.Tag = "N�mero RUC"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(284, 266)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 13)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "N�mero RUC"
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(127, 262)
        Me.txtTelefono.MaxLength = 30
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(145, 20)
        Me.txtTelefono.TabIndex = 27
        Me.txtTelefono.Tag = "C�digo asignado a la agencia"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(12, 266)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(107, 13)
        Me.Label12.TabIndex = 28
        Me.Label12.Text = "Telefono Agencia"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 238)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 13)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Direcion Agencia"
        '
        'txtDireccion
        '
        Me.txtDireccion.Location = New System.Drawing.Point(127, 235)
        Me.txtDireccion.MaxLength = 500
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(388, 20)
        Me.txtDireccion.TabIndex = 25
        Me.txtDireccion.Tag = "Nombre del c�digo de la agencia ingresada"
        '
        'cmbCasaMatriz
        '
        Me.cmbCasaMatriz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCasaMatriz.Items.AddRange(New Object() {"no", "si"})
        Me.cmbCasaMatriz.Location = New System.Drawing.Point(438, 96)
        Me.cmbCasaMatriz.Name = "cmbCasaMatriz"
        Me.cmbCasaMatriz.Size = New System.Drawing.Size(80, 21)
        Me.cmbCasaMatriz.TabIndex = 23
        Me.cmbCasaMatriz.Tag = "Agencia por defecto"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(345, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(90, 13)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "Es casa Matriz"
        '
        'txtRegistroVendedor
        '
        Me.txtRegistroVendedor.Location = New System.Drawing.Point(386, 208)
        Me.txtRegistroVendedor.Name = "txtRegistroVendedor"
        Me.txtRegistroVendedor.Size = New System.Drawing.Size(24, 20)
        Me.txtRegistroVendedor.TabIndex = 22
        Me.txtRegistroVendedor.Visible = False
        '
        'txtVendedor
        '
        Me.txtVendedor.Location = New System.Drawing.Point(128, 208)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Size = New System.Drawing.Size(64, 20)
        Me.txtVendedor.TabIndex = 20
        Me.txtVendedor.Tag = "Vendedor"
        '
        'txtNombreVendedor
        '
        Me.txtNombreVendedor.Location = New System.Drawing.Point(190, 208)
        Me.txtNombreVendedor.Name = "txtNombreVendedor"
        Me.txtNombreVendedor.ReadOnly = True
        Me.txtNombreVendedor.Size = New System.Drawing.Size(324, 20)
        Me.txtNombreVendedor.TabIndex = 21
        Me.txtNombreVendedor.TabStop = False
        Me.txtNombreVendedor.Tag = "Vendedor"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 211)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 13)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Vendedor"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(386, 147)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(8, 20)
        Me.TextBox5.TabIndex = 18
        Me.TextBox5.Text = "TextBox5"
        Me.TextBox5.Visible = False
        '
        'ddlCompras
        '
        Me.ddlCompras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlCompras.Items.AddRange(New Object() {"originales", "nuevas"})
        Me.ddlCompras.Location = New System.Drawing.Point(354, 147)
        Me.ddlCompras.Name = "ddlCompras"
        Me.ddlCompras.Size = New System.Drawing.Size(162, 21)
        Me.ddlCompras.TabIndex = 17
        Me.ddlCompras.Tag = "Que tipo de Compras est� utilizando la agencia"
        '
        'ComboBox6
        '
        Me.ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox6.Location = New System.Drawing.Point(289, 178)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox6.TabIndex = 16
        Me.ComboBox6.Tag = "Departamento donde reside la agencia"
        Me.ComboBox6.Visible = False
        '
        'ComboBox5
        '
        Me.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox5.Location = New System.Drawing.Point(128, 178)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(388, 21)
        Me.ComboBox5.TabIndex = 15
        Me.ComboBox5.Tag = "Departamento donde reside la agencia"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Departamento"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(169, 148)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(8, 20)
        Me.TextBox4.TabIndex = 13
        Me.TextBox4.Text = "TextBox4"
        Me.TextBox4.Visible = False
        '
        'ComboBox4
        '
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.Items.AddRange(New Object() {"originales", "nuevas"})
        Me.ComboBox4.Location = New System.Drawing.Point(128, 148)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(161, 21)
        Me.ComboBox4.TabIndex = 5
        Me.ComboBox4.Tag = "Que tipo de facturas est� utilizando la agencia"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Facturas"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Items.AddRange(New Object() {"activa", "inactiva", "eliminada"})
        Me.ComboBox3.Location = New System.Drawing.Point(128, 122)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(163, 21)
        Me.ComboBox3.TabIndex = 4
        Me.ComboBox3.Tag = "Estado del registro"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Estado"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Items.AddRange(New Object() {"si", "no"})
        Me.ComboBox2.Location = New System.Drawing.Point(263, 96)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(80, 21)
        Me.ComboBox2.TabIndex = 3
        Me.ComboBox2.Tag = "Agencia por defecto"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(191, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Por Defecto"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(128, 32)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(8, 20)
        Me.TextBox3.TabIndex = 6
        Me.TextBox3.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"si", "no"})
        Me.ComboBox1.Location = New System.Drawing.Point(128, 96)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(48, 21)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.Tag = "Podr� o no realizar traslados a otras agencias"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Traslados"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(128, 32)
        Me.TextBox1.MaxLength = 5
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(56, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "C�digo asignado a la agencia"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nombre"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "C�digo"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(128, 64)
        Me.TextBox2.MaxLength = 30
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(386, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Nombre del c�digo de la agencia ingresada"
        '
        'Timer1
        '
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(295, 151)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 13)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "Compras"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 380)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel4.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel3, UltraStatusPanel4})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(537, 23)
        Me.UltraStatusBar1.TabIndex = 19
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnListado, Me.LabelItem4, Me.btnRegistros, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(537, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 55
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnListado
        '
        Me.btnListado.AutoExpandOnClick = True
        Me.btnListado.Image = CType(resources.GetObject("btnListado.Image"), System.Drawing.Image)
        Me.btnListado.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnListado.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnListado.Name = "btnListado"
        Me.btnListado.OptionGroup = "Listado de productos"
        Me.btnListado.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnGeneral, Me.btnCambios, Me.btnReportes})
        Me.btnListado.Text = "Listados"
        Me.btnListado.Tooltip = "Listados"
        '
        'btnGeneral
        '
        Me.btnGeneral.Image = CType(resources.GetObject("btnGeneral.Image"), System.Drawing.Image)
        Me.btnGeneral.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnGeneral.Name = "btnGeneral"
        Me.btnGeneral.Text = "General"
        Me.btnGeneral.Tooltip = "General"
        '
        'btnCambios
        '
        Me.btnCambios.Image = CType(resources.GetObject("btnCambios.Image"), System.Drawing.Image)
        Me.btnCambios.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnCambios.Name = "btnCambios"
        Me.btnCambios.Tag = "Cambios"
        Me.btnCambios.Text = "Cambios"
        Me.btnCambios.Tooltip = "Cambios"
        '
        'btnReportes
        '
        Me.btnReportes.Image = CType(resources.GetObject("btnReportes.Image"), System.Drawing.Image)
        Me.btnReportes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Tag = "Reportes"
        Me.btnReportes.Text = "Reportes"
        Me.btnReportes.Tooltip = "Reportes"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'btnRegistros
        '
        Me.btnRegistros.AutoExpandOnClick = True
        Me.btnRegistros.Image = CType(resources.GetObject("btnRegistros.Image"), System.Drawing.Image)
        Me.btnRegistros.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnRegistros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.OptionGroup = "Recorre Registros"
        Me.btnRegistros.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPrimerRegistro, Me.btnAnteriorRegistro, Me.btnSiguienteRegistro, Me.btnUltimoRegistro, Me.btnIgual})
        Me.btnRegistros.Text = "Recorre Registros"
        Me.btnRegistros.Tooltip = "Recorre Registros"
        '
        'btnPrimerRegistro
        '
        Me.btnPrimerRegistro.Image = CType(resources.GetObject("btnPrimerRegistro.Image"), System.Drawing.Image)
        Me.btnPrimerRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrimerRegistro.Name = "btnPrimerRegistro"
        Me.btnPrimerRegistro.Text = "Primer registro"
        Me.btnPrimerRegistro.Tooltip = "Primer registro"
        '
        'btnAnteriorRegistro
        '
        Me.btnAnteriorRegistro.Image = CType(resources.GetObject("btnAnteriorRegistro.Image"), System.Drawing.Image)
        Me.btnAnteriorRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnAnteriorRegistro.Name = "btnAnteriorRegistro"
        Me.btnAnteriorRegistro.Text = "Anterior registro"
        Me.btnAnteriorRegistro.Tooltip = "Anterior registro"
        '
        'btnSiguienteRegistro
        '
        Me.btnSiguienteRegistro.Image = CType(resources.GetObject("btnSiguienteRegistro.Image"), System.Drawing.Image)
        Me.btnSiguienteRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnSiguienteRegistro.Name = "btnSiguienteRegistro"
        Me.btnSiguienteRegistro.Tag = "Siguiente registro"
        Me.btnSiguienteRegistro.Text = "Siguiente registro"
        Me.btnSiguienteRegistro.Tooltip = "Siguiente registro"
        '
        'btnUltimoRegistro
        '
        Me.btnUltimoRegistro.Image = CType(resources.GetObject("btnUltimoRegistro.Image"), System.Drawing.Image)
        Me.btnUltimoRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnUltimoRegistro.Name = "btnUltimoRegistro"
        Me.btnUltimoRegistro.Tag = "Ultimo registro"
        Me.btnUltimoRegistro.Text = "Ultimo registro"
        Me.btnUltimoRegistro.Tooltip = "Ultimo registro"
        '
        'btnIgual
        '
        Me.btnIgual.Image = CType(resources.GetObject("btnIgual.Image"), System.Drawing.Image)
        Me.btnIgual.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnIgual.Name = "btnIgual"
        Me.btnIgual.Tag = "Igual"
        Me.btnIgual.Text = "Igual"
        Me.btnIgual.Tooltip = "Igual"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmAgencias
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(537, 403)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAgencias"
        Me.Text = "frmAgencias"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strCambios(9) As String

    Private Sub frmAgencias_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

    End Sub

    Private Sub frmAgencias_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        CreateMyContextMenu()
        Iniciar()
        Limpiar()
        intModulo = 1

    End Sub

    Private Sub frmAgencias_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        ElseIf e.KeyCode = Keys.F5 Then
            intListadoAyuda = 1
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                ' Dim frmNew As New frmGeneral
                lngRegistro = TextBox3.Text
                Timer1.Interval = 200
                Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = TextBox3.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)


    End Sub

    Sub Guardar()

        If IsNumeric(TextBox1.Text) = False Then
            TextBox1.Text = "0"
            MsgBox("El C�digo de la Agencia debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error de Ingreso de Datos")
            Exit Sub
        End If
        If ComboBox4.SelectedItem <> strCambios(4) And strCambios(4) = "nuevas" Then
            MsgBox("No puede regresar el tipo de factura a utilizar al formato ""Originales"".", MsgBoxStyle.Critical, "Acci�n Denegada")
            Exit Sub
        End If
        If ddlCompras.SelectedItem <> strCambios(6) And strCambios(6) = "nuevas" Then
            MsgBox("No puede regresar el tipo de factura a utilizar al formato ""Originales"".", MsgBoxStyle.Critical, "Acci�n Denegada")
            Exit Sub
        End If
        If ComboBox4.SelectedIndex = 1 And TextBox4.Text = "0" Then
            TextBox4.Text = Format(Now, "yyyyMMdd")
        End If
        If ddlCompras.SelectedIndex = 1 And TextBox5.Text = "0" Then
            TextBox5.Text = Format(Now, "yyyyMMdd")
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngAgencias", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter

        lngRegistro = 0
        ComboBox6.SelectedIndex = ComboBox5.SelectedIndex
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = TextBox1.Text
        End With
        With prmTmp03
            .ParameterName = "@strDescripcion"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox2.Text
        End With
        With prmTmp04
            .ParameterName = "@intTraslado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox1.SelectedIndex
        End With
        With prmTmp05
            .ParameterName = "@intDefecto"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox2.SelectedIndex
        End With
        With prmTmp06
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox3.SelectedIndex
        End With
        With prmTmp07
            .ParameterName = "@intFactura"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox4.SelectedIndex
        End With
        With prmTmp08
            .ParameterName = "@intFechaCambio"
            .SqlDbType = SqlDbType.Int
            .Value = CInt(TextBox4.Text)
        End With
        With prmTmp09
            .ParameterName = "@intDepartamento"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox6.SelectedItem
        End With
        With prmTmp10
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp11
            .ParameterName = "@intFechaCambioCompra"
            .SqlDbType = SqlDbType.Int
            .Value = CInt(TextBox4.Text)
        End With
        With prmTmp12
            .ParameterName = "@intCompra"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox4.SelectedIndex
        End With
        With prmTmp13
            .ParameterName = "@RegistroVendedor"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ConvierteAInt(txtRegistroVendedor.Text.Trim())
        End With
        With prmTmp14
            .ParameterName = "@EsCasaMatriz"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ConvierteAInt(cmbCasaMatriz.SelectedIndex)
        End With
        With prmTmp15
            .ParameterName = "@Direccion"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtDireccion.Text
        End With
        With prmTmp16
            .ParameterName = "@Telefono"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtTelefono.Text
        End With
        With prmTmp17
            .ParameterName = "@RUC"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNumeroRUC.Text
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        'cmdTmp.Connection.Open()
        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            lngRegistro = TextBox3.Text
            cmdTmp.Parameters(0).Value = lngRegistro
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        cmdTmp.Parameters(9).Value = 1

        Try
            If cmdTmp.Connection.State = ConnectionState.Closed Then
                cmdTmp.Connection.Open()
            End If
            cmdTmp.ExecuteNonQuery()
            If intResp = 6 Then
                If TextBox2.Text <> strCambios(0) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Descripci�n", strCambios(0), TextBox2.Text)
                End If
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Traslado", strCambios(1), ComboBox1.SelectedItem)
                End If
                If ComboBox2.SelectedItem <> strCambios(2) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Por Defecto", strCambios(2), ComboBox2.SelectedItem)
                End If
                If ComboBox3.SelectedItem <> strCambios(3) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Estado", strCambios(3), ComboBox3.SelectedItem)
                End If
                If ComboBox4.SelectedItem <> strCambios(4) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Factura", strCambios(4), ComboBox4.SelectedItem)
                End If
                If ComboBox5.SelectedItem <> strCambios(5) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Departamento", strCambios(5), ComboBox5.SelectedItem)
                End If
                If ddlCompras.SelectedItem <> strCambios(6) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Compra", strCambios(6), ddlCompras.SelectedItem)
                End If
            ElseIf intResp = 0 Then
                Ing_Bitacora(intModulo, lngRegistro, "C�digo", "", TextBox1.Text)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If

        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Departamentos"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox5.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox6.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        TextBox1.Text = "0"
        TextBox2.Text = ""
        TextBox3.Text = "0"
        TextBox4.Text = "0"
        txtDireccion.Text = String.Empty
        txtTelefono.Text = String.Empty
        ComboBox1.SelectedIndex = -1
        ComboBox2.SelectedIndex = -1
        ComboBox3.SelectedIndex = -1
        ComboBox4.SelectedIndex = -1
        ComboBox5.SelectedIndex = -1
        cmbCasaMatriz.SelectedIndex = -1
        strCambios(0) = ""
        strCambios(1) = ""
        TextBox1.Focus()
        txtNumeroRUC.Text = String.Empty
        txtVendedor.Text = String.Empty
    End Sub

    Sub MoverRegistro(ByVal StrDato As String)
        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(Codigo) from prm_Agencias"
            Case "Anterior"
                strQuery = "Select Max(Codigo) from prm_Agencias Where codigo < '" & TextBox1.Text & "'"
            Case "Siguiente"
                strQuery = "Select Min(Codigo) from prm_Agencias Where codigo > '" & TextBox1.Text & "'"
            Case "Ultimo"
                strQuery = "Select Max(Codigo) from prm_Agencias"
            Case "Igual"
                strQuery = "Select Codigo from prm_Agencias Where codigo = '" & TextBox1.Text & "'"
        End Select
        Limpiar()
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim strCodigo As String
        Dim intDept As Integer

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        strCodigo = ""
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                strCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If strCodigo = "" Then
            If StrDato = "Igual" Then
                MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
            Else
                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        ' strQuery = "Select * From prm_Agencias Where Codigo = '" & strCodigo & "'"
        'strQuery = "Select a.*,isnull(b.Codigo,'') CodigoVendedor,isnull(b.Nombre,'') NombreVendedor From prm_Agencias a left outer join  prm_vendedores b on a.IdVendedor = b.registro where a.Codigo = '" & strCodigo & "'"
        strQuery = "exec obtieneDatosAgencia '" & strCodigo & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            'TextBox3.Text = dtrAgro2K.GetValue(0)
            TextBox3.Text = dtrAgro2K.Item("registro")
            'TextBox1.Text = dtrAgro2K.GetValue(1)
            TextBox1.Text = dtrAgro2K.Item("Codigo")
            'TextBox2.Text = dtrAgro2K.GetValue(2)
            TextBox2.Text = dtrAgro2K.Item("descripcion")
            'ComboBox1.SelectedIndex = dtrAgro2K.GetValue(3)
            ComboBox1.SelectedIndex = dtrAgro2K.Item("traslados")
            'ComboBox2.SelectedIndex = dtrAgro2K.GetValue(4)
            ComboBox2.SelectedIndex = dtrAgro2K.Item("defecto")
            'ComboBox3.SelectedIndex = dtrAgro2K.GetValue(5)
            ComboBox3.SelectedIndex = dtrAgro2K.Item("estado")
            'ComboBox4.SelectedIndex = dtrAgro2K.GetValue(6)
            ComboBox4.SelectedIndex = dtrAgro2K.Item("facturas")
            'cmbCasaMatriz.SelectedIndex = dtrAgro2K.GetValue(14)
            cmbCasaMatriz.SelectedIndex = dtrAgro2K.Item("CasaMatriz")
            'txtDireccion.Text = dtrAgro2K.GetValue(15)
            txtDireccion.Text = dtrAgro2K.Item("Direccion")
            'txtTelefono.Text = dtrAgro2K.GetValue(16)
            txtTelefono.Text = dtrAgro2K.Item("Telefono")
            'TextBox4.Text = dtrAgro2K.GetValue(7)
            TextBox4.Text = dtrAgro2K.Item("fechacambiofactura")
            'intDept = dtrAgro2K.GetValue(10)
            intDept = dtrAgro2K.Item("deptregistro")
            'txtVendedor.Text = dtrAgro2K.GetValue(12)
            txtVendedor.Text = dtrAgro2K.Item("CodigoVendedor")
            'txtNombreVendedor.Text = dtrAgro2K.GetValue(13)
            txtNombreVendedor.Text = dtrAgro2K.Item("NombreVendedor")
            'txtRegistroVendedor.Text = ConvierteAInt(dtrAgro2K.GetValue(11))
            txtRegistroVendedor.Text = ConvierteAInt(dtrAgro2K.Item("IdVendedor"))
            'TextBox5.Text = dtrAgro2K.GetValue(8)
            TextBox5.Text = dtrAgro2K.Item("compras")
            'ddlCompras.SelectedIndex = dtrAgro2K.GetValue(9)
            ddlCompras.SelectedIndex = dtrAgro2K.Item("fechacambiocompra")
            txtNumeroRUC.Text = dtrAgro2K.Item("Ruc")
            'strCambios(0) = dtrAgro2K.GetValue(2)
            strCambios(0) = dtrAgro2K.Item("descripcion")
            strCambios(1) = ComboBox1.SelectedItem
            strCambios(2) = ComboBox2.SelectedItem
            strCambios(3) = ComboBox3.SelectedItem
            strCambios(4) = ComboBox4.SelectedItem
            strCambios(6) = ddlCompras.SelectedItem()
            strCambios(8) = cmbCasaMatriz.SelectedIndex
        End While
        ComboBox6.SelectedIndex = -1
        For intIncr = 0 To ComboBox6.Items.Count - 1
            ComboBox6.SelectedIndex = intIncr
            If ComboBox6.SelectedItem = intDept Then
                ComboBox5.SelectedIndex = ComboBox6.SelectedIndex
                Exit For
            End If
        Next intIncr
        strCambios(5) = ComboBox5.SelectedItem
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem
        Dim cntmenuItem5 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"
        cntmenuItem5.Text = "Igual"

        cntmnuAgenciasRg.MenuItems.Add(cntmenuItem1)
        cntmnuAgenciasRg.MenuItems.Add(cntmenuItem2)
        cntmnuAgenciasRg.MenuItems.Add(cntmenuItem3)
        cntmnuAgenciasRg.MenuItems.Add(cntmenuItem4)
        cntmnuAgenciasRg.MenuItems.Add(cntmenuItem5)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, ComboBox1.GotFocus, ComboBox2.GotFocus, ComboBox3.GotFocus, ComboBox4.GotFocus, ComboBox5.GotFocus, txtVendedor.GotFocus

        UltraStatusBar1.Panels.Item(0).Text = IIf(sender.name = "TextBox1", "C�digo", IIf(sender.name = "TextBox2", "Descripci�n", IIf(sender.name = "ComboBox1", "Traslados", IIf(sender.name = "ComboBox2", "Por Defecto", IIf(sender.name = "ComboBox3", "Estado", IIf(sender.name = "ComboBox4", "Factura", "Departamento"))))))
        UltraStatusBar1.Panels.Item(1).Text = sender.tag
        If (intListadoAyuda <> 1) Then
            Select Case sender.name.ToString
                Case "TextBox1" : intListadoAyuda = 8
                Case "txtVendedor" : intListadoAyuda = 1
            End Select
        End If
        sender.selectall()

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, ComboBox1.KeyDown, ComboBox2.KeyDown, ComboBox3.KeyDown, ComboBox4.KeyDown, ComboBox5.KeyDown, txtDireccion.KeyDown, txtTelefono.KeyDown, txtNumeroRUC.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "TextBox1" : MoverRegistro("Igual") : TextBox2.Focus()
                Case "TextBox2" : ComboBox1.Focus()
                Case "ComboBox1" : ComboBox2.Focus()
                Case "ComboBox2" : cmbCasaMatriz.Focus()
                Case "cmbCasaMatriz" : ComboBox3.Focus()
                Case "ComboBox3" : ComboBox4.Focus()
                Case "ComboBox4" : ddlCompras.Focus()
                Case "ddlCompras" : ComboBox5.Focus()
                Case "ComboBox5" : txtVendedor.Focus()
                Case "txtVendedor" : txtDireccion.Focus()
                Case "txtDireccion" : txtTelefono.Focus()
                Case "txtTelefono" : txtNumeroRUC.Focus()
                Case "txtNumeroRUC" : TextBox1.Focus()

            End Select
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            If intListadoAyuda = 1 Then
                txtVendedor.Text = strUbicar
                UbicarVendedor(txtVendedor.Text)

            Else
                If intListadoAyuda = 8 Then
                    blnUbicar = False
                    Timer1.Enabled = False
                    If strUbicar <> "" Then
                        TextBox1.Text = strUbicar
                        MoverRegistro("Igual")
                    End If
                End If
            End If
        End If
    End Sub

    Function UbicarVendedor(ByVal strDato As String) As Boolean

        'txtVendedor.Text = "0"
        strQuery = ""
        strQuery = "Select Nombre, Registro from prm_Vendedores Where Codigo = '" & txtVendedor.Text & "' "
        strQuery = strQuery + "And Estado = 0"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            txtNombreVendedor.Text = dtrAgro2K.GetValue(0)
            txtRegistroVendedor.Text = dtrAgro2K.GetValue(1)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If txtVendedor.Text = "0" Then
            UbicarVendedor = False
            Exit Function
        End If
        UbicarVendedor = True

    End Function

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        'Dim frmNew As New frmGeneral
        'lngRegistro = TextBox3.Text
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        'frmNew.ShowDialog()
    End Sub

    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Dim frmNew As New frmBitacora
        lngRegistro = TextBox3.Text
        frmNew.ShowDialog()
    End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Dim frmNew As New actrptViewer
        intRptFactura = 30
        strQuery = "exec sp_Reportes " & intModulo & " "
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        MoverRegistro("Primero")
    End Sub

    Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
        MoverRegistro("Anterior")
    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        MoverRegistro("Siguiente")
    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        MoverRegistro("Ultimo")
    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        MoverRegistro("Igual")
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label13.Click

    End Sub

    Private Sub TextBox6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumeroRUC.TextChanged

    End Sub
End Class
