﻿Imports System.Windows.Forms
'Imports Microsoft.Reporting.WinForms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Public Class FrmRPT_TotalesComprobante

    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "FrmRPT_TotalesComprobante"

    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load

        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty
        Dim strConexion As String = String.Empty
        Dim sqlconexion As SqlConnection = Nothing 'conexión
        Dim DATotalesComprobante As SqlDataAdapter = Nothing
        Dim dset_TotalesComprobante As DataSet = Nothing
        Dim sql_TotalesComprobante As String = String.Empty
        Dim report_TotalesComprobante As RPT_TotalesComprobante = Nothing
        Try
            dset_TotalesComprobante = New DataSet
            strConexion = RNConeccion.ObtieneCadenaConexion()
            sqlconexion = New SqlConnection(strConexion)
            sql_TotalesComprobante = "Select * From masientos as TotalesComprobante"
            If SIMF_CONTA.sql_reporte <> "" Then
                sql_TotalesComprobante += " where " & SIMF_CONTA.sql_reporte
            End If
            DATotalesComprobante = New SqlDataAdapter(sql_TotalesComprobante, sqlconexion)
            DATotalesComprobante.Fill(dset_TotalesComprobante, "TotalesComprobante")

            report_TotalesComprobante = New RPT_TotalesComprobante
            report_TotalesComprobante.SetDataSource(dset_TotalesComprobante)

            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_TotalesComprobante.SetParameterValue(nombreformula, valor)
                End If
            Next
            report_TotalesComprobante.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_TotalesComprobante
            Me.CrystalReportViewer1.Refresh()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error " & ex.Message, " Comprobantes Contables", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
End Class