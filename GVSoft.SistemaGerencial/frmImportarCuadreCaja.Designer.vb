<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportarCuadreCaja
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportarCuadreCaja))
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtMensajeError = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.cbNumerosCuadre = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdEliminarArqueo = New DevComponents.DotNetBar.ButtonX()
        Me.txtGastos = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtDeposito = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtSaldoInicial = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtReciboFinal = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtReciboInicial = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFacturaFinal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtFacturaInicial = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvxDetalleCuadreCaja = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lblFechaArqueo = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtMontoTotalCuadres = New System.Windows.Forms.TextBox()
        Me.txtTotalCantidadCuadre = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.cmdImportar = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.NumerosFacturas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumerosRecibos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoRecibo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvxDetalleCuadreCaja, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Importar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Procesar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtMensajeError)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.cbNumerosCuadre)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(743, 62)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas en Carga de la Agencia"
        '
        'txtMensajeError
        '
        '
        '
        '
        Me.txtMensajeError.Border.Class = "TextBoxBorder"
        Me.txtMensajeError.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMensajeError.Location = New System.Drawing.Point(188, 12)
        Me.txtMensajeError.Multiline = True
        Me.txtMensajeError.Name = "txtMensajeError"
        Me.txtMensajeError.Size = New System.Drawing.Size(547, 43)
        Me.txtMensajeError.TabIndex = 6
        Me.txtMensajeError.WatermarkColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.txtMensajeError.WatermarkText = "<b><i><font color=""#CF7B79"">Mensajes de Error</font></i></b>"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(56, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(0, 13)
        Me.Label18.TabIndex = 5
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(140, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(20, 21)
        Me.ComboBox2.TabIndex = 4
        Me.ComboBox2.Text = "ComboBox2"
        Me.ComboBox2.Visible = False
        '
        'cbNumerosCuadre
        '
        Me.cbNumerosCuadre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbNumerosCuadre.Location = New System.Drawing.Point(56, 21)
        Me.cbNumerosCuadre.Name = "cbNumerosCuadre"
        Me.cbNumerosCuadre.Size = New System.Drawing.Size(126, 21)
        Me.cbNumerosCuadre.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Cuadre"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdEliminarArqueo)
        Me.GroupBox2.Controls.Add(Me.txtGastos)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.txtDeposito)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtSaldoInicial)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtReciboFinal)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtReciboInicial)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.txtFacturaFinal)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtFacturaInicial)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(2, 133)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(743, 86)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Encabezado del Cuadre de Caja"
        '
        'cmdEliminarArqueo
        '
        Me.cmdEliminarArqueo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEliminarArqueo.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdEliminarArqueo.Location = New System.Drawing.Point(580, 49)
        Me.cmdEliminarArqueo.Name = "cmdEliminarArqueo"
        Me.cmdEliminarArqueo.Size = New System.Drawing.Size(133, 26)
        Me.cmdEliminarArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdEliminarArqueo.TabIndex = 41
        Me.cmdEliminarArqueo.Text = "Quitar Arqueo Existente del Import"
        '
        'txtGastos
        '
        Me.txtGastos.Location = New System.Drawing.Point(632, 22)
        Me.txtGastos.Name = "txtGastos"
        Me.txtGastos.Size = New System.Drawing.Size(81, 20)
        Me.txtGastos.TabIndex = 40
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(577, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(46, 13)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Gastos"
        '
        'txtDeposito
        '
        Me.txtDeposito.Location = New System.Drawing.Point(482, 56)
        Me.txtDeposito.Name = "txtDeposito"
        Me.txtDeposito.Size = New System.Drawing.Size(81, 20)
        Me.txtDeposito.TabIndex = 38
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(419, 58)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 37
        Me.Label8.Text = "Deposito"
        '
        'txtSaldoInicial
        '
        Me.txtSaldoInicial.Location = New System.Drawing.Point(482, 21)
        Me.txtSaldoInicial.Name = "txtSaldoInicial"
        Me.txtSaldoInicial.Size = New System.Drawing.Size(81, 20)
        Me.txtSaldoInicial.TabIndex = 36
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(399, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(77, 13)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Saldo Inicial"
        '
        'txtReciboFinal
        '
        Me.txtReciboFinal.Location = New System.Drawing.Point(294, 52)
        Me.txtReciboFinal.Name = "txtReciboFinal"
        Me.txtReciboFinal.Size = New System.Drawing.Size(96, 20)
        Me.txtReciboFinal.TabIndex = 34
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(207, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Recibo Final"
        '
        'txtReciboInicial
        '
        Me.txtReciboInicial.Location = New System.Drawing.Point(104, 51)
        Me.txtReciboInicial.Name = "txtReciboInicial"
        Me.txtReciboInicial.Size = New System.Drawing.Size(95, 20)
        Me.txtReciboInicial.TabIndex = 32
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(85, 13)
        Me.Label5.TabIndex = 31
        Me.Label5.Text = "Recibo Inicial"
        '
        'txtFacturaFinal
        '
        Me.txtFacturaFinal.Location = New System.Drawing.Point(294, 21)
        Me.txtFacturaFinal.Name = "txtFacturaFinal"
        Me.txtFacturaFinal.Size = New System.Drawing.Size(96, 20)
        Me.txtFacturaFinal.TabIndex = 30
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(207, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 13)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Factura Final"
        '
        'txtFacturaInicial
        '
        Me.txtFacturaInicial.Location = New System.Drawing.Point(104, 20)
        Me.txtFacturaInicial.Name = "txtFacturaInicial"
        Me.txtFacturaInicial.Size = New System.Drawing.Size(95, 20)
        Me.txtFacturaInicial.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Factura Inicial"
        '
        'CheckBox1
        '
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.Location = New System.Drawing.Point(558, 11)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox1.TabIndex = 25
        Me.CheckBox1.Text = "Eliminar"
        Me.CheckBox1.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvxDetalleCuadreCaja)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(2, 221)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(743, 123)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle del Cuadre de Caja"
        '
        'dgvxDetalleCuadreCaja
        '
        Me.dgvxDetalleCuadreCaja.AllowUserToAddRows = False
        Me.dgvxDetalleCuadreCaja.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvxDetalleCuadreCaja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxDetalleCuadreCaja.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumerosFacturas, Me.MontoFactura, Me.NumerosRecibos, Me.MontoRecibo})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxDetalleCuadreCaja.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvxDetalleCuadreCaja.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxDetalleCuadreCaja.Location = New System.Drawing.Point(8, 16)
        Me.dgvxDetalleCuadreCaja.Name = "dgvxDetalleCuadreCaja"
        Me.dgvxDetalleCuadreCaja.Size = New System.Drawing.Size(726, 101)
        Me.dgvxDetalleCuadreCaja.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(2, 352)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(743, 90)
        Me.GroupBox4.TabIndex = 19
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Resumen de la Importación"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lblFechaArqueo)
        Me.GroupBox5.Controls.Add(Me.Label11)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.txtMontoTotalCuadres)
        Me.GroupBox5.Controls.Add(Me.txtTotalCantidadCuadre)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Location = New System.Drawing.Point(11, 18)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(720, 61)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Cantidad Cuadre"
        '
        'lblFechaArqueo
        '
        Me.lblFechaArqueo.AutoSize = True
        Me.lblFechaArqueo.Location = New System.Drawing.Point(559, 27)
        Me.lblFechaArqueo.Name = "lblFechaArqueo"
        Me.lblFechaArqueo.Size = New System.Drawing.Size(14, 13)
        Me.lblFechaArqueo.TabIndex = 18
        Me.lblFechaArqueo.Text = "F"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(452, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(86, 13)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Fecha Arqueo"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(206, 27)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(125, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Monto Total Cuadres"
        '
        'txtMontoTotalCuadres
        '
        Me.txtMontoTotalCuadres.Location = New System.Drawing.Point(344, 24)
        Me.txtMontoTotalCuadres.Name = "txtMontoTotalCuadres"
        Me.txtMontoTotalCuadres.ReadOnly = True
        Me.txtMontoTotalCuadres.Size = New System.Drawing.Size(88, 20)
        Me.txtMontoTotalCuadres.TabIndex = 16
        Me.txtMontoTotalCuadres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalCantidadCuadre
        '
        Me.txtTotalCantidadCuadre.Location = New System.Drawing.Point(110, 25)
        Me.txtTotalCantidadCuadre.Name = "txtTotalCantidadCuadre"
        Me.txtTotalCantidadCuadre.ReadOnly = True
        Me.txtTotalCantidadCuadre.Size = New System.Drawing.Size(90, 20)
        Me.txtTotalCantidadCuadre.TabIndex = 14
        Me.txtTotalCantidadCuadre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Cantidad Cuadre"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.Bump
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdImportar, Me.cmdProcesar, Me.cmdSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 45)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(746, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.bHerramientas.TabIndex = 20
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Importar"
        '
        'cmdImportar
        '
        Me.cmdImportar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdImportar.Image = CType(resources.GetObject("cmdImportar.Image"), System.Drawing.Image)
        Me.cmdImportar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportar.Name = "cmdImportar"
        Me.cmdImportar.Text = "Importar<F3>"
        Me.cmdImportar.Tooltip = "Buscar Archivo a Importar"
        '
        'cmdProcesar
        '
        Me.cmdProcesar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Procesar<F4>"
        Me.cmdProcesar.Tooltip = "Procesar Archivo"
        '
        'cmdSalir
        '
        Me.cmdSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdSalir.Image = CType(resources.GetObject("cmdSalir.Image"), System.Drawing.Image)
        Me.cmdSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdSalir.Name = "cmdSalir"
        Me.cmdSalir.Text = "Salir<ESC>"
        Me.cmdSalir.Tooltip = "Salir de la Ventana"
        '
        'NumerosFacturas
        '
        Me.NumerosFacturas.HeaderText = "Numero Factura"
        Me.NumerosFacturas.Name = "NumerosFacturas"
        Me.NumerosFacturas.ReadOnly = True
        Me.NumerosFacturas.Width = 200
        '
        'MontoFactura
        '
        Me.MontoFactura.HeaderText = "Monto de la Factura"
        Me.MontoFactura.Name = "MontoFactura"
        Me.MontoFactura.ReadOnly = True
        Me.MontoFactura.Width = 150
        '
        'NumerosRecibos
        '
        Me.NumerosRecibos.HeaderText = "Numero Recibo"
        Me.NumerosRecibos.Name = "NumerosRecibos"
        Me.NumerosRecibos.ReadOnly = True
        Me.NumerosRecibos.Width = 200
        '
        'MontoRecibo
        '
        Me.MontoRecibo.HeaderText = "Monto del Recibo"
        Me.MontoRecibo.Name = "MontoRecibo"
        Me.MontoRecibo.Width = 120
        '
        'frmImportarCuadreCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(746, 446)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmImportarCuadreCaja"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Importar Cuadre Caja"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvxDetalleCuadreCaja, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents cbNumerosCuadre As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFacturaInicial As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvxDetalleCuadreCaja As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents txtMensajeError As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtMontoTotalCuadres As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalCantidadCuadre As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdImportar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents txtDeposito As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtSaldoInicial As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtReciboFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtReciboInicial As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtFacturaFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtGastos As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmdEliminarArqueo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lblFechaArqueo As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents NumerosFacturas As DataGridViewTextBoxColumn
    Friend WithEvents MontoFactura As DataGridViewTextBoxColumn
    Friend WithEvents NumerosRecibos As DataGridViewTextBoxColumn
    Friend WithEvents MontoRecibo As DataGridViewTextBoxColumn
End Class
