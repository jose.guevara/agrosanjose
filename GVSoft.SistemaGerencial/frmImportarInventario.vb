Imports System.IO
Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv

Public Class frmImportarInventario
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(200, 32)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 32)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Salir"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(72, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Importar"
        '
        'frmImportarInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(336, 93)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportarInventario"
        Me.Text = "frmImportarInventario"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmImportarInventario"

    Private Sub frmImportarInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0

    End Sub

    Private Sub frmImportarInventario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click

        If sender.name = "Button1" Then
            ProcesarArchivo()
        ElseIf sender.name = "Button2" Then
            Me.Close()
        End If

    End Sub

    Sub ProcesarArchivo()

        Dim intAgencia, intNumFecha As Integer
        Dim dblCantidad As Double
        Dim strLine As String
        Dim dlgNew As New OpenFileDialog
        Dim strCodigo, strDescrip, strUnidad As String
        Dim strArchivo, strArchivo2, strClave, strComando As String

        dlgNew.InitialDirectory = "C:\"
        dlgNew.Filter = "ZIP files (*.zip)|*.zip"
        dlgNew.RestoreDirectory = True
        If dlgNew.ShowDialog() <> DialogResult.OK Then
            MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
            Exit Sub
        End If
        strComando = ""
        strArchivo = ""
        strArchivo2 = ""
        strClave = ""
        intIncr = 0
        intIncr = InStr(dlgNew.FileName, "inventario_")
        strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
        If intIncr = 0 Then
            MsgBox("No es un archivo que contiene el estado del inventario. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
            Exit Sub
        End If
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        strArchivo = dlgNew.FileName
        strClave = "Agro2K_2008"
        intIncr = 0
        intIncr = InStrRev(dlgNew.FileName, "\")
        strUnidad = ""
        strUnidad = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
        'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt c:\", vbNormalFocus, True)
        'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt c:\", vbNormalFocus, True)
        ' intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strUnidad & """", vbNormalFocus, True)
        If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
            MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
            Exit Sub
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If

        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "truncate table tmp_EstadoInvent"
        Try
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
        Catch exc As Exception
            intError = 1
            MsgBox(exc.Message.ToString)
        End Try
        Dim sr As StreamReader = File.OpenText(strArchivo2)
        intIncr = 0
        intError = 0
        If intError = 0 Then
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                intIncr = InStr(1, strLine, "|")
                strCodigo = ""
                strCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strDescrip = ""
                strDescrip = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strUnidad = ""
                strUnidad = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblCantidad = 0
                dblCantidad = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                intAgencia = 0
                intAgencia = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intNumFecha = 0
                intNumFecha = CInt(strLine)
                strQuery = ""
                strQuery = "insert into tmp_EstadoInvent values ('" & strCodigo & "', '" & strDescrip & "', "
                strQuery = strQuery + "'" & strUnidad & "', " & dblCantidad & ", '', '', 0, " & intAgencia & ", " & intNumFecha & ")"
                Try
                    cmdAgro2K.CommandText = strQuery
                    cmdAgro2K.ExecuteNonQuery()
                    IngresarCorreccionIF(strCodigo, intAgencia.ToString(), dblCantidad)

                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Exit Do
                End Try
            Loop
        End If
        sr.Close()
        If File.Exists(strArchivo2) = True Then
            File.Delete(strArchivo2)
        End If
        intNumFechaEstInv = intNumFecha
        strQuery = ""
        strQuery = "Select * From prm_Agencias Where Codigo = " & intAgencia & ""
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            strNombAgenEstInv = dtrAgro2K.GetValue(2)
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "exec sp_PrcEstadoInventario"
        Try
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
        Catch exc As Exception
            intError = 1
            MsgBox(exc.Message.ToString)
        End Try
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptCtasCbr = 0
        intRptPresup = 0
        intRptTipoPrecio = 0
        intRptExportar = 1
        intRptOtros = 3
        Dim frmNew As New actrptViewer
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
        If intError = 0 Then
            MsgBox("Finalizado satisfactoriamente la revisión del estado del inventario.", MsgBoxStyle.Information, "Proceso Finalizado")
        Else
            MsgBox("Hubieron errores al importar el archivo de estado del inventario.", MsgBoxStyle.Critical, "Error en la Importación")
        End If
        strQuery = ""
        strQuery = "truncate table tmp_EstadoInvent"
        Try
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
        Catch exc As Exception
            intError = 1
            MsgBox(exc.Message.ToString)
        End Try
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Public Sub IngresarCorreccionIF(ByVal CodProducto As String, ByVal CodAgnecia As String, ByVal Cantidad As Double)
        Dim NumFecha, NumTiempo, IdAgencia, IdProducto As Integer
        Dim strFecha As String
        Dim TotalFac, Costo, Saldo As Double

        Try
            dtrAgro2K = RNProduto.ObtenerCamposImportarCorreccionIF(CodProducto, CodAgnecia, Cantidad)
            While dtrAgro2K.Read
                IdAgencia = dtrAgro2K.Item("IdAgencia")
                IdProducto = dtrAgro2K.Item("registro")
                TotalFac = dtrAgro2K.Item("TotalFact")
                Costo = dtrAgro2K.Item("PrecioCostoCOR")
                Saldo = dtrAgro2K.Item("Saldo")
            End While
            NumFecha = Format(Now, "yyyyMMdd")
            NumTiempo = Format(Now, "Hmmss")
            strFecha = Format(Now, "dd-MMM-yyyy")
            dtrAgro2K.Close()
            RNProduto.IngresaMovimientoCorreccionIF(0, NumFecha, NumTiempo, IdAgencia, IdProducto, strFecha, "", _
                                                     "IF", "COR", TotalFac, Cantidad, Costo, Saldo, "", "", lngRegUsuario, _
                                                     Cantidad, IdProducto, 0, 0)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Exit Sub
            'MessageBoxEx.Show("Error en la Correccion: " & ex.Message, " Guardar Inventario Fisico ", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try




    End Sub

End Class
