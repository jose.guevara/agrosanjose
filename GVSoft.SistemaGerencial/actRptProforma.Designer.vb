<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actRptProforma 
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actRptProforma))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader()
        Me.lblTelefonoAgencia = New DataDynamics.ActiveReports.Label()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.lblDireccionAgencia = New DataDynamics.ActiveReports.Label()
        Me.Picture1 = New DataDynamics.ActiveReports.Picture()
        Me.Picture2 = New DataDynamics.ActiveReports.Picture()
        Me.Detail1 = New DataDynamics.ActiveReports.Detail()
        Me.txtCantidad = New DataDynamics.ActiveReports.TextBox()
        Me.txtCodigo = New DataDynamics.ActiveReports.TextBox()
        Me.txtDescripcionProducto = New DataDynamics.ActiveReports.TextBox()
        Me.txtImpuesto = New DataDynamics.ActiveReports.TextBox()
        Me.txtPrecioUnitario = New DataDynamics.ActiveReports.TextBox()
        Me.txtTotal = New DataDynamics.ActiveReports.TextBox()
        Me.txtUnidad = New DataDynamics.ActiveReports.TextBox()
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter()
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox()
        Me.Label23 = New DataDynamics.ActiveReports.Label()
        Me.TextBox23 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox()
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader()
        Me.Shape1 = New DataDynamics.ActiveReports.Shape()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.Label26 = New DataDynamics.ActiveReports.Label()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label15 = New DataDynamics.ActiveReports.Label()
        Me.Label16 = New DataDynamics.ActiveReports.Label()
        Me.Label17 = New DataDynamics.ActiveReports.Label()
        Me.Label18 = New DataDynamics.ActiveReports.Label()
        Me.Label19 = New DataDynamics.ActiveReports.Label()
        Me.Label20 = New DataDynamics.ActiveReports.Label()
        Me.Label21 = New DataDynamics.ActiveReports.Label()
        Me.Label22 = New DataDynamics.ActiveReports.Label()
        Me.lblDias = New DataDynamics.ActiveReports.Label()
        Me.lblFechaVence = New DataDynamics.ActiveReports.Label()
        Me.Label27 = New DataDynamics.ActiveReports.Label()
        Me.Line1 = New DataDynamics.ActiveReports.Line()
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.Label14 = New DataDynamics.ActiveReports.Label()
        Me.Label28 = New DataDynamics.ActiveReports.Label()
        Me.lblSubTotal = New DataDynamics.ActiveReports.Label()
        Me.lblImpVentas = New DataDynamics.ActiveReports.Label()
        Me.lblRetencion = New DataDynamics.ActiveReports.Label()
        Me.Line2 = New DataDynamics.ActiveReports.Line()
        Me.Label32 = New DataDynamics.ActiveReports.Label()
        Me.lblTotal = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Line3 = New DataDynamics.ActiveReports.Line()
        CType(Me.lblTelefonoAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDireccionAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDescripcionProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtImpuesto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrecioUnitario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFechaVence, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSubTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblImpVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblRetencion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblTelefonoAgencia, Me.Label, Me.Label5, Me.lblDireccionAgencia, Me.Picture1, Me.Picture2})
        Me.PageHeader1.Height = 1.572917!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblTelefonoAgencia
        '
        Me.lblTelefonoAgencia.Height = 0.25!
        Me.lblTelefonoAgencia.HyperLink = Nothing
        Me.lblTelefonoAgencia.Left = 0!
        Me.lblTelefonoAgencia.Name = "lblTelefonoAgencia"
        Me.lblTelefonoAgencia.Style = "ddo-char-set: 0; text-align: center; font-weight: normal; font-style: italic; fon" &
    "t-size: 12pt; font-family: Times New Roman"
        Me.lblTelefonoAgencia.Text = "TELEFONO:"
        Me.lblTelefonoAgencia.Top = 1.01!
        Me.lblTelefonoAgencia.Width = 7.375!
        '
        'Label
        '
        Me.Label.Height = 0.375!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: center; font-weight: bold; font-size: 20.25pt; font-family: Times New" &
    " Roman"
        Me.Label.Text = "Agro Centro, S.A."
        Me.Label.Top = 0!
        Me.Label.Width = 7.375!
        '
        'Label5
        '
        Me.Label5.Height = 0.25!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "text-align: center; font-weight: bold; font-size: 15.75pt; font-family: Times New" &
    " Roman"
        Me.Label5.Text = "PROFORMAS"
        Me.Label5.Top = 1.283!
        Me.Label5.Width = 7.375!
        '
        'lblDireccionAgencia
        '
        Me.lblDireccionAgencia.Height = 0.54!
        Me.lblDireccionAgencia.HyperLink = Nothing
        Me.lblDireccionAgencia.Left = 1.22!
        Me.lblDireccionAgencia.Name = "lblDireccionAgencia"
        Me.lblDireccionAgencia.Style = "ddo-char-set: 0; text-align: center; font-weight: normal; font-style: italic; fon" &
    "t-size: 12pt; font-family: Times New Roman"
        Me.lblDireccionAgencia.Text = "DIRECCION:  "
        Me.lblDireccionAgencia.Top = 0.427!
        Me.lblDireccionAgencia.Width = 4.925!
        '
        'Picture1
        '
        Me.Picture1.Height = 1.0!
        Me.Picture1.ImageData = CType(resources.GetObject("Picture1.ImageData"), System.IO.Stream)
        Me.Picture1.Left = 6.25!
        Me.Picture1.LineWeight = 0!
        Me.Picture1.Name = "Picture1"
        Me.Picture1.Top = 0!
        Me.Picture1.Width = 1.125!
        '
        'Picture2
        '
        Me.Picture2.Height = 1.0!
        Me.Picture2.ImageData = CType(resources.GetObject("Picture2.ImageData"), System.IO.Stream)
        Me.Picture2.Left = 0.0625!
        Me.Picture2.LineWeight = 0!
        Me.Picture2.Name = "Picture2"
        Me.Picture2.Top = 0!
        Me.Picture2.Width = 1.125!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtCantidad, Me.txtCodigo, Me.txtDescripcionProducto, Me.txtImpuesto, Me.txtPrecioUnitario, Me.txtTotal, Me.txtUnidad})
        Me.Detail1.Height = 0.25!
        Me.Detail1.Name = "Detail1"
        '
        'txtCantidad
        '
        Me.txtCantidad.DataField = "Cantidad"
        Me.txtCantidad.Height = 0.1875!
        Me.txtCantidad.Left = 0.0625!
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman"
        Me.txtCantidad.Text = Nothing
        Me.txtCantidad.Top = 0!
        Me.txtCantidad.Width = 0.5625!
        '
        'txtCodigo
        '
        Me.txtCodigo.DataField = "Codigo"
        Me.txtCodigo.Height = 0.1875!
        Me.txtCodigo.Left = 1.5625!
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Style = "text-align: left; font-size: 9pt; font-family: Times New Roman"
        Me.txtCodigo.Text = Nothing
        Me.txtCodigo.Top = 0!
        Me.txtCodigo.Width = 0.6875!
        '
        'txtDescripcionProducto
        '
        Me.txtDescripcionProducto.DataField = "DescProductos"
        Me.txtDescripcionProducto.Height = 0.1875!
        Me.txtDescripcionProducto.Left = 2.3125!
        Me.txtDescripcionProducto.Name = "txtDescripcionProducto"
        Me.txtDescripcionProducto.OutputFormat = resources.GetString("txtDescripcionProducto.OutputFormat")
        Me.txtDescripcionProducto.Style = "text-align: left; font-size: 9pt; font-family: Times New Roman"
        Me.txtDescripcionProducto.Text = Nothing
        Me.txtDescripcionProducto.Top = 0!
        Me.txtDescripcionProducto.Width = 2.625!
        '
        'txtImpuesto
        '
        Me.txtImpuesto.DataField = "Igv"
        Me.txtImpuesto.Height = 0.1875!
        Me.txtImpuesto.Left = 5.0!
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman"
        Me.txtImpuesto.Text = Nothing
        Me.txtImpuesto.Top = 0!
        Me.txtImpuesto.Width = 0.5625!
        '
        'txtPrecioUnitario
        '
        Me.txtPrecioUnitario.DataField = "Precio"
        Me.txtPrecioUnitario.Height = 0.1875!
        Me.txtPrecioUnitario.Left = 5.5625!
        Me.txtPrecioUnitario.Name = "txtPrecioUnitario"
        Me.txtPrecioUnitario.OutputFormat = resources.GetString("txtPrecioUnitario.OutputFormat")
        Me.txtPrecioUnitario.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman"
        Me.txtPrecioUnitario.Text = Nothing
        Me.txtPrecioUnitario.Top = 0!
        Me.txtPrecioUnitario.Width = 0.875!
        '
        'txtTotal
        '
        Me.txtTotal.DataField = "Valor"
        Me.txtTotal.Height = 0.1875!
        Me.txtTotal.Left = 6.4375!
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat")
        Me.txtTotal.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman"
        Me.txtTotal.Text = Nothing
        Me.txtTotal.Top = 0!
        Me.txtTotal.Width = 1.0!
        '
        'txtUnidad
        '
        Me.txtUnidad.DataField = "Unidad"
        Me.txtUnidad.Height = 0.1875!
        Me.txtUnidad.Left = 0.875!
        Me.txtUnidad.Name = "txtUnidad"
        Me.txtUnidad.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman"
        Me.txtUnidad.Text = Nothing
        Me.txtUnidad.Top = 0!
        Me.txtUnidad.Width = 0.5!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox26, Me.Label23, Me.TextBox23, Me.TextBox21})
        Me.PageFooter1.Height = 0.6041667!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'TextBox26
        '
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 7.0!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox26.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox26.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.4375!
        Me.TextBox26.Width = 0.3499999!
        '
        'Label23
        '
        Me.Label23.Height = 0.2!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 6.4375!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-weight: bold; font-size: 8.25pt; font-family: Times New Roman"
        Me.Label23.Text = "P�gina"
        Me.Label23.Top = 0.4375!
        Me.Label23.Width = 0.5500001!
        '
        'TextBox23
        '
        Me.TextBox23.Height = 0.1875!
        Me.TextBox23.Left = 3.0!
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Style = "text-align: center; font-weight: bold; font-style: italic; font-size: 8.25pt; fon" &
    "t-family: Times New Roman"
        Me.TextBox23.Text = "TextBox23"
        Me.TextBox23.Top = 0.4375!
        Me.TextBox23.Width = 2.0!
        '
        'TextBox21
        '
        Me.TextBox21.Height = 0.1875!
        Me.TextBox21.Left = 0!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: center; font-weight: bold; font-style: italic; font-size: 8.25pt; fon" &
    "t-family: Times New Roman"
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0.4375!
        Me.TextBox21.Width = 2.0!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Shape1, Me.Label3, Me.Label1, Me.Label4, Me.Label2, Me.Label8, Me.Label12, Me.Label9, Me.Label26, Me.Label10, Me.Label11, Me.Label15, Me.Label16, Me.Label17, Me.Label18, Me.Label19, Me.Label20, Me.Label21, Me.Label22, Me.lblDias, Me.lblFechaVence, Me.Label27, Me.Line1})
        Me.GroupHeader1.DataField = "NumeroProforma"
        Me.GroupHeader1.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.GroupHeader1.Height = 1.53125!
        Me.GroupHeader1.KeepTogether = True
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'Shape1
        '
        Me.Shape1.Height = 0.9375!
        Me.Shape1.Left = 0!
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = 9.999999!
        Me.Shape1.Style = DataDynamics.ActiveReports.ShapeType.RoundRect
        Me.Shape1.Top = 0!
        Me.Shape1.Width = 7.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.0625!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label3.Text = "Fecha de Emision:"
        Me.Label3.Top = 0.0625!
        Me.Label3.Width = 1.0625!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 4.25!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label1.Text = "Vendedor:"
        Me.Label1.Top = 0.0625!
        Me.Label1.Width = 0.625!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 2.1875!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label4.Text = "Numero Proforma:"
        Me.Label4.Top = 0.0625!
        Me.Label4.Width = 1.0625!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.0625!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label2.Text = "Cantidad"
        Me.Label2.Top = 1.25!
        Me.Label2.Width = 0.5625!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.875!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label8.Text = "Unidad"
        Me.Label8.Top = 1.25!
        Me.Label8.Width = 0.5!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 1.5625!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" &
    "an"
        Me.Label12.Text = "C�digo"
        Me.Label12.Top = 1.25!
        Me.Label12.Width = 0.6875!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 2.3125!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" &
    "an"
        Me.Label9.Text = "Producto"
        Me.Label9.Top = 1.25!
        Me.Label9.Width = 2.625!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 5.0!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" &
    "an"
        Me.Label26.Text = "Impuesto"
        Me.Label26.Top = 1.25!
        Me.Label26.Width = 0.5625!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 5.5625!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label10.Text = "Precio Unitario"
        Me.Label10.Top = 1.25!
        Me.Label10.Width = 0.875!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 6.4375!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" &
    "an"
        Me.Label11.Text = "Total"
        Me.Label11.Top = 1.25!
        Me.Label11.Width = 1.0!
        '
        'Label15
        '
        Me.Label15.DataField = "FechaIngreso"
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 1.125!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label15.Text = ""
        Me.Label15.Top = 0.0625!
        Me.Label15.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.DataField = "NumeroProforma"
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.25!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label16.Text = ""
        Me.Label16.Top = 0.0625!
        Me.Label16.Width = 0.9375!
        '
        'Label17
        '
        Me.Label17.DataField = "Nombre"
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 4.875!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label17.Text = ""
        Me.Label17.Top = 0.0625!
        Me.Label17.Width = 2.5!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 0.0625!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label18.Text = "Cliente:"
        Me.Label18.Top = 0.375!
        Me.Label18.Width = 0.625!
        '
        'Label19
        '
        Me.Label19.DataField = "Cliente"
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0.6875!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label19.Text = ""
        Me.Label19.Top = 0.375!
        Me.Label19.Width = 2.5625!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 4.25!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label20.Text = "Direccion:"
        Me.Label20.Top = 0.375!
        Me.Label20.Width = 0.625!
        '
        'Label21
        '
        Me.Label21.DataField = "Direccion"
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 4.875!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label21.Text = ""
        Me.Label21.Top = 0.375!
        Me.Label21.Width = 2.5!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 0.0625!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label22.Text = "Dias Validos:"
        Me.Label22.Top = 0.6875!
        Me.Label22.Width = 0.75!
        '
        'lblDias
        '
        Me.lblDias.DataField = "DiasCredito"
        Me.lblDias.Height = 0.1875!
        Me.lblDias.HyperLink = Nothing
        Me.lblDias.Left = 0.8125!
        Me.lblDias.Name = "lblDias"
        Me.lblDias.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.lblDias.Text = ""
        Me.lblDias.Top = 0.6875!
        Me.lblDias.Width = 0.375!
        '
        'lblFechaVence
        '
        Me.lblFechaVence.DataField = "FechaVence"
        Me.lblFechaVence.Height = 0.1875!
        Me.lblFechaVence.HyperLink = Nothing
        Me.lblFechaVence.Left = 2.625!
        Me.lblFechaVence.Name = "lblFechaVence"
        Me.lblFechaVence.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.lblFechaVence.Text = ""
        Me.lblFechaVence.Top = 0.6875!
        Me.lblFechaVence.Width = 1.0625!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 1.375!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label27.Text = "Fecha de Vencimiento:"
        Me.Label27.Top = 0.6875!
        Me.Label27.Width = 1.25!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.5!
        Me.Line1.Width = 7.5!
        Me.Line1.X1 = 7.5!
        Me.Line1.X2 = 0!
        Me.Line1.Y1 = 1.5!
        Me.Line1.Y2 = 1.5!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.Label14, Me.Label28, Me.lblSubTotal, Me.lblImpVentas, Me.lblRetencion, Me.Line2, Me.Label32, Me.lblTotal, Me.Label6, Me.Line3})
        Me.GroupFooter1.Height = 1.177083!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.NewPage = DataDynamics.ActiveReports.NewPage.After
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 5.375!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label13.Text = "Sub Total:"
        Me.Label13.Top = 0.0625!
        Me.Label13.Width = 1.0!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 5.375!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label14.Text = "Imp. Venta:"
        Me.Label14.Top = 0.3125!
        Me.Label14.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 5.375!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label28.Text = "Retencion:"
        Me.Label28.Top = 0.5625!
        Me.Label28.Width = 1.0!
        '
        'lblSubTotal
        '
        Me.lblSubTotal.DataField = "SubTotal"
        Me.lblSubTotal.Height = 0.1875!
        Me.lblSubTotal.HyperLink = Nothing
        Me.lblSubTotal.Left = 6.4375!
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.lblSubTotal.Text = ""
        Me.lblSubTotal.Top = 0.0625!
        Me.lblSubTotal.Width = 1.0!
        '
        'lblImpVentas
        '
        Me.lblImpVentas.DataField = "Impuesto"
        Me.lblImpVentas.Height = 0.1875!
        Me.lblImpVentas.HyperLink = Nothing
        Me.lblImpVentas.Left = 6.4375!
        Me.lblImpVentas.Name = "lblImpVentas"
        Me.lblImpVentas.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.lblImpVentas.Text = ""
        Me.lblImpVentas.Top = 0.3125!
        Me.lblImpVentas.Width = 1.0!
        '
        'lblRetencion
        '
        Me.lblRetencion.DataField = "Retencion"
        Me.lblRetencion.Height = 0.1875!
        Me.lblRetencion.HyperLink = Nothing
        Me.lblRetencion.Left = 6.4375!
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.lblRetencion.Text = ""
        Me.lblRetencion.Top = 0.5625!
        Me.lblRetencion.Width = 1.0!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0!
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0!
        Me.Line2.Width = 7.5!
        Me.Line2.X1 = 7.5!
        Me.Line2.X2 = 0!
        Me.Line2.Y1 = 0!
        Me.Line2.Y2 = 0!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 5.375!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label32.Text = "Total:"
        Me.Label32.Top = 0.8125!
        Me.Label32.Width = 1.0!
        '
        'lblTotal
        '
        Me.lblTotal.DataField = "Total"
        Me.lblTotal.Height = 0.1875!
        Me.lblTotal.HyperLink = Nothing
        Me.lblTotal.Left = 6.4375!
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.lblTotal.Text = ""
        Me.lblTotal.Top = 0.8125!
        Me.lblTotal.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 2.0!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" &
    "an"
        Me.Label6.Text = "Firma"
        Me.Label6.Top = 0.813!
        Me.Label6.Width = 1.0!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 1.0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.8125!
        Me.Line3.Width = 3.0!
        Me.Line3.X1 = 4.0!
        Me.Line3.X2 = 1.0!
        Me.Line3.Y1 = 0.8125!
        Me.Line3.Y2 = 0.8125!
        '
        'actRptProforma
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Left = 0.35!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.552083!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.lblTelefonoAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDireccionAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDescripcionProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtImpuesto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrecioUnitario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFechaVence, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSubTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblImpVentas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblRetencion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Private WithEvents Label1 As DataDynamics.ActiveReports.Label
    Private WithEvents Label4 As DataDynamics.ActiveReports.Label
    Private WithEvents Label2 As DataDynamics.ActiveReports.Label
    Private WithEvents Label8 As DataDynamics.ActiveReports.Label
    Private WithEvents Label12 As DataDynamics.ActiveReports.Label
    Private WithEvents Label9 As DataDynamics.ActiveReports.Label
    Private WithEvents Label26 As DataDynamics.ActiveReports.Label
    Private WithEvents Label10 As DataDynamics.ActiveReports.Label
    Private WithEvents Label11 As DataDynamics.ActiveReports.Label
    Private WithEvents Label15 As DataDynamics.ActiveReports.Label
    Private WithEvents Label16 As DataDynamics.ActiveReports.Label
    Private WithEvents Label17 As DataDynamics.ActiveReports.Label
    Private WithEvents Label18 As DataDynamics.ActiveReports.Label
    Private WithEvents Label19 As DataDynamics.ActiveReports.Label
    Private WithEvents Label20 As DataDynamics.ActiveReports.Label
    Private WithEvents Label21 As DataDynamics.ActiveReports.Label
    Private WithEvents Label22 As DataDynamics.ActiveReports.Label
    Private WithEvents lblDias As DataDynamics.ActiveReports.Label
    Private WithEvents lblFechaVence As DataDynamics.ActiveReports.Label
    Private WithEvents Label27 As DataDynamics.ActiveReports.Label
    Friend WithEvents Shape1 As DataDynamics.ActiveReports.Shape
    Private WithEvents txtCantidad As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtCodigo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDescripcionProducto As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtImpuesto As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtPrecioUnitario As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTotal As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtUnidad As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Line1 As DataDynamics.ActiveReports.Line
    Private WithEvents Label13 As DataDynamics.ActiveReports.Label
    Private WithEvents Label14 As DataDynamics.ActiveReports.Label
    Private WithEvents Label28 As DataDynamics.ActiveReports.Label
    Private WithEvents lblSubTotal As DataDynamics.ActiveReports.Label
    Private WithEvents lblImpVentas As DataDynamics.ActiveReports.Label
    Private WithEvents lblRetencion As DataDynamics.ActiveReports.Label
    Friend WithEvents Line2 As DataDynamics.ActiveReports.Line
    Private WithEvents Label32 As DataDynamics.ActiveReports.Label
    Private WithEvents lblTotal As DataDynamics.ActiveReports.Label
    Private WithEvents Label As DataDynamics.ActiveReports.Label
    Private WithEvents Label5 As DataDynamics.ActiveReports.Label
    Private WithEvents lblDireccionAgencia As DataDynamics.ActiveReports.Label
    Private WithEvents lblTelefonoAgencia As DataDynamics.ActiveReports.Label
    Friend WithEvents Picture1 As DataDynamics.ActiveReports.Picture
    Friend WithEvents Picture2 As DataDynamics.ActiveReports.Picture
    Private WithEvents Label6 As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Private WithEvents TextBox26 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label23 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox23 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox21 As DataDynamics.ActiveReports.TextBox
End Class 
