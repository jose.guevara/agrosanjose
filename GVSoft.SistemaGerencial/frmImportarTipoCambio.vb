Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmImportarTipoCambio
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmImportarParametros"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(200, 32)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 32)
        Me.Button2.TabIndex = 7
        Me.Button2.Text = "Salir"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(72, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Importar"
        '
        'frmImportarTipoCambio
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(336, 93)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportarTipoCambio"
        Me.Text = "frmImportarTasasCambio"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmImportarTipoCambio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0

    End Sub

    Private Sub frmImportarTipoCambio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click
        Try
            If sender.name = "Button1" Then
                ProcesarArchivo()
            ElseIf sender.name = "Button2" Then
                Me.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub ProcesarArchivo()

        Dim dlgNew As New OpenFileDialog
        Dim strLine As String

        Dim intFechaIng As Integer
        Dim strFechaIng As String
        Dim dblValor As Double
        Dim strArchivo, strArchivo2, strClave, strComando As String

        Try

            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
                Exit Sub
            End If
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "tcambio")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MsgBox("No es un archivo que contiene las tasas de cambio. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
                Exit Sub
            End If
            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt c:\", vbNormalFocus, True)
            'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt c:\", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
                Exit Sub
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            ' ''If cmdAgro2K.Connection.State = ConnectionState.Open Then
            ' ''    cmdAgro2K.Connection.Close()
            ' ''End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                intIncr = InStr(1, strLine, "|")
                strFechaIng = String.Empty
                strFechaIng = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                intFechaIng = 0
                'intFechaIng = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                intFechaIng = SUConversiones.ConvierteAInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                dblValor = 0
                'dblValor = CDbl(strLine)
                dblValor = SUConversiones.ConvierteADecimal(strLine)
                'strQuery = ""
                'strQuery = "delete from prm_tipocambio where numfecha = " & intFechaIng & ""
                'Try
                '    cmdAgro2K.CommandText = strQuery
                '    cmdAgro2K.ExecuteNonQuery()
                'Catch exc As Exception
                '    intError = 1
                '    MsgBox(exc.Message.ToString)
                '    Exit Do
                'End Try
                strQuery = ""
                ' strQuery = "insert into prm_tipocambio values ('" & strFechaIng & "', " & dblValor & ", " & intFechaIng & ")"
                strQuery = "exec sp_IngTipoCambio '" & strFechaIng & "', " & dblValor & ", " & intFechaIng & ",1"
                Try
                    cmdAgro2K.CommandText = strQuery
                    cmdAgro2K.ExecuteNonQuery()
                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Exit Do
                End Try
            Loop
            sr.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If
            If intError = 0 Then
                MsgBox("Finalizado satisfactoriamente la importación de las tasas de cambio.", MsgBoxStyle.Information, "Proceso Finalizado")
            Else
                MsgBox("Hubieron errores al importar el archivo de tasas de cambio.", MsgBoxStyle.Critical, "Error en la Importación")
            End If
            If cmdAgro2K.Connection IsNot Nothing Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

End Class
