Imports System.Data.SqlClient
Imports System.Text

Public Class frmNegocios
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cntmnuNegociosRg As System.Windows.Forms.ContextMenu
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown2 As System.Windows.Forms.NumericUpDown
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmNegocios))
        Me.cntmnuNegociosRg = New System.Windows.Forms.ContextMenu
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.NumericUpDown2 = New System.Windows.Forms.NumericUpDown
        Me.Label6 = New System.Windows.Forms.Label
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 104)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de los Negocios"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(136, 32)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(8, 20)
        Me.TextBox3.TabIndex = 7
        Me.TextBox3.Text = ""
        Me.TextBox3.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"activo", "inactivo", "eliminado"})
        Me.ComboBox1.Location = New System.Drawing.Point(376, 64)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(104, 21)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.Tag = "Estado del registro"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(328, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Estado"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 32)
        Me.TextBox1.MaxLength = 12
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(80, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "C�digo asignado al negocio"
        Me.TextBox1.Text = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Descripci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "C�digo"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(80, 64)
        Me.TextBox2.MaxLength = 30
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(232, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Descripci�n del c�digo del negocio ingresado"
        Me.TextBox2.Text = ""
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(488, 56)
        Me.ToolBar1.TabIndex = 12
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuNegociosRg
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.ToolBarButton4.ToolTipText = "Registros"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(31, 31)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8, Me.MenuItem10})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 4
        Me.MenuItem10.Text = "Igual"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem9})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 225)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(488, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 13
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 386
        '
        'Timer1
        '
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NumericUpDown2)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.ComboBox2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 168)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(480, 56)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "D�as de Cr�dito"
        '
        'NumericUpDown2
        '
        Me.NumericUpDown2.Location = New System.Drawing.Point(320, 24)
        Me.NumericUpDown2.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.NumericUpDown2.Name = "NumericUpDown2"
        Me.NumericUpDown2.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown2.TabIndex = 5
        Me.NumericUpDown2.Tag = "Si es negociable, cuantos d�as m�s puede darsele"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(272, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 16)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "M�ximo"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Items.AddRange(New Object() {"no", "si"})
        Me.ComboBox2.Location = New System.Drawing.Point(200, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(56, 21)
        Me.ComboBox2.TabIndex = 4
        Me.ComboBox2.Tag = "Es o no negociable la cantidad de d�as de c�dito ya establecido"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(128, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 16)
        Me.Label5.TabIndex = 8
        Me.Label5.Tag = "Si es negociable la cantidad de d�as establecidas"
        Me.Label5.Text = "Negociable"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(64, 24)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(48, 20)
        Me.NumericUpDown1.TabIndex = 3
        Me.NumericUpDown1.Tag = "Cantidad de d�as para un cr�dito"
        Me.NumericUpDown1.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Cantidad"
        '
        'frmNegocios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(488, 249)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.StatusBar1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmNegocios"
        Me.Text = "frmNegocios"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.NumericUpDown2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim strCambios(5) As String

    Private Sub frmNegocios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        CreateMyContextMenu()
        Limpiar()
        intModulo = 13

    End Sub

    Private Sub frmNegocios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = TextBox3.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = TextBox3.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
            Case 3
                MoverRegistro(sender.text)
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub Guardar()

        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngNegocios", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter

        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp03
            .ParameterName = "@strDescripcion"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox2.Text
        End With
        With prmTmp04
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox1.SelectedIndex
        End With
        With prmTmp05
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp06
            .ParameterName = "@intDias"
            .SqlDbType = SqlDbType.Int
            .Value = NumericUpDown1.Value
        End With
        With prmTmp07
            .ParameterName = "@intNegociable"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox2.SelectedIndex
        End With
        With prmTmp08
            .ParameterName = "@intMaximo"
            .SqlDbType = SqlDbType.Int
            .Value = NumericUpDown2.Value
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            lngRegistro = TextBox3.Text
            cmdTmp.Parameters(0).Value = lngRegistro
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        cmdTmp.Parameters(4).Value = 1

        Try
            cmdTmp.ExecuteNonQuery()
            If intResp = 6 Then
                If TextBox2.Text <> strCambios(0) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Descripci�n", strCambios(0), TextBox2.Text)
                End If
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Estado", strCambios(1), ComboBox1.SelectedItem)
                End If
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "D�as", strCambios(2), NumericUpDown1.Value)
                End If
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "Negociable", strCambios(3), ComboBox2.SelectedItem)
                End If
                If ComboBox1.SelectedItem <> strCambios(1) Then
                    Ing_Bitacora(intModulo, TextBox3.Text, "M�ximo", strCambios(4), NumericUpDown2.Value)
                End If
            ElseIf intResp = 0 Then
                Ing_Bitacora(intModulo, lngRegistro, "C�digo", "", TextBox1.Text)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar()

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = "0"
        ComboBox1.SelectedIndex = 0
        ComboBox2.SelectedIndex = 0
        NumericUpDown1.Value = 30
        NumericUpDown2.Value = 1
        NumericUpDown2.Visible = False
        strCambios(0) = ""
        strCambios(1) = ""
        strCambios(2) = ""
        strCambios(3) = ""
        strCambios(4) = ""
        TextBox1.Focus()

    End Sub

    Sub MoverRegistro(ByVal StrDato As String)

        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(Codigo) from prm_Negocios"
            Case "Anterior"
                strQuery = "Select Max(Codigo) from prm_Negocios Where codigo < '" & TextBox1.Text & "'"
            Case "Siguiente"
                strQuery = "Select Min(Codigo) from prm_Negocios Where codigo > '" & TextBox1.Text & "'"
            Case "Ultimo"
                strQuery = "Select Max(Codigo) from prm_Negocios"
            Case "Igual"
                strQuery = "Select Codigo from prm_Negocios Where codigo = '" & TextBox1.Text & "'"
                strCodigoTmp = ""
                strCodigoTmp = TextBox1.Text
        End Select
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim strCodigo As String

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        strCodigo = ""
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                strCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If strCodigo = "" Then
            If StrDato = "Igual" Then
                TextBox1.Text = strCodigoTmp
                MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
            Else
                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From prm_Negocios Where Codigo = '" & strCodigo & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox3.Text = dtrAgro2K.GetValue(0)
            TextBox1.Text = dtrAgro2K.GetValue(1)
            TextBox2.Text = dtrAgro2K.GetValue(2)
            ComboBox1.SelectedIndex = dtrAgro2K.GetValue(3)
            NumericUpDown1.Value = dtrAgro2K.GetValue(4)
            ComboBox2.SelectedIndex = dtrAgro2K.GetValue(5)
            NumericUpDown2.Value = dtrAgro2K.GetValue(6)
            strCambios(0) = TextBox2.Text
            strCambios(1) = ComboBox1.SelectedItem
            strCambios(2) = NumericUpDown1.Value
            strCambios(3) = ComboBox2.SelectedItem
            strCambios(4) = NumericUpDown2.Value
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem
        Dim cntmenuItem5 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"
        cntmenuItem5.Text = "Igual"

        cntmnuNegociosRg.MenuItems.Add(cntmenuItem1)
        cntmnuNegociosRg.MenuItems.Add(cntmenuItem2)
        cntmnuNegociosRg.MenuItems.Add(cntmenuItem3)
        cntmnuNegociosRg.MenuItems.Add(cntmenuItem4)
        cntmnuNegociosRg.MenuItems.Add(cntmenuItem5)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, ComboBox1.GotFocus, ComboBox2.GotFocus, NumericUpDown1.GotFocus, NumericUpDown2.GotFocus

        Dim strCampo As String = String.Empty

        Select Case sender.name.ToString
            Case "TextBox1" : strCampo = "C�digo"
            Case "TextBox2" : strCampo = "Descripci�n"
            Case "ComboBox1" : strCampo = "Estado"
            Case "NumericUpDown1" : strCampo = "D�as"
            Case "ComboBox2" : strCampo = "Negociable"
            Case "NumericUpDown2" : strCampo = "M�ximo"
        End Select
        If sender.name <> "NumericUpDown1" And sender.name <> "NumericUpDown2" Then
            sender.selectall()
        End If
        StatusBar1.Panels.Item(0).Text = strCampo
        StatusBar1.Panels.Item(1).Text = sender.tag

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, ComboBox1.KeyDown, ComboBox2.KeyDown, NumericUpDown1.KeyDown, NumericUpDown2.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "TextBox1" : MoverRegistro("Igual") : TextBox2.Focus()
                Case "TextBox2" : ComboBox1.Focus()
                Case "ComboBox1" : NumericUpDown1.Focus()
                Case "NumericUpDown1" : ComboBox2.Focus()
                Case "ComboBox2" : NumericUpDown2.Focus()
                Case "NumericUpDown2" : TextBox1.Focus()
            End Select
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                TextBox1.Text = strUbicar
                MoverRegistro("Igual")
            End If
        End If

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged

        If ComboBox2.SelectedIndex = 0 Then
            Label6.Visible = False
            NumericUpDown2.Visible = False
            NumericUpDown2.Value = 0
        ElseIf ComboBox2.SelectedIndex = 1 Then
            Label6.Visible = True
            NumericUpDown2.Visible = True
        End If

    End Sub

End Class
