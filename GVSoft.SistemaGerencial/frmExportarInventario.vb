Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmExportarInventario
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(168, 64)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 32)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Salir"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(32, 64)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Exportar"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker1.Location = New System.Drawing.Point(128, 16)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(40, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Fecha Exportar"
        '
        'frmExportarInventario
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(272, 109)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarInventario"
        Me.Text = "frmExportarInventario"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmExportarInventario"

    Private Sub frmExportarInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            UbicarAgencia(lngRegUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try



    End Sub

    Private Sub frmExportarInventario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click

        Try
            If sender.name = "Button1" Then
                ProcesarArchivo()
            ElseIf sender.name = "Button2" Then
                Me.Close()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Public Sub ProcesarArchivo()

        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim lsRutaCompletaArchivoZip As String = String.Empty

        Try


            Dim cmdTmp As New SqlCommand("sp_ExportarArchivos", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter

            Me.Cursor = Cursors.WaitCursor
            With prmTmp01
                .ParameterName = "@intExportar"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With prmTmp02
                .ParameterName = "@intAgencia"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegAgencia 'intAgenciaDefecto
            End With
            With prmTmp03
                .ParameterName = "@lngNumFechaIni"
                .SqlDbType = SqlDbType.Int
                .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
            End With
            With prmTmp04
                .ParameterName = "@lngNumFechaFin"
                .SqlDbType = SqlDbType.Int
                .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K

            'strClave = ""
            'strClave = "Agro2K_2008"
            strArchivo = ""
            strArchivo = ConfigurationManager.AppSettings("RutaArchivosInventario").ToString() & "inventario_" & Format(intAgenciaDefecto, "00") & ".txt"
            If File.Exists(strArchivo) = True Then
                File.Delete(strArchivo)
            End If
            strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosInventario").ToString() & "inventario_" & Format(intAgenciaDefecto, "00")
            Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosInventario").ToString() & "inventario_" & Format(intAgenciaDefecto, "00") & ".txt")
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                sr.WriteLine(dtrAgro2K.GetValue(0))
            End While
            sr.Close()
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Try
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                lsRutaCompletaArchivoZip = String.Empty
                lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
                If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                    MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                    Exit Sub
                End If

                If File.Exists(strArchivo) = True Then
                    File.Delete(strArchivo)
                End If
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                Exit Sub
            End Try
            MsgBox("Finaliz� la exportaci�n del estado de inventario", MsgBoxStyle.Information, "Exportar Datos")
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
    End Sub


End Class
