<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actrptSaldosDetalladosXProveedores 
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub
    
    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actrptSaldosDetalladosXProveedores))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.Label25 = New DataDynamics.ActiveReports.Label
        Me.Detail1 = New DataDynamics.ActiveReports.Detail
        Me.txtFechaEmisionRecibo = New DataDynamics.ActiveReports.TextBox
        Me.txtNumeroRecibo = New DataDynamics.ActiveReports.TextBox
        Me.txtAbonoRecibo = New DataDynamics.ActiveReports.TextBox
        Me.txtSaldo = New DataDynamics.ActiveReports.TextBox
        Me.txtDiasCredito = New DataDynamics.ActiveReports.TextBox
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.Label23 = New DataDynamics.ActiveReports.Label
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox23 = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.Line = New DataDynamics.ActiveReports.Line
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Line2 = New DataDynamics.ActiveReports.Line
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.txtNombre = New DataDynamics.ActiveReports.TextBox
        Me.txtDireccion = New DataDynamics.ActiveReports.TextBox
        Me.txtTelefono = New DataDynamics.ActiveReports.TextBox
        Me.Line4 = New DataDynamics.ActiveReports.Line
        Me.Line5 = New DataDynamics.ActiveReports.Line
        Me.Line6 = New DataDynamics.ActiveReports.Line
        Me.Line7 = New DataDynamics.ActiveReports.Line
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.Label7 = New DataDynamics.ActiveReports.Label
        Me.Label8 = New DataDynamics.ActiveReports.Label
        Me.Label9 = New DataDynamics.ActiveReports.Label
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.Label11 = New DataDynamics.ActiveReports.Label
        Me.Label12 = New DataDynamics.ActiveReports.Label
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.Label14 = New DataDynamics.ActiveReports.Label
        Me.txtCodigoProveedor = New DataDynamics.ActiveReports.TextBox
        Me.Label24 = New DataDynamics.ActiveReports.Label
        Me.TextBox7 = New DataDynamics.ActiveReports.TextBox
        Me.Label27 = New DataDynamics.ActiveReports.Label
        Me.TextBox24 = New DataDynamics.ActiveReports.TextBox
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.Label26 = New DataDynamics.ActiveReports.Label
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.Line8 = New DataDynamics.ActiveReports.Line
        Me.Line9 = New DataDynamics.ActiveReports.Line
        Me.Line10 = New DataDynamics.ActiveReports.Line
        Me.Line11 = New DataDynamics.ActiveReports.Line
        Me.txtSaldoProveedor = New DataDynamics.ActiveReports.TextBox
        Me.Line12 = New DataDynamics.ActiveReports.Line
        Me.Line14 = New DataDynamics.ActiveReports.Line
        Me.Line15 = New DataDynamics.ActiveReports.Line
        Me.Line16 = New DataDynamics.ActiveReports.Line
        Me.txtSubTotalAbono = New DataDynamics.ActiveReports.TextBox
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.txtNoVencido = New DataDynamics.ActiveReports.TextBox
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.txt1_30 = New DataDynamics.ActiveReports.TextBox
        Me.Label17 = New DataDynamics.ActiveReports.Label
        Me.txt31_60 = New DataDynamics.ActiveReports.TextBox
        Me.Label18 = New DataDynamics.ActiveReports.Label
        Me.txt61_Mas = New DataDynamics.ActiveReports.TextBox
        Me.Label19 = New DataDynamics.ActiveReports.Label
        Me.txtTotal = New DataDynamics.ActiveReports.TextBox
        Me.Line13 = New DataDynamics.ActiveReports.Line
        Me.Label20 = New DataDynamics.ActiveReports.Label
        Me.Label21 = New DataDynamics.ActiveReports.Label
        Me.Label22 = New DataDynamics.ActiveReports.Label
        Me.Label29 = New DataDynamics.ActiveReports.Label
        Me.Label30 = New DataDynamics.ActiveReports.Label
        Me.Line17 = New DataDynamics.ActiveReports.Line
        Me.Label31 = New DataDynamics.ActiveReports.Label
        Me.txtSubTotalFacturas = New DataDynamics.ActiveReports.TextBox
        Me.txtSubTotalDescuento = New DataDynamics.ActiveReports.TextBox
        Me.Label28 = New DataDynamics.ActiveReports.Label
        Me.GroupHeader2 = New DataDynamics.ActiveReports.GroupHeader
        Me.txtNumeroFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtFechaIngreso = New DataDynamics.ActiveReports.TextBox
        Me.txtFechaVence = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoFactura = New DataDynamics.ActiveReports.TextBox
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
        Me.txtSaldoInicial = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter2 = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFechaEmisionRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbonoRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiasCredito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalAbono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNoVencido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt1_30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt31_60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt61_Mas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSubTotalDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumeroFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFechaIngreso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFechaVence, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoInicial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label, Me.Label1, Me.Label25})
        Me.PageHeader1.Height = 1.28125!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Label
        '
        Me.Label.Border.BottomColor = System.Drawing.Color.Black
        Me.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.LeftColor = System.Drawing.Color.Black
        Me.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.RightColor = System.Drawing.Color.Black
        Me.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.TopColor = System.Drawing.Color.Black
        Me.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Height = 0.375!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.375!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: center; font-weight: bold; font-size: 20.25pt; font-family: Times New" & _
            " Roman; "
        Me.Label.Text = "Agro Centro, S.A."
        Me.Label.Top = 0.0!
        Me.Label.Width = 6.6875!
        '
        'Label1
        '
        Me.Label1.Border.BottomColor = System.Drawing.Color.Black
        Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.LeftColor = System.Drawing.Color.Black
        Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.RightColor = System.Drawing.Color.Black
        Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.TopColor = System.Drawing.Color.Black
        Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Height = 0.25!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.375!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-align: center; font-weight: bold; font-size: 15.75pt; font-family: Times New" & _
            " Roman; "
        Me.Label1.Text = "Estado de Cuenta Detallado por Proveedor"
        Me.Label1.Top = 0.5!
        Me.Label1.Width = 6.6875!
        '
        'Label25
        '
        Me.Label25.Border.BottomColor = System.Drawing.Color.Black
        Me.Label25.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Border.LeftColor = System.Drawing.Color.Black
        Me.Label25.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Border.RightColor = System.Drawing.Color.Black
        Me.Label25.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Border.TopColor = System.Drawing.Color.Black
        Me.Label25.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Height = 0.25!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0.375!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "text-align: center; font-weight: bold; font-size: 15.75pt; font-family: Times New" & _
            " Roman; "
        Me.Label25.Text = "Fecha de Corte:"
        Me.Label25.Top = 1.0!
        Me.Label25.Width = 6.6875!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0.0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtFechaEmisionRecibo, Me.txtNumeroRecibo, Me.txtAbonoRecibo, Me.txtSaldo})
        Me.Detail1.Height = 0.1979167!
        Me.Detail1.Name = "Detail1"
        '
        'txtFechaEmisionRecibo
        '
        Me.txtFechaEmisionRecibo.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFechaEmisionRecibo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaEmisionRecibo.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFechaEmisionRecibo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaEmisionRecibo.Border.RightColor = System.Drawing.Color.Black
        Me.txtFechaEmisionRecibo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaEmisionRecibo.Border.TopColor = System.Drawing.Color.Black
        Me.txtFechaEmisionRecibo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaEmisionRecibo.DataField = "FechaIngresoRecibo"
        Me.txtFechaEmisionRecibo.Height = 0.1875!
        Me.txtFechaEmisionRecibo.Left = 4.0625!
        Me.txtFechaEmisionRecibo.Name = "txtFechaEmisionRecibo"
        Me.txtFechaEmisionRecibo.OutputFormat = resources.GetString("txtFechaEmisionRecibo.OutputFormat")
        Me.txtFechaEmisionRecibo.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman; "
        Me.txtFechaEmisionRecibo.Text = "TextBox8"
        Me.txtFechaEmisionRecibo.Top = 0.0!
        Me.txtFechaEmisionRecibo.Width = 0.8125!
        '
        'txtNumeroRecibo
        '
        Me.txtNumeroRecibo.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNumeroRecibo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroRecibo.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNumeroRecibo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroRecibo.Border.RightColor = System.Drawing.Color.Black
        Me.txtNumeroRecibo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroRecibo.Border.TopColor = System.Drawing.Color.Black
        Me.txtNumeroRecibo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroRecibo.DataField = "NumeroRecibo"
        Me.txtNumeroRecibo.Height = 0.1875!
        Me.txtNumeroRecibo.Left = 4.9375!
        Me.txtNumeroRecibo.Name = "txtNumeroRecibo"
        Me.txtNumeroRecibo.OutputFormat = resources.GetString("txtNumeroRecibo.OutputFormat")
        Me.txtNumeroRecibo.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman; "
        Me.txtNumeroRecibo.Text = "TextBox10"
        Me.txtNumeroRecibo.Top = 0.0!
        Me.txtNumeroRecibo.Width = 0.875!
        '
        'txtAbonoRecibo
        '
        Me.txtAbonoRecibo.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAbonoRecibo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAbonoRecibo.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAbonoRecibo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAbonoRecibo.Border.RightColor = System.Drawing.Color.Black
        Me.txtAbonoRecibo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAbonoRecibo.Border.TopColor = System.Drawing.Color.Black
        Me.txtAbonoRecibo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAbonoRecibo.DataField = "AbonoAFactura"
        Me.txtAbonoRecibo.Height = 0.1875!
        Me.txtAbonoRecibo.Left = 5.875!
        Me.txtAbonoRecibo.Name = "txtAbonoRecibo"
        Me.txtAbonoRecibo.OutputFormat = resources.GetString("txtAbonoRecibo.OutputFormat")
        Me.txtAbonoRecibo.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman; "
        Me.txtAbonoRecibo.Text = "TextBox19"
        Me.txtAbonoRecibo.Top = 0.0!
        Me.txtAbonoRecibo.Width = 0.875!
        '
        'txtSaldo
        '
        Me.txtSaldo.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSaldo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldo.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSaldo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldo.Border.RightColor = System.Drawing.Color.Black
        Me.txtSaldo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldo.Border.TopColor = System.Drawing.Color.Black
        Me.txtSaldo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldo.Height = 0.1875!
        Me.txtSaldo.Left = 6.875!
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.OutputFormat = resources.GetString("txtSaldo.OutputFormat")
        Me.txtSaldo.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman; "
        Me.txtSaldo.Text = "0"
        Me.txtSaldo.Top = 0.0!
        Me.txtSaldo.Width = 0.6875!
        '
        'txtDiasCredito
        '
        Me.txtDiasCredito.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDiasCredito.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDiasCredito.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDiasCredito.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDiasCredito.Border.RightColor = System.Drawing.Color.Black
        Me.txtDiasCredito.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDiasCredito.Border.TopColor = System.Drawing.Color.Black
        Me.txtDiasCredito.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDiasCredito.DataField = "diascredito"
        Me.txtDiasCredito.Height = 0.1875!
        Me.txtDiasCredito.Left = 1.8125!
        Me.txtDiasCredito.Name = "txtDiasCredito"
        Me.txtDiasCredito.OutputFormat = resources.GetString("txtDiasCredito.OutputFormat")
        Me.txtDiasCredito.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman; "
        Me.txtDiasCredito.Text = Nothing
        Me.txtDiasCredito.Top = 0.0!
        Me.txtDiasCredito.Width = 0.375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label23, Me.TextBox26, Me.TextBox21, Me.TextBox23})
        Me.PageFooter1.Height = 0.25!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'Label23
        '
        Me.Label23.Border.BottomColor = System.Drawing.Color.Black
        Me.Label23.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Border.LeftColor = System.Drawing.Color.Black
        Me.Label23.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Border.RightColor = System.Drawing.Color.Black
        Me.Label23.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Border.TopColor = System.Drawing.Color.Black
        Me.Label23.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Height = 0.2!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 7.25!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-weight: bold; font-size: 8.25pt; font-family: Times New Roman; "
        Me.Label23.Text = "P�gina"
        Me.Label23.Top = 0.0!
        Me.Label23.Width = 0.5500001!
        '
        'TextBox26
        '
        Me.TextBox26.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox26.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox26.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox26.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox26.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox26.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox26.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox26.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 7.8125!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right; font-size: 8.25pt; font-family: Times New Roman; "
        Me.TextBox26.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox26.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.0!
        Me.TextBox26.Width = 0.3499999!
        '
        'TextBox21
        '
        Me.TextBox21.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Height = 0.1875!
        Me.TextBox21.Left = 0.0!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: center; font-weight: bold; font-style: italic; font-size: 8.25pt; fon" & _
            "t-family: Times New Roman; "
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0.0!
        Me.TextBox21.Width = 2.0!
        '
        'TextBox23
        '
        Me.TextBox23.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox23.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox23.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox23.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox23.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Height = 0.1875!
        Me.TextBox23.Left = 3.0!
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Style = "text-align: center; font-weight: bold; font-style: italic; font-size: 8.25pt; fon" & _
            "t-family: Times New Roman; "
        Me.TextBox23.Text = "TextBox23"
        Me.TextBox23.Top = 0.0!
        Me.TextBox23.Width = 2.0!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Line, Me.Line1, Me.Line2, Me.Label3, Me.Label4, Me.Label5, Me.txtNombre, Me.txtDireccion, Me.txtTelefono, Me.Line4, Me.Line5, Me.Line6, Me.Line7, Me.Label6, Me.Label7, Me.Label8, Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Label14, Me.txtCodigoProveedor, Me.Label24, Me.TextBox7, Me.Label27, Me.TextBox24, Me.Line3, Me.Label2, Me.Label26})
        Me.GroupHeader1.DataField = "CodigoProveedor"
        Me.GroupHeader1.GroupKeepTogether = DataDynamics.ActiveReports.GroupKeepTogether.All
        Me.GroupHeader1.Height = 1.270833!
        Me.GroupHeader1.KeepTogether = True
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'Line
        '
        Me.Line.Border.BottomColor = System.Drawing.Color.Black
        Me.Line.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.LeftColor = System.Drawing.Color.Black
        Me.Line.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.RightColor = System.Drawing.Color.Black
        Me.Line.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.TopColor = System.Drawing.Color.Black
        Me.Line.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Height = 0.6875!
        Me.Line.Left = 0.0!
        Me.Line.LineWeight = 3.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 0.0!
        Me.Line.Width = 0.0!
        Me.Line.X1 = 0.0!
        Me.Line.X2 = 0.0!
        Me.Line.Y1 = 0.0!
        Me.Line.Y2 = 0.6875!
        '
        'Line1
        '
        Me.Line1.Border.BottomColor = System.Drawing.Color.Black
        Me.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.LeftColor = System.Drawing.Color.Black
        Me.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.RightColor = System.Drawing.Color.Black
        Me.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.TopColor = System.Drawing.Color.Black
        Me.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Height = 0.6875!
        Me.Line1.Left = 7.6875!
        Me.Line1.LineWeight = 3.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.0!
        Me.Line1.Width = 0.0!
        Me.Line1.X1 = 7.6875!
        Me.Line1.X2 = 7.6875!
        Me.Line1.Y1 = 0.0!
        Me.Line1.Y2 = 0.6875!
        '
        'Line2
        '
        Me.Line2.Border.BottomColor = System.Drawing.Color.Black
        Me.Line2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Border.LeftColor = System.Drawing.Color.Black
        Me.Line2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Border.RightColor = System.Drawing.Color.Black
        Me.Line2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Border.TopColor = System.Drawing.Color.Black
        Me.Line2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line2.Height = 0.0!
        Me.Line2.Left = 0.0!
        Me.Line2.LineWeight = 3.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.6875!
        Me.Line2.Width = 7.6875!
        Me.Line2.X1 = 7.6875!
        Me.Line2.X2 = 0.0!
        Me.Line2.Y1 = 0.6875!
        Me.Line2.Y2 = 0.6875!
        '
        'Label3
        '
        Me.Label3.Border.BottomColor = System.Drawing.Color.Black
        Me.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.LeftColor = System.Drawing.Color.Black
        Me.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.RightColor = System.Drawing.Color.Black
        Me.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.TopColor = System.Drawing.Color.Black
        Me.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.0625!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label3.Text = "Nombre"
        Me.Label3.Top = 0.0625!
        Me.Label3.Width = 0.5625!
        '
        'Label4
        '
        Me.Label4.Border.BottomColor = System.Drawing.Color.Black
        Me.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.LeftColor = System.Drawing.Color.Black
        Me.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.RightColor = System.Drawing.Color.Black
        Me.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.TopColor = System.Drawing.Color.Black
        Me.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.0625!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label4.Text = "Direcci�n"
        Me.Label4.Top = 0.25!
        Me.Label4.Width = 0.5625!
        '
        'Label5
        '
        Me.Label5.Border.BottomColor = System.Drawing.Color.Black
        Me.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.LeftColor = System.Drawing.Color.Black
        Me.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.RightColor = System.Drawing.Color.Black
        Me.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.TopColor = System.Drawing.Color.Black
        Me.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0.0625!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label5.Text = "Tel�fono"
        Me.Label5.Top = 0.4375!
        Me.Label5.Width = 0.5625!
        '
        'txtNombre
        '
        Me.txtNombre.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNombre.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombre.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNombre.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombre.Border.RightColor = System.Drawing.Color.Black
        Me.txtNombre.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombre.Border.TopColor = System.Drawing.Color.Black
        Me.txtNombre.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNombre.DataField = "NombreProveedor"
        Me.txtNombre.Height = 0.1875!
        Me.txtNombre.Left = 0.625!
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Style = "font-weight: normal; font-style: normal; font-size: 9pt; font-family: Times New R" & _
            "oman; "
        Me.txtNombre.Text = "TextBox"
        Me.txtNombre.Top = 0.0625!
        Me.txtNombre.Width = 6.0625!
        '
        'txtDireccion
        '
        Me.txtDireccion.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDireccion.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDireccion.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDireccion.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDireccion.Border.RightColor = System.Drawing.Color.Black
        Me.txtDireccion.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDireccion.Border.TopColor = System.Drawing.Color.Black
        Me.txtDireccion.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDireccion.Height = 0.1875!
        Me.txtDireccion.Left = 0.625!
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Style = "font-weight: normal; font-style: normal; font-size: 9pt; font-family: Times New R" & _
            "oman; "
        Me.txtDireccion.Text = "TextBox1"
        Me.txtDireccion.Top = 0.25!
        Me.txtDireccion.Width = 6.6875!
        '
        'txtTelefono
        '
        Me.txtTelefono.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTelefono.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTelefono.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTelefono.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTelefono.Border.RightColor = System.Drawing.Color.Black
        Me.txtTelefono.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTelefono.Border.TopColor = System.Drawing.Color.Black
        Me.txtTelefono.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTelefono.Height = 0.1875!
        Me.txtTelefono.Left = 0.625!
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Style = "font-weight: normal; font-style: normal; font-size: 9pt; font-family: Times New R" & _
            "oman; "
        Me.txtTelefono.Text = "TextBox2"
        Me.txtTelefono.Top = 0.4375!
        Me.txtTelefono.Width = 3.5625!
        '
        'Line4
        '
        Me.Line4.Border.BottomColor = System.Drawing.Color.Black
        Me.Line4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line4.Border.LeftColor = System.Drawing.Color.Black
        Me.Line4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line4.Border.RightColor = System.Drawing.Color.Black
        Me.Line4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line4.Border.TopColor = System.Drawing.Color.Black
        Me.Line4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line4.Height = 0.5!
        Me.Line4.Left = 0.0!
        Me.Line4.LineWeight = 3.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 0.75!
        Me.Line4.Width = 0.0!
        Me.Line4.X1 = 0.0!
        Me.Line4.X2 = 0.0!
        Me.Line4.Y1 = 0.75!
        Me.Line4.Y2 = 1.25!
        '
        'Line5
        '
        Me.Line5.Border.BottomColor = System.Drawing.Color.Black
        Me.Line5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line5.Border.LeftColor = System.Drawing.Color.Black
        Me.Line5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line5.Border.RightColor = System.Drawing.Color.Black
        Me.Line5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line5.Border.TopColor = System.Drawing.Color.Black
        Me.Line5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line5.Height = 0.0!
        Me.Line5.Left = 0.0!
        Me.Line5.LineWeight = 3.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 1.25!
        Me.Line5.Width = 7.6875!
        Me.Line5.X1 = 7.6875!
        Me.Line5.X2 = 0.0!
        Me.Line5.Y1 = 1.25!
        Me.Line5.Y2 = 1.25!
        '
        'Line6
        '
        Me.Line6.Border.BottomColor = System.Drawing.Color.Black
        Me.Line6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Border.LeftColor = System.Drawing.Color.Black
        Me.Line6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Border.RightColor = System.Drawing.Color.Black
        Me.Line6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Border.TopColor = System.Drawing.Color.Black
        Me.Line6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line6.Height = 0.0!
        Me.Line6.Left = 0.0!
        Me.Line6.LineWeight = 3.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 0.75!
        Me.Line6.Width = 7.6875!
        Me.Line6.X1 = 7.6875!
        Me.Line6.X2 = 0.0!
        Me.Line6.Y1 = 0.75!
        Me.Line6.Y2 = 0.75!
        '
        'Line7
        '
        Me.Line7.Border.BottomColor = System.Drawing.Color.Black
        Me.Line7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line7.Border.LeftColor = System.Drawing.Color.Black
        Me.Line7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line7.Border.RightColor = System.Drawing.Color.Black
        Me.Line7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line7.Border.TopColor = System.Drawing.Color.Black
        Me.Line7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line7.Height = 0.5!
        Me.Line7.Left = 7.6875!
        Me.Line7.LineWeight = 3.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 0.75!
        Me.Line7.Width = 0.0!
        Me.Line7.X1 = 7.6875!
        Me.Line7.X2 = 7.6875!
        Me.Line7.Y1 = 0.75!
        Me.Line7.Y2 = 1.25!
        '
        'Label6
        '
        Me.Label6.Border.BottomColor = System.Drawing.Color.Black
        Me.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.LeftColor = System.Drawing.Color.Black
        Me.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.RightColor = System.Drawing.Color.Black
        Me.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.TopColor = System.Drawing.Color.Black
        Me.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 0.0625!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label6.Text = "N�mero Factura"
        Me.Label6.Top = 1.0!
        Me.Label6.Width = 0.9375!
        '
        'Label7
        '
        Me.Label7.Border.BottomColor = System.Drawing.Color.Black
        Me.Label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.LeftColor = System.Drawing.Color.Black
        Me.Label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.RightColor = System.Drawing.Color.Black
        Me.Label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.TopColor = System.Drawing.Color.Black
        Me.Label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 1.1875!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label7.Text = "Fecha"
        Me.Label7.Top = 1.0!
        Me.Label7.Width = 0.5!
        '
        'Label8
        '
        Me.Label8.Border.BottomColor = System.Drawing.Color.Black
        Me.Label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.LeftColor = System.Drawing.Color.Black
        Me.Label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.RightColor = System.Drawing.Color.Black
        Me.Label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.TopColor = System.Drawing.Color.Black
        Me.Label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 2.4375!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" & _
            "an; "
        Me.Label8.Text = "Vence"
        Me.Label8.Top = 1.0!
        Me.Label8.Width = 0.4375!
        '
        'Label9
        '
        Me.Label9.Border.BottomColor = System.Drawing.Color.Black
        Me.Label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.LeftColor = System.Drawing.Color.Black
        Me.Label9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.RightColor = System.Drawing.Color.Black
        Me.Label9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.TopColor = System.Drawing.Color.Black
        Me.Label9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 3.6875!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label9.Text = "Monto"
        Me.Label9.Top = 1.0!
        Me.Label9.Width = 0.4375!
        '
        'Label10
        '
        Me.Label10.Border.BottomColor = System.Drawing.Color.Black
        Me.Label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.LeftColor = System.Drawing.Color.Black
        Me.Label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.RightColor = System.Drawing.Color.Black
        Me.Label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.TopColor = System.Drawing.Color.Black
        Me.Label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 4.1875!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label10.Text = "Fecha"
        Me.Label10.Top = 1.0!
        Me.Label10.Width = 0.6875!
        '
        'Label11
        '
        Me.Label11.Border.BottomColor = System.Drawing.Color.Black
        Me.Label11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Border.LeftColor = System.Drawing.Color.Black
        Me.Label11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Border.RightColor = System.Drawing.Color.Black
        Me.Label11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Border.TopColor = System.Drawing.Color.Black
        Me.Label11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 4.9375!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label11.Text = "Documento"
        Me.Label11.Top = 1.0!
        Me.Label11.Width = 0.875!
        '
        'Label12
        '
        Me.Label12.Border.BottomColor = System.Drawing.Color.Black
        Me.Label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Border.LeftColor = System.Drawing.Color.Black
        Me.Label12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Border.RightColor = System.Drawing.Color.Black
        Me.Label12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Border.TopColor = System.Drawing.Color.Black
        Me.Label12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 1.8125!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label12.Text = "D�as"
        Me.Label12.Top = 1.0!
        Me.Label12.Width = 0.3125!
        '
        'Label13
        '
        Me.Label13.Border.BottomColor = System.Drawing.Color.Black
        Me.Label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.LeftColor = System.Drawing.Color.Black
        Me.Label13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.RightColor = System.Drawing.Color.Black
        Me.Label13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.TopColor = System.Drawing.Color.Black
        Me.Label13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 5.875!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label13.Text = "Abono a Factura"
        Me.Label13.Top = 1.0!
        Me.Label13.Width = 0.9375!
        '
        'Label14
        '
        Me.Label14.Border.BottomColor = System.Drawing.Color.Black
        Me.Label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.LeftColor = System.Drawing.Color.Black
        Me.Label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.RightColor = System.Drawing.Color.Black
        Me.Label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.TopColor = System.Drawing.Color.Black
        Me.Label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 7.125!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label14.Text = "Saldo"
        Me.Label14.Top = 1.0!
        Me.Label14.Width = 0.375!
        '
        'txtCodigoProveedor
        '
        Me.txtCodigoProveedor.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCodigoProveedor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProveedor.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCodigoProveedor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProveedor.Border.RightColor = System.Drawing.Color.Black
        Me.txtCodigoProveedor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProveedor.Border.TopColor = System.Drawing.Color.Black
        Me.txtCodigoProveedor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoProveedor.DataField = "CodigoProveedor"
        Me.txtCodigoProveedor.Height = 0.1875!
        Me.txtCodigoProveedor.Left = 6.6875!
        Me.txtCodigoProveedor.Name = "txtCodigoProveedor"
        Me.txtCodigoProveedor.Style = "text-align: right; font-weight: normal; font-style: normal; font-size: 9pt; font-" & _
            "family: Times New Roman; "
        Me.txtCodigoProveedor.Text = "TextBox22"
        Me.txtCodigoProveedor.Top = 0.0625!
        Me.txtCodigoProveedor.Width = 0.625!
        '
        'Label24
        '
        Me.Label24.Border.BottomColor = System.Drawing.Color.Black
        Me.Label24.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Border.LeftColor = System.Drawing.Color.Black
        Me.Label24.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Border.RightColor = System.Drawing.Color.Black
        Me.Label24.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Border.TopColor = System.Drawing.Color.Black
        Me.Label24.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 5.125!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label24.Text = "Abonos Realizados  a Facturas"
        Me.Label24.Top = 0.75!
        Me.Label24.Width = 1.6875!
        '
        'TextBox7
        '
        Me.TextBox7.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox7.DataField = "CodigoProveedor"
        Me.TextBox7.Height = 0.1875!
        Me.TextBox7.Left = 6.0625!
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Style = "font-weight: normal; font-style: normal; font-size: 9pt; font-family: Times New R" & _
            "oman; "
        Me.TextBox7.Text = "TextBox7"
        Me.TextBox7.Top = 0.0625!
        Me.TextBox7.Visible = False
        Me.TextBox7.Width = 0.625!
        '
        'Label27
        '
        Me.Label27.Border.BottomColor = System.Drawing.Color.Black
        Me.Label27.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label27.Border.LeftColor = System.Drawing.Color.Black
        Me.Label27.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label27.Border.RightColor = System.Drawing.Color.Black
        Me.Label27.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label27.Border.TopColor = System.Drawing.Color.Black
        Me.Label27.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 4.1875!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label27.Text = "Fax:"
        Me.Label27.Top = 0.4375!
        Me.Label27.Width = 0.3125!
        '
        'TextBox24
        '
        Me.TextBox24.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox24.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox24.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox24.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox24.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Height = 0.1875!
        Me.TextBox24.Left = 4.5!
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Style = "font-weight: normal; font-style: normal; font-size: 9pt; font-family: Times New R" & _
            "oman; "
        Me.TextBox24.Text = "TextBox24"
        Me.TextBox24.Top = 0.4375!
        Me.TextBox24.Width = 2.8125!
        '
        'Line3
        '
        Me.Line3.Border.BottomColor = System.Drawing.Color.Black
        Me.Line3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.LeftColor = System.Drawing.Color.Black
        Me.Line3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.RightColor = System.Drawing.Color.Black
        Me.Line3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Border.TopColor = System.Drawing.Color.Black
        Me.Line3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 0.0!
        Me.Line3.LineWeight = 3.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.0!
        Me.Line3.Width = 7.6875!
        Me.Line3.X1 = 7.6875!
        Me.Line3.X2 = 0.0!
        Me.Line3.Y1 = 0.0!
        Me.Line3.Y2 = 0.0!
        '
        'Label2
        '
        Me.Label2.Border.BottomColor = System.Drawing.Color.Black
        Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.LeftColor = System.Drawing.Color.Black
        Me.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.RightColor = System.Drawing.Color.Black
        Me.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.TopColor = System.Drawing.Color.Black
        Me.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 1.25!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label2.Text = "Detalle de Compras Realizadas"
        Me.Label2.Top = 0.75!
        Me.Label2.Width = 1.75!
        '
        'Label26
        '
        Me.Label26.Border.BottomColor = System.Drawing.Color.Black
        Me.Label26.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Border.LeftColor = System.Drawing.Color.Black
        Me.Label26.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Border.RightColor = System.Drawing.Color.Black
        Me.Label26.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Border.TopColor = System.Drawing.Color.Black
        Me.Label26.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.0!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label26.Text = "D�as Venc"
        Me.Label26.Top = 1.0!
        Me.Label26.Width = 0.625!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Line8, Me.Line9, Me.Line10, Me.Line11, Me.txtSaldoProveedor, Me.Line12, Me.Line14, Me.Line15, Me.Line16, Me.txtSubTotalAbono, Me.Label15, Me.txtNoVencido, Me.Label16, Me.txt1_30, Me.Label17, Me.txt31_60, Me.Label18, Me.txt61_Mas, Me.Label19, Me.txtTotal, Me.Line13, Me.Label20, Me.Label21, Me.Label22, Me.Label29, Me.Label30, Me.Line17, Me.Label31, Me.txtSubTotalFacturas, Me.txtSubTotalDescuento, Me.Label28})
        Me.GroupFooter1.Height = 2.75!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.NewPage = DataDynamics.ActiveReports.NewPage.After
        '
        'Line8
        '
        Me.Line8.Border.BottomColor = System.Drawing.Color.Black
        Me.Line8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line8.Border.LeftColor = System.Drawing.Color.Black
        Me.Line8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line8.Border.RightColor = System.Drawing.Color.Black
        Me.Line8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line8.Border.TopColor = System.Drawing.Color.Black
        Me.Line8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line8.Height = 0.0!
        Me.Line8.Left = 0.0!
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 0.0!
        Me.Line8.Width = 7.6875!
        Me.Line8.X1 = 7.6875!
        Me.Line8.X2 = 0.0!
        Me.Line8.Y1 = 0.0!
        Me.Line8.Y2 = 0.0!
        '
        'Line9
        '
        Me.Line9.Border.BottomColor = System.Drawing.Color.Black
        Me.Line9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line9.Border.LeftColor = System.Drawing.Color.Black
        Me.Line9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line9.Border.RightColor = System.Drawing.Color.Black
        Me.Line9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line9.Border.TopColor = System.Drawing.Color.Black
        Me.Line9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line9.Height = 0.3125!
        Me.Line9.Left = 0.0!
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 0.0!
        Me.Line9.Width = 0.0!
        Me.Line9.X1 = 0.0!
        Me.Line9.X2 = 0.0!
        Me.Line9.Y1 = 0.0!
        Me.Line9.Y2 = 0.3125!
        '
        'Line10
        '
        Me.Line10.Border.BottomColor = System.Drawing.Color.Black
        Me.Line10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line10.Border.LeftColor = System.Drawing.Color.Black
        Me.Line10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line10.Border.RightColor = System.Drawing.Color.Black
        Me.Line10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line10.Border.TopColor = System.Drawing.Color.Black
        Me.Line10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line10.Height = 0.0!
        Me.Line10.Left = 0.0!
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 0.3125!
        Me.Line10.Width = 7.6875!
        Me.Line10.X1 = 7.6875!
        Me.Line10.X2 = 0.0!
        Me.Line10.Y1 = 0.3125!
        Me.Line10.Y2 = 0.3125!
        '
        'Line11
        '
        Me.Line11.Border.BottomColor = System.Drawing.Color.Black
        Me.Line11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line11.Border.LeftColor = System.Drawing.Color.Black
        Me.Line11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line11.Border.RightColor = System.Drawing.Color.Black
        Me.Line11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line11.Border.TopColor = System.Drawing.Color.Black
        Me.Line11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line11.Height = 0.3125!
        Me.Line11.Left = 7.6875!
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 0.0!
        Me.Line11.Width = 0.0!
        Me.Line11.X1 = 7.6875!
        Me.Line11.X2 = 7.6875!
        Me.Line11.Y1 = 0.0!
        Me.Line11.Y2 = 0.3125!
        '
        'txtSaldoProveedor
        '
        Me.txtSaldoProveedor.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSaldoProveedor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoProveedor.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSaldoProveedor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoProveedor.Border.RightColor = System.Drawing.Color.Black
        Me.txtSaldoProveedor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoProveedor.Border.TopColor = System.Drawing.Color.Black
        Me.txtSaldoProveedor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoProveedor.DataField = "SaldoProveedor"
        Me.txtSaldoProveedor.Height = 0.1875!
        Me.txtSaldoProveedor.Left = 6.8125!
        Me.txtSaldoProveedor.Name = "txtSaldoProveedor"
        Me.txtSaldoProveedor.OutputFormat = resources.GetString("txtSaldoProveedor.OutputFormat")
        Me.txtSaldoProveedor.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txtSaldoProveedor.SummaryGroup = "GroupHeader1"
        Me.txtSaldoProveedor.Text = Nothing
        Me.txtSaldoProveedor.Top = 0.0625!
        Me.txtSaldoProveedor.Width = 0.75!
        '
        'Line12
        '
        Me.Line12.Border.BottomColor = System.Drawing.Color.Black
        Me.Line12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line12.Border.LeftColor = System.Drawing.Color.Black
        Me.Line12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line12.Border.RightColor = System.Drawing.Color.Black
        Me.Line12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line12.Border.TopColor = System.Drawing.Color.Black
        Me.Line12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line12.Height = 0.5!
        Me.Line12.Left = 0.0!
        Me.Line12.LineWeight = 3.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 0.375!
        Me.Line12.Width = 0.0!
        Me.Line12.X1 = 0.0!
        Me.Line12.X2 = 0.0!
        Me.Line12.Y1 = 0.375!
        Me.Line12.Y2 = 0.875!
        '
        'Line14
        '
        Me.Line14.Border.BottomColor = System.Drawing.Color.Black
        Me.Line14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line14.Border.LeftColor = System.Drawing.Color.Black
        Me.Line14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line14.Border.RightColor = System.Drawing.Color.Black
        Me.Line14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line14.Border.TopColor = System.Drawing.Color.Black
        Me.Line14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line14.Height = 0.0!
        Me.Line14.Left = 0.0!
        Me.Line14.LineWeight = 3.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 0.875!
        Me.Line14.Width = 7.6875!
        Me.Line14.X1 = 7.6875!
        Me.Line14.X2 = 0.0!
        Me.Line14.Y1 = 0.875!
        Me.Line14.Y2 = 0.875!
        '
        'Line15
        '
        Me.Line15.Border.BottomColor = System.Drawing.Color.Black
        Me.Line15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line15.Border.LeftColor = System.Drawing.Color.Black
        Me.Line15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line15.Border.RightColor = System.Drawing.Color.Black
        Me.Line15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line15.Border.TopColor = System.Drawing.Color.Black
        Me.Line15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line15.Height = 0.5!
        Me.Line15.Left = 7.6875!
        Me.Line15.LineWeight = 3.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 0.375!
        Me.Line15.Width = 0.0!
        Me.Line15.X1 = 7.6875!
        Me.Line15.X2 = 7.6875!
        Me.Line15.Y1 = 0.375!
        Me.Line15.Y2 = 0.875!
        '
        'Line16
        '
        Me.Line16.Border.BottomColor = System.Drawing.Color.Black
        Me.Line16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line16.Border.LeftColor = System.Drawing.Color.Black
        Me.Line16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line16.Border.RightColor = System.Drawing.Color.Black
        Me.Line16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line16.Border.TopColor = System.Drawing.Color.Black
        Me.Line16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line16.Height = 0.0!
        Me.Line16.Left = 0.0!
        Me.Line16.LineWeight = 3.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 0.375!
        Me.Line16.Width = 7.6875!
        Me.Line16.X1 = 7.6875!
        Me.Line16.X2 = 0.0!
        Me.Line16.Y1 = 0.375!
        Me.Line16.Y2 = 0.375!
        '
        'txtSubTotalAbono
        '
        Me.txtSubTotalAbono.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSubTotalAbono.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalAbono.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSubTotalAbono.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalAbono.Border.RightColor = System.Drawing.Color.Black
        Me.txtSubTotalAbono.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalAbono.Border.TopColor = System.Drawing.Color.Black
        Me.txtSubTotalAbono.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalAbono.DataField = "AbonoAFactura"
        Me.txtSubTotalAbono.Height = 0.1875!
        Me.txtSubTotalAbono.Left = 5.9375!
        Me.txtSubTotalAbono.Name = "txtSubTotalAbono"
        Me.txtSubTotalAbono.OutputFormat = resources.GetString("txtSubTotalAbono.OutputFormat")
        Me.txtSubTotalAbono.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txtSubTotalAbono.SummaryGroup = "GroupHeader1"
        Me.txtSubTotalAbono.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtSubTotalAbono.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtSubTotalAbono.Text = "TextBox20"
        Me.txtSubTotalAbono.Top = 0.0625!
        Me.txtSubTotalAbono.Width = 0.8125!
        '
        'Label15
        '
        Me.Label15.Border.BottomColor = System.Drawing.Color.Black
        Me.Label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.LeftColor = System.Drawing.Color.Black
        Me.Label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.RightColor = System.Drawing.Color.Black
        Me.Label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.TopColor = System.Drawing.Color.Black
        Me.Label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0.125!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label15.Text = "No vencido"
        Me.Label15.Top = 0.4375!
        Me.Label15.Width = 0.6875!
        '
        'txtNoVencido
        '
        Me.txtNoVencido.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNoVencido.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoVencido.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNoVencido.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoVencido.Border.RightColor = System.Drawing.Color.Black
        Me.txtNoVencido.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoVencido.Border.TopColor = System.Drawing.Color.Black
        Me.txtNoVencido.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNoVencido.DataField = "NoVencido"
        Me.txtNoVencido.Height = 0.1875!
        Me.txtNoVencido.Left = 0.125!
        Me.txtNoVencido.Name = "txtNoVencido"
        Me.txtNoVencido.OutputFormat = resources.GetString("txtNoVencido.OutputFormat")
        Me.txtNoVencido.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txtNoVencido.Text = Nothing
        Me.txtNoVencido.Top = 0.625!
        Me.txtNoVencido.Width = 0.6875!
        '
        'Label16
        '
        Me.Label16.Border.BottomColor = System.Drawing.Color.Black
        Me.Label16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.LeftColor = System.Drawing.Color.Black
        Me.Label16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.RightColor = System.Drawing.Color.Black
        Me.Label16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.TopColor = System.Drawing.Color.Black
        Me.Label16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 1.5!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label16.Text = "1 - 30"
        Me.Label16.Top = 0.4375!
        Me.Label16.Width = 0.6875!
        '
        'txt1_30
        '
        Me.txt1_30.Border.BottomColor = System.Drawing.Color.Black
        Me.txt1_30.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt1_30.Border.LeftColor = System.Drawing.Color.Black
        Me.txt1_30.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt1_30.Border.RightColor = System.Drawing.Color.Black
        Me.txt1_30.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt1_30.Border.TopColor = System.Drawing.Color.Black
        Me.txt1_30.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt1_30.DataField = "Vencido1_30"
        Me.txt1_30.Height = 0.1875!
        Me.txt1_30.Left = 1.5!
        Me.txt1_30.Name = "txt1_30"
        Me.txt1_30.OutputFormat = resources.GetString("txt1_30.OutputFormat")
        Me.txt1_30.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txt1_30.Text = Nothing
        Me.txt1_30.Top = 0.625!
        Me.txt1_30.Width = 0.6875!
        '
        'Label17
        '
        Me.Label17.Border.BottomColor = System.Drawing.Color.Black
        Me.Label17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.LeftColor = System.Drawing.Color.Black
        Me.Label17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.RightColor = System.Drawing.Color.Black
        Me.Label17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.TopColor = System.Drawing.Color.Black
        Me.Label17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 3.125!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label17.Text = "31 - 60"
        Me.Label17.Top = 0.4375!
        Me.Label17.Width = 0.6875!
        '
        'txt31_60
        '
        Me.txt31_60.Border.BottomColor = System.Drawing.Color.Black
        Me.txt31_60.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt31_60.Border.LeftColor = System.Drawing.Color.Black
        Me.txt31_60.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt31_60.Border.RightColor = System.Drawing.Color.Black
        Me.txt31_60.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt31_60.Border.TopColor = System.Drawing.Color.Black
        Me.txt31_60.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt31_60.DataField = "Vencido31_60"
        Me.txt31_60.Height = 0.1875!
        Me.txt31_60.Left = 3.125!
        Me.txt31_60.Name = "txt31_60"
        Me.txt31_60.OutputFormat = resources.GetString("txt31_60.OutputFormat")
        Me.txt31_60.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txt31_60.Text = Nothing
        Me.txt31_60.Top = 0.625!
        Me.txt31_60.Width = 0.6875!
        '
        'Label18
        '
        Me.Label18.Border.BottomColor = System.Drawing.Color.Black
        Me.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Border.LeftColor = System.Drawing.Color.Black
        Me.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Border.RightColor = System.Drawing.Color.Black
        Me.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Border.TopColor = System.Drawing.Color.Black
        Me.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 4.875!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label18.Text = "61 - Mas"
        Me.Label18.Top = 0.4375!
        Me.Label18.Width = 0.6875!
        '
        'txt61_Mas
        '
        Me.txt61_Mas.Border.BottomColor = System.Drawing.Color.Black
        Me.txt61_Mas.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt61_Mas.Border.LeftColor = System.Drawing.Color.Black
        Me.txt61_Mas.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt61_Mas.Border.RightColor = System.Drawing.Color.Black
        Me.txt61_Mas.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt61_Mas.Border.TopColor = System.Drawing.Color.Black
        Me.txt61_Mas.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txt61_Mas.DataField = "Vencido61_Mas"
        Me.txt61_Mas.Height = 0.1875!
        Me.txt61_Mas.Left = 4.875!
        Me.txt61_Mas.Name = "txt61_Mas"
        Me.txt61_Mas.OutputFormat = resources.GetString("txt61_Mas.OutputFormat")
        Me.txt61_Mas.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txt61_Mas.Text = Nothing
        Me.txt61_Mas.Top = 0.625!
        Me.txt61_Mas.Width = 0.6875!
        '
        'Label19
        '
        Me.Label19.Border.BottomColor = System.Drawing.Color.Black
        Me.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.LeftColor = System.Drawing.Color.Black
        Me.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.RightColor = System.Drawing.Color.Black
        Me.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.TopColor = System.Drawing.Color.Black
        Me.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 6.875!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.Label19.Text = "Total"
        Me.Label19.Top = 0.4375!
        Me.Label19.Width = 0.6875!
        '
        'txtTotal
        '
        Me.txtTotal.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotal.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotal.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotal.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotal.DataField = "SaldoProveedor"
        Me.txtTotal.Height = 0.1875!
        Me.txtTotal.Left = 6.875!
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.OutputFormat = resources.GetString("txtTotal.OutputFormat")
        Me.txtTotal.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txtTotal.Text = Nothing
        Me.txtTotal.Top = 0.625!
        Me.txtTotal.Width = 0.6875!
        '
        'Line13
        '
        Me.Line13.Border.BottomColor = System.Drawing.Color.Black
        Me.Line13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line13.Border.LeftColor = System.Drawing.Color.Black
        Me.Line13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line13.Border.RightColor = System.Drawing.Color.Black
        Me.Line13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line13.Border.TopColor = System.Drawing.Color.Black
        Me.Line13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line13.Height = 0.0!
        Me.Line13.Left = 3.0!
        Me.Line13.LineWeight = 3.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 2.5!
        Me.Line13.Width = 1.9375!
        Me.Line13.X1 = 4.9375!
        Me.Line13.X2 = 3.0!
        Me.Line13.Y1 = 2.5!
        Me.Line13.Y2 = 2.5!
        '
        'Label20
        '
        Me.Label20.Border.BottomColor = System.Drawing.Color.Black
        Me.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.LeftColor = System.Drawing.Color.Black
        Me.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.RightColor = System.Drawing.Color.Black
        Me.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.TopColor = System.Drawing.Color.Black
        Me.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.8125!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label20.Text = "En caso de inconformidad con el saldo presentado, favor llamar al Departamento de" & _
            " Cartera y Cobro al Telf:"
        Me.Label20.Top = 1.125!
        Me.Label20.Width = 5.875!
        '
        'Label21
        '
        Me.Label21.Border.BottomColor = System.Drawing.Color.Black
        Me.Label21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.LeftColor = System.Drawing.Color.Black
        Me.Label21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.RightColor = System.Drawing.Color.Black
        Me.Label21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.TopColor = System.Drawing.Color.Black
        Me.Label21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label21.Text = "Atentamente,"
        Me.Label21.Top = 1.9375!
        Me.Label21.Width = 0.875!
        '
        'Label22
        '
        Me.Label22.Border.BottomColor = System.Drawing.Color.Black
        Me.Label22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Border.LeftColor = System.Drawing.Color.Black
        Me.Label22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Border.RightColor = System.Drawing.Color.Black
        Me.Label22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Border.TopColor = System.Drawing.Color.Black
        Me.Label22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 3.0!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "text-align: center; font-weight: bold; font-size: 9pt; font-family: Times New Rom" & _
            "an; "
        Me.Label22.Text = "Cartera y Cobro"
        Me.Label22.Top = 2.5625!
        Me.Label22.Width = 1.9375!
        '
        'Label29
        '
        Me.Label29.Border.BottomColor = System.Drawing.Color.Black
        Me.Label29.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label29.Border.LeftColor = System.Drawing.Color.Black
        Me.Label29.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label29.Border.RightColor = System.Drawing.Color.Black
        Me.Label29.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label29.Border.TopColor = System.Drawing.Color.Black
        Me.Label29.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.8125!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label29.Text = "De no comunicacarse dentro del t�rmino de 7 d�as despu�s de recibido este Estado " & _
            "de Cuenta, daremos por aceptado el mismo."
        Me.Label29.Top = 1.3125!
        Me.Label29.Width = 6.8125!
        '
        'Label30
        '
        Me.Label30.Border.BottomColor = System.Drawing.Color.Black
        Me.Label30.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label30.Border.LeftColor = System.Drawing.Color.Black
        Me.Label30.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label30.Border.RightColor = System.Drawing.Color.Black
        Me.Label30.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label30.Border.TopColor = System.Drawing.Color.Black
        Me.Label30.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 0.8125!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label30.Text = "Este Estado de Cuenta presenta saldo(s) en mora, favor ponerse al d�a; Si ya efec" & _
            "tu� pago, hacer caso omiso al mismo."
        Me.Label30.Top = 1.5!
        Me.Label30.Width = 6.375!
        '
        'Line17
        '
        Me.Line17.Border.BottomColor = System.Drawing.Color.Black
        Me.Line17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line17.Border.LeftColor = System.Drawing.Color.Black
        Me.Line17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line17.Border.RightColor = System.Drawing.Color.Black
        Me.Line17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line17.Border.TopColor = System.Drawing.Color.Black
        Me.Line17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line17.Height = 0.0!
        Me.Line17.Left = 1.3125!
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 1.3125!
        Me.Line17.Width = 0.9375!
        Me.Line17.X1 = 1.3125!
        Me.Line17.X2 = 2.25!
        Me.Line17.Y1 = 1.3125!
        Me.Line17.Y2 = 1.3125!
        '
        'Label31
        '
        Me.Label31.Border.BottomColor = System.Drawing.Color.Black
        Me.Label31.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label31.Border.LeftColor = System.Drawing.Color.Black
        Me.Label31.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label31.Border.RightColor = System.Drawing.Color.Black
        Me.Label31.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label31.Border.TopColor = System.Drawing.Color.Black
        Me.Label31.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 6.6875!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label31.Text = "Label31"
        Me.Label31.Top = 1.125!
        Me.Label31.Width = 1.0!
        '
        'txtSubTotalFacturas
        '
        Me.txtSubTotalFacturas.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSubTotalFacturas.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalFacturas.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSubTotalFacturas.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalFacturas.Border.RightColor = System.Drawing.Color.Black
        Me.txtSubTotalFacturas.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalFacturas.Border.TopColor = System.Drawing.Color.Black
        Me.txtSubTotalFacturas.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalFacturas.DataField = "TotalFactProv"
        Me.txtSubTotalFacturas.Height = 0.1875!
        Me.txtSubTotalFacturas.Left = 2.875!
        Me.txtSubTotalFacturas.Name = "txtSubTotalFacturas"
        Me.txtSubTotalFacturas.OutputFormat = resources.GetString("txtSubTotalFacturas.OutputFormat")
        Me.txtSubTotalFacturas.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txtSubTotalFacturas.Text = Nothing
        Me.txtSubTotalFacturas.Top = 0.0625!
        Me.txtSubTotalFacturas.Width = 0.8125!
        '
        'txtSubTotalDescuento
        '
        Me.txtSubTotalDescuento.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSubTotalDescuento.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalDescuento.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSubTotalDescuento.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalDescuento.Border.RightColor = System.Drawing.Color.Black
        Me.txtSubTotalDescuento.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalDescuento.Border.TopColor = System.Drawing.Color.Black
        Me.txtSubTotalDescuento.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSubTotalDescuento.DataField = "MontoDescuentoFact"
        Me.txtSubTotalDescuento.Height = 0.1875!
        Me.txtSubTotalDescuento.Left = 5.0625!
        Me.txtSubTotalDescuento.Name = "txtSubTotalDescuento"
        Me.txtSubTotalDescuento.OutputFormat = resources.GetString("txtSubTotalDescuento.OutputFormat")
        Me.txtSubTotalDescuento.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" & _
            "n; "
        Me.txtSubTotalDescuento.SummaryGroup = "GroupHeader1"
        Me.txtSubTotalDescuento.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtSubTotalDescuento.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtSubTotalDescuento.Text = "DescuentoFact"
        Me.txtSubTotalDescuento.Top = 0.0625!
        Me.txtSubTotalDescuento.Width = 0.8125!
        '
        'Label28
        '
        Me.Label28.Border.BottomColor = System.Drawing.Color.Black
        Me.Label28.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label28.Border.LeftColor = System.Drawing.Color.Black
        Me.Label28.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label28.Border.RightColor = System.Drawing.Color.Black
        Me.Label28.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label28.Border.TopColor = System.Drawing.Color.Black
        Me.Label28.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 3.8125!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman; "
        Me.Label28.Text = "Descuento a Factura"
        Me.Label28.Top = 0.0625!
        Me.Label28.Width = 1.125!
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtNumeroFactura, Me.txtFechaIngreso, Me.txtFechaVence, Me.txtDiasCredito, Me.txtMontoFactura, Me.TextBox1, Me.txtSaldoInicial})
        Me.GroupHeader2.DataField = "NumeroFactura"
        Me.GroupHeader2.Height = 0.21875!
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'txtNumeroFactura
        '
        Me.txtNumeroFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtNumeroFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumeroFactura.DataField = "NumeroFactura"
        Me.txtNumeroFactura.Height = 0.1875!
        Me.txtNumeroFactura.Left = 0.0625!
        Me.txtNumeroFactura.Name = "txtNumeroFactura"
        Me.txtNumeroFactura.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman; "
        Me.txtNumeroFactura.Text = Nothing
        Me.txtNumeroFactura.Top = 0.0!
        Me.txtNumeroFactura.Width = 0.9375!
        '
        'txtFechaIngreso
        '
        Me.txtFechaIngreso.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFechaIngreso.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaIngreso.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFechaIngreso.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaIngreso.Border.RightColor = System.Drawing.Color.Black
        Me.txtFechaIngreso.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaIngreso.Border.TopColor = System.Drawing.Color.Black
        Me.txtFechaIngreso.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaIngreso.DataField = "FechaIngreso"
        Me.txtFechaIngreso.Height = 0.1875!
        Me.txtFechaIngreso.Left = 1.0!
        Me.txtFechaIngreso.Name = "txtFechaIngreso"
        Me.txtFechaIngreso.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman; "
        Me.txtFechaIngreso.Text = Nothing
        Me.txtFechaIngreso.Top = 0.0!
        Me.txtFechaIngreso.Width = 0.8125!
        '
        'txtFechaVence
        '
        Me.txtFechaVence.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFechaVence.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaVence.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFechaVence.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaVence.Border.RightColor = System.Drawing.Color.Black
        Me.txtFechaVence.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaVence.Border.TopColor = System.Drawing.Color.Black
        Me.txtFechaVence.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaVence.DataField = "fechaVen"
        Me.txtFechaVence.Height = 0.1875!
        Me.txtFechaVence.Left = 2.1875!
        Me.txtFechaVence.Name = "txtFechaVence"
        Me.txtFechaVence.Style = "font-size: 9pt; font-family: Times New Roman; "
        Me.txtFechaVence.Text = Nothing
        Me.txtFechaVence.Top = 0.0!
        Me.txtFechaVence.Width = 0.875!
        '
        'txtMontoFactura
        '
        Me.txtMontoFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoFactura.DataField = "TotalFactura"
        Me.txtMontoFactura.Height = 0.1875!
        Me.txtMontoFactura.Left = 3.625!
        Me.txtMontoFactura.Name = "txtMontoFactura"
        Me.txtMontoFactura.OutputFormat = resources.GetString("txtMontoFactura.OutputFormat")
        Me.txtMontoFactura.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman; "
        Me.txtMontoFactura.Text = Nothing
        Me.txtMontoFactura.Top = 0.0!
        Me.txtMontoFactura.Width = 0.75!
        '
        'TextBox1
        '
        Me.TextBox1.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.DataField = "DiasVencidos"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 3.0625!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat")
        Me.TextBox1.Style = "text-align: center; font-size: 9pt; font-family: Times New Roman; "
        Me.TextBox1.Text = Nothing
        Me.TextBox1.Top = 0.0!
        Me.TextBox1.Width = 0.4375!
        '
        'txtSaldoInicial
        '
        Me.txtSaldoInicial.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSaldoInicial.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoInicial.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSaldoInicial.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoInicial.Border.RightColor = System.Drawing.Color.Black
        Me.txtSaldoInicial.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoInicial.Border.TopColor = System.Drawing.Color.Black
        Me.txtSaldoInicial.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSaldoInicial.Height = 0.1875!
        Me.txtSaldoInicial.Left = 6.875!
        Me.txtSaldoInicial.Name = "txtSaldoInicial"
        Me.txtSaldoInicial.OutputFormat = resources.GetString("txtSaldoInicial.OutputFormat")
        Me.txtSaldoInicial.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman; "
        Me.txtSaldoInicial.Text = "0"
        Me.txtSaldoInicial.Top = 0.0!
        Me.txtSaldoInicial.Width = 0.6875!
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Height = 0.0!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'actrptSaldosDetalladosXProveedores
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Left = 0.4!
        Me.PageSettings.Margins.Right = 0.0!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.81225!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.GroupHeader2)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.GroupFooter2)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black; ", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"))
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFechaEmisionRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbonoRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiasCredito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalAbono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNoVencido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt1_30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt31_60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt61_Mas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSubTotalDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumeroFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFechaIngreso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFechaVence, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoInicial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label As DataDynamics.ActiveReports.Label
    Private WithEvents Label1 As DataDynamics.ActiveReports.Label
    Private WithEvents Label25 As DataDynamics.ActiveReports.Label
    Private WithEvents Line As DataDynamics.ActiveReports.Line
    Private WithEvents Line1 As DataDynamics.ActiveReports.Line
    Private WithEvents Line2 As DataDynamics.ActiveReports.Line
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Private WithEvents Label4 As DataDynamics.ActiveReports.Label
    Private WithEvents Label5 As DataDynamics.ActiveReports.Label
    Private WithEvents txtNombre As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDireccion As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTelefono As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line4 As DataDynamics.ActiveReports.Line
    Private WithEvents Line5 As DataDynamics.ActiveReports.Line
    Private WithEvents Line6 As DataDynamics.ActiveReports.Line
    Private WithEvents Line7 As DataDynamics.ActiveReports.Line
    Private WithEvents Label6 As DataDynamics.ActiveReports.Label
    Private WithEvents Label7 As DataDynamics.ActiveReports.Label
    Private WithEvents Label8 As DataDynamics.ActiveReports.Label
    Private WithEvents Label9 As DataDynamics.ActiveReports.Label
    Private WithEvents Label10 As DataDynamics.ActiveReports.Label
    Private WithEvents Label11 As DataDynamics.ActiveReports.Label
    Private WithEvents Label12 As DataDynamics.ActiveReports.Label
    Private WithEvents Label13 As DataDynamics.ActiveReports.Label
    Private WithEvents Label14 As DataDynamics.ActiveReports.Label
    Private WithEvents txtCodigoProveedor As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label24 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox7 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label27 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox24 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line3 As DataDynamics.ActiveReports.Line
    Private WithEvents Label2 As DataDynamics.ActiveReports.Label
    Private WithEvents txtFechaEmisionRecibo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtNumeroRecibo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtAbonoRecibo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtSaldo As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtDiasCredito As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtNumeroFactura As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtFechaIngreso As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtFechaVence As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtMontoFactura As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line8 As DataDynamics.ActiveReports.Line
    Private WithEvents Line9 As DataDynamics.ActiveReports.Line
    Private WithEvents Line10 As DataDynamics.ActiveReports.Line
    Private WithEvents Line11 As DataDynamics.ActiveReports.Line
    Private WithEvents txtSubTotalFacturas As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtSaldoProveedor As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line12 As DataDynamics.ActiveReports.Line
    Private WithEvents Line14 As DataDynamics.ActiveReports.Line
    Private WithEvents Line15 As DataDynamics.ActiveReports.Line
    Private WithEvents Line16 As DataDynamics.ActiveReports.Line
    Private WithEvents txtSubTotalAbono As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label15 As DataDynamics.ActiveReports.Label
    Private WithEvents txtNoVencido As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label16 As DataDynamics.ActiveReports.Label
    Private WithEvents txt1_30 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label17 As DataDynamics.ActiveReports.Label
    Private WithEvents txt31_60 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label18 As DataDynamics.ActiveReports.Label
    Private WithEvents txt61_Mas As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label19 As DataDynamics.ActiveReports.Label
    Private WithEvents txtTotal As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line13 As DataDynamics.ActiveReports.Line
    Private WithEvents Label20 As DataDynamics.ActiveReports.Label
    Private WithEvents Label21 As DataDynamics.ActiveReports.Label
    Private WithEvents Label22 As DataDynamics.ActiveReports.Label
    Private WithEvents Label29 As DataDynamics.ActiveReports.Label
    Private WithEvents Label30 As DataDynamics.ActiveReports.Label
    Private WithEvents Line17 As DataDynamics.ActiveReports.Line
    Private WithEvents Label31 As DataDynamics.ActiveReports.Label
    Private WithEvents Label23 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox26 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox21 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox23 As DataDynamics.ActiveReports.TextBox
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents Label26 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtSaldoInicial As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtSubTotalDescuento As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label28 As DataDynamics.ActiveReports.Label
End Class
