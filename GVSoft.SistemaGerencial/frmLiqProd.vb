Imports System.Data.SqlClient
Imports System.Text

Public Class frmLiqProd
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents cntmnuLiqProdRg As System.Windows.Forms.ContextMenu
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox21 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents MSFlexGrid1 As AxMSFlexGridLib.AxMSFlexGrid
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox11 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLiqProd))
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.cntmnuLiqProdRg = New System.Windows.Forms.ContextMenu
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TextBox25 = New System.Windows.Forms.TextBox
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ComboBox11 = New System.Windows.Forms.ComboBox
        Me.ComboBox10 = New System.Windows.Forms.ComboBox
        Me.ComboBox9 = New System.Windows.Forms.ComboBox
        Me.ComboBox8 = New System.Windows.Forms.ComboBox
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.MSFlexGrid1 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.TextBox24 = New System.Windows.Forms.TextBox
        Me.TextBox23 = New System.Windows.Forms.TextBox
        Me.TextBox22 = New System.Windows.Forms.TextBox
        Me.TextBox21 = New System.Windows.Forms.TextBox
        Me.TextBox20 = New System.Windows.Forms.TextBox
        Me.TextBox19 = New System.Windows.Forms.TextBox
        Me.TextBox18 = New System.Windows.Forms.TextBox
        Me.TextBox17 = New System.Windows.Forms.TextBox
        Me.TextBox16 = New System.Windows.Forms.TextBox
        Me.TextBox15 = New System.Windows.Forms.TextBox
        Me.TextBox14 = New System.Windows.Forms.TextBox
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.ComboBox7 = New System.Windows.Forms.ComboBox
        Me.ComboBox6 = New System.Windows.Forms.ComboBox
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 601)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(600, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 14
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Name = "StatusBarPanel1"
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        Me.StatusBarPanel1.Width = 120
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Name = "StatusBarPanel2"
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 480
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(600, 65)
        Me.ToolBar1.TabIndex = 12
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Name = "ToolBarButton1"
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Name = "ToolBarButton2"
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Name = "ToolBarButton3"
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuLiqProdRg
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Name = "ToolBarButton4"
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.ToolBarButton4.ToolTipText = "Registros"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Name = "ToolBarButton7"
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8, Me.MenuItem10})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 4
        Me.MenuItem10.Text = "Igual"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox25)
        Me.GroupBox1.Controls.Add(Me.TextBox9)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.TextBox8)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(600, 152)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Encabezado"
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(552, 24)
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(40, 20)
        Me.TextBox25.TabIndex = 28
        Me.TextBox25.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(504, 120)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(80, 20)
        Me.TextBox9.TabIndex = 27
        Me.TextBox9.Tag = "Monto del Costo de Introducci�n"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(440, 120)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(63, 13)
        Me.Label32.TabIndex = 26
        Me.Label32.Text = "Cto Introd"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(344, 120)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(80, 20)
        Me.TextBox8.TabIndex = 25
        Me.TextBox8.Tag = "Monto del Constante para CIF US$"
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(280, 120)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(64, 13)
        Me.Label31.TabIndex = 24
        Me.Label31.Text = "Constante"
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(440, 8)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 16)
        Me.Label30.TabIndex = 23
        Me.Label30.Text = "0.00"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label30.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(184, 120)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(80, 20)
        Me.TextBox7.TabIndex = 10
        Me.TextBox7.Tag = "Monto del TSIM Expresado en C�rdobas"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(144, 120)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(37, 13)
        Me.Label29.TabIndex = 22
        Me.Label29.Text = "TSIM"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(16, 120)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(27, 13)
        Me.Label28.TabIndex = 21
        Me.Label28.Text = "IVA"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(472, 88)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(28, 13)
        Me.Label27.TabIndex = 20
        Me.Label27.Text = "DAI"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(48, 120)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(80, 20)
        Me.TextBox6.TabIndex = 9
        Me.TextBox6.Tag = "Monto del IVA Expresado en C�rdobas"
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(504, 88)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(80, 20)
        Me.TextBox5.TabIndex = 8
        Me.TextBox5.Tag = "Monto del DAI Expresado en C�rdobas"
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(16, 88)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(75, 13)
        Me.Label14.TabIndex = 17
        Me.Label14.Text = "Tipo Pedido"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Location = New System.Drawing.Point(88, 88)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(56, 21)
        Me.ComboBox3.TabIndex = 5
        Me.ComboBox3.Tag = "Tipo de Pedido"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Location = New System.Drawing.Point(568, 56)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox2.TabIndex = 15
        Me.ComboBox2.TabStop = False
        Me.ComboBox2.Visible = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(248, 56)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(312, 21)
        Me.ComboBox1.TabIndex = 4
        Me.ComboBox1.Tag = "Proveedor del Pedido"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(368, 88)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(88, 20)
        Me.TextBox4.TabIndex = 7
        Me.TextBox4.Tag = "Monto Total de la Factura"
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(232, 88)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(80, 20)
        Me.TextBox3.TabIndex = 6
        Me.TextBox3.Tag = "N�mero de la Factura"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(88, 56)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(80, 20)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Tag = "N�mero de Pedido de la Liquidaci�n"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(264, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(40, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Tag = "Cantidad de D�as de Cr�dito"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(432, 24)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 2
        Me.DateTimePicker2.Tag = "Fecha de Vencimiento del Cr�dito"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(104, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 0
        Me.DateTimePicker1.Tag = "Fecha de la Factura"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(328, 88)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Monto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(160, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "No Factura"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(184, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Proveedor"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "No Pedido"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(320, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Fecha Vencimiento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(232, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "D�as"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha Factura"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ComboBox11)
        Me.GroupBox2.Controls.Add(Me.ComboBox10)
        Me.GroupBox2.Controls.Add(Me.ComboBox9)
        Me.GroupBox2.Controls.Add(Me.ComboBox8)
        Me.GroupBox2.Controls.Add(Me.TextBox12)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.TextBox11)
        Me.GroupBox2.Controls.Add(Me.TextBox10)
        Me.GroupBox2.Controls.Add(Me.ComboBox7)
        Me.GroupBox2.Controls.Add(Me.ComboBox6)
        Me.GroupBox2.Controls.Add(Me.ComboBox5)
        Me.GroupBox2.Controls.Add(Me.ComboBox4)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 216)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(600, 384)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detalle del Pedido"
        '
        'ComboBox11
        '
        Me.ComboBox11.Location = New System.Drawing.Point(328, 112)
        Me.ComboBox11.Name = "ComboBox11"
        Me.ComboBox11.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox11.TabIndex = 30
        Me.ComboBox11.Text = "ComboBox11"
        Me.ComboBox11.Visible = False
        '
        'ComboBox10
        '
        Me.ComboBox10.Location = New System.Drawing.Point(304, 112)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox10.TabIndex = 29
        Me.ComboBox10.Text = "ComboBox10"
        Me.ComboBox10.Visible = False
        '
        'ComboBox9
        '
        Me.ComboBox9.Location = New System.Drawing.Point(280, 112)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox9.TabIndex = 28
        Me.ComboBox9.Text = "ComboBox9"
        Me.ComboBox9.Visible = False
        '
        'ComboBox8
        '
        Me.ComboBox8.Location = New System.Drawing.Point(256, 112)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox8.TabIndex = 27
        Me.ComboBox8.Text = "ComboBox8"
        Me.ComboBox8.Visible = False
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(504, 24)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(80, 20)
        Me.TextBox12.TabIndex = 26
        Me.TextBox12.Tag = "Cantidad del Producto en el Pedido"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(448, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Cantidad"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.MSFlexGrid1)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.Label19)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.TextBox24)
        Me.GroupBox3.Controls.Add(Me.TextBox23)
        Me.GroupBox3.Controls.Add(Me.TextBox22)
        Me.GroupBox3.Controls.Add(Me.TextBox21)
        Me.GroupBox3.Controls.Add(Me.TextBox20)
        Me.GroupBox3.Controls.Add(Me.TextBox19)
        Me.GroupBox3.Controls.Add(Me.TextBox18)
        Me.GroupBox3.Controls.Add(Me.TextBox17)
        Me.GroupBox3.Controls.Add(Me.TextBox16)
        Me.GroupBox3.Controls.Add(Me.TextBox15)
        Me.GroupBox3.Controls.Add(Me.TextBox14)
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 128)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(584, 248)
        Me.GroupBox3.TabIndex = 24
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Costos e Impuestos"
        '
        'MSFlexGrid1
        '
        Me.MSFlexGrid1.Location = New System.Drawing.Point(8, 120)
        Me.MSFlexGrid1.Name = "MSFlexGrid1"
        Me.MSFlexGrid1.OcxState = CType(resources.GetObject("MSFlexGrid1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid1.Size = New System.Drawing.Size(568, 120)
        Me.MSFlexGrid1.TabIndex = 50
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(448, 88)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(52, 13)
        Me.Label26.TabIndex = 49
        Me.Label26.Text = "TOT U$"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(304, 88)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(51, 13)
        Me.Label25.TabIndex = 48
        Me.Label25.Text = "TOT C$"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(152, 88)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(57, 13)
        Me.Label24.TabIndex = 47
        Me.Label24.Text = "UN. US$"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(8, 88)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(48, 13)
        Me.Label23.TabIndex = 46
        Me.Label23.Text = "UN. C$"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(416, 56)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(75, 13)
        Me.Label22.TabIndex = 45
        Me.Label22.Text = "COSTO UN."
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(272, 56)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(51, 13)
        Me.Label21.TabIndex = 44
        Me.Label21.Text = "POLIZA"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(136, 56)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(37, 13)
        Me.Label20.TabIndex = 43
        Me.Label20.Text = "TSIM"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(8, 56)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(27, 13)
        Me.Label19.TabIndex = 42
        Me.Label19.Text = "IVA"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(464, 24)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(28, 13)
        Me.Label18.TabIndex = 41
        Me.Label18.Text = "DAI"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(320, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 40
        Me.Label17.Text = "CIF C$"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(168, 24)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(54, 13)
        Me.Label16.TabIndex = 39
        Me.Label16.Text = "CIF US$"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 24)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(59, 13)
        Me.Label15.TabIndex = 38
        Me.Label15.Text = "FOB US$"
        '
        'TextBox24
        '
        Me.TextBox24.Location = New System.Drawing.Point(496, 88)
        Me.TextBox24.MaxLength = 10
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.ReadOnly = True
        Me.TextBox24.Size = New System.Drawing.Size(80, 20)
        Me.TextBox24.TabIndex = 28
        Me.TextBox24.Tag = "Total del Producto Expresado en D�lares"
        Me.TextBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox23
        '
        Me.TextBox23.Location = New System.Drawing.Point(360, 88)
        Me.TextBox23.MaxLength = 10
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.ReadOnly = True
        Me.TextBox23.Size = New System.Drawing.Size(80, 20)
        Me.TextBox23.TabIndex = 27
        Me.TextBox23.Tag = "Total del Producto Expresado en C�rdobas"
        Me.TextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(208, 88)
        Me.TextBox22.MaxLength = 10
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.ReadOnly = True
        Me.TextBox22.Size = New System.Drawing.Size(80, 20)
        Me.TextBox22.TabIndex = 26
        Me.TextBox22.Tag = "Costo Unitario Expresado en D�lares"
        Me.TextBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox21
        '
        Me.TextBox21.Location = New System.Drawing.Point(56, 88)
        Me.TextBox21.MaxLength = 10
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.ReadOnly = True
        Me.TextBox21.Size = New System.Drawing.Size(80, 20)
        Me.TextBox21.TabIndex = 25
        Me.TextBox21.Tag = "Costo Unitario Expresado en C�rdobas"
        Me.TextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox20
        '
        Me.TextBox20.Location = New System.Drawing.Point(496, 56)
        Me.TextBox20.MaxLength = 10
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.ReadOnly = True
        Me.TextBox20.Size = New System.Drawing.Size(80, 20)
        Me.TextBox20.TabIndex = 24
        Me.TextBox20.Tag = "Costo Unitario del Producto"
        Me.TextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(320, 56)
        Me.TextBox19.MaxLength = 10
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.ReadOnly = True
        Me.TextBox19.Size = New System.Drawing.Size(80, 20)
        Me.TextBox19.TabIndex = 23
        Me.TextBox19.Tag = "Monto de la Poliza Expresado en C�rdobas"
        Me.TextBox19.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(176, 56)
        Me.TextBox18.MaxLength = 10
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.ReadOnly = True
        Me.TextBox18.Size = New System.Drawing.Size(80, 20)
        Me.TextBox18.TabIndex = 22
        Me.TextBox18.Tag = "Monto del TSIM Expresado en C�rdobas"
        Me.TextBox18.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(40, 56)
        Me.TextBox17.MaxLength = 10
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.ReadOnly = True
        Me.TextBox17.Size = New System.Drawing.Size(80, 20)
        Me.TextBox17.TabIndex = 21
        Me.TextBox17.Tag = "Monto del IVA Expresado en C�rdobas"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(496, 24)
        Me.TextBox16.MaxLength = 10
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.ReadOnly = True
        Me.TextBox16.Size = New System.Drawing.Size(80, 20)
        Me.TextBox16.TabIndex = 20
        Me.TextBox16.Tag = "Monto del DAI Expresado en C�rdobas"
        Me.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(368, 24)
        Me.TextBox15.MaxLength = 10
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(80, 20)
        Me.TextBox15.TabIndex = 19
        Me.TextBox15.Tag = "Monto del CIF Expresado en C�rdobas"
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(224, 24)
        Me.TextBox14.MaxLength = 10
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(80, 20)
        Me.TextBox14.TabIndex = 18
        Me.TextBox14.Tag = "Monto del CIF Expresado en D�lares"
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(72, 24)
        Me.TextBox13.MaxLength = 10
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(80, 20)
        Me.TextBox13.TabIndex = 17
        Me.TextBox13.Tag = "Monto del FOB Expresado en D�lares"
        Me.TextBox13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(160, 24)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(280, 20)
        Me.TextBox11.TabIndex = 12
        Me.TextBox11.Tag = "Descripci�n del Producto"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(64, 24)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(80, 20)
        Me.TextBox10.TabIndex = 11
        Me.TextBox10.Tag = "C�digo del Producto"
        '
        'ComboBox7
        '
        Me.ComboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox7.Location = New System.Drawing.Point(360, 88)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(224, 21)
        Me.ComboBox7.TabIndex = 16
        Me.ComboBox7.Tag = "Empaque del Producto"
        '
        'ComboBox6
        '
        Me.ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox6.Location = New System.Drawing.Point(64, 88)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(216, 21)
        Me.ComboBox6.TabIndex = 15
        Me.ComboBox6.Tag = "Unidad del Producto"
        '
        'ComboBox5
        '
        Me.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox5.Location = New System.Drawing.Point(360, 56)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(224, 21)
        Me.ComboBox5.TabIndex = 14
        Me.ComboBox5.Tag = "SubClase del Producto"
        '
        'ComboBox4
        '
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.Location = New System.Drawing.Point(56, 54)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(224, 21)
        Me.ComboBox4.TabIndex = 13
        Me.ComboBox4.Tag = "Clase del Producto"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(296, 88)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 13)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "Empaque"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 88)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "Unidad"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(296, 56)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "SubClase"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Clase"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "C�digo"
        '
        'frmLiqProd
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(600, 625)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmLiqProd"
        Me.Text = "frmLiqProd"
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.MSFlexGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Dim lngCodigo As Long
    Dim lngRegistro2 As Long
    Public cboCollection As New Collection
    Public txtCollection As New Collection

    Private Sub frmLiqProd_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        cboCollection.Add(ComboBox1)
        cboCollection.Add(ComboBox2)
        cboCollection.Add(ComboBox3)
        cboCollection.Add(ComboBox4)
        cboCollection.Add(ComboBox5)
        cboCollection.Add(ComboBox6)
        cboCollection.Add(ComboBox7)
        cboCollection.Add(ComboBox8)
        cboCollection.Add(ComboBox9)
        cboCollection.Add(ComboBox10)
        cboCollection.Add(ComboBox11)
        txtCollection.Add(TextBox1)
        txtCollection.Add(TextBox2)
        txtCollection.Add(TextBox3)
        txtCollection.Add(TextBox4)
        txtCollection.Add(TextBox5)
        txtCollection.Add(TextBox6)
        txtCollection.Add(TextBox7)
        txtCollection.Add(TextBox8)
        txtCollection.Add(TextBox9)
        txtCollection.Add(TextBox10)
        txtCollection.Add(TextBox11)
        txtCollection.Add(TextBox12)
        txtCollection.Add(TextBox13)
        txtCollection.Add(TextBox14)
        txtCollection.Add(TextBox15)
        txtCollection.Add(TextBox16)
        txtCollection.Add(TextBox17)
        txtCollection.Add(TextBox18)
        txtCollection.Add(TextBox19)
        txtCollection.Add(TextBox20)
        txtCollection.Add(TextBox21)
        txtCollection.Add(TextBox22)
        txtCollection.Add(TextBox23)
        txtCollection.Add(TextBox24)
        txtCollection.Add(TextBox25)
        Iniciar()
        Limpiar()
        dblTipoCambio = 0
        intIncr = 0
        MSFlexGrid1.FormatString = "|<C�digo      |<Descripci�n                        |>Cantidad  |>FOB US$  |^Extraer |^Eliminar |<Registro"
        intModulo = 10

    End Sub

    Private Sub frmLiqProd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString()
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
            Case 3
                MoverRegistro(sender.text)
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub Guardar()

        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngLiqProdEnc", cnnAgro2K)       ' Asignacion a un Store Procedure ya creado en SQL
        Dim prmTmp01 As New SqlParameter                                ' Primer Parametro en el Store Procedure
        Dim prmTmp02 As New SqlParameter                                ' Segundo Parametro en el Store Procedure
        Dim prmTmp03 As New SqlParameter                                ' Tercer Parametro en el Store Procedure
        Dim prmTmp04 As New SqlParameter                                ' Cuarto Parametro en el Store Procedure
        Dim prmTmp05 As New SqlParameter                                ' Quinto Parametro en el Store Procedure
        Dim prmTmp06 As New SqlParameter                                ' Primer Parametro en el Store Procedure
        Dim prmTmp07 As New SqlParameter                                ' Segundo Parametro en el Store Procedure
        Dim prmTmp08 As New SqlParameter                                ' Tercer Parametro en el Store Procedure
        Dim prmTmp09 As New SqlParameter                                ' Cuarto Parametro en el Store Procedure
        Dim prmTmp10 As New SqlParameter                                ' Quinto Parametro en el Store Procedure
        Dim prmTmp11 As New SqlParameter                                ' Primer Parametro en el Store Procedure
        Dim prmTmp12 As New SqlParameter                                ' Segundo Parametro en el Store Procedure
        Dim prmTmp13 As New SqlParameter                                ' Tercer Parametro en el Store Procedure
        Dim prmTmp14 As New SqlParameter                                ' Cuarto Parametro en el Store Procedure
        Dim prmTmp15 As New SqlParameter                                ' Quinto Parametro en el Store Procedure
        Dim lngNumFecha As Long

        lngRegistro = 0
        lngNumFecha = 0
        lngRegistro2 = 0
        lngNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
        lngRegistro = (lngNumFecha * 1000000) + Format(Now, "HHmmss")
        ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
        lngRegistro2 = ComboBox2.SelectedItem
        With prmTmp01                                                   ' Primer Parametro
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegistro
        End With
        With prmTmp02                                                   ' Primer Parametro
            .ParameterName = "@strFechaFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp03                                                   ' Segundo Parametro
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngNumFecha
        End With
        With prmTmp04                                                   ' Tercer Parametro
            .ParameterName = "@intCantDias"
            .SqlDbType = SqlDbType.TinyInt
            .Value = TextBox1.Text
        End With
        With prmTmp05                                                   ' Cuarto Parametro
            .ParameterName = "@strNumPedido"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox2.Text
        End With
        With prmTmp06                                                   ' Primer Parametro
            .ParameterName = "@lngProveedor"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegistro2
        End With
        With prmTmp07                                                   ' Primer Parametro
            .ParameterName = "@intTipoPedido"
            .SqlDbType = SqlDbType.Int
            .Value = 0
        End With
        With prmTmp08                                                   ' Segundo Parametro
            .ParameterName = "@strNumFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox3.Text
        End With
        With prmTmp09                                                   ' Tercer Parametro
            .ParameterName = "@dblMonto"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox4.Text
        End With
        With prmTmp10                                                   ' Cuarto Parametro
            .ParameterName = "@dblDAI"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox5.Text
        End With
        With prmTmp11                                                   ' Primer Parametro
            .ParameterName = "@dblIVA"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox6.Text
        End With
        With prmTmp12                                                   ' Primer Parametro
            .ParameterName = "@dblTSIM"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox7.Text
        End With
        With prmTmp13                                                   ' Segundo Parametro
            .ParameterName = "@dblConstante"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox8.Text
        End With
        With prmTmp14                                                   ' Tercer Parametro
            .ParameterName = "@dblCstoIntrod"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox9.Text
        End With
        With prmTmp15                                                   ' Cuarto Parametro
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp                                                     ' Asignar Parametros al Store Procedures
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then          ' Cerrar la Conexion si esta Abierta
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then                  ' Cerrar la Conexion si esta Abierta
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()                                                ' Abrir Conexion
        cmdTmp.Connection = cnnAgro2K                                   ' Asignar el Commando a la Conexion

        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            lngRegistro = TextBox3.Text
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        cmdTmp.Parameters(0).Value = lngRegistro
        cmdTmp.Parameters(14).Value = 1

        Try
            cmdTmp.ExecuteNonQuery()                                    ' Ejecuta el Store Procedure sin Esperar Datos de Regreso
            If intResp = 6 Then
            ElseIf intResp = 0 Then
            End If
            GuardarDetalle()
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub GuardarDetalle()

        Dim cmdTmp As New SqlCommand("sp_IngLiqProdDet", cnnAgro2K)       ' Asignacion a un Store Procedure ya creado en SQL
        Dim prmTmp01 As New SqlParameter                                ' Primer Parametro en el Store Procedure
        Dim prmTmp02 As New SqlParameter                                ' Segundo Parametro en el Store Procedure
        Dim prmTmp03 As New SqlParameter                                ' Tercer Parametro en el Store Procedure
        Dim prmTmp04 As New SqlParameter                                ' Cuarto Parametro en el Store Procedure
        Dim prmTmp05 As New SqlParameter                                ' Quinto Parametro en el Store Procedure
        Dim prmTmp06 As New SqlParameter                                ' Primer Parametro en el Store Procedure

        lngRegistro2 = 0
        lngRegistro2 = lngRegistro
        lngRegistro = 0
        lngRegistro = Format(Now, "Hmmssffff")
        With prmTmp01                                                   ' Primer Parametro
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegistro
        End With
        With prmTmp02                                                   ' Primer Parametro
            .ParameterName = "@lngProducto"
            .SqlDbType = SqlDbType.BigInt
            .Value = 0
        End With
        With prmTmp03                                                   ' Segundo Parametro
            .ParameterName = "@dblCantidad"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp04                                                   ' Tercer Parametro
            .ParameterName = "@dblMontoFob"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp05                                                   ' Cuarto Parametro
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp06                                                   ' Cuarto Parametro
            .ParameterName = "@lngRegistro2"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegistro2
        End With
        With cmdTmp                                                     ' Asignar Parametros al Store Procedures
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then          ' Cerrar la Conexion si esta Abierta
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then                  ' Cerrar la Conexion si esta Abierta
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()                                                ' Abrir Conexion
        cmdTmp.Connection = cnnAgro2K                                   ' Asignar el Commando a la Conexion

        MSFlexGrid1.Col = 1
        For intIncr = 1 To MSFlexGrid1.Rows - 2
            MSFlexGrid1.Row = intIncr
            lngRegistro = 0
            lngRegistro = Format(Now, "Hmmssffff")
            cmdTmp.Parameters(0).Value = lngRegistro
            MSFlexGrid1.Col = 7
            cmdTmp.Parameters(1).Value = MSFlexGrid1.Text
            MSFlexGrid1.Col = 3
            cmdTmp.Parameters(2).Value = MSFlexGrid1.Text
            MSFlexGrid1.Col = 4
            cmdTmp.Parameters(3).Value = MSFlexGrid1.Text
            cmdTmp.Parameters(4).Value = 0
            Try
                cmdTmp.ExecuteNonQuery()                                    ' Ejecuta el Store Procedure sin Esperar Datos de Regreso
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                Exit Sub
            End Try
        Next intIncr

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Nombre From prm_Proveedores"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Clases"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox4.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox8.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_SubClases"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox5.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox9.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Unidades"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox6.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox10.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Empaques"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox7.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox11.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        DateTimePicker1.Value = Now
        DateTimePicker2.Value = Now
        For intIncr = 1 To cboCollection.Count
            cboCollection.Item(intIncr).SelectedIndex = -1
        Next
        For intIncr = 1 To txtCollection.Count
            txtCollection.Item(intIncr).Text = "0.00"
        Next
        TextBox1.Text = "0"
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = "0"
        DateTimePicker1.Focus()

    End Sub

    Sub Limpiar2()

        For intIncr = 4 To cboCollection.Count
            cboCollection.Item(intIncr).SelectedIndex = -1
        Next
        For intIncr = 10 To txtCollection.Count
            txtCollection.Item(intIncr).Text = "0.00"
        Next
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = "0"
        TextBox10.Focus()

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, TextBox3.GotFocus, TextBox4.GotFocus, ComboBox1.GotFocus, ComboBox3.GotFocus, ComboBox4.GotFocus, ComboBox5.GotFocus, ComboBox6.GotFocus, ComboBox7.GotFocus, DateTimePicker1.GotFocus, DateTimePicker2.GotFocus, TextBox7.GotFocus, TextBox6.GotFocus, TextBox5.GotFocus, TextBox8.GotFocus, TextBox11.GotFocus, TextBox10.GotFocus, TextBox9.GotFocus, TextBox24.GotFocus, TextBox23.GotFocus, TextBox22.GotFocus, TextBox21.GotFocus, TextBox20.GotFocus, TextBox19.GotFocus, TextBox18.GotFocus, TextBox17.GotFocus, TextBox16.GotFocus, TextBox15.GotFocus, TextBox14.GotFocus, TextBox13.GotFocus, TextBox12.GotFocus

        Dim strCampo As String = String.Empty

        Select Case sender.name.ToString()
            Case "TextBox1" : strCampo = "D�as"
            Case "TextBox2" : strCampo = "No Pedido"
            Case "TextBox3" : strCampo = "No Factura"
            Case "TextBox4" : strCampo = "Monto"
            Case "TextBox5" : strCampo = "DAI"
            Case "TextBox6" : strCampo = "IVA"
            Case "TextBox7" : strCampo = "TSIM"
            Case "TextBox8" : strCampo = "CONSTANTE"
            Case "TextBox9" : strCampo = "CSTO INTROD"
            Case "TextBox10" : strCampo = "C�digo"
            Case "TextBox11" : strCampo = "Descripci�n"
            Case "TextBox12" : strCampo = "Cantidad"
            Case "TextBox13" : strCampo = "FOB US$"
            Case "TextBox14" : strCampo = "CIF US$"
            Case "TextBox15" : strCampo = "CIF C$"
            Case "TextBox16" : strCampo = "DAI"
            Case "TextBox17" : strCampo = "IVA"
            Case "TextBox18" : strCampo = "TSIM"
            Case "TextBox19" : strCampo = "POLIZA"
            Case "TextBox20" : strCampo = "COSTO UNITARIO"
            Case "TextBox21" : strCampo = "UN. C$"
            Case "TextBox22" : strCampo = "UN. US$"
            Case "TextBox23" : strCampo = "TOT C$"
            Case "TextBox24" : strCampo = "TOT US$"
            Case "ComboBox1" : strCampo = "Proveedor"
            Case "ComboBox3" : strCampo = "Tipo Pedido"
            Case "ComboBox4" : strCampo = "Clase"
            Case "ComboBox5" : strCampo = "SubClase"
            Case "ComboBox6" : strCampo = "Unidad"
            Case "ComboBox7" : strCampo = "Empaque"
            Case "DateTimePicker1" : strCampo = "Fecha Factura"
            Case "DateTimePicker2" : strCampo = "Fecha Vencimiento"
        End Select
        StatusBar1.Panels.Item(0).Text = strCampo
        StatusBar1.Panels.Item(1).Text = sender.tag
        If sender.name <> "DateTimePicker1" And sender.name <> "DateTimePicker2" Then
            sender.selectall()
        End If

    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, TextBox3.KeyDown, TextBox4.KeyDown, ComboBox1.KeyDown, ComboBox3.KeyDown, ComboBox4.KeyDown, ComboBox5.KeyDown, ComboBox6.KeyDown, ComboBox7.KeyDown, DateTimePicker1.KeyDown, DateTimePicker2.KeyDown, TextBox7.KeyDown, TextBox6.KeyDown, TextBox5.KeyDown, TextBox8.KeyDown, TextBox11.KeyDown, TextBox10.KeyDown, TextBox9.KeyDown, TextBox24.KeyDown, TextBox23.KeyDown, TextBox22.KeyDown, TextBox21.KeyDown, TextBox20.KeyDown, TextBox19.KeyDown, TextBox18.KeyDown, TextBox17.KeyDown, TextBox16.KeyDown, TextBox15.KeyDown, TextBox14.KeyDown, TextBox13.KeyDown, TextBox12.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString()
                Case "DateTimePicker1" : TextBox1.Focus()
                Case "TextBox1" : TextBox2.Focus()
                Case "TextBox2" : ComboBox1.Focus()
                Case "ComboBox1" : ComboBox3.Focus()
                Case "ComboBox3" : TextBox3.Focus()
                Case "TextBox3" : TextBox4.Focus()
                Case "TextBox4" : TextBox5.Focus() : ValidarFormatear(sender)
                Case "TextBox5" : TextBox6.Focus() : ValidarFormatear(sender)
                Case "TextBox6" : TextBox7.Focus() : ValidarFormatear(sender)
                Case "TextBox7" : TextBox8.Focus() : ValidarFormatear(sender)
                Case "TextBox8" : TextBox9.Focus() : ValidarFormatear(sender) : Label30.Text = Format(dblTipoCambio * (CDbl(TextBox4.Text) + CDbl(TextBox8.Text)), "#,##0.#0")
                Case "TextBox9" : TextBox10.Focus() : ValidarFormatear(sender)
                Case "TextBox10" : TextBox11.Focus()
                    blnUbicar = False
                    If UbicarCodigo(TextBox10.Text) = False Then
                        MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                        TextBox10.Focus()
                    End If
                Case "TextBox11" : TextBox12.Focus()
                Case "TextBox12" : IIf(lngCodigo = 0, ComboBox4.Focus(), TextBox13.Focus())
                Case "ComboBox4" : ComboBox5.Focus()
                Case "ComboBox5" : ComboBox6.Focus()
                Case "ComboBox6" : ComboBox7.Focus()
                Case "ComboBox7" : TextBox13.Focus()
                Case "TextBox13"
                    CalcularCostos()
                    intResp = MsgBox("�Desea Grabar la Transacci�n?", MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1, "Ingreso de Pedido")
                    If intResp = 6 Then
                        intIncr = intIncr + 1
                        If MSFlexGrid1.Rows = 1 Then
                            MSFlexGrid1.Rows = MSFlexGrid1.Rows + 1
                        End If
                        MSFlexGrid1.Row = MSFlexGrid1.Rows - 1
                        MSFlexGrid1.Col = 1
                        MSFlexGrid1.Text = TextBox10.Text
                        MSFlexGrid1.Col = 2
                        MSFlexGrid1.Text = TextBox11.Text
                        MSFlexGrid1.Col = 3
                        MSFlexGrid1.Text = Format(CDbl(TextBox12.Text), "#####0.#0")
                        MSFlexGrid1.Col = 4
                        MSFlexGrid1.Text = Format(CDbl(TextBox13.Text), "#####0.#0")
                        MSFlexGrid1.Col = 5
                        MSFlexGrid1.Text = "O"
                        MSFlexGrid1.Col = 6
                        MSFlexGrid1.Text = "X"
                        MSFlexGrid1.Col = 7
                        MSFlexGrid1.Text = lngCodigo
                        MSFlexGrid1.Rows = MSFlexGrid1.Rows + 1
                        Limpiar2()
                    Else
                        TextBox13.Focus()
                    End If
            End Select
        End If

    End Sub

    Sub ValidarFormatear(ByVal sender As Object)

        If IsNumeric(sender.text) = False Then
            sender.text = "0.00"
        Else
            sender.text = Format(CDbl(sender.text), "#,###.#0")
        End If

    End Sub

    Sub CalcularCostos()

        TextBox13.Text = Format(CDbl(TextBox13.Text), "#,##0.#0")
        TextBox14.Text = Format((CDbl(TextBox13.Text) * (CDbl(TextBox8.Text) / CDbl(TextBox4.Text))) + CDbl(TextBox13.Text), "#,##0.#0")
        TextBox15.Text = Format(CDbl(TextBox14.Text) * dblTipoCambio, "#,##0.#0")
        TextBox16.Text = Format((CDbl(TextBox5.Text) / CDbl(Label30.Text)) * CDbl(TextBox15.Text), "#,##0.#0")
        TextBox17.Text = Format((CDbl(TextBox6.Text) / Label30.Text) * CDbl(TextBox15.Text), "#,##0.#0")
        TextBox18.Text = Format((CDbl(TextBox7.Text) / Label30.Text) * CDbl(TextBox15.Text), "#,##0.#0")
        TextBox19.Text = Format(CDbl(TextBox16.Text) + CDbl(TextBox17.Text) + CDbl(TextBox18.Text), "#,##0.#0")
        TextBox20.Text = Format((CDbl(TextBox9.Text) / Label30.Text) * CDbl(TextBox15.Text), "#,##0.#0")
        TextBox23.Text = Format(CDbl(TextBox15.Text) + CDbl(TextBox19.Text) + CDbl(TextBox20.Text), "#,##0.#0")
        TextBox24.Text = Format(CDbl(TextBox23.Text) / dblTipoCambio, "#,##0.#0")
        TextBox21.Text = Format(CDbl(TextBox23.Text) / CDbl(TextBox12.Text), "#,##0.#0")
        TextBox22.Text = Format(CDbl(TextBox21.Text) / dblTipoCambio, "#,##0.#0")

    End Sub

    Private Sub MSFlexGrid1_ClickEvent(ByVal sender As Object, ByVal e As System.EventArgs) Handles MSFlexGrid1.ClickEvent

        If MSFlexGrid1.Col = 5 Then
            MSFlexGrid1.Col = 1
            If Trim(MSFlexGrid1.Text) <> "" Then
                TextBox10.Text = MSFlexGrid1.Text
                TextBox10.Focus()
                If UbicarCodigo(TextBox10.Text) = True Then
                    MSFlexGrid1.Col = 3
                    TextBox12.Text = MSFlexGrid1.Text
                    MSFlexGrid1.Col = 4
                    TextBox13.Text = MSFlexGrid1.Text
                    CalcularCostos()
                End If
            End If
        ElseIf MSFlexGrid1.Col = 6 Then
            intResp = MsgBox("�Desea Eliminar el Registro del Pedido?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, "Eliminaci�n de Registro")
            If intResp = 6 Then
                If MSFlexGrid1.Rows = 1 Then
                    MSFlexGrid1.Row = 1
                    MSFlexGrid1.Col = 1
                    MSFlexGrid1.Text = ""
                    MSFlexGrid1.Col = 2
                    MSFlexGrid1.Text = ""
                    MSFlexGrid1.Col = 3
                    MSFlexGrid1.Text = ""
                    MSFlexGrid1.Col = 4
                    MSFlexGrid1.Text = ""
                Else
                    MSFlexGrid1.RemoveItem(MSFlexGrid1.Row)
                End If
            End If
        End If

    End Sub

    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker1.LostFocus

        Dim lngFecha As Long

        lngFecha = 0
        lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
        strQuery = "Select Tipo_Cambio from prm_TipoCambio Where numfecha = " & lngFecha & ""
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        dblTipoCambio = 0
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                dblTipoCambio = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()

    End Sub

    Function UbicarCodigo(ByVal strDato As String) As Boolean

        Dim lngClase, lngSubClase, lngUnidad, lngEmpaque As Long

        TextBox11.ReadOnly = False
        ComboBox4.Enabled = True
        ComboBox5.Enabled = True
        ComboBox6.Enabled = True
        ComboBox7.Enabled = True

        strQuery = ""
        strQuery = "Select Codigo, Descripcion, RegClase, RegSubClase, RegUnidad, RegEmpaque from prm_Productos Where codigo = '" & strDato & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        lngCodigo = 0
        While dtrAgro2K.Read
            TextBox11.Text = dtrAgro2K.GetValue(1)
            TextBox11.ReadOnly = True
            TextBox12.Focus()
            lngClase = dtrAgro2K.GetValue(2)
            lngSubClase = dtrAgro2K.GetValue(3)
            lngUnidad = dtrAgro2K.GetValue(4)
            lngEmpaque = dtrAgro2K.GetValue(5)
            ComboBox4.Enabled = False
            ComboBox5.Enabled = False
            ComboBox6.Enabled = False
            ComboBox7.Enabled = False
        End While
        ComboBox8.SelectedIndex = -1
        For intIncr = 0 To ComboBox8.Items.Count - 1
            ComboBox8.SelectedIndex = intIncr
            If ComboBox8.SelectedItem = lngClase Then
                ComboBox4.SelectedIndex = ComboBox8.SelectedIndex
                Exit For
            End If
        Next intIncr
        ComboBox9.SelectedIndex = -1
        For intIncr = 0 To ComboBox9.Items.Count - 1
            ComboBox9.SelectedIndex = intIncr
            If ComboBox9.SelectedItem = lngSubClase Then
                ComboBox5.SelectedIndex = ComboBox9.SelectedIndex
                Exit For
            End If
        Next intIncr
        ComboBox10.SelectedIndex = -1
        For intIncr = 0 To ComboBox10.Items.Count - 1
            ComboBox10.SelectedIndex = intIncr
            If ComboBox10.SelectedItem = lngUnidad Then
                ComboBox6.SelectedIndex = ComboBox10.SelectedIndex
                Exit For
            End If
        Next intIncr
        ComboBox11.SelectedIndex = -1
        For intIncr = 0 To ComboBox11.Items.Count - 1
            ComboBox11.SelectedIndex = intIncr
            If ComboBox11.SelectedItem = lngEmpaque Then
                ComboBox7.SelectedIndex = ComboBox11.SelectedIndex
                Exit For
            End If
        Next intIncr
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        UbicarCodigo = True

    End Function

    Sub MoverRegistro(ByVal StrDato As String)

        Dim lngNumFecha As Long

        lngNumFecha = 0
        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(Registro) from tbl_LiqProd_Enc"
            Case "Anterior"
                strQuery = "Select Max(Registro) from tbl_LiqProd_Enc Where Registro < '" & TextBox25.Text & "'"
            Case "Siguiente"
                strQuery = "Select Min(Registro) from tbl_LiqProd_Enc Where Registro > '" & TextBox25.Text & "'"
            Case "Ultimo"
                strQuery = "Select Max(Registro) from tbl_LiqProd_Enc"
            Case "Igual"
                lngNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
                strQuery = "Select Registro from tbl_LiqProd_Enc Where numfecha = " & lngNumFecha & " and documento = '" & TextBox1.Text & "'"
        End Select
        Limpiar()
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        lngRegistro = 0
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                lngRegistro = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If lngRegistro = 0 Then
            If StrDato = "Igual" Then
                MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
            Else
                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From tbl_LiqProd_Enc Where Registro = '" & lngRegistro & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox25.Text = dtrAgro2K.GetValue(0)
            DateTimePicker1.Value = dtrAgro2K.GetValue(1)
            TextBox1.Text = dtrAgro2K.GetValue(3)
            TextBox2.Text = dtrAgro2K.GetValue(4)
            TextBox6.Text = dtrAgro2K.GetValue(7)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

End Class
