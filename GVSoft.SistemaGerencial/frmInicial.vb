Public Class frmInicial
    Inherits DevComponents.DotNetBar.Office2007Form
    ' Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInicial))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(13, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(473, 386)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Courier New", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(499, 129)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(461, 119)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Sistemas Gerenciales"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label3.Location = New System.Drawing.Point(589, 316)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(180, 27)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Windows 7/10"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Courier New", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label4.Location = New System.Drawing.Point(587, 351)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(236, 27)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Versi�n 24.01.21"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmInicial
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(8, 19)
        Me.ClientSize = New System.Drawing.Size(956, 430)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmInicial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private CloseApp As Boolean = True

    Private Sub frmInicial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Timer1.Enabled = True
        Timer1.Interval = 3000
        CloseApp = False
        For intIncr = 0 To 42
            strModuloNombre(intIncr) = ""
        Next intIncr
        For intIncr = 0 To 236
            intAccesos(intIncr) = 0
        Next intIncr
        strModuloNombre(0) = "Usuarios"
        strModuloNombre(1) = "Agencias"
        strModuloNombre(2) = "Departamentos"
        strModuloNombre(3) = "Municipios"
        strModuloNombre(4) = "Vendedores"
        strModuloNombre(5) = "Clientes"
        strModuloNombre(6) = "Clases"
        strModuloNombre(7) = "SubClases"
        strModuloNombre(8) = "Empaques"
        strModuloNombre(9) = "Unidades"
        strModuloNombre(10) = "Proveedores"
        strModuloNombre(11) = "Productos"
        strModuloNombre(12) = "Conversiones"
        strModuloNombre(13) = "Negocios"
        strModuloNombre(14) = "ProductoMovimiento"
        strModuloNombre(15) = "ProductoConversiones"
        strModuloNombre(16) = "Presupuestos"
        strModuloNombre(17) = "Razones_Anular"
        strModuloNombre(18) = "Facturas"
        strModuloNombre(19) = "TipoCambio"
        strModuloNombre(20) = "Reportes"
        strModuloNombre(21) = "CuentasCobrar"
        strModuloNombre(22) = "Bancos"
        strModuloNombre(23) = "CuentasContables"
        strModuloNombre(24) = "CambiarNumero"
        strModuloNombre(25) = "Areas"
        strModuloNombre(26) = "Puestos"
        strModuloNombre(27) = "Empleados"
        strModuloNombre(28) = "CuentasBancarias"
        strModuloNombre(29) = "Chequeras"
        strModuloNombre(30) = "Porcentajes"
        strModuloNombre(31) = "Porcentajes2"
        strModuloNombre(32) = "PrmRecibosCaja"
        strModuloNombre(33) = "PrmNotasCredito"
        strModuloNombre(34) = "PrmNotasDebito"
        strModuloNombre(35) = "Comisiones"
        strModuloNombre(36) = "LetrasCambio"
        strModuloNombre(37) = "PartidasContables"
        strModuloNombre(38) = "Cheques"
        strModuloNombre(39) = "Depositos"
        strModuloNombre(40) = "NotasDeCredito"
        strModuloNombre(41) = "NotasDeDebito"
        strModuloNombre(42) = "Beneficiarios"
        ' 6 es Yes
        ' 7 es No
        ' 2 es Cancel

    End Sub

    Private Sub frmInicial_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        If CloseApp = True Then
            Application.Exit()
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Dim frmNew As New frmLogin

        Timer1.Interval = 1
        Timer1.Enabled = False
        frmNew.Show()
        Me.Close()

    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub
End Class
