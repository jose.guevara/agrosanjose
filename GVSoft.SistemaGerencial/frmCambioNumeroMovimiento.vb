Imports System.Data.SqlClient
Imports System.Text

Public Class frmCambioNumeroMovimiento
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNumero As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ddlAgencia As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ddlEstado As System.Windows.Forms.ComboBox
    Friend WithEvents cntmnuCambNumFact As System.Windows.Forms.ContextMenu
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtIdRegistro As System.Windows.Forms.TextBox
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnListado As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnGeneral As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCambios As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReportes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnRegistros As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrimerRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAnteriorRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSiguienteRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUltimoRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents txtIdRegistroAActualizar As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCambioNumeroMovimiento))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ddlSerie = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtIdRegistroAActualizar = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.ddlEstado = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.ddlAgencia = New System.Windows.Forms.ComboBox
        Me.txtIdRegistro = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNumero = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.cntmnuCambNumFact = New System.Windows.Forms.ContextMenu
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.btnListado = New DevComponents.DotNetBar.ButtonItem
        Me.btnGeneral = New DevComponents.DotNetBar.ButtonItem
        Me.btnCambios = New DevComponents.DotNetBar.ButtonItem
        Me.btnReportes = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.btnRegistros = New DevComponents.DotNetBar.ButtonItem
        Me.btnPrimerRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnAnteriorRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnSiguienteRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.btnUltimoRegistro = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ddlSerie)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtIdRegistroAActualizar)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.ComboBox4)
        Me.GroupBox1.Controls.Add(Me.ddlEstado)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.ddlAgencia)
        Me.GroupBox1.Controls.Add(Me.txtIdRegistro)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNumero)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(468, 138)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de los Cambios de N�meros"
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Location = New System.Drawing.Point(62, 32)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(43, 21)
        Me.ddlSerie.TabIndex = 13
        Me.ddlSerie.Tag = "Serie del Requisa"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 32)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Serie"
        '
        'txtIdRegistroAActualizar
        '
        Me.txtIdRegistroAActualizar.Location = New System.Drawing.Point(243, 67)
        Me.txtIdRegistroAActualizar.MaxLength = 7
        Me.txtIdRegistroAActualizar.Name = "txtIdRegistroAActualizar"
        Me.txtIdRegistroAActualizar.ReadOnly = True
        Me.txtIdRegistroAActualizar.Size = New System.Drawing.Size(88, 20)
        Me.txtIdRegistroAActualizar.TabIndex = 11
        Me.txtIdRegistroAActualizar.Tag = "Ultimo n�mero de Requisa procesada"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(161, 67)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "N�mero Reg"
        '
        'ComboBox4
        '
        Me.ComboBox4.Location = New System.Drawing.Point(440, 56)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(16, 21)
        Me.ComboBox4.TabIndex = 9
        Me.ComboBox4.Text = "ComboBox4"
        Me.ComboBox4.Visible = False
        '
        'ddlEstado
        '
        Me.ddlEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlEstado.Items.AddRange(New Object() {"activo", "procesado", "cancelado"})
        Me.ddlEstado.Location = New System.Drawing.Point(62, 100)
        Me.ddlEstado.Name = "ddlEstado"
        Me.ddlEstado.Size = New System.Drawing.Size(96, 21)
        Me.ddlEstado.TabIndex = 3
        Me.ddlEstado.Tag = "Estado del registro"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Estado"
        '
        'ddlAgencia
        '
        Me.ddlAgencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlAgencia.Location = New System.Drawing.Point(169, 32)
        Me.ddlAgencia.Name = "ddlAgencia"
        Me.ddlAgencia.Size = New System.Drawing.Size(232, 21)
        Me.ddlAgencia.TabIndex = 1
        Me.ddlAgencia.Tag = "Agencia a cambiar"
        '
        'txtIdRegistro
        '
        Me.txtIdRegistro.Location = New System.Drawing.Point(152, 32)
        Me.txtIdRegistro.Name = "txtIdRegistro"
        Me.txtIdRegistro.Size = New System.Drawing.Size(8, 20)
        Me.txtIdRegistro.TabIndex = 6
        Me.txtIdRegistro.TabStop = False
        Me.txtIdRegistro.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "N�mero"
        '
        'txtNumero
        '
        Me.txtNumero.Location = New System.Drawing.Point(62, 67)
        Me.txtNumero.MaxLength = 7
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(88, 20)
        Me.txtNumero.TabIndex = 2
        Me.txtNumero.Tag = "Ultimo n�mero de Requisa procesada"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(113, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Agencia"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem9})
        Me.MenuItem11.Text = "Listados"
        Me.MenuItem11.Visible = False
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'Timer1
        '
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 223)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(477, 23)
        Me.UltraStatusBar1.TabIndex = 22
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnListado, Me.LabelItem4, Me.btnRegistros, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(477, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 58
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnListado
        '
        Me.btnListado.AutoExpandOnClick = True
        Me.btnListado.Image = CType(resources.GetObject("btnListado.Image"), System.Drawing.Image)
        Me.btnListado.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnListado.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnListado.Name = "btnListado"
        Me.btnListado.OptionGroup = "Listado de productos"
        Me.btnListado.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnGeneral, Me.btnCambios, Me.btnReportes})
        Me.btnListado.Text = "Listados"
        Me.btnListado.Tooltip = "Listados"
        '
        'btnGeneral
        '
        Me.btnGeneral.Image = CType(resources.GetObject("btnGeneral.Image"), System.Drawing.Image)
        Me.btnGeneral.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnGeneral.Name = "btnGeneral"
        Me.btnGeneral.Text = "General"
        Me.btnGeneral.Tooltip = "General"
        '
        'btnCambios
        '
        Me.btnCambios.Image = CType(resources.GetObject("btnCambios.Image"), System.Drawing.Image)
        Me.btnCambios.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnCambios.Name = "btnCambios"
        Me.btnCambios.Tag = "Cambios"
        Me.btnCambios.Text = "Cambios"
        Me.btnCambios.Tooltip = "Cambios"
        '
        'btnReportes
        '
        Me.btnReportes.Image = CType(resources.GetObject("btnReportes.Image"), System.Drawing.Image)
        Me.btnReportes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Tag = "Reportes"
        Me.btnReportes.Text = "Reportes"
        Me.btnReportes.Tooltip = "Reportes"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'btnRegistros
        '
        Me.btnRegistros.AutoExpandOnClick = True
        Me.btnRegistros.Image = CType(resources.GetObject("btnRegistros.Image"), System.Drawing.Image)
        Me.btnRegistros.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnRegistros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.OptionGroup = "Recorre Registros"
        Me.btnRegistros.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPrimerRegistro, Me.btnAnteriorRegistro, Me.btnSiguienteRegistro, Me.btnUltimoRegistro})
        Me.btnRegistros.Text = "Recorre Registros"
        Me.btnRegistros.Tooltip = "Recorre Registros"
        '
        'btnPrimerRegistro
        '
        Me.btnPrimerRegistro.Image = CType(resources.GetObject("btnPrimerRegistro.Image"), System.Drawing.Image)
        Me.btnPrimerRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrimerRegistro.Name = "btnPrimerRegistro"
        Me.btnPrimerRegistro.Text = "Primer registro"
        Me.btnPrimerRegistro.Tooltip = "Primer registro"
        '
        'btnAnteriorRegistro
        '
        Me.btnAnteriorRegistro.Image = CType(resources.GetObject("btnAnteriorRegistro.Image"), System.Drawing.Image)
        Me.btnAnteriorRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnAnteriorRegistro.Name = "btnAnteriorRegistro"
        Me.btnAnteriorRegistro.Text = "Anterior registro"
        Me.btnAnteriorRegistro.Tooltip = "Anterior registro"
        '
        'btnSiguienteRegistro
        '
        Me.btnSiguienteRegistro.Image = CType(resources.GetObject("btnSiguienteRegistro.Image"), System.Drawing.Image)
        Me.btnSiguienteRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnSiguienteRegistro.Name = "btnSiguienteRegistro"
        Me.btnSiguienteRegistro.Tag = "Siguiente registro"
        Me.btnSiguienteRegistro.Text = "Siguiente registro"
        Me.btnSiguienteRegistro.Tooltip = "Siguiente registro"
        '
        'btnUltimoRegistro
        '
        Me.btnUltimoRegistro.Image = CType(resources.GetObject("btnUltimoRegistro.Image"), System.Drawing.Image)
        Me.btnUltimoRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnUltimoRegistro.Name = "btnUltimoRegistro"
        Me.btnUltimoRegistro.Tag = "Ultimo registro"
        Me.btnUltimoRegistro.Text = "Ultimo registro"
        Me.btnUltimoRegistro.Tooltip = "Ultimo registro"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmCambioNumeroMovimiento
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(477, 246)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmCambioNumeroMovimiento"
        Me.Text = "M�dulo para Cambiar N�mero de Requisa"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strCambios(1) As String
    Dim Serie As String = String.Empty
    Dim nIndicaError As Integer

    Private Sub frmCambioNumeroMovimiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        CreateMyContextMenu()
        Iniciar()
        Limpiar()
        intModulo = 24

    End Sub

    Private Sub frmCambioNumeroMovimiento_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            verificaNumeroRecibo()
            If nIndicaError = 1 Then
                MsgBox("Ya existe un numero de Requisa con este n�mero, Cambie el n�mero por otro que no exista.", MsgBoxStyle.Critical, "Error al ingresar")
            End If
            verificaNumeroRecibo()
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar"
                verificaNumeroRecibo()
                Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = txtIdRegistro.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = txtIdRegistro.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)


    End Sub

    Sub verificaNumeroRecibo()
        Dim cmdTmp As New SqlCommand("verificaNumeroMovimientoInventario", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim sNumeroRecibo As String = String.Empty

        If cnnAgro2K.State = ConnectionState.Closed Then
            cnnAgro2K.Open()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Closed Then
            cmdAgro2K.Connection.Open()
        End If

        sNumeroRecibo = Format(ConvierteAInt(txtIdRegistroAActualizar.Text), "0000000")
        If IsNumeric((Microsoft.VisualBasic.Left(sNumeroRecibo, 1))) = True Then
            sNumeroRecibo = ddlSerie.SelectedItem & sNumeroRecibo
        End If

        With prmTmp01
            .ParameterName = "@NumeroRecibo"
            .SqlDbType = SqlDbType.VarChar
            .Value = sNumeroRecibo
        End With

        nIndicaError = 0
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K.Close()
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            'nIndicaError = ConvierteAInt(dtrAgro2K.Item("IndicaError"))
            nIndicaError = ConvierteAInt(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        'cmdAgro2K.Connection.Close()
        'cnnAgro2K.Close()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If

    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("ManConsecutivoInventario", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter

        Dim strNumFact As String

        intResp = 0
        lngRegistro = 0
        If txtIdRegistro.Text <> "0" Then
            lngRegistro = CInt(txtIdRegistro.Text)
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            If intResp = 7 Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        End If
        ComboBox4.SelectedIndex = ddlAgencia.SelectedIndex
        'strNumFact = Format(CLng(strNumFact), "0000000")
        strNumFact = Format(CLng(txtNumero.Text), "0000000")
        With prmTmp01
            .ParameterName = "@cIdSerie"
            .SqlDbType = SqlDbType.Char
            .Value = ddlSerie.SelectedItem
        End With
        With prmTmp02
            .ParameterName = "@nagenregistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox4.SelectedItem
        End With
        With prmTmp03
            .ParameterName = "@nUltimoNumeroRecibo"
            .SqlDbType = SqlDbType.Int
            .Value = ConvierteAInt(txtNumero.Text.Trim())
        End With
        With prmTmp04
            .ParameterName = "@cIdUsuario"
            .SqlDbType = SqlDbType.VarChar
            .Value = strUsuario
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
            If intResp = 0 Then
                Ing_Bitacora(intModulo, lngRegistro, "Agencia", "", ddlAgencia.SelectedItem)
                Ing_Bitacora(intModulo, lngRegistro, "Serie", "", ddlSerie.SelectedItem)
                Ing_Bitacora(intModulo, lngRegistro, "N�mero", "", txtNumero.Text)
                Ing_Bitacora(intModulo, lngRegistro, "Estado", "", ddlEstado.SelectedItem)
            ElseIf intResp = 6 Then
                Ing_Bitacora(intModulo, lngRegistro, "Estado", strCambios(0), ddlEstado.SelectedItem)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Agencias"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ddlAgencia.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox4.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select IdSerie, Descripcion From catSerieFactura where Activo=1"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ddlSerie.Items.Add(dtrAgro2K.GetValue(1))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
    End Sub

    Sub Limpiar()

        txtNumero.Text = ""
        txtIdRegistro.Text = "0"
        ddlAgencia.SelectedIndex = -1
        ddlEstado.SelectedIndex = 0
        ComboBox4.SelectedIndex = -1
        ddlSerie.SelectedIndex = -1
        strCambios(0) = ""

    End Sub

    Sub MoverRegistro(ByVal StrDato As String)

        Select Case StrDato
            Case "Primero"
                'strQuery = "Select Min(Registro) from prmConsecutivoRecibo"
                strQuery = "exec RecorreConsecutivoInventario 1,0"
            Case "Anterior"
                'strQuery = "Select Max(Registro) from prmConsecutivoRecibo Where Registro < " & txtIdRegistro.Text & ""
                strQuery = "exec RecorreConsecutivoInventario 2, " & ConvierteAInt(txtIdRegistro.Text.Trim())
            Case "Siguiente"
                'strQuery = "Select Min(Registro) from prmConsecutivoRecibo Where Registro > " & txtIdRegistro.Text & ""
                strQuery = "exec RecorreConsecutivoInventario 3, " & ConvierteAInt(txtIdRegistro.Text.Trim())
            Case "Ultimo"
                'strQuery = "Select Max(Registro) from prmConsecutivoRecibo"
                strQuery = "exec RecorreConsecutivoInventario 4, " & ConvierteAInt(txtIdRegistro.Text.Trim())
            Case "Igual"
                'strQuery = "Select Registro from prmConsecutivoRecibo Where Registro = " & TextBox2.Text & ""
                strQuery = "exec RecorreConsecutivoInventario 5, " & ConvierteAInt(txtIdRegistro.Text.Trim())
        End Select
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim strCodigo As String
        Dim lngAgencia As Long

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        strCodigo = 0
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                strCodigo = dtrAgro2K.GetValue(0)
                txtIdRegistro.Text = dtrAgro2K.GetValue(0)
                'ComboBox1.SelectedIndex = dtrAgro2K.GetValue(2)
                lngAgencia = dtrAgro2K.GetValue(1)
                txtNumero.Text = dtrAgro2K.GetValue(4)
                txtIdRegistro.Text = dtrAgro2K.GetValue(0)
                ddlSerie.SelectedItem = dtrAgro2K.GetValue(5)
                ddlEstado.SelectedIndex = dtrAgro2K.GetValue(9)
                strCambios(0) = ddlEstado.SelectedItem
            End If
        End While
        dtrAgro2K.Close()
        If strCodigo = 0 Then
            MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        'strQuery = ""
        'strQuery = "Select * From tbl_CambiarNumFact Where Registro = " & strCodigo & ""
        'cmdAgro2K.CommandText = strQuery
        'dtrAgro2K = cmdAgro2K.ExecuteReader
        'While dtrAgro2K.Read
        '    ComboBox1.SelectedIndex = dtrAgro2K.GetValue(2)
        '    lngAgencia = dtrAgro2K.GetValue(1)
        '    TextBox1.Text = dtrAgro2K.GetValue(4)
        '    TextBox2.Text = dtrAgro2K.GetValue(0)
        '    ddlSerie.SelectedItem = dtrAgro2K.GetValue(5)
        '    ComboBox3.SelectedIndex = dtrAgro2K.GetValue(9)
        '    strCambios(0) = ComboBox3.SelectedItem
        'End While
        'ComboBox4.SelectedIndex = -1
        'For intIncr = 0 To ComboBox4.Items.Count - 1
        '    ComboBox4.SelectedIndex = intIncr
        '    If ComboBox4.SelectedItem = lngAgencia Then
        '        ddlAgencia.SelectedIndex = ComboBox4.SelectedIndex
        '        Exit For
        '    End If
        'Next intIncr
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"

        cntmnuCambNumFact.MenuItems.Add(cntmenuItem1)
        cntmnuCambNumFact.MenuItems.Add(cntmenuItem2)
        cntmnuCambNumFact.MenuItems.Add(cntmenuItem3)
        cntmnuCambNumFact.MenuItems.Add(cntmenuItem4)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Private Sub txtNumero_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumero.GotFocus, ddlAgencia.GotFocus, ddlEstado.GotFocus, ddlSerie.GotFocus

        UltraStatusBar1.Panels.Item(0).Text = IIf(sender.name = "ComboBox1", "Tipo", IIf(sender.name = "ddlAgencia", "Agencia", IIf(sender.name = "ddlSerie", "Serie", IIf(sender.name = "txtNumero", "N�mero", IIf(sender.name = "ddlEstado", "Estado", "Estado")))))
        UltraStatusBar1.Panels.Item(1).Text = sender.tag
        sender.selectall()

    End Sub

    Private Sub txtNumero_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumero.KeyDown, ddlAgencia.KeyDown, ddlEstado.KeyDown, ddlSerie.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "ddlAgencia" : ddlSerie.Focus()
                Case "ddlSerie" : txtNumero.Focus()
                Case "txtNumero" : ddlEstado.Focus()

            End Select
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                txtIdRegistro.Text = strUbicar
                MoverRegistro("Igual")
            End If
        End If

    End Sub

    Private Sub ddlAgencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAgencia.SelectedIndexChanged


    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged
        Serie = ddlSerie.SelectedItem
        If Serie = Nothing Then
            Serie = String.Empty
        End If
        ComboBox4.SelectedIndex = ddlAgencia.SelectedIndex
        lngRegAgencia = ComboBox4.SelectedItem
        'txtIdRegistroAActualizar.Text = ExtraerNumeroFactura(lngRegAgencia, intTipoFactura, Serie)
        txtIdRegistroAActualizar.Text = ExtraerParametroNumeroInventario(Serie)
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        'Dim frmNew As New frmGeneral
        'lngRegistro = txtIdRegistro.Text
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        'frmNew.ShowDialog()
    End Sub

    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Dim frmNew As New frmBitacora
        lngRegistro = txtIdRegistro.Text
        frmNew.ShowDialog()
    End Sub

    Private Sub LabelItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LabelItem4.Click

    End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Dim frmNew As New actrptViewer
        intRptFactura = 30
        strQuery = "exec sp_Reportes " & intModulo & " "
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
