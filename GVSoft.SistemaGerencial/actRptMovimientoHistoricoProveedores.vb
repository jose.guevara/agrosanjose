Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document 

Public Class actRptMovimientoHistoricoProveedores 
    Inherits DataDynamics.ActiveReports.ActiveReport
    
    Private Sub actRptMovimientoHistoricoProveedores_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        Label5.Text = strNombEmpresa
        TextBox15.Text = Format(Now, "Long Date")
        TextBox16.Text = Format(Now, "Long Time")

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actRptMovimientoHistoricoProveedores_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        
    End Sub

    Private Sub Detail1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail1.Format

        If TextBox5.Text = "1" Or TextBox5.Text = "2" Then
            TextBox3.Visible = True
            TextBox2.Visible = False
        ElseIf TextBox5.Text = "3" Or TextBox5.Text = "4" Then
            TextBox3.Visible = False
            TextBox2.Visible = True
        End If

        'saldAnt = TextBox3.Text
        If TextBox3.Visible = True Then
            txtTotalCredito.Text = Format(ConvierteADouble(txtTotalCredito.Text) + ConvierteADouble(TextBox3.Text), "###,###,###,###.00")
        ElseIf TextBox2.Visible = True Then
            txtTotalDebito.Text = Format(ConvierteADouble(txtTotalDebito.Text) + ConvierteADouble(TextBox2.Text), "###,###,###,###.00")
        End If


    End Sub

    Private Sub GroupHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format
        txtTotalCredito.Text = 0
        txtTotalDebito.Text = 0
    End Sub
End Class
