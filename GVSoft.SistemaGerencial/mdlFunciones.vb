Imports System.Data.SqlClient
Imports System.Text
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Utilidades

Module mdlFunciones

    Dim cMontext As String

    Sub Main()
        Try
            Dim frmNew As New frmInicial
            cmdAgro2K = New SqlCommand

            frmNew.Show()
            Application.Run()
        Catch ex As Exception
            MessageBoxEx.Show("Error de Aplicacion--> " & ex.Message)
            Application.Restart()
        End Try

    End Sub
    Public Function ConvierteADouble(ByVal oValor As Object) As Double
        Dim nValorNumerico As Double = 0
        Dim outresult As Double = 0

        Try
            If oValor IsNot Nothing Then
                Double.TryParse(oValor.ToString().Trim(), outresult)
                If outresult = 0 Then
                    nValorNumerico = 0
                Else
                    nValorNumerico = Convert.ToDouble(oValor.ToString().Trim())
                End If

            End If

        Catch ex As Exception
            nValorNumerico = 0
        End Try
        Return nValorNumerico
    End Function
    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ConvierteAInt(ByVal oValor As Object) As Int64
        Dim nValorNumerico As Integer = 0
        Dim outresult As Integer = 0

        Try
            If oValor IsNot Nothing Then
                Integer.TryParse(oValor.ToString().Trim(), outresult)
                If outresult = 0 Then
                    nValorNumerico = 0
                Else
                    nValorNumerico = CInt(oValor.ToString().Trim())
                End If
            End If

        Catch ex As Exception
            nValorNumerico = 0
        End Try
        Return nValorNumerico
    End Function
    Public Sub Ing_Bitacora(ByVal strPar01 As Integer, ByVal strPar02 As Long, ByVal strPar03 As String, ByVal strPar04 As String, ByVal strPar05 As String)

        Dim cmdTmp As New SqlCommand("sp_IngBitacora", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim strFecha As String, strTiempo As String
        Dim lngFecha As Long, lngTiempo As Long

        lngFecha = 0
        lngTiempo = 0
        lngFecha = Format(Now, "yyyyMMdd")
        lngTiempo = Format(Now, "Hmmss")
        strFecha = ""
        strTiempo = ""
        strFecha = Format(Now, "dd-MMM-yyyy")
        strTiempo = Format(Now, "h:mm:ss tt")
        With prmTmp01
            .ParameterName = "@strTabla"
            .SqlDbType = SqlDbType.TinyInt
            .Value = strPar01
        End With
        With prmTmp02
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.BigInt
            .Value = strPar02
        End With
        With prmTmp03
            .ParameterName = "@strFecha"
            .SqlDbType = SqlDbType.VarChar
            .Value = strFecha
        End With
        With prmTmp04
            .ParameterName = "@strTiempo"
            .SqlDbType = SqlDbType.VarChar
            .Value = strTiempo
        End With
        With prmTmp05
            .ParameterName = "@strCampo"
            .SqlDbType = SqlDbType.VarChar
            .Value = strPar03
        End With
        With prmTmp06
            .ParameterName = "@strAntes"
            .SqlDbType = SqlDbType.VarChar
            .Value = Left(strPar04, 65)
        End With
        With prmTmp07
            .ParameterName = "@strDespues"
            .SqlDbType = SqlDbType.VarChar
            .Value = Left(strPar05, 65)
        End With
        With prmTmp08
            .ParameterName = "@lngFecha"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngFecha
        End With
        With prmTmp09
            .ParameterName = "@lngTiempo"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngTiempo
        End With
        With prmTmp10
            .ParameterName = "@strUsuario"
            .SqlDbType = SqlDbType.VarChar
            .Value = strUsuario
        End With
        With prmTmp11
            .ParameterName = "@lngRegUsuario"
            .SqlDbType = SqlDbType.VarChar
            .Value = lngRegUsuario
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try

    End Sub

    Public Function UbicarTipoCambio(ByVal strDato As Long) As Double

        cmdAgro2K = New SqlCommand

        UbicarTipoCambio = 0
        strQuery = ""
        strQuery = "Select NumFecha, Tipo_Cambio From prm_TipoCambio Where NumFecha = " & strDato & ""
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            UbicarTipoCambio = dtrAgro2K.GetValue(1)
        End While

    End Function
    Sub UbicarAgencia(ByVal lngRegUsuario As Integer)
        cmdAgro2K = New SqlCommand

        strQuery = ""
        'strQuery = "Select isnull(count(*),0),isnull(max(agenregistro),0) from prm_UsuariosAgencias u  Where u.usrregistro = " & lngRegUsuario & ""
        strQuery = "select distinct a.registro, isnull(a.Direccion,' ') Direccion, isnull(a.Telefono,' ') Telefono, isnull(a.descripcion,' ') descripcion from prm_agencias a, prm_UsuariosAgencias ua, prm_usuarios u Where a.defecto = 0 and a.registro = ua.agenregistro and ua.usrregistro = " & lngRegUsuario
        SUFunciones.CierraConexionBD(cnnAgro2K)
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            'If CInt(dtrAgro2K.GetValue(0)) > 1 Then
            '    lngRegAgencia = 0
            'Else
            lngRegAgencia = dtrAgro2K.GetValue(0)
            sDireccionAgencia = dtrAgro2K.GetValue(1)
            sTelefonoAjencia = dtrAgro2K.GetValue(2)
            sNombreAgencia = dtrAgro2K.GetValue(3)
            'End If

        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Public Function ExtraerNumeroFactura(ByVal lngDato As Byte, ByVal intTipo As Integer, ByVal Serie As String) As String
        ExtraerNumeroFactura = String.Empty
        cmdAgro2K = New SqlCommand

        If intTipo = 1 Then
            ExtraerNumeroFactura = "0000001"
        ElseIf intTipo = 4 Then
            If intFechaCambioFactura = 0 Then
                ExtraerNumeroFactura = "0000001"
            Else
                ExtraerNumeroFactura = "0000001"
            End If
        End If
        SUFunciones.CierraConexionBD(cnnAgro2K)
        strQuery = String.Empty
        strQuery = " exec ObtieneParametrosNumeroFactura " & lngDato & ",'" & Serie.Trim() & "'," & intTipo
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If intTipo = 1 Then
                ExtraerNumeroFactura = Format(dtrAgro2K.GetValue(3) + 1, "0000000")
            ElseIf intTipo = 4 Then
                If intFechaCambioFactura = 0 Then
                    ExtraerNumeroFactura = Format(dtrAgro2K.GetValue(3) + 1, "0000000")
                Else
                    ExtraerNumeroFactura = Format(dtrAgro2K.GetValue(3) + 1, "0000000")
                End If
            End If
        End While
        Return ExtraerNumeroFactura
    End Function
    Public Function ExtraerNumeroFacturaRapida(ByVal lngDato As Byte, ByVal intTipo As Integer, ByVal Serie As String) As String
        ExtraerNumeroFacturaRapida = String.Empty
        cmdAgro2K = New SqlCommand

        If intTipo = 1 Then
            ExtraerNumeroFacturaRapida = "0000001"
        ElseIf intTipo = 4 Then
            If intFechaCambioFactura = 0 Then
                ExtraerNumeroFacturaRapida = "0000001"
            Else
                ExtraerNumeroFacturaRapida = "0000001"
            End If
        End If
        strQuery = ""
        strQuery = " exec ObtieneParametrosNumeroFacturaRapida " & lngDato & ",'" & Serie.Trim() & "'," & intTipo
        SUFunciones.CierraConexionBD(cnnAgro2K)
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If intTipo = 1 Then
                ExtraerNumeroFacturaRapida = Format(dtrAgro2K.GetValue(3) + 1, "0000000")
            ElseIf intTipo = 4 Then
                If intFechaCambioFactura = 0 Then
                    ExtraerNumeroFacturaRapida = Format(dtrAgro2K.GetValue(3) + 1, "0000000")
                Else
                    ExtraerNumeroFacturaRapida = Format(dtrAgro2K.GetValue(3) + 1, "0000000")
                End If
            End If
        End While
        Return ExtraerNumeroFacturaRapida
    End Function
    Public Function ExtraerNumeroInventario(ByVal lngDato As Byte, ByVal Serie As String) As String
        ExtraerNumeroInventario = String.Empty
        cmdAgro2K = New SqlCommand

        ExtraerNumeroInventario = "0000001"
        strQuery = ""
        strQuery = " exec ObtieneParametrosNumeroInventario " & lngDato & ",'" & Serie.Trim() & "'"
        SUFunciones.CierraConexionBD(cnnAgro2K)
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ExtraerNumeroInventario = Format(dtrAgro2K.GetValue(3), "0000000")
        End While
        Return ExtraerNumeroInventario
    End Function
    Public Function ExtraerParametroNumeroRecibo(ByVal Serie As String) As String
        ExtraerParametroNumeroRecibo = String.Empty
        cmdAgro2K = New SqlCommand

        ExtraerParametroNumeroRecibo = "0000001"
        strQuery = ""
        strQuery = " exec ObtieneConsecutivoRecibo " & "'" & Serie.Trim() & "'"
        SUFunciones.CierraConexionBD(cnnAgro2K)
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ExtraerParametroNumeroRecibo = Format(dtrAgro2K.Item("UltimoNumeroRecibo") + 1, "0000000")
        End While
        Return ExtraerParametroNumeroRecibo
    End Function
    Public Function ExtraerParametroNumeroInventario(ByVal Serie As String) As String
        ExtraerParametroNumeroInventario = String.Empty
        cmdAgro2K = New SqlCommand

        ExtraerParametroNumeroInventario = "0000001"
        strQuery = ""
        strQuery = " exec ObtieneConsecutivoInventario " & "'" & Serie.Trim() & "'"
        SUFunciones.CierraConexionBD(cnnAgro2K)
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ExtraerParametroNumeroInventario = Format(dtrAgro2K.Item("UltimoConsecutivo") + 1, "0000000")
        End While
        Return ExtraerParametroNumeroInventario
    End Function
    Public Function ExtraerSerieNumeroFactura(ByVal lngDato As Byte, ByVal intTipo As Integer) As String
        ExtraerSerieNumeroFactura = String.Empty
        cmdAgro2K = New SqlCommand

        strQuery = ""
        strQuery = "Select * from tbl_UltimoNumero Where AgenRegistro = " & lngDato & ""
        SUFunciones.CierraConexionBD(cnnAgro2K)
        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ExtraerSerieNumeroFactura = dtrAgro2K.GetValue(1)
        End While
        Return ExtraerSerieNumeroFactura
    End Function
    Public Function ExtraerSerieNumeroRecibo(ByVal sSerie As String) As String
        ExtraerSerieNumeroRecibo = String.Empty
        cmdAgro2K = New SqlCommand

        strQuery = ""
        strQuery = "ObtieneConsecutivoRecibo " & sSerie & ""
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ExtraerSerieNumeroRecibo = dtrAgro2K.GetValue(3)
        End While
        Return ExtraerSerieNumeroRecibo
    End Function

    Public Function ExtraerSerieNumeroReciboProveedor(ByVal sSerie As String) As String
        ExtraerSerieNumeroReciboProveedor = String.Empty
        cmdAgro2K = New SqlCommand

        strQuery = ""
        strQuery = "ObtieneConsecutivoReciboProveedor " & sSerie & ""
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ExtraerSerieNumeroReciboProveedor = dtrAgro2K.GetValue(3)
        End While
        Return ExtraerSerieNumeroReciboProveedor
    End Function

    Public Function ExtraerNumeroCompra(ByVal lngDato As Byte, ByVal intTipo As Integer) As String
        ExtraerNumeroCompra = String.Empty
        cmdAgro2K = New SqlCommand

        If intTipo = 1 Then
            ExtraerNumeroCompra = "0000001C"
        Else
            If intFechaCambioCompra = 0 Then
                ExtraerNumeroCompra = "0000001"
            Else
                ExtraerNumeroCompra = "0000001A"
            End If
        End If
        strQuery = ""
        strQuery = "exec ObtieneUltimoNumeroCompra " & lngDato & "," & intTipo
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If intTipo = 1 Then 'Compra al contado
                ExtraerNumeroCompra = Format(dtrAgro2K.GetValue(0) + 1, "0000000")
            Else
                If intFechaCambioCompra = 0 Then
                    ExtraerNumeroCompra = Format(dtrAgro2K.GetValue(1) + 1, "0000000C")
                Else
                    ExtraerNumeroCompra = Format(dtrAgro2K.GetValue(1) + 1, "0000000A")
                End If
            End If
        End While
        Return ExtraerNumeroCompra
    End Function
    Public Sub ActualizarNumeroCompra(ByVal intDato1 As Byte, ByVal intDato2 As Byte)

        Dim cmdTmp As New SqlCommand("ActualizaNumeroCompra", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter

        With prmTmp01
            .ParameterName = "@AgenciaRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato1
        End With
        With prmTmp02
            .ParameterName = "@IdTipoCompra"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato2
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try

    End Sub
    Public Sub ActualizarNumeroInventario(ByVal intDato1 As Byte, ByVal Serie As String)

        Dim cmdTmp As New SqlCommand("ActualizaNumeroInventario", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter

        With prmTmp01
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato1
        End With
        With prmTmp02
            .ParameterName = "@IdSerie"
            .SqlDbType = SqlDbType.Char
            .Value = Serie
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try

    End Sub
    Public Sub ActualizarNumeroFactura(ByVal intDato1 As Byte, ByVal intDato2 As Byte, ByVal Serie As String)

        Dim cmdTmp As New SqlCommand("sp_NumFactura", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter

        With prmTmp01
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato1
        End With
        With prmTmp02
            .ParameterName = "@intTipo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato2
        End With
        With prmTmp03
            .ParameterName = "@IdSerie"
            .SqlDbType = SqlDbType.Char
            .Value = Serie
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try

    End Sub

    Public Sub ActualizarNumeroFacturaRapida(ByVal intDato1 As Byte, ByVal intDato2 As Byte, ByVal Serie As String)

        Dim cmdTmp As New SqlCommand("sp_NumFacturaRapida", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter

        With prmTmp01
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato1
        End With
        With prmTmp02
            .ParameterName = "@intTipo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intDato2
        End With
        With prmTmp03
            .ParameterName = "@IdSerie"
            .SqlDbType = SqlDbType.Char
            .Value = Serie
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try

    End Sub
    Public Function DefinirFecha(ByVal strDato As String) As Date

        Dim intDay As Integer
        Dim intMonth As Integer
        Dim lngYear As Long

        intDay = Right(strDato, 2)
        intMonth = Mid(strDato, 5, 2)
        lngYear = Left(strDato, 4)
        DefinirFecha = DateSerial(lngYear, intMonth, intDay)

    End Function

    Public Function NumPalabra(ByVal Numero As Double) As String

        Dim Numerofmt As String
        Dim centenas As Integer
        Dim pos As Integer
        Dim cen As Integer
        Dim dec As Integer
        Dim uni As Integer
        Dim textuni As String = String.Empty
        Dim textdec As String = String.Empty
        Dim textcen As String = String.Empty
        Dim milestxt As String = String.Empty
        Dim monedatxt As String = String.Empty
        Dim txtPalabra As String = String.Empty

        Numerofmt = Format(Numero, "000000000000000.00") 'Le da un formato fijo
        centenas = 1
        pos = 1
        txtPalabra = ""
        Do While centenas <= 5
            ' extrae series de Centena, Decena, Unidad
            cen = Val(Mid(Numerofmt, pos, 1))
            dec = Val(Mid(Numerofmt, pos + 1, 1))
            uni = Val(Mid(Numerofmt, pos + 2, 1))
            pos = pos + 3
            textcen = Centena(uni, dec, cen)
            textdec = Decena(uni, dec)
            textuni = Unidad(uni, dec)
            ' determina separador de miles/millones
            Select Case centenas
                Case 1
                    If cen + dec + uni = 1 Then
                        milestxt = "Billon "
                    ElseIf cen + dec + uni > 1 Then
                        milestxt = "Billones "
                    End If
                Case 2
                    If cen + dec + uni >= 1 And Val(Mid(Numerofmt, 7, 3)) = 0 Then
                        milestxt = "Mil Millones "
                    ElseIf cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 3
                    If cen + dec = 0 And uni = 1 Then
                        milestxt = "Millon "
                    ElseIf cen > 0 Or dec > 0 Or uni > 1 Then
                        milestxt = "Millones "
                    End If
                Case 4
                    If cen + dec + uni >= 1 Then
                        milestxt = "Mil "
                    End If
                Case 5
                    If cen + dec + uni >= 1 Then
                        milestxt = ""
                    End If
            End Select
            centenas = centenas + 1
            'va formando el texto del importe en palabras
            txtPalabra = txtPalabra + textcen + textdec + textuni + milestxt
            milestxt = ""
            textuni = ""
            textdec = ""
            textcen = ""
        Loop
        ' agrega denominacion de moneda
        If intFechaCambioFactura = 0 Or (lngFechaFactura < intFechaCambioFactura) Then
            Select Case Val(Numerofmt)
                Case 0
                    monedatxt = "Cero C�rdobas "
                Case 1
                    monedatxt = "C�rdoba "
                Case Is < 1000000
                    monedatxt = "C�rdobas "
                Case Is >= 1000000
                    monedatxt = "de C�rdobas "
            End Select
        ElseIf intFechaCambioFactura <> 0 Then
            Select Case Val(Numerofmt)
                Case 0
                    monedatxt = "Cero D�lares Americanos "
                Case 1
                    monedatxt = "D�lar Americano "
                Case Is < 1000000
                    monedatxt = "D�lares Americanos "
                Case Is >= 1000000
                    monedatxt = "de D�lares Americanos "
            End Select
        End If
        txtPalabra = txtPalabra & monedatxt & "con " & Mid(Numerofmt, 17) & "/100"
        Return txtPalabra.ToUpper

    End Function
    'Funci�n para quitar los saltos de l�nea de un texto 
    Public Function quitarSaltosLinea(ByVal texto As String) As String
        quitarSaltosLinea = Replace(Replace(texto, Chr(10), ""), Chr(13), "")
        quitarSaltosLinea = Replace(quitarSaltosLinea, "|", "")
    End Function
    Private Function Centena(ByVal uni As Integer, ByVal dec As Integer, ByVal cen As Integer) As String
        Select Case cen
            Case 1
                If dec + uni = 0 Then
                    cMontext = "cien "
                Else
                    cMontext = "ciento "
                End If
            Case 2 : cMontext = "doscientos "
            Case 3 : cMontext = "trescientos "
            Case 4 : cMontext = "cuatrocientos "
            Case 5 : cMontext = "quinientos "
            Case 6 : cMontext = "seiscientos "
            Case 7 : cMontext = "setecientos "
            Case 8 : cMontext = "ochocientos "
            Case 9 : cMontext = "novecientos "
            Case Else : cMontext = ""
        End Select
        Centena = cMontext
        cMontext = ""

    End Function

    Private Function Decena(ByVal uni As Integer, ByVal dec As Integer) As String

        Select Case dec
            Case 1
                Select Case uni
                    Case 0 : cMontext = "diez "
                    Case 1 : cMontext = "once "
                    Case 2 : cMontext = "doce "
                    Case 3 : cMontext = "trece "
                    Case 4 : cMontext = "catorce "
                    Case 5 : cMontext = "quince "
                    Case 6 To 9 : cMontext = "dieci"
                End Select
            Case 2
                If uni = 0 Then
                    cMontext = "veinte "
                ElseIf uni > 0 Then
                    cMontext = "veinti"
                End If
            Case 3 : cMontext = "treinta "
            Case 4 : cMontext = "cuarenta "
            Case 5 : cMontext = "cincuenta "
            Case 6 : cMontext = "sesenta "
            Case 7 : cMontext = "setenta "
            Case 8 : cMontext = "ochenta "
            Case 9 : cMontext = "noventa "
            Case Else : cMontext = ""
        End Select
        If uni > 0 And dec > 2 Then cMontext = cMontext + "y "
        Decena = cMontext
        cMontext = ""

    End Function

    Private Function Unidad(ByVal uni As Integer, ByVal dec As Integer) As String

        If dec <> 1 Then
            Select Case uni
                Case 1 : cMontext = "un "
                Case 2 : cMontext = "dos "
                Case 3 : cMontext = "tres "
                Case 4 : cMontext = "cuatro "
                Case 5 : cMontext = "cinco "
            End Select
        End If
        Select Case uni
            Case 6 : cMontext = "seis "
            Case 7 : cMontext = "siete "
            Case 8 : cMontext = "ocho "
            Case 9 : cMontext = "nueve "
        End Select
        Unidad = cMontext
        cMontext = ""

    End Function

End Module
