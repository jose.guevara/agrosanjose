Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmFactCorr
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents cmdVendedor As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdClientes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProductos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdRefrescar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem3 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem6 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem8 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem4 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ControlContainerItem5 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents cbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFactCorr))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.ddlSerie = New System.Windows.Forms.ComboBox
        Me.lblSerie = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.MSFlexGrid2 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem
        Me.cmdVendedor = New DevComponents.DotNetBar.ButtonItem
        Me.cmdClientes = New DevComponents.DotNetBar.ButtonItem
        Me.cmdProductos = New DevComponents.DotNetBar.ButtonItem
        Me.cmdRefrescar = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem3 = New DevComponents.DotNetBar.ControlContainerItem
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem6 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem8 = New DevComponents.DotNetBar.ButtonItem
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem4 = New DevComponents.DotNetBar.ControlContainerItem
        Me.ControlContainerItem5 = New DevComponents.DotNetBar.ControlContainerItem
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cbAgencias)
        Me.GroupBox1.Controls.Add(Me.ddlSerie)
        Me.GroupBox1.Controls.Add(Me.lblSerie)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(524, 91)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de la Agencia y Factura"
        '
        'cbAgencias
        '
        Me.cbAgencias.DisplayMember = "Text"
        Me.cbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbAgencias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAgencias.FormattingEnabled = True
        Me.cbAgencias.ItemHeight = 15
        Me.cbAgencias.Location = New System.Drawing.Point(56, 24)
        Me.cbAgencias.Name = "cbAgencias"
        Me.cbAgencias.Size = New System.Drawing.Size(225, 21)
        Me.cbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.cbAgencias.TabIndex = 30
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Location = New System.Drawing.Point(453, 26)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(48, 21)
        Me.ddlSerie.TabIndex = 28
        Me.ddlSerie.Tag = "Serie por agencia"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(417, 29)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(36, 13)
        Me.lblSerie.TabIndex = 29
        Me.lblSerie.Text = "Serie"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(163, 55)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(8, 20)
        Me.TextBox3.TabIndex = 8
        Me.TextBox3.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(193, 55)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(12, 20)
        Me.TextBox2.TabIndex = 7
        Me.TextBox2.Visible = False
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Location = New System.Drawing.Point(200, 24)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox3.TabIndex = 6
        Me.ComboBox3.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(57, 55)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(80, 20)
        Me.TextBox1.TabIndex = 5
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Items.AddRange(New Object() {"contado", "cr�dito"})
        Me.ComboBox2.Location = New System.Drawing.Point(319, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(88, 21)
        Me.ComboBox2.TabIndex = 4
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(56, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(144, 21)
        Me.ComboBox1.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "N�mero"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(287, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tipo"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Agencia"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.MSFlexGrid2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 175)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(608, 202)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de los Productos Vendidos"
        '
        'MSFlexGrid2
        '
        Me.MSFlexGrid2.Location = New System.Drawing.Point(8, 24)
        Me.MSFlexGrid2.Name = "MSFlexGrid2"
        Me.MSFlexGrid2.OcxState = CType(resources.GetObject("MSFlexGrid2.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid2.Size = New System.Drawing.Size(592, 172)
        Me.MSFlexGrid2.TabIndex = 53
        Me.MSFlexGrid2.TabStop = False
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'cmdVendedor
        '
        Me.cmdVendedor.GlobalItem = False
        Me.cmdVendedor.Image = CType(resources.GetObject("cmdVendedor.Image"), System.Drawing.Image)
        Me.cmdVendedor.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdVendedor.Name = "cmdVendedor"
        Me.cmdVendedor.Text = "Vendedores"
        '
        'cmdClientes
        '
        Me.cmdClientes.GlobalItem = False
        Me.cmdClientes.Image = CType(resources.GetObject("cmdClientes.Image"), System.Drawing.Image)
        Me.cmdClientes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdClientes.Name = "cmdClientes"
        Me.cmdClientes.Text = "Clientes"
        '
        'cmdProductos
        '
        Me.cmdProductos.GlobalItem = False
        Me.cmdProductos.Image = CType(resources.GetObject("cmdProductos.Image"), System.Drawing.Image)
        Me.cmdProductos.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdProductos.Name = "cmdProductos"
        Me.cmdProductos.Text = "Productos"
        '
        'cmdRefrescar
        '
        Me.cmdRefrescar.GlobalItem = False
        Me.cmdRefrescar.Image = CType(resources.GetObject("cmdRefrescar.Image"), System.Drawing.Image)
        Me.cmdRefrescar.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdRefrescar.Name = "cmdRefrescar"
        Me.cmdRefrescar.Text = "Refrescar"
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.GlobalItem = False
        Me.ButtonItem1.Image = CType(resources.GetObject("ButtonItem1.Image"), System.Drawing.Image)
        Me.ButtonItem1.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Text = "Vendedores"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.GlobalItem = False
        Me.ButtonItem2.Image = CType(resources.GetObject("ButtonItem2.Image"), System.Drawing.Image)
        Me.ButtonItem2.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Text = "Clientes"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.GlobalItem = False
        Me.ButtonItem3.Image = CType(resources.GetObject("ButtonItem3.Image"), System.Drawing.Image)
        Me.ButtonItem3.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.Text = "Productos"
        '
        'ButtonItem4
        '
        Me.ButtonItem4.GlobalItem = False
        Me.ButtonItem4.Image = CType(resources.GetObject("ButtonItem4.Image"), System.Drawing.Image)
        Me.ButtonItem4.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.Text = "Refrescar"
        '
        'ControlContainerItem3
        '
        Me.ControlContainerItem3.AllowItemResize = False
        Me.ControlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem3.Name = "ControlContainerItem3"
        '
        'ButtonItem5
        '
        Me.ButtonItem5.GlobalItem = False
        Me.ButtonItem5.Image = CType(resources.GetObject("ButtonItem5.Image"), System.Drawing.Image)
        Me.ButtonItem5.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.Text = "Vendedores"
        '
        'ButtonItem6
        '
        Me.ButtonItem6.GlobalItem = False
        Me.ButtonItem6.Image = CType(resources.GetObject("ButtonItem6.Image"), System.Drawing.Image)
        Me.ButtonItem6.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem6.Name = "ButtonItem6"
        Me.ButtonItem6.Text = "Clientes"
        '
        'ButtonItem7
        '
        Me.ButtonItem7.GlobalItem = False
        Me.ButtonItem7.Image = CType(resources.GetObject("ButtonItem7.Image"), System.Drawing.Image)
        Me.ButtonItem7.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.Text = "Productos"
        '
        'ButtonItem8
        '
        Me.ButtonItem8.GlobalItem = False
        Me.ButtonItem8.Image = CType(resources.GetObject("ButtonItem8.Image"), System.Drawing.Image)
        Me.ButtonItem8.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.ButtonItem8.Name = "ButtonItem8"
        Me.ButtonItem8.Text = "Refrescar"
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(614, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 34
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'ControlContainerItem4
        '
        Me.ControlContainerItem4.AllowItemResize = False
        Me.ControlContainerItem4.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem4.Name = "ControlContainerItem4"
        '
        'ControlContainerItem5
        '
        Me.ControlContainerItem5.AllowItemResize = False
        Me.ControlContainerItem5.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem5.Name = "ControlContainerItem5"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 383)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 630
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(614, 23)
        Me.UltraStatusBar1.TabIndex = 35
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'frmFactCorr
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(614, 406)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmFactCorr"
        Me.Text = "frmFactCorr"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmFactCorr_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Top = 0
        'Me.Left = 0
        Iniciar()
        Limpiar()
        MSFlexGrid2.FormatString = "<C�digo     |<Producto                                          |>Cantidad  |>Precio     |>Monto     |<C�digo     |||"
        intModulo = 18

        RNAgencias.CargarComboAgencias(cbAgencias, lngRegUsuario)

    End Sub

    Private Sub frmFactCorr_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem12.Click

        Select Case sender.text.ToString()
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Guardar()
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        Me.Close()
        'End Select

    End Sub

    Sub Limpiar()

        ComboBox1.SelectedIndex = -1
        ComboBox2.SelectedIndex = -1
        ComboBox3.SelectedIndex = -1
        TextBox1.Text = ""
        MSFlexGrid2.Clear()
        MSFlexGrid2.Rows = 1
        MSFlexGrid2.FormatString = "<C�digo     |<Producto                                          |>Cantidad  |>Precio     |>Monto     |<C�digo     |||"
        ComboBox1.Focus()

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Agencias"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader

        While dtrAgro2K.Read
            ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox3.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Guardar()

        Dim lngNumTiempo, lngNumFecha As Long
        Dim strFechaIng As String, dblCantidad As Double
        Dim lngProdReg_New, lngProdReg_Old As Long
        Dim cmdTmp As New SqlCommand("sp_IngFacturasCorr", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter

        Me.Cursor = Cursors.WaitCursor
        lngNumFecha = 0
        lngRegistro = 0
        lngNumTiempo = 0
        strFechaIng = ""
        lngNumTiempo = Format(Now, "Hmmss")
        strFechaIng = Format(Now, "dd-MMM-yyyy")
        lngNumFecha = Format(Now, "yyyyMMdd")
        ComboBox3.SelectedIndex = ComboBox1.SelectedIndex
        lngRegAgencia = ComboBox3.SelectedItem
        With prmTmp01
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegAgencia
        End With
        With prmTmp02
            .ParameterName = "@lngUsuario"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegUsuario
        End With
        With prmTmp03
            .ParameterName = "@lngFechaIng"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumFecha
        End With
        With prmTmp04
            .ParameterName = "@strFechaIng"
            .SqlDbType = SqlDbType.VarChar
            .Value = strFechaIng
        End With
        With prmTmp05
            .ParameterName = "@intTipoFactura"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox2.SelectedIndex
        End With
        With prmTmp06
            .ParameterName = "@strFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp07
            .ParameterName = "@lngProdRegistro_Old"
            .SqlDbType = SqlDbType.SmallInt
            .Value = 0
        End With
        With prmTmp08
            .ParameterName = "@lngProdRegistro_New"
            .SqlDbType = SqlDbType.SmallInt
            .Value = 0
        End With
        With prmTmp09
            .ParameterName = "@lngFechaFactura"
            .SqlDbType = SqlDbType.Int
            .Value = CInt(TextBox3.Text)
        End With
        With prmTmp10
            .ParameterName = "@dblCantidad"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp11
            .ParameterName = "@lngNumTiempo"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumTiempo
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        MSFlexGrid2.Col = 1
        For intIncr = 1 To MSFlexGrid2.Rows - 1
            MSFlexGrid2.Row = intIncr
            dblCantidad = 0
            lngProdReg_Old = 0
            lngProdReg_New = 0
            MSFlexGrid2.Col = 6
            lngProdReg_Old = CInt(MSFlexGrid2.Text)
            MSFlexGrid2.Col = 7
            lngProdReg_New = CInt(MSFlexGrid2.Text)
            If lngProdReg_Old <> lngProdReg_New Then
                cmdTmp.Parameters(6).Value = lngProdReg_Old
                cmdTmp.Parameters(7).Value = lngProdReg_New
                MSFlexGrid2.Col = 2
                cmdTmp.Parameters(9).Value = CDbl(MSFlexGrid2.Text)
                Try
                    cmdTmp.ExecuteNonQuery()
                Catch exc As Exception
                    MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                    cmdTmp.Connection.Close()
                    cnnAgro2K.Close()
                    Exit Sub
                End Try
            End If
        Next intIncr
        cmdTmp.Connection.Close()
        cnnAgro2K.Close()
        MsgBox("Registro Procesado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
        Limpiar()
        Me.Cursor = Cursors.Default

    End Sub

    Sub ExtraerFactura()

        Me.Cursor = Cursors.WaitCursor
        Dim lngFecha As Long
        Dim cmdTmp As New SqlCommand("sp_ExtraerFactura", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter

        'If IsNumeric(TextBox1.Text) = False Then
        '    MsgBox("Tiene que ingresar un n�mero de factura a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
        '    Me.Cursor = Cursors.Default
        '    Exit Sub
        'End If
        If Trim(TextBox1.Text) = "" Then
            MsgBox("Tiene que ingresar un n�mero de factura a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        'Mejorar para obtener la factura a corregir por agencia y con la serie que le corresponde como en consulta de facturas
        If ComboBox2.SelectedItem = "contado" Then
            'TextBox1.Text = Format(CLng(TextBox1.Text), "0000000")
            intTipoFactura = 11
        ElseIf ComboBox2.SelectedItem = "cr�dito" Then
            'If Mid(TextBox1.Text, Len(TextBox1.Text), 1) = "C" Then
            '    'TextBox1.Text = Mid(TextBox1.Text, 1, Len(TextBox1.Text) - 1)
            '    TextBox1.Text = Mid(TextBox1.Text, 1, Len(TextBox1.Text) - 1)
            '    'TextBox1.Text = "('" & Format(CLng(TextBox1.Text), "0000000") & "C','" & Format(CLng(TextBox1.Text), "0000000") & "A')"
            'End If
            'TextBox1.Text = Format(CLng(TextBox1.Text), "0000000")
            'TextBox1.Text = TextBox1.Text & "C"
            intTipoFactura = 12
        End If

        ComboBox3.SelectedIndex = ComboBox1.SelectedIndex
        lngRegAgencia = ComboBox3.SelectedItem
        With prmTmp01
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegAgencia
        End With
        With prmTmp02
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = Trim(TextBox1.Text)
        End With
        With prmTmp03
            .ParameterName = "@intTipoFactura"
            .SqlDbType = SqlDbType.Int
            .Value = intTipoFactura
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        dtrAgro2K = cmdTmp.ExecuteReader
        Limpiar()
        lngFecha = 0
        While dtrAgro2K.Read
            TextBox1.Text = dtrAgro2K.GetValue(7)
            MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
            MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
            MSFlexGrid2.Col = 0
            MSFlexGrid2.Text = dtrAgro2K.GetValue(0)
            MSFlexGrid2.Col = 1
            MSFlexGrid2.Text = dtrAgro2K.GetValue(1)
            MSFlexGrid2.Col = 2
            MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(2), "#,##0.#0")
            MSFlexGrid2.Col = 3
            MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(3), "#,##0.#0")
            MSFlexGrid2.Col = 4
            MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(4), "#,##0.#0")
            MSFlexGrid2.Col = 5
            MSFlexGrid2.Text = dtrAgro2K.GetValue(0)
            MSFlexGrid2.Col = 6
            MSFlexGrid2.Text = dtrAgro2K.GetValue(5)
            MSFlexGrid2.Col = 7
            MSFlexGrid2.Text = dtrAgro2K.GetValue(5)
            MSFlexGrid2.Col = 8
            MSFlexGrid2.Text = dtrAgro2K.GetValue(6)
            TextBox1.Text = dtrAgro2K.GetValue(7)
            TextBox3.Text = dtrAgro2K.GetValue(8)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown, ComboBox2.KeyDown, TextBox1.KeyDown

        Dim intAgencia As Integer, intTipoFactura As Integer

        intAgencia = 0
        intTipoFactura = 0
        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString()
                Case "ComboBox1" : ComboBox2.Focus()
                Case "ComboBox2" : TextBox1.Focus() : TextBox1.SelectAll()
                Case "TextBox1"
                    ComboBox3.SelectedIndex = ComboBox1.SelectedIndex
                    intAgencia = ComboBox3.SelectedIndex
                    intTipoFactura = ComboBox2.SelectedIndex
                    ExtraerFactura()
                    If TextBox1.Text = "" Then
                        MsgBox("El N�mero Ingresado no Pertenece a una Factura Emitida o la Factura est� Anulada.", MsgBoxStyle.Information, "Factura No Emitida o Anulada")
                        ComboBox1.Focus()
                        Exit Sub
                    Else
                        ComboBox1.SelectedIndex = intAgencia
                        ComboBox2.SelectedIndex = intTipoFactura
                    End If
            End Select
        End If

    End Sub

    Private Sub MSFlexGrid2_DblClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles MSFlexGrid2.DblClick

        Dim strCodigo As String, intFila As Integer

        If MSFlexGrid2.Col = 5 Then
            intFila = MSFlexGrid2.Row
            strCodigo = ""
            strCodigo = InputBox("Digite el C�digo de Producto a Facturar", "Sustituci�n de C�digo de Producto")
            If Trim(strCodigo) = "" Then
                Exit Sub
            End If
            If UbicarProducto(strCodigo, intFila) = False Then
                MsgBox("C�digo Inv�lido", MsgBoxStyle.Critical, "Error de Dato")
            End If
        End If

    End Sub

    Function UbicarProducto(ByVal prm01 As String, ByVal prm02 As Integer) As Boolean

        Dim intEnc As Integer

        intEnc = 0
        strQuery = ""
        strQuery = "Select P.Codigo, P.Registro from prm_Productos P Where P.Codigo = '" & prm01 & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            MSFlexGrid2.Row = prm02
            MSFlexGrid2.Col = 5
            MSFlexGrid2.Text = dtrAgro2K.GetValue(0)
            MSFlexGrid2.Col = 7
            MSFlexGrid2.Text = dtrAgro2K.GetValue(1)
            intEnc = 1
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If intEnc = 0 Then
            UbicarProducto = False
            Exit Function
        End If
        UbicarProducto = True

    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

End Class
