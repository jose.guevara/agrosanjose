Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmCuadreCaja
    Private sSentenciaSql As String = String.Empty
    Private objError As SEError = New SEError()
    Private sNombreMetodo As String = String.Empty
    Private sNombreClase As String = "frmCuadreCaja"

    Private Sub ButtonItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonItem1.Click
        Me.Close()
    End Sub

    Private Sub frmCuadreCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dtCuadreCaja As DataTable = Nothing
        Try
            dtCuadreCaja = RNArqueoCaja.ObtenerUltimoArqueoxUsuario(lngRegUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Cuadre de caja")
        End Try
    End Sub
End Class