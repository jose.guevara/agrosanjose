Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmImportarMovimientoTraslado
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRetencionFactContado As System.Windows.Forms.TextBox
    Friend WithEvents txtImpuestoFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTotalFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotalFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtImpuesto As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtRentencion As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImportar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Valor As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents IVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label18 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportarMovimientoTraslado))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtRentencion = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtImpuesto = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtSubtotal = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtSubTotalFactContado = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCantidadFactContado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTotalFactContado = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtRetencionFactContado = New System.Windows.Forms.TextBox()
        Me.txtImpuestoFactContado = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImportar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 73)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(755, 72)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Requisas en Carga de la Agencia"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(56, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(0, 13)
        Me.Label18.TabIndex = 5
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(136, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox2.TabIndex = 4
        Me.ComboBox2.Text = "ComboBox2"
        Me.ComboBox2.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(197, 24)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(544, 40)
        Me.TextBox1.TabIndex = 3
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(56, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(138, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Requisa"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Controls.Add(Me.txtTotal)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.txtRentencion)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtImpuesto)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtSubtotal)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 150)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(755, 80)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Encabezado de la Requisa"
        '
        'CheckBox1
        '
        Me.CheckBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CheckBox1.Location = New System.Drawing.Point(608, 56)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
        Me.CheckBox1.TabIndex = 25
        Me.CheckBox1.Text = "Eliminar"
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(504, 25)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(88, 20)
        Me.txtTotal.TabIndex = 23
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(464, 25)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(36, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Total"
        '
        'txtRentencion
        '
        Me.txtRentencion.Location = New System.Drawing.Point(376, 25)
        Me.txtRentencion.Name = "txtRentencion"
        Me.txtRentencion.Size = New System.Drawing.Size(72, 20)
        Me.txtRentencion.TabIndex = 20
        Me.txtRentencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(312, 25)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(65, 13)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "Retención"
        '
        'txtImpuesto
        '
        Me.txtImpuesto.Location = New System.Drawing.Point(224, 25)
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.Size = New System.Drawing.Size(72, 20)
        Me.txtImpuesto.TabIndex = 18
        Me.txtImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(168, 25)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 13)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "Impuesto"
        '
        'txtSubtotal
        '
        Me.txtSubtotal.Location = New System.Drawing.Point(64, 25)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.Size = New System.Drawing.Size(88, 20)
        Me.txtSubtotal.TabIndex = 16
        Me.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 25)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "SubTotal"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(616, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 32)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Guardar Cambios"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvDetalle)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 232)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(755, 158)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle de la Requisa"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Cantidad, Me.Precio, Me.Valor, Me.IVA})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(9, 18)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(694, 131)
        Me.dgvDetalle.TabIndex = 96
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Descripcion
        '
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 280
        '
        'Cantidad
        '
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        Me.Cantidad.Width = 65
        '
        'Precio
        '
        '
        '
        '
        Me.Precio.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Precio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Increment = 1.0R
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Increment = 1.0R
        Me.Valor.Name = "Valor"
        Me.Valor.ReadOnly = True
        Me.Valor.Width = 90
        '
        'IVA
        '
        Me.IVA.HeaderText = "IVA"
        Me.IVA.Name = "IVA"
        Me.IVA.ReadOnly = True
        Me.IVA.Visible = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Importar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Procesar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 393)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(755, 78)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Resumen de la Importación"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtSubTotalFactContado)
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.txtCantidadFactContado)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.txtTotalFactContado)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.txtRetencionFactContado)
        Me.GroupBox5.Controls.Add(Me.txtImpuestoFactContado)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Location = New System.Drawing.Point(16, 14)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(680, 56)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Requisas al Contado"
        '
        'txtSubTotalFactContado
        '
        Me.txtSubTotalFactContado.Location = New System.Drawing.Point(64, 24)
        Me.txtSubTotalFactContado.Name = "txtSubTotalFactContado"
        Me.txtSubTotalFactContado.Size = New System.Drawing.Size(88, 20)
        Me.txtSubTotalFactContado.TabIndex = 20
        Me.txtSubTotalFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "SubTotal"
        '
        'txtCantidadFactContado
        '
        Me.txtCantidadFactContado.Location = New System.Drawing.Point(632, 24)
        Me.txtCantidadFactContado.Name = "txtCantidadFactContado"
        Me.txtCantidadFactContado.Size = New System.Drawing.Size(40, 20)
        Me.txtCantidadFactContado.TabIndex = 18
        Me.txtCantidadFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(576, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Cantidad"
        '
        'txtTotalFactContado
        '
        Me.txtTotalFactContado.Location = New System.Drawing.Point(480, 24)
        Me.txtTotalFactContado.Name = "txtTotalFactContado"
        Me.txtTotalFactContado.Size = New System.Drawing.Size(88, 20)
        Me.txtTotalFactContado.TabIndex = 16
        Me.txtTotalFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(440, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Total"
        '
        'txtRetencionFactContado
        '
        Me.txtRetencionFactContado.Location = New System.Drawing.Point(360, 24)
        Me.txtRetencionFactContado.Name = "txtRetencionFactContado"
        Me.txtRetencionFactContado.Size = New System.Drawing.Size(72, 20)
        Me.txtRetencionFactContado.TabIndex = 14
        Me.txtRetencionFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpuestoFactContado
        '
        Me.txtImpuestoFactContado.Location = New System.Drawing.Point(216, 24)
        Me.txtImpuestoFactContado.Name = "txtImpuestoFactContado"
        Me.txtImpuestoFactContado.Size = New System.Drawing.Size(72, 20)
        Me.txtImpuestoFactContado.TabIndex = 13
        Me.txtImpuestoFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(296, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Retención"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(160, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Impuesto"
        '
        'Timer1
        '
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdImportar, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(762, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 36
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdImportar
        '
        Me.cmdImportar.Image = CType(resources.GetObject("cmdImportar.Image"), System.Drawing.Image)
        Me.cmdImportar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdImportar.Name = "cmdImportar"
        Me.cmdImportar.Text = "Importar<F3>"
        Me.cmdImportar.Tooltip = "Importar"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Procesar<F4>"
        Me.cmdiLimpiar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmImportarMovimientoTraslado
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(762, 474)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmImportarMovimientoTraslado"
        Me.Text = "frmImportarMovimientoTraslado"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String
    Dim strNombAlmacenar As String

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmImportarMovimientoTraslado"


    Private Sub frmImportarMovimientoTraslado_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try


            txtSubTotalFactContado.ReadOnly = True
            txtImpuestoFactContado.ReadOnly = True
            txtRetencionFactContado.ReadOnly = True
            txtTotalFactContado.ReadOnly = True
            txtCantidadFactContado.ReadOnly = True
            txtSubtotal.ReadOnly = True
            txtImpuesto.ReadOnly = True
            txtRentencion.ReadOnly = True
            txtTotal.ReadOnly = True
            txtSubTotalFactContado.Text = ""
            txtImpuestoFactContado.Text = ""
            txtRetencionFactContado.Text = ""
            txtTotalFactContado.Text = ""
            txtCantidadFactContado.Text = ""
            txtSubtotal.Text = ""
            txtImpuesto.Text = ""
            txtRentencion.Text = ""
            txtTotal.Text = ""
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            ValidarDatosImportados()
            CargarResumen()
            CargarFacturas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Private Sub frmImportarMovimientoTraslado_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try

            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                ObtenerArchivo()
            ElseIf e.KeyCode = Keys.F4 Then
                ProcesarArchivo()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 2
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click

        Select Case sender.name.ToString
            Case "Importar" : ObtenerArchivo()
            Case "Procesar"
            Case "Salir" : Me.Close()
        End Select

    End Sub

    Sub Limpiar()
        Try
            TextBox1.Text = ""
            txtSubtotal.Text = ""
            txtImpuesto.Text = ""
            txtRentencion.Text = ""
            txtTotal.Text = ""
            dgvDetalle.Rows.Clear()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try


    End Sub

    Sub ObtenerArchivo()
        Dim dlgNew As New OpenFileDialog
        Dim strLine, strDrive As String

        Try

            Dim lngRegistro As Long
            Dim strFacturas As String
            Dim intFechaIng As Integer
            Dim strFechaIng As String
            Dim strVendCodigo As String
            Dim strCliCodigo As String
            Dim strCliNombre As String
            Dim dblSubTotalCosto As Double
            Dim dblSubTotal As Double
            Dim dblImpuesto As Double
            Dim dblRetencion As Double
            Dim dblTotal As Double
            Dim strUsrCodigo As String
            Dim bytAgenCodigo As Byte
            Dim bytStatus As Byte
            Dim bytImpreso As Byte
            Dim bytTipoFactura As Byte
            Dim intDiasCredito As Integer
            Dim intNumFecAnul As Integer
            Dim strProdCodigo As String
            Dim strProdDescrip As String
            Dim dblCantidad As Double
            Dim dblPrecio As Double
            Dim dblPrecioCosto As Double
            Dim dblValor As Double
            Dim dblIva As Double
            Dim strIdMovimiento As String = ""
            Dim intAgenciaDestino As Integer = 0
            Dim intRegDetalle As Integer = 0
            Dim strArchivo, strArchivo2, strClave, strComando As String

            Dim existe, er As Integer
            Dim strQueryVal As String = ""

            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
                Exit Sub
            End If
            strDrive = ""
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "tras_")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MsgBox("No es un archivo que contiene las Requisa de una agencia. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            strNombAlmacenar = ""
            strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
                Exit Sub
            End If
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "truncate table tmpImportarMovimientos"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            strNombArchivo = ""
            strNombArchivo = strArchivo2
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            intIncr = 0
            intIncr = InStrRev(strNombArchivo, "\")
            strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
            strQuery = ""
            strQuery = "select * from tbl_ArchivosImpMovimientoTraslado where archivo = '" & strNombAlmacenar & "'"
            'strQuery = "select * from tbl_ArchivosImpMovimientoTraslado where archivo = ''"
            cmdAgro2K.CommandText = strQuery
            Try
                intError = 0
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    intError = 0  'intError = 1 Se asigna cero para no validar el nombre del archivo de requisas que ya existe
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            If intError = 1 Then
                MsgBox("Archivo ya fue procesado.  Verifique.", MsgBoxStyle.Critical, "Error de Archivo")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            intRegDetalle = 0

            er = 1  ' con uno indica que las requisa del archivo que se importa ya existen, su valor cambia si al menos una requisa no existe
            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                intIncr = InStr(1, strLine, "|")
                lngRegistro = 0
                lngRegistro = CLng(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strFacturas = ""
                strFacturas = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                intFechaIng = 0
                intFechaIng = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strFechaIng = ""
                strFechaIng = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strVendCodigo = ""
                strVendCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strCliCodigo = ""
                strCliCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strCliNombre = ""
                strCliNombre = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblSubTotalCosto = 0
                dblSubTotalCosto = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblSubTotal = 0
                dblSubTotal = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblImpuesto = 0
                dblImpuesto = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblRetencion = 0
                dblRetencion = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblTotal = 0
                dblTotal = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strUsrCodigo = ""
                strUsrCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                bytAgenCodigo = 0
                bytAgenCodigo = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                bytStatus = 0
                bytStatus = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                bytImpreso = 0
                bytImpreso = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                bytTipoFactura = 0
                bytTipoFactura = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                intDiasCredito = 0
                intDiasCredito = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                intNumFecAnul = 0
                intNumFecAnul = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strProdCodigo = ""
                strProdCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                strProdDescrip = ""
                strProdDescrip = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblCantidad = 0
                dblCantidad = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblPrecio = 0
                dblPrecio = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                intIncr = InStr(1, strLine, "|")
                dblPrecioCosto = 0
                If (intIncr > 0) Then
                    dblPrecioCosto = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                Else
                    If strLine.Trim().Length > 0 Then
                        dblPrecioCosto = CDbl(strLine.Trim())
                        strLine = ""
                    End If
                End If
                intIncr = InStr(1, strLine, "|")
                dblValor = 0
                If (intIncr > 0) Then
                    strIdMovimiento = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                End If
                If (strLine.Trim().Length > 0) Then
                    intAgenciaDestino = CDbl(strLine)
                    'strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                End If
                intRegDetalle = intRegDetalle + 1
                If bytStatus = 1 Then
                    bytImpreso = 0
                End If

                strQueryVal = "exec spValidaRequisaImportar '" & strFacturas & "' "

                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If

                cnnAgro2K.Open()
                Try
                    cmdAgro2K.CommandText = strQueryVal
                    dtrAgro2K = cmdAgro2K.ExecuteReader()
                    While dtrAgro2K.Read
                        existe = dtrAgro2K.GetValue(0)
                    End While
                    dtrAgro2K.Close()

                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try

                If existe = 0 Then  'Cero indica que la requisa no existe en el sitema y por tanto se ingresa, 1 indica que existe y no se ingresa
                    er = existe
                    strQuery = ""
                    strQuery = "insert into tmpImportarMovimientos values (" & lngRegistro & ", '" & strFacturas & "', '" & strIdMovimiento & "', "
                    strQuery = strQuery + "" & intFechaIng & ", '" & strFechaIng & "', '" & strVendCodigo & "', "
                    strQuery = strQuery + "'" & strCliCodigo & "', '" & strCliNombre & "', " & dblSubTotalCosto & ", " & dblSubTotal & ", "
                    strQuery = strQuery + "" & dblImpuesto & ", " & dblRetencion & ", " & dblTotal & ", "
                    strQuery = strQuery + "'" & strUsrCodigo & "', " & bytAgenCodigo & ", " & intAgenciaDestino & ", " & bytStatus & ", "
                    strQuery = strQuery + "" & bytImpreso & ", " & bytTipoFactura & ", " & intDiasCredito & ", "
                    strQuery = strQuery + "" & intNumFecAnul & ", '" & strProdCodigo & "', "
                    strQuery = strQuery + "'" & strProdDescrip & "', " & dblCantidad & ", " & dblPrecio & ", " & dblPrecioCosto & ", "
                    strQuery = strQuery + "" & dblValor & "," & dblIva & ", " & intRegDetalle & ",'Sin Error')"
                    Try
                        cmdAgro2K.CommandText = strQuery
                        cmdAgro2K.ExecuteNonQuery()
                    Catch exc As Exception
                        intError = 1
                        MsgBox(exc.Message.ToString)
                        Me.Cursor = Cursors.Default
                        Exit Do
                    End Try
                End If
            Loop
            sr.Close()
            If er = 0 Then 'Condicionamos para indicarle al husuario si ya ha ingresado las requisas
                If intError = 0 Then
                    ValidarDatosImportados()
                    CargarResumen()
                    CargarFacturas()
                    MsgBox("Finalizado satisfactoriamente la importación de las Requisa.", MsgBoxStyle.Information, "Proceso Finalizado")
                Else
                    MsgBox("Hubieron errores al importar el archivo de Requisa.", MsgBoxStyle.Critical, "Error en la Importación")
                End If
            Else
                MsgBox("El archivo de Requisa ya fue Importado.", MsgBoxStyle.Critical, "Error en la Importación")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub ValidarDatosImportados()

        Try

            Dim cmdTmp As New SqlCommand("PrcInventarioTrasladoImp", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter

            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(0) = 0 Then
                        MsgBox("No hubieron errores en la validación de las Requisa", MsgBoxStyle.Information, "Validación de Datos")
                    ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                        MsgBox("Hubieron errores en la validación de las Requisa", MsgBoxStyle.Critical, "Validación de Datos")
                    End If
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Sub

    Sub CargarResumen()
        Try

            Dim cmdTmp As New SqlCommand("PrcInventarioTrasladoImp", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 1
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                        txtSubTotalFactContado.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(1)), "##,##0.#0")
                        txtImpuestoFactContado.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(2)), "##,##0.#0")
                        txtRetencionFactContado.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(3)), "##,##0.#0")
                        txtTotalFactContado.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(4)), "##,##0.#0")
                        txtCantidadFactContado.Text = dtrAgro2K.GetValue(5)
                    End If
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try

            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            cmdTmp.Parameters(0).Value = 2
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                        'txtSubTotalFactCredito.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(0)), "##,##0.#0")
                        'txtImpuestoFactCredito.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(1)), "##,##0.#0")
                        'txtRetencionFactCredito.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(2)), "##,##0.#0")
                        'txtTotalFactCredito.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(3)), "##,##0.#0")
                        'txtCantidadFactCredito.Text = dtrAgro2K.GetValue(4)
                    End If
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            Finally
                dtrAgro2K.Close()
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Sub CargarFacturas()
        Try

            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strQuery = ""
            strQuery = "select distinct registro, numero, tipofactura from tmpImportarMovimientos where status = 0 order by tipofactura, numero"
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
                    ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Try


            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Limpiar()
            ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
            TextBox1.Text = "Sin Error"
            strQuery = ""
            strQuery = "select * from tmpImportarMovimientos where registro = " & CLng(ComboBox2.SelectedItem) & ""
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    Label18.Text = dtrAgro2K.GetValue(3)
                    'txtCliente.Text = dtrAgro2K.GetValue(5)
                    'txtNombreCliente.Text = dtrAgro2K.GetValue(6)
                    'txtDiasCredito.Text = dtrAgro2K.GetValue(17)
                    txtSubtotal.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(8)), "##,##0.#0")
                    txtImpuesto.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(10)), "##,##0.#0")
                    txtRentencion.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(11)), "##,##0.#0")
                    txtTotal.Text = Format(ConvierteADouble(dtrAgro2K.GetValue(12)), "##,##0.#0")

                    dgvDetalle.Rows.Add(dtrAgro2K.GetValue(21), dtrAgro2K.GetValue(22), Format(dtrAgro2K.GetValue(23), "##,##0.#0"),
                                        Format(dtrAgro2K.GetValue(24), "##,##0.#0"), Format(dtrAgro2K.GetValue(25), "##,##0.#0"),
                                        Format(dtrAgro2K.GetValue(27), "##,##0.#0"), CInt(dtrAgro2K.GetValue(28)), Format(dtrAgro2K.GetValue(24), "##,##0.#0"))

                    If dtrAgro2K.GetValue(29) <> "Sin Error" Then
                        If Len(TextBox1.Text) < Len(dtrAgro2K.GetValue(29)) Then
                            TextBox1.Text = dtrAgro2K.GetValue(29)
                        End If
                    End If
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try
    End Sub

    Function UbicarProducto(ByVal prm01 As String) As Boolean

        Dim intEnc As Integer = 0

        Try


            intEnc = 0
            strQuery = ""
            strQuery = "Select Codigo, Descripcion, Pvpc, Impuesto from prm_Productos Where Codigo = '" & prm01 & "'"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read

            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If intEnc = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Function

    Sub ProcesarArchivo()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("PrcInventarioTrasladoImp", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim bytContinuar As Byte
        Try


            bytContinuar = 0
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(0) = 0 Then
                        MsgBox("No hay errores en la validación de las Requisa. Se procedera a ingresar las Requisa.", MsgBoxStyle.Information, "Validación de Datos")
                    ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                        bytContinuar = 1
                        MsgBox("Continuan los errores en la validación de las Requisa", MsgBoxStyle.Critical, "Validación de Datos")
                        Exit Sub
                    End If
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                dtrAgro2K.Close()
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            End Try
            If bytContinuar = 0 Then
                cmdTmp.Parameters(0).Value = 3
                Try
                    cmdTmp.ExecuteNonQuery()
                Catch exc As Exception
                    MsgBox(exc.Message.ToString)
                    dtrAgro2K.Close()
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Sub
                End Try
            End If
            strQuery = ""
            strQuery = "insert into tbl_ArchivosImpMovimientoTraslado values ('" & strNombAlmacenar & "', "
            strQuery = strQuery + "'" & Format(Now, "dd-MMM-yyyy") & "', " & Format(Now, "yyyyMMdd") & ", "
            strQuery = strQuery + "" & lngRegUsuario & ")"
            cmdAgro2K.CommandText = strQuery
            Try
                cmdAgro2K.ExecuteNonQuery()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            strQuery = ""
            strQuery = "truncate table tmpImportarMovimientos"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            MsgBox("Finalizo el proceso de importar Requisa de una agencia a la Casa Matriz.", MsgBoxStyle.Exclamation, "Proceso Finalizado")
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            'If strUbicar <> "" Then
            '    txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text) : txtNombreCliente.Focus()
            'End If
        End If

    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        'If e.KeyCode = Keys.Enter Then
        '    txtNombreCliente.Focus()
        'End If

    End Sub

    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

        'UbicarCliente(txtCliente.Text)

    End Sub

    Private Sub cmdImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportar.Click
        Try
            ObtenerArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try


    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            ProcesarArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try


    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

    End Sub
End Class
