Imports System.IO
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmExportarCuadreCaja

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmExportarCuadreCaja"

    Private Sub frmExportarCuadreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            DPFechaInicio.Value = Now
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub frmExportarCuadreCaja_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Procesar()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub Procesar()

        Me.Cursor = Cursors.WaitCursor
        Dim intCantidad As Integer = 0
        Dim intContador As Integer = 0
        Dim intNumFechaInicio As Integer = 0
        Dim intNumFechaFin As Integer = 0
        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim lsRutaCompletaArchivoZip As String = String.Empty
        Try
            ProgressBar1.Focus()
            intCantidad = 0
            intContador = 0
            intNumFechaInicio = 0
            intNumFechaInicio = Format(DPFechaInicio.Value, "yyyyMMdd")
            intNumFechaFin = 0
            intNumFechaFin = Format(DPFechaFin.Value, "yyyyMMdd")
            strQuery = ""
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If

            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "exec VerificaRegistrosAExportarCuadre " & intNumFechaInicio & "," & intNumFechaFin & "," & lngAgenRegExp
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    intCantidad = dtrAgro2K.GetValue(0)
                End If
            End While
            dtrAgro2K.Close()
            strNombreArchivo = ""
            If intCantidad <> 0 Then
                ProgressBar1.Minimum = 0
                ProgressBar1.Maximum = intCantidad
                ProgressBar1.Value = 0
                strQuery = ""
                strQuery = " exec spConsultaCuadreAExportar " & intNumFechaInicio & "," & intNumFechaFin & "," & lngAgenRegExp
                cmdAgro2K.CommandText = strQuery
                strClave = ""
                strClave = "Agro2K_2008"
                strArchivo = ""
                strArchivo = ConfigurationManager.AppSettings("RutaArchivosArqueos").ToString() & "CuadreCaja_" & Format(intAgenciaDefecto, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt"
                strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosArqueos").ToString() & "CuadreCaja_" & Format(intAgenciaDefecto, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd")
                Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosArqueos").ToString() & "CuadreCaja_" & Format(intAgenciaDefecto, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt")
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ProgressBar1.Value = ProgressBar1.Value + 1
                    sr.WriteLine(dtrAgro2K.Item("IdCuadre") & "|" & Trim(dtrAgro2K.Item("IdMoneda")) & "|" & dtrAgro2K.Item("IdUsuario") & "|" & Trim(dtrAgro2K.Item("IdSucursal")) & "|" & Trim(dtrAgro2K.Item("NumeroCuadre")) & "|" & dtrAgro2K.Item("FechaInicioCuadre") & "|" &
                             Trim(dtrAgro2K.Item("FechaInicioCuadreNum")) & "|" & Trim(dtrAgro2K.Item("FechaCuadreFin")) & "|" & Trim(dtrAgro2K.Item("FechaCuadreFinNum")) & "|" & dtrAgro2K.Item("FacturaCreditoInicial") & "|" &
                             dtrAgro2K.Item("FacturaCreditoFinal") & "|" & dtrAgro2K.Item("FacturaContadoInicial") & "|" & dtrAgro2K.Item("FacturaContadoFinal") & "|" & Trim(dtrAgro2K.Item("ReciboInicialCredito")) & "|" &
                             dtrAgro2K.Item("ReciboFinalCredito") & "|" & dtrAgro2K.Item("ReciboInicialDebito") & "|" & dtrAgro2K.Item("ReciboFinalDebito") & "|" & dtrAgro2K.Item("SaldoInicial") & "|" & dtrAgro2K.Item("MontoTotalVentas") & "|" &
                             dtrAgro2K.Item("MontoTotalRecibos") & "|" & Trim(dtrAgro2K.Item("MontoGastos")) & "|" & Trim(dtrAgro2K.Item("MontoDepositos")) & "|" & dtrAgro2K.Item("MontoTotalDineroCaja") & "|" & dtrAgro2K.Item("MontoTotalIngreso") & "|" &
                             dtrAgro2K.Item("MontoTotalEgreso") & "|" & dtrAgro2K.Item("SaldoFinal") & "|" & dtrAgro2K.Item("Tasa") & "|" & quitarSaltosLinea(dtrAgro2K.Item("Observacion")) & "|" & dtrAgro2K.Item("IdEstado") & "|" & dtrAgro2K.Item("FechaModifica") & "|" &
                             dtrAgro2K.Item("FechaModificaNum") & "|" & dtrAgro2K.Item("FechaAnula") & "|" & dtrAgro2K.Item("FechaAnulaNum") & "|" & dtrAgro2K.Item("FechaIngreso") & "|" & dtrAgro2K.Item("FechaIngresoNum") & "|" &
                             dtrAgro2K.Item("IdUsuarioIngresa") & "|" & dtrAgro2K.Item("IdUsuarioModifica") & "|" & dtrAgro2K.Item("IdUsuarioAnula") & "|" & dtrAgro2K.Item("IdUltimoCuadre") & "|" & dtrAgro2K.Item("NumeroDocumento") & "|" &
                             dtrAgro2K.Item("MontoTotalDocumento") & "|" & dtrAgro2K.Item("IdTipoDocumento") & "|" & dtrAgro2K.Item("FechaDocumento") & "|" & dtrAgro2K.Item("FechaDocumentoNum") & "|" & dtrAgro2K.Item("IdUsuarioDocumento") & "|" &
                             dtrAgro2K.Item("IdEstadoDocumento") & "|" & dtrAgro2K.Item("IdMonedaC") & "|" & dtrAgro2K.Item("DenC_500") & "|" & dtrAgro2K.Item("CantC_500") & "|" & dtrAgro2K.Item("DenC_200") & "|" &
                             dtrAgro2K.Item("CantC_200") & "|" & dtrAgro2K.Item("DenC_100") & "|" & dtrAgro2K.Item("CantC_100") & "|" & dtrAgro2K.Item("DenC_50") & "|" & dtrAgro2K.Item("CantC_50") & "|" & dtrAgro2K.Item("DenC_20") & "|" &
                             dtrAgro2K.Item("CantC_20") & "|" & dtrAgro2K.Item("DenC_10") & "|" & dtrAgro2K.Item("CantC_10") & "|" & dtrAgro2K.Item("DenC_5") & "|" & dtrAgro2K.Item("CantC_5") & "|" & dtrAgro2K.Item("DenC_1") & "|" &
                             dtrAgro2K.Item("CantC_1") & "|" & dtrAgro2K.Item("DenC_050") & "|" & dtrAgro2K.Item("CantC_050") & "|" & dtrAgro2K.Item("DenC_025") & "|" & dtrAgro2K.Item("CantC_025") & "|" & dtrAgro2K.Item("DenC_010") & "|" &
                             dtrAgro2K.Item("CantC_010") & "|" & dtrAgro2K.Item("DenC_005") & "|" & dtrAgro2K.Item("CantC_005") & "|" & dtrAgro2K.Item("DenC_001") & "|" & dtrAgro2K.Item("CantC_001") & "|" & dtrAgro2K.Item("IdMonedaD") & "|" &
                             dtrAgro2K.Item("DenD_100") & "|" & dtrAgro2K.Item("CantD_100") & "|" & dtrAgro2K.Item("DenD_50") & "|" & dtrAgro2K.Item("CantD_50") & "|" & dtrAgro2K.Item("DenD_20") & "|" & dtrAgro2K.Item("CantD_20") & "|" &
                             dtrAgro2K.Item("DenD_10") & "|" & dtrAgro2K.Item("CantD_10") & "|" & dtrAgro2K.Item("DenD_5") & "|" & dtrAgro2K.Item("CantD_5") & "|" & dtrAgro2K.Item("DenD_1") & "|" & dtrAgro2K.Item("CantD_1") & "|" &
                             dtrAgro2K.Item("DenD_050") & "|" & dtrAgro2K.Item("CantD_050") & "|" & dtrAgro2K.Item("DenD_010") & "|" & dtrAgro2K.Item("CantD_010") & "|" & dtrAgro2K.Item("HoraInicioCuadre") & "|" & dtrAgro2K.Item("HoraCuadreFin") & "|" &
                             dtrAgro2K.Item("HoraModifica") & "|" & dtrAgro2K.Item("HoraAnula") & "|" & dtrAgro2K.Item("HoraIngreso") & "|" & dtrAgro2K.Item("HoraDocumento") & "|" & dtrAgro2K.Item("OtrosIngresos") & "|" & dtrAgro2K.Item("SobranteCaja"))
                End While
                'dtrAgro2K.Item("IdEstadoDocumento") & "|" & dtrAgro2K.Item("IdCuadreDM") & "|" & dtrAgro2K.Item("IdMonedaC") & "|" & dtrAgro2K.Item("DenC_500") & "|" & dtrAgro2K.Item("CantC_500") & "|" & dtrAgro2K.Item("DenC_200") & "|" & _
                sr.Close()
                'sr2.Close()
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                intIncr = 0
                Try
                    'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                    'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)

                    'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                    lsRutaCompletaArchivoZip = String.Empty
                    lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
                    If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                        MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                        Exit Sub
                    End If
                    If File.Exists(strArchivo) = True Then
                        File.Delete(strArchivo)
                    End If
                Catch exx As Exception
                    MsgBox(exx.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End Try
            Else
                MsgBox("No hay Cierres de Cajas que exportar para el d�a solicitado.", MsgBoxStyle.Information, "Error en Solicitud")
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                ProgressBar1.Value = 0
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            MsgBox("Finalizo el proceso de exportaci�n de Cuadre de Caja", MsgBoxStyle.Information, "Proceso Finalizado")
            ProgressBar1.Value = 0

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Me.Cursor = Cursors.Default
            Throw ex
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Try
            Procesar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

End Class