Imports System.IO
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmExportarRecibosCtasXPagar

    Private Sub frmExportarRecibosCtasXPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DPFechaInicio.Value = Now
    End Sub

    Private Sub frmExportarRecibosCtasXPagar_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Procesar()
        End If
    End Sub

    Sub Procesar()

        Me.Cursor = Cursors.WaitCursor
        Dim intCantidad, intContador, intNumFechaInicio, intNumFechaFin As Integer
        Dim strArchivo, strArchivo2, strClave As String
        Dim lsRutaCompletaArchivoZip As String = String.Empty
        ProgressBar1.Focus()
        intCantidad = 0
        intContador = 0
        intNumFechaInicio = 0
        intNumFechaInicio = Format(DPFechaInicio.Value, "yyyyMMdd")
        intNumFechaFin = 0
        intNumFechaFin = Format(DPFechaFin.Value, "yyyyMMdd")
        strQuery = ""
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If

        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "exec VerificaRegistrosAExportarReciboCtasxPagar " & intNumFechaInicio & "," & intNumFechaFin & "," & lngAgenRegExp
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                intCantidad = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        strNombreArchivo = ""
        If intCantidad <> 0 Then
            ProgressBar1.Minimum = 0
            ProgressBar1.Maximum = intCantidad
            ProgressBar1.Value = 0
            strQuery = ""
            strQuery = " exec ConsultaExportarRecibosCtasxPagar " & intNumFechaInicio & "," & intNumFechaFin & "," & lngAgenRegExp
            cmdAgro2K.CommandText = strQuery
            strClave = ""
            strClave = "Agro2K_2008"
            strArchivo = ""
            strArchivo = ConfigurationManager.AppSettings("RutaArchivosCtasXPagar").ToString() & "recibosCtasXPagar_" & Format(intAgenciaDefecto, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt"
            strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosCtasXPagar").ToString() & "recibosCtasXPagar_" & Format(intAgenciaDefecto, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd")
            Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosCtasXPagar").ToString() & "recibosCtasXPagar_" & Format(intAgenciaDefecto, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt")
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ProgressBar1.Value = ProgressBar1.Value + 1
                sr.WriteLine(dtrAgro2K.Item("IdRegistro") & "|" & Trim(dtrAgro2K.Item("fecha")) & "|" & dtrAgro2K.Item("numfecha") & "|" & Trim(dtrAgro2K.Item("CodigoProveedor")) & "|" & dtrAgro2K.Item("NombreProveedor") & "|" &
                             Trim(dtrAgro2K.Item("Codigovendedor")) & "|" & Trim(dtrAgro2K.Item("NumeroRecibo")) & "|" & Trim(dtrAgro2K.Item("MontoTotal")) & "|" & dtrAgro2K.Item("InteresTotal") & "|" &
                             dtrAgro2K.Item("RetencionTotal") & "|" & dtrAgro2K.Item("formapago") & "|" & dtrAgro2K.Item("numchqtarj") & "|" & Trim(dtrAgro2K.Item("nombrebanco")) & "|" &
                             dtrAgro2K.Item("reciboprov") & "|" & dtrAgro2K.Item("DescrMaestro") & "|" & dtrAgro2K.Item("CodigoUsuario") & "|" & dtrAgro2K.Item("tiporecibo") & "|" &
                             dtrAgro2K.Item("HoraIngreso") & "|" & dtrAgro2K.Item("saldofavor") & "|" & Trim(dtrAgro2K.Item("estado")) & "|" & Trim(dtrAgro2K.Item("descr01")) & "|" &
                             dtrAgro2K.Item("descr02") & "|" & dtrAgro2K.Item("descr03") & "|" & dtrAgro2K.Item("CodigoAgencia") & "|" & dtrAgro2K.Item("RegistroDetalle") & "|" &
                             dtrAgro2K.Item("NumeroFactura") & "|" & dtrAgro2K.Item("DescrDetalle") & "|" & dtrAgro2K.Item("monto") & "|" & dtrAgro2K.Item("interes") & "|" & dtrAgro2K.Item("mantvlr") & "|" &
                             dtrAgro2K.Item("pendiente") & "|" & dtrAgro2K.Item("FechaAnulado") & "|" & dtrAgro2K.Item("HoraAnulado") & "|" & dtrAgro2K.Item("PorcDescuentoFact") & "|" & dtrAgro2K.Item("MontoDescuentoFact"))
            End While
            sr.Close()
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            intIncr = 0
            Try
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)

                lsRutaCompletaArchivoZip = String.Empty
                lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
                If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                    MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                    Exit Sub
                End If

                If File.Exists(strArchivo) = True Then
                    File.Delete(strArchivo)
                End If
            Catch exx As Exception
                MsgBox(exx.Message.ToString)
                Me.Cursor = Cursors.Default
                Exit Sub
            End Try
        Else
            MsgBox("No hay recibos que exportar para el d�a solicitado.", MsgBoxStyle.Information, "Error en Solicitud")
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            ProgressBar1.Value = 0
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        MsgBox("Finalizo el proceso de exportaci�n de los recibos", MsgBoxStyle.Information, "Proceso Finalizado")
        ProgressBar1.Value = 0
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Procesar()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

End Class