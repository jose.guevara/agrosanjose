Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptInventKardexSimple
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader3 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter3 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label21 As DataDynamics.ActiveReports.Label = Nothing
    Private Label22 As DataDynamics.ActiveReports.Label = Nothing
    Private Label23 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox22 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox23 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label25 As DataDynamics.ActiveReports.Label = Nothing
    Private Label26 As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private txtMes As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtAnio As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label20 As DataDynamics.ActiveReports.Label = Nothing
    Private txtCodigoAgencia As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private Label2 As DataDynamics.ActiveReports.Label = Nothing
    Private txtDesProducto As DataDynamics.ActiveReports.TextBox = Nothing
    Private Line As DataDynamics.ActiveReports.Line = Nothing
    Private Line1 As DataDynamics.ActiveReports.Line = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private Label15 As DataDynamics.ActiveReports.Label = Nothing
    Private Label17 As DataDynamics.ActiveReports.Label = Nothing
    Private txtFechaMovimiento As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtDocumento As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtMovimiento As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtEntrada As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtSalida As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtExistencia As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label24 As DataDynamics.ActiveReports.Label = Nothing
    Friend WithEvents txtCantidadInicial As DataDynamics.ActiveReports.TextBox
    Private TextBox24 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptInventKardexSimple))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtFechaMovimiento = New DataDynamics.ActiveReports.TextBox
        Me.txtDocumento = New DataDynamics.ActiveReports.TextBox
        Me.txtMovimiento = New DataDynamics.ActiveReports.TextBox
        Me.txtEntrada = New DataDynamics.ActiveReports.TextBox
        Me.txtSalida = New DataDynamics.ActiveReports.TextBox
        Me.txtExistencia = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.Label21 = New DataDynamics.ActiveReports.Label
        Me.Label22 = New DataDynamics.ActiveReports.Label
        Me.Label23 = New DataDynamics.ActiveReports.Label
        Me.TextBox22 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox23 = New DataDynamics.ActiveReports.TextBox
        Me.Label25 = New DataDynamics.ActiveReports.Label
        Me.Label26 = New DataDynamics.ActiveReports.Label
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.Label24 = New DataDynamics.ActiveReports.Label
        Me.TextBox24 = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.txtMes = New DataDynamics.ActiveReports.TextBox
        Me.txtAnio = New DataDynamics.ActiveReports.TextBox
        Me.Label20 = New DataDynamics.ActiveReports.Label
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.GroupHeader2 = New DataDynamics.ActiveReports.GroupHeader
        Me.txtCodigoAgencia = New DataDynamics.ActiveReports.TextBox
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.txtCantidadInicial = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter2 = New DataDynamics.ActiveReports.GroupFooter
        Me.GroupHeader3 = New DataDynamics.ActiveReports.GroupHeader
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.txtDesProducto = New DataDynamics.ActiveReports.TextBox
        Me.Line = New DataDynamics.ActiveReports.Line
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.Label7 = New DataDynamics.ActiveReports.Label
        Me.Label9 = New DataDynamics.ActiveReports.Label
        Me.Label14 = New DataDynamics.ActiveReports.Label
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.Label17 = New DataDynamics.ActiveReports.Label
        Me.GroupFooter3 = New DataDynamics.ActiveReports.GroupFooter
        CType(Me.txtFechaMovimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMovimiento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEntrada, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSalida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtExistencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAnio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCantidadInicial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDesProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtFechaMovimiento, Me.txtDocumento, Me.txtMovimiento, Me.txtEntrada, Me.txtSalida, Me.txtExistencia})
        Me.Detail.Height = 0.1875!
        Me.Detail.Name = "Detail"
        '
        'txtFechaMovimiento
        '
        Me.txtFechaMovimiento.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFechaMovimiento.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaMovimiento.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFechaMovimiento.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaMovimiento.Border.RightColor = System.Drawing.Color.Black
        Me.txtFechaMovimiento.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaMovimiento.Border.TopColor = System.Drawing.Color.Black
        Me.txtFechaMovimiento.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFechaMovimiento.DataField = "FechaMovimiento"
        Me.txtFechaMovimiento.Height = 0.1875!
        Me.txtFechaMovimiento.Left = 0.875!
        Me.txtFechaMovimiento.Name = "txtFechaMovimiento"
        Me.txtFechaMovimiento.OutputFormat = resources.GetString("txtFechaMovimiento.OutputFormat")
        Me.txtFechaMovimiento.Style = "text-align: left; font-weight: normal; font-size: 8.25pt; "
        Me.txtFechaMovimiento.Text = Nothing
        Me.txtFechaMovimiento.Top = 0.0!
        Me.txtFechaMovimiento.Width = 1.3125!
        '
        'txtDocumento
        '
        Me.txtDocumento.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDocumento.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDocumento.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDocumento.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDocumento.Border.RightColor = System.Drawing.Color.Black
        Me.txtDocumento.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDocumento.Border.TopColor = System.Drawing.Color.Black
        Me.txtDocumento.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDocumento.DataField = "documento"
        Me.txtDocumento.Height = 0.1875!
        Me.txtDocumento.Left = 0.0625!
        Me.txtDocumento.Name = "txtDocumento"
        Me.txtDocumento.OutputFormat = resources.GetString("txtDocumento.OutputFormat")
        Me.txtDocumento.Style = "text-align: left; font-weight: normal; font-size: 8.25pt; "
        Me.txtDocumento.Text = Nothing
        Me.txtDocumento.Top = 0.0!
        Me.txtDocumento.Width = 0.75!
        '
        'txtMovimiento
        '
        Me.txtMovimiento.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMovimiento.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMovimiento.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMovimiento.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMovimiento.Border.RightColor = System.Drawing.Color.Black
        Me.txtMovimiento.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMovimiento.Border.TopColor = System.Drawing.Color.Black
        Me.txtMovimiento.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMovimiento.DataField = "Movimiento"
        Me.txtMovimiento.Height = 0.1875!
        Me.txtMovimiento.Left = 2.25!
        Me.txtMovimiento.Name = "txtMovimiento"
        Me.txtMovimiento.OutputFormat = resources.GetString("txtMovimiento.OutputFormat")
        Me.txtMovimiento.Style = "text-align: left; font-weight: normal; font-size: 8.25pt; "
        Me.txtMovimiento.Text = Nothing
        Me.txtMovimiento.Top = 0.0!
        Me.txtMovimiento.Width = 2.4375!
        '
        'txtEntrada
        '
        Me.txtEntrada.Border.BottomColor = System.Drawing.Color.Black
        Me.txtEntrada.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtEntrada.Border.LeftColor = System.Drawing.Color.Black
        Me.txtEntrada.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtEntrada.Border.RightColor = System.Drawing.Color.Black
        Me.txtEntrada.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtEntrada.Border.TopColor = System.Drawing.Color.Black
        Me.txtEntrada.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtEntrada.DataField = "Entrada"
        Me.txtEntrada.Height = 0.1875!
        Me.txtEntrada.Left = 4.8125!
        Me.txtEntrada.Name = "txtEntrada"
        Me.txtEntrada.OutputFormat = resources.GetString("txtEntrada.OutputFormat")
        Me.txtEntrada.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; "
        Me.txtEntrada.Text = Nothing
        Me.txtEntrada.Top = 0.0!
        Me.txtEntrada.Width = 0.6875!
        '
        'txtSalida
        '
        Me.txtSalida.Border.BottomColor = System.Drawing.Color.Black
        Me.txtSalida.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSalida.Border.LeftColor = System.Drawing.Color.Black
        Me.txtSalida.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSalida.Border.RightColor = System.Drawing.Color.Black
        Me.txtSalida.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSalida.Border.TopColor = System.Drawing.Color.Black
        Me.txtSalida.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtSalida.DataField = "Salida"
        Me.txtSalida.Height = 0.1875!
        Me.txtSalida.Left = 5.625!
        Me.txtSalida.Name = "txtSalida"
        Me.txtSalida.OutputFormat = resources.GetString("txtSalida.OutputFormat")
        Me.txtSalida.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; "
        Me.txtSalida.Text = Nothing
        Me.txtSalida.Top = 0.0!
        Me.txtSalida.Width = 0.6875!
        '
        'txtExistencia
        '
        Me.txtExistencia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtExistencia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtExistencia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtExistencia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtExistencia.Border.RightColor = System.Drawing.Color.Black
        Me.txtExistencia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtExistencia.Border.TopColor = System.Drawing.Color.Black
        Me.txtExistencia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtExistencia.DataField = "ExistenciaActual"
        Me.txtExistencia.Height = 0.1875!
        Me.txtExistencia.Left = 6.8125!
        Me.txtExistencia.Name = "txtExistencia"
        Me.txtExistencia.OutputFormat = resources.GetString("txtExistencia.OutputFormat")
        Me.txtExistencia.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; "
        Me.txtExistencia.Text = Nothing
        Me.txtExistencia.Top = 0.0!
        Me.txtExistencia.Width = 0.625!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label21, Me.Label22, Me.Label23, Me.TextBox22, Me.TextBox23, Me.Label25, Me.Label26})
        Me.PageHeader.Height = 0.9270833!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label21
        '
        Me.Label21.Border.BottomColor = System.Drawing.Color.Black
        Me.Label21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.LeftColor = System.Drawing.Color.Black
        Me.Label21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.RightColor = System.Drawing.Color.Black
        Me.Label21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Border.TopColor = System.Drawing.Color.Black
        Me.Label21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label21.Height = 0.2!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label21.Text = "KARDEX DE LOS PRODUCTOS"
        Me.Label21.Top = 0.25!
        Me.Label21.Width = 2.75!
        '
        'Label22
        '
        Me.Label22.Border.BottomColor = System.Drawing.Color.Black
        Me.Label22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Border.LeftColor = System.Drawing.Color.Black
        Me.Label22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Border.RightColor = System.Drawing.Color.Black
        Me.Label22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Border.TopColor = System.Drawing.Color.Black
        Me.Label22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label22.Height = 0.2!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 5.15!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label22.Text = "Fecha de Impresi�n"
        Me.Label22.Top = 0.05!
        Me.Label22.Width = 1.35!
        '
        'Label23
        '
        Me.Label23.Border.BottomColor = System.Drawing.Color.Black
        Me.Label23.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Border.LeftColor = System.Drawing.Color.Black
        Me.Label23.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Border.RightColor = System.Drawing.Color.Black
        Me.Label23.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Border.TopColor = System.Drawing.Color.Black
        Me.Label23.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label23.Height = 0.2!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 5.15!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label23.Text = "Hora de Impresi�n"
        Me.Label23.Top = 0.25!
        Me.Label23.Width = 1.35!
        '
        'TextBox22
        '
        Me.TextBox22.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Height = 0.2!
        Me.TextBox22.Left = 6.5!
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Style = "text-align: right; "
        Me.TextBox22.Text = "TextBox22"
        Me.TextBox22.Top = 0.05!
        Me.TextBox22.Width = 0.9!
        '
        'TextBox23
        '
        Me.TextBox23.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox23.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox23.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox23.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox23.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox23.Height = 0.2!
        Me.TextBox23.Left = 6.5!
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Style = "text-align: right; "
        Me.TextBox23.Text = "TextBox23"
        Me.TextBox23.Top = 0.25!
        Me.TextBox23.Width = 0.9!
        '
        'Label25
        '
        Me.Label25.Border.BottomColor = System.Drawing.Color.Black
        Me.Label25.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Border.LeftColor = System.Drawing.Color.Black
        Me.Label25.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Border.RightColor = System.Drawing.Color.Black
        Me.Label25.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Border.TopColor = System.Drawing.Color.Black
        Me.Label25.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label25.Height = 0.2!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0.0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label25.Text = "INVENTARIO DE CLASES DETALLE"
        Me.Label25.Top = 0.45!
        Me.Label25.Width = 2.75!
        '
        'Label26
        '
        Me.Label26.Border.BottomColor = System.Drawing.Color.Black
        Me.Label26.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Border.LeftColor = System.Drawing.Color.Black
        Me.Label26.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Border.RightColor = System.Drawing.Color.Black
        Me.Label26.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Border.TopColor = System.Drawing.Color.Black
        Me.Label26.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label26.Height = 0.2!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 0.0!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label26.Text = "REPORTE GENERADO DEL INVENTARIO"
        Me.Label26.Top = 0.0625!
        Me.Label26.Width = 3.0!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label24, Me.TextBox24})
        Me.PageFooter.Height = 0.7604167!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label24
        '
        Me.Label24.Border.BottomColor = System.Drawing.Color.Black
        Me.Label24.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Border.LeftColor = System.Drawing.Color.Black
        Me.Label24.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Border.RightColor = System.Drawing.Color.Black
        Me.Label24.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Border.TopColor = System.Drawing.Color.Black
        Me.Label24.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label24.Height = 0.2!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 6.45!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label24.Text = "P�gina"
        Me.Label24.Top = 0.1!
        Me.Label24.Width = 0.5500001!
        '
        'TextBox24
        '
        Me.TextBox24.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox24.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox24.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox24.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox24.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox24.Height = 0.2!
        Me.TextBox24.Left = 6.985417!
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Style = "text-align: right; "
        Me.TextBox24.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox24.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox24.Text = "TextBox24"
        Me.TextBox24.Top = 0.1!
        Me.TextBox24.Width = 0.3499999!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label1, Me.txtMes, Me.txtAnio, Me.Label20})
        Me.GroupHeader1.DataField = "AnioMes"
        Me.GroupHeader1.Height = 0.2388889!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'Label1
        '
        Me.Label1.Border.BottomColor = System.Drawing.Color.Black
        Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.LeftColor = System.Drawing.Color.Black
        Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.RightColor = System.Drawing.Color.Black
        Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.TopColor = System.Drawing.Color.Black
        Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.0625!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label1.Text = "Periodo"
        Me.Label1.Top = 0.0!
        Me.Label1.Width = 0.5!
        '
        'txtMes
        '
        Me.txtMes.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMes.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMes.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMes.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMes.Border.RightColor = System.Drawing.Color.Black
        Me.txtMes.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMes.Border.TopColor = System.Drawing.Color.Black
        Me.txtMes.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMes.DataField = "Mes"
        Me.txtMes.Height = 0.1875!
        Me.txtMes.Left = 0.5625!
        Me.txtMes.Name = "txtMes"
        Me.txtMes.Style = "text-align: center; font-weight: bold; font-size: 9pt; "
        Me.txtMes.Text = Nothing
        Me.txtMes.Top = 0.0!
        Me.txtMes.Width = 0.3125!
        '
        'txtAnio
        '
        Me.txtAnio.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAnio.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAnio.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAnio.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAnio.Border.RightColor = System.Drawing.Color.Black
        Me.txtAnio.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAnio.Border.TopColor = System.Drawing.Color.Black
        Me.txtAnio.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAnio.DataField = "Anio"
        Me.txtAnio.Height = 0.1875!
        Me.txtAnio.Left = 1.5!
        Me.txtAnio.Name = "txtAnio"
        Me.txtAnio.Style = "text-align: center; font-weight: bold; font-size: 9pt; "
        Me.txtAnio.Text = Nothing
        Me.txtAnio.Top = 0.0!
        Me.txtAnio.Width = 0.375!
        '
        'Label20
        '
        Me.Label20.Border.BottomColor = System.Drawing.Color.Black
        Me.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.LeftColor = System.Drawing.Color.Black
        Me.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.RightColor = System.Drawing.Color.Black
        Me.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Border.TopColor = System.Drawing.Color.Black
        Me.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 1.1875!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label20.Text = "A�o"
        Me.Label20.Top = 0.0!
        Me.Label20.Width = 0.3125!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.07222223!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtCodigoAgencia, Me.Label})
        Me.GroupHeader2.DataField = "Agencia"
        Me.GroupHeader2.Height = 0.2291667!
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'txtCodigoAgencia
        '
        Me.txtCodigoAgencia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCodigoAgencia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAgencia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCodigoAgencia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAgencia.Border.RightColor = System.Drawing.Color.Black
        Me.txtCodigoAgencia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAgencia.Border.TopColor = System.Drawing.Color.Black
        Me.txtCodigoAgencia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCodigoAgencia.DataField = "Agencia"
        Me.txtCodigoAgencia.Height = 0.1875!
        Me.txtCodigoAgencia.Left = 0.6875!
        Me.txtCodigoAgencia.Name = "txtCodigoAgencia"
        Me.txtCodigoAgencia.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtCodigoAgencia.Text = Nothing
        Me.txtCodigoAgencia.Top = 0.0!
        Me.txtCodigoAgencia.Width = 3.5625!
        '
        'Label
        '
        Me.Label.Border.BottomColor = System.Drawing.Color.Black
        Me.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.LeftColor = System.Drawing.Color.Black
        Me.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.RightColor = System.Drawing.Color.Black
        Me.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Border.TopColor = System.Drawing.Color.Black
        Me.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.0625!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label.Text = "Agencia"
        Me.Label.Top = 0.0!
        Me.Label.Width = 0.5625!
        '
        'txtCantidadInicial
        '
        Me.txtCantidadInicial.Border.BottomColor = System.Drawing.Color.Black
        Me.txtCantidadInicial.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidadInicial.Border.LeftColor = System.Drawing.Color.Black
        Me.txtCantidadInicial.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidadInicial.Border.RightColor = System.Drawing.Color.Black
        Me.txtCantidadInicial.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidadInicial.Border.TopColor = System.Drawing.Color.Black
        Me.txtCantidadInicial.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtCantidadInicial.DataField = "CantidadInicial"
        Me.txtCantidadInicial.Height = 0.1875!
        Me.txtCantidadInicial.Left = 4.375!
        Me.txtCantidadInicial.Name = "txtCantidadInicial"
        Me.txtCantidadInicial.Style = ""
        Me.txtCantidadInicial.Text = Nothing
        Me.txtCantidadInicial.Top = 0.0!
        Me.txtCantidadInicial.Width = 1.0!
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Height = 0.07222223!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'GroupHeader3
        '
        Me.GroupHeader3.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label2, Me.txtDesProducto, Me.Line, Me.Line1, Me.Label6, Me.Label7, Me.Label9, Me.Label14, Me.Label15, Me.Label17, Me.txtCantidadInicial})
        Me.GroupHeader3.DataField = "Producto"
        Me.GroupHeader3.Height = 0.5520833!
        Me.GroupHeader3.Name = "GroupHeader3"
        '
        'Label2
        '
        Me.Label2.Border.BottomColor = System.Drawing.Color.Black
        Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.LeftColor = System.Drawing.Color.Black
        Me.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.RightColor = System.Drawing.Color.Black
        Me.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.TopColor = System.Drawing.Color.Black
        Me.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.05208333!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label2.Text = "Producto"
        Me.Label2.Top = 0.0!
        Me.Label2.Width = 0.625!
        '
        'txtDesProducto
        '
        Me.txtDesProducto.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDesProducto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesProducto.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDesProducto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesProducto.Border.RightColor = System.Drawing.Color.Black
        Me.txtDesProducto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesProducto.Border.TopColor = System.Drawing.Color.Black
        Me.txtDesProducto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesProducto.DataField = "Producto"
        Me.txtDesProducto.Height = 0.1875!
        Me.txtDesProducto.Left = 0.8125!
        Me.txtDesProducto.Name = "txtDesProducto"
        Me.txtDesProducto.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtDesProducto.Text = Nothing
        Me.txtDesProducto.Top = 0.0!
        Me.txtDesProducto.Width = 3.4375!
        '
        'Line
        '
        Me.Line.Border.BottomColor = System.Drawing.Color.Black
        Me.Line.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.LeftColor = System.Drawing.Color.Black
        Me.Line.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.RightColor = System.Drawing.Color.Black
        Me.Line.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.TopColor = System.Drawing.Color.Black
        Me.Line.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Height = 0.0!
        Me.Line.Left = 0.0625!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 0.0!
        Me.Line.Width = 7.375!
        Me.Line.X1 = 7.4375!
        Me.Line.X2 = 0.0625!
        Me.Line.Y1 = 0.0!
        Me.Line.Y2 = 0.0!
        '
        'Line1
        '
        Me.Line1.Border.BottomColor = System.Drawing.Color.Black
        Me.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.LeftColor = System.Drawing.Color.Black
        Me.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.RightColor = System.Drawing.Color.Black
        Me.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.TopColor = System.Drawing.Color.Black
        Me.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.0625!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0.1875!
        Me.Line1.Width = 7.375!
        Me.Line1.X1 = 7.4375!
        Me.Line1.X2 = 0.0625!
        Me.Line1.Y1 = 0.1875!
        Me.Line1.Y2 = 0.1875!
        '
        'Label6
        '
        Me.Label6.Border.BottomColor = System.Drawing.Color.Black
        Me.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.LeftColor = System.Drawing.Color.Black
        Me.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.RightColor = System.Drawing.Color.Black
        Me.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.TopColor = System.Drawing.Color.Black
        Me.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 0.9375!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label6.Text = "Fecha"
        Me.Label6.Top = 0.3125!
        Me.Label6.Width = 0.5!
        '
        'Label7
        '
        Me.Label7.Border.BottomColor = System.Drawing.Color.Black
        Me.Label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.LeftColor = System.Drawing.Color.Black
        Me.Label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.RightColor = System.Drawing.Color.Black
        Me.Label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.TopColor = System.Drawing.Color.Black
        Me.Label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.125!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label7.Text = "Documento"
        Me.Label7.Top = 0.3125!
        Me.Label7.Width = 0.6875!
        '
        'Label9
        '
        Me.Label9.Border.BottomColor = System.Drawing.Color.Black
        Me.Label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.LeftColor = System.Drawing.Color.Black
        Me.Label9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.RightColor = System.Drawing.Color.Black
        Me.Label9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.TopColor = System.Drawing.Color.Black
        Me.Label9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 2.3125!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "text-align: left; font-weight: bold; font-size: 8.25pt; "
        Me.Label9.Text = "Movimiento"
        Me.Label9.Top = 0.3125!
        Me.Label9.Width = 1.0625!
        '
        'Label14
        '
        Me.Label14.Border.BottomColor = System.Drawing.Color.Black
        Me.Label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.LeftColor = System.Drawing.Color.Black
        Me.Label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.RightColor = System.Drawing.Color.Black
        Me.Label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.TopColor = System.Drawing.Color.Black
        Me.Label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 4.75!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.Label14.Text = "Entrada"
        Me.Label14.Top = 0.3125!
        Me.Label14.Width = 0.75!
        '
        'Label15
        '
        Me.Label15.Border.BottomColor = System.Drawing.Color.Black
        Me.Label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.LeftColor = System.Drawing.Color.Black
        Me.Label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.RightColor = System.Drawing.Color.Black
        Me.Label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.TopColor = System.Drawing.Color.Black
        Me.Label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 5.6875!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.Label15.Text = "Salida"
        Me.Label15.Top = 0.3125!
        Me.Label15.Width = 0.625!
        '
        'Label17
        '
        Me.Label17.Border.BottomColor = System.Drawing.Color.Black
        Me.Label17.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.LeftColor = System.Drawing.Color.Black
        Me.Label17.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.RightColor = System.Drawing.Color.Black
        Me.Label17.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Border.TopColor = System.Drawing.Color.Black
        Me.Label17.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 6.4375!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.Label17.Text = "Existencia Actual"
        Me.Label17.Top = 0.3125!
        Me.Label17.Width = 1.0!
        '
        'GroupFooter3
        '
        Me.GroupFooter3.Height = 0.08333334!
        Me.GroupFooter3.Name = "GroupFooter3"
        '
        'actrptInventKardexSimple
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.5!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.489583!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.GroupHeader2)
        Me.Sections.Add(Me.GroupHeader3)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter3)
        Me.Sections.Add(Me.GroupFooter2)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" & _
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        CType(Me.txtFechaMovimiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMovimiento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEntrada, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSalida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtExistencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAnio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCantidadInicial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDesProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptInventKardexSimple_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

    End Sub

    Private Sub actrptInventKardexSimple_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        TextBox22.Text = Format(Now, "dd-MMM-yyyy")
        TextBox23.Text = Format(Now, "hh:mm:ss tt")
        Me.Label25.Text = strFechaReporte

        Me.Document.Printer.PrinterName = ""
    End Sub


    Private Sub GroupHeader3_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader3.Format

    End Sub
End Class
