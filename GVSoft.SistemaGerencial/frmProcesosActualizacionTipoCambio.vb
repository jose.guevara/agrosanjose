﻿Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmProcesosActualizacionTipoCambio
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmProcesosActualizacionTipoCambio"

    Private Sub frmProcesosActualizacionTipoCambio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            txtTasaCambioDelDia.Text = RNTipoCambio.ObtieneTipoCambioDelDia()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tipo de cambio")
            Exit Sub
        End Try

    End Sub

    Private Sub btnActulizaPrecio_Click(sender As Object, e As EventArgs) Handles btnActulizaPrecio.Click
        Try
            If SUConversiones.ConvierteADecimal(txtTasaCambioDelDia.Text) = 0 Then
                MsgBox("No se ha ingresado la tasa de cambio del día, favor ingresar la tasa de cambio del día. ", MsgBoxStyle.Critical, "Procesos de actualización ")
                Exit Sub
            End If
            RNProduto.AcutalizaPrecioProductosCORConTasaDelDia()
            MsgBox("Acción realizada exitosamente ", MsgBoxStyle.Information, "Procesos de actualización ")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Procesos de actualización ")
            Exit Sub
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If SUConversiones.ConvierteADecimal(txtTasaCambioDelDia.Text) = 0 Then
                MsgBox("No se ha ingresado la tasa de cambio del día, favor ingresar la tasa de cambio del día. ", MsgBoxStyle.Critical, "Procesos de actualización ")
                Exit Sub
            End If
            RNCliente.AcutalizaSaldoClienteCORConTasaDelDia()
            MsgBox("Acción realizada exitosamente ", MsgBoxStyle.Information, "Procesos de actualización ")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Procesos de actualización ")
            Exit Sub
        End Try
    End Sub
End Class