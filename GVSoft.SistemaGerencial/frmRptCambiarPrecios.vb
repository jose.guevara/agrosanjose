Public Class frmRptCambiarPrecios
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem20 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ListBox10 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox9 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.MenuItem19 = New System.Windows.Forms.MenuItem
        Me.MenuItem20 = New System.Windows.Forms.MenuItem
        Me.MenuItem22 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.ListBox10 = New System.Windows.Forms.ListBox
        Me.ListBox9 = New System.Windows.Forms.ListBox
        Me.ListBox8 = New System.Windows.Forms.ListBox
        Me.ListBox7 = New System.Windows.Forms.ListBox
        Me.ListBox6 = New System.Windows.Forms.ListBox
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem15})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem19, Me.MenuItem20, Me.MenuItem22})
        Me.MenuItem1.Text = "Exportar"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Excel"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "Html"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Pdf"
        '
        'MenuItem19
        '
        Me.MenuItem19.Index = 3
        Me.MenuItem19.Text = "Rtf"
        '
        'MenuItem20
        '
        Me.MenuItem20.Index = 4
        Me.MenuItem20.Text = "Tiff"
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 5
        Me.MenuItem22.Text = "Ninguno"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem4, Me.MenuItem5})
        Me.MenuItem2.Text = "Reportes"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 0
        Me.MenuItem4.Text = "Reporte de Cambio de Precios"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 1
        Me.MenuItem5.Text = "Reporte de Correcciones en las Facturas"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 3
        Me.MenuItem15.Text = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBox2)
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ListBox10)
        Me.GroupBox1.Controls.Add(Me.ListBox9)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(624, 312)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(16, 216)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 16)
        Me.Label13.TabIndex = 43
        Me.Label13.Text = "<F5> AYUDA"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Items.AddRange(New Object() {"", "contado", "cr�dito"})
        Me.ComboBox1.Location = New System.Drawing.Point(544, 248)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(72, 21)
        Me.ComboBox1.TabIndex = 40
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(384, 248)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker2.TabIndex = 39
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(384, 192)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker1.TabIndex = 38
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(512, 248)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 13)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Tipo"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(320, 248)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "Fec Final"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(320, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 13)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Fec Inicial"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(8, 192)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(144, 20)
        Me.TextBox1.TabIndex = 34
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 176)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(288, 16)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Registro de los Art�culos"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox10
        '
        Me.ListBox10.HorizontalScrollbar = True
        Me.ListBox10.Location = New System.Drawing.Point(72, 272)
        Me.ListBox10.Name = "ListBox10"
        Me.ListBox10.Size = New System.Drawing.Size(88, 17)
        Me.ListBox10.TabIndex = 30
        Me.ListBox10.Visible = False
        '
        'ListBox9
        '
        Me.ListBox9.HorizontalScrollbar = True
        Me.ListBox9.Location = New System.Drawing.Point(160, 192)
        Me.ListBox9.Name = "ListBox9"
        Me.ListBox9.Size = New System.Drawing.Size(144, 108)
        Me.ListBox9.TabIndex = 29
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(472, 144)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(144, 17)
        Me.ListBox8.TabIndex = 28
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(320, 144)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(120, 17)
        Me.ListBox7.TabIndex = 27
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(472, 32)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(144, 108)
        Me.ListBox6.TabIndex = 26
        '
        'ListBox5
        '
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(320, 32)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(144, 108)
        Me.ListBox5.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(320, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(296, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Registro de los Usuarios"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(288, 16)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Registro de las Agencias"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(160, 144)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(144, 17)
        Me.ListBox4.TabIndex = 22
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(8, 144)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(120, 17)
        Me.ListBox3.TabIndex = 21
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(160, 32)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(144, 108)
        Me.ListBox2.TabIndex = 20
        '
        'ListBox1
        '
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(8, 32)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(144, 108)
        Me.ListBox1.TabIndex = 19
        '
        'Timer1
        '
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(490, 195)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox1.TabIndex = 44
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(490, 252)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(15, 14)
        Me.CheckBox2.TabIndex = 45
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'frmRptCambiarPrecios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(624, 313)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRptCambiarPrecios"
        Me.Text = "Reportes de Otros M�dulos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRptCambiarPrecios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        If intRptOtros = 1 Then
            MenuItem5.Visible = False
        ElseIf intRptOtros = 2 Then
            MenuItem4.Visible = False
        End If
        blnClear = False
        Iniciar()
        Limpiar()
        If intAccesos(112) = 0 Then
            MenuItem1.Visible = False
        End If

    End Sub

    Private Sub frmRptCambiarPrecios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F5 Then
            If Label13.Visible = True Then
                intListadoAyuda = 3
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        End If

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Agencias Where Estado <> 2 order by Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox1.Items.Add(dtrAgro2K.GetValue(1))
            ListBox3.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Nombre From prm_Usuarios Where Estado <> 2 order by Nombre"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox5.Items.Add(dtrAgro2K.GetValue(1))
            ListBox7.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        DateTimePicker1.Value = DateSerial(Year(Now), Month(Now), 1)
        DateTimePicker2.Value = Now
        ListBox2.Items.Clear()
        ListBox4.Items.Clear()
        ListBox6.Items.Clear()
        ListBox8.Items.Clear()
        ListBox9.Items.Clear()
        ListBox10.Items.Clear()
        ComboBox1.SelectedIndex = -1
        TextBox1.Text = ""
        CheckBox1.Checked = True
        CheckBox2.Checked = True
        Label13.Visible = False

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox9.DoubleClick, ListBox10.DoubleClick

        Select Case sender.name.ToString
            Case "ListBox1" : ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
            Case "ListBox2"
                If ListBox2.SelectedIndex >= 0 Then
                    ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                End If
            Case "ListBox5" : ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem)
            Case "ListBox6"
                If ListBox6.SelectedIndex >= 0 Then
                    ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                End If
            Case "ListBox9"
                If ListBox9.SelectedIndex >= 0 Then
                    ListBox10.Items.RemoveAt(ListBox9.SelectedIndex) : ListBox9.Items.RemoveAt(ListBox9.SelectedIndex)
                End If
        End Select

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown

        If e.KeyCode = Keys.Enter Then
            If UbicarProducto() = False Then
                MsgBox("C�digo de producto no est� registrado.   Verifique.", MsgBoxStyle.Critical, "Extracci�n de Registro")
            End If
            TextBox1.Text = ""
        End If

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus

        Label13.Visible = False
    End Sub


    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus

        Label13.Visible = True

    End Sub

    Function UbicarProducto() As Boolean

        lngRegistro = 0
        strQuery = ""
        strQuery = "Select P.Registro, P.Codigo, P.Descripcion from prm_Productos P Where P.Codigo = '" & TextBox1.Text & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            lngRegistro = dtrAgro2K.GetValue(0)
            ListBox9.Items.Add(dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2))
            ListBox10.Items.Add(lngRegistro)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If lngRegistro = 0 Then
            UbicarProducto = False
            Exit Function
        End If
        UbicarProducto = True

    End Function

    Sub GenerarQuery()

        Dim lngFecha As Long

        If ListBox4.Items.Count > 0 Then                                        'Agencias
            If intRptOtros = 1 Then
                strQuery = strQuery + "'and A.Registro in ("
            ElseIf intRptOtros = 2 Then
                strQuery = strQuery + "'and C.AgenRegistro in ("
            End If
            If ListBox4.Items.Count = 1 Then
                ListBox4.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
            ElseIf ListBox4.Items.Count > 1 Then
                For intIncr = 0 To ListBox4.Items.Count - 2
                    ListBox4.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                Next
                ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox8.Items.Count > 0 Then                                        'Usuarios
            strQuery = strQuery + "'and U.Registro in ("
            If ListBox8.Items.Count = 1 Then
                ListBox8.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
            ElseIf ListBox8.Items.Count > 1 Then
                For intIncr = 0 To ListBox8.Items.Count - 2
                    ListBox8.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ", "
                Next
                ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox10.Items.Count > 0 Then                                       'Productos
            strQuery = strQuery + "'and P.Registro in ("
            If ListBox10.Items.Count = 1 Then
                ListBox10.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox10.SelectedItem & ") ', "
            ElseIf ListBox10.Items.Count > 1 Then
                For intIncr = 0 To ListBox10.Items.Count - 2
                    ListBox10.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox10.SelectedItem & ", "
                Next
                ListBox10.SelectedIndex = ListBox10.Items.Count - 1
                strQuery = strQuery + "" & ListBox10.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ComboBox1.SelectedIndex > 0 Then                                     'Tipo de Factura
            strQuery = strQuery + "'and C.TipoFactura = " & ComboBox1.SelectedIndex - 1 & " ', "
        Else
            strQuery = strQuery + "' ', "
        End If
        lngFecha = 0
        strFechaInicial = ""
        strFechaFinal = ""
        If CheckBox1.Checked = True Then                                        'Fecha Inicial
            lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaInicial = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        Else
            If intRptOtros = 1 Then
                lngFecha = 20001101
                strFechaInicial = "01-Nov-2000"
            ElseIf intRptOtros = 2 Then
                lngFecha = 20071201
                strFechaInicial = "01-Dic-2007"
            End If
        End If
        strQuery = strQuery + "'and C.NumFechaIng >= " & lngFecha & " ', "
        If CheckBox2.Checked = True Then                                        'Fecha Final
            lngFecha = Format(DateTimePicker2.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "dd-MMM-yyyy")
        Else
            lngFecha = Format(Now, "yyyyMMdd")
            strFechaFinal = Format(Now, "dd-MMM-yyyy")
        End If
        strQuery = strQuery + "'and C.NumFechaIng <= " & lngFecha & " ' "

    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem15.Click, MenuItem16.Click, MenuItem17.Click, MenuItem18.Click, MenuItem19.Click, MenuItem20.Click, MenuItem22.Click

        Dim frmNew As New actrptViewer

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        strQuery = ""
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptExportar = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem22.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem22.Checked = True
            Case "Reporte de Cambio de Precios" : intRptOtros = 1
            Case "Reporte de Correcciones en las Facturas" : intRptOtros = 2
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptOtros > 0 Then
            strQuery = "exec sp_RptOtrosModulos " & intRptOtros & ", "
            GenerarQuery()
            strFechaReporte = "Del " & strFechaInicial & " al " & strFechaFinal & ""
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If strUbicar <> "" Then
            Timer1.Enabled = False
            TextBox1.Text = strUbicar : UbicarProducto() : ListBox9.Focus() : TextBox1.Text = ""
        End If

    End Sub

End Class
