<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportexCuenta
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportexCuenta))
        Me.ExpandablePanel2 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel3 = New DevComponents.DotNetBar.ItemPanel()
        Me.Label_ctafinal = New DevComponents.DotNetBar.LabelX()
        Me.Label_ctainicial = New DevComponents.DotNetBar.LabelX()
        Me.ItemPanel5 = New DevComponents.DotNetBar.ItemPanel()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_saldoinilocal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_saldofinlocal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_haberlocal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_debelocal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.DateF_FechaFin_Movi = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.DateF_FechaIni_Movi = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_CuentaContableMovi_fin = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextB_CuentaContableMovi_ini = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ExpandablePanel3 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel4 = New DevComponents.DotNetBar.ItemPanel()
        Me.GridP_MoviCta = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Button_limpia_ctafinal = New DevComponents.DotNetBar.ButtonX()
        Me.Button_limpia_ctainicial = New DevComponents.DotNetBar.ButtonX()
        Me.ToolbarB_BuscarMov = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_limpiartodo = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_Imprimir_MoviCta = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_Menu = New DevComponents.DotNetBar.ButtonItem()
        Me.ExpandablePanel2.SuspendLayout()
        Me.ItemPanel3.SuspendLayout()
        Me.ItemPanel5.SuspendLayout()
        CType(Me.DateF_FechaFin_Movi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateF_FechaIni_Movi, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExpandablePanel3.SuspendLayout()
        Me.ItemPanel4.SuspendLayout()
        CType(Me.GridP_MoviCta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ExpandablePanel2
        '
        Me.ExpandablePanel2.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel2.Controls.Add(Me.ItemPanel3)
        Me.ExpandablePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel2.ExpandOnTitleClick = True
        Me.ExpandablePanel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel2.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel2.Name = "ExpandablePanel2"
        Me.ExpandablePanel2.Size = New System.Drawing.Size(981, 183)
        Me.ExpandablePanel2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel2.Style.BackColor1.Color = System.Drawing.Color.SteelBlue
        Me.ExpandablePanel2.Style.BackColor2.Color = System.Drawing.Color.SteelBlue
        Me.ExpandablePanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel2.Style.GradientAngle = 90
        Me.ExpandablePanel2.TabIndex = 3
        Me.ExpandablePanel2.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel2.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel2.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel2.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.ExpandablePanel2.TitleStyle.BorderWidth = 0
        Me.ExpandablePanel2.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel2.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel2.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel2.TitleText = "Opciones de B�squedas"
        '
        'ItemPanel3
        '
        '
        '
        '
        Me.ItemPanel3.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel3.BackgroundStyle.Class = ""
        Me.ItemPanel3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel3.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel3.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel3.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel3.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel3.ContainerControlProcessDialogKey = True
        Me.ItemPanel3.Controls.Add(Me.Button_limpia_ctafinal)
        Me.ItemPanel3.Controls.Add(Me.Button_limpia_ctainicial)
        Me.ItemPanel3.Controls.Add(Me.Label_ctafinal)
        Me.ItemPanel3.Controls.Add(Me.Label_ctainicial)
        Me.ItemPanel3.Controls.Add(Me.ItemPanel5)
        Me.ItemPanel3.Controls.Add(Me.LabelX27)
        Me.ItemPanel3.Controls.Add(Me.LabelX26)
        Me.ItemPanel3.Controls.Add(Me.DateF_FechaFin_Movi)
        Me.ItemPanel3.Controls.Add(Me.DateF_FechaIni_Movi)
        Me.ItemPanel3.Controls.Add(Me.Bar1)
        Me.ItemPanel3.Controls.Add(Me.LabelX23)
        Me.ItemPanel3.Controls.Add(Me.LabelX22)
        Me.ItemPanel3.Controls.Add(Me.TextB_CuentaContableMovi_fin)
        Me.ItemPanel3.Controls.Add(Me.TextB_CuentaContableMovi_ini)
        Me.ItemPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem2})
        Me.ItemPanel3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel3.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel3.Name = "ItemPanel3"
        Me.ItemPanel3.Size = New System.Drawing.Size(981, 157)
        Me.ItemPanel3.TabIndex = 0
        Me.ItemPanel3.Text = "ItemPanel3"
        '
        'Label_ctafinal
        '
        Me.Label_ctafinal.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_ctafinal.BackgroundStyle.Class = ""
        Me.Label_ctafinal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_ctafinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ctafinal.ForeColor = System.Drawing.Color.Red
        Me.Label_ctafinal.Location = New System.Drawing.Point(377, 67)
        Me.Label_ctafinal.Name = "Label_ctafinal"
        Me.Label_ctafinal.Size = New System.Drawing.Size(308, 23)
        Me.Label_ctafinal.TabIndex = 52
        Me.Label_ctafinal.Text = "?"
        Me.Label_ctafinal.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'Label_ctainicial
        '
        Me.Label_ctainicial.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_ctainicial.BackgroundStyle.Class = ""
        Me.Label_ctainicial.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_ctainicial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ctainicial.ForeColor = System.Drawing.Color.Red
        Me.Label_ctainicial.Location = New System.Drawing.Point(15, 67)
        Me.Label_ctainicial.Name = "Label_ctainicial"
        Me.Label_ctainicial.Size = New System.Drawing.Size(333, 23)
        Me.Label_ctainicial.TabIndex = 51
        Me.Label_ctainicial.Text = "?"
        Me.Label_ctainicial.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ItemPanel5
        '
        Me.ItemPanel5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.ItemPanel5.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel5.ContainerControlProcessDialogKey = True
        Me.ItemPanel5.Controls.Add(Me.LabelX5)
        Me.ItemPanel5.Controls.Add(Me.TextB_saldoinilocal)
        Me.ItemPanel5.Controls.Add(Me.LabelX20)
        Me.ItemPanel5.Controls.Add(Me.TextB_saldofinlocal)
        Me.ItemPanel5.Controls.Add(Me.LabelX19)
        Me.ItemPanel5.Controls.Add(Me.TextB_haberlocal)
        Me.ItemPanel5.Controls.Add(Me.LabelX18)
        Me.ItemPanel5.Controls.Add(Me.TextB_debelocal)
        Me.ItemPanel5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel5.Location = New System.Drawing.Point(29, 97)
        Me.ItemPanel5.Name = "ItemPanel5"
        Me.ItemPanel5.Size = New System.Drawing.Size(918, 51)
        Me.ItemPanel5.TabIndex = 50
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.Class = ""
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(57, 2)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(132, 23)
        Me.LabelX5.TabIndex = 32
        Me.LabelX5.Text = "Saldo Inicial"
        '
        'TextB_saldoinilocal
        '
        Me.TextB_saldoinilocal.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_saldoinilocal.Border.Class = "TextBoxBorder"
        Me.TextB_saldoinilocal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_saldoinilocal.Enabled = False
        Me.TextB_saldoinilocal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_saldoinilocal.Location = New System.Drawing.Point(57, 27)
        Me.TextB_saldoinilocal.Name = "TextB_saldoinilocal"
        Me.TextB_saldoinilocal.Size = New System.Drawing.Size(154, 21)
        Me.TextB_saldoinilocal.TabIndex = 33
        Me.TextB_saldoinilocal.Text = "0.00"
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.Class = ""
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(707, 1)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(132, 23)
        Me.LabelX20.TabIndex = 23
        Me.LabelX20.Text = "Saldo Final"
        '
        'TextB_saldofinlocal
        '
        Me.TextB_saldofinlocal.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_saldofinlocal.Border.Class = "TextBoxBorder"
        Me.TextB_saldofinlocal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_saldofinlocal.Enabled = False
        Me.TextB_saldofinlocal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_saldofinlocal.Location = New System.Drawing.Point(707, 26)
        Me.TextB_saldofinlocal.Name = "TextB_saldofinlocal"
        Me.TextB_saldofinlocal.Size = New System.Drawing.Size(154, 21)
        Me.TextB_saldofinlocal.TabIndex = 31
        Me.TextB_saldofinlocal.Text = "0.00"
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.Class = ""
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(490, 2)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(78, 23)
        Me.LabelX19.TabIndex = 22
        Me.LabelX19.Text = "Cr�dito"
        '
        'TextB_haberlocal
        '
        Me.TextB_haberlocal.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_haberlocal.Border.Class = "TextBoxBorder"
        Me.TextB_haberlocal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_haberlocal.Enabled = False
        Me.TextB_haberlocal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_haberlocal.Location = New System.Drawing.Point(490, 27)
        Me.TextB_haberlocal.Name = "TextB_haberlocal"
        Me.TextB_haberlocal.Size = New System.Drawing.Size(154, 21)
        Me.TextB_haberlocal.TabIndex = 30
        Me.TextB_haberlocal.Text = "0.00"
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.Class = ""
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(269, 2)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(78, 23)
        Me.LabelX18.TabIndex = 21
        Me.LabelX18.Text = "D�bito"
        '
        'TextB_debelocal
        '
        Me.TextB_debelocal.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_debelocal.Border.Class = "TextBoxBorder"
        Me.TextB_debelocal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_debelocal.Enabled = False
        Me.TextB_debelocal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_debelocal.Location = New System.Drawing.Point(269, 27)
        Me.TextB_debelocal.Name = "TextB_debelocal"
        Me.TextB_debelocal.Size = New System.Drawing.Size(154, 21)
        Me.TextB_debelocal.TabIndex = 29
        Me.TextB_debelocal.Text = "0.00"
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.Class = ""
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(730, 65)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(65, 23)
        Me.LabelX27.TabIndex = 49
        Me.LabelX27.Text = "Fecha Hasta"
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.Class = ""
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(728, 40)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(71, 23)
        Me.LabelX26.TabIndex = 48
        Me.LabelX26.Text = "Fecha Desde"
        '
        'DateF_FechaFin_Movi
        '
        '
        '
        '
        Me.DateF_FechaFin_Movi.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaFin_Movi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_Movi.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaFin_Movi.ButtonDropDown.Visible = True
        Me.DateF_FechaFin_Movi.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaFin_Movi.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaFin_Movi.IsPopupCalendarOpen = False
        Me.DateF_FechaFin_Movi.Location = New System.Drawing.Point(806, 67)
        '
        '
        '
        Me.DateF_FechaFin_Movi.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin_Movi.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaFin_Movi.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_Movi.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaFin_Movi.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaFin_Movi.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_Movi.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaFin_Movi.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaFin_Movi.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin_Movi.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaFin_Movi.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin_Movi.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaFin_Movi.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaFin_Movi.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_Movi.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaFin_Movi.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaFin_Movi.Name = "DateF_FechaFin_Movi"
        Me.DateF_FechaFin_Movi.Size = New System.Drawing.Size(138, 21)
        Me.DateF_FechaFin_Movi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaFin_Movi.TabIndex = 47
        '
        'DateF_FechaIni_Movi
        '
        '
        '
        '
        Me.DateF_FechaIni_Movi.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaIni_Movi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_Movi.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaIni_Movi.ButtonDropDown.Visible = True
        Me.DateF_FechaIni_Movi.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaIni_Movi.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaIni_Movi.IsPopupCalendarOpen = False
        Me.DateF_FechaIni_Movi.Location = New System.Drawing.Point(806, 42)
        '
        '
        '
        Me.DateF_FechaIni_Movi.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni_Movi.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaIni_Movi.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_Movi.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaIni_Movi.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaIni_Movi.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_Movi.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaIni_Movi.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaIni_Movi.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni_Movi.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaIni_Movi.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni_Movi.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaIni_Movi.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaIni_Movi.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_Movi.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaIni_Movi.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaIni_Movi.Name = "DateF_FechaIni_Movi"
        Me.DateF_FechaIni_Movi.Size = New System.Drawing.Size(138, 21)
        Me.DateF_FechaIni_Movi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaIni_Movi.TabIndex = 46
        '
        'Bar1
        '
        Me.Bar1.AccessibleDescription = "Bar1 (Bar1)"
        Me.Bar1.AccessibleName = "Bar1"
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar1.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_BuscarMov, Me.ToolbarB_limpiartodo, Me.ButtonI_Imprimir_MoviCta, Me.ButtonI_Menu})
        Me.Bar1.Location = New System.Drawing.Point(2, 2)
        Me.Bar1.MenuBar = True
        Me.Bar1.Name = "Bar1"
        Me.Bar1.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar1.Size = New System.Drawing.Size(975, 26)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar1.TabIndex = 45
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.Class = ""
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(369, 40)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(112, 23)
        Me.LabelX23.TabIndex = 38
        Me.LabelX23.Text = "Cuenta Contable Final"
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.Class = ""
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(10, 40)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(120, 23)
        Me.LabelX22.TabIndex = 37
        Me.LabelX22.Text = "Cuenta Contable Inicial"
        '
        'TextB_CuentaContableMovi_fin
        '
        Me.TextB_CuentaContableMovi_fin.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_CuentaContableMovi_fin.Border.Class = "TextBoxBorder"
        Me.TextB_CuentaContableMovi_fin.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_CuentaContableMovi_fin.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_CuentaContableMovi_fin.Location = New System.Drawing.Point(485, 42)
        Me.TextB_CuentaContableMovi_fin.Name = "TextB_CuentaContableMovi_fin"
        Me.TextB_CuentaContableMovi_fin.Size = New System.Drawing.Size(202, 21)
        Me.TextB_CuentaContableMovi_fin.TabIndex = 2
        '
        'TextB_CuentaContableMovi_ini
        '
        Me.TextB_CuentaContableMovi_ini.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_CuentaContableMovi_ini.Border.Class = "TextBoxBorder"
        Me.TextB_CuentaContableMovi_ini.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_CuentaContableMovi_ini.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_CuentaContableMovi_ini.Location = New System.Drawing.Point(136, 42)
        Me.TextB_CuentaContableMovi_ini.Name = "TextB_CuentaContableMovi_ini"
        Me.TextB_CuentaContableMovi_ini.Size = New System.Drawing.Size(202, 21)
        Me.TextB_CuentaContableMovi_ini.TabIndex = 1
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'ExpandablePanel3
        '
        Me.ExpandablePanel3.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel3.Controls.Add(Me.ItemPanel4)
        Me.ExpandablePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel3.ExpandOnTitleClick = True
        Me.ExpandablePanel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel3.Location = New System.Drawing.Point(0, 183)
        Me.ExpandablePanel3.Name = "ExpandablePanel3"
        Me.ExpandablePanel3.Size = New System.Drawing.Size(981, 293)
        Me.ExpandablePanel3.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel3.Style.BackColor1.Color = System.Drawing.SystemColors.MenuHighlight
        Me.ExpandablePanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel3.Style.GradientAngle = 90
        Me.ExpandablePanel3.TabIndex = 4
        Me.ExpandablePanel3.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel3.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel3.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel3.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel3.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel3.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel3.TitleText = "Registros Encontrados"
        '
        'ItemPanel4
        '
        '
        '
        '
        Me.ItemPanel4.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel4.BackgroundStyle.Class = ""
        Me.ItemPanel4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel4.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel4.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel4.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel4.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel4.ContainerControlProcessDialogKey = True
        Me.ItemPanel4.Controls.Add(Me.GridP_MoviCta)
        Me.ItemPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel4.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel4.Name = "ItemPanel4"
        Me.ItemPanel4.Size = New System.Drawing.Size(981, 267)
        Me.ItemPanel4.TabIndex = 3
        Me.ItemPanel4.Text = "ItemPanel4"
        '
        'GridP_MoviCta
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_MoviCta.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.GridP_MoviCta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_MoviCta.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.GridP_MoviCta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_MoviCta.DefaultCellStyle = DataGridViewCellStyle7
        Me.GridP_MoviCta.EnableHeadersVisualStyles = False
        Me.GridP_MoviCta.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_MoviCta.Location = New System.Drawing.Point(4, 5)
        Me.GridP_MoviCta.Name = "GridP_MoviCta"
        Me.GridP_MoviCta.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_MoviCta.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.GridP_MoviCta.RowHeadersWidth = 20
        Me.GridP_MoviCta.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_MoviCta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_MoviCta.Size = New System.Drawing.Size(970, 253)
        Me.GridP_MoviCta.TabIndex = 0
        '
        'Button_limpia_ctafinal
        '
        Me.Button_limpia_ctafinal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button_limpia_ctafinal.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.Button_limpia_ctafinal.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Close
        Me.Button_limpia_ctafinal.Location = New System.Drawing.Point(687, 42)
        Me.Button_limpia_ctafinal.Name = "Button_limpia_ctafinal"
        Me.Button_limpia_ctafinal.Size = New System.Drawing.Size(22, 21)
        Me.Button_limpia_ctafinal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Button_limpia_ctafinal.TabIndex = 54
        '
        'Button_limpia_ctainicial
        '
        Me.Button_limpia_ctainicial.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button_limpia_ctainicial.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.Button_limpia_ctainicial.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Close
        Me.Button_limpia_ctainicial.Location = New System.Drawing.Point(338, 42)
        Me.Button_limpia_ctainicial.Name = "Button_limpia_ctainicial"
        Me.Button_limpia_ctainicial.Size = New System.Drawing.Size(22, 21)
        Me.Button_limpia_ctainicial.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Button_limpia_ctainicial.TabIndex = 53
        '
        'ToolbarB_BuscarMov
        '
        Me.ToolbarB_BuscarMov.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_BuscarMov.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_BuscarMov.Image = CType(resources.GetObject("ToolbarB_BuscarMov.Image"), System.Drawing.Image)
        Me.ToolbarB_BuscarMov.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_BuscarMov.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_BuscarMov.Name = "ToolbarB_BuscarMov"
        Me.ToolbarB_BuscarMov.Text = "Buscar Movimientos"
        '
        'ToolbarB_limpiartodo
        '
        Me.ToolbarB_limpiartodo.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_limpiartodo.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_limpiartodo.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ResourceForkEraser
        Me.ToolbarB_limpiartodo.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_limpiartodo.Name = "ToolbarB_limpiartodo"
        Me.ToolbarB_limpiartodo.Text = "Limpiar Controles"
        '
        'ButtonI_Imprimir_MoviCta
        '
        Me.ButtonI_Imprimir_MoviCta.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_Imprimir_MoviCta.ForeColor = System.Drawing.Color.White
        Me.ButtonI_Imprimir_MoviCta.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ButtonI_Imprimir_MoviCta.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_Imprimir_MoviCta.Name = "ButtonI_Imprimir_MoviCta"
        Me.ButtonI_Imprimir_MoviCta.Text = "Imprimir Movimientos"
        '
        'ButtonI_Menu
        '
        Me.ButtonI_Menu.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_Menu.ForeColor = System.Drawing.Color.White
        Me.ButtonI_Menu.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Back
        Me.ButtonI_Menu.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_Menu.Name = "ButtonI_Menu"
        Me.ButtonI_Menu.Text = "Men� Principal"
        '
        'frmReportexCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(981, 474)
        Me.Controls.Add(Me.ExpandablePanel3)
        Me.Controls.Add(Me.ExpandablePanel2)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmReportexCuenta"
        Me.Text = "Reporte x Cuenta"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ExpandablePanel2.ResumeLayout(False)
        Me.ItemPanel3.ResumeLayout(False)
        Me.ItemPanel5.ResumeLayout(False)
        CType(Me.DateF_FechaFin_Movi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateF_FechaIni_Movi, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExpandablePanel3.ResumeLayout(False)
        Me.ItemPanel4.ResumeLayout(False)
        CType(Me.GridP_MoviCta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ExpandablePanel2 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents ItemPanel3 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents Button_limpia_ctafinal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Button_limpia_ctainicial As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label_ctafinal As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label_ctainicial As DevComponents.DotNetBar.LabelX
    Friend WithEvents ItemPanel5 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_saldoinilocal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_saldofinlocal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_haberlocal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_debelocal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DateF_FechaFin_Movi As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents DateF_FechaIni_Movi As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_BuscarMov As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_limpiartodo As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonI_Imprimir_MoviCta As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonI_Menu As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_CuentaContableMovi_fin As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TextB_CuentaContableMovi_ini As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ExpandablePanel3 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents ItemPanel4 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents GridP_MoviCta As DevComponents.DotNetBar.Controls.DataGridViewX
End Class
