Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document 

Public Class actrptRecibosProveedorAnulados 
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actrptRecibosProveedorAnulados_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.5F
        TextBox25.Text = Format(Now, "dd-MMM-yyyy")
        TextBox26.Text = Format(Now, "hh:mm:ss tt")
        'Me.Label18.Text = strFechaReporte
        'If intRptFactura = 15 Then
        '    GroupFooter1.Visible = False
        '    GroupFooter2.Visible = False
        '    ReportFooter.Visible = False
        'End If

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actrptRecibosProveedorAnulados_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select
    End Sub

   
End Class
