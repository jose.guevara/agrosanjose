Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptUtilidades
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private Label15 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox20 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox21 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label19 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Line As DataDynamics.ActiveReports.Line = Nothing
    Private Line1 As DataDynamics.ActiveReports.Line = Nothing
    Private txtAgencia As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label2 As DataDynamics.ActiveReports.Label = Nothing
    Private txtDesAgencia As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtNumero As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtMontoCostoFactura As DataDynamics.ActiveReports.TextBox = Nothing
    Private txtFecha As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label16 As DataDynamics.ActiveReports.Label = Nothing
    Friend WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader
    Private WithEvents txtTipoRegistro As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox5 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents LblTotalTipoRecibo As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox1 As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTotalTipoRegistro As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label1 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox3 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtMontoVentaFactura As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtUtilidad As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label7 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtPrcUtilidad As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label8 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtMontoCostoFact As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalUtilidad As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalFinMontoVentaFactura As DataDynamics.ActiveReports.TextBox
    Friend WithEvents txtTotalFinMargenUtilidad As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox4 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label9 As DataDynamics.ActiveReports.Label
    Private WithEvents lblFechas As DataDynamics.ActiveReports.Label
    Private WithEvents Label10 As DataDynamics.ActiveReports.Label
    Private WithEvents txtVendedor As DataDynamics.ActiveReports.TextBox
    Private TextBox22 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptUtilidades))
        Me.Detail = New DataDynamics.ActiveReports.Detail
        Me.txtNumero = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoCostoFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtFecha = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoVentaFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtUtilidad = New DataDynamics.ActiveReports.TextBox
        Me.txtPrcUtilidad = New DataDynamics.ActiveReports.TextBox
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.Label14 = New DataDynamics.ActiveReports.Label
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.TextBox20 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox
        Me.Label19 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.Line = New DataDynamics.ActiveReports.Line
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.Label7 = New DataDynamics.ActiveReports.Label
        Me.Label8 = New DataDynamics.ActiveReports.Label
        Me.Label9 = New DataDynamics.ActiveReports.Label
        Me.lblFechas = New DataDynamics.ActiveReports.Label
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.TextBox22 = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.txtAgencia = New DataDynamics.ActiveReports.TextBox
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.txtDesAgencia = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalFinMontoVentaFactura = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalFinMargenUtilidad = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader2 = New DataDynamics.ActiveReports.GroupHeader
        Me.txtTipoRegistro = New DataDynamics.ActiveReports.TextBox
        Me.TextBox5 = New DataDynamics.ActiveReports.TextBox
        Me.GroupFooter2 = New DataDynamics.ActiveReports.GroupFooter
        Me.LblTotalTipoRecibo = New DataDynamics.ActiveReports.Label
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalTipoRegistro = New DataDynamics.ActiveReports.TextBox
        Me.txtMontoCostoFact = New DataDynamics.ActiveReports.TextBox
        Me.txtTotalUtilidad = New DataDynamics.ActiveReports.TextBox
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.txtVendedor = New DataDynamics.ActiveReports.TextBox
        CType(Me.txtNumero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoCostoFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoVentaFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUtilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPrcUtilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFechas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDesAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalFinMontoVentaFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalFinMargenUtilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTipoRegistro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblTotalTipoRecibo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalTipoRegistro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMontoCostoFact, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalUtilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0.0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtNumero, Me.txtMontoCostoFactura, Me.txtFecha, Me.txtMontoVentaFactura, Me.txtUtilidad, Me.txtPrcUtilidad, Me.TextBox4})
        Me.Detail.Height = 0.25!
        Me.Detail.Name = "Detail"
        '
        'txtNumero
        '
        Me.txtNumero.Border.BottomColor = System.Drawing.Color.Black
        Me.txtNumero.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumero.Border.LeftColor = System.Drawing.Color.Black
        Me.txtNumero.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumero.Border.RightColor = System.Drawing.Color.Black
        Me.txtNumero.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumero.Border.TopColor = System.Drawing.Color.Black
        Me.txtNumero.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtNumero.DataField = "Numero"
        Me.txtNumero.Height = 0.25!
        Me.txtNumero.Left = 0.0625!
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.OutputFormat = resources.GetString("txtNumero.OutputFormat")
        Me.txtNumero.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtNumero.Text = Nothing
        Me.txtNumero.Top = 0.0!
        Me.txtNumero.Width = 0.9375!
        '
        'txtMontoCostoFactura
        '
        Me.txtMontoCostoFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoCostoFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoCostoFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoCostoFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoCostoFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFactura.DataField = "MontoCostoFactura"
        Me.txtMontoCostoFactura.Height = 0.25!
        Me.txtMontoCostoFactura.Left = 3.375!
        Me.txtMontoCostoFactura.Name = "txtMontoCostoFactura"
        Me.txtMontoCostoFactura.OutputFormat = resources.GetString("txtMontoCostoFactura.OutputFormat")
        Me.txtMontoCostoFactura.Style = "text-align: right; font-size: 8.25pt; "
        Me.txtMontoCostoFactura.Text = Nothing
        Me.txtMontoCostoFactura.Top = 0.0!
        Me.txtMontoCostoFactura.Width = 1.0!
        '
        'txtFecha
        '
        Me.txtFecha.Border.BottomColor = System.Drawing.Color.Black
        Me.txtFecha.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFecha.Border.LeftColor = System.Drawing.Color.Black
        Me.txtFecha.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFecha.Border.RightColor = System.Drawing.Color.Black
        Me.txtFecha.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFecha.Border.TopColor = System.Drawing.Color.Black
        Me.txtFecha.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtFecha.DataField = "FechaNormal"
        Me.txtFecha.Height = 0.25!
        Me.txtFecha.Left = 1.375!
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.OutputFormat = resources.GetString("txtFecha.OutputFormat")
        Me.txtFecha.Style = "text-align: justify; font-size: 8.25pt; "
        Me.txtFecha.Text = Nothing
        Me.txtFecha.Top = 0.0!
        Me.txtFecha.Width = 0.8125!
        '
        'txtMontoVentaFactura
        '
        Me.txtMontoVentaFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoVentaFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoVentaFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoVentaFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoVentaFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoVentaFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoVentaFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoVentaFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoVentaFactura.DataField = "MontoVentaFactura"
        Me.txtMontoVentaFactura.Height = 0.25!
        Me.txtMontoVentaFactura.Left = 4.5!
        Me.txtMontoVentaFactura.Name = "txtMontoVentaFactura"
        Me.txtMontoVentaFactura.OutputFormat = resources.GetString("txtMontoVentaFactura.OutputFormat")
        Me.txtMontoVentaFactura.Style = "ddo-char-set: 1; text-align: right; font-size: 8.25pt; "
        Me.txtMontoVentaFactura.Text = Nothing
        Me.txtMontoVentaFactura.Top = 0.0!
        Me.txtMontoVentaFactura.Width = 0.9375!
        '
        'txtUtilidad
        '
        Me.txtUtilidad.Border.BottomColor = System.Drawing.Color.Black
        Me.txtUtilidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUtilidad.Border.LeftColor = System.Drawing.Color.Black
        Me.txtUtilidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUtilidad.Border.RightColor = System.Drawing.Color.Black
        Me.txtUtilidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUtilidad.Border.TopColor = System.Drawing.Color.Black
        Me.txtUtilidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtUtilidad.DataField = "MargenUtilidad"
        Me.txtUtilidad.Height = 0.25!
        Me.txtUtilidad.Left = 5.5625!
        Me.txtUtilidad.Name = "txtUtilidad"
        Me.txtUtilidad.OutputFormat = resources.GetString("txtUtilidad.OutputFormat")
        Me.txtUtilidad.Style = "ddo-char-set: 1; text-align: right; font-size: 8.25pt; "
        Me.txtUtilidad.Text = Nothing
        Me.txtUtilidad.Top = 0.0!
        Me.txtUtilidad.Width = 0.9375!
        '
        'txtPrcUtilidad
        '
        Me.txtPrcUtilidad.Border.BottomColor = System.Drawing.Color.Black
        Me.txtPrcUtilidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrcUtilidad.Border.LeftColor = System.Drawing.Color.Black
        Me.txtPrcUtilidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrcUtilidad.Border.RightColor = System.Drawing.Color.Black
        Me.txtPrcUtilidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrcUtilidad.Border.TopColor = System.Drawing.Color.Black
        Me.txtPrcUtilidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtPrcUtilidad.DataField = "PorcentajeUtilidad"
        Me.txtPrcUtilidad.Height = 0.25!
        Me.txtPrcUtilidad.Left = 6.625!
        Me.txtPrcUtilidad.Name = "txtPrcUtilidad"
        Me.txtPrcUtilidad.OutputFormat = resources.GetString("txtPrcUtilidad.OutputFormat")
        Me.txtPrcUtilidad.Style = "ddo-char-set: 1; text-align: center; font-size: 8.25pt; "
        Me.txtPrcUtilidad.Text = Nothing
        Me.txtPrcUtilidad.Top = 0.0!
        Me.txtPrcUtilidad.Width = 0.75!
        '
        'TextBox4
        '
        Me.TextBox4.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox4.DataField = "NumeroRecibo"
        Me.TextBox4.Height = 0.25!
        Me.TextBox4.Left = 2.375!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat")
        Me.TextBox4.Style = "text-align: right; font-size: 8.25pt; "
        Me.TextBox4.Text = Nothing
        Me.TextBox4.Top = 0.0!
        Me.TextBox4.Width = 0.875!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.Label14, Me.Label15, Me.TextBox20, Me.TextBox21, Me.Label19, Me.Label4, Me.Label5, Me.Label6, Me.Line, Me.Line1, Me.Label3, Me.Label7, Me.Label8, Me.Label9, Me.lblFechas})
        Me.PageHeader.Height = 1.354167!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label13
        '
        Me.Label13.Border.BottomColor = System.Drawing.Color.Black
        Me.Label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.LeftColor = System.Drawing.Color.Black
        Me.Label13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.RightColor = System.Drawing.Color.Black
        Me.Label13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Border.TopColor = System.Drawing.Color.Black
        Me.Label13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0.0625!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label13.Text = "UTILIDADES"
        Me.Label13.Top = 0.2625!
        Me.Label13.Width = 2.8125!
        '
        'Label14
        '
        Me.Label14.Border.BottomColor = System.Drawing.Color.Black
        Me.Label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.LeftColor = System.Drawing.Color.Black
        Me.Label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.RightColor = System.Drawing.Color.Black
        Me.Label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Border.TopColor = System.Drawing.Color.Black
        Me.Label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label14.Height = 0.2!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 5.15!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label14.Text = "Fecha de Impresi�n"
        Me.Label14.Top = 0.0625!
        Me.Label14.Width = 1.35!
        '
        'Label15
        '
        Me.Label15.Border.BottomColor = System.Drawing.Color.Black
        Me.Label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.LeftColor = System.Drawing.Color.Black
        Me.Label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.RightColor = System.Drawing.Color.Black
        Me.Label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Border.TopColor = System.Drawing.Color.Black
        Me.Label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 5.15!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label15.Text = "Hora de Impresi�n"
        Me.Label15.Top = 0.2625!
        Me.Label15.Width = 1.35!
        '
        'TextBox20
        '
        Me.TextBox20.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox20.Height = 0.2!
        Me.TextBox20.Left = 6.5!
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Style = "text-align: right; "
        Me.TextBox20.Text = "TextBox20"
        Me.TextBox20.Top = 0.0625!
        Me.TextBox20.Width = 0.9!
        '
        'TextBox21
        '
        Me.TextBox21.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox21.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox21.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox21.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox21.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox21.Height = 0.2!
        Me.TextBox21.Left = 6.5!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: right; "
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0.2625!
        Me.TextBox21.Width = 0.9!
        '
        'Label19
        '
        Me.Label19.Border.BottomColor = System.Drawing.Color.Black
        Me.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.LeftColor = System.Drawing.Color.Black
        Me.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.RightColor = System.Drawing.Color.Black
        Me.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Border.TopColor = System.Drawing.Color.Black
        Me.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label19.Height = 0.2!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0.0625!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label19.Text = "REPORTE GENERADO DE FACTURACION Y RECIBOS"
        Me.Label19.Top = 0.075!
        Me.Label19.Width = 3.0!
        '
        'Label4
        '
        Me.Label4.Border.BottomColor = System.Drawing.Color.Black
        Me.Label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.LeftColor = System.Drawing.Color.Black
        Me.Label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.RightColor = System.Drawing.Color.Black
        Me.Label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Border.TopColor = System.Drawing.Color.Black
        Me.Label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label4.Height = 0.3125!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.1875!
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = True
        Me.Label4.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label4.Text = "Numero Factura"
        Me.Label4.Top = 0.9375!
        Me.Label4.Width = 0.8125!
        '
        'Label5
        '
        Me.Label5.Border.BottomColor = System.Drawing.Color.Black
        Me.Label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.LeftColor = System.Drawing.Color.Black
        Me.Label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.RightColor = System.Drawing.Color.Black
        Me.Label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Border.TopColor = System.Drawing.Color.Black
        Me.Label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 3.5!
        Me.Label5.MultiLine = False
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = True
        Me.Label5.Style = "text-align: left; font-weight: bold; font-size: 8.25pt; "
        Me.Label5.Text = "Monto Costo"
        Me.Label5.Top = 1.0625!
        Me.Label5.Width = 0.875!
        '
        'Label6
        '
        Me.Label6.Border.BottomColor = System.Drawing.Color.Black
        Me.Label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.LeftColor = System.Drawing.Color.Black
        Me.Label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.RightColor = System.Drawing.Color.Black
        Me.Label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Border.TopColor = System.Drawing.Color.Black
        Me.Label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label6.Height = 0.3125!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 1.375!
        Me.Label6.Name = "Label6"
        Me.Label6.RightToLeft = True
        Me.Label6.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; vertical-align: top; "
        Me.Label6.Text = "Fecha Elab. Fact"
        Me.Label6.Top = 0.9375!
        Me.Label6.Width = 0.8125!
        '
        'Line
        '
        Me.Line.Border.BottomColor = System.Drawing.Color.Black
        Me.Line.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.LeftColor = System.Drawing.Color.Black
        Me.Line.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.RightColor = System.Drawing.Color.Black
        Me.Line.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Border.TopColor = System.Drawing.Color.Black
        Me.Line.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line.Height = 0.0!
        Me.Line.Left = 0.0625!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 0.9375!
        Me.Line.Width = 7.375!
        Me.Line.X1 = 0.0625!
        Me.Line.X2 = 7.4375!
        Me.Line.Y1 = 0.9375!
        Me.Line.Y2 = 0.9375!
        '
        'Line1
        '
        Me.Line1.Border.BottomColor = System.Drawing.Color.Black
        Me.Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.LeftColor = System.Drawing.Color.Black
        Me.Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.RightColor = System.Drawing.Color.Black
        Me.Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Border.TopColor = System.Drawing.Color.Black
        Me.Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.0625!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.25!
        Me.Line1.Width = 7.375!
        Me.Line1.X1 = 0.0625!
        Me.Line1.X2 = 7.4375!
        Me.Line1.Y1 = 1.25!
        Me.Line1.Y2 = 1.25!
        '
        'Label3
        '
        Me.Label3.Border.BottomColor = System.Drawing.Color.Black
        Me.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.LeftColor = System.Drawing.Color.Black
        Me.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.RightColor = System.Drawing.Color.Black
        Me.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Border.TopColor = System.Drawing.Color.Black
        Me.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 4.625!
        Me.Label3.MultiLine = False
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = True
        Me.Label3.Style = "text-align: left; font-weight: bold; font-size: 8.25pt; "
        Me.Label3.Text = "Monto Venta"
        Me.Label3.Top = 1.0625!
        Me.Label3.Width = 0.8125!
        '
        'Label7
        '
        Me.Label7.Border.BottomColor = System.Drawing.Color.Black
        Me.Label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.LeftColor = System.Drawing.Color.Black
        Me.Label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.RightColor = System.Drawing.Color.Black
        Me.Label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Border.TopColor = System.Drawing.Color.Black
        Me.Label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 5.5625!
        Me.Label7.MultiLine = False
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = True
        Me.Label7.Style = "text-align: left; font-weight: bold; font-size: 8.25pt; "
        Me.Label7.Text = "Monto Utilidad"
        Me.Label7.Top = 1.0625!
        Me.Label7.Width = 0.9375!
        '
        'Label8
        '
        Me.Label8.Border.BottomColor = System.Drawing.Color.Black
        Me.Label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.LeftColor = System.Drawing.Color.Black
        Me.Label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.RightColor = System.Drawing.Color.Black
        Me.Label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Border.TopColor = System.Drawing.Color.Black
        Me.Label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label8.Height = 0.3125!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 6.5625!
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = True
        Me.Label8.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; white-space: inherit; "
        Me.Label8.Text = "Porcentaje Utilidad"
        Me.Label8.Top = 0.9375!
        Me.Label8.Width = 0.8125!
        '
        'Label9
        '
        Me.Label9.Border.BottomColor = System.Drawing.Color.Black
        Me.Label9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.LeftColor = System.Drawing.Color.Black
        Me.Label9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.RightColor = System.Drawing.Color.Black
        Me.Label9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Border.TopColor = System.Drawing.Color.Black
        Me.Label9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label9.Height = 0.3125!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 2.375!
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = True
        Me.Label9.Style = "text-align: center; font-weight: bold; font-size: 8.25pt; "
        Me.Label9.Text = "Numero Recibo"
        Me.Label9.Top = 0.9375!
        Me.Label9.Width = 0.625!
        '
        'lblFechas
        '
        Me.lblFechas.Border.BottomColor = System.Drawing.Color.Black
        Me.lblFechas.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechas.Border.LeftColor = System.Drawing.Color.Black
        Me.lblFechas.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechas.Border.RightColor = System.Drawing.Color.Black
        Me.lblFechas.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechas.Border.TopColor = System.Drawing.Color.Black
        Me.lblFechas.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechas.Height = 0.2!
        Me.lblFechas.HyperLink = Nothing
        Me.lblFechas.Left = 0.0625!
        Me.lblFechas.Name = "lblFechas"
        Me.lblFechas.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.lblFechas.Text = ""
        Me.lblFechas.Top = 0.5!
        Me.lblFechas.Width = 2.8125!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label16, Me.TextBox22})
        Me.PageFooter.Height = 0.5!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label16
        '
        Me.Label16.Border.BottomColor = System.Drawing.Color.Black
        Me.Label16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.LeftColor = System.Drawing.Color.Black
        Me.Label16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.RightColor = System.Drawing.Color.Black
        Me.Label16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Border.TopColor = System.Drawing.Color.Black
        Me.Label16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 6.5625!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9.75pt; "
        Me.Label16.Text = "P�gina"
        Me.Label16.Top = 0.1875!
        Me.Label16.Width = 0.5500001!
        '
        'TextBox22
        '
        Me.TextBox22.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox22.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox22.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox22.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox22.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox22.Height = 0.2!
        Me.TextBox22.Left = 7.125!
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Style = "text-align: right; "
        Me.TextBox22.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox22.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox22.Text = "TextBox22"
        Me.TextBox22.Top = 0.1875!
        Me.TextBox22.Width = 0.3229165!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtAgencia, Me.Label2, Me.txtDesAgencia, Me.Label10, Me.txtVendedor})
        Me.GroupHeader1.DataField = "Agencia"
        Me.GroupHeader1.Height = 0.2291667!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'txtAgencia
        '
        Me.txtAgencia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtAgencia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAgencia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtAgencia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAgencia.Border.RightColor = System.Drawing.Color.Black
        Me.txtAgencia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAgencia.Border.TopColor = System.Drawing.Color.Black
        Me.txtAgencia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtAgencia.DataField = "Agencia"
        Me.txtAgencia.Height = 0.1875!
        Me.txtAgencia.Left = 0.6875!
        Me.txtAgencia.Name = "txtAgencia"
        Me.txtAgencia.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtAgencia.Text = Nothing
        Me.txtAgencia.Top = 0.0!
        Me.txtAgencia.Width = 0.5625!
        '
        'Label2
        '
        Me.Label2.Border.BottomColor = System.Drawing.Color.Black
        Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.LeftColor = System.Drawing.Color.Black
        Me.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.RightColor = System.Drawing.Color.Black
        Me.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.TopColor = System.Drawing.Color.Black
        Me.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.0625!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label2.Text = "Agencia"
        Me.Label2.Top = 0.0!
        Me.Label2.Width = 0.5625!
        '
        'txtDesAgencia
        '
        Me.txtDesAgencia.Border.BottomColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.Border.LeftColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.Border.RightColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.Border.TopColor = System.Drawing.Color.Black
        Me.txtDesAgencia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtDesAgencia.DataField = "DescripcionAgencia"
        Me.txtDesAgencia.Height = 0.1875!
        Me.txtDesAgencia.Left = 1.3125!
        Me.txtDesAgencia.Name = "txtDesAgencia"
        Me.txtDesAgencia.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtDesAgencia.Text = Nothing
        Me.txtDesAgencia.Top = 0.0!
        Me.txtDesAgencia.Width = 2.0625!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label1, Me.TextBox2, Me.TextBox3, Me.txtTotalFinMontoVentaFactura, Me.txtTotalFinMargenUtilidad})
        Me.GroupFooter1.Height = 0.4166667!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label1
        '
        Me.Label1.Border.BottomColor = System.Drawing.Color.Black
        Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.LeftColor = System.Drawing.Color.Black
        Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.RightColor = System.Drawing.Color.Black
        Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.TopColor = System.Drawing.Color.Black
        Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; "
        Me.Label1.Text = "Total"
        Me.Label1.Top = 0.0!
        Me.Label1.Width = 0.375!
        '
        'TextBox2
        '
        Me.TextBox2.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox2.DataField = "DescripcionAgencia"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 0.4375!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Style = "font-weight: bold; font-size: 9pt; "
        Me.TextBox2.Text = Nothing
        Me.TextBox2.Top = 0.0!
        Me.TextBox2.Width = 2.25!
        '
        'TextBox3
        '
        Me.TextBox3.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox3.DataField = "MontoCostoFactura"
        Me.TextBox3.Height = 0.25!
        Me.TextBox3.Left = 3.375!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat")
        Me.TextBox3.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.TextBox3.SummaryGroup = "GroupHeader1"
        Me.TextBox3.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox3.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox3.Text = Nothing
        Me.TextBox3.Top = 0.0!
        Me.TextBox3.Width = 1.0!
        '
        'txtTotalFinMontoVentaFactura
        '
        Me.txtTotalFinMontoVentaFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalFinMontoVentaFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMontoVentaFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalFinMontoVentaFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMontoVentaFactura.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalFinMontoVentaFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMontoVentaFactura.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalFinMontoVentaFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMontoVentaFactura.DataField = "MontoVentaFactura"
        Me.txtTotalFinMontoVentaFactura.Height = 0.25!
        Me.txtTotalFinMontoVentaFactura.Left = 4.5!
        Me.txtTotalFinMontoVentaFactura.Name = "txtTotalFinMontoVentaFactura"
        Me.txtTotalFinMontoVentaFactura.OutputFormat = resources.GetString("txtTotalFinMontoVentaFactura.OutputFormat")
        Me.txtTotalFinMontoVentaFactura.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalFinMontoVentaFactura.SummaryGroup = "GroupHeader1"
        Me.txtTotalFinMontoVentaFactura.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotalFinMontoVentaFactura.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotalFinMontoVentaFactura.Text = Nothing
        Me.txtTotalFinMontoVentaFactura.Top = 0.0!
        Me.txtTotalFinMontoVentaFactura.Width = 0.9375!
        '
        'txtTotalFinMargenUtilidad
        '
        Me.txtTotalFinMargenUtilidad.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalFinMargenUtilidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMargenUtilidad.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalFinMargenUtilidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMargenUtilidad.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalFinMargenUtilidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMargenUtilidad.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalFinMargenUtilidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalFinMargenUtilidad.DataField = "MargenUtilidad"
        Me.txtTotalFinMargenUtilidad.Height = 0.25!
        Me.txtTotalFinMargenUtilidad.Left = 5.5625!
        Me.txtTotalFinMargenUtilidad.Name = "txtTotalFinMargenUtilidad"
        Me.txtTotalFinMargenUtilidad.OutputFormat = resources.GetString("txtTotalFinMargenUtilidad.OutputFormat")
        Me.txtTotalFinMargenUtilidad.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalFinMargenUtilidad.SummaryGroup = "GroupHeader1"
        Me.txtTotalFinMargenUtilidad.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotalFinMargenUtilidad.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotalFinMargenUtilidad.Text = Nothing
        Me.txtTotalFinMargenUtilidad.Top = 0.0!
        Me.txtTotalFinMargenUtilidad.Width = 0.9375!
        '
        'GroupHeader2
        '
        Me.GroupHeader2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtTipoRegistro, Me.TextBox5})
        Me.GroupHeader2.DataField = "tipoFactura"
        Me.GroupHeader2.Height = 0.25!
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'txtTipoRegistro
        '
        Me.txtTipoRegistro.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTipoRegistro.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoRegistro.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTipoRegistro.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoRegistro.Border.RightColor = System.Drawing.Color.Black
        Me.txtTipoRegistro.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoRegistro.Border.TopColor = System.Drawing.Color.Black
        Me.txtTipoRegistro.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTipoRegistro.DataField = "tipoFactura"
        Me.txtTipoRegistro.Height = 0.1875!
        Me.txtTipoRegistro.Left = 0.125!
        Me.txtTipoRegistro.Name = "txtTipoRegistro"
        Me.txtTipoRegistro.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtTipoRegistro.Text = Nothing
        Me.txtTipoRegistro.Top = 0.0!
        Me.txtTipoRegistro.Width = 0.6875!
        '
        'TextBox5
        '
        Me.TextBox5.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox5.DataField = "DesTipoFactura"
        Me.TextBox5.Height = 0.1875!
        Me.TextBox5.Left = 0.9375!
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Style = "font-weight: bold; font-size: 9pt; "
        Me.TextBox5.Text = Nothing
        Me.TextBox5.Top = 0.0!
        Me.TextBox5.Width = 2.9375!
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.LblTotalTipoRecibo, Me.TextBox1, Me.txtTotalTipoRegistro, Me.txtMontoCostoFact, Me.txtTotalUtilidad})
        Me.GroupFooter2.Height = 0.2604167!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'LblTotalTipoRecibo
        '
        Me.LblTotalTipoRecibo.Border.BottomColor = System.Drawing.Color.Black
        Me.LblTotalTipoRecibo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblTotalTipoRecibo.Border.LeftColor = System.Drawing.Color.Black
        Me.LblTotalTipoRecibo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblTotalTipoRecibo.Border.RightColor = System.Drawing.Color.Black
        Me.LblTotalTipoRecibo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblTotalTipoRecibo.Border.TopColor = System.Drawing.Color.Black
        Me.LblTotalTipoRecibo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblTotalTipoRecibo.Height = 0.1875!
        Me.LblTotalTipoRecibo.HyperLink = Nothing
        Me.LblTotalTipoRecibo.Left = 0.125!
        Me.LblTotalTipoRecibo.Name = "LblTotalTipoRecibo"
        Me.LblTotalTipoRecibo.Style = "font-weight: bold; "
        Me.LblTotalTipoRecibo.Text = "Total"
        Me.LblTotalTipoRecibo.Top = 0.0!
        Me.LblTotalTipoRecibo.Width = 0.375!
        '
        'TextBox1
        '
        Me.TextBox1.Border.BottomColor = System.Drawing.Color.Black
        Me.TextBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.LeftColor = System.Drawing.Color.Black
        Me.TextBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.RightColor = System.Drawing.Color.Black
        Me.TextBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.Border.TopColor = System.Drawing.Color.Black
        Me.TextBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.TextBox1.DataField = "DesTipoFactura"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 0.5!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Style = "font-weight: bold; font-size: 9pt; "
        Me.TextBox1.Text = Nothing
        Me.TextBox1.Top = 0.0!
        Me.TextBox1.Width = 2.1875!
        '
        'txtTotalTipoRegistro
        '
        Me.txtTotalTipoRegistro.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalTipoRegistro.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalTipoRegistro.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalTipoRegistro.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalTipoRegistro.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalTipoRegistro.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalTipoRegistro.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalTipoRegistro.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalTipoRegistro.DataField = "MontoVentaFactura"
        Me.txtTotalTipoRegistro.Height = 0.25!
        Me.txtTotalTipoRegistro.Left = 4.5!
        Me.txtTotalTipoRegistro.Name = "txtTotalTipoRegistro"
        Me.txtTotalTipoRegistro.OutputFormat = resources.GetString("txtTotalTipoRegistro.OutputFormat")
        Me.txtTotalTipoRegistro.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalTipoRegistro.SummaryGroup = "GroupHeader2"
        Me.txtTotalTipoRegistro.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotalTipoRegistro.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotalTipoRegistro.Text = Nothing
        Me.txtTotalTipoRegistro.Top = 0.0!
        Me.txtTotalTipoRegistro.Width = 0.9375!
        '
        'txtMontoCostoFact
        '
        Me.txtMontoCostoFact.Border.BottomColor = System.Drawing.Color.Black
        Me.txtMontoCostoFact.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFact.Border.LeftColor = System.Drawing.Color.Black
        Me.txtMontoCostoFact.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFact.Border.RightColor = System.Drawing.Color.Black
        Me.txtMontoCostoFact.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFact.Border.TopColor = System.Drawing.Color.Black
        Me.txtMontoCostoFact.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtMontoCostoFact.DataField = "MontoCostoFactura"
        Me.txtMontoCostoFact.Height = 0.25!
        Me.txtMontoCostoFact.Left = 3.375!
        Me.txtMontoCostoFact.Name = "txtMontoCostoFact"
        Me.txtMontoCostoFact.OutputFormat = resources.GetString("txtMontoCostoFact.OutputFormat")
        Me.txtMontoCostoFact.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtMontoCostoFact.SummaryGroup = "GroupHeader2"
        Me.txtMontoCostoFact.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtMontoCostoFact.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtMontoCostoFact.Text = Nothing
        Me.txtMontoCostoFact.Top = 0.0!
        Me.txtMontoCostoFact.Width = 1.0!
        '
        'txtTotalUtilidad
        '
        Me.txtTotalUtilidad.Border.BottomColor = System.Drawing.Color.Black
        Me.txtTotalUtilidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalUtilidad.Border.LeftColor = System.Drawing.Color.Black
        Me.txtTotalUtilidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalUtilidad.Border.RightColor = System.Drawing.Color.Black
        Me.txtTotalUtilidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalUtilidad.Border.TopColor = System.Drawing.Color.Black
        Me.txtTotalUtilidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtTotalUtilidad.DataField = "MargenUtilidad"
        Me.txtTotalUtilidad.Height = 0.25!
        Me.txtTotalUtilidad.Left = 5.5625!
        Me.txtTotalUtilidad.Name = "txtTotalUtilidad"
        Me.txtTotalUtilidad.OutputFormat = resources.GetString("txtTotalUtilidad.OutputFormat")
        Me.txtTotalUtilidad.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; font-size: 8.25pt; "
        Me.txtTotalUtilidad.SummaryGroup = "GroupHeader2"
        Me.txtTotalUtilidad.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.txtTotalUtilidad.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.txtTotalUtilidad.Text = Nothing
        Me.txtTotalUtilidad.Top = 0.0!
        Me.txtTotalUtilidad.Width = 0.9375!
        '
        'Label10
        '
        Me.Label10.Border.BottomColor = System.Drawing.Color.Black
        Me.Label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.LeftColor = System.Drawing.Color.Black
        Me.Label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.RightColor = System.Drawing.Color.Black
        Me.Label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Border.TopColor = System.Drawing.Color.Black
        Me.Label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 3.6875!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold; font-size: 9pt; "
        Me.Label10.Text = "Vendedor"
        Me.Label10.Top = 0.0!
        Me.Label10.Width = 0.5625!
        '
        'txtVendedor
        '
        Me.txtVendedor.Border.BottomColor = System.Drawing.Color.Black
        Me.txtVendedor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtVendedor.Border.LeftColor = System.Drawing.Color.Black
        Me.txtVendedor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtVendedor.Border.RightColor = System.Drawing.Color.Black
        Me.txtVendedor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtVendedor.Border.TopColor = System.Drawing.Color.Black
        Me.txtVendedor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.txtVendedor.DataField = "NombreVendedor"
        Me.txtVendedor.Height = 0.1875!
        Me.txtVendedor.Left = 4.3125!
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Style = "font-weight: bold; font-size: 9pt; "
        Me.txtVendedor.Text = Nothing
        Me.txtVendedor.Top = 0.0!
        Me.txtVendedor.Width = 2.1875!
        '
        'actrptUtilidades
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.5!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.5!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.GroupHeader2)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter2)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" & _
                    "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" & _
                    "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"))
        CType(Me.txtNumero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoCostoFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoVentaFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUtilidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPrcUtilidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFechas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDesAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalFinMontoVentaFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalFinMargenUtilidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTipoRegistro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblTotalTipoRecibo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalTipoRegistro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMontoCostoFact, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalUtilidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtVendedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region
    Private Sub actrptUtilidades_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub actrptUtilidades_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        TextBox20.Text = Format(Now, "dd-MMM-yyyy")
        TextBox21.Text = Format(Now, "hh:mm:ss tt")
        lblFechas.Text = strFechaReporte
        'Me.Label17.Text = strFechaReporte

    End Sub

    Private Sub GroupFooter2_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupFooter2.Format

    End Sub

    Private Sub PageFooter_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageFooter.Format

    End Sub

    Private Sub PageHeader_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageHeader.Format

    End Sub
End Class
