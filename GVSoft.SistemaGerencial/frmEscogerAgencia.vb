Public Class frmEscogerAgencia
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(0, 8)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(296, 121)
        Me.ListBox1.TabIndex = 0
        '
        'ListBox2
        '
        Me.ListBox2.Location = New System.Drawing.Point(48, 104)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(48, 17)
        Me.ListBox2.TabIndex = 1
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(128, 104)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(48, 17)
        Me.ListBox3.TabIndex = 2
        '
        'frmEscogerAgencia
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(298, 130)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.ListBox3)
        Me.Controls.Add(Me.ListBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "frmEscogerAgencia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmEscogerAgencia"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmEscogerAgencia_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Iniciar()
        lngRegAgencia = 0

    End Sub

    Private Sub frmEscogerAgencia_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        If intAgenciaDefecto = 1 Then
            strQuery = ""
            strQuery = "Select a.Registro, a.Descripcion From prm_Agencias a, prm_UsuariosAgencias u "
            strQuery = strQuery + "Where a.registro = u.agenregistro and u.usrregistro = " & lngRegUsuario & ""
        ElseIf intAgenciaDefecto <> 1 Then
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Agencias"
        End If
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox1.Items.Add(dtrAgro2K.GetValue(1))
            ListBox2.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick

        If ListBox1.Items.Count >= 1 Then
            ListBox2.SelectedIndex = ListBox1.SelectedIndex
            ListBox3.Items.Add(ListBox2.SelectedItem)
            ListBox3.SelectedIndex = 0
            lngRegAgencia = CLng(ListBox3.SelectedItem)
            strNombreSuc = ""
            strNombreSuc = ListBox1.SelectedItem
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(Now, "yyyyMMdd") & " "
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                dblTipoCambio = dtrAgro2K.GetValue(0)
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Me.Close()
        End If

    End Sub

    Private Sub ListBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ListBox1.KeyDown

        If e.KeyCode = Keys.Enter Then
            If ListBox1.Items.Count >= 1 Then
                ListBox2.SelectedIndex = ListBox1.SelectedIndex
                ListBox3.Items.Add(ListBox2.SelectedItem)
                ListBox3.SelectedIndex = 0
                lngRegAgencia = CLng(ListBox3.SelectedItem)
                strNombreSuc = ""
                strNombreSuc = ListBox1.SelectedItem
                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If
                If cmdAgro2K.Connection.State = ConnectionState.Open Then
                    cmdAgro2K.Connection.Close()
                End If
                cnnAgro2K.Open()
                cmdAgro2K.Connection = cnnAgro2K
                strQuery = ""
                strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(Now, "yyyyMMdd") & " "
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    dblTipoCambio = dtrAgro2K.GetValue(0)
                End While
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                Me.Close()
            End If
        End If

    End Sub

End Class
