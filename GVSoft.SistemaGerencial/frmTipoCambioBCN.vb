Imports Sistemas_Gerenciales.ServicioTasaCambioBCN
Imports System.Xml
Imports System.Xml.Linq
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Imports System.Text

Public Class frmTipoCambioBCN
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnListado As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnGeneral As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCambios As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReportes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnRegistros As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrimerRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAnteriorRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSiguienteRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUltimoRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnIgual As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents gcTasaCambio As DevExpress.XtraGrid.GridControl
    Friend WithEvents gvTasaCambio As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Button1 As Button
    Friend WithEvents meMesTC As DevExpress.XtraScheduler.UI.MonthEdit
    Friend WithEvents deAnioTC As DevExpress.XtraEditors.DateEdit
    Friend WithEvents Fecha As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TipoCambio As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.deAnioTC = New DevExpress.XtraEditors.DateEdit()
        Me.meMesTC = New DevExpress.XtraScheduler.UI.MonthEdit()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.btnListado = New DevComponents.DotNetBar.ButtonItem()
        Me.btnGeneral = New DevComponents.DotNetBar.ButtonItem()
        Me.btnCambios = New DevComponents.DotNetBar.ButtonItem()
        Me.btnReportes = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.btnRegistros = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrimerRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAnteriorRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSiguienteRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUltimoRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnIgual = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.gcTasaCambio = New DevExpress.XtraGrid.GridControl()
        Me.gvTasaCambio = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Fecha = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TipoCambio = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.deAnioTC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.deAnioTC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.meMesTC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.gcTasaCambio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvTasaCambio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.deAnioTC)
        Me.GroupBox1.Controls.Add(Me.meMesTC)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(463, 75)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Tipo de Cambio"
        '
        'deAnioTC
        '
        Me.deAnioTC.EditValue = Nothing
        Me.deAnioTC.Location = New System.Drawing.Point(57, 32)
        Me.deAnioTC.Name = "deAnioTC"
        Me.deAnioTC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deAnioTC.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.deAnioTC.Properties.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista
        Me.deAnioTC.Properties.DisplayFormat.FormatString = "y"
        Me.deAnioTC.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.deAnioTC.Properties.EditFormat.FormatString = "y"
        Me.deAnioTC.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.deAnioTC.Properties.Mask.EditMask = "yyyy"
        Me.deAnioTC.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.deAnioTC.Properties.VistaCalendarInitialViewStyle = DevExpress.XtraEditors.VistaCalendarInitialViewStyle.YearsGroupView
        Me.deAnioTC.Properties.VistaCalendarViewStyle = DevExpress.XtraEditors.VistaCalendarViewStyle.YearsGroupView
        Me.deAnioTC.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[True]
        Me.deAnioTC.Size = New System.Drawing.Size(100, 20)
        Me.deAnioTC.TabIndex = 6
        '
        'meMesTC
        '
        Me.meMesTC.Location = New System.Drawing.Point(200, 32)
        Me.meMesTC.Name = "meMesTC"
        Me.meMesTC.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.meMesTC.Size = New System.Drawing.Size(100, 20)
        Me.meMesTC.TabIndex = 5
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(310, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "ObtieneTasaBCN"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(168, 32)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(8, 20)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(167, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Mes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "A�o"
        '
        'Timer1
        '
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnListado, Me.LabelItem4, Me.btnRegistros, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(465, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 61
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ok
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Remove_48x48
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnListado
        '
        Me.btnListado.AutoExpandOnClick = True
        Me.btnListado.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Edit_Yes1
        Me.btnListado.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnListado.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnListado.Name = "btnListado"
        Me.btnListado.OptionGroup = "Listado de productos"
        Me.btnListado.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnGeneral, Me.btnCambios, Me.btnReportes})
        Me.btnListado.Text = "Listados"
        Me.btnListado.Tooltip = "Listados"
        '
        'btnGeneral
        '
        Me.btnGeneral.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Book
        Me.btnGeneral.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnGeneral.Name = "btnGeneral"
        Me.btnGeneral.Text = "General"
        Me.btnGeneral.Tooltip = "General"
        '
        'btnCambios
        '
        Me.btnCambios.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Edit
        Me.btnCambios.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnCambios.Name = "btnCambios"
        Me.btnCambios.Tag = "Cambios"
        Me.btnCambios.Text = "Cambios"
        Me.btnCambios.Tooltip = "Cambios"
        '
        'btnReportes
        '
        Me.btnReportes.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.invoice_icon
        Me.btnReportes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Tag = "Reportes"
        Me.btnReportes.Text = "Reportes"
        Me.btnReportes.Tooltip = "Reportes"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'btnRegistros
        '
        Me.btnRegistros.AutoExpandOnClick = True
        Me.btnRegistros.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.import64
        Me.btnRegistros.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnRegistros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.OptionGroup = "Recorre Registros"
        Me.btnRegistros.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPrimerRegistro, Me.btnAnteriorRegistro, Me.btnSiguienteRegistro, Me.btnUltimoRegistro, Me.btnIgual})
        Me.btnRegistros.Text = "Recorre Registros"
        Me.btnRegistros.Tooltip = "Recorre Registros"
        '
        'btnPrimerRegistro
        '
        Me.btnPrimerRegistro.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.arrow_up_green
        Me.btnPrimerRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrimerRegistro.Name = "btnPrimerRegistro"
        Me.btnPrimerRegistro.Text = "Primer registro"
        Me.btnPrimerRegistro.Tooltip = "Primer registro"
        '
        'btnAnteriorRegistro
        '
        Me.btnAnteriorRegistro.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.arrow_left_green
        Me.btnAnteriorRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnAnteriorRegistro.Name = "btnAnteriorRegistro"
        Me.btnAnteriorRegistro.Text = "Anterior registro"
        Me.btnAnteriorRegistro.Tooltip = "Anterior registro"
        '
        'btnSiguienteRegistro
        '
        Me.btnSiguienteRegistro.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.arrow_right_green
        Me.btnSiguienteRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnSiguienteRegistro.Name = "btnSiguienteRegistro"
        Me.btnSiguienteRegistro.Tag = "Siguiente registro"
        Me.btnSiguienteRegistro.Text = "Siguiente registro"
        Me.btnSiguienteRegistro.Tooltip = "Siguiente registro"
        '
        'btnUltimoRegistro
        '
        Me.btnUltimoRegistro.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.arrow_down_green
        Me.btnUltimoRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnUltimoRegistro.Name = "btnUltimoRegistro"
        Me.btnUltimoRegistro.Tag = "Ultimo registro"
        Me.btnUltimoRegistro.Text = "Ultimo registro"
        Me.btnUltimoRegistro.Tooltip = "Ultimo registro"
        '
        'btnIgual
        '
        Me.btnIgual.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnIgual.Name = "btnIgual"
        Me.btnIgual.Text = "Igual"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.System___Shutdown
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.gcTasaCambio)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 161)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(463, 203)
        Me.GroupBox2.TabIndex = 62
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Detalle tasas"
        '
        'gcTasaCambio
        '
        Me.gcTasaCambio.Location = New System.Drawing.Point(13, 30)
        Me.gcTasaCambio.MainView = Me.gvTasaCambio
        Me.gcTasaCambio.Name = "gcTasaCambio"
        Me.gcTasaCambio.Size = New System.Drawing.Size(436, 165)
        Me.gcTasaCambio.TabIndex = 0
        Me.gcTasaCambio.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gvTasaCambio})
        '
        'gvTasaCambio
        '
        Me.gvTasaCambio.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.Fecha, Me.TipoCambio})
        Me.gvTasaCambio.GridControl = Me.gcTasaCambio
        Me.gvTasaCambio.Name = "gvTasaCambio"
        Me.gvTasaCambio.OptionsView.ShowAutoFilterRow = True
        Me.gvTasaCambio.OptionsView.ShowGroupPanel = False
        Me.gvTasaCambio.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.Fecha, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'Fecha
        '
        Me.Fecha.Caption = "Fecha"
        Me.Fecha.DisplayFormat.FormatString = "d"
        Me.Fecha.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.Fecha.FieldName = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.OptionsColumn.AllowEdit = False
        Me.Fecha.OptionsColumn.AllowFocus = False
        Me.Fecha.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.Fecha.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.[True]
        Me.Fecha.Visible = True
        Me.Fecha.VisibleIndex = 0
        '
        'TipoCambio
        '
        Me.TipoCambio.Caption = "Tipo Cambio"
        Me.TipoCambio.FieldName = "Valor"
        Me.TipoCambio.Name = "TipoCambio"
        Me.TipoCambio.OptionsFilter.AutoFilterCondition = DevExpress.XtraGrid.Columns.AutoFilterCondition.Contains
        Me.TipoCambio.Visible = True
        Me.TipoCambio.VisibleIndex = 1
        '
        'frmTipoCambioBCN
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(465, 395)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTipoCambioBCN"
        Me.Text = "frmTipoCambioBCN"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.deAnioTC.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.deAnioTC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.meMesTC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.gcTasaCambio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvTasaCambio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim strCambios As String
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmTipoCambioBCN"
    Private Sub frmTipoCambioBCN_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            blnClear = False
            'CreateMyContextMenu()
            Limpiar()
            intModulo = 19
            deAnioTC.EditValue = DateTime.Now
            meMesTC.EditValue = DateTime.Now.Month
            '   meMesTC.SelectedText = DateTime.Now.Month
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
    End Sub

    Public Function ObtenerTC_Dia(ByVal Fecha As Date) As Double
        Dim objServ As New Tipo_Cambio_BCNSoapClient
        Try
            Return objServ.RecuperaTC_Dia(Year(Fecha), Month(Fecha), DatePart(DateInterval.Day, Fecha))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
        End Try

    End Function

    Public Shared Function ToXmlElement(ByVal el As XElement) As XmlElement
        Try
            Dim doc As XmlDocument = New XmlDocument
            doc.Load(el.CreateReader)
            Return doc.DocumentElement
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Function

    Public Function ObtenerTC_Tabla(ByVal Fecha As Date) As DataTable
        Dim objServ As Tipo_Cambio_BCNSoapClient = Nothing
        Dim objElement As XmlElement
        Dim xmlNodLista As XmlNodeList
        Dim dt As DataTable = Nothing
        Try
            objServ = New Tipo_Cambio_BCNSoapClient
            dt = New DataTable
            ' CONSUMIMOS EL SERVICIO
            'objElement = objServ.RecuperaTC_Mes(Year(Fecha), Month(Fecha))
            'objElement = ToXmlElement(objServ.RecuperaTC_Mes(Year(Fecha), Month(Fecha)))
            objElement = objServ.RecuperaTC_Mes(Year(Fecha), Month(Fecha))
            If objElement Is Nothing Then
                MsgBox("No se pudo obtener las tasas de cambios")
                Return dt
                Exit Function
            End If
            xmlNodLista = objElement.GetElementsByTagName("Tc")

            ' AGREGAMOS LAS COLUMNAS AL DATATABLE 
            For Each Node As XmlNode In xmlNodLista.Item(0).ChildNodes
                Dim Col As New DataColumn(Node.Name, System.Type.GetType("System.String"))
                dt.Columns.Add(Col)
            Next

            ' AGREGAR LA INFORMACION AL DATATABLE 
            For IntVal As Integer = 0 To xmlNodLista.Count - 1
                Dim dr As DataRow = dt.NewRow
                For Col As Integer = 0 To dt.Columns.Count - 1
                    If Not IsDBNull(xmlNodLista.Item(IntVal).ChildNodes(Col).InnerText) Then
                        dr(Col) = xmlNodLista.Item(IntVal).ChildNodes(Col).InnerText
                    Else
                        dr(Col) = Nothing
                    End If
                Next
                dt.Rows.Add(dr)
            Next
            Return dt
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Function

    Public Function ObtenerTC_Tabla(ByVal pnAnio As Integer, ByVal pnMes As Integer) As DataTable
        Dim objServ As Tipo_Cambio_BCNSoapClient = Nothing
        Dim objElement As XmlElement
        Dim xmlNodLista As XmlNodeList
        Dim dt As DataTable = Nothing
        Try
            objServ = New Tipo_Cambio_BCNSoapClient
            dt = New DataTable
            objElement = objServ.RecuperaTC_Mes(pnAnio, pnMes)
            If objElement Is Nothing Then
                MsgBox("No se pudo obtener las tasas de cambios")
                Return dt
                Exit Function
            End If
            xmlNodLista = objElement.GetElementsByTagName("Tc")

            ' AGREGAMOS LAS COLUMNAS AL DATATABLE 
            For Each Node As XmlNode In xmlNodLista.Item(0).ChildNodes
                Dim Col As New DataColumn(Node.Name, System.Type.GetType("System.String"))
                dt.Columns.Add(Col)
            Next

            ' AGREGAR LA INFORMACION AL DATATABLE 
            For IntVal As Integer = 0 To xmlNodLista.Count - 1
                Dim dr As DataRow = dt.NewRow
                For Col As Integer = 0 To dt.Columns.Count - 1
                    If Not IsDBNull(xmlNodLista.Item(IntVal).ChildNodes(Col).InnerText) Then
                        dr(Col) = xmlNodLista.Item(IntVal).ChildNodes(Col).InnerText
                    Else
                        dr(Col) = Nothing
                    End If
                Next
                dt.Rows.Add(dr)
            Next
            Return dt
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Function

    Private Sub frmTipoCambioBCN_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Guardar()
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try

    End Sub


    'Private Sub DateTimePicker1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker1.GotFocus

    '    UltraStatusBar1.Panels.Item(0).Text = IIf(sender.name = "DateTimePicker1", "Fecha", "Tipo de Cambio")
    '    UltraStatusBar1.Panels.Item(1).Text = sender.tag
    '    If sender.name <> "DateTimePicker1" Then
    '        sender.selectall()
    '    End If

    'End Sub

    'Private Sub DateTimePicker1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DateTimePicker1.KeyDown

    '    If e.KeyCode = Keys.Enter Then
    '        Select Case sender.name.ToString
    '            Case "DateTimePicker1"  'TextBox1.Focus()
    '                'Case "TextBox1" : DateTimePicker1.Focus()
    '        End Select
    '    End If

    'End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Guardar()
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        MoverRegistro(sender.text)
        '    Case 4
        '        Me.Close()
        'End Select

    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Try
            Dim cmdTmp As New SqlCommand("sp_IngTipoCambioOficial", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter

            lngRegistro = 0
            'lngRegistro = Format(DateTimePicker1.Value, "yyyyMMdd")
            With prmTmp01
                .ParameterName = "@strFecha"
                .SqlDbType = SqlDbType.VarChar
                '.Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            End With
            With prmTmp02
                .ParameterName = "@dblCambio"
                .SqlDbType = SqlDbType.Decimal
                .Value = String.Empty
                '.Value = TextBox1.Text
            End With
            With prmTmp03
                .ParameterName = "@lngFecha"
                .SqlDbType = SqlDbType.Int
                .Value = lngRegistro
            End With
            With prmTmp04
                .ParameterName = "@intVerificar"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .CommandType = CommandType.StoredProcedure
            End With

            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K

            intResp = 0
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
                lngRegistro = TextBox2.Text
                Exit While
            End While
            dtrAgro2K.Close()
            If intResp = 7 Then
                dtrAgro2K.Close()
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
                Exit Sub
            End If
            cmdTmp.Parameters(2).Value = lngRegistro
            cmdTmp.Parameters(3).Value = 1

            Try
                cmdTmp.ExecuteNonQuery()
                If intResp = 6 Then
                    'If TextBox1.Text <> strCambios Then
                    '    Ing_Bitacora(intModulo, TextBox2.Text, "TipoCambio", strCambios, TextBox1.Text)
                    'End If
                ElseIf intResp = 0 Then
                    'Ing_Bitacora(intModulo, lngRegistro, "Fecha", "", DateTimePicker1.Value)
                End If
                MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
                MoverRegistro("Primero")
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                ' TextBox1.Text = "0.0000"
            Finally
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
            End Try
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub GuardarNuevo()
        Me.Cursor = Cursors.WaitCursor
        Dim lsFecha As String = String.Empty
        Dim lnTipoCambio As Double = 0
        Dim lnFechaNumerica As Integer = 0
        Dim lsFechaFormateadaSistema As String = String.Empty
        Try

            For i As Integer = 0 To gvTasaCambio.DataRowCount - 1
                lsFecha = String.Empty
                lsFecha = gvTasaCambio.GetRowCellValue(i, "Fecha").ToString()
                lnTipoCambio = 0
                lnTipoCambio = SUConversiones.ConvierteADouble(gvTasaCambio.GetRowCellValue(i, "Valor").ToString())
                lnFechaNumerica = SUConversiones.ConvierteAInt(ObtieneFechaNumerica(lsFecha))
                lsFechaFormateadaSistema = SUFunciones.ObtieneFechaFormateadaSistema(lnFechaNumerica.ToString)
                RNTipoCambio.IngresaTipoCambio(lsFecha, lnFechaNumerica, lnTipoCambio)
            Next i
            MsgBox("Registros ingresados correctamente", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Function ObtieneFechaNumerica(ByVal psFecha As String) As String
        Dim lsFechaNumerica As String = String.Empty
        Dim lssFechaSplit As String()
        Dim lsAnio As String = String.Empty
        Dim lsMes As String = String.Empty
        Dim lsDia As String = String.Empty
        Try
            lsFechaNumerica.Equals(String.Empty)
            lssFechaSplit = psFecha.Split(New Char() {"-"c})
            If lssFechaSplit IsNot Nothing Then
                If lssFechaSplit.Length > 0 Then
                    lsAnio = lssFechaSplit(0)
                    lsMes = lssFechaSplit(1)
                    lsDia = lssFechaSplit(2)

                    lsFechaNumerica = lsAnio & lsMes & lsDia
                End If
            End If


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Return lsFechaNumerica
    End Function
    Sub Limpiar()
        Try
            Me.Cursor = Cursors.Default
            'DateTimePicker1.Value = Now
            'TextBox1.Text = "0.0000"
            TextBox2.Text = "0"
            strCambios = ""
            'DateTimePicker1.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
    End Sub

    Sub MoverRegistro(ByVal StrDato As String)

        Dim lngFecha As Long = 0
        Try
            lngFecha = 0
            'lngFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
            Select Case StrDato
                Case "Primero"
                    strQuery = "Select Max(NumFecha) from prm_TipoCambio"
                Case "Anterior"
                    strQuery = "Select Min(NumFecha) from prm_TipoCambio Where numfecha > " & lngFecha & ""
                Case "Siguiente"
                    strQuery = "Select Max(NumFecha) from prm_TipoCambio Where numfecha < " & lngFecha & ""
                Case "Ultimo"
                    strQuery = "Select Min(NumFecha) from prm_TipoCambio"
                Case "Igual"
                    strQuery = "Select NumFecha from prm_TipoCambio Where numfecha = " & lngFecha & ""
            End Select
            Limpiar()
            UbicarRegistro(StrDato)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try


    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim lngCodigo As String = 0
        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            lngCodigo = 0
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    lngCodigo = dtrAgro2K.GetValue(0)
                End If
            End While
            dtrAgro2K.Close()
            If lngCodigo = 0 Then
                If StrDato = "Igual" Then
                    MsgBox("No existe un registro con esa fecha en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
                Else
                    MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
                End If
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                Exit Sub
            End If
            strQuery = ""
            strQuery = "exec ObtieneTasaCambioxFechaNum " & lngCodigo & ""
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                'TextBox1.Text = dtrAgro2K.GetValue(1)
                ' TextBox1.Text = dtrAgro2K.Item("tipo_cambio")
                'strCambios = dtrAgro2K.GetValue(1)
                strCambios = dtrAgro2K.Item("tipo_cambio")
                'TextBox2.Text = dtrAgro2K.GetValue(2)
                TextBox2.Text = dtrAgro2K.Item("numfecha")
                'DateTimePicker1.Value = DefinirFecha(dtrAgro2K.GetValue(2))
                'DateTimePicker1.Value = DefinirFecha(dtrAgro2K.Item("numfecha"))
                'DateTimePicker1.Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try


    End Sub



    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            If blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    Dim strFecha As String
                    strFecha = ""
                    strFecha = CDate(strUbicar)
                    'DateTimePicker1.Value = strFecha
                    MoverRegistro("Igual")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            GuardarNuevo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnGeneral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGeneral.Click
        Try
            'Dim frmNew As New frmGeneral
            'lngRegistro = TextBox2.Text
            'Timer1.Interval = 200
            'Timer1.Enabled = True
            'frmNew.ShowDialog()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Try
            Dim frmNew As New frmBitacora
            lngRegistro = TextBox2.Text
            frmNew.ShowDialog()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReportes.Click
        Try
            Dim frmNew As New actrptViewer
            intRptFactura = 30
            strQuery = "exec sp_Reportes " & intModulo & " "
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnListado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListado.Click

    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        Try
            MoverRegistro("Primero")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
        Try
            MoverRegistro("Anterior")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default


    End Sub

    Private Sub btnRegistros_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegistros.Click

    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        Try
            MoverRegistro("Siguiente")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        Try
            MoverRegistro("Ultimo")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        Try
            MoverRegistro("Igual")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dtTasa As DataTable = Nothing
        Dim lnMes As Integer = 0
        Dim lnAnio As Integer = 0
        Try
            lnAnio = SUConversiones.ConvierteAInt(Year(deAnioTC.EditValue))
            lnMes = SUConversiones.ConvierteAInt(meMesTC.EditValue)
            dtTasa = ObtenerTC_Tabla(lnAnio, lnMes)
            gcTasaCambio.DataSource = dtTasa

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Tasas De Cambio")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub
End Class
