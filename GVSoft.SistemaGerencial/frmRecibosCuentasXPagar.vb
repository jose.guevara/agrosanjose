Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Text
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar

Public Class frmRecibosCuentasXPagar

    Dim liTipoReciboCaja As Integer = 0
    Dim strRecibo As String
    Dim nUltimoNumeroRecibo As Integer
    Dim intContar As Integer
    Dim intImprimir As Integer
    Dim SaldoAnterior As Double
    Dim txtCollection As New Collection
    Dim dblMontoRecibo, dblInteresRecibo, dblMntVlrRecibo, dblPendienteRecibo As Double
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmRecibosCuentasXPagar"


    Private Sub frmRecibosCuentasXPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Top = 0
        'Me.Left = 0

        Try

            liTipoReciboCaja = intTipoReciboCaja
            Label22.Text = intTipoReciboCaja
            txtCodigoProveedor.Focus()
            Select Case liTipoReciboCaja
                Case ENTipoRecibo.RECIBO_CAJA,
                ENTipoRecibo.RECIBO_MANUAL
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                    cmdBuscar.Visible = False
                Case ENTipoRecibo.RECIBO_IMPRIMIR
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                    dgvxPagosFacturas.Columns("FechaEmis").HeaderText = "Concepto"
                    dgvxPagosFacturas.Columns("FechaEmis").Width = 220
                    dgvxPagosFacturas.Columns("FechaVenc").Visible = False
                    dgvxPagosFacturas.Columns("SaldoFact").Visible = False
                    dgvxPagosFacturas.Columns("NvoSaldo").Visible = False
                    dgvxPagosFacturas.Columns("PorAplicar").Visible = False
                    'dgvxPagosFacturas.Enabled = False
                    dgvxPagosFacturas.Columns("PorcDescuentoFact").ReadOnly = True
                    dgvxPagosFacturas.Columns("MontoDescuentoFact").ReadOnly = True
                    dgvxPagosFacturas.Columns("Monto").ReadOnly = True
                    dgvxPagosFacturas.Columns("Interes").ReadOnly = True
                    dgvxPagosFacturas.Columns("MantValor").ReadOnly = True
                    dgvxPagosFacturas.Columns("PorAplicar").ReadOnly = True
                Case ENTipoRecibo.RECIBO_ANULAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
                    dgvxPagosFacturas.Columns("FechaEmis").HeaderText = "Concepto"
                    dgvxPagosFacturas.Columns("FechaEmis").Width = 220
                    dgvxPagosFacturas.Columns("FechaVenc").Visible = False
                    dgvxPagosFacturas.Columns("SaldoFact").Visible = False
                    dgvxPagosFacturas.Columns("NvoSaldo").Visible = False
                    dgvxPagosFacturas.Columns("PorAplicar").Visible = False
                    'dgvxPagosFacturas.Enabled = False
                    dgvxPagosFacturas.Columns("PorcDescuentoFact").ReadOnly = True
                    dgvxPagosFacturas.Columns("MontoDescuentoFact").ReadOnly = True
                    dgvxPagosFacturas.Columns("Monto").ReadOnly = True
                    dgvxPagosFacturas.Columns("Interes").ReadOnly = True
                    dgvxPagosFacturas.Columns("MantValor").ReadOnly = True
                    dgvxPagosFacturas.Columns("PorAplicar").ReadOnly = True
            End Select

            DTPFechaRecibo.Value = Now
            txtCollection.Add(txtCodigoProveedor)
            txtCollection.Add(TextBox2)
            txtCollection.Add(TextBox3)
            txtCollection.Add(TextBox4)
            txtCollection.Add(TextBox5)
            txtCollection.Add(TextBox6)
            txtCollection.Add(TextBox7)
            txtCollection.Add(TextBox8)
            txtCollection.Add(txtNumeroRecibo)
            txtCollection.Add(txtMonto)
            txtCollection.Add(txtNumeroTarjeta)
            txtCollection.Add(txtDescripcion)
            txtCollection.Add(txtNumeroRecibido)
            Limpiar()

            dblMontoRecibo = 0
            dblInteresRecibo = 0
            dblMntVlrRecibo = 0
            intImprimir = 0
            'For intIncr = 0 To 9
            For intIncr = 0 To 11
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            For intIncr = 0 To 7
                arrRecibo(intIncr, 0) = ""
                arrRecibo(intIncr, 1) = ""
                arrRecibo(intIncr, 2) = ""
                arrRecibo(intIncr, 3) = ""
                arrRecibo(intIncr, 4) = ""
            Next intIncr
            intContar = 0
            If intTipoReciboCaja = 1 Then
                ''txtNumeroRecibo.ReadOnly = True
            End If
            If intTipoReciboCaja = 9 Then
                'Me.ToolBar1.Buttons.Item(0).Enabled = False
                cmdImprimir.Visible = True
            End If
            Label22.Text = intTipoReciboCaja
            UbicarAgencia(lngRegUsuario)
            If (lngRegAgencia = 0) Then
                lngRegAgencia = 0
                Dim frmNew As New frmEscogerAgencia
                frmNew.ShowDialog()
                If lngRegAgencia = 0 Then
                    MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
                End If
            End If
            ObtieneSeries()

            txtCodigoProveedor.Focus()
            txtCodigoProveedor.Select()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub frmRecibosCuentasXPagar_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try


            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
                    If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                    Else
                        MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                        Exit Sub
                    End If
                    ''Anular()
                ElseIf intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                    ''Guardar()
                End If
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If Label17.Visible = True Then
                    intListadoAyuda = 7
                    Dim frmNew As New frmListadoAyuda
                    'Timer1.Interval = 200
                    'Timer1.Enabled = True
                    frmNew.ShowDialog()
                End If
            ElseIf e.KeyCode = Keys.F6 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                If intTipoReciboCaja <> 1 And intTipoReciboCaja <> 15 Then
                    Extraer()
                End If
            ElseIf e.KeyCode = Keys.F7 Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                    ''VistaPrevia()
                End If
            ElseIf e.KeyCode = Keys.F8 Then
                If intTipoReciboCaja = 9 Then
                    'Me.ToolBarButton5.Visible = True
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    ''ImprimirRecibo()
                End If
            End If

            ' Select Case intTipoReciboCaja
            Select Case liTipoReciboCaja
                Case ENTipoRecibo.RECIBO_CAJA,
                ENTipoRecibo.RECIBO_MANUAL
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                    cmdBuscar.Visible = False
                Case ENTipoRecibo.RECIBO_IMPRIMIR
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    cmdBuscar.Visible = True
                Case ENTipoRecibo.RECIBO_ANULAR
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
            End Select
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Guardar()

        Try


            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                If txtNumeroRecibo.Text <> "" Then
                    UbicarRecibo()
                End If
            End If
            If TextBox14.Text = "0" Then
                TextBox14.Focus()
                MsgBox("Tiene que digitar el codigo del Proveedor.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If txtNumeroRecibo.Text = "" Then
                txtNumeroRecibo.Focus()
                MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If txtMonto.Text = "" Then
                txtMonto.Focus()
                MsgBox("Tiene que digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If txtMonto.Text = "0.00" Then
                txtMonto.Focus()
                MsgBox("Tiene que digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If IsNumeric(txtMonto.Text) = False Then
                txtMonto.Focus()
                MsgBox("El monto del recibo debe ser num�rico.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If SUConversiones.ConvierteADouble(txtMonto.Text) <= 0 Then
                txtMonto.Focus()
                MsgBox("El monto del recibo debe ser mayor que 0.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                MsgBox("La sumatoria de los pagos no pueden ser mayor que el monto del Recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If SUConversiones.ConvierteADouble(Label15.Text) > 0 Then
                MsgBox("No puede haber Saldo Disponible para un cliente.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If intTipoReciboCaja = 2 And SUConversiones.ConvierteADouble(Label15.Text) > 0 Then
                MsgBox("No puede haber Saldo Disponible para un cliente cuando est� elaborando una Nota de Credito.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            If IsNumeric(txtDescripcionRecibo.Text) = False Then
                txtDescripcionRecibo.Focus()
                txtDescripcionRecibo.Text = "0.00"
            End If

            intContar = 0
            If SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text) > 0 Then
                intContar = intContar + 1
            End If

            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                If dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value <> "0.00" Then
                    intContar = intContar + 1
                Else
                    'MSFlexGrid2.Col = 6
                    If dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value <> "0.00" Then
                        intContar = intContar + 1
                    Else
                        'MSFlexGrid2.Col = 7
                        If dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value <> "0.00" Then
                            intContar = intContar + 1
                        End If
                    End If
                End If
            Next intIncr
            If intContar > 8 Then
                MsgBox("No puede gestionar en un solo recibo m�s de 8 pagos. Favor de elaborar 2 o m�s recibos.", MsgBoxStyle.Exclamation, "Exceso de Datos")
                Exit Sub
            End If
            txtBanco.Text = UCase(txtBanco.Text)
            Dim cmdTmp As New SqlCommand("spIngresarReciboProveedor", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim prmTmp07 As New SqlParameter
            Dim prmTmp08 As New SqlParameter
            Dim prmTmp09 As New SqlParameter
            Dim prmTmp10 As New SqlParameter
            Dim prmTmp11 As New SqlParameter
            Dim prmTmp12 As New SqlParameter
            Dim prmTmp13 As New SqlParameter
            Dim prmTmp14 As New SqlParameter
            Dim prmTmp15 As New SqlParameter
            Dim prmTmp16 As New SqlParameter
            Dim prmTmp17 As New SqlParameter
            Dim prmTmp18 As New SqlParameter
            Dim prmTmp19 As New SqlParameter
            Dim prmTmp20 As New SqlParameter
            Dim prmTmp21 As New SqlParameter
            Dim prmTmp22 As New SqlParameter

            Me.Cursor = Cursors.WaitCursor
            lngRegistro = 0
            With prmTmp01
                .ParameterName = "@lngRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegistro
            End With
            With prmTmp02
                .ParameterName = "@strFechaFactura"
                .SqlDbType = SqlDbType.VarChar
                .Value = Format(DTPFechaRecibo.Value, "dd-MMM-yyyy")
            End With
            With prmTmp03
                .ParameterName = "@lngNumFecha"
                .SqlDbType = SqlDbType.Int
                .Value = Format(DTPFechaRecibo.Value, "yyyyMMdd")
            End With
            With prmTmp04
                .ParameterName = "@lngIdProveedor"
                .SqlDbType = SqlDbType.SmallInt
                .Value = SUConversiones.ConvierteAInt(TextBox14.Text)
            End With
            With prmTmp05
                .ParameterName = "@lngVendedor"
                .SqlDbType = SqlDbType.TinyInt
                .Value = SUConversiones.ConvierteAInt(TextBox15.Text)
            End With
            With prmTmp06
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                '.Value = UCase(txtNumeroRecibo.Text)
                'If ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                '    .Value = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                '    strNumRec = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                'Else
                .Value = UCase(txtNumeroRecibo.Text)
                strNumRec = UCase(txtNumeroRecibo.Text)
                'End If

            End With
            With prmTmp07
                .ParameterName = "@dblMonto"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtMonto.Text)
            End With
            With prmTmp08
                .ParameterName = "@dblInteres"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(TextBox16.Text)
            End With
            With prmTmp09
                .ParameterName = "@dblRetencion"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text)
            End With
            With prmTmp10
                .ParameterName = "@intFormaPago"
                .SqlDbType = SqlDbType.TinyInt
                .Value = ddlFormaPago.SelectedIndex
            End With
            With prmTmp11
                .ParameterName = "@strNumChqTarj"
                .SqlDbType = SqlDbType.VarChar
                .Value = UCase(txtNumeroTarjeta.Text)
            End With
            With prmTmp12
                .ParameterName = "@strBanco"
                .SqlDbType = SqlDbType.VarChar
                .Value = txtBanco.Text
            End With
            With prmTmp13
                .ParameterName = "@strRecProv"
                .SqlDbType = SqlDbType.VarChar
                .Value = UCase(txtDescripcion.Text)
            End With
            With prmTmp14
                .ParameterName = "@strDescrip"
                .SqlDbType = SqlDbType.Text
                .Value = UCase(txtNumeroRecibido.Text)
            End With
            With prmTmp15
                .ParameterName = "@lngUsuario"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegUsuario
            End With

            With prmTmp16
                .ParameterName = "@intTipoRecibo"
                .SqlDbType = SqlDbType.TinyInt
                .Value = intTipoReciboCaja
                'If intTipoReciboCaja = 15 Then
                '    .Value = 1
                'Else
                '    .Value = intTipoReciboCaja
                'End If

            End With
            With prmTmp17
                .ParameterName = "@FechaIng"
                .SqlDbType = SqlDbType.SmallDateTime
                .Value = DTPFechaRecibo.Value
            End With
            With prmTmp18
                .ParameterName = "@dblSaldoFavor"
                .SqlDbType = SqlDbType.Decimal
                If SUConversiones.ConvierteADouble(Label15.Text) <> 0 Then
                    .Value = SUConversiones.ConvierteAInt(Label15.Text)
                Else
                    .Value = 0
                End If
            End With
            With prmTmp19
                .ParameterName = "@strDescrip01"
                .SqlDbType = SqlDbType.Text
                .Value = txtBanco.Text '""
            End With
            With prmTmp20
                .ParameterName = "@strDescrip02"
                .SqlDbType = SqlDbType.Text
                .Value = txtNumeroTarjeta.Text '""
            End With
            With prmTmp21
                .ParameterName = "@strDescrip03"
                .SqlDbType = SqlDbType.Text
                .Value = txtRefElectronica.Text '""
            End With
            With prmTmp22
                .ParameterName = "@nregistroAgencia"
                .SqlDbType = SqlDbType.Int
                .Value = lngRegAgencia
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .Parameters.Add(prmTmp07)
                .Parameters.Add(prmTmp08)
                .Parameters.Add(prmTmp09)
                .Parameters.Add(prmTmp10)
                .Parameters.Add(prmTmp11)
                .Parameters.Add(prmTmp12)
                .Parameters.Add(prmTmp13)
                .Parameters.Add(prmTmp14)
                .Parameters.Add(prmTmp15)
                .Parameters.Add(prmTmp16)
                .Parameters.Add(prmTmp17)
                .Parameters.Add(prmTmp18)
                .Parameters.Add(prmTmp19)
                .Parameters.Add(prmTmp20)
                .Parameters.Add(prmTmp21)
                .Parameters.Add(prmTmp22)
                .CommandType = CommandType.StoredProcedure
            End With
            SUFunciones.CierraComando(cmdTmp)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cmdTmp.Connection.State = ConnectionState.Open Then
            '    cmdTmp.Connection.Close()
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            cmdTmp.ExecuteNonQuery()
            GuardarDetalle()
            If SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text) <> 0 Then
                arrRecibo(intContar, 0) = "Retenci�n del Recibo"
                arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "D"
                intContar = intContar + 1
                GuardarRetencion()
            End If
            If dblPendienteRecibo <> 0 Then
                arrRecibo(intContar, 0) = "Saldo Pendiente"
                arrRecibo(intContar, 1) = Format(dblPendienteRecibo, "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "C"
                GuardarSaldoPendiente()
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            intResp = MsgBox("�Desea imprimir el recibo en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Recibo")
            If intResp = 6 Then     'Cancelar
                ImprimirRecibo()
            End If
            Limpiar()

            SUFunciones.CierraComando(cmdTmp)
            SUFunciones.CierraConexionBD(cnnAgro2K)
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        Finally
            '  SUFunciones.CierraComando(cmdTmp)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'cmdTmp.Connection.Close()
            'cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub GuardarDetalle()

        Try


            Dim cmdTmp As New SqlCommand("spIngresarDetRecibosProveedor", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim prmTmp07 As New SqlParameter
            Dim prmTmp08 As New SqlParameter
            Dim prmTmp09 As New SqlParameter
            Dim prmTmp10 As New SqlParameter
            Dim prmTmp11 As New SqlParameter
            Dim prmTmp12 As New SqlParameter
            Dim dblAbono, dblInteres, dblMantVlr, dblPendiente As Double

            strRecibo = ""
            With prmTmp01
                .ParameterName = "@lngRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegistro
            End With
            With prmTmp02
                .ParameterName = "@strFactura"
                .SqlDbType = SqlDbType.VarChar
                .Value = 0
            End With
            With prmTmp03
                .ParameterName = "@strDescrip"
                .SqlDbType = SqlDbType.Text
                .Value = txtNumeroRecibido.Text
            End With
            With prmTmp04
                .ParameterName = "@dblAbono"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp05
                .ParameterName = "@IdProveedor"
                .SqlDbType = SqlDbType.SmallInt
                .Value = CLng(TextBox14.Text)
            End With
            With prmTmp06
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                '.Value = UCase(txtNumeroRecibo.Text)
                'If ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                '    .Value = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                'Else
                .Value = UCase(txtNumeroRecibo.Text)
                'End If
            End With
            With prmTmp07
                .ParameterName = "@dblInteres"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp08
                .ParameterName = "@dblMantVlr"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp09
                .ParameterName = "@dblPendiente"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp10
                .ParameterName = "@intTipoRecibo"
                .SqlDbType = SqlDbType.TinyInt
                .Value = intTipoReciboCaja
            End With
            With prmTmp11
                .ParameterName = "@PorcDescuento"
                .SqlDbType = SqlDbType.Float
                .Value = 0
            End With
            With prmTmp12
                .ParameterName = "@MontoDesc"
                .SqlDbType = SqlDbType.Float
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .Parameters.Add(prmTmp07)
                .Parameters.Add(prmTmp08)
                .Parameters.Add(prmTmp09)
                .Parameters.Add(prmTmp10)
                .Parameters.Add(prmTmp11)
                .Parameters.Add(prmTmp12)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            cmdTmp.Parameters(9).Value = intTipoReciboCaja
            intContar = 0
            dblPendienteRecibo = 0
            'MSFlexGrid2.Col = 1
            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                'MSFlexGrid2.Row = intIncr
                dblAbono = 0
                dblInteres = 0
                dblMantVlr = 0
                dblPendiente = 0
                'MSFlexGrid2.Col = 4
                dblAbono = SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                'MSFlexGrid2.Col = 6
                dblInteres = SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
                'MSFlexGrid2.Col = 7
                dblMantVlr = SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
                'MSFlexGrid2.Col = 8
                dblPendiente = SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("PorAplicar").Value)
                If dblAbono <> 0 Or dblInteres <> 0 Or dblMantVlr <> 0 Or dblPendiente <> 0 Then
                    'MSFlexGrid2.Col = 0
                    cmdTmp.Parameters(1).Value = dgvxPagosFacturas.Rows(intIncr).Cells("NumFactura").Value
                    'MSFlexGrid2.Col = 4
                    cmdTmp.Parameters(3).Value = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value), "###0.#0")
                    'MSFlexGrid2.Col = 5
                    If dgvxPagosFacturas.Rows(intIncr).Cells("NvoSaldo").Value = "0.00" Then
                        'MSFlexGrid2.Col = 0
                        strRecibo = "Cancelaci�n de la Factura " & dgvxPagosFacturas.Rows(intIncr).Cells("NumFactura").Value
                    Else
                        'MSFlexGrid2.Col = 0
                        strRecibo = "Abono a la Factura " & dgvxPagosFacturas.Rows(intIncr).Cells("NumFactura").Value
                    End If
                    cmdTmp.Parameters(2).Value = strRecibo
                    'MSFlexGrid2.Col = 6
                    cmdTmp.Parameters(6).Value = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value), "###0.#0")
                    'MSFlexGrid2.Col = 7
                    cmdTmp.Parameters(7).Value = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value), "###0.#0")
                    'MSFlexGrid2.Col = 8
                    cmdTmp.Parameters(8).Value = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("PorAplicar").Value), "###0.#0")
                    cmdTmp.Parameters(10).Value = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("PorcDescuentoFact").Value), "###0.#0")
                    cmdTmp.Parameters(11).Value = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("MontoDescuentoFact").Value), "###0.#0")

                    dblPendienteRecibo = dblPendienteRecibo + SUConversiones.ConvierteADouble(cmdTmp.Parameters(8).Value)
                    arrRecibo(intContar, 0) = strRecibo
                    arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADouble(cmdTmp.Parameters(3).Value), "#,##0.#0")
                    arrRecibo(intContar, 2) = Format(SUConversiones.ConvierteADouble(cmdTmp.Parameters(6).Value), "#,##0.#0")
                    arrRecibo(intContar, 3) = Format(SUConversiones.ConvierteADouble(cmdTmp.Parameters(7).Value), "#,##0.#0")
                    arrRecibo(intContar, 4) = "C"
                    intContar = intContar + 1
                    cmdTmp.ExecuteNonQuery()
                    'Try
                    '    cmdTmp.ExecuteNonQuery()
                    'Catch exc As Exception
                    '    MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                    '    Exit Sub
                    'End Try
                End If
            Next intIncr
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

        'Try
        'cmdTmp.ExecuteNonQuery()
        'Catch exc As Exception
        'MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        'Exit Sub
        'End Try

    End Sub

    Sub GuardarRetencion()

        Try


            Dim cmdTmp As New SqlCommand("spIngresarDetRecibosProveedor", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim prmTmp07 As New SqlParameter
            Dim prmTmp08 As New SqlParameter
            Dim prmTmp09 As New SqlParameter
            Dim prmTmp10 As New SqlParameter

            With prmTmp01
                .ParameterName = "@lngRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegistro
            End With
            With prmTmp02
                .ParameterName = "@strFactura"
                .SqlDbType = SqlDbType.VarChar
                .Value = 0
            End With
            With prmTmp03
                .ParameterName = "@strDescrip"
                .SqlDbType = SqlDbType.VarChar
                .Value = "Retenci�n del Recibo"
            End With
            With prmTmp04
                .ParameterName = "@dblAbono"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text)
            End With
            With prmTmp05
                .ParameterName = "@IdProveedor"
                .SqlDbType = SqlDbType.SmallInt
                .Value = CLng(TextBox14.Text)
            End With
            With prmTmp06
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                .Value = UCase(txtNumeroRecibo.Text)
            End With
            With prmTmp07
                .ParameterName = "@dblInteres"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp08
                .ParameterName = "@dblMantVlr"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp09
                .ParameterName = "@dblPendiente"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp10
                .ParameterName = "@intTipoRecibo"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .Parameters.Add(prmTmp07)
                .Parameters.Add(prmTmp08)
                .Parameters.Add(prmTmp09)
                .Parameters.Add(prmTmp10)
                .CommandType = CommandType.StoredProcedure
            End With
            SUFunciones.CierraComando(cmdTmp)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cmdTmp.Connection.State = ConnectionState.Open Then
            '    cmdTmp.Connection.Close()
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K

            cmdTmp.ExecuteNonQuery()

            Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub GuardarSaldoPendiente()
        Try

            Dim cmdTmp As New SqlCommand("spIngresarDetRecibosProveedor", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim prmTmp07 As New SqlParameter
            Dim prmTmp08 As New SqlParameter
            Dim prmTmp09 As New SqlParameter
            Dim prmTmp10 As New SqlParameter

            With prmTmp01
                .ParameterName = "@lngRegistro"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegistro
            End With
            With prmTmp02
                .ParameterName = "@strFactura"
                .SqlDbType = SqlDbType.VarChar
                .Value = 0
            End With
            With prmTmp03
                .ParameterName = "@strDescrip"
                .SqlDbType = SqlDbType.VarChar
                .Value = "Saldo Pendiente"
            End With
            With prmTmp04
                .ParameterName = "@dblAbono"
                .SqlDbType = SqlDbType.Decimal
                .Value = dblPendienteRecibo
            End With
            With prmTmp05
                .ParameterName = "@IdProveedor"
                .SqlDbType = SqlDbType.SmallInt
                .Value = CLng(TextBox14.Text)
            End With
            With prmTmp06
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                .Value = UCase(txtNumeroRecibo.Text)
            End With
            With prmTmp07
                .ParameterName = "@dblInteres"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp08
                .ParameterName = "@dblMantVlr"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp09
                .ParameterName = "@dblPendiente"
                .SqlDbType = SqlDbType.Decimal
                .Value = 0
            End With
            With prmTmp10
                .ParameterName = "@intTipoRecibo"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .Parameters.Add(prmTmp07)
                .Parameters.Add(prmTmp08)
                .Parameters.Add(prmTmp09)
                .Parameters.Add(prmTmp10)
                .CommandType = CommandType.StoredProcedure
            End With
            SUFunciones.CierraComando(cmdTmp)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cmdTmp.Connection.State = ConnectionState.Open Then
            '    cmdTmp.Connection.Close()
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            cmdTmp.ExecuteNonQuery()
            'Try
            '    cmdTmp.ExecuteNonQuery()
            'Catch exc As Exception
            '    sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            '    objError.Clase = sNombreClase
            '    objError.Metodo = sNombreMetodo
            '    objError.descripcion = exc.Message.ToString()
            '    SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            '    SNError.IngresaError(objError)

            '    MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            '    Exit Sub
            'End Try
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Sub Anular()

        Try

            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            Me.Cursor = Cursors.WaitCursor
            Dim cmdTmp As New SqlCommand("spAnularRecibosProveedor", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim lngNumFecha As Long

            lngNumFecha = 0
            lngNumFecha = Format(DTPFechaRecibo.Value, "yyyyMMdd")
            With prmTmp01
                .ParameterName = "@IdProveedor"
                .SqlDbType = SqlDbType.SmallInt
                .Value = SUConversiones.ConvierteAInt(TextBox14.Text)
            End With
            With prmTmp02
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                'If ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                '    .Value = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                'Else
                .Value = UCase(txtNumeroRecibo.Text)
                'End If

            End With
            With prmTmp03
                .ParameterName = "@lngNumFecha"
                .SqlDbType = SqlDbType.Int
                .Value = lngNumFecha
            End With
            With prmTmp04
                .ParameterName = "@dblMontoRec"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtMonto.Text)
            End With
            With prmTmp05
                .ParameterName = "@dblSaldoFavor"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(Label15.Text)
            End With
            With prmTmp06
                .ParameterName = "@intTipoRecibo"
                .SqlDbType = SqlDbType.TinyInt
                .Value = intTipoReciboCaja
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .CommandType = CommandType.StoredProcedure
            End With
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdTmp)
            'If cmdTmp.Connection.State = ConnectionState.Open Then
            '    cmdTmp.Connection.Close()
            'End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                cmdTmp.ExecuteNonQuery()
                MsgBox("Recibo Anulado", MsgBoxStyle.Information, "Anulaci�n Satisfactoria")
                Limpiar()
            Catch exc As Exception
                Me.Cursor = Cursors.Default
                sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
                objError.Clase = sNombreClase
                objError.Metodo = sNombreMetodo
                objError.descripcion = exc.Message.ToString()
                SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
                SNError.IngresaError(objError)
                'Throw exc
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Finally
                SUFunciones.CierraComando(cmdAgro2K)
                SUFunciones.CierraConexionBD(cnnAgro2K)
                'cmdAgro2K.Connection.Close()
                'cnnAgro2K.Close()
            End Try
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Sub ImprimirRecibo()
        Try


            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            intRptPresup = 0
            intRptTipoPrecio = 0
            For intIncr = 0 To 10
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            strQuery = ""
            strQuery = "exec ObtieneDatosGeneralesProveedor " & SUConversiones.ConvierteAInt(TextBox14.Text) & ""
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            'arrRecibosExtra(0) = Format(DTPFechaRecibo.Value, "dd-MMM-yyyy")
            arrRecibosExtra(0) = Format(DTPFechaRecibo.Value, "dd-MMM")
            'objEncabezadoRecibo.FechaRecibo = Format(DTPFechaRecibo.Value, "dd-MMM")
            While dtrAgro2K.Read
                'objEncabezadoRecibo.Nombre = dtrAgro2K.GetValue(0)
                'objEncabezadoRecibo.Codigo = dtrAgro2K.GetValue(1)
                'objEncabezadoRecibo.CuentaContable = dtrAgro2K.GetValue(2)
                arrRecibosExtra(1) = dtrAgro2K.GetValue(0)
                arrRecibosExtra(2) = dtrAgro2K.GetValue(1)
                arrRecibosExtra(4) = dtrAgro2K.GetValue(2)
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            ''objDetalleRecibo.MontoRecibo = Format(SUConversiones.ConvierteADouble(txtMonto.Text), "#,##0.#0")
            ''objEncabezadoRecibo.Monto = Format(SUConversiones.ConvierteADouble(txtMonto.Text), "#,##0.#0")
            ''objEncabezadoRecibo.NumeroTarjeta = txtNumeroTarjeta.Text
            ''objEncabezadoRecibo.Banco = txtBanco.Text

            ''objEncabezadoRecibo.NumeroRecibo = UCase(txtNumeroRecibo.Text)
            ''objEncabezadoRecibo.Descripcion = UCase(txtDescripcion.Text)
            ''objEncabezadoRecibo.Recibido = UCase(txtNumeroRecibido.Text)
            ''objEncabezadoRecibo.Anio = Format(DTPFechaRecibo.Value, "yy")

            arrRecibosExtra(3) = Format(SUConversiones.ConvierteADouble(txtMonto.Text), "#,##0.#0")
            arrRecibosExtra(5) = txtNumeroTarjeta.Text
            arrRecibosExtra(6) = txtBanco.Text
            arrRecibosExtra(7) = strNumRec  'UCase(txtNumeroRecibo.Text)
            arrRecibosExtra(8) = UCase(txtDescripcion.Text)
            arrRecibosExtra(9) = UCase(txtNumeroRecibido.Text)
            arrRecibosExtra(10) = ""
            If Label23.Text = "ANULADO" Then
                arrRecibosExtra(10) = "ANULADO"
                'objEncabezadoRecibo.Anulado = "ANULADO"
            End If
            arrRecibosExtra(11) = Format(DTPFechaRecibo.Value, "yy")
            If ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                strNumeroFact = strNumRec 'ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
            Else
                strNumeroFact = strNumRec 'txtNumeroRecibo.Text.Trim()
            End If
            ' frmNew.NumeroRecibo = UCase(txtNumeroRecibo.Text)
            Dim frmNew As New actrptViewer
            If strNumeroFact = "" Then
                strNumeroFact = UCase(txtNumeroRecibo.Text)
            End If
            frmNew.NumeroRecibo = UCase(strNumeroFact)
            frmNew.IdProveedor = SUConversiones.ConvierteAInt(TextBox14.Text)
            '  intRptImpRecibos = intTipoReciboCaja
            intrptCtasPagar = 13
            ' frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
            '    frmNew.WindowState = FormWindowState.Minimized
            '    frmNew.Close()
            'End If
            'For intIncr = 0 To 11
            For intIncr = 0 To 10
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            For intIncr = 0 To 7
                arrRecibo(intIncr, 0) = ""
                arrRecibo(intIncr, 1) = ""
                arrRecibo(intIncr, 2) = ""
                arrRecibo(intIncr, 3) = ""
                arrRecibo(intIncr, 4) = ""
            Next intIncr

            Limpiar()
            strNumRec = ""
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
    End Sub

    Sub Limpiar()

        Try

            For intIncr = 1 To 13
                txtCollection.Item(intIncr).text = ""
            Next
            txtMonto.Text = "0.00"
            TextBox14.Text = "0"
            TextBox15.Text = "0"
            TextBox16.Text = "0.00"
            TextBox17.Text = "0.00"
            txtBanco.Text = ""
            txtNumeroTarjeta.Text = ""
            txtRefElectronica.Text = ""
            txtDescripcionRecibo.Text = "0.00"
            dblMontoRecibo = 0
            dblInteresRecibo = 0
            dblMntVlrRecibo = 0
            DTPFechaRecibo.Value = Now
            ddlFormaPago.SelectedIndex = 0
            'ddlSerie.SelectedIndex = 0

            dgvxPagosFacturas.Rows.Clear()
            Label15.Text = dblMontoRecibo
            TextBox16.Text = dblInteresRecibo
            TextBox17.Text = dblMntVlrRecibo
            If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then
                For intIncr = 1 To 13
                    txtCollection.Item(intIncr).readonly = True
                Next
                txtCodigoProveedor.ReadOnly = False
                txtNumeroRecibo.ReadOnly = False
                ddlFormaPago.Enabled = False
                txtDescripcionRecibo.ReadOnly = True
                txtBanco.ReadOnly = True
                DTPFechaRecibo.Enabled = False
            End If
            'txtNumeroRecibo.Text = "R-"
            txtNumeroRecibo.Text = ""
            txtCodigoProveedor.Focus()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Sub Extraer()
        Try

            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                If TextBox14.Text = "0" Then
                    MsgBox("Tiene que digitar el codigo del Proveedor.", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
            End If
            If txtNumeroRecibo.Text = "" Then
                MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            intRptCtasCbr = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptOtros = 0
            intRptImpRecibos = 0
            strNumeroFact = String.Empty
            Me.Cursor = Cursors.WaitCursor
            txtMonto.Text = "0.00"
            Label15.Text = "0.00"
            ddlFormaPago.SelectedIndex = 0
            txtNumeroTarjeta.Text = ""
            txtDescripcion.Text = ""
            txtNumeroRecibido.Text = ""
            TextBox16.Text = "0.00"
            TextBox17.Text = "0.00"
            txtDescripcionRecibo.Text = "0.00"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""

            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                strQuery = "ObtieneDetalleReciboxProveedorYNumeroRecibo " & SUConversiones.ConvierteAInt(TextBox14.Text.Trim()) & ", '" & UCase(txtNumeroRecibo.Text) & "'"
            Else
                If ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                    'strNumeroFact = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                    strNumeroFact = txtNumeroRecibo.Text.Trim()
                Else
                    strNumeroFact = txtNumeroRecibo.Text.Trim()
                End If
                strQuery = "ObtieneDetalleReciboProveedorxNumeroRecibo '" & UCase(strNumeroFact) & "'," & ConvierteAInt(TextBox14.Text.Trim())
            End If

            intContar = 0

            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            dgvxPagosFacturas.Rows.Clear()

            'dgvxPagosFacturas.Columns("FechaEmis").HeaderText = "Concepto"
            'dgvxPagosFacturas.Columns("FechaVenc").Visible = False
            'dgvxPagosFacturas.Columns("SaldoFact").Visible = False
            'dgvxPagosFacturas.Columns("NvoSaldo").Visible = False
            'dgvxPagosFacturas.Columns("PorAplicar").Visible = False

            While dtrAgro2K.Read
                txtCodigoProveedor.Text = dtrAgro2K.GetValue(30)
                'txtCodigoCliente.Text = dtrAgro2K.GetName()
                If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then

                    If dtrAgro2K.Item("DescripcionPago") <> "Retenci�n del Recibo" Then
                        dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("DescripcionPago"), " ", " ", Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("PorcDescuentoFact")), "#,##0.#0"), Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("MontoDescuentoFact")), "#,##0.#0"), Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("MontoAPagarFactura")), "#,##0.#0"), " ", Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("interes")), "#,##0.#0"), Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("mantvlr")), "#,##0.#0"), " ")
                        'dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("NumeroFactura"), dtrAgro2K.Item("DescripcionPago"), " ", " ", Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("MontoRecibo")), "#,##0.#0"), " ", Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("interes")), "#,##0.#0"), Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("mantvlr")), "#,##0.#0"), " ")
                    End If

                End If

                If dtrAgro2K.Item("DescripcionPago") <> "Retenci�n del Recibo" Then
                    arrRecibo(intContar, 0) = dtrAgro2K.Item("DescripcionPago")
                    'arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("MontoAPagarFactura")), "#,##0.#0")
                    arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("MontoRecibo")), "#,##0.#0")
                    arrRecibo(intContar, 2) = Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("Interes")), "#,##0.#0")
                    arrRecibo(intContar, 3) = Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("mantvlr")), "#,##0.#0")
                    arrRecibo(intContar, 4) = "C"
                End If

                intContar = intContar + 1
                txtMonto.Text = Format(dtrAgro2K.GetValue(6), "#,##0.#0")
                ' txtMonto.Text = Format(dtrAgro2K.Item("MontoRecibo"), "#,##0.#0")
                TextBox16.Text = dtrAgro2K.GetValue(7)
                ' TextBox16.Text = dtrAgro2K.Item("interes")
                txtDescripcionRecibo.Text = dtrAgro2K.GetValue(8)
                ' txtDescripcionRecibo.Text = dtrAgro2K.Item("NumeroRecibido")
                Label15.Text = dtrAgro2K.GetValue(17)
                ddlFormaPago.SelectedIndex = dtrAgro2K.GetValue(9)
                txtNumeroTarjeta.Text = dtrAgro2K.GetValue(10)
                txtBanco.Text = dtrAgro2K.GetValue(11)
                txtDescripcion.Text = dtrAgro2K.GetValue(12)
                txtNumeroRecibido.Text = dtrAgro2K.GetValue(13)
                DTPFechaRecibo.Value = dtrAgro2K.GetValue(16)
                TextBox17.Text = SUConversiones.ConvierteADouble(TextBox17.Text) + dtrAgro2K.GetValue(27)

                'Label23.Text = ""
                'If dtrAgro2K.GetValue(18) = 1 Then
                '    Label23.Text = "ANULADO"
                'End If
                'If intTipoReciboCaja = 4 Then
                '    If dtrAgro2K.GetValue(18) = 1 Then
                '        dtrAgro2K.Close()
                '        MsgBox("El recibo ya fue anulado.", MsgBoxStyle.Information, "Recibo Anulado")
                '        Limpiar()
                '        Me.Cursor = Cursors.Default
                '        Exit Sub
                '    End If
                'End If

            End While
            TextBox16.Text = Format(SUConversiones.ConvierteADouble(TextBox16.Text), "#,##0.#0")
            TextBox17.Text = Format(SUConversiones.ConvierteADouble(TextBox17.Text), "#,##0.#0")
            txtDescripcionRecibo.Text = Format(SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
            dtrAgro2K.Close()
            If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then
                If txtMonto.Text = "" Or txtMonto.Text = "0.00" Then
                    MsgBox("El n�mero del recibo de caja no es valido.  Verifique.", MsgBoxStyle.Critical, "Error de Datos")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.Default
            'If intTipoReciboCaja = 1 Or intTipoReciboCaja = 9 Then
            '    ImprimirRecibo()
            'End If

            If intTipoReciboCaja = 4 Or intTipoReciboCaja = 9 Then
                UbicarProveedor(txtCodigoProveedor.Text.Trim())
                ' LlenarGrid(txtCodigoCliente.Text.Trim())
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try


    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProveedor.KeyDown, txtNumeroRecibo.KeyDown, DTPFechaRecibo.KeyDown, txtMonto.KeyDown, ddlFormaPago.KeyDown, txtNumeroTarjeta.KeyDown, txtDescripcion.KeyDown, txtNumeroRecibido.KeyDown, txtDescripcionRecibo.KeyDown, txtBanco.KeyDown  ', ddlSerie.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
                Select Case intTipoReciboCaja
                    Case ENTipoRecibo.RECIBO_CAJA,
                    ENTipoRecibo.RECIBO_MANUAL
                        cmdOK.Visible = True
                        CmdAnular.Visible = False
                        cmdImprimir.Visible = False
                        cmdBuscar.Visible = False
                    Case ENTipoRecibo.RECIBO_IMPRIMIR
                        cmdOK.Visible = False
                        CmdAnular.Visible = False
                        cmdImprimir.Visible = True
                        cmdBuscar.Visible = True
                    Case ENTipoRecibo.RECIBO_ANULAR
                        cmdOK.Visible = False
                        CmdAnular.Visible = True
                        cmdImprimir.Visible = False
                End Select
                Select Case sender.name.ToString
                    Case "txtCodigoProveedor"
                        If UbicarProveedor(txtCodigoProveedor.Text) = True Then
                            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                                txtNumeroRecibo.Focus()
                            Else
                                txtNumeroRecibo.Focus()
                            End If
                            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                                LlenarGrid(txtCodigoProveedor.Text)
                                txtNumeroRecibo.Focus()
                            End If
                        End If
                    Case "ddlSerie"
                        If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                            DTPFechaRecibo.Focus()
                        Else
                            txtNumeroRecibo.Focus()
                        End If

                    Case "txtNumeroRecibo"
                        DTPFechaRecibo.Focus()
                        If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                            If txtCodigoProveedor.Text = "" Then
                                MsgBox("Debe Ingresar El Porveedor Primero", MsgBoxStyle.Information, "Ctas x Pagar")
                                txtNumeroRecibido.Text = String.Empty
                                txtCodigoProveedor.Focus()
                                Exit Sub
                            Else
                                UbicarRecibo()
                            End If

                        ElseIf intTipoReciboCaja <> 1 Then
                            Extraer()
                        End If
                    Case "DTPFechaRecibo" : txtMonto.Focus()
                    Case "txtMonto"
                        Label15.Text = "0.00"
                        If IsNumeric(txtMonto.Text) = True Then
                            txtMonto.Text = Format(SUConversiones.ConvierteADouble(txtMonto.Text), "#,##0.#0")
                            If intTipoReciboCaja <> 2 And intTipoReciboCaja <> 5 Then
                                ddlFormaPago.Focus()
                            Else
                                txtNumeroRecibido.Focus()
                            End If
                            dblMontoRecibo = SUConversiones.ConvierteADouble(txtMonto.Text)
                            If dgvxPagosFacturas.Rows.Count >= 1 Then
                                For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                                    ''MSFlexGrid2.Col = 4
                                    dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                                    ''MSFlexGrid2.Col = 6
                                    dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
                                    ''MSFlexGrid2.Col = 7
                                    dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
                                Next intIncr
                                Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                                If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                                    Label15.ForeColor = System.Drawing.Color.Red
                                Else
                                    Label15.ForeColor = System.Drawing.Color.Black
                                End If
                            End If
                        End If
                    Case "ddlFormaPago"
                        If txtNumeroTarjeta.Visible = True Then
                            txtNumeroTarjeta.Focus()
                        Else
                            txtDescripcion.Focus()
                        End If
                    Case "txtNumeroTarjeta" : txtDescripcion.Focus()
                    Case "txtDescripcion"
                        If ddlFormaPago.SelectedIndex = 1 Then
                            txtBanco.Focus()
                        Else
                            txtNumeroRecibido.Focus()
                        End If
                    Case "txtBanco" : txtNumeroRecibido.Focus()
                    Case "txtNumeroRecibido" : txtDescripcionRecibo.Focus()
                    Case "txtDescripcionRecibo"
                        txtDescripcionRecibo.Text = Format(SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                        dblMontoRecibo = SUConversiones.ConvierteADouble(txtMonto.Text)
                        If dgvxPagosFacturas.Rows.Count >= 2 Then
                            For intIncr = 0 To dgvxPagosFacturas.Rows.Count - 1
                                ''MSFlexGrid2.Col = 4
                                dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value)
                                ''MSFlexGrid2.Col = 6
                                dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value)
                                ''MSFlexGrid2.Col = 7
                                dblMontoRecibo = dblMontoRecibo - SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("MantValor").Value)
                            Next intIncr
                            Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                            If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                                Label15.ForeColor = System.Drawing.Color.Red
                            Else
                                Label15.ForeColor = System.Drawing.Color.Black
                            End If
                        End If
                        dgvxPagosFacturas.Focus()
                End Select
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try


    End Sub

    Sub ObtieneSeries()
        Dim IndicaObtieneRegistro As Integer = 0
        Try
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            ddlSerie.SelectedItem = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "ObtieneSerieParametrosReciboProveedor " & lngRegAgencia
            If strQuery.Trim.Length > 0 Then
                ddlSerie.Items.Clear()
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                    IndicaObtieneRegistro = 1
                End While
            End If
            If IndicaObtieneRegistro = 1 Then
                ddlSerie.SelectedIndex = 0
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try


    End Sub

    Sub UbicarRecibo()

        Try


            Dim strRecibotmp As String = String.Empty
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""

            If ConvierteAInt(txtNumeroRecibo.Text.Trim) <> 0 Then
                'strRecibotmp = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
                strRecibotmp = txtNumeroRecibo.Text
            Else
                strRecibotmp = txtNumeroRecibo.Text
            End If
            UCase(txtNumeroRecibo.Text)
            strQuery = "exec VerificaEstadoNumeroReciboProveedor '" & strRecibotmp & "'," & ConvierteAInt(TextBox14.Text.Trim())
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(1) = 0 Then
                        If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                            MsgBox("El recibo de caja ya fue emitido.", MsgBoxStyle.Critical, "Recibo de Caja Emitido")
                        End If
                    ElseIf dtrAgro2K.GetValue(1) = 1 Then
                        MsgBox("El recibo de caja ya fue anulado.", MsgBoxStyle.Critical, "Recibo de Caja Anulado")
                    End If
                    txtNumeroRecibo.Focus()
                    txtNumeroRecibo.Text = ""
                    Exit While
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'dtrAgro2K.Close()
            '    cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Private Sub CalcularMontosGrid()

        Dim strMonto As String
        Dim PorcDesc As Double
        Dim MontoDesc As Double
        Dim dblMonto As Double
        Dim dblSaldo As Double
        Dim strMtoAnt As String
        Dim dblMtoAnt As Double
        Dim intResp As Integer = 0

        Try

            If dgvxPagosFacturas.Rows.Count >= 1 Then
                If dgvxPagosFacturas.CurrentCell.ColumnIndex = 6 Then   'Celda en la que se ingresa el monto del recibo
                    dblMonto = 0
                    strMonto = ""
                    dblMtoAnt = 0
                    strMtoAnt = ""
                    ''strMtoAnt = dgvxPagosFacturas.CurrentCell.Value
                    strMtoAnt = SaldoAnterior
                    strMonto = dgvxPagosFacturas.CurrentCell.Value
                    dblMtoAnt = SUConversiones.ConvierteADouble(strMtoAnt)

                    If IsNumeric(Trim(strMonto)) = False Then
                        Exit Sub
                    End If
                    dblMonto = SUConversiones.ConvierteADouble(strMonto)
                    If dblMonto < 0 Then
                        MsgBox("El monto a abonar debe ser mayor o igual a 0.00", MsgBoxStyle.Critical, "Error de Dato")
                        Exit Sub
                    End If

                    MontoDesc = dgvxPagosFacturas.CurrentRow.Cells("MontoDescuentoFact").Value

                    dblSaldo = SUConversiones.ConvierteADouble(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
                    If dblSaldo - (dblMonto + MontoDesc) < 0 Then
                        MsgBox("No es permitido pagar de m�s una factura.", MsgBoxStyle.Critical, "Error de Dato")
                        dblMontoRecibo = 0
                        dgvxPagosFacturas.CurrentCell.Value = 0
                        Exit Sub
                    End If


                    dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(dblSaldo - (dblMonto + MontoDesc), "###,##0.#0")
                    dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                    Label15.Text = Format(dblMontoRecibo, "###,##0.#0")
                    Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                    If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                        Label15.ForeColor = System.Drawing.Color.Red
                    Else
                        Label15.ForeColor = System.Drawing.Color.Black
                    End If
                ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 8 Then   'Columna para ingresar el monto del interes
                    dblMonto = 0
                    strMtoAnt = ""
                    dblMtoAnt = 0
                    strMtoAnt = ""
                    ''strMtoAnt = MSFlexGrid2.Text
                    strMtoAnt = SaldoAnterior
                    strMonto = dgvxPagosFacturas.CurrentCell.Value
                    dblMtoAnt = SUConversiones.ConvierteADouble(strMtoAnt)
                    ''strMonto = InputBox("Digite el Monto de Intereses para la Factura", "Monto de Intereses")
                    intResp = MsgBox("�Usted esta abonando a intereses, Desea Continuar?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Recibo")
                    If IsNumeric(Trim(strMonto)) = False Then
                        dgvxPagosFacturas.CurrentCell.Value = 0
                        Exit Sub
                    End If
                    dblMonto = SUConversiones.ConvierteADouble(strMonto)
                    ''MSFlexGrid2.Text = Format(dblMonto, "###,##0.#0")
                    dgvxPagosFacturas.CurrentCell.Value = Format(dblMonto, "###,##0.#0")
                    If intResp <> 6 Then
                        ''MSFlexGrid2.Text = 0
                        dgvxPagosFacturas.CurrentCell.Value = 0
                        dblMonto = 0
                    End If
                    dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                    dblInteresRecibo = dblInteresRecibo - dblMtoAnt + dblMonto
                    Label15.Text = Format(dblMontoRecibo, "###,##0.#0")
                    Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                    If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                        Label15.ForeColor = System.Drawing.Color.Red
                    Else
                        Label15.ForeColor = System.Drawing.Color.Black
                    End If
                    TextBox16.Text = Format(dblInteresRecibo, "###,##0.#0")
                ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 9 Then   'Columna para ingresar el monto de Mant. al Valor
                    dblMonto = 0
                    strMtoAnt = ""
                    dblMtoAnt = 0
                    strMtoAnt = ""
                    ''strMtoAnt = MSFlexGrid2.Text
                    strMtoAnt = SaldoAnterior
                    strMonto = dgvxPagosFacturas.CurrentCell.Value
                    dblMtoAnt = SUConversiones.ConvierteADouble(strMtoAnt)
                    ''strMonto = InputBox("Digite el Monto de Mantenimiento de Valor para la Factura", "Monto de Intereses")
                    If IsNumeric(Trim(strMonto)) = False Then
                        Exit Sub
                    End If
                    dblMonto = SUConversiones.ConvierteADouble(strMonto)
                    ''MSFlexGrid2.Text = Format(dblMonto, "###,##0.#0")
                    dgvxPagosFacturas.CurrentCell.Value = Format(dblMonto, "###,##0.#0")
                    dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                    dblMntVlrRecibo = dblMntVlrRecibo - dblMtoAnt + dblMonto
                    Label15.Text = Format(dblMontoRecibo, "###,##0.#0")
                    Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                    If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                        Label15.ForeColor = System.Drawing.Color.Red
                    Else
                        Label15.ForeColor = System.Drawing.Color.Black
                    End If
                    TextBox17.Text = Format(dblMntVlrRecibo, "###,##0.#0")
                ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 10 Then  'Columna para ingresar el monto de por Aplicar
                    dblMonto = 0
                    strMtoAnt = ""
                    dblMtoAnt = 0
                    strMtoAnt = ""
                    ''strMtoAnt = MSFlexGrid2.Text
                    strMtoAnt = SaldoAnterior
                    strMonto = dgvxPagosFacturas.CurrentCell.Value
                    dblMtoAnt = SUConversiones.ConvierteADouble(strMtoAnt)
                    ''strMonto = InputBox("Digite el Monto Pendiente a Aplicar para la Factura", "Monto Pendiente")
                    If IsNumeric(Trim(strMonto)) = False Then
                        Exit Sub
                    End If
                    dblMonto = SUConversiones.ConvierteADouble(strMonto)
                    ''MSFlexGrid2.Text = Format(dblMonto, "###,##0.#0")
                    dgvxPagosFacturas.CurrentCell.Value = Format(dblMonto, "###,##0.#0")
                    ''MSFlexGrid2.Col = 4
                    If dgvxPagosFacturas.CurrentRow.Cells("Monto").Value = "0.00" Then
                        MsgBox("No puede poner el saldo a aplicar en una factura que no est� siendo abonada o cancelada.", MsgBoxStyle.Critical, "Error de Dato")
                        ''MSFlexGrid2.Col = 8
                        ''MSFlexGrid2.Text = Format(dblMtoAnt, "###,##0.#0")
                        dgvxPagosFacturas.CurrentCell.Value = Format(dblMtoAnt, "###,##0.#0")
                        dblMonto = 0
                    End If
                    dblMontoRecibo = dblMontoRecibo + dblMtoAnt - dblMonto
                    dblPendienteRecibo = dblPendienteRecibo - dblMtoAnt + dblMonto
                    Label15.Text = Format(dblMontoRecibo, "###,##0.#0")
                    Label15.Text = Format(dblMontoRecibo + SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                    If SUConversiones.ConvierteADouble(Label15.Text) < 0 Then
                        Label15.ForeColor = System.Drawing.Color.Red
                    Else
                        Label15.ForeColor = System.Drawing.Color.Black
                    End If
                ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 4 Then   'Porcentaje de Descuento
                    Dim MontoFact, Deposito, SaldoFact As Double
                    PorcDesc = dgvxPagosFacturas.CurrentCell.Value
                    MontoFact = dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value

                    MontoDesc = (PorcDesc / 100) * MontoFact

                    dgvxPagosFacturas.CurrentRow.Cells("MontoDescuentoFact").Value = Format(MontoDesc, "#,##0.#0")

                    Deposito = dgvxPagosFacturas.CurrentRow.Cells("Monto").Value

                    SaldoFact = SUConversiones.ConvierteADouble(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
                    If SaldoFact - (Deposito + MontoDesc) < 0 Then
                        MsgBox("No es permitido pagar de m�s una factura.", MsgBoxStyle.Critical, "Error de Dato")
                        dgvxPagosFacturas.CurrentCell.Value = 0
                        dgvxPagosFacturas.CurrentRow.Cells("MontoDescuentoFact").Value = 0
                        Exit Sub
                    End If
                    dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(SaldoFact - (Deposito + MontoDesc), "###,##0.#0")

                ElseIf dgvxPagosFacturas.CurrentCell.ColumnIndex = 5 Then   'Monto del Descuento
                    Dim MontoFact, Deposito, SaldoFact As Double
                    MontoDesc = dgvxPagosFacturas.CurrentCell.Value
                    MontoFact = dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value

                    PorcDesc = (MontoDesc / MontoFact) * 100

                    dgvxPagosFacturas.CurrentRow.Cells("PorcDescuentoFact").Value = Format(PorcDesc, "#,##0.#0")

                    Deposito = dgvxPagosFacturas.CurrentRow.Cells("Monto").Value

                    SaldoFact = SUConversiones.ConvierteADouble(dgvxPagosFacturas.CurrentRow.Cells("SaldoFact").Value)
                    If SaldoFact - (Deposito + MontoDesc) < 0 Then
                        MsgBox("No es permitido pagar de m�s una factura.", MsgBoxStyle.Critical, "Error de Dato")
                        dgvxPagosFacturas.CurrentCell.Value = 0
                        dgvxPagosFacturas.CurrentRow.Cells("PorcDescuentoFact").Value = 0
                        Exit Sub
                    End If
                    dgvxPagosFacturas.CurrentRow.Cells("NvoSaldo").Value = Format(SaldoFact - (Deposito + MontoDesc), "###,##0.#0")
                End If
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProveedor.GotFocus, txtNumeroRecibo.GotFocus, DTPFechaRecibo.GotFocus, txtMonto.GotFocus, ddlFormaPago.GotFocus, txtNumeroTarjeta.GotFocus, txtDescripcion.GotFocus, txtBanco.GotFocus

        Dim strCampo As String = String.Empty
        Try

            strCampo = ""
            Select Case sender.name.ToString
            ' Case "txtCodigoCliente" : Label17.Visible = True : strCampo = "C�digo"
                Case "txtCodigoProveedor" : Label17.Visible = True : strCampo = "C�digo"
                Case "ddlSerie" : strCampo = "Serie"
                Case "txtNumeroRecibo" : strCampo = "N�mero"
                Case "DTPFechaRecibo" : strCampo = "Fecha D�a"
                Case "txtMonto" : strCampo = "Monto"
                Case "ddlFormaPago" : strCampo = "Forma Pago"
                Case "txtNumeroTarjeta" : strCampo = "N�mero"
                Case "txtDescripcion" : strCampo = "Descripci�n"
                Case "ComboBox2" : strCampo = "Facturas"
                Case "TextBox2" : strCampo = "Banco"
            End Select
            'StatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            'StatusBar1.Panels.Item(1).Text = sender.tag
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name <> "DTPFechaRecibo" And sender.name <> "DateTimePicker2" And sender.name <> "DateTimePicker3" Then
                sender.selectall()
            End If

            If blnUbicar Then
                ObtieneDatosListadoAyuda()
            End If

            If intTipoReciboCaja = 1 Then
                'nUltimoNumeroRecibo = ExtraerSerieNumeroReciboProveedor(ddlSerie.SelectedItem)
                'txtNumeroRecibo.Text = ddlSerie.SelectedItem & Format(ConvierteAInt(nUltimoNumeroRecibo), "0000000")
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProveedor.LostFocus

        Label17.Visible = False

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlFormaPago.SelectedIndexChanged
        Try
            If ddlFormaPago.SelectedIndex = 0 Then
                Label12.Visible = False
                txtNumeroTarjeta.Visible = False
                Label7.Visible = False
                txtBanco.Visible = False
                Label25.Visible = False
                txtRefElectronica.Visible = False
            ElseIf ddlFormaPago.SelectedIndex = 1 Then
                Label12.Visible = True
                Label12.Text = "# Cheque"
                txtNumeroTarjeta.Visible = True
                Label7.Visible = True
                txtBanco.Visible = True
                Label25.Visible = False
                txtRefElectronica.Visible = False
            ElseIf ddlFormaPago.SelectedIndex = 2 Then
                Label12.Visible = True
                Label12.Text = "# Tarjeta"
                txtNumeroTarjeta.Visible = True
                Label7.Visible = False
                txtBanco.Visible = False
                Label25.Visible = False
                txtRefElectronica.Visible = False
            ElseIf ddlFormaPago.SelectedIndex = 3 Then
                Label12.Visible = True
                Label12.Text = "# Ctas"
                txtNumeroTarjeta.Visible = True
                Label7.Visible = True
                txtBanco.Visible = True
                Label25.Visible = True
                txtRefElectronica.Visible = True
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Function UbicarProveedor(ByVal strDato As String) As Boolean

        Dim strVendedor As String
        Try

            strVendedor = strDato
            Me.Cursor = Cursors.WaitCursor
            TextBox14.Text = "0"
            TextBox15.Text = "0"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "select * from prm_Proveedores where codigo = '" & strVendedor & "'"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    TextBox14.Text = dtrAgro2K.GetValue(0)  'Almacena el IdProveedor
                End If
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "exec ObtieneDetalleProveedorParaRecibo " & ConvierteAInt(TextBox14.Text.Trim())

            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                Limpiar()
            End If
            If dblTipoCambio = 0 Then
                dblTipoCambio = 1
            End If
            strVendedor = ""
            While dtrAgro2K.Read
                txtCodigoProveedor.Text = dtrAgro2K.GetValue(0)
                TextBox2.Text = dtrAgro2K.GetValue(1)
                txtNumeroRecibido.Text = dtrAgro2K.GetValue(1)
                TextBox3.Text = "" 'dtrAgro2K.GetValue(2)
                TextBox4.Text = "" 'dtrAgro2K.GetValue(3)
                TextBox5.Text = "" 'dtrAgro2K.GetValue(4)
                TextBox6.Text = "" 'dtrAgro2K.GetValue(5)
                TextBox7.Text = Format(dtrAgro2K.GetValue(2), "#,##0.#0")
                TextBox8.Text = IIf(ConvierteADouble(dtrAgro2K.GetValue(2)) <> 0, Format(ConvierteADouble(dtrAgro2K.GetValue(2)) / dblTipoCambio, "#,##0.#0"), "0.00")
                strVendedor = 0 'dtrAgro2K.GetValue(7)
                TextBox14.Text = dtrAgro2K.GetValue(3)
                TextBox19.Text = Format(dtrAgro2K.GetValue(2), "#,##0.#0")
                TextBox20.Text = IIf(ConvierteADouble(dtrAgro2K.GetValue(2)) <> 0, Format(ConvierteADouble(dtrAgro2K.GetValue(2)) / dblTipoCambio, "#,##0.#0"), "0.00")
            End While
            dtrAgro2K.Close()
            If TextBox2.Text = "" Then
                UbicarProveedor = False
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                Me.Cursor = Cursors.Default
                Exit Function
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            UbicarProveedor = False
            Throw ex
            ' MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

        UbicarProveedor = True
        Me.Cursor = Cursors.Default

    End Function

    Sub LlenarGrid(ByVal strDato As String)
        Try

            strQuery = ""
            strNumeroFact = String.Empty
            If ConvierteAInt(txtNumeroRecibo.Text.Trim()) <> 0 Then
                strNumeroFact = ddlSerie.SelectedItem & Format(ConvierteAInt(txtNumeroRecibo.Text.Trim()), "0000000")
            Else
                strNumeroFact = txtNumeroRecibo.Text.Trim()

            End If


            If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                strQuery = "Exec ObtieneComprasActivasxProveedor  " & SUConversiones.ConvierteAInt(TextBox14.Text)
                'strQuery = "Select M.numero, M.fechaing, M.fechaven, M.saldo "
                'strQuery = strQuery + "From detObligacionEmpresa M "
                'strQuery = strQuery + "Where M.IdProveedor = " & ConvierteAInt(TextBox14.Text) & " And M.estado = 0 and M.saldo <> 0 "
            Else

                strQuery = "exec ConsultaDetalleReciboProveedorxNumeroRecibo '" & strNumeroFact & "'"
            End If
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader

            While dtrAgro2K.Read
                dgvxPagosFacturas.Rows.Add(dtrAgro2K.Item("numero"), dtrAgro2K.Item("fechaing"), dtrAgro2K.Item("fechaven"), Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("saldo")), "#,##0.#0"), 0, 0.0, 0.0, Format(SUConversiones.ConvierteADouble(dtrAgro2K.Item("saldo")), "#,##0.#0"), 0.0, 0.0, 0.0)
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Private Sub ObtieneDatosListadoAyuda()
        Try
            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 7
                            txtCodigoProveedor.Text = strUbicar
                            If UbicarProveedor(txtCodigoProveedor.Text) = True Then
                                txtNumeroRecibo.Focus()
                                If intTipoReciboCaja = 1 Or intTipoReciboCaja = 2 Or intTipoReciboCaja = 15 Then
                                    LlenarGrid(txtCodigoProveedor.Text)
                                End If
                            End If
                    End Select
                End If
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try
    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigoProveedor.DoubleClick

        intListadoAyuda = 7
        Dim frmNew As New frmListadoAyuda
        'Timer1.Interval = 200
        'Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Sub VistaPrevia()
        Try

            intContar = 0
            dblPendienteRecibo = 0
            For intIncr = 1 To dgvxPagosFacturas.Rows.Count - 1

                If dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value <> "0.00" Then
                    arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Monto").Value), "###0.#0")
                    If dgvxPagosFacturas.Rows(intIncr).Cells("NvoSaldo").Value = "0.00" Then
                        strRecibo = "Cancelaci�n de la Factura " & dgvxPagosFacturas.Rows(intIncr).Cells("NumFactura").Value.Text
                    Else
                        strRecibo = "Abono a la Factura " & dgvxPagosFacturas.Rows(intIncr).Cells("NumFactura").Value
                    End If
                    arrRecibo(intContar, 0) = strRecibo
                    arrRecibo(intContar, 2) = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("Interes").Value), "###0.#0")
                    arrRecibo(intContar, 3) = Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("MontValor").Value), "###0.#0")
                    dblPendienteRecibo = dblPendienteRecibo + Format(SUConversiones.ConvierteADouble(dgvxPagosFacturas.Rows(intIncr).Cells("PorAplicar").Value), "###0.#0")
                    arrRecibo(intContar, 4) = "C"
                    intContar = intContar + 1
                End If
            Next intIncr
            If SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text) <> 0 Then
                arrRecibo(intContar, 0) = "Retenci�n del Recibo"
                arrRecibo(intContar, 1) = Format(SUConversiones.ConvierteADouble(txtDescripcionRecibo.Text), "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "D"
                intContar = intContar + 1
            End If
            If dblPendienteRecibo <> 0 Then
                arrRecibo(intContar, 0) = "Saldo Pendiente"
                arrRecibo(intContar, 1) = Format(dblPendienteRecibo, "#,##0.#0")
                arrRecibo(intContar, 2) = "0.00"
                arrRecibo(intContar, 3) = "0.00"
                arrRecibo(intContar, 4) = "C"
            End If
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            strQuery = ""
            'strQuery = "select nombre, codigo , cta_contable from prm_clientes where registro = " & CLng(TextBox14.Text) & ""
            strQuery = "select nombre, codigo from prm_Proveedores where registro = " & SUConversiones.ConvierteALong(TextBox14.Text) & ""
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            arrRecibosExtra(0) = Format(DTPFechaRecibo.Value, "dd-MMM-yyyy")
            While dtrAgro2K.Read
                arrRecibosExtra(1) = dtrAgro2K.GetValue(0)
                arrRecibosExtra(2) = dtrAgro2K.GetValue(1)
                arrRecibosExtra(4) = "0" 'dtrAgro2K.GetValue(2)
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            arrRecibosExtra(3) = Format(SUConversiones.ConvierteADouble(txtMonto.Text), "#,##0.#0")
            arrRecibosExtra(5) = txtNumeroTarjeta.Text
            arrRecibosExtra(6) = txtBanco.Text
            arrRecibosExtra(7) = UCase(txtNumeroRecibo.Text)
            arrRecibosExtra(8) = UCase(txtDescripcion.Text)
            arrRecibosExtra(9) = UCase(txtNumeroRecibido.Text)
            Dim frmNew As New actrptViewer
            intRptImpRecibos = 9
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
            For intIncr = 0 To 7
                arrRecibosExtra(intIncr) = ""
            Next intIncr
            For intIncr = 0 To 7
                arrRecibo(intIncr, 0) = ""
                arrRecibo(intIncr, 1) = ""
                arrRecibo(intIncr, 2) = ""
                arrRecibo(intIncr, 3) = ""
                arrRecibo(intIncr, 4) = ""
            Next intIncr
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MessageBoxEx.Show("Error al obtener informaci�n ", " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Exit Sub
        End Try

    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged
        'If intTipoReciboCaja = 1 Then
        '    nUltimoNumeroRecibo = ExtraerSerieNumeroReciboProveedor(ddlSerie.SelectedItem)
        '    txtNumeroRecibo.Text = ddlSerie.SelectedItem & Format(ConvierteAInt(nUltimoNumeroRecibo), "0000000")
        '    txtNumeroRecibo.Enabled = True
        '    txtNumeroRecibido.Focus()
        'End If
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try

            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            Limpiar()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try


    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        Try


            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If cmdBuscar.Text <> "<F7>" Then
                Extraer()
            ElseIf cmdBuscar.Text = "<F7>" Then
                If intTipoReciboCaja = 1 Or intTipoReciboCaja = 15 Then
                    VistaPrevia()
                End If
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub CambiaIcoAnular(ByVal Boton As DevComponents.DotNetBar.ButtonItem)
        Try
            Boton.Tooltip = "Anular recibo"
            Boton.Text = "Anular<F7>"
            Boton.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
            Boton.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
            Boton.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
            Boton.ImageFixedSize = New System.Drawing.Size(40, 40)
            Boton.Image = ObtieneImagen("Remove_48x48.png")
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Function ObtieneImagen(ByVal NombreImagen As String) As System.Drawing.Image
        Dim imagen As System.Drawing.Image
        Try
            'Dim file_name As String = Application.
            Dim NombreCarpeta As String
            Dim NombreArchivo As String = String.Empty
            NombreCarpeta = System.IO.Directory.GetCurrentDirectory()
            If NombreCarpeta.EndsWith("\bin\Debug") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin\Debug", "")
            End If
            If NombreCarpeta.EndsWith("\bin") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin", "")
            End If
            NombreCarpeta = NombreCarpeta & "\Imagenes\"
            NombreArchivo = NombreCarpeta & NombreImagen

            'imagen = Image.FromFile(NombreArchivo)
            imagen = System.Drawing.Image.FromFile(NombreArchivo)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Return Nothing
        End Try
        Return imagen
    End Function

    Private Sub cmdImprimir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        Try
            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If intTipoReciboCaja = 9 Then
                ImprimirRecibo()
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub CmdAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
                If txtNumeroRecibo.Text.Length <= 0 Then
                    MsgBox("Favor buscar el recibo a Anular.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                ''Anular()

            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Sub CmdAnular_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAnular.Click
        Try


            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
                If txtNumeroRecibo.Text.Length <= 0 Then
                    MsgBox("Favor buscar el recibo a Anular.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvxPagosFacturas.CellBeginEdit
        Try
            SaldoAnterior = dgvxPagosFacturas.CurrentCell.Value
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Sub dgvxPagosFacturas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvxPagosFacturas.KeyPress
        Try
            If e.KeyChar = Keys.Enter.ToString Then
                CalcularMontosGrid()
            ElseIf e.KeyChar = Keys.Tab.ToString Then
                CalcularMontosGrid()
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvxPagosFacturas.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                CalcularMontosGrid()
            ElseIf e.KeyCode = Keys.Tab Then
                CalcularMontosGrid()
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try
    End Sub

    Private Sub dgvxPagosFacturas_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvxPagosFacturas.CellEndEdit
        Try
            CalcularMontosGrid()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try

            intTipoReciboCaja = SUConversiones.ConvierteAInt(Label22.Text)
            If intTipoReciboCaja = 4 Or intTipoReciboCaja = 5 Then
                If Year(DTPFechaRecibo.Value) = Year(Now) And Month(DTPFechaRecibo.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            Else
                Guardar()
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MessageBoxEx.Show(ex.Message.ToString(), " Pedidos a proveedor ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End Try

    End Sub

End Class