Imports System.Data.SqlClient
Imports System.Data.OleDb.OleDbConnection
Imports GVSoft.SistemaGerencial.ReglaNegocio

Module mdlVariables

    Public intListadoAyuda, lngRegistro, intDiaFact, lngAgenRegExp, intExportarModulo, intFechaCambioFactura, intFechaCambioCompra, lngFechaFactura, nIdModulo, nIdUsuario, nIdUsuarioRelogin, lngRegUsuarioRelogin, nIdVet As Integer
    Public lngRegUsuario, lngRegAgencia, intError, intAgenciaTraslado, intAgenciaDefecto, intImpresoraAsignada As Byte
    Public intRptFactura, intRptInventario, intRptCtasCbr, intRptImpFactura, intRptImppedidoProveedor, intRptImpRecibos, intrptCtasPagar, intRptCuadreCaja, intRptProformas, intRptImpCompras As Byte
    Public intRptExportar, intRptPresup, intRptOtros, intRptTipoPrecio, intRptCheques As Byte
    Public intIncr, intResp, intModulo, intTipoFactura, intTipoCompra, intTipoReciboCaja, intTipoCheque, intAccesos(237), intIncr2, intNumFechaEstInv, gnMonedaFactura As Integer
    Public blnMainMenu, blnClear, blnIgual, blnUbicar, blnVistaPrevia As Boolean
    Public strQuery, strAppPath, strUsuario, strUsuarioRelogin, strAgro2K, strAgroAccess, strNombreSuc As String
    Public strNombreUsuario, strUbicar, strUbicar02, strModuloNombre(43), strCodigoTmp As String
    Public arrContado(26, 8), arrCredito(26, 8), arrRecibo(60, 5), arrRecibosExtra(12), strNombreArchivo As String
    Public strQueryParte00, strQueryParte01, strQueryParte02, strQueryParte03, strQueryParte04, strQueryParte05 As String
    Public strFechaInicial, strFechaFinal, strFechaReporte, strVistaPreviaNTCNTD(9) As String
    Public strLaOrden, strLibrador, strDepartamento, strNumeroUbicarFactura, strNombEmpresa, strTelefEmpresa, sDireccionAgencia, sTelefonoAjencia, sNombreAgencia As String
    Public strNumRec, strNumCompras As String, ConnectionStringSIMF As String = RNConeccion.ObtieneCadenaConexion(), ConnectionStringMundisis As String = RNConeccion.ObtieneCadenaConexion()

    Public lngYearFact, lngClienteContado, lngNegocioContado, lngDepartContado As Long
    Public strNumeroFact, strClienteFact, strCodCliFact, strVendedor, strDirecFact, strDireccionClienteFact As String
    Public strMesFact, strFecVencFact, strDiasFact, strValorFactCOR, strNombAgenEstInv As String
    Public dblSubtotal, dblImpuesto, dblRetencion, dblTotal, dblTipoCambio, dblMontoReciboNTDNTC As Double
    Public dblPorcParam, dblRetParam As Double
    Public dblTotalPagar, dblCambio, dblPagoContado, dblTotalTarjeta, dblTotalCheque As Double  'Variables para el modulo de pagar factura

    Public dtrAgro2K As IDataReader
    Public cmdAgro2K As New SqlCommand
    Public cnnAgro2K As New SqlConnection

    Public dtrAgro2KTmp As IDataReader
    Public cmdAgro2KTmp As New SqlCommand
    Public cnnAgro2KTmp As New SqlConnection

End Module
