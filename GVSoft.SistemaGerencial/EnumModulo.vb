﻿Public Enum EnumModulo
    Facturacion = 1
    Inventario = 2
    Compra = 3
    CtaxCobrar = 4
    Parametros = 5
    ExportarImportar = 6
    Operaciones = 8
    CtasxPagar = 9
    Proformas = 10
    Pedidos = 11
    Devoluciones = 11
End Enum

Public Enum Accion
    Ingresar = 1
    Modificar = 2
End Enum


