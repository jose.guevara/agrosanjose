<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actRptDetalleDeposito
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actRptDetalleDeposito))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.TextBox25 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox
        Me.Label20 = New DataDynamics.ActiveReports.Label
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.Line = New DataDynamics.ActiveReports.Line
        Me.Detail1 = New DataDynamics.ActiveReports.Detail
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.txtTotalProductos = New DataDynamics.ActiveReports.TextBox
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.lblSucursalDatos = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.txtFechaDeposito = New DataDynamics.ActiveReports.TextBox
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSucursalDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFechaDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label15, Me.Label16, Me.TextBox25, Me.TextBox26, Me.Label20, Me.Label})
        Me.PageHeader1.Height = 1.489584!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 3.383!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label15.Text = "Fecha de Impresi�n"
        Me.Label15.Top = 0.5!
        Me.Label15.Width = 1.35!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.383!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label16.Text = "Hora de Impresi�n"
        Me.Label16.Top = 0.6875!
        Me.Label16.Width = 1.35!
        '
        'TextBox25
        '
        Me.TextBox25.Height = 0.2!
        Me.TextBox25.Left = 4.757997!
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Style = "text-align: right"
        Me.TextBox25.Text = "TextBox25"
        Me.TextBox25.Top = 0.5!
        Me.TextBox25.Width = 0.9!
        '
        'TextBox26
        '
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 4.757997!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right"
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.6875!
        Me.TextBox26.Width = 0.9!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.1875!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 13pt"
        Me.Label20.Text = "Detalle Depositos"
        Me.Label20.Top = 0.625!
        Me.Label20.Width = 2.5!
        '
        'Label
        '
        Me.Label.Height = 0.375!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.4375!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: center; font-weight: bold; font-size: 20.25pt; font-family: Times New" & _
            " Roman"
        Me.Label.Text = "Agro Centro, S.A."
        Me.Label.Top = 0.0!
        Me.Label.Width = 5.2495!
        '
        'Line
        '
        Me.Line.Height = 0.0!
        Me.Line.Left = 0.0!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 0.5649999!
        Me.Line.Width = 5.687!
        Me.Line.X1 = 0.0!
        Me.Line.X2 = 5.687!
        Me.Line.Y1 = 0.5649999!
        Me.Line.Y2 = 0.5649999!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0.0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox2, Me.TextBox1, Me.txtFechaDeposito})
        Me.Detail1.Height = 0.2291663!
        Me.Detail1.Name = "Detail1"
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "MontoDepositos"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 3.136!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.OutputFormat = resources.GetString("TextBox2.OutputFormat")
        Me.TextBox2.Text = Nothing
        Me.TextBox2.Top = 0.0!
        Me.TextBox2.Width = 1.797!
        '
        'TextBox1
        '
        Me.TextBox1.DataField = "NumeroCuadre"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 1.45!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat")
        Me.TextBox1.Text = Nothing
        Me.TextBox1.Top = 0.0!
        Me.TextBox1.Width = 1.437!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 1.45!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label3.Text = "N�mero Cuadre"
        Me.Label3.Top = 0.327!
        Me.Label3.Width = 0.9170001!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.TextBox21, Me.Label10, Me.txtTotalProductos})
        Me.PageFooter1.Height = 0.3979167!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'Label13
        '
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 4.708001!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label13.Text = "P�gina"
        Me.Label13.Top = 0.0!
        Me.Label13.Width = 0.5500001!
        '
        'TextBox21
        '
        Me.TextBox21.Height = 0.2!
        Me.TextBox21.Left = 5.270501!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: right"
        Me.TextBox21.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox21.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox21.Text = Nothing
        Me.TextBox21.Top = 0.0!
        Me.TextBox21.Width = 0.3499999!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0.0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold"
        Me.Label10.Text = "Total Depositos:"
        Me.Label10.Top = 0.0!
        Me.Label10.Width = 1.25!
        '
        'txtTotalProductos
        '
        Me.txtTotalProductos.DataField = "NumeroCuadre"
        Me.txtTotalProductos.Height = 0.1979167!
        Me.txtTotalProductos.Left = 1.375!
        Me.txtTotalProductos.Name = "txtTotalProductos"
        Me.txtTotalProductos.Style = "font-weight: bold"
        Me.txtTotalProductos.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.txtTotalProductos.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalProductos.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalProductos.Text = Nothing
        Me.txtTotalProductos.Top = 0.0!
        Me.txtTotalProductos.Width = 1.0!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblSucursalDatos, Me.Label4, Me.Label3, Me.Label2, Me.Line, Me.Label1})
        Me.GroupHeader1.Height = 0.625!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lblSucursalDatos
        '
        Me.lblSucursalDatos.DataField = "NombreAgencia"
        Me.lblSucursalDatos.Height = 0.1875!
        Me.lblSucursalDatos.HyperLink = Nothing
        Me.lblSucursalDatos.Left = 0.625!
        Me.lblSucursalDatos.Name = "lblSucursalDatos"
        Me.lblSucursalDatos.Style = "font-weight: bold; font-size: 8.25pt"
        Me.lblSucursalDatos.Text = ""
        Me.lblSucursalDatos.Top = 0.0!
        Me.lblSucursalDatos.Width = 3.375!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label4.Text = "Sucursal"
        Me.Label4.Top = 0.0!
        Me.Label4.Width = 0.5625!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 3.136!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label2.Text = "Monto"
        Me.Label2.Top = 0.327!
        Me.Label2.Width = 0.917!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox3, Me.TextBox4, Me.Label5})
        Me.GroupFooter1.Height = 0.1875!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.03!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label1.Text = "Fecha Dep�sito:"
        Me.Label1.Top = 0.327!
        Me.Label1.Width = 1.0!
        '
        'txtFechaDeposito
        '
        Me.txtFechaDeposito.DataField = "FechaIngreso"
        Me.txtFechaDeposito.Height = 0.1875!
        Me.txtFechaDeposito.Left = 0.02999997!
        Me.txtFechaDeposito.Name = "txtFechaDeposito"
        Me.txtFechaDeposito.OutputFormat = resources.GetString("txtFechaDeposito.OutputFormat")
        Me.txtFechaDeposito.Text = Nothing
        Me.txtFechaDeposito.Top = 0.0!
        Me.txtFechaDeposito.Width = 1.264!
        '
        'TextBox3
        '
        Me.TextBox3.DataField = "MontoDepositos"
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 3.136!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.OutputFormat = resources.GetString("TextBox3.OutputFormat")
        Me.TextBox3.SummaryGroup = "GroupHeader1"
        Me.TextBox3.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox3.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox3.Text = Nothing
        Me.TextBox3.Top = 0.0!
        Me.TextBox3.Width = 1.797!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0.0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label5.Text = "Totales:"
        Me.Label5.Top = 0.0!
        Me.Label5.Width = 0.49!
        '
        'TextBox4
        '
        Me.TextBox4.DataField = "NumeroCuadre"
        Me.TextBox4.Height = 0.1875!
        Me.TextBox4.Left = 0.625!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.OutputFormat = resources.GetString("TextBox4.OutputFormat")
        Me.TextBox4.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.TextBox4.SummaryGroup = "GroupHeader2"
        Me.TextBox4.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox4.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox4.Text = Nothing
        Me.TextBox4.Top = 0.0!
        Me.TextBox4.Width = 0.497!
        '
        'actRptDetalleDeposito
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 5.760417!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSucursalDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFechaDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label15 As DataDynamics.ActiveReports.Label
    Private WithEvents Label16 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox25 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox26 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label20 As DataDynamics.ActiveReports.Label
    Private WithEvents Line As DataDynamics.ActiveReports.Line
    Private WithEvents Label As DataDynamics.ActiveReports.Label
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Friend WithEvents TextBox2 As DataDynamics.ActiveReports.TextBox
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents Label1 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtFechaDeposito As DataDynamics.ActiveReports.TextBox
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
    Friend WithEvents TextBox1 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents lblSucursalDatos As DataDynamics.ActiveReports.Label
    Friend WithEvents Label4 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label2 As DataDynamics.ActiveReports.Label
    Friend WithEvents TextBox3 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label5 As DataDynamics.ActiveReports.Label
    Friend WithEvents TextBox4 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label13 As DataDynamics.ActiveReports.Label
    Friend WithEvents TextBox21 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label10 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalProductos As DataDynamics.ActiveReports.TextBox
End Class
