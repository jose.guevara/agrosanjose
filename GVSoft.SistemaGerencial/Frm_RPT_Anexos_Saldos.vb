﻿Imports System.Windows.Forms
'Imports Microsoft.Reporting.WinForms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class Frm_RPT_Anexos_Saldos
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "Frm_RPT_Anexos_Saldos"
    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load

        Dim i As Integer = 0
        Dim val_max As Integer = SUConversiones.ConvierteAInt(SIMF_CONTA.numeformulas)
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim dset_anexo_saldos As New DS_Anexos_saldos
        Dim dtCatalogoNivel1 As DataTable = Nothing
        Dim dtCatalogoNivel2 As DataTable = Nothing
        Dim dtCatalogoNivel3 As DataTable = Nothing
        Dim dtCatalogoNivel4 As DataTable = Nothing
        Dim dtAnexosSaldo As DataTable = Nothing
        Try
            dtCatalogoNivel1 = RNCuentaContable.ObtienerCatalogoNivel1()
            dtCatalogoNivel1.TableName = "CataNivel1"
            dset_anexo_saldos.Tables.Add(dtCatalogoNivel1.Clone())

            dtCatalogoNivel2 = RNCuentaContable.ObtienerCatalogoNivel2()
            dtCatalogoNivel2.TableName = "CataNivel2"
            dset_anexo_saldos.Tables.Add(dtCatalogoNivel2.Clone())

            dtCatalogoNivel3 = RNCuentaContable.ObtienerCatalogoNivel3()
            dtCatalogoNivel3.TableName = "CataNivel3"
            dset_anexo_saldos.Tables.Add(dtCatalogoNivel3.Clone())

            dtCatalogoNivel4 = RNCuentaContable.ObtienerCatalogoNivel4()
            dtCatalogoNivel4.TableName = "CataNivel4"
            dset_anexo_saldos.Tables.Add(dtCatalogoNivel4.Clone())

            dtAnexosSaldo = RNCuentaContable.ObtienerAnexosSaldo()
            dtAnexosSaldo.TableName = "anexos_saldos"
            dset_anexo_saldos.Tables.Add(dtAnexosSaldo.Clone())


            Dim report_Anexos_saldos As New RPT_Anexos_Saldos1
            'report_Anexos_saldos.SetDatabaseLogon(SIMF_CONTA.usuario, SIMF_CONTA.pws, SIMF_CONTA.server, SIMF_CONTA.base)

            ' establecer la fórmula de selección de registros
            ' CrystalReportViewer1.ParameterFieldInfo = jackcrystal.lista_parametros
            report_Anexos_saldos.SetDataSource(dset_anexo_saldos)
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_Anexos_saldos.SetParameterValue(nombreformula, valor)
                End If
            Next
            report_Anexos_saldos.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_Anexos_saldos
            Me.CrystalReportViewer1.Refresh()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub Frm_RPT_Anexos_Saldos_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim dataset_rpt_anexos_saldos As New SIMF_CONTADataSet


        ' dataset_rpt_anexos_saldos.
        ' '' ''Dim i As Integer
        ' '' ''Dim val_max As Integer = SIMF_CONTA.numeformulas
        ' '' ''Dim nombreformula, valor As String
        ' '' ''Dim report_Anexos_saldos As New RPT_Anexos_Saldos
        ' '' ''report_Anexos_saldos.SetDatabaseLogon(SIMF_CONTA.usuario, SIMF_CONTA.pws, SIMF_CONTA.server, SIMF_CONTA.base)

        '' '' ''report_Anexos_saldos.SetDataSource(dset)
        ' '' ''For i = 0 To val_max - 1
        ' '' ''    If Not SIMF_CONTA.formula(i) = Nothing Then
        ' '' ''        jackcrystal.retorna_parametro(nombreformula, valor, i)
        ' '' ''        report_Anexos_saldos.SetParameterValue(nombreformula, valor)
        ' '' ''    End If
        ' '' ''Next
        ' '' ''report_Anexos_saldos.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
        '' '' '' asignar el objeto informe al control visualizador
        ' '' ''Me.CrystalReportViewer1.ReportSource = report_Anexos_saldos
        ' '' ''Me.CrystalReportViewer1.Refresh()
    End Sub
End Class