Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptCtasCbrFactPend
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Line4 As DataDynamics.ActiveReports.Line = Nothing
    Private Line5 As DataDynamics.ActiveReports.Line = Nothing
    Private Line7 As DataDynamics.ActiveReports.Line = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private Label10 As DataDynamics.ActiveReports.Label = Nothing
    Private Label11 As DataDynamics.ActiveReports.Label = Nothing
    Private Label12 As DataDynamics.ActiveReports.Label = Nothing
    Private Line6 As DataDynamics.ActiveReports.Line = Nothing
    Private TextBox As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label23 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox26 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
    Private WithEvents Label2 As DataDynamics.ActiveReports.Label
    Private WithEvents txtTotalMonto As DataDynamics.ActiveReports.TextBox
    Private WithEvents txtTotalAbono As DataDynamics.ActiveReports.TextBox
    Private WithEvents Line1 As DataDynamics.ActiveReports.Line
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox10 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox11 As DataDynamics.ActiveReports.TextBox
    Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptCtasCbrFactPend))
        Me.Detail = New DataDynamics.ActiveReports.Detail()
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox6 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox5 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox10 = New DataDynamics.ActiveReports.TextBox()
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Line4 = New DataDynamics.ActiveReports.Line()
        Me.Line5 = New DataDynamics.ActiveReports.Line()
        Me.Line7 = New DataDynamics.ActiveReports.Line()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Line6 = New DataDynamics.ActiveReports.Line()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter()
        Me.Label23 = New DataDynamics.ActiveReports.Label()
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox7 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox8 = New DataDynamics.ActiveReports.TextBox()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.txtTotalMonto = New DataDynamics.ActiveReports.TextBox()
        Me.txtTotalAbono = New DataDynamics.ActiveReports.TextBox()
        Me.Line1 = New DataDynamics.ActiveReports.Line()
        Me.TextBox11 = New DataDynamics.ActiveReports.TextBox()
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader()
        Me.TextBox = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox9 = New DataDynamics.ActiveReports.TextBox()
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalMonto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalAbono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox3, Me.TextBox4, Me.TextBox6, Me.TextBox1, Me.TextBox2, Me.TextBox5, Me.TextBox10})
        Me.Detail.Height = 0.1875!
        Me.Detail.Name = "Detail"
        '
        'TextBox3
        '
        Me.TextBox3.DataField = "numero"
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 0.8125!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Style = "font-size: 9pt; font-family: Arial"
        Me.TextBox3.Text = Nothing
        Me.TextBox3.Top = 0!
        Me.TextBox3.Width = 0.8125!
        '
        'TextBox4
        '
        Me.TextBox4.DataField = "fechaing"
        Me.TextBox4.Height = 0.1875!
        Me.TextBox4.Left = 1.75!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Style = "font-size: 9pt; font-family: Arial"
        Me.TextBox4.Text = Nothing
        Me.TextBox4.Top = 0!
        Me.TextBox4.Width = 0.8125!
        '
        'TextBox6
        '
        Me.TextBox6.DataField = "monto"
        Me.TextBox6.Height = 0.1875!
        Me.TextBox6.Left = 2.625!
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.OutputFormat = resources.GetString("TextBox6.OutputFormat")
        Me.TextBox6.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman"
        Me.TextBox6.Text = Nothing
        Me.TextBox6.Top = 0!
        Me.TextBox6.Width = 0.8125!
        '
        'TextBox1
        '
        Me.TextBox1.DataField = "abono"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 3.5!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.OutputFormat = resources.GetString("TextBox1.OutputFormat")
        Me.TextBox1.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman"
        Me.TextBox1.Text = Nothing
        Me.TextBox1.Top = 0!
        Me.TextBox1.Width = 0.8125!
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "fechaven"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 5.1875!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Style = "font-size: 9pt; font-family: Times New Roman"
        Me.TextBox2.Text = Nothing
        Me.TextBox2.Top = 0!
        Me.TextBox2.Width = 0.8125!
        '
        'TextBox5
        '
        Me.TextBox5.DataField = "vendcodigo"
        Me.TextBox5.Height = 0.1875!
        Me.TextBox5.Left = 6.0625!
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Style = "font-size: 9pt; font-family: Times New Roman"
        Me.TextBox5.Text = Nothing
        Me.TextBox5.Top = 0!
        Me.TextBox5.Width = 0.75!
        '
        'TextBox10
        '
        Me.TextBox10.DataField = "Saldo"
        Me.TextBox10.Height = 0.1875!
        Me.TextBox10.Left = 4.375!
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.OutputFormat = resources.GetString("TextBox10.OutputFormat")
        Me.TextBox10.Style = "text-align: right; font-size: 9pt; font-family: Times New Roman"
        Me.TextBox10.Text = Nothing
        Me.TextBox10.Top = 0!
        Me.TextBox10.Width = 0.75!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label, Me.Label1, Me.Line4, Me.Line5, Me.Line7, Me.Label6, Me.Label7, Me.Label8, Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Line6, Me.Label3})
        Me.PageHeader.Height = 1.256944!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label
        '
        Me.Label.Height = 0.375!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.25!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: center; font-weight: bold; font-size: 20.25pt; font-family: Times New" &
    " Roman"
        Me.Label.Text = "Agro Centro, S.A."
        Me.Label.Top = 0.0625!
        Me.Label.Width = 6.5625!
        '
        'Label1
        '
        Me.Label1.Height = 0.25!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.25!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-align: center; font-weight: bold; font-size: 14.25pt; font-family: Times New" &
    " Roman"
        Me.Label1.Text = "Facturas Pendientes"
        Me.Label1.Top = 0.5625!
        Me.Label1.Width = 6.5625!
        '
        'Line4
        '
        Me.Line4.Height = 0.3125!
        Me.Line4.Left = 0.0625!
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 0.9375!
        Me.Line4.Width = 0!
        Me.Line4.X1 = 0.0625!
        Me.Line4.X2 = 0.0625!
        Me.Line4.Y1 = 0.9375!
        Me.Line4.Y2 = 1.25!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 0.0625!
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 1.25!
        Me.Line5.Width = 6.75!
        Me.Line5.X1 = 6.8125!
        Me.Line5.X2 = 0.0625!
        Me.Line5.Y1 = 1.25!
        Me.Line5.Y2 = 1.25!
        '
        'Line7
        '
        Me.Line7.Height = 0.3125!
        Me.Line7.Left = 6.8125!
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 0.9375!
        Me.Line7.Width = 0!
        Me.Line7.X1 = 6.8125!
        Me.Line7.X2 = 6.8125!
        Me.Line7.Y1 = 0.9375!
        Me.Line7.Y2 = 1.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 0.125!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label6.Text = "Cliente"
        Me.Label6.Top = 1.0!
        Me.Label6.Width = 0.5625!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 1.75!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label7.Text = "Fecha"
        Me.Label7.Top = 1.0!
        Me.Label7.Width = 0.5625!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.8125!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label8.Text = "N�mero"
        Me.Label8.Top = 1.0!
        Me.Label8.Width = 0.5625!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 2.875!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label9.Text = "Monto"
        Me.Label9.Top = 1.0!
        Me.Label9.Width = 0.5625!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 3.625!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label10.Text = "Abonos"
        Me.Label10.Top = 1.0!
        Me.Label10.Width = 0.6875!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 5.25!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "text-align: left; font-weight: bold; font-size: 9pt; font-family: Times New Roman" &
    ""
        Me.Label11.Text = "Fecha venc."
        Me.Label11.Top = 1.0!
        Me.Label11.Width = 0.5625!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 6.0625!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "text-align: left; font-weight: bold; font-size: 9pt; font-family: Times New Roman" &
    ""
        Me.Label12.Text = "Vendedor"
        Me.Label12.Top = 1.0!
        Me.Label12.Width = 0.5625!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 0.0625!
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 0.9375!
        Me.Line6.Width = 6.75!
        Me.Line6.X1 = 6.8125!
        Me.Line6.X2 = 0.0625!
        Me.Line6.Y1 = 0.9375!
        Me.Line6.Y2 = 0.9375!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 4.4375!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.Label3.Text = "Saldo"
        Me.Label3.Top = 1.0!
        Me.Label3.Width = 0.6875!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label23, Me.TextBox26, Me.TextBox7, Me.TextBox8, Me.Label2, Me.txtTotalMonto, Me.txtTotalAbono, Me.Line1, Me.TextBox11})
        Me.PageFooter.Height = 0.84375!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label23
        '
        Me.Label23.Height = 0.2!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 5.5625!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-weight: bold; font-size: 8.25pt; font-family: Times New Roman"
        Me.Label23.Text = "P�gina"
        Me.Label23.Top = 0.375!
        Me.Label23.Width = 0.6125001!
        '
        'TextBox26
        '
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 6.1875!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox26.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox26.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.375!
        Me.TextBox26.Width = 0.375!
        '
        'TextBox7
        '
        Me.TextBox7.Height = 0.2!
        Me.TextBox7.Left = 0.0625!
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Style = "text-align: center; font-style: italic; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox7.Text = "TextBox7"
        Me.TextBox7.Top = 0.375!
        Me.TextBox7.Width = 2.1875!
        '
        'TextBox8
        '
        Me.TextBox8.Height = 0.2!
        Me.TextBox8.Left = 3.0!
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Style = "text-align: center; font-style: italic; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox8.Text = "TextBox8"
        Me.TextBox8.Top = 0.375!
        Me.TextBox8.Width = 1.125!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.125!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 9pt; font-family: Times New Roman"
        Me.Label2.Text = "Totales"
        Me.Label2.Top = 0.0625!
        Me.Label2.Width = 0.5625!
        '
        'txtTotalMonto
        '
        Me.txtTotalMonto.DataField = "monto"
        Me.txtTotalMonto.DistinctField = "monto"
        Me.txtTotalMonto.Height = 0.1875!
        Me.txtTotalMonto.Left = 2.625!
        Me.txtTotalMonto.Name = "txtTotalMonto"
        Me.txtTotalMonto.OutputFormat = resources.GetString("txtTotalMonto.OutputFormat")
        Me.txtTotalMonto.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.txtTotalMonto.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalMonto.Text = Nothing
        Me.txtTotalMonto.Top = 0.0625!
        Me.txtTotalMonto.Width = 0.75!
        '
        'txtTotalAbono
        '
        Me.txtTotalAbono.DataField = "abono"
        Me.txtTotalAbono.Height = 0.1875!
        Me.txtTotalAbono.Left = 3.5!
        Me.txtTotalAbono.Name = "txtTotalAbono"
        Me.txtTotalAbono.OutputFormat = resources.GetString("txtTotalAbono.OutputFormat")
        Me.txtTotalAbono.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.txtTotalAbono.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalAbono.Text = Nothing
        Me.txtTotalAbono.Top = 0.0625!
        Me.txtTotalAbono.Width = 0.75!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 0!
        Me.Line1.Width = 6.8125!
        Me.Line1.X1 = 6.8125!
        Me.Line1.X2 = 0!
        Me.Line1.Y1 = 0!
        Me.Line1.Y2 = 0!
        '
        'TextBox11
        '
        Me.TextBox11.DataField = "Saldo"
        Me.TextBox11.Height = 0.1875!
        Me.TextBox11.Left = 4.375!
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.OutputFormat = resources.GetString("TextBox11.OutputFormat")
        Me.TextBox11.Style = "text-align: right; font-weight: bold; font-size: 9pt; font-family: Times New Roma" &
    "n"
        Me.TextBox11.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox11.Text = Nothing
        Me.TextBox11.Top = 0.0625!
        Me.TextBox11.Width = 0.75!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox, Me.TextBox9})
        Me.GroupHeader1.DataField = "cliente"
        Me.GroupHeader1.Height = 0.1763889!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'TextBox
        '
        Me.TextBox.DataField = "cliente"
        Me.TextBox.Height = 0.1875!
        Me.TextBox.Left = 0.8125!
        Me.TextBox.Name = "TextBox"
        Me.TextBox.Style = "text-decoration: underline; font-weight: bold; font-size: 9pt; font-family: Times" &
    " New Roman"
        Me.TextBox.Text = "TextBox"
        Me.TextBox.Top = 0!
        Me.TextBox.Width = 4.1875!
        '
        'TextBox9
        '
        Me.TextBox9.DataField = "codigo"
        Me.TextBox9.Height = 0.1875!
        Me.TextBox9.Left = 0.125!
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Style = "text-decoration: underline; font-weight: bold; font-size: 9pt; font-family: Times" &
    " New Roman"
        Me.TextBox9.Text = "TextBox9"
        Me.TextBox9.Top = 0!
        Me.TextBox9.Width = 0.625!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.09375!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'actrptCtasCbrFactPend
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.4!
        Me.PageSettings.Margins.Right = 0.3!
        Me.PageSettings.Margins.Top = 0.2!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.03125!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" &
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalMonto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalAbono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptCtasCbrFactPend_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.0F
        Label.Text = strNombEmpresa
        TextBox7.Text = Format(Now, "Long Date")
        TextBox8.Text = Format(Now, "Long Time")
        Select Case intRptCtasCbr
            Case 13
                Label1.Text = "Facturas Pendientes"
            Case 27
                Label1.Text = "Facturas Pendientes de mas de un a�o"
            Case 28
                Label1.Text = "Facturas Pendientes de mas de 4 meses"
        End Select

        Me.Document.Printer.PrinterName = ""


    End Sub

    Private Sub actrptCtasCbrFactPend_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

    End Sub

End Class
