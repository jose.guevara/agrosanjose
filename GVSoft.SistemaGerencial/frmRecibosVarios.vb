Imports System.Data.SqlClient
Imports System.Text

Public Class frmRecibosVarios
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRecibosVarios))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 56)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(152, 24)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(8, 20)
        Me.TextBox6.TabIndex = 39
        Me.TextBox6.Tag = "C�digo del Cliente"
        Me.TextBox6.Text = ""
        Me.TextBox6.Visible = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(304, 120)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(109, 16)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Saldo Pendiente C$"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(224, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 24)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "<F5> AYUDA"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label17.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(344, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 16)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Negocio"
        Me.Label4.Visible = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(168, 24)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(320, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Descripci�n de la cuenta contable"
        Me.TextBox2.Text = ""
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 24)
        Me.TextBox1.MaxLength = 11
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(88, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "C�digo de la Cuenta Contable"
        Me.TextBox1.Text = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 120)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Saldo C$"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(336, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Departamento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Direcci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(31, 31)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Timer1
        '
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(496, 56)
        Me.ToolBar1.TabIndex = 16
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Text = "<F6>"
        Me.ToolBarButton4.ToolTipText = "Extraer datos del Recibo"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.Text = "Cuentas"
        Me.MenuItem15.Visible = False
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 192)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(496, 136)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Descripciones y Conceptos"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(8, 16)
        Me.TextBox5.MaxLength = 7995
        Me.TextBox5.Multiline = True
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox5.Size = New System.Drawing.Size(480, 112)
        Me.TextBox5.TabIndex = 10
        Me.TextBox5.Tag = "Descripci�n/Concepto"
        Me.TextBox5.Text = ""
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 329)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(496, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 38
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        Me.StatusBarPanel1.Width = 120
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 375
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.TextBox3)
        Me.GroupBox3.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox3.Controls.Add(Me.TextBox4)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 120)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(496, 64)
        Me.GroupBox3.TabIndex = 39
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Recibo Varios"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(448, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 16)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Label7"
        Me.Label7.Visible = False
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(64, 24)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(88, 20)
        Me.TextBox3.TabIndex = 9
        Me.TextBox3.Tag = "N�mero del recibo a elaborar"
        Me.TextBox3.Text = ""
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = ""
        Me.DateTimePicker1.Enabled = False
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short
        Me.DateTimePicker1.Location = New System.Drawing.Point(216, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 10
        Me.DateTimePicker1.Tag = "Fecha del Recibo a Elaborar"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(392, 24)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(96, 20)
        Me.TextBox4.TabIndex = 11
        Me.TextBox4.Tag = "Monto del recibo a elaborar"
        Me.TextBox4.Text = "0.00"
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(344, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 16)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Monto"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(168, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 16)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Fecha"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(46, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "N�mero"
        '
        'frmRecibosVarios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(496, 353)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ToolBar1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRecibosVarios"
        Me.Text = "frmRecibosVarios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public txtCollection As New Collection

    Private Sub frmRecibosVarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        txtCollection.Add(TextBox1)
        txtCollection.Add(TextBox2)
        txtCollection.Add(TextBox3)
        txtCollection.Add(TextBox4)
        txtCollection.Add(TextBox5)
        txtCollection.Add(TextBox6)
        If intTipoReciboCaja = 12 Then
            Me.ToolBar1.Buttons.Item(0).Enabled = False
            Me.ToolBar1.Buttons.Item(3).Enabled = False
        End If
        Label7.Text = intTipoReciboCaja
        Limpiar()

    End Sub

    Private Sub frmRecibosVarios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            intTipoReciboCaja = CInt(Label7.Text)
            If intTipoReciboCaja = 8 Then
                If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            ElseIf intTipoReciboCaja = 7 Then
                Guardar()
            End If
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        ElseIf e.KeyCode = Keys.F5 Then
            If Label17.Visible = True Then
                intListadoAyuda = 4
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        ElseIf e.KeyCode = Keys.F6 Then
            intTipoReciboCaja = CInt(Label7.Text)
            Extraer()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem12.Click, MenuItem15.Click

        intListadoAyuda = 0
        intTipoReciboCaja = CInt(Label7.Text)
        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Cuentas" : intListadoAyuda = 4
            Case "&Salir" : Me.Close()
        End Select
        If intListadoAyuda <> 0 Then
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        intTipoReciboCaja = CInt(Label7.Text)
        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                If intTipoReciboCaja = 8 Then
                    If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
                    Else
                        MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                        Exit Sub
                    End If
                    Anular()
                Else
                    Guardar()
                End If
            Case 1, 2
                Limpiar()
            Case 3
                Extraer()
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub Limpiar()

        For intIncr = 1 To 6
            txtCollection.Item(intIncr).text = ""
        Next
        If intTipoReciboCaja = 8 Then
            For intIncr = 1 To 6
                txtCollection.Item(intIncr).readonly = True
            Next
            TextBox1.ReadOnly = False
            TextBox3.ReadOnly = False
        End If
        TextBox4.Text = "0.00"
        TextBox1.Focus()

    End Sub

    Sub Extraer()

        intTipoReciboCaja = CInt(Label7.Text)

        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        If intTipoReciboCaja = 7 Or intTipoReciboCaja = 12 Then
            Dim frmNew As New actrptViewer
            intRptImpRecibos = intTipoReciboCaja
            strQuery = ""
            strQuery = "exec sp_ExtraerReciboRpt " & intTipoReciboCaja & ", " & CInt(TextBox6.Text) & ", "
            strQuery = strQuery + "'" & UCase(TextBox3.Text) & "', 0"
            intRptImpRecibos = intTipoReciboCaja
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        ElseIf intTipoReciboCaja = 8 Then
            Me.Cursor = Cursors.WaitCursor
            TextBox5.Text = ""
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "select * from tbl_RecibosVarios where CtaContable = " & CInt(TextBox6.Text) & " "
            strQuery = strQuery + "And Numero = '" & UCase(TextBox3.Text) & "'"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            Limpiar()
            While dtrAgro2K.Read
                TextBox4.Text = Format(dtrAgro2K.GetValue(5), "#,##0.#0")
                TextBox5.Text = dtrAgro2K.GetValue(6)
                DateTimePicker1.Value = dtrAgro2K.GetValue(9)
                If dtrAgro2K.GetValue(7) = 1 Then
                    dtrAgro2K.Close()
                    MsgBox("El recibo ya fue anulado.", MsgBoxStyle.Information, "Recibo Anulado")
                    Limpiar()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End While
            dtrAgro2K.Close()
            If TextBox5.Text = "" Then
                MsgBox("El n�mero del recibo varios no es v�lido.  Verifique.", MsgBoxStyle.Critical, "Error de Datos")
                Exit Sub
            End If
            Me.Cursor = Cursors.Default
        End If

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox3.GotFocus, TextBox4.GotFocus, TextBox5.GotFocus, DateTimePicker1.GotFocus

        Dim strCampo As String = String.Empty

        MenuItem15.Visible = False
        strCampo = ""
        Select Case sender.name.ToString
            Case "TextBox1" : Label17.Visible = True : strCampo = "C�digo" : MenuItem11.Visible = True : MenuItem15.Visible = True
            Case "TextBox2" : strCampo = "Descripci�n"
            Case "TextBox3" : strCampo = "Recibo"
            Case "TextBox4" : strCampo = "Monto"
            Case "TextBox5" : strCampo = "Concepto"
            Case "DateTimePicker1" : strCampo = "Fecha"
        End Select
        StatusBar1.Panels.Item(0).Text = strCampo
        StatusBar1.Panels.Item(1).Text = sender.tag
        If sender.name <> "DateTimePicker1" Then
            sender.selectall()
        End If

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox3.KeyDown, TextBox4.KeyDown, DateTimePicker1.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "TextBox1" : UbicarCuenta(TextBox1.Text) : TextBox3.Focus()
                Case "TextBox3"
                    TextBox4.Focus()
                    If intTipoReciboCaja = 7 Then
                        UbicarRecibo()
                    ElseIf intTipoReciboCaja <> 7 Then
                        Extraer()
                    End If
                Case "TextBox4" : TextBox5.Focus()
            End Select
        End If

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus

        Label17.Visible = False

    End Sub

    Sub UbicarRecibo()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "select Numero, Estado from tbl_RecibosVarios where "
        strQuery = strQuery + "Numero = '" & UCase(TextBox3.Text) & "'"
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                If dtrAgro2K.GetValue(1) = 0 Then
                    MsgBox("El recibo varios ya fue emitido.", MsgBoxStyle.Critical, "Recibo Varios Emitido")
                ElseIf dtrAgro2K.GetValue(1) = 1 Then
                    MsgBox("El recibo varios ya fue anulado.", MsgBoxStyle.Critical, "Recibo Varios Anulado")
                End If
                TextBox3.Focus()
                TextBox3.Text = ""
                Exit While
            End While
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Function UbicarCuenta(ByVal strDato As String) As Boolean

        Dim strCuenta As String

        TextBox6.Text = "0"
        strCuenta = ""
        strCuenta = strDato
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "select * from prm_CtasContables where cuenta = '" & strCuenta & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                TextBox6.Text = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Cuenta, Descripcion From prm_CtasContables Where registro = " & CInt(TextBox6.Text) & ""
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        Limpiar()
        While dtrAgro2K.Read
            TextBox6.Text = dtrAgro2K.GetValue(0)
            TextBox1.Text = dtrAgro2K.GetValue(1)
            TextBox2.Text = dtrAgro2K.GetValue(2)
        End While
        dtrAgro2K.Close()
        If TextBox2.Text = "" Then
            UbicarCuenta = False
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Function
        End If
        dtrAgro2K.Close()
        UbicarCuenta = True

    End Function

    Sub Guardar()

        blnVistaPrevia = False
        intTipoReciboCaja = CInt(Label7.Text)
        If TextBox6.Text = "0" Then
            MsgBox("Tiene que digitar la cuenta contable para aplicar la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox3.Text = "" Then
            MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox4.Text = "" Then
            MsgBox("Tiene que digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox4.Text = "0.00" Then
            MsgBox("Tiene que digitar el monto del recibo.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If IsNumeric(TextBox4.Text) = False Then
            MsgBox("El monto del recibo debe ser numerico.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If CDbl(TextBox4.Text) <= 0 Then
            MsgBox("El monto del recibo debe ser mayor que 0.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        Dim cmdTmp As New SqlCommand("sp_IngRecibosVarios", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter

        Me.Cursor = Cursors.WaitCursor
        TextBox1.Focus()
        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strFechaFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp03
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With prmTmp04
            .ParameterName = "@lngCuenta"
            .SqlDbType = SqlDbType.SmallInt
            .Value = CLng(TextBox6.Text)
        End With
        With prmTmp05
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox3.Text
        End With
        With prmTmp06
            .ParameterName = "@dblMonto"
            .SqlDbType = SqlDbType.Decimal
            .Value = CDbl(TextBox4.Text)
        End With
        With prmTmp07
            .ParameterName = "@strDescrip01"
            .SqlDbType = SqlDbType.Text
            .Value = TextBox5.Text
        End With
        With prmTmp08
            .ParameterName = "@intAccion"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp09
            .ParameterName = "@dteFecha"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            ImprimirRecibo()
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Anular()

        intTipoReciboCaja = CInt(Label7.Text)
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngRecibosVarios", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        'Dim lngNumFecha As Long

        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strFechaFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp03
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With prmTmp04
            .ParameterName = "@lngCuenta"
            .SqlDbType = SqlDbType.SmallInt
            .Value = CLng(TextBox6.Text)
        End With
        With prmTmp05
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox3.Text
        End With
        With prmTmp06
            .ParameterName = "@dblMonto"
            .SqlDbType = SqlDbType.Decimal
            .Value = CDbl(TextBox4.Text)
        End With
        With prmTmp07
            .ParameterName = "@strDescrip01"
            .SqlDbType = SqlDbType.Text
            .Value = TextBox5.Text
        End With
        With prmTmp08
            .ParameterName = "@intAccion"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 1
        End With
        With prmTmp09
            .ParameterName = "@dteFecha"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
            MsgBox("Recibo Anulado", MsgBoxStyle.Information, "Anulaci�n Satisfactoria")
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub ImprimirRecibo()

        intTipoReciboCaja = CInt(Label7.Text)
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptExportar = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        Dim frmNew As New actrptViewer
        intRptImpRecibos = intTipoReciboCaja
        strQuery = ""
        strQuery = "exec sp_ExtraerReciboRpt " & intRptImpRecibos & ", " & CInt(TextBox6.Text) & ", "
        strQuery = strQuery + "'" & UCase(TextBox3.Text) & "', " & Format(DateTimePicker1.Value, "yyyyMMdd") & ""
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                Select Case intListadoAyuda
                    Case 4 : TextBox1.Text = strUbicar
                        If UbicarCuenta(TextBox1.Text) = True Then
                            TextBox3.Focus()
                        End If
                End Select
            End If
        End If

    End Sub

    Private Sub TextBox4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.LostFocus

        If IsNumeric(sender.text) = True Then
            sender.text = Format(CDbl(sender.text), "#,##0.#0")
        Else
            MsgBox("Tiene que ser un valor num�rico v�lido. Verifique.", MsgBoxStyle.Critical, "Error de Dato")
            sender.focus()
        End If

    End Sub

End Class
