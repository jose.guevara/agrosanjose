Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document

Public Class actrptVtasFact
    Inherits DataDynamics.ActiveReports.ActiveReport
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents ReportHeader As DataDynamics.ActiveReports.ReportHeader = Nothing
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents GroupHeader2 As DataDynamics.ActiveReports.GroupHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents GroupFooter2 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private WithEvents ReportFooter As DataDynamics.ActiveReports.ReportFooter = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private Label15 As DataDynamics.ActiveReports.Label = Nothing
    Private Label16 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox25 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox26 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label18 As DataDynamics.ActiveReports.Label = Nothing
    Private Line As DataDynamics.ActiveReports.Line = Nothing
    Private Line1 As DataDynamics.ActiveReports.Line = Nothing
    Private Label2 As DataDynamics.ActiveReports.Label = Nothing
    Private Label3 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private Label11 As DataDynamics.ActiveReports.Label = Nothing
    Private Label12 As DataDynamics.ActiveReports.Label = Nothing
    Private Label20 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox1 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox2 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox3 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox4 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox5 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox6 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox7 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox8 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox9 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox11 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox12 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox34 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Line3 As DataDynamics.ActiveReports.Line = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox13 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox14 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox15 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox17 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox18 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox19 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox20 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox21 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox23 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox24 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Label19 As DataDynamics.ActiveReports.Label = Nothing
    Private TextBox28 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox29 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox30 As DataDynamics.ActiveReports.TextBox = Nothing
    Private TextBox32 As DataDynamics.ActiveReports.TextBox = Nothing
    Friend WithEvents ReportInfo1 As DataDynamics.ActiveReports.ReportInfo
    Private TextBox33 As DataDynamics.ActiveReports.TextBox = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(actrptVtasFact))
        Me.Detail = New DataDynamics.ActiveReports.Detail()
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox3 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox4 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox5 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox6 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox7 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox8 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox9 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox11 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox12 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox34 = New DataDynamics.ActiveReports.TextBox()
        Me.ReportHeader = New DataDynamics.ActiveReports.ReportHeader()
        Me.ReportFooter = New DataDynamics.ActiveReports.ReportFooter()
        Me.Label19 = New DataDynamics.ActiveReports.Label()
        Me.TextBox28 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox29 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox30 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox32 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox33 = New DataDynamics.ActiveReports.TextBox()
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader()
        Me.Label14 = New DataDynamics.ActiveReports.Label()
        Me.Label15 = New DataDynamics.ActiveReports.Label()
        Me.Label16 = New DataDynamics.ActiveReports.Label()
        Me.TextBox25 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox()
        Me.Label18 = New DataDynamics.ActiveReports.Label()
        Me.Line = New DataDynamics.ActiveReports.Line()
        Me.Line1 = New DataDynamics.ActiveReports.Line()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Label20 = New DataDynamics.ActiveReports.Label()
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter()
        Me.ReportInfo1 = New DataDynamics.ActiveReports.ReportInfo()
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader()
        Me.TextBox = New DataDynamics.ActiveReports.TextBox()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox()
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.TextBox19 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox20 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox23 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox24 = New DataDynamics.ActiveReports.TextBox()
        Me.GroupHeader2 = New DataDynamics.ActiveReports.GroupHeader()
        Me.GroupFooter2 = New DataDynamics.ActiveReports.GroupFooter()
        Me.Line3 = New DataDynamics.ActiveReports.Line()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.TextBox13 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox14 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox15 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox17 = New DataDynamics.ActiveReports.TextBox()
        Me.TextBox18 = New DataDynamics.ActiveReports.TextBox()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox2, Me.TextBox3, Me.TextBox4, Me.TextBox5, Me.TextBox6, Me.TextBox7, Me.TextBox8, Me.TextBox9, Me.TextBox11, Me.TextBox12, Me.TextBox34})
        Me.Detail.Height = 0.15625!
        Me.Detail.Name = "Detail"
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "Numero"
        Me.TextBox2.Height = 0.1875!
        Me.TextBox2.Left = 0!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox2.Text = "TextBox2"
        Me.TextBox2.Top = 0!
        Me.TextBox2.Width = 0.6875!
        '
        'TextBox3
        '
        Me.TextBox3.DataField = "CodigoCliente"
        Me.TextBox3.Height = 0.1875!
        Me.TextBox3.Left = 0.75!
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox3.Text = "TextBox3"
        Me.TextBox3.Top = 0!
        Me.TextBox3.Width = 0.5!
        '
        'TextBox4
        '
        Me.TextBox4.DataField = "Cliente"
        Me.TextBox4.Height = 0.1875!
        Me.TextBox4.Left = 1.3125!
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox4.Text = "TextBox4"
        Me.TextBox4.Top = 0!
        Me.TextBox4.Width = 1.375!
        '
        'TextBox5
        '
        Me.TextBox5.DataField = "Vendedor"
        Me.TextBox5.Height = 0.1875!
        Me.TextBox5.Left = 2.75!
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Style = "text-align: center; font-weight: normal; font-size: 8.25pt; font-family: Times Ne" &
    "w Roman"
        Me.TextBox5.Text = "TextBox5"
        Me.TextBox5.Top = 0!
        Me.TextBox5.Width = 0.4375!
        '
        'TextBox6
        '
        Me.TextBox6.DataField = "FechaIngreso"
        Me.TextBox6.Height = 0.1875!
        Me.TextBox6.Left = 3.3125!
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox6.Text = "TextBox6"
        Me.TextBox6.Top = 0!
        Me.TextBox6.Width = 0.6875!
        '
        'TextBox7
        '
        Me.TextBox7.DataField = "Contado"
        Me.TextBox7.Height = 0.1875!
        Me.TextBox7.Left = 3.875!
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.OutputFormat = resources.GetString("TextBox7.OutputFormat")
        Me.TextBox7.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox7.Text = "TextBox7"
        Me.TextBox7.Top = 0!
        Me.TextBox7.Width = 0.625!
        '
        'TextBox8
        '
        Me.TextBox8.DataField = "Credito"
        Me.TextBox8.Height = 0.1875!
        Me.TextBox8.Left = 4.5625!
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.OutputFormat = resources.GetString("TextBox8.OutputFormat")
        Me.TextBox8.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox8.Text = "TextBox8"
        Me.TextBox8.Top = 0!
        Me.TextBox8.Width = 0.625!
        '
        'TextBox9
        '
        Me.TextBox9.DataField = "Retencion"
        Me.TextBox9.Height = 0.1875!
        Me.TextBox9.Left = 5.3125!
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.OutputFormat = resources.GetString("TextBox9.OutputFormat")
        Me.TextBox9.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox9.Text = "TextBox9"
        Me.TextBox9.Top = 0!
        Me.TextBox9.Width = 0.5!
        '
        'TextBox11
        '
        Me.TextBox11.DataField = "TotCont"
        Me.TextBox11.Height = 0.1875!
        Me.TextBox11.Left = 6.0!
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.OutputFormat = resources.GetString("TextBox11.OutputFormat")
        Me.TextBox11.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox11.Text = "TextBox11"
        Me.TextBox11.Top = 0!
        Me.TextBox11.Width = 0.4375!
        '
        'TextBox12
        '
        Me.TextBox12.DataField = "TotCred"
        Me.TextBox12.Height = 0.1875!
        Me.TextBox12.Left = 6.5!
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.OutputFormat = resources.GetString("TextBox12.OutputFormat")
        Me.TextBox12.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox12.Text = "TextBox12"
        Me.TextBox12.Top = 0!
        Me.TextBox12.Width = 0.5!
        '
        'TextBox34
        '
        Me.TextBox34.DataField = "Anulado"
        Me.TextBox34.Height = 0.1875!
        Me.TextBox34.Left = 0!
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Style = "font-weight: normal; font-size: 8.25pt; font-family: Times New Roman"
        Me.TextBox34.Text = Nothing
        Me.TextBox34.Top = 0!
        Me.TextBox34.Width = 0.125!
        '
        'ReportHeader
        '
        Me.ReportHeader.Height = 0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label19, Me.TextBox28, Me.TextBox29, Me.TextBox30, Me.TextBox32, Me.TextBox33})
        Me.ReportFooter.Height = 0.3222222!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 2.5625!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-weight: bold; font-size: 9pt"
        Me.Label19.Text = "Totales del Reporte"
        Me.Label19.Top = 0!
        Me.Label19.Width = 1.25!
        '
        'TextBox28
        '
        Me.TextBox28.DataField = "Contado"
        Me.TextBox28.Height = 0.1875!
        Me.TextBox28.Left = 3.9375!
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.OutputFormat = resources.GetString("TextBox28.OutputFormat")
        Me.TextBox28.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox28.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox28.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox28.Text = "TextBox28"
        Me.TextBox28.Top = 0!
        Me.TextBox28.Width = 0.5!
        '
        'TextBox29
        '
        Me.TextBox29.DataField = "Credito"
        Me.TextBox29.Height = 0.1875!
        Me.TextBox29.Left = 4.625!
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.OutputFormat = resources.GetString("TextBox29.OutputFormat")
        Me.TextBox29.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox29.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox29.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox29.Text = "TextBox29"
        Me.TextBox29.Top = 0!
        Me.TextBox29.Width = 0.5625!
        '
        'TextBox30
        '
        Me.TextBox30.DataField = "Retencion"
        Me.TextBox30.Height = 0.1875!
        Me.TextBox30.Left = 5.3125!
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.OutputFormat = resources.GetString("TextBox30.OutputFormat")
        Me.TextBox30.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox30.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox30.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox30.Text = "TextBox30"
        Me.TextBox30.Top = 0!
        Me.TextBox30.Width = 0.5!
        '
        'TextBox32
        '
        Me.TextBox32.DataField = "TotCont"
        Me.TextBox32.Height = 0.1875!
        Me.TextBox32.Left = 6.0!
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.OutputFormat = resources.GetString("TextBox32.OutputFormat")
        Me.TextBox32.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox32.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox32.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox32.Text = "TextBox32"
        Me.TextBox32.Top = 0!
        Me.TextBox32.Width = 0.4375!
        '
        'TextBox33
        '
        Me.TextBox33.DataField = "TotCred"
        Me.TextBox33.Height = 0.1875!
        Me.TextBox33.Left = 6.5!
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.OutputFormat = resources.GetString("TextBox33.OutputFormat")
        Me.TextBox33.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox33.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox33.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox33.Text = "TextBox33"
        Me.TextBox33.Top = 0!
        Me.TextBox33.Width = 0.4375!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label14, Me.Label15, Me.Label16, Me.TextBox25, Me.TextBox26, Me.Label18, Me.Line, Me.Line1, Me.Label2, Me.Label3, Me.Label4, Me.Label5, Me.Label6, Me.Label7, Me.Label1, Me.Label8, Me.Label11, Me.Label12, Me.Label20})
        Me.PageHeader.Height = 1.302083!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label14
        '
        Me.Label14.Height = 0.2!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.0625!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label14.Text = "VENTAS POR FACTURAS"
        Me.Label14.Top = 0.25!
        Me.Label14.Width = 3.7495!
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 4.5!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label15.Text = "Fecha de Impresi�n"
        Me.Label15.Top = 0.0625!
        Me.Label15.Width = 1.35!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 4.5!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label16.Text = "Hora de Impresi�n"
        Me.Label16.Top = 0.25!
        Me.Label16.Width = 1.35!
        '
        'TextBox25
        '
        Me.TextBox25.Height = 0.2!
        Me.TextBox25.Left = 5.875!
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Style = "text-align: right"
        Me.TextBox25.Text = "TextBox25"
        Me.TextBox25.Top = 0.0625!
        Me.TextBox25.Width = 0.9!
        '
        'TextBox26
        '
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 5.875!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right"
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.25!
        Me.TextBox26.Width = 0.9!
        '
        'Label18
        '
        Me.Label18.Height = 0.2!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 0.0625!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label18.Text = "INVENTARIO DE CLASES CONSOLIDADO"
        Me.Label18.Top = 0.45!
        Me.Label18.Width = 2.75!
        '
        'Line
        '
        Me.Line.Height = 0!
        Me.Line.Left = 0!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 0.9375!
        Me.Line.Width = 7.0625!
        Me.Line.X1 = 0!
        Me.Line.X2 = 7.0625!
        Me.Line.Y1 = 0.9375!
        Me.Line.Y2 = 0.9375!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0!
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.25!
        Me.Line1.Width = 7.0625!
        Me.Line1.X1 = 0!
        Me.Line1.X2 = 7.0625!
        Me.Line1.Y1 = 1.25!
        Me.Line1.Y2 = 1.25!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label2.Text = "Factura"
        Me.Label2.Top = 1.0!
        Me.Label2.Width = 0.5625!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.8125!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label3.Text = "Cliente"
        Me.Label3.Top = 1.0!
        Me.Label3.Width = 0.4375!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 2.625!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label4.Text = "Vendedor"
        Me.Label4.Top = 1.0!
        Me.Label4.Width = 0.625!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 3.25!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "text-align: center; font-weight: bold; font-size: 8.25pt"
        Me.Label5.Text = "Fecha"
        Me.Label5.Top = 1.0!
        Me.Label5.Width = 0.625!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.8125!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label6.Text = "Contado"
        Me.Label6.Top = 1.0!
        Me.Label6.Width = 0.6875!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 4.5625!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label7.Text = "Cr�dito"
        Me.Label7.Top = 1.0!
        Me.Label7.Width = 0.625!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 1.3125!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold; font-size: 8.25pt"
        Me.Label1.Text = "Nombre del Cliente"
        Me.Label1.Top = 1.0!
        Me.Label1.Width = 1.3125!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 5.1875!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label8.Text = "Retenci�n"
        Me.Label8.Top = 1.0!
        Me.Label8.Width = 0.625!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 5.9375!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label11.Text = "Tot Cont"
        Me.Label11.Top = 1.0!
        Me.Label11.Width = 0.5!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 6.5!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "text-align: right; font-weight: bold; font-size: 8.25pt"
        Me.Label12.Text = "Tot Cred"
        Me.Label12.Top = 1.0!
        Me.Label12.Width = 0.5!
        '
        'Label20
        '
        Me.Label20.Height = 0.2!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.0625!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label20.Text = "REPORTE GENERADO DE LA FACTURACION"
        Me.Label20.Top = 0.0625!
        Me.Label20.Width = 3.0!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.ReportInfo1})
        Me.PageFooter.Height = 0.7694445!
        Me.PageFooter.Name = "PageFooter"
        '
        'ReportInfo1
        '
        Me.ReportInfo1.FormatString = "P�gina {PageNumber} de {PageCount}"
        Me.ReportInfo1.Height = 0.1875!
        Me.ReportInfo1.Left = 5.5625!
        Me.ReportInfo1.Name = "ReportInfo1"
        Me.ReportInfo1.Style = "ddo-char-set: 1; font-weight: bold"
        Me.ReportInfo1.Top = 0.125!
        Me.ReportInfo1.Width = 1.3125!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.TextBox, Me.Label, Me.TextBox1})
        Me.GroupHeader1.DataField = "CodigoSucursal"
        Me.GroupHeader1.Height = 0.25!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'TextBox
        '
        Me.TextBox.DataField = "CodigoSucursal"
        Me.TextBox.Height = 0.1875!
        Me.TextBox.Left = 0.5625!
        Me.TextBox.Name = "TextBox"
        Me.TextBox.Style = "font-weight: bold; font-size: 9pt"
        Me.TextBox.Text = "TextBox"
        Me.TextBox.Top = 0!
        Me.TextBox.Width = 0.375!
        '
        'Label
        '
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-weight: bold; font-size: 9pt"
        Me.Label.Text = "Agencia"
        Me.Label.Top = 0!
        Me.Label.Width = 0.5625!
        '
        'TextBox1
        '
        Me.TextBox1.DataField = "Sucursal"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 0.9375!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Style = "font-weight: bold; font-size: 9pt"
        Me.TextBox1.Text = "TextBox1"
        Me.TextBox1.Top = 0!
        Me.TextBox1.Width = 2.0!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label9, Me.TextBox19, Me.TextBox20, Me.TextBox21, Me.TextBox23, Me.TextBox24})
        Me.GroupFooter1.Height = 0.2597222!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 2.5!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-weight: bold; font-size: 9pt"
        Me.Label9.Text = "Totales de la Agencia"
        Me.Label9.Top = 0!
        Me.Label9.Width = 1.3125!
        '
        'TextBox19
        '
        Me.TextBox19.DataField = "Contado"
        Me.TextBox19.Height = 0.1875!
        Me.TextBox19.Left = 3.8125!
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.OutputFormat = resources.GetString("TextBox19.OutputFormat")
        Me.TextBox19.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox19.SummaryGroup = "GroupHeader1"
        Me.TextBox19.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox19.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox19.Text = "TextBox19"
        Me.TextBox19.Top = 0!
        Me.TextBox19.Width = 0.6875!
        '
        'TextBox20
        '
        Me.TextBox20.DataField = "Credito"
        Me.TextBox20.Height = 0.1875!
        Me.TextBox20.Left = 4.5625!
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.OutputFormat = resources.GetString("TextBox20.OutputFormat")
        Me.TextBox20.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox20.SummaryGroup = "GroupHeader1"
        Me.TextBox20.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox20.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox20.Text = "TextBox20"
        Me.TextBox20.Top = 0!
        Me.TextBox20.Width = 0.625!
        '
        'TextBox21
        '
        Me.TextBox21.DataField = "Retencion"
        Me.TextBox21.Height = 0.1875!
        Me.TextBox21.Left = 5.25!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.OutputFormat = resources.GetString("TextBox21.OutputFormat")
        Me.TextBox21.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox21.SummaryGroup = "GroupHeader1"
        Me.TextBox21.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox21.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox21.Text = "TextBox21"
        Me.TextBox21.Top = 0!
        Me.TextBox21.Width = 0.5625!
        '
        'TextBox23
        '
        Me.TextBox23.DataField = "TotCont"
        Me.TextBox23.Height = 0.1875!
        Me.TextBox23.Left = 6.0!
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.OutputFormat = resources.GetString("TextBox23.OutputFormat")
        Me.TextBox23.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox23.SummaryGroup = "GroupHeader1"
        Me.TextBox23.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox23.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox23.Text = "TextBox23"
        Me.TextBox23.Top = 0!
        Me.TextBox23.Width = 0.4375!
        '
        'TextBox24
        '
        Me.TextBox24.DataField = "TotCred"
        Me.TextBox24.Height = 0.1875!
        Me.TextBox24.Left = 6.4375!
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.OutputFormat = resources.GetString("TextBox24.OutputFormat")
        Me.TextBox24.Style = "text-align: right; font-weight: normal; font-size: 8.25pt; font-family: Times New" &
    " Roman"
        Me.TextBox24.SummaryGroup = "GroupHeader1"
        Me.TextBox24.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox24.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox24.Text = "TextBox24"
        Me.TextBox24.Top = 0!
        Me.TextBox24.Width = 0.5625!
        '
        'GroupHeader2
        '
        Me.GroupHeader2.DataField = "NumFechaIng"
        Me.GroupHeader2.Height = 0!
        Me.GroupHeader2.Name = "GroupHeader2"
        '
        'GroupFooter2
        '
        Me.GroupFooter2.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Line3, Me.Label13, Me.TextBox13, Me.TextBox14, Me.TextBox15, Me.TextBox17, Me.TextBox18})
        Me.GroupFooter2.Height = 0.3333333!
        Me.GroupFooter2.Name = "GroupFooter2"
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 0!
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0!
        Me.Line3.Width = 7.0625!
        Me.Line3.X1 = 0!
        Me.Line3.X2 = 7.0625!
        Me.Line3.Y1 = 0!
        Me.Line3.Y2 = 0!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 2.875!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9pt"
        Me.Label13.Text = "Totales del D�a"
        Me.Label13.Top = 0!
        Me.Label13.Width = 0.9375!
        '
        'TextBox13
        '
        Me.TextBox13.DataField = "Contado"
        Me.TextBox13.Height = 0.1875!
        Me.TextBox13.Left = 3.875!
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.OutputFormat = resources.GetString("TextBox13.OutputFormat")
        Me.TextBox13.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" &
    "oman"
        Me.TextBox13.SummaryGroup = "GroupHeader2"
        Me.TextBox13.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox13.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox13.Text = "TextBox13"
        Me.TextBox13.Top = 0!
        Me.TextBox13.Width = 0.625!
        '
        'TextBox14
        '
        Me.TextBox14.DataField = "Credito"
        Me.TextBox14.Height = 0.1875!
        Me.TextBox14.Left = 4.5625!
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.OutputFormat = resources.GetString("TextBox14.OutputFormat")
        Me.TextBox14.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" &
    "oman"
        Me.TextBox14.SummaryGroup = "GroupHeader2"
        Me.TextBox14.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox14.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox14.Text = "TextBox14"
        Me.TextBox14.Top = 0!
        Me.TextBox14.Width = 0.625!
        '
        'TextBox15
        '
        Me.TextBox15.DataField = "Retencion"
        Me.TextBox15.Height = 0.1875!
        Me.TextBox15.Left = 5.25!
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.OutputFormat = resources.GetString("TextBox15.OutputFormat")
        Me.TextBox15.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" &
    "oman"
        Me.TextBox15.SummaryGroup = "GroupHeader2"
        Me.TextBox15.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox15.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox15.Text = "TextBox15"
        Me.TextBox15.Top = 0!
        Me.TextBox15.Width = 0.5625!
        '
        'TextBox17
        '
        Me.TextBox17.DataField = "TotCont"
        Me.TextBox17.Height = 0.1875!
        Me.TextBox17.Left = 5.9375!
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.OutputFormat = resources.GetString("TextBox17.OutputFormat")
        Me.TextBox17.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" &
    "oman"
        Me.TextBox17.SummaryGroup = "GroupHeader2"
        Me.TextBox17.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox17.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox17.Text = "TextBox17"
        Me.TextBox17.Top = 0!
        Me.TextBox17.Width = 0.5!
        '
        'TextBox18
        '
        Me.TextBox18.DataField = "TotCred"
        Me.TextBox18.Height = 0.1875!
        Me.TextBox18.Left = 6.4375!
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.OutputFormat = resources.GetString("TextBox18.OutputFormat")
        Me.TextBox18.Style = "text-align: right; font-weight: bold; font-size: 8.25pt; font-family: Times New R" &
    "oman"
        Me.TextBox18.SummaryGroup = "GroupHeader2"
        Me.TextBox18.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.Group
        Me.TextBox18.SummaryType = DataDynamics.ActiveReports.SummaryType.SubTotal
        Me.TextBox18.Text = "TextBox18"
        Me.TextBox18.Top = 0!
        Me.TextBox18.Width = 0.5625!
        '
        'actrptVtasFact
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.2!
        Me.PageSettings.Margins.Right = 0.2!
        Me.PageSettings.Margins.Top = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.083333!
        Me.Sections.Add(Me.ReportHeader)
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.GroupHeader2)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter2)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter)
        Me.Sections.Add(Me.ReportFooter)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" &
            "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" &
            "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"))
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportInfo1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub actrptVtasFact_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

    End Sub

    Private Sub actrptVtasFact_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 7.0F
        TextBox25.Text = Format(Now, "dd-MMM-yyyy")
        TextBox26.Text = Format(Now, "hh:mm:ss tt")
        Me.Label18.Text = strFechaReporte
        If intRptFactura = 62 Then
            Label14.Text = "Facturas Emitidas a Clientes "
        End If
        '  If intRptFactura = 15 Then
        'GroupFooter1.Visible = False
        '    GroupFooter2.Visible = False
        '    ReportFooter.Visible = False
        ' End If

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub PageFooter_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageFooter.Format

    End Sub
End Class
