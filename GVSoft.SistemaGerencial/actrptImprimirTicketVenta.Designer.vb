<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actrptImprimirTicketVenta
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actrptImprimirTicketVenta))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.Label136 = New DataDynamics.ActiveReports.Label
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.Label3 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label117 = New DataDynamics.ActiveReports.Label
        Me.Label118 = New DataDynamics.ActiveReports.Label
        Me.Label123 = New DataDynamics.ActiveReports.Label
        Me.Label115 = New DataDynamics.ActiveReports.Label
        Me.Label114 = New DataDynamics.ActiveReports.Label
        Me.codig = New DataDynamics.ActiveReports.Label
        Me.Label116 = New DataDynamics.ActiveReports.Label
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.Label124 = New DataDynamics.ActiveReports.Label
        Me.Label125 = New DataDynamics.ActiveReports.Label
        Me.Label126 = New DataDynamics.ActiveReports.Label
        Me.Label107 = New DataDynamics.ActiveReports.Label
        Me.Line1 = New DataDynamics.ActiveReports.Line
        Me.Label127 = New DataDynamics.ActiveReports.Label
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.Label137 = New DataDynamics.ActiveReports.Label
        Me.Label138 = New DataDynamics.ActiveReports.Label
        Me.Detail1 = New DataDynamics.ActiveReports.Detail
        Me.Label6 = New DataDynamics.ActiveReports.Label
        Me.Label8 = New DataDynamics.ActiveReports.Label
        Me.Label9 = New DataDynamics.ActiveReports.Label
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.Label11 = New DataDynamics.ActiveReports.Label
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.Label20 = New DataDynamics.ActiveReports.Label
        Me.Label23 = New DataDynamics.ActiveReports.Label
        Me.Label27 = New DataDynamics.ActiveReports.Label
        Me.Label30 = New DataDynamics.ActiveReports.Label
        Me.Label34 = New DataDynamics.ActiveReports.Label
        Me.Label37 = New DataDynamics.ActiveReports.Label
        Me.Label41 = New DataDynamics.ActiveReports.Label
        Me.Label44 = New DataDynamics.ActiveReports.Label
        Me.Label48 = New DataDynamics.ActiveReports.Label
        Me.Label51 = New DataDynamics.ActiveReports.Label
        Me.Label55 = New DataDynamics.ActiveReports.Label
        Me.Label58 = New DataDynamics.ActiveReports.Label
        Me.Label62 = New DataDynamics.ActiveReports.Label
        Me.Label65 = New DataDynamics.ActiveReports.Label
        Me.Label69 = New DataDynamics.ActiveReports.Label
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.Label72 = New DataDynamics.ActiveReports.Label
        Me.Label76 = New DataDynamics.ActiveReports.Label
        Me.Label79 = New DataDynamics.ActiveReports.Label
        Me.Label83 = New DataDynamics.ActiveReports.Label
        Me.Label86 = New DataDynamics.ActiveReports.Label
        Me.Label90 = New DataDynamics.ActiveReports.Label
        Me.Label93 = New DataDynamics.ActiveReports.Label
        Me.Label97 = New DataDynamics.ActiveReports.Label
        Me.Label100 = New DataDynamics.ActiveReports.Label
        Me.Label104 = New DataDynamics.ActiveReports.Label
        Me.Label105 = New DataDynamics.ActiveReports.Label
        Me.Label108 = New DataDynamics.ActiveReports.Label
        Me.Label111 = New DataDynamics.ActiveReports.Label
        Me.Label112 = New DataDynamics.ActiveReports.Label
        Me.Label120 = New DataDynamics.ActiveReports.Label
        Me.Label121 = New DataDynamics.ActiveReports.Label
        Me.Label7 = New DataDynamics.ActiveReports.Label
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter
        Me.Label103 = New DataDynamics.ActiveReports.Label
        Me.Label106 = New DataDynamics.ActiveReports.Label
        Me.Label109 = New DataDynamics.ActiveReports.Label
        Me.Label110 = New DataDynamics.ActiveReports.Label
        Me.Label113 = New DataDynamics.ActiveReports.Label
        Me.Label119 = New DataDynamics.ActiveReports.Label
        Me.Label122 = New DataDynamics.ActiveReports.Label
        Me.Line2 = New DataDynamics.ActiveReports.Line
        Me.Label128 = New DataDynamics.ActiveReports.Label
        Me.Label130 = New DataDynamics.ActiveReports.Label
        Me.Label131 = New DataDynamics.ActiveReports.Label
        Me.lblPago = New DataDynamics.ActiveReports.Label
        Me.Label133 = New DataDynamics.ActiveReports.Label
        Me.lblCambio = New DataDynamics.ActiveReports.Label
        Me.Label135 = New DataDynamics.ActiveReports.Label
        Me.Line3 = New DataDynamics.ActiveReports.Line
        Me.TextBox1 = New DataDynamics.ActiveReports.TextBox
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.codig, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label126, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label137, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label138, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPago, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCambio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label136, Me.Label2, Me.Label3, Me.Label4, Me.Label117, Me.Label118, Me.Label123, Me.Label115, Me.Label114, Me.codig, Me.Label116, Me.Label, Me.Label124, Me.Label125, Me.Label126, Me.Label107, Me.Line1, Me.Label127, Me.Label1, Me.Label137, Me.Label138})
        Me.PageHeader1.Height = 1.729167!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Label136
        '
        Me.Label136.Height = 0.1875!
        Me.Label136.HyperLink = Nothing
        Me.Label136.Left = 0.125!
        Me.Label136.Name = "Label136"
        Me.Label136.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label136.Text = "Direccion"
        Me.Label136.Top = 0.1875!
        Me.Label136.Width = 2.5!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.4375!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label2.Text = "Label2"
        Me.Label2.Top = 0.9375!
        Me.Label2.Width = 0.3125!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.75!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label3.Text = "Label3"
        Me.Label3.Top = 0.9375!
        Me.Label3.Width = 0.3125!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 1.0625!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label4.Text = "Label4"
        Me.Label4.Top = 0.9375!
        Me.Label4.Width = 0.4025!
        '
        'Label117
        '
        Me.Label117.Height = 0.1875!
        Me.Label117.HyperLink = Nothing
        Me.Label117.Left = 5.3125!
        Me.Label117.Name = "Label117"
        Me.Label117.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label117.Text = "P/U"
        Me.Label117.Top = 0.9375!
        Me.Label117.Visible = False
        Me.Label117.Width = 0.375!
        '
        'Label118
        '
        Me.Label118.Height = 0.1875!
        Me.Label118.HyperLink = Nothing
        Me.Label118.Left = 2.1875!
        Me.Label118.Name = "Label118"
        Me.Label118.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label118.Text = "Total"
        Me.Label118.Top = 1.5!
        Me.Label118.Width = 0.4375!
        '
        'Label123
        '
        Me.Label123.Height = 0.1875!
        Me.Label123.HyperLink = Nothing
        Me.Label123.Left = 0.125!
        Me.Label123.Name = "Label123"
        Me.Label123.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label123.Text = "Fecha:"
        Me.Label123.Top = 0.9375!
        Me.Label123.Width = 0.3125!
        '
        'Label115
        '
        Me.Label115.Height = 0.1875!
        Me.Label115.HyperLink = Nothing
        Me.Label115.Left = 0.625!
        Me.Label115.Name = "Label115"
        Me.Label115.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label115.Text = "Descripcion"
        Me.Label115.Top = 1.5!
        Me.Label115.Width = 1.0!
        '
        'Label114
        '
        Me.Label114.Height = 0.1875!
        Me.Label114.HyperLink = Nothing
        Me.Label114.Left = 3.625!
        Me.Label114.Name = "Label114"
        Me.Label114.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label114.Text = "Empaque"
        Me.Label114.Top = 1.1875!
        Me.Label114.Visible = False
        Me.Label114.Width = 0.625!
        '
        'codig
        '
        Me.codig.Height = 0.1875!
        Me.codig.HyperLink = Nothing
        Me.codig.Left = 0.125!
        Me.codig.Name = "codig"
        Me.codig.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.codig.Text = "Codigo"
        Me.codig.Top = 1.5!
        Me.codig.Width = 0.5!
        '
        'Label116
        '
        Me.Label116.Height = 0.1875!
        Me.Label116.HyperLink = Nothing
        Me.Label116.Left = 1.8125!
        Me.Label116.Name = "Label116"
        Me.Label116.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label116.Text = "Cant."
        Me.Label116.Top = 1.5!
        Me.Label116.Width = 0.375!
        '
        'Label
        '
        Me.Label.DataField = "CliNombre"
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.625!
        Me.Label.Name = "Label"
        Me.Label.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label.Text = "Label"
        Me.Label.Top = 1.3125!
        Me.Label.Width = 2.0!
        '
        'Label124
        '
        Me.Label124.Height = 0.1875!
        Me.Label124.HyperLink = Nothing
        Me.Label124.Left = 0.125!
        Me.Label124.Name = "Label124"
        Me.Label124.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label124.Text = "Cajero"
        Me.Label124.Top = 1.125!
        Me.Label124.Width = 0.5!
        '
        'Label125
        '
        Me.Label125.Height = 0.1875!
        Me.Label125.HyperLink = Nothing
        Me.Label125.Left = 0.125!
        Me.Label125.Name = "Label125"
        Me.Label125.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label125.Text = "Cliente"
        Me.Label125.Top = 1.3125!
        Me.Label125.Width = 0.5!
        '
        'Label126
        '
        Me.Label126.Height = 0.1875!
        Me.Label126.HyperLink = Nothing
        Me.Label126.Left = 0.125!
        Me.Label126.Name = "Label126"
        Me.Label126.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label126.Text = "Fact."
        Me.Label126.Top = 0.75!
        Me.Label126.Width = 0.375!
        '
        'Label107
        '
        Me.Label107.Height = 0.1875!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 0.5!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "ddo-char-set: 0; text-align: right; font-size: 9.75pt; font-family: Arial"
        Me.Label107.Text = "Label107"
        Me.Label107.Top = 0.75!
        Me.Label107.Width = 0.6875!
        '
        'Line1
        '
        Me.Line1.Height = 0.0!
        Me.Line1.Left = 0.0!
        Me.Line1.LineStyle = DataDynamics.ActiveReports.LineStyle.Dash
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 1.6875!
        Me.Line1.Width = 2.75!
        Me.Line1.X1 = 0.0!
        Me.Line1.X2 = 2.75!
        Me.Line1.Y1 = 1.6875!
        Me.Line1.Y2 = 1.6875!
        '
        'Label127
        '
        Me.Label127.Height = 0.1875!
        Me.Label127.HyperLink = Nothing
        Me.Label127.Left = 0.125!
        Me.Label127.Name = "Label127"
        Me.Label127.Style = "ddo-char-set: 0; text-align: center; font-weight: normal; font-size: 9.75pt; font" & _
            "-family: Arial"
        Me.Label127.Text = "Nombre Empresa"
        Me.Label127.Top = 0.0!
        Me.Label127.Width = 2.5!
        '
        'Label1
        '
        Me.Label1.DataField = "Nombre"
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.625!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "ddo-char-set: 0; font-size: 9.75pt; font-family: Arial"
        Me.Label1.Text = "Label1"
        Me.Label1.Top = 1.125!
        Me.Label1.Width = 2.0!
        '
        'Label137
        '
        Me.Label137.Height = 0.1875!
        Me.Label137.HyperLink = Nothing
        Me.Label137.Left = 0.125!
        Me.Label137.Name = "Label137"
        Me.Label137.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label137.Text = "Telefono"
        Me.Label137.Top = 0.375!
        Me.Label137.Width = 2.5!
        '
        'Label138
        '
        Me.Label138.Height = 0.1875!
        Me.Label138.HyperLink = Nothing
        Me.Label138.Left = 0.125!
        Me.Label138.Name = "Label138"
        Me.Label138.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Arial"
        Me.Label138.Text = "RUC"
        Me.Label138.Top = 0.5625!
        Me.Label138.Width = 2.5!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0.0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label6, Me.Label8, Me.Label9, Me.Label10, Me.Label11, Me.Label13, Me.Label16, Me.Label20, Me.Label23, Me.Label27, Me.Label30, Me.Label34, Me.Label37, Me.Label41, Me.Label44, Me.Label48, Me.Label51, Me.Label55, Me.Label58, Me.Label62, Me.Label65, Me.Label69, Me.Label5, Me.Label72, Me.Label76, Me.Label79, Me.Label83, Me.Label86, Me.Label90, Me.Label93, Me.Label97, Me.Label100, Me.Label104, Me.Label105, Me.Label108, Me.Label111, Me.Label112, Me.Label120, Me.Label121, Me.Label7})
        Me.Detail1.Height = 0.1770833!
        Me.Detail1.Name = "Detail1"
        '
        'Label6
        '
        Me.Label6.Height = 0.1375!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.625!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label6.Text = ""
        Me.Label6.Top = 0.0!
        Me.Label6.Visible = False
        Me.Label6.Width = 0.5625!
        '
        'Label8
        '
        Me.Label8.DataField = "Descripcion"
        Me.Label8.Height = 0.15!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.5625!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label8.Text = ""
        Me.Label8.Top = 0.0!
        Me.Label8.Width = 1.25!
        '
        'Label9
        '
        Me.Label9.Height = 0.1375!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 5.3125!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label9.Text = ""
        Me.Label9.Top = 0.0!
        Me.Label9.Visible = False
        Me.Label9.Width = 0.875!
        '
        'Label10
        '
        Me.Label10.DataField = "Valor"
        Me.Label10.Height = 0.15!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 2.125!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label10.Text = ""
        Me.Label10.Top = 0.0!
        Me.Label10.Width = 0.5!
        '
        'Label11
        '
        Me.Label11.DataField = "Impuesto"
        Me.Label11.Height = 0.15!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 2.625!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label11.Text = ""
        Me.Label11.Top = 0.0!
        Me.Label11.Visible = False
        Me.Label11.Width = 0.125!
        '
        'Label13
        '
        Me.Label13.Height = 0.15!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 3.625!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label13.Text = ""
        Me.Label13.Top = 0.125!
        Me.Label13.Visible = False
        Me.Label13.Width = 0.5625!
        '
        'Label16
        '
        Me.Label16.Height = 0.15!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 5.3125!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label16.Text = ""
        Me.Label16.Top = 0.15!
        Me.Label16.Visible = False
        Me.Label16.Width = 0.875!
        '
        'Label20
        '
        Me.Label20.Height = 0.15!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 3.625!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label20.Text = ""
        Me.Label20.Top = 0.25!
        Me.Label20.Visible = False
        Me.Label20.Width = 0.5625!
        '
        'Label23
        '
        Me.Label23.Height = 0.15!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 5.3125!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label23.Text = ""
        Me.Label23.Top = 0.2895833!
        Me.Label23.Visible = False
        Me.Label23.Width = 0.875!
        '
        'Label27
        '
        Me.Label27.Height = 0.15!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.625!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label27.Text = ""
        Me.Label27.Top = 0.4375!
        Me.Label27.Visible = False
        Me.Label27.Width = 0.5625!
        '
        'Label30
        '
        Me.Label30.Height = 0.15!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 5.3125!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label30.Text = ""
        Me.Label30.Top = 0.4499999!
        Me.Label30.Visible = False
        Me.Label30.Width = 0.875!
        '
        'Label34
        '
        Me.Label34.Height = 0.1666666!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 3.625!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label34.Text = ""
        Me.Label34.Top = 0.5625!
        Me.Label34.Visible = False
        Me.Label34.Width = 0.5625!
        '
        'Label37
        '
        Me.Label37.Height = 0.1666666!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 5.3125!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label37.Text = ""
        Me.Label37.Top = 0.6!
        Me.Label37.Visible = False
        Me.Label37.Width = 0.875!
        '
        'Label41
        '
        Me.Label41.Height = 0.15!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 3.625!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label41.Text = ""
        Me.Label41.Top = 0.75!
        Me.Label41.Visible = False
        Me.Label41.Width = 0.5625!
        '
        'Label44
        '
        Me.Label44.Height = 0.15!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 5.3125!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label44.Text = ""
        Me.Label44.Top = 0.75!
        Me.Label44.Visible = False
        Me.Label44.Width = 0.875!
        '
        'Label48
        '
        Me.Label48.Height = 0.1625!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 3.625!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label48.Text = ""
        Me.Label48.Top = 0.875!
        Me.Label48.Visible = False
        Me.Label48.Width = 0.5625!
        '
        'Label51
        '
        Me.Label51.Height = 0.1625!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 5.3125!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label51.Text = ""
        Me.Label51.Top = 0.9!
        Me.Label51.Visible = False
        Me.Label51.Width = 0.875!
        '
        'Label55
        '
        Me.Label55.Height = 0.15!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 3.625!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label55.Text = ""
        Me.Label55.Top = 1.0!
        Me.Label55.Visible = False
        Me.Label55.Width = 0.5625!
        '
        'Label58
        '
        Me.Label58.Height = 0.15!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 5.3125!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label58.Text = ""
        Me.Label58.Top = 1.05!
        Me.Label58.Visible = False
        Me.Label58.Width = 0.875!
        '
        'Label62
        '
        Me.Label62.Height = 0.15!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 3.625!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label62.Text = ""
        Me.Label62.Top = 1.1875!
        Me.Label62.Visible = False
        Me.Label62.Width = 0.5625!
        '
        'Label65
        '
        Me.Label65.Height = 0.15!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 5.3125!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label65.Text = ""
        Me.Label65.Top = 1.2!
        Me.Label65.Visible = False
        Me.Label65.Width = 0.875!
        '
        'Label69
        '
        Me.Label69.Height = 0.15!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 3.625!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label69.Text = ""
        Me.Label69.Top = 1.3125!
        Me.Label69.Visible = False
        Me.Label69.Width = 0.5625!
        '
        'Label5
        '
        Me.Label5.DataField = "Cantidad"
        Me.Label5.Height = 0.15!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 1.8125!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label5.Text = ""
        Me.Label5.Top = 0.0!
        Me.Label5.Width = 0.3125!
        '
        'Label72
        '
        Me.Label72.Height = 0.15!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 5.3125!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label72.Text = ""
        Me.Label72.Top = 1.35!
        Me.Label72.Visible = False
        Me.Label72.Width = 0.875!
        '
        'Label76
        '
        Me.Label76.Height = 0.15!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 3.625!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label76.Text = ""
        Me.Label76.Top = 1.5!
        Me.Label76.Visible = False
        Me.Label76.Width = 0.5625!
        '
        'Label79
        '
        Me.Label79.Height = 0.15!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 5.3125!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label79.Text = ""
        Me.Label79.Top = 1.5!
        Me.Label79.Visible = False
        Me.Label79.Width = 0.875!
        '
        'Label83
        '
        Me.Label83.Height = 0.15!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 3.625!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label83.Text = ""
        Me.Label83.Top = 1.625!
        Me.Label83.Visible = False
        Me.Label83.Width = 0.5625!
        '
        'Label86
        '
        Me.Label86.Height = 0.15!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 5.3125!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label86.Text = ""
        Me.Label86.Top = 1.65!
        Me.Label86.Visible = False
        Me.Label86.Width = 0.875!
        '
        'Label90
        '
        Me.Label90.Height = 0.15!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 3.625!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label90.Text = ""
        Me.Label90.Top = 1.791667!
        Me.Label90.Visible = False
        Me.Label90.Width = 0.5625!
        '
        'Label93
        '
        Me.Label93.Height = 0.15!
        Me.Label93.HyperLink = Nothing
        Me.Label93.Left = 5.3125!
        Me.Label93.Name = "Label93"
        Me.Label93.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label93.Text = ""
        Me.Label93.Top = 1.854167!
        Me.Label93.Visible = False
        Me.Label93.Width = 0.875!
        '
        'Label97
        '
        Me.Label97.Height = 0.15!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 3.625!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label97.Text = ""
        Me.Label97.Top = 1.979167!
        Me.Label97.Visible = False
        Me.Label97.Width = 0.5625!
        '
        'Label100
        '
        Me.Label100.Height = 0.15!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 5.3125!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label100.Text = ""
        Me.Label100.Top = 1.979167!
        Me.Label100.Visible = False
        Me.Label100.Width = 0.875!
        '
        'Label104
        '
        Me.Label104.Height = 0.1875!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 4.4375!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label104.Text = ""
        Me.Label104.Top = 2.375!
        Me.Label104.Visible = False
        Me.Label104.Width = 0.875!
        '
        'Label105
        '
        Me.Label105.Height = 0.1875!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 4.4375!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label105.Text = ""
        Me.Label105.Top = 2.625!
        Me.Label105.Visible = False
        Me.Label105.Width = 0.875!
        '
        'Label108
        '
        Me.Label108.Height = 0.15!
        Me.Label108.HyperLink = Nothing
        Me.Label108.Left = 3.25!
        Me.Label108.Name = "Label108"
        Me.Label108.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label108.Text = ""
        Me.Label108.Top = 3.125!
        Me.Label108.Visible = False
        Me.Label108.Width = 0.875!
        '
        'Label111
        '
        Me.Label111.Height = 0.1875!
        Me.Label111.HyperLink = Nothing
        Me.Label111.Left = 4.1875!
        Me.Label111.Name = "Label111"
        Me.Label111.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label111.Text = "C$"
        Me.Label111.Top = 2.375!
        Me.Label111.Visible = False
        Me.Label111.Width = 0.25!
        '
        'Label112
        '
        Me.Label112.Height = 0.1875!
        Me.Label112.HyperLink = Nothing
        Me.Label112.Left = 4.1875!
        Me.Label112.Name = "Label112"
        Me.Label112.Style = "ddo-char-set: 0; text-align: right; font-size: 9pt; font-family: Arial"
        Me.Label112.Text = "C$"
        Me.Label112.Top = 2.625!
        Me.Label112.Visible = False
        Me.Label112.Width = 0.25!
        '
        'Label120
        '
        Me.Label120.Height = 0.1875!
        Me.Label120.HyperLink = Nothing
        Me.Label120.Left = 3.4375!
        Me.Label120.Name = "Label120"
        Me.Label120.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label120.Text = "Impuesto"
        Me.Label120.Top = 2.375!
        Me.Label120.Visible = False
        Me.Label120.Width = 0.625!
        '
        'Label121
        '
        Me.Label121.Height = 0.1875!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 3.4375!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label121.Text = "Retencion"
        Me.Label121.Top = 2.625!
        Me.Label121.Visible = False
        Me.Label121.Width = 0.625!
        '
        'Label7
        '
        Me.Label7.DataField = "Codigo"
        Me.Label7.Height = 0.15!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.125!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial"
        Me.Label7.Text = ""
        Me.Label7.Top = 0.0!
        Me.Label7.Width = 0.4375!
        '
        'PageFooter1
        '
        Me.PageFooter1.Height = 0.2291667!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Height = 0.0!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label103, Me.Label106, Me.Label109, Me.Label110, Me.Label113, Me.Label119, Me.Label122, Me.Line2, Me.Label128, Me.Label130, Me.Label131, Me.lblPago, Me.Label133, Me.lblCambio, Me.Label135, Me.Line3, Me.TextBox1})
        Me.GroupFooter1.Height = 1.3125!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'Label103
        '
        Me.Label103.Height = 0.1875!
        Me.Label103.HyperLink = Nothing
        Me.Label103.Left = 2.125!
        Me.Label103.Name = "Label103"
        Me.Label103.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label103.Text = ""
        Me.Label103.Top = 0.0!
        Me.Label103.Width = 0.5!
        '
        'Label106
        '
        Me.Label106.Height = 0.1875!
        Me.Label106.HyperLink = Nothing
        Me.Label106.Left = 2.125!
        Me.Label106.Name = "Label106"
        Me.Label106.Style = "ddo-char-set: 0; text-align: right; font-weight: normal; font-size: 8.25pt; font-" & _
            "family: Arial"
        Me.Label106.Text = ""
        Me.Label106.Top = 0.3125!
        Me.Label106.Width = 0.5!
        '
        'Label109
        '
        Me.Label109.Height = 0.125!
        Me.Label109.HyperLink = Nothing
        Me.Label109.Left = 0.0!
        Me.Label109.Name = "Label109"
        Me.Label109.Style = "ddo-char-set: 0; text-align: left; font-weight: normal; font-size: 8.25pt; font-f" & _
            "amily: Arial"
        Me.Label109.Text = "T/C"
        Me.Label109.Top = 0.3125!
        Me.Label109.Visible = False
        Me.Label109.Width = 0.75!
        '
        'Label110
        '
        Me.Label110.Height = 0.1875!
        Me.Label110.HyperLink = Nothing
        Me.Label110.Left = 1.875!
        Me.Label110.Name = "Label110"
        Me.Label110.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label110.Text = "C$"
        Me.Label110.Top = 0.0!
        Me.Label110.Width = 0.25!
        '
        'Label113
        '
        Me.Label113.Height = 0.1875!
        Me.Label113.HyperLink = Nothing
        Me.Label113.Left = 1.875!
        Me.Label113.Name = "Label113"
        Me.Label113.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label113.Text = "C$"
        Me.Label113.Top = 0.3125!
        Me.Label113.Width = 0.25!
        '
        'Label119
        '
        Me.Label119.Height = 0.1875!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 1.25!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "ddo-char-set: 0; font-size: 8.25pt; font-family: Arial"
        Me.Label119.Text = "Sub Total"
        Me.Label119.Top = 0.0!
        Me.Label119.Width = 0.625!
        '
        'Label122
        '
        Me.Label122.Height = 0.1875!
        Me.Label122.HyperLink = Nothing
        Me.Label122.Left = 1.4375!
        Me.Label122.Name = "Label122"
        Me.Label122.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label122.Text = "Total"
        Me.Label122.Top = 0.3125!
        Me.Label122.Width = 0.4375!
        '
        'Line2
        '
        Me.Line2.Height = 0.0!
        Me.Line2.Left = 1.9375!
        Me.Line2.LineStyle = DataDynamics.ActiveReports.LineStyle.Dash
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 0.25!
        Me.Line2.Width = 0.6875!
        Me.Line2.X1 = 2.625!
        Me.Line2.X2 = 1.9375!
        Me.Line2.Y1 = 0.25!
        Me.Line2.Y2 = 0.25!
        '
        'Label128
        '
        Me.Label128.Height = 0.1875!
        Me.Label128.HyperLink = Nothing
        Me.Label128.Left = 0.125!
        Me.Label128.Name = "Label128"
        Me.Label128.Style = "ddo-char-set: 0; font-size: 8.25pt; font-family: Arial"
        Me.Label128.Text = "Total de Articulos"
        Me.Label128.Top = 1.125!
        Me.Label128.Width = 1.125!
        '
        'Label130
        '
        Me.Label130.Height = 0.1875!
        Me.Label130.HyperLink = Nothing
        Me.Label130.Left = 1.5!
        Me.Label130.Name = "Label130"
        Me.Label130.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label130.Text = "Pago:"
        Me.Label130.Top = 0.625!
        Me.Label130.Width = 0.375!
        '
        'Label131
        '
        Me.Label131.Height = 0.1875!
        Me.Label131.HyperLink = Nothing
        Me.Label131.Left = 1.375!
        Me.Label131.Name = "Label131"
        Me.Label131.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label131.Text = "Cambio:"
        Me.Label131.Top = 0.875!
        Me.Label131.Width = 0.5!
        '
        'lblPago
        '
        Me.lblPago.Height = 0.1875!
        Me.lblPago.HyperLink = Nothing
        Me.lblPago.Left = 2.125!
        Me.lblPago.Name = "lblPago"
        Me.lblPago.Style = "ddo-char-set: 0; text-align: right; font-weight: normal; font-size: 8.25pt; font-" & _
            "family: Arial"
        Me.lblPago.Text = ""
        Me.lblPago.Top = 0.625!
        Me.lblPago.Width = 0.5!
        '
        'Label133
        '
        Me.Label133.Height = 0.1875!
        Me.Label133.HyperLink = Nothing
        Me.Label133.Left = 1.875!
        Me.Label133.Name = "Label133"
        Me.Label133.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label133.Text = "C$"
        Me.Label133.Top = 0.875!
        Me.Label133.Width = 0.25!
        '
        'lblCambio
        '
        Me.lblCambio.Height = 0.1875!
        Me.lblCambio.HyperLink = Nothing
        Me.lblCambio.Left = 2.125!
        Me.lblCambio.Name = "lblCambio"
        Me.lblCambio.Style = "ddo-char-set: 0; text-align: right; font-weight: normal; font-size: 8.25pt; font-" & _
            "family: Arial"
        Me.lblCambio.Text = ""
        Me.lblCambio.Top = 0.875!
        Me.lblCambio.Width = 0.5!
        '
        'Label135
        '
        Me.Label135.Height = 0.1875!
        Me.Label135.HyperLink = Nothing
        Me.Label135.Left = 1.875!
        Me.Label135.Name = "Label135"
        Me.Label135.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial"
        Me.Label135.Text = "C$"
        Me.Label135.Top = 0.625!
        Me.Label135.Width = 0.25!
        '
        'Line3
        '
        Me.Line3.Height = 0.0!
        Me.Line3.Left = 1.9375!
        Me.Line3.LineStyle = DataDynamics.ActiveReports.LineStyle.Dash
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 0.5625!
        Me.Line3.Width = 0.6875!
        Me.Line3.X1 = 2.625!
        Me.Line3.X2 = 1.9375!
        Me.Line3.Y1 = 0.5625!
        Me.Line3.Y2 = 0.5625!
        '
        'TextBox1
        '
        Me.TextBox1.DataField = "Codigo"
        Me.TextBox1.Height = 0.1875!
        Me.TextBox1.Left = 1.25!
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Style = "ddo-char-set: 0; font-size: 8.25pt"
        Me.TextBox1.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.TextBox1.SummaryGroup = "GroupHeader1"
        Me.TextBox1.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.TextBox1.Text = "tProd"
        Me.TextBox1.Top = 1.125!
        Me.TextBox1.Width = 0.375!
        '
        'actrptImprimirTicketVenta
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.01!
        Me.PageSettings.Margins.Left = 0.15!
        Me.PageSettings.Margins.Right = 0.05!
        Me.PageSettings.Margins.Top = 0.0!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 2.729167!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.codig, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label126, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label137, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label138, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPago, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCambio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label136 As DataDynamics.ActiveReports.Label
    Private WithEvents Label2 As DataDynamics.ActiveReports.Label
    Private WithEvents Label3 As DataDynamics.ActiveReports.Label
    Private WithEvents Label4 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label117 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label118 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label123 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label115 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label114 As DataDynamics.ActiveReports.Label
    Friend WithEvents codig As DataDynamics.ActiveReports.Label
    Friend WithEvents Label116 As DataDynamics.ActiveReports.Label
    Private WithEvents Label As DataDynamics.ActiveReports.Label
    Friend WithEvents Label124 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label125 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label126 As DataDynamics.ActiveReports.Label
    Private WithEvents Label107 As DataDynamics.ActiveReports.Label
    Friend WithEvents Line1 As DataDynamics.ActiveReports.Line
    Friend WithEvents Label127 As DataDynamics.ActiveReports.Label
    Private WithEvents Label1 As DataDynamics.ActiveReports.Label
    Private WithEvents Label137 As DataDynamics.ActiveReports.Label
    Private WithEvents Label138 As DataDynamics.ActiveReports.Label
    Private WithEvents Label6 As DataDynamics.ActiveReports.Label
    Private WithEvents Label8 As DataDynamics.ActiveReports.Label
    Private WithEvents Label9 As DataDynamics.ActiveReports.Label
    Private WithEvents Label10 As DataDynamics.ActiveReports.Label
    Private WithEvents Label11 As DataDynamics.ActiveReports.Label
    Private WithEvents Label13 As DataDynamics.ActiveReports.Label
    Private WithEvents Label16 As DataDynamics.ActiveReports.Label
    Private WithEvents Label20 As DataDynamics.ActiveReports.Label
    Private WithEvents Label23 As DataDynamics.ActiveReports.Label
    Private WithEvents Label27 As DataDynamics.ActiveReports.Label
    Private WithEvents Label30 As DataDynamics.ActiveReports.Label
    Private WithEvents Label34 As DataDynamics.ActiveReports.Label
    Private WithEvents Label37 As DataDynamics.ActiveReports.Label
    Private WithEvents Label41 As DataDynamics.ActiveReports.Label
    Private WithEvents Label44 As DataDynamics.ActiveReports.Label
    Private WithEvents Label48 As DataDynamics.ActiveReports.Label
    Private WithEvents Label51 As DataDynamics.ActiveReports.Label
    Private WithEvents Label55 As DataDynamics.ActiveReports.Label
    Private WithEvents Label58 As DataDynamics.ActiveReports.Label
    Private WithEvents Label62 As DataDynamics.ActiveReports.Label
    Private WithEvents Label65 As DataDynamics.ActiveReports.Label
    Private WithEvents Label69 As DataDynamics.ActiveReports.Label
    Private WithEvents Label5 As DataDynamics.ActiveReports.Label
    Private WithEvents Label72 As DataDynamics.ActiveReports.Label
    Private WithEvents Label76 As DataDynamics.ActiveReports.Label
    Private WithEvents Label79 As DataDynamics.ActiveReports.Label
    Private WithEvents Label83 As DataDynamics.ActiveReports.Label
    Private WithEvents Label86 As DataDynamics.ActiveReports.Label
    Private WithEvents Label90 As DataDynamics.ActiveReports.Label
    Private WithEvents Label93 As DataDynamics.ActiveReports.Label
    Private WithEvents Label97 As DataDynamics.ActiveReports.Label
    Private WithEvents Label100 As DataDynamics.ActiveReports.Label
    Private WithEvents Label104 As DataDynamics.ActiveReports.Label
    Private WithEvents Label105 As DataDynamics.ActiveReports.Label
    Private WithEvents Label108 As DataDynamics.ActiveReports.Label
    Private WithEvents Label111 As DataDynamics.ActiveReports.Label
    Private WithEvents Label112 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label120 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label121 As DataDynamics.ActiveReports.Label
    Private WithEvents Label7 As DataDynamics.ActiveReports.Label
    Friend WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents Label103 As DataDynamics.ActiveReports.Label
    Private WithEvents Label106 As DataDynamics.ActiveReports.Label
    Private WithEvents Label109 As DataDynamics.ActiveReports.Label
    Private WithEvents Label110 As DataDynamics.ActiveReports.Label
    Private WithEvents Label113 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label119 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label122 As DataDynamics.ActiveReports.Label
    Friend WithEvents Line2 As DataDynamics.ActiveReports.Line
    Friend WithEvents Label128 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label130 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label131 As DataDynamics.ActiveReports.Label
    Private WithEvents lblPago As DataDynamics.ActiveReports.Label
    Private WithEvents Label133 As DataDynamics.ActiveReports.Label
    Private WithEvents lblCambio As DataDynamics.ActiveReports.Label
    Private WithEvents Label135 As DataDynamics.ActiveReports.Label
    Friend WithEvents Line3 As DataDynamics.ActiveReports.Line
    Friend WithEvents TextBox1 As DataDynamics.ActiveReports.TextBox
End Class
