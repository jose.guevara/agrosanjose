﻿Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class Frm_RPT_ER
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Dim jackreportes As New ReportesConta
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPT_ER"

    Private Sub CrystalReportViewer1_Load(sender As System.Object, e As System.EventArgs) Handles CrystalReportViewer1.Load
        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim formula As String = String.Empty
        Dim report_ER_form As New RPT_ER_form
        Dim sql_reader_report_formulas As SqlDataReader = Nothing
        Dim clave As String = String.Empty
        Dim inertir_signo As String = String.Empty
        Try
            'sql_reader_report_formulas = jackconsulta.RetornaReader("select * from report_formulas  where Cdgo_reporte = 'RPT_ER_form'", True)
            sql_reader_report_formulas = RNCuentaContable.ObtenerFormulasReportes("RPT_ER_form")
            If sql_reader_report_formulas.HasRows Then
                While sql_reader_report_formulas.Read
                    nombreformula = sql_reader_report_formulas!nomb_formula
                    clave = sql_reader_report_formulas!suma_formula
                    inertir_signo = IIf(IsDBNull(sql_reader_report_formulas!Mod_Signo), False, sql_reader_report_formulas!Mod_Signo)
                    If nombreformula.Contains("_mes") Then
                        valor = jackreportes.Report_Damemonto_form(clave, False, inertir_signo, "MV")
                    Else
                        valor = jackreportes.Report_Damemonto_form(clave, False, inertir_signo, "SF")
                    End If

                    report_ER_form.SetParameterValue(nombreformula, valor)

                End While

            End If
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_ER_form.SetParameterValue(nombreformula, valor)

                End If
            Next
            report_ER_form.RecordSelectionFormula = ""
            ' asignar el objeto informe al control visualizado
            Me.CrystalReportViewer1.ReportSource = report_ER_form
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("CrystalReportViewer1_Load - Error: " + ex.Message.ToString(), "Frm_RPT_ER", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub
End Class