<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuadreCaja
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCuadreCaja))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Bar1 = New DevComponents.DotNetBar.Bar
        Me.cmdGuardar = New DevComponents.DotNetBar.ButtonItem
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem
        Me.UltraGroupBox1 = New Infragistics.Win.Misc.UltraGroupBox
        Me.diDeposito = New DevComponents.Editors.DoubleInput
        Me.Label10 = New System.Windows.Forms.Label
        Me.diGastos = New DevComponents.Editors.DoubleInput
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtExNumeroReciboFinal = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtExNumeroReciboInicial = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtExNumeroFacturaFinal = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtExNumeroFacturaInicial = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.Label6 = New System.Windows.Forms.Label
        Me.dtiFechaFinArqueo = New DevComponents.Editors.DateTimeAdv.DateTimeInput
        Me.Label5 = New System.Windows.Forms.Label
        Me.diSaldoInicial = New DevComponents.Editors.DoubleInput
        Me.Label4 = New System.Windows.Forms.Label
        Me.lbltxtNumeroArqueo = New DevComponents.DotNetBar.LabelX
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtiFechaInicioArqueo = New DevComponents.Editors.DateTimeAdv.DateTimeInput
        Me.Label2 = New System.Windows.Forms.Label
        Me.lbltxtUsuario = New DevComponents.DotNetBar.LabelX
        Me.Label1 = New System.Windows.Forms.Label
        Me.UltraGroupBox2 = New Infragistics.Win.Misc.UltraGroupBox
        Me.UltraGroupBox4 = New Infragistics.Win.Misc.UltraGroupBox
        Me.dgxDetalleDolares = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UltraNumericEditorColumn1 = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
        Me.UltraNumericEditorColumn2 = New Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn(Me.components)
        Me.UltraGroupBox3 = New Infragistics.Win.Misc.UltraGroupBox
        Me.dgxDetalleCordobas = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.Denominacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
        Me.Total = New Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn(Me.components)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox1.SuspendLayout()
        CType(Me.diDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtiFechaFinArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.diSaldoInicial, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtiFechaInicioArqueo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox2.SuspendLayout()
        CType(Me.UltraGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox4.SuspendLayout()
        CType(Me.dgxDetalleDolares, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.UltraGroupBox3.SuspendLayout()
        CType(Me.dgxDetalleCordobas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Bar1
        '
        Me.Bar1.AntiAlias = True
        Me.Bar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdGuardar, Me.cmdProcesar, Me.ButtonItem1})
        Me.Bar1.Location = New System.Drawing.Point(0, 0)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(832, 63)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar1.TabIndex = 0
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'cmdGuardar
        '
        Me.cmdGuardar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdGuardar.Image = CType(resources.GetObject("cmdGuardar.Image"), System.Drawing.Image)
        Me.cmdGuardar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdGuardar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdGuardar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Text = "Guardar<F3>"
        Me.cmdGuardar.Tooltip = "Guardar Arqueo"
        '
        'cmdProcesar
        '
        Me.cmdProcesar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Procesar<F4>"
        Me.cmdProcesar.Tooltip = "Procesar"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem1.Image = CType(resources.GetObject("ButtonItem1.Image"), System.Drawing.Image)
        Me.ButtonItem1.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.ButtonItem1.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.ButtonItem1.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Text = "Salir"
        '
        'UltraGroupBox1
        '
        Me.UltraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox1.Controls.Add(Me.diDeposito)
        Me.UltraGroupBox1.Controls.Add(Me.Label10)
        Me.UltraGroupBox1.Controls.Add(Me.diGastos)
        Me.UltraGroupBox1.Controls.Add(Me.Label11)
        Me.UltraGroupBox1.Controls.Add(Me.txtExNumeroReciboFinal)
        Me.UltraGroupBox1.Controls.Add(Me.Label8)
        Me.UltraGroupBox1.Controls.Add(Me.txtExNumeroReciboInicial)
        Me.UltraGroupBox1.Controls.Add(Me.Label9)
        Me.UltraGroupBox1.Controls.Add(Me.txtExNumeroFacturaFinal)
        Me.UltraGroupBox1.Controls.Add(Me.Label7)
        Me.UltraGroupBox1.Controls.Add(Me.txtExNumeroFacturaInicial)
        Me.UltraGroupBox1.Controls.Add(Me.Label6)
        Me.UltraGroupBox1.Controls.Add(Me.dtiFechaFinArqueo)
        Me.UltraGroupBox1.Controls.Add(Me.Label5)
        Me.UltraGroupBox1.Controls.Add(Me.diSaldoInicial)
        Me.UltraGroupBox1.Controls.Add(Me.Label4)
        Me.UltraGroupBox1.Controls.Add(Me.lbltxtNumeroArqueo)
        Me.UltraGroupBox1.Controls.Add(Me.Label3)
        Me.UltraGroupBox1.Controls.Add(Me.dtiFechaInicioArqueo)
        Me.UltraGroupBox1.Controls.Add(Me.Label2)
        Me.UltraGroupBox1.Controls.Add(Me.lbltxtUsuario)
        Me.UltraGroupBox1.Controls.Add(Me.Label1)
        Me.UltraGroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UltraGroupBox1.Location = New System.Drawing.Point(0, 63)
        Me.UltraGroupBox1.Name = "UltraGroupBox1"
        Me.UltraGroupBox1.Size = New System.Drawing.Size(832, 211)
        Me.UltraGroupBox1.TabIndex = 1
        Me.UltraGroupBox1.Text = "Datos Generales Arqueo"
        Me.UltraGroupBox1.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'diDeposito
        '
        '
        '
        '
        Me.diDeposito.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diDeposito.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diDeposito.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diDeposito.Increment = 1
        Me.diDeposito.Location = New System.Drawing.Point(537, 177)
        Me.diDeposito.Name = "diDeposito"
        Me.diDeposito.Size = New System.Drawing.Size(103, 20)
        Me.diDeposito.TabIndex = 45
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label10.Location = New System.Drawing.Point(483, 177)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 13)
        Me.Label10.TabIndex = 44
        Me.Label10.Text = "Deposito:"
        '
        'diGastos
        '
        '
        '
        '
        Me.diGastos.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diGastos.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diGastos.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diGastos.Increment = 1
        Me.diGastos.Location = New System.Drawing.Point(208, 175)
        Me.diGastos.Name = "diGastos"
        Me.diGastos.Size = New System.Drawing.Size(103, 20)
        Me.diGastos.TabIndex = 43
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label11.Location = New System.Drawing.Point(162, 175)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "Gastos:"
        '
        'txtExNumeroReciboFinal
        '
        Me.txtExNumeroReciboFinal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroReciboFinal.Border.Class = "TextBoxBorder"
        Me.txtExNumeroReciboFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroReciboFinal.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroReciboFinal.Location = New System.Drawing.Point(537, 121)
        Me.txtExNumeroReciboFinal.Name = "txtExNumeroReciboFinal"
        Me.txtExNumeroReciboFinal.Size = New System.Drawing.Size(103, 20)
        Me.txtExNumeroReciboFinal.TabIndex = 41
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label8.Location = New System.Drawing.Point(426, 121)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(109, 13)
        Me.Label8.TabIndex = 40
        Me.Label8.Text = "N�mero Recibo Final:"
        '
        'txtExNumeroReciboInicial
        '
        Me.txtExNumeroReciboInicial.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroReciboInicial.Border.Class = "TextBoxBorder"
        Me.txtExNumeroReciboInicial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroReciboInicial.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroReciboInicial.Location = New System.Drawing.Point(208, 121)
        Me.txtExNumeroReciboInicial.Name = "txtExNumeroReciboInicial"
        Me.txtExNumeroReciboInicial.Size = New System.Drawing.Size(103, 20)
        Me.txtExNumeroReciboInicial.TabIndex = 39
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label9.Location = New System.Drawing.Point(89, 121)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(114, 13)
        Me.Label9.TabIndex = 38
        Me.Label9.Text = "N�mero Recibo Inicial:"
        '
        'txtExNumeroFacturaFinal
        '
        Me.txtExNumeroFacturaFinal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroFacturaFinal.Border.Class = "TextBoxBorder"
        Me.txtExNumeroFacturaFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroFacturaFinal.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroFacturaFinal.Location = New System.Drawing.Point(537, 93)
        Me.txtExNumeroFacturaFinal.Name = "txtExNumeroFacturaFinal"
        Me.txtExNumeroFacturaFinal.Size = New System.Drawing.Size(103, 20)
        Me.txtExNumeroFacturaFinal.TabIndex = 37
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label7.Location = New System.Drawing.Point(424, 93)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(111, 13)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "N�mero Factura Final:"
        '
        'txtExNumeroFacturaInicial
        '
        Me.txtExNumeroFacturaInicial.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtExNumeroFacturaInicial.Border.Class = "TextBoxBorder"
        Me.txtExNumeroFacturaInicial.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExNumeroFacturaInicial.ForeColor = System.Drawing.Color.Black
        Me.txtExNumeroFacturaInicial.Location = New System.Drawing.Point(208, 93)
        Me.txtExNumeroFacturaInicial.Name = "txtExNumeroFacturaInicial"
        Me.txtExNumeroFacturaInicial.Size = New System.Drawing.Size(103, 20)
        Me.txtExNumeroFacturaInicial.TabIndex = 35
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label6.Location = New System.Drawing.Point(89, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 13)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "N�mero Factura Inicial:"
        '
        'dtiFechaFinArqueo
        '
        '
        '
        '
        Me.dtiFechaFinArqueo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtiFechaFinArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtiFechaFinArqueo.ButtonDropDown.Visible = True
        Me.dtiFechaFinArqueo.CustomFormat = "dd/MM/yyyy"
        Me.dtiFechaFinArqueo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.dtiFechaFinArqueo.IsPopupCalendarOpen = False
        Me.dtiFechaFinArqueo.Location = New System.Drawing.Point(537, 65)
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtiFechaFinArqueo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.DisplayMonth = New Date(2011, 9, 1, 0, 0, 0, 0)
        Me.dtiFechaFinArqueo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtiFechaFinArqueo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtiFechaFinArqueo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaFinArqueo.MonthCalendar.TodayButtonVisible = True
        Me.dtiFechaFinArqueo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtiFechaFinArqueo.Name = "dtiFechaFinArqueo"
        Me.dtiFechaFinArqueo.Size = New System.Drawing.Size(147, 20)
        Me.dtiFechaFinArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtiFechaFinArqueo.TabIndex = 33
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label5.Location = New System.Drawing.Point(430, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(105, 13)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "Fecha Inicio Arqueo:"
        '
        'diSaldoInicial
        '
        '
        '
        '
        Me.diSaldoInicial.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.diSaldoInicial.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.diSaldoInicial.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.diSaldoInicial.Increment = 1
        Me.diSaldoInicial.Location = New System.Drawing.Point(208, 149)
        Me.diSaldoInicial.Name = "diSaldoInicial"
        Me.diSaldoInicial.Size = New System.Drawing.Size(103, 20)
        Me.diSaldoInicial.TabIndex = 31
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label4.Location = New System.Drawing.Point(138, 149)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 13)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Saldo Inicial:"
        '
        'lbltxtNumeroArqueo
        '
        Me.lbltxtNumeroArqueo.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lbltxtNumeroArqueo.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.lbltxtNumeroArqueo.BackgroundStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.lbltxtNumeroArqueo.BackgroundStyle.Class = ""
        Me.lbltxtNumeroArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lbltxtNumeroArqueo.Location = New System.Drawing.Point(537, 34)
        Me.lbltxtNumeroArqueo.Name = "lbltxtNumeroArqueo"
        Me.lbltxtNumeroArqueo.Size = New System.Drawing.Size(201, 23)
        Me.lbltxtNumeroArqueo.TabIndex = 29
        Me.lbltxtNumeroArqueo.Text = "NoArqueo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label3.Location = New System.Drawing.Point(451, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "N�mero Arqueo:"
        '
        'dtiFechaInicioArqueo
        '
        '
        '
        '
        Me.dtiFechaInicioArqueo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.dtiFechaInicioArqueo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.dtiFechaInicioArqueo.ButtonDropDown.Visible = True
        Me.dtiFechaInicioArqueo.CustomFormat = "dd/MM/yyyy"
        Me.dtiFechaInicioArqueo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.dtiFechaInicioArqueo.IsPopupCalendarOpen = False
        Me.dtiFechaInicioArqueo.Location = New System.Drawing.Point(208, 65)
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.dtiFechaInicioArqueo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.DisplayMonth = New Date(2011, 9, 1, 0, 0, 0, 0)
        Me.dtiFechaInicioArqueo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.dtiFechaInicioArqueo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.dtiFechaInicioArqueo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.dtiFechaInicioArqueo.MonthCalendar.TodayButtonVisible = True
        Me.dtiFechaInicioArqueo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.dtiFechaInicioArqueo.Name = "dtiFechaInicioArqueo"
        Me.dtiFechaInicioArqueo.Size = New System.Drawing.Size(159, 20)
        Me.dtiFechaInicioArqueo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtiFechaInicioArqueo.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label2.Location = New System.Drawing.Point(100, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 13)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Fecha Inicio Arqueo:"
        '
        'lbltxtUsuario
        '
        Me.lbltxtUsuario.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lbltxtUsuario.BackgroundStyle.BackColor = System.Drawing.Color.Transparent
        Me.lbltxtUsuario.BackgroundStyle.BackColor2 = System.Drawing.Color.Transparent
        Me.lbltxtUsuario.BackgroundStyle.Class = ""
        Me.lbltxtUsuario.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lbltxtUsuario.Location = New System.Drawing.Point(208, 34)
        Me.lbltxtUsuario.Name = "lbltxtUsuario"
        Me.lbltxtUsuario.Size = New System.Drawing.Size(200, 23)
        Me.lbltxtUsuario.TabIndex = 25
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Label1.Location = New System.Drawing.Point(159, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Usuario:"
        '
        'UltraGroupBox2
        '
        Me.UltraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox2.Controls.Add(Me.UltraGroupBox4)
        Me.UltraGroupBox2.Controls.Add(Me.UltraGroupBox3)
        Me.UltraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UltraGroupBox2.Location = New System.Drawing.Point(0, 274)
        Me.UltraGroupBox2.Name = "UltraGroupBox2"
        Me.UltraGroupBox2.Size = New System.Drawing.Size(832, 301)
        Me.UltraGroupBox2.TabIndex = 2
        Me.UltraGroupBox2.Text = "Detalle Arqueo"
        Me.UltraGroupBox2.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'UltraGroupBox4
        '
        Me.UltraGroupBox4.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox4.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox4.Controls.Add(Me.dgxDetalleDolares)
        Me.UltraGroupBox4.Location = New System.Drawing.Point(419, 20)
        Me.UltraGroupBox4.Name = "UltraGroupBox4"
        Me.UltraGroupBox4.Size = New System.Drawing.Size(411, 260)
        Me.UltraGroupBox4.TabIndex = 1
        Me.UltraGroupBox4.Text = "Detalle Dolares"
        Me.UltraGroupBox4.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgxDetalleDolares
        '
        Me.dgxDetalleDolares.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleDolares.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgxDetalleDolares.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgxDetalleDolares.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.UltraNumericEditorColumn1, Me.UltraNumericEditorColumn2})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgxDetalleDolares.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgxDetalleDolares.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgxDetalleDolares.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgxDetalleDolares.Location = New System.Drawing.Point(3, 20)
        Me.dgxDetalleDolares.Name = "dgxDetalleDolares"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleDolares.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgxDetalleDolares.Size = New System.Drawing.Size(405, 237)
        Me.dgxDetalleDolares.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Denominaci�n"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'UltraNumericEditorColumn1
        '
        '
        '
        '
        Me.UltraNumericEditorColumn1.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.UltraNumericEditorColumn1.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.UltraNumericEditorColumn1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.UltraNumericEditorColumn1.BackgroundStyle.TextColor = System.Drawing.Color.Black
        Me.UltraNumericEditorColumn1.Frozen = True
        Me.UltraNumericEditorColumn1.HeaderText = "Cantidad"
        Me.UltraNumericEditorColumn1.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.UltraNumericEditorColumn1.Name = "UltraNumericEditorColumn1"
        Me.UltraNumericEditorColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UltraNumericEditorColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.UltraNumericEditorColumn1.Width = 70
        '
        'UltraNumericEditorColumn2
        '
        Me.UltraNumericEditorColumn2.DefaultNewRowValue = CType(resources.GetObject("UltraNumericEditorColumn2.DefaultNewRowValue"), Object)
        Me.UltraNumericEditorColumn2.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.[Default]
        Me.UltraNumericEditorColumn2.Frozen = True
        Me.UltraNumericEditorColumn2.HeaderText = "Total"
        Me.UltraNumericEditorColumn2.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.UltraNumericEditorColumn2.MaskInput = Nothing
        Me.UltraNumericEditorColumn2.Name = "UltraNumericEditorColumn2"
        Me.UltraNumericEditorColumn2.PadChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.UltraNumericEditorColumn2.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.UltraNumericEditorColumn2.ReadOnly = True
        Me.UltraNumericEditorColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UltraNumericEditorColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.UltraNumericEditorColumn2.SpinButtonAlignment = Infragistics.Win.SpinButtonDisplayStyle.None
        Me.UltraNumericEditorColumn2.Width = 150
        '
        'UltraGroupBox3
        '
        Me.UltraGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.Rectangular3D
        Me.UltraGroupBox3.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center
        Me.UltraGroupBox3.Controls.Add(Me.dgxDetalleCordobas)
        Me.UltraGroupBox3.Location = New System.Drawing.Point(1, 20)
        Me.UltraGroupBox3.Name = "UltraGroupBox3"
        Me.UltraGroupBox3.Size = New System.Drawing.Size(411, 260)
        Me.UltraGroupBox3.TabIndex = 0
        Me.UltraGroupBox3.Text = "Detalle C�rdobas"
        Me.UltraGroupBox3.ViewStyle = Infragistics.Win.Misc.GroupBoxViewStyle.Office2007
        '
        'dgxDetalleCordobas
        '
        Me.dgxDetalleCordobas.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleCordobas.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgxDetalleCordobas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgxDetalleCordobas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Denominacion, Me.Cantidad, Me.Total})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgxDetalleCordobas.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgxDetalleCordobas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgxDetalleCordobas.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgxDetalleCordobas.Location = New System.Drawing.Point(3, 20)
        Me.dgxDetalleCordobas.Name = "dgxDetalleCordobas"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgxDetalleCordobas.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgxDetalleCordobas.Size = New System.Drawing.Size(405, 237)
        Me.dgxDetalleCordobas.TabIndex = 1
        '
        'Denominacion
        '
        Me.Denominacion.Frozen = True
        Me.Denominacion.HeaderText = "Denominaci�n"
        Me.Denominacion.Name = "Denominacion"
        Me.Denominacion.ReadOnly = True
        Me.Denominacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Denominacion.Width = 80
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.BackgroundStyle.TextColor = System.Drawing.Color.Black
        Me.Cantidad.Frozen = True
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Cantidad.Width = 70
        '
        'Total
        '
        Me.Total.DefaultNewRowValue = CType(resources.GetObject("Total.DefaultNewRowValue"), Object)
        Me.Total.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.[Default]
        Me.Total.Frozen = True
        Me.Total.HeaderText = "Total"
        Me.Total.MaskDisplayMode = Infragistics.Win.UltraWinMaskedEdit.MaskMode.Raw
        Me.Total.MaskInput = Nothing
        Me.Total.Name = "Total"
        Me.Total.PadChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Total.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.Total.ReadOnly = True
        Me.Total.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Total.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Total.SpinButtonAlignment = Infragistics.Win.SpinButtonDisplayStyle.None
        Me.Total.Width = 150
        '
        'frmCuadreCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(832, 575)
        Me.Controls.Add(Me.UltraGroupBox2)
        Me.Controls.Add(Me.UltraGroupBox1)
        Me.Controls.Add(Me.Bar1)
        Me.Name = "frmCuadreCaja"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cuadre Caja"
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox1.ResumeLayout(False)
        Me.UltraGroupBox1.PerformLayout()
        CType(Me.diDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diGastos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtiFechaFinArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.diSaldoInicial, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtiFechaInicioArqueo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox2.ResumeLayout(False)
        CType(Me.UltraGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox4.ResumeLayout(False)
        CType(Me.dgxDetalleDolares, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.UltraGroupBox3.ResumeLayout(False)
        CType(Me.dgxDetalleCordobas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdGuardar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents UltraGroupBox1 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox2 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox3 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents UltraGroupBox4 As Infragistics.Win.Misc.UltraGroupBox
    Friend WithEvents diDeposito As DevComponents.Editors.DoubleInput
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents diGastos As DevComponents.Editors.DoubleInput
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroReciboFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroReciboInicial As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroFacturaFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtExNumeroFacturaInicial As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtiFechaFinArqueo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents diSaldoInicial As DevComponents.Editors.DoubleInput
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbltxtNumeroArqueo As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtiFechaInicioArqueo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbltxtUsuario As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgxDetalleCordobas As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Denominacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents Total As Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn
    Friend WithEvents dgxDetalleDolares As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltraNumericEditorColumn1 As DevComponents.DotNetBar.Controls.DataGridViewIntegerInputColumn
    Friend WithEvents UltraNumericEditorColumn2 As Infragistics.Win.UltraDataGridView.UltraNumericEditorColumn
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
End Class
