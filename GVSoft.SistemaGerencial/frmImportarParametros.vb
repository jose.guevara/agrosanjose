Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmImportarParametros
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(200, 32)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 32)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Salir"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(72, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Importar"
        '
        'frmImportarParametros
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(336, 93)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmImportarParametros"
        Me.Text = "frmImportarParametros"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmImportarParametros"

    Private Sub frmImportarParametros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0

    End Sub

    Private Sub frmImportarParametros_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click
        Try
            If sender.name = "Button1" Then
                ProcesarArchivo()
            ElseIf sender.name = "Button2" Then
                Me.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub ProcesarArchivo()
        Dim dlgNew As New OpenFileDialog
        Dim strQuery2 As String = String.Empty
        Dim strFile As String = String.Empty
        Dim strLine As String = String.Empty
        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim strComando As String = String.Empty
        Dim strCtaCont As String = String.Empty
        Dim strDirec As String = String.Empty
        Dim intRegistro As Integer = 0
        Dim intEstado As Integer = 0
        Dim intTipo As Integer = 0
        Dim intTipoID As Integer = 0
        Dim intFacturar As Integer = 0
        Dim strCodigo As String = String.Empty
        Dim strDescrip As String = String.Empty
        Dim strLocal As String = String.Empty
        Dim intRegClase As Integer = 0
        Dim intRegSClase As Integer = 0
        Dim intRegUnidad As Integer = 0
        Dim intRegEmpaq As Integer = 0
        Dim intRegProv As Integer = 0
        Dim intImpuesto As Integer = 0
        Dim dblPrec01 As Double = 0
        Dim dblPrec02 As Double = 0
        Dim dblPrec03 As Double = 0
        Dim dblPrec04 As Double = 0
        Dim dblLimite As Double = 0
        Dim dblSaldo As Double = 0
        Dim dblSaldoFavor As Double = 0
        Dim dblPrecioCostoCOR As Double = 0
        Dim dblPrecioCostoUSD As Double = 0
        Dim strNumID As String = String.Empty
        Dim strFechaIng As String = String.Empty
        Dim strTelef As String = String.Empty
        Dim strFax As String = String.Empty
        Dim strCorreo As String = String.Empty
        Dim strNegocio As String = String.Empty
        Dim strNegDirec As String = String.Empty
        Dim strCont01 As String = String.Empty
        Dim strCont02 As String = String.Empty
        Dim intNumFecha As Integer = 0
        Dim intRegNeg As Integer = 0
        Dim intRegVend As Integer = 0
        Dim intRegDept As Integer = 0
        Dim intRegMun As Integer = 0
        Dim intRegSucursal As Integer = 0
        Dim intDias As Integer = 0
        Dim CodigoVendedor As String = String.Empty
        Dim CodigoDepartamento As String = String.Empty
        Dim CodigoMunicipio As String = String.Empty
        Dim CodigoNegocio As String = String.Empty
        Dim strDireccionProv, strTelefonoProv As String
        Dim NombreContacto1 As String = String.Empty
        Dim TelefonoContacto1 As String = String.Empty
        Dim NombreContacto2 As String = String.Empty
        Dim TelefonoContacto2 As String = String.Empty
        Dim Columnas As String()
        Dim strDrive As String
        Dim lnSaldoUSD As Decimal = 0
        Dim lnPrecioMinomoUSD As Decimal = 0
        Dim dblPrecioMinimoFactura As Double = 0

        Try
            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
                Exit Sub
            End If
            strDrive = ""
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            Select Case intExportarModulo
                Case 2 : intIncr = InStr(dlgNew.FileName, "clientes_")
                Case 3 : intIncr = InStr(dlgNew.FileName, "clases_")
                Case 4 : intIncr = InStr(dlgNew.FileName, "subclases_")
                Case 5 : intIncr = InStr(dlgNew.FileName, "unidades_")
                Case 6 : intIncr = InStr(dlgNew.FileName, "empaques_")
                Case 7 : intIncr = InStr(dlgNew.FileName, "proveedores_")
                Case 8 : intIncr = InStr(dlgNew.FileName, "productos_")
                Case 9 : intIncr = InStr(dlgNew.FileName, "vendedores_")
            End Select
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MsgBox("No es un archivo que contiene los registros correspondientes. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
                Exit Sub
            End If
            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            ' intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Datos")
                Exit Sub
            End If
            'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt c:\", vbNormalFocus, True)
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            Do While sr.Peek() >= 0

                intRegistro = 0
                strCodigo = ""
                strDescrip = ""
                intTipo = 0
                intFacturar = 0
                intEstado = 0
                intRegSucursal = 0
                strLine = sr.ReadLine()
                If strLine.Trim().Length > 0 Then
                    Columnas = strLine.Trim().Split("|")

                    If Columnas.Length > 0 Then


                        intIncr = InStr(1, strLine, "|")
                        intRegistro = 0
                        'intRegistro = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        intRegistro = ConvierteAInt(Columnas(0).Trim())
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                        intIncr = InStr(1, strLine, "|")
                        strCodigo = ""
                        'strCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                        strCodigo = Columnas(1).Trim()
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                        intIncr = InStr(1, strLine, "|")
                        If intExportarModulo = 2 Then
                            intTipo = 0
                            'intTipo = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            intTipo = ConvierteAInt(Columnas(2).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intTipoID = 0
                            'intTipoID = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            intTipoID = ConvierteAInt(Columnas(3).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strNumID = ""
                            'strNumID = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strNumID = Columnas(4).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strDescrip = ""
                            'strDescrip = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strDescrip = Columnas(5).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strFechaIng = ""
                            'strFechaIng = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strFechaIng = Columnas(6).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intNumFecha = 0
                            'intNumFecha = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            intNumFecha = ConvierteAInt(Columnas(7).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strDirec = ""
                            'strDirec = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strDirec = Columnas(8).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intRegNeg = 0
                            'intRegNeg = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            'intRegNeg = ConvierteAInt(Columnas(9).Trim())
                            CodigoNegocio = ""
                            CodigoNegocio = Columnas(9).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            CodigoVendedor = ""
                            'intRegVend = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            CodigoVendedor = Columnas(10).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intRegDept = 0
                            'intRegDept = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            'intRegDept = ConvierteAInt(Columnas(11).Trim())
                            CodigoDepartamento = ""
                            CodigoDepartamento = Columnas(11).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intRegMun = 0
                            'intRegMun = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            'intRegMun = ConvierteAInt(Columnas(12).Trim())
                            CodigoMunicipio = ""
                            CodigoMunicipio = Columnas(12).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strTelef = ""
                            'strTelef = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strTelef = Columnas(13).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strFax = ""
                            'strFax = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strFax = Columnas(14).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strCorreo = ""
                            'strCorreo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strCorreo = Columnas(15).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strNegocio = ""
                            'strNegocio = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strNegocio = Columnas(16).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strNegDirec = ""
                            'strNegDirec = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strNegDirec = Columnas(17).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strCont01 = ""
                            'strCont01 = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strCont01 = Columnas(18).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strCont02 = ""
                            'strCont02 = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strCont02 = Columnas(19).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            dblLimite = 0
                            'dblLimite = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            dblLimite = ConvierteADouble(Columnas(20).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            dblSaldo = 0
                            'dblSaldo = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            dblSaldo = ConvierteADouble(Columnas(21).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intDias = 0
                            'intDias = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            intDias = ConvierteAInt(Columnas(22).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            strCtaCont = ""
                            'strCtaCont = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strCtaCont = Columnas(23).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            intEstado = 0
                            'intEstado = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                            intEstado = ConvierteAInt(Columnas(24).Trim())
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            dblSaldoFavor = 0
                            'dblSaldoFavor = CDbl(strLine)
                            dblSaldoFavor = ConvierteADouble(Columnas(25).Trim())
                            'dblSaldoFavor = CDbl(strLine)
                            dblSaldoFavor = ConvierteADouble(Columnas(25).Trim())
                            intRegSucursal = 0
                            intRegSucursal = ConvierteADouble(Columnas(26).Trim())
                            lnSaldoUSD = 0
                            If Columnas.Length >= 28 Then
                                lnSaldoUSD = ConvierteADouble(Columnas(27).Trim())
                            End If


                            strQuery2 = "ReingresoClientesDeImportar " & intRegistro & ", '" & strCodigo & "', "
                            strQuery2 = strQuery2 + "" & intTipo & ", " & intTipoID & ", '" & strNumID & "', '" & strDescrip & "', "
                            strQuery2 = strQuery2 + "'" & strFechaIng & "', " & intNumFecha & ", '" & strDirec & "', "
                            strQuery2 = strQuery2 + "'" & CodigoNegocio & "', '" & CodigoVendedor & "', '" & CodigoDepartamento & "', "
                            strQuery2 = strQuery2 + "'" & CodigoMunicipio & "', '" & strTelef & "', '" & strFax & "', "
                            strQuery2 = strQuery2 + "'" & strCorreo & "', '" & strNegocio & "', '" & strNegDirec & "', "
                            strQuery2 = strQuery2 + "'" & strCont01 & "', '" & strCont02 & "', " & dblLimite & ", "
                            strQuery2 = strQuery2 + "" & dblSaldo & ", " & intDias & ", '" & strCtaCont & "', " & intEstado & ", "
                            strQuery2 = strQuery2 + "" & dblSaldoFavor & ", " & dblSaldoFavor & "," & intRegSucursal

                        Else
                            strDescrip = ""
                            'strDescrip = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                            strDescrip = Columnas(2).Trim()
                            strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                            intIncr = InStr(1, strLine, "|")
                            If intExportarModulo = 3 Or intExportarModulo = 5 Or intExportarModulo = 6 Then
                                intEstado = 0
                                'intEstado = CInt(strLine)
                                intEstado = ConvierteAInt(Columnas(3).Trim())
                                strQuery = ""
                                Select Case intExportarModulo
                                    Case 3
                                        strQuery = "delete from prm_clases where registro = " & intRegistro & ""
                                        strQuery2 = "insert into prm_clases values (" & intRegistro & ", '" & strCodigo & "', "
                                        strQuery2 = strQuery2 + "'" & strDescrip & "', " & intEstado & ")"
                                    Case 5
                                        strQuery = "delete from prm_unidades where registro = " & intRegistro & ""
                                        strQuery2 = "insert into prm_unidades values (" & intRegistro & ", '" & strCodigo & "', "
                                        strQuery2 = strQuery2 + "'" & strDescrip & "', " & intEstado & ")"
                                    Case 6
                                        strQuery = "delete from prm_empaques where registro = " & intRegistro & ""
                                        strQuery2 = "insert into prm_empaques values (" & intRegistro & ", '" & strCodigo & "', "
                                        strQuery2 = strQuery2 + "'" & strDescrip & "', " & intEstado & ")"
                                End Select
                            ElseIf intExportarModulo = 4 Then
                                intTipo = 0
                                'intTipo = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intTipo = ConvierteAInt(Columnas(3).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                intFacturar = 0
                                'intFacturar = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intFacturar = ConvierteAInt(Columnas(4).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intEstado = 0
                                'intEstado = CInt(strLine)
                                intEstado = ConvierteAInt(Columnas(5).Trim())
                                strQuery = "delete from prm_subclases where registro = " & intRegistro & ""
                                strQuery2 = "insert into prm_subclases values (" & intRegistro & ", '" & strCodigo & "', "
                                strQuery2 = strQuery2 + "'" & strDescrip & "', " & intTipo & ", " & intFacturar & ", "
                                strQuery2 = strQuery2 + "" & intEstado & ")"
                            ElseIf intExportarModulo = 7 Then
                                intTipo = 0
                                'intTipo = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intTipo = ConvierteAInt(Columnas(3).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intEstado = 0
                                'intEstado = CInt(strLine)
                                intEstado = ConvierteAInt(Columnas(4).Trim())
                                dblSaldo = ConvierteADouble(Columnas(5).Trim())
                                intDias = ConvierteAInt(Columnas(6).Trim())
                                strDireccionProv = Columnas(7).Trim()
                                strTelefonoProv = Columnas(8).Trim()
                                NombreContacto1 = String.Empty
                                TelefonoContacto1 = String.Empty
                                NombreContacto2 = String.Empty
                                TelefonoContacto2 = String.Empty
                                If Columnas.Length > 9 And Columnas.Length <= 12 Then
                                    NombreContacto1 = Columnas(9).Trim()
                                    TelefonoContacto1 = Columnas(10).Trim()
                                    NombreContacto2 = Columnas(11).Trim()
                                    TelefonoContacto2 = Columnas(12).Trim()
                                End If
                                'strQuery = "delete from prm_proveedores where registro = " & intRegistro & ""
                                'strQuery2 = "insert into prm_proveedores values (" & intRegistro & ", '" & strCodigo & "', "
                                'strQuery2 = strQuery2 + "'" & strDescrip & "', " & dblSaldo & ", " & intDias & ", " & intTipo & ", " & intEstado & ", '" & strDireccionProv & "', '" & strTelefonoProv & "'" & ")"
                                strQuery2 = "exec IngresaProveedorImportacion " & intRegistro & ", '" & strCodigo & "', "
                                strQuery2 = strQuery2 + "'" & strDescrip & "', " & dblSaldo & ", " & intDias & ", " & intTipo & ", " & intEstado & ", '" & strDireccionProv & "', '" & strTelefonoProv & "', '" & NombreContacto1 & "', '" & TelefonoContacto1 & "', '" & NombreContacto2 & "', '" & TelefonoContacto2 & "'"
                            ElseIf intExportarModulo = 8 Then
                                intRegClase = 0
                                'intRegClase = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intRegClase = SUConversiones.ConvierteAInt(Columnas(3).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                intRegSClase = 0
                                'intRegSClase = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intRegSClase = SUConversiones.ConvierteAInt(Columnas(4).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                intRegUnidad = 0
                                'intRegUnidad = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intRegUnidad = SUConversiones.ConvierteAInt(Columnas(5).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                intRegEmpaq = 0
                                'intRegEmpaq = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intRegEmpaq = SUConversiones.ConvierteAInt(Columnas(6).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                intRegProv = 0
                                'intRegProv = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intRegProv = SUConversiones.ConvierteAInt(Columnas(7).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                dblPrec01 = 0
                                'dblPrec01 = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                dblPrec01 = SUConversiones.ConvierteADouble(Columnas(8).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                dblPrec02 = 0
                                'dblPrec02 = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                dblPrec02 = SUConversiones.ConvierteADouble(Columnas(9).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                dblPrec03 = 0
                                'dblPrec03 = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                dblPrec03 = SUConversiones.ConvierteADouble(Columnas(10).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                dblPrec04 = 0
                                'dblPrec04 = CDbl(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                dblPrec04 = SUConversiones.ConvierteADouble(Columnas(11).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intIncr = InStr(1, strLine, "|")
                                intImpuesto = 0
                                'intImpuesto = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                                intImpuesto = SUConversiones.ConvierteADouble(Columnas(12).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intEstado = 0
                                'intEstado = CInt(strLine)
                                intEstado = ConvierteAInt(Columnas(13).Trim())
                                dblPrecioCostoUSD = SUConversiones.ConvierteADouble(Columnas(14).Trim())
                                dblPrecioCostoCOR = SUConversiones.ConvierteADouble(Columnas(15).Trim())
                                dblPrecioMinimoFactura = 0
                                dblPrecioMinimoFactura = SUConversiones.ConvierteADouble(Columnas(16).Trim())
                                lnPrecioMinomoUSD = 0
                                If Columnas.Length >= 16 Then
                                    lnPrecioMinomoUSD = SUConversiones.ConvierteADouble(Columnas(17).Trim())
                                End If


                                strQuery2 = "exec ReingresaProductoDeImportar " & intRegistro & ", '" & strCodigo & "', "
                                strQuery2 = strQuery2 + "'" & strDescrip & "', " & intRegClase & ", " & intRegSClase & ", "
                                strQuery2 = strQuery2 + "" & intRegUnidad & ", " & intRegEmpaq & ", " & intRegProv & ", " & dblPrecioCostoUSD & ", " & dblPrecioCostoCOR & ", "
                                strQuery2 = strQuery2 + "" & dblPrec01 & ", " & dblPrec02 & ", " & dblPrec03 & ", "
                                strQuery2 = strQuery2 + "" & dblPrec04 & ", " & intImpuesto & ", " & intEstado & ", " & dblPrecioMinimoFactura & ", " & lnPrecioMinomoUSD


                                'strQuery2 = "insert into prm_productos values (" & intRegistro & ", '" & strCodigo & "', "
                                'strQuery2 = strQuery2 + "'" & strDescrip & "', " & intRegClase & ", " & intRegSClase & ", "
                                'strQuery2 = strQuery2 + "" & intRegUnidad & ", " & intRegEmpaq & ", " & intRegProv & ", " & dblPrecioCostoUSD & ", " & dblPrecioCostoCOR & ", "
                                'strQuery2 = strQuery2 + "" & dblPrec01 & ", " & dblPrec02 & ", " & dblPrec03 & ", "
                                'strQuery2 = strQuery2 + "" & dblPrec04 & ", " & intImpuesto & ", " & intEstado & ")"
                            ElseIf intExportarModulo = 9 Then
                                strLocal = 0
                                'strLocal = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                                strLocal = ConvierteADouble(Columnas(3).Trim())
                                strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                                intEstado = 0
                                'intEstado = CInt(strLine)
                                intEstado = ConvierteADouble(Columnas(4).Trim())
                                'strQuery = "delete from prm_vendedores where registro = " & intRegistro & ""
                                'strQuery2 = "insert into prm_vendedores values (" & intRegistro & ", '" & strCodigo & "', "
                                'strQuery2 = strQuery2 + "'" & strDescrip & "', '" & strLocal & "', " & intEstado & ")"
                                strQuery2 = "ReingresaVendedoresDeImportar " & intRegistro & ", '" & strCodigo & "', "
                                strQuery2 = strQuery2 + "'" & strDescrip & "', '" & strLocal & "', " & intEstado
                            End If
                        End If
                        Try
                            If strQuery.Trim().Length > 0 Then
                                cmdAgro2K.CommandText = strQuery
                                cmdAgro2K.ExecuteNonQuery()
                            End If
                        Catch exc As Exception
                            intError = 1
                            MsgBox(exc.Message.ToString)
                            Exit Do
                        End Try
                        Try
                            If strQuery2.Trim().Length > 0 Then
                                cmdAgro2K.CommandText = strQuery2
                                cmdAgro2K.ExecuteNonQuery()
                            End If
                        Catch exc As Exception
                            intError = 1
                            MsgBox(exc.Message.ToString)
                            Exit Do
                        End Try
                    End If
                End If
            Loop
            sr.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If
            If intError = 0 Then
                MsgBox("Finalizado satisfactoriamente la importación de los registros.", MsgBoxStyle.Information, "Proceso Finalizado")
            Else
                MsgBox("Hubieron errores al importar el archivo de los registros.", MsgBoxStyle.Critical, "Error en la Importación")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

End Class
