Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class actrptSaldosDetalladosXProveedores
    Inherits DataDynamics.ActiveReports.ActiveReport

    Public CodProvAnterior As String
    Public CodProvActual As String
    Public Saldo, CambMontFact As Integer


    Private Sub actrptSaldosDetalladosXProveedores_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        Label.Text = strNombEmpresa
        Label31.Text = strTelefEmpresa
        If intRptCtasCbr = 1 Then
            Label2.Text = "Moneda: C�rdobas Sin Deslizamiento"
        ElseIf intRptCtasCbr = 3 Then
            Label2.Text = "Moneda: D�lares Sin Intereses"
        End If
        TextBox21.Text = Format(Now, "Long Date")
        TextBox23.Text = Format(Now, "Long Time")
        Label25.Text = Label25.Text & " " & Format(Now, "dd-MMM-yyyy")

        Me.Document.Printer.PrinterName = ""
    End Sub

    Private Sub actrptSaldosDetalladosXProveedores_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd

        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select

        Label20.Text = "En caso de inconformidad con el saldo presentado, favor llamar al Departamento de Cartera y Cobro al Telf: " & strTelefEmpresa

    End Sub

    Private Sub actrptSaldosDetalladosXProveedores_FetchData(ByVal sender As System.Object, ByVal eArgs As DataDynamics.ActiveReports.ActiveReport.FetchEventArgs) Handles MyBase.FetchData
        If txt1_30.Text <> "0.00" Or txt31_60.Text <> "0.00" Or txt61_Mas.Text <> "0.00" Then
            Label30.Visible = True
        Else
            Label30.Visible = False
        End If
    End Sub

    Private Sub Detail1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Detail1.Format
        Dim SaldoAnterior, SaldoActual, AbonoFact, MontoFact As Double

        CodProvActual = txtCodigoProveedor.Text
        If Saldo = 0 Then
            Saldo = 1
            SaldoAnterior = 0
        Else
            SaldoAnterior = ConvierteADouble(txtSaldo.Text)
        End If

        If CambMontFact = 0 Then
            CambMontFact = 1
            MontoFact = ConvierteADouble(txtMontoFactura.Text)
        Else
            MontoFact = 0
        End If

        AbonoFact = ConvierteADouble(txtAbonoRecibo.Text)
        SaldoActual = (SaldoAnterior + MontoFact) - AbonoFact
        'SaldoActual = MontoFact - AbonoFact
        txtSaldo.Text = Format(SaldoActual, "###,###,###,###.00")


    End Sub

    Private Sub GroupHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader1.Format
        CodProvAnterior = txtCodigoProveedor.Text
        Saldo = 0
    End Sub

    Private Sub GroupHeader2_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupHeader2.Format
        Dim sald As Double
        CambMontFact = 0
        If Saldo = 0 Then
            sald = 0
        Else
            sald = ConvierteADouble(txtSaldo.Text)
        End If
        txtSaldoInicial.Text = Format((ConvierteADouble(txtMontoFactura.Text) + sald), "###,###,###,###.00")
    End Sub
End Class
