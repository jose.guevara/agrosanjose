<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCierreContable
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCierreContable))
        Me.ItemPanel2 = New DevComponents.DotNetBar.ItemPanel()
        Me.ExpandablePanel5 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.GridP_CierreDefinitivo = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ExpandablePanel3 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.GridP_SaldosN = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ItemPanel1 = New DevComponents.DotNetBar.ItemPanel()
        Me.ExpandablePanel2 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel3 = New DevComponents.DotNetBar.ItemPanel()
        Me.TextB_IR = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.ComboB_Reportes = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Balanza_Comprobacion_Saldos = New DevComponents.Editors.ComboItem()
        Me.BalanzaXNivel = New DevComponents.Editors.ComboItem()
        Me.Balance_General = New DevComponents.Editors.ComboItem()
        Me.Estado_Resultado = New DevComponents.Editors.ComboItem()
        Me.Anexo_ER = New DevComponents.Editors.ComboItem()
        Me.Anexo_BG = New DevComponents.Editors.ComboItem()
        Me.Diario_General = New DevComponents.Editors.ComboItem()
        Me.ComboB_Nivel = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.Bar3 = New DevComponents.DotNetBar.Bar()
        Me.Button_Imprimir_SaldosNivel = New DevComponents.DotNetBar.ButtonItem()
        Me.expandablePanel1 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Label_periodoCerrar = New DevComponents.DotNetBar.LabelX()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.DateF_FechaFin = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.DateF_FechaIni = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.Bar2 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_EjecutarPrecierre = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem_msg_Precierre = New DevComponents.DotNetBar.LabelItem()
        Me.SuperTabItem_Buscar = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ItemPanel4 = New DevComponents.DotNetBar.ItemPanel()
        Me.Bar4 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_Guardarimpuesto = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_Nuevoimpuesto = New DevComponents.DotNetBar.ButtonItem()
        Me.ComboB_Tipo_TabBusquedas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_codmoneda_TabBusquedas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_Imp_SR = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.SuperTabItem_mantimpuesto = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Label_tabcierres = New DevComponents.DotNetBar.LabelX()
        Me.ComboB_mes_TabCierres = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_ano_TabCierres = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_Buscarmes = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_TabCierre_cerrarmes = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_TabCierre_abrirmes = New DevComponents.DotNetBar.ButtonItem()
        Me.SuperTabItem_detalle = New DevComponents.DotNetBar.SuperTabItem()
        Me.ButtonItem_menuprincipal = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_cerrarpantalla = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem_pantalla = New DevComponents.DotNetBar.LabelItem()
        Me.BalloonTip_cierre = New DevComponents.DotNetBar.BalloonTip()
        Me.ItemPanel2.SuspendLayout()
        Me.ExpandablePanel5.SuspendLayout()
        CType(Me.GridP_CierreDefinitivo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExpandablePanel3.SuspendLayout()
        CType(Me.GridP_SaldosN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ItemPanel1.SuspendLayout()
        Me.ExpandablePanel2.SuspendLayout()
        Me.ItemPanel3.SuspendLayout()
        CType(Me.Bar3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.expandablePanel1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        CType(Me.DateF_FechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateF_FechaIni, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.ItemPanel4.SuspendLayout()
        CType(Me.Bar4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel1.SuspendLayout()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ItemPanel2
        '
        '
        '
        '
        Me.ItemPanel2.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel2.ContainerControlProcessDialogKey = True
        Me.ItemPanel2.Controls.Add(Me.ExpandablePanel5)
        Me.ItemPanel2.Controls.Add(Me.ExpandablePanel3)
        Me.ItemPanel2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel2.Location = New System.Drawing.Point(-4, 188)
        Me.ItemPanel2.Name = "ItemPanel2"
        Me.ItemPanel2.Size = New System.Drawing.Size(1038, 305)
        Me.ItemPanel2.TabIndex = 3
        Me.ItemPanel2.Text = "ItemPanel2"
        '
        'ExpandablePanel5
        '
        Me.ExpandablePanel5.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel5.Controls.Add(Me.GridP_CierreDefinitivo)
        Me.ExpandablePanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExpandablePanel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ExpandablePanel5.Location = New System.Drawing.Point(0, 281)
        Me.ExpandablePanel5.Name = "ExpandablePanel5"
        Me.ExpandablePanel5.Size = New System.Drawing.Size(1038, 24)
        Me.ExpandablePanel5.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel5.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel5.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel5.Style.GradientAngle = 90
        Me.ExpandablePanel5.TabIndex = 15
        Me.ExpandablePanel5.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel5.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel5.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel5.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel5.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel5.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel5.TitleText = "CIERRES CONTABLES DEFINITIVOS"
        '
        'GridP_CierreDefinitivo
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_CierreDefinitivo.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.GridP_CierreDefinitivo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_CierreDefinitivo.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.GridP_CierreDefinitivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_CierreDefinitivo.DefaultCellStyle = DataGridViewCellStyle3
        Me.GridP_CierreDefinitivo.EnableHeadersVisualStyles = False
        Me.GridP_CierreDefinitivo.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_CierreDefinitivo.Location = New System.Drawing.Point(3, 22)
        Me.GridP_CierreDefinitivo.Name = "GridP_CierreDefinitivo"
        Me.GridP_CierreDefinitivo.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_CierreDefinitivo.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.GridP_CierreDefinitivo.RowHeadersWidth = 20
        Me.GridP_CierreDefinitivo.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_CierreDefinitivo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_CierreDefinitivo.Size = New System.Drawing.Size(1023, 252)
        Me.GridP_CierreDefinitivo.TabIndex = 2
        '
        'ExpandablePanel3
        '
        Me.ExpandablePanel3.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel3.Controls.Add(Me.GridP_SaldosN)
        Me.ExpandablePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ExpandablePanel3.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel3.Name = "ExpandablePanel3"
        Me.ExpandablePanel3.Size = New System.Drawing.Size(1038, 281)
        Me.ExpandablePanel3.Style.Alignment = System.Drawing.StringAlignment.Far
        Me.ExpandablePanel3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel3.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile
        Me.ExpandablePanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel3.Style.GradientAngle = 90
        Me.ExpandablePanel3.TabIndex = 14
        Me.ExpandablePanel3.TitleStyle.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel3.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel3.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel3.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel3.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel3.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel3.TitleText = "RESUMEN DE SALDOS"
        '
        'GridP_SaldosN
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_SaldosN.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.GridP_SaldosN.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_SaldosN.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.GridP_SaldosN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_SaldosN.DefaultCellStyle = DataGridViewCellStyle7
        Me.GridP_SaldosN.EnableHeadersVisualStyles = False
        Me.GridP_SaldosN.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_SaldosN.Location = New System.Drawing.Point(6, 30)
        Me.GridP_SaldosN.Name = "GridP_SaldosN"
        Me.GridP_SaldosN.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_SaldosN.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.GridP_SaldosN.RowHeadersWidth = 20
        Me.GridP_SaldosN.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_SaldosN.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_SaldosN.Size = New System.Drawing.Size(1020, 251)
        Me.GridP_SaldosN.TabIndex = 1
        '
        'ItemPanel1
        '
        '
        '
        '
        Me.ItemPanel1.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel1.ContainerControlProcessDialogKey = True
        Me.ItemPanel1.Controls.Add(Me.ExpandablePanel2)
        Me.ItemPanel1.Controls.Add(Me.expandablePanel1)
        Me.ItemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel1.Location = New System.Drawing.Point(2, 2)
        Me.ItemPanel1.Name = "ItemPanel1"
        Me.ItemPanel1.Size = New System.Drawing.Size(1024, 161)
        Me.ItemPanel1.TabIndex = 2
        Me.ItemPanel1.Text = "ItemPanel1"
        '
        'ExpandablePanel2
        '
        Me.ExpandablePanel2.AutoSize = True
        Me.ExpandablePanel2.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel2.CollapseDirection = DevComponents.DotNetBar.eCollapseDirection.RightToLeft
        Me.ExpandablePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel2.Controls.Add(Me.ItemPanel3)
        Me.ExpandablePanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExpandablePanel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ExpandablePanel2.Location = New System.Drawing.Point(619, 0)
        Me.ExpandablePanel2.Name = "ExpandablePanel2"
        Me.ExpandablePanel2.Size = New System.Drawing.Size(405, 161)
        Me.ExpandablePanel2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandablePanel2.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile
        Me.ExpandablePanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel2.Style.GradientAngle = 90
        Me.ExpandablePanel2.Style.WordWrap = True
        Me.ExpandablePanel2.TabIndex = 14
        Me.ExpandablePanel2.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel2.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel2.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel2.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel2.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel2.TitleStyle.MarginLeft = 6
        Me.ExpandablePanel2.TitleText = "Reportes Financieros"
        '
        'ItemPanel3
        '
        '
        '
        '
        Me.ItemPanel3.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel3.ContainerControlProcessDialogKey = True
        Me.ItemPanel3.Controls.Add(Me.TextB_IR)
        Me.ItemPanel3.Controls.Add(Me.LabelX3)
        Me.ItemPanel3.Controls.Add(Me.ComboB_Reportes)
        Me.ItemPanel3.Controls.Add(Me.ComboB_Nivel)
        Me.ItemPanel3.Controls.Add(Me.LabelX1)
        Me.ItemPanel3.Controls.Add(Me.LabelX2)
        Me.ItemPanel3.Controls.Add(Me.Bar3)
        Me.ItemPanel3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel3.Location = New System.Drawing.Point(0, 29)
        Me.ItemPanel3.Name = "ItemPanel3"
        Me.ItemPanel3.Size = New System.Drawing.Size(405, 131)
        Me.ItemPanel3.TabIndex = 1
        Me.ItemPanel3.Text = "ItemPanel3"
        '
        'TextB_IR
        '
        Me.TextB_IR.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_IR.Border.Class = "TextBoxBorder"
        Me.TextB_IR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_IR.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.TextB_IR.Location = New System.Drawing.Point(105, 101)
        Me.TextB_IR.Name = "TextB_IR"
        Me.TextB_IR.Size = New System.Drawing.Size(120, 21)
        Me.TextB_IR.TabIndex = 53
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.Class = ""
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(12, 101)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(95, 23)
        Me.LabelX3.TabIndex = 52
        Me.LabelX3.Text = "Impuesto S/R (%)"
        '
        'ComboB_Reportes
        '
        Me.ComboB_Reportes.DisplayMember = "Text"
        Me.ComboB_Reportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_Reportes.FormattingEnabled = True
        Me.ComboB_Reportes.ItemHeight = 15
        Me.ComboB_Reportes.Items.AddRange(New Object() {Me.Balanza_Comprobacion_Saldos, Me.BalanzaXNivel, Me.Balance_General, Me.Estado_Resultado, Me.Anexo_ER, Me.Anexo_BG, Me.Diario_General})
        Me.ComboB_Reportes.Location = New System.Drawing.Point(105, 70)
        Me.ComboB_Reportes.Name = "ComboB_Reportes"
        Me.ComboB_Reportes.Size = New System.Drawing.Size(270, 21)
        Me.ComboB_Reportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_Reportes.TabIndex = 49
        '
        'Balanza_Comprobacion_Saldos
        '
        Me.Balanza_Comprobacion_Saldos.Text = "Balanza de Comprobaci�n de Saldos"
        '
        'BalanzaXNivel
        '
        Me.BalanzaXNivel.Text = "Balanza Comprobaci�n Por Nivel"
        '
        'Balance_General
        '
        Me.Balance_General.Text = "Balance General"
        '
        'Estado_Resultado
        '
        Me.Estado_Resultado.Text = "Estado Resultado"
        '
        'Anexo_ER
        '
        Me.Anexo_ER.Text = "Anexo Estado Resultado"
        '
        'Anexo_BG
        '
        Me.Anexo_BG.Text = "Anexo Balance General"
        '
        'Diario_General
        '
        Me.Diario_General.Text = "Diario General"
        '
        'ComboB_Nivel
        '
        Me.ComboB_Nivel.DisplayMember = "Text"
        Me.ComboB_Nivel.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_Nivel.FormattingEnabled = True
        Me.ComboB_Nivel.ItemHeight = 15
        Me.ComboB_Nivel.Location = New System.Drawing.Point(105, 35)
        Me.ComboB_Nivel.Name = "ComboB_Nivel"
        Me.ComboB_Nivel.Size = New System.Drawing.Size(270, 21)
        Me.ComboB_Nivel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_Nivel.TabIndex = 48
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(12, 66)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(83, 23)
        Me.LabelX1.TabIndex = 51
        Me.LabelX1.Text = "Nombre Reporte"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(12, 35)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(83, 23)
        Me.LabelX2.TabIndex = 50
        Me.LabelX2.Text = "Resumen Nivel"
        '
        'Bar3
        '
        Me.Bar3.AccessibleDescription = "DotNetBar Bar (Bar3)"
        Me.Bar3.AccessibleName = "DotNetBar Bar"
        Me.Bar3.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar3.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar3.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Bar3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.Button_Imprimir_SaldosNivel})
        Me.Bar3.Location = New System.Drawing.Point(1, 1)
        Me.Bar3.MenuBar = True
        Me.Bar3.Name = "Bar3"
        Me.Bar3.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar3.Size = New System.Drawing.Size(404, 26)
        Me.Bar3.Stretch = True
        Me.Bar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar3.TabIndex = 47
        Me.Bar3.TabStop = False
        Me.Bar3.Text = "Bar3"
        '
        'Button_Imprimir_SaldosNivel
        '
        Me.Button_Imprimir_SaldosNivel.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.Button_Imprimir_SaldosNivel.ForeColor = System.Drawing.Color.White
        Me.Button_Imprimir_SaldosNivel.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.Button_Imprimir_SaldosNivel.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.Button_Imprimir_SaldosNivel.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.Button_Imprimir_SaldosNivel.Name = "Button_Imprimir_SaldosNivel"
        Me.Button_Imprimir_SaldosNivel.Text = "Generar Reporte"
        '
        'expandablePanel1
        '
        Me.expandablePanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.expandablePanel1.CollapseDirection = DevComponents.DotNetBar.eCollapseDirection.RightToLeft
        Me.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.expandablePanel1.Controls.Add(Me.SuperTabControl1)
        Me.expandablePanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.expandablePanel1.ExpandButtonVisible = False
        Me.expandablePanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.expandablePanel1.Location = New System.Drawing.Point(0, 0)
        Me.expandablePanel1.Name = "expandablePanel1"
        Me.expandablePanel1.Size = New System.Drawing.Size(619, 161)
        Me.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.expandablePanel1.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile
        Me.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.expandablePanel1.Style.GradientAngle = 90
        Me.expandablePanel1.Style.WordWrap = True
        Me.expandablePanel1.TabIndex = 13
        Me.expandablePanel1.TitleStyle.BackColor1.Color = System.Drawing.Color.Transparent
        Me.expandablePanel1.TitleStyle.BackColor2.Color = System.Drawing.Color.Transparent
        Me.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.expandablePanel1.TitleStyle.BorderColor.Color = System.Drawing.Color.Transparent
        Me.expandablePanel1.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.expandablePanel1.TitleStyle.BorderSide = DevComponents.DotNetBar.eBorderSide.None
        Me.expandablePanel1.TitleStyle.BorderWidth = 0
        Me.expandablePanel1.TitleStyle.CornerDiameter = 1
        Me.expandablePanel1.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.expandablePanel1.TitleStyle.GradientAngle = 90
        Me.expandablePanel1.TitleStyle.MarginLeft = 6
        Me.expandablePanel1.TitleText = "Title Bar"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel3)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 3)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(619, 158)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.SuperTabControl1.TabIndex = 17
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem_Buscar, Me.SuperTabItem_detalle, Me.SuperTabItem_mantimpuesto, Me.ButtonItem_menuprincipal, Me.ButtonItem_cerrarpantalla, Me.LabelItem_pantalla})
        Me.SuperTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Label_periodoCerrar)
        Me.SuperTabControlPanel2.Controls.Add(Me.LabelX27)
        Me.SuperTabControlPanel2.Controls.Add(Me.LabelX26)
        Me.SuperTabControlPanel2.Controls.Add(Me.DateF_FechaFin)
        Me.SuperTabControlPanel2.Controls.Add(Me.DateF_FechaIni)
        Me.SuperTabControlPanel2.Controls.Add(Me.Bar2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(619, 132)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem_Buscar
        '
        'Label_periodoCerrar
        '
        Me.Label_periodoCerrar.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_periodoCerrar.BackgroundStyle.Class = ""
        Me.Label_periodoCerrar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_periodoCerrar.Location = New System.Drawing.Point(12, 34)
        Me.Label_periodoCerrar.Name = "Label_periodoCerrar"
        Me.Label_periodoCerrar.Size = New System.Drawing.Size(604, 23)
        Me.Label_periodoCerrar.TabIndex = 51
        Me.Label_periodoCerrar.Text = "Per�odo a Cerrar"
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.Class = ""
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(12, 96)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(75, 23)
        Me.LabelX27.TabIndex = 50
        Me.LabelX27.Text = "Cierre Hasta"
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.Class = ""
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(11, 63)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(75, 23)
        Me.LabelX26.TabIndex = 49
        Me.LabelX26.Text = "Cierre Desde"
        '
        'DateF_FechaFin
        '
        '
        '
        '
        Me.DateF_FechaFin.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaFin.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaFin.ButtonDropDown.Visible = True
        Me.DateF_FechaFin.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaFin.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaFin.IsPopupCalendarOpen = False
        Me.DateF_FechaFin.Location = New System.Drawing.Point(109, 98)
        Me.DateF_FechaFin.MaxDate = New Date(9998, 12, 30, 0, 0, 0, 0)
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaFin.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaFin.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaFin.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaFin.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaFin.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaFin.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaFin.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaFin.Name = "DateF_FechaFin"
        Me.DateF_FechaFin.Size = New System.Drawing.Size(412, 21)
        Me.DateF_FechaFin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaFin.TabIndex = 48
        '
        'DateF_FechaIni
        '
        '
        '
        '
        Me.DateF_FechaIni.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaIni.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaIni.ButtonDropDown.Visible = True
        Me.DateF_FechaIni.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaIni.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaIni.IsPopupCalendarOpen = False
        Me.DateF_FechaIni.Location = New System.Drawing.Point(106, 64)
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaIni.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaIni.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaIni.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaIni.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaIni.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaIni.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaIni.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaIni.Name = "DateF_FechaIni"
        Me.DateF_FechaIni.Size = New System.Drawing.Size(415, 21)
        Me.DateF_FechaIni.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaIni.TabIndex = 47
        '
        'Bar2
        '
        Me.Bar2.AccessibleDescription = "DotNetBar Bar (Bar2)"
        Me.Bar2.AccessibleName = "DotNetBar Bar"
        Me.Bar2.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar2.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar2.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_EjecutarPrecierre, Me.LabelItem_msg_Precierre})
        Me.Bar2.Location = New System.Drawing.Point(2, 2)
        Me.Bar2.MenuBar = True
        Me.Bar2.Name = "Bar2"
        Me.Bar2.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar2.Size = New System.Drawing.Size(779, 26)
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar2.TabIndex = 46
        Me.Bar2.TabStop = False
        Me.Bar2.Text = "Bar2"
        '
        'ToolbarB_EjecutarPrecierre
        '
        Me.ToolbarB_EjecutarPrecierre.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_EjecutarPrecierre.ForeColor = System.Drawing.Color.White
        '  Me.ToolbarB_EjecutarPrecierre.Image = Global.WinForm_Conta.My.Resources.Resources.MSN_MESSENGER_15
        Me.ToolbarB_EjecutarPrecierre.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.imageres_dll_202_16
        Me.ToolbarB_EjecutarPrecierre.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_EjecutarPrecierre.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_EjecutarPrecierre.Name = "ToolbarB_EjecutarPrecierre"
        Me.ToolbarB_EjecutarPrecierre.Text = "Ejecutar PreCierre"
        '
        'LabelItem_msg_Precierre
        '
        Me.LabelItem_msg_Precierre.DividerStyle = True
        Me.LabelItem_msg_Precierre.ForeColor = System.Drawing.Color.Yellow
        Me.LabelItem_msg_Precierre.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.LabelItem_msg_Precierre.Name = "LabelItem_msg_Precierre"
        Me.LabelItem_msg_Precierre.Stretch = True
        Me.LabelItem_msg_Precierre.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'SuperTabItem_Buscar
        '
        Me.SuperTabItem_Buscar.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem_Buscar.GlobalItem = False
        Me.SuperTabItem_Buscar.Name = "SuperTabItem_Buscar"
        Me.SuperTabItem_Buscar.Text = "Pre Cierre Mensual"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.ItemPanel4)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(619, 158)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem_mantimpuesto
        '
        'ItemPanel4
        '
        '
        '
        '
        Me.ItemPanel4.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel4.BackgroundStyle.Class = ""
        Me.ItemPanel4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel4.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel4.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel4.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel4.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel4.ContainerControlProcessDialogKey = True
        Me.ItemPanel4.Controls.Add(Me.Bar4)
        Me.ItemPanel4.Controls.Add(Me.ComboB_Tipo_TabBusquedas)
        Me.ItemPanel4.Controls.Add(Me.ComboB_codmoneda_TabBusquedas)
        Me.ItemPanel4.Controls.Add(Me.LabelX4)
        Me.ItemPanel4.Controls.Add(Me.LabelX5)
        Me.ItemPanel4.Controls.Add(Me.LabelX23)
        Me.ItemPanel4.Controls.Add(Me.LabelX22)
        Me.ItemPanel4.Controls.Add(Me.TextB_Imp_SR)
        Me.ItemPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel4.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem2})
        Me.ItemPanel4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel4.Location = New System.Drawing.Point(0, 0)
        Me.ItemPanel4.Name = "ItemPanel4"
        Me.ItemPanel4.Size = New System.Drawing.Size(619, 158)
        Me.ItemPanel4.TabIndex = 1
        Me.ItemPanel4.Text = "ItemPanel4"
        '
        'Bar4
        '
        Me.Bar4.AccessibleDescription = "DotNetBar Bar (Bar4)"
        Me.Bar4.AccessibleName = "DotNetBar Bar"
        Me.Bar4.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar4.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar4.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar4.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar4.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_Guardarimpuesto, Me.ToolbarB_Nuevoimpuesto})
        Me.Bar4.Location = New System.Drawing.Point(2, 2)
        Me.Bar4.MenuBar = True
        Me.Bar4.Name = "Bar4"
        Me.Bar4.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar4.Size = New System.Drawing.Size(1052, 26)
        Me.Bar4.Stretch = True
        Me.Bar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar4.TabIndex = 45
        Me.Bar4.TabStop = False
        Me.Bar4.Text = "Bar4"
        '
        'ToolbarB_Guardarimpuesto
        '
        Me.ToolbarB_Guardarimpuesto.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_Guardarimpuesto.ForeColor = System.Drawing.Color.White
        'Me.ToolbarB_Guardarimpuesto.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.DDORes_dll_21_10
        Me.ToolbarB_Guardarimpuesto.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.DDORes_dll_21_101
        Me.ToolbarB_Guardarimpuesto.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_Guardarimpuesto.Name = "ToolbarB_Guardarimpuesto"
        Me.ToolbarB_Guardarimpuesto.Text = "Guardar Impuesto"
        '
        'ToolbarB_Nuevoimpuesto
        '
        Me.ToolbarB_Nuevoimpuesto.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_Nuevoimpuesto.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_Nuevoimpuesto.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Add
        Me.ToolbarB_Nuevoimpuesto.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_Nuevoimpuesto.Name = "ToolbarB_Nuevoimpuesto"
        Me.ToolbarB_Nuevoimpuesto.Text = "Nuevo Impuesto"
        Me.ToolbarB_Nuevoimpuesto.Visible = False
        '
        'ComboB_Tipo_TabBusquedas
        '
        Me.ComboB_Tipo_TabBusquedas.DisplayMember = "Text"
        Me.ComboB_Tipo_TabBusquedas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_Tipo_TabBusquedas.FormattingEnabled = True
        Me.ComboB_Tipo_TabBusquedas.ItemHeight = 15
        Me.ComboB_Tipo_TabBusquedas.Location = New System.Drawing.Point(765, 85)
        Me.ComboB_Tipo_TabBusquedas.Name = "ComboB_Tipo_TabBusquedas"
        Me.ComboB_Tipo_TabBusquedas.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_Tipo_TabBusquedas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_Tipo_TabBusquedas.TabIndex = 4
        '
        'ComboB_codmoneda_TabBusquedas
        '
        Me.ComboB_codmoneda_TabBusquedas.DisplayMember = "Text"
        Me.ComboB_codmoneda_TabBusquedas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_codmoneda_TabBusquedas.FormattingEnabled = True
        Me.ComboB_codmoneda_TabBusquedas.ItemHeight = 15
        Me.ComboB_codmoneda_TabBusquedas.Location = New System.Drawing.Point(765, 50)
        Me.ComboB_codmoneda_TabBusquedas.Name = "ComboB_codmoneda_TabBusquedas"
        Me.ComboB_codmoneda_TabBusquedas.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_codmoneda_TabBusquedas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_codmoneda_TabBusquedas.TabIndex = 3
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.Class = ""
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(643, 81)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(94, 23)
        Me.LabelX4.TabIndex = 40
        Me.LabelX4.Text = "Tipo Cuenta"
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.Class = ""
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(646, 50)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(94, 23)
        Me.LabelX5.TabIndex = 39
        Me.LabelX5.Text = "Moneda Cuenta"
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.Class = ""
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(62, 84)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(108, 23)
        Me.LabelX23.TabIndex = 38
        Me.LabelX23.Text = "% Impuesto S/R"
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.Class = ""
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LabelX22.Location = New System.Drawing.Point(44, 50)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(559, 23)
        Me.LabelX22.TabIndex = 37
        Me.LabelX22.Text = "Impuesto Sobre la Renta"
        Me.LabelX22.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'TextB_Imp_SR
        '
        Me.TextB_Imp_SR.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_Imp_SR.Border.Class = "TextBoxBorder"
        Me.TextB_Imp_SR.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_Imp_SR.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_Imp_SR.Location = New System.Drawing.Point(178, 86)
        Me.TextB_Imp_SR.Name = "TextB_Imp_SR"
        Me.TextB_Imp_SR.Size = New System.Drawing.Size(243, 21)
        Me.TextB_Imp_SR.TabIndex = 2
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'SuperTabItem_mantimpuesto
        '
        Me.SuperTabItem_mantimpuesto.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem_mantimpuesto.GlobalItem = False
        Me.SuperTabItem_mantimpuesto.Name = "SuperTabItem_mantimpuesto"
        Me.SuperTabItem_mantimpuesto.Text = "Mantenimiento Impuesto"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Label_tabcierres)
        Me.SuperTabControlPanel1.Controls.Add(Me.ComboB_mes_TabCierres)
        Me.SuperTabControlPanel1.Controls.Add(Me.ComboB_ano_TabCierres)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX25)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX24)
        Me.SuperTabControlPanel1.Controls.Add(Me.Bar1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(619, 158)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem_detalle
        '
        'Label_tabcierres
        '
        Me.Label_tabcierres.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_tabcierres.BackgroundStyle.Class = ""
        Me.Label_tabcierres.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_tabcierres.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_tabcierres.Location = New System.Drawing.Point(30, 94)
        Me.Label_tabcierres.Name = "Label_tabcierres"
        Me.Label_tabcierres.Size = New System.Drawing.Size(586, 23)
        Me.Label_tabcierres.TabIndex = 52
        Me.Label_tabcierres.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ComboB_mes_TabCierres
        '
        Me.ComboB_mes_TabCierres.DisplayMember = "Text"
        Me.ComboB_mes_TabCierres.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_mes_TabCierres.FormattingEnabled = True
        Me.ComboB_mes_TabCierres.ItemHeight = 15
        Me.ComboB_mes_TabCierres.Location = New System.Drawing.Point(76, 70)
        Me.ComboB_mes_TabCierres.Name = "ComboB_mes_TabCierres"
        Me.ComboB_mes_TabCierres.Size = New System.Drawing.Size(434, 21)
        Me.ComboB_mes_TabCierres.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_mes_TabCierres.TabIndex = 48
        '
        'ComboB_ano_TabCierres
        '
        Me.ComboB_ano_TabCierres.DisplayMember = "Text"
        Me.ComboB_ano_TabCierres.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_ano_TabCierres.FormattingEnabled = True
        Me.ComboB_ano_TabCierres.ItemHeight = 15
        Me.ComboB_ano_TabCierres.Location = New System.Drawing.Point(76, 35)
        Me.ComboB_ano_TabCierres.Name = "ComboB_ano_TabCierres"
        Me.ComboB_ano_TabCierres.Size = New System.Drawing.Size(434, 21)
        Me.ComboB_ano_TabCierres.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_ano_TabCierres.TabIndex = 47
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.Class = ""
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(26, 68)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(44, 23)
        Me.LabelX25.TabIndex = 50
        Me.LabelX25.Text = "Mes"
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.Class = ""
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(26, 33)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(44, 23)
        Me.LabelX24.TabIndex = 49
        Me.LabelX24.Text = "A�o"
        '
        'Bar1
        '
        Me.Bar1.AccessibleDescription = "DotNetBar Bar (Bar1)"
        Me.Bar1.AccessibleName = "DotNetBar Bar"
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar1.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_Buscarmes, Me.ToolbarB_TabCierre_cerrarmes, Me.ToolbarB_TabCierre_abrirmes})
        Me.Bar1.Location = New System.Drawing.Point(1, 0)
        Me.Bar1.MenuBar = True
        Me.Bar1.Name = "Bar1"
        Me.Bar1.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar1.Size = New System.Drawing.Size(617, 26)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar1.TabIndex = 46
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'ToolbarB_Buscarmes
        '
        Me.ToolbarB_Buscarmes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_Buscarmes.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_Buscarmes.Image = CType(resources.GetObject("ToolbarB_Buscarmes.Image"), System.Drawing.Image)
        Me.ToolbarB_Buscarmes.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_Buscarmes.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_Buscarmes.Name = "ToolbarB_Buscarmes"
        Me.ToolbarB_Buscarmes.Text = "Buscar Mes"
        '
        'ToolbarB_TabCierre_cerrarmes
        '
        Me.ToolbarB_TabCierre_cerrarmes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_TabCierre_cerrarmes.ForeColor = System.Drawing.Color.White
        'Me.ToolbarB_TabCierre_cerrarmes.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Security
        Me.ToolbarB_TabCierre_cerrarmes.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.imageres_dll_202_16
        Me.ToolbarB_TabCierre_cerrarmes.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_TabCierre_cerrarmes.Name = "ToolbarB_TabCierre_cerrarmes"
        Me.ToolbarB_TabCierre_cerrarmes.Text = "Cerrar Mes Definitivamente"
        '
        'ToolbarB_TabCierre_abrirmes
        '
        Me.ToolbarB_TabCierre_abrirmes.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_TabCierre_abrirmes.ForeColor = System.Drawing.Color.White
        ' Me.ToolbarB_TabCierre_abrirmes.Image = Global.WinForm_Conta.My.Resources.Resources.systemcpl_dll_01_14
        Me.ToolbarB_TabCierre_abrirmes.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.imageres_dll_202_16
        Me.ToolbarB_TabCierre_abrirmes.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_TabCierre_abrirmes.Name = "ToolbarB_TabCierre_abrirmes"
        Me.ToolbarB_TabCierre_abrirmes.Text = "Volver Abrir Mes"
        '
        'SuperTabItem_detalle
        '
        Me.SuperTabItem_detalle.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem_detalle.GlobalItem = False
        Me.SuperTabItem_detalle.Name = "SuperTabItem_detalle"
        Me.SuperTabItem_detalle.Text = "Cierre Mensual"
        '
        'ButtonItem_menuprincipal
        '
        Me.ButtonItem_menuprincipal.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_menuprincipal.Image = CType(resources.GetObject("ButtonItem_menuprincipal.Image"), System.Drawing.Image)
        Me.ButtonItem_menuprincipal.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_menuprincipal.Name = "ButtonItem_menuprincipal"
        Me.ButtonItem_menuprincipal.Text = "Men� Principal"
        Me.ButtonItem_menuprincipal.Tooltip = "Retorna al Men� Principal de la Contabilidad"
        '
        'ButtonItem_cerrarpantalla
        '
        Me.ButtonItem_cerrarpantalla.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_cerrarpantalla.Image = CType(resources.GetObject("ButtonItem_cerrarpantalla.Image"), System.Drawing.Image)
        Me.ButtonItem_cerrarpantalla.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_cerrarpantalla.Name = "ButtonItem_cerrarpantalla"
        Me.ButtonItem_cerrarpantalla.Text = "Cerrar"
        Me.ButtonItem_cerrarpantalla.Tooltip = "Cierra la Ventana y Sale de la Aplicaci�n"
        '
        'LabelItem_pantalla
        '
        Me.LabelItem_pantalla.Enabled = False
        Me.LabelItem_pantalla.Name = "LabelItem_pantalla"
        Me.LabelItem_pantalla.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'BalloonTip_cierre
        '
        Me.BalloonTip_cierre.AutoClose = False
        'Me.BalloonTip_cierre.CaptionImage = Global.Sistemas_Gerenciales.My.Resources.Resources.Archivo_Asientos
        Me.BalloonTip_cierre.CaptionImage = Global.Sistemas_Gerenciales.My.Resources.Resources.Notepad
        Me.BalloonTip_cierre.ShowBalloonOnFocus = True
        '
        'frmCierreContable
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1039, 496)
        Me.Controls.Add(Me.ItemPanel2)
        Me.Controls.Add(Me.ItemPanel1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCierreContable"
        Me.Text = "Cierres Contables"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ItemPanel2.ResumeLayout(False)
        Me.ExpandablePanel5.ResumeLayout(False)
        CType(Me.GridP_CierreDefinitivo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExpandablePanel3.ResumeLayout(False)
        CType(Me.GridP_SaldosN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ItemPanel1.ResumeLayout(False)
        Me.ItemPanel1.PerformLayout()
        Me.ExpandablePanel2.ResumeLayout(False)
        Me.ItemPanel3.ResumeLayout(False)
        CType(Me.Bar3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.expandablePanel1.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        CType(Me.DateF_FechaFin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateF_FechaIni, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.ItemPanel4.ResumeLayout(False)
        CType(Me.Bar4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel1.ResumeLayout(False)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ItemPanel2 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents ExpandablePanel5 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents GridP_CierreDefinitivo As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ExpandablePanel3 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents GridP_SaldosN As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents ItemPanel1 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents ExpandablePanel2 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents ItemPanel3 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents TextB_IR As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ComboB_Reportes As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Balanza_Comprobacion_Saldos As DevComponents.Editors.ComboItem
    Friend WithEvents BalanzaXNivel As DevComponents.Editors.ComboItem
    Friend WithEvents Balance_General As DevComponents.Editors.ComboItem
    Friend WithEvents Estado_Resultado As DevComponents.Editors.ComboItem
    Friend WithEvents Anexo_ER As DevComponents.Editors.ComboItem
    Friend WithEvents Anexo_BG As DevComponents.Editors.ComboItem
    Friend WithEvents Diario_General As DevComponents.Editors.ComboItem
    Friend WithEvents ComboB_Nivel As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Bar3 As DevComponents.DotNetBar.Bar
    Friend WithEvents Button_Imprimir_SaldosNivel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents expandablePanel1 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents Label_periodoCerrar As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DateF_FechaFin As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents DateF_FechaIni As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_EjecutarPrecierre As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem_msg_Precierre As DevComponents.DotNetBar.LabelItem
    Friend WithEvents SuperTabItem_Buscar As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents ItemPanel4 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents Bar4 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_Guardarimpuesto As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_Nuevoimpuesto As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ComboB_Tipo_TabBusquedas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboB_codmoneda_TabBusquedas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TextB_Imp_SR As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents SuperTabItem_mantimpuesto As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents Label_tabcierres As DevComponents.DotNetBar.LabelX
    Friend WithEvents ComboB_mes_TabCierres As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboB_ano_TabCierres As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents ToolbarB_Buscarmes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_TabCierre_cerrarmes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ToolbarB_TabCierre_abrirmes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents SuperTabItem_detalle As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ButtonItem_menuprincipal As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_cerrarpantalla As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem_pantalla As DevComponents.DotNetBar.LabelItem
    Friend WithEvents BalloonTip_cierre As DevComponents.DotNetBar.BalloonTip
End Class
