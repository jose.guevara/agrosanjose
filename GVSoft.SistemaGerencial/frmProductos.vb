Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports Microsoft.Reporting.WinForms

Public Class frmProductos
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cntmnuProductosRg As System.Windows.Forms.ContextMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCodigoProducto As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ddlEmpaque As System.Windows.Forms.ComboBox
    Friend WithEvents ddlUnidad As System.Windows.Forms.ComboBox
    Friend WithEvents ddlSubClase As System.Windows.Forms.ComboBox
    Friend WithEvents ddlClase As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPVDU As System.Windows.Forms.TextBox
    Friend WithEvents txtPVPU As System.Windows.Forms.TextBox
    Friend WithEvents txtPVDC As System.Windows.Forms.TextBox
    Friend WithEvents txtPVPC As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents ddlEstado As System.Windows.Forms.ComboBox
    Friend WithEvents ddlImpuesto As System.Windows.Forms.ComboBox
    Friend WithEvents txtRegistro As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ddlProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents txtPrecioCostoCOR As System.Windows.Forms.TextBox
    Friend WithEvents lblPrecioCostoCOR As System.Windows.Forms.Label
    Friend WithEvents lblPrecioCostoUSD As System.Windows.Forms.Label
    Friend WithEvents txtPrecioCostoUSD As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnListado As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents btnListadoProducto As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrecioCosto As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnlstPrecioMinorista As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnLstVentaMayorista As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnRegistros As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrimerRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAnteriorRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSiguienteRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUltimoRegistro As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnIgual As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents txtPrecioMinimoFactura As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtPrecioMinimoFacturaUSD As TextBox
    Friend WithEvents txtObservacion As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProductos))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtRegistro = New System.Windows.Forms.TextBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtCodigoProducto = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cntmnuProductosRg = New System.Windows.Forms.ContextMenu()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.MenuItem14 = New System.Windows.Forms.MenuItem()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItem16 = New System.Windows.Forms.MenuItem()
        Me.MenuItem17 = New System.Windows.Forms.MenuItem()
        Me.MenuItem18 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ComboBox12 = New System.Windows.Forms.ComboBox()
        Me.ddlProveedor = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBox10 = New System.Windows.Forms.ComboBox()
        Me.ComboBox9 = New System.Windows.Forms.ComboBox()
        Me.ComboBox8 = New System.Windows.Forms.ComboBox()
        Me.ComboBox7 = New System.Windows.Forms.ComboBox()
        Me.ddlEmpaque = New System.Windows.Forms.ComboBox()
        Me.ddlUnidad = New System.Windows.Forms.ComboBox()
        Me.ddlSubClase = New System.Windows.Forms.ComboBox()
        Me.ddlClase = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtPrecioMinimoFacturaUSD = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtPrecioMinimoFactura = New System.Windows.Forms.TextBox()
        Me.txtPrecioCostoUSD = New System.Windows.Forms.TextBox()
        Me.lblPrecioCostoUSD = New System.Windows.Forms.Label()
        Me.txtPrecioCostoCOR = New System.Windows.Forms.TextBox()
        Me.lblPrecioCostoCOR = New System.Windows.Forms.Label()
        Me.ddlEstado = New System.Windows.Forms.ComboBox()
        Me.ddlImpuesto = New System.Windows.Forms.ComboBox()
        Me.txtPVDU = New System.Windows.Forms.TextBox()
        Me.txtPVPU = New System.Windows.Forms.TextBox()
        Me.txtPVDC = New System.Windows.Forms.TextBox()
        Me.txtPVPC = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.btnListado = New DevComponents.DotNetBar.ButtonItem()
        Me.btnListadoProducto = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrecioCosto = New DevComponents.DotNetBar.ButtonItem()
        Me.btnlstPrecioMinorista = New DevComponents.DotNetBar.ButtonItem()
        Me.btnLstVentaMayorista = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.btnRegistros = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrimerRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAnteriorRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSiguienteRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUltimoRegistro = New DevComponents.DotNetBar.ButtonItem()
        Me.btnIgual = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtRegistro)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Controls.Add(Me.txtCodigoProducto)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 85)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(882, 85)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de los Productos"
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(486, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(128, 16)
        Me.Label14.TabIndex = 33
        Me.Label14.Text = "<F5> AYUDA"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtRegistro
        '
        Me.txtRegistro.Location = New System.Drawing.Point(231, 24)
        Me.txtRegistro.Name = "txtRegistro"
        Me.txtRegistro.Size = New System.Drawing.Size(8, 20)
        Me.txtRegistro.TabIndex = 2
        Me.txtRegistro.TabStop = False
        Me.txtRegistro.Visible = False
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(154, 56)
        Me.txtDescripcion.MaxLength = 65
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(713, 20)
        Me.txtDescripcion.TabIndex = 1
        Me.txtDescripcion.Tag = "Descripci�n del c�digo del producto ingresado"
        '
        'txtCodigoProducto
        '
        Me.txtCodigoProducto.Location = New System.Drawing.Point(154, 24)
        Me.txtCodigoProducto.Name = "txtCodigoProducto"
        Me.txtCodigoProducto.Size = New System.Drawing.Size(265, 20)
        Me.txtCodigoProducto.TabIndex = 0
        Me.txtCodigoProducto.Tag = "C�digo asignado al producto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8, Me.MenuItem10})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 4
        Me.MenuItem10.Text = "Igual"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem15, Me.MenuItem9, Me.MenuItem16, Me.MenuItem17, Me.MenuItem18})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Detalle"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 2
        Me.MenuItem15.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 3
        Me.MenuItem9.Text = "Listado de Precios"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 4
        Me.MenuItem16.Text = "Listado de Precios Costo"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 5
        Me.MenuItem17.Text = "Listado de precios de Venta Minorista"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 6
        Me.MenuItem18.Text = "Listado de precios de Venta Mayorista"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ComboBox12)
        Me.GroupBox2.Controls.Add(Me.ddlProveedor)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.ComboBox10)
        Me.GroupBox2.Controls.Add(Me.ComboBox9)
        Me.GroupBox2.Controls.Add(Me.ComboBox8)
        Me.GroupBox2.Controls.Add(Me.ComboBox7)
        Me.GroupBox2.Controls.Add(Me.ddlEmpaque)
        Me.GroupBox2.Controls.Add(Me.ddlUnidad)
        Me.GroupBox2.Controls.Add(Me.ddlSubClase)
        Me.GroupBox2.Controls.Add(Me.ddlClase)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 176)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(882, 120)
        Me.GroupBox2.TabIndex = 48
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de su Presentaci�n"
        '
        'ComboBox12
        '
        Me.ComboBox12.Location = New System.Drawing.Point(192, 8)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox12.TabIndex = 62
        Me.ComboBox12.Text = "ComboBox11"
        Me.ComboBox12.Visible = False
        '
        'ddlProveedor
        '
        Me.ddlProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlProveedor.Location = New System.Drawing.Point(154, 88)
        Me.ddlProveedor.Name = "ddlProveedor"
        Me.ddlProveedor.Size = New System.Drawing.Size(715, 21)
        Me.ddlProveedor.TabIndex = 61
        Me.ddlProveedor.Tag = "Empaque asignado al producto"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 13)
        Me.Label7.TabIndex = 60
        Me.Label7.Text = "Proveedor"
        '
        'ComboBox10
        '
        Me.ComboBox10.Location = New System.Drawing.Point(168, 8)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox10.TabIndex = 59
        Me.ComboBox10.Text = "ComboBox11"
        Me.ComboBox10.Visible = False
        '
        'ComboBox9
        '
        Me.ComboBox9.Location = New System.Drawing.Point(144, 8)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox9.TabIndex = 58
        Me.ComboBox9.Text = "ComboBox10"
        Me.ComboBox9.Visible = False
        '
        'ComboBox8
        '
        Me.ComboBox8.Location = New System.Drawing.Point(120, 8)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox8.TabIndex = 57
        Me.ComboBox8.Text = "ComboBox9"
        Me.ComboBox8.Visible = False
        '
        'ComboBox7
        '
        Me.ComboBox7.Location = New System.Drawing.Point(96, 8)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox7.TabIndex = 56
        Me.ComboBox7.Text = "ComboBox8"
        Me.ComboBox7.Visible = False
        '
        'ddlEmpaque
        '
        Me.ddlEmpaque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlEmpaque.Location = New System.Drawing.Point(549, 56)
        Me.ddlEmpaque.Name = "ddlEmpaque"
        Me.ddlEmpaque.Size = New System.Drawing.Size(321, 21)
        Me.ddlEmpaque.TabIndex = 5
        Me.ddlEmpaque.Tag = "Empaque asignado al producto"
        '
        'ddlUnidad
        '
        Me.ddlUnidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlUnidad.Location = New System.Drawing.Point(154, 56)
        Me.ddlUnidad.Name = "ddlUnidad"
        Me.ddlUnidad.Size = New System.Drawing.Size(321, 21)
        Me.ddlUnidad.TabIndex = 4
        Me.ddlUnidad.Tag = "Unidad asignada al producto"
        '
        'ddlSubClase
        '
        Me.ddlSubClase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSubClase.Location = New System.Drawing.Point(549, 24)
        Me.ddlSubClase.Name = "ddlSubClase"
        Me.ddlSubClase.Size = New System.Drawing.Size(321, 21)
        Me.ddlSubClase.TabIndex = 3
        Me.ddlSubClase.Tag = "SubClase asignada al producto"
        '
        'ddlClase
        '
        Me.ddlClase.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlClase.Location = New System.Drawing.Point(154, 24)
        Me.ddlClase.Name = "ddlClase"
        Me.ddlClase.Size = New System.Drawing.Size(321, 21)
        Me.ddlClase.TabIndex = 2
        Me.ddlClase.Tag = "Clase asignada al producto"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(489, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 49
        Me.Label6.Text = "Empaque"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Unidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(489, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "SubClase"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 46
        Me.Label3.Text = "Clase"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.txtObservacion)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.txtPrecioMinimoFacturaUSD)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.txtPrecioMinimoFactura)
        Me.GroupBox3.Controls.Add(Me.txtPrecioCostoUSD)
        Me.GroupBox3.Controls.Add(Me.lblPrecioCostoUSD)
        Me.GroupBox3.Controls.Add(Me.txtPrecioCostoCOR)
        Me.GroupBox3.Controls.Add(Me.lblPrecioCostoCOR)
        Me.GroupBox3.Controls.Add(Me.ddlEstado)
        Me.GroupBox3.Controls.Add(Me.ddlImpuesto)
        Me.GroupBox3.Controls.Add(Me.txtPVDU)
        Me.GroupBox3.Controls.Add(Me.txtPVPU)
        Me.GroupBox3.Controls.Add(Me.txtPVDC)
        Me.GroupBox3.Controls.Add(Me.txtPVPC)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 299)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(882, 207)
        Me.GroupBox3.TabIndex = 49
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Datos de su Precios"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(12, 47)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(153, 13)
        Me.Label16.TabIndex = 61
        Me.Label16.Text = "Precio Minimo Factura U$"
        '
        'txtPrecioMinimoFacturaUSD
        '
        Me.txtPrecioMinimoFacturaUSD.Location = New System.Drawing.Point(192, 47)
        Me.txtPrecioMinimoFacturaUSD.Name = "txtPrecioMinimoFacturaUSD"
        Me.txtPrecioMinimoFacturaUSD.Size = New System.Drawing.Size(130, 20)
        Me.txtPrecioMinimoFacturaUSD.TabIndex = 60
        Me.txtPrecioMinimoFacturaUSD.Tag = "Precio Minimo Facturaci�n"
        Me.txtPrecioMinimoFacturaUSD.Text = "0.00"
        Me.txtPrecioMinimoFacturaUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(341, 47)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(152, 13)
        Me.Label15.TabIndex = 59
        Me.Label15.Text = "Precio Minimo Factura C$"
        '
        'txtPrecioMinimoFactura
        '
        Me.txtPrecioMinimoFactura.Location = New System.Drawing.Point(531, 47)
        Me.txtPrecioMinimoFactura.Name = "txtPrecioMinimoFactura"
        Me.txtPrecioMinimoFactura.Size = New System.Drawing.Size(130, 20)
        Me.txtPrecioMinimoFactura.TabIndex = 58
        Me.txtPrecioMinimoFactura.Tag = "Precio Minimo Facturaci�n"
        Me.txtPrecioMinimoFactura.Text = "0.00"
        Me.txtPrecioMinimoFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrecioCostoUSD
        '
        Me.txtPrecioCostoUSD.Location = New System.Drawing.Point(192, 19)
        Me.txtPrecioCostoUSD.Name = "txtPrecioCostoUSD"
        Me.txtPrecioCostoUSD.Size = New System.Drawing.Size(130, 20)
        Me.txtPrecioCostoUSD.TabIndex = 57
        Me.txtPrecioCostoUSD.Tag = "Precio Costo D�lares"
        Me.txtPrecioCostoUSD.Text = "0.00"
        Me.txtPrecioCostoUSD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPrecioCostoUSD
        '
        Me.lblPrecioCostoUSD.AutoSize = True
        Me.lblPrecioCostoUSD.Location = New System.Drawing.Point(12, 19)
        Me.lblPrecioCostoUSD.Name = "lblPrecioCostoUSD"
        Me.lblPrecioCostoUSD.Size = New System.Drawing.Size(99, 13)
        Me.lblPrecioCostoUSD.TabIndex = 56
        Me.lblPrecioCostoUSD.Text = "Precio Costo U$"
        '
        'txtPrecioCostoCOR
        '
        Me.txtPrecioCostoCOR.Location = New System.Drawing.Point(531, 19)
        Me.txtPrecioCostoCOR.Name = "txtPrecioCostoCOR"
        Me.txtPrecioCostoCOR.Size = New System.Drawing.Size(130, 20)
        Me.txtPrecioCostoCOR.TabIndex = 54
        Me.txtPrecioCostoCOR.Tag = ""
        Me.txtPrecioCostoCOR.Text = "0.00"
        Me.txtPrecioCostoCOR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPrecioCostoCOR
        '
        Me.lblPrecioCostoCOR.AutoSize = True
        Me.lblPrecioCostoCOR.Location = New System.Drawing.Point(341, 19)
        Me.lblPrecioCostoCOR.Name = "lblPrecioCostoCOR"
        Me.lblPrecioCostoCOR.Size = New System.Drawing.Size(98, 13)
        Me.lblPrecioCostoCOR.TabIndex = 55
        Me.lblPrecioCostoCOR.Text = "Precio Costo C$"
        '
        'ddlEstado
        '
        Me.ddlEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlEstado.Items.AddRange(New Object() {"activo", "inactivo", "eliminado"})
        Me.ddlEstado.Location = New System.Drawing.Point(736, 47)
        Me.ddlEstado.Name = "ddlEstado"
        Me.ddlEstado.Size = New System.Drawing.Size(131, 21)
        Me.ddlEstado.TabIndex = 11
        Me.ddlEstado.Tag = "Estado del registro"
        '
        'ddlImpuesto
        '
        Me.ddlImpuesto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlImpuesto.Items.AddRange(New Object() {"No", "Si"})
        Me.ddlImpuesto.Location = New System.Drawing.Point(736, 19)
        Me.ddlImpuesto.Name = "ddlImpuesto"
        Me.ddlImpuesto.Size = New System.Drawing.Size(131, 21)
        Me.ddlImpuesto.TabIndex = 10
        Me.ddlImpuesto.Tag = "Producto est� sujeto a impuesto"
        '
        'txtPVDU
        '
        Me.txtPVDU.Location = New System.Drawing.Point(192, 76)
        Me.txtPVDU.Name = "txtPVDU"
        Me.txtPVDU.Size = New System.Drawing.Size(130, 20)
        Me.txtPVDU.TabIndex = 9
        Me.txtPVDU.Tag = "Precio de Venta al Distribuidor en D�lares"
        Me.txtPVDU.Text = "0.00"
        Me.txtPVDU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPVPU
        '
        Me.txtPVPU.Location = New System.Drawing.Point(192, 107)
        Me.txtPVPU.Name = "txtPVPU"
        Me.txtPVPU.Size = New System.Drawing.Size(130, 20)
        Me.txtPVPU.TabIndex = 8
        Me.txtPVPU.Tag = "Precio de Venta al P�blico en D�lares"
        Me.txtPVPU.Text = "0.00"
        Me.txtPVPU.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPVDC
        '
        Me.txtPVDC.Location = New System.Drawing.Point(531, 76)
        Me.txtPVDC.Name = "txtPVDC"
        Me.txtPVDC.Size = New System.Drawing.Size(130, 20)
        Me.txtPVDC.TabIndex = 7
        Me.txtPVDC.Tag = "Precio de Venta al Distribuidor en C�rdobas"
        Me.txtPVDC.Text = "0.00"
        Me.txtPVDC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPVPC
        '
        Me.txtPVPC.Location = New System.Drawing.Point(531, 107)
        Me.txtPVPC.Name = "txtPVPC"
        Me.txtPVPC.Size = New System.Drawing.Size(130, 20)
        Me.txtPVPC.TabIndex = 6
        Me.txtPVPC.Tag = "Precio de Venta al P�blico en C�rdobas"
        Me.txtPVPC.Text = "0.00"
        Me.txtPVPC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(672, 47)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(46, 13)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "Estado"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(672, 19)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 13)
        Me.Label12.TabIndex = 52
        Me.Label12.Text = "Impuesto"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 75)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(168, 13)
        Me.Label11.TabIndex = 51
        Me.Label11.Text = "Precio Venta Distribuidor U$"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 107)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(146, 13)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Precio Venta Publico U$"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(341, 75)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(167, 13)
        Me.Label9.TabIndex = 49
        Me.Label9.Text = "Precio Venta Distribuidor C$"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(341, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(145, 13)
        Me.Label8.TabIndex = 48
        Me.Label8.Text = "Precio Venta Publico C$"
        '
        'Timer1
        '
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 515)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(882, 23)
        Me.UltraStatusBar1.TabIndex = 50
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnListado, Me.LabelItem4, Me.btnRegistros, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(882, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 51
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnListado
        '
        Me.btnListado.AutoExpandOnClick = True
        Me.btnListado.Image = CType(resources.GetObject("btnListado.Image"), System.Drawing.Image)
        Me.btnListado.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnListado.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnListado.Name = "btnListado"
        Me.btnListado.OptionGroup = "Listado de productos"
        Me.btnListado.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnListadoProducto, Me.btnPrecioCosto, Me.btnlstPrecioMinorista, Me.btnLstVentaMayorista})
        Me.btnListado.Text = "ListadoPrecio"
        Me.btnListado.Tooltip = "Listados de precios"
        '
        'btnListadoProducto
        '
        Me.btnListadoProducto.Image = CType(resources.GetObject("btnListadoProducto.Image"), System.Drawing.Image)
        Me.btnListadoProducto.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnListadoProducto.Name = "btnListadoProducto"
        Me.btnListadoProducto.Text = "Listado de productos"
        Me.btnListadoProducto.Tooltip = "Listado de productos"
        '
        'btnPrecioCosto
        '
        Me.btnPrecioCosto.Image = CType(resources.GetObject("btnPrecioCosto.Image"), System.Drawing.Image)
        Me.btnPrecioCosto.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrecioCosto.Name = "btnPrecioCosto"
        Me.btnPrecioCosto.Text = "Listado de precio costo"
        Me.btnPrecioCosto.Tooltip = "Listado de precio costo"
        '
        'btnlstPrecioMinorista
        '
        Me.btnlstPrecioMinorista.Image = CType(resources.GetObject("btnlstPrecioMinorista.Image"), System.Drawing.Image)
        Me.btnlstPrecioMinorista.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnlstPrecioMinorista.Name = "btnlstPrecioMinorista"
        Me.btnlstPrecioMinorista.Tag = "Listado de precios  de venta minorista"
        Me.btnlstPrecioMinorista.Text = "Listado de precios de venta minorista"
        Me.btnlstPrecioMinorista.Tooltip = "Listado de precios  de venta minorista"
        '
        'btnLstVentaMayorista
        '
        Me.btnLstVentaMayorista.Image = CType(resources.GetObject("btnLstVentaMayorista.Image"), System.Drawing.Image)
        Me.btnLstVentaMayorista.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnLstVentaMayorista.Name = "btnLstVentaMayorista"
        Me.btnLstVentaMayorista.Tag = "Listado de precios  de venta mayoristas"
        Me.btnLstVentaMayorista.Text = "Listado de precios  de venta mayoristas"
        Me.btnLstVentaMayorista.Tooltip = "Listado de precios  de venta mayorista"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'btnRegistros
        '
        Me.btnRegistros.AutoExpandOnClick = True
        Me.btnRegistros.Image = CType(resources.GetObject("btnRegistros.Image"), System.Drawing.Image)
        Me.btnRegistros.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnRegistros.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.OptionGroup = "Recorre Registros"
        Me.btnRegistros.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPrimerRegistro, Me.btnAnteriorRegistro, Me.btnSiguienteRegistro, Me.btnUltimoRegistro, Me.btnIgual})
        Me.btnRegistros.Text = "Recorre Registros"
        Me.btnRegistros.Tooltip = "Recorre Registros"
        '
        'btnPrimerRegistro
        '
        Me.btnPrimerRegistro.Image = CType(resources.GetObject("btnPrimerRegistro.Image"), System.Drawing.Image)
        Me.btnPrimerRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnPrimerRegistro.Name = "btnPrimerRegistro"
        Me.btnPrimerRegistro.Text = "Primer registro"
        Me.btnPrimerRegistro.Tooltip = "Primer registro"
        '
        'btnAnteriorRegistro
        '
        Me.btnAnteriorRegistro.Image = CType(resources.GetObject("btnAnteriorRegistro.Image"), System.Drawing.Image)
        Me.btnAnteriorRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnAnteriorRegistro.Name = "btnAnteriorRegistro"
        Me.btnAnteriorRegistro.Text = "Anterior registro"
        Me.btnAnteriorRegistro.Tooltip = "Anterior registro"
        '
        'btnSiguienteRegistro
        '
        Me.btnSiguienteRegistro.Image = CType(resources.GetObject("btnSiguienteRegistro.Image"), System.Drawing.Image)
        Me.btnSiguienteRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnSiguienteRegistro.Name = "btnSiguienteRegistro"
        Me.btnSiguienteRegistro.Tag = "Siguiente registro"
        Me.btnSiguienteRegistro.Text = "Siguiente registro"
        Me.btnSiguienteRegistro.Tooltip = "Siguiente registro"
        '
        'btnUltimoRegistro
        '
        Me.btnUltimoRegistro.Image = CType(resources.GetObject("btnUltimoRegistro.Image"), System.Drawing.Image)
        Me.btnUltimoRegistro.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnUltimoRegistro.Name = "btnUltimoRegistro"
        Me.btnUltimoRegistro.Tag = "Ultimo registro"
        Me.btnUltimoRegistro.Text = "Ultimo registro"
        Me.btnUltimoRegistro.Tooltip = "Ultimo registro"
        '
        'btnIgual
        '
        Me.btnIgual.Image = CType(resources.GetObject("btnIgual.Image"), System.Drawing.Image)
        Me.btnIgual.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.btnIgual.Name = "btnIgual"
        Me.btnIgual.Tag = "Igual"
        Me.btnIgual.Text = "Igual"
        Me.btnIgual.Tooltip = "Igual"
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(191, 141)
        Me.txtObservacion.MaxLength = 200
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(677, 60)
        Me.txtObservacion.TabIndex = 62
        Me.txtObservacion.Tag = "Descripci�n del c�digo del producto ingresado"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(12, 144)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(79, 13)
        Me.Label17.TabIndex = 63
        Me.Label17.Text = "Promociones"
        '
        'frmProductos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(882, 538)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmProductos"
        Me.Text = "frmProductos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim strCambios(13) As String
    Public cboCollection As New Collection
    Public txtCollection As New Collection
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmProductos"
    Private Sub frmProductos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Dim lnTasaCambio As Decimal = 0
        Try
            txtPrecioCostoCOR.ReadOnly = True
            txtPrecioMinimoFactura.ReadOnly = True
            txtPVPC.ReadOnly = True
            txtPVDC.ReadOnly = True

            lnTasaCambio = SUConversiones.ConvierteADecimal(RNTipoCambio.ObtieneTipoCambioDelDia())
            If lnTasaCambio <= 0 Then
                MsgBox("Tasa del d�a no ha sido ingresada, favor ingresar la tasa del d�a ", MsgBoxStyle.Critical, "Productos")
                Return
            End If
            blnClear = False
            CreateMyContextMenu()
            cboCollection.Add(ddlClase)
            cboCollection.Add(ddlSubClase)
            cboCollection.Add(ddlUnidad)
            cboCollection.Add(ddlEmpaque)
            cboCollection.Add(ddlImpuesto)
            cboCollection.Add(ddlEstado)
            cboCollection.Add(ComboBox7)
            cboCollection.Add(ComboBox8)
            cboCollection.Add(ComboBox9)
            cboCollection.Add(ComboBox10)
            cboCollection.Add(ddlProveedor)
            cboCollection.Add(ComboBox12)
            txtCollection.Add(txtCodigoProducto)
            txtCollection.Add(txtDescripcion)
            txtCollection.Add(txtPVPC)
            txtCollection.Add(txtPVDC)
            txtCollection.Add(txtPVPU)
            txtCollection.Add(txtPVDU)
            txtCollection.Add(txtRegistro)
            Iniciar()
            Limpiar()
            intModulo = 11
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try

    End Sub
    Private Sub LlamaReporte(ByVal pnIdReporte As Integer)
        Dim lsNombreYRutaReporte As String = String.Empty
        Dim lffrmNew As frmRptRSVisor = Nothing
        Dim llocalReport As LocalReport = Nothing
        Dim lpTipo As ReportParameter = Nothing
        Try
            lffrmNew = New frmRptRSVisor()
            lffrmNew.ReportViewer1.ProcessingMode = ProcessingMode.Local
            Select Case pnIdReporte
                Case 1
                    lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoDeProducto.rdlc"
                Case 2
                    lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoPrecioCostoProducto.rdlc"
                Case 3
                    lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoPrecioVentaProducto.rdlc"
                Case 4
                    lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoPrecioVentaProductoMayorista.rdlc"
                Case 5
                    lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoPrecioVentaProductoMinorista.rdlc"
                Case 6
                    lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoDeProductoYExistenciaActual.rdlc"
                    'lsNombreYRutaReporte = "Sistemas_Gerenciales.ListadoDeProducto.rdlc"

            End Select


            llocalReport = lffrmNew.ReportViewer1.LocalReport
            llocalReport.ReportEmbeddedResource = lsNombreYRutaReporte

            Dim dsListadoProducto As New ReportDataSource()
            dsListadoProducto.Name = "dsListadoPrecio"
            dsListadoProducto.Value = RNProduto.ObtieneListadoPrecioVenta(pnIdReporte)

            llocalReport.DataSources.Add(dsListadoProducto)
            'Refresh the report  
            lffrmNew.ReportViewer1.RefreshReport()

            lffrmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            lffrmNew.Show()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Boton OK")
        End Try
    End Sub
    Private Sub ObtengoDatosListadoAyuda()
        Try
            If blnUbicar = True Then
                blnUbicar = False
                'Timer1.Enabled = False
                If strUbicar <> "" Then
                    txtCodigoProducto.Text = strUbicar
                    MoverRegistro("Igual")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try


    End Sub
    Private Sub frmProductos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try


            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Guardar()
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 3
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click, MenuItem15.Click
        Try


            Select Case sender.text.ToString
                Case "Guardar" : Guardar()
                Case "Cancelar", "Limpiar" : Limpiar()
                Case "Salir" : Me.Close()
                Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
                Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = txtRegistro.Text
                ''Timer1.Interval = 200
                ''Timer1.Enabled = True
                'frmNew.ShowDialog()
                Case "Cambios"
                    Dim frmNew As New frmBitacora
                    lngRegistro = txtRegistro.Text
                    frmNew.ShowDialog()
                Case "Detalle"
                    lngRegistro = txtRegistro.Text
                    Dim frmNew As New frmProductoDetalle
                    frmNew.ShowDialog()
                'Case "Reporte"
                Case "Listado de Precios"
                    intModulo = 11
                    Dim frmNew As New actrptViewer
                    intRptFactura = 30
                    strQuery = "exec sp_Reportes " & intModulo & " "
                    frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                    frmNew.Show()
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)
        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Guardar()
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        MoverRegistro(sender.text)
        '    Case 4
        '        Me.Close()
        'End Select
    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Try


            Dim cmdTmp As New SqlCommand("sp_IngProductos", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim prmTmp05 As New SqlParameter
            Dim prmTmp06 As New SqlParameter
            Dim prmTmp07 As New SqlParameter
            Dim prmTmp08 As New SqlParameter
            Dim prmTmp09 As New SqlParameter
            Dim prmTmp10 As New SqlParameter
            Dim prmTmp11 As New SqlParameter
            Dim prmTmp12 As New SqlParameter
            Dim prmTmp13 As New SqlParameter
            Dim prmTmp14 As New SqlParameter
            Dim prmTmp15 As New SqlParameter
            Dim prmTmp16 As New SqlParameter
            Dim prmTmp17 As New SqlParameter
            Dim prmTmp18 As New SqlParameter
            Dim prmTmp19 As New SqlParameter
            Dim prmTmp20 As New SqlParameter
            Dim lngClase, lngSubClase, lngUnidad, lngEmpaque, lngProveedor As Byte

            If IsNumeric(txtPVPC.Text) = False Then
                txtPVPC.Text = "0.00"
                MsgBox("El Precio de Venta al P�blico en C�rdobas debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPVDC.Text) = False Then
                txtPVDC.Text = "0.00"
                MsgBox("El Precio de Venta al Distribuidor en C�rdobas debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPVPU.Text) = False Then
                txtPVPU.Text = "0.00"
                MsgBox("El Precio de Venta al P�blico en D�lares debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPVDU.Text) = False Then
                txtPVDU.Text = "0.00"
                MsgBox("El Precio de Venta al Distribuidor en D�lares debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPrecioCostoCOR.Text) = False Then
                txtPrecioCostoCOR.Text = "0.00"
                MsgBox("El precio costo permitodo debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPrecioCostoUSD.Text) = False Then
                txtPrecioCostoUSD.Text = "0.00"
                MsgBox("El precio costo en dolares permitodo debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPrecioMinimoFactura.Text) = False Then
                txtPrecioMinimoFactura.Text = "0.00"
                MsgBox("El precio m�nimo de factura permitodo debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            If IsNumeric(txtPrecioMinimoFacturaUSD.Text) = False Then
                txtPrecioMinimoFacturaUSD.Text = "0.00"
                MsgBox("El precio m�nimo de factura en d�lares permitodo debe ser Numerico. Verifique.", MsgBoxStyle.Critical, "Error en Ingreso de Datos")
                Exit Sub
            End If
            lngRegistro = 0
            lngClase = 0
            lngSubClase = 0
            lngUnidad = 0
            lngEmpaque = 0
            lngProveedor = 0
            ComboBox7.SelectedIndex = ddlClase.SelectedIndex
            lngClase = ComboBox7.SelectedItem
            ComboBox8.SelectedIndex = ddlSubClase.SelectedIndex
            lngSubClase = ComboBox8.SelectedItem
            ComboBox9.SelectedIndex = ddlUnidad.SelectedIndex
            lngUnidad = ComboBox9.SelectedItem
            ComboBox10.SelectedIndex = ddlEmpaque.SelectedIndex
            lngEmpaque = ComboBox10.SelectedItem
            ComboBox12.SelectedIndex = ddlProveedor.SelectedIndex
            lngProveedor = ComboBox12.SelectedItem
            With prmTmp01
                .ParameterName = "@lngRegistro"
                .SqlDbType = SqlDbType.SmallInt
                .Value = 0
            End With
            With prmTmp02
                .ParameterName = "@strCodigo"
                .SqlDbType = SqlDbType.VarChar
                .Value = txtCodigoProducto.Text
            End With
            With prmTmp03
                .ParameterName = "@strDescripcion"
                .SqlDbType = SqlDbType.VarChar
                .Value = txtDescripcion.Text
            End With
            With prmTmp04
                .ParameterName = "@lngClase"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngClase
            End With
            With prmTmp05
                .ParameterName = "@lngSubClase"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngSubClase
            End With
            With prmTmp06
                .ParameterName = "@lngUnidad"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngUnidad
            End With
            With prmTmp07
                .ParameterName = "@lngEmpaque"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngEmpaque
            End With
            With prmTmp08
                .ParameterName = "@PrecioCostoUSD"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text.Trim())
            End With
            With prmTmp09
                .ParameterName = "@PrecioCostoCOR"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPrecioCostoCOR.Text.Trim())
            End With
            With prmTmp10
                .ParameterName = "@dblPVPC"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPVPC.Text)
            End With
            With prmTmp11
                .ParameterName = "@dblPVDC"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPVDC.Text)
            End With
            With prmTmp12
                .ParameterName = "@dblPVPU"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPVPU.Text)
            End With
            With prmTmp13
                .ParameterName = "@dblPVDU"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPVDU.Text)
            End With
            With prmTmp14
                .ParameterName = "@intImpuesto"
                .SqlDbType = SqlDbType.TinyInt
                .Value = ddlImpuesto.SelectedIndex
            End With
            With prmTmp15
                .ParameterName = "@intEstado"
                .SqlDbType = SqlDbType.TinyInt
                .Value = ddlEstado.SelectedIndex
            End With
            With prmTmp16
                .ParameterName = "@intVerificar"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With prmTmp17
                .ParameterName = "@lngProveedor"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngProveedor
            End With
            With prmTmp18
                .ParameterName = "@pnPrecioMinimo"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPrecioMinimoFactura.Text.Trim())
            End With
            With prmTmp19
                .ParameterName = "@pnPrecioMinimoUSD"
                .SqlDbType = SqlDbType.Decimal
                .Value = SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text.Trim())
            End With
            With prmTmp20
                .ParameterName = "@Observacion"
                .SqlDbType = SqlDbType.VarChar
                .Value = SUConversiones.ConvierteAString(txtObservacion.Text)
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .Parameters.Add(prmTmp05)
                .Parameters.Add(prmTmp06)
                .Parameters.Add(prmTmp07)
                .Parameters.Add(prmTmp08)
                .Parameters.Add(prmTmp09)
                .Parameters.Add(prmTmp10)
                .Parameters.Add(prmTmp11)
                .Parameters.Add(prmTmp12)
                .Parameters.Add(prmTmp13)
                .Parameters.Add(prmTmp14)
                .Parameters.Add(prmTmp15)
                .Parameters.Add(prmTmp16)
                .Parameters.Add(prmTmp17)
                .Parameters.Add(prmTmp18)
                .Parameters.Add(prmTmp19)
                .Parameters.Add(prmTmp20)
                .CommandType = CommandType.StoredProcedure
            End With

            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K

            intResp = 0
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
                lngRegistro = txtRegistro.Text
                cmdTmp.Parameters(0).Value = lngRegistro
                Exit While
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            If intResp = 7 Then
                'dtrAgro2K.Close()
                SUFunciones.CierraConexioDR(dtrAgro2K)
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            cmdTmp.Parameters(15).Value = 1

            Try
                cmdTmp.ExecuteNonQuery()
                If intResp = 6 Then
                    If txtDescripcion.Text <> strCambios(0) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Descripci�n", strCambios(0), txtDescripcion.Text)
                    End If
                    If ddlClase.SelectedItem <> strCambios(1) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Clase", strCambios(1), ddlClase.SelectedItem)
                    End If
                    If ddlSubClase.SelectedItem <> strCambios(2) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "SubClase", strCambios(2), ddlSubClase.SelectedItem)
                    End If
                    If ddlUnidad.SelectedItem <> strCambios(3) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Unidad", strCambios(3), ddlUnidad.SelectedItem)
                    End If
                    If ddlEmpaque.SelectedItem <> strCambios(4) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Empaque", strCambios(4), ddlEmpaque.SelectedItem)
                    End If
                    If ddlProveedor.SelectedItem <> strCambios(11) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Proveedor", strCambios(11), ddlProveedor.SelectedItem)
                    End If
                    If txtPVPC.Text <> strCambios(5) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "PVP C$", strCambios(5), txtPVPC.Text)
                    End If
                    If txtPVDC.Text <> strCambios(6) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "PVD C$", strCambios(6), txtPVDC.Text)
                    End If
                    If txtPVPU.Text <> strCambios(7) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "PVP US$", strCambios(7), txtPVPU.Text)
                    End If
                    If txtPVDU.Text <> strCambios(8) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "PVD US$", strCambios(8), txtPVDU.Text)
                    End If
                    If ddlImpuesto.SelectedItem <> strCambios(9) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Impuesto", strCambios(9), ddlImpuesto.SelectedItem)
                    End If
                    If ddlEstado.SelectedItem <> strCambios(10) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Estado", strCambios(10), ddlEstado.SelectedItem)
                    End If
                    If txtPrecioCostoCOR.Text <> strCambios(12) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Precio Costo COR", strCambios(12), txtPrecioCostoCOR.Text)
                    End If
                    If txtPrecioCostoCOR.Text <> strCambios(13) Then
                        Ing_Bitacora(intModulo, txtRegistro.Text, "Precio Costo USD", strCambios(13), txtPrecioCostoUSD.Text)
                    End If
                ElseIf intResp = 0 Then
                    Ing_Bitacora(intModulo, lngRegistro, "C�digo", "", txtCodigoProducto.Text)
                End If
                MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
                MoverRegistro("Primero")
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Finally
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
            End Try
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar()
        Try


            For intIncr = 1 To txtCollection.Count
                txtCollection.Item(intIncr).Text = "0.00"
            Next
            txtCodigoProducto.Text = ""
            txtPrecioCostoCOR.Text = "0.00"
            txtDescripcion.Text = ""
            txtRegistro.Text = "0"
            For intIncr = 1 To cboCollection.Count
                cboCollection.Item(intIncr).SelectedIndex = -1
            Next
            For intIncr = 0 To 12
                strCambios(intIncr) = ""
            Next
            ddlImpuesto.SelectedIndex = 0
            ddlEstado.SelectedIndex = 0
            txtCodigoProducto.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Sub Iniciar()
        Dim col As AutoCompleteStringCollection = Nothing
        Try
            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            ''If cmdAgro2K.Connection.State = ConnectionState.Open Then
            ''    cmdAgro2K.Connection.Close()
            ''End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'strQuery = ""
            ''strQuery = "Select Registro, Descripcion From prm_Clases"
            'strQuery = "exec ObtieneClases "
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNClase.ObtieneClase()
            col = Nothing
            col = New AutoCompleteStringCollection
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    'ddlClase.Items.Add(dtrAgro2K.GetValue(1))
                    ddlClase.Items.Add(dtrAgro2K.Item("Descripcion"))
                    'ComboBox7.Items.Add(dtrAgro2K.GetValue(0))
                    ComboBox7.Items.Add(dtrAgro2K.Item("registro"))
                    col.Add(dtrAgro2K.Item("Descripcion"))
                    'col.Add(dtrAgro2K.GetValue(1))
                End While
            End If
            dtrAgro2K = Nothing
            'dtrAgro2K.Close()
            ddlClase.DropDownStyle = ComboBoxStyle.DropDown
            'ddlProveedor.AutoCompleteSource = AutoCompleteSource.ListItems
            ddlClase.AutoCompleteSource = AutoCompleteSource.CustomSource
            ddlClase.AutoCompleteCustomSource = col
            ddlClase.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            SUFunciones.CierraConexioDR(dtrAgro2K)

            'strQuery = ""
            'strQuery = "Select Registro, Descripcion From prm_SubClases"
            'strQuery = "exec ObtieneSubClases "
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNSubClase.ObtieneSubClase()
            col = Nothing
            col = New AutoCompleteStringCollection
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    ' ddlSubClase.Items.Add(dtrAgro2K.GetValue(1))
                    ddlSubClase.Items.Add(dtrAgro2K.Item("Descripcion"))
                    'ComboBox8.Items.Add(dtrAgro2K.GetValue(0))
                    ComboBox8.Items.Add(dtrAgro2K.Item("registro"))
                    'col.Add(dtrAgro2K.GetValue(1))
                    col.Add(dtrAgro2K.Item("Descripcion"))
                End While
            End If
            'dtrAgro2K.Close()
            ddlSubClase.DropDownStyle = ComboBoxStyle.DropDown
            'ddlProveedor.AutoCompleteSource = AutoCompleteSource.ListItems
            ddlSubClase.AutoCompleteSource = AutoCompleteSource.CustomSource
            ddlSubClase.AutoCompleteCustomSource = col
            ddlSubClase.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            SUFunciones.CierraConexioDR(dtrAgro2K)

            'strQuery = ""
            'strQuery = "Select Registro, Descripcion From prm_Unidades"
            'strQuery = "exec ObtieneUnidades "
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNUnidad.ObtieneUnidad()
            col = Nothing
            col = New AutoCompleteStringCollection
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    ' ddlUnidad.Items.Add(dtrAgro2K.GetValue(1))
                    ddlUnidad.Items.Add(dtrAgro2K.Item("Descripcion"))
                    'ComboBox9.Items.Add(dtrAgro2K.GetValue(0))
                    ComboBox9.Items.Add(dtrAgro2K.Item("registro"))
                    'col.Add(dtrAgro2K.GetValue(1))
                    col.Add(dtrAgro2K.Item("Descripcion"))
                End While
            End If
            'dtrAgro2K.Close()
            ddlUnidad.DropDownStyle = ComboBoxStyle.DropDown
            'ddlProveedor.AutoCompleteSource = AutoCompleteSource.ListItems
            ddlUnidad.AutoCompleteSource = AutoCompleteSource.CustomSource
            ddlUnidad.AutoCompleteCustomSource = col
            ddlUnidad.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            SUFunciones.CierraConexioDR(dtrAgro2K)

            'strQuery = ""
            'strQuery = "Select Registro, Descripcion From prm_Empaques"
            'strQuery = "exec ObtieneEmpaque "
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = Nothing
            dtrAgro2K = RNEmpaque.ObtieneEmpaque()
            col = Nothing
            col = New AutoCompleteStringCollection
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    'ddlEmpaque.Items.Add(dtrAgro2K.GetValue(1))
                    ddlEmpaque.Items.Add(dtrAgro2K.Item("Descripcion"))
                    'ComboBox10.Items.Add(dtrAgro2K.GetValue(0))
                    ComboBox10.Items.Add(dtrAgro2K.Item("Registro"))
                    'col.Add(dtrAgro2K.GetValue(1))
                    col.Add(dtrAgro2K.Item("Descripcion"))
                End While
            End If
            'dtrAgro2K.Close()
            ddlEmpaque.DropDownStyle = ComboBoxStyle.DropDown
            'ddlProveedor.AutoCompleteSource = AutoCompleteSource.ListItems
            ddlEmpaque.AutoCompleteSource = AutoCompleteSource.CustomSource
            ddlEmpaque.AutoCompleteCustomSource = col
            ddlEmpaque.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            SUFunciones.CierraConexioDR(dtrAgro2K)

            'strQuery = ""
            'strQuery = "Select Registro, Nombre From prm_Proveedores"
            'strQuery = "exec ObtieneProveedoresProducto "
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            dtrAgro2K = RNProveedor.ObtieneProveedoresProducto()
            col = Nothing
            col = New AutoCompleteStringCollection
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    ' ddlProveedor.Items.Add(dtrAgro2K.GetValue(1))
                    ddlProveedor.Items.Add(dtrAgro2K.Item("Nombre"))
                    'ComboBox12.Items.Add(dtrAgro2K.GetValue(0))
                    ComboBox12.Items.Add(dtrAgro2K.Item("registro"))
                    'col.Add(dtrAgro2K.GetValue(1))
                    col.Add(dtrAgro2K.Item("Nombre"))
                End While

            End If
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            ddlProveedor.DropDownStyle = ComboBoxStyle.DropDown
            'ddlProveedor.AutoCompleteSource = AutoCompleteSource.ListItems
            ddlProveedor.AutoCompleteSource = AutoCompleteSource.CustomSource
            ddlProveedor.AutoCompleteCustomSource = col
            ddlProveedor.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            SUFunciones.CierraConexioDR(dtrAgro2K)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Sub MoverRegistro(ByVal StrDato As String)
        Try

            'Select Case StrDato
            '    Case "Primero"
            '        strQuery = "Select top 1 Codigo from prm_Productos order by registro asc"
            '    Case "Anterior"
            '        strQuery = "Select Max(Codigo) from prm_Productos Where codigo < '" & txtCodigoProducto.Text & "'"
            '    Case "Siguiente"
            '        strQuery = "Select Min(Codigo) from prm_Productos Where codigo > '" & txtCodigoProducto.Text & "'"
            '    Case "Ultimo"
            '        strQuery = "Select top 1 Codigo from prm_Productos order by registro desc"
            '    Case "Igual"
            '        strQuery = "Select Codigo from prm_Productos Where codigo = '" & txtCodigoProducto.Text & "'"
            '        strCodigoTmp = ""
            '        strCodigoTmp = txtCodigoProducto.Text
            'End Select
            strQuery = "exec BuscaProductoxTipoBusqueda '" & StrDato & "', '" & txtCodigoProducto.Text.Trim() & "'"
            Limpiar()
            UbicarRegistro(StrDato)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim strCodigo As String
        Dim lngClase, lngSubClase, lngUnidad, lngEmpaque, lngProveedor As Long
        Try


            Me.Cursor = Cursors.Default
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection IsNot Nothing Then
                If cmdAgro2K.Connection.State = ConnectionState.Open Then
                    cmdAgro2K.Connection.Close()
                End If

            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            strCodigo = ""
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    strCodigo = dtrAgro2K.GetValue(0)
                End If
            End While
            dtrAgro2K.Close()
            If strCodigo = "" Then
                If StrDato = "Igual" Then
                    txtCodigoProducto.Text = strCodigoTmp
                    MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
                Else
                    MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
                End If
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                Exit Sub
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            strQuery = String.Empty
            'strQuery = "Select registro ,codigo ,descripcion , regclase ,regsubclase , regunidad , regempaque , regproveedor , PrecioCostoUSD , PrecioCostoCOR ,pvpc,pvdc,pvpu ,pvdu,impuesto,estado,isnull(PrecioMinimoFactura,0) PrecioMinimoFactura From prm_Productos Where Codigo = '" & strCodigo & "'"
            strQuery = "exec ObtieneProductoxCodigoProducto '" & strCodigo.Trim() & "'"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            lngClase = 0
            lngSubClase = 0
            lngUnidad = 0
            lngEmpaque = 0
            lngProveedor = 0
            While dtrAgro2K.Read
                'txtRegistro.Text = dtrAgro2K.GetValue(0)
                txtRegistro.Text = SUConversiones.ConvierteAInt(dtrAgro2K.Item("registro"))
                'txtCodigoProducto.Text = dtrAgro2K.GetValue(1)
                txtCodigoProducto.Text = dtrAgro2K.Item("codigo")
                'txtDescripcion.Text = dtrAgro2K.GetValue(2)
                txtDescripcion.Text = dtrAgro2K.Item("descripcion")
                'lngClase = dtrAgro2K.GetValue(3)
                lngClase = SUConversiones.ConvierteAInt(dtrAgro2K.Item("regclase"))
                'lngSubClase = dtrAgro2K.GetValue(4)
                lngSubClase = SUConversiones.ConvierteAInt(dtrAgro2K.Item("regsubclase"))
                'lngUnidad = dtrAgro2K.GetValue(5)
                lngUnidad = SUConversiones.ConvierteAInt(dtrAgro2K.Item("regunidad"))
                'lngEmpaque = dtrAgro2K.GetValue(6)
                lngEmpaque = SUConversiones.ConvierteAInt(dtrAgro2K.Item("regempaque"))
                'lngProveedor = dtrAgro2K.GetValue(7)
                lngProveedor = SUConversiones.ConvierteAInt(dtrAgro2K.Item("regproveedor"))
                'txtPrecioCostoUSD.Text = dtrAgro2K.GetValue(8)
                txtPrecioCostoUSD.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("PrecioCostoUSD"))
                'txtPrecioCostoCOR.Text = dtrAgro2K.GetValue(9)
                txtPrecioCostoCOR.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("PrecioCostoCOR"))
                'txtPVPC.Text = dtrAgro2K.GetValue(10)
                txtPVPC.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("pvpc"))
                'txtPVDC.Text = dtrAgro2K.GetValue(11)
                txtPVDC.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("pvdc"))
                'txtPVPU.Text = dtrAgro2K.GetValue(12)
                txtPVPU.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("pvpu"))
                'txtPVDU.Text = dtrAgro2K.GetValue(13)
                txtPVDU.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("pvdu"))
                'ddlImpuesto.SelectedIndex = dtrAgro2K.GetValue(14)
                ddlImpuesto.SelectedIndex = dtrAgro2K.Item("Impuesto")
                'ddlEstado.SelectedIndex = dtrAgro2K.GetValue(15)
                ddlEstado.SelectedIndex = dtrAgro2K.Item("Estado")
                txtPrecioMinimoFactura.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("PrecioMinimoFactura"))
                txtPrecioMinimoFacturaUSD.Text = SUConversiones.ConvierteADouble(dtrAgro2K.Item("PrecioMinimoFacturaUSD"))
                txtObservacion.Text = SUConversiones.ConvierteAString(dtrAgro2K.Item("Observacion"))
                'txtPVPC.Text = dtrAgro2K.GetValue(8)
                'txtPVDC.Text = dtrAgro2K.GetValue(9)
                'txtPVPU.Text = dtrAgro2K.GetValue(10)
                'txtPVDU.Text = dtrAgro2K.GetValue(11)
                'ddlImpuesto.SelectedIndex = dtrAgro2K.GetValue(12)
                'ddlEstado.SelectedIndex = dtrAgro2K.GetValue(13)
                'strCambios(0) = dtrAgro2K.GetValue(2)
                strCambios(0) = dtrAgro2K.Item("descripcion")
                'strCambios(5) = dtrAgro2K.GetValue(10)
                strCambios(5) = dtrAgro2K.Item("pvpc")
                'strCambios(6) = dtrAgro2K.GetValue(11)
                strCambios(6) = dtrAgro2K.Item("pvdc")
                'strCambios(7) = dtrAgro2K.GetValue(12)
                strCambios(7) = dtrAgro2K.Item("pvpu")
                'strCambios(8) = dtrAgro2K.GetValue(13)
                strCambios(8) = dtrAgro2K.Item("pvdu")
                strCambios(9) = ddlImpuesto.SelectedItem
                strCambios(10) = ddlEstado.SelectedItem
                strCambios(11) = ddlProveedor.SelectedItem
                'strCambios(12) = dtrAgro2K.GetValue(8)
                strCambios(12) = dtrAgro2K.Item("PrecioCostoUSD")
                'strCambios(13) = dtrAgro2K.GetValue(9)
                strCambios(13) = dtrAgro2K.Item("PrecioCostoCOR")

            End While

            SUFunciones.CierraConexioDR(dtrAgro2K)
            ComboBox7.SelectedIndex = -1
            For intIncr = 0 To ComboBox7.Items.Count - 1
                ComboBox7.SelectedIndex = intIncr
                If ComboBox7.SelectedItem = lngClase Then
                    ddlClase.SelectedIndex = ComboBox7.SelectedIndex
                    strCambios(1) = ddlClase.SelectedItem
                    Exit For
                End If
            Next intIncr
            ComboBox8.SelectedIndex = -1
            For intIncr = 0 To ComboBox8.Items.Count - 1
                ComboBox8.SelectedIndex = intIncr
                If ComboBox8.SelectedItem = lngSubClase Then
                    ddlSubClase.SelectedIndex = ComboBox8.SelectedIndex
                    strCambios(2) = ddlSubClase.SelectedItem
                    Exit For
                End If
            Next intIncr
            ComboBox9.SelectedIndex = -1
            For intIncr = 0 To ComboBox9.Items.Count - 1
                ComboBox9.SelectedIndex = intIncr
                If ComboBox9.SelectedItem = lngUnidad Then
                    ddlUnidad.SelectedIndex = ComboBox9.SelectedIndex
                    strCambios(3) = ddlUnidad.SelectedItem
                    Exit For
                End If
            Next intIncr
            ComboBox10.SelectedIndex = -1
            For intIncr = 0 To ComboBox10.Items.Count - 1
                ComboBox10.SelectedIndex = intIncr
                If ComboBox10.SelectedItem = lngEmpaque Then
                    ddlEmpaque.SelectedIndex = ComboBox10.SelectedIndex
                    strCambios(4) = ddlEmpaque.SelectedItem
                    Exit For
                End If
            Next intIncr
            ComboBox12.SelectedIndex = -1
            For intIncr = 0 To ComboBox12.Items.Count - 1
                ComboBox12.SelectedIndex = intIncr
                If ComboBox12.SelectedItem = lngProveedor Then
                    ddlProveedor.SelectedIndex = ComboBox12.SelectedIndex
                    strCambios(11) = ddlProveedor.SelectedItem
                    Exit For
                End If
            Next intIncr
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            dtrAgro2K = Nothing
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
    End Sub

    Sub CreateMyContextMenu()
        Try


            Dim cntmenuItem1 As New MenuItem
            Dim cntmenuItem2 As New MenuItem
            Dim cntmenuItem3 As New MenuItem
            Dim cntmenuItem4 As New MenuItem
            Dim cntmenuItem5 As New MenuItem

            cntmenuItem1.Text = "Primero"
            cntmenuItem2.Text = "Anterior"
            cntmenuItem3.Text = "Siguiente"
            cntmenuItem4.Text = "Ultimo"
            cntmenuItem5.Text = "Igual"

            cntmnuProductosRg.MenuItems.Add(cntmenuItem1)
            cntmnuProductosRg.MenuItems.Add(cntmenuItem2)
            cntmnuProductosRg.MenuItems.Add(cntmenuItem3)
            cntmnuProductosRg.MenuItems.Add(cntmenuItem4)
            cntmnuProductosRg.MenuItems.Add(cntmenuItem5)

            AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
            AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
        End Try
    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoProducto.GotFocus, txtDescripcion.GotFocus, txtPrecioCostoCOR.GotFocus, txtPrecioCostoUSD.GotFocus, txtPVPC.GotFocus, txtPVDC.GotFocus, txtPVPU.GotFocus, txtPVDU.GotFocus, ddlClase.GotFocus, ddlSubClase.GotFocus, ddlUnidad.GotFocus, ddlEmpaque.GotFocus, ddlImpuesto.GotFocus, ddlEstado.GotFocus, txtPrecioCostoCOR.GotFocus

        Dim strCampo As String = String.Empty
        Try
            Label14.Visible = False
            Select Case sender.name.ToString
                Case "txtCodigoProducto" : strCampo = "C�digo" : Label14.Visible = True
                Case "txtDescripcion" : strCampo = "Descripci�n"
                Case "txtPVPC" : strCampo = "PVP C$"
                Case "txtPVDC" : strCampo = "PVD C$"
                Case "txtPVPU" : strCampo = "PVP U$"
                Case "txtPVDU" : strCampo = "PVD U$"
                Case "txtCostoCOR" : strCampo = "Precio Costo C$"
                Case "txtCostoUSD" : strCampo = "Precio Costo U$"
                Case "ddlClase" : strCampo = "Clase"
                Case "ddlSubClase" : strCampo = "SubClase"
                Case "ddlUnidad" : strCampo = "Unidad"
                Case "ddlEmpaque" : strCampo = "Empaque"
                Case "ddlImpuesto" : strCampo = "Impuesto"
                Case "ddlEstado" : strCampo = "Estado"
                Case "txtObservacion" : strCampo = "txtObservacion"
            End Select
            sender.selectall()
            'StatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            'StatusBar1.Panels.Item(1).Text = sender.tag
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            ObtengoDatosListadoAyuda()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            If blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    txtCodigoProducto.Text = strUbicar
                    MoverRegistro("Igual")
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
            Timer1.Stop()
            Exit Sub
        End Try

    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigoProducto.KeyDown, txtDescripcion.KeyDown, txtPrecioCostoCOR.KeyDown, txtPrecioCostoUSD.KeyDown, txtPVPC.KeyDown, txtPVDC.KeyDown, txtPVPU.KeyDown, txtPVDU.KeyDown, ddlClase.KeyDown, ddlSubClase.KeyDown, ddlUnidad.KeyDown, ddlEmpaque.KeyDown, ddlImpuesto.KeyDown, ddlEstado.KeyDown, ddlProveedor.KeyDown, txtPrecioCostoCOR.KeyDown, txtPrecioCostoUSD.KeyDown, txtPrecioMinimoFacturaUSD.KeyDown, txtObservacion.KeyDown
        Dim lnTasaCambio As Decimal = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Tab Then
                objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
                lnTasaCambio = RNTipoCambio.ObtieneTipoCambioDelDia()
                Select Case sender.name.ToString
                    Case "txtCodigoProducto" : MoverRegistro("Igual") : txtDescripcion.Focus()
                    Case "txtDescripcion" : ddlClase.Focus()
                    Case "ddlClase" : ddlSubClase.Focus()
                    Case "ddlSubClase" : ddlUnidad.Focus()
                    Case "ddlUnidad" : ddlEmpaque.Focus()
                    Case "ddlEmpaque" : ddlProveedor.Focus()
                    Case "ddlProveedor" : txtPrecioCostoUSD.Focus()
                    Case "txtPrecioCostoUSD" : txtPrecioMinimoFacturaUSD.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If

                    Case "txtPrecioCostoCOR" : txtPrecioMinimoFacturaUSD.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        'Case "txtPrecioMinimoFacturaUSD" : txtPrecioMinimoFactura.Focus()
                    Case "txtPrecioMinimoFacturaUSD" : txtPVDU.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If

                    Case "txtPrecioMinimoFactura" : txtPVDU.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        '  Case "txtPVDU" : txtPVDC.Focus()
                    Case "txtPVDU" : txtPVPU.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                    Case "txtPVDC" : txtPVPU.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If

                        '   Case "txtPVPU" : txtPVPC.Focus()
                    Case "txtPVPU" : ddlImpuesto.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If


                    Case "txtPVPC" : ddlImpuesto.Focus()
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVPU.Text) > 0 Then
                            txtPVPC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVPU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPVDU.Text) > 0 Then
                            txtPVDC.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPVDU.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) > 0 Then
                            txtPrecioCostoCOR.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioCostoUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                        If lnTasaCambio > 0 AndAlso SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) > 0 Then
                            txtPrecioMinimoFactura.Text = SUFunciones.RedondeoNumero((SUConversiones.ConvierteADouble(txtPrecioMinimoFacturaUSD.Text) * lnTasaCambio), objParametro.IndicaTipoRedondeo)
                        End If
                    Case "ddlImpuesto" : ddlEstado.Focus()
                    Case "ddlEstado" : txtObservacion.Focus()
                    Case "txtObservacion" : txtCodigoProducto.Focus()
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
            Timer1.Stop()
            Exit Sub
        End Try


    End Sub

    Private Sub TextBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrecioCostoUSD.LostFocus, txtPrecioCostoCOR.LostFocus, txtPVPC.LostFocus, txtPVDC.LostFocus, txtPVPU.LostFocus, txtPVDU.LostFocus
        Try
            If IsNumeric(sender.text) Then
                sender.text = Format(SUConversiones.ConvierteADouble(sender.text), "#,##0.#0")
            Else
                MsgBox("Tiene que ser un valor num�tico v�lido. Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                sender.focus()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Productos")
            Timer1.Stop()
            Exit Sub
        End Try

    End Sub

    Private Sub btnListadoProducto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListadoProducto.Click
        Try
            LlamaReporte(1)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Importar Recibos")
        End Try
    End Sub

    Private Sub btnPrecioCosto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrecioCosto.Click
        Try
            'Dim frmNew As New actrptViewer
            'intRptFactura = 30
            'intModulo = 43
            'strQuery = "exec sp_Reportes " & intModulo & " "
            'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            'frmNew.Show()
            If RNFacturas.ValidaPermisoVerCosto(lngRegUsuario) = 1 Then
                LlamaReporte(2)
            Else
                MsgBox("No tiene privilegios para ver este reporte")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Importar Recibos")
        End Try
    End Sub

    Private Sub btnlstPrecioMinorista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnlstPrecioMinorista.Click
        Try
            'Dim frmNew As New actrptViewer
            'intRptFactura = 30
            'intModulo = 44
            'strQuery = "exec sp_Reportes " & intModulo & " "
            'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            'frmNew.Show()
            LlamaReporte(5)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Importar Recibos")
        End Try
    End Sub

    Private Sub btnLstVentaMayorista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLstVentaMayorista.Click
        Try
            'Dim frmNew As New actrptViewer
            'intRptFactura = 30
            'intModulo = 45
            'strQuery = "exec sp_Reportes " & intModulo & " "
            'frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            'frmNew.Show()
            LlamaReporte(4)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Importar Recibos")
        End Try

    End Sub

    Private Sub btnPrimerRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrimerRegistro.Click
        Try
            MoverRegistro("Primero")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Buscar registro: " & ex.Message)
        End Try

    End Sub

    Private Sub btnAnteriorRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnteriorRegistro.Click
        Try
            MoverRegistro("Anterior")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Buscar registro: " & ex.Message)
        End Try

    End Sub

    Private Sub btnSiguienteRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSiguienteRegistro.Click
        Try
            MoverRegistro("Siguiente")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Buscar registro: " & ex.Message)
        End Try

    End Sub

    Private Sub btnUltimoRegistro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUltimoRegistro.Click
        Try
            MoverRegistro("Ultimo")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Buscar registro: " & ex.Message)
        End Try

    End Sub

    Private Sub btnIgual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIgual.Click
        Try
            MoverRegistro("Igual")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Buscar registro: " & ex.Message)
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error al limpiar: " & ex.Message)
        End Try

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            Guardar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error al guardar: " & ex.Message)
        End Try

    End Sub

    Private Sub btnListado_Click(sender As Object, e As EventArgs) Handles btnListado.Click

    End Sub

    Private Sub Label10_Click(sender As Object, e As EventArgs) Handles Label10.Click

    End Sub
End Class
