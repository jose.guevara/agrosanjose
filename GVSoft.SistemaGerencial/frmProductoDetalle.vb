Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv
Imports Microsoft.Office.Interop

Public Class frmProductoDetalle
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvxDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Agencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantidadAgencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoAgencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoAgencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaUltEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocUltEntra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantUltEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoUltEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoUltEntrada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaUltSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocUltSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantUltSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoUltSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoUltSalida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dgvxDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Agencia = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantidadAgencia = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostoAgencia = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MontoAgencia = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DocEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MontoEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DocSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostoSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MontoSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaUltEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DocUltEntra = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantUltEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostoUltEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MontoUltEntrada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaUltSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DocUltSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantUltSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CostoUltSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MontoUltSalida = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvxDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(752, 56)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Producto"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(672, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 32)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Exportar"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(256, 22)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(384, 20)
        Me.TextBox2.TabIndex = 3
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(88, 20)
        Me.TextBox1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(184, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvxDetalle)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(8, 80)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(752, 328)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos de los Movimientos"
        '
        'dgvxDetalle
        '
        Me.dgvxDetalle.AllowUserToAddRows = False
        Me.dgvxDetalle.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.dgvxDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Agencia, Me.CantidadAgencia, Me.CostoAgencia, Me.MontoAgencia, Me.FechaEntrada, Me.DocEntrada, Me.CantEntrada, Me.CostEntrada, Me.MontoEntrada, Me.FechaSalida, Me.DocSalida, Me.CantSalida, Me.CostoSalida, Me.MontoSalida, Me.FechaUltEntrada, Me.DocUltEntra, Me.CantUltEntrada, Me.CostoUltEntrada, Me.MontoUltEntrada, Me.FechaUltSalida, Me.DocUltSalida, Me.CantUltSalida, Me.CostoUltSalida, Me.MontoUltSalida})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxDetalle.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvxDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxDetalle.Location = New System.Drawing.Point(8, 24)
        Me.dgvxDetalle.Name = "dgvxDetalle"
        Me.dgvxDetalle.Size = New System.Drawing.Size(738, 295)
        Me.dgvxDetalle.TabIndex = 55
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        '
        'Agencia
        '
        Me.Agencia.HeaderText = "Agencia"
        Me.Agencia.Name = "Agencia"
        Me.Agencia.ReadOnly = True
        Me.Agencia.Width = 300
        '
        'CantidadAgencia
        '
        Me.CantidadAgencia.HeaderText = "Cantidad en Agencia"
        Me.CantidadAgencia.Name = "CantidadAgencia"
        Me.CantidadAgencia.ReadOnly = True
        '
        'CostoAgencia
        '
        Me.CostoAgencia.HeaderText = "Costo en Agencia"
        Me.CostoAgencia.Name = "CostoAgencia"
        Me.CostoAgencia.ReadOnly = True
        '
        'MontoAgencia
        '
        Me.MontoAgencia.HeaderText = "Monto en Agencia"
        Me.MontoAgencia.Name = "MontoAgencia"
        Me.MontoAgencia.ReadOnly = True
        '
        'FechaEntrada
        '
        Me.FechaEntrada.HeaderText = "Fecha 1ra Entrada"
        Me.FechaEntrada.Name = "FechaEntrada"
        Me.FechaEntrada.ReadOnly = True
        '
        'DocEntrada
        '
        Me.DocEntrada.HeaderText = "Doc 1ra Entrada"
        Me.DocEntrada.Name = "DocEntrada"
        Me.DocEntrada.ReadOnly = True
        '
        'CantEntrada
        '
        Me.CantEntrada.HeaderText = "Cant 1ra Entrada"
        Me.CantEntrada.Name = "CantEntrada"
        Me.CantEntrada.ReadOnly = True
        '
        'CostEntrada
        '
        Me.CostEntrada.HeaderText = "Costo 1ra Entrada"
        Me.CostEntrada.Name = "CostEntrada"
        Me.CostEntrada.ReadOnly = True
        '
        'MontoEntrada
        '
        Me.MontoEntrada.HeaderText = "Monto 1ra Entrada"
        Me.MontoEntrada.Name = "MontoEntrada"
        Me.MontoEntrada.ReadOnly = True
        '
        'FechaSalida
        '
        Me.FechaSalida.HeaderText = "Fecha 1ra Salida"
        Me.FechaSalida.Name = "FechaSalida"
        Me.FechaSalida.ReadOnly = True
        '
        'DocSalida
        '
        Me.DocSalida.HeaderText = "Doc 1ra Salida"
        Me.DocSalida.Name = "DocSalida"
        Me.DocSalida.ReadOnly = True
        '
        'CantSalida
        '
        Me.CantSalida.HeaderText = "Cant 1ra Salida"
        Me.CantSalida.Name = "CantSalida"
        Me.CantSalida.ReadOnly = True
        '
        'CostoSalida
        '
        Me.CostoSalida.HeaderText = "Costo 1ra Salidad"
        Me.CostoSalida.Name = "CostoSalida"
        Me.CostoSalida.ReadOnly = True
        '
        'MontoSalida
        '
        Me.MontoSalida.HeaderText = "Monto 1ra Salida"
        Me.MontoSalida.Name = "MontoSalida"
        Me.MontoSalida.ReadOnly = True
        '
        'FechaUltEntrada
        '
        Me.FechaUltEntrada.HeaderText = "Fecha Ult Entrada"
        Me.FechaUltEntrada.Name = "FechaUltEntrada"
        Me.FechaUltEntrada.ReadOnly = True
        '
        'DocUltEntra
        '
        Me.DocUltEntra.HeaderText = "Doc. Ult. Entrada"
        Me.DocUltEntra.Name = "DocUltEntra"
        Me.DocUltEntra.ReadOnly = True
        '
        'CantUltEntrada
        '
        Me.CantUltEntrada.HeaderText = "Cant. Ult. Entrada"
        Me.CantUltEntrada.Name = "CantUltEntrada"
        Me.CantUltEntrada.ReadOnly = True
        '
        'CostoUltEntrada
        '
        Me.CostoUltEntrada.HeaderText = "Costo Ult. Entrada"
        Me.CostoUltEntrada.Name = "CostoUltEntrada"
        Me.CostoUltEntrada.ReadOnly = True
        '
        'MontoUltEntrada
        '
        Me.MontoUltEntrada.HeaderText = "Monto Ult. Entrada"
        Me.MontoUltEntrada.Name = "MontoUltEntrada"
        Me.MontoUltEntrada.ReadOnly = True
        '
        'FechaUltSalida
        '
        Me.FechaUltSalida.HeaderText = "Fecha Ult. Salida"
        Me.FechaUltSalida.Name = "FechaUltSalida"
        Me.FechaUltSalida.ReadOnly = True
        '
        'DocUltSalida
        '
        Me.DocUltSalida.HeaderText = "Doc. Ult. Salida"
        Me.DocUltSalida.Name = "DocUltSalida"
        Me.DocUltSalida.ReadOnly = True
        '
        'CantUltSalida
        '
        Me.CantUltSalida.HeaderText = "Cant. Ult. Salida"
        Me.CantUltSalida.Name = "CantUltSalida"
        Me.CantUltSalida.ReadOnly = True
        '
        'CostoUltSalida
        '
        Me.CostoUltSalida.HeaderText = "Costo Ult. Salida"
        Me.CostoUltSalida.Name = "CostoUltSalida"
        Me.CostoUltSalida.ReadOnly = True
        '
        'MontoUltSalida
        '
        Me.MontoUltSalida.HeaderText = "Monto Ult. Salida"
        Me.MontoUltSalida.Name = "MontoUltSalida"
        Me.MontoUltSalida.ReadOnly = True
        '
        'frmProductoDetalle
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(768, 411)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProductoDetalle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmProductoDetalle"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvxDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmProductoDetalle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Limpiar()
        'MSFlexGrid2.FormatString = "C�digo   |<Agencia                                                        |<Cantidad en Agencia  |<Costo en Agencia |>Monto en Agencia |<Fecha 1ra Entrada  |<Docum. 1ra Entrada |>Cant. 1ra Entrada |>Costo 1ra Entrada |>Monto 1ra Entrada |<Fecha 1ra Salida |<Docum. 1ra Salida |>Cant. 1ra Salida |>Costo 1ra Salida |>Monto 1ra Salida |<Fecha Ult Entrada |<Docum. Ult Entrada |>Cant. Ult Entrada |>Costo Ult Entrada |>Monto Ult Entrada |<Fecha Ult Salida |<Docum. Ult Salida |>Cant. Ult Salida |>Costo Ult Salida |>Monto Ult Salida "
        ExtraerDatos()

    End Sub

    Private Sub frmProductoDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Sub ExtraerDatos()
        Try
            dtrAgro2K = RNProduto.ObtieneDetalleProducto(lngRegistro, 0)
            While dtrAgro2K.Read
                TextBox1.Text = dtrAgro2K.GetValue(2)
                TextBox2.Text = dtrAgro2K.GetValue(3)
                dgvxDetalle.Rows.Add(dtrAgro2K.GetValue(0), dtrAgro2K.GetValue(1), Format(dtrAgro2K.GetValue(24), "#,##0.#0"), _
                                     Format(dtrAgro2K.GetValue(25), "#,##0.#0"), Format(dtrAgro2K.GetValue(26), "#,##0.#0"), _
                                     dtrAgro2K.GetValue(4), dtrAgro2K.GetValue(5), Format(dtrAgro2K.GetValue(6), "#,##0.#0"), _
                                     Format(dtrAgro2K.GetValue(7), "#,##0.#0"), Format(dtrAgro2K.GetValue(8), "#,##0.#0"), _
                                     dtrAgro2K.GetValue(9), dtrAgro2K.GetValue(10), Format(dtrAgro2K.GetValue(11), "#,##0.#0"), _
                                     Format(dtrAgro2K.GetValue(12), "#,##0.#0"), Format(dtrAgro2K.GetValue(13), "#,##0.#0"), _
                                     dtrAgro2K.GetValue(14), dtrAgro2K.GetValue(15), Format(dtrAgro2K.GetValue(16), "#,##0.#0"), _
                                     Format(dtrAgro2K.GetValue(17), "#,##0.#0"), Format(dtrAgro2K.GetValue(18), "#,##0.#0"), _
                                     dtrAgro2K.GetValue(19), dtrAgro2K.GetValue(20), Format(dtrAgro2K.GetValue(21), "#,##0.#0"), _
                                     Format(dtrAgro2K.GetValue(22), "#,##0.#0"), Format(dtrAgro2K.GetValue(23), "#,##0.#0"))
            End While
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            dtrAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar()

        TextBox1.Text = ""
        TextBox2.Text = ""
        dgvxDetalle.Rows.Clear()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ExportarDatos()

    End Sub

    Sub ExportarDatos()

        Dim objApp As Excel.Application
        Dim objBooks As Excel.Workbooks
        Dim objBook As Excel._Workbook
        Dim objSheets As Excel.Sheets
        Dim objSheet As Excel._Worksheet
        Dim strRange As Excel.Range
        Dim saRet(dgvxDetalle.Rows.Count - 1, 25) As String
        Dim intFila As Integer, intColumna As Integer

        objApp = New Excel.Application
        objBooks = objApp.Workbooks
        objBook = objBooks.Add
        objSheets = objBook.Worksheets
        objSheet = objSheets(1)

        strRange = objSheet.Range("A1:Y1", Reflection.Missing.Value)
        strRange.Cells.Font.Bold = True
        strRange = objSheet.Range("A1", Reflection.Missing.Value)
        strRange = strRange.Resize(dgvxDetalle.Rows.Count - 1, 25)
        For intFila = 0 To dgvxDetalle.Rows.Count - 1
            For intColumna = 0 To 24
                saRet(intFila, intColumna) = dgvxDetalle.Rows(intFila).Cells(intColumna).Value
            Next intColumna
        Next intFila
        strRange.Value = saRet
        strRange.EntireColumn.AutoFit()
        objApp.Visible = True
        objApp.UserControl = True
        strRange = Nothing
        objSheet = Nothing
        objSheets = Nothing
        objBooks = Nothing

    End Sub

End Class
