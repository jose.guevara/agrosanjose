Imports GVSoft.SistemaGerencial.Entidades
Imports System.Collections.Generic
Public Class actrptViewer
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    Friend WithEvents Viewer1 As DataDynamics.ActiveReports.Viewer.Viewer

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Viewer1 = New DataDynamics.ActiveReports.Viewer.Viewer
        Me.SuspendLayout()
        '
        'Viewer1
        '
        Me.Viewer1.BackColor = System.Drawing.SystemColors.Control
        Me.Viewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Viewer1.Document = New DataDynamics.ActiveReports.Document.Document("ARNet Document")
        Me.Viewer1.Location = New System.Drawing.Point(0, 0)
        Me.Viewer1.Name = "Viewer1"
        Me.Viewer1.ReportViewer.CurrentPage = 0
        Me.Viewer1.ReportViewer.MultiplePageCols = 3
        Me.Viewer1.ReportViewer.MultiplePageRows = 2
        Me.Viewer1.ReportViewer.ViewType = DataDynamics.ActiveReports.Viewer.ViewType.Normal
        Me.Viewer1.Size = New System.Drawing.Size(840, 605)
        Me.Viewer1.TabIndex = 0
        Me.Viewer1.TableOfContents.Text = "Table Of Contents"
        Me.Viewer1.TableOfContents.Width = 200
        Me.Viewer1.TabTitleLength = 35
        Me.Viewer1.Toolbar.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        '
        'actrptViewer
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(840, 605)
        Me.Controls.Add(Me.Viewer1)
        Me.KeyPreview = True
        Me.Name = "actrptViewer"
        Me.Text = "actrptViewer"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private _NumeroRecibo As String
    Public Property NumeroRecibo() As String
        Get
            Return (_NumeroRecibo)
        End Get
        Set(ByVal value As String)
            _NumeroRecibo = value
        End Set
    End Property
    Private _IdProveedor As Integer
    Public Property IdProveedor() As Integer
        Get
            Return (_IdProveedor)
        End Get
        Set(ByVal value As Integer)
            _IdProveedor = value
        End Set
    End Property
    Private Sub actrptViewer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim sqlDS As New DataDynamics.ActiveReports.DataSources.SqlDBDataSource
        'Parent = Sistemas_Gerenciales.frmPrincipal
        'MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
        'MdiParent = Sistemas_Gerenciales.frmPrincipal
        sqlDS.ConnectionString = strAgro2K
        If blnVistaPrevia = True Then
            strQuery = "select * from cat_Generos"
        End If
        sqlDS.SQL = strQuery
        sqlDS.CommandTimeout = 0

        'MsgBox(strQuery)
        strNombreArchivo = ""
        If intRptExportar > 0 Then
            Dim SaveFileDialog1 As New SaveFileDialog
            Select Case intRptExportar
                Case 1 : SaveFileDialog1.Filter = "Excel files (*.xls)|*.xls"
                Case 2 : SaveFileDialog1.Filter = "Html files (*.htm)|*.htm"
                Case 3 : SaveFileDialog1.Filter = "Adobe Reader files (*.pdf)|*.pdf"
                Case 4 : SaveFileDialog1.Filter = "Rich Text File files (*.rtf)|*.rtf"
                Case 5 : SaveFileDialog1.Filter = "Image files (*.tiff)|*.tiff"
            End Select
            SaveFileDialog1.RestoreDirectory = True
            If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                strNombreArchivo = SaveFileDialog1.FileName
            Else
                intRptExportar = 0
            End If
        Else
            intRptExportar = 0
        End If

        Try
            If intRptFactura > 0 Then
                Select Case intRptFactura
                    Case 1
                        Dim rpt01 As New actrptVtasDetVtasArtic
                        Me.Viewer1.Document = rpt01.Document
                        rpt01.Document.Printer.PrinterName = ""
                        rpt01.DataSource = sqlDS
                        rpt01.Run()
                    Case 2
                        Dim rpt02 As New actrptVtasDetVtasVend
                        Me.Viewer1.Document = rpt02.Document
                        rpt02.Document.Printer.PrinterName = ""
                        rpt02.DataSource = sqlDS
                        rpt02.Run()
                    Case 3
                        Dim rpt03 As New actrptVtasArtic
                        Me.Viewer1.Document = rpt03.Document
                        rpt03.Document.Printer.PrinterName = ""
                        rpt03.DataSource = sqlDS
                        rpt03.Run()
                    Case 4
                        Dim rpt04 As New actrptVtasClas
                        Me.Viewer1.Document = rpt04.Document
                        rpt04.Document.Printer.PrinterName = ""
                        rpt04.DataSource = sqlDS
                        rpt04.Run()
                    Case 51
                        Dim rpt51 As New actrptVtasClts
                        Me.Viewer1.Document = rpt51.Document
                        rpt51.Document.Printer.PrinterName = ""
                        rpt51.DataSource = sqlDS
                        rpt51.Run()
                    Case 52
                        Dim rpt52 As New actrptVtasCltsDet
                        Me.Viewer1.Document = rpt52.Document
                        rpt52.Document.Printer.PrinterName = ""
                        rpt52.DataSource = sqlDS
                        rpt52.Run()
                    Case 6, 15, 62
                        Dim rpt06 As New actrptVtasFact
                        Me.Viewer1.Document = rpt06.Document
                        rpt06.Document.Printer.PrinterName = ""
                        rpt06.DataSource = sqlDS
                        rpt06.Run()
                    Case 7
                        Dim rpt07 As New actrptVtasNegc
                        Me.Viewer1.Document = rpt07.Document
                        rpt07.Document.Printer.PrinterName = ""
                        rpt07.DataSource = sqlDS
                        rpt07.Run()
                    Case 8
                        Dim rpt08 As New actrptVtasVend
                        Me.Viewer1.Document = rpt08.Document
                        rpt08.Document.Printer.PrinterName = ""
                        rpt08.DataSource = sqlDS
                        rpt08.Run()
                    Case 9
                        Dim rpt09 As New actrptVtasDept
                        Me.Viewer1.Document = rpt09.Document
                        rpt09.Document.Printer.PrinterName = ""
                        rpt09.DataSource = sqlDS
                        rpt09.Run()
                    Case 10, 13
                        Dim rpt10 As New actrptVtasVendClas
                        Me.Viewer1.Document = rpt10.Document
                        rpt10.Document.Printer.PrinterName = ""
                        rpt10.DataSource = sqlDS
                        rpt10.Run()
                    Case 11
                        Dim rpt11 As New actrptVtasCosto
                        Me.Viewer1.Document = rpt11.Document
                        rpt11.Document.Printer.PrinterName = ""
                        rpt11.DataSource = sqlDS
                        rpt11.Run()
                    Case 12
                        Dim rpt12 As New actrptVtasCostoVend
                        Me.Viewer1.Document = rpt12.Document
                        rpt12.Document.Printer.PrinterName = ""
                        rpt12.DataSource = sqlDS
                        rpt12.Run()
                    Case 14
                        Dim rpt14 As New actrptVtasCosto
                        Me.Viewer1.Document = rpt14.Document
                        rpt14.Document.Printer.PrinterName = ""
                        rpt14.DataSource = sqlDS
                        rpt14.Run()
                    Case 30
                        Dim rpt30 As New actrptClientes
                        Me.Viewer1.Document = rpt30.Document
                        rpt30.Document.Printer.PrinterName = ""
                        rpt30.DataSource = sqlDS
                        rpt30.Run()
                    Case 16
                        Dim rpt16 As New actrptIngesosMensuales
                        Me.Viewer1.Document = rpt16.Document
                        rpt16.Document.Printer.PrinterName = ""
                        rpt16.DataSource = sqlDS
                        rpt16.Run()
                    Case 17
                        Dim rpt16 As New actrptUtilidades
                        Me.Viewer1.Document = rpt16.Document
                        rpt16.Document.Printer.PrinterName = ""
                        rpt16.DataSource = sqlDS
                        rpt16.Run()
                    Case 61
                        Dim rpt61 As New actrptFacturaAnuladas
                        Me.Viewer1.Document = rpt61.Document
                        rpt61.Document.Printer.PrinterName = ""
                        rpt61.DataSource = sqlDS
                        rpt61.Run()
                    Case 70
                        Dim rpt70 As New actRptClientesConCuentaContable
                        Me.Viewer1.Document = rpt70.Document
                        rpt70.Document.Printer.PrinterName = ""
                        rpt70.DataSource = sqlDS
                        rpt70.Run()
                End Select
            ElseIf intRptInventario > 0 Then
                Select Case intRptInventario
                    Case 1
                        Dim rpt01 As New actrptInventKardex
                        Me.Viewer1.Document = rpt01.Document
                        rpt01.Document.Printer.PrinterName = ""
                        rpt01.DataSource = sqlDS
                        rpt01.Run()
                    Case 2
                        Dim rpt02 As New actrptInventGenMov
                        Me.Viewer1.Document = rpt02.Document
                        rpt02.Document.Printer.PrinterName = ""
                        rpt02.DataSource = sqlDS
                        rpt02.Run()
                    Case 3
                        Dim rpt03 As New actrptInventCostos
                        Me.Viewer1.Document = rpt03.Document
                        rpt03.Document.Printer.PrinterName = ""
                        rpt03.DataSource = sqlDS
                        rpt03.Run()
                    Case 4
                        Dim rpt04 As New actrptInventProdRevision
                        Me.Viewer1.Document = rpt04.Document
                        rpt04.Document.Printer.PrinterName = ""
                        rpt04.DataSource = sqlDS
                        rpt04.Run()
                    Case 5, 6, 7, 8
                        Dim rpt51 As New actrptInventProdCorUsd
                        Me.Viewer1.Document = rpt51.Document
                        rpt51.Document.Printer.PrinterName = ""
                        rpt51.DataSource = sqlDS
                        rpt51.Run()
                    Case 9, 10
                        Dim rpt09 As New actrptInventClaseCons
                        Me.Viewer1.Document = rpt09.Document
                        rpt09.Document.Printer.PrinterName = ""
                        rpt09.DataSource = sqlDS
                        rpt09.Run()
                    Case 11
                        Dim rpt11 As New actrptInventProdRevision
                        Me.Viewer1.Document = rpt11.Document
                        rpt11.Document.Printer.PrinterName = ""
                        rpt11.DataSource = sqlDS
                        rpt11.Run()
                    Case 12, 13
                        Dim rpt12 As New actrptInventAgenCons
                        Me.Viewer1.Document = rpt12.Document
                        rpt12.Document.Printer.PrinterName = ""
                        rpt12.DataSource = sqlDS
                        rpt12.Run()
                    Case 14
                        Dim rpt14 As New actrptInventLaboratorio
                        Me.Viewer1.Document = rpt14.Document
                        rpt14.Document.Printer.PrinterName = ""
                        rpt14.DataSource = sqlDS
                        rpt14.Run()
                    Case 15
                        Dim rpt15 As New actrptVerificarCostos
                        Me.Viewer1.Document = rpt15.Document
                        rpt15.Document.Printer.PrinterName = ""
                        rpt15.DataSource = sqlDS
                        rpt15.Run()
                    Case 16, 17
                        Dim rpt16 As New actrptInventClaseAgencias
                        Me.Viewer1.Document = rpt16.Document
                        rpt16.Document.Printer.PrinterName = ""
                        rpt16.DataSource = sqlDS
                        rpt16.Run()
                    Case 18, 19
                        Dim rpt18 As New actrptInventCompraFactura
                        Me.Viewer1.Document = rpt18.Document
                        rpt18.Document.Printer.PrinterName = ""
                        rpt18.DataSource = sqlDS
                        rpt18.Run()
                    Case 20
                        Dim rpt20 As New actrptInventKardexSimple
                        Me.Viewer1.Document = rpt20.Document
                        rpt20.Document.Printer.PrinterName = ""
                        rpt20.DataSource = sqlDS
                        rpt20.Run()
                    Case 21
                        Dim rpt21 As New actRptTrasladosInventarios
                        Me.Viewer1.Document = rpt21.Document
                        rpt21.Document.Printer.PrinterName = ""
                        rpt21.DataSource = sqlDS
                        rpt21.Run()
                    Case 22
                        Dim rpt22 As New actRptTrasladosInventarios
                        Me.Viewer1.Document = rpt22.Document
                        rpt22.Document.Printer.PrinterName = ""
                        rpt22.DataSource = sqlDS
                        rpt22.Run()
                    Case 23
                        Dim rpt51 As New actrptInventExistenciaProdCorUsd
                        Me.Viewer1.Document = rpt51.Document
                        rpt51.Document.Printer.PrinterName = ""
                        rpt51.DataSource = sqlDS
                        rpt51.Run()
                End Select
            ElseIf intRptImpFactura > 0 Then
                If intRptImpFactura = 1 Then
                    Dim rpt01 As New actrptFactContado
                    Me.Viewer1.Document = rpt01.Document
                    If intImpresoraAsignada = 0 Then
                        'rpt01.Document.Printer.PrinterName = ""
                    Else
                        rpt01.Document.Printer.PrinterName = ""
                    End If
                    rpt01.Run()
                ElseIf intRptImpFactura = 12 Then
                    'Imprimir Ticket de Venta
                    Dim rpt01 As New actrptImprimirTicketVenta2
                    Me.Viewer1.Document = rpt01.Document
                    If intImpresoraAsignada = 0 Then
                        'rpt01.Document.Printer.PrinterName = ""
                    Else
                        rpt01.Document.Printer.PrinterName = ""
                    End If
                    'sqlDS.SQL = dtDatos.ToString
                    rpt01.DataSource = sqlDS
                    rpt01.Run()

                ElseIf intRptImpFactura = 4 Then
                    If intFechaCambioFactura = 0 Or (lngFechaFactura < intFechaCambioFactura) Then
                        Dim rpt02 As New actrptFactCredito
                        Me.Viewer1.Document = rpt02.Document
                        If intImpresoraAsignada = 0 Then
                            'rpt02.Document.Printer.PrinterName = ""
                        Else
                            rpt02.Document.Printer.PrinterName = ""
                        End If
                        rpt02.Run()
                    ElseIf intFechaCambioFactura <> 0 Then
                        Dim rpt03 As New actrptFactCreditoNvo
                        Me.Viewer1.Document = rpt03.Document
                        If intImpresoraAsignada = 0 Then
                            'rpt03.Document.Printer.PrinterName = ""
                        Else
                            rpt03.Document.Printer.PrinterName = ""
                        End If
                        rpt03.Run()
                    End If
                End If
            ElseIf intRptCtasCbr > 0 Then
                If intRptCtasCbr = 1 Or intRptCtasCbr = 3 Then
                    Dim rpt01 As New actrptCtasCbrEstCtasCSD
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 2 Or intRptCtasCbr = 4 Then
                    Dim rpt01 As New actrptCtasCbrEstCtasCCD
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 30 Or intRptCtasCbr = 31 Then
                    Dim rpt01 As New actrptCtasCbrEstCtasCCD
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 7 Then
                    Dim rpt01 As New actrptCtasCbrSldsClts
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 8 Then
                    Dim rpt01 As New actrptCtasCbrSldsCltsVend
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 9 Then
                    Dim rpt01 As New actrptCtasCbrSldsAnalisisAntig
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 12 Then
                    Dim rpt01 As New actrptCtasCbrSldsAnalisisAntig2
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 10 Or intRptCtasCbr = 11 Then
                    Dim rpt01 As New actrptCtasCbrClientesVendedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 13 Then
                    Dim rpt01 As New actrptCtasCbrFactPend
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 14 Then
                    Dim rpt01 As New actrptCtasCbrSldsPend
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 15 Then
                    Dim rpt01 As New actrptCtasCbrSldsHistMov
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 16 Then
                    Dim rpt01 As New actrptCtasCbrSldsHistInt
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 17 Or intRptCtasCbr = 20 Or intRptCtasCbr = 23 Then
                    Dim rpt01 As New actrptCtasCbrRbsFecha
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 18 Or intRptCtasCbr = 21 Or intRptCtasCbr = 24 Then
                    Dim rpt01 As New actrptCtasCbrRbsVndCts
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 19 Or intRptCtasCbr = 22 Or intRptCtasCbr = 25 Then
                    Dim rpt01 As New actrptCtasCbrRbsClientes
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 27 Then
                    Dim rpt01 As New actrptCtasCbrFactPend
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 28 Then
                    Dim rpt01 As New actrptCtasCbrFactPend
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 29 Then
                    Dim rpt01 As New actrptRecibosAnulados
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCtasCbr = 32 Then
                    Dim rpt01 As New actrptFaturasPendientesDetalladas
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                End If

            ElseIf intRptPresup > 0 Then
                If intRptPresup = 2 Then
                    Dim rpt01 As New actrptPresupVariacionVend
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptPresup = 3 Or intRptPresup = 4 Then
                    Dim rpt01 As New actrptPresupVariacionGbl
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptPresup = 5 Then
                    Dim rpt01 As New actrptPresupVariacionProd
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                End If
            ElseIf intRptOtros > 0 Then
                If intRptOtros = 1 Then
                    Dim rpt01 As New actrptCambiarPrecios
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptOtros = 2 Then
                    Dim rpt02 As New actrptFactCorr
                    Me.Viewer1.Document = rpt02.Document
                    rpt02.Document.Printer.PrinterName = ""
                    rpt02.DataSource = sqlDS
                    rpt02.Run()
                ElseIf intRptOtros = 3 Then
                    Dim rpt02 As New actrptPrcEstadoInvent
                    Me.Viewer1.Document = rpt02.Document
                    rpt02.Document.Printer.PrinterName = ""
                    rpt02.DataSource = sqlDS
                    rpt02.Run()
                ElseIf intRptOtros = 4 Then
                    Dim rpt02 As New actrptCierreAnualDif
                    Me.Viewer1.Document = rpt02.Document
                    rpt02.Document.Printer.PrinterName = ""
                    rpt02.DataSource = sqlDS
                    rpt02.Run()
                End If
            ElseIf intRptTipoPrecio > 0 Then
                If intRptTipoPrecio = 1 Then
                    Dim rpt01 As New actrptTipoPrecioProv
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptTipoPrecio = 2 Then
                    Dim rpt02 As New actrptTipoPrecioClase
                    Me.Viewer1.Document = rpt02.Document
                    rpt02.Document.Printer.PrinterName = ""
                    rpt02.DataSource = sqlDS
                    rpt02.Run()
                ElseIf intRptTipoPrecio = 3 Then
                    Dim rpt02 As New actrptTipoPrecioSubClase
                    Me.Viewer1.Document = rpt02.Document
                    rpt02.Document.Printer.PrinterName = ""
                    rpt02.DataSource = sqlDS
                    rpt02.Run()
                End If
            ElseIf intRptImpRecibos > 0 Then
                If intRptImpRecibos = 1 Then
                    Dim rpt01 As New actrptRecibosCaja
                    rpt01.NumeroRecibo = NumeroRecibo
                    Me.Viewer1.Document = rpt01.Document
                    'rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 2 Then
                    Dim rpt01 As New actrptRecibosNTC
                    Me.Viewer1.Document = rpt01.Document
                    'rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 3 Then
                    Dim rpt01 As New actrptRecibosNTD
                    Me.Viewer1.Document = rpt01.Document
                    'rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 7 Then
                    Dim rpt01 As New actrptRecibosVarios
                    Me.Viewer1.Document = rpt01.Document
                    'rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 9 Then
                    Dim rpt01 As New actrptRecibosCaja
                    rpt01.NumeroRecibo = NumeroRecibo
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 10 Then
                    Dim rpt01 As New actrptRecibosNTC
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 11 Then
                    Dim rpt01 As New actrptRecibosNTD
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptImpRecibos = 12 Then
                    Dim rpt01 As New actrptRecibosVarios
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                End If
            ElseIf intRptCheques > 0 Then
                If intRptCheques = 1 Then
                    Dim rpt01 As New actrptMovCheques
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCheques = 2 Then
                    Dim rpt01 As New actrptMovDepositos
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCheques = 3 Then
                    Dim rpt01 As New actrptMovNTD
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intRptCheques = 4 Then
                    Dim rpt01 As New actrptMovNTC
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                End If
            ElseIf intrptCtasPagar > 0 Then
                If intrptCtasPagar = 1 Then
                    Dim rpt01 As New actrptEstadoCuentasProveedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 2 Then
                    Dim rpt01 As New actrptSaldosDetalladosXProveedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    'lngRegistro = rpt01.Parameters("NumeroFactura").Value
                    rpt01.Run()
                ElseIf intrptCtasPagar = 3 Then
                    Dim rpt01 As New actRptSaldosGeneralesDetallados
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 4 Then
                    Dim rpt01 As New actrptRecibosProveedorAnulados
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 5 Then
                    Dim rpt01 As New actRptRecibosCajaXFechas
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 6 Then
                    Dim rpt01 As New actRptMovimientoHistoricoProveedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 7 Then ' Factruas pendientes
                    Dim rpt01 As New actRptFacturasPendientesProveedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 8 Then  ' Factruas pendientes con mas de 4 meses y menos de un a�o
                    Dim rpt01 As New actRptFacturasPendientesProveedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 9 Then  ' Factruas pendientes con mas de un a�o
                    Dim rpt01 As New actRptFacturasPendientesProveedores
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 10 Then  ' Resumen de Facturas de Compras
                    Dim rpt01 As New actRptResumenCompras
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 11 Then  ' Descuentos de Facturas de Compras
                    Dim rpt01 As New actRptInformacionDescuentoCompras
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 12 Then  ' Informacion de Interes de Factuas de Compras
                    Dim rpt01 As New actRptInteresFacturasCompra
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                ElseIf intrptCtasPagar = 13 Then  ' Informacion de Interes de Factuas de Compras
                    Dim rpt01 As New actrptRecibosCajaCtaPorPagar
                    Me.Viewer1.Document = rpt01.Document
                    rpt01.IdProveedor = IdProveedor
                    rpt01.NumeroRecibo = NumeroRecibo
                    rpt01.Document.Printer.PrinterName = ""
                    rpt01.DataSource = sqlDS
                    rpt01.Run()
                End If

            ElseIf intRptCuadreCaja > 0 Then
                If intRptCuadreCaja = 1 Then
                    Dim rpt As New actRptImprimeCierreCuadreCaja
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 2 Then
                    Dim rpt As New actRptImprimeCierreCuadreCaja
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 3 Then
                    Dim rpt As New actRptImprimeCierreCuadreCaja
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 4 Then
                    Dim rpt As New actRptArqueoCajasTodos
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 5 Then
                    Dim rpt As New actRptImprimeCierreCuadreCaja
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 6 Then
                    Dim rpt As New actRptImprimeDeposito
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 7 Then
                    Dim rpt As New actRptSobrantesCaja
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptCuadreCaja = 8 Then
                    Dim rpt As New actRptDetalleDeposito
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()

                End If

            ElseIf intRptProformas > 0 Then
                If intRptProformas = 1 Then
                    Dim rpt As New actRptProforma
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptProformas = 2 Then
                    Dim rpt As New actRptProforma
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                End If

            ElseIf intRptImpCompras > 0 Then
                If intRptImpCompras = 1 Then
                    'Dim rpt As New actrptImprimeFactCredito
                    Dim rpt As New actrptImprimeFactContado
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                ElseIf intRptImpCompras = 4 Then
                    Dim rpt As New actrptImprimeFactContado
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()
                End If
            ElseIf intRptImppedidoProveedor > 0 Then
                If intRptImppedidoProveedor = 1 Then
                    'Dim rpt As New actrptImprimeFactCredito
                    Dim rpt As New actRptPedidoProveedor
                    Me.Viewer1.Document = rpt.Document
                    rpt.Document.Printer.PrinterName = ""
                    rpt.DataSource = sqlDS
                    rpt.Run()

                End If

            End If

            intRptImpCompras = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            intRptCtasCbr = 0
            intRptPresup = 0
            intRptOtros = 0
            intRptTipoPrecio = 0
            intRptImpRecibos = 0
            intRptCheques = 0
            intrptCtasPagar = 0
            intRptCuadreCaja = 0
            intRptProformas = 0
            strDireccionClienteFact = ""

        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try

    End Sub

    Private Sub actrptViewer_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
