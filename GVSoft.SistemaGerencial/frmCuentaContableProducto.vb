﻿Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar
Public Class frmCuentaContableProducto
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmCuentaContableProducto"

    Private Sub CargaComboSucursales()
        Try
            RNAgencias.CargarComboAgencias(cmbAgencia, lngRegUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub CargaComboProductos()
        Try
            RNProduto.CargarComboProducto(cmbProducto)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub IngresaCuentaContableProducto()
        Dim lsCuentaContable As String = String.Empty
        Dim lnIdProducto As Integer = 0
        Dim lnIdAgencia As Integer = 0
        Try
            If cmbProducto.SelectedItem IsNot Nothing Then
                lnIdProducto = SUConversiones.ConvierteAInt(cmbProducto.SelectedItem.ToString())
            End If

            If cmbAgencia.SelectedItem IsNot Nothing Then
                lnIdAgencia = SUConversiones.ConvierteAInt(cmbAgencia.SelectedItem.ToString())
            End If

            lsCuentaContable = txtCuentaContable.Text.Trim
            If (lnIdProducto = 0) Then
                MessageBox.Show("Producto es requerido", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lnIdAgencia = 0) Then
                MessageBox.Show("Agencia es requerida", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lsCuentaContable.Trim().Length <= 0) Then
                MessageBox.Show("Cuenta contable es requerida", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            RNCuentaContableProducto.IngresaCuentaContableProducto(lnIdAgencia, lnIdProducto, lsCuentaContable)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ActualizaCuentaContableProducto()
        Dim lsCuentaContable As String = String.Empty
        Dim lnIdProducto As Integer = 0
        Dim lnIdAgencia As Integer = 0
        Dim lnActivo As Integer = 0
        Try
            If cmbProducto.SelectedItem IsNot Nothing Then
                lnIdProducto = SUConversiones.ConvierteAInt(cmbProducto.SelectedItem.ToString())
            End If

            If cmbAgencia.SelectedItem IsNot Nothing Then
                lnIdAgencia = SUConversiones.ConvierteAInt(cmbAgencia.SelectedItem.ToString())
            End If

            lsCuentaContable = txtCuentaContable.Text.Trim
            If (lnIdProducto = 0) Then
                MessageBox.Show("Producto es requerido", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lnIdAgencia = 0) Then
                MessageBox.Show("Agencia es requerida", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lsCuentaContable.Trim().Length <= 0) Then
                MessageBox.Show("Cuenta contable es requerida", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            lnActivo = 0
            If chbActivo.Checked Then
                lnActivo = 1
            End If

            RNCuentaContableProducto.ActualizaCuentaContableProducto(lnIdAgencia, lnIdProducto, lsCuentaContable, lnActivo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ObtieneCuentaContableProductoxLlave()
        Dim lnIdProducto As Integer = 0
        Dim lnIdAgencia As Integer = 0
        Dim dtDatos As DataTable = Nothing
        Try
            If cmbProducto.SelectedItem IsNot Nothing Then
                lnIdProducto = SUConversiones.ConvierteAInt(cmbProducto.SelectedItem.ToString())
            End If

            If cmbAgencia.SelectedItem IsNot Nothing Then
                lnIdAgencia = SUConversiones.ConvierteAInt(cmbAgencia.SelectedItem.ToString())
            End If

            If (lnIdProducto = 0) Then
                MessageBox.Show("Producto es requerido", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            If (lnIdAgencia = 0) Then
                MessageBox.Show("Agencia es requerida", "Ingreso registro", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If


            dtDatos = RNCuentaContableProducto.ObtieneCuentaContableProductoxLlave(lnIdAgencia, lnIdProducto)

            If SUFunciones.ValidaDataTable(dtDatos) Then
                cmbAgencia.SelectedValue = dtDatos.Rows(0)("IdAgencia")
                cmbProducto.SelectedValue = dtDatos.Rows(0)("IdProducto")
                txtCuentaContable.Text = dtDatos.Rows(0)("CuentaContable")
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
    End Sub

    Private Sub frmCuentaContableProducto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            CargaComboProductos()
            CargaComboSucursales()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ObtieneDatosCuentaContable()
        Dim IdAgencia As Integer = 0
        Dim IdProducto As Integer = 0
        Dim dtDatos As DataTable = Nothing
        Dim IndicaEjecutaConsulta As Boolean = False
        Try
            Limpiar2()
            If cmbAgencia.Focused Then
                IndicaEjecutaConsulta = True
            End If
            If cmbProducto.Focused Then
                IndicaEjecutaConsulta = True
            End If

            If IndicaEjecutaConsulta Then
                IdAgencia = ConvierteAInt(cmbAgencia.SelectedValue)
                IdProducto = ConvierteAInt(cmbProducto.SelectedValue)

                dtDatos = RNCuentaContableProducto.ObtieneCuentaContableProductoxLlave(IdAgencia, IdProducto)

                If SUFunciones.ValidaDataTable(dtDatos) Then
                    txtCuentaContable.Text = dtDatos.Rows(0)("CuentaContable").ToString()
                    chbActivo.Checked = False
                    If dtDatos.Rows(0)("Activo").ToString() = "True" Then
                        chbActivo.Checked = True
                    End If
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Private Sub cmbProducto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProducto.SelectedIndexChanged
        Try
            ObtieneDatosCuentaContable()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub Limpiar()
        Try
            cmbAgencia.SelectedValue = 0
            cmbProducto.SelectedValue = 0
            txtCuentaContable.Text = String.Empty
            chbActivo.Checked = False
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub
    Private Sub Limpiar2()
        Try
            txtCuentaContable.Text = String.Empty
            chbActivo.Checked = False
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Try
            Guardar()
            MsgBox("Registro guardado exitosamente ", MsgBoxStyle.Information, "Guardar datos")
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Private Sub Guardar()
        Dim IdAgencia As Integer = 0
        Dim IdProducto As Integer = 0
        Dim CuentaContable As String = String.Empty
        Dim Activo As Integer = 0
        Try
            IdAgencia = SUConversiones.ConvierteAInt(cmbAgencia.SelectedValue)
            IdProducto = SUConversiones.ConvierteAInt(cmbProducto.SelectedValue)
            CuentaContable = txtCuentaContable.Text.Trim
            Activo = 0
            If chbActivo.Checked Then
                Activo = 1
            End If
            RNCuentaContableProducto.GuardaCuentaContableProducto(IdAgencia, IdProducto, CuentaContable, Activo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
    End Sub

    Private Sub cmbAgencia_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgencia.SelectedIndexChanged
        Try
            ObtieneDatosCuentaContable()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub btnReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReporte.Click
        Try
            Dim frmNew As New actrptViewer
            intRptFactura = 30
            strQuery = "exec sp_Reportes " & intModulo & " "
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Reportes")
        End Try
    End Sub
End Class