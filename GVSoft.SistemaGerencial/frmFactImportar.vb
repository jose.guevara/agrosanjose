Imports System.IO
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmFactImportar
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDiasCredito As System.Windows.Forms.TextBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents btnEliminarRegistro As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRetencionFactContado As System.Windows.Forms.TextBox
    Friend WithEvents txtImpuestoFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTotalFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotalFactCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtImpuestoFactCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtRetencionFactCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalFactCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidadFactCredito As System.Windows.Forms.TextBox
    Friend WithEvents txtSubtotal As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotalFactContado As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtImpuesto As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtRentencion As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents cmdVendedor As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdClientes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProductos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdRefrescar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImportar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem3 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents txtMoneda As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Codigo As DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As DataGridViewTextBoxColumn
    Friend WithEvents Precio As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Valor As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents IVA As DataGridViewTextBoxColumn
    Friend WithEvents Label18 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFactImportar))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtRentencion = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtImpuesto = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtSubtotal = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnEliminarRegistro = New System.Windows.Forms.Button()
        Me.txtDiasCredito = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNombreCliente = New System.Windows.Forms.TextBox()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtCantidadFactCredito = New System.Windows.Forms.TextBox()
        Me.txtTotalFactCredito = New System.Windows.Forms.TextBox()
        Me.txtRetencionFactCredito = New System.Windows.Forms.TextBox()
        Me.txtImpuestoFactCredito = New System.Windows.Forms.TextBox()
        Me.txtSubTotalFactCredito = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtSubTotalFactContado = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCantidadFactContado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTotalFactContado = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtRetencionFactContado = New System.Windows.Forms.TextBox()
        Me.txtImpuestoFactContado = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.cmdVendedor = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdClientes = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdProductos = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdRefrescar = New DevComponents.DotNetBar.ButtonItem()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImportar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem3 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.txtMoneda = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 72)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(791, 72)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas en Carga de la Agencia"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(56, 48)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(0, 13)
        Me.Label18.TabIndex = 5
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(136, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(24, 21)
        Me.ComboBox2.TabIndex = 4
        Me.ComboBox2.Text = "ComboBox2"
        Me.ComboBox2.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(179, 24)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(544, 40)
        Me.TextBox1.TabIndex = 3
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(56, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(122, 21)
        Me.ComboBox1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Factura"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtMoneda)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.txtTotal)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.txtRentencion)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtImpuesto)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtSubtotal)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.btnEliminarRegistro)
        Me.GroupBox2.Controls.Add(Me.txtDiasCredito)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtNombreCliente)
        Me.GroupBox2.Controls.Add(Me.txtCliente)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 148)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(791, 88)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Encabezado de la Factura"
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(491, 56)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(88, 20)
        Me.txtTotal.TabIndex = 23
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(451, 56)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(36, 13)
        Me.Label17.TabIndex = 22
        Me.Label17.Text = "Total"
        '
        'txtRentencion
        '
        Me.txtRentencion.Location = New System.Drawing.Point(363, 56)
        Me.txtRentencion.Name = "txtRentencion"
        Me.txtRentencion.Size = New System.Drawing.Size(72, 20)
        Me.txtRentencion.TabIndex = 20
        Me.txtRentencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(299, 56)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(65, 13)
        Me.Label16.TabIndex = 19
        Me.Label16.Text = "Retenci�n"
        '
        'txtImpuesto
        '
        Me.txtImpuesto.Location = New System.Drawing.Point(211, 56)
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.Size = New System.Drawing.Size(72, 20)
        Me.txtImpuesto.TabIndex = 18
        Me.txtImpuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(155, 56)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 13)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "Impuesto"
        '
        'txtSubtotal
        '
        Me.txtSubtotal.Location = New System.Drawing.Point(67, 56)
        Me.txtSubtotal.Name = "txtSubtotal"
        Me.txtSubtotal.Size = New System.Drawing.Size(88, 20)
        Me.txtSubtotal.TabIndex = 16
        Me.txtSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 56)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(58, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "SubTotal"
        '
        'btnEliminarRegistro
        '
        Me.btnEliminarRegistro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminarRegistro.Location = New System.Drawing.Point(611, 50)
        Me.btnEliminarRegistro.Name = "btnEliminarRegistro"
        Me.btnEliminarRegistro.Size = New System.Drawing.Size(120, 32)
        Me.btnEliminarRegistro.TabIndex = 6
        Me.btnEliminarRegistro.Text = "Eliminar Factura"
        '
        'txtDiasCredito
        '
        Me.txtDiasCredito.Location = New System.Drawing.Point(565, 24)
        Me.txtDiasCredito.Name = "txtDiasCredito"
        Me.txtDiasCredito.Size = New System.Drawing.Size(40, 20)
        Me.txtDiasCredito.TabIndex = 5
        Me.txtDiasCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(486, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "D�as Cr�dito"
        '
        'txtNombreCliente
        '
        Me.txtNombreCliente.Location = New System.Drawing.Point(156, 24)
        Me.txtNombreCliente.Name = "txtNombreCliente"
        Me.txtNombreCliente.Size = New System.Drawing.Size(327, 20)
        Me.txtNombreCliente.TabIndex = 3
        '
        'txtCliente
        '
        Me.txtCliente.Location = New System.Drawing.Point(69, 24)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(88, 20)
        Me.txtCliente.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Cliente"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvDetalle)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 238)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(791, 143)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Detalle de la Factura"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Cantidad, Me.Precio, Me.Valor, Me.IVA})
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(8, 19)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(773, 115)
        Me.dgvDetalle.TabIndex = 94
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Importar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Procesar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(0, 387)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(791, 138)
        Me.GroupBox4.TabIndex = 15
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Resumen de la Importaci�n"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtCantidadFactCredito)
        Me.GroupBox6.Controls.Add(Me.txtTotalFactCredito)
        Me.GroupBox6.Controls.Add(Me.txtRetencionFactCredito)
        Me.GroupBox6.Controls.Add(Me.txtImpuestoFactCredito)
        Me.GroupBox6.Controls.Add(Me.txtSubTotalFactCredito)
        Me.GroupBox6.Controls.Add(Me.Label9)
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Controls.Add(Me.Label11)
        Me.GroupBox6.Controls.Add(Me.Label12)
        Me.GroupBox6.Controls.Add(Me.Label13)
        Me.GroupBox6.Location = New System.Drawing.Point(16, 76)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(766, 56)
        Me.GroupBox6.TabIndex = 13
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Facturas al Cr�dito"
        '
        'txtCantidadFactCredito
        '
        Me.txtCantidadFactCredito.Location = New System.Drawing.Point(632, 24)
        Me.txtCantidadFactCredito.Name = "txtCantidadFactCredito"
        Me.txtCantidadFactCredito.Size = New System.Drawing.Size(40, 20)
        Me.txtCantidadFactCredito.TabIndex = 22
        Me.txtCantidadFactCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalFactCredito
        '
        Me.txtTotalFactCredito.Location = New System.Drawing.Point(480, 24)
        Me.txtTotalFactCredito.Name = "txtTotalFactCredito"
        Me.txtTotalFactCredito.Size = New System.Drawing.Size(88, 20)
        Me.txtTotalFactCredito.TabIndex = 21
        Me.txtTotalFactCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRetencionFactCredito
        '
        Me.txtRetencionFactCredito.Location = New System.Drawing.Point(360, 24)
        Me.txtRetencionFactCredito.Name = "txtRetencionFactCredito"
        Me.txtRetencionFactCredito.Size = New System.Drawing.Size(72, 20)
        Me.txtRetencionFactCredito.TabIndex = 20
        Me.txtRetencionFactCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpuestoFactCredito
        '
        Me.txtImpuestoFactCredito.Location = New System.Drawing.Point(216, 24)
        Me.txtImpuestoFactCredito.Name = "txtImpuestoFactCredito"
        Me.txtImpuestoFactCredito.Size = New System.Drawing.Size(72, 20)
        Me.txtImpuestoFactCredito.TabIndex = 19
        Me.txtImpuestoFactCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSubTotalFactCredito
        '
        Me.txtSubTotalFactCredito.Location = New System.Drawing.Point(64, 24)
        Me.txtSubTotalFactCredito.Name = "txtSubTotalFactCredito"
        Me.txtSubTotalFactCredito.Size = New System.Drawing.Size(88, 20)
        Me.txtSubTotalFactCredito.TabIndex = 18
        Me.txtSubTotalFactCredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(576, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Cantidad"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(440, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(36, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Total"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(296, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(65, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Retenci�n"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(160, 24)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 13)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Impuesto"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 13)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "SubTotal"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtSubTotalFactContado)
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.txtCantidadFactContado)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.txtTotalFactContado)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.txtRetencionFactContado)
        Me.GroupBox5.Controls.Add(Me.txtImpuestoFactContado)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Location = New System.Drawing.Point(16, 18)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(766, 56)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Facturas al Contado"
        '
        'txtSubTotalFactContado
        '
        Me.txtSubTotalFactContado.Location = New System.Drawing.Point(64, 24)
        Me.txtSubTotalFactContado.Name = "txtSubTotalFactContado"
        Me.txtSubTotalFactContado.Size = New System.Drawing.Size(88, 20)
        Me.txtSubTotalFactContado.TabIndex = 20
        Me.txtSubTotalFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "SubTotal"
        '
        'txtCantidadFactContado
        '
        Me.txtCantidadFactContado.Location = New System.Drawing.Point(632, 24)
        Me.txtCantidadFactContado.Name = "txtCantidadFactContado"
        Me.txtCantidadFactContado.Size = New System.Drawing.Size(40, 20)
        Me.txtCantidadFactContado.TabIndex = 18
        Me.txtCantidadFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(576, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Cantidad"
        '
        'txtTotalFactContado
        '
        Me.txtTotalFactContado.Location = New System.Drawing.Point(480, 24)
        Me.txtTotalFactContado.Name = "txtTotalFactContado"
        Me.txtTotalFactContado.Size = New System.Drawing.Size(88, 20)
        Me.txtTotalFactContado.TabIndex = 16
        Me.txtTotalFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(440, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(36, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Total"
        '
        'txtRetencionFactContado
        '
        Me.txtRetencionFactContado.Location = New System.Drawing.Point(360, 24)
        Me.txtRetencionFactContado.Name = "txtRetencionFactContado"
        Me.txtRetencionFactContado.Size = New System.Drawing.Size(72, 20)
        Me.txtRetencionFactContado.TabIndex = 14
        Me.txtRetencionFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpuestoFactContado
        '
        Me.txtImpuestoFactContado.Location = New System.Drawing.Point(216, 24)
        Me.txtImpuestoFactContado.Name = "txtImpuestoFactContado"
        Me.txtImpuestoFactContado.Size = New System.Drawing.Size(72, 20)
        Me.txtImpuestoFactContado.TabIndex = 13
        Me.txtImpuestoFactContado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(296, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Retenci�n"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(160, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Impuesto"
        '
        'Timer1
        '
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'cmdVendedor
        '
        Me.cmdVendedor.GlobalItem = False
        Me.cmdVendedor.Image = CType(resources.GetObject("cmdVendedor.Image"), System.Drawing.Image)
        Me.cmdVendedor.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdVendedor.Name = "cmdVendedor"
        Me.cmdVendedor.Text = "Vendedores"
        '
        'cmdClientes
        '
        Me.cmdClientes.GlobalItem = False
        Me.cmdClientes.Image = CType(resources.GetObject("cmdClientes.Image"), System.Drawing.Image)
        Me.cmdClientes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdClientes.Name = "cmdClientes"
        Me.cmdClientes.Text = "Clientes"
        '
        'cmdProductos
        '
        Me.cmdProductos.GlobalItem = False
        Me.cmdProductos.Image = CType(resources.GetObject("cmdProductos.Image"), System.Drawing.Image)
        Me.cmdProductos.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdProductos.Name = "cmdProductos"
        Me.cmdProductos.Text = "Productos"
        '
        'cmdRefrescar
        '
        Me.cmdRefrescar.GlobalItem = False
        Me.cmdRefrescar.Image = CType(resources.GetObject("cmdRefrescar.Image"), System.Drawing.Image)
        Me.cmdRefrescar.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdRefrescar.Name = "cmdRefrescar"
        Me.cmdRefrescar.Text = "Refrescar"
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdImportar, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(791, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 34
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdImportar
        '
        Me.cmdImportar.Image = CType(resources.GetObject("cmdImportar.Image"), System.Drawing.Image)
        Me.cmdImportar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImportar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImportar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdImportar.Name = "cmdImportar"
        Me.cmdImportar.Text = "Importar<F3>"
        Me.cmdImportar.Tooltip = "Importar"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Procesar<F4>"
        Me.cmdiLimpiar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'ControlContainerItem3
        '
        Me.ControlContainerItem3.AllowItemResize = False
        Me.ControlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem3.Name = "ControlContainerItem3"
        '
        'txtMoneda
        '
        Me.txtMoneda.Location = New System.Drawing.Point(662, 24)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.Size = New System.Drawing.Size(112, 20)
        Me.txtMoneda.TabIndex = 25
        Me.txtMoneda.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(609, 24)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(52, 13)
        Me.Label19.TabIndex = 24
        Me.Label19.Text = "Moneda"
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 140
        '
        'Descripcion
        '
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 280
        '
        'Cantidad
        '
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        Me.Cantidad.Width = 65
        '
        'Precio
        '
        '
        '
        '
        Me.Precio.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Precio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Increment = 1.0R
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Increment = 1.0R
        Me.Valor.Name = "Valor"
        Me.Valor.ReadOnly = True
        Me.Valor.Width = 120
        '
        'IVA
        '
        Me.IVA.HeaderText = "IVA"
        Me.IVA.Name = "IVA"
        Me.IVA.ReadOnly = True
        Me.IVA.Visible = False
        '
        'frmFactImportar
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(791, 529)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmFactImportar"
        Me.Text = "frmFactImportar"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String
    Dim strNombAlmacenar As String
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmFacturas"

    Private Sub frmFactImportar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0

        Try
            txtNombreCliente.ReadOnly = True
            txtDiasCredito.ReadOnly = True
            txtSubTotalFactContado.ReadOnly = True
            txtImpuestoFactContado.ReadOnly = True
            txtRetencionFactContado.ReadOnly = True
            txtTotalFactContado.ReadOnly = True
            txtCantidadFactContado.ReadOnly = True
            txtSubTotalFactCredito.ReadOnly = True
            txtImpuestoFactCredito.ReadOnly = True
            txtRetencionFactCredito.ReadOnly = True
            txtMoneda.ReadOnly = True
            txtTotalFactCredito.ReadOnly = True
            txtCantidadFactCredito.ReadOnly = True
            txtSubtotal.ReadOnly = True
            txtImpuesto.ReadOnly = True
            txtRentencion.ReadOnly = True
            txtTotal.ReadOnly = True
            txtCliente.Text = ""
            txtNombreCliente.Text = ""
            txtDiasCredito.Text = ""
            txtSubTotalFactContado.Text = ""
            txtImpuestoFactContado.Text = ""
            txtRetencionFactContado.Text = ""
            txtTotalFactContado.Text = ""
            txtCantidadFactContado.Text = ""
            txtSubTotalFactCredito.Text = ""
            txtImpuestoFactCredito.Text = ""
            txtRetencionFactCredito.Text = ""
            txtTotalFactCredito.Text = ""
            txtCantidadFactCredito.Text = ""
            txtSubtotal.Text = ""
            txtImpuesto.Text = ""
            txtRentencion.Text = ""
            txtTotal.Text = ""

            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "truncate table tmp_facturasimp"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()

            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            ValidarDatosImportados()
            CargarResumen()
            CargarFacturas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub frmFactImportar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                ObtenerArchivo()
            ElseIf e.KeyCode = Keys.F4 Then
                ProcesarArchivo()
            ElseIf e.KeyCode = Keys.F5 Then
                intListadoAyuda = 2
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case bHerramientas.Buttons.IndexOf(e.Button)
        '    Case 0 : ObtenerArchivo()
        '    Case 1 : ProcesarArchivo()
        '    Case 2 : Me.Close()
        'End Select

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click

        Select Case sender.name.ToString
            Case "Importar" : ObtenerArchivo()
            Case "Procesar"
            Case "Salir" : Me.Close()
        End Select

    End Sub

    Sub LimpiaDatosFactura()

        Try
            TextBox1.Text = ""
            txtCliente.Text = ""
            txtNombreCliente.Text = ""
            txtDiasCredito.Text = ""
            txtSubtotal.Text = ""
            txtImpuesto.Text = ""
            txtRentencion.Text = ""
            txtTotal.Text = ""
            txtMoneda.Text = ""
            txtImpuestoFactContado.Text = ""
            txtRetencionFactContado.Text = ""
            txtTotalFactContado.Text = ""
            txtCantidadFactContado.Text = ""

            txtSubTotalFactCredito.Text = ""
            txtImpuestoFactCredito.Text = ""
            txtRetencionFactCredito.Text = ""
            txtTotalFactCredito.Text = ""
            txtCantidadFactCredito.Text = ""
            'MSFlexGrid2.Clear()
            'MSFlexGrid2.FormatString = "<C�digo      |<Descripci�n                                                       |>Cantidad      |>Precio       |>Valor         |>IVA       |"
            'MSFlexGrid2.Rows = 1
            dgvDetalle.Rows.Clear()
            CargarFacturas()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub


    Sub Limpiar()

        Try
            TextBox1.Text = ""
            txtCliente.Text = ""
            txtMoneda.Text = ""
            txtNombreCliente.Text = ""
            txtDiasCredito.Text = ""
            txtSubtotal.Text = ""
            txtImpuesto.Text = ""
            txtRentencion.Text = ""
            txtTotal.Text = ""
            'MSFlexGrid2.Clear()
            'MSFlexGrid2.FormatString = "<C�digo      |<Descripci�n                                                       |>Cantidad      |>Precio       |>Valor         |>IVA       |"
            'MSFlexGrid2.Rows = 1
            dgvDetalle.Rows.Clear()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub ObtenerArchivo()
        Dim myStream As Stream = Nothing
        Dim dlgNew As New OpenFileDialog
        Dim strFile As String = String.Empty
        Dim strLine As String = String.Empty
        Dim strDrive As String = String.Empty

        Dim lngRegistro As Long = 0
        Dim strFacturas As String = String.Empty
        Dim intFechaIng As Integer = 0
        Dim strFechaIng As String = String.Empty
        Dim strVendCodigo As String = String.Empty
        Dim strCliCodigo As String = String.Empty
        Dim strCliNombre As String = String.Empty
        Dim dblSubTotalCosto As Double = 0
        Dim dblSubTotal As Double = 0
        Dim dblImpuesto As Double = 0
        Dim dblRetencion As Double = 0
        Dim dblTotal As Double = 0
        Dim strUsrCodigo As String = String.Empty
        Dim bytAgenCodigo As Byte
        Dim bytStatus As Byte
        Dim bytImpreso As Byte
        Dim bytTipoFactura As Byte
        Dim intDiasCredito As Integer = 0
        Dim intNumFecAnul As Integer = 0
        Dim strProdCodigo As String = String.Empty
        Dim strProdDescrip As String = String.Empty
        Dim dblCantidad As Double = 0
        Dim dblPrecio As Double = 0
        Dim dblPrecioCosto As Double = 0
        Dim dblValor As Double = 0
        Dim dblIva As Double = 0
        Dim intNumeroArqueo As Integer = 0
        Dim intRegDetalle As Integer = 0
        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim strComando As String = String.Empty
        Dim strDireccion As String = String.Empty

        Dim existe As Integer = 0
        Dim er As Integer = 0
        Dim strQueryVal As String = ""
        Dim lsCodigoMoneda As String = ""
        Dim lnTipoCambio As Decimal = 0
        Try
            dlgNew.InitialDirectory = "C:\"
            dlgNew.Filter = "ZIP files (*.zip)|*.zip"
            dlgNew.RestoreDirectory = True
            If dlgNew.ShowDialog() <> DialogResult.OK Then
                MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importaci�n")
                Exit Sub
            End If
            strDrive = ""
            strComando = ""
            strArchivo = ""
            strArchivo2 = ""
            strClave = ""
            intIncr = 0
            intIncr = InStr(dlgNew.FileName, "vtas_")
            strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
            If intIncr = 0 Then
                MsgBox("No es un archivo que contiene las ventas de una agencia. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strArchivo = dlgNew.FileName
            strClave = "Agro2K_2008"
            intIncr = 0
            intIncr = InStrRev(dlgNew.FileName, "\")
            strDrive = ""
            strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
            strNombAlmacenar = ""
            strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
            intIncr = 0
            'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            'intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
            If Not SUFunciones.ExtraerArchivoDeArchivoZip(strArchivo) Then
                MsgBox("No se pudo obtener el archivo, favor validar", MsgBoxStyle.Critical, "Importar Facturas")
                Exit Sub
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "truncate table tmp_facturasimp"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            strNombArchivo = ""
            strNombArchivo = strArchivo2
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            intIncr = 0
            intIncr = InStrRev(strNombArchivo, "\")
            strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
            strQuery = ""
            strQuery = "select * from tbl_archivosimp where archivo = '" & strNombAlmacenar & "'"
            cmdAgro2K.CommandText = strQuery
            Try
                intError = 0
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    intError = 0  'intError = 1 Se asigna cero para no validar el nombre del archivo de requisas que ya existe
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            If intError = 1 Then
                MsgBox("Archivo ya fue procesado.  Verifique.", MsgBoxStyle.Critical, "Error de Archivo")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim sr As StreamReader = File.OpenText(strArchivo2)
            intIncr = 0
            intError = 0
            intRegDetalle = 0

            'strQuery = ""
            'strQuery = "truncate table tmp_facturasimp"
            'cmdAgro2K.CommandText = strQuery
            'cmdAgro2K.ExecuteNonQuery()
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()

            er = 1  ' con uno indica que las requisa del archivo que se importa ya existen, su valor cambia si al menos una requisa no existe

            Do While sr.Peek() >= 0
                strLine = sr.ReadLine()
                If strLine.Trim().Length > 0 Then


                    intIncr = InStr(1, strLine, "|")
                    lngRegistro = 0
                    'lngRegistro = CLng(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    lngRegistro = SUConversiones.ConvierteALong(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strFacturas = ""
                    strFacturas = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    intFechaIng = 0
                    'intFechaIng = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    intFechaIng = SUConversiones.ConvierteAInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strFechaIng = ""
                    strFechaIng = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strVendCodigo = ""
                    strVendCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strCliCodigo = ""
                    strCliCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strCliNombre = ""
                    strCliNombre = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblSubTotalCosto = 0
                    'dblSubTotalCosto = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    dblSubTotalCosto = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblSubTotal = 0
                    'dblSubTotal = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    dblSubTotal = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblImpuesto = 0
                    'dblImpuesto = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    dblImpuesto = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblRetencion = 0
                    'dblRetencion = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    dblRetencion = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblTotal = 0
                    'dblTotal = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    dblTotal = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strUsrCodigo = ""
                    strUsrCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    bytAgenCodigo = 0
                    bytAgenCodigo = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    bytStatus = 0
                    bytStatus = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    bytImpreso = 0
                    bytImpreso = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    bytTipoFactura = 0
                    bytTipoFactura = CByte(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    intDiasCredito = 0
                    'intDiasCredito = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    intDiasCredito = SUConversiones.ConvierteAInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    intNumFecAnul = 0
                    'intNumFecAnul = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    intNumFecAnul = SUConversiones.ConvierteAInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strProdCodigo = ""
                    strProdCodigo = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    strProdDescrip = ""
                    strProdDescrip = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblCantidad = 0
                    dblCantidad = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    'dblCantidad = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblPrecio = 0
                    'dblPrecio = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    dblPrecio = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                    strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    intIncr = InStr(1, strLine, "|")
                    dblPrecioCosto = 0
                    If (intIncr > 0) Then
                        'dblPrecioCosto = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        dblPrecioCosto = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    Else
                        If strLine.Trim().Length > 0 Then
                            'dblPrecioCosto = SUConversiones.ConvierteADecimal(strLine.Trim())
                            dblPrecioCosto = SUConversiones.ConvierteADouble(strLine.Trim())
                            strLine = ""
                        End If
                    End If
                    intIncr = InStr(1, strLine, "|")
                    dblValor = 0
                    If (intIncr > 0) Then
                        'dblValor = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        dblValor = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    End If
                    intIncr = InStr(1, strLine, "|")
                    dblIva = 0
                    If (strLine.Trim().Length > 0) Then
                        'dblIva = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        dblIva = SUConversiones.ConvierteADouble(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    End If
                    intIncr = InStr(1, strLine, "|")
                    intNumeroArqueo = 0

                    If (strLine.Trim().Length > 0) Then
                        'intNumeroArqueo = CInt(strLine)
                        'intNumeroArqueo = CInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        intNumeroArqueo = SUConversiones.ConvierteAInt(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    End If
                    intIncr = InStr(1, strLine, "|")
                    If (strLine.Trim().Length > 0) Then
                        'strDireccion = strLine
                        strDireccion = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    End If
                    intIncr = InStr(1, strLine, "|")
                    lsCodigoMoneda = String.Empty
                    If (strLine.Trim().Length > 0) Then
                        'strDireccion = strLine
                        lsCodigoMoneda = Microsoft.VisualBasic.Left(strLine, intIncr - 1)
                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    End If
                    intIncr = InStr(1, strLine, "|")
                    lnTipoCambio = 0
                    If (strLine.Trim().Length > 0) Then
                        'strDireccion = strLine
                        If intIncr > 0 Then
                            lnTipoCambio = SUConversiones.ConvierteADecimal(Microsoft.VisualBasic.Left(strLine, intIncr - 1))
                        Else
                            lnTipoCambio = SUConversiones.ConvierteADecimal(strLine)
                        End If

                        strLine = Microsoft.VisualBasic.Right(strLine, Len(strLine) - intIncr)
                    End If
                    intIncr = InStr(1, strLine, "|")

                    intRegDetalle = intRegDetalle + 1
                    If bytStatus = 1 Then
                        bytImpreso = 0
                    End If

                    strQueryVal = "exec spValidaVentasImportar '" & strFacturas & "', " & bytAgenCodigo

                    If cnnAgro2K.State = ConnectionState.Open Then
                        cnnAgro2K.Close()
                    End If

                    cnnAgro2K.Open()
                    Try
                        cmdAgro2K.CommandText = strQueryVal
                        dtrAgro2K = cmdAgro2K.ExecuteReader()
                        While dtrAgro2K.Read
                            existe = dtrAgro2K.GetValue(0)
                        End While
                        dtrAgro2K.Close()

                    Catch exc As Exception
                        intError = 1
                        MsgBox(exc.Message.ToString)
                        Me.Cursor = Cursors.Default
                        Exit Do
                    End Try

                    If existe = 0 Then  'Cero indica que la requisa no existe en el sitema y por tanto se ingresa, 1 indica que existe y no se ingresa
                        er = existe
                        strQuery = ""
                        strQuery = "insert into tmp_facturasimp values (" & lngRegistro & ", '" & strFacturas & "', "
                        strQuery = strQuery + "" & intFechaIng & ", '" & strFechaIng & "', '" & strVendCodigo & "', "
                        strQuery = strQuery + "'" & strCliCodigo & "', '" & strCliNombre & "', " & dblSubTotalCosto & ", " & dblSubTotal & ", "
                        strQuery = strQuery + "" & dblImpuesto & ", " & dblRetencion & ", " & dblTotal & ", "
                        strQuery = strQuery + "'" & strUsrCodigo & "', " & bytAgenCodigo & ", " & bytStatus & ", "
                        strQuery = strQuery + "" & bytImpreso & ", " & bytTipoFactura & ", " & intDiasCredito & ", "
                        strQuery = strQuery + "" & intNumFecAnul & "," & intNumeroArqueo & ", '" & strProdCodigo & "', "
                        strQuery = strQuery + "'" & strProdDescrip & "', " & dblCantidad & ", " & dblPrecio & ", " & dblPrecioCosto & ", "
                        strQuery = strQuery + "" & dblValor & "," & dblIva & ",'" & strDireccion & "', " & intRegDetalle & ",'Sin Error','" & lsCodigoMoneda & "'," & lnTipoCambio & ")"
                        Try
                            cmdAgro2K.CommandText = strQuery
                            cmdAgro2K.ExecuteNonQuery()
                        Catch exc As Exception
                            intError = 1
                            MsgBox(exc.Message.ToString)
                            Me.Cursor = Cursors.Default
                            Exit Do
                        End Try
                    End If
                End If
            Loop
            sr.Close()

            If er = 0 Then 'Condicionamos para indicarle al husuario si ya ha ingresado las Facturas de compras
                If intError = 0 Then
                    ValidarDatosImportados()
                    CargarResumen()
                    CargarFacturas()
                    MsgBox("Finalizado satisfactoriamente la importaci�n de las ventas.", MsgBoxStyle.Information, "Proceso Finalizado")
                Else
                    MsgBox("Hubieron errores al importar el archivo de ventas.", MsgBoxStyle.Critical, "Error en la Importaci�n")
                End If
            Else
                MsgBox("El archivo de Facturas de Ventas ya fue Importado.", MsgBoxStyle.Critical, "Error en la Importaci�n")
            End If

            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If File.Exists(strArchivo2) = True Then
                File.Delete(strArchivo2)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub ValidarDatosImportados()

        Dim cmdTmp As New SqlCommand("sp_PrcFacturasImp", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter

        Try
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(0) = 0 Then
                        MsgBox("No hubieron errores en la validaci�n de las ventas", MsgBoxStyle.Information, "Validaci�n de Datos")
                    ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                        MsgBox("Hubieron errores en la validaci�n de las ventas", MsgBoxStyle.Critical, "Validaci�n de Datos")
                    End If
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub CargarResumen()

        Try
            Dim cmdTmp As New SqlCommand("sp_PrcFacturasImp", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 1
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                        txtSubTotalFactContado.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(1)), "##,##0.#0")
                        txtImpuestoFactContado.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(2)), "##,##0.#0")
                        txtRetencionFactContado.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(3)), "##,##0.#0")
                        txtTotalFactContado.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(4)), "##,##0.#0")
                        txtCantidadFactContado.Text = dtrAgro2K.GetValue(5)
                    End If
                End While
                SUFunciones.CierraConexioDR(dtrAgro2K)
                'dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try

            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            cmdTmp.Parameters(0).Value = 2
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                        txtSubTotalFactCredito.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(0)), "##,##0.#0")
                        txtImpuestoFactCredito.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(1)), "##,##0.#0")
                        txtRetencionFactCredito.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(2)), "##,##0.#0")
                        txtTotalFactCredito.Text = Format(SUConversiones.ConvierteADecimal(dtrAgro2K.GetValue(3)), "##,##0.#0")
                        txtCantidadFactCredito.Text = dtrAgro2K.GetValue(4)
                    End If
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            Finally
                'dtrAgro2K.Close()
                SUFunciones.CierraConexioDR(dtrAgro2K)
                cmdTmp.Connection.Close()
                cnnAgro2K.Close()
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Sub CargarFacturas()

        Try
            ComboBox1.Items.Clear()
            ComboBox2.Items.Clear()
            strQuery = ""
            strQuery = "select distinct registro, numero, tipofactura from tmp_facturasimp where status = 0 order by tipofactura, numero"
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ComboBox1.Items.Add(dtrAgro2K.GetValue(1))
                    ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            Limpiar()
            ComboBox2.SelectedIndex = ComboBox1.SelectedIndex
            TextBox1.Text = "Sin Error"
            strQuery = ""
            strQuery = "select * from tmp_facturasimp where registro = " & CLng(ComboBox2.SelectedItem) & ""
            Try
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    Label18.Text = dtrAgro2K.GetValue(3)
                    txtCliente.Text = dtrAgro2K.GetValue(5)
                    txtNombreCliente.Text = dtrAgro2K.GetValue(6)
                    txtMoneda.Text = dtrAgro2K.Item("CodigoMoneda")
                    txtDiasCredito.Text = dtrAgro2K.GetValue(17)
                    txtSubtotal.Text = Format(dtrAgro2K.GetValue(8), "##,##0.#0")
                    txtImpuesto.Text = Format(dtrAgro2K.GetValue(9), "##,##0.#0")
                    txtRentencion.Text = Format(dtrAgro2K.GetValue(10), "##,##0.#0")
                    txtTotal.Text = Format(dtrAgro2K.GetValue(11), "##,##0.#0")

                    dgvDetalle.Rows.Add(dtrAgro2K.GetValue(20), dtrAgro2K.GetValue(21), Format(dtrAgro2K.GetValue(22), "##,##0.#0"),
                                    Format(dtrAgro2K.GetValue(23), "##,##0.#0"), Format(dtrAgro2K.GetValue(25), "##,##0.#0"),
                                    Format(dtrAgro2K.GetValue(26), "##,##0.#0"), SUConversiones.ConvierteAInt(dtrAgro2K.GetValue(28)),
                                    Format(dtrAgro2K.GetValue(24), "##,##0.#0"))

                    If dtrAgro2K.GetValue(29) <> "Sin Error" Then
                        If Len(TextBox1.Text) < Len(dtrAgro2K.GetValue(29)) Then
                            TextBox1.Text = dtrAgro2K.GetValue(29)
                        End If
                    End If
                End While
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Function UbicarProducto(ByVal prm01 As String) As Boolean

        Dim intEnc As Integer

        Try
            intEnc = 0
            strQuery = ""
            strQuery = "Select Codigo, Descripcion, Pvpc, Impuesto from prm_Productos Where Codigo = '" & prm01 & "'"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                'MSFlexGrid2.Col = 0
                'MSFlexGrid2.Text = dtrAgro2K.GetValue(0)
                'MSFlexGrid2.Col = 1
                'MSFlexGrid2.Text = dtrAgro2K.GetValue(1)
                'MSFlexGrid2.Col = 4
                'MSFlexGrid2.Text = dtrAgro2K.GetValue(2)
                'bytImpuesto = dtrAgro2K.GetValue(3)
                'intEnc = 1
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If intEnc = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Function

    Sub ProcesarArchivo()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            Dim cmdTmp As New SqlCommand("sp_PrcFacturasImp", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim bytContinuar As Byte

            bytContinuar = 0
            With prmTmp01
                .ParameterName = "@intAccion"
                .SqlDbType = SqlDbType.TinyInt
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmp.ExecuteReader
                While dtrAgro2K.Read
                    If dtrAgro2K.GetValue(0) = 0 Then
                        MsgBox("No hay errores en la validaci�n de las ventas. Se procedera a ingresar las ventas.", MsgBoxStyle.Information, "Validaci�n de Datos")
                    ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                        bytContinuar = 1
                        MsgBox("Continuan los errores en la validaci�n de las ventas", MsgBoxStyle.Critical, "Validaci�n de Datos")
                        Exit Sub
                    End If
                End While
                dtrAgro2K.Close()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                dtrAgro2K.Close()
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            End Try
            If bytContinuar = 0 Then
                cmdTmp.CommandTimeout = 300
                cmdTmp.Parameters(0).Value = 3
                Try
                    cmdTmp.ExecuteNonQuery()
                Catch exc As Exception
                    MsgBox(exc.Message.ToString)
                    dtrAgro2K.Close()
                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Exit Sub
                End Try
            End If
            strQuery = ""
            strQuery = "insert into tbl_ArchivosImp values ('" & strNombAlmacenar & "', "
            strQuery = strQuery + "'" & Format(Now, "dd-MMM-yyyy") & "', " & Format(Now, "yyyyMMdd") & ", "
            strQuery = strQuery + "" & lngRegUsuario & ")"
            cmdAgro2K.CommandText = strQuery
            Try
                cmdAgro2K.ExecuteNonQuery()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
            End Try
            strQuery = ""
            strQuery = "truncate table tmp_facturasimp"
            cmdAgro2K.CommandText = strQuery
            cmdAgro2K.ExecuteNonQuery()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            MsgBox("Finalizo el proceso de importar ventar de una agencia a la Casa Matriz.", MsgBoxStyle.Exclamation, "Proceso Finalizado")

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Close()

    End Sub

    Function UbicarCliente(ByVal strDato As String) As Boolean

        Try
            strDirecFact = ""
            txtCantidadFactCredito.Text = "0"
            strQuery = ""
            strQuery = "Select C.nombre from prm_Clientes C Where C.Codigo = '" & txtCliente.Text & "' AND C.Estado = 0"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            txtNombreCliente.Text = ""
            intError = 1
            While dtrAgro2K.Read
                intError = 0
                txtNombreCliente.Text = dtrAgro2K.GetValue(0)
            End While
            If intError = 1 Then
                txtCliente.Text = ""
                txtNombreCliente.Text = ""
                MsgBox("C�digo de cliente no est� registrado.  Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                Exit Function
            End If
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If txtCantidadFactCredito.Text = "0" Then
                UbicarCliente = False
                Exit Function
            End If
            UbicarCliente = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            If blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text) : txtNombreCliente.Focus()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub TextBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCliente.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                txtNombreCliente.Focus()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub TextBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.LostFocus
        Try
            UbicarCliente(txtCliente.Text)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Private Sub cmdImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportar.Click
        Try
            ObtenerArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            ProcesarArchivo()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnEliminarRegistro.Click
        Dim lsNumeroFactura As String = String.Empty
        Try
            lsNumeroFactura = ComboBox1.SelectedItem
            If ComboBox1.Items.Count > 0 Then
                RNFacturas.EliminarFacturaAImportar(lsNumeroFactura)
                LimpiaDatosFactura()
                CargarResumen()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub GroupBox3_Enter(sender As Object, e As EventArgs) Handles GroupBox3.Enter

    End Sub
End Class
