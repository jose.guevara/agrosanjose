Imports System
Imports System.Data
Imports System.Data.SqlClient
'Imports MegasysJack.consultas
Imports System.Text
Imports System.ComponentModel
Imports Microsoft.VisualBasic
Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.DotNetBar.Rendering
Imports CrystalDecisions.CrystalReports
' Imports Microsoft.Reporting
Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Windows.Forms.ReportDocuments
Imports CrystalDecisions.ReportSource
Imports System.Globalization
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports System.Collections.Generic

Public Class frmAsientosContables
    Public Shared ReadOnly Property CurrentUICulture As CultureInfo
        Get
            Dim value As CultureInfo

            value = CultureInfo.CurrentUICulture

        End Get
    End Property
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "frmAsientosContables"
    'Usage


    Dim jack As New megajack
    Friend jack_simf As New SIMF_CONTA
    'Dim jackconsulta As New MegasysJack.consultas
    'Dim sender As Object
    'Dim source As ICommandSource = TryCast(sender, ICommandSource)
    Public data_filtro_reporte As New DataSet
    Dim dsCatClientes As New DataSet()
    'Public style As eStyle = CType(System.Enum.Parse(GetType(eStyle), source.CommandParameter.ToString()), eStyle)
    'eStyle.Office2007VistaGlass
    Friend WithEvents balloonTipFocus As DevComponents.DotNetBar.BalloonTip

    Dim jackvalida As New Valida
    'Dim jackmensaje As New MegasysJack.Mensaje
    Dim jackAsientos As New Asientos

    '  Dim meee As New JyOSISE.ConsultasDB

    Friend jackconsulta As New ConsultasDB
    Private Shared mostrar_todo_Asiento As Boolean
    Private Shared numemov_select As String = ""
    Private Shared nuevalinea_opcion As Boolean
    Private Shared cerrar_nuevalinea As Boolean
    Private Shared existe_asiento, eliminar_documento As Boolean
    Private Shared descc_linea As String = ""
    Private Shared debe_temp As String = ""
    Private Shared haber_temp As String = ""
    Private Shared fechadoc_mantiene As String = ""
    'Private Sub Button_MenuPrincipal_Click(sender As System.Object, e As System.EventArgs) Handles Button_MenuPrincipal.Click
    '    Me.Hide()
    '    menu_conta.Show()
    'End Sub

    Private Sub Button_cerrar_Click(sender As System.Object, e As System.EventArgs) Handles Button_cerrar.Click
        cerrar_ventana()
    End Sub
    Sub cerrar_ventana()
        If MessageBoxEx.Show("Desea Cerrar la Ventana?", "Asientos/Comprobantes", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
            Me.Close()
            Me.Dispose()
        End If
    End Sub


    Private Sub ExpandablePanel1_Click(sender As System.Object, e As System.EventArgs)

    End Sub
#Region "Private variables"

    Private _MColorSelected As Boolean
    Private _MBaseColorScheme As eOffice2007ColorScheme = eOffice2007ColorScheme.Blue
    Private _ContactTypes As List(Of String)

#End Region

    Public Sub New()
        InitializeComponent()

        ' Initialize our X1 and X2 DataGridViews

        X1_Initialize()
        X2_Initialize()

    End Sub



#Region "Form1_Load"

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        ' Dim cultura As CultureInfo = New CultureInfoConverter("es-Ni")

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-NI")
        Dim dTOS As DataTable = Nothing
        Dim dtos_mes As DataTable = Nothing
        Dim dtos_mes_revisar As New DataTable
        Dim dtsDatos As DataTable = Nothing
        Dim dtdatos_mes As DataTable = Nothing
        Dim dtdatos_mes_revisar As DataTable = Nothing

        Try
            dTOS = New DataTable
            dtos_mes = New DataTable
            dtos_mes_revisar = New DataTable

            LabelItem_pantalla.Text = " �ltimo Cierre: " & jackconsulta.DameCampo_UltimoCierre(True, False)

            REM  cargamos autocomplete de cuenta contable

            TextB_cuenta__tipomov.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
            TextB_cuenta__tipomov.AutoCompleteMode = AutoCompleteMode.Suggest
            TextB_cuenta__tipomov.AutoCompleteSource = AutoCompleteSource.CustomSource
            DateF_Fechaasiento_Nuevo.Text = Now()
            jackconsulta.cargacombo("select descvariable,valorvariable from Macombos where variable='Mes' order by ordenvariable", True)
            jackconsulta.carga_combo_ano_DN(ComboB_ano_TabBusquedas, 15)
            dtdatos_mes = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='Mes' order by ordenvariable", dtos_mes, True)
            jackconsulta.carga_combo_DN(ComboB_mes_TabBusquedas, dtdatos_mes)
            jackconsulta.carga_combo_ano_DN(ComboB_ano_TabRevisar, 15)
            dtdatos_mes_revisar = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='Mes' order by ordenvariable", dtos_mes_revisar, True)
            jackconsulta.carga_combo_DN(ComboB_mes_TabRevisar, dtdatos_mes_revisar)

            ComboB_mes_TabBusquedas.SelectedValue = -1
            ComboB_mes_TabRevisar.SelectedValue = Now.Month
            ComboB_ano_TabRevisar.Text = Now.Year
            ComboBoxItem_SelectReport.SelectedIndex = 0
            TextF_numecompro.Focus()
            GridP_Asientos.Hide()
            GridP_Asientos.DataSource = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Form1_Load - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub actualiza_lineas_dasientos()
        Dim sql_datos As SqlDataReader = Nothing
        Dim asiento As String = String.Empty
        Dim anomes As String = String.Empty
        Dim sql_datos_linea As SqlDataReader = Nothing
        Dim lineas As Integer = 0
        Dim linea As Integer = 0
        Dim debe As Double = 0
        Dim haber As Double = 0
        Dim desccuenta As String = String.Empty
        Try
            sql_datos = jackconsulta.RetornaReader("select numeasiento,anomes from dasientos group by numeasiento,anomes", True)
            If sql_datos.HasRows Then
                While sql_datos.Read
                    asiento = sql_datos!numeasiento
                    anomes = sql_datos!anomes
                    sql_datos_linea = jackconsulta.RetornaReader("select * from dasientos where numeasiento='" & asiento & "' and anomes='" & anomes & "' and  nlinea is null", True)

                    If sql_datos_linea.HasRows Then
                        lineas = 0
                        lineas = jackconsulta.EjecutaSQL("select max(nlinea) from dasientos where numeasiento='" & asiento & "' and anomes='" & anomes & "'", True)
                        linea = 0
                        debe = 0
                        haber = 0
                        desccuenta = String.Empty
                        While sql_datos_linea.Read
                            debe = 0
                            haber = 0
                            desccuenta = String.Empty
                            debe = sql_datos_linea!debelocal
                            haber = sql_datos_linea!haberlocal
                            desccuenta = sql_datos_linea!desccuenta
                            lineas = lineas + 1
                            jackconsulta.EjecutaSQL("update dasientos set nlinea=" & lineas & " where numeasiento='" & asiento & "' and anomes='" & anomes & "' and debelocal=" & debe & " and haberlocal=" & haber & " and desccuenta='" & desccuenta & "'", True)
                        End While
                    End If
                    sql_datos_linea.Close()
                End While
            End If
            sql_datos.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("actualiza_lineas_dasientos - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
#Region "LoadOrderData"

    ''' <summary>
    ''' Loads default Order data into the
    ''' customersDataSet.Orders data table
    ''' </summary>
    Private Sub LoadOrderData()
        Dim r As New Random()
        Dim [date] As DateTime = DateTime.Now
        Dim sb As New StringBuilder()

        ' Loop through the customersDataSet.Customers data table and
        ' add an Order into the customersDataSet.Orders for each one

        'For i As Integer = 0 To customersDataSet.Customers.Count - 1
        '    CustomersDataSet.Orders.AddOrdersRow(CustomersDataSet.Customers(i).CustomerID, _
        '        r.[Next](1000, 9999).ToString(), [date].AddDays(-r.[Next](2, 90)), _
        '        GetNewPart(sb, r), CDec((r.NextDouble() * 100)), r.[Next](1, 100), _
        '        r.[Next](1, 100), Math.Max(0, Math.Min(r.[Next](-20, 140), 100)), GetNewFeedback(r))
        'Next
    End Sub

#Region "GetNewPart"

    ''' <summary>
    ''' Gets a new Part number for the order that
    ''' conforms to the "DD-AAA-DDDDA" pattern
    ''' </summary>
    ''' <param name="sb"></param>
    ''' <param name="r"></param>
    ''' <returns></returns>
    Private Function GetNewPart(ByVal sb As StringBuilder, ByVal r As Random) As String
        sb.Length = 0

        sb.Append(r.[Next](10, 99))
        sb.Append(Microsoft.VisualBasic.Chr(r.[Next](&H41, &H5B)))
        sb.Append(Chr(r.[Next](&H41, &H5B)))
        sb.Append(Chr(r.[Next](&H41, &H5B)))
        sb.Append(r.[Next](1000, 9999))

        If (r.[Next](0, 2) = 0) Then
            sb.Append("N")
        Else
            sb.Append("R")
        End If

        Return (sb.ToString())
    End Function

#End Region

#Region "GetNewFeedback"

    ''' <summary>
    ''' Gets a new order feedback value
    ''' </summary>
    ''' <param name="r"></param>
    ''' <returns></returns>
    Private Function GetNewFeedback(ByVal r As Random) As String
        Dim fb As Integer = r.[Next](0, 4)

        Select Case fb
            Case 0
                Return "U"
            Case 1
                Return "N"
            Case Else
                Return "Y"
        End Select

    End Function

#End Region

#End Region

#End Region
#Region "X1_Initialize"

    ''' <summary>
    ''' Initialize our Data~GridViewX1 sample view
    ''' </summary>
    Private Sub X1_Initialize()
        X1Contact_Initialize()
        X1Country_Initialize()
        X1Region_Initialize()
        X1PostalCode_Initialize()

        ' Hook onto the CellContentClick so we can
        ' demonstrate one way to process cell clicks

        AddHandler GridView_dasientos.CellContentClick, AddressOf DataGridViewX1_CellContentClick
    End Sub

#End Region
#Region "X1Contact_Initialize"

    ''' <summary>
    ''' Initializes out X1 Contact environment
    ''' </summary>
    Private Sub X1Contact_Initialize()
        Dim bcx As DataGridViewButtonXColumn = TryCast(GridView_dasientos.Columns("Contact"), DataGridViewButtonXColumn)

        If bcx IsNot Nothing Then
            ' Allocate our running list of
            ' selected contact types

            _ContactTypes = New List(Of String)()
            _ContactTypes.Add("Owner")

            ' We want to be able to specify our own button text
            ' instead of using the bound data value for the text

            bcx.UseColumnTextForButtonValue = False

            ' Hook onto the following events so we can
            ' demonstrate cell customization and click processing

            AddHandler bcx.BeforeCellPaint, AddressOf X1Contact_BeforeCellPaint
            AddHandler bcx.Click, AddressOf X1Contact_ButtonClick
        End If
    End Sub

#Region "X1Contact_BeforeCellPaint"

    ''' <summary>
    ''' Handles "Contact" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewButtonXColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X1Contact_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim bcx As DataGridViewButtonXColumn = TryCast(sender, DataGridViewButtonXColumn)

        If bcx IsNot Nothing Then
            ' If the cell text is in our _ContactTypes list, then
            ' give our button a default background - otherwise not.

            If (_ContactTypes.Contains(bcx.Text)) Then
                bcx.ColorTable = eButtonColor.OrangeWithBackground
            Else
                bcx.ColorTable = eButtonColor.Orange
            End If
        End If

    End Sub

#End Region

#Region "X1Contact_ButtonClick"

    ''' <summary>
    ''' Handles our "Contact" cell button clicks.
    ''' 
    ''' <remarks>
    ''' Clicking on a "Contact" cell will result in the addition or removal of the
    ''' cell text from our _ContactTypes list.  The _ContactTypes list is then used
    ''' in the BeforeContactCellPaint routine to format the cell display.
    ''' </remarks>
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X1Contact_ButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim cell As DataGridViewButtonXCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewButtonXCell)

        If cell IsNot Nothing Then
            Dim s As String = Convert.ToString(cell.Value)

            ' If the text was already in our list, then remove it.
            ' If it wasn't, then add it.

            If _ContactTypes.Contains(s) Then
                _ContactTypes.Remove(s)
            Else
                _ContactTypes.Add(s)
            End If

            ' Update the entire column so that each column cell
            ' will be reformatted according to the new list values.

            GridView_dasientos.InvalidateColumn(cell.ColumnIndex)
        End If
    End Sub

#End Region

#End Region

#Region "X1Country_Initialize"

    ''' <summary>
    ''' Initializes our X1 Country environment
    ''' </summary>
    Private Sub X1Country_Initialize()
        Dim bcx As DataGridViewButtonXColumn = TryCast(GridView_dasientos.Columns("Country"), DataGridViewButtonXColumn)

        If bcx IsNot Nothing Then
            ' We want to be able to specify our own button text
            ' instead of using the bound data value for the text

            bcx.UseColumnTextForButtonValue = False

            ' Hook onto the BeforeCellPaint event so we can
            ' demonstrate cell customization

            AddHandler bcx.BeforeCellPaint, AddressOf X1Country_BeforeCellPaint
        End If
    End Sub

#Region "X1Country_BeforeCellPaint"

    ''' <summary>
    ''' Handles our "Country" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewButtonXColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X1Country_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim bcx As DataGridViewButtonXColumn = TryCast(sender, DataGridViewButtonXColumn)

        ' Set the button flag image to correspond to country

        'If bcx IsNot Nothing Then
        '    bcx.Image = imageList1.Images(bcx.Text)
        'End If
    End Sub

#End Region

#End Region

#Region "X1Region_Initialize"

    ''' <summary>
    ''' Initializes our X1 Region environment
    ''' </summary>
    Private Sub X1Region_Initialize()
        Dim bcx As DataGridViewButtonXColumn = TryCast(GridView_dasientos.Columns("RegionC"), DataGridViewButtonXColumn)

        If bcx IsNot Nothing Then
            ' Create and add a couple of levels
            ' of SubItems to our cell button

            X1Region_AddSubItems(bcx)

            ' We want to be able to specify our own button text
            ' instead of using the bound data value for the text

            bcx.UseColumnTextForButtonValue = False

            ' Hook onto the BeforeCellPaint event so we can
            ' demonstrate cell customization

            AddHandler bcx.BeforeCellPaint, AddressOf X1Region_BeforeCellPaint
        End If
    End Sub

#Region "X1Region_AddSubItems"

    ''' <summary>
    ''' Creates and adds a couple of SubItem buttons
    ''' to the Region cell button
    ''' </summary>
    ''' <param name="bcx"></param>
    Private Sub X1Region_AddSubItems(ByVal bcx As DataGridViewButtonXColumn)
        Dim bi As New ButtonItem()
        bi.Text = "Display Region Map"
        AddHandler bi.Click, AddressOf X1Region_DefaultButtonClick

        bcx.SubItems.Add(bi)

        bi = New ButtonItem()
        bi.Text = "Print..."
        ' bi.Image = imageList1.Images("Print")

        bcx.SubItems.Add(bi)

        Dim sbi As New ButtonItem()
        sbi.Text = "Region Contacts"
        'sbi.Image = imageList1.Images("User")
        AddHandler sbi.Click, AddressOf X1Region_DefaultButtonClick

        bi.SubItems.Add(sbi)

        sbi = New ButtonItem()
        sbi.Text = "Region Statistics"
        AddHandler sbi.Click, AddressOf X1Region_DefaultButtonClick

        bi.SubItems.Add(sbi)
    End Sub

#Region "X1Region_DefaultButtonClick"

    ''' <summary>
    ''' Handles X2 "Region" SubItem button clicks
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X1Region_DefaultButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim bi As ButtonItem = TryCast(sender, ButtonItem)

        If bi IsNot Nothing Then
            MessageBox.Show(bi.Text)
        End If
    End Sub

#End Region

#End Region

#Region "X1Region_BeforeCellPaint"

    ''' <summary>
    ''' Handles "Region" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewButtonXColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X1Region_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim bcx As DataGridViewButtonXColumn = TryCast(sender, DataGridViewButtonXColumn)

        If bcx IsNot Nothing Then
            ' If button text is null or empty, then set the text and
            ' ColorTable to reflect that fact

            If String.IsNullOrEmpty(bcx.Text) = True Then
                bcx.Text = "<font color=""FireBrick"">(Unknown)</font>"
                bcx.ColorTable = eButtonColor.Magenta
            Else
                bcx.ColorTable = eButtonColor.OrangeWithBackground
            End If
        End If
    End Sub

#End Region

#End Region

#Region "X1PostalCode_Initialize"

    ''' <summary>
    ''' Initializes our X1 PostalCode environment
    ''' </summary>
    Private Sub X1PostalCode_Initialize()
        Dim bcx As DataGridViewLabelXColumn = TryCast(GridView_dasientos.Columns("PostalCode"), DataGridViewLabelXColumn)

        If bcx IsNot Nothing Then
            ' Hook onto the BeforeCellPaint event so we can
            ' demonstrate cell customization

            AddHandler bcx.BeforeCellPaint, AddressOf X1PostalCode_BeforeCellPaint
        End If
    End Sub

#Region "X1PostalCode_BeforeCellPaint"

    ''' <summary>
    ''' Handles "PostalCode" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewButtonXColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X1PostalCode_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim bcx As DataGridViewLabelXColumn = TryCast(sender, DataGridViewLabelXColumn)

        If bcx IsNot Nothing Then
            ' Set the label image

            'If String.IsNullOrEmpty(bcx.Text) = True Then
            '    bcx.Image = imageList1.Images("SecHigh")
            '    bcx.Text = "<font color=""red"">Postal Code not Specified</font>"
            'ElseIf bcx.Text.Contains(" ") Then
            '    bcx.Image = imageList1.Images("SecMedium")
            'Else

            '    bcx.Image = imageList1.Images("SecLow")
            'End If
        End If
    End Sub

#End Region

#End Region
#Region "DataGridViewX1_CellContentClick"

    Private Sub DataGridViewX1_CellContentClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs)
        Dim cell As DataGridViewButtonXCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewButtonXCell)

        If cell IsNot Nothing Then
            Dim bc As DataGridViewButtonXColumn = TryCast(GridView_dasientos.Columns(e.ColumnIndex), DataGridViewButtonXColumn)

            If bc IsNot Nothing Then
                Dim s As String = Convert.ToString(cell.Value)

                Select Case bc.Name
                    Case "Country"
                        MessageBox.Show("What a great country " & s & " is!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Select

                    Case "RegionC"
                        If (String.IsNullOrEmpty(s)) Then
                            cell.Value = "Global"
                        Else
                            cell.Value = ""
                        End If
                        Exit Select
                End Select
            End If
        End If
    End Sub

#End Region
#Region "GridView_dasientos support"

#Region "X2_Initialize"

    ''' <summary>
    ''' Initialize out GridView_dasientos sample view.
    ''' </summary>
    Private Sub X2_Initialize()
        X2Order_Initialize()
        X2Date_Initialize()
        X2Part_Initialize()
        X2Quality_Initialize()
        X2Progress_Initialize()
        X2Feedback_Initialize()
    End Sub

#End Region

#Region "X2Order_Initialize"

    ''' <summary>
    ''' Initializes our X2 "Order" environment
    ''' </summary>
    Private Sub X2Order_Initialize()
        Dim oc As DataGridViewMaskedTextBoxAdvColumn = TryCast(GridView_dasientos.Columns("OrderC"), DataGridViewMaskedTextBoxAdvColumn)

        If oc IsNot Nothing Then
            ' Hook onto the following events so we can
            ' demonstrate cell customization and click processing

            AddHandler oc.BeforeCellPaint, AddressOf X2Order_BeforeCellPaint
            AddHandler oc.ButtonClearClick, AddressOf X2Order_ButtonClearClick
            AddHandler oc.ButtonCustomClick, AddressOf X2Order_ButtonCustomClick
        End If
    End Sub

#Region "X2Order_BeforeCellPaint"

    ''' <summary>
    ''' Handles "Order" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewMaskedTextBoxAdvColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X2Order_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim oc As DataGridViewMaskedTextBoxAdvColumn = TryCast(sender, DataGridViewMaskedTextBoxAdvColumn)

        If oc IsNot Nothing Then
            Dim s As String = oc.Text.Substring(1)

            Dim value As Integer

            If Integer.TryParse(s, value) Then
                If value <= 3000 Then
                    oc.BackColor = Color.MistyRose

                ElseIf value >= 7000 Then
                    oc.BackColor = Color.PaleTurquoise
                End If
            End If
        End If
    End Sub

#End Region

#Region "X2Order_ButtonClearClick"

    ''' <summary>
    ''' Handles X2 "Order" ButtonClear Clicks
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X2Order_ButtonClearClick(ByVal sender As Object, ByVal e As CancelEventArgs)
        Dim dr As DialogResult = MessageBox.Show("Do you really want to clear this Order Number?", "Clear Order Number", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)

        If dr = DialogResult.No Then
            e.Cancel = True
        End If
    End Sub

#End Region

#Region "X2Order_ButtonCustomClick"

    ''' <summary>
    ''' Handles X2 "Order" ButtonCustom clicks
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X2Order_ButtonCustomClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim dr As DialogResult = MessageBox.Show("Assign new Order Number?", "New Order Number", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

        If dr = DialogResult.Yes Then
            Dim cell As DataGridViewMaskedTextBoxAdvCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewMaskedTextBoxAdvCell)

            If cell IsNot Nothing Then
                Dim ec As DataGridViewMaskedTextBoxAdvEditingControl = TryCast(cell.DataGridView.EditingControl, DataGridViewMaskedTextBoxAdvEditingControl)

                If ec IsNot Nothing Then
                    Dim rand As New Random()

                    ec.Text = rand.[Next](1, 9999).ToString()
                End If
            End If
        End If
    End Sub

#End Region

#End Region

#Region "X2Date_Initialize"

    ''' <summary>
    ''' Initializes our X2 "Date" environment
    ''' </summary>
    Private Sub X2Date_Initialize()
        Dim oc As DataGridViewDateTimeInputColumn = TryCast(GridView_dasientos.Columns("DateC"), DataGridViewDateTimeInputColumn)

        If oc IsNot Nothing Then
            ' Hook onto the following events so we can
            ' demonstrate cell click processing

            AddHandler oc.ButtonClearClick, AddressOf X2Date_ButtonClearClick
            AddHandler oc.ButtonCustomClick, AddressOf X2Date_ButtonCustomClick
        End If
    End Sub

#Region "X2Date_ButtonClearClick"

    ''' <summary>
    ''' Handles X2 "Date" ButtonClear Clicks
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X2Date_ButtonClearClick(ByVal sender As Object, ByVal e As CancelEventArgs)
        Dim dr As DialogResult = MessageBox.Show("Do you really want to clear this Date?", "Clear Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)

        If dr = DialogResult.No Then
            e.Cancel = True
        End If
    End Sub

#End Region

#Region "X2Date_ButtonCustomClick"

    ''' <summary>
    ''' Handles X2 "Date" ButtonCustom click events
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X2Date_ButtonCustomClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim dr As DialogResult = MessageBox.Show("Set Date to today?", "Set Date", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

        If dr = DialogResult.Yes Then
            Dim cell As DataGridViewDateTimeInputCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewDateTimeInputCell)

            If cell IsNot Nothing Then
                Dim ec As DataGridViewDateTimeInputEditingControl = TryCast(cell.DataGridView.EditingControl, DataGridViewDateTimeInputEditingControl)

                If ec IsNot Nothing Then
                    ec.Text = DateTime.Today.ToString()
                End If
            End If
        End If
    End Sub

#End Region

#End Region

#Region "X2Part_Initialize"

    ''' <summary>
    ''' Initializes our X2 "Part" environment
    ''' </summary>
    Private Sub X2Part_Initialize()
        Dim oc As DataGridViewMaskedTextBoxAdvColumn = TryCast(GridView_dasientos.Columns("PartC"), DataGridViewMaskedTextBoxAdvColumn)

        If oc IsNot Nothing Then
            ' Hook onto the following events so we can
            ' demonstrate cell customization and click processing

            AddHandler oc.BeforeCellPaint, AddressOf X2Part_BeforeCellPaint
            AddHandler oc.ButtonCustomClick, AddressOf X2Part_ButtonCustomClick
            AddHandler oc.ButtonDropDownClick, AddressOf X2Part_ButtonDropDownClick
        End If
    End Sub

#Region "X2Part_BeforeCellPaint"

    ''' <summary>
    ''' Handles X2 "Part" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewMaskedTextBoxAdvColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X2Part_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim oc As DataGridViewMaskedTextBoxAdvColumn = TryCast(sender, DataGridViewMaskedTextBoxAdvColumn)

        If oc IsNot Nothing Then
            Dim s As String = oc.Text

            If s.EndsWith("N") = True Then
                oc.ForeColor = Color.Green

            ElseIf s.EndsWith("X") = True Then
                oc.ForeColor = Color.Red
            End If
        End If
    End Sub

#End Region

#Region "X2Part_ButtonCustomClick"

    ''' <summary>
    ''' Handles X2 "Part" ButtonCustomClick events
    ''' </summary>
    ''' <param name="sender">DataGridViewMaskedTextBoxAdvCell</param>
    ''' <param name="e">EventArgs</param>
    Private Sub X2Part_ButtonCustomClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim cell As DataGridViewMaskedTextBoxAdvCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewMaskedTextBoxAdvCell)

        If cell IsNot Nothing Then
            Dim ec As DataGridViewMaskedTextBoxAdvEditingControl = TryCast(cell.DataGridView.EditingControl, DataGridViewMaskedTextBoxAdvEditingControl)

            If ec IsNot Nothing Then
                Dim s As String = ec.Text

                If s.Length > 0 Then
                    ' Changed the ending text char and display some
                    ' nonsense to the user for feedback

                    ec.Text = s.Substring(0, s.Length - 1) & "X"

                    MessageBox.Show("Inventory part " & s & " added to bug tracking database.", "Bug Track", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        End If
    End Sub

#End Region

#Region "X2Part_ButtonDropDownClick"

    ''' <summary>
    ''' Handles X2 "Part" ButtonDropDown click events
    ''' </summary>
    ''' <param name="sender">DataGridViewMaskedTextBoxAdvCell</param>
    ''' <param name="e">CancelEventArgs</param>
    Private Sub X2Part_ButtonDropDownClick(ByVal sender As Object, ByVal e As CancelEventArgs)
        Dim cell As DataGridViewMaskedTextBoxAdvCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewMaskedTextBoxAdvCell)

        If cell IsNot Nothing Then
            Dim ec As DataGridViewMaskedTextBoxAdvEditingControl = TryCast(cell.DataGridView.EditingControl, DataGridViewMaskedTextBoxAdvEditingControl)

            'If ec IsNot Nothing Then
            '    cbNewParts.Checked = ec.Text.EndsWith("N")
            '    cbRefurbishedParts.Checked = ec.Text.EndsWith("R")
            'End If
        End If
    End Sub

#End Region

#End Region

#Region "X2Quality_Initialize"

    ''' <summary>
    ''' Initializes our X2 "Quality" environment
    ''' </summary>
    Private Sub X2Quality_Initialize()
        Dim sc As DataGridViewSliderColumn = TryCast(GridView_dasientos.Columns("QualityC"), DataGridViewSliderColumn)

        ' Hook onto the BeforeCellPaint event so we can
        ' demonstrate cell customization

        If sc IsNot Nothing Then
            AddHandler sc.BeforeCellPaint, AddressOf X2Quality_BeforeCellPaint
        End If
    End Sub

#Region "X2Quality_BeforeCellPaint"

    ''' <summary>
    ''' Handles X2 "Quality" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub X2Quality_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim sc As DataGridViewSliderColumn = TryCast(sender, DataGridViewSliderColumn)

        If sc IsNot Nothing Then
            sc.Text = Convert.ToString(sc.Value)

            If (sc.Value <= 30) Then
                sc.TextColor = Color.Red
            ElseIf (sc.Value >= 90) Then
                sc.TextColor = Color.Green
            Else
                sc.TextColor = Color.Blue
            End If
        End If
    End Sub

#End Region

#End Region

#Region "X2Progress_Initialize"

    ''' <summary>
    ''' Initializes our X2 "Progress" environment
    ''' </summary>
    Private Sub X2Progress_Initialize()
        Dim pb As DataGridViewProgressBarXColumn = TryCast(GridView_dasientos.Columns("ProgressC"), DataGridViewProgressBarXColumn)

        ' Hook onto the BeforeCellPaint event so we can
        ' demonstrate cell customization

        If pb IsNot Nothing Then
            AddHandler pb.BeforeCellPaint, AddressOf X2Progress_BeforeCellPaint
        End If
    End Sub

#Region "X2Progress_BeforeCellPaint"

    ''' <summary>
    ''' Handles X2 "Progress" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewProgressBarXColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X2Progress_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim pbc As DataGridViewProgressBarXColumn = TryCast(sender, DataGridViewProgressBarXColumn)

        If pbc IsNot Nothing Then
            Select Case pbc.Value
                Case 0
                    pbc.Text = "Not Started"
                    Exit Select

                Case 100
                    pbc.Text = "Completed"
                    pbc.ColorTable = eProgressBarItemColor.Normal
                    Exit Select
                Case Else

                    pbc.Text = ""
                    pbc.ColorTable = eProgressBarItemColor.Paused
                    Exit Select
            End Select

            If pbc.Value < 30 Then
                pbc.ColorTable = eProgressBarItemColor.[Error]
            End If
        End If
    End Sub

#End Region

#End Region

#Region "X2Feedback_Initialize"

    ''' <summary>
    ''' Initializes our X2 "Feedback" environment
    ''' </summary>
    Private Sub X2Feedback_Initialize()
        Dim cb As DataGridViewCheckBoxXColumn = TryCast(GridView_dasientos.Columns("FeedbackC"), DataGridViewCheckBoxXColumn)

        ' Hook onto the BeforeCellPaint event so we can
        ' demonstrate cell customization

        If cb IsNot Nothing Then
            AddHandler cb.BeforeCellPaint, AddressOf X2Feedback_BeforeCellPaint
        End If
    End Sub

#Region "X2Feedback_BeforeCellPaint"

    ''' <summary>
    ''' Handles X2 "Feedback" BeforeCellPaint events
    ''' </summary>
    ''' <param name="sender">DataGridViewCheckBoxXColumn</param>
    ''' <param name="e">BeforeCellPaintEventArgs</param>
    Private Sub X2Feedback_BeforeCellPaint(ByVal sender As Object, ByVal e As BeforeCellPaintEventArgs)
        Dim sc As DataGridViewCheckBoxXColumn = TryCast(sender, DataGridViewCheckBoxXColumn)

        If sc IsNot Nothing Then
            Select Case sc.CheckState
                Case CheckState.Checked
                    sc.Text = "Positive"
                    sc.TextColor = Color.Green
                    Exit Select

                Case CheckState.Unchecked
                    sc.Text = "Negative"
                    sc.TextColor = Color.Red
                    Exit Select
                Case Else

                    sc.Text = "----"
                    sc.TextColor = Color.DarkGray
                    Exit Select
            End Select
        End If
    End Sub

#End Region

#Region "cbNewParts_Click"



#End Region

#Region "cbRefurbishedParts_Click"


#End Region

#Region "X2Part_UpdatePartType"

    ''' <summary>
    ''' Updates the X2 "Part" type from the users
    ''' RadioButton selection
    ''' </summary>
    ''' <param name="t">Text to replace</param>
    Private Sub X2Part_UpdatePartType(ByVal t As String)
        Dim cell As DataGridViewMaskedTextBoxAdvCell = TryCast(GridView_dasientos.CurrentCell, DataGridViewMaskedTextBoxAdvCell)

        If cell IsNot Nothing Then
            Dim ec As DataGridViewMaskedTextBoxAdvEditingControl = TryCast(cell.DataGridView.EditingControl, DataGridViewMaskedTextBoxAdvEditingControl)

            If ec IsNot Nothing Then
                Dim s As String = ec.Text

                If s.EndsWith(t) = False Then
                    ec.Text = s.Substring(0, s.Length - 1) + t
                End If
            End If
        End If
    End Sub

#End Region

#End Region

#End Region
#Region "Tab_Buscar"

    Protected Sub Buscar_Asiento()
        Dim numeasiento_detalle As String = String.Empty
        Dim anomes_detalle As String = String.Empty
        Dim numemov_detalle As String = String.Empty
        Dim sql_buscarAsiento As String = String.Empty
        Dim where As Boolean = False
        Dim datos_cantidad As DataTable = Nothing

        Try
            datos_cantidad = RNAsientos.RealizaBusquedaAsiento(TextB_cdgocuenta_buscar.Text.Trim, TextF_numecompro.Text.Trim, TextF_Documento.Text.Trim, SUConversiones.ConvierteAInt(ComboB_ano_TabBusquedas.Text), ComboB_mes_TabBusquedas.SelectedValue)

            If datos_cantidad.Rows.Count > 0 Then
                GridP_Asientos.DataSource = datos_cantidad
                GridP_Asientos.Show()
                GridP_Asientos.Columns(3).DefaultCellStyle.Format = "##,##0.00"
                GridP_Asientos.Columns(4).DefaultCellStyle.Format = "##,##0.00"
                GridP_Asientos.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_Asientos.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            Else
                GridP_Asientos.Hide()
            End If
            If datos_cantidad.Rows.Count = 1 Then
                numeasiento_detalle = GridP_Asientos.Rows(0).Cells("no.asiento").Value
                anomes_detalle = GridP_Asientos.Rows(0).Cells("A�o-Mes").Value
                numemov_detalle = GridP_Asientos.Rows(0).Cells("No.Movimiento").Value
                numemov_select = numemov_detalle

                SuperTabControl1.SelectedTabIndex = 1
                TextB_numeasiento_nuevo.Text = numeasiento_detalle
                DateF_Fechaasiento_Nuevo.Text = GridP_Asientos.Rows(0).Cells("Fecha Asiento").Value

                REM cargamos detalle del comprobante
                carga_detalle_Asiento(numeasiento_detalle, anomes_detalle, numemov_detalle)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Buscar_Asiento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub ToolbarB_BuscarAsiento_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_BuscarAsiento.Click
        Try
            Buscar_Asiento()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Buscar_Asiento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub
    Protected Sub imprimir_report()
        Dim tituloreporte As String = String.Empty
        SIMF_CONTA.filtro_reporte = String.Empty
        SIMF_CONTA.sql_reporte = String.Empty
        'SIMF_CONTA.formula(0) = "compania," & SIMF_CONTA.nombre_cia.Trim
        SIMF_CONTA.formula(0) = "compania," & SUConversiones.ConvierteAString(RNEmpresa.ObtieneNombreEmpreaContabilidad())

        Try
            Select Case ComboBoxItem_SelectReport.SelectedIndex
                Case "0" REM TOTALES POR COMPROBANTES
                    SIMF_CONTA.formula(1) = "titulo," & SUConversiones.ConvierteAString(ComboBoxItem_SelectReport.SelectedItem).ToUpper
                    SIMF_CONTA.numeformulas = 2
                    Dim AND_sql As Boolean = False
                    '  If ComboB_ano_TabBusquedas.Text <> "" And ComboB_ano_TabBusquedas.SelectedIndex <> -1 Then
                    If ComboB_ano_TabBusquedas.Text <> "" Then
                        SIMF_CONTA.filtro_reporte = "year({totalescomprobante.fechaasiento})=" & SUConversiones.ConvierteAString(ComboB_ano_TabBusquedas.Text).Trim
                        AND_sql = True
                    End If

                    If ComboB_mes_TabBusquedas.Text <> "" And ComboB_mes_TabBusquedas.SelectedIndex > 0 Then
                        If AND_sql = True Then
                            SIMF_CONTA.filtro_reporte += " and "
                        End If
                        SIMF_CONTA.filtro_reporte += "month({totalescomprobante.fechaasiento})=" & SUConversiones.ConvierteAString(ComboB_mes_TabBusquedas.SelectedIndex).Trim

                    End If
                    If TextF_numecompro.Text <> "" Then
                        If AND_sql = True Then
                            SIMF_CONTA.filtro_reporte += " and "
                        End If
                        SIMF_CONTA.filtro_reporte += "({totalescomprobante.numeasiento})='" & SUConversiones.ConvierteAString(TextF_numecompro.Text).Trim & "'"

                    End If
                    FrmRPT_TotalesComprobante.Text = "Reporte de Asiento " & tituloreporte
                    FrmRPT_TotalesComprobante.Show()
                Case "1" REM DETALLE POR COMPROBANTE

                Case "2"

            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("imprimir_report - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub ButtonI_GenerarReporte_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_GenerarReporte.Click
        Try
            imprimir_report()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_GenerarReporte_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

    Private Sub ButtonItem_menuprincipal_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_menuprincipal.Click
        Try
            'Me.Close()
            'menu_conta.Show()
            cerrar_ventana()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem_menuprincipal_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonItem1_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_cerrarpantalla.Click
        Try
            cerrar_ventana()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem1_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub Imprimir()
        Try
            'C�digo para mostrar el reporte
            'Lo primero que hacemos es declarar una instancia
            'del data set dsRepCatClientes
            Dim mi_dsRepCatclientes As New DataSet
            'Lo llenamos con el contenido de la tabla CAT_CLIENTES
            Dim sqldata As New SqlClient.SqlDataAdapter
            Dim dataset_registros As DataSet = jackconsulta.retornaDataset("select * from dasientos", True)
            'Delcaramos una instancia del formulario frmReprotes
            Dim miForma As New Reportes
            'Le indicamos que debe mostrar mi_rptCatClientes
            miForma.CrystalReportViewer1.ReportSource = "C:\Users\Jack\Desktop\winfor_conta\WinForm_Conta\WinForm_Conta\Reportes\CrystalReport1.rpt"
            'que muestre el titulo "Reporte de Clientes"
            miForma.Text = "Reporte de Clientes"
            'Mostramos el formulario (el cual contiene el reporte)
            miForma.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBox.Show(ex.Message, "Imprimir",
            MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Protected Sub Nuevo_asiento()
        Dim anomes As Integer = 0
        Dim reader_existe_asiento As SqlDataReader = Nothing
        Dim ejecuta_resumen As String = String.Empty
        Try
            If TextB_numeasiento_nuevo.Text <> String.Empty Then
                If Format(CDate(DateF_Fechaasiento_Nuevo.Value.ToString.Trim), "yyyyMMdd") <> "00010101" Then
                    anomes = Year(DateF_Fechaasiento_Nuevo.Value) * 100 + (Month(DateF_Fechaasiento_Nuevo.Value))
                    reader_existe_asiento = jackAsientos.busca_asiento(TextB_numeasiento_nuevo.Text, anomes)
                    If reader_existe_asiento Is Nothing Then
                        REM agregamos el Asiento
                        ejecuta_resumen = jackconsulta.EjecutaSQL(String.Format("EXEC nuevocompro @anomes='{0}',@numeasiento='{1}'", anomes, TextB_numeasiento_nuevo.Text), True)
                        If jackAsientos.inserta_masicon(anomes, DateF_Fechaasiento_Nuevo.Value, TextB_numeasiento_nuevo.Text, ejecuta_resumen) = False Then
                            'jackmensaje.msg_izquierda("Error en la Generaci�n del Asiento", "Validaci�n de Asiento")
                        Else
                            carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, anomes, ejecuta_resumen)
                            jackAsientos.numemov = ejecuta_resumen
                            numemov_select = ejecuta_resumen
                        End If
                    Else
                        numemov_select = jackconsulta.Retorna_Campo("select numemov from Masientos where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes=" & anomes, True, True)
                        REM cargarmos el asiento sin n�mero movimiento
                        carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, anomes)
                    End If
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Nuevo_asiento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Protected Function carga_detalle_Asiento(ByVal numecompro As String, ByVal anomes As String, Optional ByVal numemov As String = "", Optional numedocumento As String = "") As Boolean
        REM cargamos masientos
        Dim ejecuta_resumen As String = String.Empty
        REM cargamos dasientos
        Dim sql_lineas_asientos As String = String.Empty
        Dim sql_para_reader As String = String.Empty
        Dim numemov_min As String = String.Empty
        Dim sql_masientos As SqlDataReader = Nothing
        Dim fecha_asiento As Date = Nothing
        Dim debe As Double = 0
        Dim haber As Double = 0
        Dim debeext As Double = 0
        Dim haberext As Double = 0
        Dim mes As Integer = 0
        Dim desc_masiento As Object = Nothing
        Dim dTOS As DataTable = Nothing
        Dim dtsDatos As DataTable = Nothing
        Try
            ejecuta_resumen = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", anomes, numemov, numecompro), True)
            sql_lineas_asientos = "SELECT numeasiento 'No.Asiento',Nlinea 'No.L�nea',numedoc 'No.Documento',desclinea 'Descripci�n l�nea',cdgocuenta 'Cuenta Contable',debelocal 'D�bito',haberlocal 'Cr�dito',numemov 'No.Movimiento',desccuenta 'Descripci�n Cuenta Contable',fechadoc 'Fecha.Doc' FROM Dasientos where estado='V' and  numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim
            sql_para_reader = "select numeasiento,anomes,fechaasiento,descasiento,debelocal,haberlocal,moneda,numemov  from Masientos where numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim

            If numedocumento <> "" Then
                sql_lineas_asientos += " and numedoc='" & numedocumento.Trim & "'"
            End If
            If numemov <> String.Empty Then
                sql_lineas_asientos += " and numemov='" & numemov.Trim & "'"
                sql_para_reader += " and numemov='" & numemov.Trim & "'"
                mostrar_todo_Asiento = False
            Else
                numemov_min = jackconsulta.Retorna_Campo("select MIN(numemov) from Masientos where numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim, True)
                sql_para_reader = "select sum(debelocal) debelocal,sum(haberlocal) haberlocal,sum(debeext) debeext,sum(haberext) haberext  from Masientos where numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim
                mostrar_todo_Asiento = True
            End If
            sql_masientos = jackconsulta.RetornaReader(sql_para_reader, True)
            If sql_masientos.HasRows Then
                While sql_masientos.Read
                    fecha_asiento = Nothing

                    debe = 0
                    haber = 0
                    debeext = 0
                    haberext = 0

                    If mostrar_todo_Asiento = True Then
                        fecha_asiento = jackconsulta.Retorna_Campo("select fechaasiento from Masientos where numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim & " and numemov=" & numemov_min, True)
                    Else
                        If numemov_min <> String.Empty Then
                            fecha_asiento = jackconsulta.Retorna_Campo("select fechaasiento from Masientos where numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim & " and numemov=" & numemov_min, True)
                        Else
                            fecha_asiento = sql_masientos!fechaasiento
                        End If
                    End If
                    mes = 0
                    mes = Month(fecha_asiento)
                    If IsDBNull(sql_masientos!debelocal) Then
                        debe = 0
                    Else
                        debe = sql_masientos!debelocal

                    End If
                    If IsDBNull(sql_masientos!haberlocal) Then
                        haber = 0
                    Else
                        haber = sql_masientos!haberlocal
                    End If

                    NumberF_debelocal.Text = debe
                    NumberF_habelocal.Text = haber
                    NumberF_debelocal.Text = debe
                    NumberF_diferencia_Mov.Text = debe - haber

                    If mostrar_todo_Asiento = True Then
                        desc_masiento = Nothing
                        desc_masiento = jackconsulta.Retorna_Campo("select descasiento from Masientos where numeasiento='" & numecompro.Trim & "' and anomes=" & anomes.Trim & " and numemov=" & numemov_min, True)
                        If IsDBNull(desc_masiento) Then
                            TextField_Descrip_asiento_detalle.Text = ""
                        Else
                            TextField_Descrip_asiento_detalle.Text = desc_masiento
                        End If

                    Else
                        If IsDBNull(sql_masientos!descasiento) Or IsNothing(sql_masientos!descasiento) Then
                            TextField_Descrip_asiento_detalle.Text = ""
                        Else
                            TextField_Descrip_asiento_detalle.Text = sql_masientos!descasiento
                        End If
                    End If
                End While
                sql_masientos.Close()

            Else
                ' jackmensaje.msg_izquierda("No se han Encontrado Coincidencias", "Buscar Asineto")
                Return False
                Exit Function
            End If
            sql_lineas_asientos += " order by numedoc"

            dTOS = New DataTable
            dtsDatos = Nothing

            dtsDatos = jackconsulta.Retornadatatable(sql_lineas_asientos, dTOS, True) 'clase que genera la consulta y devuelve un dataTable

            If dtsDatos.Rows.Count > 0 Then
                GridView_dasientos.DataSource = dtsDatos
                GridView_dasientos.Columns(5).DefaultCellStyle.Format = "##,##0.00"
                GridView_dasientos.Columns(6).DefaultCellStyle.Format = "##,##0.00"
                GridView_dasientos.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridView_dasientos.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("carga_detalle_Asiento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return True
    End Function

    Private Sub DateF_Fechaasiento_Nuevo_TextChanged(sender As Object, e As System.EventArgs) Handles DateF_Fechaasiento_Nuevo.TextChanged
        Try
            If DateF_Fechaasiento_Nuevo.Text <> "" Then
                TextB_anonuevo.Text = Year(DateF_Fechaasiento_Nuevo.Text)
                TextB_mesnuevo.Text = Month(DateF_Fechaasiento_Nuevo.Text)

                TextB_numeasiento_nuevo.Focus()

                If jackvalida.DameMescerrado(Year(DateF_Fechaasiento_Nuevo.Text) * 100 + Month(DateF_Fechaasiento_Nuevo.Text), True) = True Then

                    habilita_controles(False)
                    mensaje(ButtonItem_NuevoAsiento, "�ste Mes ya est� Cerrado Definitivamente", "Mes Cerrado Definitovamente")
                Else
                    habilita_controles(True)

                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("DateF_Fechaasiento_Nuevo_TextChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GridView_dasientos_CellErrorTextChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridView_dasientos.CellErrorTextChanged

    End Sub



    Private Sub GridView_dasientos_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridView_dasientos.CellValueChanged
        '  GridView_dasientos.Select(GridView_dasientos.CurrentRow.Index)
    End Sub

#Region "Nueva L�nea"

    Protected Sub Agregar_linea()
        Dim linea As Integer = 0
        Dim ejecuta_resumen As String = String.Empty
        Try
            jackAsientos.anomes = Year(DateF_Fechaasiento_Nuevo.Value) * 100 + (Month(DateF_Fechaasiento_Nuevo.Value))
            jackAsientos.numeasientos = TextB_numeasiento_nuevo.Text

            If SUConversiones.ConvierteAInt(numemov_select) = 0 Then
                numemov_select = TextB_numeasiento_nuevo.Text
            End If
            jackAsientos.numemov = SUConversiones.ConvierteAInt(numemov_select)
            If nuevalinea_opcion = True Then
                linea = jackAsientos.nueva_linea()
                If linea = 0 Then
                    Exit Sub
                End If
            Else
                linea = TextB_lineanueva.Text
            End If
            actualiza_linea(linea, jackAsientos.anomes)
            ejecuta_resumen = String.Empty
            ejecuta_resumen = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", jackAsientos.anomes, jackAsientos.numemov, TextB_numeasiento_nuevo.Text), True)

            If carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, jackAsientos.anomes, jackAsientos.numemov) = False Then
                Exit Sub
            End If
            If cerrar_nuevalinea = False And nuevalinea_opcion = True Then
                limpia_controles_linea()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Agregar_linea - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Protected Sub actualiza_linea(ByVal numelinea As Integer, ByVal anomes As Integer)
        Dim actualiza_dasientos_linea As String = String.Empty
        Try
            actualiza_dasientos_linea = "Update dasientos set desclinea='" & TextB_Deslinea.Text.ToUpper.Trim & "',debelocal=" & jackvalida.formato_moneda(TextB_debelinea.Text, "#########.##") & ",haberlocal=" & jackvalida.formato_moneda(TextB_haberlinea.Text, "#########.##")
            actualiza_dasientos_linea += " ,cdgocuenta='" & TextB_cdgocuenta.Text.Trim & "',"
            actualiza_dasientos_linea += "  desccuenta='" & Label_desccuenta.Text.Trim & "',numedoc='" & TextB_Numedocnuevo.Text & "',fechadoc='" & Format(CDate(DateF_FechaDoc_Nuevo.Text), "yyyyMMdd") & "' from Dasientos "
            actualiza_dasientos_linea += " where numeasiento='" & TextB_numeasiento_nuevo.Text & "' and anomes=" & anomes & " and numemov=" & numemov_select & " and nlinea=" & numelinea

            If jackconsulta.EjecutaSQL(actualiza_dasientos_linea, True) = -1 Then
                'jackmensaje.msg_izquierda("Error al Agregar la L�nea", "Validaci�n Asiento", Icon.JoystickError)
                MessageBox.Show("Error al Agregar la L�nea", "Validaci�n Asiento", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                descc_linea = TextB_Deslinea.Text.ToUpper.Trim
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("actualiza_linea - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Protected Sub actualiza_linea_tipomov(ByVal numelinea As Integer, ByVal anomes As Integer)
        Dim actualiza_dasientos_linea As String = String.Empty
        Try
            actualiza_dasientos_linea = String.Empty
            actualiza_dasientos_linea = "Update dasientos set desclinea='" & TextB_desc_tipomov.Text.ToUpper.Trim & "',debelocal=" & jackvalida.formato_moneda(jackAsientos.debelocal_tipomov, "#########.##") & ",haberlocal=" & jackvalida.formato_moneda(jackAsientos.haberlocal_tipomov, "#########.##")
            actualiza_dasientos_linea += " ,cdgocuenta='" & TextB_cuenta__tipomov.Text.Trim & "',"
            actualiza_dasientos_linea += "  desccuenta='" & Label_desc_tipomov.Text.Trim & "',numedoc='" & TextB_numedoc_tipomov.Text & "',Fechadoc='" & jackAsientos.FechaDocumento & "' from Dasientos "
            actualiza_dasientos_linea += " where numeasiento='" & jackAsientos.numeasientos & "' and anomes=" & anomes & " and numemov=" & jackAsientos.numemov_tipomov & " and nlinea=" & numelinea

            If jackconsulta.EjecutaSQL(actualiza_dasientos_linea, True) = -1 Then
                MessageBox.Show("Error al Agregar la L�nea", "Validaci�n Asiento", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            REM Actualizamos 
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("actualiza_linea_tipomov - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

    Private Sub GridP_Asientos_Click(sender As Object, e As System.EventArgs) Handles GridP_Asientos.Click
        Dim row As DataGridViewRow = Nothing
        Dim numeasiento As String = String.Empty
        Dim anomes As Integer = 0
        Dim numemov As String = String.Empty
        Dim max_nlinea_Gridp_Asientos As Integer = 0
        Try
            row = GridP_Asientos.CurrentRow
            max_nlinea_Gridp_Asientos = GridP_Asientos.RowCount - 1
            If row.Index < max_nlinea_Gridp_Asientos Then
                numeasiento = row.Cells(0).Value
                anomes = SUConversiones.ConvierteAInt(row.Cells(5).Value)
                numemov = row.Cells(6).Value
                numemov_select = numemov
                existe_asiento = True
                mostrar_todo_Asiento = False
                TextB_numeasiento_nuevo.Text = row.Cells(0).Value
                DateF_Fechaasiento_Nuevo.Text = row.Cells(1).Value
                If jackvalida.DameMescerrado(anomes, True) = True Then
                    habilita_controles(False)
                    mensaje(ButtonItem_NuevoAsiento, "�ste Mes ya est� Cerrado Definitivamente", "Mes Cerrado Definitovamente")
                Else
                    habilita_controles(True)
                End If
                carga_detalle_Asiento(numeasiento, anomes, numemov_select)
                SuperTabControl1.SelectedTabIndex = 1
                Dim ejecuta_resumen As String = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", anomes, numemov_select, numeasiento), True)

                limpia_controles_linea()
                cargar_documento()
            Else
                mensaje(ButtonItem_NuevoAsiento, "Debe Seleccionar un Registro V�lido", "Detalle del Asiento")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridP_Asientos_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonI_Anularasiento_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_Anularasiento.Click
        Try
            jackAsientos.anomes = Year(DateF_Fechaasiento_Nuevo.Value) * 100 + (Month(DateF_Fechaasiento_Nuevo.Value))
            jackAsientos.numeasientos = TextB_numeasiento_nuevo.Text

            jackAsientos.numemov = numemov_select
            If MessageBoxEx.Show("Se proceder� a Anular el Comprobante?", "Asientos/Comprobantes", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                If jackAsientos.elimina_asiento(False) = False Then
                    MessageBox.Show("Error al Anular Asiento", "Anular Asiento", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    limpia_controles_linea()
                    TextB_numeasiento_nuevo.Text = String.Empty
                    TextField_Descrip_asiento_detalle.Text = String.Empty
                    NumberF_debelocal.Text = "0.00"
                    NumberF_habelocal.Text = "0.00"
                    NumberF_diferencia_Mov.Text = "0.00"
                    DateF_Fechaasiento_Nuevo.Text = Now()
                    GridView_dasientos.DataSource = Nothing
                    SuperTabControl1.SelectedTabIndex = 1
                    TextB_numeasiento_nuevo.Focus()
                    MessageBox.Show("Asiento Anulado Exitosamente", "Anulnar Asiento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_Anularasiento_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button_guardar_Detalle_Click(sender As System.Object, e As System.EventArgs)
        Try
            Agregar_linea()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_guardar_Detalle_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GridView_dasientos_Click(sender As Object, e As System.EventArgs) Handles GridView_dasientos.Click
        Dim row_dasiento As DataGridViewRow = Nothing
        Dim max_nlinea As Integer = 0
        Dim numeline As String = String.Empty
        Dim numedoc As String = String.Empty
        Try
            row_dasiento = GridView_dasientos.CurrentRow
            max_nlinea = GridView_dasientos.RowCount - 1
            If row_dasiento Is Nothing Then
                Exit Sub
            End If
            If row_dasiento.Index < max_nlinea Then

                TextB_lineanueva.Text = IIf(IsDBNull(row_dasiento.Cells(1).Value), "", row_dasiento.Cells(1).Value)
                TextB_Numedocnuevo.Text = IIf(IsDBNull(row_dasiento.Cells(2).Value), "", row_dasiento.Cells(2).Value)
                TextB_cdgocuenta.Text = IIf(IsDBNull(row_dasiento.Cells(4).Value), "", row_dasiento.Cells(4).Value)
                TextB_debelinea.Text = Format(SUConversiones.ConvierteADouble(row_dasiento.Cells(5).Value), "###,###,###.##")
                TextB_haberlinea.Text = Format(SUConversiones.ConvierteADouble(row_dasiento.Cells(6).Value), "###,###,###.##")
                TextB_Deslinea.Text = IIf(IsDBNull(row_dasiento.Cells(3).Value), "", row_dasiento.Cells(3).Value)
                Label_desccuenta.Text = IIf(IsDBNull(row_dasiento.Cells(8).Value), "", row_dasiento.Cells(8).Value)
                DateF_FechaDoc_Nuevo.Text = IIf(IsDBNull(row_dasiento.Cells(9).Value), "", row_dasiento.Cells(9).Value)
                nuevalinea_opcion = False
            Else
                If MessageBoxEx.Show("Desea Insertar una Nueva l�nea Contable?", "Asientos/Comprobantes", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                    limpia_controles_linea()
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridView_dasientos_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub limpia_controles_linea()
        Try
            TextB_lineanueva.Text = ""
            TextB_Numedocnuevo.Text = ""
            TextB_cdgocuenta.Text = ""
            TextB_debelinea.Text = 0.0
            TextB_haberlinea.Text = 0.0
            ComboB_documentos.SelectedValue = -1
            TextB_Deslinea.Text = ""
            Label_desccuenta.Text = "?"
            TextB_Numedocnuevo.Focus()
            nuevalinea_opcion = True
            DateF_FechaDoc_Nuevo.Text = ""

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_Guarda_linea_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub habilita_controles(ByVal enable As Boolean)
        Try
            'Button_Guarda_linea.Enabled = enable
            Button_Guarda_linea.Visible = enable
            TextB_Numedocnuevo.Enabled = enable
            TextB_cdgocuenta.Enabled = enable
            TextB_Deslinea.Enabled = enable
            TextB_debelinea.Enabled = enable
            TextB_haberlinea.Enabled = enable
            ComboB_documentos.Enabled = enable
            'ButtonItem3.Expanded = False
            Bar2.Enabled = enable
            'ButtonItem_NuevoAsiento.Enabled = False

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_Guarda_linea_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button_Guarda_linea_Click(sender As System.Object, e As System.EventArgs) Handles Button_Guarda_linea.Click
        Try
            If TextB_numeasiento_nuevo.Text.Trim().Equals(String.Empty) Then
                '                numemov_select = RNCuentaContable.ObtenerUltimoConsecutivoAsiento() + 1
                TextB_numeasiento_nuevo.Text = RNCuentaContable.ObtenerUltimoConsecutivoAsiento() + 1
            End If
            If TextB_cdgocuenta.Text = "" Then
                valida_cuenta()
            Else
                Agregar_linea()
                limpia_controles_linea()
            End If
            actualiza_masientos()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_Guarda_linea_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub TextB_cdgocuenta_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles TextB_cdgocuenta.KeyDown
    '    If e.KeyValue = Keys.Enter Then
    '        valida_cuenta()
    '    End If
    'End Sub

    Private Sub valida_cuenta()
        Dim msg As Balloon = Nothing
        Try
            If jackvalida.existe_cuenta(TextB_cdgocuenta.Text.ToString.Trim) = False Or jackvalida.DameTipo_cuenta(TextB_cdgocuenta.Text.ToString.Trim) <> 2 Then
                Button_Guarda_linea.Enabled = False
                ButtonItem3.Enabled = False
                msg = New Balloon()

                msg.Style = eBallonStyle.Alert
                msg.CaptionImage = CType(BalloonTip.CaptionImage.Clone(), Image)
                msg.CaptionText = "Nueva L�nea"
                msg.Text = "Cuenta Contable no V�lida"
                msg.AlertAnimation = eAlertAnimation.TopToBottom
                msg.AutoClose = True
                msg.AutoCloseTimeOut = 8
                msg.AutoResize()
                msg.Owner = Me
                msg.Show(TextB_cdgocuenta, False)
            Else
                Button_Guarda_linea.Enabled = True
                ButtonItem3.Enabled = True
                TextB_Deslinea.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("valida_cuenta - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub TextB_cdgocuenta_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_cdgocuenta.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                If nuevalinea_opcion = True And TextB_Deslinea.Text = String.Empty Then
                    TextB_Deslinea.Text = descc_linea.ToUpper.Trim
                    Label_desccuenta.Text = jackconsulta.Dame_DescCuenata(TextB_cdgocuenta.Text.ToString.Trim)
                End If
                TextB_Deslinea.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cdgocuenta_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_cdgocuenta_LostFocus(sender As Object, e As System.EventArgs) Handles TextB_cdgocuenta.LostFocus
        Try
            If TextB_cdgocuenta.Text <> "" Then
                valida_cuenta()
                Label_desccuenta.Text = jackconsulta.Dame_DescCuenata(TextB_cdgocuenta.Text.ToString.Trim)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cdgocuenta_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_debelinea_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_debelinea.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                If IsNumeric(TextB_debelinea.Text) Then

                    If TextB_haberlinea.Text = "" Then
                        TextB_haberlinea.Text = "0.00"
                    End If
                    TextB_haberlinea.Focus()
                    If SUConversiones.ConvierteADouble(TextB_debelinea.Text) > 0 Then
                        debe_temp = Format(SUConversiones.ConvierteADouble(TextB_debelinea.Text), "###,###,###.##")
                        TextB_debelinea.Text = jackvalida.formato_moneda(TextB_debelinea.Text)
                    Else
                        TextB_debelinea.Text = "0.00"
                    End If
                Else
                    TextB_debelinea.Text = "0.00"
                    TextB_debelinea.Focus()
                    MessageBoxEx.Show("El Monto debe ser num�rico")
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_debelinea_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_debelinea_LostFocus(sender As Object, e As System.EventArgs) Handles TextB_debelinea.LostFocus
        Try
            If TextB_debelinea.Text = String.Empty Then
                TextB_debelinea.Text = 0.0
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_debelinea_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_debelinea_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextB_debelinea.TextChanged
        Try
            If TextB_debelinea.Text = String.Empty Then
                TextB_debelinea.Text = 0.0
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_debelinea_TextChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_haberlinea_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_haberlinea.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                If IsNumeric(TextB_haberlinea.Text) Then
                    Button_Guarda_linea.Focus()
                    If CDbl(TextB_haberlinea.Text) > 0 Then
                        TextB_haberlinea.Text = jackvalida.formato_moneda(TextB_haberlinea.Text)
                    Else

                        TextB_haberlinea.Text = "0.00"
                    End If
                Else
                    TextB_haberlinea.Text = "0.00"
                    TextB_haberlinea.Focus()
                    MessageBoxEx.Show("El Monto debe ser num�rico")
                End If

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_haberlinea_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_haberlinea_LostFocus(sender As Object, e As System.EventArgs) Handles TextB_haberlinea.LostFocus
        Try
            If TextB_haberlinea.Text = String.Empty Then
                TextB_haberlinea.Text = 0.0
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_haberlinea_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_Numedocnuevo_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_Numedocnuevo.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                If TextB_Numedocnuevo.Text = String.Empty Then
                    TextB_Numedocnuevo.Text = TextB_numeasiento_nuevo.Text.Trim
                    TextB_Deslinea.Text = descc_linea
                    DateF_FechaDoc_Nuevo.Text = fechadoc_mantiene
                End If
                DateF_FechaDoc_Nuevo.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_Numedocnuevo_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_Numedocnuevo_LostFocus(sender As Object, e As System.EventArgs) Handles TextB_Numedocnuevo.LostFocus
        Try
            If TextB_Numedocnuevo.Text = String.Empty Then
                TextB_Numedocnuevo.Text = TextB_numeasiento_nuevo.Text.Trim
                TextB_Deslinea.Text = descc_linea
                DateF_FechaDoc_Nuevo.Text = fechadoc_mantiene
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_Numedocnuevo_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub DateF_FechaDoc_Nuevo_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles DateF_FechaDoc_Nuevo.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                If DateF_FechaDoc_Nuevo.Text = String.Empty Then
                    DateF_FechaDoc_Nuevo.Text = DateF_Fechaasiento_Nuevo.Text
                End If
                TextB_cdgocuenta.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("DateF_FechaDoc_Nuevo_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub DateF_FechaDoc_Nuevo_LostFocus(sender As Object, e As System.EventArgs) Handles DateF_FechaDoc_Nuevo.LostFocus
        Try
            If DateF_FechaDoc_Nuevo.Text = String.Empty Then
                DateF_FechaDoc_Nuevo.Text = DateF_Fechaasiento_Nuevo.Text
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("DateF_FechaDoc_Nuevo_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub ButtonI_agregar_nuevalinea_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_agregar_nuevalinea.Click
        Try
            limpia_controles_linea()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_agregar_nuevalinea_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ButtonItem_NuevoAsiento_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_NuevoAsiento.Click
        limpia_controles_linea()
        numemov_select = ""
        TextB_numeasiento_nuevo.Text = String.Empty

        TextField_Descrip_asiento_detalle.Text = String.Empty
        NumberF_debelocal.Text = "0.00"
        NumberF_habelocal.Text = "0.00"
        NumberF_diferencia_Mov.Text = "0.00"
        DateF_Fechaasiento_Nuevo.Text = Now()
        GridView_dasientos.DataSource = Nothing
        SuperTabControl1.SelectedTabIndex = 1
        TextB_numeasiento_nuevo.Focus()
    End Sub

    Private Sub TextB_numeasiento_nuevo_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_numeasiento_nuevo.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                TextField_Descrip_asiento_detalle.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_numeasiento_nuevo_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_numeasiento_nuevo_LostFocus(sender As Object, e As System.EventArgs) Handles TextB_numeasiento_nuevo.LostFocus
        Dim item As Integer = 0
        Dim control As Control = Nothing
        Try
            If TextB_numeasiento_nuevo.Text <> String.Empty Then
                Nuevo_asiento()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_numeasiento_nuevo_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ButtonI_eliminalinea_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_eliminalinea.Click
        Dim row_dasiento As DataGridViewRow = Nothing
        Dim max_nlinea As Integer = 0
        Dim msg As DevComponents.DotNetBar.Balloon = Nothing
        Try
            row_dasiento = GridView_dasientos.CurrentRow
            max_nlinea = GridView_dasientos.RowCount - 1
            If row_dasiento.Index < max_nlinea Then
                jackAsientos.numemov = numemov_select
                jackAsientos.anomes = TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text
                jackAsientos.nlinea = TextB_lineanueva.Text
                jackAsientos.numeasientos = TextB_numeasiento_nuevo.Text
                If jackAsientos.elimina_linea = False Then
                    MessageBoxEx.Show("Error al eliminar la l�nea")
                Else
                    carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, jackAsientos.anomes, jackAsientos.numemov)
                    limpia_controles_linea()
                End If
            Else
                msg = New Balloon()

                msg.Style = eBallonStyle.Alert
                msg.CaptionImage = CType(BalloonTip.CaptionImage.Clone(), Image)
                msg.CaptionText = "Eliminar L�nea"
                msg.Text = "No exite L�nea a eliminar, L�nea Vac�a"
                msg.AlertAnimation = eAlertAnimation.TopToBottom
                msg.AutoClose = True
                msg.AutoCloseTimeOut = 8
                msg.AutoResize()
                msg.Owner = Me
                msg.Show(ButtonI_eliminalinea, False)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_eliminalinea_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextField_Descrip_asiento_detalle_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextField_Descrip_asiento_detalle.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                TextB_Numedocnuevo.Focus()
                nuevalinea_opcion = True
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextField_Descrip_asiento_detalle_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextField_Descrip_asiento_detalle_LostFocus(sender As Object, e As System.EventArgs) Handles TextField_Descrip_asiento_detalle.LostFocus

        Try
            actualiza_masientos()
            TextField_Descrip_asiento_detalle.Text = TextField_Descrip_asiento_detalle.Text.ToUpper.Trim()
            nuevalinea_opcion = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextField_Descrip_asiento_detalle_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub actualiza_masientos()
        Dim actualiza_masiento As String = String.Empty

        Try
            actualiza_masiento = "update masientos  set descasiento='" & TextField_Descrip_asiento_detalle.Text.ToUpper.Trim & "' where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes='" & TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text & "'"
            If numemov_select <> String.Empty Then
                actualiza_masiento += " and numemov=" & numemov_select
            End If
            jackconsulta.EjecutaSQL(actualiza_masiento, True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("actualiza_masientos - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonItem4_Click(sender As System.Object, e As System.EventArgs)

        Dim datos_repor As DataSet = Nothing
        Try
            datos_repor = New DataSet
            datos_repor = jackconsulta.retornaDataset("select * from dasientos where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes='" & TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text & "' and numemov=" & numemov_select, True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem4_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Sub cargar_documento()

        'Dim sql_documento As String = "select numedoc,(numedoc + '-' + (case estado when 'V' then 'Vigente' when 'N' then 'Anulado' end )) as numedocestado from dasientos where numeasiento='" & TextF_numecompro_detalle.Text.Trim & "' and anomes=" & Year(DateF_fechcompro_detalle.Value) * 100 + (Month(DateF_fechcompro_detalle.Value)) & " group by numedoc,estado "
        Dim sql_documento As String = String.Empty
        Dim dtcombo As DataTable = Nothing
        Dim dtcombo_new As DataTable = Nothing
        Try
            sql_documento = "select (numedoc + '-' + (case estado when 'V' then 'Vigente' when 'N' then 'Anulado' end )) as numedocestado,numedoc from dasientos where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes='" & TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text & "' group by numedoc,estado "
            dtcombo_new = New DataTable
            dtcombo = jackconsulta.Retornadatatable(sql_documento, dtcombo_new, True)
            jackconsulta.carga_combo_DN(ComboB_documentos, dtcombo, True)
            'ComboB_documentos.cargacombo(sql_documento, True)

            If numemov_select <> String.Empty And numemov_select > 0 And ComboB_documentos.SelectedIndex > 0 Then
                Dim numedoc_select As String = "select numedoc from dasientos where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes='" & TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text & "' And numemov = " & numemov_select
                ComboB_documentos.SelectedValue = jackconsulta.Retorna_Campo(numedoc_select, True, True)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("cargar_documento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub ButtonItem7_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem7.Click
        Try
            eliminar_documento = True
            cambios_eliminadocumento()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem7_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub ButtonItem_reestablecerdocuemento_Click(sender As Object, e As System.EventArgs) Handles ButtonItem_reestablecerdocuemento.Click
        Try
            eliminar_documento = False
            cambios_restablecedocumento()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem_reestablecerdocuemento_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Protected Sub cambios_eliminadocumento()

        Try
            If ComboB_documentos.SelectedValue <> String.Empty Then
                If jackconsulta.Retorna_Campo("select estado from dasientos  where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes='" & TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text & "' and numemov=" & numemov_select & " and numedoc='" & ComboB_documentos.SelectedValue & "'", True) <> "N" Then

                    If jackAsientos.elimina_documento(ComboB_documentos.SelectedValue, TextB_numeasiento_nuevo.Text, numemov_select, TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, eliminar_documento) = True Then
                        Dim ejecuta_resumen As String = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, numemov_select, TextB_numeasiento_nuevo.Text), True)
                        carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, numemov_select)

                        MessageBox.Show("Documento " & ComboB_documentos.SelectedValue & " eliminado exitosamente", "Borrar Documento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("No se Pudo Anular el Documento, Consulte con su Administrador Inform�tico", "Anular Documento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    MessageBox.Show("�ste Documento ya se encuentra Anulado", "Anular Documento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                cargar_documento()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("cambios_eliminadocumento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub
    Protected Sub cambios_restablecedocumento()

        Try
            If ComboB_documentos.SelectedValue.ToString <> String.Empty And ComboB_documentos.SelectedValue.ToString <> "-1" Then ' And ComboB_documentos.Text <> "" Then
                If jackconsulta.Retorna_Campo("select estado from dasientos  where numeasiento='" & TextB_numeasiento_nuevo.Text.Trim & "' and anomes='" & TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text & "'  and numedoc='" & ComboB_documentos.SelectedValue & "' group by numedoc,estado", True) <> "V" Then

                    If jackAsientos.elimina_documento(ComboB_documentos.SelectedValue, TextB_numeasiento_nuevo.Text, numemov_select, TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, eliminar_documento) = True Then
                        Dim ejecuta_resumen As String = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, numemov_select, TextB_numeasiento_nuevo.Text), True)
                        carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, numemov_select)

                        'MessageBox.Show("Documento " & ComboB_documentos.SelectedValue & " Restablecido exitosamente", "Restablecer Documento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        mensaje(ButtonItem_reestablecerdocuemento, "Documento " & ComboB_documentos.SelectedValue & " Restablecido exitosamente", "Restablecer Documento")
                    Else
                        'MessageBox.Show("No se Pudo Restablecer el Documento, Consulte con su Administrador Inform�tico", "Restablecer Documento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        mensaje(ButtonItem_reestablecerdocuemento, "No se Pudo Restablecer el Documento, Consulte con su Administrador Inform�tico", "Restablecer Documento")
                    End If
                Else
                    '    MessageBox.Show("�ste Documento ya se encuentra Vigente", "Restablecer Documento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mensaje(ButtonItem_reestablecerdocuemento, "�ste Documento ya se encuentra Vigente", "Restablecer Documento")
                End If
            End If
            cargar_documento()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("cambios_restablecedocumento - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub mensaje(ByVal controls As DevComponents.DotNetBar.ButtonItem, ByVal mensaje As String, ByVal caption As String)
        Dim msg As Balloon = New Balloon()

        Try
            msg = New DevComponents.DotNetBar.Balloon()
            msg.Style = eBallonStyle.Alert
            msg.BackColor = System.Drawing.Color.DarkSlateGray
            msg.BackColor2 = System.Drawing.Color.SteelBlue
            msg.CaptionImage = CType(BalloonTip.CaptionImage.Clone(), Image)
            msg.CaptionText = caption
            msg.CaptionColor = System.Drawing.Color.Blue
            msg.Text = mensaje
            msg.ForeColor = System.Drawing.Color.White

            msg.AlertAnimation = eAlertAnimation.LeftToRight
            msg.AutoClose = True
            msg.AutoCloseTimeOut = 8
            msg.AutoResize()
            msg.Owner = Me
            msg.Show(controls, False)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("mensaje - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_Deslinea_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_Deslinea.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            TextB_debelinea.Focus()
        End If
    End Sub

    Private Sub TextB_Deslinea_LostFocus(sender As Object, e As System.EventArgs) Handles TextB_Deslinea.LostFocus
        Try
            TextB_Deslinea.Text = TextB_Deslinea.Text.ToUpper.Trim
            descc_linea = TextB_Deslinea.Text
            fechadoc_mantiene = DateF_Fechaasiento_Nuevo.Text
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_Deslinea_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonI_limpiar_busqueda_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_limpiar_busqueda.Click
        TextF_numecompro.Text = String.Empty
        TextF_Documento.Text = String.Empty
        ComboB_ano_TabBusquedas.SelectedValue = ""
        ComboB_mes_TabBusquedas.SelectedValue = -1
        DateF_FechaIni_compro.Text = ""
        DateF_FechaFin_compro.Text = ""
        GridP_Asientos.DataSource = Nothing
        TextF_numecompro.Focus()
    End Sub

    Private Sub ButtonI_limpiar_detalle_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_limpiar_detalle.Click
        Try
            ButtonItem_NuevoAsiento_Click(sender, e)
            limpia_controles_linea()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_limpiar_detalle_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ToolbarB_BuscarInconsistencias_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_BuscarInconsistencias.Click
        Dim sql_buscarinconsistencias As String = String.Empty
        Dim datos_cantidad As DataTable = Nothing
        Dim anomes As String = String.Empty
        Dim celdas As DataGridViewRow = Nothing
        Dim sql_inconcistencia_cuentas As String = String.Empty
        Try
            sql_buscarinconsistencias = "select M.numeasiento 'No.Asiento',M.fechaasiento 'Fecha Asiento',M.descasiento 'Descripci�n Asiento',M.debelocal 'D�bito',M.haberlocal 'Cr�dito',M.anomes 'A�o-Mes',M.numemov 'No.Movimiento',(debelocal)-(haberlocal) Descuadre from Masientos M where ((debelocal)-(haberlocal))<>0"
            datos_cantidad = New DataTable
            If ComboB_ano_TabRevisar.Text <> "-1" And ComboB_ano_TabRevisar.Text <> "" Then
                sql_buscarinconsistencias += " and substring((CONVERT( nvarchar(6),M.anomes)),1,4)=" & ComboB_ano_TabRevisar.Text.Trim
                anomes = ComboB_ano_TabRevisar.Text.Trim
            End If
            If ComboB_mes_TabRevisar.SelectedValue <> -1 And ComboB_mes_TabRevisar.SelectedValue <> "-1" And ComboB_mes_TabRevisar.SelectedValue <> "" Then

                sql_buscarinconsistencias += " and substring((CONVERT( nvarchar(6),M.anomes)),5,2)=" & ComboB_mes_TabRevisar.SelectedValue.trim
                If anomes <> "" Then
                    anomes = anomes * 100 + ComboB_mes_TabRevisar.SelectedValue
                End If

            End If

            datos_cantidad = jackconsulta.cargaGrid(sql_buscarinconsistencias, True)

            If datos_cantidad.Rows.Count > 0 Then
                ExpandablePanel5.Visible = True
                GridP_inconsistencias.Show()
                GridP_inconsistencias.DataSource = datos_cantidad
                celdas = GridP_inconsistencias.CurrentRow
                GridP_inconsistencias.Columns(3).DefaultCellStyle.Format = "##,##0.00"
                GridP_inconsistencias.Columns(4).DefaultCellStyle.Format = "##,##0.00"
                GridP_inconsistencias.Columns(7).DefaultCellStyle.Format = "##,##0.00"
                GridP_inconsistencias.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_inconsistencias.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_inconsistencias.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            Else
                REM evaluamos las cuentas contables de los asientos
                sql_inconcistencia_cuentas = String.Empty
                sql_inconcistencia_cuentas = "select numeasiento 'No.Asiento',fechacrea,numedoc 'Documento',nlinea 'L�nea',d.cdgocuenta 'Cuenta',anomes 'A�o-Mes',numemov 'No.Movimiento', 'Cuenta Contable no Existe' as 'Error' from dasientos d "
                sql_inconcistencia_cuentas += " where anomes=" & anomes & " and d.cdgocuenta not in (select cdgocuenta from catalogoconta)"
                datos_cantidad = jackconsulta.cargaGrid(sql_inconcistencia_cuentas, True)
                ExpandablePanel5.Visible = True
                GridP_inconsistencias.Show()
                GridP_inconsistencias.DataSource = datos_cantidad
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_BuscarInconsistencias_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GridP_inconsistencias_Click(sender As Object, e As System.EventArgs) Handles GridP_inconsistencias.Click
        Dim row As DataGridViewRow = Nothing
        Dim max_linea_inconsistencias As Integer = 0
        Dim numeasiento As String = String.Empty
        Dim anomes As String = String.Empty
        Dim numemov As String = String.Empty
        Dim ejecuta_resumen As String = String.Empty
        Try
            row = GridP_inconsistencias.CurrentRow
            max_linea_inconsistencias = GridP_inconsistencias.RowCount - 1
            If row Is Nothing Then
                Exit Sub
            End If
            If row.Index < max_linea_inconsistencias Then
                numeasiento = String.Empty
                anomes = String.Empty
                numemov = String.Empty
                numeasiento = row.Cells(0).Value
                anomes = row.Cells(5).Value
                numemov = row.Cells(6).Value
                numemov_select = numemov
                existe_asiento = True
                mostrar_todo_Asiento = False
                TextB_numeasiento_nuevo.Text = row.Cells(0).Value
                DateF_Fechaasiento_Nuevo.Text = row.Cells(1).Value

                carga_detalle_Asiento(numeasiento, anomes, numemov_select)
                SuperTabControl1.SelectedTabIndex = 1
                ejecuta_resumen = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", anomes, numemov_select, numeasiento), True)
                'If jackvalida.DameMescerrado(anomes, True) = True Then
                '    'Button_guardar_Detalle.Visible = False
                'End If
                limpia_controles_linea()
                cargar_documento()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridP_inconsistencias_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub ToolbarB_Exportadescuadres_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_Exportadescuadres.Click
        Try
            jackconsulta.Exportar_Grilla_Excel(GridP_inconsistencias, "Descuadres")
            Reportes.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_Exportadescuadres_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub expor(ByVal _reporte As String, ByVal _dtTabla As DataTable)
        Dim rptDocument As ReportDocument = Nothing
        Dim _spathreporte As String = String.Empty
        Try
            rptDocument = New ReportDocument
            _spathreporte = CType(configurationAppSettings.GetValue("PathReportes", GetType(System.String)), String)
            rptDocument.Load(_spathreporte & _reporte & ".rpt")
            rptDocument.SetDataSource(_dtTabla)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("expor - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function configurationAppSettings() As Object
        Throw New NotImplementedException
    End Function

    Private Function CrystalReportViewer1() As Object
        Throw New NotImplementedException
    End Function

    Private Sub ButtonI_imprimirdescuadre_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_imprimirdescuadre.Click
        Dim and_ As Boolean = False
        Try
            SIMF_CONTA.filtro_reporte = ""
            If ComboB_ano_TabRevisar.Text <> "-1" Then
                and_ = True
                SIMF_CONTA.filtro_reporte = " Year (fechaasiento)=" & ComboB_ano_TabRevisar.Text.Trim
            End If
            If ComboB_mes_TabRevisar.SelectedValue <> "-1" And ComboB_mes_TabRevisar.SelectedValue <> "" Then
                If and_ = True Then
                    SIMF_CONTA.filtro_reporte += " and"
                End If
                SIMF_CONTA.filtro_reporte += "  Month (fechaasiento)=" & ComboB_mes_TabRevisar.SelectedValue.trim
            End If

            Frm_RPT_AsientosDescuadres.Text = "Reporte de Asientos Descuadrados"
            Frm_RPT_AsientosDescuadres.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_imprimirdescuadre_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ButtonI_ImprimeAsiento_detalle_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_ImprimeAsiento_detalle.Click
        Dim tituloreporte As String = "ASIENTO CONTABLE"
        Try
            SIMF_CONTA.filtro_reporte = ""
            'SIMF_CONTA.formula(0) = "compania," & SIMF_CONTA.nombre_cia.Trim
            SIMF_CONTA.formula(0) = "compania," & SUConversiones.ConvierteAString(RNEmpresa.ObtieneNombreEmpreaContabilidad())
            SIMF_CONTA.formula(1) = "titulo," & tituloreporte
            SIMF_CONTA.formula(2) = SUConversiones.ConvierteAString(TextB_numeasiento_nuevo.Text).Trim
            SIMF_CONTA.formula(3) = SUConversiones.ConvierteAInt(TextB_anonuevo.Text.Trim) * 100 + SUConversiones.ConvierteAInt(TextB_mesnuevo.Text.Trim)
            SIMF_CONTA.formula(4) = numemov_select
            SIMF_CONTA.numeformulas = 5
            If TextB_numeasiento_nuevo.Text <> String.Empty And Len(TextB_numeasiento_nuevo.Text.Trim) > 0 Then
                SIMF_CONTA.filtro_reporte = "{dasientos.numeasiento}='" & SUConversiones.ConvierteAString(TextB_numeasiento_nuevo.Text).Trim & "' and {dasientos.anomes}=" & SUConversiones.ConvierteAInt(TextB_anonuevo.Text.Trim) * 100 + SUConversiones.ConvierteAInt(TextB_mesnuevo.Text.Trim)
                If numemov_select.Trim <> String.Empty And mostrar_todo_Asiento = False Then
                    SIMF_CONTA.filtro_reporte += " and {dasientos.numemov}=" & numemov_select
                End If
                SIMF_CONTA.filtro_reporte += " order by dasientos.fechadoc "
                Frm_RPTAsientos.Text = "Reporte de Asiento No." & TextB_numeasiento_nuevo.Text.Trim
                Frm_RPTAsientos.Show()
            Else
                mensaje(ButtonI_ImprimeAsiento_detalle, "Indique un N�mero de Asiento a Imprimir", "Generaci�n de Reporte")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_ImprimeAsiento_detalle_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GridView_dasientos_SelectionChanged(sender As Object, e As System.EventArgs) Handles GridView_dasientos.SelectionChanged
        Try
            GridView_dasientos_Click(sender, e)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridView_dasientos_SelectionChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GridView_dasientos_Sorted(sender As Object, e As System.EventArgs) Handles GridView_dasientos.Sorted
        Try
            GridView_dasientos_Click(sender, e)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridView_dasientos_Sorted - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonItem2_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem2.Click
        Try
            ComboB_ano_TabRevisar.Text = ""
            ComboB_mes_TabRevisar.SelectedValue = "-1"
            GridP_inconsistencias.DataSource = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem2_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonItem3_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem3.Click
        Try
            If TextB_cdgocuenta.Text = "" Then
                valida_cuenta()
            Else
                Agregar_linea()
                limpia_controles_linea()

            End If
            actualiza_masientos()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem3_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ComboB_tipomov_Click(sender As System.Object, e As System.EventArgs) Handles ComboB_tipomov.Click
        Dim datos_cantidad As New DataTable
        Dim sql_facturas As String = String.Empty
        Dim sql_recibos As String = String.Empty
        Try
            Button_buscardocumento_xintegrar.Enabled = True
            Select Case ComboB_tipomov.SelectedItem.ToString
                Case "Facturas"
                    sql_facturas = String.Empty
                    sql_facturas = "select distinct m.idAsiento, m.numeroasiento 'N�mero Asiento',descripcionasiento 'Descripci�n Asiento',totaldeber 'D�bito',"
                    sql_facturas += " totalhaber 'Cr�dito'  from mstasientosfacturas m inner join detasientosfacturas d on m.idasiento=d.idasiento and m.numeroasiento=d.numeroasiento where d.iderror>3"
                    datos_cantidad = jackconsulta.cargaGrid(sql_facturas, True)
                Case "Recibos"
                    sql_recibos = String.Empty
                    sql_recibos = "select distinct m.idAsiento, m.numeroasiento 'N�mero Asiento',descripcionasiento 'Descripci�n Asiento',totaldeber 'D�bito',"
                    sql_recibos += " totalhaber 'Cr�dito'  from mstasientosrecibos m inner join DetAsientosRecibos d on m.idasiento=d.idasiento and m.numeroasiento=d.numeroasiento where d.iderror>3"
                    datos_cantidad = jackconsulta.cargaGrid(sql_recibos, True)
            End Select
            If datos_cantidad.Rows.Count > 0 Then
                ExpandablePanel5.Visible = True
                DGV_asientos_tipomov.Show()
                DGV_asientos_tipomov.DataSource = datos_cantidad
                DGV_asientos_tipomov.Columns(3).DefaultCellStyle.Format = "##,##0.00"
                DGV_asientos_tipomov.Columns(4).DefaultCellStyle.Format = "##,##0.00"
                jackAsientos.tipomov = ComboB_tipomov.SelectedItem.ToString
            Else
                DGV_asientos_tipomov.DataSource = Nothing
            End If
            DGV_Detalle_tipomov.DataSource = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ComboB_tipomov_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Protected Sub cargar_asientos_tipomov(ByVal tipomov As String, ByVal idasiento As String)
        Dim datos_cantidad As DataTable = Nothing
        Dim sql_buscarinconsistencias As String = String.Empty
        Dim sql_recibos As String = String.Empty
        Dim sql_facturas As String = String.Empty
        Try
            datos_cantidad = New DataTable
            Select Case tipomov
                Case "Facturas"
                    sql_facturas = String.Empty
                    sql_facturas = "select catErrorProcesoAsiento.descripcion Error,numeroasiento 'N�mero Asiento',idlinea 'L�nea',descripcionlinea 'Descripci�n l�nea',cuentacontable 'Cuenta Contable' ,"
                    sql_facturas += "numerodocumento 'Documento',montohaber 'Cr�dito',montodeber 'D�bito' ,aniomes as 'Anio-mes',FechaIngreso 'Fecha Documento',convert(char(8),FechaIngreso,112) from detasientosfacturas inner join catErrorProcesoAsiento on detasientosfacturas.iderror= catErrorProcesoAsiento.iderror"
                    sql_facturas += " where idasiento =" & idasiento & " And detasientosfacturas.iderror > 3 And detasientosfacturas.iderror <> 6 order by numerodocumento"
                    datos_cantidad = jackconsulta.cargaGrid(sql_facturas, True)
                Case "Recibos"
                    sql_recibos = String.Empty
                    sql_recibos = "select catErrorProcesoAsiento.descripcion Error,numeroasiento 'N�mero Asiento',idlinea 'L�nea',descripcionlinea 'Descripci�n l�nea',cuentacontable 'Cuenta Contable' ,"
                    sql_recibos += "numerodocumento 'Documento',montohaber 'Cr�dito',montodeber 'D�bito' ,aniomes as 'Anio-mes',FechaIngreso 'Fecha Documento',convert(char(8),FechaIngreso,112) from DetAsientosRecibos inner join catErrorProcesoAsiento on DetAsientosRecibos.iderror= catErrorProcesoAsiento.iderror"
                    sql_recibos += " where idasiento =" & idasiento & " And DetAsientosRecibos.iderror > 3 And DetAsientosRecibos.iderror <> 6 order by numerodocumento"
                    datos_cantidad = jackconsulta.cargaGrid(sql_recibos, True)
            End Select

            sql_buscarinconsistencias = String.Empty
            sql_buscarinconsistencias = "select M.numeasiento 'No.Asiento',M.fechaasiento 'Fecha Asiento',M.descasiento 'Descripci�n Asiento',M.debelocal 'D�bito',M.haberlocal 'Cr�dito',M.anomes 'A�o-Mes',M.numemov 'No.Movimiento',(debelocal)-(haberlocal) Descuadre from Masientos M where ((debelocal)-(haberlocal))<>0"

            If datos_cantidad.Rows.Count > 0 Then
                ExpandablePanel5.Visible = True
                DGV_Detalle_tipomov.Show()
                DGV_Detalle_tipomov.DataSource = datos_cantidad
                DGV_Detalle_tipomov.Columns(7).DefaultCellStyle.Format = "##,##0.00"
                DGV_Detalle_tipomov.Columns(6).DefaultCellStyle.Format = "##,##0.00"
            Else
                DGV_Detalle_tipomov.DataSource = Nothing
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("cargar_asientos_tipomov - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DGV_asientos_tipomov_Click(sender As Object, e As System.EventArgs) Handles DGV_asientos_tipomov.Click
        Dim row As DataGridViewRow = Nothing
        Dim numeasiento As String = String.Empty
        Dim anomes As String = String.Empty
        Dim numemov As String = String.Empty
        Dim max_nlinea_DGV_asientos_tipomov As Integer = 0

        Try
            row = DGV_asientos_tipomov.CurrentRow
            max_nlinea_DGV_asientos_tipomov = DGV_asientos_tipomov.RowCount - 1
            If row Is Nothing Then
                Exit Sub
            End If
            If row.Index < max_nlinea_DGV_asientos_tipomov Then
                cargar_asientos_tipomov(ComboB_tipomov.SelectedItem.ToString, row.Cells(0).Value)
                jackAsientos.idasiento = row.Cells(0).Value.ToString.Trim
                Dim reader_datos_tipomov As SqlDataReader = jackconsulta.RetornaReader("select * from mstasientosfacturas where idasiento=" & row.Cells(0).Value & " and numeroasiento='" & row.Cells(1).Value & "'", True)
                If Not reader_datos_tipomov Is Nothing Then
                    While reader_datos_tipomov.Read
                        jackAsientos.anomes_tipomov = reader_datos_tipomov!aniomes
                    End While
                    reader_datos_tipomov.Close()
                End If
            Else
                mensaje(ButtonItem_NuevoAsiento, "Debe Seleccionar un Registro V�lido", "Detalle del Asiento")
            End If
            Button_contabiliza_tipomov.Enabled = False
            limpia_detallelinea_tipomov()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("DGV_asientos_tipomov_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub DGV_Detalle_tipomov_Click(sender As Object, e As System.EventArgs) Handles DGV_Detalle_tipomov.Click
        Dim row_dasiento As DataGridViewRow = Nothing
        Dim max_nlinea As Integer = 0
        Dim numeline As String = String.Empty
        Dim numedoc As String = String.Empty
        Try
            row_dasiento = DGV_Detalle_tipomov.CurrentRow
            max_nlinea = DGV_Detalle_tipomov.RowCount - 1
            If row_dasiento Is Nothing Then
                Exit Sub
            End If
            If row_dasiento.Index < max_nlinea Then
                TextB_cuenta__tipomov.Text = row_dasiento.Cells(4).Value
                TextB_desc_tipomov.Text = row_dasiento.Cells(3).Value
                TextB_nlinea_tipomov.Text = row_dasiento.Cells(2).Value
                TextB_numedoc_tipomov.Text = row_dasiento.Cells(5).Value
                'TextB_haberlinea.Text = Format(row_dasiento.Cells(6).Value, "###,###,###.##")
                'TextB_Deslinea.Text = IIf(IsDBNull(row_dasiento.Cells(3).Value), "", row_dasiento.Cells(3).Value)
                'Label_desccuenta.Text = row_dasiento.Cells(8).Value
                'nuevalinea_opcion = False
                jackAsientos.nlinea_tipomov = TextB_nlinea_tipomov.Text
                jackAsientos.numeasiento_tipomov = row_dasiento.Cells(1).Value
                jackAsientos.numedoc_tipomov = TextB_numedoc_tipomov.Text
                '  jackAsientos.numemov_tipomov = row_dasiento.Cells(3).Value
                jackAsientos.haberlocal_tipomov = row_dasiento.Cells(6).Value
                jackAsientos.debelocal_tipomov = row_dasiento.Cells(7).Value
                jackAsientos.aniomes_linea_tipomov = row_dasiento.Cells(8).Value
                If row_dasiento.Cells.Count >= 10 Then
                    jackAsientos.FechaDocumento = row_dasiento.Cells(10).Value
                End If
            End If
            valida_cuenta_tipomov(TextB_cuenta__tipomov.Text.ToString.Trim)
            Button_contabiliza_tipomov.Enabled = False

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("DGV_Detalle_tipomov_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_cuenta__tipomov_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles TextB_cuenta__tipomov.KeyDown
        Try
            If e.KeyValue = Keys.Enter Then
                valida_cuenta_tipomov()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cuenta__tipomov_KeyDown - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_cuenta__tipomov_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_cuenta__tipomov.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                TextB_desc_tipomov.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cuenta__tipomov_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_cuenta__tipomov_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextB_cuenta__tipomov.TextChanged
        Try
            Label_desc_tipomov.Text = jackconsulta.Dame_DescCuenata(TextB_cuenta__tipomov.Text.ToString.Trim)

            If jackvalida.existe_cuenta(TextB_cuenta__tipomov.Text.ToString.Trim) = False Then
                Label_desc_tipomov.Text = " **** La Cuenta No Existe****"
                Button_contabiliza_tipomov.Enabled = False
                TextB_cuenta__tipomov.Focus()

            Else
                If jackvalida.DameTipo_cuenta(TextB_cuenta__tipomov.Text.ToString.Trim) <> 2 Then
                    Label_desc_tipomov.Text += " **** La Cuenta No admite Movimientos****"
                    TextB_cuenta__tipomov.Focus()
                    Button_contabiliza_tipomov.Enabled = True
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cuenta__tipomov_TextChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub valida_cuenta_tipomov(Optional ByVal evalua_cuenta As String = "")
        Dim cuenta As String = String.Empty
        Try
            If evalua_cuenta <> "" Then
                cuenta = evalua_cuenta.ToString.Trim
            Else
                cuenta = TextB_cuenta__tipomov.Text.ToString.Trim
            End If
            If jackvalida.existe_cuenta(cuenta) = False Or jackvalida.DameTipo_cuenta(cuenta) <> 2 Then
                Button_contabiliza_tipomov.Enabled = False
            Else
                Button_contabiliza_tipomov.Enabled = True

                Button_contabiliza_tipomov.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("valida_cuenta_tipomov - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_numedoc_tipomov_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles TextB_numedoc_tipomov.KeyPress
        Try
            If e.KeyChar = Convert.ToChar(Keys.Enter) Then
                TextB_cuenta__tipomov.Focus()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_numedoc_tipomov_KeyPress - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button_contabiliza_tipomov_Click(sender As System.Object, e As System.EventArgs) Handles Button_contabiliza_tipomov.Click

        Try
            contabilizar_linea()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_contabiliza_tipomov_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Protected Sub contabilizar_linea()
        Dim linea As Integer = 0
        Dim reader_existe_asiento As SqlDataReader = Nothing
        Dim ejecuta_resumen As String = String.Empty
        Try
            jackAsientos.anomes = Year(DateF_Fechaasiento_Nuevo.Value) * 100 + (Month(DateF_Fechaasiento_Nuevo.Value))
            jackAsientos.numeasientos = TextB_numeasiento_nuevo.Text

            reader_existe_asiento = jackAsientos.busca_asiento(jackAsientos.numeasiento_tipomov, jackAsientos.aniomes_linea_tipomov)
            If reader_existe_asiento IsNot Nothing Then
                jackAsientos.anomes = jackAsientos.aniomes_linea_tipomov
                jackAsientos.numeasientos = jackAsientos.numeasiento_tipomov
                While reader_existe_asiento.Read
                    jackAsientos.numemov_tipomov = IIf(IsDBNull(reader_existe_asiento!numemov), 0, reader_existe_asiento!numemov)
                    jackAsientos.numemov = jackAsientos.numemov_tipomov
                End While
            End If
            reader_existe_asiento.Close()
            linea = jackAsientos.nueva_linea()
            If linea = 0 Then
                Exit Sub
            End If
            actualiza_linea_tipomov(linea, jackAsientos.anomes)
            ejecuta_resumen = jackconsulta.EjecutaSQL(String.Format("EXEC actualiza_masientos @anomes='{0}',@numemov='{1}',@numeasientos='{2}'", jackAsientos.anomes, jackAsientos.numemov, jackAsientos.numeasientos), True)
            Select Case jackAsientos.tipomov.ToString.Trim
                Case "Facturas"
                    jackconsulta.EjecutaSQL("update detasientosfacturas set iderror=0,cuentacontable='" & TextB_cuenta__tipomov.Text.Trim & "' where idasiento=" & jackAsientos.idasiento & " and numeroasiento='" & jackAsientos.numeasientos & "' and aniomes=" & jackAsientos.anomes & " and idlinea=" & jackAsientos.nlinea_tipomov, True)
                Case "Recibos"
                    jackconsulta.EjecutaSQL("update DetAsientosRecibos set iderror=0,cuentacontable='" & TextB_cuenta__tipomov.Text.Trim & "' where idasiento=" & jackAsientos.idasiento & " and numeroasiento='" & jackAsientos.numeasientos & "' and aniomes=" & jackAsientos.anomes & " and idlinea=" & jackAsientos.nlinea_tipomov, True)

            End Select

            limpia_detallelinea_tipomov()
            cargar_asientos_tipomov(jackAsientos.tipomov.ToString.Trim, jackAsientos.idasiento)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("contabilizar_linea - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextB_numeasiento_nuevo_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextB_numeasiento_nuevo.TextChanged

    End Sub
    Protected Sub limpia_detallelinea_tipomov()
        Try
            TextB_cuenta__tipomov.Text = ""
            TextB_desc_tipomov.Text = ""
            TextB_nlinea_tipomov.Text = ""
            TextB_numedoc_tipomov.Text = ""
            Label_desc_tipomov.Text = "?"

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("limpia_detallelinea_tipomov - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button_buscardocumento_xintegrar_Click(sender As System.Object, e As System.EventArgs) Handles Button_buscardocumento_xintegrar.Click
        Dim datos_cantidad As DataTable = Nothing
        Dim sql_facturas As String = String.Empty
        Dim sql_recibos As String = String.Empty
        Try
            datos_cantidad = New DataTable
            If jackAsientos.tipomov = Nothing Then
                MessageBoxEx.Show("Favor seleccione primero el tipo movimiento ", "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                Select Case jackAsientos.tipomov.ToString.Trim
                    Case "Facturas"
                        sql_facturas = String.Empty
                        sql_facturas = "select catErrorProcesoAsiento.descripcion Error,numeroasiento 'N�mero Asiento',idlinea 'L�nea',descripcionlinea 'Descripci�n l�nea',cuentacontable 'Cuenta Contable' ,"
                        sql_facturas += "numerodocumento 'Documento',montohaber 'Cr�dito',montodeber 'D�bito' ,aniomes 'A�o-Mes' from detasientosfacturas inner join catErrorProcesoAsiento on detasientosfacturas.iderror= catErrorProcesoAsiento.iderror"
                        sql_facturas += " where numerodocumento ='" & TextB_numedoc_xintegrar.Text.Trim & "' And detasientosfacturas.iderror > 3 order by numerodocumento"
                        datos_cantidad = jackconsulta.cargaGrid(sql_facturas, True)
                    Case "Recibos"
                        sql_recibos = String.Empty
                        sql_recibos = "select catErrorProcesoAsiento.descripcion Error,numeroasiento 'N�mero Asiento',idlinea 'L�nea',descripcionlinea 'Descripci�n l�nea',cuentacontable 'Cuenta Contable' ,"
                        sql_recibos += "numerodocumento 'Documento',montohaber 'Cr�dito',montodeber 'D�bito',aniomes 'A�o-Mes' from DetAsientosRecibos inner join catErrorProcesoAsiento on DetAsientosRecibos.iderror= catErrorProcesoAsiento.iderror"
                        sql_recibos += " where numerodocumento ='" & TextB_numedoc_xintegrar.Text.Trim & "' And DetAsientosRecibos.iderror > 3 order by numerodocumento"
                        datos_cantidad = jackconsulta.cargaGrid(sql_recibos, True)
                End Select
                If datos_cantidad.Rows.Count > 0 Then
                    ExpandablePanel5.Visible = True
                    DGV_Detalle_tipomov.Show()
                    DGV_Detalle_tipomov.DataSource = datos_cantidad
                    DGV_Detalle_tipomov.Columns(7).DefaultCellStyle.Format = "##,##0.00"
                    DGV_Detalle_tipomov.Columns(6).DefaultCellStyle.Format = "##,##0.00"
                Else
                    DGV_Detalle_tipomov.DataSource = Nothing
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_buscardocumento_xintegrar_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub resultado_busca_documento_xintegrar(Optional ByVal tipomov As String = "")

    End Sub

    Private Sub Button_anulalinea_Xintegrar_Click(sender As System.Object, e As System.EventArgs) Handles Button_anulalinea_Xintegrar.Click
        Try
            Select Case jackAsientos.tipomov.ToString.Trim
                Case "Facturas"
                    jackconsulta.EjecutaSQL("update detasientosfacturas set iderror=6,cuentacontable='" & TextB_cuenta__tipomov.Text.Trim & "' where idasiento=" & jackAsientos.idasiento & " and numeroasiento='" & jackAsientos.numeasiento_tipomov & "' and aniomes=" & jackAsientos.aniomes_linea_tipomov & " and idlinea=" & jackAsientos.nlinea_tipomov, True)
                Case "Recibos"
                    jackconsulta.EjecutaSQL("update DetAsientosRecibos set iderror=6,cuentacontable='" & TextB_cuenta__tipomov.Text.Trim & "' where idasiento=" & jackAsientos.idasiento & " and numeroasiento='" & jackAsientos.numeasiento_tipomov & "' and aniomes=" & jackAsientos.aniomes_linea_tipomov & " and idlinea=" & jackAsientos.nlinea_tipomov, True)

            End Select
            limpia_detallelinea_tipomov()
            cargar_asientos_tipomov(jackAsientos.tipomov.ToString.Trim, jackAsientos.idasiento)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_anulalinea_Xintegrar_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub DGV_Detalle_tipomov_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGV_Detalle_tipomov.CellContentClick

    End Sub

    Private Sub TextB_cdgocuenta_buscar_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextB_cdgocuenta_buscar.TextChanged
        Try
            Label_desccuenta_buscar.Text = jackconsulta.Dame_DescCuenata(TextB_cdgocuenta_buscar.Text.ToString.Trim)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("TextB_cdgocuenta_buscar_TextChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ComboB_documentos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboB_documentos.SelectedIndexChanged
        Try
            If ComboB_documentos.SelectedValue.ToString <> String.Empty And ComboB_documentos.SelectedValue.ToString <> "-1" And ComboB_documentos.SelectedIndex > 0 Then
                carga_detalle_Asiento(TextB_numeasiento_nuevo.Text.Trim, TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, numemov_select, ComboB_documentos.SelectedValue)
                REM 
            Else

                carga_detalle_Asiento(TextB_numeasiento_nuevo.Text, TextB_anonuevo.Text * 100 + TextB_mesnuevo.Text, numemov_select)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ComboB_documentos_SelectedIndexChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ComboB_tipomov_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboB_tipomov.SelectedIndexChanged
        Try
            Button_buscardocumento_xintegrar.Enabled = True

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ComboB_tipomov_SelectedIndexChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class