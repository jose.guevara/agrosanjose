Imports System
Imports System.Collections.Generic
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.Common

Public Class actrptRecibosCaja
    Inherits DataDynamics.ActiveReports.ActiveReport
    Private Shared sSentenciaSql As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private drRecibos As IDataReader = Nothing
    Friend WithEvents lblNoDeposito As DataDynamics.ActiveReports.Label
    Friend WithEvents Efectivo As DataDynamics.ActiveReports.Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label43 As Label
    Friend WithEvents Label44 As Label
    Friend WithEvents Label45 As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label55 As Label
    Friend WithEvents Label56 As Label
    Friend WithEvents lblReciboMoneda As Label
    Friend WithEvents lblTasaCambio As Label
    Friend WithEvents Label58 As Label
    Private dtRecibos As DataTable = Nothing
    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub
#Region "ActiveReports Designer generated code"
    Private WithEvents PageHeader As DataDynamics.ActiveReports.PageHeader = Nothing
    Private WithEvents Detail As DataDynamics.ActiveReports.Detail = Nothing
    Private WithEvents PageFooter As DataDynamics.ActiveReports.PageFooter = Nothing
    Private Label As DataDynamics.ActiveReports.Label = Nothing
    Private Label1 As DataDynamics.ActiveReports.Label = Nothing
    Private Label3 As DataDynamics.ActiveReports.Label = Nothing
    Private Label4 As DataDynamics.ActiveReports.Label = Nothing
    Private Label5 As DataDynamics.ActiveReports.Label = Nothing
    Private Label6 As DataDynamics.ActiveReports.Label = Nothing
    Private Label7 As DataDynamics.ActiveReports.Label = Nothing
    Private Label8 As DataDynamics.ActiveReports.Label = Nothing
    Private Label49 As DataDynamics.ActiveReports.Label = Nothing
    Private Label41 As DataDynamics.ActiveReports.Label = Nothing
    Private Label9 As DataDynamics.ActiveReports.Label = Nothing
    Private Label10 As DataDynamics.ActiveReports.Label = Nothing
    Private Label11 As DataDynamics.ActiveReports.Label = Nothing
    Private Label12 As DataDynamics.ActiveReports.Label = Nothing
    Private Label13 As DataDynamics.ActiveReports.Label = Nothing
    Private Label14 As DataDynamics.ActiveReports.Label = Nothing
    Private Label15 As DataDynamics.ActiveReports.Label = Nothing
    Private Label16 As DataDynamics.ActiveReports.Label = Nothing
    Private Label17 As DataDynamics.ActiveReports.Label = Nothing
    Private Label18 As DataDynamics.ActiveReports.Label = Nothing
    Private Label19 As DataDynamics.ActiveReports.Label = Nothing
    Private Label20 As DataDynamics.ActiveReports.Label = Nothing
    Private Label21 As DataDynamics.ActiveReports.Label = Nothing
    Private Label22 As DataDynamics.ActiveReports.Label = Nothing
    Private Label23 As DataDynamics.ActiveReports.Label = Nothing
    Private Label24 As DataDynamics.ActiveReports.Label = Nothing
    Private Label25 As DataDynamics.ActiveReports.Label = Nothing
    Private Label26 As DataDynamics.ActiveReports.Label = Nothing
    Private Label27 As DataDynamics.ActiveReports.Label = Nothing
    Private Label28 As DataDynamics.ActiveReports.Label = Nothing
    Private Label29 As DataDynamics.ActiveReports.Label = Nothing
    Private Label30 As DataDynamics.ActiveReports.Label = Nothing
    Private Label31 As DataDynamics.ActiveReports.Label = Nothing
    Private Label32 As DataDynamics.ActiveReports.Label = Nothing
    Private Label33 As DataDynamics.ActiveReports.Label = Nothing
    Private Label34 As DataDynamics.ActiveReports.Label = Nothing
    Private Label35 As DataDynamics.ActiveReports.Label = Nothing
    Private Label36 As DataDynamics.ActiveReports.Label = Nothing
    Private Label37 As DataDynamics.ActiveReports.Label = Nothing
    Private Label38 As DataDynamics.ActiveReports.Label = Nothing
    Private Label39 As DataDynamics.ActiveReports.Label = Nothing
    Private Label40 As DataDynamics.ActiveReports.Label = Nothing
    Private Label50 As DataDynamics.ActiveReports.Label = Nothing
    Private Label51 As DataDynamics.ActiveReports.Label = Nothing
    Private Label52 As DataDynamics.ActiveReports.Label = Nothing
    Private Label53 As DataDynamics.ActiveReports.Label = Nothing
    Private Label54 As DataDynamics.ActiveReports.Label = Nothing
    Friend WithEvents txtAnio As DataDynamics.ActiveReports.TextBox
    Private Label42 As DataDynamics.ActiveReports.Label = Nothing
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actrptRecibosCaja))
        Me.Detail = New DataDynamics.ActiveReports.Detail()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.Label14 = New DataDynamics.ActiveReports.Label()
        Me.Label15 = New DataDynamics.ActiveReports.Label()
        Me.Label16 = New DataDynamics.ActiveReports.Label()
        Me.Label17 = New DataDynamics.ActiveReports.Label()
        Me.Label18 = New DataDynamics.ActiveReports.Label()
        Me.Label19 = New DataDynamics.ActiveReports.Label()
        Me.Label20 = New DataDynamics.ActiveReports.Label()
        Me.Label21 = New DataDynamics.ActiveReports.Label()
        Me.Label22 = New DataDynamics.ActiveReports.Label()
        Me.Label23 = New DataDynamics.ActiveReports.Label()
        Me.Label24 = New DataDynamics.ActiveReports.Label()
        Me.Label25 = New DataDynamics.ActiveReports.Label()
        Me.Label26 = New DataDynamics.ActiveReports.Label()
        Me.Label27 = New DataDynamics.ActiveReports.Label()
        Me.Label28 = New DataDynamics.ActiveReports.Label()
        Me.Label29 = New DataDynamics.ActiveReports.Label()
        Me.Label30 = New DataDynamics.ActiveReports.Label()
        Me.Label31 = New DataDynamics.ActiveReports.Label()
        Me.Label32 = New DataDynamics.ActiveReports.Label()
        Me.Label33 = New DataDynamics.ActiveReports.Label()
        Me.Label34 = New DataDynamics.ActiveReports.Label()
        Me.Label35 = New DataDynamics.ActiveReports.Label()
        Me.Label36 = New DataDynamics.ActiveReports.Label()
        Me.Label37 = New DataDynamics.ActiveReports.Label()
        Me.Label38 = New DataDynamics.ActiveReports.Label()
        Me.Label39 = New DataDynamics.ActiveReports.Label()
        Me.Label40 = New DataDynamics.ActiveReports.Label()
        Me.Label50 = New DataDynamics.ActiveReports.Label()
        Me.Label51 = New DataDynamics.ActiveReports.Label()
        Me.Label52 = New DataDynamics.ActiveReports.Label()
        Me.Label53 = New DataDynamics.ActiveReports.Label()
        Me.Label42 = New DataDynamics.ActiveReports.Label()
        Me.Label41 = New DataDynamics.ActiveReports.Label()
        Me.lblNoDeposito = New DataDynamics.ActiveReports.Label()
        Me.Efectivo = New DataDynamics.ActiveReports.Label()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label43 = New DataDynamics.ActiveReports.Label()
        Me.Label44 = New DataDynamics.ActiveReports.Label()
        Me.Label45 = New DataDynamics.ActiveReports.Label()
        Me.Label46 = New DataDynamics.ActiveReports.Label()
        Me.Label47 = New DataDynamics.ActiveReports.Label()
        Me.Label48 = New DataDynamics.ActiveReports.Label()
        Me.Label55 = New DataDynamics.ActiveReports.Label()
        Me.Label54 = New DataDynamics.ActiveReports.Label()
        Me.PageHeader = New DataDynamics.ActiveReports.PageHeader()
        Me.Label = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.Label49 = New DataDynamics.ActiveReports.Label()
        Me.txtAnio = New DataDynamics.ActiveReports.TextBox()
        Me.PageFooter = New DataDynamics.ActiveReports.PageFooter()
        Me.Label56 = New DataDynamics.ActiveReports.Label()
        Me.lblReciboMoneda = New DataDynamics.ActiveReports.Label()
        Me.lblTasaCambio = New DataDynamics.ActiveReports.Label()
        Me.Label58 = New DataDynamics.ActiveReports.Label()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNoDeposito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Efectivo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAnio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblReciboMoneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTasaCambio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.ColumnSpacing = 0!
        Me.Detail.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label9, Me.Label10, Me.Label11, Me.Label12, Me.Label13, Me.Label14, Me.Label15, Me.Label16, Me.Label17, Me.Label18, Me.Label19, Me.Label20, Me.Label21, Me.Label22, Me.Label23, Me.Label24, Me.Label25, Me.Label26, Me.Label27, Me.Label28, Me.Label29, Me.Label30, Me.Label31, Me.Label32, Me.Label33, Me.Label34, Me.Label35, Me.Label36, Me.Label37, Me.Label38, Me.Label39, Me.Label40, Me.Label50, Me.Label51, Me.Label52, Me.Label53, Me.Label42, Me.Label41, Me.lblNoDeposito, Me.Efectivo, Me.Label2, Me.Label43, Me.Label44, Me.Label45, Me.Label46, Me.Label47, Me.Label48, Me.Label55})
        Me.Detail.Height = 2.677083!
        Me.Detail.Name = "Detail"
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0.8125!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "ddo-char-set: 1; text-align: left; font-size: 11pt"
        Me.Label9.Text = ""
        Me.Label9.Top = 0!
        Me.Label9.Width = 2.8125!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 3.6875!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "text-align: right; font-size: 11pt"
        Me.Label10.Text = ""
        Me.Label10.Top = 0!
        Me.Label10.Width = 0.8125!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 4.5!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "text-align: right; font-size: 11pt"
        Me.Label11.Text = ""
        Me.Label11.Top = 0!
        Me.Label11.Width = 0.6875!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 5.1875!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "text-align: right"
        Me.Label12.Text = ""
        Me.Label12.Top = 0!
        Me.Label12.Width = 0.8125!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0.8125!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "text-align: left; font-size: 11pt"
        Me.Label13.Text = ""
        Me.Label13.Top = 0.1875!
        Me.Label13.Width = 2.8125!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 3.6875!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: right; font-size: 11pt"
        Me.Label14.Text = ""
        Me.Label14.Top = 0.1875!
        Me.Label14.Width = 0.8125!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 4.5!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "text-align: right; font-size: 11pt"
        Me.Label15.Text = ""
        Me.Label15.Top = 0.1875!
        Me.Label15.Width = 0.6875!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 5.1875!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "text-align: right"
        Me.Label16.Text = ""
        Me.Label16.Top = 0.1875!
        Me.Label16.Width = 0.8125!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 0.8125!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: left; font-size: 11pt"
        Me.Label17.Text = ""
        Me.Label17.Top = 0.375!
        Me.Label17.Width = 2.8125!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 3.6875!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "text-align: right; font-size: 11pt"
        Me.Label18.Text = ""
        Me.Label18.Top = 0.375!
        Me.Label18.Width = 0.8125!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 4.5!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "text-align: right; font-size: 11pt"
        Me.Label19.Text = ""
        Me.Label19.Top = 0.375!
        Me.Label19.Width = 0.6875!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 5.1875!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "text-align: right"
        Me.Label20.Text = ""
        Me.Label20.Top = 0.375!
        Me.Label20.Width = 0.8125!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.8125!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "text-align: left; font-size: 11pt"
        Me.Label21.Text = ""
        Me.Label21.Top = 0.5625!
        Me.Label21.Width = 2.8125!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 3.6875!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "text-align: right; font-size: 11pt"
        Me.Label22.Text = ""
        Me.Label22.Top = 0.5625!
        Me.Label22.Width = 0.8125!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 4.5!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "text-align: right; font-size: 11pt"
        Me.Label23.Text = ""
        Me.Label23.Top = 0.5625!
        Me.Label23.Width = 0.6875!
        '
        'Label24
        '
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 5.1875!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "text-align: right"
        Me.Label24.Text = ""
        Me.Label24.Top = 0.5625!
        Me.Label24.Width = 0.8125!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0.8125!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "text-align: left; font-size: 11pt"
        Me.Label25.Text = ""
        Me.Label25.Top = 0.75!
        Me.Label25.Width = 2.8125!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.6875!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "text-align: right; font-size: 11pt"
        Me.Label26.Text = ""
        Me.Label26.Top = 0.75!
        Me.Label26.Width = 0.8125!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 4.5!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "text-align: right; font-size: 11pt"
        Me.Label27.Text = ""
        Me.Label27.Top = 0.75!
        Me.Label27.Width = 0.6875!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 5.1875!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "text-align: right"
        Me.Label28.Text = ""
        Me.Label28.Top = 0.75!
        Me.Label28.Width = 0.8125!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.8125!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "text-align: left; font-size: 11pt"
        Me.Label29.Text = ""
        Me.Label29.Top = 0.9375!
        Me.Label29.Width = 2.8125!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 3.6875!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "text-align: right; font-size: 11pt"
        Me.Label30.Text = ""
        Me.Label30.Top = 0.9375!
        Me.Label30.Width = 0.8125!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 4.5!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "text-align: right; font-size: 11pt"
        Me.Label31.Text = ""
        Me.Label31.Top = 0.9375!
        Me.Label31.Width = 0.6875!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 5.1875!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "text-align: right"
        Me.Label32.Text = ""
        Me.Label32.Top = 0.9375!
        Me.Label32.Width = 0.8125!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0.8125!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "text-align: left; font-size: 11pt"
        Me.Label33.Text = ""
        Me.Label33.Top = 1.125!
        Me.Label33.Width = 2.8125!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 3.6875!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "text-align: right; font-size: 11pt"
        Me.Label34.Text = ""
        Me.Label34.Top = 1.125!
        Me.Label34.Width = 0.8125!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 4.5!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "text-align: right; font-size: 11pt"
        Me.Label35.Text = ""
        Me.Label35.Top = 1.125!
        Me.Label35.Width = 0.6875!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 5.1875!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "text-align: right"
        Me.Label36.Text = ""
        Me.Label36.Top = 1.125!
        Me.Label36.Width = 0.8125!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0.8125!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "text-align: left; font-size: 11pt"
        Me.Label37.Text = ""
        Me.Label37.Top = 1.3125!
        Me.Label37.Width = 2.8125!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 3.6875!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "text-align: right; font-size: 11pt"
        Me.Label38.Text = ""
        Me.Label38.Top = 1.3125!
        Me.Label38.Width = 0.8125!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 4.5!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "text-align: right; font-size: 11pt"
        Me.Label39.Text = ""
        Me.Label39.Top = 1.3125!
        Me.Label39.Width = 0.6875!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 5.1875!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "text-align: right"
        Me.Label40.Text = ""
        Me.Label40.Top = 1.3125!
        Me.Label40.Width = 0.8125!
        '
        'Label50
        '
        Me.Label50.Height = 0.188!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 2.625!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "text-align: right"
        Me.Label50.Text = ""
        Me.Label50.Top = 2.145!
        Me.Label50.Width = 1.187!
        '
        'Label51
        '
        Me.Label51.Height = 0.1880002!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 2.625!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "text-align: center"
        Me.Label51.Text = ""
        Me.Label51.Top = 1.947!
        Me.Label51.Width = 1.187!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 0.187!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "text-align: right"
        Me.Label52.Text = "Monto"
        Me.Label52.Top = 2.355!
        Me.Label52.Width = 1.0!
        '
        'Label53
        '
        Me.Label53.Height = 0.1875!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 0.187!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "text-align: right"
        Me.Label53.Text = "Monto"
        Me.Label53.Top = 1.7925!
        Me.Label53.Width = 1.0!
        '
        'Label42
        '
        Me.Label42.Height = 0.375!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 4.3!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "text-align: center; font-weight: bold; font-size: 18pt; vertical-align: middle"
        Me.Label42.Text = "ANULADO"
        Me.Label42.Top = 1.812!
        Me.Label42.Visible = False
        Me.Label42.Width = 2.575!
        '
        'Label41
        '
        Me.Label41.Height = 0.188!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 2.625!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "text-align: right"
        Me.Label41.Text = ""
        Me.Label41.Top = 1.521!
        Me.Label41.Width = 1.187!
        '
        'lblNoDeposito
        '
        Me.lblNoDeposito.Height = 0.1880002!
        Me.lblNoDeposito.HyperLink = Nothing
        Me.lblNoDeposito.Left = 2.625!
        Me.lblNoDeposito.Name = "lblNoDeposito"
        Me.lblNoDeposito.Style = "text-align: right"
        Me.lblNoDeposito.Text = ""
        Me.lblNoDeposito.Top = 2.344!
        Me.lblNoDeposito.Width = 1.187!
        '
        'Efectivo
        '
        Me.Efectivo.Height = 0.1880002!
        Me.Efectivo.HyperLink = Nothing
        Me.Efectivo.Left = 2.625!
        Me.Efectivo.Name = "Efectivo"
        Me.Efectivo.Style = "text-align: center"
        Me.Efectivo.Text = ""
        Me.Efectivo.Top = 1.759!
        Me.Efectivo.Width = 1.187!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 6.04!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "text-align: right"
        Me.Label2.Text = ""
        Me.Label2.Top = 0!
        Me.Label2.Width = 0.8125!
        '
        'Label43
        '
        Me.Label43.Height = 0.1875!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 6.04!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "text-align: right"
        Me.Label43.Text = ""
        Me.Label43.Top = 0.1875!
        Me.Label43.Width = 0.8125!
        '
        'Label44
        '
        Me.Label44.Height = 0.1875!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 6.04!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "text-align: right"
        Me.Label44.Text = ""
        Me.Label44.Top = 0.375!
        Me.Label44.Width = 0.8125!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 6.04!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "text-align: right"
        Me.Label45.Text = ""
        Me.Label45.Top = 0.5625!
        Me.Label45.Width = 0.8125!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 6.04!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "text-align: right"
        Me.Label46.Text = ""
        Me.Label46.Top = 0.75!
        Me.Label46.Width = 0.8125!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 6.04!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "text-align: right"
        Me.Label47.Text = ""
        Me.Label47.Top = 0.9375!
        Me.Label47.Width = 0.8125!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 6.04!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "text-align: right"
        Me.Label48.Text = ""
        Me.Label48.Top = 1.125!
        Me.Label48.Width = 0.8125!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 6.04!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "text-align: right"
        Me.Label55.Text = ""
        Me.Label55.Top = 1.3125!
        Me.Label55.Width = 0.8125!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 5.9375!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "text-align: center"
        Me.Label54.Text = "NumeroRecibo"
        Me.Label54.Top = 0.9375!
        Me.Label54.Width = 0.9375!
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label, Me.Label1, Me.Label3, Me.Label4, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label49, Me.Label54, Me.txtAnio, Me.Label56, Me.lblReciboMoneda, Me.lblTasaCambio, Me.Label58})
        Me.PageHeader.Height = 2.270833!
        Me.PageHeader.Name = "PageHeader"
        '
        'Label
        '
        Me.Label.Height = 0.1875!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 2.4375!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: right"
        Me.Label.Text = "Fecha"
        Me.Label.Top = 1.125!
        Me.Label.Width = 1.375!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 1.125!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-align: left"
        Me.Label1.Text = "Nombre"
        Me.Label1.Top = 1.5625!
        Me.Label1.Width = 4.3125!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 5.5!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "text-align: right"
        Me.Label3.Text = "CliCodigo"
        Me.Label3.Top = 1.5625!
        Me.Label3.Width = 1.375!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 1.1875!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: left"
        Me.Label4.Text = "Monto_Letras"
        Me.Label4.Top = 1.8125!
        Me.Label4.Width = 4.3125!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0.6875!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "text-align: right; font-size: 11pt"
        Me.Label5.Text = "Monto"
        Me.Label5.Top = 1.1875!
        Me.Label5.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.8475!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: right"
        Me.Label6.Text = "Facturas"
        Me.Label6.Top = 2.0625!
        Me.Label6.Width = 0.6525002!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 4.5!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "text-align: right"
        Me.Label7.Text = "Intereses"
        Me.Label7.Top = 2.0625!
        Me.Label7.Width = 0.6875!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 5.1875!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "text-align: right"
        Me.Label8.Text = "Mant. Valor"
        Me.Label8.Top = 2.0625!
        Me.Label8.Width = 0.8125!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 6.0625!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "text-align: center"
        Me.Label49.Text = "Moneda F."
        Me.Label49.Top = 2.0625!
        Me.Label49.Width = 0.8125!
        '
        'txtAnio
        '
        Me.txtAnio.Height = 0.1875!
        Me.txtAnio.Left = 4.75!
        Me.txtAnio.Name = "txtAnio"
        Me.txtAnio.Style = "ddo-char-set: 1; font-size: 10pt"
        Me.txtAnio.Text = Nothing
        Me.txtAnio.Top = 1.125!
        Me.txtAnio.Width = 0.6875!
        '
        'PageFooter
        '
        Me.PageFooter.Height = 0.1451389!
        Me.PageFooter.Name = "PageFooter"
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 0.822!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "text-align: center"
        Me.Label56.Text = "Moneda Recibo:"
        Me.Label56.Top = 2.062!
        Me.Label56.Width = 1.043!
        '
        'lblReciboMoneda
        '
        Me.lblReciboMoneda.Height = 0.1875!
        Me.lblReciboMoneda.HyperLink = Nothing
        Me.lblReciboMoneda.Left = 1.917!
        Me.lblReciboMoneda.Name = "lblReciboMoneda"
        Me.lblReciboMoneda.Style = "text-align: left"
        Me.lblReciboMoneda.Text = ""
        Me.lblReciboMoneda.Top = 2.062!
        Me.lblReciboMoneda.Width = 0.855!
        '
        'lblTasaCambio
        '
        Me.lblTasaCambio.Height = 0.1875!
        Me.lblTasaCambio.HyperLink = Nothing
        Me.lblTasaCambio.Left = 3.216!
        Me.lblTasaCambio.Name = "lblTasaCambio"
        Me.lblTasaCambio.Style = "text-align: left"
        Me.lblTasaCambio.Text = ""
        Me.lblTasaCambio.Top = 2.062!
        Me.lblTasaCambio.Width = 0.5350002!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 2.831!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "text-align: center"
        Me.Label58.Text = "T/C"
        Me.Label58.Top = 2.062!
        Me.Label58.Width = 0.3429999!
        '
        'actrptRecibosCaja
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Bottom = 0.2!
        Me.PageSettings.Margins.Left = 0.5!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.Margins.Top = 0.2!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.0!
        Me.Sections.Add(Me.PageHeader)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter)
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNoDeposito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Efectivo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAnio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblReciboMoneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTasaCambio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private _NumeroRecibo As String
    Public Property NumeroRecibo() As String
        Get
            Return (_NumeroRecibo)
        End Get
        Set(ByVal value As String)
            _NumeroRecibo = value
        End Set
    End Property
    Private Sub actrptRecibosCaja_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart
        Try
            'ObtieneInformacionRecibo()
            ObtieneDTInformacionRecibo()
            If Not dtRecibos Is Nothing Then
                If dtRecibos.Rows.Count > 0 Then
                    Label.Text = dtRecibos.Rows(0).Item("fecha")
                    Label1.Text = dtRecibos.Rows(0).Item("NumeroRecibido")
                    Label3.Text = dtRecibos.Rows(0).Item("CodigoCliente")
                    Label4.Text = NumPalabra(dtRecibos.Rows(0).Item("MontoRecibo"))
                    Label5.Text = Format(SUConversiones.ConvierteADouble(dtRecibos.Rows(0).Item("montoRecibo")), "#,##0.#0")
                    'Label49.Text = dtRecibos.Rows(0).Item("cta_contable")
                    Label50.Text = dtRecibos.Rows(0).Item("numchqtarj")
                    Label51.Text = dtRecibos.Rows(0).Item("nombrebanco")
                    lblNoDeposito.Text = dtRecibos.Rows(0).Item("NoDeposito")
                    Label52.Text = Format(SUConversiones.ConvierteADouble(dtRecibos.Rows(0).Item("montoRecibo")), "#,##0.#0")
                    Label53.Text = Format(SUConversiones.ConvierteADouble(dtRecibos.Rows(0).Item("montoRecibo")), "#,##0.#0")
                    Label54.Text = dtRecibos.Rows(0).Item("NumeroRecibo")
                    Label41.Text = dtRecibos.Rows(0).Item("reciboprov")
                    lblTasaCambio.Text = RNTipoCambio.ObtieneTipoCambioDelDia()
                    lblReciboMoneda.Text = dtRecibos.Rows(0).Item("DesMonedaRecibo")
                    If dtRecibos.Rows(0).Item("ANULADO") = "ANULADO" Then
                        Label42.Visible = True
                    End If
                    txtAnio.Text = dtRecibos.Rows(0).Item("Anio")
                End If
            End If
            If Label50.Text <> "" Then
                Label52.Visible = True
                Label53.Visible = True
            Else
                Label52.Visible = True
                Label53.Visible = True
            End If
            Me.Document.Printer.PrinterName = ""
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "actrptRecibosCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error no se puedo generar el reporte correctamente, favor reimprimir", MsgBoxStyle.Critical)
            Exit Sub
        End Try
    End Sub

    Private Sub Detail_Format(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Format

        Try
            If Not dtRecibos Is Nothing Then
                If dtRecibos.Rows.Count > 0 Then
                    Label9.Text = dtRecibos.Rows(0).Item("DescripcionPago")
                    Label10.Text = dtRecibos.Rows(0).Item("MontoAPagarFactura")
                    Label11.Text = dtRecibos.Rows(0).Item("interes")
                    Label12.Text = dtRecibos.Rows(0).Item("mantvlr")
                    Label2.Text = dtRecibos.Rows(0).Item("Moneda")

                    If dtRecibos.Rows.Count >= 2 Then
                        Label13.Text = dtRecibos.Rows(1).Item("DescripcionPago")
                        Label14.Text = dtRecibos.Rows(1).Item("MontoAPagarFactura")
                        Label15.Text = dtRecibos.Rows(1).Item("interes")
                        Label16.Text = dtRecibos.Rows(1).Item("mantvlr")
                        Label43.Text = dtRecibos.Rows(1).Item("Moneda")
                    End If
                    If dtRecibos.Rows.Count >= 3 Then
                        Label17.Text = dtRecibos.Rows(2).Item("DescripcionPago")
                        Label18.Text = dtRecibos.Rows(2).Item("MontoAPagarFactura")
                        Label19.Text = dtRecibos.Rows(2).Item("interes")
                        Label20.Text = dtRecibos.Rows(2).Item("mantvlr")
                        Label44.Text = dtRecibos.Rows(2).Item("Moneda")
                    End If
                    If dtRecibos.Rows.Count >= 4 Then
                        Label21.Text = dtRecibos.Rows(3).Item("DescripcionPago")
                        Label22.Text = dtRecibos.Rows(3).Item("MontoAPagarFactura")
                        Label23.Text = dtRecibos.Rows(3).Item("interes")
                        Label24.Text = dtRecibos.Rows(3).Item("mantvlr")
                        Label45.Text = dtRecibos.Rows(3).Item("Moneda")
                    End If
                    If dtRecibos.Rows.Count >= 5 Then
                        Label25.Text = dtRecibos.Rows(4).Item("DescripcionPago")
                        Label26.Text = dtRecibos.Rows(4).Item("MontoAPagarFactura")
                        Label27.Text = dtRecibos.Rows(4).Item("interes")
                        Label28.Text = dtRecibos.Rows(4).Item("mantvlr")
                        Label46.Text = dtRecibos.Rows(4).Item("Moneda")
                    End If
                    If dtRecibos.Rows.Count >= 6 Then
                        Label29.Text = dtRecibos.Rows(5).Item("DescripcionPago")
                        Label30.Text = dtRecibos.Rows(5).Item("MontoAPagarFactura")
                        Label31.Text = dtRecibos.Rows(5).Item("interes")
                        Label32.Text = dtRecibos.Rows(5).Item("mantvlr")
                        Label47.Text = dtRecibos.Rows(5).Item("Moneda")
                    End If
                    If dtRecibos.Rows.Count >= 7 Then
                        Label33.Text = dtRecibos.Rows(6).Item("DescripcionPago")
                        Label34.Text = dtRecibos.Rows(6).Item("MontoAPagarFactura")
                        Label35.Text = dtRecibos.Rows(6).Item("interes")
                        Label36.Text = dtRecibos.Rows(6).Item("mantvlr")
                        Label48.Text = dtRecibos.Rows(6).Item("Moneda")
                    End If
                    If dtRecibos.Rows.Count >= 8 Then
                        Label37.Text = dtRecibos.Rows(7).Item("DescripcionPago")
                        Label38.Text = dtRecibos.Rows(7).Item("MontoAPagarFactura")
                        Label39.Text = dtRecibos.Rows(7).Item("interes")
                        Label40.Text = dtRecibos.Rows(7).Item("mantvlr")
                        Label49.Text = dtRecibos.Rows(7).Item("Moneda")
                    End If

                Else
                    MsgBox("Error no se puedo generar el reporte correctamente, favor reimprimir", MsgBoxStyle.Critical)
                    Exit Sub
                End If
            Else
                MsgBox("Error no se puedo generar el reporte correctamente, favor reimprimir", MsgBoxStyle.Critical)
                Exit Sub
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "actrptRecibosCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error no se puedo generar el reporte correctamente, favor reimprimir. " & ex.Message, MsgBoxStyle.Critical)
            Exit Sub
        End Try

    End Sub

    Private Sub actrptRecibosCaja_ReportEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Try
            If intRptImpRecibos = 1 Then
                Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
                Me.PageSettings.PaperHeight = 11.0F
                Me.PageSettings.PaperWidth = 7.0F
                Me.Document.Print(True, False)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "actrptRecibosCaja"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error no se puedo generar el reporte correctamente, favor reimprimir. " & ex.Message, MsgBoxStyle.Critical)
            Exit Sub
        End Try


    End Sub

    Private Sub ObtieneInformacionRecibo()
        Try
            drRecibos = Nothing
            drRecibos = RNRecibos.ObtenerInformacionRecibo(NumeroRecibo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNRecibos"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub
    Private Sub ObtieneDTInformacionRecibo()
        Try
            dtRecibos = RNRecibos.ObtenerDTInformacionRecibo(NumeroRecibo)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "RNRecibos"
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
        End Try
    End Sub
End Class
