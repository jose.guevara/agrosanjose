Imports System.Data.SqlClient
Imports System.Text
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv
Imports System.Collections.Generic

Public Class frmCompras
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DPkFechaFactura As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNumeroCompra As System.Windows.Forms.TextBox
    Friend WithEvents txtDias As System.Windows.Forms.TextBox
    Friend WithEvents txtVence As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreProveedor As System.Windows.Forms.TextBox
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    'Friend WithEvents medPrecio As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroProveedor As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartamento As System.Windows.Forms.TextBox
    Friend WithEvents txtNegocio As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents txtProveedor As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdVendedor As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdClientes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProductos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdRefrescar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents CmdAnular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImprimir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem5 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents dgvxProductosCompras As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents chkBonificacion As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbAgencias As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbImpuesto As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents cbiNo As DevComponents.Editors.ComboItem
    Friend WithEvents cbiSi As DevComponents.Editors.ComboItem
    Friend WithEvents ddlRetencion As System.Windows.Forms.ComboBox
    Friend WithEvents lblRetencion As System.Windows.Forms.Label
    Friend WithEvents cbPedidoCompra As ComboBoxEx
    Friend WithEvents EsBonificacion As Infragistics.Win.UltraDataGridView.UltraCheckEditorColumn
    Friend WithEvents Cantidad As DataGridViewDoubleInputColumn
    Friend WithEvents Unidad As DataGridViewTextBoxColumn
    Friend WithEvents Codigo As DataGridViewTextBoxColumn
    Friend WithEvents Producto As DataGridViewTextBoxColumn
    Friend WithEvents Impuesto As DataGridViewTextBoxColumn
    Friend WithEvents Precio As DataGridViewDoubleInputColumn
    Friend WithEvents Valor As DataGridViewDoubleInputColumn
    Friend WithEvents IdProducto As DataGridViewTextBoxColumn
    Friend WithEvents medPrecio As DevExpress.XtraEditors.SpinEdit
    Friend WithEvents Label20 As Label
    Friend WithEvents txtPrecioActualCosto As TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCompras))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtPrecioActualCosto = New System.Windows.Forms.TextBox()
        Me.medPrecio = New DevExpress.XtraEditors.SpinEdit()
        Me.cbPedidoCompra = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ddlRetencion = New System.Windows.Forms.ComboBox()
        Me.lblRetencion = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmbImpuesto = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.cbiNo = New DevComponents.Editors.ComboItem()
        Me.cbiSi = New DevComponents.Editors.ComboItem()
        Me.cmbAgencias = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkBonificacion = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtProveedor = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.txtNegocio = New System.Windows.Forms.TextBox()
        Me.txtDepartamento = New System.Windows.Forms.TextBox()
        Me.txtNumeroProveedor = New System.Windows.Forms.TextBox()
        Me.txtNumeroVendedor = New System.Windows.Forms.TextBox()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.txtNombreProveedor = New System.Windows.Forms.TextBox()
        Me.txtVence = New System.Windows.Forms.TextBox()
        Me.txtDias = New System.Windows.Forms.TextBox()
        Me.txtNumeroCompra = New System.Windows.Forms.TextBox()
        Me.DPkFechaFactura = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvxProductosCompras = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.EsBonificacion = New Infragistics.Win.UltraDataGridView.UltraCheckEditorColumn(Me.components)
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Unidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Producto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IdProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.cmdVendedor = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdClientes = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdProductos = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdRefrescar = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.CmdAnular = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdImprimir = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem5 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox1.SuspendLayout()
        CType(Me.medPrecio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvxProductosCompras, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12, Me.MenuItem4})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "&Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "L&impiar"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem12.Text = "&Listado"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Vendedores"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Proveedores"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Productos"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Refrescar"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 4
        Me.MenuItem4.Text = "&Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtPrecioActualCosto)
        Me.GroupBox1.Controls.Add(Me.medPrecio)
        Me.GroupBox1.Controls.Add(Me.cbPedidoCompra)
        Me.GroupBox1.Controls.Add(Me.ddlRetencion)
        Me.GroupBox1.Controls.Add(Me.lblRetencion)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.cmbImpuesto)
        Me.GroupBox1.Controls.Add(Me.cmbAgencias)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.chkBonificacion)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtProveedor)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.txtNegocio)
        Me.GroupBox1.Controls.Add(Me.txtDepartamento)
        Me.GroupBox1.Controls.Add(Me.txtNumeroProveedor)
        Me.GroupBox1.Controls.Add(Me.txtNumeroVendedor)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.txtNombreProveedor)
        Me.GroupBox1.Controls.Add(Me.txtVence)
        Me.GroupBox1.Controls.Add(Me.txtDias)
        Me.GroupBox1.Controls.Add(Me.txtNumeroCompra)
        Me.GroupBox1.Controls.Add(Me.DPkFechaFactura)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1019, 151)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(173, 126)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(128, 13)
        Me.Label20.TabIndex = 62
        Me.Label20.Text = "Ultimo Precio Compra"
        '
        'txtPrecioActualCosto
        '
        Me.txtPrecioActualCosto.Enabled = False
        Me.txtPrecioActualCosto.Location = New System.Drawing.Point(307, 123)
        Me.txtPrecioActualCosto.Name = "txtPrecioActualCosto"
        Me.txtPrecioActualCosto.Size = New System.Drawing.Size(206, 20)
        Me.txtPrecioActualCosto.TabIndex = 61
        Me.txtPrecioActualCosto.Tag = "Valor del Precio"
        Me.txtPrecioActualCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'medPrecio
        '
        Me.medPrecio.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.medPrecio.Location = New System.Drawing.Point(70, 123)
        Me.medPrecio.Name = "medPrecio"
        Me.medPrecio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.medPrecio.Properties.DisplayFormat.FormatString = "N4"
        Me.medPrecio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.medPrecio.Properties.Mask.EditMask = "n4"
        Me.medPrecio.Size = New System.Drawing.Size(100, 20)
        Me.medPrecio.TabIndex = 60
        '
        'cbPedidoCompra
        '
        Me.cbPedidoCompra.DisplayMember = "Text"
        Me.cbPedidoCompra.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbPedidoCompra.FormattingEnabled = True
        Me.cbPedidoCompra.ItemHeight = 14
        Me.cbPedidoCompra.Location = New System.Drawing.Point(752, 17)
        Me.cbPedidoCompra.Name = "cbPedidoCompra"
        Me.cbPedidoCompra.Size = New System.Drawing.Size(254, 20)
        Me.cbPedidoCompra.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.cbPedidoCompra.TabIndex = 59
        Me.cbPedidoCompra.WatermarkColor = System.Drawing.Color.Black
        Me.cbPedidoCompra.WatermarkFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPedidoCompra.WatermarkText = "Numero de Pedidos Compra"
        '
        'ddlRetencion
        '
        Me.ddlRetencion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlRetencion.Items.AddRange(New Object() {"No", "Si"})
        Me.ddlRetencion.Location = New System.Drawing.Point(432, 50)
        Me.ddlRetencion.Name = "ddlRetencion"
        Me.ddlRetencion.Size = New System.Drawing.Size(48, 21)
        Me.ddlRetencion.TabIndex = 5
        Me.ddlRetencion.Tag = "Tiene Retenci�n de Impuesto"
        '
        'lblRetencion
        '
        Me.lblRetencion.AutoSize = True
        Me.lblRetencion.Location = New System.Drawing.Point(363, 53)
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Size = New System.Drawing.Size(65, 13)
        Me.lblRetencion.TabIndex = 56
        Me.lblRetencion.Text = "Retenci�n"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(363, 91)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 13)
        Me.Label10.TabIndex = 55
        Me.Label10.Text = "Impuesto"
        '
        'cmbImpuesto
        '
        Me.cmbImpuesto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbImpuesto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbImpuesto.DisplayMember = "Text"
        Me.cmbImpuesto.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbImpuesto.FormattingEnabled = True
        Me.cmbImpuesto.ItemHeight = 14
        Me.cmbImpuesto.Items.AddRange(New Object() {Me.cbiNo, Me.cbiSi})
        Me.cmbImpuesto.Location = New System.Drawing.Point(432, 88)
        Me.cmbImpuesto.Name = "cmbImpuesto"
        Me.cmbImpuesto.Size = New System.Drawing.Size(123, 20)
        Me.cmbImpuesto.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbImpuesto.TabIndex = 9
        '
        'cbiNo
        '
        Me.cbiNo.Text = "NO"
        '
        'cbiSi
        '
        Me.cbiSi.Text = "SI"
        '
        'cmbAgencias
        '
        Me.cmbAgencias.DisplayMember = "Text"
        Me.cmbAgencias.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cmbAgencias.FormattingEnabled = True
        Me.cmbAgencias.ItemHeight = 14
        Me.cmbAgencias.Location = New System.Drawing.Point(70, 17)
        Me.cmbAgencias.Name = "cmbAgencias"
        Me.cmbAgencias.Size = New System.Drawing.Size(290, 20)
        Me.cmbAgencias.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmbAgencias.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(5, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Agencias"
        '
        'chkBonificacion
        '
        '
        '
        '
        Me.chkBonificacion.BackgroundStyle.Class = ""
        Me.chkBonificacion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkBonificacion.Location = New System.Drawing.Point(593, 88)
        Me.chkBonificacion.Name = "chkBonificacion"
        Me.chkBonificacion.Size = New System.Drawing.Size(84, 20)
        Me.chkBonificacion.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.chkBonificacion.TabIndex = 10
        Me.chkBonificacion.Text = "Bonificacion"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(5, 124)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Precio"
        '
        'txtProveedor
        '
        Me.txtProveedor.Location = New System.Drawing.Point(432, 17)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(64, 20)
        Me.txtProveedor.TabIndex = 1
        Me.txtProveedor.Tag = "Proveedor"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(852, 123)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(24, 20)
        Me.TextBox17.TabIndex = 25
        Me.TextBox17.TabStop = False
        Me.TextBox17.Text = "0"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox17.Visible = False
        '
        'txtNegocio
        '
        Me.txtNegocio.Location = New System.Drawing.Point(806, 121)
        Me.txtNegocio.Name = "txtNegocio"
        Me.txtNegocio.Size = New System.Drawing.Size(40, 20)
        Me.txtNegocio.TabIndex = 24
        Me.txtNegocio.TabStop = False
        Me.txtNegocio.Tag = ""
        Me.txtNegocio.Text = "Negocio"
        Me.txtNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNegocio.Visible = False
        '
        'txtDepartamento
        '
        Me.txtDepartamento.Location = New System.Drawing.Point(806, 97)
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.Size = New System.Drawing.Size(40, 20)
        Me.txtDepartamento.TabIndex = 23
        Me.txtDepartamento.TabStop = False
        Me.txtDepartamento.Tag = ""
        Me.txtDepartamento.Text = "Departamento"
        Me.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDepartamento.Visible = False
        '
        'txtNumeroProveedor
        '
        Me.txtNumeroProveedor.Location = New System.Drawing.Point(806, 73)
        Me.txtNumeroProveedor.Name = "txtNumeroProveedor"
        Me.txtNumeroProveedor.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroProveedor.TabIndex = 22
        Me.txtNumeroProveedor.TabStop = False
        Me.txtNumeroProveedor.Tag = ""
        Me.txtNumeroProveedor.Text = "Proveedor"
        Me.txtNumeroProveedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroProveedor.Visible = False
        '
        'txtNumeroVendedor
        '
        Me.txtNumeroVendedor.Location = New System.Drawing.Point(806, 49)
        Me.txtNumeroVendedor.Name = "txtNumeroVendedor"
        Me.txtNumeroVendedor.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroVendedor.TabIndex = 21
        Me.txtNumeroVendedor.TabStop = False
        Me.txtNumeroVendedor.Tag = ""
        Me.txtNumeroVendedor.Text = "Vendedor"
        Me.txtNumeroVendedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroVendedor.Visible = False
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(69, 88)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(104, 20)
        Me.txtCantidad.TabIndex = 7
        Me.txtCantidad.Tag = "Cantidad a Comprar"
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProducto
        '
        Me.txtProducto.Location = New System.Drawing.Point(256, 88)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(104, 20)
        Me.txtProducto.TabIndex = 8
        Me.txtProducto.Tag = "C�digo del Producto"
        '
        'txtNombreProveedor
        '
        Me.txtNombreProveedor.Location = New System.Drawing.Point(497, 17)
        Me.txtNombreProveedor.Name = "txtNombreProveedor"
        Me.txtNombreProveedor.ReadOnly = True
        Me.txtNombreProveedor.Size = New System.Drawing.Size(249, 20)
        Me.txtNombreProveedor.TabIndex = 2
        Me.txtNombreProveedor.Tag = ""
        '
        'txtVence
        '
        Me.txtVence.Location = New System.Drawing.Point(802, 50)
        Me.txtVence.Name = "txtVence"
        Me.txtVence.ReadOnly = True
        Me.txtVence.Size = New System.Drawing.Size(106, 20)
        Me.txtVence.TabIndex = 6
        Me.txtVence.Tag = "Fecha de Vencimiento"
        '
        'txtDias
        '
        Me.txtDias.Location = New System.Drawing.Point(622, 48)
        Me.txtDias.Name = "txtDias"
        Me.txtDias.Size = New System.Drawing.Size(64, 20)
        Me.txtDias.TabIndex = 6
        Me.txtDias.Tag = "D�as de Cr�dito"
        Me.txtDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNumeroCompra
        '
        Me.txtNumeroCompra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroCompra.Location = New System.Drawing.Point(256, 50)
        Me.txtNumeroCompra.Name = "txtNumeroCompra"
        Me.txtNumeroCompra.Size = New System.Drawing.Size(104, 20)
        Me.txtNumeroCompra.TabIndex = 4
        Me.txtNumeroCompra.Tag = "N�mero de Compra"
        '
        'DPkFechaFactura
        '
        Me.DPkFechaFactura.CustomFormat = "dd-MMM-yyyy"
        Me.DPkFechaFactura.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkFechaFactura.Location = New System.Drawing.Point(69, 50)
        Me.DPkFechaFactura.Name = "DPkFechaFactura"
        Me.DPkFechaFactura.Size = New System.Drawing.Size(104, 20)
        Me.DPkFechaFactura.TabIndex = 3
        Me.DPkFechaFactura.Tag = "Fecha de Ingreso"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(5, 91)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Cantidad"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(187, 91)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Producto"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(361, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Proveedor"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(752, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Vence"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(577, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "D�as"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(187, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "N�mero"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvxProductosCompras)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.TextBox12)
        Me.GroupBox2.Controls.Add(Me.TextBox11)
        Me.GroupBox2.Controls.Add(Me.TextBox10)
        Me.GroupBox2.Controls.Add(Me.TextBox9)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 219)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1019, 252)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        '
        'dgvxProductosCompras
        '
        Me.dgvxProductosCompras.AllowUserToAddRows = False
        Me.dgvxProductosCompras.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvxProductosCompras.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvxProductosCompras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvxProductosCompras.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EsBonificacion, Me.Cantidad, Me.Unidad, Me.Codigo, Me.Producto, Me.Impuesto, Me.Precio, Me.Valor, Me.IdProducto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvxProductosCompras.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvxProductosCompras.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvxProductosCompras.Location = New System.Drawing.Point(8, 15)
        Me.dgvxProductosCompras.Name = "dgvxProductosCompras"
        Me.dgvxProductosCompras.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvxProductosCompras.Size = New System.Drawing.Size(998, 128)
        Me.dgvxProductosCompras.TabIndex = 66
        '
        'EsBonificacion
        '
        Me.EsBonificacion.CheckedAppearance = Appearance4
        Me.EsBonificacion.DefaultNewRowValue = CType(resources.GetObject("EsBonificacion.DefaultNewRowValue"), Object)
        Me.EsBonificacion.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.ScenicRibbon
        Me.EsBonificacion.HeaderText = "Bonificacion"
        Me.EsBonificacion.HotTrackingAppearance = Appearance5
        Me.EsBonificacion.IndeterminateAppearance = Appearance6
        Me.EsBonificacion.Name = "EsBonificacion"
        Me.EsBonificacion.ReadOnly = True
        Me.EsBonificacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.EsBonificacion.ToolTipText = "Seleccionar par Indicar Bonificacion"
        Me.EsBonificacion.Width = 85
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Increment = 1.0R
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.Width = 65
        '
        'Unidad
        '
        Me.Unidad.HeaderText = "Unidad"
        Me.Unidad.Name = "Unidad"
        Me.Unidad.ReadOnly = True
        Me.Unidad.Width = 70
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Producto
        '
        Me.Producto.HeaderText = "Producto"
        Me.Producto.Name = "Producto"
        Me.Producto.ReadOnly = True
        Me.Producto.Width = 370
        '
        'Impuesto
        '
        Me.Impuesto.HeaderText = "Impuesto"
        Me.Impuesto.Name = "Impuesto"
        Me.Impuesto.ReadOnly = True
        Me.Impuesto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Impuesto.Width = 70
        '
        'Precio
        '
        '
        '
        '
        Me.Precio.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Precio.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Precio.DisplayFormat = "#,##0.####"
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Increment = 1.0R
        Me.Precio.Name = "Precio"
        Me.Precio.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Precio.Width = 80
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.BackgroundStyle.TextColor = System.Drawing.SystemColors.ControlText
        Me.Valor.DisplayFormat = "#,##0.####"
        Me.Valor.HeaderText = "Total"
        Me.Valor.Increment = 1.0R
        Me.Valor.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Left
        Me.Valor.Name = "Valor"
        Me.Valor.ReadOnly = True
        Me.Valor.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Valor.Width = 120
        '
        'IdProducto
        '
        Me.IdProducto.HeaderText = "IdProducto"
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.ReadOnly = True
        Me.IdProducto.Visible = False
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(64, 144)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 65
        Me.Label19.Text = "Label19"
        Me.Label19.Visible = False
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(8, 144)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 64
        Me.Label18.Text = "Label18"
        Me.Label18.Visible = False
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(541, 200)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(64, 16)
        Me.Label17.TabIndex = 63
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(296, 176)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(93, 20)
        Me.Label16.TabIndex = 62
        Me.Label16.Text = "ANULADA"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(304, 152)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 13)
        Me.Label15.TabIndex = 61
        Me.Label15.Text = "<F5> AYUDA"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(885, 224)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(118, 20)
        Me.TextBox12.TabIndex = 60
        Me.TextBox12.TabStop = False
        Me.TextBox12.Tag = "Cantidad a Comprar"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(885, 200)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(118, 20)
        Me.TextBox11.TabIndex = 59
        Me.TextBox11.TabStop = False
        Me.TextBox11.Tag = "Cantidad a Comprar"
        Me.TextBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(885, 176)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(118, 20)
        Me.TextBox10.TabIndex = 58
        Me.TextBox10.TabStop = False
        Me.TextBox10.Tag = "Cantidad a Comprar"
        Me.TextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(885, 152)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(118, 20)
        Me.TextBox9.TabIndex = 57
        Me.TextBox9.TabStop = False
        Me.TextBox9.Tag = "Cantidad a Comprar"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(821, 224)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 56
        Me.Label14.Text = "Total Fact"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(821, 200)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 55
        Me.Label13.Text = "Retenci�n"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(821, 176)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 54
        Me.Label12.Text = "Imp Venta"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(821, 152)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 53
        Me.Label11.Text = "Sub Total"
        '
        'Timer1
        '
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 473)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 700
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(1023, 23)
        Me.UltraStatusBar1.TabIndex = 20
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.ButtonX1)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.CmdAnular, Me.LabelItem4, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdImprimir, Me.LabelItem5, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(1023, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 34
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.AutoExpandOnClick = True
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Image = CType(resources.GetObject("ButtonX1.Image"), System.Drawing.Image)
        Me.ButtonX1.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.ButtonX1.Location = New System.Drawing.Point(6, 8)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 54)
        Me.ButtonX1.SplitButton = True
        Me.ButtonX1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdVendedor, Me.cmdClientes, Me.cmdProductos, Me.cmdRefrescar})
        Me.ButtonX1.TabIndex = 0
        Me.ButtonX1.Tooltip = "<b><font color=""#17365D"">Listado</font></b>"
        '
        'cmdVendedor
        '
        Me.cmdVendedor.GlobalItem = False
        Me.cmdVendedor.Image = CType(resources.GetObject("cmdVendedor.Image"), System.Drawing.Image)
        Me.cmdVendedor.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdVendedor.Name = "cmdVendedor"
        Me.cmdVendedor.Text = "Vendedores"
        '
        'cmdClientes
        '
        Me.cmdClientes.GlobalItem = False
        Me.cmdClientes.Image = CType(resources.GetObject("cmdClientes.Image"), System.Drawing.Image)
        Me.cmdClientes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdClientes.Name = "cmdClientes"
        Me.cmdClientes.Text = "Clientes"
        '
        'cmdProductos
        '
        Me.cmdProductos.GlobalItem = False
        Me.cmdProductos.Image = CType(resources.GetObject("cmdProductos.Image"), System.Drawing.Image)
        Me.cmdProductos.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdProductos.Name = "cmdProductos"
        Me.cmdProductos.Text = "Productos"
        '
        'cmdRefrescar
        '
        Me.cmdRefrescar.GlobalItem = False
        Me.cmdRefrescar.Image = CType(resources.GetObject("cmdRefrescar.Image"), System.Drawing.Image)
        Me.cmdRefrescar.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdRefrescar.Name = "cmdRefrescar"
        Me.cmdRefrescar.Text = "Refrescar"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.ButtonX1
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'CmdAnular
        '
        Me.CmdAnular.Image = CType(resources.GetObject("CmdAnular.Image"), System.Drawing.Image)
        Me.CmdAnular.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.CmdAnular.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.CmdAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.CmdAnular.Name = "CmdAnular"
        Me.CmdAnular.Text = "Anular<F3>"
        Me.CmdAnular.Tooltip = "Anular Recibo"
        Me.CmdAnular.Visible = False
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Image = CType(resources.GetObject("cmdImprimir.Image"), System.Drawing.Image)
        Me.cmdImprimir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimir.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.cmdImprimir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Text = "Imprimir<F8>"
        Me.cmdImprimir.Tooltip = "Imprimir Compra"
        Me.cmdImprimir.Visible = False
        '
        'LabelItem5
        '
        Me.LabelItem5.Name = "LabelItem5"
        Me.LabelItem5.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmCompras
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1023, 496)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmCompras"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCompras"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.medPrecio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvxProductosCompras, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strUnidad As String = String.Empty
    Dim strFacturas As String = String.Empty
    Dim strDescrip As String = String.Empty
    Dim intImpuesto As Integer = 0
    Dim intImprimir As Integer = 0
    Public IdAgencia As Integer = 0
    Public IdProv As Integer = 0
    Dim intRow As Integer = 0
    Dim intCol As Integer = 0
    Dim lngRegistro2 As Long = 0
    Dim NumeroCompraExiste As Boolean = False
    Dim chk As Boolean
    Public txtCollection As New Collection
    Dim CantidadAnterior As Double = 0
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmCompras"
    Dim strCurrency As String = String.Empty
    Dim acceptableKey As Boolean = False
    Dim gbGuardar As Boolean = False
    Private Sub frmCompras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim frmNew As New frmEscogerAgencia
        Dim clPedidoCompras As RNPedidoCompra = Nothing
        Try
            clPedidoCompras = New RNPedidoCompra()
            cbPedidoCompra.Enabled = True
            gbGuardar = False
            Select Case intTipoCompra
                Case ENTipoCompra.COMPRA_CONTADO,
                    ENTipoCompra.COMPRA_CREDITO,
                    ENTipoCompra.COMPRA_MANUAL_CONTADO,
                    ENTipoCompra.COMPRA_MANUAL_CREDITO
                    cmbImpuesto.SelectedIndex = 0
                    cmdOK.Visible = True
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = False
                    txtNumeroCompra.Enabled = True
                    cbPedidoCompra.Enabled = True
                Case ENTipoCompra.COMPRA_CONSULTAR_CONTADO,
                    ENTipoCompra.COMPRA_CONSULTAR_CREDITO
                    cmdOK.Visible = False
                    CmdAnular.Visible = False
                    cmdImprimir.Visible = True
                    'dgvxProductosCompras.Enabled = False
                    dgvxProductosCompras.Columns("Cantidad").ReadOnly = True
                    dgvxProductosCompras.Columns("Precio").ReadOnly = True
                    txtNumeroCompra.Enabled = True
                    cbPedidoCompra.Enabled = False
                Case ENTipoCompra.COMPRA_ANULAR_CONTADO,
                    ENTipoCompra.COMPRA_ANULAR_CREDITO
                    cmdOK.Visible = False
                    CmdAnular.Visible = True
                    cmdImprimir.Visible = False
                    'dgvxProductosCompras.Enabled = False
                    dgvxProductosCompras.Columns("Cantidad").ReadOnly = True
                    dgvxProductosCompras.Columns("Precio").ReadOnly = True
                    txtNumeroCompra.Enabled = True
                    cbPedidoCompra.Enabled = False
            End Select

            intDiaFact = 0
            strMesFact = String.Empty
            lngYearFact = 0
            strVendedor = String.Empty
            strNumeroFact = String.Empty
            strCodCliFact = String.Empty
            strClienteFact = String.Empty

            Me.Top = 0
            Me.Left = 0
            strUnidad = String.Empty
            strDescrip = String.Empty
            lngRegistro = 0
            intImprimir = 0
            txtCollection.Add(txtNumeroCompra)
            txtCollection.Add(txtDias)
            txtCollection.Add(txtVence)
            'txtCollection.Add(txtNombreVendedor)
            'txtCollection.Add(txtNombreProveedor)
            txtCollection.Add(txtProducto)
            txtCollection.Add(txtCantidad)
            'txtCollection.Add(medPrecio)
            txtCollection.Add(TextBox9)
            txtCollection.Add(TextBox10)
            txtCollection.Add(TextBox11)
            txtCollection.Add(TextBox12)
            txtCollection.Add(txtNumeroVendedor)
            'txtCollection.Add(txtNumeroProveedor)
            txtCollection.Add(txtDepartamento)
            txtCollection.Add(txtNegocio)

            UbicarAgencia(lngRegUsuario)
            If (lngRegAgencia = 0) Then
                lngRegAgencia = 0
                frmNew.ShowDialog()
                If lngRegAgencia = 0 Then
                    MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
                End If
            End If
            'clPedidoCompras.CargarComboPedidosCompraPendiente(cbPedidoCompra, lngRegUsuario)
            Limpiar()
            Limpiar2()
            txtProveedor.Text = String.Empty
            txtNumeroProveedor.Text = String.Empty
            txtNombreProveedor.Text = String.Empty
            MenuItem8.Visible = False
            Label16.Visible = False
            Label18.Text = intTipoCompra
            Label19.Text = lngRegAgencia
            FuncionTimer()
            LlenarComboSucursales()
            txtNumeroCompra.ReadOnly = False
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Sub UbicarPrecio()
        Dim lnIdAgencia As Integer = 0
        Try
            lnIdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.UbicarPrecio(lngRegistro, 0, 0, lnIdAgencia)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    txtPrecioActualCosto.Text = dtrAgro2K.Item("PrecioCosto")
                End While
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            dtrAgro2K = Nothing

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Private Sub LlenarComboSucursales()
        Try
            RNAgencias.CargarComboAgencias(cmbAgencias, lngRegUsuario)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

        'cmbAgencias.ValueMember = lngRegAgencia
    End Sub

    Private Sub frmCompras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try
            intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                'If intTipoCompra = 3 Or intTipoCompra = 6 Then
                Select Case intTipoCompra
                    Case ENTipoCompra.COMPRA_ANULAR_CONTADO, ENTipoCompra.COMPRA_ANULAR_CREDITO
                        If dgvxProductosCompras.Rows.Count <= 0 Then
                            MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                            Exit Sub
                        End If
                        txtNumeroCompra.Focus()
                    Case 2, 5, ENTipoCompra.COMPRA_MANUAL_CREDITO, ENTipoCompra.COMPRA_MANUAL_CONTADO, ENTipoCompra.COMPRA_CONSULTAR_CONTADO, ENTipoCompra.COMPRA_CONSULTAR_CREDITO
                        If dgvxProductosCompras.Rows.Count <= 0 Then
                            MsgBox("No hay productos a Comprar", MsgBoxStyle.Critical, "Error de Datos")
                            txtCantidad.Focus()
                            Exit Sub
                        End If
                End Select


                'If intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Then
                '    If dgvxProductosCompras.Rows.Count <= 0 Then
                '        MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                '        Exit Sub
                '    End If
                '    txtNumeroCompra.Focus()
                'ElseIf intTipoCompra = 2 Or intTipoCompra = 5 Or intTipoCompra = 7 Or intTipoCompra = 8 Or intTipoCompra = 9 Or intTipoCompra = 10 Then
                '    If dgvxProductosCompras.Rows.Count <= 0 Then
                '        MsgBox("No hay productos a Comprar", MsgBoxStyle.Critical, "Error de Datos")
                '        txtCantidad.Focus()
                '        Exit Sub
                '    End If
                '    'Guardar()
                'End If
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If MenuItem5.Visible = True Then
                    intListadoAyuda = 1
                ElseIf MenuItem6.Visible = True Then
                    intListadoAyuda = 7
                ElseIf MenuItem7.Visible = True Then
                    intListadoAyuda = 3
                End If
                Dim frmNew As New frmListadoAyuda
                frmNew.ShowDialog()
            ElseIf e.KeyCode = Keys.F7 Then
                If dgvxProductosCompras.Rows.Count <= 0 Then
                    MsgBox("No hay productos a Comprar", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                'Guardar()
            ElseIf e.KeyCode = Keys.F8 Then
                intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                If dgvxProductosCompras.Rows.Count <= 0 Then
                    MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                'Guardar()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem12.Click

        Try
            intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            Select Case sender.text.ToString
                Case "Imprimir"
                    If dgvxProductosCompras.Rows.Count <= 0 Then
                        MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                        txtCantidad.Focus()
                        Exit Sub
                    End If
                    Guardar()
                Case "&Guardar"
                    If dgvxProductosCompras.Rows.Count <= 0 Then
                        MsgBox("No hay productos a Comprar", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    Guardar()
                Case "Anular"
                    If dgvxProductosCompras.Rows.Count <= 0 Then
                        MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                        Exit Sub
                    End If
                    If intTipoCompra = 3 Or intTipoCompra = 6 Then
                        intResp = MsgBox("�Est� Seguro de Anular esta Compra?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Acci�n")
                        If intResp = 6 Then
                            Anular()
                            MsgBox("Compra fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Compra")
                            txtNumeroCompra.Focus()
                        End If
                    End If
                Case "&Cancelar", "L&impiar" : Limpiar()
                Case "Vendedores" : intListadoAyuda = 1
                Case "Proveedores" : intListadoAyuda = 2
                Case "Productos" : intListadoAyuda = 3
                Case "&Salir" : Me.Close()
            End Select
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)
        Try
            intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumeroCompra.GotFocus, txtDias.GotFocus, txtVence.GotFocus, txtNombreProveedor.GotFocus, txtProducto.GotFocus, txtCantidad.GotFocus, txtProveedor.GotFocus, DPkFechaFactura.GotFocus, txtProveedor.GotFocus

        Dim strCampo As String = String.Empty

        Try
            Label15.Visible = False
            MenuItem5.Visible = False
            MenuItem6.Visible = False
            MenuItem7.Visible = False
            MenuItem12.Visible = False
            strCampo = ""
            Select Case sender.name.ToString
                Case "DPkFechaFactura" : strCampo = "Fecha"
                Case "txtNumeroCompra" : strCampo = "N�mero"
                    If intTipoCompra = 2 Or intTipoCompra = 5 Then
                        MenuItem12.Visible = True
                    End If
                Case "txtDia" : strCampo = "D�as Cr�dito"
                Case "txtVencimiento" : strCampo = "Vencimiento"
                Case "txtVendedor" : strCampo = "Vendedor" : MenuItem12.Visible = True : MenuItem5.Visible = True : Label15.Visible = True
                Case "txtProveedor" : strCampo = "Proveedor"
                    MenuItem6.Visible = True
                    Label15.Visible = True
                    If intTipoCompra = 4 Or intTipoCompra = 8 Then
                        MenuItem12.Visible = True
                        MenuItem6.Visible = True
                        Label15.Visible = True
                    End If
                Case "txtProducto" : strCampo = "Producto" : MenuItem12.Visible = True : MenuItem7.Visible = True : Label15.Visible = True
                Case "txtCantidad" : strCampo = "Cantidad"
                Case "TextBox8" : strCampo = "Tipo de Precio"
                Case "ddlRentencion" : strCampo = "Retenci�n"
                Case "ddlTipoPrecio" : strCampo = "Tipo Precio"
            End Select
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name <> "DPkFechaFactura" Then
                sender.selectall()
            End If
            If blnUbicar Then
                FuncionTimer()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub ExtraerPedidoCompraPendiente()
        Dim clPedidoCompra As RNPedidoCompra = Nothing
        Dim lnIdPedido As Integer = 0
        Dim lsFechaIngreso As String = String.Empty
        Try

            dgvxProductosCompras.Rows.Clear()
            clPedidoCompra = New RNPedidoCompra()

            If cbPedidoCompra.SelectedValue IsNot Nothing Then
                lnIdPedido = SUConversiones.ConvierteAInt(cbPedidoCompra.SelectedValue)
            End If
            dtrAgro2K = Nothing
            dtrAgro2K = clPedidoCompra.ObtienePedidoCompraxId(lnIdPedido)

            If dtrAgro2K IsNot Nothing Then
                TextBox9.Text = "0.00"
                TextBox10.Text = "0.00"
                TextBox11.Text = "0.00"
                TextBox12.Text = "0.00"

                While dtrAgro2K.Read
                    TextBox17.Text = SUConversiones.ConvierteAInt(dtrAgro2K.Item("IdPedido"))
                    txtNumeroProveedor.Text = dtrAgro2K.Item("IdProveedor")
                    lsFechaIngreso = String.Empty
                    lsFechaIngreso = dtrAgro2K.Item("FechaIngresoNum")
                    'If lsFechaIngreso.Trim().Length > 0 Then
                    '    DPkFechaFactura.Value = New Date(lsFechaIngreso.Substring(0, 4), lsFechaIngreso.Substring(4, 2), lsFechaIngreso.Substring(6, 2))
                    'End If

                    'DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                    ' txtNumeroCompra.Text = dtrAgro2K.Item("NumeroDocumento")
                    txtNombreProveedor.Text = dtrAgro2K.Item("NombreProveedor")
                    txtProveedor.Text = dtrAgro2K.Item("CodigoProveedor")

                    Dim imp As String = String.Empty
                    imp = "No"
                    If dtrAgro2K.Item("Impuesto") > 0 Then
                        imp = "Si"
                    End If

                    dgvxProductosCompras.Rows.Add(dtrAgro2K.Item("EsBonificable"), Format(dtrAgro2K.Item("Cantidad"), "#,##0.#0"), dtrAgro2K.Item("Unidad"), dtrAgro2K.Item("CodigoProducto"), dtrAgro2K.Item("DesProducto"), imp, Format(dtrAgro2K.Item("PrecioCosto"), "#,##0.####"), Format(dtrAgro2K.Item("Total"), "#,##0.####"), dtrAgro2K.Item("IdProducto"))

                    'TextBox9.Text = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                    TextBox9.Text = Format(dtrAgro2K.Item("SubTotal"), "#,##0.####")
                    TextBox10.Text = Format(dtrAgro2K.Item("TotalImpuesto"), "#,##0.####")
                    TextBox11.Text = Format(dtrAgro2K.Item("TotalRetencion"), "#,##0.####")
                    TextBox12.Text = Format(dtrAgro2K.Item("Total"), "#,##0.####")
                    dblTipoCambio = Format(dtrAgro2K.Item("TasaCambio"), "###0.####")
                    If dtrAgro2K.Item("IndicaAnulado") = 1 Then
                        Label16.Visible = True
                    Else
                        Label16.Visible = False
                    End If
                End While
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub


    Sub ExtraerCompra()

        Me.Cursor = Cursors.WaitCursor

        Try
            Dim lngFecha As Long
            Dim cmdTmp As New SqlCommand("ExtraerCompra", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter
            Dim cmdTmpx As New SqlCommand("UbicarCompras", cnnAgro2K)
            Dim prmTmpx01 As New SqlParameter
            Dim prmTmpx02 As New SqlParameter
            Dim strGenNumFact As String
            Dim dteFechaIng As Date

            strNumeroUbicarFactura = ""
            strFacturas = ""
            IdProv = SUConversiones.ConvierteAInt(txtNumeroProveedor.Text.Trim())
            'If intTipoCompra <> 2 And intTipoCompra <> 5 Then
            If intTipoCompra <> ENTipoCompra.COMPRA_IMPRIMIR_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_IMPRIMIR_CONTADO Then

                If Trim(txtNumeroCompra.Text) = "" Then
                    MsgBox("Tiene que ingresar un n�mero de Compra a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else
                If Trim(txtNumeroCompra.Text) = "" Then
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If
            IdAgencia = ConvierteAInt(cmbAgencias.SelectedValue)
            strFacturas = txtNumeroCompra.Text
            'If intFechaCambioCompra >= 0 And (intTipoCompra = 6 Or intTipoCompra = 10) Then
            If intFechaCambioCompra >= 0 And (intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO) Then
                strGenNumFact = ""
                strGenNumFact = txtNumeroCompra.Text.Trim()
                With prmTmpx01
                    .ParameterName = "@lngAgencia"
                    .SqlDbType = SqlDbType.TinyInt
                    .Value = IdAgencia  'lngRegAgencia
                End With
                With prmTmpx02
                    .ParameterName = "@strNumero"
                    .SqlDbType = SqlDbType.VarChar
                    .Value = strGenNumFact
                End With
                With cmdTmpx
                    .Parameters.Add(prmTmpx01)
                    .Parameters.Add(prmTmpx02)
                    .CommandType = CommandType.StoredProcedure
                End With
                If cmdTmpx.Connection.State = ConnectionState.Open Then
                    cmdTmpx.Connection.Close()
                End If
                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If
                cnnAgro2K.Open()
                cmdTmpx.Connection = cnnAgro2K
                Try
                    dtrAgro2K = cmdTmpx.ExecuteReader
                Catch exc As Exception
                    MsgBox(exc.Message.ToString)
                    dtrAgro2K.Close()
                    cmdTmpx.Connection.Close()
                    cnnAgro2K.Close()
                    Exit Sub
                End Try

                'If (intTipoCompra = 6 Or intTipoCompra = 10) And intFechaCambioCompra > 0 Then
                If (intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO) And intFechaCambioCompra > 0 Then
                    If dtrAgro2K.Read() Then
                        strNumeroUbicarFactura = ""
                        dtrAgro2K.Close()
                        dtrAgro2K = cmdTmpx.ExecuteReader
                        Dim frmNew As New frmUbicarCompras
                        frmNew.ShowDialog()
                        txtNumeroCompra.Text = strNumeroUbicarFactura
                    End If
                End If
            End If
            ' Nueva Modalidad de Busqueda y Verificacion por Facturas Nuevas (F)

            'If intTipoCompra = 3 Or intTipoCompra = 7 Or intTipoCompra = 9 Then
            If intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Then
                txtNumeroCompra.Text = txtNumeroCompra.Text.Trim
            End If
            With prmTmp01
                .ParameterName = "@lngAgencia"
                .SqlDbType = SqlDbType.TinyInt
                .Value = IdAgencia  'lngRegAgencia
            End With
            With prmTmp02
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                .Value = txtNumeroCompra.Text
            End With
            With prmTmp03
                .ParameterName = "@intTipoCompra"
                .SqlDbType = SqlDbType.TinyInt
                .Value = intTipoCompra
            End With
            With prmTmp04
                .ParameterName = "@IdProveedor"
                .SqlDbType = SqlDbType.TinyInt
                .Value = IdProv
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .CommandType = CommandType.StoredProcedure
            End With
            'If dtrAgro2K IsNot Nothing Then
            '    dtrAgro2K.Close()
            'End If
            'If cnnAgro2K IsNot Nothing Then
            '    If cnnAgro2K.State = ConnectionState.Open Then
            '        cnnAgro2K.Close()
            '    End If
            'End If
            'If cmdAgro2K IsNot Nothing Then
            '    If cmdAgro2K.Connection IsNot Nothing Then
            '        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '            cmdAgro2K.Connection.Close()
            '        End If
            '    End If
            'End If

            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraConexioDR(dtrAgro2K)
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K
            dtrAgro2K = cmdTmp.ExecuteReader
            'If intTipoCompra = 7 Or intTipoCompra = 8 Then
            If intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                strFacturas = txtNumeroCompra.Text
                dteFechaIng = DPkFechaFactura.Value
            End If
            Limpiar()
            Limpiar2()
            lngFecha = 0

            'If intTipoCompra = 2 Or intTipoCompra = 3 Or intTipoCompra = 9 Then
            If intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Then
                While dtrAgro2K.Read
                    txtNumeroProveedor.Text = dtrAgro2K.GetValue(21)
                    TextBox17.Text = dtrAgro2K.GetValue(0)
                    lngFecha = dtrAgro2K.GetValue(19)
                    DPkFechaFactura.Value = DefinirFecha(lngFecha)
                    DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                    txtNumeroCompra.Text = dtrAgro2K.GetValue(2)
                    txtNombreProveedor.Text = dtrAgro2K.GetValue(5)

                    Dim imp As String = String.Empty
                    imp = "No"
                    If dtrAgro2K.GetValue(10) > 0 Then
                        imp = "Si"
                    End If

                    dgvxProductosCompras.Rows.Add(dtrAgro2K.GetValue(22), Format(dtrAgro2K.GetValue(6), "#,##0.#0"), dtrAgro2K.GetValue(7), dtrAgro2K.GetValue(8), dtrAgro2K.GetValue(9), imp, Format(dtrAgro2K.GetValue(11), "#,##0.####"), dtrAgro2K.GetValue(12), dtrAgro2K.GetValue(13))

                    'TextBox9.Text = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                    TextBox9.Text = Format(dtrAgro2K.Item("Subtotal"), "#,##0.##00")
                    TextBox10.Text = Format(dtrAgro2K.Item("impuesto"), "#,##0.##00")
                    TextBox11.Text = Format(dtrAgro2K.Item("retencion"), "#,##0.##00")
                    TextBox12.Text = Format(dtrAgro2K.Item("total"), "#,##0.##00")
                    dblTipoCambio = Format(dtrAgro2K.GetValue(20), "###0.####")
                    If dtrAgro2K.GetValue(18) = 1 Then
                        Label16.Visible = True
                    Else
                        Label16.Visible = False
                    End If
                End While
            ElseIf intTipoCompra = 5 Or intTipoCompra = 6 Or intTipoCompra = 10 Then
                While dtrAgro2K.Read
                    txtNumeroProveedor.Text = dtrAgro2K.GetValue(23)
                    TextBox17.Text = dtrAgro2K.GetValue(0)
                    lngFecha = dtrAgro2K.GetValue(21)
                    DPkFechaFactura.Value = DefinirFecha(lngFecha)
                    txtNumeroCompra.Text = dtrAgro2K.GetValue(2)
                    txtDias.Text = dtrAgro2K.GetValue(3)
                    txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                    DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")

                    If dtrAgro2K.GetValue(6) = "" Then
                        txtProveedor.Text = ""
                    Else
                        txtProveedor.Text = dtrAgro2K.GetValue(6)
                    End If
                    txtNombreProveedor.Text = dtrAgro2K.GetValue(7)

                    Dim imp As String
                    imp = "No"
                    If dtrAgro2K.GetValue(12) > 0 Then
                        'MSFlexGrid2.Text = "Si"
                        imp = "Si"
                    End If
                    'MSFlexGrid2.Col = 5
                    dblTipoCambio = SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(22))

                    dgvxProductosCompras.Rows.Add(dtrAgro2K.GetValue(24), Format(dtrAgro2K.GetValue(8), "#,##0.#0"), dtrAgro2K.GetValue(9), dtrAgro2K.GetValue(10), dtrAgro2K.GetValue(11), imp, Format(dtrAgro2K.GetValue(13), "#,##0.#0"), dtrAgro2K.GetValue(14), dtrAgro2K.GetValue(15))

                    'If Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "A" Then
                    '    'TextBox9.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(16)) / dblTipoCambio, "#,##0.#0")
                    '    TextBox9.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(16)) / dblTipoCambio, "#,##0.####")
                    '    TextBox10.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(17)) / dblTipoCambio, "#,##0.####")
                    '    TextBox11.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(18)) / dblTipoCambio, "#,##0.####")
                    '    TextBox12.Text = Format(SUConversiones.ConvierteADouble(dtrAgro2K.GetValue(19)) / dblTipoCambio, "#,##0.####")
                    'Else
                    '    TextBox9.Text = Format(dtrAgro2K.GetValue(16), "#,##0.####")
                    '    TextBox10.Text = Format(dtrAgro2K.GetValue(17), "#,##0.####")
                    '    TextBox11.Text = Format(dtrAgro2K.GetValue(18), "#,##0.####")
                    '    TextBox12.Text = Format(dtrAgro2K.GetValue(19), "#,##0.####")
                    'End If
                    TextBox9.Text = Format(dtrAgro2K.Item("Subtotal"), "#,##0.##00")
                    TextBox10.Text = Format(dtrAgro2K.Item("impuesto"), "#,##0.##00")
                    TextBox11.Text = Format(dtrAgro2K.Item("retencion"), "#,##0.##00")
                    TextBox12.Text = Format(dtrAgro2K.Item("total"), "#,##0.##00")
                    If dtrAgro2K.GetValue(20) = 1 Then
                        Label16.Visible = True
                    Else
                        Label16.Visible = False
                    End If
                End While
            ElseIf intTipoCompra = 7 Or intTipoCompra = 8 Then
                If dtrAgro2K.IsClosed Then
                    cnnAgro2K.Open()
                    cmdTmp.Connection = cnnAgro2K
                    dtrAgro2K = cmdTmp.ExecuteReader
                End If
                NumeroCompraExiste = False
                While dtrAgro2K.Read
                    txtNumeroCompra.Text = dtrAgro2K.GetValue(0)
                    strFacturas = dtrAgro2K.GetValue(0)
                    NumeroCompraExiste = True
                End While

                DPkFechaFactura.Value = dteFechaIng
            End If

            'If intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 6 Or intTipoCompra = 10 Then
            'If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
            If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CREDITO Then
                UbicarProveedor(txtProveedor.Text)
            End If
            Me.Cursor = Cursors.Default
            'If intTipoCompra = 9 Or intTipoCompra = 10 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                txtNumeroCompra.Focus()
                txtNumeroCompra.SelectAll()
            End If
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                If dtrAgro2K IsNot Nothing Then
                    SUFunciones.CierraComando(cmdAgro2K)
                    SUFunciones.CierraConexionBD(cnnAgro2K)
                    SUFunciones.CierraConexioDR(dtrAgro2K)
                    cmdTmp.Connection = cnnAgro2K

                    If dtrAgro2K.IsClosed Then
                        cnnAgro2K.Open()
                        cmdTmp.Connection = cnnAgro2K
                        dtrAgro2K = cmdTmp.ExecuteReader
                    Else
                        cmdTmp.Connection = cnnAgro2K
                        dtrAgro2K = cmdTmp.ExecuteReader
                    End If
                    NumeroCompraExiste = False
                    While dtrAgro2K.Read
                        txtNumeroCompra.Text = dtrAgro2K.GetValue(2)
                        strFacturas = dtrAgro2K.GetValue(2)
                        NumeroCompraExiste = True
                    End While
                End If
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)

            'If dtrAgro2K IsNot Nothing Then
            '    dtrAgro2K.Close()
            'End If
            'If cnnAgro2K IsNot Nothing Then
            '    If cnnAgro2K.State = ConnectionState.Open Then
            '        cnnAgro2K.Close()
            '    End If
            'End If
            'If cmdAgro2K IsNot Nothing Then
            '    If cmdAgro2K.Connection IsNot Nothing Then
            '        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '            cmdAgro2K.Connection.Close()
            '        End If
            '    End If
            'End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    'Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroCompra.KeyDown, txtDias.KeyDown, txtVence.KeyDown, txtNombreProveedor.KeyDown, txtProducto.KeyDown, txtCantidad.KeyDown, txtProveedor.KeyDown, medPrecio.KeyDown, DPkFechaFactura.KeyDown, txtProveedor.KeyDown, chkBonificacion.KeyDown
    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroCompra.KeyDown, txtDias.KeyDown, txtVence.KeyDown, txtNombreProveedor.KeyDown, txtProducto.KeyDown, txtCantidad.KeyDown, txtProveedor.KeyDown, DPkFechaFactura.KeyDown, chkBonificacion.KeyDown, cmbImpuesto.KeyDown, ddlRetencion.KeyDown, medPrecio.KeyDown
        Dim IndicaBonificacion As Integer = 0

        Try
            If e.KeyCode = Keys.Enter Then
                intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                Select Case sender.name.ToString
                    Case "DPkFechaFactura"
                        Select Case intTipoCompra
                            Case 1, 4, 7, 8, 3, 5, 6 : txtProveedor.Focus()
                        End Select
                    Case "txtNumeroCompra"
                        Select Case intTipoCompra
                            Case 3, 6 : ExtraerCompra()
                                If txtNumeroCompra.Text = "" Then
                                    MsgBox("El N�mero Ingresado no Pertenece a una Compra Emitida o la Compra est� Anulada.", MsgBoxStyle.Information, "Compra No Emitida o Anulada")
                                    Limpiar()
                                    Limpiar2()
                                    txtNumeroCompra.Focus()
                                    Exit Sub
                                Else
                                    ddlRetencion.Focus()
                                    'txtDias.Focus()
                                End If
                            Case 1, 4, 7, 8 : ExtraerCompra()
                                If txtNumeroCompra.Text <> "" Then
                                    MsgBox("El N�mero Ingresado ya Pertenece a una Compra Emitida o la Compra est� Anulada.", MsgBoxStyle.Information, "Compra Emitida o Anulada")
                                    txtNumeroCompra.Focus()
                                    txtNumeroCompra.Text = ""
                                    Exit Sub
                                Else
                                    txtCantidad.Focus()
                                End If
                                If (intTipoCompra = 1 Or intTipoCompra = 4) And NumeroCompraExiste Then
                                    MsgBox("El N�mero Ingresado ya Pertenece a una Compra Emitida o la Compra est� Anulada.", MsgBoxStyle.Information, "Compra Emitida o Anulada")
                                    txtNumeroCompra.Focus()
                                    txtNumeroCompra.Text = ""
                                    Exit Sub
                                Else
                                    'txtDias.Focus()
                                    ddlRetencion.Focus()
                                End If


                                If strFacturas <> "" Then
                                    txtNumeroCompra.Text = strFacturas
                                End If
                                If intTipoCompra = 8 Then
                                    'txtDias.Focus()
                                    ddlRetencion.Focus()
                                End If
                                If intTipoCompra = 1 Or intTipoCompra = 4 Then
                                    'txtDias.Focus()
                                    ddlRetencion.Focus()
                                End If
                            Case 9, 10 : ExtraerCompra()
                                If txtNumeroCompra.Text = "" Then
                                    MsgBox("El N�mero Ingresado no Pertenece a una Compra Emitida.", MsgBoxStyle.Information, "Compra No Emitida")
                                    Limpiar()
                                    txtNumeroCompra.Focus()
                                    Exit Sub
                                End If
                        End Select
                    Case "ddlRetencion"
                        If txtDias.Visible Then
                            txtDias.Focus()
                        Else
                            txtCantidad.Focus()
                        End If
                        Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%"
                        CalculaTotalasFactura()
                    Case "txtDias"
                        If IsNumeric(Trim(txtDias.Text)) = False Then
                            txtDias.Text = "0"
                        End If
                        txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                        txtCantidad.Focus()
                    Case "txtVendedor"
                        blnUbicar = False
                        If txtProveedor.Visible = True Then
                            txtProveedor.Focus()
                        Else
                            txtNombreProveedor.Focus()
                        End If
                    Case "txtProveedor"
                        'If intTipoCompra = 1 Or intTipoCompra = 7 Or intTipoCompra = 6 Or intTipoCompra = 10 Or intTipoCompra = 4 Or intTipoCompra = 8 Or intTipoCompra = 9 Or intTipoCompra = 3 Then
                        If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Then
                            blnUbicar = False
                            If txtProveedor.Text = "" Then
                                MsgBox("Ingresar Codigo del Proveedor", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                                txtProveedor.Focus()
                                Exit Sub
                            Else
                                If UbicarProveedor(txtProveedor.Text) = False Then
                                    MsgBox("El C�digo de proveedor no est� registrado o el vendedor no es el asignado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                                    'If txtVendedor.Text = "" Then
                                    '    txtVendedor.Focus()
                                    'Else
                                    '    txtProveedor.Focus()
                                    'End If
                                    Exit Sub
                                Else
                                    txtNumeroCompra.Enabled = True
                                End If
                            End If
                            txtNumeroCompra.Enabled = True
                            txtNumeroCompra.Focus()
                        End If
                    Case "txtNombreProveedor" : txtNumeroCompra.Focus()
                    Case "txtProducto"
                        blnUbicar = False
                        If UbicarProducto(txtProducto.Text) = False Then
                            MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtProducto.Focus()
                            Exit Sub
                        End If
                        CalculaTotalasFactura()
                        cmbImpuesto.Focus()

                    Case "txtCantidad"
                        'If MSFlexGrid2.Rows > 1 And Trim(txtCantidad.Text) = "0" Then
                        If dgvxProductosCompras.Rows.Count >= 1 And Trim(txtCantidad.Text) = "0" Then
                            If intTipoCompra = 1 Or intTipoCompra = 4 Then
                                If cmbAgencias.SelectedValue = 0 Then
                                    MsgBox("Debe Seleccionar una Sucursal a la cual se le Ingresara la Compra", MsgBoxStyle.Exclamation, "Guardar Compras")
                                    Exit Sub
                                Else
                                    Guardar()
                                    'txtNumeroCompra.Enabled = False
                                    Exit Sub
                                End If
                                'Guardar()

                            End If
                        Else
                            'chkBonificacion.Focus()
                            txtProducto.Focus()
                        End If
                        CalculaTotalasFactura()
                    Case "chkBonificacion"
                        If e.KeyCode = Keys.Enter Then
                            chkBonificacion.Checked = chk
                        End If
                        medPrecio.Focus()
                        CalculaTotalasFactura()
                        'medPrecio.SelectAll()
                        'txtProducto.Focus()
                    Case "cmbImpuesto"
                        CalculaTotalasFactura()
                        chkBonificacion.Focus()
                    Case "medPrecio"
                        If ConvierteADouble(txtCantidad.Text) > 0 Then
                            If chkBonificacion.Checked Then
                                IndicaBonificacion = 1
                                If ConvierteADouble(medPrecio.Text) = 0 Then
                                    LlenarGrid(IndicaBonificacion)
                                Else
                                    MsgBox("No puede Ingresar un producto Bonificado con monto ", MsgBoxStyle.Critical, "Error de Cantidad")
                                    medPrecio.Focus()
                                End If
                            Else
                                IndicaBonificacion = 0
                                If ConvierteADouble(medPrecio.Text) > 0 Then
                                    LlenarGrid(IndicaBonificacion)
                                Else
                                    MsgBox("No puede comprar un producto con monto 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                                    medPrecio.Focus()
                                End If

                            End If
                            CalculaTotalasFactura()
                            'If ddlRetencion.SelectedIndex = 0 Then
                            '    TextBox11.Text = "0.00"
                            'ElseIf ddlRetencion.SelectedIndex = 1 Then
                            '    TextBox11.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) * dblRetParam, "#,##0.####")
                            'End If
                        Else
                            MsgBox("No puede comprar un producto con cantidad 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                            txtCantidad.Focus()
                        End If
                End Select
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try

    End Sub

    Sub LlenarGrid(ByVal IndicaBonificacion As Integer)

        Try
            DPkFechaFactura.Focus()

            dgvxProductosCompras.Rows.Add(IndicaBonificacion, Format(SUConversiones.ConvierteADouble(txtCantidad.Text), "####0.#0"), UCase(strUnidad), UCase(txtProducto.Text), UCase(strDescrip), cmbImpuesto.SelectedItem.ToString().ToUpper(), medPrecio.Text, Format(SUConversiones.ConvierteADouble(medPrecio.Text) * SUConversiones.ConvierteADouble(txtCantidad.Text), "#,##0.#000"), lngRegistro)

            TextBox9.Text = Format(Format((SUConversiones.ConvierteADouble(medPrecio.Text) * SUConversiones.ConvierteADouble(txtCantidad.Text)), "#,##0.####") + SUConversiones.ConvierteADouble(TextBox9.Text), "#,##0.####")

            If cmbImpuesto.SelectedItem.ToString().ToUpper() = "NO" Then
                TextBox10.Text = Format(SUConversiones.ConvierteADouble(TextBox10.Text), "#,##0.####")
                'ElseIf intImpuesto = 1 Then
            Else
                TextBox10.Text = Format(Format((Format((SUConversiones.ConvierteADouble(medPrecio.Text) * SUConversiones.ConvierteADouble(txtCantidad.Text)), "#,##0.####") * dblPorcParam), "#,##0.####") + SUConversiones.ConvierteADouble(TextBox10.Text), "#,##0.####")
            End If

            TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.####")
            Limpiar2()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try
    End Sub

    Private Function CalculaTotalasFactura() As SETotalesFactura
        Dim objTotalesFactura As SETotalesFactura = Nothing
        Dim dblSubtotal As Double = 0
        Dim dblTotal As Double = 0
        Dim dblImpuesto As Double = 0
        Dim dblRetencion As Double = 0
        Dim lnCantidad As Double = 0
        Dim intIncr As Integer = 0
        Dim lnImpuesto As Double = 0
        Dim lsIndicaImpuesto As String = String.Empty
        Dim lnPrecioUnitario As Double = 0
        Try
            objTotalesFactura = New SETotalesFactura()
            If dgvxProductosCompras.IsCurrentCellDirty Then
                dgvxProductosCompras.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            If dgvxProductosCompras.Rows.Count > 0 Then

                For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1
                    lnImpuesto = 0
                    lsIndicaImpuesto = String.Empty
                    lnCantidad = 0
                    lnPrecioUnitario = 0
                    lnCantidad = lnCantidad + SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value)
                    lnPrecioUnitario = lnPrecioUnitario + SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value)
                    dblSubtotal = dblSubtotal + (lnCantidad * lnPrecioUnitario)
                    lsIndicaImpuesto = dgvxProductosCompras.Rows(intIncr).Cells("Impuesto").Value
                    If lsIndicaImpuesto.Trim().ToUpper = "SI" Then
                        lnImpuesto = (lnCantidad * lnPrecioUnitario) * dblPorcParam
                    End If
                    dblImpuesto = dblImpuesto + lnImpuesto
                Next
            End If

            dblRetencion = 0
            If ddlRetencion.SelectedIndex = 1 Then
                dblRetencion = dblSubtotal * dblRetParam
            End If
            ' dblRetencion = SUConversiones.ConvierteADouble(dblSubtotal) * dblRetParam
            dblTotal = dblSubtotal + dblImpuesto - dblRetencion

            objTotalesFactura.SubTotal = dblSubtotal
            objTotalesFactura.TotalImpuesto = dblImpuesto
            objTotalesFactura.TotalRetencion = dblRetencion
            objTotalesFactura.Total = dblTotal

            TextBox9.Text = Format(objTotalesFactura.SubTotal, "#,##0.##00")
            TextBox10.Text = Format(objTotalesFactura.TotalImpuesto, "#,##0.##00")
            TextBox11.Text = Format(objTotalesFactura.TotalRetencion, "#,##0.##00")
            TextBox12.Text = Format(objTotalesFactura.Total, "#,##0.##00")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Timer1.Enabled = True
            objTotalesFactura.SubTotal = 0
            objTotalesFactura.TotalImpuesto = 0
            objTotalesFactura.TotalRetencion = 0
            objTotalesFactura.Total = 0
        End Try
        Return objTotalesFactura
    End Function

    Sub Guardar()

        Dim objTotalesFactura As SETotalesFactura = Nothing
        Dim objMaestroCompra As SEEncabezadoCompra = Nothing
        Dim objDetalleCompra As SEDetalleCompra = Nothing
        Dim lstObjDetalleCompra As List(Of SEDetalleCompra) = Nothing
        Dim lnSubTotal As Double = 0
        Dim lnTotal As Double = 0
        Dim lnImpuesto As Double = 0
        Dim lnRetencion As Double = 0
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing
        Dim frmLogin2 As frmLoginSuprvisor = Nothing
        Dim ldFechaDelDia As Date = Nothing
        Try
            ldFechaDelDia = New Date(Now.Year, Now.Month, Now.Day)
            objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                If Format(DPkFechaFactura.Value, "yyyyMMdd") <> Format(ldFechaDelDia, "yyyyMMdd") Then
                    If RNFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(lngRegUsuario) = 0 Then
                        If objParametroEmpresa IsNot Nothing Then
                            intResp = MsgBox("Usuario no tiene privilegio para realizar cambio de fecha. �Desea continuar?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Critical, "Ingreso Factura")
                            Select Case intResp
                        ' Case Windows.Forms.DialogResult.Yes
                                Case 6
                                    Select Case objParametroEmpresa.DesactivaObjetoFechaEnOperaciones.ToUpper
                                        Case "N"
                                            MsgBox("Usuario no tiene permisos para poder realizar cambio de fecha. ", MsgBoxStyle.Critical, "Facturas ")
                                            DPkFechaFactura.Value = ldFechaDelDia
                                            DPkFechaFactura.Focus()
                                            Return
                                        Case "S"
                                            frmLogin2 = New frmLoginSuprvisor()
                                            frmLogin2.ShowDialog()
                                            If lngRegUsuarioRelogin = -1 Then
                                                MsgBox("Usuario no tiene privilegio para cambiar la fecha para esta operaci�n", MsgBoxStyle.Critical)
                                                DPkFechaFactura.Value = ldFechaDelDia
                                                DPkFechaFactura.Focus()
                                                Return
                                                Exit Sub
                                            End If
                                            If RNFacturas.ValidaPermisoPermiteCambiarFechaEnOperaciones(lngRegUsuarioRelogin) = 0 Then
                                                MsgBox("Usuario no tiene privilegio para cambiar la fecha para esta operaci�n.", MsgBoxStyle.Critical)
                                                DPkFechaFactura.Value = ldFechaDelDia
                                                DPkFechaFactura.Focus()
                                                Return
                                                Exit Sub
                                            End If

                                        Case Else

                                    End Select
                                Case 7
                                    DPkFechaFactura.Value = ldFechaDelDia
                                    DPkFechaFactura.Focus()
                                    Return
                                    Exit Sub
                                Case Else
                            End Select
                        End If
                    End If
                End If
            End If


            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 13
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr

            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                If txtNumeroCompra.Text.Trim.Length <= 0 Then
                    MsgBox("No ha digitado el numero de compra", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
            End If

            'MSFlexGrid2.Col = 1
            If dgvxProductosCompras.IsCurrentCellDirty Then
                dgvxProductosCompras.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If dgvxProductosCompras.Rows.Count <= 0 Then
                MsgBox("No se pudo obtener el detalle de la compra ", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If

            For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1
                If IsNumeric(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value) = False Then
                    MsgBox("No es un valor numerico la cantidad a comprar", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                ElseIf SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value) <= 0 Then
                    MsgBox("La cantidad a comprar debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If

                If IsNumeric(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value) = False Then
                    MsgBox("No es un valor numerico el precio del producto", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                Else
                    If dgvxProductosCompras.Rows(intIncr).Cells("EsBonificacion").Value = 0 Then
                        If SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value) <= 0 Then
                            MsgBox("El precio del producto debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                            Exit Sub
                        End If
                    End If
                End If
            Next intIncr

            'If intTipoCompra = 4 Or intTipoCompra = 8 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                blnUbicar = False
                If UbicarProveedor(txtProveedor.Text) = False Then
                    MsgBox("El C�digo de proveedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                    txtProveedor.Focus()
                    Exit Sub
                End If
            End If

            Dim frmNew As New actrptViewer
            intResp = 0
            intImprimir = 0
            'If intTipoCompra = 1 Or intTipoCompra = 7 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Then
                If SUConversiones.ConvierteAInt(txtDias.Text) <= 0 Then
                    MsgBox("No puede generar una Compra sin d�as de cr�dito.", MsgBoxStyle.Critical, "Error en D�as")
                    txtDias.Focus()
                    Exit Sub
                End If
                If txtNombreProveedor.Text = "" Then
                    MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del proveedor.", MsgBoxStyle.Critical, "Error de Ingreso")
                    txtProveedor.Focus()
                    Exit Sub
                End If
            End If
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then        ' Del D�a
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then        ' Del D�a
                IdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
                'If lngRegAgencia = 1 Then
                If IdAgencia = 1 Then
                    intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Impresi�n de Compra")
                Else
                    intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Compra")
                End If
                If intResp = 2 Then     'Cancelar
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                ElseIf intResp = 7 Then 'No
                    intImprimir = 1
                End If
                'ElseIf intTipoCompra = 2 Or intTipoCompra = 5 Then    ' Imprimir Compra
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CONTADO Then    ' Imprimir Compra
                intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Compra")
                If intResp <> 6 Then 'Cancelar o No
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
                'ElseIf intTipoCompra = 3 Or intTipoCompra = 6 Then    ' Anular Compra
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Then    ' Anular Compra
                intResp = MsgBox("�Desea anular la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Anulaci�n de Compra")
                If intResp <> 6 Then 'No
                    txtNumeroCompra.Focus()
                    Exit Sub
                End If
                'ElseIf intTipoCompra = 7 Or intTipoCompra = 8 Then    ' Ingreso Manual
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then    ' Ingreso Manual
                intResp = MsgBox("�Desea guardar la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Compra")
                If intResp <> 6 Then    'Cancelar o No
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
                'ElseIf intTipoCompra = 9 Or intTipoCompra = 10 Then   ' Consultar Compra
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then   ' Consultar Compra
                intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Compra")
                If intResp <> 6 Then 'Cancelar o No
                    txtNumeroCompra.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
            End If

            Me.Cursor = Cursors.WaitCursor
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                ActualizarNumeroCompra(IdAgencia, intTipoCompra)
            End If
            intIncr = 0
            intIncr2 = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            dblSubtotal = 0
            dblImpuesto = 0
            dblRetencion = 0
            dblTotal = 0
            strFecVencFact = ""
            strVendedor = ""
            strNumeroFact = txtNumeroCompra.Text
            'If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            '    intDiaFact = Format(Now, "dd")
            '    strMesFact = Format(Now, "MMM")
            '    lngYearFact = Format(Now, "yyyy")
            '    lngFechaFactura = Format(Now, "yyyyMMdd")
            'ElseIf intTipoCompra <> 1 And intTipoCompra <> 4 Then
            '    intDiaFact = Format(DPkFechaFactura.Value, "dd")
            '    strMesFact = Format(DPkFechaFactura.Value, "MMM")
            '    lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
            '    lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")
            'End If
            intDiaFact = Format(DPkFechaFactura.Value, "dd")
            strMesFact = Format(DPkFechaFactura.Value, "MMM")
            lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
            lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")

            'If intTipoCompra = 4 Or intTipoCompra = 3 Or intTipoCompra = 8 Or intTipoCompra = 9 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Then
                strClienteFact = txtNombreProveedor.Text

                For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1 'Se Recorre el grid para ingresar en un array los campos a imprimir en la factura
                    For intIncr2 = 0 To 7
                        If intIncr2 = 6 Or intIncr2 = 7 Then
                            arrContado(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        Else
                            arrContado(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        End If
                    Next intIncr2
                Next intIncr

                'intRptImpCompras = intTipoCompra
                intRptImpCompras = 1
                'ElseIf intTipoCompra = 1 Or intTipoCompra = 6 Or intTipoCompra = 7 Or intTipoCompra = 10 Then
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                strCodCliFact = txtProveedor.Text
                strClienteFact = txtNombreProveedor.Text
                strFecVencFact = txtVence.Text
                strDiasFact = txtDias.Text
                If dgvxProductosCompras.IsCurrentCellDirty Then
                    dgvxProductosCompras.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1 'Se Recorre el grid para ingresar en un array los campos a imprimir en la factura
                    For intIncr2 = 0 To 7
                        If intIncr2 = 6 Or intIncr2 = 7 Then
                            arrCredito(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        Else
                            arrCredito(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        End If
                    Next intIncr2
                Next intIncr
                'intRptImpCompras = intTipoCompra
                intRptImpCompras = 4
            End If
            objTotalesFactura = CalculaTotalasFactura()
            If objTotalesFactura Is Nothing Then
                MsgBox("No se pudo obtener los totales de la factura.", MsgBoxStyle.Critical, "Error de Ingreso")
                txtCantidad.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If
            If objTotalesFactura.Total = 0 Then
                MsgBox("No se pudo obtener los totales de la factura.", MsgBoxStyle.Critical, "Error de Ingreso")
                txtCantidad.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If

            dblSubtotal = objTotalesFactura.SubTotal
            dblImpuesto = objTotalesFactura.TotalImpuesto
            dblRetencion = objTotalesFactura.TotalRetencion
            dblTotal = objTotalesFactura.Total

            TextBox9.Text = Format(dblSubtotal, "#,##0.####")
            TextBox10.Text = Format(dblImpuesto, "#,##0.####")
            TextBox11.Text = Format(dblRetencion, "#,##0.####")
            TextBox12.Text = Format(dblTotal, "#,##0.####")


            Dim lngNumFecha As Long = 0

            IdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
            lngNumFecha = 0
            lngRegistro = 0

            If objMaestroCompra Is Nothing Then
                objMaestroCompra = New SEEncabezadoCompra()
            End If
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
            lnSubTotal = 0
            dblTipoCambio = 0
            dblTipoCambio = UbicarTipoCambio(lngNumFecha)
            If dblTipoCambio = 0 Then
                dblTipoCambio = 1
            End If

            lnSubTotal = SUConversiones.ConvierteADouble(TextBox9.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.subtotal = Format((lnSubTotal * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.subtotal = Format(lnSubTotal, "#####0.#0")
            End If

            lnImpuesto = 0
            lnImpuesto = SUConversiones.ConvierteADouble(TextBox10.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.impuesto = Format((lnImpuesto * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.impuesto = Format(lnImpuesto, "#####0.#0")
            End If
            objMaestroCompra.registro = 0
            objMaestroCompra.numero = txtNumeroCompra.Text.Trim()
            objMaestroCompra.numfechaing = lngNumFecha
            objMaestroCompra.fechaingreso = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
            objMaestroCompra.vendregistro = SUConversiones.ConvierteAInt(txtNumeroVendedor.Text)
            objMaestroCompra.cliregistro = SUConversiones.ConvierteAInt(txtNumeroProveedor.Text)
            objMaestroCompra.clinombre = StrConv(txtNombreProveedor.Text, VbStrConv.Uppercase)
            lnRetencion = 0
            lnRetencion = SUConversiones.ConvierteADouble(TextBox11.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.retencion = Format((lnRetencion * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.retencion = Format(lnRetencion, "#####0.#0")
            End If
            lnTotal = 0
            lnTotal = SUConversiones.ConvierteADouble(TextBox12.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.total = Format((lnTotal * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.total = Format(lnTotal, "#####0.#0")
            End If
            objMaestroCompra.userregistro = lngRegUsuario
            objMaestroCompra.agenregistro = IdAgencia
            objMaestroCompra.impreso = intImprimir
            objMaestroCompra.deptregistro = 0
            objMaestroCompra.negregistro = 0
            objMaestroCompra.tipoCompra = intTipoCompra
            objMaestroCompra.diascredito = SUConversiones.ConvierteAInt(txtDias.Text)
            objMaestroCompra.tipocambio = dblTipoCambio
            Select Case intTipoCompra
                Case ENTipoCompra.COMPRA_IMPRIMIR_CREDITO, ENTipoCompra.COMPRA_IMPRIMIR_CONTADO
                    objMaestroCompra.status = 2
                Case ENTipoCompra.COMPRA_ANULAR_CONTADO, ENTipoCompra.COMPRA_ANULAR_CREDITO
                    objMaestroCompra.status = 1
                Case Else
                    objMaestroCompra.status = 0
            End Select
            objMaestroCompra.IdPedido = SUConversiones.ConvierteAInt(TextBox17.Text.Trim())
            lstObjDetalleCompra = New List(Of SEDetalleCompra)

            If dgvxProductosCompras.IsCurrentCellDirty Then
                dgvxProductosCompras.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1
                objDetalleCompra = New SEDetalleCompra()
                objDetalleCompra.registro = lngRegistro
                objDetalleCompra.prodregistro = SUConversiones.ConvierteAInt(dgvxProductosCompras.Rows(intIncr).Cells("IdProducto").Value)
                objDetalleCompra.cantidad = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value), "#####0.#0")
                If intFechaCambioCompra > 0 Then
                    objDetalleCompra.precio = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value) * dblTipoCambio, "###0.#0")
                Else
                    objDetalleCompra.precio = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value), "#####0.#0")
                End If
                If intFechaCambioCompra > 0 Then
                    objDetalleCompra.valor = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Valor").Value) * dblTipoCambio, "###0.#0")
                Else
                    objDetalleCompra.valor = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Valor").Value), "#####0.#0")
                End If
                If UCase(dgvxProductosCompras.Rows(intIncr).Cells("Impuesto").Value) = "SI" Then
                    If intFechaCambioCompra > 0 Then
                        objDetalleCompra.igv = Format(((objDetalleCompra.valor * dblTipoCambio) * dblPorcParam), "###0.#0")
                    Else
                        objDetalleCompra.igv = Format((objDetalleCompra.valor * dblPorcParam), "#####0.#0")
                    End If
                Else
                    objDetalleCompra.igv = 0.0
                End If
                objDetalleCompra.EsBonificacion = SUConversiones.ConvierteAInt(dgvxProductosCompras.Rows(intIncr).Cells("EsBonificacion").Value)
                'objDetalleCompra.Ubicacion = dgvxProductosCompras.Rows(intIncr).Cells("Ubicacion").Value
                lstObjDetalleCompra.Add(objDetalleCompra)
            Next
            Try
                'If intTipoCompra = 1 Or intTipoCompra = 2 Or intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                    'cmdTmp.ExecuteNonQuery()
                    strNumCompras = txtNumeroCompra.Text
                    nIdVet = SUConversiones.ConvierteAInt(txtNumeroProveedor.Text)
                End If
                'If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                'If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                '    GuardarDetalle1()
                'End If
                ''If intTipoCompra <> 9 And intTipoCompra <> 10 Then
                'If intTipoCompra <> ENTipoCompra.COMPRA_CONSULTAR_CONTADO And intTipoCompra <> ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                '    GuardarDetalle2()
                'End If
                RNCompra.IngresaTransaccionCompra(objMaestroCompra, lstObjDetalleCompra, intTipoCompra)
                MsgBox("Acci�n realizada exitosamente", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
                strValorFactCOR = ""
                strValorFactCOR = "C$ " & Format((dblTotal * dblTipoCambio), "#,##0.#0")
                'If intTipoCompra <> 3 And intTipoCompra <> 6 And intTipoCompra <> 7 And intTipoCompra <> 8 Then
                If intTipoCompra <> ENTipoCompra.COMPRA_ANULAR_CONTADO And intTipoCompra <> ENTipoCompra.COMPRA_ANULAR_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_MANUAL_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                    If intImprimir = 0 Then
                        If intTipoCompra = 4 Then
                            UbicarProveedor(txtProveedor.Text)
                        End If
                        '     If intRptImpCompras = 1 Then
                        Select Case intRptImpCompras
                            Case ENTipoCompra.COMPRA_CREDITO, ENTipoCompra.COMPRA_CONSULTAR_CREDITO, ENTipoCompra.COMPRA_IMPRIMIR_CREDITO, ENTipoCompra.COMPRA_MANUAL_CREDITO
                                strQuery = "spObtieneCompraImprimir '" & txtNumeroCompra.Text.Trim() & "', " & txtNumeroProveedor.Text & ", " & intRptImpCompras
                            Case ENTipoCompra.COMPRA_CONTADO, ENTipoCompra.COMPRA_MANUAL_CONTADO, ENTipoCompra.COMPRA_CONSULTAR_CONTADO, ENTipoCompra.COMPRA_IMPRIMIR_CONTADO
                                strQuery = "spObtieneCompraImprimir '" & txtNumeroCompra.Text.Trim() & "', " & txtNumeroProveedor.Text & ", " & intRptImpCompras
                        End Select

                        'If intRptImpCompras = ENTipoCompra.COMPRA_CREDITO Then
                        '    strQuery = "spObtieneCompraImprimir '" & strNumCompras & "', " & nIdVet & ", " & intRptImpCompras
                        '    ' ElseIf intRptImpCompras = 4 Then
                        'ElseIf intRptImpCompras = ENTipoCompra.COMPRA_CONTADO Then
                        '    strQuery = "spObtieneCompraImprimir '" & strNumCompras & "', " & nIdVet & ", " & intRptImpCompras
                        'End If
                        frmNew.Show()
                        'If intImpresoraAsignada = 0 Then
                        '    frmNew.WindowState = FormWindowState.Minimized
                        '    frmNew.Close()
                        '    strNumCompras = String.Empty
                        'End If
                    End If
                End If
                Limpiar()
                Limpiar2()
                DPkFechaFactura.Value = Now
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                Me.Cursor = Cursors.Default
            Finally
                'If cmdTmp.Connection.State = ConnectionState.Open Then
                '    cmdTmp.Connection.Close()
                'End If
                'If cnnAgro2K.State = ConnectionState.Open Then
                '    cnnAgro2K.Close()
                'End If
            End Try
            txtNumeroCompra.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default

    End Sub


    Sub Limpiar()

        Try
            txtNumeroCompra.Enabled = True
            intIncr = 0
            For intIncr = 1 To 6
                txtCollection.Item(intIncr).Text = ""
            Next intIncr
            intIncr = 0
            For intIncr = 7 To 12
                txtCollection.Item(intIncr).Text = "0.00"
            Next intIncr
            For intIncr = 13 To txtCollection.Count
                txtCollection.Item(intIncr).Text = "0"
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 13
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            ddlRetencion.SelectedIndex = 0
            txtDias.Text = "30"
            txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
            DPkFechaFactura.Focus()
            Select Case intTipoCompra
                Case 4, 5, 3, 8, 9 : Label3.Visible = False : Label4.Visible = False : txtDias.Visible = False : txtVence.Visible = False
                    txtProveedor.Visible = True : txtNombreProveedor.ReadOnly = True : txtNombreProveedor.Width = 280 ': txtNombreProveedor.Location = New Point(138, 48)
                Case 1, 2, 6, 7, 10 : Label3.Visible = True : Label4.Visible = True : txtDias.Visible = True : txtVence.Visible = True
            End Select
            Select Case intTipoCompra
                Case 1, 4 : txtNumeroCompra.Text = "" 'txtNumeroCompra.Text = ExtraerNumeroCompra(lngRegAgencia, intTipoCompra)
                Case 2, 3, 5, 6 : BloquearCampos()
                Case 9, 10 : BloquearCampos()
            End Select
            medPrecio.Text = 0
            dgvxProductosCompras.Rows.Clear()
            strCurrency = String.Empty
            DPkFechaFactura.Focus()
            txtNumeroCompra.ReadOnly = False
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub Limpiar2()

        txtProducto.Text = ""
        txtCantidad.Text = "0"
        'medPrecio.Text = "0.00"
        strCurrency = String.Empty
        medPrecio.Text = 0
        'txtNumeroCompra.Focus()
        txtCantidad.Focus()

    End Sub

    Sub BloquearCampos()

        For intIncr = 1 To txtCollection.Count - 1
            txtCollection.Item(intIncr).readonly = True
        Next
        DPkFechaFactura.Enabled = False
        ddlRetencion.Enabled = False
        If intTipoCompra = 3 Or intTipoCompra = 6 Then
            'txtNumeroCompra.ReadOnly = False
            txtProveedor.ReadOnly = False
        End If

    End Sub


    Private Function CalculaTotalesCompra() As SETotalesCompra
        Dim objTotalesCompra As SETotalesCompra = Nothing
        Dim dblSubtotal As Double = 0
        Dim dblTotal As Double = 0
        Dim dblImpuesto As Double = 0
        Dim dblRetencion As Double = 0
        Dim lnCantidad As Double = 0
        Dim intIncr As Integer = 0
        Dim lnImpuesto As Double = 0
        Dim lsIndicaImpuesto As String = String.Empty
        Dim lnPrecioUnitario As Double = 0
        Try
            objTotalesCompra = New SETotalesCompra()

            objTotalesCompra.SubTotal = 0
            objTotalesCompra.TotalImpuesto = 0
            objTotalesCompra.TotalRetencion = 0
            objTotalesCompra.Total = 0

            If dgvxProductosCompras.Rows IsNot Nothing Then
                If dgvxProductosCompras.IsCurrentCellDirty Then
                    dgvxProductosCompras.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                If dgvxProductosCompras.Rows.Count > 0 Then
                    For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1
                        lnImpuesto = 0
                        lsIndicaImpuesto = String.Empty
                        lnCantidad = 0
                        lnPrecioUnitario = 0
                        lnCantidad = lnCantidad + SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value)
                        lnPrecioUnitario = lnPrecioUnitario + SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value)
                        dblSubtotal = dblSubtotal + (lnCantidad * lnPrecioUnitario)
                        lsIndicaImpuesto = dgvxProductosCompras.Rows(intIncr).Cells("Impuesto").Value
                        If lsIndicaImpuesto.Trim().ToUpper = "SI" Then
                            lnImpuesto = (lnCantidad * lnPrecioUnitario) * dblPorcParam
                        End If
                        dblImpuesto = dblImpuesto + lnImpuesto
                    Next
                End If
            End If
            dblRetencion = 0
            If ddlRetencion.SelectedIndex = 1 Then
                dblRetencion = dblSubtotal * dblRetParam
            End If
            ' dblRetencion = SUConversiones.ConvierteADouble(dblSubtotal) * dblRetParam
            dblTotal = dblSubtotal + dblImpuesto - dblRetencion

            objTotalesCompra.SubTotal = dblSubtotal
            objTotalesCompra.TotalImpuesto = dblImpuesto
            objTotalesCompra.TotalRetencion = dblRetencion
            objTotalesCompra.Total = dblTotal
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Timer1.Enabled = True
            objTotalesCompra.SubTotal = 0
            objTotalesCompra.TotalImpuesto = 0
            objTotalesCompra.TotalRetencion = 0
            objTotalesCompra.Total = 0
        End Try
        Return objTotalesCompra
    End Function
    Sub Anular()

        Dim objTotalesCompra As SETotalesCompra = Nothing
        Dim objMaestroCompra As SEEncabezadoCompra = Nothing
        Dim objDetalleCompra As SEDetalleCompra = Nothing
        Dim lstObjDetalleCompra As List(Of SEDetalleCompra) = Nothing
        Dim objParametroEmpresa As SEParametroEmpresa = Nothing
        Dim lnSubTotal As Double = 0
        Dim lnTotal As Double = 0
        Dim lnImpuesto As Double = 0
        Dim lnRetencion As Double = 0
        Try
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 13
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr

            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                If txtNumeroCompra.Text.Trim.Length <= 0 Then
                    MsgBox("No ha digitado el numero de compra", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If
            End If
            If Label16.Visible = True Then
                MsgBox("No puede anular una Compra anulada.  Verifique.", MsgBoxStyle.Critical, "Compra Anulada")
                Exit Sub
            End If

            objParametroEmpresa = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            If objParametroEmpresa IsNot Nothing Then
                If objParametroEmpresa.IndicaAnulaCompraMesAnterior = 0 Then
                    If Year(DPkFechaFactura.Value) = Year(Now) And Month(DPkFechaFactura.Value) = Month(Now) Then
                    Else
                        MsgBox("La Compra a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Compra Inv�lida")
                        Exit Sub
                    End If
                End If
            Else
                MsgBox("La Compra a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Compra Inv�lida")
                Exit Sub
            End If
            'MSFlexGrid2.Col = 1
            For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1

                If IsNumeric(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value) = False Then
                    MsgBox("No es un valor numerico la cantidad a comprar", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                ElseIf SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value) <= 0 Then
                    MsgBox("La cantidad a comprar debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                End If

                If IsNumeric(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value) = False Then
                    MsgBox("No es un valor numerico el precio del producto", MsgBoxStyle.Critical, "Error de Dato")
                    Exit Sub
                Else
                    If dgvxProductosCompras.Rows(intIncr).Cells("EsBonificacion").Value = 0 Then
                        If SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value) <= 0 Then
                            MsgBox("El precio del producto debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                            Exit Sub
                        End If
                    End If
                End If
            Next intIncr

            'If intTipoCompra = 4 Or intTipoCompra = 8 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                blnUbicar = False
                If UbicarProveedor(txtProveedor.Text) = False Then
                    MsgBox("El C�digo de proveedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                    txtProveedor.Focus()
                    Exit Sub
                End If
            End If

            Dim frmNew As New actrptViewer
            intResp = 0
            intImprimir = 0
            'If intTipoCompra = 1 Or intTipoCompra = 7 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Then
                If SUConversiones.ConvierteAInt(txtDias.Text) <= 0 Then
                    MsgBox("No puede generar una Compra sin d�as de cr�dito.", MsgBoxStyle.Critical, "Error en D�as")
                    txtDias.Focus()
                    Exit Sub
                End If
                If txtNombreProveedor.Text = "" Then
                    MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del proveedor.", MsgBoxStyle.Critical, "Error de Ingreso")
                    txtProveedor.Focus()
                    Exit Sub
                End If
            End If
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then        ' Del D�a
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then        ' Del D�a
                IdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
                'If lngRegAgencia = 1 Then
                'If IdAgencia = 1 Then
                '    intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Impresi�n de Compra")
                'Else
                '    intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Compra")
                'End If
                intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Impresi�n de Compra")
                If intResp = 2 Then     'Cancelar
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                ElseIf intResp = 7 Then 'No
                    intImprimir = 1
                End If
                'ElseIf intTipoCompra = 2 Or intTipoCompra = 5 Then    ' Imprimir Compra
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CONTADO Then    ' Imprimir Compra
                intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Compra")
                If intResp <> 6 Then 'Cancelar o No
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
                'ElseIf intTipoCompra = 3 Or intTipoCompra = 6 Then    ' Anular Compra
                'ElseIf intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Then    ' Anular Compra
                '    intResp = MsgBox("�Desea anular la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Anulaci�n de Compra")
                '    If intResp <> 6 Then 'No
                '        txtNumeroCompra.Focus()
                '        Exit Sub
                '    End If
                '    'ElseIf intTipoCompra = 7 Or intTipoCompra = 8 Then    ' Ingreso Manual
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then    ' Ingreso Manual
                intResp = MsgBox("�Desea guardar la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Compra")
                If intResp <> 6 Then    'Cancelar o No
                    txtCantidad.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
                'ElseIf intTipoCompra = 9 Or intTipoCompra = 10 Then   ' Consultar Compra
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then   ' Consultar Compra
                intResp = MsgBox("�Desea imprimir la Compra en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Compra")
                If intResp <> 6 Then 'Cancelar o No
                    txtNumeroCompra.Focus()
                    Exit Sub
                ElseIf intResp = 6 Then 'Yes
                    intImprimir = 0
                End If
            End If

            Me.Cursor = Cursors.WaitCursor
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                ActualizarNumeroCompra(IdAgencia, intTipoCompra)
            End If
            intIncr = 0
            intIncr2 = 0
            intRptFactura = 0
            intRptInventario = 0
            intRptImpFactura = 0
            dblSubtotal = 0
            dblImpuesto = 0
            dblRetencion = 0
            dblTotal = 0
            strFecVencFact = ""
            strVendedor = ""
            strNumeroFact = txtNumeroCompra.Text
            'If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
            'If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
            '    intDiaFact = Format(Now, "dd")
            '    strMesFact = Format(Now, "MMM")
            '    lngYearFact = Format(Now, "yyyy")
            '    lngFechaFactura = Format(Now, "yyyyMMdd")
            '    'ElseIf intTipoCompra <> 1 And intTipoCompra <> 4 Then
            'ElseIf intTipoCompra <> ENTipoCompra.COMPRA_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_CONTADO Then
            '    intDiaFact = Format(DPkFechaFactura.Value, "dd")
            '    strMesFact = Format(DPkFechaFactura.Value, "MMM")
            '    lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
            '    lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")
            'End If
            intDiaFact = Format(DPkFechaFactura.Value, "dd")
            strMesFact = Format(DPkFechaFactura.Value, "MMM")
            lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
            lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")
            'If intTipoCompra = 4 Or intTipoCompra = 3 Or intTipoCompra = 8 Or intTipoCompra = 9 Then
            If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Then
                strClienteFact = txtNombreProveedor.Text
                For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1 'Se Recorre el grid para ingresar en un array los campos a imprimir en la factura
                    For intIncr2 = 0 To 7
                        If intIncr2 = 6 Or intIncr2 = 7 Then
                            arrContado(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        Else
                            arrContado(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        End If
                    Next intIncr2
                Next intIncr
                intRptImpCompras = intTipoCompra

                'ElseIf intTipoCompra = 1 Or intTipoCompra = 6 Or intTipoCompra = 7 Or intTipoCompra = 10 Then
            ElseIf intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                strCodCliFact = txtProveedor.Text
                strClienteFact = txtNombreProveedor.Text
                strFecVencFact = txtVence.Text
                strDiasFact = txtDias.Text
                For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1 'Se Recorre el grid para ingresar en un array los campos a imprimir en la factura
                    For intIncr2 = 0 To 7
                        If intIncr2 = 6 Or intIncr2 = 7 Then
                            arrCredito(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        Else
                            arrCredito(intIncr, intIncr2) = dgvxProductosCompras.Rows(intIncr).Cells(intIncr2).Value
                        End If
                    Next intIncr2
                Next intIncr
                intRptImpCompras = intTipoCompra
            End If
            objTotalesCompra = CalculaTotalesCompra()
            If objTotalesCompra.Total = 0 Then
                MsgBox("No se pudo obtener los totales de la factura.", MsgBoxStyle.Critical, "Error de Ingreso")
                txtCantidad.Focus()
                Timer1.Enabled = True
                Exit Sub
            End If

            dblSubtotal = objTotalesCompra.SubTotal
            dblImpuesto = objTotalesCompra.TotalImpuesto
            dblRetencion = objTotalesCompra.TotalRetencion
            dblTotal = objTotalesCompra.Total

            TextBox9.Text = Format(dblSubtotal, "#,##0.#0")
            TextBox10.Text = Format(dblImpuesto, "#,##0.#0")
            TextBox11.Text = Format(dblRetencion, "#,##0.#0")
            TextBox12.Text = Format(dblTotal, "#,##0.#0")

            'dblSubtotal = SUConversiones.ConvierteADouble(TextBox9.Text)
            'dblImpuesto = SUConversiones.ConvierteADouble(TextBox10.Text)
            'dblRetencion = SUConversiones.ConvierteADouble(TextBox11.Text)
            'dblTotal = SUConversiones.ConvierteADouble(TextBox12.Text)
            strValorFactCOR = ""
            strValorFactCOR = "C$ " & Format((dblTotal * dblTipoCambio), "#,##0.#0")
            'If intTipoCompra <> 3 And intTipoCompra <> 6 And intTipoCompra <> 7 And intTipoCompra <> 8 Then
            If intTipoCompra <> ENTipoCompra.COMPRA_ANULAR_CONTADO And intTipoCompra <> ENTipoCompra.COMPRA_ANULAR_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_MANUAL_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                If intImprimir = 0 Then
                    ' If intTipoCompra = 9 Or intTipoCompra = 10 Then
                    If intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                        frmNew.Show()
                        If intImpresoraAsignada = 1 Then
                            Exit Sub
                        End If
                        frmNew.WindowState = FormWindowState.Minimized
                        Me.Cursor = Cursors.Default
                        frmNew.Close()
                        Exit Sub
                    End If
                End If
            End If

            Dim lngNumFecha As Long = 0

            IdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
            lngNumFecha = 0
            lngRegistro = 0

            If objMaestroCompra Is Nothing Then
                objMaestroCompra = New SEEncabezadoCompra()
            End If
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
            lnSubTotal = 0
            dblTipoCambio = 0
            dblTipoCambio = UbicarTipoCambio(lngNumFecha)
            If dblTipoCambio = 0 Then
                dblTipoCambio = 1
            End If

            lnSubTotal = SUConversiones.ConvierteADouble(TextBox9.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.subtotal = Format((lnSubTotal * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.subtotal = Format(lnSubTotal, "#####0.#0")
            End If

            lnImpuesto = 0
            lnImpuesto = SUConversiones.ConvierteADouble(TextBox10.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.impuesto = Format((lnImpuesto * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.impuesto = Format(lnImpuesto, "#####0.#0")
            End If
            objMaestroCompra.registro = 0
            objMaestroCompra.numero = txtNumeroCompra.Text
            objMaestroCompra.numfechaing = lngNumFecha
            objMaestroCompra.fechaingreso = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
            objMaestroCompra.vendregistro = SUConversiones.ConvierteAInt(txtNumeroVendedor.Text)
            objMaestroCompra.cliregistro = SUConversiones.ConvierteAInt(txtNumeroProveedor.Text)
            objMaestroCompra.clinombre = StrConv(txtNombreProveedor.Text, VbStrConv.Uppercase)
            lnRetencion = 0
            lnRetencion = SUConversiones.ConvierteADouble(TextBox11.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.retencion = Format((lnRetencion * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.retencion = Format(lnRetencion, "#####0.#0")
            End If
            lnTotal = 0
            lnTotal = SUConversiones.ConvierteADouble(TextBox12.Text)
            If intFechaCambioCompra > 0 Then
                objMaestroCompra.total = Format((lnTotal * dblTipoCambio), "#####0.#0")
            Else
                objMaestroCompra.total = Format(lnTotal, "#####0.#0")
            End If
            objMaestroCompra.userregistro = lngRegUsuario
            objMaestroCompra.agenregistro = IdAgencia
            objMaestroCompra.impreso = intImprimir
            objMaestroCompra.deptregistro = 0
            objMaestroCompra.negregistro = 0
            objMaestroCompra.tipoCompra = intTipoCompra
            objMaestroCompra.diascredito = SUConversiones.ConvierteAInt(txtDias.Text)
            objMaestroCompra.tipocambio = dblTipoCambio
            Select Case intTipoCompra
                Case ENTipoCompra.COMPRA_IMPRIMIR_CREDITO, ENTipoCompra.COMPRA_IMPRIMIR_CONTADO
                    objMaestroCompra.status = 2
                Case ENTipoCompra.COMPRA_ANULAR_CONTADO, ENTipoCompra.COMPRA_ANULAR_CREDITO
                    objMaestroCompra.status = 1
                Case Else
                    objMaestroCompra.status = 0
            End Select

            lstObjDetalleCompra = New List(Of SEDetalleCompra)


            If dgvxProductosCompras.IsCurrentCellDirty Then
                dgvxProductosCompras.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            For intIncr = 0 To dgvxProductosCompras.Rows.Count - 1
                objDetalleCompra = New SEDetalleCompra()
                objDetalleCompra.registro = lngRegistro
                objDetalleCompra.prodregistro = SUConversiones.ConvierteAInt(dgvxProductosCompras.Rows(intIncr).Cells("IdProducto").Value)
                objDetalleCompra.cantidad = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Cantidad").Value), "#####0.#0")
                If intFechaCambioCompra > 0 Then
                    objDetalleCompra.precio = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value) * dblTipoCambio, "###0.#0")
                Else
                    objDetalleCompra.precio = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Precio").Value), "#####0.#0")
                End If
                If intFechaCambioCompra > 0 Then
                    objDetalleCompra.valor = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Valor").Value) * dblTipoCambio, "###0.#0")
                Else
                    objDetalleCompra.valor = Format(SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(intIncr).Cells("Valor").Value), "#####0.#0")
                End If
                If UCase(dgvxProductosCompras.Rows(intIncr).Cells("Impuesto").Value) = "SI" Then
                    If intFechaCambioCompra > 0 Then
                        objDetalleCompra.igv = Format(((objDetalleCompra.valor * dblTipoCambio) * dblPorcParam), "###0.#0")
                    Else
                        objDetalleCompra.igv = Format((objDetalleCompra.valor * dblPorcParam), "#####0.#0")
                    End If
                Else
                    objDetalleCompra.igv = 0.0
                End If
                objDetalleCompra.EsBonificacion = SUConversiones.ConvierteAInt(dgvxProductosCompras.Rows(intIncr).Cells("EsBonificacion").Value)
                ' objDetalleCompra.Ubicacion = dgvxProductosCompras.Rows(intIncr).Cells("Ubicacion").Value
                objDetalleCompra.Ubicacion = String.Empty
                lstObjDetalleCompra.Add(objDetalleCompra)
            Next

            Try
                'If intTipoCompra = 1 Or intTipoCompra = 2 Or intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = 2 Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = 5 Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                    strNumCompras = txtNumeroCompra.Text
                    nIdVet = SUConversiones.ConvierteAInt(txtNumeroProveedor.Text)
                End If
                'If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                'If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                '    GuardarDetalle1()
                'End If
                ''If intTipoCompra <> 9 And intTipoCompra <> 10 Then
                'If intTipoCompra <> ENTipoCompra.COMPRA_CONSULTAR_CONTADO And intTipoCompra <> ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                '    GuardarDetalle2()
                'End If
                RNCompra.IngresaTransaccionCompra(objMaestroCompra, lstObjDetalleCompra, intTipoCompra)
                MsgBox("Registro Ingresado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
                'If intTipoCompra <> 3 And intTipoCompra <> 6 And intTipoCompra <> 7 And intTipoCompra <> 8 Then
                If intTipoCompra <> ENTipoCompra.COMPRA_ANULAR_CONTADO And intTipoCompra <> ENTipoCompra.COMPRA_ANULAR_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_MANUAL_CREDITO And intTipoCompra <> ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                    If intImprimir = 0 Then
                        'If intTipoCompra = 4 Then
                        If intTipoCompra = ENTipoCompra.COMPRA_CONTADO Then
                            UbicarProveedor(txtProveedor.Text)
                        End If
                        'If intRptImpCompras = 1 Then
                        If intRptImpCompras = ENTipoCompra.COMPRA_CREDITO Then
                            strQuery = "spObtieneCompraImprimir '" & strNumCompras & "', " & nIdVet & ", " & intRptImpCompras
                            'ElseIf intRptImpCompras = 4 Then
                        ElseIf intRptImpCompras = ENTipoCompra.COMPRA_CONTADO Then
                            strQuery = "spObtieneCompraImprimir '" & strNumCompras & "', " & nIdVet & ", " & intRptImpCompras
                        End If
                        frmNew.Show()
                        If intImpresoraAsignada = 0 Then
                            frmNew.WindowState = FormWindowState.Minimized
                            frmNew.Close()
                            strNumCompras = String.Empty
                        End If
                    End If
                End If
                Limpiar()
                Limpiar2()
                DPkFechaFactura.Value = Now
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                Me.Cursor = Cursors.Default
            Finally
                'If cmdTmp.Connection.State = ConnectionState.Open Then
                '    cmdTmp.Connection.Close()
                'End If
                'If cnnAgro2K.State = ConnectionState.Open Then
                '    cnnAgro2K.Close()
                'End If
            End Try
            txtNumeroCompra.Focus()
            txtNombreProveedor.Text = String.Empty
            txtProveedor.Text = String.Empty
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Me.Cursor = Cursors.Default
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Function UbicarProducto(ByVal strDato As String) As Boolean

        Try
            strDescrip = ""
            strUnidad = ""
            lngRegistro = 0
            intImpuesto = 0
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.BuscarProductoxCodigo(strDato)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    strDescrip = dtrAgro2K.Item("Descripcion")
                    strUnidad = dtrAgro2K.Item("Codigo")
                    lngRegistro = dtrAgro2K.Item("Registro")
                    intImpuesto = dtrAgro2K.Item("Impuesto")
                End While

            End If
            'If dtrAgro2K IsNot Nothing Then
            '    dtrAgro2K.Close()
            'End If

            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True
            UbicarPrecio()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Function

    Function UbicarProveedor(ByVal strDato As String) As Boolean
        Dim clPedidoCompra As RNPedidoCompra = Nothing
        Dim lnIdAgencia As Integer = 0
        Try
            lnIdAgencia = SUConversiones.ConvierteAInt(lngRegAgencia)
            ' clPedidoCompra = New RNPedidoCompra()
            strDirecFact = ""
            txtNumeroProveedor.Text = "0"
            'strQuery = ""
            'strQuery = " exec ObtieneProveedores '" & txtProveedor.Text.Trim() & "'"

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'cmdAgro2K.CommandText = strQuery
            'dtrAgro2K = cmdAgro2K.ExecuteReader
            'dtrAgro2K = Nothing
            dtrAgro2K = RNProveedor.ObtieneProveedorxCodigo(txtProveedor.Text.Trim())
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    'txtNombreProveedor.Text = dtrAgro2K.GetValue(0)
                    'txtNumeroProveedor.Text = dtrAgro2K.GetValue(1)
                    'txtProveedor.Text = dtrAgro2K.GetValue(2)
                    txtNombreProveedor.Text = dtrAgro2K.Item("nombre")
                    txtNumeroProveedor.Text = dtrAgro2K.Item("registro")
                    txtProveedor.Text = dtrAgro2K.Item("Codigo")
                    RNPedidoCompra.CargarComboPedidosCompraPendientexAgenciaYProveedor(cbPedidoCompra, lngRegUsuario, lnIdAgencia, SUConversiones.ConvierteAInt(dtrAgro2K.Item("registro").ToString().Trim()))
                End While
                'dtrAgro2K.Close()
                'cmdAgro2K.Connection.Close()
                'cnnAgro2K.Close()
                If txtNumeroProveedor.Text = "0" Then
                    UbicarProveedor = False
                    Exit Function
                End If
                UbicarProveedor = True
                'dtrAgro2K = Nothing

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Function

    Sub EliminarRegistroGrid()
        'Dim strCodigo As String
        'Dim strPrecio As String
        'Dim strCantidad As String
        Dim dblCantidad As Double
        'Dim dblPrecio As Double
        'Dim dblValor As Double
        'Dim dblImpuesto As Double
        Dim objTotales As SETotalesFactura = Nothing

        Try
            If dgvxProductosCompras.Rows.Count > 0 And (intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8) Then
                intResp = MsgBox("�Est� Seguro de Eliminar el Registro?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Eliminaci�n de Registros")

                If intResp = 6 Then

                    dgvxProductosCompras.Rows.Remove(dgvxProductosCompras.CurrentRow)
                    objTotales = CalculaTotalasFactura()

                    If dgvxProductosCompras.CurrentRow IsNot Nothing Then
                        dblCantidad = SUConversiones.ConvierteADouble(dgvxProductosCompras.CurrentRow.Cells("Cantidad").Value)
                    End If
                    If objTotales IsNot Nothing Then
                        TextBox9.Text = Format(objTotales.SubTotal, "#,##0.####")

                        TextBox10.Text = Format(objTotales.TotalImpuesto, "#,##0.####")
                        TextBox12.Text = Format(objTotales.Total, "#,##0.####")
                        TextBox11.Text = Format(objTotales.TotalRetencion, "#,##0.####")
                    End If
                End If
            End If
            txtCantidad.Focus()
            txtCantidad.SelectAll()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub



    Private Sub FuncionTimer()
        Try
            If blnUbicar Then

                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 1
                            If txtProveedor.Visible = True Then
                                txtProveedor.Focus()
                            Else
                                txtNombreProveedor.Focus()
                            End If
                        Case 7 : txtProveedor.Text = strUbicar : UbicarProveedor(txtProveedor.Text)
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : medPrecio.Focus()
                        Case 4 : txtNumeroCompra.Text = strUbicar : ExtraerCompra()
                    End Select
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try

            If lngRegAgencia = 0 Then
                Timer1.Enabled = False
                Me.Close()
            ElseIf blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 1
                            If txtProveedor.Visible = True Then
                                txtProveedor.Focus()
                            Else
                                txtNombreProveedor.Focus()
                            End If
                        Case 7 : txtProveedor.Text = strUbicar : UbicarProveedor(txtProveedor.Text)
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : medPrecio.Focus()
                        Case 4 : txtNumeroCompra.Text = strUbicar : ExtraerCompra()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombreProveedor.LostFocus

        txtNombreProveedor.Text = StrConv(txtNombreProveedor.Text, VbStrConv.ProperCase)

    End Sub

    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DPkFechaFactura.LostFocus

        'If cnnAgro2K.State = ConnectionState.Open Then
        '    cnnAgro2K.Close()
        'End If
        Try
            SUFunciones.CierraConexionBD(cnnAgro2K)
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(DPkFechaFactura.Value, "yyyyMMdd") & " "
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                dblTipoCambio = dtrAgro2K.GetValue(0)
            End While
            'dtrAgro2K.Close()
            'cmdAgro2K.Connection.Close()
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexioDR(dtrAgro2K)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try

    End Sub

    Private Sub TextBox18_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProveedor.DoubleClick, txtProducto.DoubleClick ', txtProveedor.DoubleClick
        Try
            intListadoAyuda = 0
            If MenuItem5.Visible = True Then
                intListadoAyuda = 1
            ElseIf MenuItem6.Visible = True Then
                intListadoAyuda = 7
            ElseIf MenuItem7.Visible = True Then
                intListadoAyuda = 3
            End If
            If intListadoAyuda <> 0 Then
                Dim frmNew As New frmListadoAyuda
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try

        'Select Case sender.name
        '    Case "txtProveedor"
        '        intListadoAyuda = 7
        'End Select

    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        Try
            gbGuardar = True
            If dgvxProductosCompras.Rows.Count > 0 Then
                If cmbAgencias.SelectedValue = 0 Then
                    MsgBox("Debe Seleccionar una Sucursal a la cual se le Ingresara la Compra", MsgBoxStyle.Exclamation, "Guardar Compras")
                    Exit Sub
                Else
                    Guardar()
                    'txtNumeroCompra.Enabled = False
                End If
            Else
                MsgBox("No hay Producto a Ingresar - Ingrese Producto a la Tabla...", MsgBoxStyle.Information, "Grabar Compas")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try


    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
            txtProveedor.Text = ""
            txtNumeroProveedor.Text = ""
            txtNombreProveedor.Text = ""
            'txtNumeroCompra.Enabled = False

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'Throw ex
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Return
        End Try


    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub CmdAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAnular.Click
        Dim strQ As String = ""
        Dim ValidoAnul As Integer = 0
        Try
            intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            IdAgencia = SUConversiones.ConvierteAInt(cmbAgencias.SelectedValue)
            'If MSFlexGrid2.Rows <= 1 Then
            If dgvxProductosCompras.Rows.Count <= 0 Then
                MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                Exit Sub
            End If

            intResp = MsgBox("�Est� Seguro de Anular esta Compra?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Acci�n")

            If intResp = 6 Then
                If intTipoCompra = 6 Then   'Anulamos Compras al credito

                    strQ = "EXEC spValidarCompraSinRecibo '" & txtNumeroCompra.Text.Trim() & "', " & SUConversiones.ConvierteAInt(txtNumeroProveedor.Text.Trim)
                    If cnnAgro2K.State = ConnectionState.Open Then
                        cnnAgro2K.Close()
                    End If

                    cnnAgro2K.Open()
                    Try
                        cmdAgro2K.CommandText = strQ
                        dtrAgro2K = cmdAgro2K.ExecuteReader()
                        While dtrAgro2K.Read
                            ValidoAnul = dtrAgro2K.Item(0)
                        End While
                        dtrAgro2K.Close()

                        If ValidoAnul = 0 Then
                            Anular()
                            MsgBox("Compra fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Compra")
                            txtNumeroCompra.Focus()
                        Else
                            MsgBox("Compra no Puede se Anulada, Hay Recibos Emitidos..", MsgBoxStyle.Critical, "Anulaci�n de Compra")
                        End If

                    Catch exc As Exception
                        intError = 1
                        MsgBox(exc.Message.ToString)
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End Try

                ElseIf intTipoCompra = 3 Then   'Anulamos las Compras de contado
                    Anular()
                    MsgBox("Compra fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Compra")
                    txtNumeroCompra.Focus()
                End If
            Else
                MsgBox("Se Ha Cancelado la Anulacion de Compra", MsgBoxStyle.Exclamation, "Confirmaci�n de Acci�n")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click

        Try
            intTipoCompra = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            IdAgencia = ConvierteAInt(cmbAgencias.SelectedValue)
            If dgvxProductosCompras.Rows.Count <= 0 Then
                MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                txtCantidad.Focus()
                Exit Sub
            End If
            Guardar()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try

    End Sub

    Private Sub dgvxProductosCompras_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvxProductosCompras.DoubleClick
        Try
            EliminarRegistroGrid()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try

    End Sub

    Private Sub chkBonificacion_CheckedChanging(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.Controls.CheckBoxXChangeEventArgs) Handles chkBonificacion.CheckedChanging
        chk = chkBonificacion.Checked
    End Sub

    Private Sub chkBonificacion_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkBonificacion.KeyPress

    End Sub

    Private Sub dgvxProductosCompras_CellBeginEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvxProductosCompras.CellBeginEdit
        'CantidadAnterior = dgvxProductosCompras.CurrentCell.Value
        'valorAnt = dgvxProductosCompras.CurrentRow.Cells("Precio").Value
    End Sub

    Private Sub dgvxProductosCompras_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvxProductosCompras.CellEndEdit
        Dim i As Integer
        Dim TotalAnterior, ImpuestoAnterior As Double

        Try
            If dgvxProductosCompras.CurrentCell.ColumnIndex = 1 Or
                dgvxProductosCompras.CurrentCell.ColumnIndex = 5 Then

                If dgvxProductosCompras.Rows(i).Cells("Impuesto").Value.ToString().ToLower() <> "no" And
                    dgvxProductosCompras.Rows(i).Cells("Impuesto").Value.ToString().ToLower() <> "si" Then

                    MsgBox("El valor del impuesto debe SI/NO", MsgBoxStyle.Exclamation, "Compras")
                End If

                dgvxProductosCompras.CurrentRow.Cells("Valor").Value = dgvxProductosCompras.CurrentRow.Cells("Cantidad").Value * dgvxProductosCompras.CurrentRow.Cells("Precio").Value
                TotalAnterior = 0
                TextBox9.Text = 0
                ImpuestoAnterior = 0
                TextBox10.Text = 0
                For i = 0 To dgvxProductosCompras.Rows.Count - 1
                    TotalAnterior = ConvierteADouble(TextBox9.Text)
                    TextBox9.Text = Format(Format((SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Precio").Value) * SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Cantidad").Value)), "#,##0.####") + TotalAnterior, "#,##0.####")

                    If dgvxProductosCompras.Rows(i).Cells("Impuesto").Value.ToString().ToLower() = "no" Then
                        TextBox10.Text = Format(SUConversiones.ConvierteADouble(TextBox10.Text), "#,##0.####")
                    Else
                        ImpuestoAnterior = ConvierteADouble(TextBox10.Text)
                        TextBox10.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Precio").Value) * SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Cantidad").Value)), "#,##0.####") * dblPorcParam), "#,##0.####") + ImpuestoAnterior, "#,##0.####")
                    End If

                    TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.####")

                Next
                Limpiar2()
            ElseIf dgvxProductosCompras.CurrentCell.ColumnIndex = 6 Then
                If dgvxProductosCompras.Rows(i).Cells("Impuesto").Value.ToString().ToLower() <> "no" And
        dgvxProductosCompras.Rows(i).Cells("Impuesto").Value.ToString().ToLower() <> "si" Then

                    MsgBox("El valor del impuesto debe SI/NO", MsgBoxStyle.Exclamation, "Compras")
                End If
                TotalAnterior = 0
                TextBox9.Text = 0
                ImpuestoAnterior = 0
                TextBox10.Text = 0
                If dgvxProductosCompras.CurrentRow.Cells("EsBonificacion").Value = 1 Then
                    MsgBox("Es un Producto en Bonificacion no se puede modificar su precio", MsgBoxStyle.Exclamation, "Compras")
                    dgvxProductosCompras.CurrentCell.Value = 0.0
                    Exit Sub
                Else
                    dgvxProductosCompras.CurrentRow.Cells("Valor").Value = dgvxProductosCompras.CurrentRow.Cells("Cantidad").Value * dgvxProductosCompras.CurrentRow.Cells("Precio").Value
                    TotalAnterior = 0
                    TextBox9.Text = 0
                    ImpuestoAnterior = 0
                    TextBox10.Text = 0
                    For i = 0 To dgvxProductosCompras.Rows.Count - 1
                        TotalAnterior = ConvierteADouble(TextBox9.Text)
                        TextBox9.Text = Format(Format((SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Precio").Value) * SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Cantidad").Value)), "#,##0.####") + TotalAnterior, "#,##0.####")
                        If dgvxProductosCompras.Rows(i).Cells("Impuesto").Value.ToString().ToLower() = "no" Then
                            TextBox10.Text = Format(SUConversiones.ConvierteADouble(TextBox10.Text), "#,##0.####")
                        Else
                            ImpuestoAnterior = ConvierteADouble(TextBox10.Text)
                            TextBox10.Text = Format(Format((Format((SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Precio").Value) * SUConversiones.ConvierteADouble(dgvxProductosCompras.Rows(i).Cells("Cantidad").Value)), "#,##0.####") * dblPorcParam), "#,##0.####") + ImpuestoAnterior, "#,##0.####")
                        End If

                        TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.####")

                    Next
                    Limpiar2()
                End If
            End If
            CalculaTotalasFactura()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try


    End Sub

    Private Sub cmbAgencias_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbAgencias.KeyDown
        If Keys.Enter = e.KeyCode Then
            txtProveedor.Focus()
        End If
    End Sub

    Private Sub ddlRetencion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRetencion.SelectedIndexChanged
        Try
            If sender.name = "ddlRentencion" Then
                Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%"
                If sender.selectedindex = 0 Then
                    TextBox11.Text = "0.00"
                    TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.####")
                ElseIf sender.selectedindex = 1 Then
                    If intTipoFactura = 4 Or intTipoFactura = 8 Then
                        sender.selectedindex = 0
                    Else
                        TextBox11.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) * dblRetParam, "#,##0.####")
                        TextBox12.Text = Format(SUConversiones.ConvierteADouble(TextBox9.Text) + SUConversiones.ConvierteADouble(TextBox10.Text) - SUConversiones.ConvierteADouble(TextBox11.Text), "#,##0.####")
                    End If
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try
    End Sub

    Private Sub TextBox1_KeyDown_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles medPrecio.KeyDown
        Try
            If e.KeyCode = Keys.Delete Then
                strCurrency = String.Empty
            End If
            If (e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.D9) OrElse (e.KeyCode >= Keys.NumPad0 And e.KeyCode <= Keys.NumPad9) OrElse e.KeyCode = Keys.Back Then
                acceptableKey = True
            Else
                acceptableKey = False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try
    End Sub

    Private Sub cbPedidoCompra_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbPedidoCompra.SelectedIndexChanged
        Try
            If Not gbGuardar Then
                ExtraerPedidoCompraPendiente()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Try
            ' Check for the flag being set in the KeyDown event.
            If acceptableKey = False Then
                ' Stop the character from being entered into the control since it is non-numerical.
                e.Handled = True
                Return
            Else
                If e.KeyChar = Convert.ToChar(Keys.Delete) Then
                    strCurrency = String.Empty
                End If
                If e.KeyChar = Convert.ToChar(Keys.Back) Then
                    If strCurrency.Length > 0 Then
                        strCurrency = strCurrency.Substring(0, strCurrency.Length - 1)
                    End If
                Else
                    strCurrency = strCurrency & e.KeyChar
                End If

                If strCurrency.Length = 0 Then
                    medPrecio.Text = ""
                ElseIf strCurrency.Length = 1 Then
                    medPrecio.Text = "0.000" & strCurrency
                ElseIf strCurrency.Length = 2 Then
                    medPrecio.Text = "0.00" & strCurrency
                ElseIf strCurrency.Length = 3 Then
                    medPrecio.Text = "0.0" & strCurrency
                ElseIf strCurrency.Length > 4 Then
                    medPrecio.Text = strCurrency.Substring(0, strCurrency.Length - 4) & "." & strCurrency.Substring(strCurrency.Length - 4)
                End If
                medPrecio.Select(medPrecio.Text.Length, 0)

            End If
            e.Handled = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Exit Sub
        End Try

    End Sub


End Class
