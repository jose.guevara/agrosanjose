Imports System.Data.SqlClient
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmLoginSuprvisor
    Inherits DevComponents.DotNetBar.Office2007Form
    ' Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtClave As System.Windows.Forms.TextBox
    Friend WithEvents cmdOk As System.Windows.Forms.Button
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLoginSuprvisor))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.txtClave = New System.Windows.Forms.TextBox
        Me.cmdOk = New System.Windows.Forms.Button
        Me.cmdCancelar = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Usuario:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(48, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(36, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Clave:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(104, 24)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(88, 20)
        Me.txtCodigo.TabIndex = 2
        Me.txtCodigo.Text = ""
        '
        'txtClave
        '
        Me.txtClave.Location = New System.Drawing.Point(104, 64)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtClave.Size = New System.Drawing.Size(88, 20)
        Me.txtClave.TabIndex = 3
        Me.txtClave.Text = ""
        '
        'cmdOk
        '
        Me.cmdOk.Location = New System.Drawing.Point(24, 112)
        Me.cmdOk.Name = "cmdOk"
        Me.cmdOk.Size = New System.Drawing.Size(72, 32)
        Me.cmdOk.TabIndex = 4
        Me.cmdOk.Text = "&OK"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(144, 112)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(72, 32)
        Me.cmdCancelar.TabIndex = 5
        Me.cmdCancelar.Text = "&Cancelar"
        '
        'frmLoginSuprvisor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(238, 155)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.cmdOk)
        Me.Controls.Add(Me.txtClave)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoginSuprvisor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso al Sistema"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmLoginSuprvisor"
    Private Sub frmLoginSuprvisor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim intError As Integer = 0

        intError = 0
        strQuery = ""
        strAgro2K = ""
        strUsuario = ""
        strAppPath = ""
        strNombEmpresa = ""
        lngClienteContado = 0
        lngNegocioContado = 0
        lngDepartContado = 0
        intFechaCambioFactura = 0
        lngRegUsuarioRelogin = -1

        Try
            strAppPath = System.Windows.Forms.Application.StartupPath
            'strAgro2K = "data source=transidea;persist security info=True;initial catalog=Agrocentro2K;user id=Agro2K;password=Agro2K_2008"
            strAgro2K = ConfigurationManager.AppSettings("ConSistemaGerencial").ToString()
            'strAgro2K = "data source=work\work;persist security info=True;initial catalog=Agrocentro2K;user id=sa;password=djvallecillo"
            'strAgro2K = "data source=Servidor;persist security info=True;initial catalog=Agrocentro2K;user id=Agro2K;password=Agro2K_2008"
            If cnnAgro2K.State = ConnectionState.Closed Then
                cnnAgro2K.ConnectionString = strAgro2K
            End If

            If cnnAgro2KTmp.State = ConnectionState.Closed Then
                cnnAgro2KTmp.ConnectionString = strAgro2K
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            MessageBox.Show("Error: " & ex.Message, "Error de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmLoginSuprvisor_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
                Application.Exit()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            MessageBox.Show("Error: " & ex.Message, "Error de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOk.Click

        Dim intValor As Integer
        Dim drUsuario As IDataReader = Nothing
        If ValidarDatos() = False Then
            Exit Sub
        End If
        Try
            drUsuario = RNUsuario.ValidaUsuario(txtCodigo.Text, txtClave.Text)
            intValor = 0
            'lngRegUsuario = 0
            nIdUsuarioRelogin = -1
            lngRegUsuarioRelogin = -1
            intImpresoraAsignada = 0
            If Not drUsuario Is Nothing Then
                While drUsuario.Read
                    lngRegUsuarioRelogin = drUsuario.GetValue(0)
                    nIdUsuarioRelogin = drUsuario.GetValue(0)
                    intValor = 1
                End While
                If intValor = 0 Then
                    MsgBox("No Existe Este Usuario en el Sistema o est� Inactivo. Verifique.", MsgBoxStyle.Critical, "Error de Datos Ingresados")
                    txtClave.Text = ""
                    txtClave.Focus()
                    Exit Sub
                End If
                Me.Close()
            End If
        Catch exc As Exception
            If Not drUsuario.IsClosed Then
                drUsuario.Close()
            End If
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = exc.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", exc.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: ", MsgBoxStyle.Critical, "Error de Datos")
            MessageBox.Show("Error: " & exc.Message, "Error de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        lngRegUsuarioRelogin = -1
        Me.Close()
        ' Application.Exit()

    End Sub

    Function ValidarDatos() As Boolean
        Try
            If txtCodigo.Text = "" Or Len(Trim(txtCodigo.Text)) = 0 Then
                MsgBox("Tiene Que Ingresar el Codigo.", MsgBoxStyle.Critical)
                txtCodigo.Focus()
                Return False
            ElseIf txtClave.Text = "" Or Len(Trim(txtClave.Text)) = 0 Then
                MsgBox("Tiene Que Ingresar la Clave.", MsgBoxStyle.Critical)
                txtClave.Focus()
                Return False
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Return True

    End Function

    Private Sub txtCodigo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigo.GotFocus, txtClave.GotFocus

        sender.selectall()

    End Sub

    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown, txtClave.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                If sender.name = "txtCodigo" Then
                    txtClave.Focus()
                Else
                    cmdOk.PerformClick()
                End If
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MessageBox.Show("Error: " & ex.Message, "Error de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

End Class