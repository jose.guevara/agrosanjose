Public Class frmRazonesAnular
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cntmnuRazonesAnularRg As System.Windows.Forms.ContextMenu
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmRazonesAnular))
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.cntmnuRazonesAnularRg = New System.Windows.Forms.ContextMenu
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem9})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(544, 56)
        Me.ToolBar1.TabIndex = 16
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuRazonesAnularRg
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.ToolBarButton4.ToolTipText = "Registros"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(31, 31)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 185)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(544, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 17
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 445
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(544, 128)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Razones para Anular Facturas/Recibos"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(184, 24)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(8, 20)
        Me.TextBox2.TabIndex = 5
        Me.TextBox2.Text = "TextBox2"
        Me.TextBox2.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 56)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(480, 20)
        Me.TextBox1.TabIndex = 4
        Me.TextBox1.Text = "TextBox1"
        '
        'ComboBox2
        '
        Me.ComboBox2.Location = New System.Drawing.Point(56, 88)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(120, 21)
        Me.ComboBox2.TabIndex = 3
        Me.ComboBox2.Text = "ComboBox2"
        '
        'ComboBox1
        '
        Me.ComboBox1.Location = New System.Drawing.Point(56, 24)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(120, 21)
        Me.ComboBox1.TabIndex = 2
        Me.ComboBox1.Text = "ComboBox1"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Raz�n"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "M�dulo"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Estado"
        '
        'frmRazonesAnular
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(544, 209)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.ToolBar1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRazonesAnular"
        Me.Text = "frmRazonesAnular"
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strCambios(3) As String

    Private Sub frmRazonesAnular_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Left = 0
        Me.Top = 0
        intModulo = 17

    End Sub

    Private Sub frmRazonesAnular_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = TextBox2.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = TextBox2.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
            Case 3
                MoverRegistro(sender.text)
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub Limpiar()

        ComboBox1.SelectedIndex = 0
        TextBox1.Text = ""
        TextBox2.Text = 0
        ComboBox2.SelectedIndex = 0
        strCambios(0) = ""
        strCambios(1) = ""
        ComboBox1.Focus()

    End Sub

    Sub Guardar()

    End Sub

    Sub MoverRegistro(ByVal intDato As Integer)
        Select Case intDato
            Case "Primero"
                strQuery = "Select Min(Registro) from prm_Razones_Anular"
            Case "Anterior"
                strQuery = "Select Max(Registro) from prm_Razones_Anular Where Registro < " & TextBox2.Text & ""
            Case "Siguiente"
                strQuery = "Select Min(Registro) from prm_Razones_Anular Where Registro > " & TextBox2.Text & ""
            Case "Ultimo"
                strQuery = "Select Max(Registro) from prm_Razones_Anular"
        End Select
        UbicarRegistro(intDato)

    End Sub

    Sub UbicarRegistro(ByVal intDato As Integer)

        Dim intCodigo As Integer

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        intCodigo = 0
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                intCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If intCodigo = 0 Then
            MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From prm_Razones_Anular Where Registro = " & intCodigo & ""
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox2.Text = dtrAgro2K.GetValue(0)
            TextBox1.Text = dtrAgro2K.GetValue(2)
            ComboBox1.SelectedIndex = dtrAgro2K.GetValue(1)
            ComboBox2.SelectedIndex = dtrAgro2K.GetValue(3)
            strCambios(0) = TextBox2.Text
            strCambios(1) = ComboBox1.SelectedItem
            strCambios(2) = ComboBox2.SelectedItem
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"

        cntmnuRazonesAnularRg.MenuItems.Add(cntmenuItem1)
        cntmnuRazonesAnularRg.MenuItems.Add(cntmenuItem2)
        cntmnuRazonesAnularRg.MenuItems.Add(cntmenuItem3)
        cntmnuRazonesAnularRg.MenuItems.Add(cntmenuItem4)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

End Class
