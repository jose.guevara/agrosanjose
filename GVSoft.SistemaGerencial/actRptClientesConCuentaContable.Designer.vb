<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actRptClientesConCuentaContable
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actRptClientesConCuentaContable))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.Label15 = New DataDynamics.ActiveReports.Label
        Me.Label16 = New DataDynamics.ActiveReports.Label
        Me.TextBox25 = New DataDynamics.ActiveReports.TextBox
        Me.TextBox26 = New DataDynamics.ActiveReports.TextBox
        Me.Label20 = New DataDynamics.ActiveReports.Label
        Me.Line = New DataDynamics.ActiveReports.Line
        Me.Label = New DataDynamics.ActiveReports.Label
        Me.Detail1 = New DataDynamics.ActiveReports.Detail
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.txtCodigoCliente = New DataDynamics.ActiveReports.TextBox
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.txtNombre = New DataDynamics.ActiveReports.TextBox
        Me.TextBox2 = New DataDynamics.ActiveReports.TextBox
        Me.Label2 = New DataDynamics.ActiveReports.Label
        Me.Label4 = New DataDynamics.ActiveReports.Label
        Me.Label5 = New DataDynamics.ActiveReports.Label
        Me.txtEstado = New DataDynamics.ActiveReports.TextBox
        Me.Label13 = New DataDynamics.ActiveReports.Label
        Me.TextBox21 = New DataDynamics.ActiveReports.TextBox
        Me.Label10 = New DataDynamics.ActiveReports.Label
        Me.txtTotalProductos = New DataDynamics.ActiveReports.TextBox
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigoCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEstado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label15, Me.Label16, Me.TextBox25, Me.TextBox26, Me.Label20, Me.Line, Me.Label, Me.Label1, Me.Label2, Me.Label4, Me.Label5})
        Me.PageHeader1.Height = 1.375!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 3.572917!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label15.Text = "Fecha de Impresi�n"
        Me.Label15.Top = 0.5!
        Me.Label15.Width = 1.35!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.572917!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label16.Text = "Hora de Impresi�n"
        Me.Label16.Top = 0.6875!
        Me.Label16.Width = 1.35!
        '
        'TextBox25
        '
        Me.TextBox25.Height = 0.2!
        Me.TextBox25.Left = 4.947917!
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Style = "text-align: right"
        Me.TextBox25.Text = "TextBox25"
        Me.TextBox25.Top = 0.5!
        Me.TextBox25.Width = 0.9!
        '
        'TextBox26
        '
        Me.TextBox26.Height = 0.2!
        Me.TextBox26.Left = 4.947917!
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Style = "text-align: right"
        Me.TextBox26.Text = "TextBox26"
        Me.TextBox26.Top = 0.6875!
        Me.TextBox26.Width = 0.9!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.1875!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-weight: bold; font-size: 13pt"
        Me.Label20.Text = "Listado Cliente"
        Me.Label20.Top = 0.5!
        Me.Label20.Width = 2.4375!
        '
        'Line
        '
        Me.Line.Height = 0.0009999275!
        Me.Line.Left = 0.0!
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 1.313!
        Me.Line.Width = 5.938!
        Me.Line.X1 = 0.0!
        Me.Line.X2 = 5.938!
        Me.Line.Y1 = 1.313!
        Me.Line.Y2 = 1.314!
        '
        'Label
        '
        Me.Label.Height = 0.375!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 0.4375!
        Me.Label.Name = "Label"
        Me.Label.Style = "text-align: center; font-weight: bold; font-size: 20.25pt; font-family: Times New" & _
            " Roman"
        Me.Label.Text = "Agro Centro, S.A."
        Me.Label.Top = 0.0!
        Me.Label.Width = 5.4375!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0.0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.txtCodigoCliente, Me.txtNombre, Me.TextBox2, Me.txtEstado})
        Me.Detail1.Height = 0.1979167!
        Me.Detail1.Name = "Detail1"
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label13, Me.TextBox21, Me.Label10, Me.txtTotalProductos})
        Me.PageFooter1.Height = 0.4895833!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'txtCodigoCliente
        '
        Me.txtCodigoCliente.DataField = "Codigo"
        Me.txtCodigoCliente.Height = 0.1979167!
        Me.txtCodigoCliente.Left = 0.027!
        Me.txtCodigoCliente.Name = "txtCodigoCliente"
        Me.txtCodigoCliente.Style = "font-size: 8pt"
        Me.txtCodigoCliente.Top = 0.0!
        Me.txtCodigoCliente.Width = 0.8200001!
        '
        'Label1
        '
        Me.Label1.Height = 0.2!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.06!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-weight: bold"
        Me.Label1.Text = "C�digo"
        Me.Label1.Top = 1.0395!
        Me.Label1.Width = 0.7!
        '
        'txtNombre
        '
        Me.txtNombre.DataField = "Nombre"
        Me.txtNombre.Height = 0.1979167!
        Me.txtNombre.Left = 0.9300001!
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Style = "font-size: 8pt"
        Me.txtNombre.Top = 0.0!
        Me.txtNombre.Width = 2.553!
        '
        'TextBox2
        '
        Me.TextBox2.DataField = "cta_contable"
        Me.TextBox2.Height = 0.1979167!
        Me.TextBox2.Left = 3.56!
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Style = "font-size: 8pt"
        Me.TextBox2.Top = 0.0!
        Me.TextBox2.Width = 1.393!
        '
        'Label2
        '
        Me.Label2.Height = 0.2!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.9300001!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-weight: bold"
        Me.Label2.Text = "Nombre"
        Me.Label2.Top = 1.04!
        Me.Label2.Width = 1.7!
        '
        'Label4
        '
        Me.Label4.Height = 0.2!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 3.56!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-weight: bold"
        Me.Label4.Text = "Cuenta Contable"
        Me.Label4.Top = 1.04!
        Me.Label4.Width = 1.183001!
        '
        'Label5
        '
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 5.04!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-weight: bold"
        Me.Label5.Text = "Estado"
        Me.Label5.Top = 1.04!
        Me.Label5.Width = 0.7139999!
        '
        'txtEstado
        '
        Me.txtEstado.DataField = "Estado"
        Me.txtEstado.Height = 0.1979167!
        Me.txtEstado.Left = 5.02!
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Style = "font-size: 8pt"
        Me.txtEstado.Top = 0.0!
        Me.txtEstado.Width = 0.9230001!
        '
        'Label13
        '
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 4.708!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-weight: bold; font-size: 9.75pt"
        Me.Label13.Text = "P�gina"
        Me.Label13.Top = 0.06!
        Me.Label13.Width = 0.5500001!
        '
        'TextBox21
        '
        Me.TextBox21.Height = 0.2!
        Me.TextBox21.Left = 5.2705!
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Style = "text-align: right"
        Me.TextBox21.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.TextBox21.SummaryType = DataDynamics.ActiveReports.SummaryType.PageCount
        Me.TextBox21.Top = 0.06!
        Me.TextBox21.Width = 0.3499999!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0.0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-weight: bold"
        Me.Label10.Text = "Total Productos:"
        Me.Label10.Top = 0.06!
        Me.Label10.Width = 1.25!
        '
        'txtTotalProductos
        '
        Me.txtTotalProductos.DataField = "Codigo"
        Me.txtTotalProductos.Height = 0.1979167!
        Me.txtTotalProductos.Left = 1.375!
        Me.txtTotalProductos.Name = "txtTotalProductos"
        Me.txtTotalProductos.Style = "font-weight: bold"
        Me.txtTotalProductos.SummaryFunc = DataDynamics.ActiveReports.SummaryFunc.Count
        Me.txtTotalProductos.SummaryRunning = DataDynamics.ActiveReports.SummaryRunning.All
        Me.txtTotalProductos.SummaryType = DataDynamics.ActiveReports.SummaryType.GrandTotal
        Me.txtTotalProductos.Text = Nothing
        Me.txtTotalProductos.Top = 0.06!
        Me.txtTotalProductos.Width = 1.0!
        '
        'actRptClientesConCuentaContable
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 6.0!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigoCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEstado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotalProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label15 As DataDynamics.ActiveReports.Label
    Private WithEvents Label16 As DataDynamics.ActiveReports.Label
    Private WithEvents TextBox25 As DataDynamics.ActiveReports.TextBox
    Private WithEvents TextBox26 As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label20 As DataDynamics.ActiveReports.Label
    Private WithEvents Line As DataDynamics.ActiveReports.Line
    Private WithEvents Label As DataDynamics.ActiveReports.Label
    Private WithEvents txtCodigoCliente As DataDynamics.ActiveReports.TextBox
    Private WithEvents Label1 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtNombre As DataDynamics.ActiveReports.TextBox
    Friend WithEvents TextBox2 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label2 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label4 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label5 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtEstado As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label13 As DataDynamics.ActiveReports.Label
    Friend WithEvents TextBox21 As DataDynamics.ActiveReports.TextBox
    Friend WithEvents Label10 As DataDynamics.ActiveReports.Label
    Friend WithEvents txtTotalProductos As DataDynamics.ActiveReports.TextBox
End Class
