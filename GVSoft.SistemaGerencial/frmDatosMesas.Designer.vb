<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDatosMesas
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDatosMesas))
        Dim RadTreeNode1 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode2 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode3 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode4 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode5 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode6 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode7 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode8 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode9 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode10 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode11 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode12 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode13 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode14 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode15 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode16 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode17 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Dim RadTreeNode18 As Telerik.WinControls.UI.RadTreeNode = New Telerik.WinControls.UI.RadTreeNode
        Me.ngpMesas = New DevComponents.DotNetBar.NavigationPane
        Me.Cell1 = New DevComponents.AdvTree.Cell
        Me.Cell2 = New DevComponents.AdvTree.Cell
        Me.NavigationPanePanel1 = New DevComponents.DotNetBar.NavigationPanePanel
        Me.PanelEx1 = New DevComponents.DotNetBar.PanelEx
        Me.cmdMesas = New DevComponents.DotNetBar.ButtonItem
        Me.NavigationPanePanel2 = New DevComponents.DotNetBar.NavigationPanePanel
        Me.RadTreeView1 = New Telerik.WinControls.UI.RadTreeView
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem
        Me.PanelEx2 = New DevComponents.DotNetBar.PanelEx
        Me.utvMesas1 = New Infragistics.Win.UltraWinTree.UltraTree
        Me.ngpMesas.SuspendLayout()
        Me.NavigationPanePanel1.SuspendLayout()
        Me.PanelEx1.SuspendLayout()
        Me.NavigationPanePanel2.SuspendLayout()
        CType(Me.RadTreeView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.utvMesas1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ngpMesas
        '
        Me.ngpMesas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.ngpMesas.CanCollapse = True
        Me.ngpMesas.Controls.Add(Me.NavigationPanePanel1)
        Me.ngpMesas.Controls.Add(Me.NavigationPanePanel2)
        Me.ngpMesas.ItemPaddingBottom = 2
        Me.ngpMesas.ItemPaddingTop = 2
        Me.ngpMesas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdMesas, Me.ButtonItem2})
        Me.ngpMesas.Location = New System.Drawing.Point(0, 12)
        Me.ngpMesas.Name = "ngpMesas"
        Me.ngpMesas.NavigationBarHeight = 102
        Me.ngpMesas.Padding = New System.Windows.Forms.Padding(1)
        Me.ngpMesas.Size = New System.Drawing.Size(221, 515)
        Me.ngpMesas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ngpMesas.TabIndex = 0
        '
        '
        '
        Me.ngpMesas.TitlePanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ngpMesas.TitlePanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.ngpMesas.TitlePanel.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ngpMesas.TitlePanel.Location = New System.Drawing.Point(1, 1)
        Me.ngpMesas.TitlePanel.Name = "panelTitle"
        Me.ngpMesas.TitlePanel.Size = New System.Drawing.Size(215, 24)
        Me.ngpMesas.TitlePanel.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ngpMesas.TitlePanel.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ngpMesas.TitlePanel.Style.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ngpMesas.TitlePanel.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ngpMesas.TitlePanel.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.Bottom
        Me.ngpMesas.TitlePanel.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.ngpMesas.TitlePanel.Style.GradientAngle = 90
        Me.ngpMesas.TitlePanel.Style.MarginLeft = 4
        Me.ngpMesas.TitlePanel.TabIndex = 0
        Me.ngpMesas.TitlePanel.Text = "Informacion General de Mesas"
        '
        'Cell1
        '
        Me.Cell1.Name = "Cell1"
        Me.Cell1.StyleMouseOver = Nothing
        '
        'Cell2
        '
        Me.Cell2.Name = "Cell2"
        Me.Cell2.StyleMouseOver = Nothing
        '
        'NavigationPanePanel1
        '
        Me.NavigationPanePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.NavigationPanePanel1.Controls.Add(Me.PanelEx1)
        Me.NavigationPanePanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPanePanel1.Location = New System.Drawing.Point(1, 25)
        Me.NavigationPanePanel1.Name = "NavigationPanePanel1"
        Me.NavigationPanePanel1.ParentItem = Me.cmdMesas
        Me.NavigationPanePanel1.Size = New System.Drawing.Size(215, 383)
        Me.NavigationPanePanel1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.NavigationPanePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.NavigationPanePanel1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPanePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.NavigationPanePanel1.Style.GradientAngle = 90
        Me.NavigationPanePanel1.TabIndex = 2
        '
        'PanelEx1
        '
        Me.PanelEx1.CanvasColor = System.Drawing.SystemColors.Control
        Me.PanelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.PanelEx1.Controls.Add(Me.utvMesas1)
        Me.PanelEx1.Location = New System.Drawing.Point(-1, 0)
        Me.PanelEx1.Name = "PanelEx1"
        Me.PanelEx1.Size = New System.Drawing.Size(216, 380)
        Me.PanelEx1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.PanelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.PanelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.PanelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.PanelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.PanelEx1.Style.GradientAngle = 90
        Me.PanelEx1.TabIndex = 1
        Me.PanelEx1.Text = "PanelEx1"
        '
        'cmdMesas
        '
        Me.cmdMesas.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdMesas.Checked = True
        Me.cmdMesas.Image = CType(resources.GetObject("cmdMesas.Image"), System.Drawing.Image)
        Me.cmdMesas.Name = "cmdMesas"
        Me.cmdMesas.OptionGroup = "navBar"
        Me.cmdMesas.Text = "Informacion General de Mesas"
        '
        'NavigationPanePanel2
        '
        Me.NavigationPanePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.NavigationPanePanel2.Controls.Add(Me.RadTreeView1)
        Me.NavigationPanePanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavigationPanePanel2.Location = New System.Drawing.Point(1, 1)
        Me.NavigationPanePanel2.Name = "NavigationPanePanel2"
        Me.NavigationPanePanel2.ParentItem = Me.ButtonItem2
        Me.NavigationPanePanel2.Size = New System.Drawing.Size(215, 509)
        Me.NavigationPanePanel2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.NavigationPanePanel2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.NavigationPanePanel2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.NavigationPanePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.NavigationPanePanel2.Style.GradientAngle = 90
        Me.NavigationPanePanel2.TabIndex = 3
        '
        'RadTreeView1
        '
        Me.RadTreeView1.BackColor = System.Drawing.Color.Transparent
        Me.RadTreeView1.Cursor = System.Windows.Forms.Cursors.Default
        Me.RadTreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.RadTreeView1.ForeColor = System.Drawing.Color.Black
        Me.RadTreeView1.Location = New System.Drawing.Point(3, 5)
        Me.RadTreeView1.Name = "RadTreeView1"
        RadTreeNode1.Expanded = True
        RadTreeNode1.Name = "Node1"
        RadTreeNode2.Expanded = True
        RadTreeNode2.Name = "Node2"
        RadTreeNode3.Expanded = True
        RadTreeNode3.Name = "Node3"
        RadTreeNode3.Text = "Node3"
        RadTreeNode4.Name = "Node4"
        RadTreeNode4.Text = "Node4"
        RadTreeNode5.Expanded = True
        RadTreeNode5.Name = "Node5"
        RadTreeNode5.Text = "Node5"
        RadTreeNode6.Name = "Node6"
        RadTreeNode6.Text = "Node6"
        RadTreeNode2.Nodes.AddRange(New Telerik.WinControls.UI.RadTreeNode() {RadTreeNode3, RadTreeNode4, RadTreeNode5, RadTreeNode6})
        RadTreeNode2.Text = "Node2"
        RadTreeNode7.Expanded = True
        RadTreeNode7.Name = "Node7"
        RadTreeNode8.Name = "Node13"
        RadTreeNode8.Text = "Node13"
        RadTreeNode9.Name = "Node14"
        RadTreeNode9.Text = "Node14"
        RadTreeNode7.Nodes.AddRange(New Telerik.WinControls.UI.RadTreeNode() {RadTreeNode8, RadTreeNode9})
        RadTreeNode7.Text = "Node7"
        RadTreeNode10.Expanded = True
        RadTreeNode10.Name = "Node8"
        RadTreeNode11.Name = "Node15"
        RadTreeNode11.Text = "Node15"
        RadTreeNode12.Name = "Node16"
        RadTreeNode12.Text = "Node16"
        RadTreeNode13.Name = "Node17"
        RadTreeNode13.Text = "Node17"
        RadTreeNode14.Expanded = True
        RadTreeNode14.Name = "Node18"
        RadTreeNode14.Text = "Node18"
        RadTreeNode10.Nodes.AddRange(New Telerik.WinControls.UI.RadTreeNode() {RadTreeNode11, RadTreeNode12, RadTreeNode13, RadTreeNode14})
        RadTreeNode10.Text = "Node8"
        RadTreeNode15.Expanded = True
        RadTreeNode15.Name = "Node9"
        RadTreeNode16.Name = "Node10"
        RadTreeNode16.Text = "Node10"
        RadTreeNode17.Name = "Node11"
        RadTreeNode17.Text = "Node11"
        RadTreeNode18.Name = "Node12"
        RadTreeNode18.Text = "Node12"
        RadTreeNode15.Nodes.AddRange(New Telerik.WinControls.UI.RadTreeNode() {RadTreeNode16, RadTreeNode17, RadTreeNode18})
        RadTreeNode15.Text = "Node9"
        RadTreeNode1.Nodes.AddRange(New Telerik.WinControls.UI.RadTreeNode() {RadTreeNode2, RadTreeNode7, RadTreeNode10, RadTreeNode15})
        RadTreeNode1.Text = "Node1"
        Me.RadTreeView1.Nodes.AddRange(New Telerik.WinControls.UI.RadTreeNode() {RadTreeNode1})
        Me.RadTreeView1.RightToLeft = System.Windows.Forms.RightToLeft.No
        '
        '
        '
        Me.RadTreeView1.RootElement.ForeColor = System.Drawing.Color.Black
        Me.RadTreeView1.Size = New System.Drawing.Size(209, 375)
        Me.RadTreeView1.TabIndex = 2
        Me.RadTreeView1.Text = "RadTreeView1"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.Image = CType(resources.GetObject("ButtonItem2.Image"), System.Drawing.Image)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.OptionGroup = "navBar"
        Me.ButtonItem2.Text = "ButtonItem2"
        '
        'PanelEx2
        '
        Me.PanelEx2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelEx2.CanvasColor = System.Drawing.SystemColors.Control
        Me.PanelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.PanelEx2.Location = New System.Drawing.Point(227, 12)
        Me.PanelEx2.Name = "PanelEx2"
        Me.PanelEx2.Size = New System.Drawing.Size(578, 515)
        Me.PanelEx2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.PanelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.PanelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.PanelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.PanelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.PanelEx2.Style.GradientAngle = 90
        Me.PanelEx2.TabIndex = 1
        '
        'utvMesas1
        '
        Me.utvMesas1.DisplayStyle = Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle.WindowsVista
        Me.utvMesas1.ImageTransparentColor = System.Drawing.Color.Transparent
        Me.utvMesas1.Location = New System.Drawing.Point(5, 3)
        Me.utvMesas1.Name = "utvMesas1"
        Me.utvMesas1.NodeConnectorColor = System.Drawing.SystemColors.ControlDark
        Me.utvMesas1.Size = New System.Drawing.Size(207, 374)
        Me.utvMesas1.TabIndex = 2
        Me.utvMesas1.ViewStyle = Infragistics.Win.UltraWinTree.ViewStyle.Grid
        '
        'frmDatosMesas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(817, 539)
        Me.Controls.Add(Me.PanelEx2)
        Me.Controls.Add(Me.ngpMesas)
        Me.Name = "frmDatosMesas"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Informacion General de Mesas"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ngpMesas.ResumeLayout(False)
        Me.NavigationPanePanel1.ResumeLayout(False)
        Me.PanelEx1.ResumeLayout(False)
        Me.NavigationPanePanel2.ResumeLayout(False)
        CType(Me.RadTreeView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.utvMesas1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ngpMesas As DevComponents.DotNetBar.NavigationPane
    Friend WithEvents NavigationPanePanel1 As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents cmdMesas As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents NavigationPanePanel2 As DevComponents.DotNetBar.NavigationPanePanel
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents PanelEx1 As DevComponents.DotNetBar.PanelEx
    Friend WithEvents RadTreeView1 As Telerik.WinControls.UI.RadTreeView
    Friend WithEvents Cell1 As DevComponents.AdvTree.Cell
    Friend WithEvents Cell2 As DevComponents.AdvTree.Cell
    Friend WithEvents PanelEx2 As DevComponents.DotNetBar.PanelEx
    Friend WithEvents utvMesas1 As Infragistics.Win.UltraWinTree.UltraTree
End Class
