Imports System.IO
Imports System.Web.UI.WebControls
Imports Infragistics.Win
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Entidades

Public Class frmExportarTraslado
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DPFechaFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents DPFechaInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents utcAgencia As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Label4 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportarTraslado))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.DPFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.DPFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.utcAgencia = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.GroupBox1.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.utcAgencia)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ProgressBar1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 79)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(459, 116)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas a Exportar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Agencia"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(87, 94)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(320, 16)
        Me.ProgressBar1.TabIndex = 2
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        '
        'DPFechaFin
        '
        Me.DPFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DPFechaFin.Location = New System.Drawing.Point(298, 101)
        Me.DPFechaFin.Name = "DPFechaFin"
        Me.DPFechaFin.Size = New System.Drawing.Size(120, 20)
        Me.DPFechaFin.TabIndex = 17
        '
        'DPFechaInicio
        '
        Me.DPFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DPFechaInicio.Location = New System.Drawing.Point(87, 101)
        Me.DPFechaInicio.Name = "DPFechaInicio"
        Me.DPFechaInicio.Size = New System.Drawing.Size(120, 20)
        Me.DPFechaInicio.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 101)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Fecha Inicio"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(219, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Fecha Fin"
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdProcesar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(471, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 37
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdProcesar
        '
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Procesar<F3>"
        Me.cmdProcesar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'utcAgencia
        '
        Me.utcAgencia.DisplayMember = "Text"
        Me.utcAgencia.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.utcAgencia.FormattingEnabled = True
        Me.utcAgencia.ItemHeight = 14
        Me.utcAgencia.Location = New System.Drawing.Point(87, 57)
        Me.utcAgencia.Name = "utcAgencia"
        Me.utcAgencia.Size = New System.Drawing.Size(327, 20)
        Me.utcAgencia.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.utcAgencia.TabIndex = 54
        '
        'frmExportarTraslado
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(471, 206)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.DPFechaFin)
        Me.Controls.Add(Me.DPFechaInicio)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarTraslado"
        Me.Text = "frmExportarTraslado"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmExportarTraslado"

    Private Sub frmExportarTraslado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            DPFechaInicio.Value = Now
            UbicarAgencia(lngRegUsuario)
            ObtieneAgencias()
            'utcAgencia.Value = lngRegAgencia

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmExportarTraslado_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Procesar()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Procesar()
        '    Case 1
        '        Me.Close()
        'End Select

    End Sub

    Sub ObtieneAgencias()
        Dim IndicaObtieneRegistro As Integer = 0
        Try


            'utcAgencia.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            RNAgencias.CargarComboAgenciasConTodos(utcAgencia, lngRegUsuario)
            'Dim table As New DataTable("Agencias")
            'table.Columns.Add("Codigo")
            'table.Columns.Add("Descripcion")

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            ''If cmdAgro2K.Connection.State = ConnectionState.Open Then
            ''    cmdAgro2K.Connection.Close()
            ''End If
            ''ddlAgencia.SelectedItem = -1
            ''utcAgencia.SelectedRow.Selected = -1
            'cnnAgro2K.Open()
            'cmdAgro2K.Connection = cnnAgro2K
            'strQuery = ""
            'strQuery = "select a.registro,a.codigo,a.Descripcion from prm_UsuariosAgencias u,prm_agencias a  Where(u.agenregistro = a.registro)  and u.usrregistro = " & lngRegUsuario & ""
            'If strQuery.Trim.Length > 0 Then
            '    'ddlAgencia.Items.Clear()
            '    cmdAgro2K.CommandText = strQuery
            '    dtrAgro2K = cmdAgro2K.ExecuteReader
            '    While dtrAgro2K.Read
            '        table.Rows.Add(New Object() {dtrAgro2K.GetValue(0), dtrAgro2K.GetValue(2)})
            '        IndicaObtieneRegistro = 1
            '    End While
            '    utcAgencia.DisplayMember = "Descripcion"
            '    utcAgencia.ValueMember = "codigo"
            '    utcAgencia.DataSource = table
            'End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

    End Sub

    Function ObtieneCodigoAgencias(ByVal lnRegistro As Int16) As Int16

        Dim IndicaObtieneRegistro As Integer = 0
        Try


            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "exec ObtieneCodigoAgencia " & lnRegistro & ""
            ObtieneCodigoAgencias = 0
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ObtieneCodigoAgencias = dtrAgro2K.GetValue(0)
                IndicaObtieneRegistro = 1
            End While
            Return ObtieneCodigoAgencias
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Function
    Sub Procesar()

        Me.Cursor = Cursors.WaitCursor
        Dim intCantidad As Integer = 0
        Dim intContador As Integer = 0
        Dim intNumFechaInicio As Integer = 0
        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim lnRegistro As Integer = 0
        Dim lnCodigoAgencia As Integer = 0
        Dim lnAgenciaOrigen As Integer = 0
        Dim lnCdigoAgenciaOrigen As Integer = 0
        Dim intNumFechaFin As Integer = 0
        Dim lsRutaCompletaArchivoZip As String = String.Empty

        Try


            'lnRegistro = ConvierteAInt(utcAgencia.SelectedRow.Cells.Item("codigo").Value)
            lnRegistro = SUConversiones.ConvierteAInt(utcAgencia.SelectedValue)
            lnCodigoAgencia = ObtieneCodigoAgencias(lnRegistro)

            'If utcAgencia.SelectedRow.Cells.Item("codigo").Text = "" Then
            If SUConversiones.ConvierteAInt(utcAgencia.SelectedValue) = 0 Then
                utcAgencia.Focus()
                MsgBox("Tiene que escoger la agencia a exportar.  Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            ProgressBar1.Focus()
            intCantidad = 0
            intContador = 0
            intNumFechaInicio = 0
            intNumFechaInicio = Format(DPFechaInicio.Value, "yyyyMMdd")
            intNumFechaFin = 0
            intNumFechaFin = Format(DPFechaFin.Value, "yyyyMMdd")
            strQuery = ""
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            'strQuery = "exec VerificaRegistrosAExportarCompras " & intNumFecha & "," & lngAgenRegExp
            strQuery = "exec VerificaRegistrosAExportarTrasladoInventario " & intNumFechaInicio & "," & intNumFechaFin & "," & lnRegistro
            'strQuery = strQuery + "e.numfechaing = " & intNumFecha & " and e.agenregistro = " & lngAgenRegExp & ""
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    intCantidad = dtrAgro2K.GetValue(0)
                    lnAgenciaOrigen = dtrAgro2K.GetValue(1)
                End If
            End While
            dtrAgro2K.Close()
            lnCdigoAgenciaOrigen = ObtieneCodigoAgencias(lnAgenciaOrigen)
            strNombreArchivo = ""
            If intCantidad <> 0 Then
                ProgressBar1.Minimum = 0
                ProgressBar1.Maximum = intCantidad
                ProgressBar1.Value = 0
                strQuery = ""
                'strQuery = " exec ConsultaExportarCompra " & intNumFecha & "," & lngAgenRegExp
                strQuery = " exec ConsultaExportarTrasladoInventario " & intNumFechaInicio & "," & intNumFechaFin & "," & lnRegistro

                dtrAgro2K.Close()
                cmdAgro2K.CommandText = strQuery
                strClave = ""
                strClave = "Agro2K_2008"
                strArchivo = ""
                strArchivo = ConfigurationManager.AppSettings("RutaArchivosTraslados").ToString() & "tras_" & Format(lnCdigoAgenciaOrigen, "00") & "_" & Format(lnCodigoAgencia, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt"
                strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosTraslados").ToString() & "tras_" & Format(lnCdigoAgenciaOrigen, "00") & "_" & Format(lnCodigoAgencia, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd")
                Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosTraslados").ToString() & "tras_" & Format(lnCdigoAgenciaOrigen, "00") & "_" & Format(lnCodigoAgencia, "00") & "_" & Format(DPFechaInicio.Value, "yyyyMMdd") & "_" & Format(DPFechaFin.Value, "yyyyMMdd") & ".txt")
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ProgressBar1.Value = ProgressBar1.Value + 1
                    sr.WriteLine(dtrAgro2K.GetValue(0) & "|" & Trim(dtrAgro2K.GetValue(1)) & "|" & dtrAgro2K.GetValue(2) & "|" & Trim(dtrAgro2K.GetValue(3)) & "|" & Trim(dtrAgro2K.GetValue(4)) & "|" & Trim(dtrAgro2K.GetValue(5)) & "|" & Trim(dtrAgro2K.GetValue(6)) & "|" & dtrAgro2K.GetValue(7) & "|" & dtrAgro2K.GetValue(8) & "|" & dtrAgro2K.GetValue(9) & "|" & dtrAgro2K.GetValue(10) & "|" & Trim(dtrAgro2K.GetValue(11)) & "|" & dtrAgro2K.GetValue(12) & "|" & dtrAgro2K.GetValue(13) & "|" & dtrAgro2K.GetValue(14) & "|" & dtrAgro2K.GetValue(15) & "|" & dtrAgro2K.GetValue(16) & "|" & dtrAgro2K.GetValue(17) & "|" & Trim(dtrAgro2K.GetValue(18)) & "|" & Trim(dtrAgro2K.GetValue(19)) & "|" & dtrAgro2K.GetValue(20) & "|" & dtrAgro2K.GetValue(21) & "|" & dtrAgro2K.GetValue(22) & "|" & dtrAgro2K.GetValue(24) & "|" & dtrAgro2K.GetValue(26) & "|" & dtrAgro2K.GetValue(27))
                End While
                sr.Close()
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                intIncr = 0
                Try
                    'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                    'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                    lsRutaCompletaArchivoZip = String.Empty
                    lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
                    If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                        MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                        Exit Sub
                    End If
                    If File.Exists(strArchivo) = True Then
                        File.Delete(strArchivo)
                    End If
                Catch exx As Exception
                    MsgBox(exx.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End Try
            Else
                MsgBox("No hay Requisas que exportar para el d�a solicitado.", MsgBoxStyle.Information, "Error en Solicitud")
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                ProgressBar1.Value = 0
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            MsgBox("Finalizo el proceso de exportaci�n de las Requisas", MsgBoxStyle.Information, "Proceso Finalizado")
            ProgressBar1.Value = 0
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            'MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            Throw ex
        End Try

        Me.Cursor = Cursors.Default

    End Sub

    Private Sub utcAgencia_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs)

    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Try
            Procesar()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Me.Cursor = Cursors.Default
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
            'Throw ex
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
