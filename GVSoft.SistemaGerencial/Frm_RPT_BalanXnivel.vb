﻿Imports System.Windows.Forms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar

Public Class Frm_RPT_BalanXnivel
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPT_BalanXnivel"

    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load

        Dim i As Integer = 0
        Dim val_max As Integer = 0
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim sqlconexion As SqlConnection = Nothing
        REM declaramos las tablas del dataset***DATAADAPTERS
        Dim DAView_catalogoconta As SqlDataAdapter = Nothing
        Dim DAsaldos_reporte_tmp As SqlDataAdapter = Nothing

        'Dim dset_saldos_reporte_tmp As New DS_Anexos_saldos
        Dim dset_saldos_reporte_tmp As DataSet = Nothing
        Dim sql_View_catalogoconta As String = String.Empty

        Dim sql_saldos_reporte_tmp As String = String.Empty
        Dim data As DataSet = Nothing
        Dim report_balanXnivel As RPT_BalanXnivel = Nothing
        Try
            val_max = SIMF_CONTA.numeformulas
            dset_saldos_reporte_tmp = New DataSet
            sql_View_catalogoconta = "select * from saldos_reporte_tmp"
            sql_saldos_reporte_tmp = "select * from saldos_reporte_tmp"
            sqlconexion = New SqlConnection(SIMF_CONTA.ConnectionStringconta)
            DAView_catalogoconta = New SqlDataAdapter(sql_View_catalogoconta, sqlconexion)
            'DAsaldos_reporte_tmp.Fill(dset_saldos_reporte_tmp, "saldos_reporte_tmp")
            DAView_catalogoconta.Fill(dset_saldos_reporte_tmp, "saldos_reporte_tmp")

            data = jackconsulta.retornaDataset(sql_saldos_reporte_tmp, True)
            report_balanXnivel = New RPT_BalanXnivel
            report_balanXnivel.SetDataSource(dset_saldos_reporte_tmp)
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_balanXnivel.SetParameterValue(nombreformula, valor)
                End If
            Next
            report_balanXnivel.RecordSelectionFormula = SIMF_CONTA.filtro_reporte
            ' asignar el objeto informe al control visualizador
            Me.CrystalReportViewer1.ReportSource = report_balanXnivel
            Me.CrystalReportViewer1.Refresh()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("CrystalReportViewer1_Load - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

End Class