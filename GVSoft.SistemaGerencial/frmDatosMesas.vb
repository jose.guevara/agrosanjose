Imports System.Data.SqlClient
Imports System.Text
'Imports System.Web.UI.WebControls
Imports System.Windows.Forms
Imports Infragistics.Win
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports System.Data

Public Class frmDatosMesas
    'Inherits DevComponents.DotNetBar.Office2007Form

    Public dataSetArbol As DataSet


    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)


    End Sub


    Private Sub CrearNodosDelPadre(ByVal indicePadre As Integer, ByVal nodePadre As UltraWinTree.UltraTreeNode)
        ' Crear un DataView con los Nodos que dependen del Nodo padre pasado como par�metro.
        Dim dataViewHijos As New DataView(dataSetArbol.Tables("TablaArbol"))
        dataViewHijos.RowFilter = dataSetArbol.Tables("TablaArbol").Columns("IdentificadorPadre").ColumnName + " = " + indicePadre

        ' Agregar al TreeView los nodos Hijos que se han obtenido en el DataView.
        For Each dataRowCurrent As DataRowView In dataViewHijos
            Dim nuevoNodo As New UltraWinTree.UltraTreeNode()
            nuevoNodo.Text = dataRowCurrent("NombreNodo").ToString().Trim()

            ' si el par�metro nodoPadre es nulo es porque es la primera llamada, son los Nodos
            ' del primer nivel que no dependen de otro nodo.
            'If nodePadre = Nothing Then
            '    utvMesas1.Nodes.Add(nuevoNodo)
            'Else
            '    ' se a�ade el nuevo nodo al nodo padre.
            '    nodePadre.Nodes.Add(nuevoNodo)
            'End If

            ' Llamada recurrente al mismo m�todo para agregar los Hijos del Nodo reci�n agregado.

            CrearNodosDelPadre(Int32.Parse(dataRowCurrent("IdentificadorNodo").ToString()), nuevoNodo)
        Next

    End Sub

    Private Sub LlenarNodos()
        ' Para sacar valores aleatorios
        Dim rnd As New Random

        ' A�adir nodos al TreeView
        ' y asignarles claves para que tengan el mismo valor
        ' que lo que devuelve FullPath
        With utvMesas1
            For i As Integer = 1 To 1
                Dim skNodo As String = "Mesas " '& i.ToString("00")
                Dim tvn As UltraWinTree.UltraTreeNode = .Nodes.Add(skNodo, "Mesas ") '& i.ToString("00"))
                For j As Integer = 1 To rnd.Next(2, 5) ' un valor de 2 a 4
                    Dim skHijo As String = skNodo & "\Mesa " & j.ToString
                    Dim tvn2 As UltraWinTree.UltraTreeNode = tvn.Nodes.Add(skHijo, "Mesa " & j.ToString)
                    'For k As Integer = 1 To Rnd.Next(2, 4) ' un valor de 2 a 3
                    '    Dim skNieto As String = skHijo & "\Nieto " & k.ToString
                    '    tvn2.Nodes.Add(skNieto, "Nieto " & k.ToString)
                    'Next
                Next
            Next
        End With

    End Sub

    Private Sub frmDatosMesas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LlenarNodos()
    End Sub

    Private Sub utvMesas1_AfterSelect(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinTree.SelectEventArgs) Handles utvMesas1.AfterSelect
        Dim sNodoSelect As String

        sNodoSelect = utvMesas1.SelectedNodes.Item(0).Text '.Nodes.ToString() '  .SelectedNodes.ToString() ' .SelectedNode.Name.ToString()

        Select Case sNodoSelect
            Case "Mesa 1"
                'MessageBox.Show("El nodo Seleccionado es: " + e.Node.Name.ToString(), "Nodos 1", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim frmFact As Form = frmFacturas
                'frmFact.MdiParent = Me.MdiParent

                frmFact.TopMost = False
                frmFact.TopLevel = False
                frmFact.Parent = Me.PanelEx2 ' Me.spSeparador.Panel2
                frmFact.Dock = DockStyle.Fill
                frmFact.FormBorderStyle = Windows.Forms.FormBorderStyle.None
                'frmFact.BackColor = Color.White
                frmFact.Show()

            Case "Mesa 2"
                MessageBox.Show("El nodo Seleccionado es: " + e.NewSelections.Item(0).Text, "Nodos 2", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Case Else
                MessageBox.Show("El nodo Seleccionado es: " + e.NewSelections.Item(0).Text, "Nodos 3", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Select

    End Sub
End Class