Imports System.Data
Imports CrystalDecisions.Shared
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports DevComponents.DotNetBar

Public Class frmReportexCuenta
    Dim jackconsulta As New ConsultasDB
    Dim jackcrystal As New crystal_jack
    Dim jackvalida As New Valida
    Dim tipo_report As String
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "frmReportexCuenta"

    Private Sub ToolbarB_BuscarMov_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_BuscarMov.Click
        Dim sql_MoviCta As String = String.Empty
        Dim sql_where As String = String.Empty
        Dim where As Boolean = False

        Dim sql_MoviCta_Temp As String = String.Empty
        Dim FechaInicio As String = String.Empty
        Dim FechaFin As String = String.Empty
        Dim dTOS As DataTable = Nothing
        Dim dtsDatos As DataTable = Nothing
        Dim si_local As Double = 0
        Dim si_ext As Double = 0
        Dim debe_local As Double = 0
        Dim haber_local As Double = 0
        Try

            dTOS = Nothing
            'Dim dtsDatos As DataTable
            dtsDatos = Nothing
            dTOS = New DataTable
            si_local = 0
            si_ext = 0
            debe_local = 0
            haber_local = 0
            FechaInicio = Format(CDate(DateF_FechaIni_Movi.Text.Trim).Year, "0000") & Format(CDate(DateF_FechaIni_Movi.Text.Trim).Month, "00") & Format(CDate(DateF_FechaIni_Movi.Text.Trim).Day, "00")
            FechaFin = Format(CDate(DateF_FechaFin_Movi.Text.Trim).Year, "0000") & Format(CDate(DateF_FechaFin_Movi.Text.Trim).Month, "00") & Format(CDate(DateF_FechaFin_Movi.Text.Trim).Day, "00")
            ' dtsDatos = jackconsulta.Retornadatatable(sql_MoviCta, dTOS, True) 'clase que genera la consulta y devuelve un dataTable
            dtsDatos = RNReporte.ObtieneReportexCuentaListado(FechaInicio, FechaFin, TextB_CuentaContableMovi_ini.Text.Trim(), TextB_CuentaContableMovi_fin.Text.Trim())
            'If GridP_MoviCta.RowCount > 0 Then
            '    GridP_MoviCta.Rows.Clear()
            'End If
            GridP_MoviCta.DataSource = Nothing
            If dtsDatos.Rows.Count > 0 Then
                GridP_MoviCta.DataSource = dtsDatos
                jackconsulta.DameMoviCta(TextB_CuentaContableMovi_ini.Text.Trim, DateF_FechaIni_Movi.Text, DateF_FechaFin_Movi.Text, debe_local, haber_local, True)
                jackconsulta.DameSaldoiniCta(TextB_CuentaContableMovi_ini.Text.Trim, DateF_FechaIni_Movi.Text, si_local, si_ext, True)
                TextB_saldoinilocal.Text = si_local
                TextB_saldoinilocal.Text = jackvalida.formato_moneda(TextB_saldoinilocal.Text)
                TextB_debelocal.Text = debe_local
                TextB_debelocal.Text = jackvalida.formato_moneda(TextB_debelocal.Text)
                TextB_haberlocal.Text = haber_local
                TextB_haberlocal.Text = jackvalida.formato_moneda(TextB_haberlocal.Text)
                TextB_saldofinlocal.Text = si_local + debe_local - haber_local
                TextB_saldofinlocal.Text = jackvalida.formato_moneda(TextB_saldofinlocal.Text)
                GridP_MoviCta.Columns(6).DefaultCellStyle.Format = "##,##0.00"
                GridP_MoviCta.Columns(5).DefaultCellStyle.Format = "##,##0.00"
                GridP_MoviCta.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_MoviCta.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_MoviCta.Show()
            Else
                GridP_MoviCta.Hide()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Frm_MoviCuentas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-NI")
        Try
            TextB_CuentaContableMovi_ini.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
            TextB_CuentaContableMovi_ini.AutoCompleteMode = AutoCompleteMode.Suggest
            TextB_CuentaContableMovi_ini.AutoCompleteSource = AutoCompleteSource.CustomSource
            TextB_CuentaContableMovi_fin.AutoCompleteCustomSource = DataHelper.LoadAutoComplete("select * from catalogoconta  order by cdgocuenta", "cdgocuenta")
            TextB_CuentaContableMovi_fin.AutoCompleteMode = AutoCompleteMode.Suggest
            TextB_CuentaContableMovi_fin.AutoCompleteSource = AutoCompleteSource.CustomSource
            DateF_FechaIni_Movi.Text = jackconsulta.DameCampo_UltimoCierre(True, False, "Fecha_ini")
            DateF_FechaFin_Movi.Text = jackconsulta.DameCampo_UltimoCierre(True, False, "Fecha_fin")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ButtonI_Imprimir_MoviCta_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_Imprimir_MoviCta.Click
        Dim si_local As Double = 0, si_ext As Double = 0
        Dim diaini As String = String.Empty
        Dim diafin As String = String.Empty
        Dim fechaini As Date
        Dim fechafin As Date
        Dim ddiaini As String = (Now.Day)
        Try
            If TextB_CuentaContableMovi_ini.Text.Trim.Length <= 0 Then
                MessageBoxEx.Show("Favor digitar cuenta inicio", " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            'If TextB_CuentaContableMovi_fin.Text.Trim.Length <= 0 Then
            '    MessageBoxEx.Show("Favor digitar cuenta fin", " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Return
            'End If
            fechaini = CDate(DateF_FechaIni_Movi.Text)
            fechafin = CDate(DateF_FechaFin_Movi.Text)
            ToolbarB_BuscarMov_Click(sender, e)
            jackconsulta.DameSaldoiniCta(TextB_CuentaContableMovi_ini.Text.Trim, DateF_FechaIni_Movi.Text, si_local, si_ext, True)
            'SIMF_CONTA.formula
            SIMF_CONTA.formula(0) = "saldoinicial," & SUConversiones.ConvierteAInt(jackconsulta.MntoCuenta_SI)
            ' SIMF_CONTA.formula(1) = "compania," & SIMF_CONTA.nombre_cia.Trim
            SIMF_CONTA.formula(1) = "compania," & SUConversiones.ConvierteAString(RNEmpresa.ObtieneNombreEmpreaContabilidad())
            SIMF_CONTA.formula(2) = "RangoFecha, Del " & fechaini & " Al " & fechafin
            SIMF_CONTA.formula(3) = SUConversiones.ConvierteAString(TextB_CuentaContableMovi_ini.Text).Trim
            SIMF_CONTA.formula(4) = SUConversiones.ConvierteAString(TextB_CuentaContableMovi_fin.Text).Trim
            SIMF_CONTA.formula(5) = Format(fechaini.Year, "0000") & Format(fechaini.Month, "00") & Format(fechaini.Day, "00")
            SIMF_CONTA.formula(6) = Format(fechafin.Year, "0000") & Format(fechafin.Month, "00") & Format(fechafin.Day, "00")
            SIMF_CONTA.numeformulas = 7
            'jackcrystal.carga_parametros(2)
            SIMF_CONTA.filtro_reporte = String.Empty
            Frm_RPT_CtaMovi.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonI_Imprimir_MoviCta_Click - Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub ButtonI_Menu_Click(sender As System.Object, e As System.EventArgs) Handles ButtonI_Menu.Click
        Me.Close()
    End Sub

    Private Sub ToolbarB_limpiartodo_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_limpiartodo.Click
        TextB_CuentaContableMovi_ini.Text = ""
        TextB_CuentaContableMovi_fin.Text = ""
        TextB_saldoinilocal.Text = "0.00"
        TextB_debelocal.Text = "0.00"
        TextB_haberlocal.Text = "0.00"
        TextB_saldofinlocal.Text = "0.00"
    End Sub

    Private Sub GridP_MoviCta_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub GridP_MoviCta_Click(sender As Object, e As System.EventArgs)
        Dim row_MoviCta As DataGridViewRow = Nothing
        Try
            row_MoviCta = GridP_MoviCta.CurrentRow
            If row_MoviCta Is Nothing Then
                Exit Sub
            End If
            TextB_CuentaContableMovi_ini.Text = SUConversiones.ConvierteAString(row_MoviCta.Cells(7).Value).Trim
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub DateF_FechaIni_Movi_TextChanged(sender As Object, e As System.EventArgs) Handles DateF_FechaIni_Movi.TextChanged
        Try
            If DateF_FechaIni_Movi.Text.Equals(String.Empty) Then
                DateF_FechaIni_Movi.Text = jackconsulta.DameCampo_UltimoCierre(True, False, "Fecha_ini")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub DateF_FechaFin_Movi_TextChanged(sender As Object, e As System.EventArgs) Handles DateF_FechaFin_Movi.TextChanged
        Try
            If DateF_FechaFin_Movi.Text.Equals(String.Empty) Then
                DateF_FechaFin_Movi.Text = jackconsulta.DameCampo_UltimoCierre(True, False, "Fecha_fin")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_CuentaContableMovi_ini_TextChanged(sender As Object, e As System.EventArgs) Handles TextB_CuentaContableMovi_ini.TextChanged
        Try
            If TextB_CuentaContableMovi_ini.Text <> "" Then
                If jackvalida.existe_cuenta(TextB_CuentaContableMovi_ini.Text.ToString.Trim) = False Then
                    Label_ctainicial.Text = " **** La Cuenta No Existe****"
                Else
                    Label_ctainicial.Text = jackconsulta.Dame_DescCuenata(TextB_CuentaContableMovi_ini.Text.ToString.Trim)
                End If
            Else
                Label_ctainicial.Text = "?"
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TextB_CuentaContableMovi_fin_TextChanged(sender As Object, e As System.EventArgs) Handles TextB_CuentaContableMovi_fin.TextChanged
        Try
            If TextB_CuentaContableMovi_fin.Text <> "" Then
                If jackvalida.existe_cuenta(TextB_CuentaContableMovi_fin.Text.ToString.Trim) = False Then
                    Label_ctafinal.Text = " **** La Cuenta No Existe****"
                Else
                    Label_ctafinal.Text = jackconsulta.Dame_DescCuenata(TextB_CuentaContableMovi_fin.Text.ToString.Trim)
                End If
            Else
                Label_ctafinal.Text = "?"
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Error: " & ex.Message, " Movimiento Cuenta", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button_limpia_ctainicial_Click(sender As System.Object, e As System.EventArgs) Handles Button_limpia_ctainicial.Click
        TextB_CuentaContableMovi_ini.Text = ""
    End Sub

    Private Sub Button_limpia_ctafinal_Click(sender As System.Object, e As System.EventArgs) Handles Button_limpia_ctafinal.Click
        TextB_CuentaContableMovi_fin.Text = ""
    End Sub


End Class