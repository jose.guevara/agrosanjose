Imports DataDynamics.ActiveReports 
Imports DataDynamics.ActiveReports.Document 

Public Class actrptImprimeFactContado
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub PageHeader1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageHeader1.Format

    End Sub

    Private Sub actrptImprimeFactContado_ReportStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ReportStart
        ' ''lblDia.Text = intDiaFact
        ' ''lblMes.Text = strMesFact
        ' ''lblAnio.Text = lngYearFact
        ' ''lblNombreCliente.Text = UCase(strClienteFact)
        ' ''lblVendedor.Text = UCase(strVendedor)

        ' ''lblSubTotal.Text = Format(dblSubtotal, "#,##0.#0")
        ' ''lblImpuesto.Text = Format(dblImpuesto, "#,##0.#0")
        ' ''lblTotal.Text = Format(dblTotal, "#,##0.#0")
        '' ''lblNumeroFactura.Text = strNumeroFact

        If intRptImpCompras = 4 Then
            Label21.Visible = False
            lblDiasCredito.Visible = False
            Label20.Visible = False
            lblFechaVence.Visible = False
        End If

        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperWidth = 8.5F

        Me.Document.Printer.PrinterName = ""

    End Sub

    Private Sub PageFooter1_Format(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PageFooter1.Format
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperHeight = 6.0F
        Me.PageSettings.PaperWidth = 8.5F

    End Sub

    Private Sub actrptImprimeFactContado_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Me.Document.Print(True, False)
    End Sub
End Class
