Imports System.Data.SqlClient
Imports System.Text

Public Class frmPorcentajes2
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnCambios As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPorcentajes2))
        Dim UltraStatusPanel5 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel6 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.btnCambios = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 75)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(448, 67)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dato de la Retención"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DecimalPlaces = 2
        Me.NumericUpDown1.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
        Me.NumericUpDown1.Location = New System.Drawing.Point(96, 32)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(64, 20)
        Me.NumericUpDown1.TabIndex = 5
        Me.NumericUpDown1.Tag = "Porcentaje a utilizar para calculo del IVA"
        Me.NumericUpDown1.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Porc Retención"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem14})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 0
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 151)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel6.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel5, UltraStatusPanel6})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(448, 23)
        Me.UltraStatusBar1.TabIndex = 21
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.btnCambios, Me.LabelItem4, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(448, 69)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 62
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'btnCambios
        '
        Me.btnCambios.Image = CType(resources.GetObject("btnCambios.Image"), System.Drawing.Image)
        Me.btnCambios.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.btnCambios.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnCambios.Name = "btnCambios"
        Me.btnCambios.Text = "Cambios"
        Me.btnCambios.Tooltip = "Limpiar"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmPorcentajes2
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(448, 174)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmPorcentajes2"
        Me.Text = "frmPorcentajes2"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strCambios(0) As Double
    Private Sub frmPorcentajes2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Limpiar()
        UbicarRegistro()
        intModulo = 31

    End Sub

    Private Sub frmPorcentajes2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem12.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = 1
                frmNew.ShowDialog()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Guardar()
        '    Case 1, 2
        '        Limpiar()
        '        UbicarRegistro()
        '    Case 3
        '        Me.Close()
        'End Select

    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngPorcentajes2", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter

        With prmTmp01
            .ParameterName = "@dblRet"
            .SqlDbType = SqlDbType.Decimal
            .Value = NumericUpDown1.Value
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            cmdTmp.ExecuteNonQuery()
            If NumericUpDown1.Value <> strCambios(0) Then
                Ing_Bitacora(intModulo, 1, "Retención", strCambios(0), NumericUpDown1.Value)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicación Satisfactoria")
            UbicarRegistro()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicación")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar()

        NumericUpDown1.Value = 2
        NumericUpDown1.Focus()

    End Sub

    Sub UbicarRegistro()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select * From prm_Porcentajes2"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            NumericUpDown1.Value = Format(dtrAgro2K.GetValue(0), "#,##0.#0")
            strCambios(0) = Format(NumericUpDown1.Value, "#,##0.#0")
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub NumericUpDown1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumericUpDown1.GotFocus

        UltraStatusBar1.Panels.Item(0).Text = "Retención"
        UltraStatusBar1.Panels.Item(1).Text = "Retención para su aplicación en el módulo de chequeras"

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub


    Private Sub btnCambios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambios.Click
        Dim frmNew As New frmBitacora
        lngRegistro = 1
        frmNew.ShowDialog()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
