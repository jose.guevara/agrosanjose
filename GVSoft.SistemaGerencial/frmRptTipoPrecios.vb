Public Class frmRptTipoPrecios
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ListBox12 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox11 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox10 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox9 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem20 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.ListBox12 = New System.Windows.Forms.ListBox
        Me.ListBox11 = New System.Windows.Forms.ListBox
        Me.ListBox10 = New System.Windows.Forms.ListBox
        Me.ListBox9 = New System.Windows.Forms.ListBox
        Me.ListBox8 = New System.Windows.Forms.ListBox
        Me.ListBox7 = New System.Windows.Forms.ListBox
        Me.ListBox6 = New System.Windows.Forms.ListBox
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.MenuItem19 = New System.Windows.Forms.MenuItem
        Me.MenuItem20 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ListBox12)
        Me.GroupBox1.Controls.Add(Me.ListBox11)
        Me.GroupBox1.Controls.Add(Me.ListBox10)
        Me.GroupBox1.Controls.Add(Me.ListBox9)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(768, 184)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(528, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(224, 16)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Registro de las SubClases"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox12
        '
        Me.ListBox12.Location = New System.Drawing.Point(672, 160)
        Me.ListBox12.Name = "ListBox12"
        Me.ListBox12.Size = New System.Drawing.Size(80, 17)
        Me.ListBox12.TabIndex = 47
        Me.ListBox12.Visible = False
        '
        'ListBox11
        '
        Me.ListBox11.Location = New System.Drawing.Point(520, 160)
        Me.ListBox11.Name = "ListBox11"
        Me.ListBox11.Size = New System.Drawing.Size(120, 17)
        Me.ListBox11.TabIndex = 46
        Me.ListBox11.Visible = False
        '
        'ListBox10
        '
        Me.ListBox10.HorizontalScrollbar = True
        Me.ListBox10.Location = New System.Drawing.Point(672, 32)
        Me.ListBox10.Name = "ListBox10"
        Me.ListBox10.Size = New System.Drawing.Size(88, 121)
        Me.ListBox10.TabIndex = 45
        '
        'ListBox9
        '
        Me.ListBox9.HorizontalScrollbar = True
        Me.ListBox9.Location = New System.Drawing.Point(520, 32)
        Me.ListBox9.Name = "ListBox9"
        Me.ListBox9.Size = New System.Drawing.Size(144, 121)
        Me.ListBox9.TabIndex = 44
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(416, 160)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(88, 17)
        Me.ListBox8.TabIndex = 43
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(264, 160)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(120, 17)
        Me.ListBox7.TabIndex = 42
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(416, 32)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(88, 121)
        Me.ListBox6.TabIndex = 41
        '
        'ListBox5
        '
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(264, 32)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(144, 121)
        Me.ListBox5.TabIndex = 40
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(272, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(232, 16)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "Registro de las Clases"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(224, 16)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Registro de los Proveedores"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(160, 160)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(80, 17)
        Me.ListBox4.TabIndex = 37
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(8, 160)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(120, 17)
        Me.ListBox3.TabIndex = 36
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(160, 32)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(88, 121)
        Me.ListBox2.TabIndex = 35
        '
        'ListBox1
        '
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(8, 32)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(144, 121)
        Me.ListBox1.TabIndex = 34
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem15})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem19, Me.MenuItem20, Me.MenuItem14})
        Me.MenuItem1.Text = "Exportar"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Excel"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "Html"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Pdf"
        '
        'MenuItem19
        '
        Me.MenuItem19.Index = 3
        Me.MenuItem19.Text = "Rtf"
        '
        'MenuItem20
        '
        Me.MenuItem20.Index = 4
        Me.MenuItem20.Text = "Tiff"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 5
        Me.MenuItem14.Text = "Ninguno"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem4, Me.MenuItem5, Me.MenuItem6})
        Me.MenuItem2.Text = "Reportes"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 0
        Me.MenuItem4.Text = "Por Proveedor"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 1
        Me.MenuItem5.Text = "Por Clase"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 2
        Me.MenuItem6.Text = "Por SubClase"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 3
        Me.MenuItem15.Text = "Salir"
        '
        'frmRptTipoPrecios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(768, 185)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRptTipoPrecios"
        Me.Text = "Reporte de los Tipos de Precios"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRptTipoPrecios_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        Iniciar()
        Limpiar()
        If intAccesos(112) = 0 Then
            MenuItem1.Visible = False
        End If

    End Sub

    Private Sub frmRptTipoPrecios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Registro, Nombre From prm_Proveedores Where Estado <> 2 order by Nombre"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox1.Items.Add(dtrAgro2K.GetValue(1))
            ListBox3.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Clases Where Estado <> 2 order by descripcion"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox5.Items.Add(dtrAgro2K.GetValue(1))
            ListBox7.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_SubClases Where Estado <> 2 order by descripcion"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ListBox9.Items.Add(dtrAgro2K.GetValue(1))
            ListBox11.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        ListBox2.Items.Clear()
        ListBox4.Items.Clear()
        ListBox6.Items.Clear()
        ListBox8.Items.Clear()
        ListBox10.Items.Clear()
        ListBox12.Items.Clear()
        intRptExportar = 0

    End Sub

    Private Sub ListBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.DoubleClick, ListBox2.DoubleClick, ListBox5.DoubleClick, ListBox6.DoubleClick, ListBox9.DoubleClick, ListBox10.DoubleClick

        Select Case sender.name.ToString
            Case "ListBox1" : ListBox2.Items.Add(ListBox1.SelectedItem) : ListBox3.SelectedIndex = ListBox1.SelectedIndex : ListBox4.Items.Add(ListBox3.SelectedItem)
            Case "ListBox2"
                If ListBox2.SelectedIndex >= 0 Then
                    ListBox4.Items.RemoveAt(ListBox2.SelectedIndex) : ListBox2.Items.RemoveAt(ListBox2.SelectedIndex)
                End If
            Case "ListBox5" : ListBox6.Items.Add(ListBox5.SelectedItem) : ListBox7.SelectedIndex = ListBox5.SelectedIndex : ListBox8.Items.Add(ListBox7.SelectedItem)
            Case "ListBox6"
                If ListBox6.SelectedIndex >= 0 Then
                    ListBox8.Items.RemoveAt(ListBox6.SelectedIndex) : ListBox6.Items.RemoveAt(ListBox6.SelectedIndex)
                End If
            Case "ListBox9" : ListBox10.Items.Add(ListBox9.SelectedItem) : ListBox11.SelectedIndex = ListBox9.SelectedIndex : ListBox12.Items.Add(ListBox11.SelectedItem)
            Case "ListBox10"
                If ListBox10.SelectedIndex >= 0 Then
                    ListBox12.Items.RemoveAt(ListBox10.SelectedIndex) : ListBox10.Items.RemoveAt(ListBox10.SelectedIndex)
                End If
        End Select

    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem14.Click, MenuItem15.Click, MenuItem16.Click, MenuItem17.Click, MenuItem18.Click, MenuItem19.Click, MenuItem20.Click

        Dim frmNew As New actrptViewer

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        strQuery = ""
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptTipoPrecio = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        MenuItem16.Checked = False
        MenuItem17.Checked = False
        MenuItem18.Checked = False
        MenuItem19.Checked = False
        MenuItem20.Checked = False
        MenuItem14.Checked = False
        Select Case sender.text.ToString
            Case "Excel" : intRptExportar = 1 : MenuItem16.Checked = True
            Case "Html" : intRptExportar = 2 : MenuItem17.Checked = True
            Case "Pdf" : intRptExportar = 3 : MenuItem18.Checked = True
            Case "Rtf" : intRptExportar = 4 : MenuItem19.Checked = True
            Case "Tiff" : intRptExportar = 5 : MenuItem20.Checked = True
            Case "Ninguno" : intRptExportar = 0 : MenuItem14.Checked = True
            Case "Por Proveedor" : intRptTipoPrecio = 1
            Case "Por Clase" : intRptTipoPrecio = 2
            Case "Por SubClase" : intRptTipoPrecio = 3
            Case "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select
        If intRptTipoPrecio > 0 Then
            strQuery = "exec sp_RptTipoPrecios " & intRptTipoPrecio & ", "
            GenerarQuery()
            strFechaReporte = ""
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub GenerarQuery()

        If ListBox4.Items.Count > 0 Then                                        'Proveedores
            strQuery = strQuery + "'and p.regproveedor in ("
            If ListBox4.Items.Count = 1 Then
                ListBox4.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
            ElseIf ListBox4.Items.Count > 1 Then
                For intIncr = 0 To ListBox4.Items.Count - 2
                    ListBox4.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox4.SelectedItem & ", "
                Next
                ListBox4.SelectedIndex = ListBox4.Items.Count - 1
                strQuery = strQuery + "" & ListBox4.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox8.Items.Count > 0 Then                                        'Clases
            strQuery = strQuery + "'and p.regclase in ("
            If ListBox8.Items.Count = 1 Then
                ListBox8.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
            ElseIf ListBox8.Items.Count > 1 Then
                For intIncr = 0 To ListBox8.Items.Count - 2
                    ListBox8.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox8.SelectedItem & ", "
                Next
                ListBox8.SelectedIndex = ListBox8.Items.Count - 1
                strQuery = strQuery + "" & ListBox8.SelectedItem & ") ', "
            End If
        Else
            strQuery = strQuery + "' ', "
        End If
        If ListBox12.Items.Count > 0 Then                                       'SubClases
            strQuery = strQuery + "'and p.regsubclase in ("
            If ListBox12.Items.Count = 1 Then
                ListBox12.SelectedIndex = 0
                strQuery = strQuery + "" & ListBox12.SelectedItem & ") ' "
            ElseIf ListBox12.Items.Count > 1 Then
                For intIncr = 0 To ListBox12.Items.Count - 2
                    ListBox12.SelectedIndex = intIncr
                    strQuery = strQuery + "" & ListBox12.SelectedItem & ", "
                Next
                ListBox12.SelectedIndex = ListBox12.Items.Count - 1
                strQuery = strQuery + "" & ListBox12.SelectedItem & ") ' "
            End If
        Else
            strQuery = strQuery + "' ' "
        End If

    End Sub

End Class
