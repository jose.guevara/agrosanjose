Imports System.Data.SqlClient
Imports System.Text

Public Class frmTraslado
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DPkFechaFactura As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtNumeroCompra As System.Windows.Forms.TextBox
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    'Friend WithEvents medPrecio As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents MSFlexGrid2 As AxMSFlexGridLib.AxMSFlexGrid
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents medPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbeAgenciaDestino As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents txtAgenciaOrigen As System.Windows.Forms.TextBox
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents CmdAnular As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdImprimir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Label19 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTraslado))
        Dim UltraStatusPanel5 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel6 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtAgenciaOrigen = New System.Windows.Forms.TextBox
        Me.cbeAgenciaDestino = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.medPrecio = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.TextBox17 = New System.Windows.Forms.TextBox
        Me.txtCantidad = New System.Windows.Forms.TextBox
        Me.txtProducto = New System.Windows.Forms.TextBox
        Me.txtNumeroCompra = New System.Windows.Forms.TextBox
        Me.DPkFechaFactura = New System.Windows.Forms.DateTimePicker
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.MSFlexGrid2 = New AxMSFlexGridLib.AxMSFlexGrid
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.CmdAnular = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.cmdImprimir = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12, Me.MenuItem4})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "&Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "L&impiar"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem12.Text = "&Listado"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Vendedores"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Proveedores"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Productos"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Refrescar"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 4
        Me.MenuItem4.Text = "&Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtAgenciaOrigen)
        Me.GroupBox1.Controls.Add(Me.cbeAgenciaDestino)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.medPrecio)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.txtNumeroCompra)
        Me.GroupBox1.Controls.Add(Me.DPkFechaFactura)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 72)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(776, 124)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        '
        'txtAgenciaOrigen
        '
        Me.txtAgenciaOrigen.Location = New System.Drawing.Point(106, 24)
        Me.txtAgenciaOrigen.Name = "txtAgenciaOrigen"
        Me.txtAgenciaOrigen.ReadOnly = True
        Me.txtAgenciaOrigen.Size = New System.Drawing.Size(219, 20)
        Me.txtAgenciaOrigen.TabIndex = 34
        '
        'cbeAgenciaDestino
        '
        Me.cbeAgenciaDestino.DisplayMember = "Text"
        Me.cbeAgenciaDestino.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbeAgenciaDestino.FormattingEnabled = True
        Me.cbeAgenciaDestino.ItemHeight = 14
        Me.cbeAgenciaDestino.Location = New System.Drawing.Point(421, 22)
        Me.cbeAgenciaDestino.Name = "cbeAgenciaDestino"
        Me.cbeAgenciaDestino.Size = New System.Drawing.Size(236, 20)
        Me.cbeAgenciaDestino.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbeAgenciaDestino.TabIndex = 33
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(335, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 13)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "A la Agencia"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(11, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "De la Agencia"
        '
        'medPrecio
        '
        Me.medPrecio.Location = New System.Drawing.Point(373, 86)
        Me.medPrecio.Name = "medPrecio"
        Me.medPrecio.Size = New System.Drawing.Size(86, 20)
        Me.medPrecio.TabIndex = 27
        Me.medPrecio.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(327, 87)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Precio"
        Me.Label9.Visible = False
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(691, 29)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(24, 20)
        Me.TextBox17.TabIndex = 25
        Me.TextBox17.TabStop = False
        Me.TextBox17.Text = "0"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox17.Visible = False
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(106, 85)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(63, 20)
        Me.txtCantidad.TabIndex = 8
        Me.txtCantidad.Tag = "Cantidad a trasladar"
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProducto
        '
        Me.txtProducto.Location = New System.Drawing.Point(231, 85)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(91, 20)
        Me.txtProducto.TabIndex = 9
        Me.txtProducto.Tag = "C�digo del Producto"
        '
        'txtNumeroCompra
        '
        Me.txtNumeroCompra.Location = New System.Drawing.Point(282, 51)
        Me.txtNumeroCompra.Name = "txtNumeroCompra"
        Me.txtNumeroCompra.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroCompra.TabIndex = 1
        Me.txtNumeroCompra.Tag = "N�mero de Requisa"
        '
        'DPkFechaFactura
        '
        Me.DPkFechaFactura.CustomFormat = "dd-MMM-yyyy"
        Me.DPkFechaFactura.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkFechaFactura.Location = New System.Drawing.Point(106, 51)
        Me.DPkFechaFactura.Name = "DPkFechaFactura"
        Me.DPkFechaFactura.Size = New System.Drawing.Size(104, 20)
        Me.DPkFechaFactura.TabIndex = 0
        Me.DPkFechaFactura.Tag = "Fecha de Ingreso"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(41, 85)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Cantidad"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(175, 85)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Producto"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(234, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "N�mero"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(55, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.MSFlexGrid2)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 204)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(778, 173)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(64, 144)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 65
        Me.Label19.Text = "Label19"
        Me.Label19.Visible = False
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(8, 144)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 64
        Me.Label18.Text = "Label18"
        Me.Label18.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(304, 152)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 13)
        Me.Label15.TabIndex = 61
        Me.Label15.Text = "<F5> AYUDA"
        Me.Label15.Visible = False
        '
        'MSFlexGrid2
        '
        Me.MSFlexGrid2.Location = New System.Drawing.Point(8, 16)
        Me.MSFlexGrid2.Name = "MSFlexGrid2"
        Me.MSFlexGrid2.OcxState = CType(resources.GetObject("MSFlexGrid2.OcxState"), System.Windows.Forms.AxHost.State)
        Me.MSFlexGrid2.Size = New System.Drawing.Size(767, 128)
        Me.MSFlexGrid2.TabIndex = 52
        Me.MSFlexGrid2.TabStop = False
        '
        'Timer1
        '
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 386)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel6.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel5, UltraStatusPanel6})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(787, 23)
        Me.UltraStatusBar1.TabIndex = 20
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.LabelItem1, Me.CmdAnular, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem4, Me.cmdImprimir, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(787, 69)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 35
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        Me.cmdOK.Visible = False
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'CmdAnular
        '
        Me.CmdAnular.Image = CType(resources.GetObject("CmdAnular.Image"), System.Drawing.Image)
        Me.CmdAnular.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.CmdAnular.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.CmdAnular.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.CmdAnular.Name = "CmdAnular"
        Me.CmdAnular.Text = "Anular<F3>"
        Me.CmdAnular.Tooltip = "Anular Recibo"
        Me.CmdAnular.Visible = False
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Image = CType(resources.GetObject("cmdImprimir.Image"), System.Drawing.Image)
        Me.cmdImprimir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdImprimir.ImageListSizeSelection = DevComponents.DotNetBar.eButtonImageListSelection.Large
        Me.cmdImprimir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Text = "Imprimir<F8>"
        Me.cmdImprimir.Tooltip = "Imprimir recibos"
        Me.cmdImprimir.Visible = False
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "  "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmTraslado
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(787, 409)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmTraslado"
        Me.Text = "frmTraslado"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.MSFlexGrid2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strUnidad As String
    Dim strFacturas As String
    Dim strDescrip As String
    Dim intImpuesto As Integer
    Dim intImprimir As Integer
    Dim intRow, intCol As Integer
    Dim lngRegistro2 As Long
    Dim NumeroCompraExiste As Boolean = False
    Dim lCodigoAgenciaxDefecto As Integer
    Public txtCollection As New Collection

    Private Sub frmTraslado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim frmNew As New frmEscogerAgencia

        intDiaFact = 0
        strMesFact = ""
        lngYearFact = 0
        strVendedor = ""
        strNumeroFact = ""
        strCodCliFact = ""
        strClienteFact = ""

        Me.Top = 0
        Me.Left = 0
        strUnidad = ""
        strDescrip = ""
        lngRegistro = 0
        intImprimir = 0
        txtCollection.Add(txtNumeroCompra)

        'txtCollection.Add(txtNombreVendedor)
        'txtCollection.Add(txtNombreProveedor)
        txtCollection.Add(txtProducto)
        txtCollection.Add(txtCantidad)
        txtCollection.Add(medPrecio)
        'txtCollection.Add(TextBox9)
        'txtCollection.Add(TextBox10)
        'txtCollection.Add(TextBox11)
        'txtCollection.Add(TextBox12)

        'UbicarAgencia(lngRegUsuario)
        'If (lngRegAgencia = 0) Then
        '    lngRegAgencia = 0
        '    frmNew.ShowDialog()
        '    If lngRegAgencia = 0 Then
        '        MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
        '    End If
        'End If
        ObtieneAgenciaxDefecto()
        cbeAgenciaDestino.DataSource = ObtieneAgenciaUsuario()
        cbeAgenciaDestino.DisplayMember = "Descripcion"
        cbeAgenciaDestino.ValueMember = "codigo"
        Limpiar()
        MenuItem8.Visible = False
        'If intTipoCompra = 2 Or intTipoCompra = 5 Then
        '    MenuItem8.Visible = True
        '    UbicarPendientes()
        'End If
        'If lngRegAgencia = 0 Then
        '    Timer1.Interval = 200
        '    Timer1.Enabled = True
        'End If
        'Label16.Visible = False
        Label18.Text = intTipoCompra
        Label19.Text = lngRegAgencia
        FuncionTimer()
    End Sub

    Private Sub frmTraslado_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            intTipoCompra = CInt(Label18.Text)
            lngRegAgencia = CInt(Label19.Text)
            If intTipoCompra = 3 Or intTipoCompra = 6 Then
                If MSFlexGrid2.Rows <= 1 Then
                    MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                    Exit Sub
                End If
                'Anular()
                MsgBox("Compra fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Requisa")
                txtNumeroCompra.Focus()
            ElseIf intTipoCompra = 2 Or intTipoCompra = 5 Or intTipoCompra = 7 Or intTipoCompra = 8 Or intTipoCompra = 9 Or intTipoCompra = 10 Then
                If MSFlexGrid2.Rows <= 1 Then
                    MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                Guardar()
            End If
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        ElseIf e.KeyCode = Keys.F5 Then
            If MenuItem5.Visible = True Then
                intListadoAyuda = 1
            ElseIf MenuItem6.Visible = True Then
                intListadoAyuda = 7
            ElseIf MenuItem7.Visible = True Then
                intListadoAyuda = 3
            End If
            Dim frmNew As New frmListadoAyuda
            'Timer1.Interval = 200
            'Timer1.Enabled = True
            frmNew.ShowDialog()
        ElseIf e.KeyCode = Keys.F7 Then
            If MSFlexGrid2.Rows <= 1 Then
                MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                txtCantidad.Focus()
                Exit Sub
            End If
            intTipoCompra = CInt(Label18.Text)
            lngRegAgencia = CInt(Label19.Text)
            Guardar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem12.Click

        intTipoCompra = CInt(Label18.Text)
        lngRegAgencia = CInt(Label19.Text)
        Select Case sender.text.ToString()
            Case "Imprimir"
                If MSFlexGrid2.Rows <= 1 Then
                    MsgBox("No hay productos a imprimir", MsgBoxStyle.Critical, "Error de Datos")
                    txtCantidad.Focus()
                    Exit Sub
                End If
                Guardar()
                'UbicarPendientes()
            Case "&Guardar"
                If MSFlexGrid2.Rows <= 1 Then
                    MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
                    Exit Sub
                End If
                Guardar()
            Case "Anular"
                If MSFlexGrid2.Rows <= 1 Then
                    MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                    Exit Sub
                End If
                If intTipoCompra = 3 Or intTipoCompra = 6 Then
                    intResp = MsgBox("�Est� Seguro de Anular esta Requisa?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Acci�n")
                    If intResp = 6 Then
                        'Anular()
                        MsgBox("Requisa fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Requisa")
                        txtNumeroCompra.Focus()
                    End If
                End If
            Case "&Cancelar", "L&impiar" : Limpiar()
            Case "Vendedores" : intListadoAyuda = 1
            Case "Proveedores" : intListadoAyuda = 2
            Case "Productos" : intListadoAyuda = 3
            Case "&Salir" : Me.Close()
        End Select
        If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
            Dim frmNew As New frmListadoAyuda
            'Timer1.Interval = 200
            'Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        intTipoCompra = CInt(Label18.Text)
        lngRegAgencia = CInt(Label19.Text)
        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        If intTipoCompra = 3 Or intTipoCompra = 6 Then
        '            If MSFlexGrid2.Rows <= 1 Then
        '                MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
        '                Exit Sub
        '            End If
        '            intResp = MsgBox("�Est� Seguro de Anular esta Requisa?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Confirmaci�n de Anulaci�n")
        '            If intResp = 6 Then
        '                'Anular()
        '                MsgBox("Requisa ha sido Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Requisa")
        '                txtNumeroCompra.Focus()
        '            End If
        '        Else
        '            If MSFlexGrid2.Rows <= 1 Then
        '                MsgBox("No hay productos a Requisar", MsgBoxStyle.Critical, "Error de Datos")
        '                txtCantidad.Focus()
        '                Exit Sub
        '            End If
        '            Guardar()
        '        End If
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        Me.Close()
        'End Select

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumeroCompra.GotFocus, txtProducto.GotFocus, txtCantidad.GotFocus, DPkFechaFactura.GotFocus

        Dim strCampo As String

        Label15.Visible = False
        MenuItem5.Visible = False
        MenuItem6.Visible = False
        MenuItem7.Visible = False
        MenuItem12.Visible = False
        strCampo = ""
        Select Case sender.name.ToString()
            Case "DPkFechaFactura" : strCampo = "Fecha"
            Case "txtNumeroCompra" : strCampo = "N�mero"
                If intTipoCompra = 2 Or intTipoCompra = 5 Then
                    MenuItem12.Visible = True
                End If
            Case "txtDia" : strCampo = "D�as Cr�dito"
            Case "txtVencimiento" : strCampo = "Vencimiento"
            Case "txtVendedor" : strCampo = "Vendedor" : MenuItem12.Visible = True : MenuItem5.Visible = True : Label15.Visible = True
            Case "txtProveedor" : strCampo = "Proveedor"
                MenuItem6.Visible = True
                Label15.Visible = True
                If intTipoCompra = 4 Or intTipoCompra = 8 Then
                    MenuItem12.Visible = True
                    MenuItem6.Visible = True
                    Label15.Visible = True
                End If
            Case "txtProducto" : strCampo = "Producto" : MenuItem12.Visible = True : MenuItem7.Visible = True : Label15.Visible = True
            Case "txtCantidad" : strCampo = "Cantidad"
            Case "TextBox8" : strCampo = "Tipo de Precio"
            Case "ddlRentencion" : strCampo = "Retenci�n"
            Case "ddlTipoPrecio" : strCampo = "Tipo Precio"
        End Select
        UltraStatusBar1.Panels.Item(0).Text = strCampo
        UltraStatusBar1.Panels.Item(1).Text = sender.tag
        If sender.name <> "DPkFechaFactura" Then
            sender.selectall()
        End If
        If blnUbicar Then
            FuncionTimer()
        End If
    End Sub

    Sub ExtraerCompra()

        Me.Cursor = Cursors.WaitCursor
        Dim lngFecha As Long
        Dim cmdTmp As New SqlCommand("ExtraerCompra", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim cmdTmpx As New SqlCommand("UbicarCompras", cnnAgro2K)
        Dim prmTmpx01 As New SqlParameter
        Dim prmTmpx02 As New SqlParameter
        Dim strGenNumFact As String
        Dim dteFechaIng As Date

        strNumeroUbicarFactura = ""
        strFacturas = ""
        If intTipoCompra <> 2 And intTipoCompra <> 5 Then

            If Trim(txtNumeroCompra.Text) = "" Then
                MsgBox("Tiene que ingresar un n�mero de Requisa a extraer.", MsgBoxStyle.Critical, "Error en Digitalizaci�n")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        Else
            If Trim(txtNumeroCompra.Text) = "" Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        End If

        strFacturas = txtNumeroCompra.Text
        ' Nueva Modalidad de Busqueda y Verificacion por Compras Nuevas (I)
        'If intFechaCambioCompra >= 0 And (intTipoCompra = 6 Or intTipoCompra = 8 Or intTipoCompra = 10) Then
        If intFechaCambioCompra >= 0 And (intTipoCompra = 6 Or intTipoCompra = 10) Then
            strGenNumFact = ""
            'If Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "C" Or Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "A" Then
            '    txtNumeroCompra.Text = Mid(txtNumeroCompra.Text, 1, Len(txtNumeroCompra.Text) - 1)
            'End If
            'strGenNumFact = "('" & Format(CLng(txtNumeroCompra.Text), "0000000") & "C','" & Format(CLng(txtNumeroCompra.Text), "0000000") & "A')"
            strGenNumFact = txtNumeroCompra.Text.Trim()
            With prmTmpx01
                .ParameterName = "@lngAgencia"
                .SqlDbType = SqlDbType.TinyInt
                .Value = lngRegAgencia
            End With
            With prmTmpx02
                .ParameterName = "@strNumero"
                .SqlDbType = SqlDbType.VarChar
                .Value = strGenNumFact
            End With
            With cmdTmpx
                .Parameters.Add(prmTmpx01)
                .Parameters.Add(prmTmpx02)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmpx.Connection.State = ConnectionState.Open Then
                cmdTmpx.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmpx.Connection = cnnAgro2K
            Try
                dtrAgro2K = cmdTmpx.ExecuteReader
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                dtrAgro2K.Close()
                cmdTmpx.Connection.Close()
                cnnAgro2K.Close()
                Exit Sub
            End Try
            'If intFechaCambioCompra = 0 Then
            '    txtNumeroCompra.Text = Format(CLng(txtNumeroCompra.Text), "0000000") & "C"
            'Else
            '    txtNumeroCompra.Text = Format(CLng(txtNumeroCompra.Text), "0000000") & "A"
            'End If

            If (intTipoCompra = 6 Or intTipoCompra = 10) And intFechaCambioCompra > 0 Then
                If dtrAgro2K.HasRows = True Then
                    strNumeroUbicarFactura = ""
                    dtrAgro2K.Close()
                    dtrAgro2K = cmdTmpx.ExecuteReader
                    Dim frmNew As New frmUbicarCompras
                    frmNew.ShowDialog()
                    txtNumeroCompra.Text = strNumeroUbicarFactura
                End If
            End If
        End If
        ' Nueva Modalidad de Busqueda y Verificacion por Facturas Nuevas (F)

        If intFechaCambioCompra >= 0 And (intTipoCompra = 6 Or intTipoCompra = 8 Or intTipoCompra = 10) Then
            If strNumeroUbicarFactura = "" Then
                If Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "C" Or Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "A" Then
                    txtNumeroCompra.Text = Mid(txtNumeroCompra.Text, 1, Len(txtNumeroCompra.Text) - 1)
                End If
                If intFechaCambioCompra = 0 Then
                    txtNumeroCompra.Text = Format(CLng(txtNumeroCompra.Text), "0000000") & "C"
                Else
                    txtNumeroCompra.Text = Format(CLng(txtNumeroCompra.Text), "0000000") & "A"
                End If
            End If
        End If
        If intTipoCompra = 3 Or intTipoCompra = 7 Or intTipoCompra = 9 Then
            txtNumeroCompra.Text = txtNumeroCompra.Text.Trim
        End If
        With prmTmp01
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegAgencia
        End With
        With prmTmp02
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNumeroCompra.Text
        End With
        With prmTmp03
            .ParameterName = "@intTipoCompra"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intTipoCompra
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        dtrAgro2K = cmdTmp.ExecuteReader
        If intTipoCompra = 7 Or intTipoCompra = 8 Then
            strFacturas = txtNumeroCompra.Text
            dteFechaIng = DPkFechaFactura.Value
        End If
        Limpiar()
        lngFecha = 0
        If intTipoCompra = 2 Or intTipoCompra = 3 Or intTipoCompra = 9 Then
            While dtrAgro2K.Read
                TextBox17.Text = dtrAgro2K.GetValue(0)
                lngFecha = dtrAgro2K.GetValue(19)
                DPkFechaFactura.Value = DefinirFecha(lngFecha)
                DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                txtNumeroCompra.Text = dtrAgro2K.GetValue(2)
                'txtVendedor.Text = dtrAgro2K.GetValue(3)
                'txtNombreVendedor.Text = dtrAgro2K.GetValue(4)
                'txtNombreProveedor.Text = dtrAgro2K.GetValue(5)
                MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
                MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
                MSFlexGrid2.Col = 0
                MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(6), "#,##0.#0")
                MSFlexGrid2.Col = 1
                MSFlexGrid2.Text = dtrAgro2K.GetValue(7)
                MSFlexGrid2.Col = 2
                MSFlexGrid2.Text = dtrAgro2K.GetValue(8)
                MSFlexGrid2.Col = 3
                MSFlexGrid2.Text = dtrAgro2K.GetValue(9)
                MSFlexGrid2.Col = 4
                MSFlexGrid2.Text = "No"
                If dtrAgro2K.GetValue(10) > 0 Then
                    MSFlexGrid2.Text = "Si"
                End If
                MSFlexGrid2.Col = 5
                MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(11), "#,##0.#0")
                MSFlexGrid2.Col = 6
                MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(12), "#,##0.#0")
                MSFlexGrid2.Col = 7
                MSFlexGrid2.Text = dtrAgro2K.GetValue(13)
                'TextBox9.Text = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                'TextBox10.Text = Format(dtrAgro2K.GetValue(15), "#,##0.#0")
                'TextBox11.Text = Format(dtrAgro2K.GetValue(16), "#,##0.#0")
                'TextBox12.Text = Format(dtrAgro2K.GetValue(17), "#,##0.#0")
                'dblTipoCambio = Format(dtrAgro2K.GetValue(20), "###0.#0")
                'If dtrAgro2K.GetValue(18) = 1 Then
                '    Label16.Visible = True
                'Else
                '    Label16.Visible = False
                'End If
            End While
        ElseIf intTipoCompra = 5 Or intTipoCompra = 6 Or intTipoCompra = 10 Then
            While dtrAgro2K.Read
                TextBox17.Text = dtrAgro2K.GetValue(0)
                lngFecha = dtrAgro2K.GetValue(21)
                DPkFechaFactura.Value = DefinirFecha(lngFecha)
                txtNumeroCompra.Text = dtrAgro2K.GetValue(2)
                DPkFechaFactura.Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
                'txtVendedor.Text = dtrAgro2K.GetValue(4)
                'txtNombreVendedor.Text = dtrAgro2K.GetValue(5)
                'If dtrAgro2K.GetValue(6) = "" Then
                '    txtProveedor.Text = ""
                'Else
                '    txtProveedor.Text = dtrAgro2K.GetValue(6)
                'End If
                'txtNombreProveedor.Text = dtrAgro2K.GetValue(7)
                MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
                MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
                MSFlexGrid2.Col = 0
                MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(8), "#,##0.#0")
                MSFlexGrid2.Col = 1
                MSFlexGrid2.Text = dtrAgro2K.GetValue(9)
                MSFlexGrid2.Col = 2
                MSFlexGrid2.Text = dtrAgro2K.GetValue(10)
                MSFlexGrid2.Col = 3
                MSFlexGrid2.Text = dtrAgro2K.GetValue(11)
                MSFlexGrid2.Col = 4
                MSFlexGrid2.Text = "No"
                If dtrAgro2K.GetValue(12) > 0 Then
                    MSFlexGrid2.Text = "Si"
                End If
                MSFlexGrid2.Col = 5
                dblTipoCambio = CDbl(dtrAgro2K.GetValue(22))
                If Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "A" Then
                    MSFlexGrid2.Text = Format(CDbl(dtrAgro2K.GetValue(13)) / dblTipoCambio, "#,##0.#0")
                Else
                    MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(13), "#,##0.#0")
                End If
                MSFlexGrid2.Col = 6
                If Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "A" Then
                    MSFlexGrid2.Text = Format(CDbl(dtrAgro2K.GetValue(14)) / dblTipoCambio, "#,##0.#0")
                Else
                    MSFlexGrid2.Text = Format(dtrAgro2K.GetValue(14), "#,##0.#0")
                End If
                MSFlexGrid2.Col = 7
                MSFlexGrid2.Text = dtrAgro2K.GetValue(15)
                'If Microsoft.VisualBasic.Right(txtNumeroCompra.Text, 1) = "A" Then
                '    TextBox9.Text = Format(CDbl(dtrAgro2K.GetValue(16)) / dblTipoCambio, "#,##0.#0")
                '    TextBox10.Text = Format(CDbl(dtrAgro2K.GetValue(17)) / dblTipoCambio, "#,##0.#0")
                '    TextBox11.Text = Format(CDbl(dtrAgro2K.GetValue(18)) / dblTipoCambio, "#,##0.#0")
                '    TextBox12.Text = Format(CDbl(dtrAgro2K.GetValue(19)) / dblTipoCambio, "#,##0.#0")
                'Else
                '    TextBox9.Text = Format(dtrAgro2K.GetValue(16), "#,##0.#0")
                '    TextBox10.Text = Format(dtrAgro2K.GetValue(17), "#,##0.#0")
                '    TextBox11.Text = Format(dtrAgro2K.GetValue(18), "#,##0.#0")
                '    TextBox12.Text = Format(dtrAgro2K.GetValue(19), "#,##0.#0")
                'End If
                'If dtrAgro2K.GetValue(20) = 1 Then
                '    Label16.Visible = True
                'Else
                '    Label16.Visible = False
                'End If
            End While
        ElseIf intTipoCompra = 7 Or intTipoCompra = 8 Then
            While dtrAgro2K.Read
                txtNumeroCompra.Text = dtrAgro2K.GetValue(0)
            End While
            DPkFechaFactura.Value = dteFechaIng
        End If
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 6 Or intTipoCompra = 10 Then
            ' UbicarVendedor(txtVendedor.Text)
            'UbicarProveedor(txtProveedor.Text)
        End If
        Me.Cursor = Cursors.Default
        If intTipoCompra = 9 Or intTipoCompra = 10 Then
            txtNumeroCompra.Focus()
            txtNumeroCompra.SelectAll()
        End If
        If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If dtrAgro2K.IsClosed Then
                cnnAgro2K.Open()
                cmdTmp.Connection = cnnAgro2K
                dtrAgro2K = cmdTmp.ExecuteReader
            End If
            'strFacturas = ""
            NumeroCompraExiste = False
            While dtrAgro2K.Read
                txtNumeroCompra.Text = dtrAgro2K.GetValue(2)
                strFacturas = dtrAgro2K.GetValue(2)
                NumeroCompraExiste = True
            End While
        End If

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroCompra.KeyDown, txtProducto.KeyDown, txtCantidad.KeyDown, medPrecio.KeyDown, DPkFechaFactura.KeyDown

        If e.KeyCode = Keys.Enter Then
            intTipoCompra = CInt(Label18.Text)
            lngRegAgencia = CInt(Label19.Text)
            Select Case sender.name
                Case "DPkFechaFactura"
                    Select Case intTipoCompra
                        ' Case 1, 2 : txtVendedor.Focus()
                        'Case 3, 5, 6 : txtDias.Focus()
                        Case 1, 4, 7, 8, 3, 5, 6 : txtNumeroCompra.Focus()
                    End Select
                Case "txtNumeroCompra"
                    Select Case intTipoCompra
                        Case 3, 6 : ExtraerCompra()
                            If txtNumeroCompra.Text = "" Then
                                MsgBox("El N�mero Ingresado no Pertenece a una Requisa Emitida o la Requisa est� Anulada.", MsgBoxStyle.Information, "Requisa No Emitida o Anulada")
                                Limpiar()
                                txtNumeroCompra.Focus()
                                Exit Sub
                            Else
                                'txtProveedor.Focus()
                            End If
                            txtCantidad.Focus()
                        Case 1, 4, 7, 8 : ExtraerCompra()
                            If txtNumeroCompra.Text <> "" Then
                                MsgBox("El N�mero Ingresado ya Pertenece a una Requisa Emitida o la Requisa est� Anulada.", MsgBoxStyle.Information, "Requisa Emitida o Anulada")
                                txtNumeroCompra.Focus()
                                txtNumeroCompra.Text = ""
                                Exit Sub
                            End If
                            If (intTipoCompra = 1 Or intTipoCompra = 4) And NumeroCompraExiste Then
                                MsgBox("El N�mero Ingresado ya Pertenece a una Requisa Emitida o la Requisa est� Anulada.", MsgBoxStyle.Information, "Requisa Emitida o Anulada")
                                txtNumeroCompra.Focus()
                                txtNumeroCompra.Text = ""
                                Exit Sub
                            End If


                            If strFacturas <> "" Then
                                txtNumeroCompra.Text = strFacturas
                            End If
                            'If intTipoCompra = 8 Then
                            '    txtDias.Focus()
                            'End If
                            'If intTipoCompra = 1 Or intTipoCompra = 4 Then
                            '    txtProveedor.Focus()
                            'End If
                            'If intTipoCompra = 7 Then
                            '    txtVendedor.Focus()
                            'ElseIf intTipoCompra = 8 Then
                            '    txtDias.Focus()
                            'End If
                            txtCantidad.Focus()
                        Case 9, 10 : ExtraerCompra()
                            If txtNumeroCompra.Text = "" Then
                                MsgBox("El N�mero Ingresado no Pertenece a una Requisa Emitida.", MsgBoxStyle.Information, "Requisa No Emitida")
                                Limpiar()
                                txtNumeroCompra.Focus()
                                Exit Sub
                            End If
                    End Select
                    'Case "txtDias"
                    '    If IsNumeric(Trim(txtDias.Text)) = False Then
                    '        txtDias.Text = "0"
                    '    End If
                    '    txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + CInt(txtDias.Text)), "dd-MMM-yyyy")
                    'txtVendedor.Focus()
                Case "txtVendedor"
                    blnUbicar = False
                    'If UbicarVendedor(txtVendedor.Text) = False Then
                    'MsgBox("El C�digo de Vendedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                    'txtVendedor.Focus()
                    'Exit Sub
                    'End If
                    'If txtProveedor.Visible = True Then
                    '    txtProveedor.Focus()
                    'Else
                    '    txtNombreProveedor.Focus()
                    'End If
                    'Case "txtProveedor"
                    '    If intTipoCompra = 4 Or intTipoCompra = 8 Then
                    '        blnUbicar = False
                    '        If UbicarProveedor(txtProveedor.Text) = False Then
                    '            MsgBox("El C�digo de proveedor no est� registrado o el vendedor no es el asignado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                    '            'If txtVendedor.Text = "" Then
                    '            '    txtVendedor.Focus()
                    '            'Else
                    '            '    txtProveedor.Focus()
                    '            'End If
                    '            Exit Sub
                    '        End If
                    '    End If
                    '    txtCantidad.Focus()
                Case "txtNombreProveedor" : txtCantidad.Focus()
                Case "txtProducto"
                    blnUbicar = False
                    If UbicarProducto(txtProducto.Text) = False Then
                        MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                        txtProducto.Focus()
                        Exit Sub
                    End If
                    'medPrecio.Focus()
                    If ConvierteADouble(txtCantidad.Text) > 0 Then
                        DPkFechaFactura.Focus()
                        MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
                        MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
                        MSFlexGrid2.Col = 0
                        MSFlexGrid2.Text = Format(CDbl(txtCantidad.Text), "####0.#0")
                        MSFlexGrid2.Col = 1
                        MSFlexGrid2.Text = UCase(strUnidad)
                        MSFlexGrid2.Col = 2
                        MSFlexGrid2.Text = UCase(txtProducto.Text)
                        MSFlexGrid2.Col = 3
                        MSFlexGrid2.Text = UCase(strDescrip)
                        MSFlexGrid2.Col = 4
                        MSFlexGrid2.Text = lngRegistro
                        'MSFlexGrid2.Text = IIf(intImpuesto = 0, "No", "Si")

                        'MSFlexGrid2.Col = 5
                        ''MSFlexGrid2.Text = medPrecio.Text
                        'MSFlexGrid2.Text = 0
                        'MSFlexGrid2.Col = 6
                        'MSFlexGrid2.Text = Format(CDbl(medPrecio.Text) * CDbl(txtCantidad.Text), "#,##0.#0")
                        'MSFlexGrid2.Col = 7
                        'MSFlexGrid2.Text = lngRegistro
                        ''MSFlexGrid2.Col = 8
                        ''MSFlexGrid2.Text = ddlTipoPrecio.SelectedItem
                        'MSFlexGrid2.Col = 9
                        'MSFlexGrid2.Text = medPrecio.Text
                        'MSFlexGrid2.Col = 10
                        'MSFlexGrid2.Text = medPrecio.Text
                        'TextBox9.Text = Format(Format((CDbl(medPrecio.Text) * CDbl(txtCantidad.Text)), "#,##0.#0") + CDbl(TextBox9.Text), "#,##0.#0")
                        'If intImpuesto = 0 Then
                        '    TextBox10.Text = Format(CDbl(TextBox10.Text), "#,##0.#0")
                        'ElseIf intImpuesto = 1 Then
                        '    TextBox10.Text = Format(Format((Format((CDbl(medPrecio.Text) * CDbl(txtCantidad.Text)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + CDbl(TextBox10.Text), "#,##0.#0")
                        'End If
                        'If ddlRetencion.SelectedIndex = 0 Then
                        '    TextBox11.Text = "0.00"
                        'ElseIf ddlRetencion.SelectedIndex = 1 Then
                        '    TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
                        'End If
                        'TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
                        Limpiar2()
                    Else
                        MsgBox("No puede Requisar un producto con cantidad 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                        txtCantidad.Focus()
                    End If
                Case "txtCantidad"
                    If MSFlexGrid2.Rows > 1 And Trim(txtCantidad.Text) = "0" Then
                        If intTipoCompra = 1 Or intTipoCompra = 4 Then
                            Guardar()
                            Exit Sub
                        End If
                    Else
                        txtProducto.Focus()
                    End If
                    'Case "ComboBox1" : Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%" : ComboBox2.Focus() : ComboBox2.SelectedIndex = 0 : UbicarPrecio()
                Case "medPrecio"
                    If ConvierteADouble(txtCantidad.Text) > 0 Then
                        If ConvierteADouble(medPrecio.Text) > 0 Then
                            DPkFechaFactura.Focus()
                            MSFlexGrid2.Rows = MSFlexGrid2.Rows + 1
                            MSFlexGrid2.Row = MSFlexGrid2.Rows - 1
                            MSFlexGrid2.Col = 0
                            MSFlexGrid2.Text = Format(CDbl(txtCantidad.Text), "####0.#0")
                            MSFlexGrid2.Col = 1
                            MSFlexGrid2.Text = UCase(strUnidad)
                            MSFlexGrid2.Col = 2
                            MSFlexGrid2.Text = UCase(txtProducto.Text)
                            MSFlexGrid2.Col = 3
                            MSFlexGrid2.Text = UCase(strDescrip)
                            MSFlexGrid2.Col = 4
                            MSFlexGrid2.Text = IIf(intImpuesto = 0, "No", "Si")
                            MSFlexGrid2.Col = 5
                            MSFlexGrid2.Text = medPrecio.Text
                            MSFlexGrid2.Col = 6
                            MSFlexGrid2.Text = Format(CDbl(medPrecio.Text) * CDbl(txtCantidad.Text), "#,##0.#0")
                            MSFlexGrid2.Col = 4
                            MSFlexGrid2.Text = lngRegistro
                            'MSFlexGrid2.Col = 8
                            'MSFlexGrid2.Text = ddlTipoPrecio.SelectedItem
                            MSFlexGrid2.Col = 9
                            MSFlexGrid2.Text = medPrecio.Text
                            MSFlexGrid2.Col = 10
                            MSFlexGrid2.Text = medPrecio.Text
                            'TextBox9.Text = Format(Format((CDbl(medPrecio.Text) * CDbl(txtCantidad.Text)), "#,##0.#0") + CDbl(TextBox9.Text), "#,##0.#0")
                            'If intImpuesto = 0 Then
                            '    TextBox10.Text = Format(CDbl(TextBox10.Text), "#,##0.#0")
                            'ElseIf intImpuesto = 1 Then
                            '    TextBox10.Text = Format(Format((Format((CDbl(medPrecio.Text) * CDbl(txtCantidad.Text)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + CDbl(TextBox10.Text), "#,##0.#0")
                            'End If
                            'If ddlRetencion.SelectedIndex = 0 Then
                            '    TextBox11.Text = "0.00"
                            'ElseIf ddlRetencion.SelectedIndex = 1 Then
                            '    TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
                            'End If
                            'TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
                            Limpiar2()
                        Else
                            MsgBox("No puede Requisar un producto con monto 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                            medPrecio.Focus()
                        End If

                    Else
                        MsgBox("No puede Requisar un producto con cantidad 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                        txtCantidad.Focus()
                    End If
            End Select
        End If

    End Sub

    Sub Guardar()

        For intIncr = 0 To 25
            For intIncr2 = 0 To 6
                arrCredito(intIncr, intIncr2) = ""
            Next intIncr2
        Next intIncr
        For intIncr = 0 To 13
            For intIncr2 = 0 To 6
                arrContado(intIncr, intIncr2) = ""
            Next intIncr2
        Next intIncr

        If intTipoCompra = 1 Or intTipoCompra = 4 Then
            If txtNumeroCompra.Text.Trim.Length <= 0 Then
                MsgBox("No ha digitado el numero de Requisa", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
        End If

        MSFlexGrid2.Col = 1
        For intIncr = 1 To MSFlexGrid2.Rows - 1
            MSFlexGrid2.Row = intIncr
            MSFlexGrid2.Col = 0
            If IsNumeric(MSFlexGrid2.Text) = False Then
                MsgBox("No es un valor numerico la cantidad a Requisar", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            ElseIf CDbl(MSFlexGrid2.Text) <= 0 Then
                MsgBox("La cantidad a Requisar debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
                Exit Sub
            End If
            MSFlexGrid2.Row = intIncr
            MSFlexGrid2.Col = 5
            'If IsNumeric(MSFlexGrid2.Text) = False Then
            '    MsgBox("No es un valor numerico el precio del producto", MsgBoxStyle.Critical, "Error de Dato")
            '    Exit Sub
            '    'ElseIf CDbl(MSFlexGrid2.Text) <= 0 Then
            '    '    MsgBox("El precio del producto debe ser mayor que 0", MsgBoxStyle.Critical, "Error de Dato")
            '    '    Exit Sub
            'End If
        Next intIncr

        If intTipoCompra = 4 Or intTipoCompra = 8 Then
            blnUbicar = False
            'If UbicarProveedor(txtProveedor.Text) = False Then
            '    MsgBox("El C�digo de proveedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
            '    txtProveedor.Focus()
            '    Exit Sub
            'End If
        End If

        Dim frmNew As New actrptViewer


        'If UbicarVendedor(txtVendedor.Text) = False Then
        '    MsgBox("El C�digo de Vendedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
        '    Exit Sub
        'End If
        'If CInt(txtNumeroVendedor.Text) = 0 Then
        '    MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del vendedor.", MsgBoxStyle.Critical, "Error de Ingreso")
        '    txtNumeroVendedor.Focus()
        '    Exit Sub
        'End If
        intResp = 0
        intImprimir = 0
        'If intTipoCompra = 4 Or intTipoCompra = 8 Then
        '    If CInt(txtDias.Text) <= 0 Then
        '        MsgBox("No puede generar una Compra sin d�as de cr�dito.", MsgBoxStyle.Critical, "Error en D�as")
        '        txtDias.Focus()
        '        Exit Sub
        '    End If
        '    'If txtNombreProveedor.Text = "" Then
        '    '    MsgBox("Tiene que digitar y presionar <Enter> despu�s del c�digo del proveedor.", MsgBoxStyle.Critical, "Error de Ingreso")
        '    '    txtProveedor.Focus()
        '    '    Exit Sub
        '    'End If
        'End If
        If intTipoCompra = 1 Or intTipoCompra = 4 Then        ' Del D�a
            If lngRegAgencia = 1 Then
                intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton2 + MsgBoxStyle.Question, "Impresi�n de Requisa")
            Else
                intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNoCancel + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Impresi�n de Requisa")
            End If
            If intResp = 2 Then     'Cancelar
                txtCantidad.Focus()
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            ElseIf intResp = 7 Then 'No
                intImprimir = 1
            End If
        ElseIf intTipoCompra = 2 Or intTipoCompra = 5 Then    ' Imprimir Requisa
            intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Requisa")
            If intResp <> 6 Then 'Cancelar o No
                txtCantidad.Focus()
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            End If
        ElseIf intTipoCompra = 3 Or intTipoCompra = 6 Then    ' Anular Compra
            intResp = MsgBox("�Desea anular la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Anulaci�n de Requisa")
            If intResp <> 6 Then 'No
                txtNumeroCompra.Focus()
                Exit Sub
            End If
        ElseIf intTipoCompra = 7 Or intTipoCompra = 8 Then    ' Ingreso Manual
            intResp = MsgBox("�Desea guardar la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Requisa")
            If intResp <> 6 Then    'Cancelar o No
                txtCantidad.Focus()
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            End If
        ElseIf intTipoCompra = 9 Or intTipoCompra = 10 Then   ' Consultar Compra
            intResp = MsgBox("�Desea imprimir la Requisa en este momento?", MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton1 + MsgBoxStyle.Question, "Ingreso de Requisa")
            If intResp <> 6 Then 'Cancelar o No
                txtNumeroCompra.Focus()
                Exit Sub
            ElseIf intResp = 6 Then 'Yes
                intImprimir = 0
            End If
        End If

        Me.Cursor = Cursors.WaitCursor
        If intTipoCompra = 1 Or intTipoCompra = 4 Then
            'txtNumeroCompra.Text = ExtraerNumeroCompra(lngRegAgencia, intTipoCompra)
            ActualizarNumeroCompra(lngRegAgencia, intTipoCompra)
        End If
        intIncr = 0
        intIncr2 = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        dblSubtotal = 0
        dblImpuesto = 0
        dblRetencion = 0
        dblTotal = 0
        strFecVencFact = ""
        ' strVendedor = txtVendedor.Text + " " + txtNombreVendedor.Text
        strVendedor = ""
        strNumeroFact = txtNumeroCompra.Text
        'If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
        If intTipoCompra = 1 Or intTipoCompra = 4 Then
            intDiaFact = Format(Now, "dd")
            strMesFact = Format(Now, "MMM")
            lngYearFact = Format(Now, "yyyy")
            lngFechaFactura = Format(Now, "yyyyMMdd")
        ElseIf intTipoCompra <> 1 And intTipoCompra <> 4 Then
            intDiaFact = Format(DPkFechaFactura.Value, "dd")
            strMesFact = Format(DPkFechaFactura.Value, "MMM")
            lngYearFact = Format(DPkFechaFactura.Value, "yyyy")
            lngFechaFactura = Format(DPkFechaFactura.Value, "yyyyMMdd")
        End If
        If intTipoCompra = 1 Or intTipoCompra = 2 Or intTipoCompra = 7 Or intTipoCompra = 9 Then
            'strClienteFact = txtNombreProveedor.Text
            For intIncr = 1 To MSFlexGrid2.Rows - 1
                MSFlexGrid2.Row = intIncr
                For intIncr2 = 0 To 6
                    MSFlexGrid2.Col = intIncr2
                    If intIncr2 = 5 Or intIncr2 = 6 Then
                        'arrContado(intIncr - 1, intIncr2) = "U$ " & MSFlexGrid2.Text   // TipoCambio
                        arrContado(intIncr - 1, intIncr2) = MSFlexGrid2.Text
                    Else
                        arrContado(intIncr - 1, intIncr2) = MSFlexGrid2.Text
                    End If
                Next intIncr2
            Next intIncr
            intRptImpFactura = 1
        ElseIf intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 8 Or intTipoCompra = 10 Then
            'strCodCliFact = txtProveedor.Text
            'strClienteFact = txtNombreProveedor.Text
            'strFecVencFact = txtVence.Text
            'strDiasFact = txtDias.Text
            For intIncr = 1 To MSFlexGrid2.Rows - 1
                MSFlexGrid2.Row = intIncr
                For intIncr2 = 0 To 6
                    MSFlexGrid2.Col = intIncr2
                    If intIncr2 = 5 Or intIncr2 = 6 Then
                        'arrCredito(intIncr - 1, intIncr2) = "U$ " & MSFlexGrid2.Text   // TipoCambio
                        arrCredito(intIncr - 1, intIncr2) = MSFlexGrid2.Text
                    Else
                        arrCredito(intIncr - 1, intIncr2) = MSFlexGrid2.Text
                    End If
                Next intIncr2
            Next intIncr
            intRptImpFactura = 4
        End If
        'If intTipoCompra = 1 Or txtNombreProveedor.Text = "" Then
        '    txtNombreProveedor.Text = "Contado"
        'End If
        'dblSubtotal = CDbl(TextBox9.Text)
        'dblImpuesto = CDbl(TextBox10.Text)
        'dblRetencion = CDbl(TextBox11.Text)
        'dblTotal = CDbl(TextBox12.Text)
        strValorFactCOR = ""
        strValorFactCOR = "C$ " & Format((dblTotal * dblTipoCambio), "#,##0.#0")
        If intTipoCompra <> 3 And intTipoCompra <> 6 And intTipoCompra <> 7 And intTipoCompra <> 8 Then
            If intImprimir = 0 Then
                If intTipoCompra = 9 Or intTipoCompra = 10 Then
                    frmNew.Show()
                    If intImpresoraAsignada = 1 Then
                        Exit Sub
                    End If
                    frmNew.WindowState = FormWindowState.Minimized
                    Me.Cursor = Cursors.Default
                    frmNew.Close()
                    Exit Sub
                End If
            End If
        End If
        Dim cmdTmp As New SqlCommand("GrabaMovimientoInventarioEnc", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim lngNumFecha As Long

        lngNumFecha = 0
        lngRegistro = 0
        If intTipoCompra = 1 Or intTipoCompra = 4 Then
            lngNumFecha = Format(Now, "yyyyMMdd")
        ElseIf intTipoCompra <> 1 And intTipoCompra <> 4 Then
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
        End If
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNumeroCompra.Text
        End With
        With prmTmp03
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumFecha
        End With
        With prmTmp04
            .ParameterName = "@strFechaFactura"
            .SqlDbType = SqlDbType.VarChar
            If intTipoCompra = 1 Or intTipoCompra = 4 Then
                .Value = Format(Now, "dd-MMM-yyyy")
            ElseIf intTipoCompra <> 1 And intTipoCompra <> 4 Then
                .Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
            End If
        End With
        With prmTmp05
            .ParameterName = "@dblSubTotal"
            .SqlDbType = SqlDbType.Money
            .Value = 0
        End With
        With prmTmp06
            .ParameterName = "@dblTotal"
            .SqlDbType = SqlDbType.Money
            .Value = 0
        End With
        With prmTmp07
            .ParameterName = "@lngUsuario"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegUsuario
        End With
        With prmTmp08
            .ParameterName = "@lngAgenciaOrigen"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lCodigoAgenciaxDefecto
        End With
        With prmTmp09
            .ParameterName = "@lngAgenciaDestino"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ConvierteAInt(cbeAgenciaDestino.SelectedValue)
        End With
        dblTipoCambio = 0
        dblTipoCambio = UbicarTipoCambio(lngNumFecha)
        With prmTmp10
            .ParameterName = "@dblCambio"
            .SqlDbType = SqlDbType.Money
            .Value = dblTipoCambio
        End With
        With prmTmp11
            .ParameterName = "@intStatus"
            .SqlDbType = SqlDbType.TinyInt
            If intTipoCompra = 2 Or intTipoCompra = 5 Then
                .Value = 2
            ElseIf intTipoCompra = 3 Or intTipoCompra = 6 Then
                .Value = 1
            Else
                .Value = 0
            End If
        End With
        With prmTmp12
            .ParameterName = "@TipoMovimiento"
            .SqlDbType = SqlDbType.Char
            .Value = "EA"
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            If intTipoCompra = 1 Or intTipoCompra = 2 Or intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                cmdTmp.ExecuteNonQuery()
            End If
            If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                GuardarDetalle1()
            End If
            If intTipoCompra <> 9 And intTipoCompra <> 10 Then
                'If intResp = 6 Then
                GuardarDetalle2()
                'If lngRegAgencia = 1 Then
                '    GuardarDetalle3()
                'End If
                'End If
            End If
            MsgBox("Registro Ingresado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            If intTipoCompra <> 3 And intTipoCompra <> 6 And intTipoCompra <> 7 And intTipoCompra <> 8 Then
                If intImprimir = 0 Then
                    'If intTipoCompra = 4 Then
                    '    UbicarProveedor(txtProveedor.Text)
                    'End If
                    frmNew.Show()
                    If intImpresoraAsignada = 0 Then
                        frmNew.WindowState = FormWindowState.Minimized
                        frmNew.Close()
                    End If
                End If
            End If
            Limpiar()
            'If intTipoCompra = 2 Or intTipoCompra = 5 Then
            '    UbicarPendientes()
            'End If
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Me.Cursor = Cursors.Default
        Finally
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub GuardarDetalle1()

        Dim cmdTmp As New SqlCommand("GrabaMovimientoInventarioDet", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter

        Dim lngAgencia As Long
        Dim lngNumFecha As Long
        Dim dblPrecioOrig As Double, dblPrecioMod As Double

        lngAgencia = 0
        lngNumFecha = 0
        lngAgencia = lngRegAgencia
        If intTipoCompra = 1 Or intTipoCompra = 4 Then
            lngNumFecha = Format(Now, "yyyyMMdd")
        Else
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
        End If
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegistro
        End With
        With prmTmp02
            .ParameterName = "@lngProducto"
            .SqlDbType = SqlDbType.BigInt
            .Value = 0
        End With
        With prmTmp03
            .ParameterName = "@dblCantidad"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp04
            .ParameterName = "@dblPrecio"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp05
            .ParameterName = "@dblValor"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp06
            .ParameterName = "@dblIgv"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp07
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngAgencia
        End With
        With prmTmp08
            .ParameterName = "@lngNumFechaIng"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumFecha
        End With
        With prmTmp09
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNumeroCompra.Text
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        MSFlexGrid2.Col = 1
        For intIncr = 1 To MSFlexGrid2.Rows - 1
            MSFlexGrid2.Row = intIncr
            MSFlexGrid2.Col = 4
            cmdTmp.Parameters(1).Value = ConvierteAInt(MSFlexGrid2.Text)
            MSFlexGrid2.Col = 0
            cmdTmp.Parameters(2).Value = Format(ConvierteADouble(MSFlexGrid2.Text), "#####0.#0")
            'MSFlexGrid2.Col = 5
            cmdTmp.Parameters(3).Value = 0
            'If intFechaCambioCompra > 0 Then
            '    cmdTmp.Parameters(3).Value = Format(CDbl(MSFlexGrid2.Text) * dblTipoCambio, "###0.#0")
            'Else
            '    cmdTmp.Parameters(3).Value = Format(CDbl(MSFlexGrid2.Text), "#####0.#0")
            'End If
            'MSFlexGrid2.Col = 6
            'If intFechaCambioCompra > 0 Then
            '    cmdTmp.Parameters(4).Value = Format(CDbl(MSFlexGrid2.Text) * dblTipoCambio, "###0.#0")
            'Else
            '    cmdTmp.Parameters(4).Value = Format(CDbl(MSFlexGrid2.Text), "#####0.#0")
            'End If
            cmdTmp.Parameters(4).Value = 0
            'MSFlexGrid2.Col = 4
            'If MSFlexGrid2.Text = "Si" Then
            '    If intFechaCambioCompra > 0 Then
            '        cmdTmp.Parameters(5).Value = Format(((cmdTmp.Parameters(4).Value * dblTipoCambio) * dblPorcParam), "###0.#0")
            '    Else
            '        cmdTmp.Parameters(5).Value = Format((cmdTmp.Parameters(4).Value * dblPorcParam), "#####0.#0")
            '    End If
            'Else
            '    cmdTmp.Parameters(5).Value = 0.0
            'End If
            cmdTmp.Parameters(5).Value = 0
            dblPrecioMod = 0
            dblPrecioOrig = 0
            'MSFlexGrid2.Col = 9
            'dblPrecioOrig = Format(CDbl(MSFlexGrid2.Text), "#####0.#0")
            dblPrecioOrig = 0
            'MSFlexGrid2.Col = 10
            'dblPrecioMod = Format(CDbl(MSFlexGrid2.Text), "#####0.#0")
            dblPrecioMod = 0
            'Try
            '    cmdTmp.ExecuteNonQuery()
            '    If dblPrecioOrig <> dblPrecioMod Then
            '        MSFlexGrid2.Col = 7
            '        MSFlexGrid2.Col = 8
            '        MSFlexGrid2.Col = 9
            '        MSFlexGrid2.Col = 10
            '    End If
            'Catch exc As Exception
            '    MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            '    Exit Sub
            'End Try
        Next intIncr
        MSFlexGrid2.Col = 1
        'For intIncr = 1 To MSFlexGrid2.Rows - 1
        '    MSFlexGrid2.Row = intIncr
        '    Try
        '        If dblPrecioOrig <> dblPrecioMod Then
        '            MSFlexGrid2.Col = 7
        '            MSFlexGrid2.Col = 8
        '            MSFlexGrid2.Col = 9
        '            MSFlexGrid2.Col = 10
        '        End If
        '    Catch exc As Exception
        '        MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        '        Exit Sub
        '    End Try
        'Next intIncr

    End Sub

    Sub GuardarDetalle2()

        Dim cmdTmp As New SqlCommand("sp_IngProductoMovim", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter
        Dim prmTmp18 As New SqlParameter
        Dim prmTmp19 As New SqlParameter
        Dim prmTmp20 As New SqlParameter
        Dim lngNumTiempo, lngNumFecha As Long
        Dim lngAgencia01, lngAgencia02 As Byte

        lngNumFecha = 0
        lngRegistro = 0
        lngAgencia01 = 0
        lngAgencia02 = 0
        lngNumTiempo = 0
        lngNumTiempo = Format(Now, "Hmmss")
        If lngRegAgencia = 1 Then
            lngNumFecha = Format(Now, "yyyyMMdd")
        Else
            lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
        End If
        lngAgencia01 = lngRegAgencia
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Decimal
            .Value = lngNumFecha
        End With
        With prmTmp03
            .ParameterName = "@lngNumTiempo"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumTiempo
        End With
        With prmTmp04
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngAgencia01
        End With
        With prmTmp05
            .ParameterName = "@lngProducto"
            .SqlDbType = SqlDbType.BigInt
            .Value = 0
        End With
        With prmTmp06
            .ParameterName = "@strFecha"
            .SqlDbType = SqlDbType.VarChar
            If lngRegAgencia = 1 Then
                .Value = Format(Now, "dd-MMM-yyyy")
            Else
                .Value = Format(DPkFechaFactura.Value, "dd-MMM-yyyy")
            End If
        End With
        With prmTmp07
            .ParameterName = "@strDocumento"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNumeroCompra.Text
        End With
        With prmTmp08
            .ParameterName = "@strTipoMov"
            .SqlDbType = SqlDbType.VarChar
            If intTipoCompra = 3 Or intTipoCompra = 6 Then
                .Value = "SA" 'Anulacion de compra (salida)
            Else
                .Value = "EA" 'Entrada compra (Entrada)
            End If
        End With
        With prmTmp09
            .ParameterName = "@strMoneda"
            .SqlDbType = SqlDbType.VarChar
            .Value = "COR"
        End With
        With prmTmp10
            .ParameterName = "@dblFactura"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
            'If intFechaCambioCompra > 0 Then
            '    .Value = ConvierteADouble(TextBox12.Text) * dblTipoCambio
            'Else
            '    .Value = ConvierteADouble(TextBox12.Text)
            'End If
        End With
        With prmTmp11
            .ParameterName = "@dblCant"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp12
            .ParameterName = "@dblCosto"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp13
            .ParameterName = "@dblSaldo"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp14
            .ParameterName = "@strDebe"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp15
            .ParameterName = "@strHaber"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp16
            .ParameterName = "@lngUsuario"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegUsuario
        End With
        With prmTmp17
            .ParameterName = "@dblCantConv"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp18
            .ParameterName = "@lngProductoConv"
            .SqlDbType = SqlDbType.SmallInt
            .Value = 0
        End With
        With prmTmp19
            .ParameterName = "@lngAgenciaConv"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp20
            .ParameterName = "@intOrigen"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 1
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .Parameters.Add(prmTmp18)
            .Parameters.Add(prmTmp19)
            .Parameters.Add(prmTmp20)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        MSFlexGrid2.Col = 1
        For intIncr = 1 To MSFlexGrid2.Rows - 1
            MSFlexGrid2.Row = intIncr
            MSFlexGrid2.Col = 4
            cmdTmp.Parameters(4).Value = MSFlexGrid2.Text
            MSFlexGrid2.Col = 0
            cmdTmp.Parameters(10).Value = Format(ConvierteADouble(MSFlexGrid2.Text), "#####0.#0")
            MSFlexGrid2.Col = 5
            cmdTmp.Parameters(11).Value = Format(ConvierteADouble(MSFlexGrid2.Text), "#####0.#0")
            MSFlexGrid2.Col = 6
            cmdTmp.Parameters(12).Value = Format(ConvierteADouble(MSFlexGrid2.Text), "#####0.#0")
            Try
                cmdTmp.ExecuteNonQuery()
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                Exit Sub
            End Try
        Next intIncr

    End Sub

    Sub GuardarDetalle3()

        Dim cmdTmp As New SqlCommand("sp_IngProductoMovim", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter
        Dim prmTmp18 As New SqlParameter
        Dim prmTmp19 As New SqlParameter
        Dim prmTmp20 As New SqlParameter
        Dim lngNumTiempo, lngNumFecha As Long
        Dim lngAgencia01, lngAgencia02 As Byte

        lngNumFecha = 0
        lngAgencia01 = 0
        lngAgencia02 = 0
        lngNumTiempo = 0
        lngNumTiempo = Format(Now, "Hmmss")
        lngNumFecha = Format(Now, "yyyyMMdd")
        lngAgencia01 = 3
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Decimal
            .Value = lngNumFecha
        End With
        With prmTmp03
            .ParameterName = "@lngNumTiempo"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumTiempo
        End With
        With prmTmp04
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngAgencia01
        End With
        With prmTmp05
            .ParameterName = "@lngProducto"
            .SqlDbType = SqlDbType.BigInt
            .Value = 0
        End With
        With prmTmp06
            .ParameterName = "@strFecha"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(Now, "dd-MMM-yyyy")
        End With
        With prmTmp07
            .ParameterName = "@strDocumento"
            .SqlDbType = SqlDbType.VarChar
            .Value = txtNumeroCompra.Text
        End With
        With prmTmp08
            .ParameterName = "@strTipoMov"
            .SqlDbType = SqlDbType.VarChar
            If intTipoCompra = 3 Or intTipoCompra = 6 Then
                .Value = "AF"
            Else
                .Value = "EC"
            End If
        End With
        With prmTmp09
            .ParameterName = "@strMoneda"
            .SqlDbType = SqlDbType.VarChar
            .Value = "COR"
        End With
        With prmTmp10
            .ParameterName = "@dblFactura"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp11
            .ParameterName = "@dblCant"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp12
            .ParameterName = "@dblCosto"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp13
            .ParameterName = "@dblSaldo"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
            'If intFechaCambioCompra > 0 Then
            '    .Value = CDbl(TextBox12.Text) * dblTipoCambio
            'Else
            '    .Value = CDbl(TextBox12.Text)
            'End If
        End With
        With prmTmp14
            .ParameterName = "@strDebe"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp15
            .ParameterName = "@strHaber"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp16
            .ParameterName = "@lngUsuario"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegUsuario
        End With
        With prmTmp17
            .ParameterName = "@dblCantConv"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp18
            .ParameterName = "@lngProductoConv"
            .SqlDbType = SqlDbType.SmallInt
            .Value = 0
        End With
        With prmTmp19
            .ParameterName = "@lngAgenciaConv"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp20
            .ParameterName = "@intOrigen"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 1
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .Parameters.Add(prmTmp18)
            .Parameters.Add(prmTmp19)
            .Parameters.Add(prmTmp20)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        MSFlexGrid2.Col = 1
        For intIncr = 1 To MSFlexGrid2.Rows - 1
            MSFlexGrid2.Row = intIncr
            MSFlexGrid2.Col = 4
            cmdTmp.Parameters(4).Value = MSFlexGrid2.Text
            MSFlexGrid2.Col = 0
            cmdTmp.Parameters(10).Value = Format(ConvierteADouble(MSFlexGrid2.Text), "#####0.#0")
            Try
                cmdTmp.ExecuteNonQuery()
            Catch exc As Exception
                MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
                Exit Sub
            End Try
        Next intIncr

    End Sub

    Sub Limpiar()

        DPkFechaFactura.Value = Now
        intIncr = 0
        For intIncr = 1 To 4
            txtCollection.Item(intIncr).Text = ""
        Next intIncr
        intIncr = 0
        'For intIncr = 7 To 12
        '    txtCollection.Item(intIncr).Text = "0.00"
        'Next intIncr
        'For intIncr = 13 To txtCollection.Count
        '    txtCollection.Item(intIncr).Text = "0"
        'Next intIncr
        For intIncr = 0 To 25
            For intIncr2 = 0 To 6
                arrCredito(intIncr, intIncr2) = ""
            Next intIncr2
        Next intIncr
        For intIncr = 0 To 13
            For intIncr2 = 0 To 6
                arrContado(intIncr, intIncr2) = ""
            Next intIncr2
        Next intIncr
        'txtDias.Text = "30"
        ''txtVendedor.Text = ""
        ''txtProveedor.Text = ""
        'txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + CInt(txtDias.Text)), "dd-MMM-yyyy")
        'ddlRetencion.SelectedIndex = 0
        'If intFechaCambioCompra > 0 Then
        '    ddlTipoPrecio.SelectedIndex = 2
        'Else
        '    ddlTipoPrecio.SelectedIndex = 0
        'End If
        DPkFechaFactura.Focus()
        'Select Case intTipoCompra
        '    Case 1, 2, 3, 7, 9 : Label3.Visible = False : Label4.Visible = False : txtDias.Visible = False : txtVence.Visible = False
        '    Case 4, 5, 6, 8, 10 : Label3.Visible = True : Label4.Visible = True : txtDias.Visible = True : txtVence.Visible = True
        'End Select
        Select Case intTipoCompra
            Case 1, 4 : txtNumeroCompra.Text = "" 'txtNumeroCompra.Text = ExtraerNumeroCompra(lngRegAgencia, intTipoCompra)
            Case 2, 3, 5, 6 : BloquearCampos()
            Case 7, 8 : txtNumeroCompra.ReadOnly = False
            Case 9, 10 : BloquearCampos() : txtNumeroCompra.ReadOnly = False
        End Select
        MSFlexGrid2.Clear()
        MSFlexGrid2.Rows = 1
        MSFlexGrid2.FormatString = "Cantidad   |<Unidad     |<Codigo     |<Producto                                                               |<|>|>|>"
        DPkFechaFactura.Focus()

    End Sub

    Sub Limpiar2()

        txtProducto.Text = ""
        txtCantidad.Text = "0"
        medPrecio.Text = "0.00"
        'If intFechaCambioCompra > 0 Then
        '    ddlTipoPrecio.SelectedIndex = 2
        'Else
        '    ddlTipoPrecio.SelectedIndex = 0
        'End If
        txtCantidad.Focus()

    End Sub

    Sub BloquearCampos()

        For intIncr = 1 To txtCollection.Count - 1
            txtCollection.Item(intIncr).readonly = True
        Next
        DPkFechaFactura.Enabled = False
        'ddlRetencion.Enabled = False
        'ddlTipoPrecio.Enabled = False
        'txtVendedor.ReadOnly = True



        'txtProveedor.ReadOnly = False
        If intTipoCompra = 3 Or intTipoCompra = 6 Then
            txtNumeroCompra.ReadOnly = False
            'txtProveedor.ReadOnly = True
        End If

    End Sub

    'Sub Anular()

    '    If Label16.Visible = True Then
    '        MsgBox("No puede anular una Compra anulada.  Verifique.", MsgBoxStyle.Critical, "Compra Anulada")
    '        Exit Sub
    '    End If
    '    If Year(DPkFechaFactura.Value) = Year(Now) And Month(DPkFechaFactura.Value) = Month(Now) Then
    '    Else
    '        MsgBox("La Compra a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Compra Inv�lida")
    '        Exit Sub
    '    End If
    '    Me.Cursor = Cursors.WaitCursor
    '    Dim cmdTmp As New SqlCommand("GrabaComprasEnc", cnnAgro2K)
    '    Dim prmTmp01 As New SqlParameter
    '    Dim prmTmp02 As New SqlParameter
    '    Dim prmTmp03 As New SqlParameter
    '    Dim prmTmp04 As New SqlParameter
    '    Dim prmTmp05 As New SqlParameter
    '    Dim prmTmp06 As New SqlParameter
    '    Dim prmTmp07 As New SqlParameter
    '    Dim prmTmp08 As New SqlParameter
    '    Dim prmTmp09 As New SqlParameter
    '    Dim prmTmp10 As New SqlParameter
    '    Dim prmTmp11 As New SqlParameter
    '    Dim prmTmp12 As New SqlParameter
    '    Dim prmTmp13 As New SqlParameter
    '    Dim prmTmp14 As New SqlParameter
    '    Dim prmTmp15 As New SqlParameter
    '    Dim prmTmp16 As New SqlParameter
    '    Dim prmTmp17 As New SqlParameter
    '    Dim prmTmp18 As New SqlParameter
    '    Dim prmTmp19 As New SqlParameter
    '    Dim prmTmp20 As New SqlParameter
    '    Dim lngNumFecha As Long

    '    lngNumFecha = 0
    '    lngNumFecha = Format(DPkFechaFactura.Value, "yyyyMMdd")
    '    With prmTmp01
    '        .ParameterName = "@lngRegistro"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp02
    '        .ParameterName = "@strNumero"
    '        .SqlDbType = SqlDbType.VarChar
    '        .Value = txtNumeroCompra.Text
    '    End With
    '    With prmTmp03
    '        .ParameterName = "@lngNumFecha"
    '        .SqlDbType = SqlDbType.Int
    '        .Value = lngNumFecha
    '    End With
    '    With prmTmp04
    '        .ParameterName = "@strFechaFactura"
    '        .SqlDbType = SqlDbType.VarChar
    '        .Value = ""
    '    End With
    '    With prmTmp05
    '        .ParameterName = "@lngVendedor"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp06
    '        .ParameterName = "@lngProveedor"
    '        .SqlDbType = SqlDbType.Int
    '        .Value = CInt(txtNumeroProveedor.Text.Trim)
    '        'If intTipoCompra <> 4 And intTipoCompra <> 8 Then
    '        '    .Value = lngClienteContado
    '        'ElseIf intTipoCompra = 4 Or intTipoCompra = 8 Then
    '        '    .Value = txtNumeroProveedor.Text.Trim
    '        'End If
    '    End With
    '    With prmTmp07
    '        .ParameterName = "@strCliente"
    '        .SqlDbType = SqlDbType.VarChar
    '        .Value = ""
    '    End With
    '    With prmTmp08
    '        .ParameterName = "@dblSubTotal"
    '        .SqlDbType = SqlDbType.Decimal
    '        .Value = 0
    '    End With
    '    With prmTmp09
    '        .ParameterName = "@dblImpuesto"
    '        .SqlDbType = SqlDbType.Decimal
    '        .Value = 0
    '    End With
    '    With prmTmp10
    '        .ParameterName = "@dblRetencion"
    '        .SqlDbType = SqlDbType.Decimal
    '        .Value = 0
    '    End With
    '    With prmTmp11
    '        .ParameterName = "@dblTotal"
    '        .SqlDbType = SqlDbType.Decimal
    '        If intFechaCambioCompra > 0 Then
    '            .Value = Format((TextBox12.Text * dblTipoCambio), "#####0.#0")
    '        Else
    '            .Value = Format(CDbl(TextBox12.Text), "#####0.#0")
    '        End If
    '    End With
    '    With prmTmp12
    '        .ParameterName = "@lngUsuario"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp13
    '        .ParameterName = "@lngAgencia"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = lngRegAgencia
    '    End With
    '    With prmTmp14
    '        .ParameterName = "@intImpreso"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp15
    '        .ParameterName = "@lngDepart"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp16
    '        .ParameterName = "@lngNegocio"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp17
    '        .ParameterName = "@intTipoCompra"
    '        .SqlDbType = SqlDbType.TinyInt
    '        If intTipoCompra = 3 Then
    '            .Value = 0
    '        ElseIf intTipoCompra = 6 Then
    '            .Value = 1
    '        End If
    '    End With
    '    With prmTmp18
    '        .ParameterName = "@intCredito"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 0
    '    End With
    '    With prmTmp19
    '        .ParameterName = "@dblCambio"
    '        .SqlDbType = SqlDbType.Decimal
    '        .Value = 0
    '    End With
    '    With prmTmp20
    '        .ParameterName = "@intStatus"
    '        .SqlDbType = SqlDbType.TinyInt
    '        .Value = 1
    '    End With
    '    With cmdTmp
    '        .Parameters.Add(prmTmp01)
    '        .Parameters.Add(prmTmp02)
    '        .Parameters.Add(prmTmp03)
    '        .Parameters.Add(prmTmp04)
    '        .Parameters.Add(prmTmp05)
    '        .Parameters.Add(prmTmp06)
    '        .Parameters.Add(prmTmp07)
    '        .Parameters.Add(prmTmp08)
    '        .Parameters.Add(prmTmp09)
    '        .Parameters.Add(prmTmp10)
    '        .Parameters.Add(prmTmp11)
    '        .Parameters.Add(prmTmp12)
    '        .Parameters.Add(prmTmp13)
    '        .Parameters.Add(prmTmp14)
    '        .Parameters.Add(prmTmp15)
    '        .Parameters.Add(prmTmp16)
    '        .Parameters.Add(prmTmp17)
    '        .Parameters.Add(prmTmp18)
    '        .Parameters.Add(prmTmp19)
    '        .Parameters.Add(prmTmp20)
    '        .CommandType = CommandType.StoredProcedure
    '    End With
    '    If cmdTmp.Connection.State = ConnectionState.Open Then
    '        cmdTmp.Connection.Close()
    '    End If
    '    If cnnAgro2K.State = ConnectionState.Open Then
    '        cnnAgro2K.Close()
    '    End If
    '    cnnAgro2K.Open()
    '    cmdTmp.Connection = cnnAgro2K
    '    Try
    '        cmdTmp.ExecuteNonQuery()
    '        GuardarDetalle2()
    '        If lngRegAgencia = 1 Then
    '            GuardarDetalle3()
    '        End If
    '        Limpiar()
    '    Catch exc As Exception
    '        MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
    '    Finally
    '        cmdAgro2K.Connection.Close()
    '        cnnAgro2K.Close()
    '    End Try
    '    Me.Cursor = Cursors.Default

    'End Sub

    Function UbicarProducto(ByVal strDato As String) As Boolean

        strDescrip = ""
        strUnidad = ""
        lngRegistro = 0
        intImpuesto = 0
        strQuery = ""
        strQuery = "Select P.Descripcion, U.Codigo, P.Registro, P.Impuesto from prm_Productos P, prm_Unidades U "
        strQuery = strQuery + "Where P.Codigo = '" & strDato & "' AND P.RegUnidad = U.Registro AND P.Estado = 0"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            strDescrip = dtrAgro2K.GetValue(0)
            strUnidad = dtrAgro2K.GetValue(1)
            lngRegistro = dtrAgro2K.GetValue(2)
            intImpuesto = dtrAgro2K.GetValue(3)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If lngRegistro = 0 Then
            UbicarProducto = False
            Exit Function
        End If
        UbicarProducto = True

    End Function

    'Function UbicarVendedor(ByVal strDato As String) As Boolean

    '    txtNumeroVendedor.Text = "0"
    '    strQuery = ""
    '    strQuery = "Select Nombre, Registro from prm_Vendedores Where Codigo = '" & txtVendedor.Text & "' "
    '    If intTipoCompra = 9 Or intTipoCompra = 10 Then
    '        strQuery = strQuery + "And registro <> 0"
    '    Else
    '        strQuery = strQuery + "And Estado = 0 And registro <> 0"
    '    End If
    '    If cnnAgro2K.State = ConnectionState.Open Then
    '        cnnAgro2K.Close()
    '    End If
    '    If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '        cmdAgro2K.Connection.Close()
    '    End If
    '    cnnAgro2K.Open()
    '    cmdAgro2K.Connection = cnnAgro2K
    '    cmdAgro2K.CommandText = strQuery
    '    dtrAgro2K = cmdAgro2K.ExecuteReader
    '    While dtrAgro2K.Read
    '        txtNombreVendedor.Text = dtrAgro2K.GetValue(0)
    '        txtNumeroVendedor.Text = dtrAgro2K.GetValue(1)
    '    End While
    '    dtrAgro2K.Close()
    '    cmdAgro2K.Connection.Close()
    '    cnnAgro2K.Close()
    '    If txtNumeroVendedor.Text = "0" Then
    '        UbicarVendedor = False
    '        Exit Function
    '    End If
    '    UbicarVendedor = True

    'End Function

    'Function UbicarProveedor(ByVal strDato As String) As Boolean

    '    strDirecFact = ""
    '    'txtNumeroProveedor.Text = "0"
    '    strQuery = ""
    '    'strQuery = "Select C.nombre, C.registro, C.deptregistro, C.negregistro, C.vendregistro, "
    '    'strQuery = strQuery + "C.direccion, C.dias_credito from prm_Clientes C Where C.Codigo = "
    '    'strQuery = strQuery + "'" & txtProveedor.Text & "' AND C.Estado = 0"
    '    'strQuery = " exec ObtieneProveedores '" & txtProveedor.Text.Trim() & "'"

    '    If cnnAgro2K.State = ConnectionState.Open Then
    '        cnnAgro2K.Close()
    '    End If
    '    If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '        cmdAgro2K.Connection.Close()
    '    End If
    '    cnnAgro2K.Open()
    '    cmdAgro2K.Connection = cnnAgro2K
    '    cmdAgro2K.CommandText = strQuery
    '    dtrAgro2K = cmdAgro2K.ExecuteReader
    '    While dtrAgro2K.Read
    '        'txtNombreProveedor.Text = dtrAgro2K.GetValue(0)
    '        'txtNumeroProveedor.Text = dtrAgro2K.GetValue(1)
    '        'txtProveedor.Text = dtrAgro2K.GetValue(2)
    '    End While
    '    dtrAgro2K.Close()
    '    cmdAgro2K.Connection.Close()
    '    cnnAgro2K.Close()
    '    If txtNumeroProveedor.Text = "0" Then
    '        UbicarProveedor = False
    '        Exit Function
    '    End If
    '    UbicarProveedor = True

    'End Function

    'Sub UbicarPrecio()

    '    If intFechaCambioCompra = 0 Then
    '        If ddlTipoPrecio.SelectedIndex = 2 Or ddlTipoPrecio.SelectedIndex = 3 Then
    '            MsgBox("El tipo de precio 3 y 4 no son v�lidos, unicamente el 1 y 2.", MsgBoxStyle.Critical, "Error de Tipo de Precio")
    '            ddlTipoPrecio.SelectedIndex = 0
    '            ddlTipoPrecio.Focus()
    '            Exit Sub
    '        End If
    '    ElseIf intFechaCambioCompra > 0 Then
    '        If ddlTipoPrecio.SelectedIndex = 0 Or ddlTipoPrecio.SelectedIndex = 1 Then
    '            MsgBox("El tipo de precio 1 y 2 no son v�lidos, unicamente el 3 y 4.", MsgBoxStyle.Critical, "Error de Tipo de Precio")
    '            ddlTipoPrecio.SelectedIndex = 2
    '            ddlTipoPrecio.Focus()
    '            Exit Sub
    '        End If
    '    End If
    '    strQuery = ""
    '    Select Case ddlTipoPrecio.SelectedIndex
    '        Case 0 : strQuery = "Select P.Pvpc from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '        Case 1 : strQuery = "Select P.Pvdc from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '        Case 2 : strQuery = "Select P.Pvpu from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '        Case 3 : strQuery = "Select P.Pvdu from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '    End Select
    '    'Select Case ddlTipoPrecio.SelectedIndex
    '    '    Case 0 : strQuery = "Select P.Pvpc from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '    '    Case 1 : strQuery = "Select P.Pvdc from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '    '    Case 2 : strQuery = "Select P.Pvpu from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '    '    Case 3 : strQuery = "Select P.Pvdu from prm_Productos P Where P.Registro = " & lngRegistro & ""
    '    'End Select
    '    If cnnAgro2K.State = ConnectionState.Open Then
    '        cnnAgro2K.Close()
    '    End If
    '    If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '        cmdAgro2K.Connection.Close()
    '    End If
    '    cnnAgro2K.Open()
    '    cmdAgro2K.Connection = cnnAgro2K
    '    cmdAgro2K.CommandText = strQuery
    '    dtrAgro2K = cmdAgro2K.ExecuteReader
    '    While dtrAgro2K.Read
    '        medPrecio.Text = dtrAgro2K.GetValue(0)
    '    End While
    '    dtrAgro2K.Close()
    '    cmdAgro2K.Connection.Close()
    '    cnnAgro2K.Close()

    'End Sub

    Private Sub MSFlexGrid2_DblClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles MSFlexGrid2.DblClick

        Dim strCodigo As String
        Dim strPrecio As String
        Dim strCantidad As String
        Dim dblCantidad As Double
        Dim dblPrecio As Double
        Dim dblValor As Double
        Dim dblImpuesto As Double

        If MSFlexGrid2.Rows >= 2 And (intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8) Then
            intRow = 0
            intCol = 0
            dblValor = 0
            dblPrecio = 0
            dblImpuesto = 0
            dblCantidad = 0
            strCantidad = ""
            strPrecio = ""
            strCodigo = ""
            If MSFlexGrid2.Col = 0 Or MSFlexGrid2.Col = 2 Or MSFlexGrid2.Col = 5 Then
                intRow = MSFlexGrid2.Row
                If MSFlexGrid2.Col = 0 Then
                    strCantidad = InputBox("Digite la nueva cantidad a Requisar", "Modificaci�n de Cantidad")
                    If IsNumeric(Trim(strCantidad)) = False Then
                        Exit Sub
                    End If
                    If CDbl(Trim(strCantidad)) <= 0 Then
                        MsgBox("La cantidad a modificar debe ser mayor que 0.00", MsgBoxStyle.Critical, "Error de Dato")
                        Exit Sub
                    End If
                ElseIf MSFlexGrid2.Col = 2 Then
                    strCodigo = InputBox("Digite el c�digo de producto a Requisar", "Modificaci�n de C�digo de Producto")
                    If Trim(strCodigo) = "" Then
                        Exit Sub
                    End If
                    If UbicarProducto(strCodigo) = False Then
                        MsgBox("C�digo de Producto Inv�lido", MsgBoxStyle.Critical, "Error de Dato")
                        Exit Sub
                    End If
                    MSFlexGrid2.Col = 0
                    dblCantidad = CDbl(MSFlexGrid2.Text)
                    MSFlexGrid2.Col = 1
                    MSFlexGrid2.Text = UCase(strUnidad)
                    MSFlexGrid2.Col = 2
                    MSFlexGrid2.Text = UCase(strCodigo)
                    MSFlexGrid2.Col = 3
                    MSFlexGrid2.Text = UCase(strDescrip)
                    'If intFechaCambioCompra > 0 Then
                    '    ddlTipoPrecio.SelectedIndex = 2
                    'Else
                    '    ddlTipoPrecio.SelectedIndex = 0
                    'End If
                    'UbicarPrecio()
                    MSFlexGrid2.Col = 5
                    MSFlexGrid2.Text = medPrecio.Text
                    strPrecio = MSFlexGrid2.Text
                    MSFlexGrid2.Col = 7
                    MSFlexGrid2.Text = lngRegistro
                    MSFlexGrid2.Col = 8
                    MSFlexGrid2.Text = 1
                    MSFlexGrid2.Col = 9
                    MSFlexGrid2.Text = strPrecio
                    MSFlexGrid2.Col = 10
                    MSFlexGrid2.Text = strPrecio
                    medPrecio.Text = ""
                    'ddlTipoPrecio.SelectedIndex = -1
                    dblCantidad = 0
                    MSFlexGrid2.Col = 2
                ElseIf MSFlexGrid2.Col = 5 Then
                    strPrecio = InputBox("Digite el Nuevo Precio a Requisar", "Modificaci�n de Precio")
                    If IsNumeric(Trim(strPrecio)) = False Then
                        Exit Sub
                    End If
                End If
                If MSFlexGrid2.Col = 0 Then
                    MSFlexGrid2.Row = intRow
                    MSFlexGrid2.Col = 0                     'Cantidad
                    dblCantidad = CDbl(strCantidad)
                    MSFlexGrid2.Text = Format(dblCantidad, "#,##0.#0")
                    MSFlexGrid2.Col = 5                     'Precio
                    dblPrecio = CDbl(MSFlexGrid2.Text)
                    MSFlexGrid2.Col = 4                     'Impuesto
                    'If MSFlexGrid2.Text = "Si" Then
                    '    MSFlexGrid2.Col = 6
                    '    dblImpuesto = Format(CDbl(MSFlexGrid2.Text) * dblPorcParam, "#,##0.#0")
                    '    TextBox10.Text = Format(CDbl(TextBox10.Text) - dblImpuesto, "#,##0.#0")
                    'End If
                    'MSFlexGrid2.Col = 6                     'Subtotal
                    'TextBox9.Text = Format(CDbl(TextBox9.Text) - CDbl(MSFlexGrid2.Text), "#,##0.#0")
                    'MSFlexGrid2.Text = Format(CDbl(dblPrecio) * CDbl(dblCantidad), "#,##0.#0")
                    'TextBox9.Text = Format(CDbl(TextBox9.Text) + CDbl(MSFlexGrid2.Text), "#,##0.#0")
                    'dblValor = CDbl(MSFlexGrid2.Text)
                    'MSFlexGrid2.Col = 4                     'Impuesto
                    'If MSFlexGrid2.Text = "Si" Then
                    '    TextBox10.Text = Format(CDbl(TextBox10.Text) + Format((dblValor * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    'End If
                    'If ddlRetencion.SelectedIndex = 0 Then     'Retencion
                    '    TextBox11.Text = "0.00"
                    'ElseIf ddlRetencion.SelectedIndex = 1 Then
                    '    TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
                    'End If                              'Total
                    'TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
                ElseIf MSFlexGrid2.Col = 2 Or MSFlexGrid2.Col = 5 Then
                    MSFlexGrid2.Row = intRow
                    MSFlexGrid2.Col = 0
                    dblCantidad = CDbl(MSFlexGrid2.Text)
                    MSFlexGrid2.Col = 6                     'Subtotal
                    'TextBox9.Text = Format(CDbl(TextBox9.Text) - CDbl(MSFlexGrid2.Text), "#,##0.#0")
                    'MSFlexGrid2.Col = 4                     'Impuesto
                    'If MSFlexGrid2.Text = "Si" Then
                    '    MSFlexGrid2.Col = 6
                    '    TextBox10.Text = Format(CDbl(TextBox10.Text) - Format((CDbl(MSFlexGrid2.Text) * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    'End If
                    'If ddlRetencion.SelectedIndex = 0 Then     'Retencion
                    '    TextBox11.Text = "0.00"
                    'ElseIf ddlRetencion.SelectedIndex = 1 Then
                    '    TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
                    'End If                                  'Total
                    'TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
                    dblPrecio = CDbl(strPrecio)
                    MSFlexGrid2.Col = 5                     'Precio
                    MSFlexGrid2.Text = Format(dblPrecio, "#,##0.#0")
                    MSFlexGrid2.Col = 6
                    MSFlexGrid2.Text = Format((dblCantidad * dblPrecio), "#,##0.#0")
                    ' TextBox9.Text = Format(CDbl(TextBox9.Text) + CDbl(MSFlexGrid2.Text), "#,##0.#0")
                    'MSFlexGrid2.Col = 4                     'Impuesto
                    'If MSFlexGrid2.Text = "Si" Then
                    '    MSFlexGrid2.Col = 6
                    '    TextBox10.Text = Format(CDbl(TextBox10.Text) + Format((CDbl(MSFlexGrid2.Text) * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    'End If
                    'If ddlRetencion.SelectedIndex = 0 Then     'Retencion
                    '    TextBox11.Text = "0.00"
                    'ElseIf ddlRetencion.SelectedIndex = 1 Then
                    '    TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
                    'End If                                  'Total
                    'TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
                    MSFlexGrid2.Col = 10
                    MSFlexGrid2.Text = Format(dblPrecio, "#,##0.#0")
                End If
            Else
                intRow = MSFlexGrid2.Row
                intResp = MsgBox("�Est� Seguro de Eliminar el Registro?", MsgBoxStyle.YesNo + MsgBoxStyle.Information, "Eliminaci�n de Registros")
                If intResp = 6 Then
                    MSFlexGrid2.Row = intRow
                    MSFlexGrid2.Col = 0
                    dblCantidad = CDbl(MSFlexGrid2.Text)
                    MSFlexGrid2.Col = 6                     'Subtotal
                    'TextBox9.Text = Format(CDbl(TextBox9.Text) - CDbl(MSFlexGrid2.Text), "#,##0.#0")
                    'MSFlexGrid2.Col = 4                     'Impuesto
                    'If MSFlexGrid2.Text = "Si" Then
                    '    MSFlexGrid2.Col = 6
                    '    TextBox10.Text = Format(CDbl(TextBox10.Text) - Format((CDbl(MSFlexGrid2.Text) * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    'End If
                    'If ddlRetencion.SelectedIndex = 0 Then     'Retencion
                    '    TextBox11.Text = "0.00"
                    'ElseIf ddlRetencion.SelectedIndex = 1 Then
                    '    TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
                    'End If                                  'Total
                    'TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
                    If MSFlexGrid2.Rows <= 2 Then
                        MSFlexGrid2.Clear()
                        MSFlexGrid2.Rows = 1
                        MSFlexGrid2.FormatString = "Cantidad   |<Unidad     |<Codigo     |<Producto                                                               |<Impuesto  |>Precio        |>Valor             |<|>|>|>"
                    Else
                        MSFlexGrid2.RemoveItem(MSFlexGrid2.Row)
                    End If
                End If
            End If
        End If
        txtCantidad.Focus()

    End Sub
    Private Sub FuncionTimer()
        If blnUbicar Then

            blnUbicar = False
            'Timer1.Enabled = False
            If strUbicar <> "" Then
                Select Case intListadoAyuda
                    'Case 1 'txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                    '    If txtProveedor.Visible = True Then
                    '        txtProveedor.Focus()
                    '    Else
                    '        txtNombreProveedor.Focus()
                    '    End If
                    'Case 7 : txtProveedor.Text = strUbicar : UbicarProveedor(txtProveedor.Text)
                    '    'If txtVendedor.Text = "" Then
                    '    '    MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                    '    '    txtVendedor.Focus()
                    '    'End If
                    Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : medPrecio.Focus()
                    Case 4 : txtNumeroCompra.Text = strUbicar : ExtraerCompra()
                End Select
            End If
        End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If lngRegAgencia = 0 Then
            Timer1.Enabled = False
            Me.Close()
        ElseIf blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                Select Case intListadoAyuda
                    'Case 1 'txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                    '    If txtProveedor.Visible = True Then
                    '        txtProveedor.Focus()
                    '    Else
                    '        txtNombreProveedor.Focus()
                    '    End If
                    'Case 7 : txtProveedor.Text = strUbicar : UbicarProveedor(txtProveedor.Text)
                    '    'If txtVendedor.Text = "" Then
                    '    '    MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                    '    '    txtVendedor.Focus()
                    '    'End If
                    Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : medPrecio.Focus()
                    Case 4 : txtNumeroCompra.Text = strUbicar : ExtraerCompra()
                End Select
            End If
        End If

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'If sender.name = "ddlRentencion" Then
        '    Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%"
        '    If sender.selectedindex = 0 Then
        '        TextBox11.Text = "0.00"
        '        TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
        '    ElseIf sender.selectedindex = 1 Then
        '        If intTipoCompra = 4 Or intTipoCompra = 8 Then
        '            sender.selectedindex = 0
        '        Else
        '            TextBox11.Text = Format(CDbl(TextBox9.Text) * dblRetParam, "#,##0.#0")
        '            TextBox12.Text = Format(CDbl(TextBox9.Text) + CDbl(TextBox10.Text) - CDbl(TextBox11.Text), "#,##0.#0")
        '        End If
        '    End If
        'ElseIf sender.name = "ComboBox2" And txtProducto.Text <> "" Then
        '    UbicarPrecio()
        'End If

    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

        'txtNombreProveedor.Text = StrConv(txtNombreProveedor.Text, VbStrConv.ProperCase)

    End Sub


    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DPkFechaFactura.LostFocus

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(DPkFechaFactura.Value, "yyyyMMdd") & " "
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            dblTipoCambio = dtrAgro2K.GetValue(0)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()

    End Sub

    Private Sub TextBox18_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProducto.DoubleClick

        intListadoAyuda = 0
        If MenuItem5.Visible = True Then
            intListadoAyuda = 1
        ElseIf MenuItem6.Visible = True Then
            intListadoAyuda = 2
        ElseIf MenuItem7.Visible = True Then
            intListadoAyuda = 3
        End If
        If intListadoAyuda <> 0 Then
            Dim frmNew As New frmListadoAyuda
            'Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click

        'If intTipoCompra = 2 Or intTipoCompra = 5 Then
        '    UbicarPendientes()
        'End If

    End Sub
    Private Function ObtieneAgenciaUsuario() As DataTable

        Dim table As New DataTable("Agencias")
        table.Columns.Add("Codigo")
        table.Columns.Add("Descripcion")

        Try
            strQuery = ""
            strQuery = "select a.registro,a.codigo,a.Descripcion from prm_UsuariosAgencias u,prm_agencias a  Where(u.agenregistro = a.registro)  and u.usrregistro = " & lngRegUsuario & " and a.codigo <> " & lCodigoAgenciaxDefecto
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                table.Rows.Add(New Object() {dtrAgro2K.GetValue(0), dtrAgro2K.GetValue(2)})
            End While
            'utcAgencia.DisplayMember = "Descripcion"
            'utcAgencia.ValueMember = "codigo"
            'utcAgencia.DataSource = table

        Catch ex As Exception
            MsgBox("Error Al obtener agencias" & ex.Message.ToString(), MsgBoxStyle.Critical)
        End Try
        Return table

    End Function

    Private Sub ObtieneAgenciaxDefecto()
        Try
            strQuery = ""
            strQuery = "select top 1 a.registro,a.codigo,a.Descripcion from prm_agencias a where defecto=0"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                lCodigoAgenciaxDefecto = ConvierteAInt(dtrAgro2K.GetValue(0))
                txtAgenciaOrigen.Text = dtrAgro2K.GetValue(2).ToString
            End While
        Catch ex As Exception
            MsgBox("Error Al obtener agencias" & ex.Message.ToString(), MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub CmdAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAnular.Click

    End Sub
End Class
