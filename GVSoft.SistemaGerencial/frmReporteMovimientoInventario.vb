Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmReporteMovimientoInventario
    Dim IdSuc As Integer
    Dim sNumero As String
    Dim IdA, FechaInicial, FechaFinal As Integer

    Private Sub frmReporteMovimientoInventario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UbicarAgencia(lngRegUsuario)
        LlenarComboSucursales()
        IdSuc = lngRegAgencia
        ObtieneSeriexAgencia()
        cmbAgencias.SelectedValue = 1
    End Sub

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        ddlSerie.SelectedItem = -1
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        'strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",1"
        If IdSuc = 0 Then
            IdSuc = lngRegAgencia
        End If
        strQuery = "ObtieneSerieParametros " & IdSuc & ",1"

        If strQuery.Trim.Length > 0 Then
            ddlSerie.Items.Clear()
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                IndicaObtieneRegistro = 1
            End While
        End If
        If IndicaObtieneRegistro = 1 Then
            ddlSerie.SelectedIndex = 0
        End If

        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
    End Sub

    Private Sub LlenarComboSucursales()
        RNAgencias.CargarComboAgencias(cmbAgencias, lngRegUsuario)
        'cmbAgencias.ValueMember = lngRegAgencia
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub cmbAgencias_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbAgencias.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdSuc = cmbAgencias.SelectedValue
            ObtieneSeriexAgencia()
            txtNumeroBuscar.Focus()
        End If
    End Sub

    Private Sub cmbAgencias_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgencias.Leave
        IdSuc = cmbAgencias.SelectedValue
        ObtieneSeriexAgencia()
    End Sub


    Private Sub cmdMostrarDatos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMostrarDatos.Click
        FechaInicial = Format(dtpFechaInicial.Value, "yyyyMMdd")
        FechaFinal = Format(dtpFechaFinal.Value, "yyyyMMdd")
        IdA = ConvierteAInt(cmbAgencias.SelectedValue)
        dgvTraslados.DataSource = RNInventario.ObtieneEncabezadoTrasladosXFecha(FechaInicial, FechaFinal, IdA)

    End Sub

    Private Sub cmdMostrarSeleccion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMostrarSeleccion.Click
        Dim Num As String
        Dim frmNew As Form
        If dgvTraslados.Rows.Count = 0 Then
            MsgBox("No Hay Registros que Mostrar", MsgBoxStyle.Information, "Reporte de Traslados")
            Exit Sub
        Else
            Num = dgvTraslados.CurrentRow.Cells("Numero").Value.ToString()
            intRptInventario = 21
            strQuery = "spObtieneTrasladoInventarioXNumero '" & Num & "'"

            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()

        End If
        
    End Sub

    Private Sub cmdMostrarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMostrarTodos.Click
        Dim frmNew As Form

        If dgvTraslados.Rows.Count = 0 Then
            MsgBox("No Hay Registros que Mostrar", MsgBoxStyle.Information, "Reporte de Traslados")
            Exit Sub
        Else
            FechaInicial = Format(dtpFechaInicial.Value, "yyyyMMdd")
            FechaFinal = Format(dtpFechaFinal.Value, "yyyyMMdd")
            IdA = ConvierteAInt(cmbAgencias.SelectedValue)
            intRptInventario = 22
            strQuery = "spObtieneTrasladoInventarioXFecha " & FechaInicial & ", " & FechaFinal & ", " & IdA

            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()

        End If

    End Sub
End Class