Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmExportarParametros
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents utcAgencia As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.utcAgencia = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(72, 54)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(72, 32)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Exportar"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(200, 54)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 32)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Salir"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Agencia"
        '
        'utcAgencia
        '
        Me.utcAgencia.DisplayMember = "Text"
        Me.utcAgencia.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.utcAgencia.FormattingEnabled = True
        Me.utcAgencia.ItemHeight = 14
        Me.utcAgencia.Location = New System.Drawing.Point(53, 23)
        Me.utcAgencia.Name = "utcAgencia"
        Me.utcAgencia.Size = New System.Drawing.Size(284, 20)
        Me.utcAgencia.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.utcAgencia.TabIndex = 53
        '
        'frmExportarParametros
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(336, 93)
        Me.Controls.Add(Me.utcAgencia)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarParametros"
        Me.Text = "frmExportarParametros"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmExportarParametros"


    Private Sub frmExportarParametros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0

        Try
            UbicarAgencia(lngRegUsuario)
            ObtieneAgencias()
            ' utcAgencia.SelectedValue = lngRegAgencia

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub ObtieneAgencias()
        ' Dim IndicaObtieneRegistro As Int16

        Try
            RNAgencias.CargarComboAgenciasConTodos(utcAgencia, lngRegUsuario)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmExportarParametros_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click, Button2.Click
        Try
            If sender.name = "Button1" Then
                ProcesarArchivo()
            ElseIf sender.name = "Button2" Then
                Me.Close()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub ProcesarArchivo()
        Dim lsRutaYNombreCompletoZip As String = String.Empty
        Try
            Dim strArchivo As String = String.Empty
            Dim strArchivo2 As String = String.Empty
            Dim strClave As String = String.Empty
            Dim cmdTmp As New SqlCommand("sp_ExportarArchivos", cnnAgro2K)
            Dim prmTmp01 As New SqlParameter
            Dim prmTmp02 As New SqlParameter
            Dim prmTmp03 As New SqlParameter
            Dim prmTmp04 As New SqlParameter

            Me.Cursor = Cursors.WaitCursor
            With prmTmp01
                .ParameterName = "@intExportar"
                .SqlDbType = SqlDbType.TinyInt
                .Value = intExportarModulo
            End With
            With prmTmp02
                .ParameterName = "@intAgencia"
                .SqlDbType = SqlDbType.TinyInt
                .Value = SUConversiones.ConvierteAInt(utcAgencia.SelectedValue)
            End With
            With prmTmp03
                .ParameterName = "@lngNumFechaIni"
                .SqlDbType = SqlDbType.Int
                .Value = 0
            End With
            With prmTmp04
                .ParameterName = "@lngNumFechaFin"
                .SqlDbType = SqlDbType.Int
                .Value = 0
            End With
            With cmdTmp
                .Parameters.Add(prmTmp01)
                .Parameters.Add(prmTmp02)
                .Parameters.Add(prmTmp03)
                .Parameters.Add(prmTmp04)
                .CommandType = CommandType.StoredProcedure
            End With
            If cmdTmp.Connection.State = ConnectionState.Open Then
                cmdTmp.Connection.Close()
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdTmp.Connection = cnnAgro2K

            strClave = ""
            strClave = "Agro2K_2008"
            strArchivo = ""
            Select Case intExportarModulo
                Case 2 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "clientes_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "clientes_" & Format(intAgenciaDefecto, "00")
                Case 3 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "clases_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "clases_" & Format(intAgenciaDefecto, "00")
                Case 4 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "subclases_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "subclases_" & Format(intAgenciaDefecto, "00")
                Case 5 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "unidades_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "unidades_" & Format(intAgenciaDefecto, "00")
                Case 6 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "empaques_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "empaques_" & Format(intAgenciaDefecto, "00")
                Case 7 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "proveedores_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "proveedores_" & Format(intAgenciaDefecto, "00")
                Case 8 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "productos_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "productos_" & Format(intAgenciaDefecto, "00")
                Case 9 : strArchivo = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "vendedores_" & Format(intAgenciaDefecto, "00") & ".txt" : strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosParametros").ToString() & "vendedores_" & Format(intAgenciaDefecto, "00")
            End Select
            If File.Exists(strArchivo) = True Then
                File.Delete(strArchivo)
            End If
            Dim sr As New System.IO.StreamWriter(strArchivo)
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                sr.WriteLine(dtrAgro2K.GetValue(0))
            End While
            sr.Close()
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Try
                intIncr = 0
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep -p""" & strClave & """ """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                lsRutaYNombreCompletoZip = String.Empty
                lsRutaYNombreCompletoZip = strArchivo2 & ".zip"
                If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaYNombreCompletoZip) Then
                    MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                    Exit Sub
                End If
                'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)
                'If File.Exists(strArchivo) = True Then
                '    File.Delete(strArchivo)
                'End If
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                Me.Cursor = Cursors.Default
                Exit Sub
            End Try
            MsgBox("Finaliz� la exportaci�n de los registros", MsgBoxStyle.Information, "Exportar Datos")
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Sub

End Class
