<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actrptImprimeFactCredito
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actrptImprimeFactCredito))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader
        Me.lblNumeroFactura = New DataDynamics.ActiveReports.Label
        Me.lblAnio = New DataDynamics.ActiveReports.Label
        Me.lblMes = New DataDynamics.ActiveReports.Label
        Me.LblDia = New DataDynamics.ActiveReports.Label
        Me.lblNombreCliente = New DataDynamics.ActiveReports.Label
        Me.lblVendedor = New DataDynamics.ActiveReports.Label
        Me.lblDireccionFactura = New DataDynamics.ActiveReports.Label
        Me.lblDias = New DataDynamics.ActiveReports.Label
        Me.lblFechaVenceFactura = New DataDynamics.ActiveReports.Label
        Me.lblCodigoCliente = New DataDynamics.ActiveReports.Label
        Me.Detail1 = New DataDynamics.ActiveReports.Detail
        Me.lblCantidad = New DataDynamics.ActiveReports.Label
        Me.lblCodigoUnidad = New DataDynamics.ActiveReports.Label
        Me.lblCodigoProducto = New DataDynamics.ActiveReports.Label
        Me.lblDescripcion = New DataDynamics.ActiveReports.Label
        Me.lblPrecio = New DataDynamics.ActiveReports.Label
        Me.lblValor = New DataDynamics.ActiveReports.Label
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter
        Me.lblSubTotal = New DataDynamics.ActiveReports.Label
        Me.lblImpuesto = New DataDynamics.ActiveReports.Label
        Me.lblTotal = New DataDynamics.ActiveReports.Label
        Me.lblTc = New DataDynamics.ActiveReports.Label
        Me.lblSimboloMoneda = New DataDynamics.ActiveReports.Label
        Me.Label1 = New DataDynamics.ActiveReports.Label
        Me.Label2 = New DataDynamics.ActiveReports.Label
        CType(Me.lblNumeroFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblAnio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblDia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNombreCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDireccionFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFechaVenceFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCodigoCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCodigoUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCodigoProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDescripcion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblValor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSubTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblImpuesto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSimboloMoneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblNumeroFactura, Me.lblAnio, Me.lblMes, Me.LblDia, Me.lblNombreCliente, Me.lblVendedor, Me.lblDireccionFactura, Me.lblDias, Me.lblFechaVenceFactura, Me.lblCodigoCliente})
        Me.PageHeader1.Height = 2.322917!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblNumeroFactura
        '
        Me.lblNumeroFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.lblNumeroFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNumeroFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.lblNumeroFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNumeroFactura.Border.RightColor = System.Drawing.Color.Black
        Me.lblNumeroFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNumeroFactura.Border.TopColor = System.Drawing.Color.Black
        Me.lblNumeroFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNumeroFactura.DataField = "Numero"
        Me.lblNumeroFactura.Height = 0.1979167!
        Me.lblNumeroFactura.HyperLink = Nothing
        Me.lblNumeroFactura.Left = 6.31!
        Me.lblNumeroFactura.Name = "lblNumeroFactura"
        Me.lblNumeroFactura.Style = ""
        Me.lblNumeroFactura.Text = ""
        Me.lblNumeroFactura.Top = 0.06!
        Me.lblNumeroFactura.Width = 1.0!
        '
        'lblAnio
        '
        Me.lblAnio.Border.BottomColor = System.Drawing.Color.Black
        Me.lblAnio.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAnio.Border.LeftColor = System.Drawing.Color.Black
        Me.lblAnio.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAnio.Border.RightColor = System.Drawing.Color.Black
        Me.lblAnio.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAnio.Border.TopColor = System.Drawing.Color.Black
        Me.lblAnio.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblAnio.Height = 0.1875!
        Me.lblAnio.HyperLink = Nothing
        Me.lblAnio.Left = 5.375!
        Me.lblAnio.Name = "lblAnio"
        Me.lblAnio.Style = ""
        Me.lblAnio.Text = ""
        Me.lblAnio.Top = 0.625!
        Me.lblAnio.Width = 0.5!
        '
        'lblMes
        '
        Me.lblMes.Border.BottomColor = System.Drawing.Color.Black
        Me.lblMes.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMes.Border.LeftColor = System.Drawing.Color.Black
        Me.lblMes.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMes.Border.RightColor = System.Drawing.Color.Black
        Me.lblMes.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMes.Border.TopColor = System.Drawing.Color.Black
        Me.lblMes.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblMes.Height = 0.1875!
        Me.lblMes.HyperLink = Nothing
        Me.lblMes.Left = 6.125!
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Style = ""
        Me.lblMes.Text = ""
        Me.lblMes.Top = 0.625!
        Me.lblMes.Width = 0.5!
        '
        'LblDia
        '
        Me.LblDia.Border.BottomColor = System.Drawing.Color.Black
        Me.LblDia.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblDia.Border.LeftColor = System.Drawing.Color.Black
        Me.LblDia.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblDia.Border.RightColor = System.Drawing.Color.Black
        Me.LblDia.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblDia.Border.TopColor = System.Drawing.Color.Black
        Me.LblDia.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.LblDia.Height = 0.15!
        Me.LblDia.HyperLink = Nothing
        Me.LblDia.Left = 6.94!
        Me.LblDia.Name = "LblDia"
        Me.LblDia.Style = ""
        Me.LblDia.Text = ""
        Me.LblDia.Top = 0.63!
        Me.LblDia.Width = 0.5!
        '
        'lblNombreCliente
        '
        Me.lblNombreCliente.Border.BottomColor = System.Drawing.Color.Black
        Me.lblNombreCliente.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNombreCliente.Border.LeftColor = System.Drawing.Color.Black
        Me.lblNombreCliente.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNombreCliente.Border.RightColor = System.Drawing.Color.Black
        Me.lblNombreCliente.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNombreCliente.Border.TopColor = System.Drawing.Color.Black
        Me.lblNombreCliente.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblNombreCliente.DataField = "CliNombre"
        Me.lblNombreCliente.Height = 0.1875!
        Me.lblNombreCliente.HyperLink = Nothing
        Me.lblNombreCliente.Left = 1.0!
        Me.lblNombreCliente.Name = "lblNombreCliente"
        Me.lblNombreCliente.Style = ""
        Me.lblNombreCliente.Text = ""
        Me.lblNombreCliente.Top = 0.81!
        Me.lblNombreCliente.Width = 4.0!
        '
        'lblVendedor
        '
        Me.lblVendedor.Border.BottomColor = System.Drawing.Color.Black
        Me.lblVendedor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblVendedor.Border.LeftColor = System.Drawing.Color.Black
        Me.lblVendedor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblVendedor.Border.RightColor = System.Drawing.Color.Black
        Me.lblVendedor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblVendedor.Border.TopColor = System.Drawing.Color.Black
        Me.lblVendedor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblVendedor.Height = 0.1875!
        Me.lblVendedor.HyperLink = Nothing
        Me.lblVendedor.Left = 1.0!
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Style = ""
        Me.lblVendedor.Text = ""
        Me.lblVendedor.Top = 1.875!
        Me.lblVendedor.Width = 4.1875!
        '
        'lblDireccionFactura
        '
        Me.lblDireccionFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.lblDireccionFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDireccionFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.lblDireccionFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDireccionFactura.Border.RightColor = System.Drawing.Color.Black
        Me.lblDireccionFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDireccionFactura.Border.TopColor = System.Drawing.Color.Black
        Me.lblDireccionFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDireccionFactura.Height = 0.1875!
        Me.lblDireccionFactura.HyperLink = Nothing
        Me.lblDireccionFactura.Left = 1.0625!
        Me.lblDireccionFactura.Name = "lblDireccionFactura"
        Me.lblDireccionFactura.Style = ""
        Me.lblDireccionFactura.Text = ""
        Me.lblDireccionFactura.Top = 1.0625!
        Me.lblDireccionFactura.Width = 4.0!
        '
        'lblDias
        '
        Me.lblDias.Border.BottomColor = System.Drawing.Color.Black
        Me.lblDias.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDias.Border.LeftColor = System.Drawing.Color.Black
        Me.lblDias.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDias.Border.RightColor = System.Drawing.Color.Black
        Me.lblDias.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDias.Border.TopColor = System.Drawing.Color.Black
        Me.lblDias.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDias.Height = 0.1979167!
        Me.lblDias.HyperLink = Nothing
        Me.lblDias.Left = 3.5!
        Me.lblDias.Name = "lblDias"
        Me.lblDias.Style = ""
        Me.lblDias.Text = ""
        Me.lblDias.Top = 1.44!
        Me.lblDias.Width = 0.5!
        '
        'lblFechaVenceFactura
        '
        Me.lblFechaVenceFactura.Border.BottomColor = System.Drawing.Color.Black
        Me.lblFechaVenceFactura.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechaVenceFactura.Border.LeftColor = System.Drawing.Color.Black
        Me.lblFechaVenceFactura.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechaVenceFactura.Border.RightColor = System.Drawing.Color.Black
        Me.lblFechaVenceFactura.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechaVenceFactura.Border.TopColor = System.Drawing.Color.Black
        Me.lblFechaVenceFactura.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblFechaVenceFactura.Height = 0.1979167!
        Me.lblFechaVenceFactura.HyperLink = Nothing
        Me.lblFechaVenceFactura.Left = 6.31!
        Me.lblFechaVenceFactura.Name = "lblFechaVenceFactura"
        Me.lblFechaVenceFactura.Style = ""
        Me.lblFechaVenceFactura.Text = ""
        Me.lblFechaVenceFactura.Top = 1.38!
        Me.lblFechaVenceFactura.Width = 1.0!
        '
        'lblCodigoCliente
        '
        Me.lblCodigoCliente.Border.BottomColor = System.Drawing.Color.Black
        Me.lblCodigoCliente.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoCliente.Border.LeftColor = System.Drawing.Color.Black
        Me.lblCodigoCliente.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoCliente.Border.RightColor = System.Drawing.Color.Black
        Me.lblCodigoCliente.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoCliente.Border.TopColor = System.Drawing.Color.Black
        Me.lblCodigoCliente.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoCliente.Height = 0.1875!
        Me.lblCodigoCliente.HyperLink = Nothing
        Me.lblCodigoCliente.Left = 6.0625!
        Me.lblCodigoCliente.Name = "lblCodigoCliente"
        Me.lblCodigoCliente.Style = ""
        Me.lblCodigoCliente.Text = ""
        Me.lblCodigoCliente.Top = 1.0!
        Me.lblCodigoCliente.Width = 1.0!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0.0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblCantidad, Me.lblCodigoUnidad, Me.lblCodigoProducto, Me.lblDescripcion, Me.lblPrecio, Me.lblValor})
        Me.Detail1.Height = 0.2291667!
        Me.Detail1.Name = "Detail1"
        '
        'lblCantidad
        '
        Me.lblCantidad.Border.BottomColor = System.Drawing.Color.Black
        Me.lblCantidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCantidad.Border.LeftColor = System.Drawing.Color.Black
        Me.lblCantidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCantidad.Border.RightColor = System.Drawing.Color.Black
        Me.lblCantidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCantidad.Border.TopColor = System.Drawing.Color.Black
        Me.lblCantidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCantidad.DataField = "Cantidad"
        Me.lblCantidad.Height = 0.19!
        Me.lblCantidad.HyperLink = Nothing
        Me.lblCantidad.Left = 0.25!
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Style = ""
        Me.lblCantidad.Text = ""
        Me.lblCantidad.Top = 0.0!
        Me.lblCantidad.Width = 0.56!
        '
        'lblCodigoUnidad
        '
        Me.lblCodigoUnidad.Border.BottomColor = System.Drawing.Color.Black
        Me.lblCodigoUnidad.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoUnidad.Border.LeftColor = System.Drawing.Color.Black
        Me.lblCodigoUnidad.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoUnidad.Border.RightColor = System.Drawing.Color.Black
        Me.lblCodigoUnidad.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoUnidad.Border.TopColor = System.Drawing.Color.Black
        Me.lblCodigoUnidad.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoUnidad.DataField = "CadigoUnidad"
        Me.lblCodigoUnidad.Height = 0.19!
        Me.lblCodigoUnidad.HyperLink = Nothing
        Me.lblCodigoUnidad.Left = 0.94!
        Me.lblCodigoUnidad.Name = "lblCodigoUnidad"
        Me.lblCodigoUnidad.Style = ""
        Me.lblCodigoUnidad.Text = ""
        Me.lblCodigoUnidad.Top = 0.0!
        Me.lblCodigoUnidad.Width = 0.56!
        '
        'lblCodigoProducto
        '
        Me.lblCodigoProducto.Border.BottomColor = System.Drawing.Color.Black
        Me.lblCodigoProducto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoProducto.Border.LeftColor = System.Drawing.Color.Black
        Me.lblCodigoProducto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoProducto.Border.RightColor = System.Drawing.Color.Black
        Me.lblCodigoProducto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoProducto.Border.TopColor = System.Drawing.Color.Black
        Me.lblCodigoProducto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblCodigoProducto.DataField = "CodigoProducto"
        Me.lblCodigoProducto.Height = 0.1875!
        Me.lblCodigoProducto.HyperLink = Nothing
        Me.lblCodigoProducto.Left = 1.625!
        Me.lblCodigoProducto.Name = "lblCodigoProducto"
        Me.lblCodigoProducto.Style = ""
        Me.lblCodigoProducto.Text = ""
        Me.lblCodigoProducto.Top = 0.0!
        Me.lblCodigoProducto.Width = 0.75!
        '
        'lblDescripcion
        '
        Me.lblDescripcion.Border.BottomColor = System.Drawing.Color.Black
        Me.lblDescripcion.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDescripcion.Border.LeftColor = System.Drawing.Color.Black
        Me.lblDescripcion.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDescripcion.Border.RightColor = System.Drawing.Color.Black
        Me.lblDescripcion.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDescripcion.Border.TopColor = System.Drawing.Color.Black
        Me.lblDescripcion.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblDescripcion.DataField = "Descripcion"
        Me.lblDescripcion.Height = 0.1875!
        Me.lblDescripcion.HyperLink = Nothing
        Me.lblDescripcion.Left = 2.4375!
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Style = ""
        Me.lblDescripcion.Text = ""
        Me.lblDescripcion.Top = 0.0!
        Me.lblDescripcion.Width = 2.875!
        '
        'lblPrecio
        '
        Me.lblPrecio.Border.BottomColor = System.Drawing.Color.Black
        Me.lblPrecio.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecio.Border.LeftColor = System.Drawing.Color.Black
        Me.lblPrecio.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecio.Border.RightColor = System.Drawing.Color.Black
        Me.lblPrecio.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecio.Border.TopColor = System.Drawing.Color.Black
        Me.lblPrecio.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblPrecio.DataField = "Precio"
        Me.lblPrecio.Height = 0.19!
        Me.lblPrecio.HyperLink = Nothing
        Me.lblPrecio.Left = 5.56!
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Style = ""
        Me.lblPrecio.Text = ""
        Me.lblPrecio.Top = 0.0!
        Me.lblPrecio.Width = 0.88!
        '
        'lblValor
        '
        Me.lblValor.Border.BottomColor = System.Drawing.Color.Black
        Me.lblValor.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblValor.Border.LeftColor = System.Drawing.Color.Black
        Me.lblValor.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblValor.Border.RightColor = System.Drawing.Color.Black
        Me.lblValor.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblValor.Border.TopColor = System.Drawing.Color.Black
        Me.lblValor.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblValor.DataField = "Valor"
        Me.lblValor.Height = 0.19!
        Me.lblValor.HyperLink = Nothing
        Me.lblValor.Left = 6.5!
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Style = ""
        Me.lblValor.Text = ""
        Me.lblValor.Top = 0.0!
        Me.lblValor.Width = 0.88!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblSubTotal, Me.lblImpuesto, Me.lblTotal, Me.lblTc, Me.lblSimboloMoneda, Me.Label1, Me.Label2})
        Me.PageFooter1.Height = 3.708333!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'lblSubTotal
        '
        Me.lblSubTotal.Border.BottomColor = System.Drawing.Color.Black
        Me.lblSubTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSubTotal.Border.LeftColor = System.Drawing.Color.Black
        Me.lblSubTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSubTotal.Border.RightColor = System.Drawing.Color.Black
        Me.lblSubTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSubTotal.Border.TopColor = System.Drawing.Color.Black
        Me.lblSubTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSubTotal.Height = 0.19!
        Me.lblSubTotal.HyperLink = Nothing
        Me.lblSubTotal.Left = 6.5!
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Style = ""
        Me.lblSubTotal.Text = ""
        Me.lblSubTotal.Top = 2.625!
        Me.lblSubTotal.Width = 0.88!
        '
        'lblImpuesto
        '
        Me.lblImpuesto.Border.BottomColor = System.Drawing.Color.Black
        Me.lblImpuesto.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblImpuesto.Border.LeftColor = System.Drawing.Color.Black
        Me.lblImpuesto.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblImpuesto.Border.RightColor = System.Drawing.Color.Black
        Me.lblImpuesto.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblImpuesto.Border.TopColor = System.Drawing.Color.Black
        Me.lblImpuesto.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblImpuesto.Height = 0.19!
        Me.lblImpuesto.HyperLink = Nothing
        Me.lblImpuesto.Left = 6.5!
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Style = ""
        Me.lblImpuesto.Text = ""
        Me.lblImpuesto.Top = 2.8125!
        Me.lblImpuesto.Width = 0.88!
        '
        'lblTotal
        '
        Me.lblTotal.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.RightColor = System.Drawing.Color.Black
        Me.lblTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Border.TopColor = System.Drawing.Color.Black
        Me.lblTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTotal.Height = 0.19!
        Me.lblTotal.HyperLink = Nothing
        Me.lblTotal.Left = 6.5!
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Style = ""
        Me.lblTotal.Text = ""
        Me.lblTotal.Top = 3.0!
        Me.lblTotal.Width = 0.88!
        '
        'lblTc
        '
        Me.lblTc.Border.BottomColor = System.Drawing.Color.Black
        Me.lblTc.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTc.Border.LeftColor = System.Drawing.Color.Black
        Me.lblTc.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTc.Border.RightColor = System.Drawing.Color.Black
        Me.lblTc.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTc.Border.TopColor = System.Drawing.Color.Black
        Me.lblTc.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblTc.Height = 0.1875!
        Me.lblTc.HyperLink = Nothing
        Me.lblTc.Left = 1.25!
        Me.lblTc.Name = "lblTc"
        Me.lblTc.Style = "font-weight: bold; "
        Me.lblTc.Text = "ESTE IMPORTE ES CON MANTENIMIENTO DE VALOR"
        Me.lblTc.Top = 3.125!
        Me.lblTc.Width = 4.875!
        '
        'lblSimboloMoneda
        '
        Me.lblSimboloMoneda.Border.BottomColor = System.Drawing.Color.Black
        Me.lblSimboloMoneda.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSimboloMoneda.Border.LeftColor = System.Drawing.Color.Black
        Me.lblSimboloMoneda.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSimboloMoneda.Border.RightColor = System.Drawing.Color.Black
        Me.lblSimboloMoneda.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSimboloMoneda.Border.TopColor = System.Drawing.Color.Black
        Me.lblSimboloMoneda.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.lblSimboloMoneda.Height = 0.1875!
        Me.lblSimboloMoneda.HyperLink = Nothing
        Me.lblSimboloMoneda.Left = 6.25!
        Me.lblSimboloMoneda.Name = "lblSimboloMoneda"
        Me.lblSimboloMoneda.Style = ""
        Me.lblSimboloMoneda.Text = "C$"
        Me.lblSimboloMoneda.Top = 2.625!
        Me.lblSimboloMoneda.Width = 0.25!
        '
        'Label1
        '
        Me.Label1.Border.BottomColor = System.Drawing.Color.Black
        Me.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.LeftColor = System.Drawing.Color.Black
        Me.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.RightColor = System.Drawing.Color.Black
        Me.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Border.TopColor = System.Drawing.Color.Black
        Me.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 6.25!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = ""
        Me.Label1.Text = "C$"
        Me.Label1.Top = 2.8125!
        Me.Label1.Width = 0.25!
        '
        'Label2
        '
        Me.Label2.Border.BottomColor = System.Drawing.Color.Black
        Me.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.LeftColor = System.Drawing.Color.Black
        Me.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.RightColor = System.Drawing.Color.Black
        Me.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Border.TopColor = System.Drawing.Color.Black
        Me.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 6.25!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = ""
        Me.Label2.Text = "C$"
        Me.Label2.Top = 3.0!
        Me.Label2.Width = 0.25!
        '
        'actrptImprimeFactCredito
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.69!
        Me.PageSettings.PaperWidth = 8.27!
        Me.PrintWidth = 7.489583!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" & _
                    "l; font-size: 10pt; color: Black; ", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" & _
                    "lic; ", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"))
        CType(Me.lblNumeroFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblAnio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblDia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNombreCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblVendedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDireccionFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFechaVenceFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCodigoCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCodigoUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCodigoProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDescripcion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblValor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSubTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblImpuesto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSimboloMoneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents lblCantidad As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCodigoUnidad As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCodigoProducto As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDescripcion As DataDynamics.ActiveReports.Label
    Friend WithEvents lblPrecio As DataDynamics.ActiveReports.Label
    Friend WithEvents lblValor As DataDynamics.ActiveReports.Label
    Friend WithEvents lblNumeroFactura As DataDynamics.ActiveReports.Label
    Friend WithEvents lblAnio As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMes As DataDynamics.ActiveReports.Label
    Friend WithEvents LblDia As DataDynamics.ActiveReports.Label
    Friend WithEvents lblNombreCliente As DataDynamics.ActiveReports.Label
    Friend WithEvents lblVendedor As DataDynamics.ActiveReports.Label
    Friend WithEvents lblSubTotal As DataDynamics.ActiveReports.Label
    Friend WithEvents lblImpuesto As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTotal As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTc As DataDynamics.ActiveReports.Label
    Friend WithEvents lblSimboloMoneda As DataDynamics.ActiveReports.Label
    Friend WithEvents Label1 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label2 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDireccionFactura As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDias As DataDynamics.ActiveReports.Label
    Friend WithEvents lblFechaVenceFactura As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCodigoCliente As DataDynamics.ActiveReports.Label
End Class
