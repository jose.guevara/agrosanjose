<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProformas
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProformas))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance()
        Me.bHerramientas = New DevComponents.DotNetBar.Bar()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.cmdVendedor = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdClientes = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdProductos = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdRefrescar = New DevComponents.DotNetBar.ButtonItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkBonificacion = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtMoneda = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtDireccion = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtPrecioCosto = New System.Windows.Forms.TextBox()
        Me.ddlSerie = New System.Windows.Forms.ComboBox()
        Me.lblSerie = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.txtVendedor = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.txtNegocio = New System.Windows.Forms.TextBox()
        Me.txtDepartamento = New System.Windows.Forms.TextBox()
        Me.txtNumeroCliente = New System.Windows.Forms.TextBox()
        Me.txtNumeroVendedor = New System.Windows.Forms.TextBox()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.ddlTipoPrecio = New System.Windows.Forms.ComboBox()
        Me.ddlRetencion = New System.Windows.Forms.ComboBox()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.txtProducto = New System.Windows.Forms.TextBox()
        Me.txtNombreCliente = New System.Windows.Forms.TextBox()
        Me.txtNombreVendedor = New System.Windows.Forms.TextBox()
        Me.txtVence = New System.Windows.Forms.TextBox()
        Me.txtDias = New System.Windows.Forms.TextBox()
        Me.txtNumeroFactura = New System.Windows.Forms.TextBox()
        Me.DPkFechaFactura = New System.Windows.Forms.DateTimePicker()
        Me.lblTipoPrecio = New System.Windows.Forms.Label()
        Me.lblRetencion = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.txtTasaCambio = New System.Windows.Forms.TextBox()
        Me.lblTasaCambio = New System.Windows.Forms.Label()
        Me.txtTotalCordobas = New System.Windows.Forms.TextBox()
        Me.lblTotalCordobas = New System.Windows.Forms.Label()
        Me.dgvDetalle = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.EsBonificacion = New Infragistics.Win.UltraDataGridView.UltraCheckEditorColumn(Me.components)
        Me.Cantidad = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Unidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Producto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Valor = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.Total = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn()
        Me.IdProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.txtRetencion = New System.Windows.Forms.TextBox()
        Me.txtImpVenta = New System.Windows.Forms.TextBox()
        Me.txtSubTotal = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.ButtonX1)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdOK, Me.LabelItem3, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(919, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 34
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.AutoExpandOnClick = True
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Image = CType(resources.GetObject("ButtonX1.Image"), System.Drawing.Image)
        Me.ButtonX1.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.ButtonX1.Location = New System.Drawing.Point(6, 8)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 54)
        Me.ButtonX1.SplitButton = True
        Me.ButtonX1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdVendedor, Me.cmdClientes, Me.cmdProductos, Me.cmdRefrescar})
        Me.ButtonX1.TabIndex = 0
        Me.ButtonX1.Tooltip = "<b><font color=""#17365D"">Listado</font></b>"
        '
        'cmdVendedor
        '
        Me.cmdVendedor.GlobalItem = False
        Me.cmdVendedor.Image = CType(resources.GetObject("cmdVendedor.Image"), System.Drawing.Image)
        Me.cmdVendedor.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdVendedor.Name = "cmdVendedor"
        Me.cmdVendedor.Text = "Vendedores"
        '
        'cmdClientes
        '
        Me.cmdClientes.GlobalItem = False
        Me.cmdClientes.Image = CType(resources.GetObject("cmdClientes.Image"), System.Drawing.Image)
        Me.cmdClientes.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdClientes.Name = "cmdClientes"
        Me.cmdClientes.Text = "Clientes"
        '
        'cmdProductos
        '
        Me.cmdProductos.GlobalItem = False
        Me.cmdProductos.Image = CType(resources.GetObject("cmdProductos.Image"), System.Drawing.Image)
        Me.cmdProductos.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdProductos.Name = "cmdProductos"
        Me.cmdProductos.Text = "Productos"
        '
        'cmdRefrescar
        '
        Me.cmdRefrescar.GlobalItem = False
        Me.cmdRefrescar.Image = CType(resources.GetObject("cmdRefrescar.Image"), System.Drawing.Image)
        Me.cmdRefrescar.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdRefrescar.Name = "cmdRefrescar"
        Me.cmdRefrescar.Text = "Refrescar"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.ButtonX1
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem3
        '
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkBonificacion)
        Me.GroupBox1.Controls.Add(Me.txtMoneda)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtPrecioCosto)
        Me.GroupBox1.Controls.Add(Me.ddlSerie)
        Me.GroupBox1.Controls.Add(Me.lblSerie)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.txtVendedor)
        Me.GroupBox1.Controls.Add(Me.TextBox17)
        Me.GroupBox1.Controls.Add(Me.txtNegocio)
        Me.GroupBox1.Controls.Add(Me.txtDepartamento)
        Me.GroupBox1.Controls.Add(Me.txtNumeroCliente)
        Me.GroupBox1.Controls.Add(Me.txtNumeroVendedor)
        Me.GroupBox1.Controls.Add(Me.txtPrecio)
        Me.GroupBox1.Controls.Add(Me.ddlTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.ddlRetencion)
        Me.GroupBox1.Controls.Add(Me.txtCantidad)
        Me.GroupBox1.Controls.Add(Me.txtProducto)
        Me.GroupBox1.Controls.Add(Me.txtNombreCliente)
        Me.GroupBox1.Controls.Add(Me.txtNombreVendedor)
        Me.GroupBox1.Controls.Add(Me.txtVence)
        Me.GroupBox1.Controls.Add(Me.txtDias)
        Me.GroupBox1.Controls.Add(Me.txtNumeroFactura)
        Me.GroupBox1.Controls.Add(Me.DPkFechaFactura)
        Me.GroupBox1.Controls.Add(Me.lblTipoPrecio)
        Me.GroupBox1.Controls.Add(Me.lblRetencion)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 77)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(919, 135)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        '
        'chkBonificacion
        '
        '
        '
        '
        Me.chkBonificacion.BackgroundStyle.Class = ""
        Me.chkBonificacion.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkBonificacion.Location = New System.Drawing.Point(357, 80)
        Me.chkBonificacion.Name = "chkBonificacion"
        Me.chkBonificacion.Size = New System.Drawing.Size(84, 20)
        Me.chkBonificacion.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.chkBonificacion.TabIndex = 46
        Me.chkBonificacion.Text = "Bonificacion"
        '
        'txtMoneda
        '
        Me.txtMoneda.Location = New System.Drawing.Point(489, 109)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.ReadOnly = True
        Me.txtMoneda.Size = New System.Drawing.Size(323, 20)
        Me.txtMoneda.TabIndex = 31
        Me.txtMoneda.Tag = "Fecha de Vencimiento"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(423, 111)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(52, 13)
        Me.Label20.TabIndex = 30
        Me.Label20.Text = "Moneda"
        '
        'txtDireccion
        '
        '
        '
        '
        Me.txtDireccion.Border.Class = "TextBoxBorder"
        Me.txtDireccion.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDireccion.Location = New System.Drawing.Point(489, 48)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDireccion.Size = New System.Drawing.Size(328, 20)
        Me.txtDireccion.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(424, 51)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Direccion"
        '
        'txtPrecioCosto
        '
        Me.txtPrecioCosto.Location = New System.Drawing.Point(779, 109)
        Me.txtPrecioCosto.Name = "txtPrecioCosto"
        Me.txtPrecioCosto.ReadOnly = True
        Me.txtPrecioCosto.Size = New System.Drawing.Size(42, 20)
        Me.txtPrecioCosto.TabIndex = 28
        Me.txtPrecioCosto.Tag = "Valor del Precio"
        Me.txtPrecioCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrecioCosto.Visible = False
        '
        'ddlSerie
        '
        Me.ddlSerie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlSerie.Location = New System.Drawing.Point(223, 16)
        Me.ddlSerie.Name = "ddlSerie"
        Me.ddlSerie.Size = New System.Drawing.Size(48, 21)
        Me.ddlSerie.TabIndex = 1
        Me.ddlSerie.Tag = "Serie por agencia"
        '
        'lblSerie
        '
        Me.lblSerie.AutoSize = True
        Me.lblSerie.Location = New System.Drawing.Point(187, 19)
        Me.lblSerie.Name = "lblSerie"
        Me.lblSerie.Size = New System.Drawing.Size(36, 13)
        Me.lblSerie.TabIndex = 27
        Me.lblSerie.Text = "Serie"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 112)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Precio"
        '
        'txtCliente
        '
        Me.txtCliente.Location = New System.Drawing.Point(72, 47)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(102, 20)
        Me.txtCliente.TabIndex = 5
        Me.txtCliente.Tag = "Cliente"
        '
        'txtVendedor
        '
        Me.txtVendedor.Location = New System.Drawing.Point(489, 17)
        Me.txtVendedor.Name = "txtVendedor"
        Me.txtVendedor.Size = New System.Drawing.Size(64, 20)
        Me.txtVendedor.TabIndex = 3
        Me.txtVendedor.Tag = "Vendedor"
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(691, 16)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(24, 20)
        Me.TextBox17.TabIndex = 25
        Me.TextBox17.TabStop = False
        Me.TextBox17.Text = "0"
        Me.TextBox17.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox17.Visible = False
        '
        'txtNegocio
        '
        Me.txtNegocio.Location = New System.Drawing.Point(781, 88)
        Me.txtNegocio.Name = "txtNegocio"
        Me.txtNegocio.Size = New System.Drawing.Size(40, 20)
        Me.txtNegocio.TabIndex = 24
        Me.txtNegocio.TabStop = False
        Me.txtNegocio.Tag = ""
        Me.txtNegocio.Text = "Negocio"
        Me.txtNegocio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNegocio.Visible = False
        '
        'txtDepartamento
        '
        Me.txtDepartamento.Location = New System.Drawing.Point(781, 64)
        Me.txtDepartamento.Name = "txtDepartamento"
        Me.txtDepartamento.Size = New System.Drawing.Size(40, 20)
        Me.txtDepartamento.TabIndex = 23
        Me.txtDepartamento.TabStop = False
        Me.txtDepartamento.Tag = ""
        Me.txtDepartamento.Text = "Departamento"
        Me.txtDepartamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDepartamento.Visible = False
        '
        'txtNumeroCliente
        '
        Me.txtNumeroCliente.Location = New System.Drawing.Point(781, 40)
        Me.txtNumeroCliente.Name = "txtNumeroCliente"
        Me.txtNumeroCliente.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroCliente.TabIndex = 22
        Me.txtNumeroCliente.TabStop = False
        Me.txtNumeroCliente.Tag = ""
        Me.txtNumeroCliente.Text = "Cliente"
        Me.txtNumeroCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroCliente.Visible = False
        '
        'txtNumeroVendedor
        '
        Me.txtNumeroVendedor.Location = New System.Drawing.Point(781, 16)
        Me.txtNumeroVendedor.Name = "txtNumeroVendedor"
        Me.txtNumeroVendedor.Size = New System.Drawing.Size(40, 20)
        Me.txtNumeroVendedor.TabIndex = 21
        Me.txtNumeroVendedor.TabStop = False
        Me.txtNumeroVendedor.Tag = ""
        Me.txtNumeroVendedor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumeroVendedor.Visible = False
        '
        'txtPrecio
        '
        Me.txtPrecio.Location = New System.Drawing.Point(72, 109)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.ReadOnly = True
        Me.txtPrecio.Size = New System.Drawing.Size(70, 20)
        Me.txtPrecio.TabIndex = 12
        Me.txtPrecio.Tag = "Valor del Precio"
        Me.txtPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ddlTipoPrecio
        '
        Me.ddlTipoPrecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlTipoPrecio.Location = New System.Drawing.Point(660, 80)
        Me.ddlTipoPrecio.Name = "ddlTipoPrecio"
        Me.ddlTipoPrecio.Size = New System.Drawing.Size(193, 21)
        Me.ddlTipoPrecio.TabIndex = 11
        Me.ddlTipoPrecio.Tag = "Tipo de Precio"
        '
        'ddlRetencion
        '
        Me.ddlRetencion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ddlRetencion.Items.AddRange(New Object() {"No", "Si"})
        Me.ddlRetencion.Location = New System.Drawing.Point(515, 80)
        Me.ddlRetencion.Name = "ddlRetencion"
        Me.ddlRetencion.Size = New System.Drawing.Size(48, 21)
        Me.ddlRetencion.TabIndex = 10
        Me.ddlRetencion.Tag = "Tiene Retenci�n de Impuesto"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(72, 80)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(70, 20)
        Me.txtCantidad.TabIndex = 8
        Me.txtCantidad.Tag = "Cantidad a Facturar"
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProducto
        '
        Me.txtProducto.Location = New System.Drawing.Point(212, 80)
        Me.txtProducto.Name = "txtProducto"
        Me.txtProducto.Size = New System.Drawing.Size(134, 20)
        Me.txtProducto.TabIndex = 9
        Me.txtProducto.Tag = "C�digo del Producto"
        '
        'txtNombreCliente
        '
        Me.txtNombreCliente.BackColor = System.Drawing.Color.White
        Me.txtNombreCliente.Location = New System.Drawing.Point(174, 47)
        Me.txtNombreCliente.Name = "txtNombreCliente"
        Me.txtNombreCliente.Size = New System.Drawing.Size(239, 20)
        Me.txtNombreCliente.TabIndex = 6
        Me.txtNombreCliente.Tag = ""
        '
        'txtNombreVendedor
        '
        Me.txtNombreVendedor.Location = New System.Drawing.Point(552, 17)
        Me.txtNombreVendedor.Name = "txtNombreVendedor"
        Me.txtNombreVendedor.ReadOnly = True
        Me.txtNombreVendedor.Size = New System.Drawing.Size(262, 20)
        Me.txtNombreVendedor.TabIndex = 4
        Me.txtNombreVendedor.TabStop = False
        Me.txtNombreVendedor.Tag = "Vendedor"
        '
        'txtVence
        '
        Me.txtVence.Location = New System.Drawing.Point(305, 109)
        Me.txtVence.Name = "txtVence"
        Me.txtVence.ReadOnly = True
        Me.txtVence.Size = New System.Drawing.Size(88, 20)
        Me.txtVence.TabIndex = 14
        Me.txtVence.Tag = "Fecha de Vencimiento"
        '
        'txtDias
        '
        Me.txtDias.Location = New System.Drawing.Point(212, 109)
        Me.txtDias.Name = "txtDias"
        Me.txtDias.Size = New System.Drawing.Size(40, 20)
        Me.txtDias.TabIndex = 13
        Me.txtDias.Tag = "D�as de Cr�dito"
        Me.txtDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNumeroFactura
        '
        Me.txtNumeroFactura.Location = New System.Drawing.Point(340, 16)
        Me.txtNumeroFactura.Name = "txtNumeroFactura"
        Me.txtNumeroFactura.ReadOnly = True
        Me.txtNumeroFactura.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroFactura.TabIndex = 2
        Me.txtNumeroFactura.Tag = "N�mero de Factura"
        '
        'DPkFechaFactura
        '
        Me.DPkFechaFactura.CustomFormat = "dd-MMM-yyyy"
        Me.DPkFechaFactura.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkFechaFactura.Location = New System.Drawing.Point(72, 16)
        Me.DPkFechaFactura.Name = "DPkFechaFactura"
        Me.DPkFechaFactura.Size = New System.Drawing.Size(104, 20)
        Me.DPkFechaFactura.TabIndex = 0
        Me.DPkFechaFactura.Tag = "Fecha de Ingreso"
        '
        'lblTipoPrecio
        '
        Me.lblTipoPrecio.AutoSize = True
        Me.lblTipoPrecio.Location = New System.Drawing.Point(588, 84)
        Me.lblTipoPrecio.Name = "lblTipoPrecio"
        Me.lblTipoPrecio.Size = New System.Drawing.Size(72, 13)
        Me.lblTipoPrecio.TabIndex = 9
        Me.lblTipoPrecio.Text = "Tipo Precio"
        '
        'lblRetencion
        '
        Me.lblRetencion.AutoSize = True
        Me.lblRetencion.Location = New System.Drawing.Point(451, 84)
        Me.lblRetencion.Name = "lblRetencion"
        Me.lblRetencion.Size = New System.Drawing.Size(65, 13)
        Me.lblRetencion.TabIndex = 8
        Me.lblRetencion.Text = "Retenci�n"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 84)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Cantidad"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(145, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Producto"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Cliente"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(424, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(261, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Vence"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(145, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "D�as"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(286, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "N�mero"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 504)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 665
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(919, 23)
        Me.UltraStatusBar1.TabIndex = 36
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.txtTasaCambio)
        Me.GroupPanel1.Controls.Add(Me.lblTasaCambio)
        Me.GroupPanel1.Controls.Add(Me.txtTotalCordobas)
        Me.GroupPanel1.Controls.Add(Me.lblTotalCordobas)
        Me.GroupPanel1.Controls.Add(Me.dgvDetalle)
        Me.GroupPanel1.Controls.Add(Me.Label19)
        Me.GroupPanel1.Controls.Add(Me.Label18)
        Me.GroupPanel1.Controls.Add(Me.Label17)
        Me.GroupPanel1.Controls.Add(Me.Label16)
        Me.GroupPanel1.Controls.Add(Me.Label15)
        Me.GroupPanel1.Controls.Add(Me.txtTotal)
        Me.GroupPanel1.Controls.Add(Me.txtRetencion)
        Me.GroupPanel1.Controls.Add(Me.txtImpVenta)
        Me.GroupPanel1.Controls.Add(Me.txtSubTotal)
        Me.GroupPanel1.Controls.Add(Me.Label14)
        Me.GroupPanel1.Controls.Add(Me.Label13)
        Me.GroupPanel1.Controls.Add(Me.Label12)
        Me.GroupPanel1.Controls.Add(Me.Label11)
        Me.GroupPanel1.Location = New System.Drawing.Point(0, 218)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(919, 277)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.Class = ""
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.Class = ""
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.Class = ""
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 37
        Me.GroupPanel1.Text = "Detalle Proformas"
        '
        'txtTasaCambio
        '
        Me.txtTasaCambio.Location = New System.Drawing.Point(193, 222)
        Me.txtTasaCambio.Name = "txtTasaCambio"
        Me.txtTasaCambio.Size = New System.Drawing.Size(160, 20)
        Me.txtTasaCambio.TabIndex = 101
        Me.txtTasaCambio.TabStop = False
        Me.txtTasaCambio.Tag = "Tasa Cambio"
        Me.txtTasaCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTasaCambio.Visible = False
        '
        'lblTasaCambio
        '
        Me.lblTasaCambio.AutoSize = True
        Me.lblTasaCambio.Location = New System.Drawing.Point(101, 222)
        Me.lblTasaCambio.Name = "lblTasaCambio"
        Me.lblTasaCambio.Size = New System.Drawing.Size(89, 13)
        Me.lblTasaCambio.TabIndex = 100
        Me.lblTasaCambio.Text = "Tasa de cambio: "
        Me.lblTasaCambio.Visible = False
        '
        'txtTotalCordobas
        '
        Me.txtTotalCordobas.Location = New System.Drawing.Point(468, 222)
        Me.txtTotalCordobas.Name = "txtTotalCordobas"
        Me.txtTotalCordobas.Size = New System.Drawing.Size(160, 20)
        Me.txtTotalCordobas.TabIndex = 99
        Me.txtTotalCordobas.TabStop = False
        Me.txtTotalCordobas.Tag = "Total Cordobas"
        Me.txtTotalCordobas.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCordobas.Visible = False
        '
        'lblTotalCordobas
        '
        Me.lblTotalCordobas.AutoSize = True
        Me.lblTotalCordobas.Location = New System.Drawing.Point(361, 222)
        Me.lblTotalCordobas.Name = "lblTotalCordobas"
        Me.lblTotalCordobas.Size = New System.Drawing.Size(101, 13)
        Me.lblTotalCordobas.TabIndex = 98
        Me.lblTotalCordobas.Text = "Total Cordobas C$: "
        Me.lblTotalCordobas.Visible = False
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EsBonificacion, Me.Cantidad, Me.Unidad, Me.Codigo, Me.Producto, Me.Impuesto, Me.Valor, Me.Total, Me.IdProducto, Me.PrecioCosto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvDetalle.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvDetalle.Location = New System.Drawing.Point(3, 3)
        Me.dgvDetalle.Name = "dgvDetalle"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(895, 134)
        Me.dgvDetalle.TabIndex = 92
        '
        'EsBonificacion
        '
        Me.EsBonificacion.CheckedAppearance = Appearance1
        Me.EsBonificacion.DefaultNewRowValue = CType(resources.GetObject("EsBonificacion.DefaultNewRowValue"), Object)
        Me.EsBonificacion.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.[Default]
        Me.EsBonificacion.HeaderText = "Bonificaci�n"
        Me.EsBonificacion.HotTrackingAppearance = Appearance2
        Me.EsBonificacion.IndeterminateAppearance = Appearance3
        Me.EsBonificacion.Name = "EsBonificacion"
        Me.EsBonificacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'Cantidad
        '
        '
        '
        '
        Me.Cantidad.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Cantidad.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cantidad.HeaderText = "Cantidad"
        Me.Cantidad.Increment = 1.0R
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Cantidad.Width = 70
        '
        'Unidad
        '
        Me.Unidad.HeaderText = "Unidad"
        Me.Unidad.Name = "Unidad"
        Me.Unidad.ReadOnly = True
        Me.Unidad.Width = 60
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Producto
        '
        Me.Producto.HeaderText = "Producto"
        Me.Producto.Name = "Producto"
        Me.Producto.ReadOnly = True
        Me.Producto.Width = 280
        '
        'Impuesto
        '
        Me.Impuesto.HeaderText = "Impuesto"
        Me.Impuesto.Name = "Impuesto"
        Me.Impuesto.ReadOnly = True
        Me.Impuesto.Width = 65
        '
        'Valor
        '
        '
        '
        '
        Me.Valor.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Valor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Valor.HeaderText = "Valor"
        Me.Valor.Increment = 1.0R
        Me.Valor.Name = "Valor"
        Me.Valor.Width = 90
        '
        'Total
        '
        '
        '
        '
        Me.Total.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Total.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Total.HeaderText = "Total"
        Me.Total.Increment = 1.0R
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'IdProducto
        '
        Me.IdProducto.HeaderText = "IdProducto"
        Me.IdProducto.Name = "IdProducto"
        Me.IdProducto.ReadOnly = True
        Me.IdProducto.Visible = False
        '
        'PrecioCosto
        '
        Me.PrecioCosto.HeaderText = "PrecioCosto"
        Me.PrecioCosto.Name = "PrecioCosto"
        Me.PrecioCosto.ReadOnly = True
        Me.PrecioCosto.Visible = False
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(61, 140)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 91
        Me.Label19.Text = "Label19"
        Me.Label19.Visible = False
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(5, 140)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 16)
        Me.Label18.TabIndex = 90
        Me.Label18.Text = "Label18"
        Me.Label18.Visible = False
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(538, 199)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(64, 16)
        Me.Label17.TabIndex = 89
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(293, 188)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(93, 20)
        Me.Label16.TabIndex = 88
        Me.Label16.Text = "ANULADA"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(301, 164)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(81, 13)
        Me.Label15.TabIndex = 87
        Me.Label15.Text = "<F5> AYUDA"
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(701, 222)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(104, 20)
        Me.txtTotal.TabIndex = 86
        Me.txtTotal.TabStop = False
        Me.txtTotal.Tag = "Cantidad a Facturar"
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRetencion
        '
        Me.txtRetencion.Location = New System.Drawing.Point(701, 198)
        Me.txtRetencion.Name = "txtRetencion"
        Me.txtRetencion.Size = New System.Drawing.Size(104, 20)
        Me.txtRetencion.TabIndex = 85
        Me.txtRetencion.TabStop = False
        Me.txtRetencion.Tag = "Cantidad a Facturar"
        Me.txtRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtImpVenta
        '
        Me.txtImpVenta.Location = New System.Drawing.Point(701, 174)
        Me.txtImpVenta.Name = "txtImpVenta"
        Me.txtImpVenta.Size = New System.Drawing.Size(104, 20)
        Me.txtImpVenta.TabIndex = 84
        Me.txtImpVenta.TabStop = False
        Me.txtImpVenta.Tag = "Cantidad a Facturar"
        Me.txtImpVenta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Location = New System.Drawing.Point(701, 150)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.Size = New System.Drawing.Size(104, 20)
        Me.txtSubTotal.TabIndex = 83
        Me.txtSubTotal.TabStop = False
        Me.txtSubTotal.Tag = "Cantidad a Facturar"
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(637, 222)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 82
        Me.Label14.Text = "Total Fact"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(637, 198)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 81
        Me.Label13.Text = "Retenci�n"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(637, 174)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 13)
        Me.Label12.TabIndex = 80
        Me.Label12.Text = "Imp Venta"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(637, 150)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 79
        Me.Label11.Text = "Sub Total"
        '
        'Timer1
        '
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12, Me.MenuItem4})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "&Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "&Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "L&impiar"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8})
        Me.MenuItem12.Text = "&Listado"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Vendedores"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Clientes"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Productos"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Refrescar"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 4
        Me.MenuItem4.Text = "&Salir"
        '
        'frmProformas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(919, 527)
        Me.Controls.Add(Me.GroupPanel1)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.bHerramientas)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmProformas"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generar Proformas"
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel1.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdVendedor As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdClientes As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdProductos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdRefrescar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPrecioCosto As System.Windows.Forms.TextBox
    Friend WithEvents ddlSerie As System.Windows.Forms.ComboBox
    Friend WithEvents lblSerie As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtVendedor As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents txtNegocio As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartamento As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents ddlTipoPrecio As System.Windows.Forms.ComboBox
    Friend WithEvents ddlRetencion As System.Windows.Forms.ComboBox
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents txtProducto As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreCliente As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreVendedor As System.Windows.Forms.TextBox
    Friend WithEvents txtVence As System.Windows.Forms.TextBox
    Friend WithEvents txtDias As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroFactura As System.Windows.Forms.TextBox
    Friend WithEvents DPkFechaFactura As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTipoPrecio As System.Windows.Forms.Label
    Friend WithEvents lblRetencion As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtRetencion As System.Windows.Forms.TextBox
    Friend WithEvents txtImpVenta As System.Windows.Forms.TextBox
    Friend WithEvents txtSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dgvDetalle As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtDireccion As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMoneda As TextBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtTasaCambio As TextBox
    Friend WithEvents lblTasaCambio As Label
    Friend WithEvents txtTotalCordobas As TextBox
    Friend WithEvents lblTotalCordobas As Label
    Friend WithEvents EsBonificacion As Infragistics.Win.UltraDataGridView.UltraCheckEditorColumn
    Friend WithEvents Cantidad As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Unidad As DataGridViewTextBoxColumn
    Friend WithEvents Codigo As DataGridViewTextBoxColumn
    Friend WithEvents Producto As DataGridViewTextBoxColumn
    Friend WithEvents Impuesto As DataGridViewTextBoxColumn
    Friend WithEvents Valor As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Total As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents IdProducto As DataGridViewTextBoxColumn
    Friend WithEvents PrecioCosto As DataGridViewTextBoxColumn
    Friend WithEvents chkBonificacion As DevComponents.DotNetBar.Controls.CheckBoxX
End Class
