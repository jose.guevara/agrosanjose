Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.DotNetBar.Rendering
Imports DevComponents.DotNetBar
Imports System.Threading
Imports System.Windows.Forms
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class frmCierreContable
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "frmCierreContable"
    Private rnd As New Random
    Dim sql, fil As String
    Dim jackvalida As New Valida
    ' Dim jackmensaje As New MegasysJack.Mensaje
    Dim jackAsientos As New Asientos
    Dim jackconsulta As New ConsultasDB
    Dim jackcrystal As New crystal_jack

    Dim todos As Boolean = True
    Dim where As Boolean

    Dim anexos_saldosDataSet As New SIMF_CONTADataSet.anexos_saldosDataTable

#Region "Load"
    Private Sub Frm_Cierres_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-NI")
        Dim dtos_mes As DataTable = Nothing
        Dim dtos_nivel As DataTable = Nothing
        Dim dtdatos_mes As DataTable = Nothing
        Dim dtdatos_nivel As DataTable = Nothing
        Dim fecha_ini As String
        Dim fecha_fin As String
        Try
            dtos_mes = New DataTable
            dtos_nivel = New DataTable

            dtdatos_nivel = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='NivelCuenta' order by ordenvariable", dtos_nivel, True)
            jackconsulta.carga_combo_DN(ComboB_Nivel, dtdatos_nivel, False)

            jackconsulta.carga_combo_ano_DN(ComboB_ano_TabCierres, 12)
            fecha_ini = jackconsulta.DameCampo_UltimoCierre(True, False, "Fecha_ini")
            fecha_fin = jackconsulta.DameCampo_UltimoCierre(True, False, "Fecha_fin")

            DateF_FechaFin.Text = fecha_fin.ToString

            DateF_FechaIni.Text = fecha_ini.ToString
            dtdatos_mes = jackconsulta.Retornadatatable("select descvariable,valorvariable from Macombos where variable='Mes' order by ordenvariable", dtos_mes, True)
            jackconsulta.carga_combo_DN(ComboB_mes_TabCierres, dtdatos_mes)
            Label_periodoCerrar.Text = "PER�ODO A CERRAR *** �LTIMO PER�ODO CERRADO " & jackconsulta.DameCampo_UltimoCierre(True)
            carga_nivel(1)
            ComboB_Nivel.SelectedValue = 1
            If ComboB_Reportes.Items.Count > 0 Then
                ComboB_Reportes.SelectedIndex = 0
            End If

            If GridP_SaldosN.Columns.Count > 0 Then
                GridP_SaldosN.Columns(2).DefaultCellStyle.Format = "##,##0.00"

                GridP_SaldosN.Columns(3).DefaultCellStyle.Format = "##,##0.00"
                GridP_SaldosN.Columns(4).DefaultCellStyle.Format = "##,##0.00"
                GridP_SaldosN.Columns(5).DefaultCellStyle.Format = "##,##0.00"
                GridP_SaldosN.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_SaldosN.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_SaldosN.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                GridP_SaldosN.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


            End If

            TextB_Imp_SR.Text = jackconsulta.Retorna_Campo("select porcimpuesto from cataimpuestos", True)
            TextB_IR.Text = jackconsulta.Retorna_Campo("select porcimpuesto from cataimpuestos", True)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Frm_Cierres_Load - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region
#Region "PRE CIERRES"
    Sub carga_nivel(Optional ByVal nivel As String = "1")

        Dim consulta As String = String.Empty
        Try
            consulta = jackvalida.carga_nivel(nivel)
            If consulta <> "1" Then
                Dim datos_cantidad As New DataTable
                datos_cantidad = jackconsulta.cargaGrid(consulta, True)
                If datos_cantidad.Rows.Count > 0 Then
                    GridP_SaldosN.DataSource = datos_cantidad
                    GridP_SaldosN.Show()
                Else
                    GridP_SaldosN.Hide()
                End If

                Label_periodoCerrar.Text = "PER�ODO A CERRAR *** �LTIMO PER�ODO CERRADO " & jackconsulta.DameCampo_UltimoCierre(True)
                ExpandablePanel3.TitleText = "Resumen de Saldos al Nivel" & nivel
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("carga_nivel - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
#Region "CIERRES"

    Sub cargar_Estado_Mes()
        Dim sql_dcierres As String = String.Empty
        Dim datos As DataTable = Nothing
        Try
            sql_dcierres = "SELECT anomes 'A�o-Mes',SUBSTRING(CONVERT(CHAR,anomes),1,4) AS 'A�o'," & _
      " (CASE SUBSTRING(CONVERT(CHAR,anomes),5,2)" & _
            " WHEN '01' THEN 'Enero'" & _
            " WHEN '02' THEN 'Febrero'" & _
            " WHEN '03' THEN 'Marzo'" & _
            " WHEN '04' THEN 'Abril'" & _
            " WHEN '05' THEN 'Mayo'" & _
            " WHEN '06' THEN 'Junio'" & _
            " WHEN '07' THEN 'Julio'" & _
            " WHEN '08' THEN 'Agosto'" & _
            " WHEN '09' THEN 'Septiembre'" & _
            " WHEN '10' THEN 'Octubre'" & _
            " WHEN '11' THEN 'Noviembre'" & _
            " WHEN '12' THEN 'Diciembre'" & _
            " WHEN '13' THEN 'Mes Trece' END)  Mes, fechacierre 'Fecha Cierre' , " & _
            " numeasientos 'Asientos', (CASE WHEN cerrado=0 THEN 'Abierto' ELSE 'Cerrado' END) AS Estado FROM dcierre_mes"
            If todos = True Then
                sql_dcierres += " ORDER BY anomes DESC"
            Else
                If ComboB_mes_TabCierres.Text <> "" And ComboB_mes_TabCierres.SelectedValue <> "-1" Then
                    sql_dcierres += " where substring(convert(char,anomes),5,2)=" & ComboB_mes_TabCierres.SelectedValue
                    where = True
                End If

                If ComboB_ano_TabCierres.Text <> "" And ComboB_ano_TabCierres.SelectedValue <> "-1" Then
                    If where = True Then
                        sql_dcierres += " and "
                    Else : sql_dcierres += " where "
                    End If
                    sql_dcierres += " substring(convert(char,anomes),1,4)=" & ComboB_ano_TabCierres.SelectedValue

                End If
            End If

            datos = jackconsulta.cargaGrid(sql_dcierres, True)
            GridP_CierreDefinitivo.DataSource = datos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("cargar_Estado_Mes - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ToolbarB_TabCierre_cerrarmes_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_TabCierre_cerrarmes.Click
        Dim row_mes As DataGridViewRow = Nothing
        Dim anomes As String = String.Empty
        Dim sql_actualiza_CerrarMes As String = String.Empty
        Try
            row_mes = GridP_CierreDefinitivo.CurrentRow
            anomes = row_mes.Cells("A�o-Mes").Value
            If jackconsulta.Retorna_Campo(" SELECT COUNT(anomes) AS cta FROM Dcierre_Mes WHERE cerrado=0 AND anomes<" & anomes, True) > 0 Then
                Label_tabcierres.Text = "****No se puede Cerrar el Mes Seleccionado, si Existen Meses Anteriores Abiertos****"
                Exit Sub
            End If
            If jackAsientos.existe_asientos_descuadrados(anomes) Then
                Label_tabcierres.Text = "****Existen Asientos Descuadrados en el Mes, Cierre no Realizado****"
                Exit Sub
            End If
            If jackconsulta.EjecutaSQL("update Dcierre_Mes set cerrado=1 where anomes=" & anomes, True) = -1 Then
                Label_tabcierres.Text = "****No se ha Efectuado El Cierre del mes Seleccionado****"
                Exit Sub
            Else
                Label_tabcierres.Text = "****Cierre del Mes " & row_mes.Cells("Mes").Value & " de " & row_mes.Cells("A�o").Value & " Efectuado Correctamente****"
                todos = True
                cargar_Estado_Mes()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_TabCierre_cerrarmes_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ToolbarB_TabCierre_abrirmes_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_TabCierre_abrirmes.Click
        Dim row_mes As DataGridViewRow = Nothing
        Dim anomes As String = String.Empty
        Dim sql_actualiza_CerrarMes As String = String.Empty
        Dim estado_mes As String = String.Empty
        Try
            row_mes = GridP_CierreDefinitivo.CurrentRow
            anomes = row_mes.Cells("A�o-Mes").Value
            estado_mes = jackconsulta.Retorna_Campo(" SELECT cerrado FROM Dcierre_Mes WHERE anomes=" & anomes, True)

            If jackconsulta.Retorna_Campo(" SELECT COUNT(anomes) AS cta FROM Dcierre_Mes WHERE cerrado=1 AND anomes>" & anomes, True) > 0 Then
                Label_tabcierres.Text = "****No se puede Abrir un Mes si Existen Meses Posteriores Cerrados****"
                Exit Sub
            End If
            If jackconsulta.EjecutaSQL("update Dcierre_Mes set cerrado=0 where anomes=" & anomes, True) = -1 Then
                Label_tabcierres.Text = "****No se ha Efectuado la Apertura del mes Seleccionado****"
                Exit Sub
            Else
                Label_tabcierres.Text = "****Apertura Exitosa del Mes " & row_mes.Cells("Mes").Value & " de " & row_mes.Cells("A�o").Value & "****"
                todos = True
                cargar_Estado_Mes()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_TabCierre_abrirmes_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
#End Region
#Region "Com�n"



    Private Sub ButtonItem_menuprincipal_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_menuprincipal.Click
        Try
            Me.Close()
            'menu_conta.Show()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ButtonItem_menuprincipal_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ToolbarB_Buscarmes_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_Buscarmes.Click
        Try
            todos = False
            cargar_Estado_Mes()
            Label_tabcierres.Text = ""
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_Buscarmes_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ToolbarB_EjecutarPrecierre_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_EjecutarPrecierre.Click
        Try
            Ejecuta_Precierre()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_EjecutarPrecierre_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ComboB_Nivel_LostFocus(sender As Object, e As System.EventArgs) Handles ComboB_Nivel.LostFocus
        Try
            carga_nivel(ComboB_Nivel.SelectedValue)
            SuperTabControl1.SelectedTabIndex = 0
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ComboB_Nivel_LostFocus - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ComboB_Nivel_TextChanged(sender As Object, e As System.EventArgs) Handles ComboB_Nivel.TextChanged
        Try
            If ComboB_Nivel.SelectedIndex > 0 Then
                carga_nivel(ComboB_Nivel.SelectedValue)
                SuperTabControl1.SelectedTabIndex = 0
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ComboB_Nivel_TextChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ComboB_Nivel_TextUpdate(sender As Object, e As System.EventArgs) Handles ComboB_Nivel.TextUpdate
        Try
            carga_nivel(ComboB_Nivel.SelectedValue)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ComboB_Nivel_TextUpdate - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SuperTabControl1_MouseClick(sender As Object, e As System.Windows.Forms.MouseEventArgs)

    End Sub

    Private Sub SuperTabControl1_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles SuperTabControl1.SelectedTabChanged

        Try
            Select Case SuperTabControl1.SelectedTabIndex
                Case "0"  'Tab Pre-Cierre
                    carga_nivel(1)
                    ComboB_Nivel.SelectedValue = 1
                    ExpandablePanel3.Expanded = True
                    ExpandablePanel5.Expanded = False
                Case "1" 'Tab Cierre Definitivo
                    ExpandablePanel3.Expanded = False
                    ExpandablePanel5.Expanded = True
                    cargar_Estado_Mes()
                    Label_tabcierres.Text = ""
                Case "2"  'Tab Cierre Anual
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("SuperTabControl1_SelectedTabChanged - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SuperTabControl1_TabIndexChanged(sender As Object, e As System.EventArgs) Handles SuperTabControl1.TabIndexChanged

    End Sub
    Sub Ejecuta_Precierre()
        Dim sql_mcierre_mes As SqlDataReader = Nothing
        Dim ano_ini As String = String.Empty
        Dim mes_ini As String = String.Empty
        Dim dia_ini As String = String.Empty
        Dim ano_fin As String = String.Empty
        Dim mes_fin As String = String.Empty
        Dim dia_fin As String = String.Empty
        Dim mes_ini_letra As String = String.Empty
        Dim mes_fin_letra As String = String.Empty
        Dim rango_fecha_letra As String = String.Empty
        Dim FECHINI_INI As String = String.Empty
        Dim fecha_ini As String = String.Empty
        Dim fecha_fin As String = String.Empty
        Dim fecha_ini_CIERRE As String = String.Empty
        Dim fecha_fin_CIERRE As String = String.Empty
        Try
            If jackvalida.existeTabla("Mcierres", True) < 1 Then
                LabelItem_msg_Precierre.Text = "Tabla no Existe"

            End If
            sql_mcierre_mes = jackconsulta.RetornaReader("select * from Mcierres where tipocierre='M'", True)
            If sql_mcierre_mes.HasRows Then
            Else
                jackconsulta.EjecutaSQL("insert into Mcierres (tipocierre,fecha_cierre,fecha_ini,fecha_fin )" & _
                                        " values('M',getdate(),getdate(),GETDATE())", True)
            End If
            ano_ini = String.Empty
            mes_ini = String.Empty
            dia_ini = String.Empty
            ano_fin = String.Empty
            mes_fin = String.Empty
            dia_fin = String.Empty
            mes_ini_letra = String.Empty
            mes_fin_letra = String.Empty
            rango_fecha_letra = String.Empty
            FECHINI_INI = String.Empty
            ano_ini = Year(DateF_FechaIni.Value)
            mes_ini = Month(DateF_FechaIni.Value)
            ano_fin = Year(DateF_FechaFin.Value)
            mes_fin = Month(DateF_FechaFin.Value)
            ' dia_ini = Day(DateF_FechaIni.Value)
            fecha_ini = String.Empty
            fecha_fin = String.Empty
            fecha_ini = Format(CDate(DateF_FechaIni.Text.Trim), "yyyyMMdd")
            fecha_fin = Format(CDate(DateF_FechaFin.Text.Trim), "yyyyMMdd")
            fecha_ini_CIERRE = String.Empty
            fecha_ini_CIERRE = "convert(DataTime(convert(varchar(10)," & DateF_FechaIni.Text & ",103)))"
            fecha_fin_CIERRE = String.Empty
            fecha_fin_CIERRE = "convert(DataTime(convert(varchar(10)," & DateF_FechaFin.Text & ",103)))"
            FECHINI_INI = CDate(DateF_FechaIni.Text)
            If jackconsulta.EjecutaSQL(String.Format("EXEC Ejecuta_Cierre @fecha_ini='{0}',@fecha_fin='{1}'", fecha_ini, fecha_fin), True) = -1 Then
                LabelItem_msg_Precierre.Text = "Pre Cierre Contable Fallido"
            Else
                LabelItem_msg_Precierre.Text = "Pre Cierre Contable Realizado Exitosamente"
                mes_ini_letra = MonthName(mes_ini)
                mes_fin_letra = MonthName(mes_fin)
                dia_ini = Mid(fecha_ini, 7, 2)
                dia_fin = Mid(fecha_fin, 7, 2)
                ' rango_fecha_letra = "DEL " & dia_ini.ToUpper & " DE " & mes_ini_letra.ToUpper.Trim & " DE " & ano_ini & " AL " & dia_fin & " DE " & mes_fin_letra.ToUpper.Trim & " DE " & Year(DateF_FechaFin.Value)
                rango_fecha_letra = " AL " & dia_fin & " DE " & mes_fin_letra.ToUpper.Trim & " DE " & Year(DateF_FechaFin.Value)
                REM Actualizo Mcierre
                jackconsulta.EjecutaSQL("update Mcierres SET fecha_ini='" & Format(CDate(DateF_FechaIni.Text), "yyyyMMdd") & "',fecha_fin='" & Format(CDate(DateF_FechaFin.Text), "yyyyMMdd") & "',fecha_cierre=GETDATE(),RANGOCIERRE= 'DEL '+((convert(varchar(10),fecha_ini,103))+ ' AL '+(convert(varchar(10),fecha_fin,103)) )  WHERE tipocierre='M'", True)
                'jackconsulta.EjecutaSQL("update Mcierres SET RANGOCIERRE= 'DEL '+((convert(varchar(10),fecha_ini,103))+ ' AL '+(convert(varchar(10),fecha_fin,103)) )  WHERE tipocierre='M'", True)
                jackconsulta.EjecutaSQL("update Mcierres SET RANGOCIERRE= '" & rango_fecha_letra & "'  WHERE tipocierre='M'", True)
                REM cargo grilla con resumen de saldos a Primer Nivel(Clase)

                carga_nivel(1)

                Label_periodoCerrar.Text = "PER�ODO A CERRAR *** �LTIMO PER�ODO CERRADO " & jackconsulta.DameCampo_UltimoCierre(True)

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Ejecuta_Precierre - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ButtonItem_cerrarpantalla_Click(sender As System.Object, e As System.EventArgs) Handles ButtonItem_cerrarpantalla.Click
        Me.Close()
    End Sub
    Sub Reportes()
        Try
            Select Case SUConversiones.ConvierteAString(ComboB_Reportes.SelectedValue)
                Case "balanza_comprobacion_saldos"
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Reportes - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button_Imprimir_SaldosNivel_Click(sender As System.Object, e As System.EventArgs) Handles Button_Imprimir_SaldosNivel.Click
        Dim filtro_reportes As String = String.Empty
        Dim tituloreport As String = String.Empty
        Dim fecha_ini As String = String.Empty
        Dim fecha_fin As String = String.Empty
        Dim dTOS As DataTable = Nothing
        Dim dtsDatos As DataTable = Nothing
        Dim sql_anexo As String = String.Empty
        Dim rango_titulo_ER As String = String.Empty
        Dim porc As Decimal = 0
        Dim ano_ini As String = String.Empty
        Dim mes_ini As String = String.Empty
        Dim dia As String = String.Empty
        Dim ini As String = String.Empty
        Dim ano_fin As String = String.Empty
        Dim mes_fin As String = String.Empty
        Dim dia_fin As String = String.Empty
        Dim dia_ini As String = String.Empty
        Dim mes_ini_letra As String = String.Empty
        Dim mes_fin_letra As String = String.Empty
        Dim rango_fecha_letra As String = String.Empty
        Dim FECHINI_INI As String = String.Empty
        Try
            jackcrystal.borra_formulas_reporte()
            'SIMF_CONTA.formula(0) = "compania," & SIMF_CONTA.nombre_cia.Trim
            SIMF_CONTA.formula(0) = "compania," & SUConversiones.ConvierteAString(RNEmpresa.ObtieneNombreEmpreaContabilidad())
            SIMF_CONTA.formula(1) = "RangoFecha, " & SUConversiones.ConvierteAString(jackconsulta.DameCampo_UltimoCierre(True, False))
            SIMF_CONTA.filtro_reporte = String.Empty

            Select Case ComboB_Reportes.SelectedItem.ToString
                Case "Balanza de Comprobaci�n de Saldos"
                    tituloreport = "Balanza Comprobaci�n de Saldos"
                    SIMF_CONTA.formula(2) = "titulo," & tituloreport
                    SIMF_CONTA.numeformulas = 3
                    REM ac� no necesitamos filtro, debemos cargar toda vista seg�n el cierre
                    SIMF_CONTA.filtro_reporte = filtro_reportes
                    Frm_RPT_Balanza_saldos.Show()
                Case "Balanza Comprobaci�n Por Nivel"
                    tituloreport = "Balanza Comprobaci�n de Saldos " & ComboB_Nivel.Text
                    carga_nivel(ComboB_Nivel.SelectedValue)
                    SIMF_CONTA.formula(2) = "titulo," & tituloreport
                    SIMF_CONTA.numeformulas = 3
                    SIMF_CONTA.filtro_reporte = filtro_reportes
                    Frm_RPT_BalanXnivel.Show()
                Case "Anexo Estado Resultado"
                    dTOS = New DataTable
                    dtsDatos = Nothing
                    tituloreport = "Anexo Estado Resultado"
                    SIMF_CONTA.formula(2) = "titulo," & tituloreport
                    SIMF_CONTA.numeformulas = 3

                    'sql_anexo = String.Empty
                    'sql_anexo = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'anexos_saldos') and OBJECTPROPERTY(id, N'IsUserTable') = 1) DROP TABLE anexos_saldos ; "
                    'sql_anexo += " select * into  anexos_saldos"
                    'sql_anexo += " from view_catalogoconta where cdgocuenta like'4%' or cdgocuenta like'5%' or cdgocuenta like'6%' or cdgocuenta like'7%' or cdgocuenta like'8%'"
                    'jackconsulta.EjecutaSQL(sql_anexo, True)
                    RNCuentaContable.GeneraAnexoEstadoResultado()
                    SIMF_CONTA.filtro_reporte = filtro_reportes
                    Frm_RPT_Anexos_Saldos.Show()
                Case "Anexo Balance General"
                    tituloreport = "Anexo Balance General"
                    SIMF_CONTA.formula(2) = "titulo," & tituloreport
                    SIMF_CONTA.numeformulas = 3
                    'sql_anexo = String.Empty
                    'sql_anexo = "IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'anexos_saldos') and OBJECTPROPERTY(id, N'IsUserTable') = 1) DROP TABLE anexos_saldos ; "
                    'sql_anexo += " select * into  anexos_saldos"
                    'sql_anexo += " from view_catalogoconta where cdgocuenta like'1%' or cdgocuenta like'2%' or cdgocuenta like'3%'"
                    'jackconsulta.EjecutaSQL(sql_anexo, True)
                    RNCuentaContable.GenerarBalanceGeneral()
                    SIMF_CONTA.filtro_reporte = filtro_reportes
                    Frm_RPT_Anexos_Saldos.Show()
                Case "Balance General"
                    tituloreport = "Balance General"
                    SIMF_CONTA.formula(2) = "titulo," & tituloreport
                    SIMF_CONTA.numeformulas = 3
                    SIMF_CONTA.filtro_reporte = "BG"
                    Frm_RPT_BGER.Show()
                Case "Estado Resultado"
                    ano_ini = String.Empty
                    mes_ini = String.Empty
                    dia_ini = String.Empty
                    ano_fin = String.Empty
                    mes_fin = String.Empty
                    dia_fin = String.Empty
                    mes_ini_letra = String.Empty
                    mes_fin_letra = String.Empty
                    rango_fecha_letra = String.Empty
                    FECHINI_INI = String.Empty
                    ano_ini = Year(DateF_FechaIni.Value)
                    mes_ini = Month(DateF_FechaIni.Value)
                    ano_fin = Year(DateF_FechaFin.Value)
                    mes_fin = Month(DateF_FechaFin.Value)
                    mes_ini_letra = MonthName(mes_ini)
                    mes_fin_letra = MonthName(mes_fin)
                    fecha_ini = String.Empty
                    If DateF_FechaIni.Text.Trim.Length > 0 Then
                        fecha_ini = Format(CDate(DateF_FechaIni.Text.Trim), "yyyyMMdd")
                    Else
                        MessageBoxEx.Show(" Error:  Debe seleccionar la fecha de inicio. ", "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return
                    End If

                    fecha_fin = String.Empty
                    If DateF_FechaFin.Text.Trim.Length > 0 Then
                        fecha_fin = Format(CDate(DateF_FechaFin.Text.Trim), "yyyyMMdd")
                    Else
                        MessageBoxEx.Show(" Error:  Debe seleccionar la fecha de fin. ", "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return
                    End If

                    If fecha_ini.Trim().Length > 0 Then
                        dia_ini = Mid(fecha_ini, 7, 2)
                    End If

                    If fecha_fin.Trim().Length > 0 Then
                        dia_fin = Mid(fecha_fin, 7, 2)
                    End If

                    'Dim rango_titulo_ER As String = "DEL " & dia_ini.ToUpper & " DE " & mes_ini_letra.ToUpper.Trim & " DE " & ano_ini & " AL " & dia_fin & " DE " & mes_fin_letra.ToUpper.Trim & " DE " & Year(DateF_FechaFin.Value)
                    rango_titulo_ER = "DEL 01 DE ENERO DE " & ano_ini & " AL " & dia_fin & " DE " & mes_fin_letra.ToUpper.Trim & " DE " & Year(DateF_FechaFin.Value)
                    SIMF_CONTA.formula(1) = "RangoFecha, " & rango_titulo_ER
                    porc = 0
                    If TextB_IR.Text = String.Empty Then
                        ' LabelItem_msg_Precierre.Text = "**** Debe Ingresar el % de Impuesto Para Generar el Estado de Resultado ****"
                        mensaje(Button_Imprimir_SaldosNivel, "**** Debe Ingresar el % de Impuesto Para Generar el Estado de Resultado ****", "Impresi�n de Estado Financiero")
                        Exit Sub
                    Else
                        If IsNumeric(TextB_IR.Text) = False Then
                            mensaje(Button_Imprimir_SaldosNivel, "**** Debe Ingresar un % de Impuesto V�lido Para Generar el Estado de Resultado ****", "Impresi�n de Estado Financiero")
                            ' LabelItem_msg_Precierre.Text = "**** Debe Ingresar un % de Impuesto V�lido Para Generar el Estado de Resultado ****"
                            Exit Sub
                        Else
                            porc = SUConversiones.ConvierteADecimal(TextB_IR.Text.Trim)
                        End If
                    End If
                    tituloreport = "Estado de Resultado"
                    SIMF_CONTA.formula(3) = "titulo," & tituloreport
                    SIMF_CONTA.formula(4) = "IR," & porc
                    SIMF_CONTA.numeformulas = 5
                    SIMF_CONTA.filtro_reporte = "ER"
                    Frm_RPT_ER.Show()

                Case "Diario General"
                    fecha_ini = String.Empty
                    fecha_fin = String.Empty
                    'fecha_ini = Format(CDate(DateF_FechaIni.Text.Trim), "yyyyMMdd")
                    'fecha_fin = Format(CDate(DateF_FechaFin.Text.Trim), "yyyyMMdd")
                    fecha_ini = String.Empty
                    If DateF_FechaIni.Text.Trim.Length > 0 Then
                        fecha_ini = Format(CDate(DateF_FechaIni.Text.Trim), "yyyyMMdd")
                    Else
                        MessageBoxEx.Show(" Error:  Debe seleccionar la fecha de inicio. ", "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return
                    End If

                    fecha_fin = String.Empty
                    If DateF_FechaFin.Text.Trim.Length > 0 Then
                        fecha_fin = Format(CDate(DateF_FechaFin.Text.Trim), "yyyyMMdd")
                    Else
                        MessageBoxEx.Show(" Error:  Debe seleccionar la fecha de fin. ", "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return
                    End If

                    tituloreport = "Diario General"
                    SIMF_CONTA.formula(1) = "RangoFecha, " & " Del " & Format(CDate(DateF_FechaFin.Text.Trim), "dd/MM/yyyy") & " AL " & Format(CDate(DateF_FechaIni.Text.Trim), "dd/MM/yyyy")
                    SIMF_CONTA.formula(2) = "titulo," & tituloreport
                    SIMF_CONTA.numeformulas = 3
                    SIMF_CONTA.filtro_reporte = "'" & fecha_ini & "' and '" & fecha_fin & "'"
                    SIMF_CONTA.formula(3) = fecha_ini
                    SIMF_CONTA.formula(4) = fecha_fin
                    Frm_RPT_DiarioGeneral.Show()
            End Select
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("Button_Imprimir_SaldosNivel_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub mensaje(ByVal controls As DevComponents.DotNetBar.ButtonItem, ByVal mensaje As String, ByVal caption As String)
        Dim msg As Balloon = New Balloon()
        Try
            msg = New Balloon()
            msg.Style = eBallonStyle.Alert
            msg.BackColor = System.Drawing.Color.DarkSlateGray
            msg.BackColor2 = System.Drawing.Color.SteelBlue
            msg.CaptionImage = CType(BalloonTip_cierre.CaptionImage.Clone(), Image)
            msg.CaptionText = caption
            msg.CaptionColor = System.Drawing.Color.Blue
            msg.Text = mensaje
            msg.ForeColor = System.Drawing.Color.White

            msg.AlertAnimation = eAlertAnimation.LeftToRight
            msg.AutoClose = True
            msg.AutoCloseTimeOut = 8
            msg.AutoResize()
            msg.Owner = Me
            msg.Show(controls, False)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("mensaje - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region
#Region "Mantenimiento Impuesto"
    Private Sub ToolbarB_Guardarimpuesto_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_Guardarimpuesto.Click
        Dim sql_actualiza_Impuesto As String = String.Empty
        Try
            If TextB_Imp_SR.Text <> String.Empty Then
                If IsNumeric(TextB_Imp_SR.Text.Trim) Then
                    sql_actualiza_Impuesto = "update cataimpuestos set Porcimpuesto=" & TextB_Imp_SR.Text.Trim
                    jackconsulta.EjecutaSQL(sql_actualiza_Impuesto, True)
                Else
                    mensaje(ToolbarB_Guardarimpuesto, "Especifique un % v�lido para el Impuesto ", "Mantenimiento de Impuesto")
                End If
            Else
                mensaje(ToolbarB_Guardarimpuesto, "Especifique un % para el Impuesto", "Mantenimiento de Impuesto")
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("ToolbarB_Guardarimpuesto_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

    Private Sub GridP_SaldosN_Click(sender As Object, e As System.EventArgs) Handles GridP_SaldosN.Click
        Dim row As DataGridViewRow = Nothing
        Try
            row = GridP_SaldosN.CurrentRow
            If row Is Nothing Then
                Exit Sub
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridP_SaldosN_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub GridP_CierreDefinitivo_Click(sender As Object, e As System.EventArgs) Handles GridP_CierreDefinitivo.Click
        Dim row As DataGridViewRow = Nothing
        Try
            row = GridP_CierreDefinitivo.CurrentRow
            If row Is Nothing Then
                Exit Sub
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("GridP_CierreDefinitivo_Click - Error: " + ex.Message.ToString(), "Asientos/Comprobantes", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

End Class