﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInsertarCheque
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtMonedaCuenta = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNumCuenta = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNombInstFinanciera = New System.Windows.Forms.TextBox
        Me.txtCodCheque = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNumTransaccion = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtDescripcionCheque = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtMontoCheque = New System.Windows.Forms.TextBox
        Me.txtSignoMonedaCuenta = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtNomBeneficiario = New System.Windows.Forms.TextBox
        Me.txtCodBeneficiario = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtSaldoActual = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.TextBox22 = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.Label19 = New System.Windows.Forms.Label
        Me.TextBox23 = New System.Windows.Forms.TextBox
        Me.TextBox24 = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.TextBox25 = New System.Windows.Forms.TextBox
        Me.TextBox26 = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtMontoEsteCheque = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtSaldoAnterior = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.txtDescCheque = New System.Windows.Forms.TextBox
        Me.txtInstFinanciera = New System.Windows.Forms.TextBox
        Me.txtValorLetras = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtMonto = New System.Windows.Forms.TextBox
        Me.txtSignoMoneda = New System.Windows.Forms.TextBox
        Me.txtNombreBeneficiario = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtFechaElavoracion = New System.Windows.Forms.TextBox
        Me.txtNumeroDoc = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.cmdGuardar = New System.Windows.Forms.Button
        Me.btnSalir = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.txtMonedaCuenta)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtNumCuenta)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNombInstFinanciera)
        Me.GroupBox1.Controls.Add(Me.txtCodCheque)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtNumTransaccion)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(10, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(600, 72)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(560, 40)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(32, 24)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Label17"
        Me.Label17.Visible = False
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(520, 40)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(32, 24)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Label16"
        Me.Label16.Visible = False
        '
        'txtMonedaCuenta
        '
        Me.txtMonedaCuenta.Location = New System.Drawing.Point(392, 40)
        Me.txtMonedaCuenta.Name = "txtMonedaCuenta"
        Me.txtMonedaCuenta.ReadOnly = True
        Me.txtMonedaCuenta.Size = New System.Drawing.Size(104, 20)
        Me.txtMonedaCuenta.TabIndex = 8
        Me.txtMonedaCuenta.Tag = "Moneda de la cuenta"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(296, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Tipo de Moneda"
        '
        'txtNumCuenta
        '
        Me.txtNumCuenta.Location = New System.Drawing.Point(120, 40)
        Me.txtNumCuenta.Name = "txtNumCuenta"
        Me.txtNumCuenta.ReadOnly = True
        Me.txtNumCuenta.Size = New System.Drawing.Size(168, 20)
        Me.txtNumCuenta.TabIndex = 6
        Me.txtNumCuenta.Tag = "Número de Cuenta"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Número de Cuenta"
        '
        'txtNombInstFinanciera
        '
        Me.txtNombInstFinanciera.Location = New System.Drawing.Point(361, 16)
        Me.txtNombInstFinanciera.Name = "txtNombInstFinanciera"
        Me.txtNombInstFinanciera.ReadOnly = True
        Me.txtNombInstFinanciera.Size = New System.Drawing.Size(231, 20)
        Me.txtNombInstFinanciera.TabIndex = 4
        Me.txtNombInstFinanciera.Tag = "Nombre de la institución financiera"
        '
        'txtCodCheque
        '
        Me.txtCodCheque.Location = New System.Drawing.Point(323, 16)
        Me.txtCodCheque.Name = "txtCodCheque"
        Me.txtCodCheque.Size = New System.Drawing.Size(32, 20)
        Me.txtCodCheque.TabIndex = 3
        Me.txtCodCheque.Tag = "Código de chequera"
        Me.txtCodCheque.Text = "0"
        Me.txtCodCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(252, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = " Chequera"
        '
        'txtNumTransaccion
        '
        Me.txtNumTransaccion.Location = New System.Drawing.Point(107, 16)
        Me.txtNumTransaccion.Name = "txtNumTransaccion"
        Me.txtNumTransaccion.ReadOnly = True
        Me.txtNumTransaccion.Size = New System.Drawing.Size(139, 20)
        Me.txtNumTransaccion.TabIndex = 1
        Me.txtNumTransaccion.Tag = "Número de transacción"
        Me.txtNumTransaccion.Text = "0"
        Me.txtNumTransaccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No. Transacción"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtDescripcionCheque)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txtMontoCheque)
        Me.GroupBox2.Controls.Add(Me.txtSignoMonedaCuenta)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtNomBeneficiario)
        Me.GroupBox2.Controls.Add(Me.txtCodBeneficiario)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(10, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(600, 96)
        Me.GroupBox2.TabIndex = 22
        Me.GroupBox2.TabStop = False
        '
        'txtDescripcionCheque
        '
        Me.txtDescripcionCheque.Location = New System.Drawing.Point(80, 64)
        Me.txtDescripcionCheque.MaxLength = 80
        Me.txtDescripcionCheque.Name = "txtDescripcionCheque"
        Me.txtDescripcionCheque.Size = New System.Drawing.Size(416, 20)
        Me.txtDescripcionCheque.TabIndex = 14
        Me.txtDescripcionCheque.Tag = "Descripción del cheque"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 64)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Descripción"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(368, 40)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker1.TabIndex = 12
        Me.DateTimePicker1.Tag = "Fecha de elaboración"
        Me.DateTimePicker1.Value = New Date(2009, 2, 16, 19, 3, 0, 0)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(258, 40)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(110, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Fecha del Cheque"
        '
        'txtMontoCheque
        '
        Me.txtMontoCheque.Location = New System.Drawing.Point(158, 41)
        Me.txtMontoCheque.Name = "txtMontoCheque"
        Me.txtMontoCheque.Size = New System.Drawing.Size(88, 20)
        Me.txtMontoCheque.TabIndex = 10
        Me.txtMontoCheque.Tag = "Monto del cheque"
        Me.txtMontoCheque.Text = "0.00"
        Me.txtMontoCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSignoMonedaCuenta
        '
        Me.txtSignoMonedaCuenta.Location = New System.Drawing.Point(120, 41)
        Me.txtSignoMonedaCuenta.Name = "txtSignoMonedaCuenta"
        Me.txtSignoMonedaCuenta.ReadOnly = True
        Me.txtSignoMonedaCuenta.Size = New System.Drawing.Size(32, 20)
        Me.txtSignoMonedaCuenta.TabIndex = 9
        Me.txtSignoMonedaCuenta.Tag = "Signo de la moneda de la cuenta"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(110, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Monto Documento"
        '
        'txtNomBeneficiario
        '
        Me.txtNomBeneficiario.Location = New System.Drawing.Point(142, 16)
        Me.txtNomBeneficiario.MaxLength = 60
        Me.txtNomBeneficiario.Name = "txtNomBeneficiario"
        Me.txtNomBeneficiario.Size = New System.Drawing.Size(360, 20)
        Me.txtNomBeneficiario.TabIndex = 7
        Me.txtNomBeneficiario.Tag = "Nombre del beneficiario"
        '
        'txtCodBeneficiario
        '
        Me.txtCodBeneficiario.Location = New System.Drawing.Point(80, 16)
        Me.txtCodBeneficiario.Name = "txtCodBeneficiario"
        Me.txtCodBeneficiario.Size = New System.Drawing.Size(56, 20)
        Me.txtCodBeneficiario.TabIndex = 6
        Me.txtCodBeneficiario.Tag = "Código del beneficiario"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Beneficiario"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtSaldoActual)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.txtMontoEsteCheque)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtSaldoAnterior)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(10, 192)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(120, 144)
        Me.GroupBox3.TabIndex = 23
        Me.GroupBox3.TabStop = False
        '
        'txtSaldoActual
        '
        Me.txtSaldoActual.Location = New System.Drawing.Point(8, 112)
        Me.txtSaldoActual.Name = "txtSaldoActual"
        Me.txtSaldoActual.ReadOnly = True
        Me.txtSaldoActual.Size = New System.Drawing.Size(104, 20)
        Me.txtSaldoActual.TabIndex = 15
        Me.txtSaldoActual.Tag = "Saldo de la cuenta en el momento del cheque"
        Me.txtSaldoActual.Text = "0.00"
        Me.txtSaldoActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 96)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Saldo Actual"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.TextBox22)
        Me.GroupBox5.Controls.Add(Me.Label18)
        Me.GroupBox5.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox5.Controls.Add(Me.Label19)
        Me.GroupBox5.Controls.Add(Me.TextBox23)
        Me.GroupBox5.Controls.Add(Me.TextBox24)
        Me.GroupBox5.Controls.Add(Me.Label20)
        Me.GroupBox5.Controls.Add(Me.TextBox25)
        Me.GroupBox5.Controls.Add(Me.TextBox26)
        Me.GroupBox5.Controls.Add(Me.Label21)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(2, -96)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(600, 96)
        Me.GroupBox5.TabIndex = 20
        Me.GroupBox5.TabStop = False
        '
        'TextBox22
        '
        Me.TextBox22.Location = New System.Drawing.Point(80, 64)
        Me.TextBox22.MaxLength = 80
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New System.Drawing.Size(416, 20)
        Me.TextBox22.TabIndex = 14
        Me.TextBox22.Tag = "Descripción del cheque"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 64)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 13)
        Me.Label18.TabIndex = 13
        Me.Label18.Text = "Descripción"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(368, 40)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(96, 20)
        Me.DateTimePicker2.TabIndex = 12
        Me.DateTimePicker2.Tag = "Fecha de elaboración"
        Me.DateTimePicker2.Value = New Date(2009, 2, 16, 19, 3, 0, 0)
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(258, 40)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 13)
        Me.Label19.TabIndex = 11
        Me.Label19.Text = "Fecha del Cheque"
        '
        'TextBox23
        '
        Me.TextBox23.Location = New System.Drawing.Point(158, 41)
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New System.Drawing.Size(88, 20)
        Me.TextBox23.TabIndex = 10
        Me.TextBox23.Tag = "Monto del cheque"
        Me.TextBox23.Text = "0.00"
        Me.TextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox24
        '
        Me.TextBox24.Location = New System.Drawing.Point(120, 41)
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.ReadOnly = True
        Me.TextBox24.Size = New System.Drawing.Size(32, 20)
        Me.TextBox24.TabIndex = 9
        Me.TextBox24.Tag = "Signo de la moneda de la cuenta"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 40)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(110, 13)
        Me.Label20.TabIndex = 8
        Me.Label20.Text = "Monto Documento"
        '
        'TextBox25
        '
        Me.TextBox25.Location = New System.Drawing.Point(142, 16)
        Me.TextBox25.MaxLength = 60
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New System.Drawing.Size(360, 20)
        Me.TextBox25.TabIndex = 7
        Me.TextBox25.Tag = "Nombre del beneficiario"
        '
        'TextBox26
        '
        Me.TextBox26.Location = New System.Drawing.Point(80, 16)
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New System.Drawing.Size(56, 20)
        Me.TextBox26.TabIndex = 6
        Me.TextBox26.Tag = "Código del beneficiario"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(8, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(74, 13)
        Me.Label21.TabIndex = 5
        Me.Label21.Text = "Beneficiario"
        '
        'txtMontoEsteCheque
        '
        Me.txtMontoEsteCheque.Location = New System.Drawing.Point(8, 72)
        Me.txtMontoEsteCheque.Name = "txtMontoEsteCheque"
        Me.txtMontoEsteCheque.ReadOnly = True
        Me.txtMontoEsteCheque.Size = New System.Drawing.Size(104, 20)
        Me.txtMontoEsteCheque.TabIndex = 13
        Me.txtMontoEsteCheque.Tag = "Monto del cheque"
        Me.txtMontoEsteCheque.Text = "0.00"
        Me.txtMontoEsteCheque.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 56)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Este Cheque"
        '
        'txtSaldoAnterior
        '
        Me.txtSaldoAnterior.Location = New System.Drawing.Point(8, 32)
        Me.txtSaldoAnterior.Name = "txtSaldoAnterior"
        Me.txtSaldoAnterior.ReadOnly = True
        Me.txtSaldoAnterior.Size = New System.Drawing.Size(104, 20)
        Me.txtSaldoAnterior.TabIndex = 11
        Me.txtSaldoAnterior.Tag = "Saldo anterior de la cuenta"
        Me.txtSaldoAnterior.Text = "0.00"
        Me.txtSaldoAnterior.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(87, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Saldo Anterior"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtDescCheque)
        Me.GroupBox4.Controls.Add(Me.txtInstFinanciera)
        Me.GroupBox4.Controls.Add(Me.txtValorLetras)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.txtMonto)
        Me.GroupBox4.Controls.Add(Me.txtSignoMoneda)
        Me.GroupBox4.Controls.Add(Me.txtNombreBeneficiario)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.txtFechaElavoracion)
        Me.GroupBox4.Controls.Add(Me.txtNumeroDoc)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(132, 192)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(480, 144)
        Me.GroupBox4.TabIndex = 24
        Me.GroupBox4.TabStop = False
        '
        'txtDescCheque
        '
        Me.txtDescCheque.Location = New System.Drawing.Point(8, 112)
        Me.txtDescCheque.MaxLength = 80
        Me.txtDescCheque.Name = "txtDescCheque"
        Me.txtDescCheque.ReadOnly = True
        Me.txtDescCheque.Size = New System.Drawing.Size(464, 20)
        Me.txtDescCheque.TabIndex = 22
        Me.txtDescCheque.Tag = "Descripción del cheque"
        '
        'txtInstFinanciera
        '
        Me.txtInstFinanciera.Location = New System.Drawing.Point(8, 88)
        Me.txtInstFinanciera.MaxLength = 60
        Me.txtInstFinanciera.Name = "txtInstFinanciera"
        Me.txtInstFinanciera.ReadOnly = True
        Me.txtInstFinanciera.Size = New System.Drawing.Size(464, 20)
        Me.txtInstFinanciera.TabIndex = 21
        Me.txtInstFinanciera.Tag = "Nombre de la institución financiera"
        '
        'txtValorLetras
        '
        Me.txtValorLetras.Location = New System.Drawing.Point(80, 64)
        Me.txtValorLetras.MaxLength = 300
        Me.txtValorLetras.Name = "txtValorLetras"
        Me.txtValorLetras.ReadOnly = True
        Me.txtValorLetras.Size = New System.Drawing.Size(392, 20)
        Me.txtValorLetras.TabIndex = 20
        Me.txtValorLetras.Tag = "Valor del cheque en letras"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 64)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 13)
        Me.Label15.TabIndex = 19
        Me.Label15.Text = "La suma de"
        '
        'txtMonto
        '
        Me.txtMonto.Location = New System.Drawing.Point(384, 40)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.ReadOnly = True
        Me.txtMonto.Size = New System.Drawing.Size(88, 20)
        Me.txtMonto.TabIndex = 18
        Me.txtMonto.Tag = "Monto del cheque"
        Me.txtMonto.Text = "0.00"
        Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSignoMoneda
        '
        Me.txtSignoMoneda.Location = New System.Drawing.Point(352, 40)
        Me.txtSignoMoneda.Name = "txtSignoMoneda"
        Me.txtSignoMoneda.ReadOnly = True
        Me.txtSignoMoneda.Size = New System.Drawing.Size(32, 20)
        Me.txtSignoMoneda.TabIndex = 17
        Me.txtSignoMoneda.Tag = "Signo de la moneda de la cuenta"
        '
        'txtNombreBeneficiario
        '
        Me.txtNombreBeneficiario.Location = New System.Drawing.Point(80, 40)
        Me.txtNombreBeneficiario.MaxLength = 60
        Me.txtNombreBeneficiario.Name = "txtNombreBeneficiario"
        Me.txtNombreBeneficiario.ReadOnly = True
        Me.txtNombreBeneficiario.Size = New System.Drawing.Size(264, 20)
        Me.txtNombreBeneficiario.TabIndex = 16
        Me.txtNombreBeneficiario.Tag = "Nombre del proveedor/beneficiario"
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(8, 32)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 24)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Pagase a o a su Orden"
        '
        'txtFechaElavoracion
        '
        Me.txtFechaElavoracion.Location = New System.Drawing.Point(384, 16)
        Me.txtFechaElavoracion.Name = "txtFechaElavoracion"
        Me.txtFechaElavoracion.ReadOnly = True
        Me.txtFechaElavoracion.Size = New System.Drawing.Size(88, 20)
        Me.txtFechaElavoracion.TabIndex = 14
        Me.txtFechaElavoracion.Tag = "Fecha de elaboración"
        '
        'txtNumeroDoc
        '
        Me.txtNumeroDoc.Location = New System.Drawing.Point(304, 16)
        Me.txtNumeroDoc.Name = "txtNumeroDoc"
        Me.txtNumeroDoc.Size = New System.Drawing.Size(72, 20)
        Me.txtNumeroDoc.TabIndex = 13
        Me.txtNumeroDoc.Tag = "Número de documento"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(208, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(97, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Cheque Número"
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(8, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(192, 16)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "NOMBRE DE LA EMPRESA"
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 387)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(622, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 25
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Name = "StatusBarPanel1"
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Name = "StatusBarPanel2"
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 500
        '
        'cmdGuardar
        '
        Me.cmdGuardar.Location = New System.Drawing.Point(535, 342)
        Me.cmdGuardar.Name = "cmdGuardar"
        Me.cmdGuardar.Size = New System.Drawing.Size(75, 23)
        Me.cmdGuardar.TabIndex = 26
        Me.cmdGuardar.Text = "Guardar"
        Me.cmdGuardar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(436, 342)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(80, 23)
        Me.btnSalir.TabIndex = 27
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmInsertarCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(622, 411)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.cmdGuardar)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmInsertarCheque"
        Me.Text = "Insertar cheques"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtMonedaCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNombInstFinanciera As System.Windows.Forms.TextBox
    Friend WithEvents txtCodCheque As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNumTransaccion As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDescripcionCheque As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtMontoCheque As System.Windows.Forms.TextBox
    Friend WithEvents txtSignoMonedaCuenta As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtNomBeneficiario As System.Windows.Forms.TextBox
    Friend WithEvents txtCodBeneficiario As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSaldoActual As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox22 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextBox23 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox24 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents TextBox25 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox26 As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtMontoEsteCheque As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtSaldoAnterior As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDescCheque As System.Windows.Forms.TextBox
    Friend WithEvents txtInstFinanciera As System.Windows.Forms.TextBox
    Friend WithEvents txtValorLetras As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents txtSignoMoneda As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreBeneficiario As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtFechaElavoracion As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents cmdGuardar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class
