Imports System.IO
Imports System.Web.UI.WebControls
Imports Infragistics.Win
Imports System.Configuration
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.ReglaNegocio

Public Class frmExportarCompras
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdProcesar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents utcAgencia As Infragistics.Win.UltraWinGrid.UltraCombo
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Appearance4 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance1 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance2 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance3 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance12 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance7 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance6 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance5 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance9 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance11 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance10 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim Appearance8 As Infragistics.Win.Appearance = New Infragistics.Win.Appearance
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportarCompras))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.utcAgencia = New Infragistics.Win.UltraWinGrid.UltraCombo
        Me.Label2 = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdProcesar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        CType(Me.utcAgencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.utcAgencia)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ProgressBar1)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 76)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(443, 133)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturas a Exportar"
        '
        'utcAgencia
        '
        Appearance4.BackColor = System.Drawing.SystemColors.Window
        Appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption
        Appearance4.TextHAlignAsString = "Left"
        Appearance4.TextVAlignAsString = "Middle"
        Me.utcAgencia.DisplayLayout.Appearance = Appearance4
        Me.utcAgencia.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns
        Me.utcAgencia.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Me.utcAgencia.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.[False]
        Appearance1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Appearance1.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance1.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical
        Appearance1.BorderColor = System.Drawing.SystemColors.Window
        Me.utcAgencia.DisplayLayout.GroupByBox.Appearance = Appearance1
        Appearance2.ForeColor = System.Drawing.SystemColors.GrayText
        Me.utcAgencia.DisplayLayout.GroupByBox.BandLabelAppearance = Appearance2
        Me.utcAgencia.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid
        Appearance3.BackColor = System.Drawing.SystemColors.ControlLightLight
        Appearance3.BackColor2 = System.Drawing.SystemColors.Control
        Appearance3.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance3.ForeColor = System.Drawing.SystemColors.GrayText
        Me.utcAgencia.DisplayLayout.GroupByBox.PromptAppearance = Appearance3
        Me.utcAgencia.DisplayLayout.MaxColScrollRegions = 1
        Me.utcAgencia.DisplayLayout.MaxRowScrollRegions = 1
        Appearance12.BackColor = System.Drawing.SystemColors.Window
        Appearance12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.utcAgencia.DisplayLayout.Override.ActiveCellAppearance = Appearance12
        Appearance7.BackColor = System.Drawing.SystemColors.Highlight
        Appearance7.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.utcAgencia.DisplayLayout.Override.ActiveRowAppearance = Appearance7
        Me.utcAgencia.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted
        Me.utcAgencia.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted
        Appearance6.BackColor = System.Drawing.SystemColors.Window
        Me.utcAgencia.DisplayLayout.Override.CardAreaAppearance = Appearance6
        Appearance5.BorderColor = System.Drawing.Color.Silver
        Appearance5.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter
        Me.utcAgencia.DisplayLayout.Override.CellAppearance = Appearance5
        Me.utcAgencia.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText
        Me.utcAgencia.DisplayLayout.Override.CellPadding = 0
        Appearance9.BackColor = System.Drawing.SystemColors.Control
        Appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark
        Appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element
        Appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal
        Appearance9.BorderColor = System.Drawing.SystemColors.Window
        Me.utcAgencia.DisplayLayout.Override.GroupByRowAppearance = Appearance9
        Appearance11.TextHAlignAsString = "Left"
        Me.utcAgencia.DisplayLayout.Override.HeaderAppearance = Appearance11
        Me.utcAgencia.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti
        Me.utcAgencia.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand
        Appearance10.BackColor = System.Drawing.SystemColors.Window
        Appearance10.BorderColor = System.Drawing.Color.Silver
        Me.utcAgencia.DisplayLayout.Override.RowAppearance = Appearance10
        Me.utcAgencia.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.[False]
        Appearance8.BackColor = System.Drawing.SystemColors.ControlLight
        Me.utcAgencia.DisplayLayout.Override.TemplateAddRowAppearance = Appearance8
        Me.utcAgencia.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill
        Me.utcAgencia.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate
        Me.utcAgencia.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
        Me.utcAgencia.Location = New System.Drawing.Point(88, 55)
        Me.utcAgencia.Name = "utcAgencia"
        Me.utcAgencia.Size = New System.Drawing.Size(343, 22)
        Me.utcAgencia.TabIndex = 29
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Agencia"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(8, 88)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(423, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(88, 29)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(120, 20)
        Me.DateTimePicker1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha Inicial"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.cmdProcesar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(443, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 36
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdProcesar
        '
        Me.cmdProcesar.Image = CType(resources.GetObject("cmdProcesar.Image"), System.Drawing.Image)
        Me.cmdProcesar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdProcesar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdProcesar.Name = "cmdProcesar"
        Me.cmdProcesar.Text = "Procesar<F3>"
        Me.cmdProcesar.Tooltip = "Procesar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(305, 28)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(122, 20)
        Me.DateTimePicker2.TabIndex = 31
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(227, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Fecha Final"
        '
        'frmExportarCompras
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(443, 219)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExportarCompras"
        Me.Text = "frmExportarCompras"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.utcAgencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmExportarCompras"
    Private Sub frmExportarCompras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Try
            DateTimePicker1.Value = Now
            UbicarAgencia(lngRegUsuario)
            ObtieneAgencias()
            utcAgencia.Value = lngRegAgencia

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub frmExportarCompras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                Procesar()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

    End Sub

    Sub ObtieneAgencias()
        Dim IndicaObtieneRegistro As Integer = 0
        Dim table As DataTable = Nothing
        Try
            utcAgencia.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            table = New DataTable("Agencias")
            table.Columns.Add("Codigo")
            table.Columns.Add("Descripcion")

            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            'ddlAgencia.SelectedItem = -1
            'utcAgencia.SelectedRow.Selected = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "select a.registro,a.codigo,a.Descripcion from prm_UsuariosAgencias u,prm_agencias a  Where(u.agenregistro = a.registro)  and u.usrregistro = " & lngRegUsuario & ""
            If strQuery.Trim.Length > 0 Then
                'ddlAgencia.Items.Clear()
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    table.Rows.Add(New Object() {dtrAgro2K.GetValue(0), dtrAgro2K.GetValue(2)})
                    IndicaObtieneRegistro = 1
                End While
                utcAgencia.DisplayMember = "Descripcion"
                utcAgencia.ValueMember = "codigo"
                utcAgencia.DataSource = table
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub
    Function ObtieneCodigoAgencias(ByVal lnRegistro As Int16) As Integer
        Dim IndicaObtieneRegistro As Integer = 0
        Try
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "exec ObtieneCodigoAgencia " & lnRegistro & ""
            ObtieneCodigoAgencias = 0
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ObtieneCodigoAgencias = dtrAgro2K.GetValue(0)
                IndicaObtieneRegistro = 1
            End While
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            ' MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Return ObtieneCodigoAgencias
    End Function
    Sub Procesar()
        Dim intCantidad As Integer = 0
        Dim intContador As Integer = 0
        Dim intNumFecha As Integer = 0
        Dim intNumFechaFinal As Integer = 0
        Dim strArchivo As String = String.Empty
        Dim strArchivo2 As String = String.Empty
        Dim strClave As String = String.Empty
        Dim lsRutaCompletaArchivoZip As String = String.Empty
        Dim lnRegistro As Integer = 0
        Dim lnCodigoAgencia As Integer = 0

        Me.Cursor = Cursors.WaitCursor
        Try
            lnRegistro = ConvierteAInt(utcAgencia.SelectedRow.Cells.Item("codigo").Value)
            lnCodigoAgencia = ObtieneCodigoAgencias(lnRegistro)

            'If ddlAgencia.SelectedItem = "" Then
            If utcAgencia.SelectedRow.Cells.Item("codigo").Text = "" Then

                utcAgencia.Focus()
                MsgBox("Tiene que escoger la agencia a exportar.  Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            ProgressBar1.Focus()
            intCantidad = 0
            intContador = 0
            intNumFecha = 0
            intNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
            intNumFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            'strQuery = "exec VerificaRegistrosAExportarCompras " & intNumFecha & "," & lngAgenRegExp
            strQuery = "exec VerificaRegistrosAExportarCompras " & intNumFecha & "," & intNumFechaFinal & "," & lnRegistro
            'strQuery = strQuery + "e.numfechaing = " & intNumFecha & " and e.agenregistro = " & lngAgenRegExp & ""
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                    intCantidad = dtrAgro2K.GetValue(0)
                End If
            End While
            dtrAgro2K.Close()
            strNombreArchivo = ""
            If intCantidad <> 0 Then
                ProgressBar1.Minimum = 0
                ProgressBar1.Maximum = intCantidad
                ProgressBar1.Value = 0
                strQuery = ""
                strQuery = " exec ConsultaExportarCompra " & intNumFecha & "," & intNumFechaFinal & "," & lnRegistro

                cmdAgro2K.CommandText = strQuery
                strClave = ""
                strClave = "Agro2K_2008"
                strArchivo = ""

                strArchivo = ConfigurationManager.AppSettings("RutaArchivosCompra").ToString() & "cpras_" & Format(lnCodigoAgencia, "00") & "_" & Format(DateTimePicker1.Value, "yyyyMMdd") & "_" & Format(DateTimePicker2.Value, "yyyyMMdd") & ".txt"
                strArchivo2 = ConfigurationManager.AppSettings("RutaArchivosCompra").ToString() & "cpras_" & Format(lnCodigoAgencia, "00") & "_" & Format(DateTimePicker1.Value, "yyyyMMdd") & "_" & Format(DateTimePicker2.Value, "yyyyMMdd")
                Dim sr As New System.IO.StreamWriter(ConfigurationManager.AppSettings("RutaArchivosCompra").ToString() & "cpras_" & Format(lnCodigoAgencia, "00") & "_" & Format(DateTimePicker1.Value, "yyyyMMdd") & "_" & Format(DateTimePicker2.Value, "yyyyMMdd") & ".txt")

                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ProgressBar1.Value = ProgressBar1.Value + 1
                    sr.WriteLine(dtrAgro2K.GetValue(0) & "|" & Trim(dtrAgro2K.GetValue(1)) & "|" & dtrAgro2K.GetValue(2) & "|" & Trim(dtrAgro2K.GetValue(3)) & "|" & Trim(dtrAgro2K.GetValue(4)) & "|" & Trim(dtrAgro2K.GetValue(5)) & "|" & Trim(dtrAgro2K.GetValue(6)) & "|" & dtrAgro2K.GetValue(7) & "|" & dtrAgro2K.GetValue(8) & "|" & dtrAgro2K.GetValue(9) & "|" & dtrAgro2K.GetValue(10) & "|" & Trim(dtrAgro2K.GetValue(11)) & "|" & dtrAgro2K.GetValue(12) & "|" & dtrAgro2K.GetValue(13) & "|" & dtrAgro2K.GetValue(14) & "|" & dtrAgro2K.GetValue(15) & "|" & dtrAgro2K.GetValue(16) & "|" & dtrAgro2K.GetValue(17) & "|" & Trim(dtrAgro2K.GetValue(18)) & "|" & Trim(dtrAgro2K.GetValue(19)) & "|" & dtrAgro2K.GetValue(20) & "|" & dtrAgro2K.GetValue(21) & "|" & dtrAgro2K.GetValue(22) & "|" & dtrAgro2K.GetValue(23) & "|" & dtrAgro2K.GetValue(24) & "|" & dtrAgro2K.GetValue(25))
                End While
                sr.Close()
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                intIncr = 0
                Try
                    lsRutaCompletaArchivoZip = String.Empty
                    lsRutaCompletaArchivoZip = strArchivo2 & ".zip"
                    If Not SUFunciones.GeneraArchivoZip(strArchivo, lsRutaCompletaArchivoZip) Then
                        MsgBox("No se pudo generar el archivo, favor validar", MsgBoxStyle.Critical, "Exportar Datos")
                        Exit Sub
                    End If
                    'intIncr = Shell(strAppPath + "\winrar.exe a -afzip -ep """ & strArchivo2 & """.zip """ & strArchivo & """ ", AppWinStyle.NormalFocus, True)

                    If File.Exists(strArchivo) = True Then
                        File.Delete(strArchivo)
                    End If
                Catch exx As Exception
                    MsgBox(exx.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End Try
            Else
                MsgBox("No hay compras que exportar para el d�a solicitado.", MsgBoxStyle.Information, "Error en Solicitud")
                cmdAgro2K.Connection.Close()
                cnnAgro2K.Close()
                ProgressBar1.Value = 0
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            MsgBox("Finalizo el proceso de exportaci�n de las compras", MsgBoxStyle.Information, "Proceso Finalizado")
            ProgressBar1.Value = 0
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " & ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub utcAgencia_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles utcAgencia.InitializeLayout
5:
    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        Procesar()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
