Imports System
Imports DataDynamics.ActiveReports
Imports DataDynamics.ActiveReports.Document 

Public Class actRptSaldosGeneralesDetallados 
    Inherits DataDynamics.ActiveReports.ActiveReport

    Private Sub actRptSaldosGeneralesDetallados_ReportStart(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportStart

        Me.PageSettings.PaperHeight = 11.0F
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom
        Me.PageSettings.PaperWidth = 8.5F
        Label.Text = strNombEmpresa
        'If intRptCtasCbr = 7 Then
        '    Label1.Text = "An�lisis de Antiguedad de Saldos General Detallado"
        'ElseIf intRptCtasCbr = 9 Then
        '    Label1.Text = "An�lisis de Antiguedad de Saldos de un Cliente"
        'End If
        TextBox14.Text = Format(Now, "Long Date")
        TextBox15.Text = Format(Now, "Long Time")
        Label2.Text = "Fecha: " & Format(Now, "dd-MMM-yyyy")

        Me.Document.Printer.PrinterName = ""
    End Sub

    Private Sub actRptSaldosGeneralesDetallados_ReportEnd(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.ReportEnd
        Dim html As New DataDynamics.ActiveReports.Export.Html.HtmlExport
        Dim pdf As New DataDynamics.ActiveReports.Export.Pdf.PdfExport
        Dim rtf As New DataDynamics.ActiveReports.Export.Rtf.RtfExport
        Dim tiff As New DataDynamics.ActiveReports.Export.Tiff.TiffExport
        Dim excel As New DataDynamics.ActiveReports.Export.Xls.XlsExport

        Select Case intRptExportar
            Case 1 : excel.Export(Me.Document, strNombreArchivo)
            Case 2 : html.Export(Me.Document, strNombreArchivo)
            Case 3 : pdf.Export(Me.Document, strNombreArchivo)
            Case 4 : rtf.Export(Me.Document, strNombreArchivo)
            Case 5 : tiff.Export(Me.Document, strNombreArchivo)
        End Select
    End Sub
End Class
