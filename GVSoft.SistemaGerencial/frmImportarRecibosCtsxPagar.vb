Imports System.IO
Imports System.Data.SqlClient

Public Class frmImportarRecibosCtsxPagar

    Dim bytImpuesto As Byte
    Dim strNombArchivo As String
    Dim strNombAlmacenar As String

    Private Sub frmImportarRecibosCtsxPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtCliente.ReadOnly = True
        txtNombreCliente.ReadOnly = True
        txtMontoRecibo.ReadOnly = True
        txtMontoRecibo.Text = ""

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If

        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        
    End Sub

    Private Sub frmImportarRecibosCtsxPagar_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            ObtenerArchivo()
        ElseIf e.KeyCode = Keys.F4 Then
            ProcesarArchivo()
        ElseIf e.KeyCode = Keys.F5 Then
            intListadoAyuda = 2
            Dim frmNew As New frmListadoAyuda
            Timer1.Interval = 200
            Timer1.Enabled = True
            frmNew.ShowDialog()
        End If
    End Sub

    Private Sub cmdSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalir.Click
        Me.Close()
    End Sub

    Private Sub cmdImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImportar.Click
        ObtenerArchivo()
    End Sub

    Private Sub cmdProcesar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProcesar.Click
        ProcesarArchivo()
    End Sub

    Sub Limpiar()

        txtMensajeError.Text = ""
        txtMontoRecibo.Text = ""
        dgvxDetalleRecibo.Rows.Clear()

    End Sub

    Sub ObtenerArchivo()
        Dim dlgNew As New OpenFileDialog
        Dim strLine, strDrive As String
        Dim lngRegistro As Long
        Dim strRecibos As String
        Dim intFechaIng As Integer
        Dim strFechaIng As String
        Dim strVendCodigo As String
        Dim strProvCodigo As String
        Dim strProvNombre As String
        Dim dblImpuesto As Double
        Dim dblRetencion As Double
        Dim dblTotal As Double
        Dim strUsrCodigo As String
        Dim intRegDetalle As Integer
        Dim strArchivo, strArchivo2, strClave, strComando As String
        Dim sNumeroChequeTarjeta As String
        Dim strFormaPago As String
        Dim sNombreBanco As String
        Dim sReciboProv As String
        Dim sDescripcion, sDescripcion1, sDescripcion2, sDescripcion3 As String
        Dim nTipoRecibo As Integer
        Dim sFechaIngreso As String
        Dim nSaldoAfavor As Double
        Dim sEstado As String
        Dim sCodigoAgencia As String
        Dim sNumeroFactura As String
        Dim sDescripcionDetalle As String
        Dim nMonto, nInteres, nMantenimientoAlValor As Double
        Dim sPendiente As String
        Dim sFechaAnulacion As String
        Dim sHoraAnulacion As String
        Dim nPorcDescuento, nDescuento As Double
        Dim Columnas As String()

        Dim existe, er As Integer
        Dim strQueryVal As String = ""

        dlgNew.InitialDirectory = "C:\"
        dlgNew.Filter = "ZIP files (*.zip)|*.zip"
        dlgNew.RestoreDirectory = True
        If dlgNew.ShowDialog() <> DialogResult.OK Then
            MsgBox("No hay archivo que importar. Verifique", MsgBoxStyle.Critical, "Error en la Importación")
            Exit Sub
        End If
        strDrive = String.Empty
        strComando = String.Empty
        strArchivo = String.Empty
        strArchivo2 = String.Empty
        strClave = String.Empty
        intIncr = 0
        intIncr = InStr(dlgNew.FileName, "recibosCtasXPagar_")
        strArchivo2 = Microsoft.VisualBasic.Mid(dlgNew.FileName, 1, Len(dlgNew.FileName) - 4) & ".txt"
        If intIncr = 0 Then
            MsgBox("No es un archivo que contiene las Recibos de una agencia. Verifique.", MsgBoxStyle.Critical, "Error en Archivo")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        cbRecibos.Items.Clear()
        ComboBox2.Items.Clear()
        strArchivo = dlgNew.FileName
        strClave = "Agro2K_2008"
        intIncr = 0
        intIncr = InStrRev(dlgNew.FileName, "\")
        strDrive = ""
        strDrive = Microsoft.VisualBasic.Left(dlgNew.FileName, intIncr)
        strNombAlmacenar = ""
        strNombAlmacenar = Microsoft.VisualBasic.Right(dlgNew.FileName, Len(dlgNew.FileName) - intIncr)
        intIncr = 0
        'intIncr = Shell(strAppPath + "\winrar.exe e -p""" & strClave & """ """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
        intIncr = Shell(strAppPath + "\winrar.exe e """ & strArchivo & """ *.txt """ & strDrive & """", vbNormalFocus, True)
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "truncate table tmpImportarRecibosCtasxPagar"
        cmdAgro2K.CommandText = strQuery
        cmdAgro2K.ExecuteNonQuery()
        strNombArchivo = ""
        strNombArchivo = strArchivo2
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        intIncr = 0
        intIncr = InStrRev(strNombArchivo, "\")
        strNombArchivo = Microsoft.VisualBasic.Right(strNombArchivo, Len(strNombArchivo) - intIncr)
        strQuery = ""
        strQuery = "select * from detArchivosRecibosImportados where archivo = '" & strNombAlmacenar & "'"
        cmdAgro2K.CommandText = strQuery
        Try
            intError = 0
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                intError = 1
            End While
            dtrAgro2K.Close()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        If intError = 1 Then
            MsgBox("Archivo ya fue procesado.  Verifique.", MsgBoxStyle.Critical, "Error de Archivo")
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim sr As StreamReader = File.OpenText(strArchivo2)
        intIncr = 0
        intError = 0
        intRegDetalle = 0

        er = 1  ' con uno indica que los Recibos del archivo que se importa ya existen, su valor cambia si al menos un Recibos no existe
        Do While sr.Peek() >= 0
            strLine = sr.ReadLine()
            Columnas = strLine.Trim().Split("|")
            lngRegistro = 0
            lngRegistro = ConvierteADouble(Columnas(0).ToString())
            strFechaIng = String.Empty
            strFechaIng = Columnas(1).ToString()
            intFechaIng = 0
            intFechaIng = ConvierteAInt(Columnas(2).ToString())
            strProvCodigo = String.Empty
            strProvCodigo = Columnas(3).ToString()
            strProvNombre = Columnas(4).ToString()
            strVendCodigo = String.Empty
            strVendCodigo = Columnas(5).ToString()
            strRecibos = String.Empty
            strRecibos = Columnas(6).ToString()
            dblTotal = 0
            dblTotal = ConvierteADouble(Columnas(7).ToString())
            dblImpuesto = 0
            dblImpuesto = ConvierteADouble(Columnas(8).ToString())
            dblRetencion = 0
            dblRetencion = ConvierteADouble(Columnas(9).ToString())
            strFormaPago = String.Empty
            strFormaPago = Columnas(10).ToString()
            sNumeroChequeTarjeta = String.Empty
            sNumeroChequeTarjeta = Columnas(11).ToString()
            sNombreBanco = String.Empty
            sNombreBanco = Columnas(12).ToString()
            sReciboProv = Columnas(13).ToString()
            sDescripcion = Columnas(14).ToString()
            strUsrCodigo = Columnas(15).ToString()
            nTipoRecibo = ConvierteAInt(Columnas(16).ToString())
            sFechaIngreso = ConvierteAInt(Columnas(17).ToString())
            nSaldoAfavor = ConvierteADouble(ConvierteAInt(Columnas(18).ToString()))
            sEstado = Columnas(19).ToString()
            sDescripcion1 = Columnas(20).ToString()
            sDescripcion2 = Columnas(21).ToString()
            sDescripcion3 = Columnas(22).ToString()
            sCodigoAgencia = Columnas(23).ToString()
            intRegDetalle = ConvierteAInt(Columnas(24).ToString())
            sNumeroFactura = Columnas(25).ToString()
            sDescripcionDetalle = Columnas(26).ToString()
            nMonto = ConvierteADouble(Columnas(27).ToString())
            nInteres = ConvierteADouble(Columnas(28).ToString())
            nMantenimientoAlValor = ConvierteADouble(Columnas(29).ToString())
            sPendiente = ConvierteADouble(Columnas(30).ToString())
            sFechaAnulacion = Columnas(31).ToString()
            sHoraAnulacion = Columnas(32).ToString()
            nPorcDescuento = Columnas(33).ToString()
            nDescuento = Columnas(34).ToString()

            strQueryVal = "exec spValidaRecibosProveedorImportar '" & strRecibos & "', " & sCodigoAgencia & ", '" & strProvCodigo & "'"

            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If

            cnnAgro2K.Open()
            Try
                cmdAgro2K.CommandText = strQueryVal
                dtrAgro2K = cmdAgro2K.ExecuteReader()
                While dtrAgro2K.Read
                    existe = dtrAgro2K.GetValue(0)
                End While
                dtrAgro2K.Close()

            Catch exc As Exception
                intError = 1
                MsgBox(exc.Message.ToString)
                Me.Cursor = Cursors.Default
                Exit Do
            End Try

            If existe = 0 Then  'Cero indica que los recibos no existe en el sitema y por tanto se ingresa, 1 indica que existe y no se ingresa
                er = existe
                strQuery = ""
                'strQuery = "insert into tmp_facturasimp values (" & lngRegistro & ", '" & strRecibos & "', "
                strQuery = "exec InresaImportaReciboCtasXPagar " & lngRegistro & ", '" & strFechaIng & "', "
                strQuery = strQuery + "" & intFechaIng & ", '" & strProvCodigo & "', '" & strProvNombre & "', '" & strVendCodigo & "', "
                strQuery = strQuery + "'" & strRecibos & "', " & dblTotal & ", " & dblImpuesto & ", "
                strQuery = strQuery + "" & dblRetencion & ", "
                strQuery = strQuery + "'" & strFormaPago & "', ' " & sNumeroChequeTarjeta & " ', '" & sNombreBanco & " ', "
                strQuery = strQuery + "'" & sReciboProv & "', '" & sDescripcion & "', '" & strUsrCodigo & "', "
                strQuery = strQuery + "" & nTipoRecibo & ", '" & sFechaIngreso & "', "
                strQuery = strQuery + "" & nSaldoAfavor & ", '" & sEstado & "', '" & sDescripcion1 & " ','" & sDescripcion2 & " ', "
                strQuery = strQuery + "'" & sDescripcion3 & "','" & sCodigoAgencia & "', " & intRegDetalle & ", '" & sNumeroFactura & "', "
                strQuery = strQuery + "'" & sDescripcionDetalle & "'," & nMonto & "," & nInteres & "," & nMantenimientoAlValor & ",'" & sFechaAnulacion & " ','" & sHoraAnulacion & " ', " & sPendiente.Trim() & ", "
                strQuery = strQuery + "" & nPorcDescuento & ", " & nDescuento & ",'Sin Error',0"
                Try
                    cmdAgro2K.CommandText = strQuery
                    cmdAgro2K.ExecuteNonQuery()
                Catch exc As Exception
                    intError = 1
                    MsgBox(exc.Message.ToString)
                    Me.Cursor = Cursors.Default
                    Exit Do
                End Try
            End If
        Loop
        sr.Close()
        ValidarDatosImportados()
        CargarRecibos()
        CargarResumen()
        If er = 0 Then 'Condicionamos para indicarle al husuario si ya ha ingresado las Facturas de compras
            If intError = 0 Then
                'ValidarDatosImportados()
                'CargarResumen()
                'CargarRecibos()
                MsgBox("Finalizado satisfactoriamente la importación de los Recibos.", MsgBoxStyle.Information, "Proceso Finalizado")
            Else
                MsgBox("Hubieron errores al importar el archivo de Recibos.", MsgBoxStyle.Critical, "Error en la Importación")
            End If
        Else
            MsgBox("El archivo de Recibos ya fue Importado.", MsgBoxStyle.Critical, "Error en la Importación")
        End If
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If File.Exists(strArchivo2) = True Then
            File.Delete(strArchivo2)
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Sub ValidarDatosImportados()

        Dim cmdTmp As New SqlCommand("ImportarRecibosCtasxPagar", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter

        intError = 0
        With prmTmp01
            .ParameterName = "@intAccion"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                If ConvierteAInt(dtrAgro2K.GetValue(0)) = 0 Then
                    intError = 0
                ElseIf ConvierteAInt(dtrAgro2K.GetValue(0)) <> 254 Then
                    intError = 1
                End If
            End While

            dtrAgro2K.Close()

        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try

    End Sub

    Sub CargarResumen()

        Dim cmdTmp As New SqlCommand("ImportarRecibosCtasxPagar", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        With prmTmp01
            .ParameterName = "@intAccion"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 1
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                txtTotalCantidaRecibos.Text = Format(ConvierteADouble(dtrAgro2K.Item("Cantidad")), "##,##0")
                txtMontoTotalRecibos.Text = Format(ConvierteADouble(dtrAgro2K.Item("MontoTotal")), "##,##0.#0")
            End While
            dtrAgro2K.Close()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try

    End Sub

    Sub CargarRecibos()

        cbRecibos.Items.Clear()
        ComboBox2.Items.Clear()
        strQuery = ""
        strQuery = "exec ObtieneRecibosProveedorACargar"
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                cbRecibos.Items.Add(dtrAgro2K.GetValue(1))
                ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
            End While
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub cbRecibos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbRecibos.SelectedIndexChanged

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        Limpiar()
        ComboBox2.SelectedIndex = cbRecibos.SelectedIndex
        txtMensajeError.Text = "Sin Error"
        strQuery = ""
        strQuery = "exec ObtieneDetalleReciboProveedor " & ComboBox2.SelectedItem & ""
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtCliente.Text = dtrAgro2K.Item("CodigoProveedor")
                txtNombreCliente.Text = dtrAgro2K.Item("NombreProveedor")
                txtMontoRecibo.Text = Format(dtrAgro2K.Item("MontoTotal"), "##,##0.#0")

                dgvxDetalleRecibo.Rows.Add(dtrAgro2K.Item("NumeroFactura"), Format(dtrAgro2K.Item("monto"), "##,##0.#0"), dtrAgro2K.Item("FechaIngFormateada"))

                If dtrAgro2K.Item("IndicaError") Then
                    txtMensajeError.Text = dtrAgro2K.Item("registroError")
                End If
            End While
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    'Function UbicarProducto(ByVal prm01 As String) As Boolean

    '    Dim intEnc As Integer

    '    intEnc = 0
    '    strQuery = ""
    '    strQuery = "Select Codigo, Descripcion, Pvpc, Impuesto from prm_Productos Where Codigo = '" & prm01 & "'"
    '    If cnnAgro2K.State = ConnectionState.Open Then
    '        cnnAgro2K.Close()
    '    End If
    '    If cmdAgro2K.Connection.State = ConnectionState.Open Then
    '        cmdAgro2K.Connection.Close()
    '    End If
    '    cnnAgro2K.Open()
    '    cmdAgro2K.Connection = cnnAgro2K
    '    cmdAgro2K.CommandText = strQuery
    '    dtrAgro2K = cmdAgro2K.ExecuteReader
    '    While dtrAgro2K.Read
    '        dgvxDetalleRecibo.Rows.Add(dtrAgro2K.Item("Codigo"), dtrAgro2K.Item("Descripcion"), dtrAgro2K.Item("Pvpc"))
    '        'MSFlexGrid2.Col = 0
    '        'MSFlexGrid2.Text = dtrAgro2K.GetValue(0)
    '        'MSFlexGrid2.Col = 1
    '        'MSFlexGrid2.Text = dtrAgro2K.GetValue(1)
    '        'MSFlexGrid2.Col = 4
    '        'MSFlexGrid2.Text = dtrAgro2K.GetValue(2)
    '        bytImpuesto = dtrAgro2K.Item("Impuesto")
    '        intEnc = 1
    '    End While
    '    dtrAgro2K.Close()
    '    cmdAgro2K.Connection.Close()
    '    cnnAgro2K.Close()
    '    If intEnc = 0 Then
    '        UbicarProducto = False
    '        Exit Function
    '    End If
    '    UbicarProducto = True

    'End Function 

    Sub ProcesarArchivo()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("ImportarRecibosCtasxPagar", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim bytContinuar As Byte

        bytContinuar = 0
        With prmTmp01
            .ParameterName = "@intAccion"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            dtrAgro2K = cmdTmp.ExecuteReader
            While dtrAgro2K.Read
                If dtrAgro2K.GetValue(0) = 0 Then
                    MsgBox("No hay errores en la validación de las Recibos. Se procedera a ingresar las Recibos.", MsgBoxStyle.Information, "Validación de Datos")
                ElseIf dtrAgro2K.GetValue(0) <> 254 Then
                    bytContinuar = 1
                    MsgBox("Continuan los errores en la validación de las Recibos", MsgBoxStyle.Critical, "Validación de Datos")
                    Exit Sub
                End If
            End While
            dtrAgro2K.Close()
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
            dtrAgro2K.Close()
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Exit Sub
        End Try
        If bytContinuar = 0 Then
            cmdTmp.Parameters(0).Value = 3
            Try
                cmdTmp.ExecuteNonQuery()
            Catch exc As Exception
                MsgBox(exc.Message.ToString)
                dtrAgro2K.Close()
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            End Try
        End If
        'strQuery = ""
        'strQuery = "insert into tbl_ArchivosImp values ('" & strNombAlmacenar & "', "
        'strQuery = strQuery + "'" & Format(Now, "dd-MMM-yyyy") & "', " & Format(Now, "yyyyMMdd") & ", "
        'strQuery = strQuery + "" & lngRegUsuario & ")"
        'cmdAgro2K.CommandText = strQuery
        'Try
        '    cmdAgro2K.ExecuteNonQuery()
        'Catch exc As Exception
        '    MsgBox(exc.Message.ToString)
        'End Try
        'strQuery = ""
        'strQuery = "truncate table tmp_facturasimp"
        'cmdAgro2K.CommandText = strQuery
        'cmdAgro2K.ExecuteNonQuery()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        MsgBox("Finalizo el proceso de importar ventar de una agencia a la Casa Matriz.", MsgBoxStyle.Exclamation, "Proceso Finalizado")
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Close()

    End Sub

End Class