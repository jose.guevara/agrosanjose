Imports System.Data.SqlClient
Imports System.Text

Public Class frmRecibosNTD
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox14 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox3 As System.Windows.Forms.RichTextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdBuscar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Label23 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRecibosNTD))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TextBox15 = New System.Windows.Forms.TextBox
        Me.TextBox14 = New System.Windows.Forms.TextBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.RichTextBox3 = New System.Windows.Forms.RichTextBox
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.cmdBuscar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox15)
        Me.GroupBox1.Controls.Add(Me.TextBox14)
        Me.GroupBox1.Controls.Add(Me.TextBox10)
        Me.GroupBox1.Controls.Add(Me.TextBox9)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox8)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 75)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(680, 152)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Clientes"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(656, 88)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.ReadOnly = True
        Me.TextBox15.Size = New System.Drawing.Size(8, 20)
        Me.TextBox15.TabIndex = 36
        Me.TextBox15.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox15.Visible = False
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(640, 88)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.ReadOnly = True
        Me.TextBox14.Size = New System.Drawing.Size(8, 20)
        Me.TextBox14.TabIndex = 35
        Me.TextBox14.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox14.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.TextBox14.Visible = False
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(536, 120)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.ReadOnly = True
        Me.TextBox10.Size = New System.Drawing.Size(104, 20)
        Me.TextBox10.TabIndex = 8
        Me.TextBox10.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(424, 120)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(104, 20)
        Me.TextBox9.TabIndex = 7
        Me.TextBox9.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(304, 120)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(119, 13)
        Me.Label21.TabIndex = 34
        Me.Label21.Text = "Saldo Pendiente C$"
        '
        'Label17
        '
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(144, 24)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 24)
        Me.Label17.TabIndex = 33
        Me.Label17.Text = "<F5> AYUDA"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(344, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Negocio"
        Me.Label4.Visible = False
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(400, 88)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(208, 20)
        Me.TextBox5.TabIndex = 4
        Me.TextBox5.Tag = "Vendedor del Cliente"
        Me.TextBox5.Visible = False
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(176, 120)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(104, 20)
        Me.TextBox8.TabIndex = 14
        Me.TextBox8.Tag = "Saldo total del Cliente en Moneda Extranjera D�lares Americanos"
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(64, 120)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(104, 20)
        Me.TextBox7.TabIndex = 7
        Me.TextBox7.Tag = "Saldo total del Cliente en Moneda Nacional"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(400, 88)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(208, 20)
        Me.TextBox6.TabIndex = 12
        Me.TextBox6.Tag = "Vendedor del Cliente"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(96, 88)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(224, 20)
        Me.TextBox4.TabIndex = 3
        Me.TextBox4.Tag = "Departamento donde reside negocio/habitaci�n del cliente"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(72, 56)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(592, 20)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Tag = "Direcci�n del Cliente"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(144, 24)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(520, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Nombre del Cliente"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 24)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(80, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "C�digo del Cliente"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 120)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Saldo C$"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(336, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Vendedor"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Departamento"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Direcci�n"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'Timer1
        '
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.Text = "Clientes"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RichTextBox3)
        Me.GroupBox2.Controls.Add(Me.RichTextBox2)
        Me.GroupBox2.Controls.Add(Me.RichTextBox1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(0, 296)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(680, 144)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Descripciones y Conceptos"
        '
        'RichTextBox3
        '
        Me.RichTextBox3.Location = New System.Drawing.Point(560, 16)
        Me.RichTextBox3.Name = "RichTextBox3"
        Me.RichTextBox3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RichTextBox3.Size = New System.Drawing.Size(112, 120)
        Me.RichTextBox3.TabIndex = 30
        Me.RichTextBox3.Text = "RichTextBox3"
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Location = New System.Drawing.Point(448, 16)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RichTextBox2.Size = New System.Drawing.Size(112, 120)
        Me.RichTextBox2.TabIndex = 29
        Me.RichTextBox2.Text = "RichTextBox2"
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Location = New System.Drawing.Point(8, 16)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(440, 120)
        Me.RichTextBox1.TabIndex = 28
        Me.RichTextBox1.Text = "RichTextBox1"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.TextBox13)
        Me.GroupBox3.Controls.Add(Me.TextBox11)
        Me.GroupBox3.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox3.Controls.Add(Me.TextBox12)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(0, 228)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(680, 64)
        Me.GroupBox3.TabIndex = 39
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Nota de D�bito"
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(520, 40)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(64, 16)
        Me.Label23.TabIndex = 34
        Me.Label23.Visible = False
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(616, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 16)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Label7"
        Me.Label7.Visible = False
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(360, 40)
        Me.TextBox13.MaxLength = 7995
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox13.Size = New System.Drawing.Size(8, 20)
        Me.TextBox13.TabIndex = 19
        Me.TextBox13.TabStop = False
        Me.TextBox13.Tag = "Descripci�n/Concepto"
        Me.TextBox13.Visible = False
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(56, 24)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(88, 20)
        Me.TextBox11.TabIndex = 9
        Me.TextBox11.Tag = "Monto del Recibo a Elaborar"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = ""
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(216, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 10
        Me.DateTimePicker1.Tag = "Fecha del Recibo a Elaborar"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(392, 24)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(96, 20)
        Me.TextBox12.TabIndex = 11
        Me.TextBox12.Tag = "N�mero del Recibo a Elaborar"
        Me.TextBox12.Text = "0.00"
        Me.TextBox12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(352, 24)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(42, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Monto"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(168, 24)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 17
        Me.Label9.Text = "Fecha"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "N�mero"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 446)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(680, 23)
        Me.UltraStatusBar1.TabIndex = 40
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem4, Me.cmdBuscar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(680, 71)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 47
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        Me.cmdOK.Visible = False
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'cmdBuscar
        '
        Me.cmdBuscar.Image = CType(resources.GetObject("cmdBuscar.Image"), System.Drawing.Image)
        Me.cmdBuscar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdBuscar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdBuscar.Name = "cmdBuscar"
        Me.cmdBuscar.Text = "Buscar<F6>"
        Me.cmdBuscar.Tooltip = "Extraer datos del recibo"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "  "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmRecibosNTD
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(680, 469)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmRecibosNTD"
        Me.Text = "frmRecibosNTD"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.UltraStatusBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public txtCollection As New Collection

    Private Sub frmRecibosNTD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        dblMontoReciboNTDNTC = 0
        txtCollection.Add(TextBox1)
        txtCollection.Add(TextBox2)
        txtCollection.Add(TextBox3)
        txtCollection.Add(TextBox4)
        txtCollection.Add(TextBox5)
        txtCollection.Add(TextBox6)
        txtCollection.Add(TextBox7)
        txtCollection.Add(TextBox8)
        txtCollection.Add(TextBox9)
        txtCollection.Add(TextBox10)
        txtCollection.Add(TextBox11)
        txtCollection.Add(TextBox12)
        txtCollection.Add(TextBox13)
        txtCollection.Add(TextBox14)
        txtCollection.Add(TextBox15)
        If intTipoReciboCaja = 11 Then
            'Me.ToolBar1.Buttons.Item(0).Enabled = False
            cmdOK.Visible = False
        End If
        Limpiar()
        Label7.Text = intTipoReciboCaja
        For intIncr = 0 To 8
            strVistaPreviaNTCNTD(intIncr) = ""
        Next

    End Sub

    Private Sub frmRecibosNTD_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            intTipoReciboCaja = CInt(Label7.Text)
            If intTipoReciboCaja = 6 Then
                If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
                Else
                    MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                    Exit Sub
                End If
                Anular()
            ElseIf intTipoReciboCaja = 3 Then
                Guardar()
            End If
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        ElseIf e.KeyCode = Keys.F5 Then
            If Label17.Visible = True Then
                intListadoAyuda = 2
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        ElseIf e.KeyCode = Keys.F6 Then
            intTipoReciboCaja = CInt(Label7.Text)
            If intTipoReciboCaja <> 3 Then
                Extraer()
            End If
        ElseIf e.KeyCode = Keys.F7 Then
            intTipoReciboCaja = CInt(Label7.Text)
            If intTipoReciboCaja = 3 Then
                VistaPrevia()
            End If
        End If

    End Sub

    Sub VistaPrevia()

        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        Dim frmNew As New actrptViewer
        intRptImpRecibos = 11
        blnVistaPrevia = True
        strQuery = ""
        strVistaPreviaNTCNTD(0) = UCase(TextBox11.Text)
        strVistaPreviaNTCNTD(1) = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        strVistaPreviaNTCNTD(2) = UCase(TextBox2.Text)
        strVistaPreviaNTCNTD(3) = UCase(TextBox1.Text)
        strVistaPreviaNTCNTD(4) = UCase(TextBox3.Text)
        strVistaPreviaNTCNTD(5) = RichTextBox1.Text
        strVistaPreviaNTCNTD(6) = RichTextBox2.Text
        strVistaPreviaNTCNTD(7) = RichTextBox3.Text
        strVistaPreviaNTCNTD(8) = Label23.Text
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
        For intIncr = 0 To 8
            strVistaPreviaNTCNTD(intIncr) = ""
        Next

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem12.Click, MenuItem15.Click

        intListadoAyuda = 0
        intTipoReciboCaja = CInt(Label7.Text)
        Select Case sender.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Clientes" : intListadoAyuda = 2
            Case "&Salir" : Me.Close()
        End Select
        If intListadoAyuda <> 0 Then
            Dim frmNew As New frmListadoAyuda
            'Timer1.Interval = 200
            'Timer1.Enabled = True
            frmNew.ShowDialog()
        End If

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'intTipoReciboCaja = CInt(Label7.Text)
        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        If intTipoReciboCaja = 6 Then
        '            If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
        '            Else
        '                MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
        '                Exit Sub
        '            End If
        '            Anular()
        '        Else
        '            Guardar()
        '        End If
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        If intTipoReciboCaja = 3 Then
        '            VistaPrevia()
        '        Else
        '            Extraer()
        '        End If
        '    Case 4
        '        Me.Close()
        'End Select

    End Sub

    Sub Limpiar()

        For intIncr = 1 To 15
            txtCollection.Item(intIncr).text = ""
        Next
        TextBox7.Text = "0.00"
        TextBox8.Text = "0.00"
        TextBox9.Text = "0.00"
        TextBox10.Text = "0.00"
        TextBox12.Text = "0.00"
        TextBox14.Text = "0"
        TextBox15.Text = "0"
        If intTipoReciboCaja = 6 Then
            For intIncr = 1 To 15
                txtCollection.Item(intIncr).readonly = True
            Next
            TextBox1.ReadOnly = False
            TextBox11.ReadOnly = False
        End If
        RichTextBox1.Text = ""
        RichTextBox2.Text = ""
        RichTextBox3.Text = ""
        TextBox1.Focus()

    End Sub

    Sub Extraer()

        intTipoReciboCaja = CInt(Label7.Text)
        If TextBox14.Text = "0" Then
            MsgBox("Tiene que digitar el codigo del cliente.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox11.Text = "" Then
            MsgBox("Tiene que digitar el n�mero del recibo.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptOtros = 0
        intRptImpRecibos = 0
        If intTipoReciboCaja = 3 Or intTipoReciboCaja = 11 Then
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "select * from tbl_Recibos_Enc e, tbl_Recibos_Det d where "
            strQuery = strQuery + "e.registro = d.registro and e.CliRegistro = " & CInt(TextBox14.Text) & " "
            strQuery = strQuery + "And e.Numero = '" & UCase(TextBox11.Text) & "' And e.TipoRecibo = 3"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                Label23.Text = ""
                If dtrAgro2K.GetValue(18) = 1 Then
                    Label23.Text = "ANULADA"
                End If
            End While
            dtrAgro2K.Close()
            Dim frmNew As New actrptViewer
            intRptImpRecibos = intTipoReciboCaja
            strVistaPreviaNTCNTD(8) = Label23.Text
            strQuery = ""
            strQuery = "exec sp_ExtraerReciboRpt " & intRptImpRecibos & ", " & CInt(TextBox14.Text) & ", "
            strQuery = strQuery + "'" & UCase(TextBox11.Text) & "', 0"
            intRptImpRecibos = intTipoReciboCaja
            frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
            frmNew.Show()
        ElseIf intTipoReciboCaja = 6 Then
            TextBox12.Text = "0.00"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "select * from tbl_Recibos_Enc e, tbl_Recibos_Det d where "
            strQuery = strQuery + "e.registro = d.registro and e.CliRegistro = " & CInt(TextBox14.Text) & " "
            strQuery = strQuery + "And e.Numero = '" & UCase(TextBox11.Text) & "' And e.TipoRecibo = 3"
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                TextBox12.Text = Format(CDbl(dtrAgro2K.GetValue(6)), "#,##0.#0")
                DateTimePicker1.Value = dtrAgro2K.GetValue(16)
                RichTextBox1.Text = dtrAgro2K.GetValue(19)
                RichTextBox2.Text = dtrAgro2K.GetValue(20)
                RichTextBox3.Text = dtrAgro2K.GetValue(21)
                Label23.Text = ""
                If dtrAgro2K.GetValue(18) = 1 Then
                    Label23.Text = "ANULADA"
                End If
                If dtrAgro2K.GetValue(18) = 1 Then
                    dtrAgro2K.Close()
                    MsgBox("La Nota de D�bito ya fue anulada.", MsgBoxStyle.Information, "Recibo Anulado")
                    Limpiar()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End While
            dtrAgro2K.Close()
            If TextBox12.Text = "" Or TextBox12.Text = "0.00" Then
                MsgBox("El n�mero de la Nota de D�bito no es v�lido.  Verifique.", MsgBoxStyle.Critical, "Error de Datos")
                Exit Sub
            End If
        End If
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox11.GotFocus, TextBox12.GotFocus

        Dim strCampo As String

        MenuItem15.Visible = False
        strCampo = ""
        Select Case sender.name.ToString
            Case "TextBox1" : Label17.Visible = True : strCampo = "C�digo" : MenuItem11.Visible = True : MenuItem15.Visible = True
            Case "TextBox11" : strCampo = "Recibo"
            Case "TextBox12" : strCampo = "Monto"
            Case "TextBox13""Descripci�n"
        End Select
        'StatusBar1.Panels.Item(0).Text = strCampo
        UltraStatusBar1.Panels.Item(0).Text = strCampo
        'StatusBar1.Panels.Item(1).Text = sender.tag
        UltraStatusBar1.Panels.Item(1).Text = sender.tag
        sender.selectall()
        If blnUbicar Then
            ObtieneDatosListadoAyuda()
        End If
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox11.KeyDown, TextBox12.KeyDown

        If e.KeyCode = Keys.Enter Then
            intTipoReciboCaja = CInt(Label7.Text)
            Select Case sender.name.ToString
                Case "TextBox1"
                    If UbicarCliente(TextBox1.Text) = True Then
                        TextBox11.Focus()
                    End If
                Case "TextBox11"
                    TextBox12.Focus()
                    If intTipoReciboCaja = 3 Then
                        UbicarRecibo()
                    ElseIf intTipoReciboCaja <> 3 Then
                        Extraer()
                    End If
                Case "TextBox12"
                    If TextBox12.Text = "0.00" Or Len(TextBox12.Text) = 0 Then
                        MsgBox("Tiene que ingresar monto a debitarle al cliente.", MsgBoxStyle.Critical, "Error de Datos")
                        TextBox12.Focus()
                        Exit Sub
                    End If
                    If IsNumeric(TextBox12.Text) = False Then
                        MsgBox("Tiene que ingresar valor n�merico a debitarle al cliente.", MsgBoxStyle.Critical, "Error de Datos")
                        TextBox12.Focus()
                        Exit Sub
                    End If
                    dblMontoReciboNTDNTC = CDbl(TextBox12.Text)
                    TextBox12.Text = Format(CDbl(dblMontoReciboNTDNTC), "#,##0.#0")
                    RichTextBox1.Focus()
            End Select
        End If

    End Sub

    Sub UbicarRecibo()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "select Numero, Estado from tbl_Recibos_Enc E where "
        strQuery = strQuery + "Numero = '" & UCase(TextBox11.Text) & "' And E.TipoRecibo = 3"
        Try
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                If dtrAgro2K.GetValue(1) = 0 Then
                    MsgBox("La nota de d�bito ya fue emitida.", MsgBoxStyle.Critical, "Nota de D�bito Emitida")
                ElseIf dtrAgro2K.GetValue(1) = 1 Then
                    MsgBox("La nota de d�bito ya fue anulada.", MsgBoxStyle.Critical, "Nota de D�bito Anulada")
                End If
                TextBox11.Focus()
                TextBox11.Text = ""
                Exit While
            End While
        Catch exc As Exception
            MsgBox(exc.Message.ToString)
        End Try
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.LostFocus

        Label17.Visible = False

    End Sub

    Function UbicarCliente(ByVal strDato As String) As Boolean

        Dim strVendedor As String

        TextBox14.Text = "0"
        TextBox15.Text = "0"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "select * from prm_Clientes where codigo = '" & strDato & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                TextBox14.Text = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select C.Codigo, C.Nombre, C.Direccion, D.Descripcion, N.Descripcion, V.Nombre, C.Saldo, V.Codigo, "
        strQuery = strQuery + "C.Registro, C.SaldoFavor From prm_Clientes C, prm_Departamentos D, prm_Vendedores V, prm_Negocios N " ', tbl_Cliente_Oblig M "
        strQuery = strQuery + "Where C.Registro = " & CLng(TextBox14.Text) & " And D.registro = C.deptregistro And "
        strQuery = strQuery + "V.registro = C.vendregistro And N.registro = C.negregistro " 'And M.cliregistro = C.registro And "
        'strQuery = strQuery + "M.estado = 0"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        Limpiar()
        strVendedor = ""
        While dtrAgro2K.Read
            TextBox1.Text = dtrAgro2K.GetValue(0)
            TextBox2.Text = dtrAgro2K.GetValue(1)
            TextBox3.Text = dtrAgro2K.GetValue(2)
            TextBox4.Text = dtrAgro2K.GetValue(3)
            TextBox5.Text = dtrAgro2K.GetValue(4)
            TextBox6.Text = dtrAgro2K.GetValue(5)
            TextBox7.Text = Format(dtrAgro2K.GetValue(6), "#,##0.#0")
            TextBox8.Text = IIf(dtrAgro2K.GetValue(6) <> 0, Format(dtrAgro2K.GetValue(6) / dblTipoCambio, "#,##0.#0"), "0.00")
            strVendedor = dtrAgro2K.GetValue(7)
            TextBox14.Text = dtrAgro2K.GetValue(8)
            TextBox9.Text = Format(dtrAgro2K.GetValue(9), "#,##0.#0")
            TextBox10.Text = IIf(dtrAgro2K.GetValue(9) <> 0, Format(dtrAgro2K.GetValue(9) / dblTipoCambio, "#,##0.#0"), "0.00")
        End While
        dtrAgro2K.Close()
        If TextBox2.Text = "" Then
            UbicarCliente = False
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Function
        End If
        strQuery = "select * from prm_Vendedores where codigo = '" & strVendedor & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                TextBox15.Text = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        UbicarCliente = True

    End Function

    Sub Guardar()

        blnVistaPrevia = False
        intTipoReciboCaja = CInt(Label7.Text)
        If intTipoReciboCaja = 1 Then
            If TextBox11.Text <> "" Then
                UbicarRecibo()
            End If
        End If
        If TextBox14.Text = "0" Then
            MsgBox("Tiene que digitar el codigo del cliente.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox11.Text = "" Then
            MsgBox("Tiene que digitar el n�mero de la nota de d�dito.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox12.Text = "" Then
            MsgBox("Tiene que digitar el monto de la nota de d�dito.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If TextBox12.Text = "0.00" Then
            MsgBox("Tiene que digitar el monto de la nota de d�dito.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If IsNumeric(TextBox12.Text) = False Then
            MsgBox("El monto de la nota de d�dito debe ser numerico.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If CDbl(TextBox12.Text) <= 0 Then
            MsgBox("El monto de la nota de d�dito debe ser mayor que 0.", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        Dim cmdTmp As New SqlCommand("sp_IngRecibosEnc", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter
        Dim prmTmp18 As New SqlParameter
        Dim prmTmp19 As New SqlParameter
        Dim prmTmp20 As New SqlParameter
        Dim prmTmp21 As New SqlParameter

        If TextBox1.Text = "" Then
            MsgBox("Tiene que digitar el cliente a quien aplicar la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If Len(Trim(TextBox12.Text)) = 0 Then
            MsgBox("Tiene que digitar el monto de la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If IsNumeric(TextBox12.Text) = False Then
            MsgBox("Tiene que digitar el monto de la transacci�n", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If
        If CDbl(TextBox12.Text) <= 0 Then
            MsgBox("El monto de la transacci�n debe ser mayor que 0.00", MsgBoxStyle.Critical, "Error de Dato")
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor
        TextBox1.Focus()
        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.BigInt
            .Value = lngRegistro
        End With
        With prmTmp02
            .ParameterName = "@strFechaFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp03
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With prmTmp04
            .ParameterName = "@lngCliente"
            .SqlDbType = SqlDbType.SmallInt
            .Value = CInt(TextBox14.Text)
        End With
        With prmTmp05
            .ParameterName = "@lngVendedor"
            .SqlDbType = SqlDbType.TinyInt
            .Value = CInt(TextBox15.Text)
        End With
        With prmTmp06
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = UCase(TextBox11.Text)
        End With
        With prmTmp07
            .ParameterName = "@dblMonto"
            .SqlDbType = SqlDbType.Decimal
            .Value = CDbl(TextBox12.Text)
        End With
        With prmTmp08
            .ParameterName = "@dblInteres"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp09
            .ParameterName = "@dblRetencion"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp10
            .ParameterName = "@intFormaPago"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp11
            .ParameterName = "@strNumChqTarj"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp12
            .ParameterName = "@strBanco"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp13
            .ParameterName = "@strRecProv"
            .SqlDbType = SqlDbType.VarChar
            .Value = ""
        End With
        With prmTmp14
            .ParameterName = "@strDescrip"
            .SqlDbType = SqlDbType.Text
            .Value = ""
        End With
        With prmTmp15
            .ParameterName = "@lngUsuario"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegUsuario
        End With
        With prmTmp16
            .ParameterName = "@intTipoRecibo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intTipoReciboCaja
        End With
        With prmTmp17
            .ParameterName = "@FechaIng"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With prmTmp18
            .ParameterName = "@dblSaldoFavor"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp19
            .ParameterName = "@strDescrip01"
            .SqlDbType = SqlDbType.Text
            .Value = RichTextBox1.Text
        End With
        With prmTmp20
            .ParameterName = "@strDescrip02"
            .SqlDbType = SqlDbType.Text
            .Value = RichTextBox2.Text
        End With
        With prmTmp21
            .ParameterName = "@strDescrip03"
            .SqlDbType = SqlDbType.Text
            .Value = RichTextBox3.Text
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .Parameters.Add(prmTmp18)
            .Parameters.Add(prmTmp19)
            .Parameters.Add(prmTmp20)
            .Parameters.Add(prmTmp21)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
            GuardarDetalle()
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            ImprimirRecibo()
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub GuardarDetalle()

        Dim cmdTmp As New SqlCommand("sp_IngRecibosDet", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        '   Dim strFactura As String

        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegistro
        End With
        With prmTmp02
            .ParameterName = "@strFactura"
            .SqlDbType = SqlDbType.VarChar
            .Value = 0
        End With
        With prmTmp03
            .ParameterName = "@strDescrip"
            .SqlDbType = SqlDbType.Text
            .Value = TextBox13.Text
        End With
        With prmTmp04
            .ParameterName = "@dblAbono"
            .SqlDbType = SqlDbType.Decimal
            .Value = CDbl(TextBox12.Text)
        End With
        With prmTmp05
            .ParameterName = "@lngCliRegistro"
            .SqlDbType = SqlDbType.SmallInt
            .Value = CLng(TextBox14.Text)
        End With
        With prmTmp06
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = UCase(TextBox11.Text)
        End With
        With prmTmp07
            .ParameterName = "@dblInteres"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp08
            .ParameterName = "@dblMantVlr"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp09
            .ParameterName = "@dblPendiente"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp10
            .ParameterName = "@intTipoRecibo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intTipoReciboCaja
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
            Exit Sub
        End Try

    End Sub

    Sub Anular()

        intTipoReciboCaja = CInt(Label7.Text)
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngRecibosAnular", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim lngNumFecha As Long

        lngNumFecha = 0
        lngNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
        With prmTmp01
            .ParameterName = "@lngCliente"
            .SqlDbType = SqlDbType.SmallInt
            .Value = CInt(TextBox14.Text)
        End With
        With prmTmp02
            .ParameterName = "@strNumero"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox11.Text
        End With
        With prmTmp03
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumFecha
        End With
        With prmTmp04
            .ParameterName = "@dblMontoRec"
            .SqlDbType = SqlDbType.Decimal
            .Value = CDbl(TextBox12.Text)
        End With
        With prmTmp05
            .ParameterName = "@dblSaldoFavor"
            .SqlDbType = SqlDbType.Decimal
            .Value = 0
        End With
        With prmTmp06
            .ParameterName = "@intTipoRecibo"
            .SqlDbType = SqlDbType.TinyInt
            .Value = intTipoReciboCaja
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K
        Try
            cmdTmp.ExecuteNonQuery()
            MsgBox("Recibo Anulado", MsgBoxStyle.Information, "Anulaci�n Satisfactoria")
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub ImprimirRecibo()

        intTipoReciboCaja = CInt(Label7.Text)
        intRptOtros = 0
        intRptPresup = 0
        intRptCtasCbr = 0
        intRptFactura = 0
        intRptInventario = 0
        intRptImpFactura = 0
        intRptExportar = 0
        intRptCheques = 0
        intRptImpRecibos = 0
        Dim frmNew As New actrptViewer
        intRptImpRecibos = intTipoReciboCaja
        strVistaPreviaNTCNTD(8) = Label23.Text
        strQuery = ""
        strQuery = "exec sp_ExtraerReciboRpt " & intRptImpRecibos & ", " & CInt(TextBox14.Text) & ", "
        strQuery = strQuery + "'" & UCase(TextBox11.Text) & "', " & Format(DateTimePicker1.Value, "yyyyMMdd") & ""
        frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
        frmNew.Show()
        If intTipoReciboCaja = 3 Then
            frmNew.WindowState = FormWindowState.Minimized
            frmNew.Close()
        End If

    End Sub
    Private Sub ObtieneDatosListadoAyuda()

        If blnUbicar = True Then
            blnUbicar = False
            If strUbicar <> "" Then
                Select Case intListadoAyuda
                    Case 2 : TextBox1.Text = strUbicar
                        If UbicarCliente(TextBox1.Text) = True Then
                            TextBox11.Focus()
                        End If
                End Select
            End If
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                Select Case intListadoAyuda
                    Case 2 : TextBox1.Text = strUbicar
                        If UbicarCliente(TextBox1.Text) = True Then
                            TextBox11.Focus()
                        End If
                End Select
            End If
        End If

    End Sub

    Private Sub TextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.DoubleClick

        intListadoAyuda = 2
        Dim frmNew As New frmListadoAyuda
        Timer1.Interval = 200
        Timer1.Enabled = True
        frmNew.ShowDialog()

    End Sub

    Private Sub TextBox12_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox12.LostFocus

        If intTipoReciboCaja <> 11 Then
            If IsNumeric(sender.text) = True Then
                sender.text = Format(CDbl(sender.text), "#,##0.#0")
            Else
                MsgBox("Tiene que ser un valor num�rico v�lido. Verifique.", MsgBoxStyle.Critical, "Error de Dato")
                sender.focus()
            End If
        End If

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        intTipoReciboCaja = CInt(Label7.Text)
        If intTipoReciboCaja = 6 Then
            If Year(DateTimePicker1.Value) = Year(Now) And Month(DateTimePicker1.Value) = Month(Now) Then
            Else
                MsgBox("El recibo a anular no est� en el mes.  No es permitido anularlo.", MsgBoxStyle.Critical, "Recibo Inv�lido")
                Exit Sub
            End If
            Anular()
        Else
            Guardar()
        End If
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub cmdBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        intTipoReciboCaja = CInt(Label7.Text)
        If intTipoReciboCaja = 3 Then
            VistaPrevia()
        Else
            Extraer()
        End If
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
