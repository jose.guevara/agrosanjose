<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class actrptImprimeFactContado
    Inherits DataDynamics.ActiveReports.ActiveReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader1 As DataDynamics.ActiveReports.PageHeader
    Private WithEvents Detail1 As DataDynamics.ActiveReports.Detail
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(actrptImprimeFactContado))
        Me.PageHeader1 = New DataDynamics.ActiveReports.PageHeader()
        Me.lblNumeroFactura = New DataDynamics.ActiveReports.Label()
        Me.lblDia = New DataDynamics.ActiveReports.Label()
        Me.lblMes = New DataDynamics.ActiveReports.Label()
        Me.LblAnio = New DataDynamics.ActiveReports.Label()
        Me.lblNombreCliente = New DataDynamics.ActiveReports.Label()
        Me.lblVendedor = New DataDynamics.ActiveReports.Label()
        Me.Label3 = New DataDynamics.ActiveReports.Label()
        Me.Label4 = New DataDynamics.ActiveReports.Label()
        Me.Label5 = New DataDynamics.ActiveReports.Label()
        Me.Label6 = New DataDynamics.ActiveReports.Label()
        Me.Label7 = New DataDynamics.ActiveReports.Label()
        Me.Label8 = New DataDynamics.ActiveReports.Label()
        Me.Label10 = New DataDynamics.ActiveReports.Label()
        Me.lblDiasCredito = New DataDynamics.ActiveReports.Label()
        Me.Label21 = New DataDynamics.ActiveReports.Label()
        Me.Label20 = New DataDynamics.ActiveReports.Label()
        Me.lblFechaVence = New DataDynamics.ActiveReports.Label()
        Me.Label9 = New DataDynamics.ActiveReports.Label()
        Me.Detail1 = New DataDynamics.ActiveReports.Detail()
        Me.lblCantidad = New DataDynamics.ActiveReports.Label()
        Me.lblCodigoUnidad = New DataDynamics.ActiveReports.Label()
        Me.lblCodigoProducto = New DataDynamics.ActiveReports.Label()
        Me.lblDescripcion = New DataDynamics.ActiveReports.Label()
        Me.lblPrecio = New DataDynamics.ActiveReports.Label()
        Me.lblValor = New DataDynamics.ActiveReports.Label()
        Me.PageFooter1 = New DataDynamics.ActiveReports.PageFooter()
        Me.lblSubTotal = New DataDynamics.ActiveReports.Label()
        Me.lblImpuesto = New DataDynamics.ActiveReports.Label()
        Me.lblTotal = New DataDynamics.ActiveReports.Label()
        Me.lblTc = New DataDynamics.ActiveReports.Label()
        Me.lblSimboloMoneda = New DataDynamics.ActiveReports.Label()
        Me.Label1 = New DataDynamics.ActiveReports.Label()
        Me.Label2 = New DataDynamics.ActiveReports.Label()
        Me.Label17 = New DataDynamics.ActiveReports.Label()
        Me.Label18 = New DataDynamics.ActiveReports.Label()
        Me.GroupHeader1 = New DataDynamics.ActiveReports.GroupHeader()
        Me.Label11 = New DataDynamics.ActiveReports.Label()
        Me.Label12 = New DataDynamics.ActiveReports.Label()
        Me.Label13 = New DataDynamics.ActiveReports.Label()
        Me.Label14 = New DataDynamics.ActiveReports.Label()
        Me.Label15 = New DataDynamics.ActiveReports.Label()
        Me.Label16 = New DataDynamics.ActiveReports.Label()
        Me.GroupFooter1 = New DataDynamics.ActiveReports.GroupFooter()
        CType(Me.lblNumeroFactura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblMes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblAnio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblNombreCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblVendedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDiasCredito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblFechaVence, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCodigoUnidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblCodigoProducto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDescripcion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblValor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSubTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblImpuesto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblSimboloMoneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblNumeroFactura, Me.lblDia, Me.lblMes, Me.LblAnio, Me.lblNombreCliente, Me.lblVendedor, Me.Label3, Me.Label4, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label10, Me.lblDiasCredito, Me.Label21, Me.Label20, Me.lblFechaVence, Me.Label9})
        Me.PageHeader1.Height = 1.614583!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'lblNumeroFactura
        '
        Me.lblNumeroFactura.DataField = "Numero"
        Me.lblNumeroFactura.Height = 0.1875!
        Me.lblNumeroFactura.HyperLink = Nothing
        Me.lblNumeroFactura.Left = 5.9375!
        Me.lblNumeroFactura.Name = "lblNumeroFactura"
        Me.lblNumeroFactura.Style = ""
        Me.lblNumeroFactura.Text = ""
        Me.lblNumeroFactura.Top = 0!
        Me.lblNumeroFactura.Width = 1.0625!
        '
        'lblDia
        '
        Me.lblDia.DataField = "Dia"
        Me.lblDia.Height = 0.1875!
        Me.lblDia.HyperLink = Nothing
        Me.lblDia.Left = 5.125!
        Me.lblDia.Name = "lblDia"
        Me.lblDia.Style = "text-align: center"
        Me.lblDia.Text = ""
        Me.lblDia.Top = 0.4375!
        Me.lblDia.Width = 0.5!
        '
        'lblMes
        '
        Me.lblMes.DataField = "Mes"
        Me.lblMes.Height = 0.1875!
        Me.lblMes.HyperLink = Nothing
        Me.lblMes.Left = 5.75!
        Me.lblMes.Name = "lblMes"
        Me.lblMes.Style = "text-align: center"
        Me.lblMes.Text = ""
        Me.lblMes.Top = 0.4375!
        Me.lblMes.Width = 0.625!
        '
        'LblAnio
        '
        Me.LblAnio.DataField = "Anio"
        Me.LblAnio.Height = 0.1875!
        Me.LblAnio.HyperLink = Nothing
        Me.LblAnio.Left = 6.5!
        Me.LblAnio.Name = "LblAnio"
        Me.LblAnio.Style = "text-align: center"
        Me.LblAnio.Text = ""
        Me.LblAnio.Top = 0.4375!
        Me.LblAnio.Width = 0.5!
        '
        'lblNombreCliente
        '
        Me.lblNombreCliente.DataField = "CliNombre"
        Me.lblNombreCliente.Height = 0.1875!
        Me.lblNombreCliente.HyperLink = Nothing
        Me.lblNombreCliente.Left = 0.8125!
        Me.lblNombreCliente.Name = "lblNombreCliente"
        Me.lblNombreCliente.Style = ""
        Me.lblNombreCliente.Text = ""
        Me.lblNombreCliente.Top = 1.147!
        Me.lblNombreCliente.Width = 3.0!
        '
        'lblVendedor
        '
        Me.lblVendedor.Height = 0.1875!
        Me.lblVendedor.HyperLink = Nothing
        Me.lblVendedor.Left = 0.8125!
        Me.lblVendedor.Name = "lblVendedor"
        Me.lblVendedor.Style = ""
        Me.lblVendedor.Text = ""
        Me.lblVendedor.Top = 1.3345!
        Me.lblVendedor.Width = 4.3125!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 5.125!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = ""
        Me.Label3.Text = "N. Factura"
        Me.Label3.Top = 0!
        Me.Label3.Width = 0.6875!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 5.75!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "text-align: center"
        Me.Label4.Text = "MES"
        Me.Label4.Top = 0.25!
        Me.Label4.Width = 0.625!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 5.125!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "text-align: center"
        Me.Label5.Text = "DIA"
        Me.Label5.Top = 0.25!
        Me.Label5.Width = 0.5!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 6.5!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "text-align: center"
        Me.Label6.Text = "A�O"
        Me.Label6.Top = 0.25!
        Me.Label6.Width = 0.5!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = ""
        Me.Label7.Text = "Proveedor:"
        Me.Label7.Top = 1.147!
        Me.Label7.Width = 0.75!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = ""
        Me.Label8.Text = "Direccion:"
        Me.Label8.Top = 1.3345!
        Me.Label8.Width = 0.75!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 2.0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "text-align: center"
        Me.Label10.Text = "FACTURA DE COMPRA"
        Me.Label10.Top = 0.494!
        Me.Label10.Width = 1.622!
        '
        'lblDiasCredito
        '
        Me.lblDiasCredito.DataField = "DiasCredito"
        Me.lblDiasCredito.Height = 0.1875!
        Me.lblDiasCredito.HyperLink = Nothing
        Me.lblDiasCredito.Left = 5.9375!
        Me.lblDiasCredito.Name = "lblDiasCredito"
        Me.lblDiasCredito.Style = "text-align: center"
        Me.lblDiasCredito.Text = ""
        Me.lblDiasCredito.Top = 0.75!
        Me.lblDiasCredito.Width = 0.25!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 5.125!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "text-align: center"
        Me.Label21.Text = "Dias Credito"
        Me.Label21.Top = 0.75!
        Me.Label21.Width = 0.8125!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 5.125!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "text-align: center"
        Me.Label20.Text = "Fecha Vence"
        Me.Label20.Top = 0.9375!
        Me.Label20.Width = 0.875!
        '
        'lblFechaVence
        '
        Me.lblFechaVence.DataField = "FechaVence"
        Me.lblFechaVence.Height = 0.1875!
        Me.lblFechaVence.HyperLink = Nothing
        Me.lblFechaVence.Left = 6.0!
        Me.lblFechaVence.Name = "lblFechaVence"
        Me.lblFechaVence.Style = "text-align: center"
        Me.lblFechaVence.Text = ""
        Me.lblFechaVence.Top = 0.9375!
        Me.lblFechaVence.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.357!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 1.84!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "text-align: center"
        Me.Label9.Text = "CLINICA VETERINARIA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """EL GANADERO"""
        Me.Label9.Top = 0.083!
        Me.Label9.Width = 2.458!
        '
        'Detail1
        '
        Me.Detail1.ColumnSpacing = 0!
        Me.Detail1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblCantidad, Me.lblCodigoUnidad, Me.lblCodigoProducto, Me.lblDescripcion, Me.lblPrecio, Me.lblValor})
        Me.Detail1.Height = 0.1979167!
        Me.Detail1.Name = "Detail1"
        '
        'lblCantidad
        '
        Me.lblCantidad.DataField = "Cantidad"
        Me.lblCantidad.Height = 0.1875!
        Me.lblCantidad.HyperLink = Nothing
        Me.lblCantidad.Left = 0!
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Style = "ddo-char-set: 0; text-align: left; font-size: 11.25pt"
        Me.lblCantidad.Text = ""
        Me.lblCantidad.Top = 0!
        Me.lblCantidad.Width = 0.75!
        '
        'lblCodigoUnidad
        '
        Me.lblCodigoUnidad.DataField = "CadigoUnidad"
        Me.lblCodigoUnidad.Height = 0.1875!
        Me.lblCodigoUnidad.HyperLink = Nothing
        Me.lblCodigoUnidad.Left = 0.875!
        Me.lblCodigoUnidad.Name = "lblCodigoUnidad"
        Me.lblCodigoUnidad.Style = "text-align: left"
        Me.lblCodigoUnidad.Text = ""
        Me.lblCodigoUnidad.Top = 0!
        Me.lblCodigoUnidad.Width = 0.5!
        '
        'lblCodigoProducto
        '
        Me.lblCodigoProducto.DataField = "CodigoProducto"
        Me.lblCodigoProducto.Height = 0.1875!
        Me.lblCodigoProducto.HyperLink = Nothing
        Me.lblCodigoProducto.Left = 1.4375!
        Me.lblCodigoProducto.Name = "lblCodigoProducto"
        Me.lblCodigoProducto.Style = ""
        Me.lblCodigoProducto.Text = ""
        Me.lblCodigoProducto.Top = 0!
        Me.lblCodigoProducto.Width = 0.75!
        '
        'lblDescripcion
        '
        Me.lblDescripcion.DataField = "Descripcion"
        Me.lblDescripcion.Height = 0.1875!
        Me.lblDescripcion.HyperLink = Nothing
        Me.lblDescripcion.Left = 2.25!
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Style = ""
        Me.lblDescripcion.Text = ""
        Me.lblDescripcion.Top = 0!
        Me.lblDescripcion.Width = 2.6875!
        '
        'lblPrecio
        '
        Me.lblPrecio.DataField = "Precio"
        Me.lblPrecio.Height = 0.19!
        Me.lblPrecio.HyperLink = Nothing
        Me.lblPrecio.Left = 5.0!
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Style = "ddo-char-set: 0; text-align: right; font-size: 11.25pt"
        Me.lblPrecio.Text = ""
        Me.lblPrecio.Top = 0!
        Me.lblPrecio.Width = 0.88!
        '
        'lblValor
        '
        Me.lblValor.DataField = "Valor"
        Me.lblValor.Height = 0.19!
        Me.lblValor.HyperLink = Nothing
        Me.lblValor.Left = 6.0!
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Style = "ddo-char-set: 0; text-align: right; font-size: 11.25pt"
        Me.lblValor.Text = ""
        Me.lblValor.Top = 0!
        Me.lblValor.Width = 0.88!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.lblSubTotal, Me.lblImpuesto, Me.lblTotal, Me.lblTc, Me.lblSimboloMoneda, Me.Label1, Me.Label2, Me.Label17, Me.Label18})
        Me.PageFooter1.Height = 0.7708333!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'lblSubTotal
        '
        Me.lblSubTotal.DataField = "SubTotal"
        Me.lblSubTotal.Height = 0.19!
        Me.lblSubTotal.HyperLink = Nothing
        Me.lblSubTotal.Left = 6.0!
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Style = "ddo-char-set: 0; text-align: right; font-size: 11.25pt"
        Me.lblSubTotal.Text = ""
        Me.lblSubTotal.Top = 0.125!
        Me.lblSubTotal.Width = 0.88!
        '
        'lblImpuesto
        '
        Me.lblImpuesto.DataField = "TotalImpuesto"
        Me.lblImpuesto.Height = 0.19!
        Me.lblImpuesto.HyperLink = Nothing
        Me.lblImpuesto.Left = 6.0!
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Style = "ddo-char-set: 0; text-align: right; font-size: 11.25pt"
        Me.lblImpuesto.Text = ""
        Me.lblImpuesto.Top = 0.3125!
        Me.lblImpuesto.Width = 0.88!
        '
        'lblTotal
        '
        Me.lblTotal.DataField = "Total"
        Me.lblTotal.Height = 0.19!
        Me.lblTotal.HyperLink = Nothing
        Me.lblTotal.Left = 6.0!
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Style = "ddo-char-set: 0; text-align: right; font-size: 11.25pt"
        Me.lblTotal.Text = ""
        Me.lblTotal.Top = 0.5!
        Me.lblTotal.Width = 0.88!
        '
        'lblTc
        '
        Me.lblTc.Height = 0.15!
        Me.lblTc.HyperLink = Nothing
        Me.lblTc.Left = 2.3125!
        Me.lblTc.Name = "lblTc"
        Me.lblTc.Style = "font-weight: bold"
        Me.lblTc.Text = "T/C"
        Me.lblTc.Top = 0.3125!
        Me.lblTc.Width = 1.13!
        '
        'lblSimboloMoneda
        '
        Me.lblSimboloMoneda.Height = 0.1875!
        Me.lblSimboloMoneda.HyperLink = Nothing
        Me.lblSimboloMoneda.Left = 5.75!
        Me.lblSimboloMoneda.Name = "lblSimboloMoneda"
        Me.lblSimboloMoneda.Style = ""
        Me.lblSimboloMoneda.Text = "C$"
        Me.lblSimboloMoneda.Top = 0.125!
        Me.lblSimboloMoneda.Width = 0.25!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 5.75!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = ""
        Me.Label1.Text = "C$"
        Me.Label1.Top = 0.3125!
        Me.Label1.Width = 0.25!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 5.75!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = ""
        Me.Label2.Text = "C$"
        Me.Label2.Top = 0.5!
        Me.Label2.Width = 0.25!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 5.0625!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "text-align: right"
        Me.Label17.Text = "Sub Total"
        Me.Label17.Top = 0.125!
        Me.Label17.Width = 0.6875!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 5.0625!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "text-align: right"
        Me.Label18.Text = "Total"
        Me.Label18.Top = 0.5!
        Me.Label18.Width = 0.6875!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New DataDynamics.ActiveReports.ARControl() {Me.Label11, Me.Label12, Me.Label13, Me.Label14, Me.Label15, Me.Label16})
        Me.GroupHeader1.Height = 0.2604167!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 0!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "ddo-char-set: 0; text-align: justify; font-size: 11.25pt"
        Me.Label11.Text = "Cantidad"
        Me.Label11.Top = 0!
        Me.Label11.Width = 0.75!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0.875!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "text-align: center"
        Me.Label12.Text = "Unidad"
        Me.Label12.Top = 0!
        Me.Label12.Width = 0.5!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 1.4375!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "text-align: center"
        Me.Label13.Text = "C�digo"
        Me.Label13.Top = 0!
        Me.Label13.Width = 0.75!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 2.25!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "text-align: center"
        Me.Label14.Text = "Producto"
        Me.Label14.Top = 0!
        Me.Label14.Width = 2.6875!
        '
        'Label15
        '
        Me.Label15.Height = 0.19!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 5.0!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "ddo-char-set: 0; text-align: center; font-size: 11.25pt"
        Me.Label15.Text = "P. Unitario"
        Me.Label15.Top = 0!
        Me.Label15.Width = 0.88!
        '
        'Label16
        '
        Me.Label16.Height = 0.19!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 6.0!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "ddo-char-set: 0; text-align: center; font-size: 11.25pt"
        Me.Label16.Text = "Total"
        Me.Label16.Top = 0!
        Me.Label16.Width = 0.88!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'actrptImprimeFactContado
        '
        Me.MasterReport = False
        Me.PageSettings.Margins.Left = 0.8!
        Me.PageSettings.Margins.Right = 0.5!
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.135417!
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail1)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.lblNumeroFactura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblMes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblAnio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblNombreCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblVendedor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDiasCredito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblFechaVence, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCantidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCodigoUnidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblCodigoProducto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDescripcion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblValor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSubTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblImpuesto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblSimboloMoneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents lblCantidad As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCodigoUnidad As DataDynamics.ActiveReports.Label
    Friend WithEvents lblCodigoProducto As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDescripcion As DataDynamics.ActiveReports.Label
    Friend WithEvents lblPrecio As DataDynamics.ActiveReports.Label
    Friend WithEvents lblValor As DataDynamics.ActiveReports.Label
    Friend WithEvents lblNumeroFactura As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDia As DataDynamics.ActiveReports.Label
    Friend WithEvents lblMes As DataDynamics.ActiveReports.Label
    Friend WithEvents LblAnio As DataDynamics.ActiveReports.Label
    Friend WithEvents lblNombreCliente As DataDynamics.ActiveReports.Label
    Friend WithEvents lblVendedor As DataDynamics.ActiveReports.Label
    Friend WithEvents lblSubTotal As DataDynamics.ActiveReports.Label
    Friend WithEvents lblImpuesto As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTotal As DataDynamics.ActiveReports.Label
    Friend WithEvents lblTc As DataDynamics.ActiveReports.Label
    Friend WithEvents lblSimboloMoneda As DataDynamics.ActiveReports.Label
    Friend WithEvents Label1 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label2 As DataDynamics.ActiveReports.Label
    Friend WithEvents GroupHeader1 As DataDynamics.ActiveReports.GroupHeader
    Friend WithEvents GroupFooter1 As DataDynamics.ActiveReports.GroupFooter
    Private WithEvents PageFooter1 As DataDynamics.ActiveReports.PageFooter
    Friend WithEvents Label3 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label4 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label5 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label6 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label7 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label8 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label10 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label17 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label11 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label12 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label13 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label14 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label15 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label16 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label18 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblDiasCredito As DataDynamics.ActiveReports.Label
    Friend WithEvents Label21 As DataDynamics.ActiveReports.Label
    Friend WithEvents Label20 As DataDynamics.ActiveReports.Label
    Friend WithEvents lblFechaVence As DataDynamics.ActiveReports.Label
    Friend WithEvents Label9 As DataDynamics.ActiveReports.Label
End Class
