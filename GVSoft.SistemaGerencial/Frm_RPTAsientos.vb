﻿Imports System.Windows.Forms
'Imports Microsoft.Reporting.WinForms
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.ViewerObjectModel
Imports CrystalDecisions.Windows.Forms.CrystalReportViewer
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Odbc
Imports DevComponents.DotNetBar
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class Frm_RPTAsientos
    Dim jackcrystal As New crystal_jack
    Dim jackconsulta As New ConsultasDB
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "Frm_RPTAsientos"
    Private Sub CrystalReportViewer1_Load(sender As Object, e As System.EventArgs) Handles CrystalReportViewer1.Load
        Dim i As Integer = 0
        Dim val_max As Integer = SIMF_CONTA.numeformulas
        Dim nombreformula As String = String.Empty
        Dim valor As String = String.Empty

        Dim sqlconexion As SqlConnection = Nothing 'conexión
        REM declaramos las tablas del dataset***DATAADAPTERS
        Dim DAcatalogoconta As SqlDataAdapter = Nothing
        Dim DADasientos As SqlDataAdapter = Nothing
        Dim DAMasientos As SqlDataAdapter = Nothing
        Dim sql_Dasientos As String = String.Empty
        'Dim dset_Asientos As New DS_Comprobantes
        Dim dset_Asientos As DataSet = Nothing
        Dim report_asiento As RPTAsientos = Nothing
        Dim sql_Masientos As String = String.Empty
        Dim dtDatos As DataTable = Nothing
        Try
            dset_Asientos = New DataSet
            'sqlconexion = New SqlConnection(SIMF_CONTA.ConnectionStringconta)
            dtDatos = RNAsientos.ObtieneReporteAsientosContables(SIMF_CONTA.formula(2).ToString(), SIMF_CONTA.formula(3).ToString(), SIMF_CONTA.formula(4).ToString())
            dtDatos.TableName = "Dasientos"
            dset_Asientos.Tables.Add(dtDatos.Copy())
            dtDatos = Nothing
            dtDatos = RNAsientos.ObtieneReporteAsientosContablesEncabezado(SIMF_CONTA.formula(2).ToString(), SIMF_CONTA.formula(3).ToString(), SIMF_CONTA.formula(4).ToString())
            dtDatos.TableName = "Masientos"
            dset_Asientos.Tables.Add(dtDatos.Copy())
            report_asiento = New RPTAsientos
            report_asiento.SetDataSource(dset_Asientos)
            For i = 0 To val_max - 1
                If Not SIMF_CONTA.formula(i) = Nothing Then
                    jackcrystal.retorna_parametro(nombreformula, valor, i)
                    report_asiento.SetParameterValue(nombreformula, valor)
                End If
            Next


            Me.CrystalReportViewer1.ReportSource = report_asiento
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("CrystalReportViewer1_Load - Error: " + ex.Message.ToString(), "Frm_RPTAsientos", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class