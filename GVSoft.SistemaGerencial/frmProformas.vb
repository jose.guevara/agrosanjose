Imports System.Data.SqlClient
Imports System.Text
Imports System.Web.UI.WebControls
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports System.Reflection
Imports Infragistics.Win
Imports Infragistics.Win.UltraWinGrid
Imports System.Collections.Generic
Imports DevComponents.DotNetBar
Imports DevComponents.AdvTree
Imports DevComponents.DotNetBar.Controls
Imports DevComponents.Editors.DateTimeAdv


Public Class frmProformas
    Dim sSerie As String = String.Empty
    Dim inExisteFactura As Integer = 0
    Dim IndicaDebeBuscar As Boolean = True
    Dim strUnidad As String
    Dim strFacturas As String
    Dim strDescrip As String
    Dim valorAnt As Double = 0
    Dim MontoAnt As Double = 0
    Dim chk As Boolean
    Dim strNumeroProforma As String = String.Empty
    Dim intImpuesto As Integer
    Dim intImprimir As Integer
    Dim intRow, intCol As Integer
    Dim lngRegistro2 As Long
    Dim IndicaImprimir As Integer = 0
    Public txtCollection As New Collection
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreClase As String = "frmProformas"


    Private Sub frmProformas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim frmNew As frmEscogerAgencia = Nothing
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim lnTasaCambioDelDia As Decimal = 0
        Try
            frmNew = New frmEscogerAgencia
            objParametro = New SEParametroEmpresa()
            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            lnTasaCambioDelDia = RNTipoCambio.ObtieneTipoCambioDelDia()

            intDiaFact = 0
            strMesFact = ""
            lngYearFact = 0
            strVendedor = ""
            strNumeroFact = ""
            strCodCliFact = ""
            strClienteFact = ""

            'Me.Top = 0
            'Me.Left = 0
            strUnidad = ""
            strDescrip = ""
            lngRegistro = 0
            intImprimir = 0
            txtCollection.Add(txtNumeroFactura)
            txtCollection.Add(txtDias)
            txtCollection.Add(txtVence)
            txtCollection.Add(txtNombreVendedor)
            'txtCollection.Add(txtNombreCliente)
            txtCollection.Add(txtProducto)
            txtCollection.Add(txtCantidad)
            txtCollection.Add(txtPrecio)
            txtCollection.Add(txtSubTotal)
            txtCollection.Add(txtImpVenta)
            txtCollection.Add(txtRetencion)
            txtCollection.Add(txtTotal)
            txtCollection.Add(txtNumeroVendedor)
            txtCollection.Add(txtNumeroCliente)
            txtCollection.Add(txtDepartamento)
            txtCollection.Add(txtNegocio)
            CargaTipoPrecio()
            UbicarAgencia(lngRegUsuario)
            If (lngRegAgencia = 0) Then
                lngRegAgencia = 0
                frmNew.ShowDialog()
                If lngRegAgencia = 0 Then
                    MsgBox("No escogio una agencia en que trabajar.", MsgBoxStyle.Critical)
                End If
            End If

            Limpiar()
            Label16.Visible = False
            Label18.Text = 0 'intTipoFactura
            Label19.Text = lngRegAgencia
            ObtieneSeriexAgencia()
            ObtieneVendedorPorDefecto()
            ObtengoDatosListadoAyuda()
            txtTasaCambio.Visible = True
            lblTasaCambio.Visible = True
            txtTotalCordobas.Visible = True
            lblTotalCordobas.Visible = True
            txtTasaCambio.Text = lnTasaCambioDelDia
            txtMoneda.Text = RNMoneda.ObtieneDescripcionMoneda(objParametro.MonedaFacturaCredito)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0
        Try

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            ddlSerie.SelectedItem = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            'If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 3 Or intTipoFactura = 7 Or intTipoFactura = 9 Then
            '    'strQuery = "Select a.IdSerie, Descripcion From catSerieFactura  a,tbl_ultimonumero b where Activo=1 and b.agenregistro =" & lngRegAgencia & " and a.IdSerie = b.IdSerie and Contado >0"
            strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",1"
            'End If
            'If intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 6 Or intTipoFactura = 8 Or intTipoFactura = 10 Then
            '    'strQuery = "Select a.IdSerie, Descripcion From catSerieFactura  a,tbl_ultimonumero b where Activo=1 and b.agenregistro =" & lngRegAgencia & " and a.IdSerie = b.IdSerie and Credito >0"
            '    strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",4"
            'End If
            If strQuery.Trim.Length > 0 Then
                ddlSerie.Items.Clear()
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                    IndicaObtieneRegistro = 1

                End While
            End If
            If IndicaObtieneRegistro = 1 Then
                ddlSerie.SelectedIndex = 0
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Function CalculaTotalasFactura() As SETotalesFactura
        Dim objTotalesFactura As SETotalesFactura = Nothing
        Dim dblSubtotal As Double = 0
        Dim dblTotal As Double = 0
        Dim dblImpuesto As Double = 0
        Dim dblRetencion As Double = 0
        Dim lnCantidad As Double = 0
        Dim intIncr As Integer = 0
        Dim lnImpuesto As Double = 0
        Dim lsIndicaImpuesto As String = String.Empty
        Dim lnPrecioUnitario As Double = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim IndicaBonificacion As Integer = 0
        Dim lnPrecio As Decimal = 0
        Try

            If dgvDetalle.Rows.Count > 0 Then
                If dgvDetalle.IsCurrentCellDirty Then
                    dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                objTotalesFactura = New SETotalesFactura()

                For intIncr = 0 To dgvDetalle.Rows.Count - 1
                    lnImpuesto = 0
                    lsIndicaImpuesto = String.Empty
                    lnCantidad = 0
                    lnPrecioUnitario = 0
                    lnCantidad = lnCantidad + SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value)
                    IndicaBonificacion = 0
                    lnPrecio = SUConversiones.ConvierteADecimal(dgvDetalle.Rows(intIncr).Cells("Valor").Value)
                    If chkBonificacion.Checked Then
                        IndicaBonificacion = 1
                        lnPrecio = 0

                    End If

                    lnPrecioUnitario = lnPrecioUnitario + lnPrecio
                    dblSubtotal = dblSubtotal + (lnCantidad * lnPrecioUnitario)
                    lsIndicaImpuesto = dgvDetalle.Rows(intIncr).Cells("Impuesto").Value
                    If lsIndicaImpuesto.Trim().ToUpper = "SI" Then
                        lnImpuesto = (lnCantidad * lnPrecioUnitario) * dblPorcParam
                    End If
                    dblImpuesto = dblImpuesto + lnImpuesto
                Next
            End If

            dblRetencion = 0
            If ddlRetencion.SelectedIndex = 1 Then
                dblRetencion = dblSubtotal * dblRetParam
            End If
            ' dblRetencion = SUConversiones.ConvierteADouble(dblSubtotal) * dblRetParam
            dblTotal = dblSubtotal + dblImpuesto - dblRetencion

            If objTotalesFactura Is Nothing Then
                objTotalesFactura = New SETotalesFactura()
            End If
            objTotalesFactura.SubTotal = dblSubtotal
            objTotalesFactura.TotalImpuesto = dblImpuesto
            objTotalesFactura.TotalRetencion = dblRetencion
            objTotalesFactura.Total = dblTotal

            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()
            If objParametro.MonedaFacturaCredito = 2 Then
                txtTasaCambio.Visible = True
                lblTasaCambio.Visible = True
                txtTasaCambio.Text = RNTipoCambio.ObtieneTipoCambioDelDia()
                'txtTasaCambio.Text = lnTasaCambioDelDia
                lblTotalCordobas.Visible = True
                txtTotalCordobas.Visible = True
                txtTotalCordobas.Text = Format((SUFunciones.RedondeoNumero(dblTotal * SUConversiones.ConvierteADecimal(txtTasaCambio.Text), objParametro.IndicaTipoRedondeo)), "#,##0.#0")
            End If
            If dgvDetalle.IsCurrentCellDirty Then
                dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
            txtSubTotal.Text = Format(objTotalesFactura.SubTotal, "#,##0.#0")
            txtImpVenta.Text = Format(objTotalesFactura.TotalImpuesto, "#,##0.#0")
            txtRetencion.Text = Format(objTotalesFactura.TotalRetencion, "#,##0.#0")
            txtTotal.Text = Format(objTotalesFactura.Total, "#,##0.#0")
            txtTotalCordobas.Text = Format(objTotalesFactura.Total * SUConversiones.ConvierteADouble(txtTasaCambio.Text), "#,##0.#0")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Timer1.Enabled = True
            objTotalesFactura.SubTotal = 0
            objTotalesFactura.TotalImpuesto = 0
            objTotalesFactura.TotalRetencion = 0
            objTotalesFactura.Total = 0
        End Try
        Return objTotalesFactura
    End Function
    Private Function ObtenerMontoAFacturarBonificadoXIdProducto(ByVal pnPrecioVentaActual As Decimal, ByVal pnCantidadActualBonificada As Integer, ByVal pnIdProducto As Integer, ByVal pnPrecioMinimoAFacturar As Decimal) As Object
        Dim lnCantidad As Double = 0
        Dim lnTotalCantidadAFacturar As Double = 0
        Dim lnTotalCantidadABonificar As Double = 0
        Dim lnPrecioUnitario As Double = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim IndicaBonificacion As Integer = 0
        Dim lnNuevoPrecioAFacturar As Decimal = 0
        Dim objResultado As Object() = New Object(3) {}
        Try
            objResultado(0) = 0
            objResultado(1) = "Todo bien"

            If dgvDetalle.Rows.Count > 0 Then
                If dgvDetalle.IsCurrentCellDirty Then
                    dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                For intIncr = 0 To dgvDetalle.Rows.Count - 1
                    lnCantidad = 0
                    lnPrecioUnitario = 0

                    IndicaBonificacion = 0

                    If pnIdProducto = SUConversiones.ConvierteAInt(dgvDetalle.Rows(intIncr).Cells("IdProducto").Value) Then
                        IndicaBonificacion = 0
                        IndicaBonificacion = SUConversiones.ConvierteAInt(dgvDetalle.Rows(intIncr).Cells("EsBonificacion").Value)
                        If IndicaBonificacion = 1 Then
                            lnTotalCantidadABonificar = lnTotalCantidadABonificar + SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value)
                        Else
                            lnPrecioUnitario = SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Valor").Value)
                            lnTotalCantidadAFacturar = lnTotalCantidadAFacturar + SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Cantidad").Value)
                        End If

                    End If
                Next
                If lnTotalCantidadABonificar = 0 AndAlso pnCantidadActualBonificada <> 0 Then
                    lnTotalCantidadABonificar = pnCantidadActualBonificada
                End If
                If lnPrecioUnitario = 0 Then
                    lnPrecioUnitario = ObtenerPrecioVentaNoBonificada(pnIdProducto)
                End If

                objResultado = RNFacturas.ValidaPrecioVenta(lnTotalCantidadAFacturar, lnTotalCantidadABonificar, lnPrecioUnitario, pnPrecioMinimoAFacturar)
            Else
                lnNuevoPrecioAFacturar = pnPrecioVentaActual
            End If
            If dgvDetalle.IsCurrentCellDirty Then
                dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If dgvDetalle.IsCurrentCellDirty Then
                dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Timer1.Enabled = True
        End Try
        Return objResultado
    End Function
    Private Function ObtenerPrecioVentaNoBonificada(ByVal pnIdProducto As Integer) As Decimal
        Dim lnPrecioUnitario As Double = 0
        Dim IndicaBonificacion As Integer = 0
        Try

            If dgvDetalle.Rows.Count > 0 Then
                If dgvDetalle.IsCurrentCellDirty Then
                    dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If

                For intIncr = 0 To dgvDetalle.Rows.Count - 1
                    IndicaBonificacion = 0

                    If pnIdProducto = SUConversiones.ConvierteAInt(dgvDetalle.Rows(intIncr).Cells("IdProducto").Value) Then
                        IndicaBonificacion = 0
                        IndicaBonificacion = SUConversiones.ConvierteAInt(dgvDetalle.Rows(intIncr).Cells("EsBonificacion").Value)
                        If IndicaBonificacion = 0 Then
                            lnPrecioUnitario = 0
                            lnPrecioUnitario = SUConversiones.ConvierteADouble(dgvDetalle.Rows(intIncr).Cells("Valor").Value)
                        End If

                    End If
                Next

            Else
                lnPrecioUnitario = 0
            End If
            If dgvDetalle.IsCurrentCellDirty Then
                dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)

        End Try
        Return lnPrecioUnitario
    End Function
    Sub ObtieneVendedorPorDefecto()
        Dim registroVendor As Integer = 0
        Try


            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            ddlSerie.SelectedItem = -1
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            strQuery = ""
            strQuery = "Select IdVendedor From prm_agencias where registro =" & lngRegAgencia
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                registroVendor = ConvierteAInt(dtrAgro2K.GetValue(0))
                txtNumeroVendedor.Text = ConvierteAInt(dtrAgro2K.GetValue(0))
            End While
            dtrAgro2K.Close()
            strQuery = ""
            strQuery = "Select registro,codigo,nombre From prm_vendedores where registro =" & registroVendor
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtVendedor.Text = dtrAgro2K.GetValue(1)
                txtNombreVendedor.Text = dtrAgro2K.GetValue(2)
            End While

            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub CargaTipoPrecio()
        Try
            ddlTipoPrecio.Items.Add(New ListItem("Precio venta distribuidor", 0))
            ddlTipoPrecio.Items.Add(New ListItem("Precio venta publico", 1))
            ddlTipoPrecio.Items.Add(New ListItem("Precio costo", 2))
            ddlTipoPrecio.SelectedIndex = 0
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub Limpiar()
        Try
            DPkFechaFactura.Value = Now
            intIncr = 0
            For intIncr = 1 To 6
                txtCollection.Item(intIncr).Text = ""
            Next intIncr
            intIncr = 0
            For intIncr = 7 To 12
                txtCollection.Item(intIncr).Text = "0.00"
            Next intIncr
            For intIncr = 13 To txtCollection.Count
                txtCollection.Item(intIncr).Text = "0"
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrCredito(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            For intIncr = 0 To 25
                For intIncr2 = 0 To 6
                    arrContado(intIncr, intIncr2) = ""
                Next intIncr2
            Next intIncr
            txtDias.Text = "10"
            txtVendedor.Text = String.Empty
            txtCliente.Text = String.Empty
            txtDireccion.Text = String.Empty
            txtNombreCliente.Text = String.Empty
            txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
            ddlRetencion.SelectedIndex = 0
            ddlSerie.SelectedIndex = -1
            'If intFechaCambioFactura > 0 Then
            '    ddlTipoPrecio.SelectedIndex = 2
            'Else
            '    ddlTipoPrecio.SelectedIndex = 0
            'End If
            DPkFechaFactura.Focus()
            Select Case intTipoFactura
                Case 1, 2, 3, 7, 9 : Label3.Visible = False : Label4.Visible = False : txtDias.Visible = False : txtVence.Visible = False
                    txtCliente.Visible = False : txtNombreCliente.ReadOnly = False : txtNombreCliente.Width = 280 : txtNombreCliente.Location = New Point(419, 48)
                Case 4, 5, 6, 8, 10 : Label3.Visible = True : Label4.Visible = True : txtDias.Visible = True : txtVence.Visible = True
            End Select
            Select Case intTipoFactura
                Case 1, 4 'ObtieneSeriexAgencia() ': txtNumeroFactura.Text = ExtraerNumeroFactura(lngRegAgencia, intTipoFactura, ddlSerie.SelectedItem)
                Case 2, 3, 5, 6 : BloquearCampos()
                Case 7, 8 : txtNumeroFactura.ReadOnly = False
                Case 9, 10 : BloquearCampos() : txtNumeroFactura.ReadOnly = False
            End Select

            If dgvDetalle.Rows.Count > 0 Then
                dgvDetalle.Rows.Clear()
            End If

            DPkFechaFactura.Focus()
            ObtieneSeriexAgencia()
            ObtieneVendedorPorDefecto()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Sub Limpiar2()
        Try

            txtProducto.Text = String.Empty
            txtCantidad.Text = "0"
            txtPrecio.Text = "0.00"
            chkBonificacion.Checked = False
            'If intFechaCambioFactura > 0 Then
            '    ddlTipoPrecio.SelectedIndex = 2
            'Else
            '    ddlTipoPrecio.SelectedIndex = 0
            'End If
            txtCantidad.Focus()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub BloquearCampos()
        Try
            For intIncr = 1 To txtCollection.Count - 1
                txtCollection.Item(intIncr).readonly = True
            Next
            DPkFechaFactura.Enabled = False
            ddlRetencion.Enabled = False
            ddlTipoPrecio.Enabled = False
            txtVendedor.ReadOnly = True
            txtCliente.ReadOnly = False
            If intTipoFactura = 3 Or intTipoFactura = 6 Then
                txtNumeroFactura.ReadOnly = False
                'txtSerie.ReadOnly = False
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Function UbicarProducto(ByVal strDato As String) As Boolean
        Try
            strDescrip = ""
            strUnidad = ""
            lngRegistro = 0
            intImpuesto = 0
            strQuery = ""
            strQuery = "Select P.Descripcion, U.Codigo, P.Registro, P.Impuesto from prm_Productos P left join prm_Unidades U  on P.RegUnidad = U.Registro "
            strQuery = strQuery + "Where P.Codigo = '" & strDato & "'  AND P.Estado = 0"
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                strDescrip = dtrAgro2K.GetValue(0)
                strUnidad = dtrAgro2K.GetValue(1)
                lngRegistro = dtrAgro2K.GetValue(2)
                intImpuesto = dtrAgro2K.GetValue(3)
            End While
            dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            If lngRegistro = 0 Then
                UbicarProducto = False
                Exit Function
            End If
            UbicarProducto = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Function

    Function UbicarVendedor(ByVal strDato As String) As Boolean
        Try
            txtNumeroVendedor.Text = "-1"
            strQuery = ""
            strQuery = "Select Nombre, Registro from prm_Vendedores Where Codigo = '" & txtVendedor.Text.Trim() & "' "
            If intTipoFactura = 9 Or intTipoFactura = 10 Then
                strQuery = strQuery + "And registro <> -1"
            Else
                strQuery = strQuery + "And Estado = 0 And registro <> -1"
            End If
            If cnnAgro2K.State = ConnectionState.Open Then
                cnnAgro2K.Close()
            End If
            If cmdAgro2K.Connection.State = ConnectionState.Open Then
                cmdAgro2K.Connection.Close()
            End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            UbicarVendedor = False
            While dtrAgro2K.Read
                txtNombreVendedor.Text = dtrAgro2K.GetValue(0)
                txtNumeroVendedor.Text = dtrAgro2K.GetValue(1)
                UbicarVendedor = True
            End While
            SUFunciones.CierraConexioDR(dtrAgro2K)
            'dtrAgro2K.Close()
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            'If txtNumeroVendedor.Text = "0" Then
            '    UbicarVendedor = False
            '    Exit Function
            'End If
            'UbicarVendedor = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Function UbicarCliente(ByVal strDato As String) As Boolean
        Try


            strDirecFact = ""
            txtNumeroCliente.Text = "0"
            strQuery = ""
            'strQuery = "Select C.nombre, C.registro, C.deptregistro, C.negregistro, C.vendregistro, "
            'strQuery = strQuery + "C.direccion, C.dias_credito from prm_Clientes C Where C.Codigo = "
            'strQuery = strQuery + "'" & txtCliente.Text & "' AND C.Estado = 0"

            strQuery = " exec ObtenerClientexCodigo '" & txtCliente.Text & "'"
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)

            'If cnnAgro2K.State = ConnectionState.Open Then
            '    cnnAgro2K.Close()
            'End If
            'If cmdAgro2K.Connection.State = ConnectionState.Open Then
            '    cmdAgro2K.Connection.Close()
            'End If
            cnnAgro2K.Open()
            cmdAgro2K.Connection = cnnAgro2K
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                txtNombreCliente.Text = dtrAgro2K.GetValue(0)
                txtNumeroCliente.Text = dtrAgro2K.GetValue(1)
                txtDepartamento.Text = dtrAgro2K.GetValue(2)
                txtNumeroVendedor.Text = dtrAgro2K.Item("vendregistro")
                txtNombreVendedor.Text = dtrAgro2K.Item("NombreVendedor")
                txtVendedor.Text = dtrAgro2K.Item("CodigoVendedor")
                txtNegocio.Text = dtrAgro2K.GetValue(3)
                ''txtDias.Text = dtrAgro2K.GetValue(6)
                ''txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                strDirecFact = dtrAgro2K.GetValue(5)
                txtDireccion.Text = dtrAgro2K.GetValue(5)
                If intTipoFactura = 4 Or intTipoFactura = 8 Then
                    If dtrAgro2K.GetValue(4) <> SUConversiones.ConvierteAInt(txtNumeroVendedor.Text) Then
                        UbicarCliente = False
                        txtVendedor.Text = ""
                        txtNombreVendedor.Text = ""
                        Exit Function
                    End If
                End If
            End While
            'dtrAgro2K.Close()
            SUFunciones.CierraConexioDR(dtrAgro2K)
            SUFunciones.CierraConexionBD(cnnAgro2K)
            SUFunciones.CierraComando(cmdAgro2K)
            'cmdAgro2K.Connection.Close()
            'cnnAgro2K.Close()
            If txtNumeroCliente.Text = "0" Then
                UbicarCliente = False
                Exit Function
            End If
            UbicarCliente = True
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Function

    Sub UbicarPrecio()
        Dim lnTipoPrecio As Integer = 0
        Try
            Select Case ddlTipoPrecio.SelectedIndex
                Case 0
                    lnTipoPrecio = 1
                Case 1
                    lnTipoPrecio = 0
                Case Else
                    lnTipoPrecio = ddlTipoPrecio.SelectedIndex
            End Select
            dtrAgro2K = Nothing
            dtrAgro2K = RNProduto.UbicarPrecio(lngRegistro, lnTipoPrecio, intTipoFactura, lngRegAgencia)
            If dtrAgro2K IsNot Nothing Then
                While dtrAgro2K.Read
                    txtPrecio.Text = dtrAgro2K.Item("Precio")
                    txtPrecioCosto.Text = dtrAgro2K.Item("PrecioCosto")
                End While
            End If
            SUFunciones.CierraConexioDR(dtrAgro2K)
            dtrAgro2K = Nothing
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ObtengoDatosListadoAyuda()
        Try


            If blnUbicar = True Then
                blnUbicar = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 1 : txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                            If txtCliente.Visible = True Then
                                txtCliente.Focus()
                            Else
                                txtNombreCliente.Focus()
                            End If
                        Case 2 : txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text)
                            If txtVendedor.Text = "" Then
                                MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                                txtVendedor.Focus()
                            End If
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : ddlRetencion.Focus()
                        Case 4 : txtNumeroFactura.Text = strUbicar ''ExtraerFactura()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Function CargarDetalleProformas() As List(Of SEDetalleProforma)
        Dim lstDetalleProf As List(Of SEDetalleProforma) = Nothing
        Dim objDetalleProf As SEDetalleProforma = Nothing
        Dim i As Integer = 0
        Try
            objDetalleProf = New SEDetalleProforma()
            lstDetalleProf = New List(Of SEDetalleProforma)
            If dgvDetalle.Rows.Count > 0 Then
                For i = 0 To dgvDetalle.Rows.Count - 1
                    objDetalleProf.IdProforma = 0
                    objDetalleProf.IdProducto = dgvDetalle.Rows(i).Cells("IdProducto").Value
                    objDetalleProf.Cantidad = dgvDetalle.Rows(i).Cells("Cantidad").Value
                    objDetalleProf.PrecioCosto = dgvDetalle.Rows(i).Cells("PrecioCosto").Value
                    objDetalleProf.Precio = dgvDetalle.Rows(i).Cells("Valor").Value
                    objDetalleProf.Valor = dgvDetalle.Rows(i).Cells("Total").Value
                    objDetalleProf.EsBonificado = dgvDetalle.Rows(i).Cells("EsBonificacion").Value
                    'objDetalleProf.Igv = dgvDetalle.Rows(i).Cells("Igv").Value
                    lstDetalleProf.Add(New SEDetalleProforma(objDetalleProf))
                Next
            End If
            Return lstDetalleProf
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try

    End Function

    Private Sub IngresarProforma()
        Dim objProforma As SEMaestroProforma
        Dim Resultado As Object() = New Object(4) {}
        Dim lstDetalleProforma As List(Of SEDetalleProforma) = Nothing
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        'Dim objReqCampo As Object = New Object(3) {}
        Dim objReqCampo As Object = Nothing
        Dim lnIdProforma As Integer = 0

        Try
            strNumeroProforma = txtNumeroFactura.Text

            objProforma = New SEMaestroProforma()
            CalculaTotalasFactura()
            objProforma.NumeroProforma = txtNumeroFactura.Text
            objProforma.NumFechaIngreso = Format(DPkFechaFactura.Value, "yyyyMMdd")
            objProforma.FechaIngreso = SUConversiones.FormateaFechaParaBD(DPkFechaFactura.Value.Day, DPkFechaFactura.Value.Month, DPkFechaFactura.Value.Year)
            objProforma.IdVendedor = ConvierteAInt(txtNumeroVendedor.Text)
            objProforma.IdCliente = ConvierteAInt(txtNumeroCliente.Text)
            objProforma.Cliente = txtNombreCliente.Text
            objProforma.Direccion = txtDireccion.Text
            objProforma.SubTotal = SUConversiones.ConvierteADouble(txtSubTotal.Text)
            objProforma.SubTotalCosto = SUConversiones.ConvierteADouble(txtSubTotal.Text)
            objProforma.Impuesto = SUConversiones.ConvierteADouble(txtImpVenta.Text)
            objProforma.Retencion = SUConversiones.ConvierteADouble(txtRetencion.Text)
            objProforma.Total = SUConversiones.ConvierteADouble(txtTotal.Text)
            objProforma.IdUsuario = lngRegUsuario
            objProforma.IdSucursal = lngRegAgencia
            objProforma.IdEstado = 0
            objProforma.Impreso = 0
            objProforma.IdDepartamento = SUConversiones.ConvierteAInt(txtDepartamento.Text)
            objProforma.IdNegocio = SUConversiones.ConvierteAInt(txtNegocio.Text)
            objProforma.TipoFactura = 0
            objProforma.DiasCredito = SUConversiones.ConvierteAInt(txtDias.Text)
            objProforma.TipoCambio = dblTipoCambio
            objProforma.NumeroArqueo = 0

            lstDetalleProforma = CargarDetalleProformas()
            If (objProforma Is Nothing) And (lstDetalleProforma Is Nothing) Then
                MessageBoxEx.Show("No se pudo obtener alguna de la informaci�n necesaria para guardar la Proforma", " Guardar Proforma ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If

            lnIdProforma = RNProforma.IngresaTransaccionProforma(lngRegAgencia, objProforma, lstDetalleProforma, ddlSerie.Text)
            MessageBoxEx.Show("Proforma Guardada exitosamente. ", " Guardar Proforma ", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Limpiar()
            Limpiar2()

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Sub EliminarRegistroGrid()
        Dim dblCantidad As Double = 0

        Try


            If dgvDetalle.Rows.Count > 0 Then
                intResp = MsgBox("�Est� Seguro de Eliminar el Registro?", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Eliminaci�n de Registros")

                If intResp = 6 Then

                    dblCantidad = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value)
                    txtSubTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) - (dblCantidad * SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value)), "#,##0.#0")
                    If dgvDetalle.CurrentRow.Cells("Impuesto").Value = "Si" Then
                        txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text) - Format((SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value) * dblPorcParam), "#,##0.#0"), "#,##0.#0")
                    End If
                    txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)

                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
            'MsgBox("Error: " +ex.message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try


            If lngRegAgencia = 0 Then
                Timer1.Enabled = False
                Me.Close()
            ElseIf blnUbicar = True Then
                blnUbicar = False
                Timer1.Enabled = False
                If strUbicar <> "" Then
                    Select Case intListadoAyuda
                        Case 1 : txtVendedor.Text = strUbicar : UbicarVendedor(txtVendedor.Text)
                            If txtCliente.Visible = True Then
                                txtCliente.Focus()
                            Else
                                txtNombreCliente.Focus()
                            End If
                        Case 2 : txtCliente.Text = strUbicar : UbicarCliente(txtCliente.Text)
                            If txtVendedor.Text = "" Then
                                MsgBox("El vendedor no est� asignado a este cliente.", MsgBoxStyle.Critical, "Error de Asignaci�n")
                                txtVendedor.Focus()
                            End If
                        Case 3 : txtProducto.Text = strUbicar : UbicarProducto(txtProducto.Text) : ddlRetencion.Focus()
                            'Case 4 : txtNumeroFactura.Text = strUbicar : ExtraerFactura()
                    End Select
                End If
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlRetencion.SelectedIndexChanged, ddlTipoPrecio.SelectedIndexChanged
        Try


            If sender.name = "ddlRentencion" Then
                Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%"
                If sender.selectedindex = 0 Then
                    txtRetencion.Text = "0.00"
                    txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                ElseIf sender.selectedindex = 1 Then
                    If intTipoFactura = 4 Or intTipoFactura = 8 Then
                        sender.selectedindex = 0
                    Else
                        txtRetencion.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) * dblRetParam, "#,##0.#0")
                        txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                    End If
                End If
            ElseIf sender.name = "ComboBox2" And txtProducto.Text <> "" Then
                UbicarPrecio()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombreCliente.LostFocus
        Try
            txtNombreCliente.Text = StrConv(txtNombreCliente.Text, VbStrConv.ProperCase)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try


    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumeroFactura.GotFocus, txtDias.GotFocus, txtVence.GotFocus, txtNombreVendedor.GotFocus, txtNombreCliente.GotFocus, txtProducto.GotFocus, txtCantidad.GotFocus, txtPrecio.GotFocus, txtVendedor.GotFocus, txtCliente.GotFocus, ddlRetencion.GotFocus, ddlTipoPrecio.GotFocus, DPkFechaFactura.GotFocus, txtCliente.GotFocus

        Dim strCampo As String
        Try
            Label15.Visible = False
            MenuItem5.Visible = False
            MenuItem6.Visible = False
            MenuItem7.Visible = False
            MenuItem12.Visible = False
            strCampo = ""
            Select Case sender.name.ToString
                Case "DPkFechaFactura" : strCampo = "Fecha"
                Case "txtNumeroFactura" : strCampo = "N�mero"
                    MenuItem12.Visible = True
                Case "txtDia" : strCampo = "D�as Cr�dito"
                Case "txtVencimiento" : strCampo = "Vencimiento"
                Case "txtVendedor" : strCampo = "Vendedor" : MenuItem12.Visible = True : MenuItem5.Visible = True : Label15.Visible = True
                Case "txtCliente" : strCampo = "Cliente"
                    MenuItem12.Visible = True
                    MenuItem6.Visible = True
                    Label15.Visible = True
                Case "txtProducto" : strCampo = "Producto" : MenuItem12.Visible = True : MenuItem7.Visible = True : Label15.Visible = True
                Case "txtCantidad" : strCampo = "Cantidad"
                Case "TextBox8" : strCampo = "Tipo de Precio"
                Case "ddlRentencion" : strCampo = "Retenci�n"
                Case "ddlTipoPrecio" : strCampo = "Tipo Precio"
            End Select
            UltraStatusBar1.Panels.Item(0).Text = strCampo
            UltraStatusBar1.Panels.Item(1).Text = sender.tag
            'StatusBar1.Panels.Item(0).Text = strCampo
            'StatusBar1.Panels.Item(1).Text = sender.tag
            If sender.name <> "DPkFechaFactura" Then
                sender.selectall()
            End If
            If blnUbicar Then
                ObtengoDatosListadoAyuda()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroFactura.KeyDown, txtDias.KeyDown, txtVence.KeyDown, txtNombreVendedor.KeyDown, txtNombreCliente.KeyDown, txtProducto.KeyDown, txtCantidad.KeyDown, txtPrecio.KeyDown, txtVendedor.KeyDown, txtCliente.KeyDown, ddlRetencion.KeyDown, ddlTipoPrecio.KeyDown, ddlSerie.KeyDown, DPkFechaFactura.KeyDown, txtCliente.KeyDown, txtDireccion.KeyDown, chkBonificacion.KeyDown
        Dim IndicaBonificacion As Integer = 0
        Dim lnPrecio As Decimal = 0
        Dim lnPrecioMinimoVenta As Decimal = 0
        Dim lnCantidadBonificada As Double = 0
        Dim objRespuesta As Object() = New Object(3) {}
        Dim objParametro As SEParametroEmpresa = Nothing
        Try
            objParametro = New SEParametroEmpresa()
            If e.KeyCode = Keys.Enter Then
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                Select Case sender.name.ToString
                    Case "DPkFechaFactura" : ddlSerie.Focus()
                    Case "txtNumeroFactura"
                    Case "txtDias"
                        If IsNumeric(Trim(txtDias.Text)) = False Then
                            txtDias.Text = "0"
                        End If
                        txtVence.Text = Format(DateSerial(Year(DPkFechaFactura.Value), Month(DPkFechaFactura.Value), Format(DPkFechaFactura.Value, "dd") + SUConversiones.ConvierteAInt(txtDias.Text)), "dd-MMM-yyyy")
                        txtCantidad.Focus()
                    Case "txtVendedor"
                        blnUbicar = False
                        If UbicarVendedor(txtVendedor.Text) = False Then
                            MsgBox("El C�digo de Vendedor no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtVendedor.Focus()
                            Exit Sub
                        End If
                        If txtCliente.Visible = True Then
                            txtCliente.Focus()
                        Else
                            txtNombreCliente.Focus()
                        End If
                    Case "chkBonificacion"
                        If e.KeyCode = Keys.Enter Then
                            chkBonificacion.Checked = chk
                        End If
                        ddlRetencion.Focus()
                    Case "txtCliente"
                        blnUbicar = False
                        If UbicarCliente(txtCliente.Text) = False Then
                            MsgBox("El C�digo de Cliente no est� registrado o el vendedor no es el asignado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            If txtVendedor.Text = "" Then
                                txtVendedor.Focus()
                            Else
                                txtCliente.Focus()
                            End If
                            Exit Sub
                        End If
                        txtCantidad.Focus()
                    Case "txtNombreCliente" : txtDireccion.Focus()
                    Case "txtDireccion" : txtCantidad.Focus()
                    Case "ddlSerie" : txtNumeroFactura.Focus()
                    Case "txtProducto"
                        blnUbicar = False
                        If UbicarProducto(txtProducto.Text) = False Then
                            MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                            txtProducto.Focus()
                            Exit Sub
                        End If
                        chkBonificacion.Focus()
                    Case "txtCantidad"
                        If Trim(txtCantidad.Text) <> "0" Then
                            txtProducto.Focus()
                        End If
                    Case "ddlRetencion" : Label17.Text = CStr(Format(dblRetParam * 100, "#,##0.#0")) & "%" : ddlTipoPrecio.Focus() : IIf(intFechaCambioFactura > 0, ddlTipoPrecio.SelectedIndex = 2, ddlTipoPrecio.SelectedIndex = 0) : UbicarPrecio()
                    Case "ddlTipoPrecio" : UbicarPrecio() : txtPrecio.Focus()
                    Case "txtPrecio"
                        If dgvDetalle.Rows.Count > 15 Then
                            MsgBox("Cantidad maxima para una factura es 16 Item", MsgBoxStyle.Critical, "Favor validar")
                            DPkFechaFactura.Focus()
                            Exit Sub
                        End If
                        If txtCantidad.Text = "" Then
                            Exit Sub
                        End If
                        If SUConversiones.ConvierteADouble(txtCantidad.Text) > 0 Then
                            DPkFechaFactura.Focus()
                            IndicaBonificacion = 0
                            lnPrecio = SUConversiones.ConvierteADecimal(txtPrecio.Text)
                            lnCantidadBonificada = 0
                            If chkBonificacion.Checked Then
                                IndicaBonificacion = 1
                                lnPrecio = 0
                                lnCantidadBonificada = SUConversiones.ConvierteADouble(txtCantidad.Text)
                            End If

                            'lnPrecio = ObtenerMontoAFacturarBonificadoXIdProducto(lnPrecio, lnCantidadBonificada, lngRegistro, RNProduto.ObtienePrecionMinimoxMoneda(UCase(txtProducto.Text), objParametro.MonedaFacturaCredito))
                            objRespuesta = ObtenerMontoAFacturarBonificadoXIdProducto(lnPrecio, lnCantidadBonificada, lngRegistro, RNProduto.ObtienePrecionMinimoxMoneda(UCase(txtProducto.Text), objParametro.MonedaFacturaCredito))

                            If objRespuesta(0) = 1 Then
                                MsgBox("No se puede facturar, por precio abajo del minimo.", MsgBoxStyle.Critical, "Favor validar")
                                Return
                            End If
                            'End If
                            dgvDetalle.Rows.Add(IndicaBonificacion, Format(SUConversiones.ConvierteADouble(txtCantidad.Text), "####0.#0"), UCase(strUnidad), UCase(txtProducto.Text), UCase(strDescrip), IIf(intImpuesto = 0, "No", "Si"), lnPrecio, Format(lnPrecio * SUConversiones.ConvierteADouble(txtCantidad.Text), "#,##0.#0"), lngRegistro, Format(SUConversiones.ConvierteADouble(txtPrecioCosto.Text), "#,##0.#0"))
                            txtSubTotal.Text = Format(Format((lnPrecio * SUConversiones.ConvierteADouble(txtCantidad.Text)), "#,##0.#0") + SUConversiones.ConvierteADouble(txtSubTotal.Text), "#,##0.#0")

                            If intImpuesto = 0 Then
                                txtImpVenta.Text = Format(SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                            ElseIf intImpuesto = 1 Then
                                txtImpVenta.Text = Format(Format((Format((lnPrecio * SUConversiones.ConvierteADouble(txtCantidad.Text)), "#,##0.#0") * dblPorcParam), "#,##0.#0") + SUConversiones.ConvierteADouble(txtImpVenta.Text), "#,##0.#0")
                            End If
                            If ddlRetencion.SelectedIndex = 0 Then
                                txtRetencion.Text = "0.00"
                            ElseIf ddlRetencion.SelectedIndex = 1 Then
                                txtRetencion.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) * dblRetParam, "#,##0.#0")
                            End If
                            txtTotal.Text = Format(SUConversiones.ConvierteADouble(txtSubTotal.Text) + SUConversiones.ConvierteADouble(txtImpVenta.Text) - SUConversiones.ConvierteADouble(txtRetencion.Text), "#,##0.#0")
                            Limpiar2()
                            CalculaTotalasFactura()
                        Else
                            MsgBox("No puede vender un producto con cantidad 0.00", MsgBoxStyle.Critical, "Error de Cantidad")
                            txtCantidad.Focus()
                        End If
                End Select
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DPkFechaFactura.LostFocus
        Try


            If IndicaImprimir = 0 Then
                If cnnAgro2K.State = ConnectionState.Open Then
                    cnnAgro2K.Close()
                End If
                cnnAgro2K.Open()
                cmdAgro2K.Connection = cnnAgro2K

                strQuery = ""
                strQuery = "Select tipo_cambio From prm_TipoCambio Where NumFecha = " & Format(DPkFechaFactura.Value, "yyyyMMdd") & " "
                cmdAgro2K.CommandText = strQuery
                dtrAgro2K = cmdAgro2K.ExecuteReader
                While dtrAgro2K.Read
                    dblTipoCambio = dtrAgro2K.GetValue(0)
                End While
                dtrAgro2K.Close()
                cmdAgro2K.Connection.Close()

            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub TextBox18_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.DoubleClick, txtProducto.DoubleClick
        Try
            intListadoAyuda = 0
            If MenuItem5.Visible = True Then
                intListadoAyuda = 1
            ElseIf MenuItem6.Visible = True Then
                intListadoAyuda = 2
            ElseIf MenuItem7.Visible = True Then
                intListadoAyuda = 3
            End If
            If intListadoAyuda <> 0 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub ddlSerie_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSerie.SelectedIndexChanged
        Try
            sSerie = ddlSerie.SelectedItem
            If sSerie Is Nothing Then
                sSerie = String.Empty
            End If

            ''If intTipoFactura = 1 Or intTipoFactura = 4 Then
            txtNumeroFactura.Text = sSerie + RNProforma.ObtieneNumeroProformaParametro(lngRegAgencia, sSerie, 0)
            ''End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdVendedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdVendedor.Click
        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            intListadoAyuda = 1
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdClientes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClientes.Click
        Try


            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            intListadoAyuda = 2
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdProductos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdProductos.Click
        Try
            intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
            lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
            intListadoAyuda = 3
            If intListadoAyuda = 1 Or intListadoAyuda = 2 Or intListadoAyuda = 3 Then
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub CambiaIcoImpresion(ByVal Boton As DevComponents.DotNetBar.ButtonItem)
        Try


            Boton.Tooltip = "Imprimir"
            Boton.Text = "Imprimir<F7>"
            Boton.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
            Boton.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
            Boton.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
            Boton.ImageFixedSize = New System.Drawing.Size(40, 40)
            Boton.Image = ObtieneImagen("multifunction_printer_icon.png")
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub cmdiSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Function ObtieneImagen(ByVal NombreImagen As String) As System.Drawing.Image
        Dim imagen As System.Drawing.Image
        Try
            'Dim file_name As String = Application.
            Dim NombreCarpeta As String
            Dim NombreArchivo As String = String.Empty
            NombreCarpeta = System.IO.Directory.GetCurrentDirectory()
            If NombreCarpeta.EndsWith("\bin\Debug") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin\Debug", "")
            End If
            If NombreCarpeta.EndsWith("\bin") Then
                NombreCarpeta = NombreCarpeta.Replace("\bin", "")
            End If
            NombreCarpeta = NombreCarpeta & "\Imagenes\"
            NombreArchivo = NombreCarpeta & NombreImagen

            'imagen = Image.FromFile(NombreArchivo)
            imagen = System.Drawing.Image.FromFile(NombreArchivo)

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            Throw ex
        End Try
        Return imagen
    End Function

    Private Sub cmdiLimpiar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Try
            Limpiar()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub frmProformas_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try


            If e.KeyCode = Keys.Escape Then
                Me.Close()
            ElseIf e.KeyCode = Keys.F3 Then
                intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                If intTipoFactura = 3 Or intTipoFactura = 6 Then
                    ''If MSFlexGrid2.Rows <= 1 Then
                    ''    MsgBox("No hay productos a anular", MsgBoxStyle.Critical, "Error de Datos")
                    ''    Exit Sub
                    ''End If
                    ''Anular()
                    MsgBox("Factura fue Anulada Satisfactoriamente.", MsgBoxStyle.Exclamation, "Anulaci�n de Factura")
                    'txtNumeroFactura.Focus()
                    ddlSerie.Focus()
                ElseIf intTipoFactura = 2 Or intTipoFactura = 5 Or intTipoFactura = 7 Or intTipoFactura = 8 Or intTipoFactura = 9 Or intTipoFactura = 10 Then
                    '                txtSerie.Enabled = True
                    ''If MSFlexGrid2.Rows <= 1 Then
                    ''    MsgBox("No hay productos a facturar", MsgBoxStyle.Critical, "Error de Datos")
                    ''    txtCantidad.Focus()
                    ''    Exit Sub
                    ''End If
                    ''Guardar()
                End If
            ElseIf e.KeyCode = Keys.F4 Then
                Limpiar()
            ElseIf e.KeyCode = Keys.F5 Then
                If MenuItem5.Visible = True Then
                    intListadoAyuda = 1
                ElseIf MenuItem6.Visible = True Then
                    intListadoAyuda = 2
                ElseIf MenuItem7.Visible = True Then
                    intListadoAyuda = 3
                End If
                Dim frmNew As New frmListadoAyuda
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                frmNew.ShowDialog()
            ElseIf e.KeyCode = Keys.F7 Then
                ''If MSFlexGrid2.Rows <= 1 Then
                ''    MsgBox("No hay productos a facturar", MsgBoxStyle.Critical, "Error de Datos")
                ''    txtCantidad.Focus()
                ''    Exit Sub
                ''End If
                ''intTipoFactura = SUConversiones.ConvierteAInt(Label18.Text)
                ''lngRegAgencia = SUConversiones.ConvierteAInt(Label19.Text)
                ''Guardar()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim rpt As New actrptViewer
        Try
            If dgvDetalle.Rows.Count > 0 Then
                IngresarProforma()

                intRptProformas = 1
                strQuery = "spConsultarProformaXNumeroProforma " & strNumeroProforma
                rpt.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                rpt.Show()
                strNumeroProforma = String.Empty

            Else
                MessageBoxEx.Show("No hay detalle a guardar", "Guardar Proforma", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        Dim i As Integer = 0
        Dim TotalAnterior As Double = 0
        Dim ImpuestoAnterior As Double = 0
        Dim lnTotalItem As Double = 0
        Dim lnBonificacion As Integer = 0
        Dim lnCantidad As Double = 0
        Dim lnPrecio As Double = 0
        Dim lnPrecioMinimoVenta As Double = 0
        Dim objParametro As SEParametroEmpresa = Nothing
        Dim objRespuesta As Object() = New Object(3) {}
        Dim lnCantidadBonificada As Double = 0
        Dim lnIdProducto As Integer = 0
        Dim lsCodigoProducto As String = String.Empty
        Dim lnCantidadAnterior As Integer = 0
        Try

            objParametro = RNParametroEmpresa.ObtieneParametroEmpreaEN()

            If dgvDetalle.CurrentCell.ColumnIndex = 1 Then
                lnBonificacion = 0
                lnBonificacion = SUConversiones.ConvierteAInt(dgvDetalle.CurrentRow.Cells("EsBonificacion").Value)
                lnPrecio = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value)
                lnIdProducto = SUConversiones.ConvierteAInt(dgvDetalle.CurrentRow.Cells("IdProducto").Value)
                lsCodigoProducto = SUConversiones.ConvierteAString(dgvDetalle.CurrentRow.Cells("Codigo").Value)
                lnCantidadBonificada = 0
                If lnBonificacion = 1 Then
                    lnPrecio = 0
                    lnCantidadBonificada = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value)
                End If
                objRespuesta = ObtenerMontoAFacturarBonificadoXIdProducto(lnPrecio, lnCantidadBonificada, lnIdProducto, RNProduto.ObtienePrecionMinimoxMoneda(lsCodigoProducto, objParametro.MonedaFacturaCredito))
                If objRespuesta(0) = 1 Then
                    dgvDetalle.CurrentRow.Cells("Cantidad").Value = valorAnt
                    MsgBox("No se puede facturar, por precio abajo del minimo.", MsgBoxStyle.Critical, "Favor validar")
                    Return
                End If

                dgvDetalle.CurrentRow.Cells("Total").Value = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value) * lnPrecio
                CalculaTotalasFactura()
                Limpiar2()
            ElseIf dgvDetalle.CurrentCell.ColumnIndex = 6 Then
                lnBonificacion = 0
                lnBonificacion = SUConversiones.ConvierteAInt(dgvDetalle.CurrentRow.Cells("EsBonificacion").Value)
                lnPrecio = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Valor").Value)
                lnIdProducto = SUConversiones.ConvierteAInt(dgvDetalle.CurrentRow.Cells("IdProducto").Value)
                lsCodigoProducto = SUConversiones.ConvierteAString(dgvDetalle.CurrentRow.Cells("Codigo").Value)
                lnCantidadBonificada = 0
                If lnBonificacion = 1 Then
                    lnPrecio = 0
                    lnCantidadBonificada = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value)
                End If
                objRespuesta = ObtenerMontoAFacturarBonificadoXIdProducto(lnPrecio, lnCantidadBonificada, lnIdProducto, RNProduto.ObtienePrecionMinimoxMoneda(lsCodigoProducto, objParametro.MonedaFacturaCredito))
                If objRespuesta(0) = 1 Then
                    dgvDetalle.CurrentRow.Cells("Cantidad").Value = valorAnt
                    MsgBox("No se puede facturar, por precio abajo del minimo.", MsgBoxStyle.Critical, "Favor validar")
                    Return
                End If

                dgvDetalle.CurrentRow.Cells("Total").Value = SUConversiones.ConvierteADouble(dgvDetalle.CurrentRow.Cells("Cantidad").Value) * lnPrecio
                CalculaTotalasFactura()
                Limpiar2()
            End If
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub chkBonificacion_CheckedChanging(sender As Object, e As CheckBoxXChangeEventArgs) Handles chkBonificacion.CheckedChanging
        Try
            chk = chkBonificacion.Checked
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try
    End Sub

    Private Sub dgvDetalle_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDetalle.DoubleClick
        Try
            EliminarRegistroGrid()
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub

    Private Sub dgvDetalle_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvDetalle.CellBeginEdit
        Try
            valorAnt = dgvDetalle.CurrentRow.Cells("Cantidad").Value

        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, "", ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresaError(objError)
            ' Throw ex
            MsgBox("Error: " + ex.Message, MsgBoxStyle.Critical, "Error de Datos")
        End Try

    End Sub
End Class