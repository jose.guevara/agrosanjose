
Imports GVSoft.SistemaGerencial.ReglaNegocio
Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports DevComponents.DotNetBar

Public Class frmCierrePeriodo

    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "frmCierrePeriodo"


    Private Sub SuperTabControl1_SelectedTabChanged(sender As System.Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles SuperTabControl1.SelectedTabChanged

    End Sub

    Private Sub ToolbarB_EjecutarPrecierre_Click(sender As System.Object, e As System.EventArgs) Handles ToolbarB_EjecutarPrecierre.Click
        Dim lsFechaInicio As String = String.Empty
        Dim lsFechaFin As String = String.Empty
        Try

            If DateF_FechaIni.Text.Length <= 0 Then
                MessageBoxEx.Show("Favor indicar la fecha de inicio del periodo", "Cierre Periodo", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
            If DateF_FechaIni.Text.Length <= 0 Then
                MessageBoxEx.Show("Favor indicar la fecha de fin del periodo", "Cierre Periodo", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
            lsFechaInicio = DateF_FechaIni.Value.ToString("yyyyMMdd")   '  New DateTime(DateF_FechaIni.Value.Year,DateF_FechaIni.Value.M)
            lsFechaFin = DateF_FechaFin.Value.ToString("yyyyMMdd")
            RNCierrePeriodo.ProcesaPrecierre(lsFechaInicio, lsFechaFin)
            MessageBoxEx.Show(" Precierre ejecutado exitosamente ", "Cierre Periodo", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, String.Empty, ex.Message.ToString(), EventLogEntryType.Error)
            SNError.IngresarError(objError)
            MessageBoxEx.Show("EjecutarPrecierre  - Error: " + ex.Message.ToString(), "Cierre Periodo", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class