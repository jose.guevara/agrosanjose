Imports System.Data.SqlClient
Imports System.Text

Public Class frmProductoMovim
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents UltraStatusBar1 As Infragistics.Win.UltraWinStatusBar.UltraStatusBar
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem4 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Label14 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProductoMovim))
        Dim UltraStatusPanel1 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Dim UltraStatusPanel2 As Infragistics.Win.UltraWinStatusBar.UltraStatusPanel = New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.ComboBox6 = New System.Windows.Forms.ComboBox
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.UltraStatusBar1 = New Infragistics.Win.UltraWinStatusBar.UltraStatusBar
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdOK = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem4 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 3
        Me.MenuItem12.Text = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        Me.ImageList1.Images.SetKeyName(5, "")
        Me.ImageList1.Images.SetKeyName(6, "")
        '
        'Timer1
        '
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.ComboBox6)
        Me.GroupBox1.Controls.Add(Me.ComboBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox9)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.TextBox8)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.ComboBox4)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.ComboBox3)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 77)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(632, 192)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de los Movimientos"
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(136, 32)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(96, 16)
        Me.Label14.TabIndex = 42
        Me.Label14.Text = "<F5> AYUDA"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ComboBox6
        '
        Me.ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox6.Location = New System.Drawing.Point(608, 152)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(8, 21)
        Me.ComboBox6.TabIndex = 33
        Me.ComboBox6.Visible = False
        '
        'ComboBox5
        '
        Me.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox5.Location = New System.Drawing.Point(312, 128)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(8, 21)
        Me.ComboBox5.TabIndex = 32
        Me.ComboBox5.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(296, 160)
        Me.TextBox9.MaxLength = 12
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(104, 20)
        Me.TextBox9.TabIndex = 30
        Me.TextBox9.Tag = "Cuenta Haber"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(208, 160)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(85, 13)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "Cuenta Haber"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(88, 160)
        Me.TextBox8.MaxLength = 12
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(104, 20)
        Me.TextBox8.TabIndex = 28
        Me.TextBox8.Tag = "Cuenta Debe"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 160)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Cuenta Debe"
        '
        'ComboBox4
        '
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.Location = New System.Drawing.Point(400, 128)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(224, 21)
        Me.ComboBox4.TabIndex = 26
        Me.ComboBox4.Tag = "Agencia Destino del Movimiento"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(328, 128)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "A la Agencia"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Location = New System.Drawing.Point(88, 128)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(224, 21)
        Me.ComboBox3.TabIndex = 24
        Me.ComboBox3.Tag = "Agencia Origen del Movimiento"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 128)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(87, 13)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "De la Agencia"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(552, 96)
        Me.TextBox7.MaxLength = 12
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(72, 20)
        Me.TextBox7.TabIndex = 22
        Me.TextBox7.Tag = "C�digo asignado a la clase"
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(392, 96)
        Me.TextBox6.MaxLength = 12
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(72, 20)
        Me.TextBox6.TabIndex = 21
        Me.TextBox6.Tag = "C�digo asignado a la clase"
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(480, 96)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(67, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Costo US$"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(328, 96)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Costo C$"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(240, 96)
        Me.TextBox5.MaxLength = 12
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(72, 20)
        Me.TextBox5.TabIndex = 18
        Me.TextBox5.Tag = "Cantidad del Producto"
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(184, 96)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Cantidad"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(88, 96)
        Me.TextBox4.MaxLength = 12
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(80, 20)
        Me.TextBox4.TabIndex = 16
        Me.TextBox4.Tag = "Total de la Factura"
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Total Factura"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Items.AddRange(New Object() {"COR"})
        Me.ComboBox2.Location = New System.Drawing.Point(536, 64)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(64, 21)
        Me.ComboBox2.TabIndex = 14
        Me.ComboBox2.Tag = "Moneda del Movimiento"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(264, 64)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(200, 21)
        Me.ComboBox1.TabIndex = 13
        Me.ComboBox1.Tag = "Tipo de Movimiento"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(80, 64)
        Me.TextBox3.MaxLength = 12
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(96, 20)
        Me.TextBox3.TabIndex = 12
        Me.TextBox3.Tag = "N�mero de Documento"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(520, 32)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker1.TabIndex = 6
        Me.DateTimePicker1.Tag = "Fecha de Ingres"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(480, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Fecha"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 32)
        Me.TextBox1.MaxLength = 12
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(72, 20)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Tag = "C�digo asignado a la clase"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "C�digo"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(128, 32)
        Me.TextBox2.MaxLength = 30
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(336, 20)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Tag = "Descripci�n del c�digo de la clase ingresada"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Documento"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(480, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Moneda"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(192, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Movimiento"
        '
        'UltraStatusBar1
        '
        Me.UltraStatusBar1.Location = New System.Drawing.Point(0, 275)
        Me.UltraStatusBar1.Name = "UltraStatusBar1"
        UltraStatusPanel2.Width = 400
        Me.UltraStatusBar1.Panels.AddRange(New Infragistics.Win.UltraWinStatusBar.UltraStatusPanel() {UltraStatusPanel1, UltraStatusPanel2})
        Me.UltraStatusBar1.Size = New System.Drawing.Size(643, 23)
        Me.UltraStatusBar1.TabIndex = 17
        Me.UltraStatusBar1.Text = "UltraStatusBar1"
        Me.UltraStatusBar1.ViewStyle = Infragistics.Win.UltraWinStatusBar.ViewStyle.Office2007
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdOK, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem4, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(643, 69)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.bHerramientas.TabIndex = 48
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdOK.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdOK.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Text = "Guardar<F3>"
        Me.cmdOK.Tooltip = "Aplicar Cambios"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "  "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar<F4>"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem4
        '
        Me.LabelItem4.Name = "LabelItem4"
        Me.LabelItem4.Text = "  "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.cmdiSalir.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir<ESC>"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'frmProductoMovim
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(643, 298)
        Me.Controls.Add(Me.bHerramientas)
        Me.Controls.Add(Me.UltraStatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmProductoMovim"
        Me.Text = "frmProductoMovim"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim lngProducto As Long
    Public cboCollection As New Collection
    Public txtCollection As New Collection

    Private Sub frmProductoMovim_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        blnClear = False
        cboCollection.Add(ComboBox1)
        cboCollection.Add(ComboBox2)
        cboCollection.Add(ComboBox3)
        cboCollection.Add(ComboBox4)
        cboCollection.Add(ComboBox5)
        cboCollection.Add(ComboBox6)
        txtCollection.Add(TextBox1)
        txtCollection.Add(TextBox2)
        txtCollection.Add(TextBox3)
        txtCollection.Add(TextBox4)
        txtCollection.Add(TextBox5)
        txtCollection.Add(TextBox6)
        txtCollection.Add(TextBox7)
        txtCollection.Add(TextBox8)
        txtCollection.Add(TextBox9)
        ComboBox1.Items.Clear()
        If intAgenciaTraslado = 0 Then
            ComboBox1.Items.Add("Devoluci�n Cliente")
            ComboBox1.Items.Add("Entrada Compra")
            ComboBox1.Items.Add("Remisi�n Cliente")
            ComboBox1.Items.Add("Salida por Baja")
            ComboBox1.Items.Add("Traslado")
            ComboBox1.Items.Add("Entrada Agencia")
            ComboBox1.Items.Add("Salida Agencia")
            ComboBox1.Items.Add("Corregir Costos")
            ComboBox1.Items.Add("Correcci�n Kardex Entrada")
            ComboBox1.Items.Add("Correcci�n Kardex Salida")
            ComboBox1.Items.Add("Correcci�n Inventario F�sico")
        Else
            ComboBox1.Items.Add("Entrada Agencia")
            ComboBox1.Items.Add("Salida Agencia")
            ComboBox1.Items.Add("Correcci�n Inventario F�sico")
            Label12.Visible = False
            Label13.Visible = False
            TextBox8.Visible = False
            TextBox9.Visible = False
        End If
        Label11.Visible = False
        ComboBox4.Visible = False
        Iniciar()
        Limpiar()

    End Sub

    Private Sub frmProductoMovim_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        ElseIf e.KeyCode = Keys.F5 Then
            If Label14.Visible = True Then
                intListadoAyuda = 3
                Dim frmNew As New frmListadoAyuda
                Timer1.Interval = 200
                Timer1.Enabled = True
                frmNew.ShowDialog()
            End If
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem12.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs)

        'Select Case ToolBar1.Buttons.IndexOf(e.Button)
        '    Case 0
        '        Guardar()
        '    Case 1, 2
        '        Limpiar()
        '    Case 3
        '        Me.Close()
        'End Select

    End Sub

    Sub Guardar()

        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngProductoMovim", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter
        Dim prmTmp18 As New SqlParameter
        Dim prmTmp19 As New SqlParameter
        Dim prmTmp20 As New SqlParameter
        Dim strTipoMov As String
        Dim lngNumTiempo, lngNumFecha As Long
        Dim lngAgencia01, lngAgencia02 As Byte

        If ComboBox3.SelectedItem = "" Then
            ComboBox3.Focus()
            MsgBox("Tiene que escoger la agencia a trasladar.  Verifique.", MsgBoxStyle.Critical, "Error de Dato")
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        If ComboBox4.SelectedItem = "" And ComboBox1.SelectedItem = "Traslado" Then
            ComboBox4.Focus()
            MsgBox("Tiene que escoger la agencia a trasladar.  Verifique.", MsgBoxStyle.Critical, "Error de Dato")
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        strTipoMov = ""
        lngNumFecha = 0
        lngRegistro = 0
        lngAgencia01 = 0
        lngAgencia02 = 0
        lngNumTiempo = 0
        lngNumTiempo = Format(Now, "Hmmss")
        lngNumFecha = Format(DateTimePicker1.Value, "yyyyMMdd")
        ComboBox5.SelectedIndex = ComboBox3.SelectedIndex
        lngAgencia01 = ComboBox5.SelectedItem
        ComboBox6.SelectedIndex = ComboBox4.SelectedIndex
        lngAgencia02 = ComboBox6.SelectedItem
        If intAgenciaTraslado = 0 Then
            Select Case ComboBox1.SelectedIndex
                Case 0 : strTipoMov = "DC"
                Case 1 : strTipoMov = "EC"
                Case 2 : strTipoMov = "RC"
                Case 3 : strTipoMov = "SB"
                Case 4 : strTipoMov = "TO"
                Case 5 : strTipoMov = "EA"
                Case 6 : strTipoMov = "SA"
                Case 7 : strTipoMov = "CC"
                Case 8 : strTipoMov = "CKE"
                Case 9 : strTipoMov = "CKS"
                Case 10 : strTipoMov = "IF"
            End Select
        Else
            Select Case ComboBox1.SelectedIndex
                Case 0 : strTipoMov = "EA"
                Case 1 : strTipoMov = "SA"
            End Select
        End If
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@lngNumFecha"
            .SqlDbType = SqlDbType.Decimal
            .Value = lngNumFecha
        End With
        With prmTmp03
            .ParameterName = "@lngNumTiempo"
            .SqlDbType = SqlDbType.Int
            .Value = lngNumTiempo
        End With
        With prmTmp04
            .ParameterName = "@lngAgencia"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngAgencia01
        End With
        With prmTmp05
            .ParameterName = "@lngProducto"
            .SqlDbType = SqlDbType.SmallInt
            .Value = lngProducto
        End With
        With prmTmp06
            .ParameterName = "@strFecha"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp07
            .ParameterName = "@strDocumento"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox3.Text
        End With
        With prmTmp08
            .ParameterName = "@strTipoMov"
            .SqlDbType = SqlDbType.VarChar
            .Value = strTipoMov
        End With
        With prmTmp09
            .ParameterName = "@strMoneda"
            .SqlDbType = SqlDbType.VarChar
            .Value = "COR"
        End With
        With prmTmp10
            .ParameterName = "@dblFactura"
            .SqlDbType = SqlDbType.Decimal
            If intAgenciaTraslado = 0 Then
                .Value = TextBox4.Text
            Else
                .Value = 0
            End If
        End With
        With prmTmp11
            .ParameterName = "@dblCant"
            .SqlDbType = SqlDbType.Decimal
            If strTipoMov = "CC" Then
                .Value = 0
            Else
                .Value = TextBox5.Text
            End If
        End With
        With prmTmp12
            .ParameterName = "@dblCosto"
            .SqlDbType = SqlDbType.Decimal
            If intAgenciaTraslado = 0 Then
                If strTipoMov = "CC" Then
                    .Value = 0
                Else
                    .Value = TextBox6.Text
                End If
            Else
                .Value = 0
            End If
        End With
        With prmTmp13
            .ParameterName = "@dblSaldo"
            .SqlDbType = SqlDbType.Decimal
            If intAgenciaTraslado = 0 Then
                .Value = TextBox4.Text
            Else
                .Value = 0
            End If
        End With
        With prmTmp14
            .ParameterName = "@strDebe"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox8.Text
        End With
        With prmTmp15
            .ParameterName = "@strHaber"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox9.Text
        End With
        With prmTmp16
            .ParameterName = "@lngUsuario"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngRegUsuario
        End With
        With prmTmp17
            .ParameterName = "@dblCantConv"
            .SqlDbType = SqlDbType.Decimal
            .Value = TextBox5.Text
        End With
        With prmTmp18
            .ParameterName = "@lngProductoConv"
            .SqlDbType = SqlDbType.SmallInt
            .Value = lngProducto
        End With
        With prmTmp19
            .ParameterName = "@lngAgenciaConv"
            .SqlDbType = SqlDbType.TinyInt
            .Value = lngAgencia02
        End With
        With prmTmp20
            .ParameterName = "@intOrigen"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .Parameters.Add(prmTmp18)
            .Parameters.Add(prmTmp19)
            .Parameters.Add(prmTmp20)
            .CommandType = CommandType.StoredProcedure
        End With
        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            cmdTmp.ExecuteNonQuery()
            MsgBox("Registro Ingresado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            Limpiar()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub Limpiar()

        DateTimePicker1.Value = Now
        For intIncr = 1 To cboCollection.Count
            cboCollection.Item(intIncr).SelectedIndex = -1
        Next
        For intIncr = 1 To txtCollection.Count
            txtCollection.Item(intIncr).Text = ""
        Next
        TextBox4.Text = "0.00"
        TextBox5.Text = "0.00"
        TextBox6.Text = "0.00"
        TextBox7.Text = "0.00"
        ComboBox2.SelectedIndex = 0
        TextBox1.Focus()

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        If intAgenciaDefecto = 1 Then
            strQuery = ""
            strQuery = "Select a.Registro, a.Descripcion From prm_Agencias a, prm_UsuariosAgencias u "
            strQuery = strQuery + "Where a.registro = u.agenregistro and u.usrregistro = " & lngRegUsuario & ""
        ElseIf intAgenciaDefecto <> 1 Then
            strQuery = ""
            strQuery = "Select Registro, Descripcion From prm_Agencias"
        End If
        'strQuery = "Select Registro, Descripcion From prm_Agencias"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox3.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox4.Items.Add(dtrAgro2K.GetValue(1))
            ComboBox5.Items.Add(dtrAgro2K.GetValue(0))
            ComboBox6.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Function UbicarCodigo(ByVal strDato As String) As Boolean

        TextBox2.Text = ""
        lngProducto = 0
        strQuery = ""
        strQuery = "Select p.Registro, p.Descripcion, u.codigo from prm_Productos p, prm_Unidades u "
        strQuery = strQuery + "Where p.regunidad = u.registro and p.codigo = '" & strDato & "'"
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            lngProducto = dtrAgro2K.GetValue(0)
            TextBox2.Text = dtrAgro2K.GetValue(1) & " " & dtrAgro2K.GetValue(2)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
        If TextBox2.Text = "" Then
            UbicarCodigo = False
        Else
            UbicarCodigo = True
        End If
        DateTimePicker1.Focus()

    End Function

    Private Sub TextBox1_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, ComboBox1.GotFocus, DateTimePicker1.GotFocus

        Label14.Visible = False
        If sender.name = "TextBox1" Then
            Label14.Visible = True
        End If
        'StatusBar1.Panels.Item(0).Text = IIf(sender.name = "TextBox1", "C�digo", IIf(sender.name = "TextBox2", "Descripci�n", "Estado"))
        UltraStatusBar1.Panels.Item(0).Text = IIf(sender.name = "TextBox1", "C�digo", IIf(sender.name = "TextBox2", "Descripci�n", "Estado"))
        'StatusBar1.Panels.Item(1).Text = sender.tag
        UltraStatusBar1.Panels.Item(1).Text = sender.tag
        If sender.name <> "DateTimePicker1" Then
            sender.selectall()
        End If

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown, ComboBox2.KeyDown, ComboBox3.KeyDown, ComboBox4.KeyDown, TextBox1.KeyDown, TextBox3.KeyDown, TextBox4.KeyDown, TextBox5.KeyDown, TextBox6.KeyDown, TextBox7.KeyDown, TextBox8.KeyDown, TextBox9.KeyDown, DateTimePicker1.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "TextBox1" : DateTimePicker1.Focus()
                    blnUbicar = False
                    If UbicarCodigo(TextBox1.Text) = False Then
                        MsgBox("El C�digo de Producto no est� registrado", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                        TextBox1.Focus()
                    End If
                Case "DateTimePicker1" : TextBox3.Focus()
                Case "TextBox3" : ComboBox1.Focus()
                Case "ComboBox1" : ComboBox2.Focus()
                Case "ComboBox2" : TextBox4.Focus()
                Case "TextBox4" : TextBox5.Focus()
                    If IsNumeric(Trim(TextBox4.Text)) = False Then
                        MsgBox("El Valor Ingresado Tiene que Ser Numerico.", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                        TextBox4.Text = "0.00"
                        TextBox4.Focus()
                    End If
                Case "TextBox5" : TextBox6.Focus()
                    If IsNumeric(Trim(TextBox5.Text)) = False Then
                        MsgBox("El Valor Ingresado Tiene que Ser Numerico.", MsgBoxStyle.Critical, "Error en Ingreso de Dato")
                        TextBox5.Text = "0.00"
                        TextBox5.Focus()
                    Else
                        If CDbl(TextBox4.Text) > 0 Then
                            If ConvierteADouble(TextBox5.Text.Trim()) = 0 Then
                                TextBox5.Text = 1
                            End If
                            TextBox6.Text = Format(CDbl(TextBox4.Text) / CDbl(TextBox5.Text), "#,##0.#0")
                        End If
                        ComboBox3.Focus()
                    End If
                Case "TextBox6" : TextBox7.Focus()
                Case "TextBox7" : ComboBox3.Focus()
                Case "ComboBox3"
                    If ComboBox4.Visible = True Then
                        ComboBox4.Focus()
                    Else
                        TextBox8.Focus()
                    End If
                    If ComboBox1.SelectedIndex <> 1 And ComboBox1.SelectedIndex <> 5 And ComboBox1.SelectedIndex <> 7 And ComboBox1.SelectedIndex <> 8 Then
                        Dim cmdTmp As New SqlCommand("sp_DetalleProducto", cnnAgro2K)
                        Dim prmTmp01 As New SqlParameter
                        Dim prmTmp02 As New SqlParameter

                        ComboBox5.SelectedIndex = ComboBox3.SelectedIndex
                        lngRegAgencia = ComboBox5.SelectedItem
                        With prmTmp01
                            .ParameterName = "@prodregistro"
                            .SqlDbType = SqlDbType.BigInt
                            .Value = lngProducto
                        End With
                        With prmTmp02
                            .ParameterName = "@agenciareg"
                            .SqlDbType = SqlDbType.BigInt
                            .Value = lngRegAgencia
                        End With
                        With cmdTmp
                            .Parameters.Add(prmTmp01)
                            .Parameters.Add(prmTmp02)
                            .CommandType = CommandType.StoredProcedure
                        End With
                        If cmdTmp.Connection.State = ConnectionState.Open Then
                            cmdTmp.Connection.Close()
                        End If
                        If cnnAgro2K.State = ConnectionState.Open Then
                            cnnAgro2K.Close()
                        End If
                        cnnAgro2K.Open()
                        cmdTmp.Connection = cnnAgro2K

                        dtrAgro2K = cmdTmp.ExecuteReader
                        While dtrAgro2K.Read
                            TextBox6.Text = dtrAgro2K.GetValue(0)
                        End While
                        dtrAgro2K.Close()
                        cmdAgro2K.Connection.Close()
                        cnnAgro2K.Close()
                        TextBox4.Text = Format(CDbl(TextBox5.Text) * CDbl(TextBox6.Text), "#,##0.#0")
                    End If
                Case "ComboBox4" : TextBox8.Focus()
                Case "TextBox8" : TextBox9.Focus()
                Case "TextBox9" : TextBox1.Focus()
            End Select
        End If

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

        If intAgenciaTraslado = 0 Then
            If ComboBox1.SelectedIndex <> 4 Then
                Label11.Visible = False
                ComboBox4.Visible = False
            ElseIf ComboBox1.SelectedIndex = 4 Then
                Label11.Visible = True
                ComboBox4.Visible = True
            End If
            If ComboBox1.SelectedIndex >= 0 And ComboBox1.SelectedIndex <= 2 Then
                Label10.Text = "A la Agencia"
            Else
                Label10.Text = "De la Agencia"
            End If
        Else
            If ComboBox1.SelectedIndex = 0 Then
                Label10.Text = "A la Agencia"
            Else
                Label10.Text = "De la Agencia"
            End If
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                TextBox1.Text = strUbicar
                UbicarCodigo(TextBox1.Text)
            End If
        End If

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Guardar()
    End Sub

    Private Sub cmdiLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiLimpiar.Click
        Limpiar()
    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub
End Class
