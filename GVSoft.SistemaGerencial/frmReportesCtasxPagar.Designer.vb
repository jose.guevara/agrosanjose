<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportesCtasxPagar
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportesCtasxPagar))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.bHerramientas = New DevComponents.DotNetBar.Bar
        Me.cmdExportar = New DevComponents.DotNetBar.ButtonX
        Me.cmdiExcel = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiHtml = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiPdf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiRtf = New DevComponents.DotNetBar.ButtonItem
        Me.cmdiTiff = New DevComponents.DotNetBar.ButtonItem
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiLimpiar = New DevComponents.DotNetBar.ButtonItem
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem
        Me.cmdiSalir = New DevComponents.DotNetBar.ButtonItem
        Me.cmdReportes = New DevComponents.DotNetBar.ButtonX
        Me.Label18 = New System.Windows.Forms.Label
        Me.cbReportes = New DevComponents.DotNetBar.Controls.ComboBoxEx
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdSeleccion = New DevComponents.DotNetBar.ButtonX
        Me.dgvProveedores = New DevComponents.DotNetBar.Controls.DataGridViewX
        Me.ckSeleccion = New DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn
        Me.NombreProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Direccion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Saldo = New DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtNombreProveedor = New DevComponents.DotNetBar.Controls.TextBoxX
        Me.ListBox18 = New System.Windows.Forms.ListBox
        Me.ListBox17 = New System.Windows.Forms.ListBox
        Me.ListBox16 = New System.Windows.Forms.ListBox
        Me.ListBox14 = New System.Windows.Forms.ListBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.ListBox8 = New System.Windows.Forms.ListBox
        Me.ListBox7 = New System.Windows.Forms.ListBox
        Me.ListBox6 = New System.Windows.Forms.ListBox
        Me.ListBox5 = New System.Windows.Forms.ListBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.ListBox4 = New System.Windows.Forms.ListBox
        Me.ListBox3 = New System.Windows.Forms.ListBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bHerramientas.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'bHerramientas
        '
        Me.bHerramientas.AntiAlias = True
        Me.bHerramientas.Controls.Add(Me.cmdExportar)
        Me.bHerramientas.Dock = System.Windows.Forms.DockStyle.Top
        Me.bHerramientas.DockedBorderStyle = DevComponents.DotNetBar.eBorderType.DoubleLine
        Me.bHerramientas.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1, Me.LabelItem1, Me.cmdiLimpiar, Me.LabelItem2, Me.cmdiSalir})
        Me.bHerramientas.Location = New System.Drawing.Point(0, 0)
        Me.bHerramientas.MinimumSize = New System.Drawing.Size(0, 50)
        Me.bHerramientas.Name = "bHerramientas"
        Me.bHerramientas.Size = New System.Drawing.Size(814, 55)
        Me.bHerramientas.Stretch = True
        Me.bHerramientas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bHerramientas.TabIndex = 38
        Me.bHerramientas.TabStop = False
        Me.bHerramientas.Text = "Exportar"
        '
        'cmdExportar
        '
        Me.cmdExportar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdExportar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdExportar.Image = CType(resources.GetObject("cmdExportar.Image"), System.Drawing.Image)
        Me.cmdExportar.Location = New System.Drawing.Point(6, 6)
        Me.cmdExportar.Name = "cmdExportar"
        Me.cmdExportar.Size = New System.Drawing.Size(75, 42)
        Me.cmdExportar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdExportar.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmdiExcel, Me.cmdiHtml, Me.cmdiPdf, Me.cmdiRtf, Me.cmdiTiff})
        Me.cmdExportar.TabIndex = 0
        Me.cmdExportar.Tooltip = "<b><font color=""#17365D"">Exportar Datos</font></b>"
        '
        'cmdiExcel
        '
        Me.cmdiExcel.GlobalItem = False
        Me.cmdiExcel.Image = CType(resources.GetObject("cmdiExcel.Image"), System.Drawing.Image)
        Me.cmdiExcel.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiExcel.Name = "cmdiExcel"
        Me.cmdiExcel.Text = "Excel"
        '
        'cmdiHtml
        '
        Me.cmdiHtml.GlobalItem = False
        Me.cmdiHtml.Image = CType(resources.GetObject("cmdiHtml.Image"), System.Drawing.Image)
        Me.cmdiHtml.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiHtml.Name = "cmdiHtml"
        Me.cmdiHtml.Text = "HTML"
        '
        'cmdiPdf
        '
        Me.cmdiPdf.GlobalItem = False
        Me.cmdiPdf.Image = CType(resources.GetObject("cmdiPdf.Image"), System.Drawing.Image)
        Me.cmdiPdf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiPdf.Name = "cmdiPdf"
        Me.cmdiPdf.Text = "PDF"
        '
        'cmdiRtf
        '
        Me.cmdiRtf.GlobalItem = False
        Me.cmdiRtf.Image = CType(resources.GetObject("cmdiRtf.Image"), System.Drawing.Image)
        Me.cmdiRtf.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiRtf.Name = "cmdiRtf"
        Me.cmdiRtf.Text = "RTF"
        '
        'cmdiTiff
        '
        Me.cmdiTiff.GlobalItem = False
        Me.cmdiTiff.Image = CType(resources.GetObject("cmdiTiff.Image"), System.Drawing.Image)
        Me.cmdiTiff.ImageFixedSize = New System.Drawing.Size(20, 20)
        Me.cmdiTiff.Name = "cmdiTiff"
        Me.cmdiTiff.Text = "TIFF"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmdExportar
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = " "
        '
        'cmdiLimpiar
        '
        Me.cmdiLimpiar.FontBold = True
        Me.cmdiLimpiar.Image = CType(resources.GetObject("cmdiLimpiar.Image"), System.Drawing.Image)
        Me.cmdiLimpiar.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiLimpiar.Name = "cmdiLimpiar"
        Me.cmdiLimpiar.Text = "Limpar"
        Me.cmdiLimpiar.Tooltip = "Limpiar"
        '
        'LabelItem2
        '
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = " "
        '
        'cmdiSalir
        '
        Me.cmdiSalir.FontBold = True
        Me.cmdiSalir.Image = CType(resources.GetObject("cmdiSalir.Image"), System.Drawing.Image)
        Me.cmdiSalir.ImageFixedSize = New System.Drawing.Size(40, 40)
        Me.cmdiSalir.Name = "cmdiSalir"
        Me.cmdiSalir.Text = "Salir"
        Me.cmdiSalir.Tooltip = "Salir"
        '
        'cmdReportes
        '
        Me.cmdReportes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdReportes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdReportes.Image = CType(resources.GetObject("cmdReportes.Image"), System.Drawing.Image)
        Me.cmdReportes.ImageFixedSize = New System.Drawing.Size(30, 30)
        Me.cmdReportes.Location = New System.Drawing.Point(403, 60)
        Me.cmdReportes.Name = "cmdReportes"
        Me.cmdReportes.Size = New System.Drawing.Size(55, 36)
        Me.cmdReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdReportes.TabIndex = 43
        Me.cmdReportes.Tooltip = "<b><font color=""#17365D"">Generar Reporte</font></b>"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(12, 71)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(58, 13)
        Me.Label18.TabIndex = 42
        Me.Label18.Text = "Reportes"
        '
        'cbReportes
        '
        Me.cbReportes.DisplayMember = "Text"
        Me.cbReportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReportes.FormattingEnabled = True
        Me.cbReportes.ItemHeight = 15
        Me.cbReportes.Location = New System.Drawing.Point(76, 68)
        Me.cbReportes.Name = "cbReportes"
        Me.cbReportes.Size = New System.Drawing.Size(324, 21)
        Me.cbReportes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cbReportes.TabIndex = 41
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdSeleccion)
        Me.GroupBox1.Controls.Add(Me.dgvProveedores)
        Me.GroupBox1.Controls.Add(Me.txtNombreProveedor)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.ListBox18)
        Me.GroupBox1.Controls.Add(Me.ListBox17)
        Me.GroupBox1.Controls.Add(Me.ListBox16)
        Me.GroupBox1.Controls.Add(Me.ListBox14)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.ListBox8)
        Me.GroupBox1.Controls.Add(Me.ListBox7)
        Me.GroupBox1.Controls.Add(Me.ListBox6)
        Me.GroupBox1.Controls.Add(Me.ListBox5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ListBox4)
        Me.GroupBox1.Controls.Add(Me.ListBox3)
        Me.GroupBox1.Controls.Add(Me.ListBox2)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 102)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(744, 395)
        Me.GroupBox1.TabIndex = 44
        Me.GroupBox1.TabStop = False
        '
        'cmdSeleccion
        '
        Me.cmdSeleccion.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSeleccion.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.cmdSeleccion.Location = New System.Drawing.Point(246, 39)
        Me.cmdSeleccion.Name = "cmdSeleccion"
        Me.cmdSeleccion.Size = New System.Drawing.Size(122, 23)
        Me.cmdSeleccion.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdSeleccion.TabIndex = 46
        Me.cmdSeleccion.Text = "Seleccionar Todos"
        '
        'dgvProveedores
        '
        Me.dgvProveedores.AllowUserToAddRows = False
        Me.dgvProveedores.BackgroundColor = System.Drawing.SystemColors.Info
        Me.dgvProveedores.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.DarkGreen
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvProveedores.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProveedores.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ckSeleccion, Me.NombreProveedor, Me.Direccion, Me.Saldo, Me.Codigo, Me.IdProveedor})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Maroon
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Maroon
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvProveedores.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvProveedores.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.dgvProveedores.Location = New System.Drawing.Point(8, 68)
        Me.dgvProveedores.Name = "dgvProveedores"
        Me.dgvProveedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvProveedores.Size = New System.Drawing.Size(728, 319)
        Me.dgvProveedores.TabIndex = 47
        '
        'ckSeleccion
        '
        Me.ckSeleccion.Checked = True
        Me.ckSeleccion.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.ckSeleccion.CheckValue = "N"
        Me.ckSeleccion.HeaderText = "Check"
        Me.ckSeleccion.Name = "ckSeleccion"
        Me.ckSeleccion.ReadOnly = True
        Me.ckSeleccion.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ckSeleccion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ckSeleccion.ToolTipText = "Chek para Seleccionar Proveedor"
        Me.ckSeleccion.Width = 50
        '
        'NombreProveedor
        '
        Me.NombreProveedor.HeaderText = "Nombre Proveedor"
        Me.NombreProveedor.Name = "NombreProveedor"
        Me.NombreProveedor.ReadOnly = True
        Me.NombreProveedor.Width = 250
        '
        'Direccion
        '
        Me.Direccion.HeaderText = "Direccion Proveedor"
        Me.Direccion.Name = "Direccion"
        Me.Direccion.ReadOnly = True
        Me.Direccion.Width = 300
        '
        'Saldo
        '
        '
        '
        '
        Me.Saldo.BackgroundStyle.Class = "DataGridViewNumericBorder"
        Me.Saldo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Saldo.HeaderText = "Saldo Pendiente"
        Me.Saldo.Increment = 1
        Me.Saldo.Name = "Saldo"
        Me.Saldo.ReadOnly = True
        Me.Saldo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Saldo.Width = 80
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        '
        'IdProveedor
        '
        Me.IdProveedor.HeaderText = "IdProveedor"
        Me.IdProveedor.Name = "IdProveedor"
        Me.IdProveedor.ReadOnly = True
        Me.IdProveedor.Visible = False
        '
        'txtNombreProveedor
        '
        '
        '
        '
        Me.txtNombreProveedor.Border.Class = "TextBoxBorder"
        Me.txtNombreProveedor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNombreProveedor.Location = New System.Drawing.Point(8, 42)
        Me.txtNombreProveedor.Name = "txtNombreProveedor"
        Me.txtNombreProveedor.Size = New System.Drawing.Size(232, 20)
        Me.txtNombreProveedor.TabIndex = 46
        Me.txtNombreProveedor.WatermarkText = "<b><i><font color=""#B77540"">Ingrese Nombre Proveedor   o Codigo a Filtrar</font><" & _
            "/i></b>"
        '
        'ListBox18
        '
        Me.ListBox18.Location = New System.Drawing.Point(632, 219)
        Me.ListBox18.Name = "ListBox18"
        Me.ListBox18.Size = New System.Drawing.Size(88, 17)
        Me.ListBox18.TabIndex = 45
        Me.ListBox18.Visible = False
        '
        'ListBox17
        '
        Me.ListBox17.Location = New System.Drawing.Point(504, 219)
        Me.ListBox17.Name = "ListBox17"
        Me.ListBox17.Size = New System.Drawing.Size(112, 17)
        Me.ListBox17.TabIndex = 44
        Me.ListBox17.Visible = False
        '
        'ListBox16
        '
        Me.ListBox16.HorizontalScrollbar = True
        Me.ListBox16.Location = New System.Drawing.Point(632, 91)
        Me.ListBox16.Name = "ListBox16"
        Me.ListBox16.Size = New System.Drawing.Size(104, 121)
        Me.ListBox16.TabIndex = 43
        Me.ListBox16.Visible = False
        '
        'ListBox14
        '
        Me.ListBox14.HorizontalScrollbar = True
        Me.ListBox14.Location = New System.Drawing.Point(504, 91)
        Me.ListBox14.Name = "ListBox14"
        Me.ListBox14.Size = New System.Drawing.Size(128, 121)
        Me.ListBox14.TabIndex = 42
        Me.ListBox14.Visible = False
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(504, 75)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(232, 16)
        Me.Label8.TabIndex = 41
        Me.Label8.Text = "Registro de los Municipios"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label8.Visible = False
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(616, 193)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(120, 16)
        Me.Label13.TabIndex = 40
        Me.Label13.Text = "<F5> AYUDA"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(232, 16)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Registro de los Proveedores"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ListBox8
        '
        Me.ListBox8.Location = New System.Drawing.Point(384, 219)
        Me.ListBox8.Name = "ListBox8"
        Me.ListBox8.Size = New System.Drawing.Size(80, 17)
        Me.ListBox8.TabIndex = 28
        Me.ListBox8.Visible = False
        '
        'ListBox7
        '
        Me.ListBox7.Location = New System.Drawing.Point(256, 219)
        Me.ListBox7.Name = "ListBox7"
        Me.ListBox7.Size = New System.Drawing.Size(112, 17)
        Me.ListBox7.TabIndex = 27
        Me.ListBox7.Visible = False
        '
        'ListBox6
        '
        Me.ListBox6.HorizontalScrollbar = True
        Me.ListBox6.Location = New System.Drawing.Point(384, 91)
        Me.ListBox6.Name = "ListBox6"
        Me.ListBox6.Size = New System.Drawing.Size(104, 121)
        Me.ListBox6.TabIndex = 26
        Me.ListBox6.Visible = False
        '
        'ListBox5
        '
        Me.ListBox5.HorizontalScrollbar = True
        Me.ListBox5.Location = New System.Drawing.Point(256, 91)
        Me.ListBox5.Name = "ListBox5"
        Me.ListBox5.Size = New System.Drawing.Size(128, 121)
        Me.ListBox5.TabIndex = 25
        Me.ListBox5.Visible = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(256, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(224, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Registro de los Departamentos"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(264, 16)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Registro de los Negocios"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label1.Visible = False
        '
        'ListBox4
        '
        Me.ListBox4.Location = New System.Drawing.Point(136, 219)
        Me.ListBox4.Name = "ListBox4"
        Me.ListBox4.Size = New System.Drawing.Size(80, 17)
        Me.ListBox4.TabIndex = 22
        Me.ListBox4.Visible = False
        '
        'ListBox3
        '
        Me.ListBox3.Location = New System.Drawing.Point(8, 219)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(112, 17)
        Me.ListBox3.TabIndex = 21
        Me.ListBox3.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.HorizontalScrollbar = True
        Me.ListBox2.Location = New System.Drawing.Point(136, 91)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(104, 121)
        Me.ListBox2.TabIndex = 20
        Me.ListBox2.Visible = False
        '
        'ListBox1
        '
        Me.ListBox1.HorizontalScrollbar = True
        Me.ListBox1.Location = New System.Drawing.Point(8, 91)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(128, 121)
        Me.ListBox1.TabIndex = 19
        Me.ListBox1.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TextBox2)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox2.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(20, 495)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(568, 57)
        Me.GroupBox2.TabIndex = 45
        Me.GroupBox2.TabStop = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(448, 26)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(112, 20)
        Me.TextBox2.TabIndex = 50
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(384, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 13)
        Me.Label7.TabIndex = 49
        Me.Label7.Text = "RecProvis"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(256, 24)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 48
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(72, 24)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker1.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(200, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "FecFinal"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(8, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(62, 13)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "FecInicial"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Timer1
        '
        '
        'frmReportesCtasxPagar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(814, 556)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdReportes)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cbReportes)
        Me.Controls.Add(Me.bHerramientas)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportesCtasxPagar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmReportesCtasxPagar"
        CType(Me.bHerramientas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bHerramientas.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bHerramientas As DevComponents.DotNetBar.Bar
    Friend WithEvents cmdExportar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdiExcel As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiHtml As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiPdf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiRtf As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdiTiff As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiLimpiar As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents cmdiSalir As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmdReportes As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cbReportes As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ListBox18 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox17 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox16 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox14 As System.Windows.Forms.ListBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ListBox8 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox7 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox6 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox5 As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox4 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents dgvProveedores As DevComponents.DotNetBar.Controls.DataGridViewX
    Friend WithEvents txtNombreProveedor As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmdSeleccion As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ckSeleccion As DevComponents.DotNetBar.Controls.DataGridViewCheckBoxXColumn
    Friend WithEvents NombreProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Direccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Saldo As DevComponents.DotNetBar.Controls.DataGridViewDoubleInputColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
