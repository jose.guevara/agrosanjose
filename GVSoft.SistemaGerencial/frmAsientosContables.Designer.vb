<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAsientosContables
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAsientosContables))
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Bar5 = New DevComponents.DotNetBar.Bar()
        Me.Button_buscardocumento_xintegrar = New DevComponents.DotNetBar.ButtonX()
        Me.TextB_numedoc_xintegrar = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelI_tipomov = New DevComponents.DotNetBar.LabelItem()
        Me.ComboB_tipomov = New DevComponents.DotNetBar.ComboBoxItem()
        Me.ComboI_facturas = New DevComponents.Editors.ComboItem()
        Me.ComboI_Recibos = New DevComponents.Editors.ComboItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.PanelEx1 = New DevComponents.DotNetBar.PanelEx()
        Me.ExpandablePanel8 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.DGV_Detalle_tipomov = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ExpandablePanel7 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.Button_anulalinea_Xintegrar = New DevComponents.DotNetBar.ButtonX()
        Me.Button_contabiliza_tipomov = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX29 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_desc_tipomov = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label_desc_tipomov = New DevComponents.DotNetBar.LabelX()
        Me.LabelX32 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_cuenta__tipomov = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextB_numedoc_tipomov = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX33 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX34 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_nlinea_tipomov = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ExpandablePanel6 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.DGV_asientos_tipomov = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.SuperTabI_asientosRecFact = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabItem_revisaAsiento = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ItemPanel6 = New DevComponents.DotNetBar.ItemPanel()
        Me.ExpandablePanel5 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel7 = New DevComponents.DotNetBar.ItemPanel()
        Me.GridP_inconsistencias = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Bar4 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_BuscarInconsistencias = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.ToolbarB_Exportadescuadres = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_imprimirdescuadre = New DevComponents.DotNetBar.ButtonItem()
        Me.ComboB_mes_TabRevisar = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_ano_TabRevisar = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelX21 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.TextField_Descrip_asiento_detalle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_numeasiento_nuevo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.DateF_Fechaasiento_Nuevo = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.TextB_mesnuevo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.SuperTabItem_detalle = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.expandablePanel1 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.Bar3 = New DevComponents.DotNetBar.Bar()
        Me.ButtonI_limpiar_detalle = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_ImprimeAsiento_detalle = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelI_msg_detalle = New DevComponents.DotNetBar.LabelItem()
        Me.expandablePanel_lineas = New DevComponents.DotNetBar.ExpandablePanel()
        Me.itemPanel2 = New DevComponents.DotNetBar.ItemPanel()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.DateF_FechaDoc_Nuevo = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.ComboB_documentos = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.Button_Guarda_linea = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_Deslinea = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label_desccuenta = New DevComponents.DotNetBar.LabelX()
        Me.GridView_dasientos = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.Cr�dito = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_haberlinea = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextB_debelinea = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_cdgocuenta = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextB_Numedocnuevo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_lineanueva = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Bar2 = New DevComponents.DotNetBar.Bar()
        Me.ButtonI_agregar_nuevalinea = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_eliminalinea = New DevComponents.DotNetBar.ButtonItem()
        Me.Button_cerrar = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_Anularasiento = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_reestablecerdocuemento = New DevComponents.DotNetBar.ButtonItem()
        Me.expandablePanel4 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.itemPanel1 = New DevComponents.DotNetBar.ItemPanel()
        Me.ItemPanel5 = New DevComponents.DotNetBar.ItemPanel()
        Me.NumberF_diferencia_Mov = New DevComponents.Editors.DoubleInput()
        Me.NumberF_habelocal = New DevComponents.Editors.DoubleInput()
        Me.NumberF_debelocal = New DevComponents.Editors.DoubleInput()
        Me.LabelX20 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX19 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.TextB_anonuevo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ComboItem3 = New DevComponents.Editors.ComboItem()
        Me.ComboItem1 = New DevComponents.Editors.ComboItem()
        Me.ComboItem2 = New DevComponents.Editors.ComboItem()
        Me.ComboItem4 = New DevComponents.Editors.ComboItem()
        Me.LabelItem_pantalla = New DevComponents.DotNetBar.LabelItem()
        Me.ComboB_mes_TabBusquedas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboB_ano_TabBusquedas = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX27 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX26 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX25 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX24 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX23 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX22 = New DevComponents.DotNetBar.LabelX()
        Me.DateF_FechaIni_compro = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.TextF_Documento = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TextF_numecompro = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.DateF_FechaFin_compro = New DevComponents.Editors.DateTimeAdv.DateTimeInput()
        Me.CI_totalCompro = New DevComponents.Editors.ComboItem()
        Me.SuperTabItem_Buscar = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ExpandablePanel3 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel4 = New DevComponents.DotNetBar.ItemPanel()
        Me.GridP_Asientos = New DevComponents.DotNetBar.Controls.DataGridViewX()
        Me.ExpandablePanel2 = New DevComponents.DotNetBar.ExpandablePanel()
        Me.ItemPanel3 = New DevComponents.DotNetBar.ItemPanel()
        Me.Label_desccuenta_buscar = New DevComponents.DotNetBar.LabelX()
        Me.TextB_cdgocuenta_buscar = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.ToolbarB_BuscarAsiento = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_limpiar_busqueda = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonI_GenerarReporte = New DevComponents.DotNetBar.ButtonItem()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.ComboBoxItem_SelectReport = New DevComponents.DotNetBar.ComboBoxItem()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.ButtonItem_NuevoAsiento = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_menuprincipal = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_cerrarpantalla = New DevComponents.DotNetBar.ButtonItem()
        Me.BindingSource_dasientos = New System.Windows.Forms.BindingSource(Me.components)
        Me.BalloonTip = New DevComponents.DotNetBar.BalloonTip()
        Me.SuperTabControlPanel4.SuspendLayout()
        CType(Me.Bar5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Bar5.SuspendLayout()
        Me.PanelEx1.SuspendLayout()
        Me.ExpandablePanel8.SuspendLayout()
        CType(Me.DGV_Detalle_tipomov, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExpandablePanel7.SuspendLayout()
        Me.ExpandablePanel6.SuspendLayout()
        CType(Me.DGV_asientos_tipomov, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.ItemPanel6.SuspendLayout()
        Me.ExpandablePanel5.SuspendLayout()
        Me.ItemPanel7.SuspendLayout()
        CType(Me.GridP_inconsistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateF_Fechaasiento_Nuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.expandablePanel1.SuspendLayout()
        CType(Me.Bar3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.expandablePanel_lineas.SuspendLayout()
        Me.itemPanel2.SuspendLayout()
        CType(Me.DateF_FechaDoc_Nuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView_dasientos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.expandablePanel4.SuspendLayout()
        Me.itemPanel1.SuspendLayout()
        Me.ItemPanel5.SuspendLayout()
        CType(Me.NumberF_diferencia_Mov, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumberF_habelocal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumberF_debelocal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateF_FechaIni_compro, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateF_FechaFin_compro, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.ExpandablePanel3.SuspendLayout()
        Me.ItemPanel4.SuspendLayout()
        CType(Me.GridP_Asientos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExpandablePanel2.SuspendLayout()
        Me.ItemPanel3.SuspendLayout()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        CType(Me.BindingSource_dasientos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Bar5)
        Me.SuperTabControlPanel4.Controls.Add(Me.PanelEx1)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(1172, 508)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabI_asientosRecFact
        '
        'Bar5
        '
        Me.Bar5.AccessibleDescription = "DotNetBar Bar (Bar5)"
        Me.Bar5.AccessibleName = "DotNetBar Bar"
        Me.Bar5.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar5.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar5.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar5.Controls.Add(Me.Button_buscardocumento_xintegrar)
        Me.Bar5.Controls.Add(Me.TextB_numedoc_xintegrar)
        Me.Bar5.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar5.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelI_tipomov, Me.ComboB_tipomov, Me.LabelItem2})
        Me.Bar5.Location = New System.Drawing.Point(2, 1)
        Me.Bar5.MenuBar = True
        Me.Bar5.Name = "Bar5"
        Me.Bar5.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar5.Size = New System.Drawing.Size(1160, 25)
        Me.Bar5.Stretch = True
        Me.Bar5.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar5.TabIndex = 46
        Me.Bar5.TabStop = False
        Me.Bar5.Text = "Bar5"
        '
        'Button_buscardocumento_xintegrar
        '
        Me.Button_buscardocumento_xintegrar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button_buscardocumento_xintegrar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.Button_buscardocumento_xintegrar.Enabled = False
        Me.Button_buscardocumento_xintegrar.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.DDORes_dll_21_101
        Me.Button_buscardocumento_xintegrar.ImageFixedSize = New System.Drawing.Size(15, 15)
        Me.Button_buscardocumento_xintegrar.Location = New System.Drawing.Point(679, 0)
        Me.Button_buscardocumento_xintegrar.Name = "Button_buscardocumento_xintegrar"
        Me.Button_buscardocumento_xintegrar.Size = New System.Drawing.Size(81, 23)
        Me.Button_buscardocumento_xintegrar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Button_buscardocumento_xintegrar.TabIndex = 10001
        Me.Button_buscardocumento_xintegrar.Text = "Buscar"
        '
        'TextB_numedoc_xintegrar
        '
        Me.TextB_numedoc_xintegrar.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_numedoc_xintegrar.Border.Class = "TextBoxBorder"
        Me.TextB_numedoc_xintegrar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_numedoc_xintegrar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_numedoc_xintegrar.Location = New System.Drawing.Point(535, 2)
        Me.TextB_numedoc_xintegrar.Name = "TextB_numedoc_xintegrar"
        Me.TextB_numedoc_xintegrar.Size = New System.Drawing.Size(144, 20)
        Me.TextB_numedoc_xintegrar.TabIndex = 10000
        '
        'LabelI_tipomov
        '
        Me.LabelI_tipomov.ForeColor = System.Drawing.Color.White
        Me.LabelI_tipomov.Name = "LabelI_tipomov"
        Me.LabelI_tipomov.Text = "      Seleccione Tipo Movimiento: "
        '
        'ComboB_tipomov
        '
        Me.ComboB_tipomov.Caption = "Seleccionar Reporte"
        Me.ComboB_tipomov.ComboWidth = 220
        Me.ComboB_tipomov.Description = "Seleccionar el Tipo de Movimientos"
        Me.ComboB_tipomov.DropDownHeight = 106
        Me.ComboB_tipomov.Items.AddRange(New Object() {Me.ComboI_facturas, Me.ComboI_Recibos})
        Me.ComboB_tipomov.Name = "ComboB_tipomov"
        Me.ComboB_tipomov.Text = "Seleccionar el Tipo de Movimientos"
        Me.ComboB_tipomov.Tooltip = "Seleccionar el Tipo de Movimientos"
        '
        'ComboI_facturas
        '
        Me.ComboI_facturas.Text = "Facturas"
        '
        'ComboI_Recibos
        '
        Me.ComboI_Recibos.Text = "Recibos"
        '
        'LabelItem2
        '
        Me.LabelItem2.ForeColor = System.Drawing.Color.White
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "      Buscar No. Documento: "
        '
        'PanelEx1
        '
        Me.PanelEx1.CanvasColor = System.Drawing.SystemColors.Control
        Me.PanelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.PanelEx1.Controls.Add(Me.ExpandablePanel8)
        Me.PanelEx1.Controls.Add(Me.ExpandablePanel7)
        Me.PanelEx1.Controls.Add(Me.ExpandablePanel6)
        Me.PanelEx1.Location = New System.Drawing.Point(8, 25)
        Me.PanelEx1.Name = "PanelEx1"
        Me.PanelEx1.Size = New System.Drawing.Size(1161, 497)
        Me.PanelEx1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.PanelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.PanelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.PanelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.PanelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.PanelEx1.Style.GradientAngle = 90
        Me.PanelEx1.TabIndex = 30
        '
        'ExpandablePanel8
        '
        Me.ExpandablePanel8.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel8.Controls.Add(Me.DGV_Detalle_tipomov)
        Me.ExpandablePanel8.ExpandOnTitleClick = True
        Me.ExpandablePanel8.Location = New System.Drawing.Point(0, 290)
        Me.ExpandablePanel8.Name = "ExpandablePanel8"
        Me.ExpandablePanel8.Size = New System.Drawing.Size(1148, 194)
        Me.ExpandablePanel8.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel8.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel8.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ExpandablePanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel8.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.ExpandablePanel8.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel8.Style.GradientAngle = 90
        Me.ExpandablePanel8.TabIndex = 51
        Me.ExpandablePanel8.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel8.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ExpandablePanel8.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel8.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandablePanel8.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.ExpandablePanel8.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel8.TitleStyle.MarginLeft = 8
        Me.ExpandablePanel8.TitleText = "L�neas del Asiento"
        '
        'DGV_Detalle_tipomov
        '
        Me.DGV_Detalle_tipomov.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Detalle_tipomov.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGV_Detalle_tipomov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_Detalle_tipomov.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGV_Detalle_tipomov.EnableHeadersVisualStyles = False
        Me.DGV_Detalle_tipomov.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.DGV_Detalle_tipomov.Location = New System.Drawing.Point(7, 30)
        Me.DGV_Detalle_tipomov.Name = "DGV_Detalle_tipomov"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_Detalle_tipomov.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DGV_Detalle_tipomov.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_Detalle_tipomov.Size = New System.Drawing.Size(1131, 160)
        Me.DGV_Detalle_tipomov.TabIndex = 1
        '
        'ExpandablePanel7
        '
        Me.ExpandablePanel7.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel7.Controls.Add(Me.Button_anulalinea_Xintegrar)
        Me.ExpandablePanel7.Controls.Add(Me.Button_contabiliza_tipomov)
        Me.ExpandablePanel7.Controls.Add(Me.LabelX29)
        Me.ExpandablePanel7.Controls.Add(Me.TextB_desc_tipomov)
        Me.ExpandablePanel7.Controls.Add(Me.Label_desc_tipomov)
        Me.ExpandablePanel7.Controls.Add(Me.LabelX32)
        Me.ExpandablePanel7.Controls.Add(Me.TextB_cuenta__tipomov)
        Me.ExpandablePanel7.Controls.Add(Me.TextB_numedoc_tipomov)
        Me.ExpandablePanel7.Controls.Add(Me.LabelX33)
        Me.ExpandablePanel7.Controls.Add(Me.LabelX34)
        Me.ExpandablePanel7.Controls.Add(Me.TextB_nlinea_tipomov)
        Me.ExpandablePanel7.ExpandOnTitleClick = True
        Me.ExpandablePanel7.Location = New System.Drawing.Point(0, 187)
        Me.ExpandablePanel7.Name = "ExpandablePanel7"
        Me.ExpandablePanel7.Size = New System.Drawing.Size(1161, 307)
        Me.ExpandablePanel7.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel7.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel7.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ExpandablePanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel7.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.ExpandablePanel7.Style.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.ExpandablePanel7.Style.BorderSide = DevComponents.DotNetBar.eBorderSide.None
        Me.ExpandablePanel7.Style.CornerDiameter = 1
        Me.ExpandablePanel7.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel7.Style.GradientAngle = 90
        Me.ExpandablePanel7.TabIndex = 50
        Me.ExpandablePanel7.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel7.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ExpandablePanel7.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel7.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandablePanel7.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.DashDotDot
        Me.ExpandablePanel7.TitleStyle.CornerDiameter = 1
        Me.ExpandablePanel7.TitleStyle.ForeColor.Color = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.ExpandablePanel7.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel7.TitleStyle.MarginLeft = 8
        Me.ExpandablePanel7.TitleText = "Detalle del Asiento"
        '
        'Button_anulalinea_Xintegrar
        '
        Me.Button_anulalinea_Xintegrar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button_anulalinea_Xintegrar.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.Button_anulalinea_Xintegrar.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.multifunction_printer_icon
        Me.Button_anulalinea_Xintegrar.ImageFixedSize = New System.Drawing.Size(15, 15)
        Me.Button_anulalinea_Xintegrar.Location = New System.Drawing.Point(898, 75)
        Me.Button_anulalinea_Xintegrar.Name = "Button_anulalinea_Xintegrar"
        Me.Button_anulalinea_Xintegrar.Size = New System.Drawing.Size(103, 23)
        Me.Button_anulalinea_Xintegrar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Button_anulalinea_Xintegrar.TabIndex = 84
        Me.Button_anulalinea_Xintegrar.Text = "Anular L�nea"
        '
        'Button_contabiliza_tipomov
        '
        Me.Button_contabiliza_tipomov.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button_contabiliza_tipomov.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.Button_contabiliza_tipomov.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Add
        Me.Button_contabiliza_tipomov.ImageFixedSize = New System.Drawing.Size(15, 15)
        Me.Button_contabiliza_tipomov.Location = New System.Drawing.Point(898, 51)
        Me.Button_contabiliza_tipomov.Name = "Button_contabiliza_tipomov"
        Me.Button_contabiliza_tipomov.Size = New System.Drawing.Size(103, 23)
        Me.Button_contabiliza_tipomov.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Button_contabiliza_tipomov.TabIndex = 83
        Me.Button_contabiliza_tipomov.Text = "Contabilizar"
        '
        'LabelX29
        '
        Me.LabelX29.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX29.BackgroundStyle.Class = ""
        Me.LabelX29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX29.ForeColor = System.Drawing.Color.Black
        Me.LabelX29.Location = New System.Drawing.Point(380, 31)
        Me.LabelX29.Name = "LabelX29"
        Me.LabelX29.Size = New System.Drawing.Size(153, 23)
        Me.LabelX29.TabIndex = 77
        Me.LabelX29.Text = "Descripci�n de la L�nea"
        '
        'TextB_desc_tipomov
        '
        Me.TextB_desc_tipomov.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_desc_tipomov.Border.Class = "TextBoxBorder"
        Me.TextB_desc_tipomov.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_desc_tipomov.Enabled = False
        Me.TextB_desc_tipomov.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_desc_tipomov.Location = New System.Drawing.Point(380, 54)
        Me.TextB_desc_tipomov.Name = "TextB_desc_tipomov"
        Me.TextB_desc_tipomov.ReadOnly = True
        Me.TextB_desc_tipomov.Size = New System.Drawing.Size(512, 20)
        Me.TextB_desc_tipomov.TabIndex = 82
        Me.TextB_desc_tipomov.TabStop = False
        '
        'Label_desc_tipomov
        '
        Me.Label_desc_tipomov.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_desc_tipomov.BackgroundStyle.Class = ""
        Me.Label_desc_tipomov.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_desc_tipomov.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_desc_tipomov.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_desc_tipomov.Location = New System.Drawing.Point(181, 75)
        Me.Label_desc_tipomov.Name = "Label_desc_tipomov"
        Me.Label_desc_tipomov.Size = New System.Drawing.Size(418, 23)
        Me.Label_desc_tipomov.TabIndex = 78
        Me.Label_desc_tipomov.Text = "?"
        '
        'LabelX32
        '
        Me.LabelX32.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX32.BackgroundStyle.Class = ""
        Me.LabelX32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX32.ForeColor = System.Drawing.Color.Black
        Me.LabelX32.Location = New System.Drawing.Point(181, 31)
        Me.LabelX32.Name = "LabelX32"
        Me.LabelX32.Size = New System.Drawing.Size(153, 23)
        Me.LabelX32.TabIndex = 76
        Me.LabelX32.Text = "Cuenta Contable"
        '
        'TextB_cuenta__tipomov
        '
        Me.TextB_cuenta__tipomov.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_cuenta__tipomov.Border.Class = "TextBoxBorder"
        Me.TextB_cuenta__tipomov.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_cuenta__tipomov.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_cuenta__tipomov.Location = New System.Drawing.Point(181, 54)
        Me.TextB_cuenta__tipomov.Name = "TextB_cuenta__tipomov"
        Me.TextB_cuenta__tipomov.Size = New System.Drawing.Size(185, 20)
        Me.TextB_cuenta__tipomov.TabIndex = 81
        '
        'TextB_numedoc_tipomov
        '
        Me.TextB_numedoc_tipomov.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_numedoc_tipomov.Border.Class = "TextBoxBorder"
        Me.TextB_numedoc_tipomov.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_numedoc_tipomov.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_numedoc_tipomov.Location = New System.Drawing.Point(68, 54)
        Me.TextB_numedoc_tipomov.Name = "TextB_numedoc_tipomov"
        Me.TextB_numedoc_tipomov.Size = New System.Drawing.Size(100, 20)
        Me.TextB_numedoc_tipomov.TabIndex = 80
        '
        'LabelX33
        '
        Me.LabelX33.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX33.BackgroundStyle.Class = ""
        Me.LabelX33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX33.ForeColor = System.Drawing.Color.Black
        Me.LabelX33.Location = New System.Drawing.Point(68, 31)
        Me.LabelX33.Name = "LabelX33"
        Me.LabelX33.Size = New System.Drawing.Size(100, 23)
        Me.LabelX33.TabIndex = 75
        Me.LabelX33.Text = "No.Documento"
        '
        'LabelX34
        '
        Me.LabelX34.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX34.BackgroundStyle.Class = ""
        Me.LabelX34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX34.ForeColor = System.Drawing.Color.Black
        Me.LabelX34.Location = New System.Drawing.Point(10, 31)
        Me.LabelX34.Name = "LabelX34"
        Me.LabelX34.Size = New System.Drawing.Size(75, 23)
        Me.LabelX34.TabIndex = 74
        Me.LabelX34.Text = "No.L�nea"
        '
        'TextB_nlinea_tipomov
        '
        Me.TextB_nlinea_tipomov.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_nlinea_tipomov.Border.Class = "TextBoxBorder"
        Me.TextB_nlinea_tipomov.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_nlinea_tipomov.Enabled = False
        Me.TextB_nlinea_tipomov.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_nlinea_tipomov.Location = New System.Drawing.Point(10, 54)
        Me.TextB_nlinea_tipomov.Name = "TextB_nlinea_tipomov"
        Me.TextB_nlinea_tipomov.ReadOnly = True
        Me.TextB_nlinea_tipomov.Size = New System.Drawing.Size(50, 20)
        Me.TextB_nlinea_tipomov.TabIndex = 79
        Me.TextB_nlinea_tipomov.TabStop = False
        '
        'ExpandablePanel6
        '
        Me.ExpandablePanel6.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ExpandablePanel6.Controls.Add(Me.DGV_asientos_tipomov)
        Me.ExpandablePanel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel6.ExpandOnTitleClick = True
        Me.ExpandablePanel6.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel6.Name = "ExpandablePanel6"
        Me.ExpandablePanel6.Size = New System.Drawing.Size(1161, 187)
        Me.ExpandablePanel6.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel6.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel6.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ExpandablePanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel6.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.ExpandablePanel6.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel6.Style.GradientAngle = 90
        Me.ExpandablePanel6.TabIndex = 49
        Me.ExpandablePanel6.TitleStyle.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandablePanel6.TitleStyle.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.ExpandablePanel6.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel6.TitleStyle.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandablePanel6.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.ExpandablePanel6.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel6.TitleStyle.MarginLeft = 8
        Me.ExpandablePanel6.TitleText = "Asientos Encontrados por Tipo de Movimiento"
        '
        'DGV_asientos_tipomov
        '
        Me.DGV_asientos_tipomov.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_asientos_tipomov.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DGV_asientos_tipomov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGV_asientos_tipomov.DefaultCellStyle = DataGridViewCellStyle5
        Me.DGV_asientos_tipomov.EnableHeadersVisualStyles = False
        Me.DGV_asientos_tipomov.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.DGV_asientos_tipomov.Location = New System.Drawing.Point(7, 28)
        Me.DGV_asientos_tipomov.Name = "DGV_asientos_tipomov"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGV_asientos_tipomov.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DGV_asientos_tipomov.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGV_asientos_tipomov.Size = New System.Drawing.Size(1141, 150)
        Me.DGV_asientos_tipomov.TabIndex = 1
        '
        'SuperTabI_asientosRecFact
        '
        Me.SuperTabI_asientosRecFact.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabI_asientosRecFact.GlobalItem = False
        Me.SuperTabI_asientosRecFact.Name = "SuperTabI_asientosRecFact"
        Me.SuperTabI_asientosRecFact.Text = "Asientos Por Integrar"
        '
        'SuperTabItem_revisaAsiento
        '
        Me.SuperTabItem_revisaAsiento.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem_revisaAsiento.GlobalItem = False
        Me.SuperTabItem_revisaAsiento.Name = "SuperTabItem_revisaAsiento"
        Me.SuperTabItem_revisaAsiento.Text = "Revisar Asientos"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.ItemPanel6)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(1172, 508)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem_revisaAsiento
        '
        'ItemPanel6
        '
        '
        '
        '
        Me.ItemPanel6.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel6.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel6.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel6.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel6.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel6.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel6.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel6.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel6.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel6.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel6.BackgroundStyle.Class = ""
        Me.ItemPanel6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel6.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel6.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel6.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel6.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel6.ContainerControlProcessDialogKey = True
        Me.ItemPanel6.Controls.Add(Me.ExpandablePanel5)
        Me.ItemPanel6.Controls.Add(Me.Bar4)
        Me.ItemPanel6.Controls.Add(Me.ComboB_mes_TabRevisar)
        Me.ItemPanel6.Controls.Add(Me.ComboB_ano_TabRevisar)
        Me.ItemPanel6.Controls.Add(Me.LabelX10)
        Me.ItemPanel6.Controls.Add(Me.LabelX11)
        Me.ItemPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel6.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem1})
        Me.ItemPanel6.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel6.Location = New System.Drawing.Point(0, 0)
        Me.ItemPanel6.Name = "ItemPanel6"
        Me.ItemPanel6.Size = New System.Drawing.Size(1172, 508)
        Me.ItemPanel6.TabIndex = 1
        Me.ItemPanel6.Text = "ItemPanel6"
        '
        'ExpandablePanel5
        '
        Me.ExpandablePanel5.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel5.Controls.Add(Me.ItemPanel7)
        Me.ExpandablePanel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ExpandablePanel5.ExpandOnTitleClick = True
        Me.ExpandablePanel5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel5.Location = New System.Drawing.Point(0, 135)
        Me.ExpandablePanel5.Name = "ExpandablePanel5"
        Me.ExpandablePanel5.Size = New System.Drawing.Size(1172, 373)
        Me.ExpandablePanel5.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel5.Style.BackColor1.Color = System.Drawing.SystemColors.MenuHighlight
        Me.ExpandablePanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel5.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel5.Style.GradientAngle = 90
        Me.ExpandablePanel5.TabIndex = 46
        Me.ExpandablePanel5.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel5.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel5.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel5.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel5.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel5.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel5.TitleText = "Registros Encontrados"
        '
        'ItemPanel7
        '
        '
        '
        '
        Me.ItemPanel7.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel7.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel7.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel7.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel7.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel7.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel7.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel7.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel7.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel7.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel7.BackgroundStyle.Class = ""
        Me.ItemPanel7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel7.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel7.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel7.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel7.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel7.ContainerControlProcessDialogKey = True
        Me.ItemPanel7.Controls.Add(Me.GridP_inconsistencias)
        Me.ItemPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel7.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel7.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel7.Name = "ItemPanel7"
        Me.ItemPanel7.Size = New System.Drawing.Size(1172, 347)
        Me.ItemPanel7.TabIndex = 3
        Me.ItemPanel7.Text = "ItemPanel7"
        '
        'GridP_inconsistencias
        '
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_inconsistencias.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.GridP_inconsistencias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_inconsistencias.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.GridP_inconsistencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_inconsistencias.DefaultCellStyle = DataGridViewCellStyle9
        Me.GridP_inconsistencias.EnableHeadersVisualStyles = False
        Me.GridP_inconsistencias.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_inconsistencias.Location = New System.Drawing.Point(4, 4)
        Me.GridP_inconsistencias.Name = "GridP_inconsistencias"
        Me.GridP_inconsistencias.ReadOnly = True
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_inconsistencias.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.GridP_inconsistencias.RowHeadersWidth = 20
        Me.GridP_inconsistencias.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_inconsistencias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_inconsistencias.Size = New System.Drawing.Size(1157, 334)
        Me.GridP_inconsistencias.TabIndex = 0
        '
        'Bar4
        '
        Me.Bar4.AccessibleDescription = "DotNetBar Bar (Bar4)"
        Me.Bar4.AccessibleName = "DotNetBar Bar"
        Me.Bar4.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar4.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar4.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar4.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar4.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_BuscarInconsistencias, Me.ButtonItem2, Me.ToolbarB_Exportadescuadres, Me.ButtonI_imprimirdescuadre})
        Me.Bar4.Location = New System.Drawing.Point(2, 2)
        Me.Bar4.MenuBar = True
        Me.Bar4.Name = "Bar4"
        Me.Bar4.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar4.Size = New System.Drawing.Size(1159, 26)
        Me.Bar4.Stretch = True
        Me.Bar4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar4.TabIndex = 45
        Me.Bar4.TabStop = False
        Me.Bar4.Text = "Bar4"
        '
        'ToolbarB_BuscarInconsistencias
        '
        Me.ToolbarB_BuscarInconsistencias.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_BuscarInconsistencias.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_BuscarInconsistencias.Image = CType(resources.GetObject("ToolbarB_BuscarInconsistencias.Image"), System.Drawing.Image)
        Me.ToolbarB_BuscarInconsistencias.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_BuscarInconsistencias.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_BuscarInconsistencias.Name = "ToolbarB_BuscarInconsistencias"
        Me.ToolbarB_BuscarInconsistencias.Text = "Buscar Inconsistencias"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.ForeColor = System.Drawing.Color.White
        Me.ButtonItem2.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ResourceForkEraser
        Me.ButtonItem2.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Text = "Limpiar Pantalla"
        '
        'ToolbarB_Exportadescuadres
        '
        Me.ToolbarB_Exportadescuadres.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_Exportadescuadres.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_Exportadescuadres.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Excel
        Me.ToolbarB_Exportadescuadres.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_Exportadescuadres.Name = "ToolbarB_Exportadescuadres"
        Me.ToolbarB_Exportadescuadres.Text = "Exportar Excel"
        '
        'ButtonI_imprimirdescuadre
        '
        Me.ButtonI_imprimirdescuadre.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_imprimirdescuadre.ForeColor = System.Drawing.Color.White
        Me.ButtonI_imprimirdescuadre.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ButtonI_imprimirdescuadre.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_imprimirdescuadre.Name = "ButtonI_imprimirdescuadre"
        Me.ButtonI_imprimirdescuadre.Text = "Imprimir Descuadres"
        '
        'ComboB_mes_TabRevisar
        '
        Me.ComboB_mes_TabRevisar.DisplayMember = "Text"
        Me.ComboB_mes_TabRevisar.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_mes_TabRevisar.FormattingEnabled = True
        Me.ComboB_mes_TabRevisar.ItemHeight = 15
        Me.ComboB_mes_TabRevisar.Location = New System.Drawing.Point(457, 35)
        Me.ComboB_mes_TabRevisar.Name = "ComboB_mes_TabRevisar"
        Me.ComboB_mes_TabRevisar.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_mes_TabRevisar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_mes_TabRevisar.TabIndex = 4
        '
        'ComboB_ano_TabRevisar
        '
        Me.ComboB_ano_TabRevisar.DisplayMember = "Text"
        Me.ComboB_ano_TabRevisar.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_ano_TabRevisar.FormattingEnabled = True
        Me.ComboB_ano_TabRevisar.ItemHeight = 15
        Me.ComboB_ano_TabRevisar.Location = New System.Drawing.Point(151, 35)
        Me.ComboB_ano_TabRevisar.Name = "ComboB_ano_TabRevisar"
        Me.ComboB_ano_TabRevisar.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_ano_TabRevisar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_ano_TabRevisar.TabIndex = 3
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.Class = ""
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(404, 33)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(44, 23)
        Me.LabelX10.TabIndex = 40
        Me.LabelX10.Text = "Mes"
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX11.BackgroundStyle.Class = ""
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(101, 33)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(44, 23)
        Me.LabelX11.TabIndex = 39
        Me.LabelX11.Text = "A�o"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelX21
        '
        Me.LabelX21.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX21.BackgroundStyle.Class = ""
        Me.LabelX21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX21.ForeColor = System.Drawing.Color.SteelBlue
        Me.LabelX21.Location = New System.Drawing.Point(651, 6)
        Me.LabelX21.Name = "LabelX21"
        Me.LabelX21.Size = New System.Drawing.Size(396, 23)
        Me.LabelX21.TabIndex = 43
        Me.LabelX21.Text = "Resumen de Saldos"
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX17.BackgroundStyle.Class = ""
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(25, 56)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(102, 23)
        Me.LabelX17.TabIndex = 41
        Me.LabelX17.Text = "Concepto del Asiento"
        '
        'TextField_Descrip_asiento_detalle
        '
        Me.TextField_Descrip_asiento_detalle.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextField_Descrip_asiento_detalle.Border.Class = "TextBoxBorder"
        Me.TextField_Descrip_asiento_detalle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextField_Descrip_asiento_detalle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextField_Descrip_asiento_detalle.Location = New System.Drawing.Point(25, 79)
        Me.TextField_Descrip_asiento_detalle.Name = "TextField_Descrip_asiento_detalle"
        Me.TextField_Descrip_asiento_detalle.Size = New System.Drawing.Size(589, 21)
        Me.TextField_Descrip_asiento_detalle.TabIndex = 36
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX16.BackgroundStyle.Class = ""
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(259, 3)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(79, 23)
        Me.LabelX16.TabIndex = 18
        Me.LabelX16.Text = "No. Asiento"
        '
        'TextB_numeasiento_nuevo
        '
        Me.TextB_numeasiento_nuevo.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_numeasiento_nuevo.Border.Class = "TextBoxBorder"
        Me.TextB_numeasiento_nuevo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_numeasiento_nuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_numeasiento_nuevo.Location = New System.Drawing.Point(259, 31)
        Me.TextB_numeasiento_nuevo.Name = "TextB_numeasiento_nuevo"
        Me.TextB_numeasiento_nuevo.Size = New System.Drawing.Size(100, 21)
        Me.TextB_numeasiento_nuevo.TabIndex = 35
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX15.BackgroundStyle.Class = ""
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(514, 6)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(78, 23)
        Me.LabelX15.TabIndex = 20
        Me.LabelX15.Text = "Mes"
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX14.BackgroundStyle.Class = ""
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(381, 4)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(79, 23)
        Me.LabelX14.TabIndex = 19
        Me.LabelX14.Text = "A�o"
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.Class = ""
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(25, 4)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(103, 23)
        Me.LabelX13.TabIndex = 17
        Me.LabelX13.Text = "Fecha del Asiento"
        '
        'DateF_Fechaasiento_Nuevo
        '
        '
        '
        '
        Me.DateF_Fechaasiento_Nuevo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_Fechaasiento_Nuevo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_Fechaasiento_Nuevo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_Fechaasiento_Nuevo.ButtonDropDown.Visible = True
        Me.DateF_Fechaasiento_Nuevo.CustomFormat = "dd/MM/yyyy"
        Me.DateF_Fechaasiento_Nuevo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_Fechaasiento_Nuevo.IsPopupCalendarOpen = False
        Me.DateF_Fechaasiento_Nuevo.Location = New System.Drawing.Point(25, 31)
        '
        '
        '
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.TodayButtonVisible = True
        Me.DateF_Fechaasiento_Nuevo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_Fechaasiento_Nuevo.Name = "DateF_Fechaasiento_Nuevo"
        Me.DateF_Fechaasiento_Nuevo.Size = New System.Drawing.Size(200, 21)
        Me.DateF_Fechaasiento_Nuevo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_Fechaasiento_Nuevo.TabIndex = 34
        '
        'TextB_mesnuevo
        '
        Me.TextB_mesnuevo.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_mesnuevo.Border.Class = "TextBoxBorder"
        Me.TextB_mesnuevo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_mesnuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_mesnuevo.Location = New System.Drawing.Point(514, 31)
        Me.TextB_mesnuevo.Name = "TextB_mesnuevo"
        Me.TextB_mesnuevo.ReadOnly = True
        Me.TextB_mesnuevo.Size = New System.Drawing.Size(100, 21)
        Me.TextB_mesnuevo.TabIndex = 33
        '
        'SuperTabItem_detalle
        '
        Me.SuperTabItem_detalle.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem_detalle.GlobalItem = False
        Me.SuperTabItem_detalle.Name = "SuperTabItem_detalle"
        Me.SuperTabItem_detalle.Text = "Detalle/Nuevo"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.expandablePanel1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(1172, 508)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem_detalle
        '
        'expandablePanel1
        '
        Me.expandablePanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.expandablePanel1.CollapseDirection = DevComponents.DotNetBar.eCollapseDirection.RightToLeft
        Me.expandablePanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.expandablePanel1.Controls.Add(Me.Bar3)
        Me.expandablePanel1.Controls.Add(Me.expandablePanel_lineas)
        Me.expandablePanel1.Controls.Add(Me.expandablePanel4)
        Me.expandablePanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.expandablePanel1.Location = New System.Drawing.Point(2, 1)
        Me.expandablePanel1.Name = "expandablePanel1"
        Me.expandablePanel1.Size = New System.Drawing.Size(1160, 501)
        Me.expandablePanel1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.expandablePanel1.Style.BackgroundImagePosition = DevComponents.DotNetBar.eBackgroundImagePosition.Tile
        Me.expandablePanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.expandablePanel1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.expandablePanel1.Style.GradientAngle = 90
        Me.expandablePanel1.Style.WordWrap = True
        Me.expandablePanel1.TabIndex = 12
        Me.expandablePanel1.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuHighlight
        Me.expandablePanel1.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.expandablePanel1.TitleStyle.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.expandablePanel1.TitleStyle.GradientAngle = 90
        Me.expandablePanel1.TitleStyle.MarginLeft = 6
        Me.expandablePanel1.TitleText = "  "
        '
        'Bar3
        '
        Me.Bar3.AccessibleDescription = "DotNetBar Bar (Bar3)"
        Me.Bar3.AccessibleName = "DotNetBar Bar"
        Me.Bar3.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar3.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar3.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonI_limpiar_detalle, Me.ButtonI_ImprimeAsiento_detalle, Me.LabelI_msg_detalle})
        Me.Bar3.Location = New System.Drawing.Point(2, 1)
        Me.Bar3.MenuBar = True
        Me.Bar3.Name = "Bar3"
        Me.Bar3.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar3.Size = New System.Drawing.Size(1070, 26)
        Me.Bar3.Stretch = True
        Me.Bar3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar3.TabIndex = 28
        Me.Bar3.TabStop = False
        Me.Bar3.Text = "Bar3"
        '
        'ButtonI_limpiar_detalle
        '
        Me.ButtonI_limpiar_detalle.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_limpiar_detalle.ForeColor = System.Drawing.Color.White
        Me.ButtonI_limpiar_detalle.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ResourceForkEraser
        Me.ButtonI_limpiar_detalle.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_limpiar_detalle.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ButtonI_limpiar_detalle.Name = "ButtonI_limpiar_detalle"
        Me.ButtonI_limpiar_detalle.Text = "Limpiar Pantalla"
        '
        'ButtonI_ImprimeAsiento_detalle
        '
        Me.ButtonI_ImprimeAsiento_detalle.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_ImprimeAsiento_detalle.ForeColor = System.Drawing.Color.White
        Me.ButtonI_ImprimeAsiento_detalle.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ButtonI_ImprimeAsiento_detalle.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_ImprimeAsiento_detalle.Name = "ButtonI_ImprimeAsiento_detalle"
        Me.ButtonI_ImprimeAsiento_detalle.Text = "Imprimir Asiento"
        '
        'LabelI_msg_detalle
        '
        Me.LabelI_msg_detalle.ForeColor = System.Drawing.Color.Yellow
        Me.LabelI_msg_detalle.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.LabelI_msg_detalle.Name = "LabelI_msg_detalle"
        Me.LabelI_msg_detalle.Stretch = True
        Me.LabelI_msg_detalle.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'expandablePanel_lineas
        '
        Me.expandablePanel_lineas.AutoScroll = True
        Me.expandablePanel_lineas.CanvasColor = System.Drawing.SystemColors.Control
        Me.expandablePanel_lineas.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.expandablePanel_lineas.Controls.Add(Me.itemPanel2)
        Me.expandablePanel_lineas.ExpandOnTitleClick = True
        Me.expandablePanel_lineas.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.expandablePanel_lineas.Location = New System.Drawing.Point(0, 157)
        Me.expandablePanel_lineas.Name = "expandablePanel_lineas"
        Me.expandablePanel_lineas.Size = New System.Drawing.Size(1165, 343)
        Me.expandablePanel_lineas.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.expandablePanel_lineas.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.expandablePanel_lineas.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.expandablePanel_lineas.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.expandablePanel_lineas.Style.GradientAngle = 90
        Me.expandablePanel_lineas.TabIndex = 14
        Me.expandablePanel_lineas.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.expandablePanel_lineas.TitleStyle.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(175, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.expandablePanel_lineas.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.expandablePanel_lineas.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.expandablePanel_lineas.TitleStyle.GradientAngle = 90
        Me.expandablePanel_lineas.TitleStyle.MarginLeft = 12
        Me.expandablePanel_lineas.TitleText = "L�neas Contables"
        '
        'itemPanel2
        '
        '
        '
        '
        Me.itemPanel2.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.itemPanel2.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel2.BackgroundStyle.BorderBottomWidth = 1
        Me.itemPanel2.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.itemPanel2.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel2.BackgroundStyle.BorderLeftWidth = 1
        Me.itemPanel2.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel2.BackgroundStyle.BorderRightWidth = 1
        Me.itemPanel2.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel2.BackgroundStyle.BorderTopWidth = 1
        Me.itemPanel2.BackgroundStyle.Class = ""
        Me.itemPanel2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.itemPanel2.BackgroundStyle.PaddingBottom = 1
        Me.itemPanel2.BackgroundStyle.PaddingLeft = 1
        Me.itemPanel2.BackgroundStyle.PaddingRight = 1
        Me.itemPanel2.BackgroundStyle.PaddingTop = 1
        Me.itemPanel2.ContainerControlProcessDialogKey = True
        Me.itemPanel2.Controls.Add(Me.LabelX8)
        Me.itemPanel2.Controls.Add(Me.DateF_FechaDoc_Nuevo)
        Me.itemPanel2.Controls.Add(Me.ComboB_documentos)
        Me.itemPanel2.Controls.Add(Me.LabelX7)
        Me.itemPanel2.Controls.Add(Me.Button_Guarda_linea)
        Me.itemPanel2.Controls.Add(Me.LabelX5)
        Me.itemPanel2.Controls.Add(Me.TextB_Deslinea)
        Me.itemPanel2.Controls.Add(Me.Label_desccuenta)
        Me.itemPanel2.Controls.Add(Me.GridView_dasientos)
        Me.itemPanel2.Controls.Add(Me.Cr�dito)
        Me.itemPanel2.Controls.Add(Me.LabelX4)
        Me.itemPanel2.Controls.Add(Me.TextB_haberlinea)
        Me.itemPanel2.Controls.Add(Me.TextB_debelinea)
        Me.itemPanel2.Controls.Add(Me.LabelX3)
        Me.itemPanel2.Controls.Add(Me.TextB_cdgocuenta)
        Me.itemPanel2.Controls.Add(Me.TextB_Numedocnuevo)
        Me.itemPanel2.Controls.Add(Me.LabelX2)
        Me.itemPanel2.Controls.Add(Me.LabelX1)
        Me.itemPanel2.Controls.Add(Me.TextB_lineanueva)
        Me.itemPanel2.Controls.Add(Me.Bar2)
        Me.itemPanel2.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.itemPanel2.Location = New System.Drawing.Point(0, 26)
        Me.itemPanel2.Name = "itemPanel2"
        Me.itemPanel2.Size = New System.Drawing.Size(1153, 317)
        Me.itemPanel2.TabIndex = 17
        Me.itemPanel2.Text = "itemPanel2"
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.Class = ""
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(176, 31)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(93, 23)
        Me.LabelX8.TabIndex = 48
        Me.LabelX8.Text = "Fecha.Documento"
        '
        'DateF_FechaDoc_Nuevo
        '
        '
        '
        '
        Me.DateF_FechaDoc_Nuevo.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaDoc_Nuevo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaDoc_Nuevo.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaDoc_Nuevo.ButtonDropDown.Visible = True
        Me.DateF_FechaDoc_Nuevo.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaDoc_Nuevo.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaDoc_Nuevo.IsPopupCalendarOpen = False
        Me.DateF_FechaDoc_Nuevo.Location = New System.Drawing.Point(176, 54)
        '
        '
        '
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaDoc_Nuevo.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaDoc_Nuevo.Name = "DateF_FechaDoc_Nuevo"
        Me.DateF_FechaDoc_Nuevo.Size = New System.Drawing.Size(133, 21)
        Me.DateF_FechaDoc_Nuevo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaDoc_Nuevo.TabIndex = 47
        '
        'ComboB_documentos
        '
        Me.ComboB_documentos.DisplayMember = "Text"
        Me.ComboB_documentos.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_documentos.FormattingEnabled = True
        Me.ComboB_documentos.ItemHeight = 15
        Me.ComboB_documentos.Location = New System.Drawing.Point(925, 77)
        Me.ComboB_documentos.Name = "ComboB_documentos"
        Me.ComboB_documentos.Size = New System.Drawing.Size(213, 21)
        Me.ComboB_documentos.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_documentos.TabIndex = 43
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.Class = ""
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(803, 76)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(123, 23)
        Me.LabelX7.TabIndex = 44
        Me.LabelX7.Text = "Documentos del Asiento"
        '
        'Button_Guarda_linea
        '
        Me.Button_Guarda_linea.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button_Guarda_linea.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.Button_Guarda_linea.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Add
        Me.Button_Guarda_linea.ImageFixedSize = New System.Drawing.Size(15, 15)
        Me.Button_Guarda_linea.Location = New System.Drawing.Point(1121, 51)
        Me.Button_Guarda_linea.Name = "Button_Guarda_linea"
        Me.Button_Guarda_linea.Size = New System.Drawing.Size(23, 23)
        Me.Button_Guarda_linea.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Button_Guarda_linea.TabIndex = 42
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX5.BackgroundStyle.Class = ""
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(487, 31)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(153, 23)
        Me.LabelX5.TabIndex = 23
        Me.LabelX5.Text = "Descripci�n de la L�nea"
        '
        'TextB_Deslinea
        '
        Me.TextB_Deslinea.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_Deslinea.Border.Class = "TextBoxBorder"
        Me.TextB_Deslinea.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_Deslinea.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_Deslinea.Location = New System.Drawing.Point(487, 54)
        Me.TextB_Deslinea.Name = "TextB_Deslinea"
        Me.TextB_Deslinea.Size = New System.Drawing.Size(414, 21)
        Me.TextB_Deslinea.TabIndex = 39
        '
        'Label_desccuenta
        '
        Me.Label_desccuenta.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_desccuenta.BackgroundStyle.Class = ""
        Me.Label_desccuenta.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_desccuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_desccuenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label_desccuenta.Location = New System.Drawing.Point(317, 75)
        Me.Label_desccuenta.Name = "Label_desccuenta"
        Me.Label_desccuenta.Size = New System.Drawing.Size(418, 23)
        Me.Label_desccuenta.TabIndex = 27
        Me.Label_desccuenta.Text = "?"
        '
        'GridView_dasientos
        '
        Me.GridView_dasientos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridView_dasientos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.GridView_dasientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridView_dasientos.DefaultCellStyle = DataGridViewCellStyle12
        Me.GridView_dasientos.EnableHeadersVisualStyles = False
        Me.GridView_dasientos.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridView_dasientos.Location = New System.Drawing.Point(4, 102)
        Me.GridView_dasientos.Name = "GridView_dasientos"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridView_dasientos.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.GridView_dasientos.RowHeadersWidth = 20
        Me.GridView_dasientos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridView_dasientos.Size = New System.Drawing.Size(1139, 211)
        Me.GridView_dasientos.TabIndex = 24
        Me.GridView_dasientos.TabStop = False
        '
        'Cr�dito
        '
        Me.Cr�dito.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Cr�dito.BackgroundStyle.Class = ""
        Me.Cr�dito.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Cr�dito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cr�dito.ForeColor = System.Drawing.Color.Black
        Me.Cr�dito.Location = New System.Drawing.Point(1014, 31)
        Me.Cr�dito.Name = "Cr�dito"
        Me.Cr�dito.Size = New System.Drawing.Size(75, 23)
        Me.Cr�dito.TabIndex = 28
        Me.Cr�dito.Text = "Cr�dito"
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.Class = ""
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(909, 31)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(75, 23)
        Me.LabelX4.TabIndex = 24
        Me.LabelX4.Text = "D�bito"
        '
        'TextB_haberlinea
        '
        Me.TextB_haberlinea.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_haberlinea.Border.Class = "TextBoxBorder"
        Me.TextB_haberlinea.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_haberlinea.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_haberlinea.Location = New System.Drawing.Point(1014, 54)
        Me.TextB_haberlinea.Name = "TextB_haberlinea"
        Me.TextB_haberlinea.Size = New System.Drawing.Size(100, 21)
        Me.TextB_haberlinea.TabIndex = 41
        '
        'TextB_debelinea
        '
        Me.TextB_debelinea.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_debelinea.Border.Class = "TextBoxBorder"
        Me.TextB_debelinea.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_debelinea.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_debelinea.Location = New System.Drawing.Point(908, 54)
        Me.TextB_debelinea.Name = "TextB_debelinea"
        Me.TextB_debelinea.Size = New System.Drawing.Size(100, 21)
        Me.TextB_debelinea.TabIndex = 40
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.Class = ""
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(313, 31)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(153, 23)
        Me.LabelX3.TabIndex = 22
        Me.LabelX3.Text = "Cuenta Contable"
        '
        'TextB_cdgocuenta
        '
        Me.TextB_cdgocuenta.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_cdgocuenta.Border.Class = "TextBoxBorder"
        Me.TextB_cdgocuenta.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_cdgocuenta.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_cdgocuenta.Location = New System.Drawing.Point(313, 54)
        Me.TextB_cdgocuenta.Name = "TextB_cdgocuenta"
        Me.TextB_cdgocuenta.Size = New System.Drawing.Size(170, 21)
        Me.TextB_cdgocuenta.TabIndex = 38
        '
        'TextB_Numedocnuevo
        '
        Me.TextB_Numedocnuevo.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_Numedocnuevo.Border.Class = "TextBoxBorder"
        Me.TextB_Numedocnuevo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_Numedocnuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_Numedocnuevo.Location = New System.Drawing.Point(72, 54)
        Me.TextB_Numedocnuevo.Name = "TextB_Numedocnuevo"
        Me.TextB_Numedocnuevo.Size = New System.Drawing.Size(100, 21)
        Me.TextB_Numedocnuevo.TabIndex = 37
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(72, 31)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(100, 23)
        Me.LabelX2.TabIndex = 21
        Me.LabelX2.Text = "No.Documento"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(14, 31)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 20
        Me.LabelX1.Text = "No.L�nea"
        '
        'TextB_lineanueva
        '
        Me.TextB_lineanueva.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_lineanueva.Border.Class = "TextBoxBorder"
        Me.TextB_lineanueva.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_lineanueva.Enabled = False
        Me.TextB_lineanueva.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_lineanueva.Location = New System.Drawing.Point(14, 54)
        Me.TextB_lineanueva.Name = "TextB_lineanueva"
        Me.TextB_lineanueva.ReadOnly = True
        Me.TextB_lineanueva.Size = New System.Drawing.Size(50, 21)
        Me.TextB_lineanueva.TabIndex = 28
        Me.TextB_lineanueva.TabStop = False
        '
        'Bar2
        '
        Me.Bar2.AccessibleDescription = "DotNetBar Bar (Bar2)"
        Me.Bar2.AccessibleName = "DotNetBar Bar"
        Me.Bar2.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar2.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonI_agregar_nuevalinea, Me.ButtonItem3, Me.ButtonI_eliminalinea, Me.Button_cerrar, Me.ButtonI_Anularasiento, Me.ButtonItem7, Me.ButtonItem_reestablecerdocuemento})
        Me.Bar2.Location = New System.Drawing.Point(3, 3)
        Me.Bar2.MenuBar = True
        Me.Bar2.Name = "Bar2"
        Me.Bar2.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar2.Size = New System.Drawing.Size(1140, 26)
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar2.TabIndex = 27
        Me.Bar2.TabStop = False
        Me.Bar2.Text = "Bar2"
        '
        'ButtonI_agregar_nuevalinea
        '
        Me.ButtonI_agregar_nuevalinea.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_agregar_nuevalinea.ForeColor = System.Drawing.Color.White
        Me.ButtonI_agregar_nuevalinea.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Add
        Me.ButtonI_agregar_nuevalinea.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_agregar_nuevalinea.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ButtonI_agregar_nuevalinea.Name = "ButtonI_agregar_nuevalinea"
        Me.ButtonI_agregar_nuevalinea.Text = "Nueva L�nea"
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem3.ForeColor = System.Drawing.Color.White
        Me.ButtonItem3.Image = CType(resources.GetObject("ButtonItem3.Image"), System.Drawing.Image)
        Me.ButtonItem3.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem3.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.Text = "Guardar"
        Me.ButtonItem3.Tooltip = "Guardar Datos Generales del Asiento"
        '
        'ButtonI_eliminalinea
        '
        Me.ButtonI_eliminalinea.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_eliminalinea.ForeColor = System.Drawing.Color.White
        Me.ButtonI_eliminalinea.Image = CType(resources.GetObject("ButtonI_eliminalinea.Image"), System.Drawing.Image)
        Me.ButtonI_eliminalinea.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_eliminalinea.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ButtonI_eliminalinea.Name = "ButtonI_eliminalinea"
        Me.ButtonI_eliminalinea.Text = "Eliminar L�nea"
        '
        'Button_cerrar
        '
        Me.Button_cerrar.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.Button_cerrar.ForeColor = System.Drawing.Color.White
        Me.Button_cerrar.Image = CType(resources.GetObject("Button_cerrar.Image"), System.Drawing.Image)
        Me.Button_cerrar.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.Button_cerrar.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.Button_cerrar.Name = "Button_cerrar"
        Me.Button_cerrar.Text = "Cerrar"
        '
        'ButtonI_Anularasiento
        '
        Me.ButtonI_Anularasiento.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_Anularasiento.ForeColor = System.Drawing.Color.White
        Me.ButtonI_Anularasiento.Image = Global.Sistemas_Gerenciales.My.Resources.Resources._Private
        Me.ButtonI_Anularasiento.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_Anularasiento.Name = "ButtonI_Anularasiento"
        Me.ButtonI_Anularasiento.Text = "Anular Asiento"
        '
        'ButtonItem7
        '
        Me.ButtonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem7.ForeColor = System.Drawing.Color.White
        Me.ButtonItem7.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Recyclebin_Empty
        Me.ButtonItem7.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.Text = "Borrar Documento"
        '
        'ButtonItem_reestablecerdocuemento
        '
        Me.ButtonItem_reestablecerdocuemento.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_reestablecerdocuemento.ForeColor = System.Drawing.Color.White
        Me.ButtonItem_reestablecerdocuemento.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.imageres_dll_202_16
        Me.ButtonItem_reestablecerdocuemento.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_reestablecerdocuemento.Name = "ButtonItem_reestablecerdocuemento"
        Me.ButtonItem_reestablecerdocuemento.Text = "Restablecer Documento"
        '
        'expandablePanel4
        '
        Me.expandablePanel4.AutoScroll = True
        Me.expandablePanel4.CanvasColor = System.Drawing.SystemColors.Control
        Me.expandablePanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.expandablePanel4.Controls.Add(Me.itemPanel1)
        Me.expandablePanel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.expandablePanel4.ExpandOnTitleClick = True
        Me.expandablePanel4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.expandablePanel4.Location = New System.Drawing.Point(0, 26)
        Me.expandablePanel4.Name = "expandablePanel4"
        Me.expandablePanel4.Size = New System.Drawing.Size(1160, 131)
        Me.expandablePanel4.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.expandablePanel4.Style.BackColor1.Color = System.Drawing.Color.White
        Me.expandablePanel4.Style.BackColor2.Color = System.Drawing.Color.White
        Me.expandablePanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.expandablePanel4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.expandablePanel4.Style.GradientAngle = 90
        Me.expandablePanel4.TabIndex = 13
        Me.expandablePanel4.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.expandablePanel4.TitleStyle.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(175, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.expandablePanel4.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.expandablePanel4.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.expandablePanel4.TitleStyle.GradientAngle = 90
        Me.expandablePanel4.TitleStyle.MarginLeft = 12
        Me.expandablePanel4.TitleText = "Datos Generales del Asiento"
        '
        'itemPanel1
        '
        '
        '
        '
        Me.itemPanel1.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.itemPanel1.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel1.BackgroundStyle.BorderBottomWidth = 1
        Me.itemPanel1.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.itemPanel1.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel1.BackgroundStyle.BorderLeftWidth = 1
        Me.itemPanel1.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel1.BackgroundStyle.BorderRightWidth = 1
        Me.itemPanel1.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.itemPanel1.BackgroundStyle.BorderTopWidth = 1
        Me.itemPanel1.BackgroundStyle.Class = ""
        Me.itemPanel1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.itemPanel1.BackgroundStyle.PaddingBottom = 1
        Me.itemPanel1.BackgroundStyle.PaddingLeft = 1
        Me.itemPanel1.BackgroundStyle.PaddingRight = 1
        Me.itemPanel1.BackgroundStyle.PaddingTop = 1
        Me.itemPanel1.ContainerControlProcessDialogKey = True
        Me.itemPanel1.Controls.Add(Me.LabelX21)
        Me.itemPanel1.Controls.Add(Me.ItemPanel5)
        Me.itemPanel1.Controls.Add(Me.LabelX17)
        Me.itemPanel1.Controls.Add(Me.TextField_Descrip_asiento_detalle)
        Me.itemPanel1.Controls.Add(Me.LabelX16)
        Me.itemPanel1.Controls.Add(Me.TextB_numeasiento_nuevo)
        Me.itemPanel1.Controls.Add(Me.LabelX15)
        Me.itemPanel1.Controls.Add(Me.LabelX14)
        Me.itemPanel1.Controls.Add(Me.LabelX13)
        Me.itemPanel1.Controls.Add(Me.DateF_Fechaasiento_Nuevo)
        Me.itemPanel1.Controls.Add(Me.TextB_mesnuevo)
        Me.itemPanel1.Controls.Add(Me.TextB_anonuevo)
        Me.itemPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.itemPanel1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.itemPanel1.Location = New System.Drawing.Point(0, 26)
        Me.itemPanel1.Name = "itemPanel1"
        Me.itemPanel1.Size = New System.Drawing.Size(1160, 105)
        Me.itemPanel1.TabIndex = 16
        Me.itemPanel1.Text = "itemPanel1"
        '
        'ItemPanel5
        '
        '
        '
        '
        Me.ItemPanel5.BackgroundStyle.Class = "ItemPanel"
        Me.ItemPanel5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel5.ContainerControlProcessDialogKey = True
        Me.ItemPanel5.Controls.Add(Me.NumberF_diferencia_Mov)
        Me.ItemPanel5.Controls.Add(Me.NumberF_habelocal)
        Me.ItemPanel5.Controls.Add(Me.NumberF_debelocal)
        Me.ItemPanel5.Controls.Add(Me.LabelX20)
        Me.ItemPanel5.Controls.Add(Me.LabelX19)
        Me.ItemPanel5.Controls.Add(Me.LabelX18)
        Me.ItemPanel5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel5.Location = New System.Drawing.Point(652, 30)
        Me.ItemPanel5.Name = "ItemPanel5"
        Me.ItemPanel5.Size = New System.Drawing.Size(396, 69)
        Me.ItemPanel5.TabIndex = 42
        '
        'NumberF_diferencia_Mov
        '
        '
        '
        '
        Me.NumberF_diferencia_Mov.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.NumberF_diferencia_Mov.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.NumberF_diferencia_Mov.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.NumberF_diferencia_Mov.Enabled = False
        Me.NumberF_diferencia_Mov.Increment = 1.0R
        Me.NumberF_diferencia_Mov.Location = New System.Drawing.Point(280, 36)
        Me.NumberF_diferencia_Mov.Name = "NumberF_diferencia_Mov"
        Me.NumberF_diferencia_Mov.Size = New System.Drawing.Size(109, 21)
        Me.NumberF_diferencia_Mov.TabIndex = 34
        '
        'NumberF_habelocal
        '
        '
        '
        '
        Me.NumberF_habelocal.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.NumberF_habelocal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.NumberF_habelocal.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.NumberF_habelocal.Enabled = False
        Me.NumberF_habelocal.Increment = 1.0R
        Me.NumberF_habelocal.Location = New System.Drawing.Point(149, 36)
        Me.NumberF_habelocal.Name = "NumberF_habelocal"
        Me.NumberF_habelocal.Size = New System.Drawing.Size(110, 21)
        Me.NumberF_habelocal.TabIndex = 33
        '
        'NumberF_debelocal
        '
        '
        '
        '
        Me.NumberF_debelocal.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.NumberF_debelocal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.NumberF_debelocal.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.NumberF_debelocal.Enabled = False
        Me.NumberF_debelocal.Increment = 1.0R
        Me.NumberF_debelocal.Location = New System.Drawing.Point(17, 36)
        Me.NumberF_debelocal.Name = "NumberF_debelocal"
        Me.NumberF_debelocal.Size = New System.Drawing.Size(116, 21)
        Me.NumberF_debelocal.TabIndex = 32
        '
        'LabelX20
        '
        Me.LabelX20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX20.BackgroundStyle.Class = ""
        Me.LabelX20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX20.ForeColor = System.Drawing.Color.Black
        Me.LabelX20.Location = New System.Drawing.Point(280, 10)
        Me.LabelX20.Name = "LabelX20"
        Me.LabelX20.Size = New System.Drawing.Size(87, 23)
        Me.LabelX20.TabIndex = 23
        Me.LabelX20.Text = "Descuadre"
        '
        'LabelX19
        '
        Me.LabelX19.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX19.BackgroundStyle.Class = ""
        Me.LabelX19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX19.ForeColor = System.Drawing.Color.Black
        Me.LabelX19.Location = New System.Drawing.Point(149, 11)
        Me.LabelX19.Name = "LabelX19"
        Me.LabelX19.Size = New System.Drawing.Size(78, 23)
        Me.LabelX19.TabIndex = 22
        Me.LabelX19.Text = "Cr�dito"
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX18.BackgroundStyle.Class = ""
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(17, 11)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(78, 23)
        Me.LabelX18.TabIndex = 21
        Me.LabelX18.Text = "D�bito"
        '
        'TextB_anonuevo
        '
        Me.TextB_anonuevo.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_anonuevo.Border.Class = "TextBoxBorder"
        Me.TextB_anonuevo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_anonuevo.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_anonuevo.Location = New System.Drawing.Point(381, 32)
        Me.TextB_anonuevo.Name = "TextB_anonuevo"
        Me.TextB_anonuevo.Size = New System.Drawing.Size(100, 21)
        Me.TextB_anonuevo.TabIndex = 32
        '
        'ComboItem3
        '
        Me.ComboItem3.Text = "Totales por Comprobante"
        '
        'ComboItem1
        '
        Me.ComboItem1.Text = "Prueba"
        '
        'ComboItem2
        '
        Me.ComboItem2.Text = "Totales por Comprobante"
        '
        'ComboItem4
        '
        Me.ComboItem4.Text = "Totales por Comprobante"
        '
        'LabelItem_pantalla
        '
        Me.LabelItem_pantalla.Enabled = False
        Me.LabelItem_pantalla.Name = "LabelItem_pantalla"
        Me.LabelItem_pantalla.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'ComboB_mes_TabBusquedas
        '
        Me.ComboB_mes_TabBusquedas.DisplayMember = "Text"
        Me.ComboB_mes_TabBusquedas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_mes_TabBusquedas.FormattingEnabled = True
        Me.ComboB_mes_TabBusquedas.ItemHeight = 15
        Me.ComboB_mes_TabBusquedas.Location = New System.Drawing.Point(436, 66)
        Me.ComboB_mes_TabBusquedas.Name = "ComboB_mes_TabBusquedas"
        Me.ComboB_mes_TabBusquedas.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_mes_TabBusquedas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_mes_TabBusquedas.TabIndex = 4
        '
        'ComboB_ano_TabBusquedas
        '
        Me.ComboB_ano_TabBusquedas.DisplayMember = "Text"
        Me.ComboB_ano_TabBusquedas.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.ComboB_ano_TabBusquedas.FormattingEnabled = True
        Me.ComboB_ano_TabBusquedas.ItemHeight = 15
        Me.ComboB_ano_TabBusquedas.Location = New System.Drawing.Point(436, 37)
        Me.ComboB_ano_TabBusquedas.Name = "ComboB_ano_TabBusquedas"
        Me.ComboB_ano_TabBusquedas.Size = New System.Drawing.Size(195, 21)
        Me.ComboB_ano_TabBusquedas.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ComboB_ano_TabBusquedas.TabIndex = 3
        '
        'LabelX27
        '
        Me.LabelX27.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX27.BackgroundStyle.Class = ""
        Me.LabelX27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX27.ForeColor = System.Drawing.Color.Black
        Me.LabelX27.Location = New System.Drawing.Point(698, 69)
        Me.LabelX27.Name = "LabelX27"
        Me.LabelX27.Size = New System.Drawing.Size(75, 23)
        Me.LabelX27.TabIndex = 42
        Me.LabelX27.Text = "Fecha Hasta"
        '
        'LabelX26
        '
        Me.LabelX26.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX26.BackgroundStyle.Class = ""
        Me.LabelX26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX26.ForeColor = System.Drawing.Color.Black
        Me.LabelX26.Location = New System.Drawing.Point(697, 37)
        Me.LabelX26.Name = "LabelX26"
        Me.LabelX26.Size = New System.Drawing.Size(75, 23)
        Me.LabelX26.TabIndex = 41
        Me.LabelX26.Text = "Fecha Desde"
        '
        'LabelX25
        '
        Me.LabelX25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX25.BackgroundStyle.Class = ""
        Me.LabelX25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX25.ForeColor = System.Drawing.Color.Black
        Me.LabelX25.Location = New System.Drawing.Point(383, 64)
        Me.LabelX25.Name = "LabelX25"
        Me.LabelX25.Size = New System.Drawing.Size(44, 23)
        Me.LabelX25.TabIndex = 40
        Me.LabelX25.Text = "Mes"
        '
        'LabelX24
        '
        Me.LabelX24.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX24.BackgroundStyle.Class = ""
        Me.LabelX24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX24.ForeColor = System.Drawing.Color.Black
        Me.LabelX24.Location = New System.Drawing.Point(386, 36)
        Me.LabelX24.Name = "LabelX24"
        Me.LabelX24.Size = New System.Drawing.Size(44, 23)
        Me.LabelX24.TabIndex = 39
        Me.LabelX24.Text = "A�o"
        '
        'LabelX23
        '
        Me.LabelX23.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX23.BackgroundStyle.Class = ""
        Me.LabelX23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX23.ForeColor = System.Drawing.Color.Black
        Me.LabelX23.Location = New System.Drawing.Point(41, 65)
        Me.LabelX23.Name = "LabelX23"
        Me.LabelX23.Size = New System.Drawing.Size(75, 23)
        Me.LabelX23.TabIndex = 38
        Me.LabelX23.Text = "No.Documento"
        '
        'LabelX22
        '
        Me.LabelX22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX22.BackgroundStyle.Class = ""
        Me.LabelX22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX22.ForeColor = System.Drawing.Color.Black
        Me.LabelX22.Location = New System.Drawing.Point(44, 37)
        Me.LabelX22.Name = "LabelX22"
        Me.LabelX22.Size = New System.Drawing.Size(75, 23)
        Me.LabelX22.TabIndex = 37
        Me.LabelX22.Text = "No.Asiento"
        '
        'DateF_FechaIni_compro
        '
        '
        '
        '
        Me.DateF_FechaIni_compro.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaIni_compro.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_compro.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaIni_compro.ButtonDropDown.Visible = True
        Me.DateF_FechaIni_compro.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaIni_compro.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaIni_compro.IsPopupCalendarOpen = False
        Me.DateF_FechaIni_compro.Location = New System.Drawing.Point(792, 38)
        '
        '
        '
        Me.DateF_FechaIni_compro.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni_compro.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaIni_compro.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_compro.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaIni_compro.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaIni_compro.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_compro.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaIni_compro.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaIni_compro.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaIni_compro.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaIni_compro.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaIni_compro.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaIni_compro.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaIni_compro.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaIni_compro.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaIni_compro.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaIni_compro.Name = "DateF_FechaIni_compro"
        Me.DateF_FechaIni_compro.Size = New System.Drawing.Size(200, 21)
        Me.DateF_FechaIni_compro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaIni_compro.TabIndex = 5
        '
        'TextF_Documento
        '
        Me.TextF_Documento.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextF_Documento.Border.Class = "TextBoxBorder"
        Me.TextF_Documento.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextF_Documento.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextF_Documento.Location = New System.Drawing.Point(135, 67)
        Me.TextF_Documento.Name = "TextF_Documento"
        Me.TextF_Documento.Size = New System.Drawing.Size(195, 21)
        Me.TextF_Documento.TabIndex = 2
        '
        'TextF_numecompro
        '
        Me.TextF_numecompro.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextF_numecompro.Border.Class = "TextBoxBorder"
        Me.TextF_numecompro.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextF_numecompro.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextF_numecompro.Location = New System.Drawing.Point(135, 36)
        Me.TextF_numecompro.Name = "TextF_numecompro"
        Me.TextF_numecompro.Size = New System.Drawing.Size(195, 21)
        Me.TextF_numecompro.TabIndex = 1
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'DateF_FechaFin_compro
        '
        '
        '
        '
        Me.DateF_FechaFin_compro.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.DateF_FechaFin_compro.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_compro.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown
        Me.DateF_FechaFin_compro.ButtonDropDown.Visible = True
        Me.DateF_FechaFin_compro.CustomFormat = "dd/MM/yyyy"
        Me.DateF_FechaFin_compro.Format = DevComponents.Editors.eDateTimePickerFormat.Custom
        Me.DateF_FechaFin_compro.IsPopupCalendarOpen = False
        Me.DateF_FechaFin_compro.Location = New System.Drawing.Point(793, 67)
        '
        '
        '
        Me.DateF_FechaFin_compro.MonthCalendar.AnnuallyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin_compro.MonthCalendar.BackgroundStyle.Class = ""
        Me.DateF_FechaFin_compro.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_compro.MonthCalendar.CalendarDimensions = New System.Drawing.Size(1, 1)
        Me.DateF_FechaFin_compro.MonthCalendar.ClearButtonVisible = True
        '
        '
        '
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.Class = ""
        Me.DateF_FechaFin_compro.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_compro.MonthCalendar.DisplayMonth = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.DateF_FechaFin_compro.MonthCalendar.MarkedDates = New Date(-1) {}
        Me.DateF_FechaFin_compro.MonthCalendar.MonthlyMarkedDates = New Date(-1) {}
        '
        '
        '
        Me.DateF_FechaFin_compro.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.DateF_FechaFin_compro.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90
        Me.DateF_FechaFin_compro.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.DateF_FechaFin_compro.MonthCalendar.NavigationBackgroundStyle.Class = ""
        Me.DateF_FechaFin_compro.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.DateF_FechaFin_compro.MonthCalendar.TodayButtonVisible = True
        Me.DateF_FechaFin_compro.MonthCalendar.WeeklyMarkedDays = New System.DayOfWeek(-1) {}
        Me.DateF_FechaFin_compro.Name = "DateF_FechaFin_compro"
        Me.DateF_FechaFin_compro.Size = New System.Drawing.Size(200, 21)
        Me.DateF_FechaFin_compro.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DateF_FechaFin_compro.TabIndex = 6
        '
        'CI_totalCompro
        '
        Me.CI_totalCompro.Text = "Totales por Comprobante"
        '
        'SuperTabItem_Buscar
        '
        Me.SuperTabItem_Buscar.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem_Buscar.GlobalItem = False
        Me.SuperTabItem_Buscar.Name = "SuperTabItem_Buscar"
        Me.SuperTabItem_Buscar.Text = "B�squedas"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.ExpandablePanel3)
        Me.SuperTabControlPanel2.Controls.Add(Me.ExpandablePanel2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(1172, 508)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem_Buscar
        '
        'ExpandablePanel3
        '
        Me.ExpandablePanel3.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel3.Controls.Add(Me.ItemPanel4)
        Me.ExpandablePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel3.ExpandOnTitleClick = True
        Me.ExpandablePanel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel3.Location = New System.Drawing.Point(0, 149)
        Me.ExpandablePanel3.Name = "ExpandablePanel3"
        Me.ExpandablePanel3.Size = New System.Drawing.Size(1172, 438)
        Me.ExpandablePanel3.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel3.Style.BackColor1.Color = System.Drawing.SystemColors.MenuHighlight
        Me.ExpandablePanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel3.Style.GradientAngle = 90
        Me.ExpandablePanel3.TabIndex = 0
        Me.ExpandablePanel3.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel3.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel3.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel3.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel3.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel3.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel3.TitleText = "Registros Encontrados"
        '
        'ItemPanel4
        '
        '
        '
        '
        Me.ItemPanel4.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel4.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel4.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel4.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel4.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel4.BackgroundStyle.Class = ""
        Me.ItemPanel4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel4.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel4.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel4.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel4.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel4.ContainerControlProcessDialogKey = True
        Me.ItemPanel4.Controls.Add(Me.GridP_Asientos)
        Me.ItemPanel4.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel4.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel4.Name = "ItemPanel4"
        Me.ItemPanel4.Size = New System.Drawing.Size(1173, 409)
        Me.ItemPanel4.TabIndex = 3
        Me.ItemPanel4.Text = "ItemPanel4"
        '
        'GridP_Asientos
        '
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.GridP_Asientos.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle14
        Me.GridP_Asientos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_Asientos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.GridP_Asientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridP_Asientos.DefaultCellStyle = DataGridViewCellStyle16
        Me.GridP_Asientos.EnableHeadersVisualStyles = False
        Me.GridP_Asientos.GridColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer))
        Me.GridP_Asientos.Location = New System.Drawing.Point(4, 4)
        Me.GridP_Asientos.Name = "GridP_Asientos"
        Me.GridP_Asientos.ReadOnly = True
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_Asientos.RowHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.GridP_Asientos.RowHeadersWidth = 20
        Me.GridP_Asientos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridP_Asientos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridP_Asientos.Size = New System.Drawing.Size(1158, 320)
        Me.GridP_Asientos.TabIndex = 0
        '
        'ExpandablePanel2
        '
        Me.ExpandablePanel2.CanvasColor = System.Drawing.SystemColors.Control
        Me.ExpandablePanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.ExpandablePanel2.Controls.Add(Me.ItemPanel3)
        Me.ExpandablePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.ExpandablePanel2.ExpandOnTitleClick = True
        Me.ExpandablePanel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExpandablePanel2.Location = New System.Drawing.Point(0, 0)
        Me.ExpandablePanel2.Name = "ExpandablePanel2"
        Me.ExpandablePanel2.Size = New System.Drawing.Size(1172, 149)
        Me.ExpandablePanel2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.ExpandablePanel2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandablePanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.ExpandablePanel2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandablePanel2.Style.GradientAngle = 90
        Me.ExpandablePanel2.TabIndex = 0
        Me.ExpandablePanel2.TitleStyle.BackColor1.Color = System.Drawing.SystemColors.MenuText
        Me.ExpandablePanel2.TitleStyle.BackColor2.Color = System.Drawing.SystemColors.GradientActiveCaption
        Me.ExpandablePanel2.TitleStyle.Border = DevComponents.DotNetBar.eBorderType.RaisedInner
        Me.ExpandablePanel2.TitleStyle.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Custom
        Me.ExpandablePanel2.TitleStyle.BorderWidth = 0
        Me.ExpandablePanel2.TitleStyle.ForeColor.Color = System.Drawing.Color.White
        Me.ExpandablePanel2.TitleStyle.GradientAngle = 90
        Me.ExpandablePanel2.TitleStyle.MarginLeft = 12
        Me.ExpandablePanel2.TitleText = "Opciones de B�squedas"
        '
        'ItemPanel3
        '
        '
        '
        '
        Me.ItemPanel3.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.ItemPanel3.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderBottomWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.ItemPanel3.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderLeftWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderRightWidth = 1
        Me.ItemPanel3.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ItemPanel3.BackgroundStyle.BorderTopWidth = 1
        Me.ItemPanel3.BackgroundStyle.Class = ""
        Me.ItemPanel3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemPanel3.BackgroundStyle.PaddingBottom = 1
        Me.ItemPanel3.BackgroundStyle.PaddingLeft = 1
        Me.ItemPanel3.BackgroundStyle.PaddingRight = 1
        Me.ItemPanel3.BackgroundStyle.PaddingTop = 1
        Me.ItemPanel3.ContainerControlProcessDialogKey = True
        Me.ItemPanel3.Controls.Add(Me.Label_desccuenta_buscar)
        Me.ItemPanel3.Controls.Add(Me.TextB_cdgocuenta_buscar)
        Me.ItemPanel3.Controls.Add(Me.LabelX6)
        Me.ItemPanel3.Controls.Add(Me.Bar1)
        Me.ItemPanel3.Controls.Add(Me.ComboB_mes_TabBusquedas)
        Me.ItemPanel3.Controls.Add(Me.ComboB_ano_TabBusquedas)
        Me.ItemPanel3.Controls.Add(Me.LabelX27)
        Me.ItemPanel3.Controls.Add(Me.LabelX26)
        Me.ItemPanel3.Controls.Add(Me.LabelX25)
        Me.ItemPanel3.Controls.Add(Me.LabelX24)
        Me.ItemPanel3.Controls.Add(Me.LabelX23)
        Me.ItemPanel3.Controls.Add(Me.LabelX22)
        Me.ItemPanel3.Controls.Add(Me.DateF_FechaFin_compro)
        Me.ItemPanel3.Controls.Add(Me.DateF_FechaIni_compro)
        Me.ItemPanel3.Controls.Add(Me.TextF_Documento)
        Me.ItemPanel3.Controls.Add(Me.TextF_numecompro)
        Me.ItemPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemPanel3.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ControlContainerItem2})
        Me.ItemPanel3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemPanel3.Location = New System.Drawing.Point(0, 26)
        Me.ItemPanel3.Name = "ItemPanel3"
        Me.ItemPanel3.Size = New System.Drawing.Size(1172, 123)
        Me.ItemPanel3.TabIndex = 0
        Me.ItemPanel3.Text = "ItemPanel3"
        '
        'Label_desccuenta_buscar
        '
        Me.Label_desccuenta_buscar.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label_desccuenta_buscar.BackgroundStyle.Class = ""
        Me.Label_desccuenta_buscar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label_desccuenta_buscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_desccuenta_buscar.ForeColor = System.Drawing.Color.Red
        Me.Label_desccuenta_buscar.Location = New System.Drawing.Point(336, 95)
        Me.Label_desccuenta_buscar.Name = "Label_desccuenta_buscar"
        Me.Label_desccuenta_buscar.Size = New System.Drawing.Size(589, 23)
        Me.Label_desccuenta_buscar.TabIndex = 48
        Me.Label_desccuenta_buscar.Text = "?"
        '
        'TextB_cdgocuenta_buscar
        '
        Me.TextB_cdgocuenta_buscar.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.TextB_cdgocuenta_buscar.Border.Class = "TextBoxBorder"
        Me.TextB_cdgocuenta_buscar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextB_cdgocuenta_buscar.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TextB_cdgocuenta_buscar.Location = New System.Drawing.Point(135, 97)
        Me.TextB_cdgocuenta_buscar.Name = "TextB_cdgocuenta_buscar"
        Me.TextB_cdgocuenta_buscar.Size = New System.Drawing.Size(195, 21)
        Me.TextB_cdgocuenta_buscar.TabIndex = 47
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.Class = ""
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(43, 92)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(75, 23)
        Me.LabelX6.TabIndex = 46
        Me.LabelX6.Text = "No.Cuenta"
        '
        'Bar1
        '
        Me.Bar1.AccessibleDescription = "DotNetBar Bar (Bar1)"
        Me.Bar1.AccessibleName = "DotNetBar Bar"
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar1.BackColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.Bar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ToolbarB_BuscarAsiento, Me.ButtonI_limpiar_busqueda, Me.ButtonI_GenerarReporte, Me.LabelItem1, Me.ComboBoxItem_SelectReport})
        Me.Bar1.Location = New System.Drawing.Point(2, 2)
        Me.Bar1.MenuBar = True
        Me.Bar1.Name = "Bar1"
        Me.Bar1.SingleLineColor = System.Drawing.Color.Transparent
        Me.Bar1.Size = New System.Drawing.Size(1171, 26)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010
        Me.Bar1.TabIndex = 45
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Bar1"
        '
        'ToolbarB_BuscarAsiento
        '
        Me.ToolbarB_BuscarAsiento.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ToolbarB_BuscarAsiento.ForeColor = System.Drawing.Color.White
        Me.ToolbarB_BuscarAsiento.Image = CType(resources.GetObject("ToolbarB_BuscarAsiento.Image"), System.Drawing.Image)
        Me.ToolbarB_BuscarAsiento.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ToolbarB_BuscarAsiento.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.ToolbarB_BuscarAsiento.Name = "ToolbarB_BuscarAsiento"
        Me.ToolbarB_BuscarAsiento.Text = "Buscar Asiento  "
        '
        'ButtonI_limpiar_busqueda
        '
        Me.ButtonI_limpiar_busqueda.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_limpiar_busqueda.ForeColor = System.Drawing.Color.White
        Me.ButtonI_limpiar_busqueda.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.ResourceForkEraser
        Me.ButtonI_limpiar_busqueda.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_limpiar_busqueda.Name = "ButtonI_limpiar_busqueda"
        Me.ButtonI_limpiar_busqueda.Text = "Limpiar Pantalla  "
        '
        'ButtonI_GenerarReporte
        '
        Me.ButtonI_GenerarReporte.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonI_GenerarReporte.ForeColor = System.Drawing.Color.White
        Me.ButtonI_GenerarReporte.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.reporteria
        Me.ButtonI_GenerarReporte.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonI_GenerarReporte.Name = "ButtonI_GenerarReporte"
        Me.ButtonI_GenerarReporte.Text = " Generar Reporte"
        '
        'LabelItem1
        '
        Me.LabelItem1.ForeColor = System.Drawing.Color.White
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "      Seleccione Reporte: "
        '
        'ComboBoxItem_SelectReport
        '
        Me.ComboBoxItem_SelectReport.Caption = "Seleccionar Reporte"
        Me.ComboBoxItem_SelectReport.ComboWidth = 220
        Me.ComboBoxItem_SelectReport.Description = "Seleccionar Reporte"
        Me.ComboBoxItem_SelectReport.DropDownHeight = 106
        Me.ComboBoxItem_SelectReport.Items.AddRange(New Object() {Me.CI_totalCompro})
        Me.ComboBoxItem_SelectReport.Name = "ComboBoxItem_SelectReport"
        Me.ComboBoxItem_SelectReport.Text = "Seleccionar Reporte"
        Me.ComboBoxItem_SelectReport.Tooltip = "Seleccionar Reporte"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel4)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel3)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(1, 12)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(1172, 534)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 16
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem_Buscar, Me.SuperTabItem_detalle, Me.SuperTabItem_revisaAsiento, Me.SuperTabI_asientosRecFact, Me.ButtonItem_NuevoAsiento, Me.ButtonItem_menuprincipal, Me.ButtonItem_cerrarpantalla, Me.LabelItem_pantalla})
        Me.SuperTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'ButtonItem_NuevoAsiento
        '
        Me.ButtonItem_NuevoAsiento.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_NuevoAsiento.Image = Global.Sistemas_Gerenciales.My.Resources.Resources.Add
        Me.ButtonItem_NuevoAsiento.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_NuevoAsiento.Name = "ButtonItem_NuevoAsiento"
        Me.ButtonItem_NuevoAsiento.Text = "Nuevo Asiento"
        '
        'ButtonItem_menuprincipal
        '
        Me.ButtonItem_menuprincipal.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_menuprincipal.Image = CType(resources.GetObject("ButtonItem_menuprincipal.Image"), System.Drawing.Image)
        Me.ButtonItem_menuprincipal.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_menuprincipal.Name = "ButtonItem_menuprincipal"
        Me.ButtonItem_menuprincipal.Text = "Men� Principal"
        Me.ButtonItem_menuprincipal.Tooltip = "Retorna al Men� Principal de la Contabilidad"
        '
        'ButtonItem_cerrarpantalla
        '
        Me.ButtonItem_cerrarpantalla.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem_cerrarpantalla.Image = CType(resources.GetObject("ButtonItem_cerrarpantalla.Image"), System.Drawing.Image)
        Me.ButtonItem_cerrarpantalla.ImageFixedSize = New System.Drawing.Size(18, 18)
        Me.ButtonItem_cerrarpantalla.Name = "ButtonItem_cerrarpantalla"
        Me.ButtonItem_cerrarpantalla.Text = "Cerrar"
        Me.ButtonItem_cerrarpantalla.Tooltip = "Cierra la Ventana y Sale de la Aplicaci�n"
        '
        'BalloonTip
        '
        Me.BalloonTip.AutoClose = False
        Me.BalloonTip.CaptionImage = Global.Sistemas_Gerenciales.My.Resources.Resources.Notepad
        Me.BalloonTip.ShowBalloonOnFocus = True
        '
        'frmAsientosContables
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1174, 546)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAsientosContables"
        Me.Text = "Asientos/Comprobantes Contables"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SuperTabControlPanel4.ResumeLayout(False)
        CType(Me.Bar5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Bar5.ResumeLayout(False)
        Me.PanelEx1.ResumeLayout(False)
        Me.ExpandablePanel8.ResumeLayout(False)
        CType(Me.DGV_Detalle_tipomov, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExpandablePanel7.ResumeLayout(False)
        Me.ExpandablePanel6.ResumeLayout(False)
        CType(Me.DGV_asientos_tipomov, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.ItemPanel6.ResumeLayout(False)
        Me.ExpandablePanel5.ResumeLayout(False)
        Me.ItemPanel7.ResumeLayout(False)
        CType(Me.GridP_inconsistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateF_Fechaasiento_Nuevo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.expandablePanel1.ResumeLayout(False)
        CType(Me.Bar3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.expandablePanel_lineas.ResumeLayout(False)
        Me.itemPanel2.ResumeLayout(False)
        CType(Me.DateF_FechaDoc_Nuevo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView_dasientos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.expandablePanel4.ResumeLayout(False)
        Me.itemPanel1.ResumeLayout(False)
        Me.ItemPanel5.ResumeLayout(False)
        CType(Me.NumberF_diferencia_Mov, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumberF_habelocal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumberF_debelocal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateF_FechaIni_compro, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateF_FechaFin_compro, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ExpandablePanel3.ResumeLayout(False)
        Me.ItemPanel4.ResumeLayout(False)
        CType(Me.GridP_Asientos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExpandablePanel2.ResumeLayout(False)
        Me.ItemPanel3.ResumeLayout(False)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        CType(Me.BindingSource_dasientos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents Button_buscardocumento_xintegrar As DevComponents.DotNetBar.ButtonX
    Private WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Private WithEvents Bar5 As DevComponents.DotNetBar.Bar
    Private WithEvents TextB_numedoc_xintegrar As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelI_tipomov As DevComponents.DotNetBar.LabelItem
    Private WithEvents ComboB_tipomov As DevComponents.DotNetBar.ComboBoxItem
    Friend WithEvents ComboI_facturas As DevComponents.Editors.ComboItem
    Friend WithEvents ComboI_Recibos As DevComponents.Editors.ComboItem
    Private WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Private WithEvents PanelEx1 As DevComponents.DotNetBar.PanelEx
    Private WithEvents ExpandablePanel8 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents DGV_Detalle_tipomov As DevComponents.DotNetBar.Controls.DataGridViewX
    Private WithEvents ExpandablePanel7 As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents Button_anulalinea_Xintegrar As DevComponents.DotNetBar.ButtonX
    Private WithEvents Button_contabiliza_tipomov As DevComponents.DotNetBar.ButtonX
    Private WithEvents LabelX29 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_desc_tipomov As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents Label_desc_tipomov As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX32 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_cuenta__tipomov As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents TextB_numedoc_tipomov As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX33 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX34 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_nlinea_tipomov As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents ExpandablePanel6 As DevComponents.DotNetBar.ExpandablePanel
    Friend WithEvents DGV_asientos_tipomov As DevComponents.DotNetBar.Controls.DataGridViewX
    Private WithEvents SuperTabI_asientosRecFact As DevComponents.DotNetBar.SuperTabItem
    Private WithEvents SuperTabItem_revisaAsiento As DevComponents.DotNetBar.SuperTabItem
    Private WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Private WithEvents ItemPanel6 As DevComponents.DotNetBar.ItemPanel
    Private WithEvents ExpandablePanel5 As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents ItemPanel7 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents GridP_inconsistencias As DevComponents.DotNetBar.Controls.DataGridViewX
    Private WithEvents Bar4 As DevComponents.DotNetBar.Bar
    Private WithEvents ToolbarB_BuscarInconsistencias As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ToolbarB_Exportadescuadres As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonI_imprimirdescuadre As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ComboB_mes_TabRevisar As DevComponents.DotNetBar.Controls.ComboBoxEx
    Private WithEvents ComboB_ano_TabRevisar As DevComponents.DotNetBar.Controls.ComboBoxEx
    Private WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Private WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Private WithEvents LabelX21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BindingSource_dasientos As System.Windows.Forms.BindingSource
    Private WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextField_Descrip_asiento_detalle As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_numeasiento_nuevo As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Private WithEvents DateF_Fechaasiento_Nuevo As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Private WithEvents TextB_mesnuevo As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents SuperTabItem_detalle As DevComponents.DotNetBar.SuperTabItem
    Private WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Private WithEvents expandablePanel1 As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents Bar3 As DevComponents.DotNetBar.Bar
    Private WithEvents ButtonI_limpiar_detalle As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonI_ImprimeAsiento_detalle As DevComponents.DotNetBar.ButtonItem
    Private WithEvents LabelI_msg_detalle As DevComponents.DotNetBar.LabelItem
    Private WithEvents expandablePanel_lineas As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents itemPanel2 As DevComponents.DotNetBar.ItemPanel
    Private WithEvents ComboB_documentos As DevComponents.DotNetBar.Controls.ComboBoxEx
    Private WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Private WithEvents Button_Guarda_linea As DevComponents.DotNetBar.ButtonX
    Private WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_Deslinea As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents Label_desccuenta As DevComponents.DotNetBar.LabelX
    Friend WithEvents GridView_dasientos As DevComponents.DotNetBar.Controls.DataGridViewX
    Private WithEvents Cr�dito As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_haberlinea As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents TextB_debelinea As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_cdgocuenta As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents TextB_Numedocnuevo As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_lineanueva As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Private WithEvents ButtonI_agregar_nuevalinea As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonI_eliminalinea As DevComponents.DotNetBar.ButtonItem
    Private WithEvents Button_cerrar As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonI_Anularasiento As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem_reestablecerdocuemento As DevComponents.DotNetBar.ButtonItem
    Private WithEvents expandablePanel4 As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents itemPanel1 As DevComponents.DotNetBar.ItemPanel
    Private WithEvents ItemPanel5 As DevComponents.DotNetBar.ItemPanel
    Private WithEvents NumberF_diferencia_Mov As DevComponents.Editors.DoubleInput
    Private WithEvents NumberF_habelocal As DevComponents.Editors.DoubleInput
    Private WithEvents NumberF_debelocal As DevComponents.Editors.DoubleInput
    Private WithEvents LabelX20 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX19 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_anonuevo As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents ButtonItem_NuevoAsiento As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem_menuprincipal As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonItem_cerrarpantalla As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ComboItem3 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem1 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem2 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem4 As DevComponents.Editors.ComboItem
    Private WithEvents LabelItem_pantalla As DevComponents.DotNetBar.LabelItem
    Friend WithEvents BalloonTip As DevComponents.DotNetBar.BalloonTip
    Private WithEvents ComboB_mes_TabBusquedas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Private WithEvents ComboB_ano_TabBusquedas As DevComponents.DotNetBar.Controls.ComboBoxEx
    Private WithEvents LabelX27 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX26 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX25 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX24 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX23 As DevComponents.DotNetBar.LabelX
    Private WithEvents LabelX22 As DevComponents.DotNetBar.LabelX
    Private WithEvents DateF_FechaIni_compro As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Private WithEvents TextF_Documento As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents TextF_numecompro As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Private WithEvents DateF_FechaFin_compro As DevComponents.Editors.DateTimeAdv.DateTimeInput
    Friend WithEvents CI_totalCompro As DevComponents.Editors.ComboItem
    Private WithEvents SuperTabItem_Buscar As DevComponents.DotNetBar.SuperTabItem
    Private WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Private WithEvents ExpandablePanel3 As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents ItemPanel4 As DevComponents.DotNetBar.ItemPanel
    Friend WithEvents GridP_Asientos As DevComponents.DotNetBar.Controls.DataGridViewX
    Private WithEvents ExpandablePanel2 As DevComponents.DotNetBar.ExpandablePanel
    Private WithEvents ItemPanel3 As DevComponents.DotNetBar.ItemPanel
    Private WithEvents Label_desccuenta_buscar As DevComponents.DotNetBar.LabelX
    Private WithEvents TextB_cdgocuenta_buscar As DevComponents.DotNetBar.Controls.TextBoxX
    Private WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Private WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Private WithEvents ToolbarB_BuscarAsiento As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonI_limpiar_busqueda As DevComponents.DotNetBar.ButtonItem
    Private WithEvents ButtonI_GenerarReporte As DevComponents.DotNetBar.ButtonItem
    Private WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Private WithEvents ComboBoxItem_SelectReport As DevComponents.DotNetBar.ComboBoxItem
    Private WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents DateF_FechaDoc_Nuevo As DevComponents.Editors.DateTimeAdv.DateTimeInput
End Class
