Imports System.Data.SqlClient
Imports System.Text

Public Class frmEmpleados
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cntmnuEmpleadosRg As System.Windows.Forms.ContextMenu
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox8 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents DateTimePicker3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton4 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TextBox11 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox12 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents TextBox13 As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmEmpleados))
        Me.cntmnuEmpleadosRg = New System.Windows.Forms.ContextMenu
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TextBox13 = New System.Windows.Forms.TextBox
        Me.ComboBox3 = New System.Windows.Forms.ComboBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.Label22 = New System.Windows.Forms.Label
        Me.TextBox8 = New System.Windows.Forms.TextBox
        Me.TextBox7 = New System.Windows.Forms.TextBox
        Me.TextBox6 = New System.Windows.Forms.TextBox
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.TextBox12 = New System.Windows.Forms.TextBox
        Me.ComboBox8 = New System.Windows.Forms.ComboBox
        Me.ComboBox7 = New System.Windows.Forms.ComboBox
        Me.ComboBox6 = New System.Windows.Forms.ComboBox
        Me.ComboBox5 = New System.Windows.Forms.ComboBox
        Me.DateTimePicker4 = New System.Windows.Forms.DateTimePicker
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.TextBox11 = New System.Windows.Forms.TextBox
        Me.TextBox10 = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.TextBox9 = New System.Windows.Forms.TextBox
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton4 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(42, 42)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Timer1
        '
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem8, Me.MenuItem10})
        Me.MenuItem4.Text = "Registros"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 0
        Me.MenuItem5.Text = "Primero"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Anterior"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "Siguiente"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "Ultimo"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 4
        Me.MenuItem10.Text = "Igual"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem13, Me.MenuItem14, Me.MenuItem9})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 0
        Me.MenuItem13.Text = "General"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 1
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = "Reporte"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 5
        Me.MenuItem12.Text = "Salir"
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 351)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(630, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 18
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 530
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(0, 64)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(632, 280)
        Me.TabControl1.TabIndex = 19
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TextBox13)
        Me.TabPage1.Controls.Add(Me.ComboBox3)
        Me.TabPage1.Controls.Add(Me.Label19)
        Me.TabPage1.Controls.Add(Me.NumericUpDown1)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.TextBox8)
        Me.TabPage1.Controls.Add(Me.TextBox7)
        Me.TabPage1.Controls.Add(Me.TextBox6)
        Me.TabPage1.Controls.Add(Me.TextBox5)
        Me.TabPage1.Controls.Add(Me.TextBox4)
        Me.TabPage1.Controls.Add(Me.TextBox3)
        Me.TabPage1.Controls.Add(Me.TextBox2)
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Controls.Add(Me.DateTimePicker1)
        Me.TabPage1.Controls.Add(Me.ComboBox2)
        Me.TabPage1.Controls.Add(Me.ComboBox1)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(624, 254)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos Generales"
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(136, 16)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(8, 20)
        Me.TextBox13.TabIndex = 16
        Me.TextBox13.Text = "0"
        Me.TextBox13.Visible = False
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.Items.AddRange(New Object() {"activo", "inactivo", "eliminado"})
        Me.ComboBox3.Location = New System.Drawing.Point(288, 208)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(96, 21)
        Me.ComboBox3.TabIndex = 13
        Me.ComboBox3.Tag = "Estado del registro del empleado"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(240, 208)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(41, 16)
        Me.Label19.TabIndex = 15
        Me.Label19.Text = "Estado"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(576, 176)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(40, 20)
        Me.NumericUpDown1.TabIndex = 10
        Me.NumericUpDown1.Tag = "N�mero de dependientes del empleado"
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(496, 176)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(77, 16)
        Me.Label22.TabIndex = 13
        Me.Label22.Text = "Dependientes"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(64, 176)
        Me.TextBox8.MaxLength = 20
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(80, 20)
        Me.TextBox8.TabIndex = 7
        Me.TextBox8.Tag = "Tel�fono del domicilio del empleado"
        Me.TextBox8.Text = ""
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(64, 144)
        Me.TextBox7.MaxLength = 80
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(552, 20)
        Me.TextBox7.TabIndex = 6
        Me.TextBox7.Tag = "Direcci�n domiciliar del empleado"
        Me.TextBox7.Text = ""
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(96, 112)
        Me.TextBox6.MaxLength = 65
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(520, 20)
        Me.TextBox6.TabIndex = 5
        Me.TextBox6.TabStop = False
        Me.TextBox6.Tag = "Nombre completo del empleado"
        Me.TextBox6.Text = ""
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(416, 80)
        Me.TextBox5.MaxLength = 30
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(200, 20)
        Me.TextBox5.TabIndex = 4
        Me.TextBox5.Tag = "Apellido de casada"
        Me.TextBox5.Text = ""
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(104, 80)
        Me.TextBox4.MaxLength = 30
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(200, 20)
        Me.TextBox4.TabIndex = 3
        Me.TextBox4.Tag = "Apellido materno del empleado"
        Me.TextBox4.Text = ""
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(416, 48)
        Me.TextBox3.MaxLength = 30
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(200, 20)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Tag = "Apellido paterno del empleado"
        Me.TextBox3.Text = ""
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(64, 48)
        Me.TextBox2.MaxLength = 30
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(240, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Primer y segundo nombre"
        Me.TextBox2.Text = ""
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(56, 16)
        Me.TextBox1.MaxLength = 12
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(72, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "C�digo asignado al empleado"
        Me.TextBox1.Text = ""
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(112, 208)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(104, 20)
        Me.DateTimePicker1.TabIndex = 11
        Me.DateTimePicker1.Tag = "Fecha de nacimiento del empleado"
        '
        'ComboBox2
        '
        Me.ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox2.Location = New System.Drawing.Point(384, 176)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(96, 21)
        Me.ComboBox2.TabIndex = 9
        Me.ComboBox2.Tag = "Estado civil del empleado"
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Location = New System.Drawing.Point(200, 176)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(88, 21)
        Me.ComboBox1.TabIndex = 8
        Me.ComboBox1.Tag = "Genero del empleado"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(8, 208)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Fecha Nacimiento"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(312, 176)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 16)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Estado Civil"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(168, 176)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(31, 16)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Sexo"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(8, 176)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 16)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Tel�fono"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 144)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Direcci�n"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Nombre Usual"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(320, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(90, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Apellido Casada"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Apellido Materno"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(320, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Apellido Paterno"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "C�digo"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TextBox12)
        Me.TabPage2.Controls.Add(Me.ComboBox8)
        Me.TabPage2.Controls.Add(Me.ComboBox7)
        Me.TabPage2.Controls.Add(Me.ComboBox6)
        Me.TabPage2.Controls.Add(Me.ComboBox5)
        Me.TabPage2.Controls.Add(Me.DateTimePicker4)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.DateTimePicker3)
        Me.TabPage2.Controls.Add(Me.DateTimePicker2)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.TextBox11)
        Me.TabPage2.Controls.Add(Me.TextBox10)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.TextBox9)
        Me.TabPage2.Controls.Add(Me.ComboBox4)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(624, 254)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Datos de la Identificaci�n y Laborales"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(96, 144)
        Me.TextBox12.MaxLength = 60
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(456, 20)
        Me.TextBox12.TabIndex = 22
        Me.TextBox12.Tag = "Jefe inmediato del empleado"
        Me.TextBox12.Text = ""
        '
        'ComboBox8
        '
        Me.ComboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox8.Location = New System.Drawing.Point(264, 176)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(20, 21)
        Me.ComboBox8.TabIndex = 32
        Me.ComboBox8.TabStop = False
        Me.ComboBox8.Visible = False
        '
        'ComboBox7
        '
        Me.ComboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox7.Location = New System.Drawing.Point(56, 176)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(208, 21)
        Me.ComboBox7.TabIndex = 23
        Me.ComboBox7.Tag = "Puesto que ocupa dentro de la empresa el empleado"
        '
        'ComboBox6
        '
        Me.ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox6.Location = New System.Drawing.Point(480, 112)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(20, 21)
        Me.ComboBox6.TabIndex = 30
        Me.ComboBox6.TabStop = False
        Me.ComboBox6.Visible = False
        '
        'ComboBox5
        '
        Me.ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox5.Location = New System.Drawing.Point(272, 112)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(208, 21)
        Me.ComboBox5.TabIndex = 21
        Me.ComboBox5.Tag = "Area donde labora el empleado"
        '
        'DateTimePicker4
        '
        Me.DateTimePicker4.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker4.Location = New System.Drawing.Point(104, 112)
        Me.DateTimePicker4.Name = "DateTimePicker4"
        Me.DateTimePicker4.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker4.TabIndex = 20
        Me.DateTimePicker4.Tag = "Fecha que fue contrato el empleado"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(8, 176)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(41, 16)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Puesto"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(8, 144)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(81, 16)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "Jefe Inmediato"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(232, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 16)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "Area"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 112)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(85, 16)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "Fecha Contrato"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(368, 16)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(58, 16)
        Me.Label23.TabIndex = 18
        Me.Label23.Text = "# Registro"
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker3.Location = New System.Drawing.Point(344, 48)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker3.TabIndex = 18
        Me.DateTimePicker3.Tag = "Fecha de vencimiento de la identificaci�n"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.CustomFormat = "dd-MMM-yyyy"
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker2.Location = New System.Drawing.Point(96, 48)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(112, 20)
        Me.DateTimePicker2.TabIndex = 17
        Me.DateTimePicker2.Tag = "Fecha de emisi�n de la identificaci�n"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(232, 48)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(105, 16)
        Me.Label21.TabIndex = 15
        Me.Label21.Text = "Fecha Vencimiento"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 48)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(82, 16)
        Me.Label20.TabIndex = 14
        Me.Label20.Text = "Fecha Emisi�n"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(104, 80)
        Me.TextBox11.MaxLength = 20
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(96, 20)
        Me.TextBox11.TabIndex = 19
        Me.TextBox11.Tag = "N�mero de seguro social del empleado"
        Me.TextBox11.Text = ""
        '
        'TextBox10
        '
        Me.TextBox10.Location = New System.Drawing.Point(432, 16)
        Me.TextBox10.MaxLength = 20
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(120, 20)
        Me.TextBox10.TabIndex = 16
        Me.TextBox10.Tag = "N�mero de registro"
        Me.TextBox10.Text = ""
        Me.TextBox10.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 80)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 16)
        Me.Label13.TabIndex = 4
        Me.Label13.Text = "# Seguro Social"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New System.Drawing.Point(232, 16)
        Me.TextBox9.MaxLength = 20
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(120, 20)
        Me.TextBox9.TabIndex = 15
        Me.TextBox9.Tag = "N�mero de la identificaci�n"
        Me.TextBox9.Text = ""
        '
        'ComboBox4
        '
        Me.ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox4.Location = New System.Drawing.Point(88, 16)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(136, 21)
        Me.ComboBox4.TabIndex = 14
        Me.ComboBox4.Tag = "Tipo de identificaci�n del empleado"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(74, 16)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Identificaci�n"
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton4, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(25, 24)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(630, 54)
        Me.ToolBar1.TabIndex = 20
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton4
        '
        Me.ToolBarButton4.DropDownMenu = Me.cntmnuEmpleadosRg
        Me.ToolBarButton4.ImageIndex = 3
        Me.ToolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton
        Me.ToolBarButton4.ToolTipText = "Registros"
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'frmEmpleados
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 13)
        Me.ClientSize = New System.Drawing.Size(630, 375)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.StatusBar1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmEmpleados"
        Me.Text = "frmEmpleados"
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim strCambios(20) As String
    Private Sub frmEmpleados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Iniciar()
        Limpiar()
        CreateMyContextMenu()
        TabControl1.SelectedIndex = 2
        TextBox1.Focus()
        intModulo = 27

    End Sub

    Private Sub frmEmpleados_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        ElseIf e.KeyCode = Keys.F3 Then
            Guardar()
        ElseIf e.KeyCode = Keys.F4 Then
            Limpiar()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem4.Click, MenuItem5.Click, MenuItem6.Click, MenuItem7.Click, MenuItem8.Click, MenuItem9.Click, MenuItem10.Click, MenuItem11.Click, MenuItem12.Click, MenuItem13.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Primero", "Anterior", "Siguiente", "Ultimo", "Igual" : MoverRegistro(sender.text)
            Case "General"
                'Dim frmNew As New frmGeneral
                'lngRegistro = TextBox13.Text
                'Timer1.Interval = 200
                'Timer1.Enabled = True
                'frmNew.ShowDialog()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = TextBox13.Text
                frmNew.ShowDialog()
            Case "Reporte"
                Dim frmNew As New actrptViewer
                intRptFactura = 30
                strQuery = "exec sp_Reportes " & intModulo & " "
                frmNew.MdiParent = Sistemas_Gerenciales.mdifrmSistemasGerenciales.ActiveForm
                frmNew.Show()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
            Case 3
                MoverRegistro(sender.text)
            Case 4
                Me.Close()
        End Select

    End Sub

    Sub Limpiar()

        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        TextBox13.Text = "0"
        TabControl1.Focus()
        TabControl1.SelectedIndex = 0
        TabPage1.Focus()
        ComboBox1.SelectedIndex = 0
        ComboBox2.SelectedIndex = 0
        ComboBox3.SelectedIndex = 0
        TextBox1.Focus()

    End Sub

    Sub Iniciar()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select Descripcion From cat_Generos Order By Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox1.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Descripcion From cat_EstadoCivil Order By Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox2.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Descripcion From cat_Identificacion Order By Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox4.Items.Add(dtrAgro2K.GetValue(0))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Areas Order By Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox6.Items.Add(dtrAgro2K.GetValue(0))
            ComboBox5.Items.Add(dtrAgro2K.GetValue(1))
        End While
        dtrAgro2K.Close()
        strQuery = ""
        strQuery = "Select Registro, Descripcion From prm_Puestos Order By Registro"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            ComboBox8.Items.Add(dtrAgro2K.GetValue(0))
            ComboBox7.Items.Add(dtrAgro2K.GetValue(1))
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged, ComboBox2.SelectedIndexChanged, ComboBox4.SelectedIndexChanged

        Select Case sender.name.ToString
            Case "ComboBox3"
                If ComboBox3.SelectedItem <> "cedula residencia" And ComboBox3.SelectedItem <> "pasaporte" Then
                    Label23.Visible = False
                    TextBox11.Visible = False
                ElseIf ComboBox3.SelectedItem = "cedula residencia" Or ComboBox3.SelectedItem = "pasaporte" Then
                    Label23.Visible = True
                    TextBox11.Visible = True
                End If
        End Select

    End Sub

    Sub CreateMyContextMenu()

        Dim cntmenuItem1 As New MenuItem
        Dim cntmenuItem2 As New MenuItem
        Dim cntmenuItem3 As New MenuItem
        Dim cntmenuItem4 As New MenuItem
        Dim cntmenuItem5 As New MenuItem

        cntmenuItem1.Text = "Primero"
        cntmenuItem2.Text = "Anterior"
        cntmenuItem3.Text = "Siguiente"
        cntmenuItem4.Text = "Ultimo"
        cntmenuItem5.Text = "Igual"

        cntmnuEmpleadosRg.MenuItems.Add(cntmenuItem1)
        cntmnuEmpleadosRg.MenuItems.Add(cntmenuItem2)
        cntmnuEmpleadosRg.MenuItems.Add(cntmenuItem3)
        cntmnuEmpleadosRg.MenuItems.Add(cntmenuItem4)
        cntmnuEmpleadosRg.MenuItems.Add(cntmenuItem5)

        AddHandler cntmenuItem1.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem2.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem3.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem4.Click, AddressOf cntmenuItem_Click
        AddHandler cntmenuItem5.Click, AddressOf cntmenuItem_Click

    End Sub

    Private Sub cntmenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)

        MoverRegistro(sender.text)

    End Sub

    Sub Guardar()
        If TextBox1.Text = "" Then
            MsgBox("Tiene que ingresar el c�digo del empleado.", MsgBoxStyle.Critical, "Error de ingreso de datos")
            TextBox1.Focus()
            Exit Sub
        End If
        If TextBox2.Text = "" Then
            MsgBox("Tiene que ingresar el primer nombre del empleado.", MsgBoxStyle.Critical, "Error de ingreso de datos")
            TextBox2.Focus()
            Exit Sub
        End If
        If TextBox3.Text = "" Then
            MsgBox("Tiene que ingresar el primer apellido del empleado.", MsgBoxStyle.Critical, "Error de ingreso de datos")
            TextBox3.Focus()
            Exit Sub
        End If
        If TextBox7.Text = "" Then
            MsgBox("Tiene que ingresar la direcci�n del empleado.", MsgBoxStyle.Critical, "Error de ingreso de datos")
            TextBox7.Focus()
            Exit Sub
        End If
        Me.Cursor = Cursors.WaitCursor
        ComboBox6.SelectedIndex = ComboBox5.SelectedIndex
        ComboBox8.SelectedIndex = ComboBox7.SelectedIndex
        Dim cmdTmp As New SqlCommand("sp_IngEmpleados", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter
        Dim prmTmp04 As New SqlParameter
        Dim prmTmp05 As New SqlParameter
        Dim prmTmp06 As New SqlParameter
        Dim prmTmp07 As New SqlParameter
        Dim prmTmp08 As New SqlParameter
        Dim prmTmp09 As New SqlParameter
        Dim prmTmp10 As New SqlParameter
        Dim prmTmp11 As New SqlParameter
        Dim prmTmp12 As New SqlParameter
        Dim prmTmp13 As New SqlParameter
        Dim prmTmp14 As New SqlParameter
        Dim prmTmp15 As New SqlParameter
        Dim prmTmp16 As New SqlParameter
        Dim prmTmp17 As New SqlParameter
        Dim prmTmp18 As New SqlParameter
        Dim prmTmp19 As New SqlParameter
        Dim prmTmp20 As New SqlParameter
        Dim prmTmp21 As New SqlParameter
        Dim prmTmp22 As New SqlParameter
        Dim prmTmp23 As New SqlParameter
        Dim prmTmp24 As New SqlParameter
        Dim prmTmp25 As New SqlParameter
        Dim prmTmp26 As New SqlParameter
        Dim prmTmp27 As New SqlParameter
        Dim prmTmp28 As New SqlParameter

        lngRegistro = 0
        With prmTmp01
            .ParameterName = "@lngRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With prmTmp02
            .ParameterName = "@strCodigo"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp03
            .ParameterName = "@strNombres"
            .SqlDbType = SqlDbType.VarChar
            .Value = StrConv(TextBox2.Text, VbStrConv.ProperCase)
        End With
        With prmTmp04
            .ParameterName = "@strPriApellido"
            .SqlDbType = SqlDbType.VarChar
            .Value = StrConv(TextBox3.Text, VbStrConv.ProperCase)
        End With
        With prmTmp05
            .ParameterName = "@strSegApellido"
            .SqlDbType = SqlDbType.VarChar
            .Value = StrConv(TextBox4.Text, VbStrConv.ProperCase)
        End With
        With prmTmp06
            .ParameterName = "@strSolApellido"
            .SqlDbType = SqlDbType.VarChar
            .Value = StrConv(TextBox5.Text, VbStrConv.ProperCase)
        End With
        With prmTmp07
            .ParameterName = "@strDireccion"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox7.Text
        End With
        With prmTmp08
            .ParameterName = "@strTelefono"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox8.Text
        End With
        With prmTmp09
            .ParameterName = "@intGenero"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox1.SelectedIndex
        End With
        With prmTmp10
            .ParameterName = "@intCivil"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox2.SelectedIndex
        End With
        With prmTmp11
            .ParameterName = "@intDepend"
            .SqlDbType = SqlDbType.TinyInt
            .Value = NumericUpDown1.Value
        End With
        With prmTmp12
            .ParameterName = "@strFechaNac"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker1.Value, "dd-MMM-yyyy")
        End With
        With prmTmp13
            .ParameterName = "@intNumFechaNac"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker1.Value, "yyyyMMdd")
        End With
        With prmTmp14
            .ParameterName = "@dteFechaNac"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker1.Value
        End With
        With prmTmp15
            .ParameterName = "@strFechaIng"
            .SqlDbType = SqlDbType.VarChar
            .Value = Format(DateTimePicker4.Value, "dd-MMM-yyyy")
        End With
        With prmTmp16
            .ParameterName = "@intNumFechaIng"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker4.Value, "yyyyMMdd")
        End With
        With prmTmp17
            .ParameterName = "@dteFechaIng"
            .SqlDbType = SqlDbType.SmallDateTime
            .Value = DateTimePicker4.Value
        End With
        With prmTmp18
            .ParameterName = "@intIdentificacion"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox4.SelectedIndex
        End With
        With prmTmp19
            .ParameterName = "@strNumIdentif"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox9.Text
        End With
        With prmTmp20
            .ParameterName = "@strNumRegistro"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox10.Text
        End With
        With prmTmp21
            .ParameterName = "@intNumFecEmi"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker2.Value, "yyyyMMdd")
        End With
        With prmTmp22
            .ParameterName = "@intNumFecVen"
            .SqlDbType = SqlDbType.Int
            .Value = Format(DateTimePicker3.Value, "yyyyMMdd")
        End With
        With prmTmp23
            .ParameterName = "@strNumInss"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox11.Text
        End With
        With prmTmp24
            .ParameterName = "@intAreaRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox6.SelectedItem
        End With
        With prmTmp25
            .ParameterName = "@strJefe"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox12.Text
        End With
        With prmTmp26
            .ParameterName = "@intPuestoRegistro"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox8.SelectedItem
        End With
        With prmTmp27
            .ParameterName = "@intEstado"
            .SqlDbType = SqlDbType.TinyInt
            .Value = ComboBox3.SelectedIndex
        End With
        With prmTmp28
            .ParameterName = "@intVerificar"
            .SqlDbType = SqlDbType.TinyInt
            .Value = 0
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .Parameters.Add(prmTmp04)
            .Parameters.Add(prmTmp05)
            .Parameters.Add(prmTmp06)
            .Parameters.Add(prmTmp07)
            .Parameters.Add(prmTmp08)
            .Parameters.Add(prmTmp09)
            .Parameters.Add(prmTmp10)
            .Parameters.Add(prmTmp11)
            .Parameters.Add(prmTmp12)
            .Parameters.Add(prmTmp13)
            .Parameters.Add(prmTmp14)
            .Parameters.Add(prmTmp15)
            .Parameters.Add(prmTmp16)
            .Parameters.Add(prmTmp17)
            .Parameters.Add(prmTmp18)
            .Parameters.Add(prmTmp19)
            .Parameters.Add(prmTmp20)
            .Parameters.Add(prmTmp21)
            .Parameters.Add(prmTmp22)
            .Parameters.Add(prmTmp23)
            .Parameters.Add(prmTmp24)
            .Parameters.Add(prmTmp25)
            .Parameters.Add(prmTmp26)
            .Parameters.Add(prmTmp27)
            .Parameters.Add(prmTmp28)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        intResp = 0
        dtrAgro2K = cmdTmp.ExecuteReader
        While dtrAgro2K.Read
            intResp = MsgBox("Ya Existe Este Registro, �Desea Actualizarlo?", MsgBoxStyle.Information + MsgBoxStyle.YesNo)
            lngRegistro = TextBox13.Text
            cmdTmp.Parameters(0).Value = lngRegistro
            Exit While
        End While
        dtrAgro2K.Close()
        If intResp = 7 Then
            dtrAgro2K.Close()
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        cmdTmp.Parameters(27).Value = 1

        Try
            cmdTmp.ExecuteNonQuery()
            If intResp = 6 Then
                'If TextBox2.Text <> strCambios(0) Then
                '    Ing_Bitacora(intModulo, TextBox1.Text, "Descripci�n", strCambios(0), TextBox2.Text)
                'End If
                'If ComboBox1.SelectedItem <> strCambios(1) Then
                '    Ing_Bitacora(intModulo, TextBox1.Text, "Estado", strCambios(1), ComboBox3.SelectedItem)
                'End If
            ElseIf intResp = 0 Then
                Ing_Bitacora(intModulo, lngRegistro, "C�digo", "", TextBox1.Text)
            End If
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicaci�n Satisfactoria")
            MoverRegistro("Primero")
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicaci�n")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub MoverRegistro(ByVal StrDato As String)
        Select Case StrDato
            Case "Primero"
                strQuery = "Select Min(Codigo) from prm_Empleados"
            Case "Anterior"
                strQuery = "Select Max(Codigo) from prm_Empleados Where codigo < '" & TextBox1.Text & "'"
            Case "Siguiente"
                strQuery = "Select Min(Codigo) from prm_Empleados Where codigo > '" & TextBox1.Text & "'"
            Case "Ultimo"
                strQuery = "Select Max(Codigo) from prm_Empleados"
            Case "Igual"
                strQuery = "Select Codigo from prm_Empleados Where codigo = '" & TextBox1.Text & "'"
                strCodigoTmp = ""
                strCodigoTmp = TextBox1.Text
        End Select
        Limpiar()
        UbicarRegistro(StrDato)

    End Sub

    Sub UbicarRegistro(ByVal StrDato As String)

        Dim lngFecha As Long
        Dim strCodigo As String
        Dim lngArea, lngPuesto As Long

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        strCodigo = ""
        While dtrAgro2K.Read
            If IsDBNull(dtrAgro2K.GetValue(0)) = False Then
                strCodigo = dtrAgro2K.GetValue(0)
            End If
        End While
        dtrAgro2K.Close()
        If strCodigo = "" Then
            If StrDato = "Igual" Then
                TextBox1.Text = strCodigoTmp
                MsgBox("No existe un registro con ese c�digo en la tabla", MsgBoxStyle.Information, "Extracci�n de Registro")
            Else
                MsgBox("Este es el primer/ultimo registro de la tabla", MsgBoxStyle.Exclamation, "Extracci�n de Registro")
            End If
            cmdAgro2K.Connection.Close()
            cnnAgro2K.Close()
            Exit Sub
        End If
        strQuery = ""
        strQuery = "Select * From prm_Empleados Where Codigo = '" & strCodigo & "'"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        lngFecha = 0
        lngArea = 0
        lngPuesto = 0
        While dtrAgro2K.Read
            TextBox13.Text = dtrAgro2K.GetValue(0)
            TextBox1.Text = dtrAgro2K.GetValue(1)
            TextBox2.Text = dtrAgro2K.GetValue(2)
            TextBox3.Text = dtrAgro2K.GetValue(3)
            TextBox4.Text = dtrAgro2K.GetValue(4)
            TextBox5.Text = dtrAgro2K.GetValue(5)
            TextBox7.Text = dtrAgro2K.GetValue(6)
            TextBox8.Text = dtrAgro2K.GetValue(7)
            ComboBox1.SelectedIndex = dtrAgro2K.GetValue(8)
            ComboBox2.SelectedIndex = dtrAgro2K.GetValue(9)
            NumericUpDown1.Text = dtrAgro2K.GetValue(10)
            DateTimePicker1.Value = dtrAgro2K.GetValue(13)
            DateTimePicker4.Value = dtrAgro2K.GetValue(16)
            ComboBox4.SelectedIndex = dtrAgro2K.GetValue(17)
            TextBox9.Text = dtrAgro2K.GetValue(18)
            TextBox10.Text = dtrAgro2K.GetValue(19)
            lngFecha = dtrAgro2K.GetValue(20)
            If lngFecha <> 0 Then
                DateTimePicker2.Value = DefinirFecha(lngFecha)
            End If
            lngFecha = dtrAgro2K.GetValue(21)
            If lngFecha <> 0 Then
                DateTimePicker3.Value = DefinirFecha(lngFecha)
            End If
            TextBox11.Text = dtrAgro2K.GetValue(22)
            lngArea = dtrAgro2K.GetValue(23)
            TextBox12.Text = dtrAgro2K.GetValue(24)
            lngPuesto = dtrAgro2K.GetValue(25)
            ComboBox3.SelectedIndex = dtrAgro2K.GetValue(26)
        End While
        TextBox6.Text = Trim(TextBox2.Text) & " " & Trim(TextBox3.Text) & " " & Trim(TextBox4.Text) & " " & Trim(TextBox5.Text)
        ComboBox6.SelectedIndex = -1
        For intIncr = 0 To ComboBox6.Items.Count - 1
            ComboBox6.SelectedIndex = intIncr
            If ComboBox6.SelectedItem = lngArea Then
                ComboBox5.SelectedIndex = ComboBox6.SelectedIndex
                Exit For
            End If
        Next intIncr
        ComboBox8.SelectedIndex = -1
        For intIncr = 0 To ComboBox8.Items.Count - 1
            ComboBox8.SelectedIndex = intIncr
            If ComboBox8.SelectedItem = lngPuesto Then
                ComboBox7.SelectedIndex = ComboBox8.SelectedIndex
                Exit For
            End If
        Next intIncr
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, TextBox3.KeyDown, TextBox4.KeyDown, TextBox5.KeyDown, TextBox6.KeyDown, TextBox7.KeyDown, TextBox8.KeyDown, TextBox9.KeyDown, TextBox10.KeyDown, TextBox11.KeyDown, TextBox12.KeyDown, ComboBox1.KeyDown, ComboBox2.KeyDown, ComboBox3.KeyDown, ComboBox4.KeyDown, ComboBox5.KeyDown, ComboBox6.KeyDown, ComboBox7.KeyDown, DateTimePicker1.KeyDown, DateTimePicker2.KeyDown, DateTimePicker3.KeyDown, DateTimePicker4.KeyDown, NumericUpDown1.KeyDown

        If e.KeyCode = Keys.Enter Then
            Select Case sender.name.ToString
                Case "TextBox1" : MoverRegistro("Igual") : TextBox2.Focus()
                Case "TextBox2" : TextBox3.Focus() : TextBox2.Text = StrConv(TextBox2.Text, VbStrConv.ProperCase)
                Case "TextBox3" : TextBox4.Focus() : TextBox3.Text = StrConv(TextBox3.Text, VbStrConv.ProperCase)
                Case "TextBox4" : TextBox5.Focus() : TextBox4.Text = StrConv(TextBox4.Text, VbStrConv.ProperCase)
                Case "TextBox5" : TextBox7.Focus() : TextBox5.Text = StrConv(TextBox5.Text, VbStrConv.ProperCase)
                Case "TextBox6" : TextBox7.Focus()
                Case "TextBox7" : TextBox8.Focus()
                Case "TextBox8" : ComboBox1.Focus()
                Case "ComboBox1" : ComboBox2.Focus()
                Case "ComboBox2" : NumericUpDown1.Focus()
                Case "NumericUpDown1" : DateTimePicker1.Focus()
                Case "DateTimePicker1" : ComboBox3.Focus()
                Case "ComboBox3" : TabControl1.SelectedIndex = 1 : ComboBox4.Focus()
                Case "ComboBox4" : TextBox9.Focus()
                Case "TextBox9" : IIf(TextBox10.Visible = True, TextBox10.Focus, DateTimePicker2.Focus())
                Case "TextBox10" : DateTimePicker2.Focus()
                Case "DateTimePicker2" : DateTimePicker3.Focus()
                Case "DateTimePicker3" : TextBox11.Focus()
                Case "TextBox11" : DateTimePicker4.Focus()
                Case "DateTimePicker4" : ComboBox5.Focus()
                Case "ComboBox5" : TextBox12.Focus()
                Case "TextBox12" : ComboBox7.Focus()
                Case "ComboBox7" : TabControl1.SelectedIndex = 0 : TextBox1.Focus()
            End Select
        End If

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus, TextBox3.GotFocus, TextBox4.GotFocus, TextBox5.GotFocus, TextBox6.GotFocus, TextBox7.GotFocus, TextBox8.GotFocus, TextBox9.GotFocus, TextBox10.GotFocus, TextBox11.GotFocus, TextBox12.GotFocus, TextBox13.GotFocus, ComboBox1.GotFocus, ComboBox2.GotFocus, ComboBox3.GotFocus, ComboBox4.GotFocus, ComboBox5.GotFocus, ComboBox6.GotFocus, ComboBox7.GotFocus, DateTimePicker1.GotFocus, DateTimePicker2.GotFocus, DateTimePicker3.GotFocus, DateTimePicker4.GotFocus, NumericUpDown1.GotFocus

        Dim strCampo As String = String.Empty

        Select Case sender.name.ToString
            Case "TextBox1" : strCampo = "C�digo"
            Case "TextBox2" : strCampo = "Nombres"
            Case "TextBox3" : strCampo = "Apellido Pat."
            Case "TextBox4" : strCampo = "Apellido Mat."
            Case "TextBox5" : strCampo = "Apellido Casada"
            Case "TextBox6" : strCampo = "Nombre Usual"
            Case "TextBox7" : strCampo = "Direcci�n"
            Case "TextBox8" : strCampo = "Tel�fono"
            Case "TextBox9" : strCampo = "Identificaci�n"
            Case "TextBox10" : strCampo = "# Registro"
            Case "TextBox11" : strCampo = "# Seg. Social"
            Case "TextBox12" : strCampo = "Jefe Inmed."
            Case "ComboBox1" : strCampo = "Genero"
            Case "ComboBox2" : strCampo = "Estado Civil"
            Case "ComboBox3" : strCampo = "Estado"
            Case "ComboBox4" : strCampo = "Tipo ID"
            Case "ComboBox5" : strCampo = "Area"
            Case "ComboBox7" : strCampo = "Puesto"
            Case "DateTimePicker1" : strCampo = "Fec. Nacim."
            Case "DateTimePicker2" : strCampo = "Fec. Emisi�n"
            Case "DateTimePicker3" : strCampo = "Fec. Vencim."
            Case "DateTimePicker4" : strCampo = "Fec. Contrto"
            Case "NumericUpDown1" : strCampo = "Dependientes"
        End Select
        If sender.name <> "DateTimePicker1" And sender.name <> "DateTimePicker2" And _
            sender.name <> "DateTimePicker3" And sender.name <> "DateTimePicker4" And _
            sender.name <> "NumericUpDown1" Then
            sender.selectall()
        End If
        StatusBar1.Panels.Item(0).Text = strCampo
        StatusBar1.Panels.Item(1).Text = sender.tag

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If blnUbicar = True Then
            blnUbicar = False
            Timer1.Enabled = False
            If strUbicar <> "" Then
                TextBox1.Text = strUbicar
                MoverRegistro("Igual")
            End If
        End If

    End Sub

End Class
