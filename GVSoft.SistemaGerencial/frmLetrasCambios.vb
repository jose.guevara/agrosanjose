Imports System.Data.SqlClient
Imports System.Text

Public Class frmLetrasCambios
    Inherits DevComponents.DotNetBar.Office2007Form
    'Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
    Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton2 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton3 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ToolBarButton7 As System.Windows.Forms.ToolBarButton
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLetrasCambios))
        Me.ToolBar1 = New System.Windows.Forms.ToolBar
        Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton2 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton3 = New System.Windows.Forms.ToolBarButton
        Me.ToolBarButton7 = New System.Windows.Forms.ToolBarButton
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBar1
        '
        Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarButton1, Me.ToolBarButton2, Me.ToolBarButton3, Me.ToolBarButton7})
        Me.ToolBar1.ButtonSize = New System.Drawing.Size(40, 50)
        Me.ToolBar1.DropDownArrows = True
        Me.ToolBar1.ImageList = Me.ImageList1
        Me.ToolBar1.Location = New System.Drawing.Point(0, 0)
        Me.ToolBar1.Name = "ToolBar1"
        Me.ToolBar1.ShowToolTips = True
        Me.ToolBar1.Size = New System.Drawing.Size(488, 56)
        Me.ToolBar1.TabIndex = 18
        '
        'ToolBarButton1
        '
        Me.ToolBarButton1.ImageIndex = 0
        Me.ToolBarButton1.Text = "<F3>"
        Me.ToolBarButton1.ToolTipText = "Guardar Registro"
        '
        'ToolBarButton2
        '
        Me.ToolBarButton2.ImageIndex = 1
        Me.ToolBarButton2.Text = "<F4>"
        Me.ToolBarButton2.ToolTipText = "Cancelar/Limpiar"
        '
        'ToolBarButton3
        '
        Me.ToolBarButton3.ImageIndex = 2
        Me.ToolBarButton3.Text = "<F4>"
        Me.ToolBarButton3.ToolTipText = "Limpiar"
        Me.ToolBarButton3.Visible = False
        '
        'ToolBarButton7
        '
        Me.ToolBarButton7.ImageIndex = 6
        Me.ToolBarButton7.Text = "<ESC>"
        Me.ToolBarButton7.ToolTipText = "Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(31, 31)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Location = New System.Drawing.Point(0, 177)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(488, 24)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 19
        Me.StatusBar1.Text = "StatusBar1"
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.Text = "StatusBarPanel1"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.Text = "StatusBarPanel2"
        Me.StatusBarPanel2.Width = 385
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.MenuItem3, Me.MenuItem11, Me.MenuItem12})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Guardar"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "Cancelar"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 2
        Me.MenuItem3.Text = "Limpiar"
        Me.MenuItem3.Visible = False
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 3
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem14})
        Me.MenuItem11.Text = "Listados"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 0
        Me.MenuItem14.Text = "Cambios"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 4
        Me.MenuItem12.Text = "Salir"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "A la orden de"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(0, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 112)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de la Letra de Cambio"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DecimalPlaces = 2
        Me.NumericUpDown1.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.NumericUpDown1.Location = New System.Drawing.Point(104, 88)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(64, 20)
        Me.NumericUpDown1.TabIndex = 5
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 88)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Tasa de Interes"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(120, 56)
        Me.TextBox2.MaxLength = 35
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(360, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Tag = "Nombre del librador de la letra de cambio"
        Me.TextBox2.Text = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(108, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Nombre del librador"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(88, 24)
        Me.TextBox1.MaxLength = 40
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(392, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Tag = "A quien se le estara pagando la letra de cambio"
        Me.TextBox1.Text = ""
        '
        'frmLetrasCambios
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(488, 201)
        Me.Controls.Add(Me.ToolBar1)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.MinimizeBox = False
        Me.Name = "frmLetrasCambios"
        Me.Text = "frmLetrasCambios"
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmLetrasCambios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Top = 0
        Me.Left = 0
        Limpiar()
        UbicarRegistro()
        intModulo = 36

    End Sub

    Private Sub frmLetrasCambios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click, MenuItem2.Click, MenuItem3.Click, MenuItem12.Click, MenuItem14.Click

        Select Case sender.text.ToString
            Case "Guardar" : Guardar()
            Case "Cancelar", "Limpiar" : Limpiar()
            Case "Salir" : Me.Close()
            Case "Cambios"
                Dim frmNew As New frmBitacora
                lngRegistro = 1
                frmNew.ShowDialog()
        End Select

    End Sub

    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick

        Select Case ToolBar1.Buttons.IndexOf(e.Button)
            Case 0
                Guardar()
            Case 1, 2
                Limpiar()
                UbicarRegistro()
            Case 3
                Me.Close()
        End Select

    End Sub

    Sub Guardar()
        Me.Cursor = Cursors.WaitCursor
        Dim cmdTmp As New SqlCommand("sp_IngLetrasCambio", cnnAgro2K)
        Dim prmTmp01 As New SqlParameter
        Dim prmTmp02 As New SqlParameter
        Dim prmTmp03 As New SqlParameter

        With prmTmp01
            .ParameterName = "@strOrden"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox1.Text
        End With
        With prmTmp02
            .ParameterName = "@strNombre"
            .SqlDbType = SqlDbType.VarChar
            .Value = TextBox2.Text
        End With
        With prmTmp03
            .ParameterName = "@dblTasa"
            .SqlDbType = SqlDbType.Decimal
            .Value = NumericUpDown1.Value
        End With
        With cmdTmp
            .Parameters.Add(prmTmp01)
            .Parameters.Add(prmTmp02)
            .Parameters.Add(prmTmp03)
            .CommandType = CommandType.StoredProcedure
        End With

        If cmdTmp.Connection.State = ConnectionState.Open Then
            cmdTmp.Connection.Close()
        End If
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        cnnAgro2K.Open()
        cmdTmp.Connection = cnnAgro2K

        Try
            cmdTmp.ExecuteNonQuery()
            MsgBox("Registro Ingresado/Actualizado", MsgBoxStyle.Information, "Aplicación Satisfactoria")
            UbicarRegistro()
        Catch exc As Exception
            MsgBox(exc.Message, MsgBoxStyle.Critical, "Error en la Aplicación")
        Finally
            cmdTmp.Connection.Close()
            cnnAgro2K.Close()
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Sub UbicarRegistro()

        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        'If cmdAgro2K.Connection.State = ConnectionState.Open Then
        '    cmdAgro2K.Connection.Close()
        'End If
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        strQuery = "Select * From prm_LetrasCambio"
        cmdAgro2K.CommandText = strQuery
        dtrAgro2K = cmdAgro2K.ExecuteReader
        While dtrAgro2K.Read
            TextBox1.Text = dtrAgro2K.GetValue(0)
            TextBox2.Text = dtrAgro2K.GetValue(1)
            NumericUpDown1.Value = dtrAgro2K.GetValue(2)
        End While
        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()

    End Sub

    Sub Limpiar()

        TextBox1.Text = ""
        TextBox2.Text = ""
        NumericUpDown1.Value = 0.0

    End Sub

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox1.GotFocus, TextBox2.GotFocus

        StatusBar1.Panels.Item(1).Text = sender.tag
        If sender.name = "TextBox1" Then
            StatusBar1.Panels.Item(0).Text = "A la Orden"
            sender.selectall()
        ElseIf sender.name = "TextBox2" Then
            StatusBar1.Panels.Item(0).Text = "Librador"
            sender.selectall()
        ElseIf sender.name = "NumericUpDown1" Then
            StatusBar1.Panels.Item(0).Text = "Tasa Interes"
        End If

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown, TextBox2.KeyDown, NumericUpDown1.KeyDown

        If e.KeyCode = Keys.Enter Then
            If sender.name = "TextBox1" Then
                TextBox2.Focus()
            ElseIf sender.name = "TextBox2" Then
                NumericUpDown1.Focus()
            ElseIf sender.name = "NumericUpDown1" Then
                TextBox1.Focus()
            End If
        End If

    End Sub

End Class
