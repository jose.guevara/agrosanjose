Imports GVSoft.SistemaGerencial.Utilidades
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.ReglaNegocio


Public Class frmReportesCuadreCaja

    Dim IdSuc As Integer
    Dim sNumeroArqueo As String

    Private Sub frmReportesCuadreCaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UbicarAgencia(lngRegUsuario)
        LlenarComboSucursales()
        RNReportes.CargarComboReportes(cbReportes, nIdUsuario, nIdModulo)
        IdSuc = lngRegAgencia
        ObtieneSeriexAgencia()

    End Sub

    Sub ObtieneSeriexAgencia()
        Dim IndicaObtieneRegistro As Integer = 0
        If cnnAgro2K.State = ConnectionState.Open Then
            cnnAgro2K.Close()
        End If
        If cmdAgro2K.Connection.State = ConnectionState.Open Then
            cmdAgro2K.Connection.Close()
        End If
        ddlSerie.SelectedItem = -1
        cnnAgro2K.Open()
        cmdAgro2K.Connection = cnnAgro2K
        strQuery = ""
        'strQuery = "ObtieneSerieParametros " & lngRegAgencia & ",1"
        If IdSuc = 0 Then
            IdSuc = lngRegAgencia
        End If
        strQuery = "ObtieneSerieParametros " & IdSuc & ",1"

        If strQuery.Trim.Length > 0 Then
            ddlSerie.Items.Clear()
            cmdAgro2K.CommandText = strQuery
            dtrAgro2K = cmdAgro2K.ExecuteReader
            While dtrAgro2K.Read
                ddlSerie.Items.Add(dtrAgro2K.GetValue(0))
                IndicaObtieneRegistro = 1
            End While
        End If
        If IndicaObtieneRegistro = 1 Then
            ddlSerie.SelectedIndex = 0
        End If

        dtrAgro2K.Close()
        cmdAgro2K.Connection.Close()
        cnnAgro2K.Close()
    End Sub

    Private Sub LlenarComboSucursales()
        RNAgencias.CargarComboAgencias(cmbAgencias, lngRegUsuario)
        'cmbAgencias.ValueMember = lngRegAgencia
    End Sub

    Public Sub MostrarReporte()

        'Dim frmNew As New frmEscogerClientes
        Dim frmNew As Form
        Dim strAgencias As String = String.Empty
        Dim strAgencias2 As String = String.Empty


        intRptCuadreCaja = cbReportes.SelectedValue

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        If intRptCuadreCaja = 1 Then
            ''strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            ''strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""
            If cmbAgencias.SelectedValue = 0 Then
                MsgBox("No ha Seleccionado una Agencia para Mostrar sus Respectivos Arqueos de Cajas...!", MsgBoxStyle.Exclamation, "Reportes Arqueos de Cajas")
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            Else
                'strQuery = "spOtieneInformacionCuadreXSucursalYFecha " & cmbAgencias.SelectedValue & ", " & strFechaInicial & ", " & strFechaFinal
                strQuery = "spOtieneInformacionCuadreXNumeroArqueo  " & cmbAgencias.SelectedValue & ", '" & sNumeroArqueo & "'"
                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If

        ElseIf intRptCuadreCaja = 2 Then

            strQuery = ""
            If cmbAgencias.SelectedValue = 0 Then
                MsgBox("No ha Seleccionado una Agencia para Mostrar sus Respectivos Arqueos de Cajas...!", MsgBoxStyle.Exclamation, "Reportes Arqueos de Cajas")
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            Else
                strQuery = "spOtieneInformacionCuadreXSucursal " & cmbAgencias.SelectedValue

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()
            End If

        ElseIf intRptCuadreCaja = 3 Then

            'strQuery = ""
            'strQuery = "spSaldosGeneralesDetallados "

            'frmNew = New actrptViewer
            'frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            'frmNew.Show()

        ElseIf intRptCuadreCaja = 4 Then
            strQuery = ""
            strQuery = "exec spOtieneInformacionCuadreTodos "
            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
        ElseIf intRptCuadreCaja = 5 Then
            strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""
            If cmbAgencias.SelectedValue = 0 Then
                MsgBox("No ha Seleccionado una Agencia para Mostrar sus Respectivos Arqueos de Cajas...!", MsgBoxStyle.Exclamation, "Reportes Arqueos de Cajas")
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Exit Sub
            Else
                strQuery = "spOtieneInformacionCuadreXSucursalYFecha " & cmbAgencias.SelectedValue & ", " & strFechaInicial & ", " & strFechaFinal

                frmNew = New actrptViewer
                frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
                frmNew.Show()

            End If
        ElseIf intRptCuadreCaja = 6 Then
            strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            'strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""

            strQuery = "spObtenerDepositoAImprimir " & lngRegUsuario & ", " & strFechaInicial '& ", " & strFechaFinal

            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()

        ElseIf intRptCuadreCaja = 7 Then
            strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""


            strQuery = "spObtenerSobrantesCaja " & cmbAgencias.SelectedValue & ", " & strFechaInicial & ", " & strFechaFinal

            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
        ElseIf intRptCuadreCaja = 8 Then
            strFechaInicial = Format(DateTimePicker1.Value, "yyyyMMdd")
            strFechaFinal = Format(DateTimePicker2.Value, "yyyyMMdd")
            strQuery = ""

            If ConvierteAInt(cmbAgencias.SelectedValue) = 0 Then
                strQuery = "ObtieneDetalleDeposito " & strFechaInicial & ", " & strFechaFinal & ", null"
            Else
                strQuery = "ObtieneDetalleDeposito " & strFechaInicial & ", " & strFechaFinal & ", " & ConvierteAInt(cmbAgencias.SelectedValue)
            End If


            frmNew = New actrptViewer
            frmNew.MdiParent = Sistemas_Gerenciales.frmPrincipal.ActiveForm
            frmNew.Show()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub cmdExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExportar.Click

    End Sub

    Private Sub cmdReportes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportes.Click
        If cbReportes.SelectedValue = -1 Then
            MsgBox("No ha Seleccionado una opcion de Reporte a Mostrar..!", MsgBoxStyle.Exclamation, "Reportes de Arqueos de Cajas")
            Exit Sub
        Else
            MostrarReporte()
        End If

    End Sub

    Private Sub cmdiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdiSalir.Click
        Me.Close()
    End Sub

    Private Sub txtNumeroCuadreBuscar_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumeroCuadreBuscar.KeyDown

        If e.KeyCode = Keys.Enter Then
            sNumeroArqueo = ddlSerie.Text & Format(ConvierteAInt(txtNumeroCuadreBuscar.Text), "000000000000")
            txtNumeroCuadreBuscar.Text = sNumeroArqueo
            cmdReportes.Focus()
        End If
    End Sub

    Private Sub cmbAgencias_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbAgencias.KeyDown
        If e.KeyCode = Keys.Enter Then
            IdSuc = cmbAgencias.SelectedValue
            ObtieneSeriexAgencia()
            txtNumeroCuadreBuscar.Focus()
        End If
    End Sub

    Private Sub cbReportes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbReportes.KeyDown
        If e.KeyCode = Keys.Enter Then
            cmbAgencias.Focus()
        End If
    End Sub

    Private Sub cmbAgencias_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAgencias.Leave
        IdSuc = cmbAgencias.SelectedValue
        ObtieneSeriexAgencia()
    End Sub
End Class