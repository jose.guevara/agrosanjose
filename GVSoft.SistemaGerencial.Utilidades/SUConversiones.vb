﻿Public Class SUConversiones

    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConvierteADouble(ByVal oValor As Object) As Double
        Dim nValorNumerico As Double = 0
        Dim outresult As Double = 0
        Try
            If oValor IsNot Nothing Then
                nValorNumerico = 0
                If IsNumeric(oValor) Then
                    nValorNumerico = Convert.ToDouble(oValor.ToString().Trim())
                End If
            End If
        Catch ex As Exception
            nValorNumerico = 0
        End Try
        Return nValorNumerico
    End Function
    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConvierteAString(ByVal oValor As Object) As String
        Dim lsValorConvertido As String = String.Empty
        Try
            If oValor IsNot Nothing Then
                lsValorConvertido = Convert.ToString(oValor)
            End If

        Catch ex As Exception
            lsValorConvertido = String.Empty
        End Try
        Return lsValorConvertido
    End Function
    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConvierteALong(ByVal oValor As Object) As Long
        Dim nValorNumerico As Double = 0
        Dim outresult As Double = 0
        Try
            If oValor IsNot Nothing Then
                nValorNumerico = 0
                If IsNumeric(oValor) Then
                    nValorNumerico = CLng(oValor.ToString().Trim())
                End If
            End If
        Catch ex As Exception
            nValorNumerico = 0
        End Try
        Return nValorNumerico
    End Function

    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConvierteADate(ByVal oValor As String) As Date
        Try
            Return Date.Parse(oValor.Trim())
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConvierteAInt(ByVal oValor As Object) As Integer
        Dim nValorNumerico As Integer = 0
        Dim outresult As Integer = 0
        Try
            If oValor IsNot Nothing Then
                nValorNumerico = 0
                If IsNumeric(oValor) Then
                    nValorNumerico = Convert.ToInt64(oValor.ToString().Trim())
                End If
            End If

        Catch ex As Exception
            nValorNumerico = 0
        End Try
        Return nValorNumerico
    End Function
    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConvierteADecimal(ByVal oValor As Object) As Decimal
        Dim nValorNumerico As Decimal = 0
        Dim outresult As Decimal = 0
        Try
            If oValor IsNot Nothing Then
                '  Decimal.TryParse(oValor.ToString().Trim(), outresult)
                nValorNumerico = 0
                If IsNumeric(oValor) Then
                    nValorNumerico = Convert.ToDecimal(oValor.ToString().Trim())
                End If
            End If

        Catch ex As Exception
            nValorNumerico = 0
        End Try
        Return nValorNumerico
    End Function

    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub ObtieneDiaMesAnio(ByVal psFecha As String, ByRef pnDia As Integer, ByRef pnMes As Integer, ByRef pnAnio As Integer)
        Dim nValorNumerico As Integer = 0
        Dim outresult As Integer = 0

        pnAnio = SUConversiones.ConvierteADouble(psFecha.Substring(0, 4))
        pnMes = SUConversiones.ConvierteADouble(psFecha.ToString().Substring(4, 2))
        pnDia = SUConversiones.ConvierteADouble(psFecha.ToString().Substring(6, 2))
    End Sub
    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function FormateaFechaParaBD(ByVal Dia As Integer, ByVal Mes As Integer, ByVal Anio As Integer) As String
        Dim sFecha As String = String.Empty
        sFecha = Format(Anio, "0000") & "-" & Format(Mes, "00") & "-" & Format(Dia, "00") & " 00:00:00.000"
        Return sFecha
    End Function

    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function FormateaFechaNumerica(ByVal Dia As Integer, ByVal Mes As Integer, ByVal Anio As Integer) As String
        Dim sFecha As String = String.Empty
        sFecha = Format(Anio, "0000") & Format(Mes, "00") & Format(Dia, "00")
        Return sFecha
    End Function

End Class
