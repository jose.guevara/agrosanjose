﻿Imports System.Data.SqlClient
Imports System.Data.Common
Imports System.Text
Imports System.IO.Compression
Imports System.IO
Imports System.Configuration

Public Class SUFunciones
    Public Shared Function ObtieneSentenciaSQL(ByVal objCmd As DbCommand) As String

        If objCmd Is Nothing Then
            Return "El objeto Command llega con valor Nulo o Nothing"
        End If
        Dim Sentencia As String = String.Empty
        Dim strparam As New StringBuilder()

        strparam.Append("EXEC " & Convert.ToString(objCmd.CommandText) & " ")
        Sentencia = "EXEC " & Convert.ToString(objCmd.CommandText) & " "
        For Each sqlparam As SqlParameter In objCmd.Parameters
            Select Case sqlparam.SqlDbType
                Case SqlDbType.[Char], SqlDbType.NChar, SqlDbType.NText, SqlDbType.NVarChar, SqlDbType.Text, SqlDbType.VarChar,
                 SqlDbType.[Date], SqlDbType.DateTime, SqlDbType.DateTime2, SqlDbType.DateTimeOffset, SqlDbType.SmallDateTime
                    strparam.Append("'" & sqlparam.Value.ToString() & "',")
                    Sentencia = Sentencia & "'" & sqlparam.Value.ToString() & "',"
                    Exit Select
                Case Else
                    If sqlparam.ParameterName <> "@RETURN_VALUE" Then
                        strparam.Append(sqlparam.Value.ToString() & ",")
                        Sentencia = Sentencia & sqlparam.Value.ToString() & ","
                    End If

                    Exit Select
            End Select
        Next
        Return TruncaCaracteres(strparam.ToString(), 0, (strparam.ToString().Trim().Length - 1))
    End Function

    Public Shared Function ValidaLlaveConfig(ByVal pesLlaveAVerificar As String)
        Dim i As Integer = 0
        Dim strKey(0 To 10 - 1) As String
        Dim lbLlaveCorrecta As Boolean = False
        Try
            For Each key As String In ConfigurationManager.AppSettings
                strKey(i) = key.ToString()
                If strKey(i) = pesLlaveAVerificar Then
                    lbLlaveCorrecta = True
                End If
            Next

        Catch ex As Exception
            lbLlaveCorrecta = False
        End Try
        Return lbLlaveCorrecta
    End Function

    Public Shared Function ObtieneValorDeLlaveConfig(ByVal pesLlaveAVerificar As String) As String
        Dim i As Integer = 0
        Dim strKey(0 To 10 - 1) As String
        Dim lsValorLlave As String = String.Empty
        Try
            For Each key As String In ConfigurationManager.AppSettings
                strKey(i) = key.ToString()
                If strKey(i) = pesLlaveAVerificar Then
                    lsValorLlave = ConfigurationManager.AppSettings(pesLlaveAVerificar)
                End If
            Next

        Catch ex As Exception
            lsValorLlave = String.Empty
        End Try
        Return lsValorLlave
    End Function

    Public Shared Function TruncaCaracteres(ByVal cadena As String, ByVal caracterInicial As Integer, ByVal longitudCaracter As Integer) As String
        'string resultado = cadena.Substring(caracterInicial, longitudCaracter);
        If caracterInicial >= cadena.Trim().Length Then
            Return ""
        Else
            Return (If(cadena.Trim().Length >= (caracterInicial + longitudCaracter), cadena.Substring(caracterInicial, longitudCaracter), cadena.Substring(caracterInicial, (cadena.Trim().Length - caracterInicial))))
        End If
        'return resultado;
    End Function

    Public Shared Function ObtieneDataTableDeDataSet(ByVal pdsDatos As DataSet) As DataTable
        Dim dtDatos As DataTable = Nothing
        Try
            If pdsDatos IsNot Nothing Then
                If pdsDatos.Tables.Count > 0 Then
                    dtDatos = pdsDatos.Tables(0)
                End If
            End If
        Catch ex As Exception
            dtDatos = Nothing
        End Try
        Return dtDatos
    End Function

    Public Shared Function ObtieneValorDeCombo(ByVal Valor As Object) As String
        Dim lsValor As String = String.Empty
        Try
            lsValor = String.Empty
            If Valor IsNot Nothing Then
                lsValor = Valor.ToString
            End If
        Catch ex As Exception
            lsValor = String.Empty
        End Try
        Return lsValor
    End Function
    Public Shared Function ValidaDataTable(ByVal pdtDatos As DataTable) As Boolean
        Dim lbRespuesta As Boolean = False
        Try
            If pdtDatos IsNot Nothing Then
                If pdtDatos.Rows.Count > 0 Then
                    lbRespuesta = True
                End If
            End If
        Catch ex As Exception
            lbRespuesta = False
        End Try
        Return lbRespuesta
    End Function


    Public Shared Function GeneraArchivoZip(ByVal psRutaCompletaArchivoAZipiar As String, ByVal psRutaCompletaArchivoZip As String) As Boolean
        Dim lbRespuesta As Boolean = False
        Dim zip As ZipArchive = Nothing
        Try
            If File.Exists(psRutaCompletaArchivoZip) Then
                File.Delete(psRutaCompletaArchivoZip)
            End If
            If psRutaCompletaArchivoZip.Trim().Length <= 0 Then
                lbRespuesta = False
                Return lbRespuesta
            End If

            If psRutaCompletaArchivoZip.Trim().Length <= 0 Then
                lbRespuesta = False
                Return lbRespuesta

            End If
            zip = ZipFile.Open(psRutaCompletaArchivoZip, ZipArchiveMode.Create)
            If zip Is Nothing Then
                lbRespuesta = False
                Return lbRespuesta
            End If
            zip.CreateEntryFromFile(psRutaCompletaArchivoAZipiar, Path.GetFileName(psRutaCompletaArchivoAZipiar), CompressionLevel.Fastest)
            zip.Dispose()
            If File.Exists(psRutaCompletaArchivoAZipiar) Then
                File.Delete(psRutaCompletaArchivoAZipiar)
            End If
            lbRespuesta = True
        Catch ex As Exception
            lbRespuesta = False
            Throw ex
        End Try

        Return lbRespuesta
    End Function

    Public Shared Function ExtraerArchivoDeArchivoZip(ByVal psRutaCompletaArchivoZip As String) As Boolean
        Dim lbRespuesta As Boolean = False
        Dim zip As ZipArchive = Nothing
        Try
            Using archive As ZipArchive = ZipFile.OpenRead(psRutaCompletaArchivoZip)
                For Each entry As ZipArchiveEntry In archive.Entries
                    If entry.FullName.EndsWith(".txt", StringComparison.OrdinalIgnoreCase) Then
                        If File.Exists(Path.Combine(Path.GetDirectoryName(psRutaCompletaArchivoZip), entry.FullName)) Then
                            File.Delete(Path.Combine(Path.GetDirectoryName(psRutaCompletaArchivoZip), entry.FullName))
                        End If
                        entry.ExtractToFile(Path.Combine(Path.GetDirectoryName(psRutaCompletaArchivoZip), entry.FullName))
                    End If
                Next
            End Using
            lbRespuesta = True
        Catch ex As Exception
            lbRespuesta = False
            Throw ex
        End Try

        Return lbRespuesta
    End Function

    Public Shared Sub CierraConexioDR(ByVal pdrDatos As IDataReader)
        Try
            If pdrDatos IsNot Nothing Then
                If Not pdrDatos.IsClosed Then
                    pdrDatos.Close()
                End If
            End If
        Catch ex As Exception
            pdrDatos = Nothing
        End Try
    End Sub
    Public Shared Sub CierraConexionBD(ByVal pcConexion As SqlClient.SqlConnection)
        Try
            If pcConexion IsNot Nothing Then
                If pcConexion.State = ConnectionState.Open Then
                    pcConexion.Close()
                End If
            End If
        Catch ex As Exception
            pcConexion = Nothing
        End Try
    End Sub
    Public Shared Sub CierraComando(ByVal pcComando As SqlClient.SqlCommand)
        Try
            If pcComando.Connection IsNot Nothing Then
                If pcComando.Connection.State = ConnectionState.Open Then
                    pcComando.Connection.Close()
                End If
            End If
        Catch ex As Exception
            pcComando = Nothing
        End Try
    End Sub
    Public Shared Function ObtieneFechaFormateadaSistema(ByVal psFechaNumero As String) As String
        Dim lsFechaFormateada As String = String.Empty
        Dim lsAnio As String = String.Empty
        Dim lsMes As String = String.Empty
        Dim lsDia As String = String.Empty
        Try
            If psFechaNumero.Trim().Length >= 8 Then
                lsAnio = psFechaNumero.Trim().Substring(1, 4)
                lsMes = psFechaNumero.Trim().Substring(4, 2)
                lsDia = psFechaNumero.Trim().Substring(6, 2)
                lsFechaFormateada = lsDia & "-" & MonthName(SUConversiones.ConvierteAInt(lsMes)).Substring(1, 3) & "-" & lsAnio
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return lsFechaFormateada
    End Function
    Public Shared Function RedondeoNumero(ByVal pnNumeroARedondear As Decimal, ByVal psTipoRedondeo As String) As Decimal
        Dim lnNumeroARedondear As Decimal = 0
        Try
            Select Case psTipoRedondeo
                Case "R"
                    lnNumeroARedondear = Math.Round(pnNumeroARedondear, 2)
                Case "S"
                    lnNumeroARedondear = Math.Ceiling(pnNumeroARedondear)
                Case "I"
                    lnNumeroARedondear = Math.Floor(pnNumeroARedondear)
                Case "T"
                    lnNumeroARedondear = Math.Round(pnNumeroARedondear, 2)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        Return lnNumeroARedondear
    End Function

    Public Shared Function RedondeoNumero(ByVal pnNumeroARedondear As Decimal, ByVal psTipoRedondeo As String, ByVal pnCantidadDecimal As Integer) As Decimal
        Dim lnNumeroARedondear As Decimal = 0
        Try
            Select Case psTipoRedondeo
                Case "R"
                    lnNumeroARedondear = Math.Round(pnNumeroARedondear, pnCantidadDecimal)
                Case "S"
                    lnNumeroARedondear = Math.Ceiling(pnNumeroARedondear)
                Case "I"
                    lnNumeroARedondear = Math.Floor(pnNumeroARedondear)
                Case "T"
                    Select Case pnCantidadDecimal
                        Case 0
                            lnNumeroARedondear = Math.Truncate(pnNumeroARedondear)
                        Case 1
                            lnNumeroARedondear = Math.Truncate(pnNumeroARedondear * 10) / 10
                        Case 2
                            lnNumeroARedondear = Math.Truncate(pnNumeroARedondear * 100) / 100
                        Case 3
                            lnNumeroARedondear = Math.Truncate(pnNumeroARedondear * 1000) / 1000
                        Case 4
                            lnNumeroARedondear = Math.Truncate(pnNumeroARedondear * 10000) / 10000
                    End Select


            End Select
        Catch ex As Exception
            Throw ex
        End Try
        If Math.Abs(lnNumeroARedondear) = 0.001 Then
            lnNumeroARedondear = 0.00
        End If
        If Math.Abs(lnNumeroARedondear) = 0.0001 Then
            lnNumeroARedondear = 0.00
        End If
        If Math.Abs(lnNumeroARedondear) = 0.01 Then
            lnNumeroARedondear = 0.00
        End If
        Return lnNumeroARedondear
    End Function


End Class
