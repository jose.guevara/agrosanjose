﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GVSoft.SistemaGerencial.Utilidades")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Jose Guevara")> 
<Assembly: AssemblyProduct("GVSoft.SistemaGerencial.Utilidades")> 
<Assembly: AssemblyCopyright("Copyright © Jose Guevara 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("313fd0b5-cb93-4874-a2a5-a68b23b12627")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
