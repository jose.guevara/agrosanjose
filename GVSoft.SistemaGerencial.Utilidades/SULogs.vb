﻿Imports System.IO
Imports System.Configuration

Public Class SULogs
    Public Shared Sub WriteLog(ByVal mensaje As String, ByVal entryType As System.Diagnostics.EventLogEntryType)
        Dim sw As StreamWriter
        'Dim sender As String = ConfigurationManager.AppSettings("NombreArchivoLog")
        Dim sender As String = String.Empty
        'Dim rutaArchivo As String = ConfigurationManager.AppSettings("NombreCarpetaLog") + "\" & ConfigurationManager.AppSettings("NombreArchivoLog") & DateTime.Now.ToString("ddMMyyyy") & ".txt"
        Dim rutaArchivo As String = String.Empty
        Dim mensajeLog As String = String.Empty

        Dim append As Boolean = True



        Try
            sender = ConfigurationManager.AppSettings("NombreArchivoLog")
            rutaArchivo = ConfigurationManager.AppSettings("NombreCarpetaLog") + "\" & ConfigurationManager.AppSettings("NombreArchivoLog") & DateTime.Now.ToString("ddMMyyyy") & ".txt"

            mensajeLog = (entryType.ToString() & "->>: " & sender.ToString() & mensaje) + Environment.NewLine
            append = True
            sw = New StreamWriter(rutaArchivo, append)

            sw.Write(mensajeLog)
            sw.Flush()
            sw.Close()
            sw = Nothing
        Catch ex As Exception
            'sw.Flush();
            'sw.Close();
        Finally
        End Try
    End Sub
    Public Shared Sub EscribeLog(ByVal Clase As String, ByVal Metodo As String, ByVal SentenciaSql As String, ByVal mensaje As String, ByVal entryType As System.Diagnostics.EventLogEntryType)
        Dim sMensajeCompleto As String = String.Empty
        Try
            sMensajeCompleto = " Clase : " & Clase & " Metodo: " & Metodo & " SentenciaSQL: " & SentenciaSql & " Mensaje Error: " & mensaje
            WriteLog(sMensajeCompleto, entryType)
        Catch ex As Exception
            WriteLog("Error: " & ex.Message.ToString(), entryType)
        End Try

    End Sub

End Class
