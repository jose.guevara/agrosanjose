@echo off

if exist C:\ArchivoSistemaFacturacion (goto CrearSubDir) else (goto CrearDir)

:CrearSubDir
	md C:\ArchivoSistemaFacturacion\ModArqueos
	md C:\ArchivoSistemaFacturacion\ModCompras
	md C:\ArchivoSistemaFacturacion\ModInventario
	md C:\ArchivoSistemaFacturacion\ModParametros
	md C:\ArchivoSistemaFacturacion\ModRecibosClientes
	md C:\ArchivoSistemaFacturacion\ModRecibosProveedor
	md C:\ArchivoSistemaFacturacion\ModTipoCambio
	md C:\ArchivoSistemaFacturacion\ModTraslados
	md C:\ArchivoSistemaFacturacion\ModVentas
	md C:\ArchivoSistemaFacturacion\bkBaseDatos

	goto Fin

:CrearDir
	md C:\ArchivoSistemaFacturacion
	md C:\ArchivoSistemaFacturacion\ModArqueos
	md C:\ArchivoSistemaFacturacion\ModCompras
	md C:\ArchivoSistemaFacturacion\ModInventario
	md C:\ArchivoSistemaFacturacion\ModParametros
	md C:\ArchivoSistemaFacturacion\ModRecibosClientes
	md C:\ArchivoSistemaFacturacion\ModRecibosProveedor
	md C:\ArchivoSistemaFacturacion\ModTipoCambio
	md C:\ArchivoSistemaFacturacion\ModTraslados
	md C:\ArchivoSistemaFacturacion\ModVentas
	md C:\ArchivoSistemaFacturacion\bkBaseDatos
	
	goto Fin

:Fin
	echo Arbol de Carpetas Creados

md C:\LogsInventario

pause

exit