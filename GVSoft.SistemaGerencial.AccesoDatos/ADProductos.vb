﻿Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Public Class ADProductos
    ''' <summary>
    ''' Obtiene todos los productos activos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneProductosxCodigo(ByVal sCodigoProducto As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "BuscarProductoxCodigoProducto"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, sCodigoProducto)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "BuscarProductoxCodigoProducto", Sentencia, ex.Message.ToString())
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los productos activos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneProductos() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "BuscarProductos"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "BuscarProductos", Sentencia, ex.Message.ToString())
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene primer producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimerProducto() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "BuscarPrimerProducto"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "BuscarPrimerProducto", Sentencia, ex.Message.ToString())
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene primer producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAnteriorProducto(ByVal nRegistro As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "BuscarProductoAnterior"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, nRegistro)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "ObtieneAnteriorProducto", Sentencia, ex.Message.ToString())
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene primer producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneSiguienteProducto(ByVal nRegistro As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "BuscarProductoSiguiente"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, nRegistro)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "ObtieneSiguienteProducto", Sentencia, ex.Message.ToString())
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimoProducto() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "BuscarUltimoProducto"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "ObtieneUltimoProducto", Sentencia, ex.Message.ToString())
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Graba producto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function GrabaProducto(ByVal nRegistro As Integer, ByVal sCodigo As String, ByVal sDescripcion As String, _
                             ByVal nClase As Integer, ByVal nSubClase As Integer, ByVal nUnidad As Integer, _
                             ByVal nEmpaque As Integer, ByVal nPVPC As Double, ByVal nPVDC As Double, _
                             ByVal nPVPU As Double, ByVal nPVDU As Double, ByVal nImpuesto As Integer, _
                             ByVal nEstado As Integer, ByVal nVerificar As Integer, ByVal nProveedor As Integer, _
                             ByVal nMaximaDescuento As Double) As Integer

        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "sp_IngProductos"
        Dim dbCommand As DbCommand
        Dim Sentencia As String = String.Empty

        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, nRegistro, sCodigo, sDescripcion, nClase, nSubClase, _
                                                                      nUnidad, nEmpaque, nPVPC, nPVDC, nPVPU, nPVDU, nImpuesto, _
                                                                      nEstado, nVerificar, nProveedor, nMaximaDescuento)
            Sentencia = ADError.ObtieneSentenciaSQL(dbCommand)
        Catch ex As Exception
            ADError.IntgresardetError("ADProductos", "GrabaProducto", Sentencia, ex.Message.ToString())
            Throw ex
        End Try
    End Function
End Class
