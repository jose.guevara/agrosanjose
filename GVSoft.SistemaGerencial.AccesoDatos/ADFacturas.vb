﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
'Imports System.Data.OleDb.OleDbConnection

Public Class ADFacturas
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADFacturas"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaPermisoFacturarBajoCosto(ByVal pnUsuario As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ValidaPermisoFacturarBajoCosto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaPermisoFacturarClienteSinDisponible(ByVal pnUsuario As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ValidaPermisoFacturarClienteSinDisponible"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaPermisoFacturarClienteConFacturasPendiente(ByVal pnUsuario As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ValidaPermisoFacturarClienteFacturasPendiente"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function VerificaNumeroFactura(ByVal NumeroFactura As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "verificaNumeroFactura"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroFactura)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaPermisoPermiteCambiarFechaEnOperaciones(ByVal pnUsuario As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ValidaPermisoPermiteCambiarFechaEnOperaciones"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteAInt(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaPermisoVercosto(ByVal pnUsuario As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ValidaPermisoVerCosto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteAInt(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function UbicarPendientes(ByVal RegistroAgencia As Integer, ByVal TipoFact As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_ExtraerFactPend"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, TipoFact)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ExtraerFacturaDT(ByVal RegistroAgencia As Integer, ByVal strNumeroFact As String, ByVal TipoFact As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_ExtraerFactura"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, strNumeroFact, TipoFact)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function ExtraerFactura(ByVal RegistroAgencia As Integer, ByVal strNumeroFact As String, ByVal TipoFact As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_ExtraerFactura"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, strNumeroFact, TipoFact)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function UbicarFacturas(ByVal RegistroAgencia As Integer, ByVal strNumeroFact As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_UbicarFacturas"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, strNumeroFact)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaTransaccionFacturas(ByVal objMaestroFacturas As SEEncabezadoFacturas, ByVal lstDetalleFactura As List(Of SEDetalleFacturas), ByVal intTipoFactura As Integer)
        Dim objConexion As Database = Nothing
        Dim transaction1 As DbTransaction = Nothing
        Dim tipoMov As String = String.Empty
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    If intTipoFactura = 3 Or intTipoFactura = 6 Then
                        tipoMov = "AF"
                        objMaestroFacturas.fechaingreso = Format(Now, "dd-MMM-yyyy")
                    Else
                        tipoMov = "SF"
                    End If

                    If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
                        IngresaMaestroFacturas(objConexion, objMaestroFacturas, transaction1)
                    End If
                    If intTipoFactura = 3 Or intTipoFactura = 6 Then
                        IngresaMaestroFacturas(objConexion, objMaestroFacturas, transaction1)
                    End If
                    If intTipoFactura = 1 Or intTipoFactura = 4 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
                        IngresaDetalleFacturas(objConexion, lstDetalleFactura, transaction1, objMaestroFacturas.agenregistro, objMaestroFacturas.numfechaing, objMaestroFacturas.numero)
                    End If
                    If intTipoFactura <> 9 And intTipoFactura <> 10 Then
                        IngresaMovimientoProducto(objConexion, lstDetalleFactura, transaction1, objMaestroFacturas, tipoMov)
                    End If

                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMaestroFacturas(ByVal objDbSistemaGerencial As Database, ByVal objMaestroFacturas As SEEncabezadoFacturas, ByVal objTrans As DbTransaction)
        Dim sp As String = "sp_IngFacturasEnc"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroFacturas.registro, objMaestroFacturas.numero,
                                                                      objMaestroFacturas.numfechaing, objMaestroFacturas.fechaingreso,
                                                                      objMaestroFacturas.vendregistro, objMaestroFacturas.cliregistro,
                                                                      objMaestroFacturas.clinombre, objMaestroFacturas.subtotal,
                                                                      objMaestroFacturas.impuesto, objMaestroFacturas.retencion,
                                                                      objMaestroFacturas.total, objMaestroFacturas.userregistro,
                                                                      objMaestroFacturas.agenregistro, objMaestroFacturas.impreso,
                                                                      objMaestroFacturas.deptregistro, objMaestroFacturas.negregistro,
                                                                      objMaestroFacturas.tipofactura, objMaestroFacturas.diascredito,
                                                                      objMaestroFacturas.tipocambio, objMaestroFacturas.status,
                                                                      objMaestroFacturas.Direccion, objMaestroFacturas.IdMonedaFactura, 0, 0)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleFacturas(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleFacturas As List(Of SEDetalleFacturas), _
                                             ByVal objTrans As DbTransaction, ByVal IdAgencia As Integer, ByVal NumFechaIng As Integer, ByVal strNumero As String)
        Dim sp As String = "sp_IngFacturasDet"
        Dim dbCommand As DbCommand = Nothing
        Try

            For i As Int32 = 0 To lstDetalleFacturas.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, lstDetalleFacturas(i).prodregistro,
                                                                           lstDetalleFacturas(i).cantidad, lstDetalleFacturas(i).precio,
                                                                           lstDetalleFacturas(i).valor, lstDetalleFacturas(i).igv, lstDetalleFacturas(i).MontoDescuento,
                                                                            IdAgencia, NumFechaIng, strNumero, lstDetalleFacturas(i).PrcDescuento, lstDetalleFacturas(i).EsBonificacion)
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMovimientoProducto(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleFacturas As List(Of SEDetalleFacturas), _
                                             ByVal objTrans As DbTransaction, ByVal objFact As SEEncabezadoFacturas, ByVal strTipoMov As String)
        Dim sp As String = "sp_IngProductoMovim"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleFacturas.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, objFact.numfechaing, Format(Now, "Hmmss"), objFact.agenregistro, _
                                                                           lstDetalleFacturas(i).prodregistro, objFact.fechaingreso, _
                                                                           objFact.numero, strTipoMov, "COR", objFact.total, _
                                                                           lstDetalleFacturas(i).cantidad, 0, 0, "", "", objFact.userregistro, _
                                                                           0, 0, 0, 1)
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function ActualizaConsecutivoFacturaRapida(ByVal pnRegistroAgencia As Integer, ByVal psSerie As String) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ActualizaNumeroSerieFacturaRapida"
        Dim dbCommand As DbCommand
        Dim lnRespuesta As Integer = 0
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnRegistroAgencia, psSerie)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            lnRespuesta = objDbSistemaGerencial.ExecuteNonQuery(dbCommand)

            Return lnRespuesta
        Catch ex As Exception
            lnRespuesta = 0
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function ObtieneConsecutivoFacturaRapida(ByVal pnRegistroAgencia As Integer, ByVal psSerie As String) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneNumeroSerieFacturaRapida"
        Dim dbCommand As DbCommand
        Dim lnRespuesta As Integer = 0
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnRegistroAgencia, psSerie)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            lnRespuesta = objDbSistemaGerencial.ExecuteScalar(dbCommand)
            Return lnRespuesta
        Catch ex As Exception
            lnRespuesta = 0
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub EliminarFacturaAImportar(ByVal psNumeroFactura As String)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "EliminaFacturaAImportar"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroFactura)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
