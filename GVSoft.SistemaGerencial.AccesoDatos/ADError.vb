﻿Imports System.Data
Imports System.Data.Common
Imports Microsoft.VisualBasic.CompilerServices
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Text
Imports System.Data.SqlClient

Public Class ADError

    Public Shared Sub IntgresardetError(ByVal Clase As String, ByVal Metodo As String, ByVal Sentencia As String, _
    ByVal Descripcion As String)
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "IngresardetError"
        Dim dbCommand As DbCommand

        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, Clase, Metodo, Sentencia, Descripcion)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ' <summary>
    ' Metodo que obtiene la sentencia sql
    ' </summary>
    ' <param name="objCmd"></param>
    ' <returns>string</returns>

    Public Shared Function ObtieneSentenciaSQL(ByVal objCmd As DbCommand) As String

        If objCmd Is Nothing Then
            Return "El objeto Command llega con valor Nulo o Nothing"
        End If

        Dim Sentencia As String = String.Empty
        Dim strparam As StringBuilder = New StringBuilder()
        Dim sqlparam As SqlParameter

        strparam.Append("EXEC " + objCmd.CommandText + " ")
        Sentencia = "EXEC " + objCmd.CommandText + " "

        For Each sqlparam In objCmd.Parameters

            Select Case sqlparam.SqlDbType
                Case SqlDbType.Char
                Case SqlDbType.NChar
                Case SqlDbType.NText
                Case SqlDbType.NVarChar
                Case SqlDbType.Text
                Case SqlDbType.VarChar
                Case SqlDbType.Date
                Case SqlDbType.DateTime
                Case SqlDbType.DateTime2
                Case SqlDbType.DateTimeOffset
                Case SqlDbType.SmallDateTime
                    strparam.Append("'" + sqlparam.Value.ToString() + "',")
                    Sentencia = Sentencia + "'" + sqlparam.Value.ToString() + "',"
                Case Else
                    If sqlparam.ParameterName <> "@RETURN_VALUE" Then
                        strparam.Append(sqlparam.Value.ToString() + ",")
                        Sentencia = Sentencia + sqlparam.Value.ToString() + ","
                    End If
            End Select
        Next

        Return strparam.ToString()
    End Function
End Class
