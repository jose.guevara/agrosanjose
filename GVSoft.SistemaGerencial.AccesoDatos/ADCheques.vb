﻿Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class ADCheques
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ObtieneCheques() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ConCheques"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el detalle de un cheque por numero de cheque 
    ''' </summary>
    ''' <param name="psNumeroCheque">Numero de cheque</param>
    ''' <retorna>un datatable</retorna>
    Public Function ObtieneCheques(ByVal psNumeroCheque As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ConChequeXNumeroCheque"
        Dim dbCommand As DbCommand

        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroCheque)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Obtiene el detalle de un cheque por numero de cheque 
    ''' </summary>
    ''' <param name="pnCtabanregistro">Numero de registro en banco</param>
    ''' <retorna>un datatable</retorna>

    Public Function InsertaCheque(ByVal pnCtabanregistro As Integer, ByVal pnBenefregistro As Integer, ByVal psBeneficiario As String, _
                                  ByVal psDescripcion As String, ByVal pdFechacheque As DateTime, ByVal psDocumento As String, _
                                  ByVal pdMonto As Double, ByVal pnTipomovim As Integer) As Object()

        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "insCheque"
        Dim dbCommand As DbCommand
        Dim Resultado As Object() = New Object(2) {}  'Declaracion de arreglos en vb
        Dim lnRegistrosIngresados As Integer = 0
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnCtabanregistro, pnBenefregistro, psBeneficiario, psDescripcion, _
                                                                   pdFechacheque, psDocumento, pdMonto, pnTipomovim)

            lnRegistrosIngresados = objDbSistemaGerencial.ExecuteScalar(dbCommand)

            Resultado(0) = 0
            Resultado(1) = "Registro Ingresado Correctamente"
            Return Resultado
        Catch ex As Exception
            Resultado(0) = 1
            Resultado(1) = ex.Message.ToString().Trim()
            Throw ex
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Obtiene el detalle de un cheque por numero de cheque 
    ''' </summary>
    ''' <param name="registrocta">Numero de registro de la cuenta bancaria</param>
    ''' <retorna>un datatable</retorna>

    Public Function ModifCtasBancarias(ByVal registrocta As Integer, ByVal blnactual As Double) As Object()

        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "sp_ModifCtasBancarias"
        Dim dbCommand As DbCommand
        Dim Resultado As Object() = New Object(2) {}  'Declaracion de arreglos en vb
        Dim lnRegistrosIngresados As Integer = 0
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, registrocta, blnactual)
            'MsgBox("Registro Actualizado", MsgBoxStyle.Information, "Atualizar")
            lnRegistrosIngresados = objDbSistemaGerencial.ExecuteScalar(dbCommand)

            Resultado(0) = 0
            Resultado(1) = "Registro Actualizado Correctamente"
            Return Resultado
        Catch ex As Exception
            Resultado(0) = 1
            Resultado(1) = ex.Message.ToString().Trim()
            Throw ex
            Return Nothing
        End Try
    End Function


End Class

