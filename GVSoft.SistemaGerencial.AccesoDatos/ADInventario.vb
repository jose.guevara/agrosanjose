﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class ADInventario
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "GVSoft.SistemaGerencial.AccesoDatos.ADInventario"

    ''' <summary>
    ''' Obtiene Requisas por rango de fecha
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneEncabezadoTrasladosXFecha(ByVal FechaIncial As Integer, ByVal FechaFinal As Integer, ByVal IdAgencia As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtenerEncabezadoTrasladosXFecha"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, FechaIncial, FechaFinal, IdAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneNumeroPedido(ByVal RegistroAgencia As Int16, ByVal Serie As String, ByVal TipoFact As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneParametrosNumeroPedido"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, Serie, TipoFact)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub ActualizarNumeroPedido(ByVal RegistroAgencia As Int16, ByVal TipoFact As Integer, ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal Serie As String)
        Dim sp As String = "spActualizarNumPedido"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, TipoFact, Serie)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try

    End Sub

    Public Shared Sub IngresaTransaccionPedidos(ByVal objPedidos As SEMaestroPedidos, ByVal lstPedidosDet As List(Of SEDetallePedidos), ByVal Serie As String)
        Dim objConexion As Database
        Dim transaction1 As DbTransaction

        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try

                    IngresaMaestroPedidos(objConexion, objPedidos, transaction1)
                    IngresaDetallePedidos(objConexion, lstPedidosDet, transaction1, objPedidos.NumeroDocumento)
                    ActualizarNumeroPedido(objPedidos.AgenregistroOrigen, 0, objConexion, transaction1, Serie)

                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMaestroPedidos(ByVal objDbSistemaGerencial As Database, ByVal objPedidos As SEMaestroPedidos, ByVal objTrans As DbTransaction)
        Dim sp As String = "spIngresarMaestroPedido"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objPedidos.NumeroDocumento, objPedidos.IdMovimiento, _
                                                                      objPedidos.NumFechaIng, objPedidos.FechaIngreso, _
                                                                      objPedidos.FechaIngresoReg, objPedidos.SubTotal, _
                                                                      objPedidos.SubTotalCosto, objPedidos.Impuesto, _
                                                                      objPedidos.Retencion, objPedidos.Total, _
                                                                      objPedidos.UserRegistro, objPedidos.AgenregistroOrigen, _
                                                                      objPedidos.IdEstado, objPedidos.TipoCambio, _
                                                                      objPedidos.NumFechaAnul)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetallePedidos(ByVal objDbSistemaGerencial As Database, ByVal lstPedidoDet As List(Of SEDetallePedidos), _
                                             ByVal objTrans As DbTransaction, ByVal strNumero As String)
        Dim sp As String = "spIngresarDetallePedido"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstPedidoDet.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstPedidoDet(i).ProdRegistro, lstPedidoDet(i).Cantidad, _
                                                                       lstPedidoDet(i).PrecioCosto, lstPedidoDet(i).CostoUnitario, _
                                                                       lstPedidoDet(i).Total, lstPedidoDet(i).IGV, strNumero)
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene el Inventario Pedido
    ''' </summary>
    ''' <retorna>Un Objecto Reader</retorna>
    Public Shared Function ExtraerPedidoXNumeroPedido(ByVal strNumeroFact As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spExtraerPedidoXNumeroPedido"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, strNumeroFact)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el Inventario Pedido
    ''' </summary>
    ''' <retorna>Un Objecto Reader</retorna>
    Public Shared Function ObtieneDetalleProductosPedidos(ByVal strNumeroPedido As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneDetalleProductosPedidos"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, strNumeroPedido)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el Inventario Proforma
    ''' </summary>
    ''' <retorna>Un Objecto Reader</retorna>
    Public Shared Function ObtieneDetalleProformasAFacturar(ByVal strNumeroPedido As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneDetalleProformasAFacturar"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, strNumeroPedido)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Ingreso de movimiento de inventario
    ''' </summary>
    ''' <retorna>Un Objecto Reader</retorna>
    Public Shared Sub IngresaMovimientoProducto(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleInventario As List(Of SEProductoMovimiento),
                                            ByVal objTrans As DbTransaction)
        Dim sp As String = "sp_IngProductoMovim"
        Dim lsObservacion As String = String.Empty
        Dim dbCommand As DbCommand = Nothing
        Try
            sSentenciaSql = String.Empty
            For i As Int32 = 0 To lstDetalleInventario.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstDetalleInventario(i).registro, lstDetalleInventario(i).numfechaing, lstDetalleInventario(i).numtiempo,
                                                                           lstDetalleInventario(i).agenregistro, lstDetalleInventario(i).prodregistro, lstDetalleInventario(i).fechaingreso,
                                                                           lstDetalleInventario(i).numdocumento, lstDetalleInventario(i).codigomovim, lstDetalleInventario(i).codigomoneda,
                                                                           lstDetalleInventario(i).totalfactura, lstDetalleInventario(i).Cantidad, lstDetalleInventario(i).TotalCosto,
                                                                           lstDetalleInventario(i).Saldo, lstDetalleInventario(i).cuenta_debe, lstDetalleInventario(i).cuenta_haber,
                                                                           lstDetalleInventario(i).userregistro, lstDetalleInventario(i).Cantidad, lstDetalleInventario(i).prodregistro,
                                                                           lstDetalleInventario(i).agenregistro, lstDetalleInventario(i).origen)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene el Inventario Pedido
    ''' </summary>
    ''' <retorna>Un Objecto Reader</retorna>
    Public Shared Function ObtieneSerieParametrosTrasladoInventario(ByVal pnIdAgencia As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneSerieParametrosTrasladoInventario"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

End Class
