﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class SDError
    ''' <summary>
    ''' Ingres registro en clientes
    ''' </summary>
    ''' <retorna>Un Objecto datatable</retorna>
    Public Shared Sub IngresarError(ByVal ObjError As SEError)
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "IngresardetError"
        Dim dbCommand As DbCommand
        Dim sSentenciaSql As String = String.Empty
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, ObjError.Clase, ObjError.Proceso, ObjError.Metodo, ObjError.Sentencia, ObjError.descripcion)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            SULogs.EscribeLog("SDError", "IngresarError", sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            Throw ex
        End Try
    End Sub
End Class
