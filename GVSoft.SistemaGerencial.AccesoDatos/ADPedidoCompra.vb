﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADPedidoCompra

    Private objAuditoria As SEAuditoria = New SEAuditoria()
    Private Shared objError As SEError = New SEError()

    Public Sub New()
        If objAuditoria Is Nothing Then
            objAuditoria = New SEAuditoria
        End If
        objAuditoria.NombreClase = "ADPedidoCompra"
    End Sub
    Public Function IngresaMaestroPedido(ByVal objDbSistemaGerencial As Database, ByVal objMaestroPedido As SEMaestroPedidoCompra, ByVal objTrans As DbTransaction) As Integer
        Dim sp As String = "spIngresarMaestroPedidoCompra"
        Dim dbCommand As DbCommand = Nothing
        Dim lnIdPedido As Integer = 0
        Try
            objAuditoria.SentenciaSql = String.Empty
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroPedido.NumeroDocumento,
                                                                       objMaestroPedido.FechaIngresoNum, objMaestroPedido.FechaIngreso,
                                                                       objMaestroPedido.IdProveedor, objMaestroPedido.SubTotal,
                                                                       objMaestroPedido.Impuesto, objMaestroPedido.Retencion,
                                                                       objMaestroPedido.Descuento, objMaestroPedido.Total,
                                                                       objMaestroPedido.UserRegistro, objMaestroPedido.AgenregistroOrigen,
                                                                        objMaestroPedido.IdEstado, objMaestroPedido.TipoCambio)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            lnIdPedido = SUConversiones.ConvierteAInt(objDbSistemaGerencial.ExecuteScalar(dbCommand, objTrans))
            Return lnIdPedido
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Sub IngresaDetallePedidoCompra(ByVal objDbSistemaGerencial As Database, ByVal lstDetallePeidoCompra As List(Of SEDetPedidoCompra),
                                           ByVal objTrans As DbTransaction, ByVal pnIdPedido As Integer, ByVal psNumero As String, ByVal pnIdAgencia As Integer)
        Dim sp As String = "GrabaPedidoCompraDetalle"
        Dim dbCommand As DbCommand = Nothing
        Dim lnIdDetallePedido As Integer = 0
        Try
            objError = Nothing
            objError = New SEError()
            For i As Int32 = 0 To lstDetallePeidoCompra.Count - 1
                lnIdDetallePedido = 0
                lnIdDetallePedido = i + 1
                lstDetallePeidoCompra(i).IdPedido = 0
                lstDetallePeidoCompra(i).IdPedido = pnIdPedido
                objAuditoria.SentenciaSql = String.Empty
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdPedido,
                                                                           lnIdDetallePedido, lstDetallePeidoCompra(i).IdProducto,
                                                                           lstDetallePeidoCompra(i).Cantidad, lstDetallePeidoCompra(i).PrecioCosto,
                                                                           lstDetallePeidoCompra(i).SubTotal, lstDetallePeidoCompra(i).Impuesto,
                                                                           lstDetallePeidoCompra(i).Descuento, lstDetallePeidoCompra(i).Total,
                                                                           pnIdAgencia, lstDetallePeidoCompra(i).FechaIngresoNum, psNumero,
                                                                           lstDetallePeidoCompra(i).EsBonificable, pnIdPedido)

                objAuditoria.SentenciaSql = String.Empty
                objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub


    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ObtienePedidosPendientesTodos() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePedidosCompraPendientes"
        Dim dbCommand As DbCommand = Nothing
        Try
            objAuditoria = New SEAuditoria()

            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ObtienePedidosPendientesxAgenciaYProveedor(ByVal IdAgencia As Integer, ByVal IdProveedor As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePedidosCompraPendientesxAgenciaYProveedor"
        Dim dbCommand As DbCommand = Nothing
        Try
            objAuditoria = New SEAuditoria()

            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdAgencia, IdProveedor)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Detalle de Productos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function UbicarPendientes(ByVal pnIdAgencia As Integer, ByVal pnIdTipoFactura As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_ExtraerFactPend"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdAgencia, pnIdTipoFactura)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Detalle de Productos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ExtraerPedidoXNumeroPedidoCompraxNumeroYAgencia(ByVal pnIdAgencia As Integer, ByVal psNumeroCompra As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spExtraerPedidoXNumeroPedidoCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdAgencia, psNumeroCompra)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Obtiene Detalle de Productos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ObtienePedidoCompraxNumero(ByVal psNUmeroPedido As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePedidoCompraPendientexNumero"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNUmeroPedido)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Detalle de Productos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ObtienePedidoCompraxId(ByVal pnIdPedido As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePedidoCompraPendientexId"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdPedido)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    Public Sub ActualizarNumeroPedidoCompra(ByVal RegistroAgencia As Integer, ByVal TipoFact As Integer, ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal Serie As String)
        Dim sp As String = "spActualizarNumPedidoCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, TipoFact, Serie)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try

    End Sub

    ''' <summary>
    ''' Obtiene Detalle de Productos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Function ObtieneSeriexAgencia(ByVal pnIdAgencia As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spObtieneSeriePedidosCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdAgencia)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Function ObtieneNumeroPedido(ByVal RegistroAgencia As Int16, ByVal Serie As String, ByVal TipoFact As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneParametrosNumeroPedidoCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, Serie, TipoFact)
            objAuditoria.SentenciaSql = String.Empty
            objAuditoria.SentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Sub IngresaTransaccionPedidoCompra(ByVal objMaestroPedidoCompra As SEMaestroPedidoCompra, ByVal lstDetallePeidoCompra As List(Of SEDetPedidoCompra), ByVal psSerie As String, ByVal pnTipoFactura As Integer)
        Dim objConexion As Database = Nothing
        Dim transaction1 As DbTransaction = Nothing
        Dim IdPedido As Integer = 0
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    IdPedido = IngresaMaestroPedido(objConexion, objMaestroPedidoCompra, transaction1)
                    IngresaDetallePedidoCompra(objConexion, lstDetallePeidoCompra, transaction1, IdPedido, objMaestroPedidoCompra.NumeroDocumento, objMaestroPedidoCompra.AgenregistroOrigen)
                    ActualizarNumeroPedidoCompra(objMaestroPedidoCompra.AgenregistroOrigen, pnTipoFactura, objConexion, transaction1, psSerie)
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            objAuditoria.NombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = objAuditoria.NombreClase
            objError.Metodo = objAuditoria.NombreMetodo
            objError.Sentencia = objAuditoria.SentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, objAuditoria.NombreMetodo, objAuditoria.SentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

End Class
