﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.SqlClient
Imports System.Data.Common

Public Class ADTesoreria
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneSoliDasientosxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneSoliDasientos"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneSoliDasientosxNumSolicDr(ByVal psNumeroSolic As Integer) As SqlDataReader
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneSoliDasientos"
        Dim dbCommand As DbCommand = Nothing
        Dim drDatos As SqlDataReader = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            drDatos = objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return drDatos
        End Try
        Return drDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneSoliMasientosxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneSoliMasientos"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneMasientosxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneMaestroContabilidadCheque"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneDasientosxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneDetalleContabilidadCheque"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function


    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneSoliMasientosxNumSolicDr(ByVal psNumeroSolic As Integer) As SqlDataReader
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneSoliMasientos"
        Dim dbCommand As DbCommand = Nothing
        Dim drDatos As SqlDataReader = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            drDatos = objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return drDatos
        End Try
        Return drDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneBancos() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneBancos"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    Public Shared Function ObtieneSolicitudesxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneSolicitudesxNumSolic"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneBancosTodos() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneBancos"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function
End Class
