﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class ADRecibos
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADRecibos"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub EliminaReciboAImportarxNumeroRecibo(ByVal psNumeroRecibo As String)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spEliminarNumeroReciboAImportar"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneInformacionRecibo(ByVal NumeroRecibo As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneDetalleReciboxNumeroRecibo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneDTInformacionRecibo(ByVal NumeroRecibo As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneDetalleReciboxNumeroRecibo"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function VerificaEstadoRecibo(ByVal NumeroRecibo As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "VerificaEstadoNumeroRecibo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function VerificaNumeroRecibo(ByVal NumeroRecibo As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "verificaNumeroRecibo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Informacion de Recibos por Numero de Recibos y Clientes
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneInformacionReciboXClienteNumeroRecibos(ByVal IdCliente As Integer, ByVal NumeroRecibo As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneDetalleReciboxClienteYNumeroRecibo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCliente, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Informacion de Facturas Pendientes de los Clientes
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneInformacionFacturasXClientes(ByVal IdCliente As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spObternerFacturasClientes"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCliente)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Informacion de Facturas Pendientes de los Clientes
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ConsultaDetalleReciboxNumeroRecibo(ByVal NumeroRecibo As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ConsultaDetalleReciboxNumeroRecibo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaPermisoPagaFacturaRecientesConFacturasAntiguaConSaldo(ByVal pnUsuario As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ValidaPermisoPagaFacturaRecientesConFacturasAntiguaConSaldo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Saldo Pendiente del Cliente
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtenerSaldoPendiente(ByVal IdCliente As Integer) As Double
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtenerSaldoPendiente"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCliente)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    Public Shared Sub IngresaTransaccionRecibos(ByVal objMaestroRecibos As SERecibosEncabezado, ByVal lstDetalleRecibo As List(Of SERecibosDetalle), ByVal Retencion As Double, ByVal Pendiente As Double)
        Dim objConexion As Database
        Dim transaction1 As DbTransaction
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    IngresaMaestroRecibos(objConexion, objMaestroRecibos, transaction1)
                    IngresaDetalleRecibo(objConexion, lstDetalleRecibo, transaction1, objMaestroRecibos.cliregistro, objMaestroRecibos.numero, objMaestroRecibos.tiporecibo)
                    If Retencion <> 0 Then
                        IngresarRetencion(objConexion, transaction1, objMaestroRecibos, Retencion)
                    End If
                    If Pendiente <> 0 Then
                        IngresarSaldoPendiente(objConexion, transaction1, objMaestroRecibos, Pendiente)
                    End If
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaTransaccionNotaCredito(ByVal objMaestroRecibos As SERecibosEncabezado, ByVal lstDetalleRecibo As List(Of SERecibosDetalle))
        Dim objConexion As Database
        Dim transaction1 As DbTransaction
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    IngresaMaestroNotaCredito(objConexion, objMaestroRecibos, transaction1)
                    IngresaDetalleNotaCredito(objConexion, lstDetalleRecibo, transaction1, objMaestroRecibos.cliregistro, objMaestroRecibos.numero, objMaestroRecibos.tiporecibo)
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMaestroRecibos(ByVal objDbSistemaGerencial As Database, ByVal objRecibos As SERecibosEncabezado, ByVal objTrans As DbTransaction)
        Dim sp As String = "sp_IngRecibosEnc"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objRecibos.registro, objRecibos.fecha, objRecibos.numfecha,
                                                                    objRecibos.cliregistro, objRecibos.vendregistro, objRecibos.numero,
                                                                    objRecibos.monto, objRecibos.interes, objRecibos.retencion,
                                                                    objRecibos.formapago, objRecibos.numchqtarj, objRecibos.nombrebanco, objRecibos.NoDeposito,
                                                                    objRecibos.reciboprov, objRecibos.descripcion, objRecibos.usrregistro,
                                                                    objRecibos.tiporecibo, objRecibos.saldofavor,
                                                                    objRecibos.descr01, objRecibos.descr02, objRecibos.descr03, objRecibos.agenRegistro, 0, Nothing,
                                                                    objRecibos.IdMoneda, objRecibos.TipoCambio)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMaestroNotaCredito(ByVal objDbSistemaGerencial As Database, ByVal objRecibos As SERecibosEncabezado, ByVal objTrans As DbTransaction)
        Dim sp As String = "sp_IngRecibosEnc"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objRecibos.registro, objRecibos.fecha, objRecibos.numfecha,
                                                                    objRecibos.cliregistro, objRecibos.vendregistro, objRecibos.numero,
                                                                    objRecibos.monto, objRecibos.interes, objRecibos.retencion,
                                                                    objRecibos.formapago, objRecibos.numchqtarj, objRecibos.nombrebanco, objRecibos.NoDeposito,
                                                                    objRecibos.reciboprov, objRecibos.descripcion, objRecibos.usrregistro,
                                                                    objRecibos.tiporecibo, objRecibos.saldofavor,
                                                                    objRecibos.descr01, objRecibos.descr02, objRecibos.descr03, objRecibos.agenRegistro, 0, Nothing,
                                                                    objRecibos.IdMoneda, objRecibos.TipoCambio)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleRecibo(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleRecibo As List(Of SERecibosDetalle), _
                                             ByVal objTrans As DbTransaction, ByVal IdCliente As Integer, ByVal strNumero As String, ByVal TipoRec As Integer)
        Dim sp As String = "sp_IngRecibosDet"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleRecibo.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstDetalleRecibo(i).registro, lstDetalleRecibo(i).numero,
                                                                           lstDetalleRecibo(i).descripcion, lstDetalleRecibo(i).monto,
                                                                           IdCliente, strNumero, lstDetalleRecibo(i).interes,
                                                                           lstDetalleRecibo(i).mantvlr, lstDetalleRecibo(i).pendiente,
                                                                           TipoRec)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleNotaCredito(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleRecibo As List(Of SERecibosDetalle), _
                                             ByVal objTrans As DbTransaction, ByVal IdCliente As Integer, ByVal strNumero As String, ByVal TipoRec As Integer)
        Dim sp As String = "sp_IngRecibosDet"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleRecibo.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstDetalleRecibo(i).registro, lstDetalleRecibo(i).numero,
                                                                           lstDetalleRecibo(i).descripcion, lstDetalleRecibo(i).monto,
                                                                           IdCliente, strNumero, 0, 0, 0, TipoRec)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresarRetencion(ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal objReciboEncabezado As SERecibosEncabezado, ByVal Retencion As Double)
        Dim sp As String = "sp_IngRecibosDet"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, "", "Retención del Recibo", Retencion,
                                                                           objReciboEncabezado.cliregistro, objReciboEncabezado.numero, 0,
                                                                           0, 0, 0)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresarSaldoPendiente(ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal objReciboEncabezado As SERecibosEncabezado, ByVal Pendiente As Double)
        Dim sp As String = "sp_IngRecibosDet"
        Dim dbCommand As DbCommand = Nothing
        Try

            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, "", "Saldo Pendiente", Pendiente,
                                                                       objReciboEncabezado.cliregistro, objReciboEncabezado.numero, 0,
                                                                       0, 0, 0)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneFacturasPendientesxCliente(ByVal IdCliente As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneFacturasPendientexCliente"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCliente)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub AnulaNotaCredito(ByVal IdCliente As Integer, ByVal Numero As String, ByVal FechaNum As Integer,
                                       ByVal MontoRec As Double, ByVal SaldoFavor As Double, ByVal TipoRecibo As Integer)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_IngRecibosAnular"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCliente, Numero, FechaNum,
                                                                    MontoRec, SaldoFavor, TipoRecibo)

            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
