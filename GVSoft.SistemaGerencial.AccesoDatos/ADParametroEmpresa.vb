﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADParametroEmpresa
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneParametroEmpresa() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneParametroEmpresa"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
            Return dtDatos
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADParametroEmpresa"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
