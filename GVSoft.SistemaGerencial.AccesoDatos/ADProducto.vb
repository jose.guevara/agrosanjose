﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class ADProducto
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADProducto"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneProductosxDescripcion(ByVal psDescripcion As String, ByVal pnAgencia As Integer, ByVal pnUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_DetalleProductoFactura"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, "%" & psDescripcion, pnAgencia, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function



    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function UbicarProducto(ByVal psCodigoProducto As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "UbicarProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCodigoProducto)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el Movimiento de Inventario de Traslados
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function BuscaProductoxCodigo(ByVal psCodigo As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneDetallaProductoxCodigo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCodigo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function



    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrecioMinimoxCodigo(ByVal psCodigoProducto As String) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePrecioMinimoAFacturar"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCodigoProducto)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteADouble(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrecioMinimoxCodigo(ByVal psCodigoProducto As String, ByVal pnIdMoneda As Integer) As Double
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePrecioMinimoAFacturarxMoneda"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCodigoProducto, pnIdMoneda)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteADouble(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrecioCostoxCodigoYMoneda(ByVal psCodigoProducto As String, ByVal pnIdMoneda As Integer) As Double
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtienePrecioCostoxCodigoYMoneda"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCodigoProducto, pnIdMoneda)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteADouble(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneExistenciaActualProducto(ByVal pnIdAgencia As Integer, ByVal pnIdProducto As Integer) As Decimal
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneExistenciaActualxProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdAgencia, pnIdProducto)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteADecimal(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneProductosxCodigo(ByVal psDescripcion As String, ByVal pnAgencia As Integer, ByVal pnUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "BuscaProductoFacturaxCodigo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psDescripcion, pnAgencia, pnUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneProductosTodos() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneProductoTodos"
        Dim dbCommand As DbCommand
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneCantidadMinimaxIdSucursalYCodigoProducto(ByVal pnIdSucursal As Integer, ByVal psCodigoProducto As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneMinimosProductoxSucursalYCodigoProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal, psCodigoProducto)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneCantidadMinimaxIdSucursal(ByVal pnIdSucursal As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneMinimosProductoxSucursalYCodigoProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneProductosTodosDR() As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneProductoTodos"
        Dim dbCommand As DbCommand
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Obtiene Detalle de Productos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneDetalleProducto(ByVal IdProducto As Integer, ByVal IdAgencia As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "sp_DetalleProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdProducto, IdAgencia)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el Inventario de Traslados
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ExtraerInventarioTraslado(ByVal IdAgencia As Integer, ByVal strNumeroFact As String, ByVal TipoFactura As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ExtraerInventarioTraslado"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdAgencia, strNumeroFact, TipoFactura)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene el Movimiento de Inventario de Traslados
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function UbicarMovimientoInventarioTraslado(ByVal IdAgencia As Integer, ByVal strNumeroFact As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "UbicarMovimientoInventarioTraslado"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdAgencia, strNumeroFact)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaTransaccionTraslado(ByVal objMovInventario As SEMovimientoInventarioEnc, ByVal lstMovInventarioDet As List(Of SEMovimientoInventarioDet), ByVal intTipoFactura As Integer)
        Dim objConexion As Database = Nothing
        Dim transaction1 As DbTransaction = Nothing
        Dim tipoMov As String
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    If intTipoFactura = 3 Or intTipoFactura = 6 Then
                        tipoMov = "EA"
                    Else
                        tipoMov = "SA"
                    End If

                    If intTipoFactura = 1 Or intTipoFactura = 2 Or intTipoFactura = 4 Or intTipoFactura = 5 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
                        IngresaMaestroTraslado(objConexion, objMovInventario, transaction1)
                    End If
                    If intTipoFactura = 3 Or intTipoFactura = 6 Then
                        IngresaMaestroTraslado(objConexion, objMovInventario, transaction1)
                    End If
                    If intTipoFactura = 1 Or intTipoFactura = 4 Or intTipoFactura = 7 Or intTipoFactura = 8 Then
                        IngresaDetalleTraslado(objConexion, lstMovInventarioDet, transaction1, objMovInventario.agenregistroOrigen, objMovInventario.numfechaing, objMovInventario.numero)
                    End If
                    If intTipoFactura <> 9 And intTipoFactura <> 10 Then
                        IngresaMovimientoProducto(objConexion, lstMovInventarioDet, transaction1, objMovInventario, tipoMov, objMovInventario.agenregistroOrigen)
                        IngresaMovimientoProducto(objConexion, lstMovInventarioDet, transaction1, objMovInventario, tipoMov, objMovInventario.agenregistroDestino)
                    End If

                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMaestroTraslado(ByVal objDbSistemaGerencial As Database, ByVal objMovInventario As SEMovimientoInventarioEnc, ByVal objTrans As DbTransaction)
        Dim sp As String = "GrabaMovimientoTrasladoEnc"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMovInventario.registro, objMovInventario.numero,
                                                                      objMovInventario.numfechaing, objMovInventario.fechaingreso,
                                                                      objMovInventario.vendregistro, objMovInventario.cliregistro,
                                                                      objMovInventario.clinombre, objMovInventario.subtotal,
                                                                      objMovInventario.impuesto, objMovInventario.retencion,
                                                                      objMovInventario.total, objMovInventario.userregistro,
                                                                      objMovInventario.agenregistroOrigen, objMovInventario.impreso,
                                                                      objMovInventario.deptregistro, objMovInventario.negregistro,
                                                                      objMovInventario.tipofactura, objMovInventario.diascredito,
                                                                      objMovInventario.tipocambio, objMovInventario.status,
                                                                      objMovInventario.IdMovimiento, objMovInventario.agenregistroDestino)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleTraslado(ByVal objDbSistemaGerencial As Database, ByVal lstMovInventarioDet As List(Of SEMovimientoInventarioDet), _
                                             ByVal objTrans As DbTransaction, ByVal IdAgencia As Integer, ByVal NumFechaIng As Integer, ByVal strNumero As String)
        Dim sp As String = "GrabaMovimientoTrasladoDet"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstMovInventarioDet.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, lstMovInventarioDet(i).prodregistro,
                                                                           lstMovInventarioDet(i).cantidad, lstMovInventarioDet(i).precio,
                                                                           lstMovInventarioDet(i).valor, lstMovInventarioDet(i).igv,
                                                                           IdAgencia, NumFechaIng, strNumero)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMovimientoProducto(ByVal objDbSistemaGerencial As Database, ByVal lstMovInventarioDet As List(Of SEMovimientoInventarioDet), _
                                             ByVal objTrans As DbTransaction, ByVal objFact As SEMovimientoInventarioEnc, ByVal strTipoMov As String, ByVal IdAgencia As Integer)
        Dim sp As String = "sp_IngProductoMovim"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstMovInventarioDet.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, objFact.numfechaing, Format(Now, "Hmmss"), IdAgencia,
                                                                           lstMovInventarioDet(i).prodregistro, objFact.fechaingreso,
                                                                           objFact.numero, strTipoMov, "COR", objFact.total,
                                                                           lstMovInventarioDet(i).cantidad, 0, 0, "", "", objFact.userregistro,
                                                                           0, 0, 0, 1)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene Campos para Importar Correccion de Inventario Fisico
    ''' </summary>
    ''' <retorna>Un Objecto DB reader </retorna>
    Public Shared Function ObtenerCamposImportarCorreccionIF(ByVal CodProducto As String, ByVal CodAgencia As String, ByVal Cantidad As Double) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spObtenerCamposImportarCorreccionIF"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, CodProducto, CodAgencia, Cantidad)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    Public Shared Sub IngresaMovimientoCorreccionIF(ByVal IdMov As Integer, ByVal NumFecha As Integer, ByVal NumTiempo As Integer,
                                                    ByVal IdAgencia As Integer, ByVal IdProducto As Integer, ByVal strFech As String,
                                                    ByVal strDoc As String, ByVal strTipoMov As String, ByVal strMoneda As String,
                                                    ByVal Total As Double, ByVal Cantidad As Double, ByVal Costo As Double, ByVal Saldo As Double,
                                                    ByVal strDebe As String, ByVal strHaber As String, ByVal IdUser As Integer, ByVal CantConv As Double,
                                                    ByVal IdProdConv As Integer, ByVal IdAgenConv As Integer, ByVal Origen As Integer)
        Dim sp As String = "sp_IngProductoMovim"
        Dim objConexion As Database = Nothing
        Dim dbCommand As DbCommand = Nothing
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objConexion.GetStoredProcCommand(sp, IdMov, NumFecha, NumTiempo, IdAgencia, IdProducto, strFech,
                                                         strDoc, strTipoMov, strMoneda, Total, Cantidad, Costo, Saldo, strDebe,
                                                         strHaber, IdUser, CantConv, IdProdConv, IdAgenConv, Origen)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objConexion.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub AcutalizaPrecioProductosCORConTasaDelDia()
        Dim sp As String = "ActualizaPrecioCORProductoConTasaDelDia"
        Dim objConexion As Database = Nothing
        Dim dbCommand As DbCommand = Nothing
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objConexion.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objConexion.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function UbicarPrecio(ByVal pnIdProducto As Integer, ByVal pnIdIndex As Integer, ByVal pnIdTipoFactura As Integer, ByVal pnIdAgencia As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "UbicarPrecio"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdProducto, pnIdIndex, pnIdTipoFactura, pnIdAgencia)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneMinimoProductosxSucursal(ByVal pnIdSucursal As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneMinimosProductoxSucursal"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneMinimoProductosxSucursalDR(ByVal pnIdSucursal As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneMinimosProductoxSucursal"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneMinimoProductosxSucursalYProductoDR(ByVal pnIdSucursal As Integer, ByVal pnIdProducto As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneMinimosProductoxSucursalYProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal, pnIdProducto)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneMinimoProductosxSucursalYProducto(ByVal pnIdSucursal As Integer, ByVal pnIdProducto As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneMinimosProductoxSucursalYProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal, pnIdProducto)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub GuardarMinimosProductos(ByVal pnIdSucursal As Integer, ByVal pnIdProducto As Integer,
                                                   ByVal pnCantidad As Integer, ByVal pnActivo As Integer)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "GuardarMinimoProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdSucursal, pnIdProducto, pnCantidad, pnActivo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneListadoProductos() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptListadoProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneListadoPrecioCostoProducto() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptListadoCostoProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneListadoPrecioVentaProducto() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptListadoPrecioVentaProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneListadoPrecioVentaMayoristaProducto() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptListadoPrecioVentaMayoristaProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneListadoPrecioVentaMinoristaProducto() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptListadoPrecioVentaMinoristaProducto"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneListadoPrecioYExistenciaActual() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptListadoProductoYExistencia"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
End Class
