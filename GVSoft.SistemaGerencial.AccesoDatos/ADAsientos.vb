﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADAsientos
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneDasientosxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneDasientosxNumSolic"
        Dim dbCommand As Common.DbCommand
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDAsientos"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneReporteAsientosContables(ByVal psNumeroAsiento As String, ByVal psAnioMes As String, ByVal pnNumeroMovimiento As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneReporteAsientoContable"
        Dim dbCommand As Common.DbCommand
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroAsiento, psAnioMes, pnNumeroMovimiento)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDAsientos"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneReporteAsientosContablesEncabezado(ByVal psNumeroAsiento As String, ByVal psAnioMes As String, ByVal pnNumeroMovimiento As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneReporteAsientoContableEncabezado"
        Dim dbCommand As Common.DbCommand
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroAsiento, psAnioMes, pnNumeroMovimiento)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDAsientos"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneMasientosxNumSolic(ByVal psNumeroSolic As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneMasientosxNumSolic"
        Dim dbCommand As Common.DbCommand
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNumeroSolic)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDTesoreria"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function RealizaBusquedaAsiento(ByVal psCuentaContable As String, ByVal psNumeroAsiento As String, ByVal psNumeroDocumento As String, ByVal pnAnio As Integer, ByVal pnMes As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "RealizaBusquedaAsientos"
        Dim dbCommand As Common.DbCommand
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCuentaContable, psNumeroAsiento, psNumeroDocumento, pnAnio, pnMes)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDAsientos"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function
End Class
