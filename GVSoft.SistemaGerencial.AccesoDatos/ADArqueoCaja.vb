﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADArqueoCaja
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADArqueoCaja"

    '''' <summary>
    '''' Obtiene todos los cheques activos 
    '''' </summary>
    '''' <retorna>Un Objecto base de datos</retorna>
    'Public Shared Function ObtieneNumeroArqueoParametro(ByVal RegistroAgencia As Int16) As IDataReader
    '    Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
    '    Dim sp As String = "ObtieneNumeroArqueoActual"
    '    Dim dbCommand As DbCommand
    '    Try
    '        dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia)
    '        sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

    '        Return objDbSistemaGerencial.ExecuteReader(dbCommand)
    '    Catch ex As Exception
    '        sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
    '        objError.Clase = "ADRecibos"
    '        objError.Metodo = sNombreMetodo
    '        objError.descripcion = ex.Message.ToString()
    '        SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
    '        SDError.IngresarError(objError)
    '        Return Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneNumeroArqueoParametro(ByVal RegistroAgencia As Int16) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneNumeroArqueoActual"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Me retorna cero o el Numero de Arqueos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaExisteArqueos(ByVal RegistroAgencia As Int16) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spValidaPrimerCuadre"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Me retorna cero o el Numero de Arqueos
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaArqueoACerrar(ByVal RegistroAgencia As Integer, ByVal NumeroArqueo As String) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spValidaArqueoExistenteACerrar"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroArqueo, RegistroAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Me el Numero de Arqueos y un numero entero Cero o Uno que verifica si existe Arqueo Abierto
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaArqueoAbierto() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spValidaArqueoAbierto"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Me retorna El saldo final del arqueo anterior y el cual sera el saldo inicial del arqueo actual
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneSaldoInicial(ByVal RegistroAgencia As Int16) As Double
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneSaldoInicialArqueo"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function GenraUltimoConsecutivoParaArqueo(ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal RegistroAgencia As Int16) As Integer
        'Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneUltimoConsecutivoArqueo"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimoArqueoxUsuario(ByVal IdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneUltimoCuadreCajaxUsuario"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Obtiene Informacion del Cuadre de caja guardado el cual se procedera a cerrar
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneInformacionCuadreGuardadoACerrar(ByVal NumeroArqueo As Integer, ByVal IdAgencia As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spOtieneInformacionCuadreGuardadoACerrar"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, NumeroArqueo, IdAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function


    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneDatosEncabezadoArqueoDiarios(ByVal IdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneDatosEncabezadoCuadreDiario"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneFacturasParaCuadreCaja(ByVal pnIdUsuario As Integer, ByVal psFacturaInicial As String, _
                                                         ByVal psFacturaFinal As String, ByVal psFechaInicial As String, _
                                                         ByVal psFechaFinal As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneFacturasParaCuadreCaja"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFechaInicial, psFechaFinal, psFacturaInicial, psFacturaFinal)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Facturas para recalcular el arqueo de caja
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneFacturasParaRecalcularCuadreCaja(ByVal pnIdUsuario As Integer, ByVal psFacturaInicial As String, _
                                                         ByVal psFacturaFinal As String, ByVal pnIdArqueo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneFacturasParaRecalcularCuadreCaja"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFacturaInicial, psFacturaFinal, pnIdArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimerReciboxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtienePrimerNumeroReciboxDia"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFecha)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimeraFacturaxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtienePrimerNumeroFacturaxDia"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFecha)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimeraFactura(ByVal pnIdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtienePrimerNumeroFactura"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimeraFacturaRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtienePrimerNumeroFacturaRecalcular"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, pnIdArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimerRecibo(ByVal pnIdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtienePrimerNumeroRecibo"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtienePrimerReciboRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtienePrimerNumeroReciboRecalcular"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, pnIdArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimaFacturaxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneUltimoNumeroFacturaxDia"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFecha)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimoReciboxFecha(ByVal pnIdUsuario As Integer, ByVal psFecha As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneUltimoNumeroReciboxDia"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFecha)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimaFactura(ByVal pnIdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneUltimoNumeroFactura"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo numero de factura a recalcular
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimaFacturaRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneUltimoNumeroFacturaRecalcular"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, pnIdArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimoRecibo(ByVal pnIdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneUltimoNumeroRecibo"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Obtiene ultimo Recibo para Recalcular
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneUltimoReciboRecalcular(ByVal pnIdUsuario As Integer, ByVal pnIdArqueo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneUltimoNumeroReciboRecalcular"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, pnIdArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene Total de Saldos de facturas y recibos por rango de facturas y recibos de inicio y fin
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtenerSaldoTotalFacturaRecibosCuadre(ByVal pnIdUsuario As Integer, ByVal sPrimerRecibo As String, ByVal sUltimoRecibo As String, ByVal sPrimerFactura As String, ByVal sUltimaFactura As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtenerSaldoTotalFacturaRecibosCuadre"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, sPrimerRecibo, sUltimoRecibo, sPrimerFactura, sUltimaFactura)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneRecibosParaCuadreCaja(ByVal pnIdUsuario As Integer, ByVal psReciboInicial As String, _
                                                         ByVal psReciboFinal As String, ByVal psFechaInicial As String, _
                                                         ByVal psFechaFinal As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneRecibosParaCuadreCaja"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psFechaInicial, psFechaFinal, psReciboInicial, psReciboFinal)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene ultimo cuadre
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneRecibosParaRecalcularCuadreCaja(ByVal pnIdUsuario As Integer, ByVal psReciboInicial As String, _
                                                         ByVal psReciboFinal As String, ByVal pnIdArqueo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneRecibosParaRecalcularCuadreCaja"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdUsuario, psReciboInicial, psReciboFinal, pnIdArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Sub IngresaDetalleArqueo(ByVal objDbSistemaGerencial As Database, ByVal IdCuadre As Integer, ByVal lstDetalleCuadreCaja As List(Of SECuadreCajaDetalle), ByVal objTrans As DbTransaction, ByVal IdAgencia As Integer)
        Dim sp As String = "IngresadetCuadreCaja"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleCuadreCaja.Count - 1
                lstDetalleCuadreCaja(i).IdCuadre = IdCuadre
                lstDetalleCuadreCaja(i).IdSucursal = IdAgencia

                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstDetalleCuadreCaja(i).IdCuadre, lstDetalleCuadreCaja(i).IdMoneda, _
                                                                           lstDetalleCuadreCaja(i).NumeroDocumento, lstDetalleCuadreCaja(i).MontoTotalDocumento, _
                                                                           lstDetalleCuadreCaja(i).TipoDocumento, lstDetalleCuadreCaja(i).IdSucursal, _
                                                                           lstDetalleCuadreCaja(i).IdEstadoDocumento, lstDetalleCuadreCaja(i).FechaDocumento, _
                                                                           lstDetalleCuadreCaja(i).IdUsuarioDocumento)
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
                If lstDetalleCuadreCaja(i).TipoDocumento = "F" Then
                    MarcaFacturaConNumeroArqueo(objDbSistemaGerencial, lstDetalleCuadreCaja(i).NumeroDocumento, lstDetalleCuadreCaja(i).IdCuadre, objTrans)
                Else
                    MarcaReciboConNumeroArqueo(objDbSistemaGerencial, lstDetalleCuadreCaja(i).NumeroDocumento, lstDetalleCuadreCaja(i).IdCuadre, objTrans)
                End If
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleInformacionMonetariaArqueo(ByVal objDbSistemaGerencial As Database, ByVal IdCuadre As Integer, ByVal lstDetalleInformacionMonetariaCuadreCaja As List(Of SEInformacionMonetaria), ByVal objTrans As DbTransaction, ByVal IdAgencia As Integer)
        Dim sp As String = "IngresaDetalleInfoMonetariaCuadreCaja"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleInformacionMonetariaCuadreCaja.Count - 1
                lstDetalleInformacionMonetariaCuadreCaja(i).IdCuadre = IdCuadre
                lstDetalleInformacionMonetariaCuadreCaja(i).IdSucursal = IdAgencia

                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstDetalleInformacionMonetariaCuadreCaja(i).IdCuadre, lstDetalleInformacionMonetariaCuadreCaja(i).IdMoneda, _
                                                                           lstDetalleInformacionMonetariaCuadreCaja(i).IdSucursal, lstDetalleInformacionMonetariaCuadreCaja(i).DenominacionBillete, _
                                                                           lstDetalleInformacionMonetariaCuadreCaja(i).Cantidad, lstDetalleInformacionMonetariaCuadreCaja(i).Total)
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMaestroArqueo(ByVal objDbSistemaGerencial As Database, ByVal IdCuadre As Integer, ByVal objMaestroArqueo As SECuadreCajaEncabezado, ByVal objTrans As DbTransaction)
        Dim sp As String = "IngresaMaestroCuadreCaja"
        Dim dbCommand As DbCommand = Nothing
        Try
            objMaestroArqueo.IdCuadre = IdCuadre
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroArqueo.IdCuadre, objMaestroArqueo.IdMoneda, _
                                                                      objMaestroArqueo.IdUsuario, objMaestroArqueo.IdSucursal, objMaestroArqueo.NumeroCuadre, _
                                                                      objMaestroArqueo.FechaInicioCuadre, objMaestroArqueo.FechaCuadreFin, _
                                                                      objMaestroArqueo.FacturaCreditoInicial, objMaestroArqueo.FacturaCreditoFinal, _
                                                                      objMaestroArqueo.FacturaContadoInicial, objMaestroArqueo.FacturaContadoFinal, _
                                                                      objMaestroArqueo.ReciboInicialCredito, objMaestroArqueo.ReciboFinalCredito, _
                                                                      objMaestroArqueo.ReciboInicialDebito, objMaestroArqueo.ReciboFinalDebito, _
                                                                      objMaestroArqueo.SaldoInicial, objMaestroArqueo.OtrosIngresos, _
                                                                      objMaestroArqueo.SobranteCaja, objMaestroArqueo.MontoTotalVentas, _
                                                                      objMaestroArqueo.MontoTotalRecibos, objMaestroArqueo.MontoGastos, _
                                                                      objMaestroArqueo.MontoDepositos, objMaestroArqueo.MontoTotalDineroCaja, objMaestroArqueo.TotalIngresos, _
                                                                      objMaestroArqueo.TotalEgresos, objMaestroArqueo.SaldoFinal, _
                                                                      objMaestroArqueo.Tasa, objMaestroArqueo.Observacion, _
                                                                      objMaestroArqueo.IdEstado, objMaestroArqueo.IdUsuarioIngresa, objMaestroArqueo.IdUltimoCuadre)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub EliminarArqueoDeImportacion(ByVal IdCuadre As Integer)
        Dim sp As String = "spQuitarNumeroArqueoImportarExistente"
        Dim dbCommand As DbCommand = Nothing
        Dim objDbSistemaGerencial As Database = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCuadre)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub EliminarDetalleArqueo(ByVal objDbSistemaGerencial As Database, ByVal IdCuadre As Integer, ByVal objTrans As DbTransaction)
        Dim sp As String = "spEliminarDetallesArqueoCaja"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCuadre)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub ModificarMaestroArqueo(ByVal objDbSistemaGerencial As Database, ByVal objMaestroArqueo As SECuadreCajaEncabezado, ByVal objTrans As DbTransaction)
        Dim sp As String = "spModificaMaestroCuadreCaja"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroArqueo.IdCuadre, objMaestroArqueo.FechaCuadreFin, _
                                                                      objMaestroArqueo.FacturaContadoInicial, objMaestroArqueo.FacturaCreditoInicial, _
                                                                      objMaestroArqueo.ReciboInicialCredito, objMaestroArqueo.ReciboInicialDebito, _
                                                                      objMaestroArqueo.FacturaCreditoFinal, objMaestroArqueo.FacturaContadoFinal, _
                                                                      objMaestroArqueo.ReciboFinalCredito, objMaestroArqueo.ReciboFinalDebito, _
                                                                      objMaestroArqueo.OtrosIngresos, objMaestroArqueo.SobranteCaja, _
                                                                      objMaestroArqueo.MontoTotalVentas, objMaestroArqueo.MontoTotalRecibos, _
                                                                      objMaestroArqueo.MontoGastos, objMaestroArqueo.MontoDepositos, _
                                                                      objMaestroArqueo.MontoTotalDineroCaja, objMaestroArqueo.TotalIngresos, _
                                                                      objMaestroArqueo.TotalEgresos, objMaestroArqueo.SaldoFinal, _
                                                                      objMaestroArqueo.Tasa, objMaestroArqueo.Observacion, _
                                                                      objMaestroArqueo.IdUsuarioModifica)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub MarcaFacturaConNumeroArqueo(ByVal objDbSistemaGerencial As Database, ByVal psNumeroFactura As String, ByVal pnIdCuadre As Integer, ByVal objTrans As DbTransaction)
        Dim sp As String = "MarcaFacturaConNumeroCuadre"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdCuadre, psNumeroFactura)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub MarcaReciboConNumeroArqueo(ByVal objDbSistemaGerencial As Database, ByVal psNumeroRecibo As String, ByVal pnIdCuadre As Integer, ByVal objTrans As DbTransaction)
        Dim sp As String = "MarcaReciboConNumeroCuadre"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdCuadre, psNumeroRecibo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function IngresaCuadreCajaCompleta(ByVal RegistroAgencia As Int16, ByVal objMaestroArqueo As SECuadreCajaEncabezado, _
                                                ByVal lstDetalleArqueo As List(Of SECuadreCajaDetalle), _
                                                ByVal lstDetalleInformacionMonetariaArqueo As List(Of SEInformacionMonetaria)) As Integer
        Dim objConexion As Database
        Dim transaction1 As DbTransaction
        Dim IdCuadre As Integer = 0
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    IdCuadre = GenraUltimoConsecutivoParaArqueo(objConexion, transaction1, RegistroAgencia)
                    IngresaMaestroArqueo(objConexion, IdCuadre, objMaestroArqueo, transaction1)
                    IngresaDetalleInformacionMonetariaArqueo(objConexion, IdCuadre, lstDetalleInformacionMonetariaArqueo, transaction1, RegistroAgencia)
                    IngresaDetalleArqueo(objConexion, IdCuadre, lstDetalleArqueo, transaction1, RegistroAgencia)
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return IdCuadre
    End Function

    Public Shared Function RecalcularCuadreCajaCompleto(ByVal RegistroAgencia As Int16, ByVal objMaestroArqueo As SECuadreCajaEncabezado, _
                                                ByVal lstDetalleArqueo As List(Of SECuadreCajaDetalle), _
                                                ByVal lstDetalleInformacionMonetariaArqueo As List(Of SEInformacionMonetaria)) As Integer
        Dim objConexion As Database
        Dim transaction1 As DbTransaction
        Dim IdCuadre As Integer = 0
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    IdCuadre = objMaestroArqueo.IdCuadre
                    EliminarDetalleArqueo(objConexion, IdCuadre, transaction1)
                    ModificarMaestroArqueo(objConexion, objMaestroArqueo, transaction1)
                    IngresaDetalleInformacionMonetariaArqueo(objConexion, IdCuadre, lstDetalleInformacionMonetariaArqueo, transaction1, RegistroAgencia)
                    IngresaDetalleArqueo(objConexion, IdCuadre, lstDetalleArqueo, transaction1, RegistroAgencia)
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return IdCuadre
    End Function

    Public Shared Sub CerrarCuadreCaja(ByVal RegistroAgencia As Int16, ByVal IdCuadre As Integer, ByVal IdUsuarioModifica As Integer)
        Dim sp As String = "spCerrarCuadreCaja"
        Dim objConexion As Database
        Dim dbCommand As DbCommand = Nothing

        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objConexion.GetStoredProcCommand(sp, IdCuadre, RegistroAgencia, IdUsuarioModifica)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objConexion.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try

    End Sub

End Class
