﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class ADProforma
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "GVSoft.SistemaGerencial.AccesoDatos.ADProforma"

    Public Shared Function ObtieneNumeroProformaParametro(ByVal RegistroAgencia As Int16, ByVal Serie As String, ByVal TipoFact As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneParametrosNumeroProforma"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, Serie, TipoFact)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function GenraUltimoConsecutivoProforma(ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal RegistroAgencia As Int16) As Integer
        'Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneUltimoConsecutivoProforma"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteScalar(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    Public Shared Function IngresaTransaccionProforma(ByVal RegistroAgencia As Int16, ByVal objMaestroProforma As SEMaestroProforma, _
                                                ByVal lstDetalleProforma As List(Of SEDetalleProforma), ByVal Serie As String) As Integer
        Dim objConexion As Database
        Dim transaction1 As DbTransaction
        Dim IdProforma As Integer = 0
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    IdProforma = GenraUltimoConsecutivoProforma(objConexion, transaction1, RegistroAgencia)
                    IngresaMaestroProforma(objConexion, IdProforma, objMaestroProforma, transaction1)
                    IngresaDetalleProforma(objConexion, IdProforma, lstDetalleProforma, transaction1)
                    ActualizarNumeroProforma(RegistroAgencia, objMaestroProforma.TipoFactura, objConexion, transaction1, Serie)
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return IdProforma
    End Function

    Public Shared Sub IngresaMaestroProforma(ByVal objDbSistemaGerencial As Database, ByVal IdProforma As Integer, ByVal objMaestroProforma As SEMaestroProforma, ByVal objTrans As DbTransaction)
        Dim sp As String = "spIngresarMaestroProforma"
        Dim dbCommand As DbCommand = Nothing
        Try
            objMaestroProforma.IdProforma = IdProforma
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroProforma.IdProforma, objMaestroProforma.NumeroProforma, _
                                                                      objMaestroProforma.NumFechaIngreso, objMaestroProforma.FechaIngreso, objMaestroProforma.IdVendedor, _
                                                                      objMaestroProforma.IdCliente, objMaestroProforma.Cliente, objMaestroProforma.Direccion, _
                                                                      objMaestroProforma.SubTotal, objMaestroProforma.SubTotalCosto, _
                                                                      objMaestroProforma.Impuesto, objMaestroProforma.Retencion, _
                                                                      objMaestroProforma.Total, objMaestroProforma.IdUsuario, _
                                                                      objMaestroProforma.IdSucursal, objMaestroProforma.IdEstado, _
                                                                      objMaestroProforma.Impreso, objMaestroProforma.IdDepartamento, _
                                                                      objMaestroProforma.IdNegocio, objMaestroProforma.TipoFactura, _
                                                                      objMaestroProforma.DiasCredito, objMaestroProforma.TipoCambio, objMaestroProforma.NumeroArqueo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleProforma(ByVal objDbSistemaGerencial As Database, ByVal IdProforma As Integer, ByVal lstDetalleProforma As List(Of SEDetalleProforma), ByVal objTrans As DbTransaction)
        Dim sp As String = "spIngresarDetalleProforma"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleProforma.Count - 1
                lstDetalleProforma(i).IdProforma = IdProforma

                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, lstDetalleProforma(i).IdProforma, lstDetalleProforma(i).IdProducto,
                                                                           lstDetalleProforma(i).Cantidad, lstDetalleProforma(i).PrecioCosto,
                                                                           lstDetalleProforma(i).Precio, lstDetalleProforma(i).Valor,
                                                                           lstDetalleProforma(i).Igv, lstDetalleProforma(i).EsBonificado)
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub ActualizarNumeroProforma(ByVal RegistroAgencia As Int16, ByVal TipoFact As Integer, ByVal objDbSistemaGerencial As Database, ByVal objTrans As DbTransaction, ByVal Serie As String)
        Dim sp As String = "spActualizarNumProforma"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, RegistroAgencia, TipoFact, Serie)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try

    End Sub


End Class
