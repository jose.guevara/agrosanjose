﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Imports System.Data.Common

Public Class ADCierrePeriodo
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADCierrePeriodo"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub ProcesaPreCierre(ByVal psFechaInicio As String, ByVal psFechaFin As String)
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spGeneraAsientoCierrePeriodo"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFechaInicio, psFechaFin)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
