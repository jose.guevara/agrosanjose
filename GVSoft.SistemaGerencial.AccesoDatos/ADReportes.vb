﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class ADReportes
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneCatReportesxModuloyUsuario(ByVal IdUsuario As Integer, ByVal IdModulo As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneCatReportesxModuloyUsuario"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdUsuario, IdModulo)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADReporetes"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub EliminaReportesxUsuarios(ByVal IdRegistroUsuario As Integer)
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "EliminaReportesxUsuario"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdRegistroUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADUsuarios"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneModulosReportexUsuario(ByVal IdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneModulosDeReportesxUsuario"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADReportes"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneReportexModuloyUsuario(ByVal IdModulo As Integer, ByVal IdUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneReportesxUsuarioYModuloTodos"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdModulo, IdUsuario)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADReportes"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneReportexCuenta(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psCuentaInicio As String, ByVal psCuentaFin As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneReportexCuenta"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFechaInicio, psFechaFin, psCuentaInicio, psCuentaFin)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDReporte"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneReportexCuentaListado(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psCuentaInicio As String, ByVal psCuentaFin As String) As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spObtieneReportexCuentaLista"
        Dim dbCommand As DbCommand = Nothing
        Dim dtDatos As DataTable = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFechaInicio, psFechaFin, psCuentaInicio, psCuentaFin)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            dtDatos = SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDReporte"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return dtDatos
        End Try
        Return dtDatos
    End Function

    ''' <summary>
    ''' Obtiene catalogo de Reportes segun el modulo y el usuario que se loguea
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ReporteFacturasConsecutivas(ByVal psFechaInicio As String, ByVal psFechaFin As String, ByVal psSucursal As String,
                                                       ByVal psTipoFactura As String, ByVal psVendedor As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "rptVentasxFacturasConsecutiva"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFechaInicio, psFechaFin, psSucursal, psTipoFactura, psVendedor)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADReporetes"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function
End Class
