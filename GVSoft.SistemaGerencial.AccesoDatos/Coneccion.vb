﻿Imports System.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports GVSoft.SistemaGerencial.Utilidades

Public Class Coneccion

    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function CrearObjectoBaseDatos() As Database
        Dim dbSistemaGerencial As Database = Nothing
        Try
            'dbSistemaGerencial = DatabaseFactory.CreateDatabase("SistemaGerencial")
            dbSistemaGerencial = New DatabaseProviderFactory().Create("SistemaGerencial")

        Catch ex As Exception
            dbSistemaGerencial = Nothing
        End Try
        Return dbSistemaGerencial
    End Function

    ''' <summary>
    ''' Crea un Objecto Database
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtenerCadenaDeConeccion() As String
        Dim dbSistemaGerencial As Database = Nothing
        Dim lsCadenaDeConeccion As String = String.Empty
        Try
            'dbSistemaGerencial = DatabaseFactory.CreateDatabase("SistemaGerencial")
            dbSistemaGerencial = New DatabaseProviderFactory().Create("SistemaGerencial")

            lsCadenaDeConeccion = SUConversiones.ConvierteAString(dbSistemaGerencial.ConnectionString)
        Catch ex As Exception
            lsCadenaDeConeccion = String.Empty
        End Try
        Return lsCadenaDeConeccion
    End Function
End Class
