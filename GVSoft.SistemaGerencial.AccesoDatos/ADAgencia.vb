﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADAgencia
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciaDefault() As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spObtieneSucursalDefault"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteAInt(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciaDefaultDr() As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spObtieneSucursalDefaultTodo"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function



    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciasxUsuarioTodos(ByVal IdRegistroUsuario As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneAgenciasxUsuarioTodos"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdRegistroUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciaTodas() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneAgenciasTodos"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciaxCodigo(ByVal IdSucursal As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spObtieneDatosSucursal"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdSucursal)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciaCasaMatriz() As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneAgenciasCasaMatriz"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneAgenciasxUsuarioExcluyendoParametro(ByVal pnIdAgencia As Integer) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneAgenciasxUsuarioExcluyendoParametro"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdAgencia)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADAgencia"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
End Class
