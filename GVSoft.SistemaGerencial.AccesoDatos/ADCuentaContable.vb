﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADCuentaContable
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneCuentaContableTodos() As DataTable
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "ObtieneCuentasContablesTodas"
        Dim dbCommand As DbCommand
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteDataSet(dbCommand).Tables(0)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "ADCuentaContable"
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Return Nothing
        End Try
    End Function

    Public Shared Function EliminaCuentaContable(ByVal psCuentaContable As String) As Integer
        Dim objDbSistemaGerencial As Database = Coneccion.CrearObjectoBaseDatos()
        Dim sp As String = "spEliminaCuentaContable"
        Dim dbCommand As DbCommand = Nothing
        Dim lnRespuesta As Integer = 0
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psCuentaContable)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            lnRespuesta = objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = "SDCuentaContable"
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
            Return lnRespuesta
        End Try
        Return lnRespuesta
    End Function
End Class
