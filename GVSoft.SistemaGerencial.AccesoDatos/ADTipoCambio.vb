﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADTipoCambio
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADTipoCambio"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneTipoCambioDelDia() As Decimal
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneTasaCambioDelDia"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteADecimal(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneTipoCambio(ByVal psFecha As String) As Double
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneTasaCambio"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFecha)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteADouble(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneTipoCambioTodas() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneTasaCambioTodas"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function


    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub IngresaTipoCambio(ByVal psFecha As String, ByVal pnFecha As Integer, ByVal pnTasaCambio As Double)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "IngresaTipoCambioOficial"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFecha, pnTasaCambio, pnFecha)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteScalar(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
End Class
