﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades

Public Class ADReciboProveedor
    Private Shared sSentenciaSql = String.Empty
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty
    Private Shared NombreClase As String = "ADADReciboProveedor"

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneInformacionRecibo(ByVal IdProveedor As Integer, ByVal NumeroRecibo As String) As IDataReader
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneDetalleReciboxNumeroReciboProveedor"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdProveedor, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return objDbSistemaGerencial.ExecuteReader(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneInformacionReciboDT(ByVal IdProveedor As Integer, ByVal NumeroRecibo As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneDetalleReciboxNumeroReciboProveedor"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdProveedor, NumeroRecibo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function
    Public Shared Function IngresaReciboProveedor(ByVal objReciboProveedor As SEReciboProveedor, ByVal lstDetalleReciboProveedor As List(Of SEDetalleReciboProveedor)) As Integer
        Dim objConexion As Database
        Dim transaction1 As DbTransaction
        Dim IdReciboProveedor As Integer = 0
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    'IdReciboProveedor = GenraUltimoConsecutivoParaArqueo(objConexion, transaction1, RegistroAgencia)
                    'IngresaMaestroArqueo(objConexion, IdCuadre, objMaestroArqueo, transaction1)
                    'IngresaDetalleInformacionMonetariaArqueo(objConexion, IdCuadre, lstDetalleInformacionMonetariaArqueo, transaction1)
                    'IngresaDetalleArqueo(objConexion, IdCuadre, lstDetalleArqueo, transaction1)
                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
        Return IdReciboProveedor
    End Function

    Public Shared Sub IngresaMaestroReciboProveedor(ByVal objDbSistemaGerencial As Database, ByVal IdCuadre As Integer, ByVal objMaestroArqueo As SECuadreCajaEncabezado, ByVal objTrans As DbTransaction)
        Dim sp As String = "IngresaMaestroCuadreCaja"
        Dim dbCommand As DbCommand = Nothing
        Try
            objMaestroArqueo.IdCuadre = IdCuadre
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroArqueo.IdCuadre, objMaestroArqueo.IdMoneda, _
                                                                      objMaestroArqueo.IdUsuario, objMaestroArqueo.IdSucursal, _
                                                                      objMaestroArqueo.FechaInicioCuadre, objMaestroArqueo.FechaCuadreFin, _
                                                                      objMaestroArqueo.FacturaCreditoInicial, objMaestroArqueo.FacturaCreditoFinal, _
                                                                      objMaestroArqueo.FacturaContadoInicial, objMaestroArqueo.FacturaContadoFinal, _
                                                                      objMaestroArqueo.ReciboInicialCredito, objMaestroArqueo.ReciboFinalCredito, _
                                                                      objMaestroArqueo.ReciboInicialDebito, objMaestroArqueo.ReciboFinalDebito, _
                                                                      objMaestroArqueo.SaldoInicial, objMaestroArqueo.MontoTotalVentas, _
                                                                      objMaestroArqueo.MontoTotalRecibos, objMaestroArqueo.MontoGastos, _
                                                                      objMaestroArqueo.MontoDepositos, objMaestroArqueo.MontoTotalDineroCaja, objMaestroArqueo.TotalIngresos, _
                                                                      objMaestroArqueo.TotalEgresos, objMaestroArqueo.SaldoFinal, _
                                                                      objMaestroArqueo.Tasa, objMaestroArqueo.Observacion, _
                                                                      objMaestroArqueo.IdEstado, objMaestroArqueo.IdUsuarioIngresa, objMaestroArqueo.IdUltimoCuadre)
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = NombreClase
            objError.Sentencia = sSentenciaSql
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

End Class
