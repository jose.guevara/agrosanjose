﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Data
Imports System.Data.Common
Imports GVSoft.SistemaGerencial.Entidades
Imports GVSoft.SistemaGerencial.Utilidades
Public Class ADCompra

    Private Shared sSentenciaSql = String.Empty
    Private Shared sNombreClase = "ADCompra"
    Private Shared objError As SEError = New SEError()
    Private Shared sNombreMetodo As String = String.Empty


    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaComprasImportar(ByVal psFactura As String, ByVal pnIdProveedor As Integer) As Integer
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "spValidaCompraImportar"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psFactura, pnIdProveedor)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUConversiones.ConvierteAInt(objDbSistemaGerencial.ExecuteScalar(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ValidaDatosImportados(ByVal pnAccion As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ImportarCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnAccion)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneComprasAImportar() As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneComprasAImportar"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneComprasAImportar(ByVal IdCompra As Integer) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneComprasAImportarxIdCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, IdCompra)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub IngresaRegistroEnArchivosImportarCompra(ByVal psNombreArchivo As String, ByVal psFechaIngreso As String, ByVal pnFechaIngresoNum As Integer, pnIdUsuario As Integer)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "IngresaRegistroEnArchivosImportarCompras"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNombreArchivo, psFechaIngreso, pnFechaIngresoNum, pnIdUsuario)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Function IngresaMaestroCompra(ByVal objDbSistemaGerencial As Database, ByVal objMaestroCompra As SEEncabezadoCompra, ByVal objTrans As DbTransaction) As Integer
        Dim sp As String = "GrabaComprasEnc"
        Dim dbCommand As DbCommand = Nothing
        Try
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, objMaestroCompra.registro, objMaestroCompra.numero,
                                                                      objMaestroCompra.numfechaing, objMaestroCompra.fechaingreso,
                                                                      objMaestroCompra.vendregistro, objMaestroCompra.cliregistro,
                                                                      objMaestroCompra.clinombre, objMaestroCompra.subtotal,
                                                                      objMaestroCompra.impuesto, objMaestroCompra.retencion,
                                                                      objMaestroCompra.total, objMaestroCompra.userregistro,
                                                                      objMaestroCompra.agenregistro, objMaestroCompra.impreso,
                                                                      objMaestroCompra.deptregistro, objMaestroCompra.negregistro,
                                                                      objMaestroCompra.tipoCompra, objMaestroCompra.diascredito,
                                                                      objMaestroCompra.tipocambio, objMaestroCompra.status, objMaestroCompra.IdPedido)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
            Return SUConversiones.ConvierteAInt(objDbSistemaGerencial.ExecuteScalar(dbCommand, objTrans))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub IngresaDetalleImportarCompra(ByVal registro As Long, ByVal numero As String, ByVal fechaingresoNum As Integer, ByVal fechaingreso As String,
                                                    ByVal vendcodigo As String, ByVal clicodigo As String, ByVal clinombre As String, ByVal subtotal As Double,
                                                    ByVal impuesto As Double, ByVal retencion As Double, ByVal total As Double, ByVal codusuario As String,
                                                    ByVal agencodigo As String, ByVal status As Integer, ByVal impreso As Integer, ByVal tipoCompra As Integer,
                                                    ByVal diascredito As Integer, ByVal numfechaanul As Integer, ByVal prodcodigo As String, ByVal proddescrip As String,
                                                    ByVal cantidad As Integer, ByVal precio As Double, ByVal valor As Double, ByVal iva As Double, ByVal EsBonificacion As Integer,
                                                    ByVal IdProveedor As String, ByVal registrodet As Long, ByVal registroerror As String)
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "IngresaDetalleImportarCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, registro, numero, fechaingresoNum, fechaingreso, vendcodigo, clicodigo, clinombre, subtotal,
                                                                       impuesto, retencion, total, codusuario, agencodigo, status, impreso, tipoCompra, diascredito,
                                                                       numfechaanul, prodcodigo, proddescrip, cantidad, precio, valor, iva, EsBonificacion,
                                                                       IdProveedor, registrodet, registroerror)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaDetalleCompra(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleCompra As List(Of SEDetalleCompra),
                                           ByVal objTrans As DbTransaction, ByVal IdAgencia As Integer, ByVal NumFechaIng As Integer,
                                           ByVal strNumero As String, ByVal pnIdProveedor As Integer, ByVal pnIdPedido As Integer)
        Dim sp As String = "GrabaComprasDet"
        Dim dbCommand As DbCommand = Nothing
        Try
            For i As Int32 = 0 To lstDetalleCompra.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, pnIdPedido, lstDetalleCompra(i).prodregistro,
                                                                           lstDetalleCompra(i).cantidad, lstDetalleCompra(i).precio,
                                                                           lstDetalleCompra(i).valor, lstDetalleCompra(i).igv,
                                                                           IdAgencia, NumFechaIng, strNumero, lstDetalleCompra(i).EsBonificacion,
                                                                           pnIdProveedor)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaTransaccionCompra(ByVal objMaestroCompra As SEEncabezadoCompra, ByVal lstDetalleCompra As List(Of SEDetalleCompra), ByVal intTipoCompra As Integer)
        Dim objConexion As Database = Nothing
        Dim transaction1 As DbTransaction = Nothing
        Dim tipoMov As String = String.Empty
        Dim lnIdPedido As Integer = 0
        Try
            objConexion = Coneccion.CrearObjectoBaseDatos()
            Using connection1 As DbConnection = objConexion.CreateConnection()
                connection1.Open()
                transaction1 = connection1.BeginTransaction()
                Try
                    'If intTipoCompra = 3 Or intTipoCompra = 6 Then
                    If intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Then
                        tipoMov = "AC"
                        objMaestroCompra.fechaingreso = Format(Now, "dd-MMM-yyyy")
                    Else
                        tipoMov = "EC"
                    End If

                    'If intTipoCompra = 1 Or intTipoCompra = 2 Or intTipoCompra = 4 Or intTipoCompra = 5 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                    If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CREDITO Or
                        intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_IMPRIMIR_CONTADO Or
                        intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Or
                        intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Then
                        lnIdPedido = IngresaMaestroCompra(objConexion, objMaestroCompra, transaction1)
                    End If
                    'If intTipoCompra = 3 Or intTipoCompra = 6 Then
                    'If intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CONTADO Or intTipoCompra = ENTipoCompra.COMPRA_ANULAR_CREDITO Then
                    '    IngresaMaestroCompra(objConexion, objMaestroCompra, transaction1)
                    'End If
                    'If intTipoCompra = 1 Or intTipoCompra = 4 Or intTipoCompra = 7 Or intTipoCompra = 8 Then
                    If intTipoCompra = ENTipoCompra.COMPRA_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_CONTADO Or
                       intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CREDITO Or intTipoCompra = ENTipoCompra.COMPRA_MANUAL_CONTADO Then
                        IngresaDetalleCompra(objConexion, lstDetalleCompra, transaction1, objMaestroCompra.agenregistro, objMaestroCompra.numfechaing, objMaestroCompra.numero, objMaestroCompra.cliregistro, lnIdPedido)
                    End If
                    'If intTipoCompra <> 9 And intTipoCompra <> 10 Then
                    If intTipoCompra <> ENTipoCompra.COMPRA_CONSULTAR_CONTADO And intTipoCompra <> ENTipoCompra.COMPRA_CONSULTAR_CREDITO Then
                        IngresaMovimientoProducto(objConexion, lstDetalleCompra, transaction1, objMaestroCompra, tipoMov)
                    End If

                    transaction1.Commit()
                Catch ex As Exception
                    transaction1.Rollback()
                    Throw ex
                End Try
            End Using
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    Public Shared Sub IngresaMovimientoProducto(ByVal objDbSistemaGerencial As Database, ByVal lstDetalleCompra As List(Of SEDetalleCompra),
                                                ByVal objTrans As DbTransaction, ByVal objCompra As SEEncabezadoCompra, ByVal strTipoMov As String)
        Dim sp As String = "sp_IngProductoMovim"
        Dim lsObservacion As String = String.Empty
        Dim dbCommand As DbCommand = Nothing
        Try
            sSentenciaSql = String.Empty
            For i As Int32 = 0 To lstDetalleCompra.Count - 1
                dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, 0, objCompra.numfechaing, Format(Now, "Hmmss"), objCompra.agenregistro,
                                                                           lstDetalleCompra(i).prodregistro, objCompra.fechaingreso,
                                                                           objCompra.numero, strTipoMov, "COR", objCompra.total,
                                                                           lstDetalleCompra(i).cantidad, lstDetalleCompra(i).precio, lstDetalleCompra(i).valor, "", "", objCompra.userregistro,
                                                                           0, 0, 0, 1)
                sSentenciaSql = String.Empty
                sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)
                objDbSistemaGerencial.ExecuteNonQuery(dbCommand, objTrans)
            Next
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub
    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Sub LimpiarArchivosImportarCompra()
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "LimpiarArchivosImportarCompras"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            objDbSistemaGerencial.ExecuteNonQuery(dbCommand)
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene todos los cheques activos 
    ''' </summary>
    ''' <retorna>Un Objecto base de datos</retorna>
    Public Shared Function ObtieneArchivoComprasAImportar(ByVal psNombreArchivo As String) As DataTable
        Dim objDbSistemaGerencial As Database = Nothing
        Dim sp As String = "ObtieneArchivosCompra"
        Dim dbCommand As DbCommand = Nothing
        Try
            objDbSistemaGerencial = Coneccion.CrearObjectoBaseDatos()
            dbCommand = objDbSistemaGerencial.GetStoredProcCommand(sp, psNombreArchivo)
            sSentenciaSql = String.Empty
            sSentenciaSql = SUFunciones.ObtieneSentenciaSQL(dbCommand)

            Return SUFunciones.ObtieneDataTableDeDataSet(objDbSistemaGerencial.ExecuteDataSet(dbCommand))
        Catch ex As Exception
            sNombreMetodo = (New System.Diagnostics.StackFrame().GetMethod().Name.Trim())
            objError.Clase = sNombreClase
            objError.Metodo = sNombreMetodo
            objError.Sentencia = sSentenciaSql
            objError.descripcion = ex.Message.ToString()
            SULogs.EscribeLog(objError.Clase, sNombreMetodo, sSentenciaSql, ex.Message.ToString(), EventLogEntryType.Error)
            SDError.IngresarError(objError)
            Throw ex
        End Try
    End Function

End Class
